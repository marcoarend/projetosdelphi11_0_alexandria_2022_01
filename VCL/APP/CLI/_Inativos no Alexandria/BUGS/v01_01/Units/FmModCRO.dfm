object FmOSRapida: TFmOSRapida
  Left = 339
  Top = 185
  Caption = 'CRO-AUXIL-000 :: Janela Auxiliar de CRO'
  ClientHeight = 629
  ClientWidth = 812
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 812
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 764
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 716
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 278
        Height = 32
        Caption = 'Janela Auxiliar de CRO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 278
        Height = 32
        Caption = 'Janela Auxiliar de CRO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 278
        Height = 32
        Caption = 'Janela Auxiliar de CRO'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 812
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 812
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 812
        Height = 467
        Align = alClient
        TabOrder = 0
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 812
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 808
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 812
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 666
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 664
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object frxCRO_AUXIL_000_01: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 39951.885535092600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  //MePagina.Visible := <VARF_PAGINAR>;  '
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      'end.')
    Left = 144
    Top = 268
    Datasets = <
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsExecucoes
        DataSetName = 'frxDsExecucoes'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object PageHeader4: TfrxPageHeader
        Height = 68.031525350000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Tempos de Execu'#231#245'es Inv'#225'lidos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 385.512060000000000000
          Top = 45.354360000000000000
          Width = 294.803266770000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tempo')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 302.362400000000000000
          Top = 45.354360000000000000
          Width = 83.149611180000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Escalonamento')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Top = 45.354360000000000000
          Width = 75.590551180000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Localizador')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 75.590600000000000000
          Top = 45.354360000000000000
          Width = 113.385826771653500000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'In'#237'cio')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 188.976500000000000000
          Top = 45.354360000000000000
          Width = 113.385826770000000000
          Height = 22.677165350000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fim')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        Height = 22.677165350000000000
        Top = 147.401670000000000000
        Width = 680.315400000000000000
        DataSet = frxDsExecucoes
        DataSetName = 'frxDsExecucoes'
        RowCount = 0
        object Memo7: TfrxMemoView
          Left = 302.362400000000000000
          Width = 83.149611180000000000
          Height = 22.677165350000000000
          ShowHint = False
          DataField = 'Escalonamento'
          DataSet = frxDsExecucoes
          DataSetName = 'frxDsExecucoes'
          DisplayFormat.FormatStr = '##0;-##0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExecucoes."Escalonamento"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Width = 75.590551180000000000
          Height = 22.677165350000000000
          ShowHint = False
          DataField = 'Codigo'
          DataSet = frxDsExecucoes
          DataSetName = 'frxDsExecucoes'
          DisplayFormat.FormatStr = '##0;-##0; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExecucoes."Codigo"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 385.512060000000000000
          Width = 294.803266770000000000
          Height = 22.677165350000000000
          ShowHint = False
          DataField = 'DIAS_TXT'
          DataSet = frxDsExecucoes
          DataSetName = 'frxDsExecucoes'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsExecucoes."DIAS_TXT"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 75.590600000000000000
          Width = 113.385826770000000000
          Height = 22.677165350000000000
          ShowHint = False
          DataField = 'Inicio'
          DataSet = frxDsExecucoes
          DataSetName = 'frxDsExecucoes'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExecucoes."Inicio"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 188.976500000000000000
          Width = 113.385826770000000000
          Height = 22.677165350000000000
          ShowHint = False
          DataField = 'Fim'
          DataSet = frxDsExecucoes
          DataSetName = 'frxDsExecucoes'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsExecucoes."Fim"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 230.551330000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrExecucoes: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrExecucoesCalcFields
    SQL.Strings = (
      'SELECT cab.Codigo, DtaExeIni Inicio, DtaExeFim Fim,'
      '0.000 Escalonamento,'
      '((UNIX_TIMESTAMP(DtaExeFim) - '
      '  UNIX_TIMESTAMP(DtaExeIni))) / 86400 DIAS'
      'FROM oscab cab'
      'WHERE (DtaExeIni > "1900-01-01"'
      'OR DtaExeFim > "1900-01-01")'
      'AND ('
      '      ('
      '        ((UNIX_TIMESTAMP(DtaExeFim) - '
      '        UNIX_TIMESTAMP(DtaExeIni))) / 86400 > 1'
      '      )'
      '    OR'
      '      ('
      '        ((UNIX_TIMESTAMP(DtaExeFim) - '
      '        UNIX_TIMESTAMP(DtaExeIni)))  < 1'
      '      )'
      ')'
      ''
      'UNION'
      ''
      'SELECT cab.Codigo, ocx.DtHrIni Inicio, ocx.DtHrFim Final,'
      'ocx.Controle + 0.000 Escalonamento,'
      '((UNIX_TIMESTAMP(ocx.DtHrFim) -'
      '  UNIX_TIMESTAMP(ocx.DtHrIni))) / 86400 DIAS'
      'FROM oscabxtr ocx'
      'LEFT JOIN oscab cab ON cab.Codigo=ocx.Codigo'
      'WHERE (ocx.DtHrIni > "1900-01-01"'
      'OR ocx.DtHrFim > "1900-01-01")'
      'AND ('
      '      ('
      '        ((UNIX_TIMESTAMP(ocx.DtHrFim) -'
      '        UNIX_TIMESTAMP(ocx.DtHrIni))) / 86400 > 1'
      '      )'
      '    OR'
      '      ('
      '        ((UNIX_TIMESTAMP(ocx.DtHrFim) -'
      '        UNIX_TIMESTAMP(ocx.DtHrIni)))  < 1'
      '      )'
      ')'
      'ORDER BY DIAS')
    Left = 144
    Top = 172
    object QrExecucoesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrExecucoesInicio: TDateTimeField
      FieldName = 'Inicio'
      Required = True
    end
    object QrExecucoesFim: TDateTimeField
      FieldName = 'Fim'
      Required = True
    end
    object QrExecucoesEscalonamento: TFloatField
      FieldName = 'Escalonamento'
      Required = True
    end
    object QrExecucoesDIAS: TFloatField
      FieldName = 'DIAS'
    end
    object QrExecucoesDIAS_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DIAS_TXT'
      Size = 255
      Calculated = True
    end
  end
  object frxDsExecucoes: TfrxDBDataset
    UserName = 'frxDsExecucoes'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Inicio=Inicio'
      'Fim=Fim'
      'Escalonamento=Escalonamento'
      'DIAS=DIAS'
      'DIAS_TXT=DIAS_TXT')
    DataSet = QrExecucoes
    BCDToCurrency = False
    Left = 144
    Top = 220
  end
end
