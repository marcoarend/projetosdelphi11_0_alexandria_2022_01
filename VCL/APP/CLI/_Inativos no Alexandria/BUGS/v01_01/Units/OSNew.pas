unit OSNew;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, PlannerMonthView, DBPlannerMonthView, Planner, DBPlanner,
  UnOSApp_PF, UnDmkEnums;

type
  TFmOSNew = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    DsSiapterCad: TDataSource;
    QrSiapTerCad: TmySQLQuery;
    QrSiapTerCadCodigo: TIntegerField;
    QrSiapTerCadNome: TWideStringField;
    Label54: TLabel;
    EdOpcao: TdmkEdit;
    Label6: TLabel;
    EdSiapTerCad: TdmkEditCB;
    CBSiapTerCad: TdmkDBLookupComboBox;
    QrPesq: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FGrupo, FCliente: Integer;
    FOQueSel: TOSBugsDuplic;
    FConfirmou: Boolean;
  end;

  var
  FmOSNew: TFmOSNew;

implementation

uses UnMyObjects, Module, DmkDAC_PF;

{$R *.DFM}

procedure TFmOSNew.BtOKClick(Sender: TObject);
var
  ContinuaA1, ContinuaB1, ContinuaA2, ContinuaB2: Boolean;
  Opcao, Lugar: Integer;
  SQLOpcao, SQLLugar: String;
begin
  Opcao := EdOpcao.ValueVariant;
  Lugar := EdSiapTerCad.ValueVariant;
  if EdSiapTerCad.Enabled and EdSiapTerCad.Visible then
  begin
    ContinuaA2 := Lugar <> 0;
    ContinuaA1 := ContinuaA2;
  end else begin
    ContinuaA1 := True;
    //ContinuaA2 := False;
  end;
  //
  if EdOpcao.Enabled and EdOpcao.Visible then
  begin
    ContinuaB2 := Opcao <> 0;
    ContinuaB1 := ContinuaB2;
  end else begin
    ContinuaB1 := True;
    //ContinuaB2 := False;
  end;
  //
  if ContinuaA1 and ContinuaB1 then
  begin
    {
    if ContinuaA2 and ContinuaB2 then
    begin
    }
      if EdOpcao.Enabled then
        SQLOpcao := 'AND Opcao=' + Geral.FF0(Opcao)
      else SQLOpcao := '';
      //
      if EdSiapterCad.Enabled then
        SQLLugar := 'AND SiapTerCad=' + Geral.FF0(Lugar)
      else SQLLugar := '';
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
      'SELECT * ',
      'FROM oscab ',
      'WHERE Grupo=' + Geral.FF0(FGrupo),
      SQLOpcao,
      SQLLugar,
      '']);
      //
      FConfirmou := QrPesq.RecordCount = 0;
    {
    end else
      FConfirmou := True;
    }
    if FConfirmou then
      Close
    else
      Geral.MB_Aviso('Inclus�o cancelada! Registro j� existe!');
  end else Geral.MB_Aviso('Defina o lugar e a op��o!');
end;

procedure TFmOSNew.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSNew.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSNew.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FConfirmou := False;
  //
  UnDmkDAC_PF.AbreQuery(QrSiapTerCad, Dmod.MyDB);
end;

procedure TFmOSNew.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
