unit RMIP_R013;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, mySQLDbTables, frxClass,
  frxDBSet, dmkGeral;

type
  TFmRMIP_R013 = class(TForm)
    Qr013Pesq: TmySQLQuery;
    Qr013PesqCodigo: TIntegerField;
    Qr013PesqQuestaoTyp: TIntegerField;
    Qr013PesqQuestaoCod: TIntegerField;
    Qr013PesqQuestaoExe: TIntegerField;
    Qr013PesqInicio: TDateTimeField;
    Qr013PesqTermino: TDateTimeField;
    Qr013PesqEmprEnti: TIntegerField;
    Qr013PesqTerceiro: TIntegerField;
    Qr013PesqLocal: TIntegerField;
    Qr013PesqCor: TIntegerField;
    Qr013PesqCption: TSmallintField;
    Qr013PesqNome: TWideStringField;
    Qr013PesqNotas: TWideStringField;
    Qr013PesqFatoGeradr: TIntegerField;
    Qr013PesqNO_FatoGeradr: TWideStringField;
    Qr013PesqNO_QuestaoExe: TWideStringField;
    Qr013PesqNO_Lugar: TWideStringField;
    frxDs013Pesq: TfrxDBDataset;
    frxReport013A: TfrxReport;
    Qr013PesqNO_ENT: TWideStringField;
    procedure frxReport013AGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
  public
    { Public declarations }
    FEmpresa, FCliente, FItemRel: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL: String;
    //FMemo1: TMemo;
    procedure GeraImp_Agenda();
    procedure ReopenAgenda();
    procedure frxReport000GetValue(frxReport: TfrxReport;
              const VarName: string; var Value: Variant);
  end;

var
  FmRMIP_R013: TFmRMIP_R013;

implementation

uses Module, UnMyObjects, DmkDAC_PF, ModOS, ModuleGeral, UnDmkProcFunc;

{$R *.dfm}

{ TFmRMIP_R013 }

procedure TFmRMIP_R013.frxReport000GetValue(frxReport: TfrxReport;
  const VarName: string; var Value: Variant);
begin
  if VarName = 'VARF_DESCRI_ATIVI' then
  begin
    case TAgendaQuestao(Qr013PesqQuestaoTyp.Value) of
      qagIndefinido : Value := '';
      qagAvulso     : Value := Qr013PesqNome.Value;
      qagOSBgstrl   : Value := DmModOS.DadosOSBgstrl(Qr013PesqQuestaoCod.Value);
      else            Value := '';
    end;
  end
  else

end;

procedure TFmRMIP_R013.frxReport013AGetValue(const VarName: string;
  var Value: Variant);
begin
  frxReport000GetValue(frxReport013A, VarName, Value);
end;

procedure TFmRMIP_R013.GeraImp_Agenda();
begin
  ReopenAgenda;
  //
  MyObjects.frxDefineDatasets(frxReport013A, [
  DModG.frxDsDono,
  frxDs013Pesq
  ]);
  //
  frxReport013A.Variables['VARF_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport013A.Variables['VARF_DATA']    := FDtaImp;
  frxReport013A.Variables['VARF_CLIENTE'] := QuotedStr(FNO_Cliente);
  frxReport013A.Variables['VARF_TITULO']  := QuotedStr(FNO_REL);
  frxReport013A.Variables['VARF_PERIODO']  := QuotedStr(dmkPF.PeriodoImp(FDtaIni,
    FDtaFim, 0, 0, True, True, False, False, '', ''));
  //
  frxReport013A.Variables['VARF_NO_EMPRESA'] := QuotedStr(FNO_Empresa);
  frxReport013A.Variables['VARF_PARTICIPANTE'] := QuotedStr(FNO_Cliente);
  //
  //MyObjects.frxPrepara(frxReport013A, 'SPs');
end;

procedure TFmRMIP_R013.ReopenAgenda;
var
  Ini, Fim, SQL_Cliente: String;
begin
  Ini := Geral.FDT(FDtaIni, 1);
  Fim := Geral.FDT(FDtaFim, 1);
  if FCliente <> 0 then
    SQL_Cliente := 'AND EntContrat=' + Geral.FF0(FCliente)
  else
    SQL_Cliente := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr013Pesq, Dmod.MyDB, [
  'SELECT eve.Codigo,  ',
  ' ',
  //'0 CtrlParti,  ',
  ' ',
  'eve.QuestaoTyp, eve.QuestaoCod, eve.QuestaoExe,  ',
  'eve.Inicio, eve.Termino, eve.EmprEnti,  ',
  'eve.Terceiro, eve.Local, eve.Cor,  ',
  'eve.Cption, eve.Nome, eve.Notas,  ',
  'eve.FatoGeradr, fge.Nome NO_FatoGeradr,  ',
  'ELT(eve.QuestaoExe + 1, "Indefinido", "Vistoria", "Execu��o", ',
  '"Escalonamento", "Desconhecido") NO_QuestaoExe,  ',
  'stc.Nome NO_Lugar, eve.Ativo, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
  'FROM agendaeve eve  ',
  'LEFT JOIN oscab cab ON Cab.Codigo=eve.QuestaoCod ',
  'LEFT JOIN fatogeradr fge ON fge.Codigo=eve.FatoGeradr ',
  'LEFT JOIN siaptercad stc ON stc.Codigo=eve.Local ',
  'LEFT JOIN entidades ent ON ent.Codigo=eve.Terceiro ',
  'WHERE eve.QuestaoTyp=2 ',
  ' ',
  // 2014-12-22
  //'AND eve.Inicio  BETWEEN "' + Ini + '" AND "' + Fim + ' 23:59:59" ',
  'AND eve.Inicio  >= "' + Ini + '" ',
  // FIM 2014-12-22
  ' ',
  SQL_Cliente,
  '']);
end;

end.
