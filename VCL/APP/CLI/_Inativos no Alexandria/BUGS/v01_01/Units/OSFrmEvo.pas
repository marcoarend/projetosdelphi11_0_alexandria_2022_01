unit OSFrmEvo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkEdit, dmkEditDateTimePicker, dmkMemo,
  Mask, mySQLDbTables, UnDmkEnums;

type
  TFmOSFrmEvo = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Label52: TLabel;
    TPDataHora: TdmkEditDateTimePicker;
    EdDataHora: TdmkEdit;
    Label1: TLabel;
    EdPercFeito: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    MeTexto: TdmkMemo;
    QrOSFrmCab: TmySQLQuery;
    QrOSFrmCabCodigo: TIntegerField;
    QrOSFrmCabControle: TIntegerField;
    QrOSFrmCabConta: TIntegerField;
    QrOSFrmCabNome: TWideStringField;
    DsOSFrmCab: TDataSource;
    GBDados: TGroupBox;
    Label7: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit15: TDBEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo,
    FControle,
    FConta,
    FIDIts: Integer;
    FQrOSSrv, FQrOSFrmCab, FQrOSFrmEvo: TmySQLQuery;
  end;

  var
  FmOSFrmEvo: TFmOSFrmEvo;

implementation

uses UnMyObjects, Module, UMySQLModule, UnOSApp_PF;

{$R *.DFM}

procedure TFmOSFrmEvo.BtOKClick(Sender: TObject);
var
  DataHora, Texto: String;
  Codigo, Controle, Conta, IDIts: Integer;
  PercFeito: Double;
begin
  Codigo         := FCodigo;
  Controle       := FControle;
  Conta          := FConta;
  IDIts          := FIDIts;
  DataHora       := Geral.FDT(TPDataHora.Date, 1) + ' ' + EdDataHora.Text;
  PercFeito      := EdPercFeito.ValueVariant;
  Texto          := MeTexto.Text;
  //
  IDIts := UMyMod.BPGS1I32('osfrmevo', 'IDIts', '', '', tsPos, ImgTipo.SQLType, IDIts);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'osfrmevo', False, [
  'Codigo', 'Controle', 'Conta',
  'DataHora', 'PercFeito', 'Texto'], [
  'IDIts'], [
  Codigo, Controle, Conta,
  DataHora, PercFeito, Texto], [
  IDIts], True) then
  begin
    OSApp_PF.AtualizaPercentualFormulaExecutado(Conta, FControle);
    if FQrOSSrv <> nil then
    begin
      FQrOSSrv.Close;
      FQrOSSrv.Open;
      FQrOSSrv.Locate('Controle', Controle, []);
    end;
    //
    if FQrOSFrmCab <> nil then
    begin
      FQrOSFrmCab.Close;
      FQrOSFrmCab.Open;
      FQrOSFrmCab.Locate('Conta', Conta, []);
    end;
    //
    if FQrOSFrmEvo <> nil then
    begin
      FQrOSFrmEvo.Close;
      FQrOSFrmEvo.Open;
      FQrOSFrmEvo.Locate('IDIts', IDIts, []);
    end;
    //
    Close;
  end;
end;

procedure TFmOSFrmEvo.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSFrmEvo.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSFrmEvo.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOSFrmEvo.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
