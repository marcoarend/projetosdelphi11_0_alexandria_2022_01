unit LugarPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit, dmkDBLookupComboBox,
  dmkEditCB, mySQLDbTables, dmkDBGridZTO;

type
  TFmLugarPesq = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel8: TPanel;
    Label12: TLabel;
    Label31: TLabel;
    Label32: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label38: TLabel;
    Label15: TLabel;
    Label17: TLabel;
    Label112: TLabel;
    EdSCEP: TdmkEdit;
    EdSRua: TdmkEdit;
    EdSNumero: TdmkEdit;
    EdSBairro: TdmkEdit;
    EdSCidade: TdmkEdit;
    EdSUF: TdmkEdit;
    EdSPais: TdmkEdit;
    EdSEndeRef: TdmkEdit;
    EdSTe1: TdmkEdit;
    QrMunici: TmySQLQuery;
    QrMuniciCodigo: TIntegerField;
    QrMuniciNome: TWideStringField;
    DsMunici: TDataSource;
    DsBacen_Pais: TDataSource;
    QrBacen_Pais: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    Label106: TLabel;
    EdSCodMunici: TdmkEditCB;
    CBSCodMunici: TdmkDBLookupComboBox;
    Label109: TLabel;
    EdSCodiPais: TdmkEditCB;
    CBSCodiPais: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdMargemNum: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    QrPesqCliente: TIntegerField;
    QrPesqNO_ENT: TWideStringField;
    QrPesqSRua: TWideStringField;
    QrPesqSNumero: TIntegerField;
    QrPesqSBairro: TWideStringField;
    QrPesqSCidade: TWideStringField;
    QrPesqSUF: TWideStringField;
    QrPesqSCEP: TIntegerField;
    QrPesqSTe1: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure dmkDBGridZTO1DblClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo, FEntidade: Integer;
  end;

  var
  FmLugarPesq: TFmLugarPesq;

implementation

uses UnMyObjects, Module, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TFmLugarPesq.BtOKClick(Sender: TObject);
var
  Numero, Margem, CEP, CodMunici, CodiPais: Integer;
  SRua, SNumero, SBairro, SCidade, SUF, SPais, SEndeRef, CEPx, CEPi, CEPf, SCEP,
  STe1, SCodMunici, SCodiPais: String;
begin
  SRua := Trim(EdSRua.Text);
  if SRua <> '' then
    SRua := 'AND stc.SRua LIKE "%' + SRua + '%"';
  //
  Numero := EdSNumero.ValueVariant;
  if Numero <> 0 then
  begin
    Margem := EdMargemNum.ValueVariant;
    SNumero := Geral.FF0(Numero - Margem);
    if Margem = 0 then
      SNumero := 'AND stc.SNumero=' + SNumero
    else
      SNumero := 'AND stc.SNumero BETWEEN ' +
        SNumero + ' AND ' + Geral.FF0(Numero + Margem);
  end else
    SNumero := '';
  //
  SBairro := Trim(EdSBairro.Text);
  if SBairro <> '' then
    SBairro := 'AND stc.SBairro LIKE "%' + SBairro + '%"';
  //
  SCidade := Trim(EdSCidade.Text);
  if SCidade <> '' then
    SCidade := 'AND stc.SCidade LIKE "%' + SCidade + '%"';
  //
  SUF := Trim(EdSUF.Text);
  if SUF <> '' then
    SUF := 'AND stc.SUF LIKE "%' + SUF + '%"';
  //
  SPais := Trim(EdSPais.Text);
  if SPais <> '' then
    SPais := 'AND stc.SPais LIKE "%' + SPais + '%"';
  //
  SEndeRef := Trim(EdSEndeRef.Text);
  if SEndeRef <> '' then
    SEndeRef := 'AND stc.SEndeRef LIKE "%' + SEndeRef + '%"';
  //
  CEP := EdSCEP.ValueVariant;
  if CEP <> 0 then
  begin
    CEPx := Geral.FF0(CEP);
    if Length(CEPx) = 8 then
      SCEP := 'AND stc.SCEP=' + CEPx
    else
    begin
      CEPi := CEPx;
      CEPf := CEPx;
      while length(CEPi) < 8 do
      begin
        CEPi := CEPi + '0';
        CEPf := CEPf + '9';
      end;
      SCEP := 'AND stc.SCEP BETWEEN ' + CEPi + ' AND ' + CEPf;
    end;
  end else
    SCEP := '';
  //
  STe1 := Trim(EdSTe1.Text);
  if STe1 <> '' then
  begin
    if pos('%', STe1) <> 0 then
      STe1 := 'AND stc.STe1 LIKE "%' + STe1 + '%"'
    else
    begin
      Margem := Length(STe1);
      STe1 := 'AND RIGHT(stc.STe1, ' + Geral.FF0(Margem) + ') = "' + Ste1 + '"';
    end;
  end;
  //
  CodMunici := EdSCodMunici.ValueVariant;
  if CodMunici <> 0 then
    SCodMunici := 'AND stc.SCodMunici=' + Geral.FF0(CodMunici);
  //
  CodiPais := EdSCodiPais.ValueVariant;
  if CodiPais <> 0 then
    SCodiPais := 'AND stc.SCodiPais=' + Geral.FF0(CodiPais);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPesq, Dmod.MyDB, [
  'SELECT stc.Codigo, stc.Nome, stc.Cliente, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
  'stc.SRua, stc.SNumero, stc.SBairro,  ',
  'stc.SCidade, stc.SUF, stc.SCEP, stc.STe1  ',
  'FROM siaptercad stc ',
  'LEFT JOIN entidades ent ON ent.Codigo=stc.Cliente ',
  'WHERE stc.Codigo <> 0 ',
  SRua,
  SNumero,
  SBairro,
  SCidade,
  SUF,
  SPais,
  SEndeRef,
  SCEP,
  STe1,
  SCodMunici,
  SCodiPais,
  '']);
end;

procedure TFmLugarPesq.BtSaidaClick(Sender: TObject);
begin
  FCodigo := 0;
  FEntidade := 0;
  Close;
end;

procedure TFmLugarPesq.dmkDBGridZTO1DblClick(Sender: TObject);
begin
  if (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0) then
  begin
    FCodigo := QrPesqCodigo.Value;
    FEntidade := QrPesqCliente.Value;
    Close;
  end;
end;

procedure TFmLugarPesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmLugarPesq.FormCreate(Sender: TObject);
begin
  FCodigo := 0;
  FEntidade := 0;
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrBacen_Pais, DModG.AllID_DB);
  UnDmkDAC_PF.AbreQuery(QrMunici, DModG.AllID_DB);
end;

procedure TFmLugarPesq.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmLugarPesq.QrPesqAfterOpen(DataSet: TDataSet);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, Geral.FF0(QrPesq.RecordCount) +
  ' registros encontrados!');
end;

procedure TFmLugarPesq.QrPesqBeforeClose(DataSet: TDataSet);
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

end.
