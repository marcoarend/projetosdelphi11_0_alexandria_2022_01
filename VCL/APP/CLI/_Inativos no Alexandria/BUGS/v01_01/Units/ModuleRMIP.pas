unit ModuleRMIP;

interface

uses
  Forms,
  System.SysUtils, System.Classes, Data.DB, mySQLDbTables, UnProjGroup_Consts,
  dmkGeral, UnMyObjects, frxClass, frxDBSet, UnDmkEnums, UnDmkProcFunc,
  UnInternalConsts, frxChart;

type
  TDmRMIP = class(TDataModule)
    QrPrgLstFxa: TmySQLQuery;
    QrPrgLstFxaQtdMax: TFloatField;
  private
    { Private declarations }
  public
    { Public declarations }
    procedure CriaTempTables();
    function  DefineAbrangencia(Cliente: Integer): TRelAbrange;
    function  SQL_FaixaQtd(PrgLstIts: Integer; FldAgrup, SQLPadrao: String): String;
    function  SQL_Funcoes(const PrgLstIts, Funcoes: Integer; var Resposta,
              CorPizza, LeftJoin, OrderBy, GroupBy: String; AgrupaTodasOSs:
              Boolean): Boolean;
    function  SQL_Funcoes2(const PrgLstIts, Funcoes: Integer; var RespCod,
              Resposta, CorPizza, LeftJoin, OrderBy, GroupBy: String): Boolean;
    procedure ReopenOSsAfetadas(Qry: TmySQLQuery; PrgLstIts: Integer;
              LeftJoin, SQL_Extra: String);

    ////////////////////////////////////////////////////////////////////////////

(*
    procedure GeraImp_PizzaUltimaRevisaoPMV(Empresa, Cliente: Integer;
              Qr003A_PrgLstIts: TmySQLQuery; frxReport003A: TfrxReport;
              frxDatasets: array of TfrxDataset);
    procedure GeraImp_PizzaRevisaoPMVPeriodo(Empresa: Integer;
              Qr003A_PrgLstIts: TmySQLQuery; DtaIni, DtaFim: TDateTime;
              frxReport003A: TfrxReport; frxDatasets: array of TfrxDataset);
    procedure Reopen003A_OSPipIts(Localizador, PrgLstIts, Funcoes: Integer;
              DtaIni, DtaFim: TDateTime; Qr003A_OSPipIts, Qr003A_OSs: TmySQLQuery;
              Abrangencia: TRelAbrange; ItemRel: Integer);
    procedure Reopen003A_PrgLstIts(Qr003A_PrgLstIts: TmySQLQuery; Cliente,
              OSCab, Aplicacao: Integer; DtaIni, DtaFim: TDateTime; Abrangencia:
              TRelAbrange);
*)
  end;

var
  DmRMIP: TDmRMIP;

implementation

uses Module, ModuleGeral, DmkDAC_PF, PrgLstIts, CreateBugs, RMIP_Cab;

{$R *.dfm}

(*
////////////////////////////////////////////////////////////////////////////////
Dedetiza��es Mae ativas

SELECT
DATE_ADD(cab.DtaExeFim, INTERVAL fcb.Periodd + 15 DAY) DataFinal,
cab.DtaExeFim, cab.Estatus, fcb.*
FROM osfrmflhcb fcb
LEFT JOIN osfrmcab frc ON fcb.Conta=frc.Conta
LEFT JOIN oscab cab ON fcb.Codigo=cab.Codigo
LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus
WHERE sta.Execucao <> 3
AND (
    cab.DtaExeFim<="1900-01-01"
    OR
    SYSDATE() <
      DATE_ADD(cab.DtaExeFim, INTERVAL fcb.Periodd + 15 DAY)
)
ORDER BY EmisUltDta, Codigo, IDIts

////////////////////////////////////////////////////////////////////////////////

Clientes de Dedetiza��es Mae ativas

SELECT DISTINCT cab.Entidade
FROM osfrmflhcb fcb
LEFT JOIN osfrmcab frc ON fcb.Conta=frc.Conta
LEFT JOIN oscab cab ON fcb.Codigo=cab.Codigo
LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus
WHERE sta.Execucao <> 3
AND (
    cab.DtaExeFim<="1900-01-01"
    OR
    SYSDATE() <
      DATE_ADD(cab.DtaExeFim, INTERVAL fcb.Periodd + 15 DAY)
)

////////////////////////////////////////////////////////////////////////////////
Monitoramento Mae ativas

SELECT
DATE_ADD(cab.DtaExeFim, INTERVAL mon.Periodd + 15 DAY) DataFinal,
cab.DtaExeFim, cab.Estatus, mon.*
FROM osmoncab mon
LEFT JOIN oscab cab ON mon.Codigo=cab.Codigo
LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus
WHERE sta.Execucao <> 3
AND (
    cab.DtaExeFim<="1900-01-01"
    OR
    SYSDATE() <
      DATE_ADD(cab.DtaExeFim, INTERVAL mon.Periodd + 15 DAY)
)

////////////////////////////////////////////////////////////////////////////////
Clientes de Monitoramento Mae ativas


SELECT DISTINCT cab.Entidade
FROM osmoncab mon
LEFT JOIN oscab cab ON mon.Codigo=cab.Codigo
LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus
WHERE sta.Execucao <> 3
AND (
    cab.DtaExeFim<="1900-01-01"
    OR
    SYSDATE() <
      DATE_ADD(cab.DtaExeFim, INTERVAL mon.Periodd + 15 DAY)
)


*)

{ TDmRMIP }

procedure TDmRMIP.CriaTempTables();
begin
//
end;

function TDmRMIP.DefineAbrangencia(Cliente: Integer): TRelAbrange;
begin
  if Cliente <> 0 then
    Result := relAbrangeCliente
  else
    Result := relAbrangeEmpresa;
end;

procedure TDmRMIP.ReopenOSsAfetadas(Qry: TmySQLQuery; PrgLstIts: Integer;
  LeftJoin, SQL_Extra: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT DISTINCT cab.Codigo, cab.Entidade, cab.SiapTerCad, ',
  'cab.Estatus, cab.DtaExeFim, cab.NumContrat, cab.Grupo, ',
  'fge.Nome NO_FatoGeradr, ',
  'sta.Nome NO_ESTATUS, stc.Nome NO_SiapTerCad, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
  LeftJoin,
  'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade ',
  'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr ',
  'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus ',
  'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad ',
  'WHERE opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  SQL_Extra,
  'ORDER BY cab.DtaExeFim',
  '']);
end;

function TDmRMIP.SQL_FaixaQtd(PrgLstIts: Integer; FldAgrup, SQLPadrao: String): String;
var
  Txt: String;
  Ant, Max: Double;
begin
  Result := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrgLstFxa, Dmod.MyDB, [
  'SELECT QtdMax ',
  'FROM prglstfxa ',
  'WHERE Controle=' + Geral.FF0(PrgLstIts),
  'ORDER BY QtdMax ',
  '']);
  Ant := 0;
  if QrPrgLstFxa.RecordCount > 0 then
  begin
    QrPrgLstFxa.First;
    while not QrPrgLstFxa.Eof do
    begin
      Max := QrPrgLstFxaQtdMax.Value;
      if Max = 0 then
        Txt := 'WHEN ' + FldAgrup + ' = 0 then "= 0"'
      else
        Txt := 'WHEN ' + FldAgrup + ' <= ' + FloatToStr(Max) + ' then "> ' +
        FloatToStr(Ant) + ' a ' + FloatToStr(Max) + '"';
      if Result = '' then
        Result := 'CASE ' + Txt
      else
        Result := Result + sLineBreak + Txt;
      //
      Ant := Max;
      QrPrgLstFxa.Next;
    end;
    Result := Result + sLineBreak + 'ELSE "> ' + FloatToStr(Ant) + ' " END';
  end else
    Result := SQLPadrao;
end;

function TDmRMIP.SQL_Funcoes(const PrgLstIts, Funcoes: Integer; var Resposta,
  CorPizza, LeftJoin, OrderBy, GroupBy: String; AgrupaTodasOSs: Boolean): Boolean;
var
  SQL_Qtde, SQLPadrao, FldAgrup, AllOSs: String;
begin
  Result := False;
  if AgrupaTodasOSs then
    AllOSs := ''
  else
    AllOSs := 'cab.DtaExeFim, ';
  //
  case Funcoes of
    //CO_PRG_LST_NENHUMA = 0;
    CO_PRG_LST_BINARIO: // 1
    begin
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO", ',
      'IF(opi.RespBin = 0, pb0.Nome,',
      'IF(opi.RespBin = 1, pb1.Nome,',
      'IF(opi.RespBin = 2, "N�O SE APLICA", "? ? ?")))) Resposta, ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      'IF(opi.RespBin = 0, 5324774/*vermelho excel 2013*/,',
      'IF(opi.RespBin = 1, 4243590/*verde excel 2013*/,',
      'IF(opi.RespBin = 2, 47871/*Amarelo excel 2013*/, ',
      '7686045/*Roxo excel 2013*/)))) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ' + TMeuDB + '.ospipits opi ',
      'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN ' + TMeuDB + '.prgbincad pb0 ON pb0.Codigo=opi.BinarCad0 ',
      'LEFT JOIN ' + TMeuDB + '.prgbincad pb1 ON pb1.Codigo=opi.BinarCad1 ',
      '']);
      GroupBy := 'GROUP BY ' + AllOSs +  'opi.Respondido, opi.RespBin ';
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    CO_PRG_LST_QUANTIT: // 2
    begin
      FldAgrup  := 'opi.RespQtd';
      SQLPadrao := 'LPAD(FORMAT(RespQtd, pli.CasasQtde), 14, " ")';
      SQL_Qtde  := DmRMIP.SQL_FaixaQtd(PrgLstIts, FldAgrup, SQLPadrao);
      //
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO", ',
      SQL_Qtde,
      ') Resposta, ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      '7686045/*Roxo excel 2013*/) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ' + TMeuDB + '.ospipits opi ',
      'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN ' + TMeuDB + '.prglstits pli ON pli.Controle=opi.PrgLstIts',
       '']);
      GroupBy := 'GROUP BY ' + AllOSs +  'opi.Respondido, Resposta ';
      OrderBy := 'ORDER BY Resposta DESC ';
    end;
    CO_PRG_LST_BXAPROD: // 3
    begin
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO",  ',
      // 2014-12-20 INI
      //'gg1.Nome) Resposta,  ',
      'IF((gg1.Nivel1 IS NULL) OR (gg1.Nivel1=0), "Sem adi��o / substitui��o", ',
      'gg1.Nome)) Resposta, ',
      // 2014-12-20 FIM
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      '7686045/*Roxo excel 2013*/) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      // 2014-12-20 INI
(*
      'FROM ' + TMeuDB + '.ospipitspr opr ',
      'LEFT JOIN ' + TMeuDB + '.ospipits opi ON opr.Conta=opi.Conta ',
*)
      'FROM ' + TMeuDB + '.ospipits opi ',
      'LEFT JOIN ' + TMeuDB + '.ospipitspr opr ON opr.Conta=opi.Conta ',
      // 2014-12-20 FIM
      'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON opr.GraGruX=ggx.Controle ',
      'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      '']);
      // 2014-12-20 INI
      //GroupBy := 'GROUP BY opi.Respondido, Resposta ';
      GroupBy := 'GROUP BY ' + AllOSs +  'opi.Respondido, Resposta ';
      // 2014-12-20 FIM
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    CO_PRG_LST_ATRIBUT: // 4
    begin
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO", ',
      'IF(opi.RespAtrIts > 0, pat.Nome, pac.Padrao)) Resposta, ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      'IF(opi.RespAtrIts > 0, pat.CorPie, pac.CorPie)) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ' + TMeuDB + '.ospipits opi ',
      'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN ' + TMeuDB + '.prgatrcad pac ON pac.Codigo=opi.PrgLstCab ',
      'LEFT JOIN ' + TMeuDB + '.prgatrits pat ON pat.Controle=opi.RespAtrIts ',
      '']);
      GroupBy := 'GROUP BY ' + AllOSs +  'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts ';
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    CO_PRG_LST_TXTLIVR: // 5
    begin
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO",  ',
      'IF(LEFT(RespTxtLvr, 512) = "", "RESPOSTA EM BRANCO", ',
      'LEFT(RespTxtLvr, 512))) Resposta,  ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      '7686045/*Roxo excel 2013*/) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ' + TMeuDB + '.ospipits opi ',
      'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=opi.Codigo ',
      '']);
      GroupBy := 'GROUP BY ' + AllOSs +  'opi.Respondido, Resposta ';
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    //
    else
    begin
      Geral.MB_Erro(
      '"Funcoes" nao implementado em "TDmRMIP.SQL_Funcoes()"' +
      sLineBreak +
      'Funcoes = ' + Geral.FF0(Funcoes));
      EXIT;
    end;
  end;
  Result := True;
end;

function TDmRMIP.SQL_Funcoes2(const PrgLstIts, Funcoes: Integer; var RespCod,
  Resposta, CorPizza, LeftJoin, OrderBy, GroupBy: String): Boolean;
var
  SQL_Qtde, SQLPadrao, FldAgrup: String;
begin
  Result := False;
  case Funcoes of
    //CO_PRG_LST_NENHUMA = 0;
    CO_PRG_LST_BINARIO: // 1
    begin
      RespCod := ' opi.RespBin + 0.000 RespCod,';
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO", ',
      'IF(opi.RespBin = 0, pb0.Nome,',
      'IF(opi.RespBin = 1, pb1.Nome,',
      'IF(opi.RespBin = 2, "N�O SE APLICA", "? ? ?")))) Resposta, ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      'IF(opi.RespBin = 0, 5324774/*vermelho excel 2013*/,',
      'IF(opi.RespBin = 1, 4243590/*verde excel 2013*/,',
      'IF(opi.RespBin = 2, 47871/*Amarelo excel 2013*/, ',
      '7686045/*Roxo excel 2013*/)))) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ' + TMeuDB + '.ospipits opi ',
      'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN ' + TMeuDB + '.prgbincad pb0 ON pb0.Codigo=opi.BinarCad0 ',
      'LEFT JOIN ' + TMeuDB + '.prgbincad pb1 ON pb1.Codigo=opi.BinarCad1 ',
      '']);
      GroupBy := 'GROUP BY opi.Respondido, opi.RespBin ';
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    CO_PRG_LST_QUANTIT: // 2
    begin
      RespCod := ' 0.000 RespCod,';
      FldAgrup  := 'opi.RespQtd';
      SQLPadrao := 'LPAD(FORMAT(RespQtd, pli.CasasQtde), 14, " ")';
      SQL_Qtde  := DmRMIP.SQL_FaixaQtd(PrgLstIts, FldAgrup, SQLPadrao);
      //
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO", ',
      SQL_Qtde,
      ') Resposta, ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      '7686045/*Roxo excel 2013*/) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ' + TMeuDB + '.ospipits opi ',
      'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN ' + TMeuDB + '.prglstits pli ON pli.Controle=opi.PrgLstIts',
       '']);
      GroupBy := 'GROUP BY opi.Respondido, Resposta ';
      OrderBy := 'ORDER BY Resposta DESC ';
    end;
    CO_PRG_LST_BXAPROD: // 3
    begin
      RespCod := ' opr.GraGruX + 0.000 RespCod,';
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO",  ',
      //'gg1.Nome) Resposta,  ',
      'IF((gg1.Nivel1 IS NULL) OR (gg1.Nivel1=0), "Sem adi��o / substitui��o", ',
      'gg1.Nome)) Resposta, ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      '7686045/*Roxo excel 2013*/) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      // 2014-12-21 - Nao precisa mudar!? Nao gera pizza!
      'FROM ' + TMeuDB + '.ospipitspr opr ',
      'LEFT JOIN ' + TMeuDB + '.ospipits opi ON opr.Conta=opi.Conta ',
      //'FROM ' + TMeuDB + '.ospipits opi ',
      //'LEFT JOIN ' + TMeuDB + '.ospipitspr opr ON opr.Conta=opi.Conta ',
      // FIM 2014-12-21
      'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON opr.GraGruX=ggx.Controle ',
      'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      '']);
      GroupBy := 'GROUP BY opi.Respondido, Resposta ';
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    CO_PRG_LST_ATRIBUT: // 4
    begin
      RespCod := ' opi.RespAtrIts + 0.000 RespCod,';
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO", ',
      'IF(opi.RespAtrIts > 0, pat.Nome, pac.Padrao)) Resposta, ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      'IF(opi.RespAtrIts > 0, pat.CorPie, pac.CorPie)) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ' + TMeuDB + '.ospipits opi ',
      'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN ' + TMeuDB + '.prgatrcad pac ON pac.Codigo=opi.PrgLstCab ',
      'LEFT JOIN ' + TMeuDB + '.prgatrits pat ON pat.Controle=opi.RespAtrIts ',
      '']);
      GroupBy := 'GROUP BY opi.Respondido, opi.RespAtrCad, opi.RespAtrIts ';
      //GroupBy := 'GROUP BY cab.DtaExeFim, opi.Respondido, opi.RespAtrCad, opi.RespAtrIts '; Nao precisa?
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    CO_PRG_LST_TXTLIVR: // 5
    begin
      RespCod := '0.000 RespCod,';
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO",  ',
      'IF(LEFT(RespTxtLvr, 512) = "", "RESPOSTA EM BRANCO", ',
      'LEFT(RespTxtLvr, 512))) Resposta,  ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      '7686045/*Roxo excel 2013*/) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ' + TMeuDB + '.ospipits opi ',
      'LEFT JOIN ' + TMeuDB + '.oscab cab ON cab.Codigo=opi.Codigo ',
      '']);
      GroupBy := 'GROUP BY opi.Respondido, Resposta ';
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    //
    else
    begin
      Geral.MB_Erro(
      '"Funcoes" nao implementado em "TDmRMIP.SQL_Funcoes2()"' +
      sLineBreak +
      'Funcoes = ' + Geral.FF0(Funcoes));
      EXIT;
    end;
  end;
  Result := True;
end;

(*
procedure TDmRMIP.GeraImp_PizzaRevisaoPMVPeriodo(Empresa: Integer;
              Qr003A_PrgLstIts: TmySQLQuery; DtaIni, DtaFim: TDateTime;
              frxReport003A: TfrxReport; frxDatasets: array of TfrxDataset);
const
  OSCab = 0;
  Cliente = 0;
  Abrangencia = relAbrangeEmpresa;
var
  Aviso: String;
begin
  Qr003A_PrgLstIts.Close;
  Aviso := ' ';
  Reopen003A_PrgLstIts(Qr003A_PrgLstIts, Cliente, OSCab,
    CO_GeraGrafRelatPMV_0001_COD_PizzaMonits, DtaIni, DtaFim, Abrangencia);
  //
  if Qr003A_PrgLstIts.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso :=
      'Question�rios dos monitoramentos pesquisados sem perguntas geradoras do gr�fico "' +
      CO_GeraGrafRelatPMV_0001_TXT_PizzaMonits + '"!';
  end;
  //
  if Trim(Aviso) = '' then
    Aviso := 'Per�odo: ' + Geral.FDT(DtaIni, 3) + ' a ' + Geral.FDT(DtaFim, 2);
  //
  MyObjects.frxDefineDataSets(frxReport003A, frxDatasets);
  frxReport003A.Variables['VARF_AVISO'] := QuotedStr(Aviso);
  MyObjects.frxMostra(frxReport003A, CO_GeraGrafRelatPMV_0001_TXT_PizzaMonits);
end;

procedure TDmRMIP.GeraImp_PizzaUltimaRevisaoPMV(Empresa, Cliente: Integer;
              Qr003A_PrgLstIts: TmySQLQuery; frxReport003A: TfrxReport;
              frxDatasets: array of TfrxDataset);
const
  Abrangencia = relAbrangeCliente;
  DtaIni = 0;
  DtaFim = 0;
var
  Qry: TmySQLQuery;
  OSCab: Integer;
  Aviso: String;
begin
  Qr003A_PrgLstIts.Close;
  OSCab := 0;
  Aviso := ' ';
// Ultimo monitormento finalizado do cliente selecionado!
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DISTINCT cab.Codigo, cab.DtaExeFim ',
    'FROM oscab cab ',
    'LEFT JOIN ospipits opi ON opi.Codigo=cab.Codigo ',
    'WHERE cab.Empresa=' + Geral.FF0(Empresa),
    'AND cab.Entidade=' + Geral.FF0(Cliente),
    'AND cab.DtaExeFim > "1900-01-01" ',
    'AND cab.Operacao & ' + Geral.FF0(CO_OS_OPERACAO_SERVICO_MONITORAMENTO),
    'AND opi.Codigo IS NOT NULL ',
    'ORDER BY cab.DtaExeFim DESC ',
    'LIMIT 1 ',
    '']);
    //
    OSCab := Qry.FieldByName('Codigo').AsInteger;
  finally
    Qry.Free;
  end;
  //
  if OSCab = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso := 'Cliente sem monitoramento respondido e finalizado!';
  end;
  Reopen003A_PrgLstIts(Qr003A_PrgLstIts, Cliente, OSCab,
    CO_GeraGrafRelatPMV_0001_COD_PizzaMonits, DtaIni, DtaFim, Abrangencia);
  //
  if Qr003A_PrgLstIts.RecordCount = 0 then
  begin
    if Trim(Aviso) = '' then
      Aviso := 'Question�rios do monitoramento do localizador ' +
      Geral.FF0(OSCab) + ' sem perguntas geradoras do gr�fico "' +
      CO_GeraGrafRelatPMV_0001_TXT_PizzaMonits + '"!';
  end;
  //
  if Trim(Aviso) = '' then
    Aviso := 'Localizador: ' + Geral.FF0(OSCab);
  MyObjects.frxDefineDataSets(frxReport003A, frxDatasets);
  frxReport003A.Variables['VARF_AVISO'] := QuotedStr(Aviso);
  MyObjects.frxMostra(frxReport003A, CO_GeraGrafRelatPMV_0001_TXT_PizzaMonits);
end;

procedure TDmRMIP.Reopen003A_OSPipIts(Localizador, PrgLstIts, Funcoes: Integer;
  DtaIni, DtaFim: TDateTime; Qr003A_OSPipIts, Qr003A_OSs: TmySQLQuery;
  Abrangencia: TRelAbrange; ItemRel: Integer);
var
  Resposta, CorPizza, LeftJoin, OrderBy, GroupBy, SQL_Extra, Xtra_Group: String;
begin
  case TItensRMIP(ItemRel) of
    rmipLinhaEvoluHistori: Xtra_Group := '';
    rmipPizzaMonits: Xtra_Group := ', DtaExeFim';
    else Xtra_Group := ', ???';
  end;
  case Funcoes of
    //CO_PRG_LST_NENHUMA = 0;
    CO_PRG_LST_BINARIO: // 1
    begin
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO", ',
      'IF(opi.RespBin = 0, pb0.Nome,',
      'IF(opi.RespBin = 1, pb1.Nome,',
      'IF(opi.RespBin = 2, "N�O SE APLICA", "? ? ?")))) Resposta, ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      'IF(opi.RespBin = 0, 5324774/*vermelho excel 2013*/,',
      'IF(opi.RespBin = 1, 4243590/*verde excel 2013*/,',
      'IF(opi.RespBin = 2, 47871/*Amarelo excel 2013*/, ',
      '7686045/*Roxo excel 2013*/)))) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ospipits opi ',
      'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN prgbincad pb0 ON pb0.Codigo=opi.BinarCad0 ',
      'LEFT JOIN prgbincad pb1 ON pb1.Codigo=opi.BinarCad1 ',
      '']);
      GroupBy := 'GROUP BY opi.Respondido, opi.RespBin ';
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    CO_PRG_LST_QUANTIT: // 2
    begin
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO", ',
      'LPAD(FORMAT(RespQtd, pli.CasasQtde), 14, " ")) Resposta, ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      '7686045/*Roxo excel 2013*/) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ospipits opi ',
      'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts',
       '']);
      GroupBy := 'GROUP BY opi.Respondido, Resposta ';
      OrderBy := 'ORDER BY Resposta DESC ';
    end;
    CO_PRG_LST_BXAPROD: // 3
    begin
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO",  ',
      'gg1.Nome) Resposta,  ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      '7686045/*Roxo excel 2013*/) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ospipitspr opr ',
      'LEFT JOIN ospipits opi ON opr.Conta=opi.Conta ',
      'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN gragrux ggx ON opr.GraGruX=ggx.Controle ',
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      '']);
      GroupBy := 'GROUP BY opi.Respondido, Resposta ';
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    CO_PRG_LST_ATRIBUT: // 4
    begin
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO", ',
      'IF(opi.RespAtrIts > 0, pat.Nome, pac.Padrao)) Resposta, ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      'IF(opi.RespAtrIts > 0, pat.CorPie, pac.CorPie)) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ospipits opi ',
      'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
      'LEFT JOIN prgatrcad pac ON pac.Codigo=opi.PrgLstCab ',
      'LEFT JOIN prgatrits pat ON pat.Controle=opi.RespAtrIts ',
      '']);
      GroupBy := 'GROUP BY opi.Respondido, opi.RespAtrCad, opi.RespAtrIts ';
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    CO_PRG_LST_TXTLIVR: // 5
    begin
      Resposta := Geral.ATS([
      'IF(opi.Respondido=0, "N�O RESPONDIDO",  ',
      'IF(LEFT(RespTxtLvr, 512) = "", "RESPOSTA EM BRANCO", ',
      'LEFT(RespTxtLvr, 512))) Resposta,  ',
      '']);
      CorPizza := Geral.ATS([
      'IF(opi.Respondido=0, 8421504/*cinza*/, ',
      '7686045/*Roxo excel 2013*/) + 0.000 CorPizza ',
      '']);
      LeftJoin := Geral.ATS([
      'FROM ospipits opi ',
      'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
      '']);
      GroupBy := 'GROUP BY opi.Respondido, Resposta ';
      OrderBy := 'ORDER BY ITENS DESC ';
    end;
    //
    else
    begin
      Geral.MB_Erro(
      '"Funcoes" nao implementado em "TDmRMIP.Reopen003A_OSPipIts()"' +
      sLineBreak +
      'Funcoes = ' + Geral.FF0(Funcoes));
      EXIT;
    end;
  end;
  //

  if (Abrangencia = relAbrangeCliente)
  and (TItensRMIP(ItemRel) = rmipPizzaMonits) then
    SQL_Extra := 'AND opi.Codigo=' + Geral.FF0(Localizador)
  else //relAbrangeEmpresa
    SQL_Extra := dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qr003A_OSPipIts, Dmod.MyDB, [
  'SELECT COUNT(opi.RespAtrIts) ITENS, ',
  'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts, ',
  'cab.DtaExeFim, ',
  Resposta,
  CorPizza,
  LeftJoin,
  'WHERE opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  SQL_Extra,
  GroupBy,
  OrderBy,
  '']);
  //Geral.MB_Info(Qr003A_OSPipIts.SQL.Text);
  //


  // OSs afetadas:
  UnDmkDAC_PF.AbreMySQLQuery0(Qr003A_OSs, Dmod.MyDB, [
  'SELECT DISTINCT cab.Codigo, cab.Entidade, cab.SiapTerCad, ',
  'cab.Estatus, cab.DtaExeFim, cab.NumContrat, cab.Grupo, ',
  'fge.Nome NO_FatoGeradr, ',
  'sta.Nome NO_ESTATUS, stc.Nome NO_SiapTerCad, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
  LeftJoin,
  'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade ',
  'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr ',
  'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus ',
  'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad ',
  'WHERE opi.PrgLstIts=' + Geral.FF0(PrgLstIts),
  SQL_Extra,
  'ORDER BY cab.DtaExeFim',
  '']);
end;

procedure TDmRMIP.Reopen003A_PrgLstIts*Antigo*(Qr003A_PrgLstIts: TmySQLQuery; OSCab,
  Aplicacao: Integer; DtaIni, DtaFim: TDateTime; Abrangencia: TRelAbrange);
var
  SQL_Codigo, SQL_Extra: String;
begin
  case Abrangencia of
    relAbrangeCliente:
    begin
      SQL_Codigo := 'opi.Codigo + 0.000';
      SQL_Extra := 'AND opi.Codigo=' + Geral.FF0(OSCab);
    end;
    relAbrangeEmpresa:
    begin
      SQL_Codigo := '0.000';
      SQL_Extra := dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True);
    end
    else
    begin
      SQL_Codigo := '???';
      SQL_Extra := '? ? ?';
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr003A_PrgLstIts, Dmod.MyDB, [
  'SELECT DISTINCT ' + SQL_Codigo + ' Codigo, opi.PrgLstCab, opi.PrgLstIts,  ',
  'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , ',
  'plp.Sigla, plp.Nome, pli.Funcoes ',
  'FROM ospipits opi ',
  'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
  'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts ',
  'LEFT JOIN prgcadprg plp ON plp.Codigo=pli.Pergunta ',
  'WHERE pli.Aplicacao & ' + Geral.FF0(CO_GeraGrafRelatPMV_0001_COD_PizzaMonits),
  SQL_Extra,
  'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ',
  '']);
end;

procedure TDmRMIP.Reopen003A_PrgLstIts(Qr003A_PrgLstIts: TmySQLQuery; Cliente, OSCab,
  Aplicacao: Integer; DtaIni, DtaFim: TDateTime; Abrangencia: TRelAbrange);
var
  SQL_Codigo, SQL_Extra, SQL_DtExe: String;
begin
  case Abrangencia of
    relAbrangeCliente:
    begin
          SQL_Codigo := 'opi.Codigo + 0.000';
          SQL_Extra := 'AND opi.Codigo=' + Geral.FF0(OSCab);
          SQL_DtExe := '0 DtaExeFim, ';
    end;
    relAbrangeEmpresa:
    begin
      SQL_Codigo := '0.000';
      SQL_Extra := dmkPF.SQL_Periodo('AND cab.DtaExeFim', DtaIni, DtaFim, True, True);
      //SQL_DtExe := 'DtaExeFim DtaExeFim, ';
      SQL_DtExe := '0 DtaExeFim, ';
    end
    else
    begin
      SQL_Codigo := '???';
      SQL_Extra := '? ? ?';
      SQL_DtExe := '??? DtaExeFim, ';
    end;
  end;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr003A_PrgLstIts, Dmod.MyDB, [
  'SELECT DISTINCT ' +
  SQL_DtExe,
  SQL_Codigo + ' Codigo, opi.PrgLstCab, opi.PrgLstIts,  ',
  'IF(plc.Titulo <> "", plc.Titulo, plc.Nome) NO_LST , ',
  'plp.Sigla, plp.Nome, pli.Funcoes ',
  'FROM ospipits opi ',
  'LEFT JOIN oscab cab ON cab.Codigo=opi.Codigo ',
  'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab ',
  'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts ',
  'LEFT JOIN prgcadprg plp ON plp.Codigo=pli.Pergunta ',
  'WHERE pli.Aplicacao & ' + Geral.FF0(Aplicacao),
  SQL_Extra,
  'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome ',
  '']);
end;
*)


end.
