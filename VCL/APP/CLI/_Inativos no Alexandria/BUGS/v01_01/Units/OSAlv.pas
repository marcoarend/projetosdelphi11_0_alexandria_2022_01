unit OSAlv;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, AdvObj, BaseGrid, AdvGrid, DBAdvGrid, UnDmkEnums;

type
  TFmOSAlv = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    GBDados: TGroupBox;
    Label7: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit15: TDBEdit;
    QrOSSrv: TmySQLQuery;
    QrOSSrvNO_DesServico: TWideStringField;
    QrOSSrvCodigo: TIntegerField;
    QrOSSrvControle: TIntegerField;
    QrOSSrvDesServico: TIntegerField;
    QrOSSrvLk: TIntegerField;
    QrOSSrvDataCad: TDateField;
    QrOSSrvDataAlt: TDateField;
    QrOSSrvUserCad: TIntegerField;
    QrOSSrvUserAlt: TIntegerField;
    QrOSSrvAlterWeb: TSmallintField;
    QrOSSrvAtivo: TSmallintField;
    DsOSSrv: TDataSource;
    Timer1: TTimer;
    Qr_OS_Alv_: TmySQLQuery;
    Ds_OS_Alv_: TDataSource;
    Qr_OS_Alv_o2Gru: TIntegerField;
    Qr_OS_Alv_c2Gru: TIntegerField;
    Qr_OS_Alv_n2Gru: TWideStringField;
    Qr_OS_Alv_a2Gru: TSmallintField;
    Qr_OS_Alv_o1Cab: TIntegerField;
    Qr_OS_Alv_c1Cab: TIntegerField;
    Qr_OS_Alv_n1Cab: TWideStringField;
    Qr_OS_Alv_a1Cab: TSmallintField;
    Qr_OS_Alv_Ativo: TSmallintField;
    QrPsq1: TmySQLQuery;
    QrPsq1o2Gru: TIntegerField;
    QrPsq1c2Gru: TIntegerField;
    QrPsq1n2Gru: TWideStringField;
    QrPsq1a2Gru: TSmallintField;
    QrPsq1o1Cab: TIntegerField;
    QrPsq1c1Cab: TIntegerField;
    QrPsq1n1Cab: TWideStringField;
    QrPsq1a1Cab: TSmallintField;
    QrPsq1Ativo: TSmallintField;
    QrOA: TmySQLQuery;
    QrOAo2Gru: TIntegerField;
    QrOAc2Gru: TIntegerField;
    QrOAn2Gru: TWideStringField;
    QrOAa2Gru: TSmallintField;
    QrOAo1Cab: TIntegerField;
    QrOAc1Cab: TIntegerField;
    QrOAn1Cab: TWideStringField;
    QrOAa1Cab: TSmallintField;
    QrOAAtivo: TSmallintField;
    Qr_OS_Alv_NO_Ativo: TWideStringField;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    AdvGridAlv: TDBAdvGrid;
    Panel5: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Edn2Gru: TdmkEdit;
    Edn1Cab: TdmkEdit;
    RB_E: TRadioButton;
    RB_Ou: TRadioButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure AdvGridAlvClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure Qr_OS_Alv_AfterOpen(DataSet: TDataSet);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure RB_EClick(Sender: TObject);
    procedure RB_OuClick(Sender: TObject);
    procedure Edn2GruChange(Sender: TObject);
    procedure Edn1CabChange(Sender: TObject);
    procedure AdvGridAlvClick(Sender: TObject);
  private
    { Private declarations }
    FCriou: Boolean;
    F_OS_Alv_: String;
    //
    procedure Reopen_OS_Alv_(Codigo: Integer);
    procedure AtivarTudo(Ativa: Integer);
  public
    { Public declarations }
    FTabela: String;
    FQrOSAlv, FQrInsere: TmySQLQuery;
    FDatabase: TmySQLDatabase;
    FCodigo, FControle: Integer;
  end;

  var
  FmOSAlv: TFmOSAlv;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, CreateBugs, ModuleGeral,
  UnOSApp_PF;

{$R *.DFM}

const
  FColsToMerge: array[0..1] of Integer = (3,5);

procedure TFmOSAlv.AdvGridAlvClick(Sender: TObject);
begin
  MyObjects.UpdMergeDBAvGrid(AdvGridAlv, FColsToMerge);
end;

procedure TFmOSAlv.AdvGridAlvClickCell(Sender: TObject; ARow, ACol: Integer);
  function CampoDeNivel(Nivel: Integer; Letra: String): String;
  begin
    case Nivel of
      1: Result := 'Cab';
      2: Result := 'Gru';
      else Result := '???';
    end;
    //
    Result := Letra + Geral.FF0(Nivel) + Result;
  end;
const
  MaxNivel = 2;
var
  cN1, Ativo, Nivel, I: Integer;
  Campo, Codigo, AtivS, FldAtiv, SQL(*, _AND_, xFld, nFld*): String;
begin
  FldAtiv := TDBAdvGrid(AdvGridAlv).Columns[ACol].FieldName;
  if Copy(FldAtiv, 1, 1) = 'a' then
  begin
    Nivel := Geral.IMV(Copy(FldAtiv, 2, 1));
    if Nivel = 0 then
      Exit;
    cN1 := Geral.IMV(TDBAdvGrid(Sender).Cells[1, ARow]);
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq1, DModG.MyPID_DB, [
    'SELECT * FROM ' + F_OS_Alv_,
    'WHERE c1Cab=' + Geral.FF0(cN1),
    '']);
    Ativo := QrPsq1.FieldByName(FldAtiv).AsInteger;
    Campo := CampoDeNivel(Nivel, 'c');
    Codigo := Geral.FF0(QrPsq1.FieldByName(Campo).AsInteger);
    if Ativo = 1 then Ativo := 0 else Ativo := 1;
    AtivS := Geral.FF0(Ativo);
    //
    SQL := '';
    for I := 1 to Nivel do
      SQL := SQL + ' ' + CampoDeNivel(I, 'a') + '=' + AtivS + ', ';
    SQL := SQL + 'Ativo=' + AtivS;
    //
    UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
    'UPDATE ' + F_OS_Alv_,
    ' SET ',
    SQL,
    'WHERE ' + Campo + '=' + Codigo,
    '']);
    //
    Reopen_OS_Alv_(cN1);
  end;
end;

procedure TFmOSAlv.AtivarTudo(Ativa: Integer);
{
var
  Codigo: Integer;
}
begin
  Screen.Cursor := crHourGlass;
  try
    UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
    'UPDATE ' + F_OS_Alv_ ,
    'SET Ativo=' + Geral.FF0(Ativa) + ', ',
    'a2Gru=' + Geral.FF0(Ativa) + ', ',
    'a1Cab=' + Geral.FF0(Ativa),
    '']);
    //
    Reopen_OS_Alv_(0);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSAlv.BtNenhumClick(Sender: TObject);
begin
  AtivarTudo(0);
end;

procedure TFmOSAlv.BtOKClick(Sender: TObject);
var
  Conta, PragaCab: Integer;
  Codigo, Controle: String;
begin
  Conta := 0;
  Screen.Cursor := crHourGlass;
  try
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Inserindo selecionados');
    //
    Codigo := Geral.FF0(FCodigo);
    Controle := Geral.FF0(FControle);
    //
    if FQrOSAlv.Database <> DmodG.MyPID_DB then
    begin
      UMyMod.ExecutaMySQLQuery1(FQrInsere, [
      'INSERT INTO livre2 (Tabela, Campo, Codigo, Vezes, AlterWeb, Ativo) ',
      'SELECT "' + FTabela + '" Tabela, "Conta" Campo, ',
      'Conta Codigo, 1 Vezes, 0 AlterWeb, 0 Ativo ',
      'FROM ' + FTabela,
      'WHERE Codigo=' + Codigo,
      'AND Controle=' + Controle,
      ';',
      'DELETE FROM ' + FTabela,
      'WHERE Codigo=' + Codigo,
      'AND Controle=' + Controle,
      '']);
    end;
    //
    QrOA.Close;
    UnDmkDAC_PF.AbreMySQLQuery0(QrOA, DModG.MyPID_DB, [
    'SELECT * FROM ' + F_OS_Alv_,
    'WHERE Ativo=1 ',
    'ORDER BY o2Gru, n2Gru, c2Gru, ',
    'o1Cab, n1Cab, c1Cab ',
    '']);
    QrOA.First;
    while not QrOA.Eof do
    begin
      PragaCab := QrOAc1Cab.Value;
      //
      if FQrOSAlv.Database <> DmodG.MyPID_DB then
        Conta := UMyMod.BPGS1I32_Reaproveita(FTabela, 'Conta', '', '', tsDef, stIns, 0)
      else
        Conta := QrOA.RecNo;
      UMyMod.SQLInsUpd(FQrInsere, stIns, FTabela, False, [
      'Codigo', 'Controle', 'Praga_Z'], [
      'Conta'], [
      Codigo, Controle, PragaCab], [
      Conta], True);
      //
      QrOA.Next;
    end;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    OSApp_PF.ReopenOSAlv(FQrOSAlv, FControle, Conta, FDatabase);
    Close;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSAlv.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSAlv.BtTodosClick(Sender: TObject);
begin
  AtivarTudo(1);
end;

procedure TFmOSAlv.Edn1CabChange(Sender: TObject);
begin
  Reopen_OS_Alv_(0);
end;

procedure TFmOSAlv.Edn2GruChange(Sender: TObject);
begin
  Reopen_OS_Alv_(0);
end;

procedure TFmOSAlv.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := True;
end;

procedure TFmOSAlv.FormCreate(Sender: TObject);
begin
  FCriou := False;
end;

procedure TFmOSAlv.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
  //
  MyObjects.UpdMergeDBAvGrid(AdvGridAlv, FColsToMerge);
end;

procedure TFmOSAlv.Qr_OS_Alv_AfterOpen(DataSet: TDataSet);
begin
  MyObjects.UpdMergeDBAvGrid(AdvGridAlv, FColsToMerge);
end;

procedure TFmOSAlv.RB_EClick(Sender: TObject);
begin
  if RB_E.Checked then
  begin
    RB_Ou.Checked := False;
    Reopen_OS_Alv_(0);
  end;
end;

procedure TFmOSAlv.RB_OuClick(Sender: TObject);
begin
  if RB_Ou.Checked then
  begin
    RB_E.Checked := False;
    Reopen_OS_Alv_(0);
  end;
end;

procedure TFmOSAlv.Reopen_OS_Alv_(Codigo: Integer);
var
  n2G_B, n1C_B, Visi: Boolean;
  n2G_X, n1C_X, Liga, Filtro: String;
begin
  if (Length(Edn2Gru.ValueVariant) > 0) or (Length(Edn1Cab.ValueVariant) > 0) then
    Visi := False
  else
    Visi := True;

  BtTodos.Visible  := Visi;
  BtNenhum.Visible := Visi;

  Filtro := '';
  n2G_X := '';
  n1C_X := '';
  Liga := '';
  n2G_B := False;
  n1C_B := False;
  //
  if Trim(Edn2Gru.Text) <> '' then
  begin
    n2G_B := True;
    n2G_X := ' (n2Gru LIKE "%' + Edn2Gru.Text + '%") ';
  end;
  if Trim(Edn1Cab.Text) <> '' then
  begin
    n1C_B := True;
    n1C_X := ' (n1Cab LIKE "%' + Edn1Cab.Text + '%") ';
  end;
  if n2G_B or n1C_B then
  begin
    Filtro := 'WHERE ';
    if n2G_B and n1C_B then
    begin
      if RB_Ou.Checked then
        Liga := ' OR '
      else
        Liga := ' AND ';
    end;
    Filtro := 'WHERE ' + n2G_X + Liga + n1C_X;
  end;
  //
  Qr_OS_Alv_.Close;
  UnDmkDAC_PF.AbreMySQLQuery0(Qr_OS_Alv_, DModG.MyPID_DB, [
    'SELECT oa.*, ELT(oa.Ativo + 1, "N", "S") NO_Ativo ',
    'FROM ' + F_OS_Alv_ + ' oa ',
    Filtro,
    'ORDER BY oa.o2Gru, oa.n2Gru, ',
    'oa.c2Gru, oa.o1Cab, oa.n1Cab, oa.c1Cab ',
    '']);
  Qr_OS_Alv_.Locate('c1Cab', Codigo, [])
end;

procedure TFmOSAlv.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  if not FCriou then
  begin
    Screen.Cursor := crHourGlass;
    try
      FCriou := True;
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabela temporária!');
      F_OS_Alv_ :=
        UnCreateBugs.RecriaTempTableNovo(ntrtt_OS_Alv_, DmodG.QrUpdPID1, False);
      //
      UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
        'DELETE FROM ' + F_OS_Alv_ + ' ; ',
        'INSERT INTO ' + F_OS_Alv_ + ' ',
        'SELECT 1 o2Gru, gru.Codigo c2Gru, gru.Nome n2Gru, 0 a2Gru, ',
        '1 o1Cab, cab.Codigo c1Cab, cab.Nome n1Cab, 0 a1Cab, 0 Ativo ',
        'FROM ' + TMeuDB + '.praga_z cab ',
        'LEFT JOIN ' + TMeuDB + '.praga_a gru ON gru.Codigo=cab.NivSup ',
        '']);

      OSApp_PF.ReopenOSAlv(FQrOSAlv, FControle, 0, FDatabase);

      if FQrOSAlv.RecordCount > 0 then
      begin
        UMyMod.ExecutaMySQLQuery1(DmodG.QrUpdPID1, [
        'UPDATE ' + F_OS_Alv_ + ' ',
        'SET a2Gru=1, a1Cab=1, Ativo=1 ',
        'WHERE c1Cab IN ( ',
        '    SELECT Praga_Z ',
        '     FROM ' + TMeuDB + '.' + FTabela,
        '     WHERE Controle=' + Geral.FF0(FControle),
        ') ',
        '']);
      end;
      //
      Reopen_OS_Alv_(0);
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      Screen.Cursor := crDefault;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

end.
