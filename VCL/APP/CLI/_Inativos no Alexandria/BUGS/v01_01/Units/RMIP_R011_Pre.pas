unit RMIP_R011_Pre;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkDBGridZTO, mySQLDbTables,
  frxClass, frxDBSet;

type
  TFmRMIP_R011_Pre = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrCunsImgCab: TmySQLQuery;
    QrCunsImgCabCodigo: TIntegerField;
    QrCunsImgCabControle: TIntegerField;
    QrCunsImgCabNome: TWideStringField;
    QrCunsImgCabStatus: TIntegerField;
    QrCunsImgCabCaminho: TWideStringField;
    QrCunsImgCabNO_STATUS: TWideStringField;
    QrCunsImgCabDependencia: TIntegerField;
    QrCunsImgCabNO_DEPENDENCIA: TWideStringField;
    QrCunsImgCabCodiNCT: TIntegerField;
    QrCunsImgCabObserv: TWideMemoField;
    QrCunsImgCabAplicacao: TIntegerField;
    QrCunsImgCabNO_NCT: TWideStringField;
    QrCunsImgCabTxtCAC: TWideStringField;
    QrCunsImgCabOrdem: TIntegerField;
    DsCunsImgCab: TDataSource;
    Panel37: TPanel;
    Panel39: TPanel;
    ImgFoto: TImage;
    Splitter4: TSplitter;
    PG_NCT_CAC: TPageControl;
    TabSheet29: TTabSheet;
    DBMemo2: TDBMemo;
    TabSheet39: TTabSheet;
    Panel40: TPanel;
    Label32: TLabel;
    Label34: TLabel;
    DBMemo3: TDBMemo;
    DBMemo4: TDBMemo;
    DBGCunsImgCab: TdmkDBGridZTO;
    QrCunsImgCabNO_STC: TWideStringField;
    frxReport011A: TfrxReport;
    QrCIC_Imp: TmySQLQuery;
    QrCIC_ImpCodigo: TIntegerField;
    QrCIC_ImpControle: TIntegerField;
    QrCIC_ImpNome: TWideStringField;
    QrCIC_ImpStatus: TIntegerField;
    QrCIC_ImpCaminho: TWideStringField;
    QrCIC_ImpNO_STATUS: TWideStringField;
    QrCIC_ImpDependencia: TIntegerField;
    QrCIC_ImpNO_DEPENDENCIA: TWideStringField;
    QrCIC_ImpCodiNCT: TIntegerField;
    QrCIC_ImpObserv: TWideMemoField;
    QrCIC_ImpAplicacao: TIntegerField;
    QrCIC_ImpNO_NCT: TWideStringField;
    QrCIC_ImpTxtCAC: TWideStringField;
    QrCIC_ImpNO_LOCAL: TWideStringField;
    QrCIC_ImpOrdem: TIntegerField;
    QrCIC_ImpNO_STC: TWideStringField;
    frxDsCIC_Imp: TfrxDBDataset;
    QrMixRel: TmySQLQuery;
    QrMixRelFC: TFloatField;
    QrMixRelNCT: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FEmpresa, FCliente, FItemRel: Integer;
    FDtaIni, FDtaFim, FDtaImp: TDateTime;
    FNO_Empresa, FNO_CLiente, FNO_REL: String;
    F_Cuns_Img_Cab: String;
    //
    FMemo1: TMemo;
    //
    procedure ReopenCunsImgCab();
  end;

  var
  FmRMIP_R011_Pre: TFmRMIP_R011_Pre;

implementation

uses UnMyObjects, Module, DmkDAC_PF, CreateBugs, ModuleGeral, UMySQLModule,
UnDmkProcFunc;

{$R *.DFM}

procedure TFmRMIP_R011_Pre.BtOKClick(Sender: TObject);
  procedure IncluiAtual();
  var
    Nome, Caminho, Observ: String;
    Codigo, Controle, Status, Dependencia, CodiNCT, Aplicacao, Ordem: Integer;
  begin
    Codigo         := QrCunsImgCabCodigo.Value;
    Controle       := QrCunsImgCabControle.Value;
    Nome           := QrCunsImgCabNome.Value;
    Status         := QrCunsImgCabStatus.Value;
    Caminho        := QrCunsImgCabCaminho.Value;
    Dependencia    := QrCunsImgCabDependencia.Value;
    CodiNCT        := QrCunsImgCabCodiNCT.Value;
    Observ         := QrCunsImgCabObserv.Value;
    Aplicacao      := QrCunsImgCabAplicacao.Value;
    Ordem          := QrCunsImgCabOrdem.Value;

    //
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, F_Cuns_Img_Cab, False, [
    'Codigo', 'Nome', 'Status',
    'Caminho', 'Dependencia', 'CodiNCT',
    'Observ', 'Aplicacao', 'Ordem'], [
    'Controle'], [
    Codigo, Nome, Status,
    Caminho, Dependencia, CodiNCT,
    Observ, Aplicacao, Ordem], [
    Controle], False);
  end;
var
  CtrlLoc, I: Integer;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando impress�o');
  //
  CtrlLoc := QrCunsImgCabControle.Value;
  if DBGCunsImgCab.SelectedRows.Count > 1 then
  begin
    with DBGCunsImgCab.DataSource.DataSet do
    for I:= 0 to DBGCunsImgCab.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(DBGCunsImgCab.SelectedRows.Items[i]));
      IncluiAtual();
    end;
  end;
  //
  Close;
end;

procedure TFmRMIP_R011_Pre.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRMIP_R011_Pre.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmRMIP_R011_Pre.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmRMIP_R011_Pre.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRMIP_R011_Pre.ReopenCunsImgCab();
begin
  F_Cuns_Img_Cab :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Cuns_Img_Cab, DmodG.QrUpdPID1, False,
    0, '_RMIP_011');
  UnDmkDAC_PF.AbreMySQLQuery0(QrCunsImgCab, Dmod.MyDB, [
  'SELECT stc.Nome NO_STC, ',
  'CONCAT(sic.SCompl2, " -> ", dep.Nome) NO_DEPENDENCIA, ',
  'cis.Nome NO_STATUS, nct.Nome NO_NCT, nct.TxtCAC, cic.*  ',
  'FROM cunsimgcab cic  ',
  'LEFT JOIN cunsimgsit cis ON cis.Codigo=cic.Status  ',
  'LEFT JOIN siapimadep sid ON sid.Controle=cic.Dependencia ',
  'LEFT JOIN siapimacad sic ON sic.Codigo=sid.Codigo',
  'LEFT JOIN siaptercad stc ON stc.Codigo=sic.SiapImaTer',
  'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci ',
  'LEFT JOIN cunsimgnct nct ON nct.Codigo=cic.CodiNCT ',
  'WHERE cic.Codigo=' + Geral.FF0(FCliente),
  //'AND stc.Codigo=' + Geral.FF0(QrSiapTerCadCodigo.Value),
  'ORDER BY cic.Ordem, cic.Codigo ',
  '']);
  //
end;

end.
