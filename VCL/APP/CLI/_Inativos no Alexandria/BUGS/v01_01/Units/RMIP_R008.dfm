object FmRMIP_R008: TFmRMIP_R008
  Left = 0
  Top = 0
  Caption = 'FmRMIP_R008'
  ClientHeight = 299
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Qr008Grupos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT cab.Grupo'
      'FROM oscab cab '
      'WHERE cab.Empresa=1'
      'AND cab.Entidade=1433'
      'AND cab.DtaExeFim BETWEEN "2013-01-01" AND "2014-12-31"'
      'ORDER BY cab.DtaExeFim, cab.Codigo')
    Left = 40
    Top = 36
    object Qr008GruposGrupo: TIntegerField
      FieldName = 'Grupo'
    end
  end
end
