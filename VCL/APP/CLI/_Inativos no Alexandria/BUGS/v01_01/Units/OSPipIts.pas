unit OSPipIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, mySQLDbTables, dmkEdit,
  dmkDBLookupComboBox, dmkEditCB, dmkValUsu, UnBugs_Tabs, UnDmkEnums,
  UnProjGroup_Consts;

type
  TFmOSPipIts = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrOSPipIts: TmySQLQuery;
    DBGrid1: TDBGrid;
    DsOSPipIts: TDataSource;
    CategoryPanelGroup1: TCategoryPanelGroup;
    CategoryPanel1: TCategoryPanel;
    CategoryPanel2: TCategoryPanel;
    CategoryPanel3: TCategoryPanel;
    CategoryPanel4: TCategoryPanel;
    CategoryPanel5: TCategoryPanel;
    QrOSPipItsCodigo: TIntegerField;
    QrOSPipItsControle: TIntegerField;
    QrOSPipItsConta: TLargeintField;
    QrOSPipItsPrgLstCab: TIntegerField;
    QrOSPipItsPrgLstIts: TIntegerField;
    QrOSPipItsOrdem: TIntegerField;
    QrOSPipItsSubOrdem: TIntegerField;
    QrOSPipItsTabela: TSmallintField;
    QrOSPipItsGraGruX: TIntegerField;
    QrOSPipItsFuncoes: TSmallintField;
    QrOSPipItsDependente: TSmallintField;
    QrOSPipItsPrgAtrCad: TIntegerField;
    QrOSPipItsFiliacao: TIntegerField;
    QrOSPipItsRelacao: TIntegerField;
    QrOSPipItsNivel: TIntegerField;
    QrOSPipItsNivSeq: TIntegerField;
    QrOSPipItsPergunta: TIntegerField;
    QrOSPipItsBinarCad0: TIntegerField;
    QrOSPipItsBinarCad1: TIntegerField;
    QrOSPipItsLk: TIntegerField;
    QrOSPipItsDataCad: TDateField;
    QrOSPipItsDataAlt: TDateField;
    QrOSPipItsUserCad: TIntegerField;
    QrOSPipItsUserAlt: TIntegerField;
    QrOSPipItsAlterWeb: TSmallintField;
    QrOSPipItsAtivo: TSmallintField;
    QrOSPipItsAcaoPrd: TSmallintField;
    QrOSPipItsRespondido: TSmallintField;
    QrOSPipItsRespBin: TSmallintField;
    QrOSPipItsRespQtd: TFloatField;
    QrOSPipItsRespAtrCad: TIntegerField;
    QrOSPipItsRespAtrIts: TIntegerField;
    QrOSPipItsRespTxtLvr: TWideMemoField;
    RGRespBin: TRadioGroup;
    QrOSPipItsBinarSIM: TWideStringField;
    QrOSPipItsBinarNAO: TWideStringField;
    EdRespQtd: TdmkEdit;
    LaRespQtd: TLabel;
    QrOSPipItsNO_PERGUNTA: TWideStringField;
    CategoryPanelGroup2: TCategoryPanelGroup;
    CategoryPanel3_1: TCategoryPanel;
    CategoryPanel3_2: TCategoryPanel;
    CategoryPanel3_3: TCategoryPanel;
    RGAcaoPrd_Tira: TRadioGroup;
    RGAcaoPrd_Troc: TRadioGroup;
    QrGraGruX: TmySQLQuery;
    DsGraGruX: TDataSource;
    Label4: TLabel;
    EdGraGruX: TdmkEditCB;
    CBGraGruX: TdmkDBLookupComboBox;
    BtOK_3_1: TBitBtn;
    Label1: TLabel;
    EdUsoQtd: TdmkEdit;
    EdUsoCusUni: TdmkEdit;
    Label2: TLabel;
    EdUsoCusTot: TdmkEdit;
    Label3: TLabel;
    CategoryPanelGroup3: TCategoryPanelGroup;
    CategoryPanel4_1: TCategoryPanel;
    BtOK_4_1: TBitBtn;
    CategoryPanel4_2: TCategoryPanel;
    QrOSPipItsNO_PrgAtrCad: TWideStringField;
    QrOSPipItsAtrTyp: TSmallintField;
    QrPrgAtrIts: TmySQLQuery;
    DsPrgAtrIts: TDataSource;
    LaRespAtrIts: TLabel;
    EdRespAtrIts: TdmkEditCB;
    CBRespAtrIts: TdmkDBLookupComboBox;
    QrPrgAtrItsCodUsu: TIntegerField;
    QrPrgAtrItsControle: TIntegerField;
    QrPrgAtrItsNome: TWideStringField;
    Panel5: TPanel;
    BtOK_4_2: TBitBtn;
    MeRespAtrTxt: TMemo;
    MeRespTxtLvr: TMemo;
    Panel6: TPanel;
    BtOK_5: TBitBtn;
    BtOK_2: TBitBtn;
    QrOSPipItsTabIdx: TLargeintField;
    Label5: TLabel;
    EdValCliDd: TdmkEdit;
    QrOri: TmySQLQuery;
    QrOriValCliDd: TIntegerField;
    QrOriUsoQtd: TFloatField;
    QrOriUsoCusUni: TFloatField;
    QrOriUsoCusTot: TFloatField;
    QrMesmoNiv: TmySQLQuery;
    QrMesmoNivConta: TLargeintField;
    QrOSPipItsNO_PIP: TWideStringField;
    QrOSPipItsRespAtrTxt: TWideStringField;
    QrOSPipItsRespPrdBin: TSmallintField;
    CategoryPanel6: TCategoryPanel;
    QrGraGruXGraGru1: TIntegerField;
    QrGraGruXGraGruX: TIntegerField;
    QrGraGruXCtrlGGV: TIntegerField;
    QrGraGruXCustoPreco: TFloatField;
    QrGraGruXNO_PRD_TAM_COR: TWideStringField;
    Panel3_2_Sim: TPanel;
    Label6: TLabel;
    EdQtdUso_3_2_Sim: TdmkEdit;
    BtOK_3_2_Sim: TBitBtn;
    QrOSPipItsSobreOrd: TIntegerField;
    QrOSPipItsLupForma: TSmallintField;
    QrOSPipItsLupQtdVzs: TSmallintField;
    QrOSPipItsLupSeqRsp: TIntegerField;
    Label7: TLabel;
    EdNumLote: TdmkEdit;
    Label8: TLabel;
    EdNumLaudo: TdmkEdit;
    Label9: TLabel;
    EdNumLote_3_2_Sim: TdmkEdit;
    Label10: TLabel;
    EdNumLaudo_3_2_Sim: TdmkEdit;
    QrG1Any: TmySQLQuery;
    QrG1AnyValCliDd: TIntegerField;
    QrG1AnyAtuNumLot: TWideStringField;
    QrG1AnyAtuNumLaud: TWideStringField;
    QrOSPipItsGraGru1: TIntegerField;
    UpDown1: TUpDown;
    UpDown2: TUpDown;
    CkPreencher: TCheckBox;
    QrOPI: TmySQLQuery;
    QrOPICodigo: TIntegerField;
    QrOPIControle: TIntegerField;
    QrOPIConta: TLargeintField;
    QrOPIPrgLstCab: TIntegerField;
    QrOPIPrgLstIts: TIntegerField;
    QrOPISobreOrd: TIntegerField;
    QrOPIOrdem: TIntegerField;
    QrOPISubOrdem: TIntegerField;
    QrOPITabela: TSmallintField;
    QrOPITabIdx: TLargeintField;
    QrOPIGraGruX: TIntegerField;
    QrOPIFuncoes: TSmallintField;
    QrOPIDependente: TSmallintField;
    QrOPIPrgAtrCad: TIntegerField;
    QrOPIFiliacao: TIntegerField;
    QrOPIRelacao: TIntegerField;
    QrOPINivel: TIntegerField;
    QrOPINivSeq: TIntegerField;
    QrOPIPergunta: TIntegerField;
    QrOPIBinarCad0: TIntegerField;
    QrOPIBinarCad1: TIntegerField;
    QrOPIAcaoPrd: TSmallintField;
    QrOPIRespondido: TSmallintField;
    QrOPIRespBin: TSmallintField;
    QrOPIRespQtd: TFloatField;
    QrOPIRespAtrCad: TIntegerField;
    QrOPIRespAtrIts: TIntegerField;
    QrOPIRespAtrTxt: TWideStringField;
    QrOPIRespTxtLvr: TWideMemoField;
    QrOPIRespPrdBin: TSmallintField;
    QrOPISuperOrd: TIntegerField;
    QrOPISuperSub: TIntegerField;
    QrOPILupForma: TSmallintField;
    QrOPILupQtdVzs: TSmallintField;
    QrOPILupSeqRsp: TIntegerField;
    QrOPILupInfVzs: TSmallintField;
    QrOPIRespMetodo: TSmallintField;
    QrAntOPIP: TmySQLQuery;
    QrAntOPIPCodigo: TIntegerField;
    QrAntOPIPControle: TIntegerField;
    QrAntOPIPConta: TLargeintField;
    QrAntOPIPIDIts: TLargeintField;
    QrAntOPIPAcao: TSmallintField;
    QrAntOPIPGraGruX: TIntegerField;
    QrAntOPIPUsoQtd: TFloatField;
    QrAntOPIPUsoCusUni: TFloatField;
    QrAntOPIPUsoCusTot: TFloatField;
    QrAntOPIPUsoPrc: TFloatField;
    QrAntOPIPUsoVal: TFloatField;
    QrAntOPIPUsoDec: TFloatField;
    QrAntOPIPUsoTot: TFloatField;
    QrAntOPIPValCliDd: TIntegerField;
    QrAntOPIPEhDiluente: TSmallintField;
    QrAntOPIPDesativado: TSmallintField;
    QrAntOPIPNumLote: TWideStringField;
    QrAntOPIPNumLaudo: TWideStringField;
    QrOPIPR: TmySQLQuery;
    QrOPIPRCodigo: TIntegerField;
    QrOPIPRControle: TIntegerField;
    QrOPIPRConta: TLargeintField;
    QrOPIPRIDIts: TLargeintField;
    QrOPIPRAcao: TSmallintField;
    QrOPIPRGraGruX: TIntegerField;
    QrOPIPRUsoQtd: TFloatField;
    QrOPIPRUsoCusUni: TFloatField;
    QrOPIPRUsoCusTot: TFloatField;
    QrOPIPRUsoPrc: TFloatField;
    QrOPIPRUsoVal: TFloatField;
    QrOPIPRUsoDec: TFloatField;
    QrOPIPRUsoTot: TFloatField;
    QrOPIPRValCliDd: TIntegerField;
    QrOPIPREhDiluente: TSmallintField;
    QrOPIPRDesativado: TSmallintField;
    QrOPIPRNumLote: TWideStringField;
    QrOPIPRNumLaudo: TWideStringField;
    QrSMIA: TmySQLQuery;
    QrAntOPIPLk: TIntegerField;
    QrAntOPIPDataCad: TDateField;
    QrAntOPIPDataAlt: TDateField;
    QrAntOPIPUserCad: TIntegerField;
    QrAntOPIPUserAlt: TIntegerField;
    QrAntOPIPAlterWeb: TSmallintField;
    QrAntOPIPAtivo: TSmallintField;
    QrAntOPIPSMI_IDCtrl: TLargeintField;
    QrSMIADataHora: TDateTimeField;
    QrSMIAIDCtrl: TIntegerField;
    QrSMIATipo: TIntegerField;
    QrSMIAOriCodi: TIntegerField;
    QrSMIAOriCtrl: TIntegerField;
    QrSMIAOriCnta: TIntegerField;
    QrSMIAOriPart: TIntegerField;
    QrSMIAEmpresa: TIntegerField;
    QrSMIAStqCenCad: TIntegerField;
    QrSMIAGraGruX: TIntegerField;
    QrSMIAQtde: TFloatField;
    QrSMIAPecas: TFloatField;
    QrSMIAPeso: TFloatField;
    QrSMIAAreaM2: TFloatField;
    QrSMIAAreaP2: TFloatField;
    QrSMIAFatorClas: TFloatField;
    QrSMIAQuemUsou: TIntegerField;
    QrSMIARetorno: TSmallintField;
    QrSMIAParTipo: TIntegerField;
    QrSMIAParCodi: TIntegerField;
    QrSMIADebCtrl: TIntegerField;
    QrSMIASMIMultIns: TIntegerField;
    QrSMIACustoAll: TFloatField;
    QrSMIAValorAll: TFloatField;
    QrSMIAGrupoBal: TIntegerField;
    QrSMIABaixa: TSmallintField;
    QrSMIAAntQtde: TFloatField;
    QrSMIAUnidMed: TIntegerField;
    QrSMIAAlterWeb: TSmallintField;
    QrSMIAAtivo: TSmallintField;
    QrSMIAStqCenLoc: TIntegerField;
    QrSMIAValiStq: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrOSPipItsAfterScroll(DataSet: TDataSet);
    procedure BtOK_4_1Click(Sender: TObject);
    procedure RGRespBinClick(Sender: TObject);
    procedure BtOK_2Click(Sender: TObject);
    procedure BtOK_3_1Click(Sender: TObject);
    procedure RGAcaoPrd_TrocClick(Sender: TObject);
    procedure RGAcaoPrd_TiraClick(Sender: TObject);
    procedure BtOK_4_2Click(Sender: TObject);
    procedure BtOK_5Click(Sender: TObject);
    procedure EdUsoQtdChange(Sender: TObject);
    procedure EdUsoCusUniChange(Sender: TObject);
    procedure EdGraGruXChange(Sender: TObject);
    procedure EdGraGruXEnter(Sender: TObject);
    procedure EdGraGruXExit(Sender: TObject);
    procedure BtOK_3_2_SimClick(Sender: TObject);
    procedure EdNumLote_3_2_SimEnter(Sender: TObject);
    procedure EdNumLote_3_2_SimExit(Sender: TObject);
    procedure EdNumLaudo_3_2_SimEnter(Sender: TObject);
    procedure EdNumLaudo_3_2_SimExit(Sender: TObject);
    procedure EdNumLaudoEnter(Sender: TObject);
    procedure EdNumLoteEnter(Sender: TObject);
    procedure EdNumLoteExit(Sender: TObject);
    procedure EdNumLaudoExit(Sender: TObject);
    procedure UpDown1Click(Sender: TObject; Button: TUDBtnType);
    procedure UpDown2Click(Sender: TObject; Button: TUDBtnType);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    FPreenchendo: Boolean;
    FPulaAfterScrool: Boolean;
    FSelGGX: Integer;
    FNumLaudo, FNumLote: String;
{
    //  Loop
    FLupAtivo: Boolean;
    FLupFiliacao, FLupQtdCiclos, FLupCicloAtual: Integer;
}
    //
    procedure DesativaPipItsPr();
(*
    procedure AlteraPipItsPrIncluido(Acao, GraGruX, ValCliDd: Integer;
              UsoQtd, UsoCusUni, UsoCusTot: Double; NumLote, NumLaudo: String);
*)
    procedure IncluiPipItsPr(Acao, GraGruX, ValCliDd: Integer;
              UsoQtd, UsoCusUni, UsoCusTot: Double; NumLote, NumLaudo: String);
    procedure My_QrOSPipItsAfterScroll();
    procedure PreencheLoteELaudo(GraGruX: Integer; EdLote, EdLaudo: TdmkEdit);


    procedure ReopenGraGruX();
    procedure ReopenQrOPI();

    procedure RePreencheAdicaoPrd();
    procedure RePreencheBinario();
    procedure RePreenchePrgAtrIts();
    procedure RePreencheQuantitativo();
    procedure RePreencheSubstituicaoPrd();
    procedure RePreencheRetiraPrd();
    procedure RePreencheRespAtrTxt();
    procedure RePreencheRespTxtLvr();

    procedure RespPipIts(RespBin: Integer; RespQtd: Double; RespAtrCad,
              RespAtrIts: Integer; RespAtrTxt, RespTxtLvr: String;
              RespPrdBin, Acao: Integer);
    procedure SubstituiPipItsPr(Quantidade: Double; NumLote, NumLaudo: String);
    procedure MudaPrecos();
    procedure TrocaIsca(RespPrdBin, Quantidade: Integer; NumLote, NumLaudo: String);

  public
    { Public declarations }
    //FIDIts, FSMI_IDCtrl: Int64;
    //FDataHoraOrig: TDateTime;
    FOSCab, FLstCusPrd, FEmpresa: Integer;
    //
    procedure ReopenOSPipIts(Conta: Integer);
    procedure ReopenOsPipItsPr(Qry: TmySQLQuery);
    procedure ReopenPrgAtrIts(PrgAtrCad: Integer);
    procedure ReopenSMIA(IDCtrl: Integer);
    procedure OcultaTodosPaineis();
  end;

  var
  FmOSPipIts: TFmOSPipIts;

implementation

uses UnMyObjects, Module, DmkDAC_PF, UnAppListas, UnPerfJan_Tabs,
  UMySQLModule, UnDmkProcFunc, ModProd, ModOS, ModuleGeral;

{$R *.DFM}

{
procedure TFmOSPipIts.AlteraPipItsPrIncluido(Acao, GraGruX, ValCliDd: Integer;
  UsoQtd, UsoCusUni, UsoCusTot: Double; NumLote, NumLaudo: String);
const
  EhDiluente = 0;
  Desativado = 0;
  UsoPrc     = 0;
  UsoVal     = 0;
  UsoDec     = 0;
  UsoTot     = 0;
var
  Codigo, Controle: Integer;
  Conta, IDIts: Int64;
begin
  Codigo   := QrOSPipItsCodigo.Value;
  Controle := QrOSPipItsControle.Value;
  Conta    := QrOSPipItsConta.Value;
  IDIts    := FIDIts;
  //
 // Excluir > BaixaInsumoDeOS? Nao Precisa, vou alterar!
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ospipitspr', False, [
  'Codigo', 'Controle', 'Conta',
  'Acao', 'GraGruX', 'UsoQtd',
  'UsoPrc', 'UsoVal', 'UsoDec',
  'UsoTot', 'ValCliDd', 'EhDiluente',
  'Desativado', 'UsoCusUni', 'UsoCusTot',
  'NumLote', 'NumLaudo'], [
  'IDIts'], [
  Codigo, Controle, Conta,
  Acao, GraGruX, UsoQtd,
  UsoPrc, UsoVal, UsoDec,
  UsoTot, ValCliDd, EhDiluente,
  Desativado, UsoCusUni, UsoCusTot,
  NumLote, NumLaudo], [
  IDIts], True) then
  begin
    case Acao of
      CO_AcaoPrd_Nada: ; // nada
      CO_AcaoPrd_Adic: DmModOS.BaixaInsumoDeOS(VAR_FATID_4103, (*Codigo, Controle, Conta,*) IDIts);
      CO_AcaoPrd_Troc: DmModOS.BaixaInsumoDeOS(VAR_FATID_4104, (*Codigo, Controle, Conta,*) IDIts);
      CO_AcaoPrd_Tira: ; // nada
      else Geral.MB_Erro('Tipo de ac�o n�o implementada em "FmOSPipIts.AlteraPipItsPrIncluido()"');
    end;
  end;
end;
}
procedure TFmOSPipIts.BtOKClick(Sender: TObject);
begin
//erro no final das pergunta de placa!
end;

procedure TFmOSPipIts.BtOK_2Click(Sender: TObject);
const
  Acao = CO_AcaoPrd_Nada (*0*);
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts, RespPrdBin: Integer;
  RespQtd: Double;
begin
  //CO_PRG_LST_QUANTIT = 2;
  RespBin        := 0;
  RespQtd        := EdRespQtd.ValueVariant;
  RespAtrCad     := 0;
  RespAtrIts     := 0;
  RespAtrTxt     := '';
  RespTxtLvr     := '';
  RespPrdBin     := -1;
  //
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr, RespPrdBin, Acao);
  //
{
  if QrOSPipItsLupForma.Value = CO_LupForma_COD_PorLoop then
  begin
    FLupAtivo      := True;
    FLupFiliacao   := QrOSPipItsPrgLstIts.Value;
    FLupQtdCiclos  := Trunc(RespQtd);
    FLupCicloAtual := 0;
  end;
}
  //
  QrOSPipIts.Next;
end;

procedure TFmOSPipIts.BtOK_3_1Click(Sender: TObject);
const
  Acao = CO_AcaoPrd_Adic (*1*);
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts, RespPrdBin: Integer;
  RespQtd: Double;
  //
  GraGruX, ValCliDd: Integer;
  UsoQtd, UsoCusUni, UsoCusTot: Double;
  NumLote, NumLaudo: String;
begin
  //CO_PRG_LST_BXAPROD = 3;
  //CO_AcaoPrd_Adic = 1;
  RespBin        := 0;
  RespQtd        := 0.000;
  RespAtrCad     := 0;
  RespAtrIts     := 0;
  RespAtrTxt     := '';
  RespTxtLvr     := '';
  RespPrdBin     := 1;
  //
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr, RespPrdBin, Acao);
  //
  GraGruX        := EdGraGruX.ValueVariant;
  ValCliDd       := EdValCliDd.ValueVariant;
  UsoQtd         := EdUsoQtd.ValueVariant;
  UsoCusUni      := EdUsoCusUni.ValueVariant;
  UsoCusTot      := EdUsoCusTot.ValueVariant;
  NumLote        := EdNumLote.Text;
  NumLaudo       := EdNumLaudo.Text;
(*
  if ImgTipo.SQLType = stUpd then
    AlteraPipItsPrIncluido(Acao, GraGruX, ValCliDd, UsoQtd, UsoCusUni, UsoCusTot,
    NumLote, NumLaudo)
  else
*)
    IncluiPipItsPr(Acao, GraGruX, ValCliDd, UsoQtd, UsoCusUni, UsoCusTot,
    NumLote, NumLaudo);
  //
  QrOSPipIts.Next;
end;

procedure TFmOSPipIts.BtOK_3_2_SimClick(Sender: TObject);
var
  Quantidade, RespPrdBin: Integer;
  NumLote, NumLaudo: String;
begin
  RespPrdBin := RGAcaoPrd_Troc.ItemIndex;
  Quantidade := Trunc(EdQtdUso_3_2_Sim.ValueVariant);
  NumLote    := EdNumLote_3_2_Sim.Text;
  NumLaudo   := EdNumLaudo_3_2_Sim.Text;
  //
  TrocaIsca(RespPrdBin, Quantidade, NumLote, NumLaudo);
  Panel3_2_Sim.Visible := False;
end;

procedure TFmOSPipIts.BtOK_4_1Click(Sender: TObject);
const
  Acao = CO_AcaoPrd_Nada (*0*);
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts, RespPrdBin: Integer;
  RespQtd: Double;
begin
  //CO_PRG_LST_ATRIBUT = 4;
  //CO_AtrTyp_PreCad = 1;
  //
  RespBin        := 0;
  RespQtd        := 0;
  RespAtrCad     := QrOSPipItsPrgAtrCad.Value;
  // CUIDADO com o CodUsu!
  if not UMyMod.ObtemCodigoDeCodUsu(EdRespAtrIts, RespAtrIts,
  'Informe o item do atributo!', 'Controle') then
    Exit;
  RespAtrTxt     := '';
  RespTxtLvr     := '';
  RespPrdBin     := -1;
  //
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr, RespPrdBin, Acao);
  QrOSPipIts.Next;
end;

procedure TFmOSPipIts.BtOK_4_2Click(Sender: TObject);
const
  Acao = CO_AcaoPrd_Nada (*0*);
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts, RespPrdBin: Integer;
  RespQtd: Double;
begin
  //CO_PRG_LST_ATRIBUT = 4;
  //CO_AtrTyp_Avulso = 2;
  RespBin        := 0;
  RespQtd        := 0;
  RespAtrCad     := 0;
  RespAtrIts     := 0;
  RespAtrTxt     := MeRespAtrTxt.Text;
  RespTxtLvr     := '';
  RespPrdBin     := -1;
  //
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr, RespPrdBin, Acao);
  QrOSPipIts.Next;
end;

procedure TFmOSPipIts.BtOK_5Click(Sender: TObject);
const
  Acao = CO_AcaoPrd_Nada (*0*);
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts, RespPrdBin: Integer;
  RespQtd: Double;
begin
  //CO_PRG_LST_TXTLIVR = 5;
  RespBin        := 0;
  RespQtd        := 0;
  RespAtrCad     := 0;
  RespAtrIts     := 0;
  RespAtrTxt     := '';
  RespTxtLvr     := MeRespTxtLvr.Text;
  RespPrdBin     := -1;
  //
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr, RespPrdBin, Acao);
  QrOSPipIts.Next;
end;

procedure TFmOSPipIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSPipIts.DesativaPipItsPr();
var
  Tabela: Integer;
  TabTxt: String;
begin
  // Nao adiciona nenhum item na tabela "ospipitspr"
  // Mas informa no registro fonte da tabela ? que o produto nao deve mais ser monitorado
  Tabela := QrOSPipItsTabela.Value;
  TabTxt := '';
  case Tabela of
    CO_OSPipIts_OSMONREC   : TabTxt := 'osmonrec';
    CO_OSPipIts_OSPIPITSPR : TabTxt := 'ospipitspr';
    else
    begin
      Geral.MB_ERRO(
      'Tabela n�o definida em remo��o de isca na procedure "DesativaPipItsPr"');
    end;
  end;
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE ' + TabTxt + ' SET ',
  'Desativado=1 ',
  'WHERE Conta=' + Geral.FI64(QrOSPipItsTabIdx.Value),
  '']);
end;

procedure TFmOSPipIts.EdGraGruXChange(Sender: TObject);
begin
  if not EdGraGruX.Focused then
    MudaPrecos();
end;

procedure TFmOSPipIts.EdGraGruXEnter(Sender: TObject);
begin
  FSelGGX := EdGraGruX.ValueVariant;
end;

procedure TFmOSPipIts.EdGraGruXExit(Sender: TObject);
begin
  if EdGraGruX.ValueVariant <> FSelGGX then
  begin
    FSelGGX := EdGraGruX.ValueVariant;
    MudaPrecos();
  end;
end;

procedure TFmOSPipIts.EdNumLaudoEnter(Sender: TObject);
begin
  FNumLaudo := EdNumLaudo.Text;
end;

procedure TFmOSPipIts.EdNumLaudoExit(Sender: TObject);
var
  GraGru1: Integer;
begin
  if EdNumLaudo.Text <> FNumLaudo then
  begin
    if EdGraGruX.ValueVariant <> 0 then
    begin
      GraGru1 := QrGraGruXGraGru1.Value;
      DmProd.AlteraNumXxxSeUserDeseja('NumLaudo', GraGru1, EdNumLaudo.Text);
    end;
  end;
end;

procedure TFmOSPipIts.EdNumLaudo_3_2_SimEnter(Sender: TObject);
begin
  FNumLaudo := EdNumLaudo_3_2_Sim.Text;
end;

procedure TFmOSPipIts.EdNumLaudo_3_2_SimExit(Sender: TObject);
begin
  if EdNumLaudo_3_2_Sim.Text <> FNumLaudo then
    DmProd.AlteraNumXxxSeUserDeseja('NumLaudo', QrOSPipItsGraGru1.Value,
      EdNumLaudo_3_2_Sim.Text);
end;

procedure TFmOSPipIts.EdNumLoteEnter(Sender: TObject);
begin
  FNumLote := EdNumLote.Text;
end;

procedure TFmOSPipIts.EdNumLoteExit(Sender: TObject);
var
  GraGru1: Integer;
begin
  if EdNumLote.Text <> FNumLote then
  begin
    if EdGraGruX.ValueVariant <> 0 then
    begin
      GraGru1 := QrGraGruXGraGru1.Value;
      DmProd.AlteraNumXxxSeUserDeseja('NumLote', GraGru1, EdNumLote.Text);
    end;
  end;
end;

procedure TFmOSPipIts.EdNumLote_3_2_SimEnter(Sender: TObject);
begin
  FNumLote := EdNumLote_3_2_Sim.Text;
end;

procedure TFmOSPipIts.EdNumLote_3_2_SimExit(Sender: TObject);
begin
  if EdNumLote_3_2_Sim.Text <> FNumLote then
    DmProd.AlteraNumXxxSeUserDeseja('NumLote', QrOSPipItsGraGru1.Value,
      EdNumLote_3_2_Sim.Text);
end;

procedure TFmOSPipIts.EdUsoCusUniChange(Sender: TObject);
begin
  DmProd.CalculaCustoProduto(EdUsoQtd, EdUsoCusUni, EdUsoCusTot);
end;

procedure TFmOSPipIts.EdUsoQtdChange(Sender: TObject);
begin
  DmProd.CalculaCustoProduto(EdUsoQtd, EdUsoCusUni, EdUsoCusTot);
end;

procedure TFmOSPipIts.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSPipIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  FPreenchendo := False;
  //FIDIts := 0;
  OcultaTodosPaineis();
end;

procedure TFmOSPipIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPipIts.IncluiPipItsPr(Acao, GraGruX, ValCliDd: Integer;
UsoQtd, UsoCusUni, UsoCusTot: Double; NumLote, NumLaudo: String);
const
  EhDiluente = 0;
  Desativado = 0;
  UsoPrc     = 0;
  UsoVal     = 0;
  UsoDec     = 0;
  UsoTot     = 0;
var
  Codigo, Controle: Integer;
  Conta, IDIts: Int64;
  DataHora: TDateTime;
begin
  Codigo   := QrOSPipItsCodigo.Value;
  Controle := QrOSPipItsControle.Value;
  Conta    := QrOSPipItsConta.Value;
  if QrAntOPIP.State <> dsInactive then
    IDIts := QrAntOPIPIDIts.Value
  else
    IDIts := 0;
  //
  IDIts := UMyMod.BPGS1I64_Reaproveita('ospipitspr', 'IDIts', '', '', tsPos, stIns, IDIts);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ospipitspr', False, [
  'Codigo', 'Controle', 'Conta',
  'Acao', 'GraGruX', 'UsoQtd',
  'UsoPrc', 'UsoVal', 'UsoDec',
  'UsoTot', 'ValCliDd', 'EhDiluente',
  'Desativado', 'UsoCusUni', 'UsoCusTot',
  'NumLote', 'NumLaudo'], [
  'IDIts'], [
  Codigo, Controle, Conta,
  Acao, GraGruX, UsoQtd,
  UsoPrc, UsoVal, UsoDec,
  UsoTot, ValCliDd, EhDiluente,
  Desativado, UsoCusUni, UsoCusTot,
  NumLote, NumLaudo], [
  IDIts], True) then
  begin
    DataHora := 0;
    if QrSMIA.State <> dsInactive then
      DataHora := QrSMIADataHora.Value;
    if DataHora < 2 then
      DataHora := DModG.ObtemAgora();
    case Acao of
      CO_AcaoPrd_Nada: ; // nada
      CO_AcaoPrd_Adic: DmModOS.BaixaInsumoDeOS(VAR_FATID_4103, IDIts, DataHora);
      CO_AcaoPrd_Troc: DmModOS.BaixaInsumoDeOS(VAR_FATID_4104, IDIts, DataHora);
      CO_AcaoPrd_Tira: ; // nada
      else Geral.MB_Erro('Tipo de ac�o n�o implementada em "FmOSPipIts.IncluiPipItsPr()"');
    end;
  end;
end;

procedure TFmOSPipIts.MudaPrecos();
var
  GraGruX: Integer;
begin
  GraGruX := EdGraGruX.ValueVariant;
  if GraGruX = 0 then
  begin
    EdUsoCusUni.ValueVariant := 0;
    EdNumLote.Text  := '';
    EdNumLaudo.Text := '';
  end else
  begin
    if FLstCusPrd = 0 then
      Geral.MB_Aviso('Tabela de custos n�o definida!')
    else
    if QrGraGruXCtrlGGV.Value = 0 then
      Geral.MB_Aviso('Pre�o n�o definido na tabela de custos!');
    //
    EdUsoCusUni.ValueVariant := QrGraGruXCustoPreco.Value;

    // Lote e laudo
    PreencheLoteELaudo(GraGruX, EdNumLote, EdNumLaudo);
  end;
end;

procedure TFmOSPipIts.My_QrOSPipItsAfterScroll();
var
  CP1, CP2, CP3: TCategoryPanel;
begin
  if FPulaAfterScrool then
    Exit;
  //CO_PRG_LST_NENHUMA = 0;
  // ------------------------
  //CO_PRG_LST_BINARIO = 1;
  RGRespBin.ItemIndex := -1;
  //
  //CO_PRG_LST_QUANTIT = 2;
  EdRespQtd.ValueVariant := 0;
  //
  //CO_PRG_LST_BXAPROD = 3;
  EdGraGruX.ValueVariant := 0;
  CBGraGruX.KeyValue := 0;
  EdRespQtd.ValueVariant := QrOSPipItsRespQtd.Value;
  EdRespQtd.Enabled := QrOSPipItsLupForma.Value <> CO_LupForma_COD_PorLoop;
  LaRespQtd.Enabled := EdRespQtd.Enabled;
  EdUsoCusUni.ValueVariant := 0;
  EdUsoCusTot.ValueVariant := 0;
  RGAcaoPrd_Troc.ItemIndex := -1;
  EdUsoQtd.ValueVariant := 1;
  EdNumLote.ValueVariant := '';
  EdNumLaudo.ValueVariant := '';
  EdQtdUso_3_2_Sim.ValueVariant := 1;
  EdNumLote_3_2_Sim.ValueVariant := '';
  EdNumLaudo_3_2_Sim.ValueVariant := '';
  RGAcaoPrd_Tira.ItemIndex := -1;
  //
  //CO_PRG_LST_ATRIBUT = 4;
  EdRespAtrIts.ValueVariant := 0;
  CBRespAtrIts.KeyValue := 0;
  MeRespAtrTxt.Text := '';
  //
  //CO_PRG_LST_TXTLIVR = 5;
  MeRespTxtLvr.Text := '';
  //
  OcultaTodosPaineis();
  case QrOSPipItsFuncoes.Value of
    //CO_PRG_LST_NENHUMA = 0;
    CO_PRG_LST_BINARIO (*1*):
    begin
      CP1 := CategoryPanel1;
      RGRespBin.Items.Clear;
      RGRespBin.Items.Add(QrOSPipItsBinarNAO.Value);
      RGRespBin.Items.Add(QrOSPipItsBinarSIM.Value);
      //
      RePreencheBinario();
    end;
    CO_PRG_LST_QUANTIT (*2*):
    begin
      CP1 := CategoryPanel2;
      RePreencheQuantitativo();
    end;
    CO_PRG_LST_BXAPROD (*3*):
    begin
      CP1 := CategoryPanel3;
      case QrOSPipItsAcaoPrd.Value of
        CO_AcaoPrd_Nada (*0*):
        begin
          //CP2 := CategoryPanel3_0;
        end;
        CO_AcaoPrd_Adic (*1*):
        begin
          CP2 := CategoryPanel3_1;
          if QrGraGruX.State = dsInactive then
            ReopenGraGruX();
          RePreencheAdicaoPrd();
        end;
        CO_AcaoPrd_Troc (*2*):
        begin
          CP2 := CategoryPanel3_2;
          RePreencheSubstituicaoPrd();
        end;
        CO_AcaoPrd_Tira (*3*):
        begin
          CP2 := CategoryPanel3_3;
          RePreencheRetiraPrd();
        end;
      end;
    end;
    CO_PRG_LST_ATRIBUT (*4*):
    begin
      CP1 := CategoryPanel4;
      case QrOSPipItsAtrTyp.Value of
        CO_AtrTyp_Indefi: (*0*) ; // Nada
        CO_AtrTyp_PreCad (*1*): // Pr�-cadastrado
        begin
          CP3 := CategoryPanel4_1;
          LaRespAtrIts.Caption := QrOSPipItsNO_PrgAtrCad.Value;
          ReopenPrgAtrIts(QrOSPipItsPrgAtrCad.Value);
          //
          RePreenchePrgAtrIts();
        end;
        CO_AtrTyp_Avulso (*2*): // Texto avulso
        begin
          CP3 := CategoryPanel4_2;
          RePreencheRespAtrTxt();
        end;
      end;
    end;
    CO_PRG_LST_TXTLIVR (*5*):
    begin
      CP1 := CategoryPanel5;
      RePreencheRespTxtLvr();
    end;
  end;
  //
  CP1.Visible := True;
  CP1.Caption := QrOSPipItsNO_Pergunta.Value;
  CP1.Collapsed := False;
  //
  if QrOSPipItsFuncoes.Value = CO_PRG_LST_BXAPROD then
  begin
    if QrOSPipItsAcaoPrd.Value > 0 then
    begin
      CP2.Visible := True;
      //CP2.Caption := //;
      CP2.Collapsed := False;
      //
      if Panel3_2_Sim.Visible then
        // Evitar erro de "Erro pois o panel pai esta invisivel"
        EdQtdUso_3_2_Sim.SetFocus;
    end;
  end;
  //
  if QrOSPipItsFuncoes.Value = CO_PRG_LST_ATRIBUT then
  begin
    if QrOSPipItsAtrTyp.Value > 0 then
    begin
      CP3.Visible := True;
      //CP2.Caption := //;
      CP3.Collapsed := False;
    end;
  end;
end;

procedure TFmOSPipIts.OcultaTodosPaineis();
var
  I: Integer;
  CategoryPanel: TCategoryPanel;
begin
  for I := 0 to CategoryPanelGroup1.Panels.Count - 1 do
  begin
    CategoryPanel := TCategoryPanel(CategoryPanelGroup1.Panels[I]);
    //
    CategoryPanel.Collapsed := True;
    CategoryPanel.Visible := False;
  end;
  for I := 0 to CategoryPanelGroup2.Panels.Count - 1 do
  begin
    CategoryPanel := TCategoryPanel(CategoryPanelGroup2.Panels[I]);
    //
    CategoryPanel.Collapsed := True;
    CategoryPanel.Visible := False;
  end;
  for I := 0 to CategoryPanelGroup3.Panels.Count - 1 do
  begin
    CategoryPanel := TCategoryPanel(CategoryPanelGroup3.Panels[I]);
    //
    CategoryPanel.Collapsed := True;
    CategoryPanel.Visible := False;
  end;
  CategoryPanel := nil;
  //
  Panel3_2_Sim.Visible := False;
end;

procedure TFmOSPipIts.RePreencheAdicaoPrd();
var
  RespAtrIts: Integer;
begin
  if not CkPreencher.Checked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    if QrOPIRespondido.Value = 1 then
    begin
      ReopenOsPipItsPr(QrOPIPR);
      //
      EdGraGruX.ValueVariant   := QrOPIPRGraGruX.Value;
      CBGraGruX.KeyValue       := QrOPIPRGraGruX.Value;
      EdUsoQtd.ValueVariant    := QrOPIPRUsoQtd.Value;
      EdUsoCusUni.ValueVariant := QrOPIPRUsoCusUni.Value;
      EdUsoCusTot.ValueVariant := QrOPIPRUsoCusTot.Value;
      EdValCliDd.ValueVariant  := QrOPIPRValCliDd.Value;
      EdNumLote.ValueVariant   := QrOPIPRNumLote.Value;
      EdNumLaudo.ValueVariant  := QrOPIPRNumLaudo.Value;
      //
    end;
  finally
    FPreenchendo := False;
  end;
end;

procedure TFmOSPipIts.RePreencheBinario();
begin
  if not CkPreencher.Checked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    //
    if QrOPIRespondido.Value = 1 then
    begin
      RGRespBin.ItemIndex := QrOPIRespBin.Value;
    end;
  finally
    FPreenchendo := False;
  end;
end;

procedure TFmOSPipIts.PreencheLoteELaudo(GraGruX: Integer; EdLote,
  EdLaudo: TdmkEdit);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrG1Any, Dmod.MyDB, [
  'SELECT ggx.Controle, ',
  'pap.ValCliDd, pap.AtuNumLot, pap.AtuNumLaud ',
  'FROM grag1prap pap ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pap.Nivel1 ',
  'LEFT JOIN gragrux ggx ON ggx.GraGru1=pap.Nivel1 ',
  'WHERE ggx.Controle=' + Geral.FF0(GraGruX),
  '',
  'UNION',
  '',
  'SELECT ggx.Controle, ',
  'pmo.ValCliDd, pmo.AtuNumLot, pmo.AtuNumLaud ',
  'FROM grag1prmo pmo ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=pmo.Nivel1 ',
  'LEFT JOIN gragrux ggx ON ggx.GraGru1=pmo.Nivel1 ',
  'WHERE ggx.Controle=' + Geral.FF0(GraGruX),
  '']);
  { N�o usa
  if TbOSFrmRecValCliDd.Value = 0 then
    TbOSFrmRecValCliDd.Value := QrG1AnyValCliDd.Value;
  }
  //if EdLote.Text = '' then
    EdLote.Text := QrG1AnyAtuNumLot.Value;
  //if EdLaudo.Text = '' then
    EdLaudo.Text := QrG1AnyAtuNumLaud.Value;
end;

procedure TFmOSPipIts.RePreenchePrgAtrIts();
var
  RespAtrIts: Integer;
begin
  if not CkPreencher.Checked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    if QrOPIRespondido.Value = 1 then
    begin
      // CUIDADO com o CodUsu!
      RespAtrIts := QrOPIRespAtrIts.Value;
      if QrPrgAtrIts.Locate('Controle', RespAtrIts, []) then
      begin
        RespAtrIts := QrPrgAtrItsCodUsu.Value;
        EdRespAtrIts.ValueVariant := RespAtrIts;
        CBRespAtrIts.KeyValue     := RespAtrIts;
      end;
    end;
  finally
    FPreenchendo := False;
  end;
end;

procedure TFmOSPipIts.RePreencheQuantitativo();
var
  RespAtrIts: Integer;
begin
  if not CkPreencher.Checked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    if QrOPIRespondido.Value = 1 then
    begin
      EdRespQtd.ValueVariant := QrOPIRespQtd.Value;
      //
    end;
  finally
    FPreenchendo := False;
  end;
end;

procedure TFmOSPipIts.RePreencheRespAtrTxt;
var
  RespAtrIts: Integer;
begin
  if not CkPreencher.Checked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    if QrOPIRespondido.Value = 1 then
    begin
      MeRespAtrTxt.Text := QrOPIRespAtrTxt.Value;
    end;
  finally
    FPreenchendo := False;
  end;
end;

procedure TFmOSPipIts.RePreencheRespTxtLvr;
var
  RespAtrIts: Integer;
begin
  if not CkPreencher.Checked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    if QrOPIRespondido.Value = 1 then
    begin
      MeRespTxtLvr.Text := QrOPIRespTxtLvr.Value;
    end;
  finally
    FPreenchendo := False;
  end;
end;

procedure TFmOSPipIts.RePreencheRetiraPrd();
var
  RespAtrIts: Integer;
begin
  if not CkPreencher.Checked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    if QrOPIRespondido.Value = 1 then
    begin
      RGAcaoPrd_Tira.ItemIndex := QrOPIRespPrdBin.Value;
    end;
  finally
    FPreenchendo := False;
  end;
end;

{
procedure TFmOSPipIts.RePreenche???();
var
  RespAtrIts: Integer;
begin
  if not CkPreencher.Checked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    if QrOPIRespondido.Value = 1 then
    begin
      //
    end;
  finally
    FPreenchendo := False;
  end;
end;
}

procedure TFmOSPipIts.RePreencheSubstituicaoPrd();
var
  RespAtrIts: Integer;
begin
  if not CkPreencher.Checked then
    Exit;
  FPreenchendo := True;
  try
    ReopenQrOPI();
    if QrOPIRespondido.Value = 1 then
    begin
      RGAcaoPrd_Troc.ItemIndex := QrOPIRespPrdBin.Value;
      //
      case RGAcaoPrd_Troc.ItemIndex of
        0: Panel3_2_Sim.Visible := False;
        1:
        begin
          ReopenOsPipItsPr(QrOPIPR);
          //
          EdQtdUso_3_2_Sim.ValueVariant := QrOPIPRUsoQtd.Value;
          EdNumLote_3_2_Sim.ValueVariant := QrOPIPRNumLote.Value;
          EdNumLaudo_3_2_Sim.ValueVariant := QrOPIPRNumLaudo.Value;
          //
          Panel3_2_Sim.Visible := True;
          // Erro pois o panel pai esta invisivel
          //EdQtdUso_3_2_Sim.SetFocus;
        end;
      end;
    end;
  finally
    FPreenchendo := False;
  end;
end;

procedure TFmOSPipIts.QrOSPipItsAfterScroll(DataSet: TDataSet);
begin
  My_QrOSPipItsAfterScroll();
  if QrOSPipIts.Eof then
    Close; // S h o w M e s s a g e ( ' E o f ' ) ;
end;

procedure TFmOSPipIts.ReopenGraGruX();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrGraGruX, Dmod.MyDB, [
{
  'SELECT ggx.Controle, gg1.Nome NO_PRODUTO ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'ORDER BY NO_PRODUTO ',
}
  'SELECT ggx.GraGru1, ggx.Controle GraGruX, ggv.Controle CtrlGGV, ',
  'ggv.CustoPreco, CONCAT(gg1.Nome, ',
  'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
  'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
  'NO_PRD_TAM_COR ',
  'FROM gragrux ggx ',
  'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
  'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
  'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
  'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
  'LEFT JOIN gragruval ggv ON ggv.GraGruX=ggx.Controle',
  '  AND ggv.GraCusPrc=' + Geral.FF0(FLstCusPrd),
  '  AND ggv.Entidade=' + Geral.FF0(FEmpresa),
  'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
  '']);
end;

procedure TFmOSPipIts.ReopenOSPipIts(Conta: Integer);
var
  SQL_Respondido: String;
begin
  if Conta = 0 then
    SQL_Respondido := 'AND opi.Respondido=0 '
  else
    SQL_Respondido := 'AND opi.Conta=' + Geral.FF0(Conta);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPipIts, Dmod.MyDB, [
  'SELECT pip.Nome NO_PIP, pac.Nome NO_PrgAtrCad, pac.AtrTyp, ',
  'pbs.Nome BinarSIM, pbn.Nome BinarNAO, ',
  'pcp.Nome NO_PERGUNTA, ggx.GraGru1, opi.* ',
  'FROM ospipits opi ',
  'LEFT JOIN ospipmon  opm ON opm.Controle=opi.Controle',
  'LEFT JOIN pipcad    pip ON pip.Codigo=opm.PipCad ',
  'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta ',
  'LEFT JOIN prgbincad pbs ON pbs.Codigo=opi.BinarCad1 ',
  'LEFT JOIN prgbincad pbn ON pbn.Codigo=opi.BinarCad0 ',
  'LEFT JOIN prgatrcad pac ON pac.Codigo=opi.PrgAtrCad',
  'LEFT JOIN gragrux ggx ON ggx.Controle=opi.GraGruX',
  'WHERE opi.Codigo=' + Geral.FF0(FOSCab),
  SQL_Respondido,
  'ORDER BY Controle, SuperOrd, SuperSub, SobreOrd, Ordem, SubOrdem',
  '']);
  //
end;

procedure TFmOSPipIts.ReopenPrgAtrIts(PrgAtrCad: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrgAtrIts, Dmod.MyDB, [
  'SELECT CodUsu, Controle, Nome ',
  'FROM prgatrits ',
  'WHERE Codigo=' + Geral.FF0(PrgAtrCad),
  'ORDER BY Nome' +
  '']);
end;

procedure TFmOSPipIts.ReopenQrOPI();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOPI, Dmod.MyDB, [
  'SELECT * ',
  'FROM ospipits ',
  'WHERE Conta=' + Geral.FI64(QrOSPipItsConta.Value),
  'AND Respondido=1 ',
  '']);
end;

procedure TFmOSPipIts.ReopenSMIA(IDCtrl: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSMIA, Dmod.MyDB, [
  'SELECT * ',
  'FROM stqmovitsa ',
  'WHERE IDCtrl=' + Geral.FF0(IDCtrl),
  '']);
end;

procedure TFmOSPipIts.ReopenOsPipItsPr(Qry: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT * ',
  'FROM ospipitspr ',
  'WHERE Conta=' + Geral.FI64(QrOPIConta.Value),
  '']);
end;

procedure TFmOSPipIts.RespPipIts(RespBin: Integer; RespQtd: Double; RespAtrCad,
  RespAtrIts: Integer; RespAtrTxt, RespTxtLvr: String; RespPrdBin, Acao: Integer);
const
  Respondido = CO_Respondido_SIM;
var
  Conta: Int64;
  Qry: TmySQLQuery;
  Tipo: Integer;
begin
  Conta := QrOSPipItsConta.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ospipits', False, [
  'Respondido', 'RespBin', 'RespQtd',
  'RespAtrCad', 'RespAtrIts', 'RespAtrTxt',
  'RespTxtLvr', 'RespPrdBin'], ['Conta'], [
  Respondido, RespBin, RespQtd,
  RespAtrCad, RespAtrIts, RespAtrTxt,
  RespTxtLvr, RespPrdBin], [Conta], True) then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      case Acao of
        CO_AcaoPrd_Nada: Tipo := 0; // nada
        CO_AcaoPrd_Adic: Tipo := VAR_FATID_4103;
        CO_AcaoPrd_Troc: Tipo := VAR_FATID_4104;
        CO_AcaoPrd_Tira: Tipo := 0; // nada
        else Geral.MB_Erro('Tipo de ac�o n�o implementada em "FmOSPipIts.RespPipIts()"');
      end;
(*
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT IDCtrl ',
      'FROM stqmovitsa ',
      'WHERE Tipo=' + Geral.FF0(Tipo),
      'AND OriCodi=' + Geral.FF0(QrOSPipItsCodigo.Value), // 3650 Localizador da OS
      'AND OriCtrl=' + Geral.FF0(QrOSPipItsControle.Value), //   399980
      'AND OriCnta=' + Geral.FF0(QrOSPipItsConta.Value), //  30828
      'AND Empresa=' + Geral.FF0(FEmpresa), //  -11
      '']);
      if Qry.FieldByName('IDCtrl').AsInteger <> 0 then
      begin
        // Exclui itens de movimentacao estoque caso foram criados em resposta anterior
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'DELETE FROM stqmovitsa ',
        'WHERE IDCtrl=' + Geral.FF0(Qry.FieldByName('IDCtrl').AsInteger),
        '']);
      end;
*)
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'DELETE FROM stqmovitsa ',
      'WHERE Tipo=' + Geral.FF0(Tipo),
      'AND OriCodi=' + Geral.FF0(QrOSPipItsCodigo.Value), // 3650 Localizador da OS
      'AND OriCtrl=' + Geral.FF0(QrOSPipItsControle.Value), //   399980
      'AND OriCnta=' + Geral.FF0(QrOSPipItsConta.Value), //  30828
      'AND Empresa=' + Geral.FF0(FEmpresa), //  -11
      '']);
    finally
      Qry.Free;
    end;
    // Exclui itens de movimentacao estoque caso foram criados em resposta anterior
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'DELETE FROM ospipitspr ',
    'WHERE Conta=' + Geral.FI64(Conta),
    '']);
  end;
end;

procedure TFmOSPipIts.RGAcaoPrd_TiraClick(Sender: TObject);
const
  Acao = CO_AcaoPrd_Nada (*0*);
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts, RespPrdBin: Integer;
  RespQtd: Double;
begin
  //CO_PRG_LST_BXAPROD = 3;
  //CO_AcaoPrd_Tira = 3;
  RespBin        := 0;
  RespQtd        := 0;
  RespAtrCad     := 0;
  RespAtrIts     := 0;
  RespAtrTxt     := '';
  RespTxtLvr     := '';
  RespPrdBin     := RGAcaoPrd_Tira.ItemIndex;
  //
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr, RespPrdBin, Acao);
  if RespPrdBin = 1 then
    DesativaPipItsPr();
  //
  QrOSPipIts.Next;
end;

procedure TFmOSPipIts.RGAcaoPrd_TrocClick(Sender: TObject);
const
  Quantidade = 0;
  NumLote    = '';
  NumLaudo   = '';
var
  RespPrdBin, GraGruX: Integer;
var
  Tabela: Integer;
  TabTxt: String;
begin
  if FPreenchendo then
    Exit;
  RespPrdBin := RGAcaoPrd_Troc.ItemIndex;
  //
  case RespPrdBin of
    0: TrocaIsca(RespPrdBin, Quantidade, NumLote, NumLaudo);
    1:
    begin
      Tabela := QrOSPipItsTabela.Value;
      TabTxt := '';
      case Tabela of
        CO_OSPipIts_OSMONREC   : TabTxt := 'osmonrec';
        CO_OSPipIts_OSPIPITSPR : TabTxt := 'ospipitspr';
        else
        begin
          Geral.MB_ERRO(
          'Tabela n�o definida em substitui��o de isca na procedure "RGAcaoPrd_TrocClick"');
        end;
      end;
      // ... para saber a quantidade!
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
      'SELECT ValCliDd, UsoQtd, UsoCusUni, UsoCusTot ',
      'FROM ' + TabTxt,
      'WHERE Conta=' + Geral.FI64(QrOSPipItsTabIdx.Value),
      '']);
      EdUsoQtd.ValueVariant := QrOriUsoQtd.Value;
      //
      // Lote e laudo
      GraGruX := QrOSPipItsGraGruX.Value;
      PreencheLoteELaudo(GraGruX, EdNumLote_3_2_Sim, EdNumLaudo_3_2_Sim);
      //
      Panel3_2_Sim.Visible := True;
      EdQtdUso_3_2_Sim.SetFocus;
    end;
  end;
end;

procedure TFmOSPipIts.RGRespBinClick(Sender: TObject);
const
  Acao = CO_AcaoPrd_Nada (*0*);
  //
  procedure LimpaRegistro();
  var
    RespTxtLvr, RespAtrTxt: String;
    RespBin, RespAtrCad, RespAtrIts, RespPrdBin: Integer;
    RespQtd: Double;
  begin
    //CO_PRG_LST_ATRIBUT = 4;
    //CO_AtrTyp_Avulso = 2;
    RespBin        := 0;
    RespQtd        := 0;
    RespAtrCad     := 0;
    RespAtrIts     := 0;
    RespAtrTxt     := '';
    RespTxtLvr     := '';
    RespPrdBin     := -1;
    //
    RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr, RespPrdBin, Acao);
  end;
const
  Respondido = CO_Respondido_SIM;
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts, RespPrdBin, Nivel, Controle: Integer;
  RespQtd: Double;
  Foi: Boolean;
  Conta: Int64;
  MinhaConta, Vezes: Integer;
begin
  if FPreenchendo then
    Exit;
  //CO_PRG_LST_NENHUMA = 0;
  //CO_PRG_LST_BINARIO = 1;
  RespBin        := RGRespBin.ItemIndex;
  RespQtd        := 0;
  RespAtrCad     := 0;
  RespAtrIts     := 0;
  RespAtrTxt     := '';
  RespTxtLvr     := '';
  RespPrdBin     := -1;
  //
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr, RespPrdBin, Acao);
  if RespBin = 1 then
  begin
    QrOSPipIts.Next;
  end else
  begin
    //CategoryPanelGroup1.Visible := False;
    FPulaAfterScrool := True;
    try
      //
      Nivel    := QrOSPipItsNivel.Value;
      Controle := QrOSPipItsControle.Value;
      Conta    := QrOSPipItsConta.Value;
      QrOSPipIts.Next;
      MinhaConta := 0;
      Vezes := 0;
      while (QrOSPipItsNivel.Value > Nivel) and
      (QrOSPipItsControle.Value = Controle) do
      begin
        if Conta = QrOSPipItsConta.Value then
        begin
          Close;
          Exit;
        end;
        // 2014-02-18
        if MinhaConta = QrOSPipItsConta.Value then
        begin
          Vezes := Vezes + 1;
          if Vezes >= 3 then
          begin
            Close;
            Exit;
          end;
        end;
        // FIM 2014-02-18
        //
        LimpaRegistro();
{
        Conta := QrOSPipItsConta.Value;
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
        'UPDATE ospipits ',
        'SET Respondido=' + Geral.FF0(Respondido),
        'WHERE Conta=' + Geral.FF0(Conta),
        '']);
}
        //
        MinhaConta := QrOSPipItsConta.Value;
        QrOSPipIts.Next;
      end;
    finally
      FPulaAfterScrool := False;
    end;
    My_QrOSPipItsAfterScroll();
  end;
end;

procedure TFmOSPipIts.SubstituiPipItsPr(Quantidade: Double; NumLote, NumLaudo: String);
const
  Acao = CO_AcaoPrd_Troc (*2*);
var
  Tabela: Integer;
  TabTxt: String;
var
  GraGruX, ValCliDd: Integer;
  UsoQtd, UsoCusUni, UsoCusTot: Double;
begin
  // Desativa atual...
  DesativaPipItsPr();
  Tabela := QrOSPipItsTabela.Value;
  TabTxt := '';
  case Tabela of
    CO_OSPipIts_OSMONREC   : TabTxt := 'osmonrec';
    CO_OSPipIts_OSPIPITSPR : TabTxt := 'ospipitspr';
    else
    begin
      Geral.MB_ERRO(
      'Tabela n�o definida em substitui��o de isca na procedure "SubstituiPipItsPr"');
    end;
  end;
  // ... para incluir novo!
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOri, Dmod.MyDB, [
  'SELECT ValCliDd, UsoQtd, UsoCusUni, UsoCusTot ',
  'FROM ' + TabTxt,
  'WHERE Conta=' + Geral.FI64(QrOSPipItsTabIdx.Value),
  '']);
  GraGruX        := QrOSPipItsGraGrux.Value;
  ValCliDd       := QrOriValCliDd.Value;
  UsoQtd         := Quantidade; //QrOriUsoQtd.Value;
  UsoCusUni      := 0;
  if FLstCusPrd = 0 then
    Geral.MB_Aviso('Tabela de custos n�o definida!')
  else
  begin
    if QrGraGruX.State = dsInactive then
      ReopenGraGruX();
    if QrGraGruX.Locate('GraGruX', GraGruX, []) then
    begin
      if QrGraGruXCtrlGGV.Value = 0 then
        Geral.MB_Aviso('Pre�o n�o definido na tabela de custos!')
      else
        UsoCusUni := QrGraGruXCustoPreco.Value;
    end else
    begin
      Geral.MB_Aviso('Reduzido n�o localizado na tabela de precos')
    end;
  end;
  if UsoCusUni = 0 then
  begin
    UsoCusUni := QrOriUsoCusUni.Value;
    DmProd.ObtemPreco(UsoCusUni);
  end;
  UsoCusTot := UsoCusUni * UsoQtd;
  //
  IncluiPipItsPr(Acao, GraGruX, ValCliDd, UsoQtd, UsoCusUni, UsoCusTot,
    NumLote, NumLaudo);
  //
end;

procedure TFmOSPipIts.TrocaIsca(RespPrdBin, Quantidade: Integer;
NumLote, NumLaudo: String);
const
  Acao = CO_AcaoPrd_Troc (*2*);
var
  RespTxtLvr, RespAtrTxt: String;
  RespBin, RespAtrCad, RespAtrIts: Integer;
  RespQtd: Double;
begin
  //CO_PRG_LST_BXAPROD = 3;
  //CO_AcaoPrd_Troc = 2;
  //
  RespBin        := 0;
  RespQtd        := 0;
  RespAtrCad     := 0;
  RespAtrIts     := 0;
  RespAtrTxt     := '';
  RespTxtLvr     := '';
  //RespPrdBin     := RGAcaoPrd_Troc.ItemIndex;
  //
  RespPipIts(RespBin, RespQtd, RespAtrCad, RespAtrIts, RespAtrTxt, RespTxtLvr,
    RespPrdBin, Acao);
  //
  if RespPrdBin = 1 then
    SubstituiPipItsPr(Quantidade, NumLote, NumLaudo);
  //
  QrOSPipIts.Next;
end;

procedure TFmOSPipIts.UpDown1Click(Sender: TObject; Button: TUDBtnType);
begin
  case Button of
    btNext: EdUsoQtd.ValueVariant := EdUsoQtd.ValueVariant + 1;
    btPrev: EdUsoQtd.ValueVariant := EdUsoQtd.ValueVariant - 1;
  end;
  if EdUsoQtd.ValueVariant < 1 then EdUsoQtd.ValueVariant := 1;
end;

procedure TFmOSPipIts.UpDown2Click(Sender: TObject; Button: TUDBtnType);
begin
  case Button of
    btNext: EdQtdUso_3_2_Sim.ValueVariant := EdQtdUso_3_2_Sim.ValueVariant + 1;
    btPrev: EdQtdUso_3_2_Sim.ValueVariant := EdQtdUso_3_2_Sim.ValueVariant - 1;
  end;
  if EdQtdUso_3_2_Sim.ValueVariant < 1 then EdQtdUso_3_2_Sim.ValueVariant := 1;
end;

end.
