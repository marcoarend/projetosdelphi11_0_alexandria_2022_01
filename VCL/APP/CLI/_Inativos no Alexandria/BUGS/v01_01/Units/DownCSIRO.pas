unit DownCSIRO;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, IdBaseComponent, IdComponent, IdTCPConnection,
  IdTCPClient, IdHTTP, Variants, dmkImage, DmkDAC_PF, UnDmkProcFunc, Menus,
  UnDmkEnums;

type
  TFmDownCSIRO = class(TForm)
    IdHTTP1: TIdHTTP;
    QrVersao: TmySQLQuery;
    QrVersaoVersao: TWideStringField;
    QrDupl: TmySQLQuery;
    QrDuplCodTxt: TWideStringField;
    QrSorc: TmySQLQuery;
    QrDest: TmySQLQuery;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    Panel16: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    StatusBar: TStatusBar;
    PB2: TProgressBar;
    Memo1: TMemo;
    Panel5: TPanel;
    Memo2: TMemo;
    Panel2: TPanel;
    Label5: TLabel;
    CkForcaCad: TCheckBox;
    BtOK: TBitBtn;
    EdMaxReg: TdmkEdit;
    TabSheet2: TTabSheet;
    Panel7: TPanel;
    Panel20: TPanel;
    Label6: TLabel;
    SpeedButton1: TSpeedButton;
    EdDiretorio: TdmkEdit;
    Panel21: TPanel;
    StaticText3: TStaticText;
    MeExtensoes: TMemo;
    BtArquivos: TBitBtn;
    CkSub1: TCheckBox;
    BtPesquisa: TBitBtn;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    ListBox1: TListBox;
    TabSheet4: TTabSheet;
    Panel8: TPanel;
    Label4: TLabel;
    EdTexto: TdmkEdit;
    Panel9: TPanel;
    GradeA: TStringGrid;
    Panel10: TPanel;
    Label7: TLabel;
    ListBox3: TListBox;
    GradeB: TStringGrid;
    CkCaseSensitive: TCheckBox;
    Panel19: TPanel;
    StaticText2: TStaticText;
    MeExcl: TMemo;
    PBScan: TProgressBar;
    Ed1Scaned: TdmkEdit;
    Label8: TLabel;
    Ed1Present: TdmkEdit;
    Label9: TLabel;
    Label10: TLabel;
    Ed1Incident: TdmkEdit;
    Me1Txt: TMemo;
    TabSheet5: TTabSheet;
    Panel11: TPanel;
    Panel12: TPanel;
    Ed_1_2_Arq: TdmkEdit;
    Me_1_2_Txt: TMemo;
    Sb_1_2_Abre: TSpeedButton;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    Me_1_4_Txt: TMemo;
    Panel13: TPanel;
    Sb_1_4_Lista: TSpeedButton;
    Ed_1_4_Col: TdmkEdit;
    Label11: TLabel;
    Grade_1_3_A: TStringGrid;
    Label12: TLabel;
    Ed_1_4_Cols: TdmkEdit;
    Label13: TLabel;
    Ed_1_4_Txt: TdmkEdit;
    PM_1_3_A: TPopupMenu;
    Largurasdagrade1: TMenuItem;
    BtCarregar: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BtArquivosClick(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure ListBox1DblClick(Sender: TObject);
    procedure Sb_1_2_AbreClick(Sender: TObject);
    procedure Sb_1_4_ListaClick(Sender: TObject);
    procedure Largurasdagrade1Click(Sender: TObject);
    procedure BtCarregarClick(Sender: TObject);
  private
    { Private declarations }
    FWorkCount, FWorkCountMax: Integer;
    FScanAborted: Boolean;
    //
    function  DownloadFileHTTP(Fonte, Destino: String): Boolean;
    //
    procedure Pesquisa_1_1();
    procedure Pesquisa_1_3();
    procedure Carrega_1_3();
    function LimpaTexto(Texto: String): String;

  public
    { Public declarations }
  end;

  var
  FmDownCSIRO: TFmDownCSIRO;

implementation

uses UnMyObjects, dmkGeral, Module, UMySQLModule;

{$R *.DFM}

const
  FTagImg = '<td width="57%" rowspan="10"><img src=..';


procedure TFmDownCSIRO.BtArquivosClick(Sender: TObject);
var
  I: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    ListBox1.Clear;
    //MyObjects.LimpaGrade(GradeA, 1, 1, True);
    for I := 0 to MeExtensoes.Lines.Count -1 do
    begin
      MyObjects.GetAllFiles(CkSub1.Checked, EdDiretorio.Text + '\' +
      MeExtensoes.Lines[I], ListBox1, False, LaAviso1, LaAviso2);
    end;
    BtPesquisa.Enabled := True;
    Pagecontrol1.ActivePageIndex := 1;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDownCSIRO.BtCarregarClick(Sender: TObject);
begin
  case PageControl2.ActivePageIndex of
    3: Carrega_1_3();
    else Geral.MB_Aviso('Carregamento n�o implementada!');
  end;
end;

procedure TFmDownCSIRO.BtOKClick(Sender: TObject);
const
  // Global Names: http://gni.globalnames.org/data_sources/30?search_term=barata&commit=Search
  // Fotos: http://eol.org/pages/1/overview
  // Systematic Names: http://www.ces.csiro.au/aicn/system/system.htm
  // Museu: http://www.mnh.si.edu/
  // Database names: http://www.itis.gov/
  //                 http://www.gbif.org/
  //                 http://www.catalogueoflife.org/
  //                 http://www.ipni.org/
  //                 http://www.ubio.org/
  /////////////////////////////////////////////
  ///
  ///  UBio WebServer: http://www.ubio.org/index.php?pagename=soap_tools
  ///
  ///  ///////////////////////////////////////
  ///
  ///  Todos sites:
  ///  http://gni.globalnames.org/data_sources/2/data_source_overlaps
  ///
  /////////////////////////////////////////////////////////////////////////////
  DirF = 'http://www.ces.csiro.au/aicn/system/';
  DirI = 'http://www.ces.csiro.au/aicn';
  DirD = 'C:\Dermatek\Taxonomia\Imagens\CSIRO\';
var
  //pF,
  MaxTabs, I, C, pI: Integer;
  //Versao, Linha,
  Tabela, Fonte, Destino, ImgDest, ArqImg: String;
  lstArq(*, lstLin*): TStringList;
  Achou: Boolean;
begin
  MaxTabs := EdMaxReg.ValueVariant;
  if Geral.MB_Pergunta('Confirma a tentativa de baixar ' + Geral.FF0(MaxTabs) +
  ' registros da URL CSIRO?') <>
  ID_YES then
    Exit;
  if Geral.MB_Pergunta('Confirma a tentativa de baixar ' + Geral.FF0(MaxTabs) +
  ' registros da URL CSIRO?') <>
  ID_YES then
    Exit;
  Screen.Cursor := crHourGlass;
  try
    Memo1.Lines.Clear;
    PB1.Position := 0;
    PB1.Max := MaxTabs;
    if not DirectoryExists(DirD) then
      ForceDirectories(DirD);
    for I := 1 to MaxTabs do
    begin
      PB1.Position := PB1.Position + 1;
      Tabela := FormatFloat('0', I);
      MyObjects.Informa(LaAviso1, True, 'P�gina: ' + Tabela + '. Baixando');
      //
      Achou := False;
      for C := 97 to 122 do
      begin
        if not Achou then
        begin
          Fonte := DirF + Char(C) + '_' + Tabela + '.htm';
          //
          Destino := DirD + Char(C) + '_' + Tabela + '.htm';
          if FileExists(Destino) then
            DeleteFile(Destino);
          try
            if DownloadFileHTTP(Fonte, Destino) then
            begin
              if FileExists(Destino) then
              begin
                MyObjects.Informa(LaAviso1, True, 'Tabela: ' +
                Tabela + '. Arquivo baixado. Verificando arquivo');
                lstArq := TStringList.Create;
                try
                  lstArq.LoadFromFile(Destino);
                  if lstArq.Count > 0 then
                  begin
                    if pos('The page cannot be found', lstArq.Text) = 0 then
                    begin
                      Achou := True;
                      pI := pos(FTagImg, lstArq.Text);
                      if pI > 0 then
                      begin
                        //pF :=
                        ArqImg := Copy(lstArq.Text, pI + Length(FTagImg));
                        ArqImg := Copy(ArqImg, 1, Pos('></td>', ArqImg) - 1);
                        if Length(ArqImg) > 0 then
                        begin
                          ImgDest := DirD + dmkPF.InverteBarras(ArqImg);
                          ArqImg := DirI + ArqImg;
                          try
                            if not DownloadFileHTTP(ArqImg, ImgDest) then
                            begin
                              Memo2.Lines.Add('Img[3]: ' + Geral.FF0(I) + ' n�o encontrado.');
                            end;
                          except
                            Memo2.Lines.Add('Img[2]: ' + Geral.FF0(I) + ' n�o encontrado.');
                          end;
                        end;
                      end else
                      begin
                        Memo2.Lines.Add('Img[1]: ' + Geral.FF0(I) + ' n�o encontrado.');
                      end;
                    end else
                    begin
                      DeleteFile(Destino);
                    end;
                  end else
                    DeleteFile(Destino);
                finally
                  if lstArq <> nil then
                    lstArq.Free;
                end;
              end;
            end;
          except
            if FileExists(Destino) then
              DeleteFile(Destino);
          end;
        end;
      end;
      //
      if not Achou then
      begin
        Memo2.Lines.Add('Txt: ' + Geral.FF0(I) + ' n�o encontrado.');
        Memo2.Update;
      end;
      Application.ProcessMessages;
    end;
      MyObjects.Informa(LaAviso1, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDownCSIRO.BtPesquisaClick(Sender: TObject);
begin
  case PageControl2.ActivePageIndex of
    1: Pesquisa_1_1();
    3: Pesquisa_1_3();
    else Geral.MB_Aviso('Pesquisa n�o implementada!');
  end;
end;
procedure TFmDownCSIRO.Pesquisa_1_1();
  procedure NomeArqImg(Arquivo, TextoLinha: String);
  var
    ArqImg: String;
    pI: Integer;
  begin
    pI := pos(FTagImg, TextoLinha);
    if pI > 0 then
    begin
      //pF :=
      ArqImg := Copy(TextoLinha, pI + Length(FTagImg));
      ArqImg := Copy(ArqImg, 1, Pos('></td>', ArqImg) - 1);
      Me1Txt.Lines.Add('OK ' + ArqImg);
    end else
    begin
      Me1Txt.Lines.Add(Arquivo);
      Me1Txt.Lines.Add(TextoLinha);
    end;
  end;
var
  i, n, k, m, ic, kc: integer;
  Lista: TStringList;
  NaoQuer: Boolean;
  //
  Achou: Integer;
begin
  Screen.Cursor := crHourGlass;
  FScanAborted := False;
  Lista := TStringList.Create;
  MyObjects.LimpaGrade(GradeA, 1, 1, True);
  MyObjects.LimpaGrade(GradeB, 1, 1, True);
  Ed1Scaned.ValueVariant := 0;
  Ed1Present.ValueVariant := 0;
  Ed1Incident.ValueVariant := 0;
  PBScan.Position := 0;
  PBScan.Max := ListBox1.Items.Count;
  Me1Txt.Lines.Clear;
  //
  ic := 0;
  kc := 0;
  for i := 0 to ListBox1.Items.Count -1 do
  begin
    Achou := 0;
    if FScanAborted then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    n := dmkPF.ScanFile(ListBox1.Items[i], EdTexto.Text, CkCaseSensitive.Checked);
    if n > 0 then
    begin
      ic := ic + 1;
      GradeA.RowCount := ic + 1;
      GradeA.Cells[0,ic] := IntToStr(ic);
      GradeA.Cells[1,ic] := IntToStr(n);
      GradeA.Cells[2,ic] := ListBox1.Items[i];
      //ListBox2.Items.Add(ListBox1.Items[i]+' Caracter ->'+IntToStr(n));
      Lista.Clear;
      Lista.LoadFromFile(ListBox1.Items[i]);
      //
      for k := 0 to Lista.Count -1 do
      begin
        if Pos(LowerCase(EdTexto.Text), LowerCase(Lista.Strings[k])) > 0 then
        begin
          NaoQuer := False;
          for m := 0 to MeExcl.Lines.Count - 1 do
          begin
            if Pos(LowerCase(MeExcl.Lines[m]), LowerCase(Lista.Strings[k])) > 0 then
            begin
              NaoQuer := True;
              Break;
            end;
          end;
          if not NaoQuer then
          begin
            kc := kc + 1;
            GradeB.RowCount := kc+1;
            GradeB.Cells[0,kc] := IntToStr(kc);
            GradeB.Cells[1,kc] := ListBox1.Items[i];
            GradeB.Cells[2,kc] := IntToStr(k);
            GradeB.Cells[3,kc] := Lista.Strings[k];
            GradeB.Cells[4,kc] := LimpaTexto(Lista.Strings[k]);
            //ListBox3.Items.Add(ListBox1.Items[i]+ '  ['+IntToStr(k)+']  '+
              //Lista.Strings[k]);
            Achou := Achou + 1;
            NomeArqImg(ListBox1.Items[i], Lista.Strings[k]);
          end;
        end;
        Application.ProcessMessages;
      end;
    end;
    //
    Ed1Scaned.ValueVariant := Ed1Scaned.ValueVariant + 1;
    if Achou > 0 then
    begin
      Ed1Present.ValueVariant := Ed1Present.ValueVariant + 1;
      Ed1Incident.ValueVariant := Ed1Incident.ValueVariant + Achou;
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmDownCSIRO.Pesquisa_1_3();
{
  procedure NomeArqImg(Arquivo, TextoLinha: String);
  var
    ArqImg: String;
    pI: Integer;
  begin
    pI := pos(FTagImg, TextoLinha);
    if pI > 0 then
    begin
      //pF :=
      ArqImg := Copy(TextoLinha, pI + Length(FTagImg));
      ArqImg := Copy(ArqImg, 1, Pos('></td>', ArqImg) - 1);
      Me1Txt.Lines.Add('OK ' + ArqImg);
    end else
    begin
      Me1Txt.Lines.Add(Arquivo);
      Me1Txt.Lines.Add(TextoLinha);
    end;
  end;
}
var
  //Y, C, M, J, kc, X,
  I, N, K, T, F, G, ic: integer;
  Lista: TStringList;
  //NaoQuer: Boolean;
  //
  Achou: Integer;
  //, _of
  TxtLoc, CamImg, _Page: String;

begin
  Screen.Cursor := crHourGlass;
  FScanAborted := False;
  Lista := TStringList.Create;
  MyObjects.LimpaGrade(Grade_1_3_A, 1, 1, True);
  Grade_1_3_A.FixedRows := 1;
  Ed1Scaned.ValueVariant := 0;
  Ed1Present.ValueVariant := 0;
  Ed1Incident.ValueVariant := 0;
  PBScan.Position := 0;
  PBScan.Max := ListBox1.Items.Count;
  //Me1Txt.Lines.Clear;
  for I := 0 to Grade_1_3_A.ColCount do
    Grade_1_3_A.Cells[I,0] := Geral.FF0(I);
  //
  Grade_1_3_A.Cells[00, 0] := 'Seq';
  Grade_1_3_A.Cells[01, 0] := 'Item';
  Grade_1_3_A.Cells[02, 0] := 'Arquivo';
  Grade_1_3_A.Cells[03, 0] := 'Linhas';
  Grade_1_3_A.Cells[04, 0] := 'Titulo';
  Grade_1_3_A.Cells[05, 0] := 'CRC';
  Grade_1_3_A.Cells[06, 0] := 'Esp�cie';
  Grade_1_3_A.Cells[07, 0] := 'Nome Popular';
  Grade_1_3_A.Cells[08, 0] := 'Nome Popular';
  Grade_1_3_A.Cells[09, 0] := 'Nome Popular';
  //
  Grade_1_3_A.Cells[10, 0] := 'Origem';
  Grade_1_3_A.Cells[11, 0] := 'Filo';
  Grade_1_3_A.Cells[12, 0] := 'Classe';
  Grade_1_3_A.Cells[13, 0] := 'Ordem';
  Grade_1_3_A.Cells[14, 0] := 'Direitos autorais da imagem';
  Grade_1_3_A.Cells[15, 0] := 'Familia';
  Grade_1_3_A.Cells[16, 0] := 'Notas:';
  Grade_1_3_A.Cells[17, 0] := 'Notas:';
  Grade_1_3_A.Cells[18, 0] := 'Notas:';
  Grade_1_3_A.Cells[19, 0] := 'Notas:';
  //
  Grade_1_3_A.Cells[20, 0] := 'P�gina';
  Grade_1_3_A.Cells[21, 0] := 'P�gina';
  Grade_1_3_A.Cells[22, 0] := 'Arquivo de imagem';
  //
  Grade_1_3_A.ColWidths[00] := 036;
  Grade_1_3_A.ColWidths[01] := 036;
  Grade_1_3_A.ColWidths[02] := 264;
  Grade_1_3_A.ColWidths[03] := 036;
  Grade_1_3_A.ColWidths[04] := 240;
  Grade_1_3_A.ColWidths[05] := 016;
  Grade_1_3_A.ColWidths[06] := 184;
  Grade_1_3_A.ColWidths[07] := 116;
  Grade_1_3_A.ColWidths[08] := 076;
  Grade_1_3_A.ColWidths[09] := 072;
  Grade_1_3_A.ColWidths[10] := 036;
  Grade_1_3_A.ColWidths[11] := 086;
  Grade_1_3_A.ColWidths[12] := 072;
  Grade_1_3_A.ColWidths[13] := 064;
  Grade_1_3_A.ColWidths[14] := 100;
  Grade_1_3_A.ColWidths[15] := 076;
  Grade_1_3_A.ColWidths[16] := 040;
  Grade_1_3_A.ColWidths[17] := 040;
  Grade_1_3_A.ColWidths[18] := 040;
  Grade_1_3_A.ColWidths[19] := 040;
  Grade_1_3_A.ColWidths[20] := 064;
  Grade_1_3_A.ColWidths[21] := 048;
  Grade_1_3_A.ColWidths[22] := 116;
  //
  ic := 0;
  //kc := 0;
  for i := 0 to ListBox1.Items.Count -1 do
  begin
    //T := 0;
    PBScan.Position := PBScan.Position + 1;
    Grade_1_3_A.RowCount := I + 1;
    Grade_1_3_A.Cells[0,ic] := IntToStr(I);
    //
    Achou := 0;
    if FScanAborted then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    //n := dmkPF.ScanFile(ListBox1.Items[i], EdTexto.Text, CkCaseSensitive.Checked);
    n := 1;
    if n > 0 then
    begin
      ic := ic + 1;
      Grade_1_3_A.Cells[1,ic] := IntToStr(ic);
      Grade_1_3_A.Cells[2,ic] := ListBox1.Items[i];
      //
      //
      Lista.Clear;
      Lista.LoadFromFile(ListBox1.Items[i]);
      Grade_1_3_A.Cells[3,ic] := Geral.FF0(Lista.Count);
      K := pos('<title>', Lista[2]);
      if K > 0 then
        Grade_1_3_A.Cells[4,ic] := LimpaTexto(Lista[2])
      else
        Geral.MB_Aviso('Item: ' + Geral.FF0(I) + ' Arquivo:' + sLineBreak +
        sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
        'Item n�o localizado: TITULO');

      Grade_1_3_A.Cells[06,ic] := LimpaTexto(Lista[073]); // Esp�cie
      //Grade_1_3_A.Cells[07,ic] := LimpaTexto(Lista[081]); // Texto: "Common Name"
      Grade_1_3_A.Cells[07,ic] := LimpaTexto(Lista[086]); // Nome comum (Popular)
      Grade_1_3_A.Cells[08,ic] := LimpaTexto(Lista[090]); //
      Grade_1_3_A.Cells[09,ic] := LimpaTexto(Lista[094]);
      //Grade_1_3_A.Cells[11,ic] := LimpaTexto(Lista[102]); Not verified
      //Grade_1_3_A.Cells[12,ic] := LimpaTexto(Lista[103]); Present
      Grade_1_3_A.Cells[10,ic] := LimpaTexto(Lista[107]);
      Grade_1_3_A.Cells[11,ic] := LimpaTexto(Lista[112]);
      Grade_1_3_A.Cells[12,ic] := LimpaTexto(Lista[116]);
      Grade_1_3_A.Cells[13,ic] := LimpaTexto(Lista[120]);
      Grade_1_3_A.Cells[14,ic] := LimpaTexto(Lista[123]);
      //Grade_1_3_A.Cells[15,ic] := LimpaTexto(Lista[124]); // family:
      Grade_1_3_A.Cells[15,ic] := LimpaTexto(Lista[125]);
      //
      Grade_1_3_A.Cells[16,ic] := LimpaTexto(Lista[128]); // Notes:
      Grade_1_3_A.Cells[17,ic] := LimpaTexto(Lista[129]);
      Grade_1_3_A.Cells[18,ic] := LimpaTexto(Lista[130]);
      Grade_1_3_A.Cells[19,ic] := LimpaTexto(Lista[132]);
      Grade_1_3_A.Cells[20,ic] := LimpaTexto(Lista[134]);
      Grade_1_3_A.Cells[21,ic] := LimpaTexto(Lista[135]);
      //x :=  23;
      if k = 124 then
      begin
        // </font></b></td>
        if LimpaTexto(Lista[k]) <> 'family:' then
          Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
          sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
          'Valor encontrado onde n�o esperado: "' +
          LimpaTexto(Lista[k]) + '"');
      end;


      for k := 87 to 89 do
      begin
        TxtLoc := LimpaTexto(Lista[k]);
        if Trim(TxtLoc) <> '' then
          Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
          sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
          'Item fora de Ordem: ' + TxtLoc);
      end;
      //
      for k := 91 to 93 do
      begin
        TxtLoc := LimpaTexto(Lista[k]);
        if Trim(TxtLoc) <> '' then
          Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
          sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
          'Item fora de Ordem: ' + TxtLoc);
      end;
      //
      for k := 95 to 101 do
      begin
        TxtLoc := LimpaTexto(Lista[k]);
        if Trim(TxtLoc) <> '' then
          Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
          sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
          'Item fora de Ordem: ' + TxtLoc);
      end;
      //
      for k := 104 to 106 do
      begin
        TxtLoc := LimpaTexto(Lista[k]);
        if Trim(TxtLoc) <> '' then
          Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
          sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
          'Item fora de Ordem: ' + TxtLoc);
      end;
      for k := 108 to 110 do
      begin
        TxtLoc := LimpaTexto(Lista[k]);
        if Trim(TxtLoc) <> '' then
          Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
          sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
          'Item fora de Ordem: ' + TxtLoc);
      end;
      //
      for k := 113 to 114 do
      begin
        TxtLoc := LimpaTexto(Lista[k]);
        if Trim(TxtLoc) <> '' then
          Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
          sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
          'Item fora de Ordem: ' + TxtLoc);
      end;
      //
      for k := 117 to 118 do
      begin
        TxtLoc := LimpaTexto(Lista[k]);
        if Trim(TxtLoc) <> '' then
          Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
          sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
          'Item fora de Ordem: ' + TxtLoc);
      end;
      //
      for k := 121 to 122 do
      begin
        TxtLoc := LimpaTexto(Lista[k]);
        if Trim(TxtLoc) <> '' then
          Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
          sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
          'Item fora de Ordem: ' + TxtLoc);
      end;
      //
      for k := 126 to 127 do
      begin
        TxtLoc := LimpaTexto(Lista[k]);
        if Trim(TxtLoc) <> '' then
          Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
          sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
          'Item fora de Ordem: ' + TxtLoc);
      end;
      //
      for k := 133 to 133 do
      begin
        TxtLoc := LimpaTexto(Lista[k]);
        if Trim(TxtLoc) <> '' then
          Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
          sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
          'Item fora de Ordem: ' + TxtLoc);
      end;
      //
      //
      for k := 0 to Lista.Count -1 do
      begin
        {
        Y := 148;
        if k > Y then
        begin
          TxtLoc := LimpaTexto(Lista[k]);
          C := X + K - Y -1;
          Grade_1_3_A.Cells[C, ic] := LimpaTexto(Lista[k]);
        end;
        }
        TxtLoc := 'page ';
        T := Pos('page ', LowerCase(Lista.Strings[k]));
        if T > 0 then
        begin
          _page := LimpaTexto(Copy(LowerCase(Lista.Strings[k]), T + 4));
          Grade_1_3_A.Cells[23, ic] := _page;
          Grade_1_3_A.Cells[24, ic] := LimpaTexto(LowerCase(Lista.Strings[k+1]));
        end;
        //
        TxtLoc := FTagImg;
        T := Pos(LowerCase(TxtLoc), LowerCase(Lista.Strings[k]));
        if T > 0 then
        begin
          CamImg := Copy(Lista.Strings[k], T + Length(TxtLoc));
          G := Pos('/images/', CamImg);
          if G = 1 then
            G := 9
          else
            G := 1;
          F := Pos('></td>', CamImg) - G;
          if F > 0 then
            CamImg := Copy(CamImg, G, F)
          else
            CamImg := Copy(CamImg, G);
          //
          Grade_1_3_A.Cells[22, ic] := CamImg;
          CamImg := 'C:\Dermatek\Taxonomia\Imagens\CSIRO\images\' + CamImg;
          if not FileExists(CamImg) then
          begin
            Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
            sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
            'Imagem n�o localizada: ' + CamImg);
          end;

          if (k <> 79) then
          begin
            Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
            sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
            'Item fora de Ordem: ' + TxtLoc);
          end;
        end;
        //
        TxtLoc := 'Phylum:';
        if Pos(LowerCase(TxtLoc), LowerCase(Lista.Strings[k])) > 0 then
        begin
          Grade_1_3_A.Cells[5,ic] := Grade_1_3_A.Cells[5,ic] + '#' + Geral.FFN(k, 4) + ' ';
          if (k <> 66) and (k <> 111) then
          begin
            Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
            sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
            'Item fora de Ordem: ' + TxtLoc);
          end;
        end;
        TxtLoc := 'Class:';
        if Pos(LowerCase(TxtLoc), LowerCase(Lista.Strings[k])) > 0 then
        begin
          Grade_1_3_A.Cells[5,ic] := Grade_1_3_A.Cells[5,ic] + '#' + Geral.FFN(k, 4) + ' ';
          if (k <> 67) and (k <> 115) then
          begin
            Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
            sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
            'Item fora de Ordem: ' + TxtLoc);
          end;
        end;
        TxtLoc := 'Order:';
        if Pos(LowerCase(TxtLoc), LowerCase(Lista.Strings[k])) > 0 then
        begin
          Grade_1_3_A.Cells[5,ic] := Grade_1_3_A.Cells[5,ic] + '#' + Geral.FFN(k, 4) + ' ';
          if (k <> 68) and (k <> 119) then
          begin
            Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
            sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
            'Item fora de Ordem: ' + TxtLoc);
          end;
        end;
        TxtLoc := 'Family:';
        if Pos(LowerCase(TxtLoc), LowerCase(Lista.Strings[k])) > 0 then
        begin
          Grade_1_3_A.Cells[5,ic] := Grade_1_3_A.Cells[5,ic] + '#' + Geral.FFN(k, 4) + ' ';
          if (k <> 69) and (k <> 124) then
          begin
            Geral.MB_Aviso('Linha: ' + Geral.FF0(k) + ' Arquivo:' + sLineBreak +
            sLineBreak + ListBox1.Items[i] + sLineBreak + sLineBreak +
            'Item fora de Ordem: ' + TxtLoc);
          end;
        end;
        Application.ProcessMessages;
      end;
    end;
    //
    Ed1Scaned.ValueVariant := Ed1Scaned.ValueVariant + 1;
    if Achou > 0 then
    begin
      Ed1Present.ValueVariant := Ed1Present.ValueVariant + 1;
      Ed1Incident.ValueVariant := Ed1Incident.ValueVariant + Achou;
    end;
  end;
  Grade_1_3_A.FixedRows := 1;
  BtCarregar.Enabled := Grade_1_3_A.RowCount > 2;
  Screen.Cursor := crDefault;
  //
end;

procedure TFmDownCSIRO.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDownCSIRO.Carrega_1_3;
var
  Fonte, Especie, Popular1, Popular2, Popular3, Popular4, Popular5, Origem, Filo, Classe, Ordem, Familia, Copyright, Observacao, ArqDir, ArqImg, ArqTxt: String;
  Codigo: Integer;
  //
  I, Max: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Max := Grade_1_3_A.RowCount - 1;
    PBScan.Position := 0;
    PBScan.Max := Max;
    for I := 1 to Max do
    begin
      PBScan.Position := PBScan.Position + 1;
      //
      Fonte          := 'CSIRO';
      Codigo         := Geral.IMV(Trim(Grade_1_3_A.Cells[23, I]));
      Especie        := Trim(Grade_1_3_A.Cells[06, I]);
      Popular1       := Trim(Grade_1_3_A.Cells[07, I]);
      Popular2       := Trim(Grade_1_3_A.Cells[08, I]);
      Popular3       := Trim(Grade_1_3_A.Cells[09, I]);
      Popular4       := '';
      Popular5       := '';
      Origem         := Trim(Grade_1_3_A.Cells[10, I]);
      Filo           := Trim(Grade_1_3_A.Cells[11, I]);
      Classe         := Trim(Grade_1_3_A.Cells[12, I]);
      Ordem          := Trim(Grade_1_3_A.Cells[13, I]);
      Familia        := Trim(Grade_1_3_A.Cells[15, I]);
      Copyright      := Trim(Grade_1_3_A.Cells[14, I]);
      ArqDir         := 'CSIRO\images';
      ArqImg         := Trim(Grade_1_3_A.Cells[22, I]);
      ArqTxt         := ExtractFileName(Grade_1_3_A.Cells[02, I]);
      Observacao     := Trim(Grade_1_3_A.Cells[16, I]) + ' ' +
                        Trim(Grade_1_3_A.Cells[17, I]) + ' ' +
                        Trim(Grade_1_3_A.Cells[18, I]) + ' ' +
                        Trim(Grade_1_3_A.Cells[19, I]);

    //Grade_1_3_A.Cells[00, 0] := 'Seq';
    //Grade_1_3_A.Cells[01, 0] := 'Item';
    //Grade_1_3_A.Cells[02, 0] := 'Arquivo';
    //Grade_1_3_A.Cells[03, 0] := 'Linhas';
    //Grade_1_3_A.Cells[04, 0] := 'Titulo';
    //Grade_1_3_A.Cells[05, 0] := 'CRC';
    //Grade_1_3_A.Cells[06, 0] := 'Esp�cie';
    //Grade_1_3_A.Cells[07, 0] := 'Nome Popular';
    //Grade_1_3_A.Cells[08, 0] := 'Nome Popular';
    //Grade_1_3_A.Cells[09, 0] := 'Nome Popular';
    //
    //Grade_1_3_A.Cells[10, 0] := 'Origem';
    //Grade_1_3_A.Cells[11, 0] := 'Filo';
    //Grade_1_3_A.Cells[12, 0] := 'Classe';
    //Grade_1_3_A.Cells[13, 0] := 'Ordem';
    //Grade_1_3_A.Cells[14, 0] := 'Direitos autorais da imagem';
    //Grade_1_3_A.Cells[15, 0] := 'Familia';
    //Grade_1_3_A.Cells[16, 0] := 'Notas:';
    //Grade_1_3_A.Cells[17, 0] := 'Notas:';
    //Grade_1_3_A.Cells[18, 0] := 'Notas:';
    //Grade_1_3_A.Cells[19, 0] := 'Notas:';
    //
    //Grade_1_3_A.Cells[20, 0] := 'P�gina';
    //Grade_1_3_A.Cells[21, 0] := 'P�gina';
    //Grade_1_3_A.Cells[22, 0] := 'Arquivo de imagem';
    //
      //if
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'taxonnet', False, [
      'Especie', 'Popular1', 'Popular2',
      'Popular3', 'Popular4', 'Popular5',
      'Origem', 'Filo', 'Classe',
      'Ordem', 'Familia', 'Copyright',
      'Observacao', 'ArqDir', 'ArqImg',
      'ArqTxt'], [
      'Fonte', 'Codigo'], [
      Especie, Popular1, Popular2,
      Popular3, Popular4, Popular5,
      Origem, Filo, Classe,
      Ordem, Familia, Copyright,
      Observacao, ArqDir, ArqImg,
      ArqTxt], [
      Fonte, Codigo], True);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmDownCSIRO.DownloadFileHTTP(Fonte, Destino: String): Boolean;
var
  MyFile: TFileStream;
begin
  //Result := False;
  FWorkCount := 0;
  FWorkCountMax := 0;
  MyFile := TFileStream.Create(Destino, fmCreate); // local no hd e nome do arquivo com a extens�o, onde vai salvar.
  try
    //IdHTTP1.Get('http://www.arquivojuridico.com/'+arquivo, MyFile); // fazendo o download do arquivo
    IdHTTP1.Get(Fonte, MyFile); // fazendo o download do arquivo
    Result := True;
  finally
    MyFile.Free;
  end;
end;

procedure TFmDownCSIRO.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDownCSIRO.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  Pagecontrol1.ActivePageIndex := 0;
  Pagecontrol2.ActivePageIndex := 0;
end;

procedure TFmDownCSIRO.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDownCSIRO.Largurasdagrade1Click(Sender: TObject);
var
  I: Integer;
  Txt: String;
begin
  Txt := '';
  for I := 0 to Grade_1_3_A.ColCount - 1 do
  begin
    Txt := Txt + '  ' + Grade_1_3_A.Name + '.ColWidths[' + Geral.FFN(I, 2) +
      '] := ' + Geral .FFN(Grade_1_3_A.ColWidths[I], 3) + ';' + sLineBreak;
  end;
  Geral.MB_Aviso(Txt);
end;

function TFmDownCSIRO.LimpaTexto(Texto: String): String;
begin
  Result := Geral.RemoveTagsHTML(Texto);
  Result := Geral.Substitui(Result, '&nbsp;', ' ');
  Result := Trim(Result);
  Result := dmkPF.ParseText(Result, True);
end;

procedure TFmDownCSIRO.ListBox1DblClick(Sender: TObject);
begin
  Ed_1_2_Arq.Text := ListBox1.Items[ListBox1.ItemIndex];
  PageControl1.ActivePageIndex := 1;
  PageControl2.ActivePageIndex := 2;
end;

procedure TFmDownCSIRO.Sb_1_2_AbreClick(Sender: TObject);
var
  Texto: String;
  Txt: String;
  P: Integer;
begin
  if Geral.LeArquivoToString(Ed_1_2_Arq.Text, Texto) then
  begin
    P := pos('<table', Texto);
    Txt := Copy(Texto, P);
    Me_1_2_Txt.Text := LimpaTexto(Texto);
  end;
end;

procedure TFmDownCSIRO.Sb_1_4_ListaClick(Sender: TObject);
var
  I, L, N, C, Q: Integer;
begin
  try
    N := 0;
    Me_1_4_Txt.Lines.Clear;
    I := Ed_1_4_Col.ValueVariant;
    Q := Ed_1_4_Cols.ValueVariant;
    PBScan.Position := 0;
    PBScan.Max := Grade_1_3_A.RowCount * Q;
    for C := I to I + Q - 1 do
    begin
      for L := 1 to Grade_1_3_A.RowCount -1 do
      begin
        PBScan.Position := PBScan.Position + 1;
        if Lowercase(Trim(Grade_1_3_A.Cells[C, L])) <> Lowercase(Ed_1_4_Txt.Text) then
        begin
          N := N + 1;
          Me_1_4_Txt.Lines.Add(Geral.FFN(N, 6) + '#' + Geral.FFN(L, 6) + ': ' +
            Grade_1_3_A.Cells[C, L]);
          //
          Me_1_4_Txt.Update;
          Application.ProcessMessages;
        end;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDownCSIRO.SpeedButton1Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdDiretorio);
end;

end.
