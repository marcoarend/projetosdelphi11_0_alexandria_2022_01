unit TaxonNet;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmTaxonNet = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    QrFilo: TmySQLQuery;
    DsFilo: TDataSource;
    QrTaxonNet: TmySQLQuery;
    DBGrid1: TDBGrid;
    DsTaxonNet: TDataSource;
    Label1: TLabel;
    LaRegLoc: TLabel;
    QrTaxonNetFonte: TWideStringField;
    QrTaxonNetCodigo: TIntegerField;
    QrTaxonNetEspecie: TWideStringField;
    QrTaxonNetPopular1: TWideStringField;
    QrTaxonNetPopular2: TWideStringField;
    QrTaxonNetPopular3: TWideStringField;
    QrTaxonNetPopular4: TWideStringField;
    QrTaxonNetPopular5: TWideStringField;
    QrTaxonNetOrigem: TWideStringField;
    QrTaxonNetFilo: TWideStringField;
    QrTaxonNetClasse: TWideStringField;
    QrTaxonNetOrdem: TWideStringField;
    QrTaxonNetFamilia: TWideStringField;
    QrTaxonNetCopyright: TWideStringField;
    QrTaxonNetObservacao: TWideMemoField;
    QrTaxonNetArqDir: TWideStringField;
    QrTaxonNetArqImg: TWideStringField;
    QrTaxonNetArqTxt: TWideStringField;
    QrTaxonNetLk: TIntegerField;
    QrTaxonNetDataCad: TDateField;
    QrTaxonNetDataAlt: TDateField;
    QrTaxonNetUserCad: TIntegerField;
    QrTaxonNetUserAlt: TIntegerField;
    QrTaxonNetAlterWeb: TSmallintField;
    QrTaxonNetAtivo: TSmallintField;
    Panel6: TPanel;
    RGFilFilo: TRadioGroup;
    EdFilFilo: TdmkEditCB;
    CBFilFilo: TdmkDBLookupComboBox;
    Panel7: TPanel;
    RgFilClasse: TRadioGroup;
    EdFilClasse: TdmkEditCB;
    CBFilClasse: TdmkDBLookupComboBox;
    QrClasse: TmySQLQuery;
    DsClasse: TDataSource;
    Panel8: TPanel;
    RGFilOrdem: TRadioGroup;
    EdFilOrdem: TdmkEditCB;
    CBFilOrdem: TdmkDBLookupComboBox;
    QrOrdem: TmySQLQuery;
    DsOrdem: TDataSource;
    Panel9: TPanel;
    RGFilFamilia: TRadioGroup;
    EdFilFamilia: TdmkEditCB;
    CBFilFamilia: TdmkDBLookupComboBox;
    DsFamilia: TDataSource;
    QrFamilia: TmySQLQuery;
    Panel10: TPanel;
    EdEspecie: TdmkEdit;
    Label2: TLabel;
    BtConfirma: TBitBtn;
    BtVerFoto: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RGFilFiloClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure QrTaxonNetBeforeClose(DataSet: TDataSet);
    procedure QrTaxonNetAfterOpen(DataSet: TDataSet);
    procedure RgFilClasseClick(Sender: TObject);
    procedure CBFilFiloClick(Sender: TObject);
    procedure QrFiloAfterOpen(DataSet: TDataSet);
    procedure CBFilFiloKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrFiloBeforeClose(DataSet: TDataSet);
    procedure RGFilOrdemClick(Sender: TObject);
    procedure CBFilClasseClick(Sender: TObject);
    procedure CBFilClasseKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBFilOrdemClick(Sender: TObject);
    procedure CBFilOrdemKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure RGFilFamiliaClick(Sender: TObject);
    procedure CBFilFamiliaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdEspecieChange(Sender: TObject);
    procedure EdFilFiloChange(Sender: TObject);
    procedure EdFilClasseChange(Sender: TObject);
    procedure EdFilOrdemChange(Sender: TObject);
    procedure EdFilFamiliaChange(Sender: TObject);
    procedure CBFilFamiliaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtVerFotoClick(Sender: TObject);
  private
    { Private declarations }
    //FItemFilo, FItemClasse, FItemOrdem, FItemFamilia: Integer;
    //
    procedure HabilitaNivel(RGFil: TRadioGroup; EdFil: TdmkEdit;
              CBFil: TdmkDBLookupComboBox(*; var FNivel: Integer*));
    procedure ReopenFilo();
    procedure ReopenClasse();
    procedure ReopenOrdem();
    procedure ReopenFamilia();
    procedure FechaPesquisa();
    procedure SelecionaItem();
  public
    { Public declarations }
    FSelecionou: Boolean;
  end;

  var
  FmTaxonNet: TFmTaxonNet;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF;

{$R *.DFM}

procedure TFmTaxonNet.BtConfirmaClick(Sender: TObject);
begin
  SelecionaItem();
end;

procedure TFmTaxonNet.BtOKClick(Sender: TObject);
var
  Filtro: String;
begin
  Filtro := 'WHERE Especie LIKE "%' + EdEspecie.Text + '%"' + sLineBreak;
  case RGFilFilo.ItemIndex  of
    1: Filtro := Filtro + 'AND Filo LIKE "%' + EdFilFilo.Text + '%"' + sLineBreak;
    2: Filtro := Filtro + 'AND Filo = "' + CBFilFilo.Text + '"' + sLineBreak;
  end;
  //
  case RGFilClasse.ItemIndex  of
    1: Filtro := Filtro + 'AND Classe LIKE "%' + EdFilClasse.Text + '%"' + sLineBreak;
    2: Filtro := Filtro + 'AND Classe = "' + CBFilClasse.Text + '"' + sLineBreak;
  end;
  //
  case RGFilOrdem.ItemIndex  of
    1: Filtro := Filtro + 'AND Ordem LIKE "%' + EdFilOrdem.Text + '%"' + sLineBreak;
    2: Filtro := Filtro + 'AND Ordem = "' + CBFilOrdem.Text + '"' + sLineBreak;
  end;
  //
  case RGFilFamilia.ItemIndex  of
    1: Filtro := Filtro + 'AND Familia LIKE "%' + EdFilFamilia.Text + '%"' + sLineBreak;
    2: Filtro := Filtro + 'AND Familia = "' + CBFilFamilia.Text + '"' + sLineBreak;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTaxonNet, Dmod.MyDB, [
  'SELECT * ',
  'FROM taxonnet ',
  Filtro,
  'ORDER BY Filo ',
  '']);
  //
end;

procedure TFmTaxonNet.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTaxonNet.BtVerFotoClick(Sender: TObject);
begin
//  Fazer ap�s definir onde ficam as fotos!
end;

procedure TFmTaxonNet.CBFilClasseClick(Sender: TObject);
begin
  ReopenOrdem;
  FechaPesquisa();
end;

procedure TFmTaxonNet.CBFilClasseKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DELETE then
  begin
    CBFilClasse.KeyValue := Null;
    //ReopenOrdem();
  end;
end;

procedure TFmTaxonNet.CBFilFiloClick(Sender: TObject);
begin
  ReopenClasse();
  FechaPesquisa();
end;

procedure TFmTaxonNet.CBFilFiloKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DELETE then
  begin
    CBFilFilo.KeyValue := Null;
    //ReopenClasse();
  end;
end;

procedure TFmTaxonNet.CBFilOrdemClick(Sender: TObject);
begin
  ReopenFamilia();
  FechaPesquisa();
end;

procedure TFmTaxonNet.CBFilOrdemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DELETE then
  begin
    CBFilOrdem.KeyValue := Null;
    //ReopenFamilia();
  end;
end;

procedure TFmTaxonNet.DBGrid1DblClick(Sender: TObject);
begin
  SelecionaItem();
end;

procedure TFmTaxonNet.EdEspecieChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTaxonNet.EdFilClasseChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTaxonNet.EdFilFamiliaChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTaxonNet.EdFilFiloChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTaxonNet.EdFilOrdemChange(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTaxonNet.CBFilFamiliaClick(Sender: TObject);
begin
  FechaPesquisa();
end;

procedure TFmTaxonNet.CBFilFamiliaKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if Key = VK_DELETE then
  begin
    CBFilFamilia.KeyValue := Null;
  end;
end;

procedure TFmTaxonNet.FechaPesquisa();
begin
  QrTaxonNet.Close;
end;

procedure TFmTaxonNet.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTaxonNet.FormCreate(Sender: TObject);
begin
  FSelecionou := False;
  ImgTipo.SQLType := stLok;
  ReopenFilo();
end;

procedure TFmTaxonNet.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTaxonNet.HabilitaNivel(RGFil: TRadioGroup; EdFil: TdmkEdit;
  CBFil: TdmkDBLookupComboBox);
begin
  EdFil.Enabled := RGFil.ItemIndex = 1;
  CBFil.Enabled := RGFil.ItemIndex = 2;
end;

procedure TFmTaxonNet.QrFiloAfterOpen(DataSet: TDataSet);
begin
  ReopenClasse();
end;

procedure TFmTaxonNet.QrFiloBeforeClose(DataSet: TDataSet);
begin
  QrClasse.Close;
end;

procedure TFmTaxonNet.QrTaxonNetAfterOpen(DataSet: TDataSet);
begin
  LaRegLoc.Caption := Geral.FFN(QrTaxonNet.RecordCount, 3);
  BtConfirma.Enabled := True;
end;

procedure TFmTaxonNet.QrTaxonNetBeforeClose(DataSet: TDataSet);
begin
  LaRegLoc.Caption := '000';
  BtConfirma.Enabled := False;
end;

procedure TFmTaxonNet.ReopenClasse();
var
  Filtro: String;
begin
  Filtro := '';
  if CBFilFilo.Text <> '' then
    Filtro := 'WHERE Filo = "' + CBFilFilo.Text + '"';
  UnDmkDAC_PF.AbreMySQLQuery0(QrClasse, Dmod.MyDB, [
  'SELECT DISTINCT Classe ',
  'FROM taxonnet ',
  Filtro,
  'ORDER BY Classe ',
  '']);
end;

procedure TFmTaxonNet.ReopenOrdem();
var
  Filtro: String;
begin
  Filtro := '';
  if CBFilClasse.Text <> '' then
    Filtro := 'WHERE Classe = "' + CBFilClasse.Text + '"';
  UnDmkDAC_PF.AbreMySQLQuery0(QrOrdem, Dmod.MyDB, [
  'SELECT DISTINCT Ordem ',
  'FROM taxonnet ',
  Filtro,
  'ORDER BY Ordem ',
  '']);
end;

procedure TFmTaxonNet.ReopenFamilia();
var
  Filtro: String;
begin
  Filtro := '';
  if CBFilOrdem.Text <> '' then
    Filtro := 'WHERE Ordem = "' + CBFilOrdem.Text + '"';
  UnDmkDAC_PF.AbreMySQLQuery0(QrFamilia, Dmod.MyDB, [
  'SELECT DISTINCT Familia ',
  'FROM taxonnet ',
  Filtro,
  'ORDER BY Familia ',
  '']);
end;

procedure TFmTaxonNet.ReopenFilo();
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFilo, Dmod.MyDB, [
  'SELECT DISTINCT Filo ',
  'FROM taxonnet ',
  'ORDER BY Filo ',
  '']);
end;

procedure TFmTaxonNet.RgFilClasseClick(Sender: TObject);
begin
  HabilitaNivel(RGFilClasse, EdFilClasse, CBFilClasse);
  FechaPesquisa();
end;

procedure TFmTaxonNet.RGFilFamiliaClick(Sender: TObject);
begin
  HabilitaNivel(RGFilFamilia, EdFilFamilia, CBFilFamilia);
  FechaPesquisa();
end;

procedure TFmTaxonNet.RGFilFiloClick(Sender: TObject);
begin
  HabilitaNivel(RGFilFilo, EdFilFilo, CBFilFilo);
  FechaPesquisa();
end;

procedure TFmTaxonNet.RGFilOrdemClick(Sender: TObject);
begin
  HabilitaNivel(RGFilOrdem, EdFilOrdem, CBFilOrdem);
  FechaPesquisa();
end;

procedure TFmTaxonNet.SelecionaItem();
begin
  FSelecionou :=
    (QrTaxonNet.State <> dsInactive) and (QrTaxonNet.RecordCount > 0);
  if FSelecionou then
    Close;
end;

end.
