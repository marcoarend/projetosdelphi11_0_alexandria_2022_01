unit TxtGeneric;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, UnAppListas,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkCheckGroup, dmkMemo, UnDmkEnums, UnProjGroup_Consts;

type
  TFmTxtGeneric = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrTxtGeneric: TmySQLQuery;
    DsTxtGeneric: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrTxtGenericCodigo: TIntegerField;
    QrTxtGenericNome: TWideStringField;
    QrTxtGenericTexto: TWideMemoField;
    QrTxtGenericAplicacao: TIntegerField;
    DBCGAplicacao: TdmkDBCheckGroup;
    DBMemo1: TDBMemo;
    CGAplicacao: TdmkCheckGroup;
    MeTexto: TdmkMemo;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTxtGenericAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTxtGenericBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmTxtGeneric: TFmTxtGeneric;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTxtGeneric.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTxtGeneric.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTxtGenericCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTxtGeneric.DefParams;
begin
  VAR_GOTOTABELA := 'txtgeneric';
  VAR_GOTOMYSQLTABLE := QrTxtGeneric;
  VAR_GOTONEG := gotoNiP;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM txtgeneric');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTxtGeneric.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTxtGeneric.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTxtGeneric.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmTxtGeneric.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTxtGeneric.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTxtGeneric.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTxtGeneric.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTxtGeneric.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmTxtGeneric.BtAlteraClick(Sender: TObject);
begin
  if (QrTxtGeneric.State <> dsInactive) and (QrTxtGeneric.RecordCount > 0) then
  begin
    UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrTxtGeneric, [PnDados],
      [PnEdita], EdNome, ImgTipo, 'txtgeneric');
  end;
end;

procedure TFmTxtGeneric.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTxtGenericCodigo.Value;
  Close;
end;

procedure TFmTxtGeneric.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('txtgeneric', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrTxtGenericCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'txtgeneric', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTxtGeneric.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'txtgeneric', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmTxtGeneric.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrTxtGeneric, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'txtgeneric');
end;

procedure TFmTxtGeneric.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MeTexto.Align := alClient;
  DBMemo1.Align := alClient;
  //
  MyObjects.ConfiguraCheckGroup(CGAplicacao, sListaAplicacaoTxtGeneric,
    QtdColsAplicacaoTxtGeneric, 0, False);
  //
  DBCGAplicacao.Items   := CGAplicacao.Items;
  DBCGAplicacao.Columns := CGAplicacao.Columns;
  //
  CriaOForm;
end;

procedure TFmTxtGeneric.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTxtGenericCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTxtGeneric.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTxtGeneric.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrTxtGenericCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTxtGeneric.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmTxtGeneric.QrTxtGenericAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTxtGeneric.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmTxtGeneric.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTxtGenericCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'txtgeneric', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTxtGeneric.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmTxtGeneric.QrTxtGenericBeforeOpen(DataSet: TDataSet);
begin
  QrTxtGenericCodigo.DisplayFormat := FFormatFloat;
end;

end.

