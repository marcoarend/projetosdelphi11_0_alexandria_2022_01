object FmRMIP_R005: TFmRMIP_R005
  Left = 0
  Top = 0
  Caption = 'RMIP 005'
  ClientHeight = 679
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object frxReport005A: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39951.885535092600000000
    ReportOptions.LastChange = 41611.839779444450000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    Left = 52
    Top = 8
    Datasets = <
      item
        DataSet = frxDs005A_OSs
        DataSetName = 'frxDs005A_OSs'
      end
      item
        DataSet = frxDs005A_PrgLstIts
        DataSetName = 'frxDs005A_PrgLstIts'
      end
      item
        DataSet = frxDs005A_OSPipIts
        DataSetName = 'frxDs005A_OSPipIts'
      end
      item
        DataSet = frxDs005B_PrgLstIts
        DataSetName = 'frxDs005B_PrgLstIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader4: TfrxPageHeader
        Height = 124.724463150000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 60.472480000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 41.574803150000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 41.574830000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 41.574830000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Top = 83.149660000000000000
          Width = 680.315400000000000000
          Height = 41.574803150000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_AVISO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MD002: TfrxMasterData
        Height = 37.795300000000000000
        Top = 204.094620000000000000
        Width = 680.315400000000000000
        DataSet = frxDs005A_PrgLstIts
        DataSetName = 'frxDs005A_PrgLstIts'
        KeepTogether = True
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Question'#225'rio: [frxDs005A_PrgLstIts."NO_LST"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pergunta: [frxDs005A_PrgLstIts."Nome"]')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 842.835190000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 377.953000000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DModFin.frxDsSdoCtas
          DataSetName = 'frxDsSdoCtas'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        Height = 18.897650000000000000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        DataSet = frxDs005B_PrgLstIts
        DataSetName = 'frxDs005B_PrgLstIts'
        KeepTogether = True
        RowCount = 0
        object Memo17: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Resposta: [frxDs005B_PrgLstIts."Resposta"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 37.795300000000000000
        Top = 668.976810000000000000
        Width = 680.315400000000000000
        object Memo5: TfrxMemoView
          Top = 18.897650000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Localizador')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 139.842610000000000000
          Top = 18.897650000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Lugar')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 540.472790000000000000
          Top = 18.897650000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Status da OS')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 52.913420000000000000
          Top = 18.897650000000000000
          Width = 86.929133859999990000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Final da execu'#231#227'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 442.205010000000000000
          Top = 18.897650000000000000
          Width = 98.267780000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Fato gerador')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 627.401980000000000000
          Top = 18.897650000000000000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'ID Contrato')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ordens de servi'#231'o envolvidas')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        Height = 11.338590000000000000
        Top = 771.024120000000000000
        Width = 680.315400000000000000
      end
      object SubdetailData1: TfrxSubdetailData
        Height = 340.157480310000000000
        Top = 306.141930000000000000
        Width = 680.315400000000000000
        DataSet = frxDs005A_OSPipIts
        DataSetName = 'frxDs005A_OSPipIts'
        RowCount = 0
        object Chart005A: TfrxChartView
          Width = 680.315400000000000000
          Height = 340.157480310000000000
          OnBeforePrint = 'Chart1OnBeforePrint'
          ShowHint = False
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E50656E2E56
            697369626C6508124C6567656E642E466F6E742E48656967687402F8104C6567
            656E642E466F6E742E4E616D650617556E6976657273204C6967687420436F6E
            64656E736564124C6567656E642E4672616D652E436F6C6F720706636C477261
            79164C6567656E642E4672616D652E536D616C6C446F747309124C6567656E64
            2E4C6567656E645374796C6507086C73536572696573154C6567656E642E5368
            61646F772E56697369626C6508114C6567656E642E53686170655374796C6507
            11666F73526F756E6452656374616E676C651C4C6567656E642E53796D626F6C
            2E536861646F772E56697369626C65080B4178697356697369626C65080E4368
            617274334450657263656E7402010D4672616D652E56697369626C6508065669
            6577334408175669657733444F7074696F6E732E456C65766174696F6E033B01
            185669657733444F7074696F6E732E4F7274686F676F6E616C08195669657733
            444F7074696F6E732E50657273706563746976650200165669657733444F7074
            696F6E732E526F746174696F6E0368010B56696577334457616C6C7308145A6F
            6F6D2E4B656570417370656374526174696F090A426576656C4F757465720706
            62764E6F6E6505436F6C6F720707636C576869746511436F6C6F7250616C6574
            7465496E646578020D000A545069655365726965730E5049455F4C4153545F4D
            4F4E49540B4C6567656E645469746C65060C4C6567656E64205469746C65134D
            61726B732E4172726F772E56697369626C6509194D61726B732E43616C6C6F75
            742E42727573682E436F6C6F720707636C426C61636B1B4D61726B732E43616C
            6C6F75742E4172726F772E56697369626C6509114D61726B732E466F6E742E48
            656967687402F80F4D61726B732E466F6E742E4E616D650617556E6976657273
            204C6967687420436F6E64656E736564114D61726B732E4672616D652E436F6C
            6F720706636C47726179144D61726B732E536861646F772E56697369626C6508
            0D4D61726B732E56697369626C65090D5856616C7565732E4F72646572070B6C
            6F417363656E64696E670C5956616C7565732E4E616D6506035069650D595661
            6C7565732E4F7264657207066C6F4E6F6E6507436972636C6564090E53686164
            6F772E56697369626C6508064461726B3344081A4672616D652E496E6E657242
            727573682E4261636B436F6C6F720705636C526564224672616D652E496E6E65
            7242727573682E4772616469656E742E456E64436F6C6F720706636C47726179
            224672616D652E496E6E657242727573682E4772616469656E742E4D6964436F
            6C6F720707636C5768697465244672616D652E496E6E657242727573682E4772
            616469656E742E5374617274436F6C6F720440404000214672616D652E496E6E
            657242727573682E4772616469656E742E56697369626C65091B4672616D652E
            4D6964646C6542727573682E4261636B436F6C6F720708636C59656C6C6F7723
            4672616D652E4D6964646C6542727573682E4772616469656E742E456E64436F
            6C6F720482828200234672616D652E4D6964646C6542727573682E4772616469
            656E742E4D6964436F6C6F720707636C5768697465254672616D652E4D696464
            6C6542727573682E4772616469656E742E5374617274436F6C6F720706636C47
            726179224672616D652E4D6964646C6542727573682E4772616469656E742E56
            697369626C65091A4672616D652E4F7574657242727573682E4261636B436F6C
            6F720707636C477265656E224672616D652E4F7574657242727573682E477261
            6469656E742E456E64436F6C6F720440404000224672616D652E4F7574657242
            727573682E4772616469656E742E4D6964436F6C6F720707636C576869746524
            4672616D652E4F7574657242727573682E4772616469656E742E537461727443
            6F6C6F720708636C53696C766572214672616D652E4F7574657242727573682E
            4772616469656E742E56697369626C65090D4672616D652E56697369626C6508
            0B4672616D652E57696474680204194F74686572536C6963652E4C6567656E64
            2E56697369626C65080C50696550656E2E436F6C6F720707636C57686974650C
            50696550656E2E57696474680202000000}
          ChartElevation = 315
          SeriesData = <
            item
              DataType = dtDBData
              DataBand = frxReport005A.MD002
              DataSet = frxDs005A_OSPipIts
              DataSetName = 'frxDs005A_OSPipIts'
              SortOrder = soNone
              TopN = 20
              TopNCaption = 'Outros'
              XType = xtText
              Source1 = 'frxDs005A_OSPipIts."Resposta"'
              Source2 = 'frxDs005A_OSPipIts."ITENS"'
              XSource = 'frxDs005A_OSPipIts."Resposta"'
              YSource = 'frxDs005A_OSPipIts."ITENS"'
            end>
        end
      end
      object SubdetailData2: TfrxSubdetailData
        Height = 18.897650000000000000
        Top = 729.449290000000000000
        Width = 680.315400000000000000
        DataSet = frxDs005A_OSs
        DataSetName = 'frxDs005A_OSs'
        RowCount = 0
        object Memo4: TfrxMemoView
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs005A_OSs."Codigo"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 139.842610000000000000
          Width = 302.362365830000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NO_SiapTerCad'
          DataSet = frxDs005A_OSs
          DataSetName = 'frxDs005A_OSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs005A_OSs."NO_SiapTerCad"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 540.472790000000000000
          Width = 86.929155830000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NO_ESTATUS'
          DataSet = frxDs005A_OSs
          DataSetName = 'frxDs005A_OSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs005A_OSs."NO_ESTATUS"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 52.913420000000000000
          Width = 86.929133859999990000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'DtaExeFim'
          DataSet = frxDs005A_OSs
          DataSetName = 'frxDs005A_OSs'
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs005A_OSs."DtaExeFim"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 442.205010000000000000
          Width = 98.267745830000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NO_FatoGeradr'
          DataSet = frxDs005A_OSs
          DataSetName = 'frxDs005A_OSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs005A_OSs."NO_FatoGeradr"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 627.401980000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          ShowHint = False
          DataField = 'NumContrat'
          DataSet = frxDs005A_OSs
          DataSetName = 'frxDs005A_OSs'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDs005A_OSs."NumContrat"]')
          ParentFont = False
        end
      end
    end
  end
  object Qr005B_PrgLstIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = Qr005B_PrgLstItsAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT opi.PrgLstCab, opi.PrgLstIts, plc.Nome NO_LST,'
      'plp.Sigla, plp.Nome'
      'FROM ospipits opi'
      'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab'
      'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts'
      'LEFT JOIN prgcadprg plp ON plp.Codigo=pli.Pergunta'
      'WHERE opi.Codigo=879'
      'AND pli.Aplicacao & 1'
      'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome')
    Left = 52
    Top = 152
    object Qr005B_PrgLstItsPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object Qr005B_PrgLstItsPrgLstIts: TIntegerField
      FieldName = 'PrgLstIts'
    end
    object Qr005B_PrgLstItsNO_LST: TWideStringField
      FieldName = 'NO_LST'
      Size = 100
    end
    object Qr005B_PrgLstItsSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 100
    end
    object Qr005B_PrgLstItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Qr005B_PrgLstItsFuncoes: TIntegerField
      FieldName = 'Funcoes'
    end
    object Qr005B_PrgLstItsCodigo: TFloatField
      FieldName = 'Codigo'
    end
    object Qr005B_PrgLstItsRespCod: TFloatField
      FieldName = 'RespCod'
    end
    object Qr005B_PrgLstItsResposta: TWideStringField
      FieldName = 'Resposta'
      Size = 255
    end
    object Qr005B_PrgLstItsRespondido: TIntegerField
      FieldName = 'Respondido'
    end
  end
  object Qr005A_OSPipIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(opi.RespAtrIts) ITENS, '
      'opi.Respondido, opi.RespAtrCad, opi.RespAtrIts,'
      'IF(opi.Respondido=0, "N'#195'O RESPONDIDO", '
      'IF(opi.RespAtrIts > 0, pat.Nome, pac.Padrao)) Resposta,'
      'IF(opi.Respondido=0, 8421504/*cinza*/, '
      'IF(opi.RespAtrIts > 0, pat.CorPie, pac.CorPie)) + 0.000 CorPizza'
      'FROM ospipits opi'
      'LEFT JOIN prgatrcad pac ON pac.Codigo=opi.PrgLstCab'
      'LEFT JOIN prgatrits pat ON pat.Controle=opi.RespAtrIts'
      'WHERE opi.Codigo=879'
      'AND opi.PrgLstIts=2'
      'GROUP BY opi.Respondido, opi.RespAtrCad, opi.RespAtrIts'
      'ORDER BY ITENS DESC')
    Left = 172
    Top = 252
    object Qr005A_OSPipItsITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object Qr005A_OSPipItsRespondido: TSmallintField
      FieldName = 'Respondido'
      Origin = 'ospipits.Respondido'
    end
    object Qr005A_OSPipItsRespAtrCad: TIntegerField
      FieldName = 'RespAtrCad'
      Origin = 'ospipits.RespAtrCad'
    end
    object Qr005A_OSPipItsRespAtrIts: TIntegerField
      FieldName = 'RespAtrIts'
      Origin = 'ospipits.RespAtrIts'
    end
    object Qr005A_OSPipItsResposta: TWideStringField
      FieldName = 'Resposta'
      Size = 50
    end
    object Qr005A_OSPipItsCorPizza: TFloatField
      FieldName = 'CorPizza'
    end
    object Qr005A_OSPipItsDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
  end
  object Qr005A_OSs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cab.Codigo, cab.Entidade, cab.SiapTerCad, cab.Estatus, '
      ' cab.DtaExeFim, cab.NumContrat, cab.Grupo,'
      'fge.Nome NO_FatoGeradr,'
      'sta.Nome NO_ESTATUS, stc.Nome NO_SiapTerCad,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT'
      'FROM oscab cab'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr'
      'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus'
      'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad'
      'WHERE cab.Codigo=0')
    Left = 52
    Top = 248
    object Qr005A_OSsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr005A_OSsEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object Qr005A_OSsSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object Qr005A_OSsEstatus: TIntegerField
      FieldName = 'Estatus'
    end
    object Qr005A_OSsDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object Qr005A_OSsNumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object Qr005A_OSsGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object Qr005A_OSsNO_FatoGeradr: TWideStringField
      FieldName = 'NO_FatoGeradr'
    end
    object Qr005A_OSsNO_ESTATUS: TWideStringField
      FieldName = 'NO_ESTATUS'
      Size = 60
    end
    object Qr005A_OSsNO_SiapTerCad: TWideStringField
      FieldName = 'NO_SiapTerCad'
      Size = 100
    end
    object Qr005A_OSsNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object frxDs005A_OSs: TfrxDBDataset
    UserName = 'frxDs005A_OSs'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Entidade=Entidade'
      'SiapTerCad=SiapTerCad'
      'Estatus=Estatus'
      'DtaExeFim=DtaExeFim'
      'NumContrat=NumContrat'
      'Grupo=Grupo'
      'NO_FatoGeradr=NO_FatoGeradr'
      'NO_ESTATUS=NO_ESTATUS'
      'NO_SiapTerCad=NO_SiapTerCad'
      'NO_ENT=NO_ENT')
    DataSet = Qr005A_OSs
    BCDToCurrency = False
    Left = 52
    Top = 296
  end
  object frxDs005B_PrgLstIts: TfrxDBDataset
    UserName = 'frxDs005B_PrgLstIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'PrgLstCab=PrgLstCab'
      'PrgLstIts=PrgLstIts'
      'NO_LST=NO_LST'
      'Sigla=Sigla'
      'Nome=Nome'
      'Funcoes=Funcoes'
      'Codigo=Codigo'
      'RespCod=RespCod'
      'Resposta=Resposta')
    DataSet = Qr005B_PrgLstIts
    BCDToCurrency = False
    Left = 52
    Top = 200
  end
  object Qr005A_PrgLstIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = Qr005A_PrgLstItsAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT opi.PrgLstCab, opi.PrgLstIts, plc.Nome NO_LST,'
      'plp.Sigla, plp.Nome'
      'FROM ospipits opi'
      'LEFT JOIN prglstcab plc ON plc.Codigo=opi.PrgLstCab'
      'LEFT JOIN prglstits pli ON pli.Controle=opi.PrgLstIts'
      'LEFT JOIN prgcadprg plp ON plp.Codigo=pli.Pergunta'
      'WHERE opi.Codigo=879'
      'AND pli.Aplicacao & 1'
      'ORDER BY plc.Ordem, plc.Nome, pli.Ordem, plp.Nome')
    Left = 52
    Top = 56
    object Qr005A_PrgLstItsPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object Qr005A_PrgLstItsPrgLstIts: TIntegerField
      FieldName = 'PrgLstIts'
    end
    object Qr005A_PrgLstItsNO_LST: TWideStringField
      FieldName = 'NO_LST'
      Size = 100
    end
    object Qr005A_PrgLstItsSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 100
    end
    object Qr005A_PrgLstItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object Qr005A_PrgLstItsFuncoes: TIntegerField
      FieldName = 'Funcoes'
    end
    object Qr005A_PrgLstItsCodigo: TFloatField
      FieldName = 'Codigo'
    end
  end
  object frxDs005A_PrgLstIts: TfrxDBDataset
    UserName = 'frxDs005A_PrgLstIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'PrgLstCab=PrgLstCab'
      'PrgLstIts=PrgLstIts'
      'NO_LST=NO_LST'
      'Sigla=Sigla'
      'Nome=Nome'
      'Funcoes=Funcoes'
      'Codigo=Codigo')
    DataSet = Qr005A_PrgLstIts
    BCDToCurrency = False
    Left = 52
    Top = 104
  end
  object frxDs005A_OSPipIts: TfrxDBDataset
    UserName = 'frxDs005A_OSPipIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'ITENS=ITENS'
      'Respondido=Respondido'
      'RespAtrCad=RespAtrCad'
      'RespAtrIts=RespAtrIts'
      'Resposta=Resposta'
      'CorPizza=CorPizza'
      'DtaExeFim=DtaExeFim')
    DataSet = Qr005A_OSPipIts
    BCDToCurrency = False
    Left = 172
    Top = 300
  end
end
