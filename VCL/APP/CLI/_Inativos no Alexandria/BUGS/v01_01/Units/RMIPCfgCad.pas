unit RMIPCfgCad;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnDmkProcFunc, UnDmkENums, UnProjGroup_Consts, dmkDBGridZTO, Variants;

type
  TFmRMIPCfgCad = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrRMIPCfgCad: TmySQLQuery;
    DsRMIPCfgCad: TDataSource;
    QrRMIPCfgIts: TmySQLQuery;
    DsRMIPCfgIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrRMIPCfgItsNO_REL: TWideStringField;
    QrRMIPCfgItsCodigo: TIntegerField;
    QrRMIPCfgItsControle: TIntegerField;
    QrRMIPCfgItsItemRel: TIntegerField;
    QrRMIPCfgItsLk: TIntegerField;
    QrRMIPCfgItsDataCad: TDateField;
    QrRMIPCfgItsDataAlt: TDateField;
    QrRMIPCfgItsUserCad: TIntegerField;
    QrRMIPCfgItsUserAlt: TIntegerField;
    QrRMIPCfgItsAlterWeb: TSmallintField;
    QrRMIPCfgItsAtivo: TSmallintField;
    QrRMIPCfgCadCodigo: TIntegerField;
    QrRMIPCfgCadNome: TWideStringField;
    QrRMIPCfgCadEmpresa: TIntegerField;
    QrRMIPCfgCadCliente: TIntegerField;
    QrRMIPCfgItsOrdem: TIntegerField;
    QrRMIPCfgCadLk: TIntegerField;
    QrRMIPCfgCadDataCad: TDateField;
    QrRMIPCfgCadDataAlt: TDateField;
    QrRMIPCfgCadUserCad: TIntegerField;
    QrRMIPCfgCadUserAlt: TIntegerField;
    QrRMIPCfgCadAlterWeb: TSmallintField;
    QrRMIPCfgCadAtivo: TSmallintField;
    QrRMIPCfgItsAplicacao: TIntegerField;
    QrRMIPCfgItsNO_APLICACAO: TWideStringField;
    QrRMIPCfgItsNome: TWideStringField;
    BtFxa: TBitBtn;
    PMFxa: TPopupMenu;
    IncluiFaixa1: TMenuItem;
    AlteraFaixa1: TMenuItem;
    ExcluiFaixa1: TMenuItem;
    QrRMIPCfgFxa: TmySQLQuery;
    DsRMIPCfgFxa: TDataSource;
    QrRMIPCfgFxaCodigo: TIntegerField;
    QrRMIPCfgFxaControle: TIntegerField;
    QrRMIPCfgFxaConta: TIntegerField;
    QrRMIPCfgFxaQtdMax: TFloatField;
    dmkDBGridZTO1: TdmkDBGridZTO;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrRMIPCfgCadAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrRMIPCfgCadBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrRMIPCfgCadAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure QrRMIPCfgCadBeforeClose(DataSet: TDataSet);
    procedure QrRMIPCfgItsBeforeClose(DataSet: TDataSet);
    procedure QrRMIPCfgItsAfterScroll(DataSet: TDataSet);
    procedure IncluiFaixa1Click(Sender: TObject);
    procedure AlteraFaixa1Click(Sender: TObject);
    procedure ExcluiFaixa1Click(Sender: TObject);
    procedure PMFxaPopup(Sender: TObject);
    procedure BtFxaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure InsAltFxa(SQLType: TSQLType);
    procedure MostraRMIPCfgIts(SQLType: TSQLType);
    procedure ReopenRMIPCfgFxa(Conta: Integer);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenRMIPCfgIts(Controle: Integer);

  end;

var
  FmRMIPCfgCad: TFmRMIPCfgCad;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, RMIPCfgIts, GetValor;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmRMIPCfgCad.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmRMIPCfgCad.MostraRMIPCfgIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmRMIPCfgIts, FmRMIPCfgIts, afmoNegarComAviso) then
  begin
    FmRMIPCfgIts.ImgTipo.SQLType := SQLType;
    FmRMIPCfgIts.FQrCab := QrRMIPCfgCad;
    FmRMIPCfgIts.FDsCab := DsRMIPCfgCad;
    FmRMIPCfgIts.FQrIts := QrRMIPCfgIts;
    if SQLType = stIns then
      FmRMIPCfgIts.EdOrdem.ValueVariant := 999
    else
    begin
      FmRMIPCfgIts.EdControle.ValueVariant := QrRMIPCfgItsControle.Value;
      //
      FmRMIPCfgIts.RGItemRel.ItemIndex     := QrRMIPCfgItsItemRel.Value;
      FmRMIPCfgIts.RGAplicacao.ItemIndex   := QrRMIPCfgItsAplicacao.Value;
      FmRMIPCfgIts.EdOrdem.ValueVariant    := QrRMIPCfgItsOrdem.Value;
      FmRMIPCfgIts.EdNome.ValueVariant     := QrRMIPCfgItsNome.Value;
    end;
    FmRMIPCfgIts.ShowModal;
    FmRMIPCfgIts.Destroy;
  end;
end;

procedure TFmRMIPCfgCad.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrRMIPCfgCad);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrRMIPCfgCad, QrRMIPCfgIts);
end;

procedure TFmRMIPCfgCad.PMFxaPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiFaixa1, QrRMIPCfgIts);
  MyObjects.HabilitaMenuItemItsUpd(AlteraFaixa1, QrRMIPCfgFxa);
  MyObjects.HabilitaMenuItemItsDel(ExcluiFaixa1, QrRMIPCfgFxa);
end;

procedure TFmRMIPCfgCad.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrRMIPCfgCad);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrRMIPCfgIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrRMIPCfgIts);
end;

procedure TFmRMIPCfgCad.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrRMIPCfgCadCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmRMIPCfgCad.DefParams;
begin
  VAR_GOTOTABELA := 'rmipcfgcad';
  VAR_GOTOMYSQLTABLE := QrRMIPCfgCad;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM rmipcfgcad');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmRMIPCfgCad.ExcluiFaixa1Click(Sender: TObject);
var
  Conta: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o da faixa selecionada?',
  'rmipcfgfxa', 'Conta', QrRMIPCfgFxaConta.Value, Dmod.MyDB) = ID_YES then
  begin
    Conta := GOTOy.LocalizaPriorNextIntQr(QrRMIPCfgFxa,
      QrRMIPCfgFxaConta, QrRMIPCfgFxaConta.Value);
    ReopenRMIPCfgFxa(Conta);
  end;
end;

procedure TFmRMIPCfgCad.IncluiFaixa1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Informa��es v�lidas apenas para perguntas quantitativas!' + sLineBreak +
  'Informa��o n�o obrigat�ria!' + sLineBreak +
  'Deseja continuar assim mesmo?') = ID_YES then
    InsAltFxa(stIns);
end;

procedure TFmRMIPCfgCad.InsAltFxa(SQLType: TSQLType);
var
  Cod: Variant;
var
  Codigo, Controle, Conta: Integer;
  QtdMax: Double;
begin
  Codigo         := QrRMIPCfgItsCodigo.Value;
  Controle       := QrRMIPCfgItsControle.Value;
  if SQLType = stUpd then
  begin
    Conta        := QrRMIPCfgFxaConta.Value;
    QtdMax       := QrRMIPCfgFxaQtdMax.Value;
  end else
  begin
    Conta        := 0;
    QtdMax       := 0;
  end;
  //
(*
function TUnMyObjects.GetValorDmk(ComponentClass: TComponentClass;
  Reference: TComponent; FormatType: TAllFormat; Default: Variant; Casas,
  LeftZeros: Integer; ValMin, ValMax: String; Obrigatorio: Boolean; FormCaption,
  ValCaption: String; WidthVal: Integer; var Resultado: Variant): Boolean;
*)
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble, QtdMax, 3, 0,
  '', '', True, 'Faixa de quantidade', 'Quantidade m�xima da faixa: ', 0, Cod) then
  begin
    QtdMax  := Cod;

(*
    if UMyMod.VerificaDuplicadoInt(Dmod.MyDB, 'rmipcfgfxa', 'Codigo', 'Controle',
    'QtdMax', Codigo, Controle, QtdMax) then
      Exit;
*)
    if UMyMod.VerificaDuplicado1((*Database: TmySQLDatabase;*) Dmod.MyDB,
    (*Tabela: String;*)'rmipcfgfxa', (*CamposFind: array of String;*)
    ['Controle', 'QtdMax'],  (*ValoresFind: array of Variant;*)
    [Controle, QtdMax], (*CampoOmit: String; ValorOmit: Variant)*) '', Null) then
      Exit;

    Conta := UMyMod.BPGS1I32('rmipcfgfxa', 'Conta', '', '', tsPos, SQLType, Conta);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'rmipcfgfxa', False, [
    'Codigo', 'Controle', 'QtdMax'], [
    'Conta'], [
    Codigo, Controle, QtdMax], [
    Conta], True) then
      ReopenRMIPCfgFxa(Conta);
  end;
end;

procedure TFmRMIPCfgCad.ItsAltera1Click(Sender: TObject);
begin
  MostraRMIPCfgIts(stUpd);
end;

procedure TFmRMIPCfgCad.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Info('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmRMIPCfgCad.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmRMIPCfgCad.QueryPrincipalAfterOpen;
begin
end;

procedure TFmRMIPCfgCad.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'RMIPCfgIts', 'Controle', QrRMIPCfgItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrRMIPCfgIts,
      QrRMIPCfgItsControle, QrRMIPCfgItsControle.Value);
    ReopenRMIPCfgIts(Controle);
  end;
end;

procedure TFmRMIPCfgCad.ReopenRMIPCfgFxa(Conta: Integer);
begin

  EXIT;

  UnDmkDAC_PF.AbreMySQLQuery0(QrRMIPCfgFxa, Dmod.MyDB, [
  'SELECT * FROM rmipcfgfxa',
  'WHERE Controle=' + Geral.FF0(QrRMIPCfgItsControle.Value),
  'ORDER BY QtdMax ',
  '']);
  //
  QrRMIPCfgFxa.Locate('Conta', Conta, []);
end;

procedure TFmRMIPCfgCad.ReopenRMIPCfgIts(Controle: Integer);
var
  NomeRel, NomeAplicacao: String;
begin
  NomeRel := dmkPF.ArrayToTexto('its.ItemRel', 'NO_REL', pvPos, True,
    sListaItensRMIP);
  NomeAplicacao := dmkPF.ArrayToTexto('its.Aplicacao', 'NO_APLICACAO', pvNo,
    True,  sListaAplicacaoRMIPCfgIts);
  UnDmkDAC_PF.AbreMySQLQuery0(QrRMIPCfgIts, Dmod.MyDB, [
  'SELECT its.*, ',
  NomeRel,
  NomeAplicacao,
  'FROM rmipcfgits its ',
  'WHERE Codigo=' + Geral.FF0(QrRMIPCfgCadCodigo.Value),
  'ORDER BY Ordem, Controle ',
  '']);
  //
  QrRMIPCfgIts.Locate('Controle', Controle, []);
end;

procedure TFmRMIPCfgCad.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmRMIPCfgCad.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmRMIPCfgCad.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmRMIPCfgCad.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmRMIPCfgCad.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmRMIPCfgCad.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRMIPCfgCad.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrRMIPCfgCadCodigo.Value;
  Close;
end;

procedure TFmRMIPCfgCad.ItsInclui1Click(Sender: TObject);
begin
  MostraRMIPCfgIts(stIns);
end;

procedure TFmRMIPCfgCad.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrRMIPCfgCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'rmipcfgcad');
end;

procedure TFmRMIPCfgCad.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
  Codigo, Empresa, Cliente: Integer;
begin
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.ValueVariant;
  //  Ver no futuro se precisa!
  Empresa        := 0; // EdEmpresa.ValueVariant;
  Cliente        := 0; // EdCliente.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('rmipcfgcad', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrRMIPCfgCadCodigo.Value);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'rmipcfgcad', False, [
  'Nome', 'Empresa', 'Cliente'], [
  'Codigo'], [
  Nome, Empresa, Cliente], [
  Codigo], True) then
  begin
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    GOTOy.BotoesSb(ImgTipo.SQLType);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmRMIPCfgCad.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'rmipcfgcad', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'rmipcfgcad', 'Codigo');
end;

procedure TFmRMIPCfgCad.BtFxaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFxa, BtFxa);
end;

procedure TFmRMIPCfgCad.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmRMIPCfgCad.AlteraFaixa1Click(Sender: TObject);
begin
  InsAltFxa(stUpd);
end;

procedure TFmRMIPCfgCad.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmRMIPCfgCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmRMIPCfgCad.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrRMIPCfgCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmRMIPCfgCad.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmRMIPCfgCad.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrRMIPCfgCadCodigo.Value, LaRegistro.Caption);
end;

procedure TFmRMIPCfgCad.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmRMIPCfgCad.QrRMIPCfgCadAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmRMIPCfgCad.QrRMIPCfgCadAfterScroll(DataSet: TDataSet);
begin
  ReopenRMIPCfgIts(0);
end;

procedure TFmRMIPCfgCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrRMIPCfgCadCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmRMIPCfgCad.SbQueryClick(Sender: TObject);
begin
  LocCod(QrRMIPCfgCadCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'rmipcfgcad', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmRMIPCfgCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmRMIPCfgCad.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrRMIPCfgCad, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'rmipcfgcad');
end;

procedure TFmRMIPCfgCad.QrRMIPCfgCadBeforeClose(
  DataSet: TDataSet);
begin
  QrRMIPCfgIts.Close;
end;

procedure TFmRMIPCfgCad.QrRMIPCfgCadBeforeOpen(DataSet: TDataSet);
begin
  QrRMIPCfgCadCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmRMIPCfgCad.QrRMIPCfgItsAfterScroll(DataSet: TDataSet);
begin
  //BtFxa.Enabled := QrRMIPCfgItsItemRel.Value = CO_PRG_LST_QUANTIT;
  ReopenRMIPCfgFxa(0);
end;

procedure TFmRMIPCfgCad.QrRMIPCfgItsBeforeClose(DataSet: TDataSet);
begin
  //BtFxa.Enabled := False;
  QrRMIPCfgFxa.Close;
end;

end.

