unit Dependenci;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, dmkEditCB, dmkDBLookupComboBox, UnDmkEnums;

type
  TFmDependenci = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrDependenci: TmySQLQuery;
    QrDependenciCodigo: TIntegerField;
    QrDependenciNome: TWideStringField;
    DsDependenci: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    CBDependType: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    EdDependType: TdmkEditCB;
    Label3: TLabel;
    QrDependtype: TmySQLQuery;
    DsDependtype: TDataSource;
    QrDependenciNO_DEP_TYPE: TWideStringField;
    QrDependenciDependType: TIntegerField;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    CkContinuar: TCheckBox;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrDependenciAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrDependenciBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmDependenci: TFmDependenci;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, Principal;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmDependenci.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmDependenci.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrDependenciCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmDependenci.DefParams;
begin
  VAR_GOTOTABELA := 'dependenci';
  VAR_GOTOMYSQLTABLE := QrDependenci;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT det.Nome NO_DEP_TYPE, ded.*');
  VAR_SQLx.Add('FROM dependenci ded');
  VAR_SQLx.Add('LEFT JOIN dependtype det ON det.Codigo=ded.DependType');
  VAR_SQLx.Add('WHERE ded.Codigo > 0');
  //
  VAR_SQL1.Add('AND ded.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND ded.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND ded.Nome LIKE :P0');
  //
end;

procedure TFmDependenci.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmDependenci.QueryPrincipalAfterOpen;
begin
end;

procedure TFmDependenci.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmDependenci.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmDependenci.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmDependenci.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmDependenci.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmDependenci.SpeedButton5Click(Sender: TObject);
begin
  FmPrincipal.MostraFormDependType();
  UMyMod.SetaCodigoPesquisado(EdDependType, CBDependType, QrDependType, VAR_CADASTRO);
end;

procedure TFmDependenci.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDependenci.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrDependenci, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'dependenci');
end;

procedure TFmDependenci.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrDependenciCodigo.Value;
  Close;
end;

procedure TFmDependenci.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('dependenci', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrDependenciCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'dependenci', Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if CkContinuar.Checked then
      BtIncluiClick(Self);
  end;
end;

procedure TFmDependenci.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'dependenci', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmDependenci.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrDependenci, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'dependenci');
end;

procedure TFmDependenci.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
  UMyMod.AbreQuery(QrDependtype, DMod.MyDB);
end;

procedure TFmDependenci.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrDependenciCodigo.Value, LaRegistro.Caption);
end;

procedure TFmDependenci.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmDependenci.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrDependenciCodigo.Value, LaRegistro.Caption);
end;

procedure TFmDependenci.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmDependenci.QrDependenciAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmDependenci.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmDependenci.SbQueryClick(Sender: TObject);
begin
  LocCod(QrDependenciCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'dependenci', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmDependenci.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDependenci.QrDependenciBeforeOpen(DataSet: TDataSet);
begin
  QrDependenciCodigo.DisplayFormat := FFormatFloat;
end;

end.

