object FmLugarPesq: TFmLugarPesq
  Left = 339
  Top = 185
  Caption = 'CAD-SUBCL-013 :: Pesquisa de Lugar'
  ClientHeight = 629
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitWidth = 812
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      ExplicitLeft = 764
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      ExplicitWidth = 716
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 226
        Height = 32
        Caption = 'Pesquisa de Lugar'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 226
        Height = 32
        Caption = 'Pesquisa de Lugar'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 226
        Height = 32
        Caption = 'Pesquisa de Lugar'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitWidth = 812
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 812
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 784
        Height = 467
        Align = alClient
        TabOrder = 0
        ExplicitWidth = 812
        object Panel8: TPanel
          Left = 2
          Top = 15
          Width = 780
          Height = 82
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label12: TLabel
            Left = 8
            Top = 0
            Width = 28
            Height = 13
            Caption = 'CEP*:'
          end
          object Label31: TLabel
            Left = 84
            Top = 0
            Width = 99
            Height = 13
            Caption = 'Nome do logradouro:'
            FocusControl = EdSRua
          end
          object Label32: TLabel
            Left = 296
            Top = 0
            Width = 40
            Height = 13
            Caption = 'N'#250'mero:'
            FocusControl = EdSNumero
          end
          object Label34: TLabel
            Left = 440
            Top = 0
            Width = 30
            Height = 13
            Caption = 'Bairro:'
            FocusControl = EdSBairro
          end
          object Label35: TLabel
            Left = 608
            Top = 0
            Width = 36
            Height = 13
            Caption = 'Cidade:'
            FocusControl = EdSCidade
          end
          object Label38: TLabel
            Left = 152
            Top = 40
            Width = 17
            Height = 13
            Caption = 'UF:'
          end
          object Label15: TLabel
            Left = 184
            Top = 40
            Width = 25
            Height = 13
            Caption = 'Pa'#237's:'
          end
          object Label17: TLabel
            Left = 8
            Top = 40
            Width = 55
            Height = 13
            Caption = 'Refer'#234'ncia:'
            FocusControl = EdSEndeRef
          end
          object Label112: TLabel
            Left = 276
            Top = 40
            Width = 62
            Height = 13
            Caption = 'Telefone 1**:'
          end
          object Label106: TLabel
            Left = 396
            Top = 40
            Width = 36
            Height = 13
            Caption = 'Cidade:'
          end
          object Label109: TLabel
            Left = 596
            Top = 40
            Width = 25
            Height = 13
            Caption = 'Pa'#237's:'
          end
          object Label1: TLabel
            Left = 368
            Top = 0
            Width = 66
            Height = 13
            Caption = 'Margem n'#186' +-:'
            FocusControl = EdMargemNum
          end
          object EdSCEP: TdmkEdit
            Left = 8
            Top = 16
            Width = 72
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'SCEP'
            UpdCampo = 'SCEP'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdSRua: TdmkEdit
            Left = 84
            Top = 16
            Width = 209
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SRua'
            UpdCampo = 'SRua'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSNumero: TdmkEdit
            Left = 296
            Top = 16
            Width = 68
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'SNumero'
            UpdCampo = 'SNumero'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
          end
          object EdSBairro: TdmkEdit
            Left = 440
            Top = 16
            Width = 165
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SBairro'
            UpdCampo = 'SBairro'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSCidade: TdmkEdit
            Left = 608
            Top = 16
            Width = 165
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SCidade'
            UpdCampo = 'SCidade'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSUF: TdmkEdit
            Left = 152
            Top = 56
            Width = 28
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtUF
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SUF'
            UpdCampo = 'SUF'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSPais: TdmkEdit
            Left = 184
            Top = 56
            Width = 89
            Height = 21
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SPais'
            UpdCampo = 'SPais'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSEndeRef: TdmkEdit
            Left = 8
            Top = 56
            Width = 141
            Height = 21
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SEndeRef'
            UpdCampo = 'SEndeRef'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSTe1: TdmkEdit
            Left = 276
            Top = 56
            Width = 112
            Height = 21
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtTelLongo
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'STe1'
            UpdCampo = 'STe1'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSCodMunici: TdmkEditCB
            Left = 392
            Top = 56
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'SCodMunici'
            UpdCampo = 'SCodMunici'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBSCodMunici
            IgnoraDBLookupComboBox = False
          end
          object CBSCodMunici: TdmkDBLookupComboBox
            Left = 452
            Top = 56
            Width = 140
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsMunici
            TabOrder = 10
            dmkEditCB = EdSCodMunici
            QryCampo = 'SCodMunici'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdSCodiPais: TdmkEditCB
            Left = 596
            Top = 56
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'SCodiPais'
            UpdCampo = 'SCodiPais'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBSCodiPais
            IgnoraDBLookupComboBox = False
          end
          object CBSCodiPais: TdmkDBLookupComboBox
            Left = 652
            Top = 56
            Width = 120
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsBacen_Pais
            TabOrder = 12
            dmkEditCB = EdSCodiPais
            QryCampo = 'SCodiPais'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdMargemNum: TdmkEdit
            Left = 368
            Top = 16
            Width = 68
            Height = 21
            Alignment = taRightJustify
            TabOrder = 13
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '50'
            QryCampo = 'SNumero'
            UpdCampo = 'SNumero'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 50
            ValWarn = False
          end
        end
        object dmkDBGridZTO1: TdmkDBGridZTO
          Left = 2
          Top = 97
          Width = 780
          Height = 368
          Align = alClient
          DataSource = DsPesq
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          RowColors = <>
          OnDblClick = dmkDBGridZTO1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Lugar'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o do lugar'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cliente'
              Width = 48
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_ENT'
              Title.Caption = 'Nome do cliente'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SRua'
              Title.Caption = 'Rua'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SNumero'
              Title.Caption = 'N'#250'mero'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SBairro'
              Title.Caption = 'Bairro'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SCidade'
              Title.Caption = 'Cidade'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SUF'
              Title.Caption = 'UF'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SCEP'
              Title.Caption = 'CEP'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STe1'
              Title.Caption = 'Telefone'
              Width = 112
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 784
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitWidth = 812
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 808
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 784
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitWidth = 812
    object PnSaiDesis: TPanel
      Left = 638
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 666
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 636
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitWidth = 664
      object Label2: TLabel
        Left = 144
        Top = 8
        Width = 111
        Height = 13
        Caption = 'CEP*:  de 1 a 8 d'#237'gitos.'
        FocusControl = EdSRua
      end
      object Label3: TLabel
        Left = 144
        Top = 24
        Width = 312
        Height = 13
        Caption = 
          'Telefone 1**:  Ser'#227'o considerados os '#250'ltimos n'#250'meros. (ou use %)' +
          '.'
        FocusControl = EdSRua
      end
      object BtOK: TBitBtn
        Tag = 18
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisar'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrMunici: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 476
    Top = 380
    object QrMuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMunici: TDataSource
    DataSet = QrMunici
    Left = 476
    Top = 428
  end
  object DsBacen_Pais: TDataSource
    DataSet = QrBacen_Pais
    Left = 408
    Top = 428
  end
  object QrBacen_Pais: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bacen_pais'
      'ORDER BY Nome'
      '')
    Left = 408
    Top = 380
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    SQL.Strings = (
      'SELECT stc.Codigo, stc.Nome, stc.Cliente,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'stc.SRua, stc.SNumero, stc.SBairro,'
      'stc.SCidade, stc.SUF, stc.SCEP, stc.STe1'
      'FROM siaptercad stc'
      'LEFT JOIN entidades ent ON ent.Codigo=stc.Cliente'
      'WHERE stc.Codigo <> 0'
      'AND stc.SRua LIKE "%paulo%"'
      'AND stc.SNumero=120'
      'AND stc.SBairro LIKE "%1%"'
      'AND stc.SCidade LIKE "%Maringa%"'
      'AND stc.SUF LIKE "PR%"'
      'AND (stc.SCEP BETWEEN 87013000 AND 87013999)'
      'AND RIGHT(stc.STe1, 2) = "44"')
    Left = 228
    Top = 268
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrPesqCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPesqNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrPesqSRua: TWideStringField
      FieldName = 'SRua'
      Size = 60
    end
    object QrPesqSNumero: TIntegerField
      FieldName = 'SNumero'
    end
    object QrPesqSBairro: TWideStringField
      FieldName = 'SBairro'
      Size = 60
    end
    object QrPesqSCidade: TWideStringField
      FieldName = 'SCidade'
      Size = 60
    end
    object QrPesqSUF: TWideStringField
      FieldName = 'SUF'
      Size = 3
    end
    object QrPesqSCEP: TIntegerField
      FieldName = 'SCEP'
    end
    object QrPesqSTe1: TWideStringField
      FieldName = 'STe1'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 228
    Top = 316
  end
end
