object FmTaxonNet: TFmTaxonNet
  Left = 339
  Top = 185
  Caption = '???-?????-999 :: ?????'
  ClientHeight = 492
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 512
        Height = 32
        Caption = 'Informa'#231#245'es de Esp'#233'cies para Importa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 512
        Height = 32
        Caption = 'Informa'#231#245'es de Esp'#233'cies para Importa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 512
        Height = 32
        Caption = 'Informa'#231#245'es de Esp'#233'cies para Importa'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 330
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 330
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 125
        Align = alTop
        Caption = ' Filtros de pesquisa: '
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 82
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 251
            Height = 82
            Align = alLeft
            TabOrder = 0
            object RGFilFilo: TRadioGroup
              Left = 1
              Top = 1
              Width = 69
              Height = 80
              Align = alLeft
              Caption = ' Filo: '
              ItemIndex = 0
              Items.Strings = (
                'Todos '
                'Pracial'
                'Igual a')
              TabOrder = 0
              OnClick = RGFilFiloClick
            end
            object EdFilFilo: TdmkEditCB
              Left = 74
              Top = 32
              Width = 172
              Height = 21
              Enabled = False
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdFilFiloChange
              IgnoraDBLookupComboBox = False
            end
            object CBFilFilo: TdmkDBLookupComboBox
              Left = 74
              Top = 56
              Width = 172
              Height = 21
              Enabled = False
              KeyField = 'Filo'
              ListField = 'Filo'
              ListSource = DsFilo
              TabOrder = 2
              OnClick = CBFilFiloClick
              OnKeyDown = CBFilFiloKeyDown
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
          object Panel7: TPanel
            Left = 251
            Top = 0
            Width = 251
            Height = 82
            Align = alLeft
            TabOrder = 1
            object RgFilClasse: TRadioGroup
              Left = 1
              Top = 1
              Width = 69
              Height = 80
              Align = alLeft
              Caption = ' Classe: '
              ItemIndex = 0
              Items.Strings = (
                'Todos '
                'Pracial'
                'Igual a')
              TabOrder = 0
              OnClick = RgFilClasseClick
            end
            object EdFilClasse: TdmkEditCB
              Left = 74
              Top = 32
              Width = 172
              Height = 21
              Enabled = False
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdFilClasseChange
              IgnoraDBLookupComboBox = False
            end
            object CBFilClasse: TdmkDBLookupComboBox
              Left = 74
              Top = 56
              Width = 172
              Height = 21
              Enabled = False
              KeyField = 'Classe'
              ListField = 'Classe'
              ListSource = DsClasse
              TabOrder = 2
              OnClick = CBFilClasseClick
              OnKeyDown = CBFilClasseKeyDown
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
          object Panel8: TPanel
            Left = 502
            Top = 0
            Width = 251
            Height = 82
            Align = alLeft
            TabOrder = 2
            object RGFilOrdem: TRadioGroup
              Left = 1
              Top = 1
              Width = 69
              Height = 80
              Align = alLeft
              Caption = ' Ordem: '
              ItemIndex = 0
              Items.Strings = (
                'Todos '
                'Pracial'
                'Igual a')
              TabOrder = 0
              OnClick = RGFilOrdemClick
            end
            object EdFilOrdem: TdmkEditCB
              Left = 74
              Top = 32
              Width = 172
              Height = 21
              Enabled = False
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdFilOrdemChange
              IgnoraDBLookupComboBox = False
            end
            object CBFilOrdem: TdmkDBLookupComboBox
              Left = 74
              Top = 56
              Width = 172
              Height = 21
              Enabled = False
              KeyField = 'Ordem'
              ListField = 'Ordem'
              ListSource = DsOrdem
              TabOrder = 2
              OnClick = CBFilOrdemClick
              OnKeyDown = CBFilOrdemKeyDown
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
          object Panel9: TPanel
            Left = 753
            Top = 0
            Width = 251
            Height = 82
            Align = alClient
            TabOrder = 3
            object RGFilFamilia: TRadioGroup
              Left = 1
              Top = 1
              Width = 69
              Height = 80
              Align = alLeft
              Caption = ' Fam'#237'lia: '
              ItemIndex = 0
              Items.Strings = (
                'Todos '
                'Pracial'
                'Igual a')
              TabOrder = 0
              OnClick = RGFilFamiliaClick
            end
            object EdFilFamilia: TdmkEditCB
              Left = 74
              Top = 32
              Width = 172
              Height = 21
              Enabled = False
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdFilFamiliaChange
              IgnoraDBLookupComboBox = False
            end
            object CBFilFamilia: TdmkDBLookupComboBox
              Left = 74
              Top = 56
              Width = 172
              Height = 21
              Enabled = False
              KeyField = 'Familia'
              ListField = 'Familia'
              ListSource = DsFamilia
              TabOrder = 2
              OnClick = CBFilFamiliaClick
              OnKeyDown = CBFilFamiliaKeyDown
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
          end
        end
        object Panel10: TPanel
          Left = 2
          Top = 97
          Width = 1004
          Height = 26
          Align = alClient
          TabOrder = 1
          object Label2: TLabel
            Left = 8
            Top = 8
            Width = 41
            Height = 13
            Caption = 'Esp'#233'cie:'
          end
          object EdEspecie: TdmkEdit
            Left = 60
            Top = 4
            Width = 241
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdEspecieChange
          end
        end
      end
      object DBGrid1: TDBGrid
        Left = 0
        Top = 125
        Width = 1008
        Height = 205
        Align = alClient
        DataSource = DsTaxonNet
        TabOrder = 1
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnDblClick = DBGrid1DblClick
        Columns = <
          item
            Expanded = False
            FieldName = 'Fonte'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Filo'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Classe'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Ordem'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Familia'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Especie'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Popular1'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Popular2'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Popular3'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Popular4'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Popular5'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ArqDir'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ArqImg'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Copyright'
            Width = 120
            Visible = True
          end>
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 378
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 422
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 480
        Top = 20
        Width = 105
        Height = 13
        Caption = 'Registros localizados: '
      end
      object LaRegLoc: TLabel
        Left = 588
        Top = 21
        Width = 22
        Height = 13
        Caption = '000'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object BtOK: TBitBtn
        Tag = 20
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Pesquisa'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 144
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Seleciona'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtConfirmaClick
      end
      object BtVerFoto: TBitBtn
        Tag = 227
        Left = 272
        Top = 4
        Width = 120
        Height = 40
        Caption = 'Ver &Foto'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtVerFotoClick
      end
    end
  end
  object QrFilo: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrFiloAfterOpen
    BeforeClose = QrFiloBeforeClose
    SQL.Strings = (
      'SELECT DISTINCT Filo'
      'FROM taxonnet'
      'ORDER BY Filo')
    Left = 124
    Top = 68
  end
  object DsFilo: TDataSource
    DataSet = QrFilo
    Left = 152
    Top = 68
  end
  object QrTaxonNet: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrTaxonNetAfterOpen
    BeforeClose = QrTaxonNetBeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM taxonnet'
      'WHERE Codigo > -999999999'
      ''
      'ORDER BY Filo')
    Left = 324
    Top = 244
    object QrTaxonNetFonte: TWideStringField
      FieldName = 'Fonte'
      Size = 30
    end
    object QrTaxonNetCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTaxonNetEspecie: TWideStringField
      FieldName = 'Especie'
      Size = 60
    end
    object QrTaxonNetPopular1: TWideStringField
      FieldName = 'Popular1'
      Size = 60
    end
    object QrTaxonNetPopular2: TWideStringField
      FieldName = 'Popular2'
      Size = 60
    end
    object QrTaxonNetPopular3: TWideStringField
      FieldName = 'Popular3'
      Size = 60
    end
    object QrTaxonNetPopular4: TWideStringField
      FieldName = 'Popular4'
      Size = 60
    end
    object QrTaxonNetPopular5: TWideStringField
      FieldName = 'Popular5'
      Size = 60
    end
    object QrTaxonNetOrigem: TWideStringField
      FieldName = 'Origem'
      Size = 60
    end
    object QrTaxonNetFilo: TWideStringField
      FieldName = 'Filo'
      Size = 60
    end
    object QrTaxonNetClasse: TWideStringField
      FieldName = 'Classe'
      Size = 60
    end
    object QrTaxonNetOrdem: TWideStringField
      FieldName = 'Ordem'
      Size = 60
    end
    object QrTaxonNetFamilia: TWideStringField
      FieldName = 'Familia'
      Size = 60
    end
    object QrTaxonNetCopyright: TWideStringField
      FieldName = 'Copyright'
      Size = 60
    end
    object QrTaxonNetObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrTaxonNetArqDir: TWideStringField
      FieldName = 'ArqDir'
      Size = 60
    end
    object QrTaxonNetArqImg: TWideStringField
      FieldName = 'ArqImg'
      Size = 60
    end
    object QrTaxonNetArqTxt: TWideStringField
      FieldName = 'ArqTxt'
      Size = 255
    end
    object QrTaxonNetLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTaxonNetDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTaxonNetDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTaxonNetUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTaxonNetUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTaxonNetAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTaxonNetAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsTaxonNet: TDataSource
    DataSet = QrTaxonNet
    Left = 352
    Top = 244
  end
  object QrClasse: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Classe'
      'FROM taxonnet'
      'ORDER BY Classe')
    Left = 404
    Top = 68
  end
  object DsClasse: TDataSource
    DataSet = QrClasse
    Left = 432
    Top = 68
  end
  object QrOrdem: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Ordem'
      'FROM taxonnet'
      'ORDER BY Ordem')
    Left = 664
    Top = 64
  end
  object DsOrdem: TDataSource
    DataSet = QrOrdem
    Left = 692
    Top = 64
  end
  object DsFamilia: TDataSource
    DataSet = QrFamilia
    Left = 916
    Top = 64
  end
  object QrFamilia: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Familia'
      'FROM taxonnet'
      'ORDER BY Familia')
    Left = 888
    Top = 64
  end
end
