unit OSCxI;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, dmkRadioGroup, UnBugs_Tabs, dmkMemo, UnDmkEnums;

type
  TFmOSCxI = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    QrOSCab: TmySQLQuery;
    QrOSCabCodigo: TIntegerField;
    QrOSCabEntidade: TIntegerField;
    QrOSCabEstatus: TIntegerField;
    QrOSCabFatoGeradr: TIntegerField;
    QrOSCabDtaContat: TDateTimeField;
    QrOSCabDtaVisPrv: TDateTimeField;
    QrOSCabDtaVisExe: TDateTimeField;
    QrOSCabDtaExePrv: TDateTimeField;
    QrOSCabDtaExeIni: TDateTimeField;
    QrOSCabDtaExeFim: TDateTimeField;
    QrOSCabLk: TIntegerField;
    QrOSCabDataCad: TDateField;
    QrOSCabDataAlt: TDateField;
    QrOSCabUserCad: TIntegerField;
    QrOSCabUserAlt: TIntegerField;
    QrOSCabAlterWeb: TSmallintField;
    QrOSCabAtivo: TSmallintField;
    QrOSCabNO_FatoGeradr: TWideStringField;
    QrOSCabNO_ESTATUS: TWideStringField;
    QrOSCabNO_ENT: TWideStringField;
    QrOSCabSiapTerCad: TIntegerField;
    QrOSCabNO_SiapTerCad: TWideStringField;
    QrOSCabTXTVisPrv: TWideStringField;
    QrOSCabTXTContat: TWideStringField;
    QrOSCabTXTVisExe: TWideStringField;
    QrOSCabTXTExePrv: TWideStringField;
    QrOSCabTXTExeIni: TWideStringField;
    QrOSCabTXTExeFim: TWideStringField;
    DsOSCab: TDataSource;
    GBDados: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit15: TDBEdit;
    Label2: TLabel;
    EdControle: TdmkEdit;
    Label1: TLabel;
    CkContinuar: TCheckBox;
    SbCaixa: TSpeedButton;
    MeDetalhes: TdmkMemo;
    Label6: TLabel;
    EdFotoCxa: TdmkEdit;
    CGAplicacao: TdmkCheckGroup;
    RGImprimir: TdmkRadioGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbCaixaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FQrInsUpd: TmySQLQuery;
    FCriou: Boolean;
    //
  end;

  var
  FmOSCxI: TFmOSCxI;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, Principal, MyDBCheck,
  CunsCad;

{$R *.DFM}

procedure TFmOSCxI.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Aplicacao, Imprimir: Integer;
  FotoCxa, Detalhes: String;
begin
  Codigo     := QrOSCabCodigo.Value;
  Controle   := EdControle.ValueVariant;
  FotoCxa    := EdFotoCxa.ValueVariant;
  Detalhes   := MeDetalhes.Text;
  Aplicacao  := CGAplicacao.Value;
  Imprimir   := RGImprimir.ItemIndex;
  //
  if MyObjects.FIC(Trim(FotoCxa) = '', EdFotoCxa, 'Informe a foto!') then
    Exit;
  if MyObjects.FIC(Aplicacao = 0, CGAplicacao, 'Informe a aplica��o!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('oscxi', 'Controle', '', '',
    tsDef, ImgTipo.SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'oscxi', False, [
  'Codigo', 'FotoCxa', 'Detalhes',
  'Aplicacao', 'Imprimir'], [
  'Controle'], [
  Codigo, FotoCxa, Detalhes,
  Aplicacao, Imprimir], [
  Controle], True) then
  begin
    if FQrInsUpd <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(FQrInsUpd, Dmod.MyDB);
      FQrInsUpd.Locate('Controle', Controle, []);
    end;
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType := stIns;
      EdControle.ValueVariant := 0;
      EdFotoCxa.ValueVariant := '';
      MeDetalhes.Text := '';
      EdFotoCxa.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmOSCxI.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSCxI.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSCxI.FormCreate(Sender: TObject);
begin
  FCriou := False;
end;

procedure TFmOSCxI.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSCxI.SbCaixaClick(Sender: TObject);
var
  Host, SDir, RDir, DirDest: String;
  Empresa: Integer;
begin
  Host := Dmod.QrOpcoesBugsCxaIPv4.Value;
  SDir := Dmod.QrOpcoesBugsCxaSDir.Value;
  Empresa := 0;
  RDir := '';
  //
  if MyObjects.TentaDefinirDiretorio(
  Host, SDir, Empresa, RDir, LaAviso1, LaAviso2, False, nil, DirDest) then
  begin
    MyObjects.DefineArquivo2(Self, EdFotoCxa, DirDest, EdFotoCxa.Text, ppInfo_Ini, True);
    if Pos(DirDest, EdFotoCxa.Text) = 1 then
    begin
      EdFotoCxa.Text := Copy(EdFotoCxa.Text, Length(DirDest) + 1);
    end;
  end;
end;

(*
object LaOSOrigem: TLabel
  Left = 468
  Top = 60
  Width = 54
  Height = 13
  Caption = 'OS Origem:'
  Color = clBtnFace
  Enabled = False
  ParentColor = False
end
object DBEdit9: TDBEdit
  Left = 524
  Top = 56
  Width = 56
  Height = 21
  TabStop = False
  DataField = 'OSOrigem'
  DataSource = DsOSCab
  TabOrder = 9
end

*)

end.

