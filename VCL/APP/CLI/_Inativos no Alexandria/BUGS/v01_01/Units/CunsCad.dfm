﻿object FmCunsCad: TFmCunsCad
  Left = 368
  Top = 194
  Caption = 'CAD-SUBCL-001 :: Cadastro de Clientes'
  ClientHeight = 960
  ClientWidth = 1765
  Color = clBtnFace
  Constraints.MinHeight = 340
  Constraints.MinWidth = 837
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 17
  object PnEdita: TPanel
    Left = 0
    Top = 68
    Width = 1765
    Height = 892
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1765
      Height = 305
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 21
        Top = 21
        Width = 48
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 99
        Top = 21
        Width = 129
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Raz'#227'o Social / Nome:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label24: TLabel
        Left = 21
        Top = 78
        Width = 113
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Atividade principal:'
      end
      object Label25: TLabel
        Left = 21
        Top = 131
        Width = 81
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Respons'#225'vel:'
      end
      object Label26: TLabel
        Left = 21
        Top = 183
        Width = 307
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Como conheceu a empresa (forma de marketing):'
      end
      object Label27: TLabel
        Left = 785
        Top = 183
        Width = 86
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data contato:'
      end
      object SpeedButton5: TSpeedButton
        Left = 900
        Top = 99
        Width = 27
        Height = 28
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object SpeedButton6: TSpeedButton
        Left = 900
        Top = 152
        Width = 27
        Height = 27
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton6Click
      end
      object SpeedButton7: TSpeedButton
        Left = 748
        Top = 204
        Width = 27
        Height = 27
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton7Click
      end
      object EdAtivPrinc: TdmkEditCB
        Left = 21
        Top = 99
        Width = 73
        Height = 28
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'AtivPrinc'
        UpdCampo = 'AtivPrinc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBAtivPrinc
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBAtivPrinc: TdmkDBLookupComboBox
        Left = 94
        Top = 99
        Width = 802
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsAtividades
        TabOrder = 1
        dmkEditCB = EdAtivPrinc
        QryCampo = 'AtivPrinc'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdAccount: TdmkEditCB
        Left = 21
        Top = 152
        Width = 73
        Height = 27
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Account'
        UpdCampo = 'Account'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBAccount
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBAccount: TdmkDBLookupComboBox
        Left = 94
        Top = 152
        Width = 802
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsAccounts
        TabOrder = 3
        dmkEditCB = EdAccount
        QryCampo = 'Account'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdHowFind: TdmkEditCB
        Left = 21
        Top = 204
        Width = 73
        Height = 27
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'HowFind'
        UpdCampo = 'HowFind'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBHowFind
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBHowFind: TdmkDBLookupComboBox
        Left = 94
        Top = 204
        Width = 650
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsHowFounded
        TabOrder = 5
        dmkEditCB = EdHowFind
        QryCampo = 'HowFind'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPDataCon: TdmkEditDateTimePicker
        Left = 785
        Top = 204
        Width = 146
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 0.487372523137310100
        Time = 0.487372523137310100
        TabOrder = 6
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DataCon'
        UpdCampo = 'DataCon'
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 21
        Top = 42
        Width = 73
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsEntidades
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 7
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utIdx
        Alignment = taRightJustify
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 99
        Top = 42
        Width = 828
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'NOMEENTIDADE'
        DataSource = DsEntidades
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object CkImpImaInfo: TdmkCheckBox
        Left = 21
        Top = 235
        Width = 493
        Height = 23
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'Imprime dados informativos secund'#225'rios dos locais na ordem de ex' +
          'ecu'#231#227'o.'
        Checked = True
        State = cbChecked
        TabOrder = 9
        QryCampo = 'ImpImaInfo'
        UpdCampo = 'ImpImaInfo'
        UpdType = utYes
        ValCheck = #0
        ValUncheck = #0
        OldValor = #0
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 809
      Width = 1765
      Height = 83
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 16
        Top = 22
        Width = 117
        Height = 53
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 1622
        Top = 19
        Width = 141
        Height = 62
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 3
          Width = 118
          Height = 52
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 68
    Width = 1765
    Height = 892
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    object GBCntrl: TGroupBox
      Left = 0
      Top = 808
      Width = 1765
      Height = 84
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 19
        Width = 225
        Height = 63
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 167
          Top = 5
          Width = 53
          Height = 53
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 115
          Top = 5
          Width = 52
          Height = 53
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 63
          Top = 5
          Width = 52
          Height = 53
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 53
          Height = 53
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 227
        Top = 19
        Width = 40
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 860
        Top = 19
        Width = 903
        Height = 63
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 729
          Top = 0
          Width = 174
          Height = 63
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 157
            Height = 53
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object Panel31: TPanel
          Left = 0
          Top = 0
          Width = 230
          Height = 63
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 1
          object BtCunsCad: TBitBtn
            Tag = 122
            Left = 8
            Top = 4
            Width = 52
            Height = 52
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtCunsCadClick
          end
          object BtCunsIts: TBitBtn
            Left = 60
            Top = 4
            Width = 52
            Height = 52
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Con'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 1
            Visible = False
            OnClick = BtCunsItsClick
          end
          object BtSiapTerCad: TBitBtn
            Left = 115
            Top = 4
            Width = 52
            Height = 52
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Ter'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 2
            Visible = False
            OnClick = BtSiapTerCadClick
          end
          object BtSiapImaCad: TBitBtn
            Left = 170
            Top = 4
            Width = 52
            Height = 52
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Loc'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 3
            Visible = False
            OnClick = BtSiapImaCadClick
          end
        end
        object Panel32: TPanel
          Left = 230
          Top = 0
          Width = 499
          Height = 63
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object BtSiapImaDep: TBitBtn
            Left = 5
            Top = 4
            Width = 53
            Height = 52
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Dep'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 0
            Visible = False
            OnClick = BtSiapImaDepClick
          end
          object BtSiapImaCdi: TBitBtn
            Left = 60
            Top = 4
            Width = 52
            Height = 52
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Car'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 1
            Visible = False
            OnClick = BtSiapImaCdiClick
          end
          object BtSiapImaCav: TBitBtn
            Left = 115
            Top = 4
            Width = 52
            Height = 52
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Vic'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 2
            Visible = False
            OnClick = BtSiapImaCavClick
          end
          object BtSiapImaRes: TBitBtn
            Left = 170
            Top = 4
            Width = 52
            Height = 52
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Res'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 3
            Visible = False
            OnClick = BtSiapImaResClick
          end
          object BtAtrSICxDef: TBitBtn
            Left = 390
            Top = 4
            Width = 52
            Height = 52
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ACx'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 4
            Visible = False
            OnClick = BtAtrSICxDefClick
          end
          object BtSiapImaCxa: TBitBtn
            Left = 335
            Top = 4
            Width = 52
            Height = 52
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cxa'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 5
            Visible = False
            OnClick = BtSiapImaCxaClick
          end
          object BtSiapImaAti: TBitBtn
            Left = 280
            Top = 4
            Width = 52
            Height = 52
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Ati'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 6
            Visible = False
            OnClick = BtSiapImaAtiClick
          end
          object BtSiapImaCui: TBitBtn
            Left = 225
            Top = 4
            Width = 52
            Height = 52
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cui'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 7
            Visible = False
            OnClick = BtSiapImaCuiClick
          end
          object BtAtrSICdDef: TBitBtn
            Left = 445
            Top = 4
            Width = 52
            Height = 52
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Atr'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 8
            Visible = False
            OnClick = BtAtrSICdDefClick
          end
        end
      end
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1765
      Height = 38
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 5
        Top = 10
        Width = 45
        Height = 17
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente:'
        FocusControl = DBEdCodigo
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 73
        Top = 5
        Width = 73
        Height = 25
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCunsCad
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 146
        Top = 5
        Width = 807
        Height = 25
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'NOMEENTIDADE'
        DataSource = DsCunsCad
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object PCDados: TPageControl
      Left = 0
      Top = 38
      Width = 1765
      Height = 617
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet1
      Align = alTop
      TabOrder = 2
      OnChange = PCDadosChange
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Terrenos e seus locais de aplica'#231#227'o'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnTerrenos: TPanel
          Left = 0
          Top = 0
          Width = 1757
          Height = 585
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel7: TPanel
            Left = 418
            Top = 0
            Width = 1339
            Height = 585
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object PCObjeto: TPageControl
              Left = 0
              Top = 0
              Width = 1339
              Height = 585
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              ActivePage = TabSheet3
              Align = alClient
              TabOrder = 0
              object TabSheet3: TTabSheet
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Im'#243'vel'
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel9: TPanel
                  Left = 0
                  Top = 0
                  Width = 1331
                  Height = 553
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Panel29: TPanel
                    Left = 0
                    Top = 0
                    Width = 1331
                    Height = 81
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 0
                    object GBTerCadEnder: TGroupBox
                      Left = 0
                      Top = 0
                      Width = 889
                      Height = 81
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      Caption = ' Dados do im'#243'vel selecionado: '
                      TabOrder = 0
                      object MeTerCadEnder: TMemo
                        Left = 2
                        Top = 19
                        Width = 885
                        Height = 60
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        ReadOnly = True
                        ScrollBars = ssVertical
                        TabOrder = 0
                      end
                    end
                    object GroupBox8: TGroupBox
                      Left = 889
                      Top = 0
                      Width = 442
                      Height = 81
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alRight
                      Caption = ' '#193'rea em m'#178': '
                      TabOrder = 1
                      object Label14: TLabel
                        Left = 5
                        Top = 21
                        Width = 71
                        Height = 17
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Constru'#237'da:'
                        FocusControl = DBEdit34
                      end
                      object Label17: TLabel
                        Left = 195
                        Top = 21
                        Width = 76
                        Height = 17
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'N'#227'o constr.:'
                        FocusControl = DBEdit37
                      end
                      object Label18: TLabel
                        Left = 5
                        Top = 48
                        Width = 53
                        Height = 17
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Terreno:'
                        FocusControl = DBEdit38
                      end
                      object Label19: TLabel
                        Left = 195
                        Top = 48
                        Width = 35
                        Height = 17
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Total:'
                        FocusControl = DBEdit39
                      end
                      object DBEdit34: TDBEdit
                        Left = 85
                        Top = 18
                        Width = 105
                        Height = 25
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        DataField = 'M2Constru'
                        DataSource = DsSiapTerCad
                        TabOrder = 0
                      end
                      object DBEdit37: TDBEdit
                        Left = 276
                        Top = 18
                        Width = 105
                        Height = 25
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        DataField = 'M2NaoBuild'
                        DataSource = DsSiapTerCad
                        TabOrder = 1
                      end
                      object DBEdit38: TDBEdit
                        Left = 85
                        Top = 45
                        Width = 105
                        Height = 25
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        DataField = 'M2Terreno'
                        DataSource = DsSiapTerCad
                        TabOrder = 2
                      end
                      object DBEdit39: TDBEdit
                        Left = 276
                        Top = 45
                        Width = 105
                        Height = 25
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        DataField = 'M2Total'
                        DataSource = DsSiapTerCad
                        TabOrder = 3
                      end
                    end
                  end
                  object PCSiapTerCad: TPageControl
                    Left = 0
                    Top = 81
                    Width = 1331
                    Height = 472
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    ActivePage = TabSheet25
                    Align = alClient
                    TabOrder = 1
                    object TabSheet25: TTabSheet
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Locais de aplica'#231#227'o do lugar selecionado'
                      ExplicitLeft = 0
                      ExplicitTop = 0
                      ExplicitWidth = 0
                      ExplicitHeight = 0
                      object Panel38: TPanel
                        Left = 0
                        Top = 0
                        Width = 1323
                        Height = 440
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 0
                        object Panel17: TPanel
                          Left = 0
                          Top = 0
                          Width = 1323
                          Height = 100
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alTop
                          BevelOuter = bvNone
                          TabOrder = 0
                          object DBGSiapImaCad: TDBGrid
                            Left = 0
                            Top = 0
                            Width = 1323
                            Height = 100
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Align = alClient
                            DataSource = DsSiapImaCad
                            PopupMenu = PMSiapImaCad
                            TabOrder = 0
                            TitleFont.Charset = DEFAULT_CHARSET
                            TitleFont.Color = clWindowText
                            TitleFont.Height = -14
                            TitleFont.Name = 'Tahoma'
                            TitleFont.Style = []
                            OnMouseUp = DBGSiapImaCadMouseUp
                            Columns = <
                              item
                                Expanded = False
                                FieldName = 'Codigo'
                                Title.Caption = 'C'#243'digo'
                                Width = 40
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'SCompl2'
                                Title.Caption = 'Complemento do endere'#231'o'
                                Width = 234
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_OBJETO'
                                Title.Caption = 'Objeto'
                                Width = 150
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_FINALID'
                                Title.Caption = 'Finalidade'
                                Width = 150
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'NO_TPCONSTRU'
                                Title.Caption = 'Tipo constru'#231#227'o'
                                Width = 150
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'M2Constru'
                                Title.Caption = 'Constru'#237'da'
                                Width = 85
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'M2NaoBuild'
                                Title.Caption = 'N'#227'o constr.'
                                Width = 85
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'M2Terreno'
                                Title.Caption = 'Terreno'
                                Width = 85
                                Visible = True
                              end
                              item
                                Expanded = False
                                FieldName = 'M2Total'
                                Title.Caption = 'Total'
                                Width = 85
                                Visible = True
                              end>
                          end
                        end
                        object GroupBox6: TGroupBox
                          Left = 0
                          Top = 100
                          Width = 1323
                          Height = 340
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alClient
                          Caption = ' Dados do local de aplica'#231#227'o selecionado: '
                          TabOrder = 1
                          object PGDadosLocaisAplic: TPageControl
                            Left = 2
                            Top = 19
                            Width = 1319
                            Height = 319
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            ActivePage = TabSheet13
                            Align = alClient
                            TabOrder = 0
                            OnChange = PGDadosLocaisAplicChange
                            ExplicitLeft = 0
                            object TabSheet13: TTabSheet
                              Margins.Left = 4
                              Margins.Top = 4
                              Margins.Right = 4
                              Margins.Bottom = 4
                              Caption = ' Depend'#234'ncias '
                              ExplicitLeft = 0
                              ExplicitTop = 0
                              ExplicitWidth = 0
                              ExplicitHeight = 0
                              object Panel01: TPanel
                                Left = 0
                                Top = 0
                                Width = 1311
                                Height = 287
                                Margins.Left = 4
                                Margins.Top = 4
                                Margins.Right = 4
                                Margins.Bottom = 4
                                Align = alClient
                                BevelOuter = bvNone
                                ParentBackground = False
                                TabOrder = 0
                                object DBGSiapImaDep: TDBGrid
                                  Left = 0
                                  Top = 0
                                  Width = 1056
                                  Height = 287
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  Align = alClient
                                  DataSource = DsSiapImaDep
                                  PopupMenu = PMSiapImaDep
                                  TabOrder = 0
                                  TitleFont.Charset = DEFAULT_CHARSET
                                  TitleFont.Color = clWindowText
                                  TitleFont.Height = -14
                                  TitleFont.Name = 'Tahoma'
                                  TitleFont.Style = []
                                  OnMouseDown = DBGSiapImaDepMouseDown
                                  Columns = <
                                    item
                                      Expanded = False
                                      FieldName = 'Controle'
                                      Title.Caption = 'ID'
                                      Width = 40
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'NO_DEPENDENCI'
                                      Title.Caption = 'Descri'#231#227'o'
                                      Width = 234
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'MLarg'
                                      Title.Caption = 'Largura'
                                      Width = 72
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'MComp'
                                      Title.Caption = 'Comprimento'
                                      Width = 72
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'MAltu'
                                      Title.Caption = 'Altura'
                                      Width = 72
                                      Visible = True
                                    end>
                                end
                                object Panel34: TPanel
                                  Left = 1056
                                  Top = 0
                                  Width = 255
                                  Height = 287
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  Align = alRight
                                  BevelOuter = bvNone
                                  TabOrder = 1
                                  object Label29: TLabel
                                    Left = 5
                                    Top = 8
                                    Width = 88
                                    Height = 17
                                    Margins.Left = 4
                                    Margins.Top = 4
                                    Margins.Right = 4
                                    Margins.Bottom = 4
                                    Caption = 'Total '#225'rea m'#178':'
                                  end
                                  object Label30: TLabel
                                    Left = 5
                                    Top = 36
                                    Width = 108
                                    Height = 17
                                    Margins.Left = 4
                                    Margins.Top = 4
                                    Margins.Right = 4
                                    Margins.Bottom = 4
                                    Caption = '% do constru'#237'do:'
                                  end
                                  object Label31: TLabel
                                    Left = 5
                                    Top = 64
                                    Width = 107
                                    Height = 17
                                    Margins.Left = 4
                                    Margins.Top = 4
                                    Margins.Right = 4
                                    Margins.Bottom = 4
                                    Caption = 'Total volume m'#179':'
                                  end
                                  object DBEdit44: TDBEdit
                                    Left = 117
                                    Top = 5
                                    Width = 131
                                    Height = 25
                                    Margins.Left = 4
                                    Margins.Top = 4
                                    Margins.Right = 4
                                    Margins.Bottom = 4
                                    DataField = 'AREA'
                                    DataSource = DsAT_Dep
                                    TabOrder = 0
                                  end
                                  object DBEdit45: TDBEdit
                                    Left = 117
                                    Top = 33
                                    Width = 131
                                    Height = 25
                                    Margins.Left = 4
                                    Margins.Top = 4
                                    Margins.Right = 4
                                    Margins.Bottom = 4
                                    DataField = 'PERC_TOT'
                                    DataSource = DsAT_Dep
                                    TabOrder = 1
                                  end
                                  object DBEdit46: TDBEdit
                                    Left = 117
                                    Top = 61
                                    Width = 131
                                    Height = 25
                                    Margins.Left = 4
                                    Margins.Top = 4
                                    Margins.Right = 4
                                    Margins.Bottom = 4
                                    DataField = 'VOLUME'
                                    DataSource = DsAT_Dep
                                    TabOrder = 2
                                  end
                                end
                              end
                            end
                            object TabSheet14: TTabSheet
                              Margins.Left = 4
                              Margins.Top = 4
                              Margins.Right = 4
                              Margins.Bottom = 4
                              Caption = 'Caracter'#237'sticas '
                              ImageIndex = 1
                              ExplicitLeft = 0
                              ExplicitTop = 0
                              ExplicitWidth = 0
                              ExplicitHeight = 0
                              object Panel22: TPanel
                                Left = 0
                                Top = 0
                                Width = 1311
                                Height = 287
                                Margins.Left = 4
                                Margins.Top = 4
                                Margins.Right = 4
                                Margins.Bottom = 4
                                Align = alClient
                                BevelOuter = bvNone
                                ParentBackground = False
                                TabOrder = 0
                                object DBGSiapImaCdi: TDBGrid
                                  Left = 0
                                  Top = 0
                                  Width = 1311
                                  Height = 287
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  Align = alClient
                                  DataSource = DsSiapImaCdi
                                  TabOrder = 0
                                  TitleFont.Charset = DEFAULT_CHARSET
                                  TitleFont.Color = clWindowText
                                  TitleFont.Height = -14
                                  TitleFont.Name = 'Tahoma'
                                  TitleFont.Style = []
                                  OnMouseDown = DBGSiapImaCdiMouseDown
                                  Columns = <
                                    item
                                      Expanded = False
                                      FieldName = 'Controle'
                                      Title.Caption = 'ID'
                                      Width = 40
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'NO_CARACTERIS'
                                      Title.Caption = 'Descri'#231#227'o'
                                      Width = 582
                                      Visible = True
                                    end>
                                end
                              end
                            end
                            object TabSheet17: TTabSheet
                              Margins.Left = 4
                              Margins.Top = 4
                              Margins.Right = 4
                              Margins.Bottom = 4
                              Caption = #193'reas vicinais '
                              ImageIndex = 2
                              ExplicitLeft = 0
                              ExplicitTop = 0
                              ExplicitWidth = 0
                              ExplicitHeight = 0
                              object Panel23: TPanel
                                Left = 0
                                Top = 0
                                Width = 1311
                                Height = 287
                                Margins.Left = 4
                                Margins.Top = 4
                                Margins.Right = 4
                                Margins.Bottom = 4
                                Align = alClient
                                BevelOuter = bvNone
                                ParentBackground = False
                                TabOrder = 0
                                object DBGSiapImaCav: TDBGrid
                                  Left = 0
                                  Top = 0
                                  Width = 1311
                                  Height = 287
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  Align = alClient
                                  DataSource = DsSiapImaCav
                                  TabOrder = 0
                                  TitleFont.Charset = DEFAULT_CHARSET
                                  TitleFont.Color = clWindowText
                                  TitleFont.Height = -14
                                  TitleFont.Name = 'Tahoma'
                                  TitleFont.Style = []
                                  OnMouseDown = DBGSiapImaCavMouseDown
                                  Columns = <
                                    item
                                      Expanded = False
                                      FieldName = 'Controle'
                                      Title.Caption = 'ID'
                                      Width = 40
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'NO_CARACTERIS'
                                      Title.Caption = 'Descri'#231#227'o'
                                      Width = 582
                                      Visible = True
                                    end>
                                end
                              end
                            end
                            object TabSheet18: TTabSheet
                              Margins.Left = 4
                              Margins.Top = 4
                              Margins.Right = 4
                              Margins.Bottom = 4
                              Caption = 'Residentes '
                              ImageIndex = 3
                              ExplicitLeft = 0
                              ExplicitTop = 0
                              ExplicitWidth = 0
                              ExplicitHeight = 0
                              object Panel24: TPanel
                                Left = 0
                                Top = 0
                                Width = 1311
                                Height = 287
                                Margins.Left = 4
                                Margins.Top = 4
                                Margins.Right = 4
                                Margins.Bottom = 4
                                Align = alClient
                                BevelOuter = bvNone
                                ParentBackground = False
                                TabOrder = 0
                                object DBGrid7: TDBGrid
                                  Left = 0
                                  Top = 0
                                  Width = 1311
                                  Height = 287
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  Align = alClient
                                  DataSource = DsSiapImaRes
                                  TabOrder = 0
                                  TitleFont.Charset = DEFAULT_CHARSET
                                  TitleFont.Color = clWindowText
                                  TitleFont.Height = -14
                                  TitleFont.Name = 'Tahoma'
                                  TitleFont.Style = []
                                end
                                object DBGSiapImaRes: TDBGrid
                                  Left = 0
                                  Top = 0
                                  Width = 1311
                                  Height = 287
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  Align = alClient
                                  DataSource = DsSiapImaRes
                                  TabOrder = 1
                                  TitleFont.Charset = DEFAULT_CHARSET
                                  TitleFont.Color = clWindowText
                                  TitleFont.Height = -14
                                  TitleFont.Name = 'Tahoma'
                                  TitleFont.Style = []
                                  OnMouseDown = DBGSiapImaResMouseDown
                                  Columns = <
                                    item
                                      Expanded = False
                                      FieldName = 'Controle'
                                      Title.Caption = 'ID'
                                      Width = 40
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'NO_RESIDENTE'
                                      Title.Caption = 'Grau Parentesco'
                                      Width = 115
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'Nome'
                                      Width = 115
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'AnoNatal'
                                      Title.Caption = 'Natal'
                                      Width = 32
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'NO_CUIDADO'
                                      Title.Caption = 'Cuidado'
                                      Width = 115
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'Observacao'
                                      Title.Caption = 'Observa'#231#227'o'
                                      Visible = True
                                    end>
                                end
                              end
                            end
                            object TabSheet19: TTabSheet
                              Margins.Left = 4
                              Margins.Top = 4
                              Margins.Right = 4
                              Margins.Bottom = 4
                              Caption = 'Cuidados '
                              ImageIndex = 4
                              ExplicitLeft = 0
                              ExplicitTop = 0
                              ExplicitWidth = 0
                              ExplicitHeight = 0
                              object Panel25: TPanel
                                Left = 0
                                Top = 0
                                Width = 1311
                                Height = 287
                                Margins.Left = 4
                                Margins.Top = 4
                                Margins.Right = 4
                                Margins.Bottom = 4
                                Align = alClient
                                BevelOuter = bvNone
                                ParentBackground = False
                                TabOrder = 0
                                object DBGSiapImaCui: TDBGrid
                                  Left = 0
                                  Top = 0
                                  Width = 1311
                                  Height = 287
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  Align = alClient
                                  DataSource = DsSiapImaCui
                                  TabOrder = 0
                                  TitleFont.Charset = DEFAULT_CHARSET
                                  TitleFont.Color = clWindowText
                                  TitleFont.Height = -14
                                  TitleFont.Name = 'Tahoma'
                                  TitleFont.Style = []
                                  OnMouseDown = DBGSiapImaCuiMouseDown
                                  Columns = <
                                    item
                                      Expanded = False
                                      FieldName = 'Controle'
                                      Title.Caption = 'ID'
                                      Width = 40
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'NO_CUIDADO'
                                      Title.Caption = 'Descri'#231#227'o'
                                      Width = 586
                                      Visible = True
                                    end>
                                end
                              end
                            end
                            object TabSheet20: TTabSheet
                              Margins.Left = 4
                              Margins.Top = 4
                              Margins.Right = 4
                              Margins.Bottom = 4
                              Caption = 'Atividades'
                              ImageIndex = 5
                              ExplicitLeft = 0
                              ExplicitTop = 0
                              ExplicitWidth = 0
                              ExplicitHeight = 0
                              object Panel26: TPanel
                                Left = 0
                                Top = 0
                                Width = 1311
                                Height = 287
                                Margins.Left = 4
                                Margins.Top = 4
                                Margins.Right = 4
                                Margins.Bottom = 4
                                Align = alClient
                                BevelOuter = bvNone
                                ParentBackground = False
                                TabOrder = 0
                                object DBGSiapImaAti: TDBGrid
                                  Left = 0
                                  Top = 0
                                  Width = 1311
                                  Height = 287
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  Align = alClient
                                  DataSource = DsSiapImaAti
                                  TabOrder = 0
                                  TitleFont.Charset = DEFAULT_CHARSET
                                  TitleFont.Color = clWindowText
                                  TitleFont.Height = -14
                                  TitleFont.Name = 'Tahoma'
                                  TitleFont.Style = []
                                  OnMouseDown = DBGSiapImaAtiMouseDown
                                  Columns = <
                                    item
                                      Expanded = False
                                      FieldName = 'Controle'
                                      Title.Caption = 'ID'
                                      Width = 40
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'NO_ATIVIDADE'
                                      Title.Caption = 'Descri'#231#227'o'
                                      Width = 586
                                      Visible = True
                                    end>
                                end
                              end
                            end
                            object TabSheet21: TTabSheet
                              Margins.Left = 4
                              Margins.Top = 4
                              Margins.Right = 4
                              Margins.Bottom = 4
                              Caption = 'Caixas d`'#225'gua '
                              ImageIndex = 6
                              ExplicitLeft = 0
                              ExplicitTop = 0
                              ExplicitWidth = 0
                              ExplicitHeight = 0
                              object Panel27: TPanel
                                Left = 0
                                Top = 0
                                Width = 1311
                                Height = 287
                                Margins.Left = 4
                                Margins.Top = 4
                                Margins.Right = 4
                                Margins.Bottom = 4
                                Align = alClient
                                BevelOuter = bvNone
                                ParentBackground = False
                                TabOrder = 0
                                object DBGSiapImaCxa: TDBGrid
                                  Left = 0
                                  Top = 0
                                  Width = 320
                                  Height = 287
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  Align = alLeft
                                  DataSource = DsSiapImaCxa
                                  TabOrder = 0
                                  TitleFont.Charset = DEFAULT_CHARSET
                                  TitleFont.Color = clWindowText
                                  TitleFont.Height = -14
                                  TitleFont.Name = 'Tahoma'
                                  TitleFont.Style = []
                                  OnMouseDown = DBGSiapImaCxaMouseDown
                                  Columns = <
                                    item
                                      Expanded = False
                                      FieldName = 'Controle'
                                      Title.Caption = 'ID'
                                      Width = 40
                                      Visible = True
                                    end
                                    item
                                      Expanded = False
                                      FieldName = 'Local'
                                      Title.Caption = 'Descri'#231#227'o da caixa (local)'
                                      Width = 168
                                      Visible = True
                                    end>
                                end
                                object PGCaixaDAgua: TPageControl
                                  Left = 320
                                  Top = 0
                                  Width = 991
                                  Height = 287
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  ActivePage = TabSheet22
                                  Align = alClient
                                  TabOrder = 1
                                  object TabSheet22: TTabSheet
                                    Margins.Left = 4
                                    Margins.Top = 4
                                    Margins.Right = 4
                                    Margins.Bottom = 4
                                    Caption = ' Informa'#231#245'es da caixa d`'#225'gua selecionada '
                                    ExplicitLeft = 0
                                    ExplicitTop = 0
                                    ExplicitWidth = 0
                                    ExplicitHeight = 0
                                    object ScrollBox1: TScrollBox
                                      Left = 0
                                      Top = 0
                                      Width = 983
                                      Height = 255
                                      Align = alClient
                                      TabOrder = 0
                                      ExplicitLeft = 184
                                      ExplicitWidth = 799
                                      object Label13: TLabel
                                        Left = 272
                                        Top = 8
                                        Width = 50
                                        Height = 17
                                        Margins.Left = 4
                                        Margins.Top = 4
                                        Margins.Right = 4
                                        Margins.Bottom = 4
                                        Caption = 'Material:'
                                        FocusControl = DBEdit33
                                      end
                                      object Label12: TLabel
                                        Left = 5
                                        Top = 8
                                        Width = 44
                                        Height = 17
                                        Margins.Left = 4
                                        Margins.Top = 4
                                        Margins.Right = 4
                                        Margins.Bottom = 4
                                        Caption = 'Forma:'
                                        FocusControl = DBEdit32
                                      end
                                      object Label15: TLabel
                                        Left = 5
                                        Top = 60
                                        Width = 74
                                        Height = 17
                                        Margins.Left = 4
                                        Margins.Top = 4
                                        Margins.Right = 4
                                        Margins.Bottom = 4
                                        Caption = 'Vol. (Litros):'
                                        FocusControl = DBEdit35
                                      end
                                      object Label16: TLabel
                                        Left = 89
                                        Top = 60
                                        Width = 53
                                        Height = 17
                                        Margins.Left = 4
                                        Margins.Top = 4
                                        Margins.Right = 4
                                        Margins.Bottom = 4
                                        Caption = 'Medidas:'
                                        FocusControl = DBEdit36
                                      end
                                      object dmkLabelRotate1: TdmkLabelRotate
                                        Left = 5
                                        Top = 115
                                        Width = 27
                                        Height = 75
                                        Margins.Left = 4
                                        Margins.Top = 4
                                        Margins.Right = 4
                                        Margins.Bottom = 4
                                        Angle = ag90
                                        Caption = 'Acesso:'
                                        Font.Charset = ANSI_CHARSET
                                        Font.Color = clWindowText
                                        Font.Height = -17
                                        Font.Name = 'Arial'
                                        Font.Style = []
                                      end
                                      object DBEdit32: TDBEdit
                                        Left = 5
                                        Top = 29
                                        Width = 262
                                        Height = 25
                                        Margins.Left = 4
                                        Margins.Top = 4
                                        Margins.Right = 4
                                        Margins.Bottom = 4
                                        DataField = 'NOME_FORMA'
                                        DataSource = DsSiapImaCxa
                                        TabOrder = 0
                                      end
                                      object DBEdit33: TDBEdit
                                        Left = 272
                                        Top = 29
                                        Width = 262
                                        Height = 25
                                        Margins.Left = 4
                                        Margins.Top = 4
                                        Margins.Right = 4
                                        Margins.Bottom = 4
                                        DataField = 'NO_MATERIAL'
                                        DataSource = DsSiapImaCxa
                                        TabOrder = 1
                                      end
                                      object DBEdit35: TDBEdit
                                        Left = 5
                                        Top = 81
                                        Width = 79
                                        Height = 25
                                        Margins.Left = 4
                                        Margins.Top = 4
                                        Margins.Right = 4
                                        Margins.Bottom = 4
                                        DataField = 'VolumeL'
                                        DataSource = DsSiapImaCxa
                                        TabOrder = 2
                                      end
                                      object DBEdit36: TDBEdit
                                        Left = 89
                                        Top = 81
                                        Width = 445
                                        Height = 25
                                        Margins.Left = 4
                                        Margins.Top = 4
                                        Margins.Right = 4
                                        Margins.Bottom = 4
                                        DataField = 'Medidas'
                                        DataSource = DsSiapImaCxa
                                        TabOrder = 3
                                      end
                                      object DBMemo1: TDBMemo
                                        Left = 34
                                        Top = 110
                                        Width = 500
                                        Height = 82
                                        Margins.Left = 4
                                        Margins.Top = 4
                                        Margins.Right = 4
                                        Margins.Bottom = 4
                                        DataField = 'Acesso'
                                        DataSource = DsSiapImaCxa
                                        TabOrder = 4
                                      end
                                    end
                                  end
                                  object TabSheet23: TTabSheet
                                    Margins.Left = 4
                                    Margins.Top = 4
                                    Margins.Right = 4
                                    Margins.Bottom = 4
                                    Caption = ' Atributos da caixa d`'#225'gua '
                                    ImageIndex = 1
                                    ExplicitLeft = 0
                                    ExplicitTop = 0
                                    ExplicitWidth = 0
                                    ExplicitHeight = 0
                                    object DBGAtrSICxDef: TdmkDBGrid
                                      Left = 0
                                      Top = 0
                                      Width = 983
                                      Height = 255
                                      Margins.Left = 4
                                      Margins.Top = 4
                                      Margins.Right = 4
                                      Margins.Bottom = 4
                                      Align = alClient
                                      Columns = <
                                        item
                                          Expanded = False
                                          FieldName = 'CU_CAD'
                                          Title.Caption = 'C'#243'digo'
                                          Width = 44
                                          Visible = True
                                        end
                                        item
                                          Expanded = False
                                          FieldName = 'NO_CAD'
                                          Title.Caption = 'Atributo'
                                          Width = 112
                                          Visible = True
                                        end
                                        item
                                          Expanded = False
                                          FieldName = 'CU_ITS'
                                          Title.Caption = 'C'#243'digo'
                                          Width = 44
                                          Visible = True
                                        end
                                        item
                                          Expanded = False
                                          FieldName = 'NO_ITS'
                                          Title.Caption = 'Descri'#231#227'o do item do atributo'
                                          Width = 236
                                          Visible = True
                                        end
                                        item
                                          Expanded = False
                                          FieldName = 'ID_Item'
                                          Title.Caption = 'ID'
                                          Width = 44
                                          Visible = True
                                        end>
                                      Color = clWindow
                                      DataSource = DsAtrSICxDef
                                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                                      TabOrder = 0
                                      TitleFont.Charset = DEFAULT_CHARSET
                                      TitleFont.Color = clWindowText
                                      TitleFont.Height = -14
                                      TitleFont.Name = 'Tahoma'
                                      TitleFont.Style = []
                                      OnMouseDown = DBGAtrSICxDefMouseDown
                                      Columns = <
                                        item
                                          Expanded = False
                                          FieldName = 'CU_CAD'
                                          Title.Caption = 'C'#243'digo'
                                          Width = 44
                                          Visible = True
                                        end
                                        item
                                          Expanded = False
                                          FieldName = 'NO_CAD'
                                          Title.Caption = 'Atributo'
                                          Width = 112
                                          Visible = True
                                        end
                                        item
                                          Expanded = False
                                          FieldName = 'CU_ITS'
                                          Title.Caption = 'C'#243'digo'
                                          Width = 44
                                          Visible = True
                                        end
                                        item
                                          Expanded = False
                                          FieldName = 'NO_ITS'
                                          Title.Caption = 'Descri'#231#227'o do item do atributo'
                                          Width = 236
                                          Visible = True
                                        end
                                        item
                                          Expanded = False
                                          FieldName = 'ID_Item'
                                          Title.Caption = 'ID'
                                          Width = 44
                                          Visible = True
                                        end>
                                    end
                                  end
                                end
                              end
                            end
                            object TabSheet24: TTabSheet
                              Margins.Left = 4
                              Margins.Top = 4
                              Margins.Right = 4
                              Margins.Bottom = 4
                              Caption = 'Atributos '
                              ImageIndex = 7
                              ExplicitLeft = 0
                              ExplicitTop = 0
                              ExplicitWidth = 0
                              ExplicitHeight = 0
                              object DBGAtrSICdDef: TdmkDBGrid
                                Left = 0
                                Top = 0
                                Width = 1311
                                Height = 287
                                Margins.Left = 4
                                Margins.Top = 4
                                Margins.Right = 4
                                Margins.Bottom = 4
                                Align = alClient
                                Columns = <
                                  item
                                    Expanded = False
                                    FieldName = 'CU_CAD'
                                    Title.Caption = 'C'#243'digo'
                                    Width = 44
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NO_CAD'
                                    Title.Caption = 'Atributo'
                                    Width = 199
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'CU_ITS'
                                    Title.Caption = 'C'#243'digo'
                                    Width = 44
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NO_ITS'
                                    Title.Caption = 'Descri'#231#227'o do item do atributo'
                                    Width = 289
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'ID_Item'
                                    Title.Caption = 'ID'
                                    Width = 44
                                    Visible = True
                                  end>
                                Color = clWindow
                                DataSource = DsAtrSICdDef
                                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                                TabOrder = 0
                                TitleFont.Charset = DEFAULT_CHARSET
                                TitleFont.Color = clWindowText
                                TitleFont.Height = -14
                                TitleFont.Name = 'Tahoma'
                                TitleFont.Style = []
                                OnMouseDown = DBGAtrSICdDefMouseDown
                                Columns = <
                                  item
                                    Expanded = False
                                    FieldName = 'CU_CAD'
                                    Title.Caption = 'C'#243'digo'
                                    Width = 44
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NO_CAD'
                                    Title.Caption = 'Atributo'
                                    Width = 199
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'CU_ITS'
                                    Title.Caption = 'C'#243'digo'
                                    Width = 44
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NO_ITS'
                                    Title.Caption = 'Descri'#231#227'o do item do atributo'
                                    Width = 289
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'ID_Item'
                                    Title.Caption = 'ID'
                                    Width = 44
                                    Visible = True
                                  end>
                              end
                            end
                            object TabSheet40: TTabSheet
                              Margins.Left = 4
                              Margins.Top = 4
                              Margins.Right = 4
                              Margins.Bottom = 4
                              Caption = 'Croquis '
                              ImageIndex = 8
                              ExplicitLeft = 0
                              ExplicitTop = 0
                              ExplicitWidth = 0
                              ExplicitHeight = 0
                              object DBGSiapImaSVG: TDBGrid
                                Left = 63
                                Top = 0
                                Width = 1248
                                Height = 287
                                Margins.Left = 4
                                Margins.Top = 4
                                Margins.Right = 4
                                Margins.Bottom = 4
                                Align = alClient
                                DataSource = DsSiapImaSVG
                                PopupMenu = PMSiapImaSVG
                                TabOrder = 0
                                TitleFont.Charset = DEFAULT_CHARSET
                                TitleFont.Color = clWindowText
                                TitleFont.Height = -14
                                TitleFont.Name = 'Tahoma'
                                TitleFont.Style = []
                                OnDblClick = DBGSiapImaSVGDblClick
                                Columns = <
                                  item
                                    Expanded = False
                                    FieldName = 'Controle'
                                    Title.Caption = 'ID'
                                    Width = 40
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'Nome'
                                    Title.Caption = 'Descri'#231#227'o'
                                    Width = 300
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NO_RotarGraus'
                                    Title.Caption = 'Rota'#231#227'o'
                                    Visible = True
                                  end
                                  item
                                    Expanded = False
                                    FieldName = 'NoArq'
                                    Title.Caption = #218'ltimo arquivo carregado'
                                    Width = 612
                                    Visible = True
                                  end>
                              end
                              object Panel47: TPanel
                                Left = 0
                                Top = 0
                                Width = 63
                                Height = 287
                                Margins.Left = 4
                                Margins.Top = 4
                                Margins.Right = 4
                                Margins.Bottom = 4
                                Align = alLeft
                                ParentBackground = False
                                TabOrder = 1
                                object BtCroMenu: TBitBtn
                                  Tag = 237
                                  Left = 5
                                  Top = 5
                                  Width = 53
                                  Height = 53
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  NumGlyphs = 2
                                  TabOrder = 0
                                  OnClick = BtCroMenuClick
                                end
                                object BtCroVis: TBitBtn
                                  Tag = 10206
                                  Left = 5
                                  Top = 63
                                  Width = 53
                                  Height = 52
                                  Margins.Left = 4
                                  Margins.Top = 4
                                  Margins.Right = 4
                                  Margins.Bottom = 4
                                  NumGlyphs = 2
                                  TabOrder = 1
                                  OnClick = BtCroVisClick
                                end
                              end
                            end
                          end
                        end
                      end
                    end
                    object TabSheet27: TTabSheet
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Atributos do lugar'
                      ImageIndex = 1
                      ExplicitLeft = 0
                      ExplicitTop = 0
                      ExplicitWidth = 0
                      ExplicitHeight = 0
                      object DBGAtrSTCdDef: TdmkDBGrid
                        Left = 0
                        Top = 0
                        Width = 1323
                        Height = 440
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'CU_CAD'
                            Title.Caption = 'C'#243'digo'
                            Width = 44
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_CAD'
                            Title.Caption = 'Atributo'
                            Width = 199
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'CU_ITS'
                            Title.Caption = 'C'#243'digo'
                            Width = 44
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_ITS'
                            Title.Caption = 'Descri'#231#227'o do item do atributo'
                            Width = 289
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'ID_Item'
                            Title.Caption = 'ID'
                            Width = 44
                            Visible = True
                          end>
                        Color = clWindow
                        DataSource = DsAtrSTCdDef
                        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -14
                        TitleFont.Name = 'Tahoma'
                        TitleFont.Style = []
                        OnMouseUp = DBGAtrSTCdDefMouseUp
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'CU_CAD'
                            Title.Caption = 'C'#243'digo'
                            Width = 44
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_CAD'
                            Title.Caption = 'Atributo'
                            Width = 199
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'CU_ITS'
                            Title.Caption = 'C'#243'digo'
                            Width = 44
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'NO_ITS'
                            Title.Caption = 'Descri'#231#227'o do item do atributo'
                            Width = 289
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'ID_Item'
                            Title.Caption = 'ID'
                            Width = 44
                            Visible = True
                          end>
                      end
                    end
                  end
                end
              end
              object TabSheet8: TTabSheet
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' M'#243'vel / ve'#237'culo'
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel33: TPanel
                  Left = 0
                  Top = 0
                  Width = 1331
                  Height = 553
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object GroupBox9: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 1331
                    Height = 553
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    Caption = ' Atributos do m'#243'vel / ve'#237'culo selecionado: '
                    TabOrder = 0
                    object DBGAtrAMovDef: TdmkDBGrid
                      Left = 2
                      Top = 19
                      Width = 1327
                      Height = 532
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'CU_CAD'
                          Title.Caption = 'C'#243'digo'
                          Width = 44
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_CAD'
                          Title.Caption = 'Atributo'
                          Width = 199
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'CU_ITS'
                          Title.Caption = 'C'#243'digo'
                          Width = 44
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_ITS'
                          Title.Caption = 'Descri'#231#227'o do item do atributo'
                          Width = 289
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ID_Item'
                          Title.Caption = 'ID'
                          Width = 44
                          Visible = True
                        end>
                      Color = clWindow
                      DataSource = DsAtrAMovDef
                      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -14
                      TitleFont.Name = 'Tahoma'
                      TitleFont.Style = []
                      OnMouseUp = DBGAtrAMovDefMouseUp
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'CU_CAD'
                          Title.Caption = 'C'#243'digo'
                          Width = 44
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_CAD'
                          Title.Caption = 'Atributo'
                          Width = 199
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'CU_ITS'
                          Title.Caption = 'C'#243'digo'
                          Width = 44
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_ITS'
                          Title.Caption = 'Descri'#231#227'o do item do atributo'
                          Width = 289
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ID_Item'
                          Title.Caption = 'ID'
                          Width = 44
                          Visible = True
                        end>
                    end
                  end
                end
              end
              object TabSheet26: TTabSheet
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Sub-clientes'
                ImageIndex = 2
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object DBGrid1: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 1331
                  Height = 553
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  DataSource = DsSubCli
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -14
                  TitleFont.Name = 'Tahoma'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'CunsSub'
                      Title.Caption = 'Entidade'
                      Width = 46
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_SUBCLI'
                      Title.Caption = 'Nome do Sub-cliente'
                      Width = 395
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'FANTASIA'
                      Title.Caption = 'Nome fantasia'
                      Visible = True
                    end>
                end
              end
              object TabSheet30: TTabSheet
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = ' Anota'#231#245'es '
                ImageIndex = 3
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object PnFmPnAnotacoes: TPanel
                  Left = 0
                  Top = 0
                  Width = 1331
                  Height = 553
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                end
              end
              object TabSheet31: TTabSheet
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Agendamentos'
                ImageIndex = 4
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object dmkDBGridZTO1: TdmkDBGridZTO
                  Left = 0
                  Top = 29
                  Width = 1331
                  Height = 524
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  DataSource = DsSTCAgeEve
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                  PopupMenu = PMSTCAgeEve
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -14
                  TitleFont.Name = 'Tahoma'
                  TitleFont.Style = []
                  RowColors = <>
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Codigo'
                      Title.Caption = 'ID'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Inicio'
                      Title.Caption = 'In'#237'cio'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Termino'
                      Title.Caption = 'T'#233'rmino'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Terceiro'
                      Width = 44
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'TEXTO'
                      Title.Caption = 'Descri'#231#227'o do compromisso'
                      Width = 660
                      Visible = True
                    end>
                end
                object Panel12: TPanel
                  Left = 0
                  Top = 0
                  Width = 1331
                  Height = 29
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 1
                  object CkAgeSoDoLugar: TCheckBox
                    Left = 10
                    Top = 3
                    Width = 410
                    Height = 22
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Mostrar somente agendamentos do lugar selecionado.'
                    TabOrder = 0
                    OnClick = CkAgeSoDoLugarClick
                  end
                end
              end
              object TabSheet34: TTabSheet
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Padr'#245'es de OSs filhas'
                ImageIndex = 5
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object DBGSiapTerFlh: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 1331
                  Height = 553
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  DataSource = DsSiapTerFlh
                  PopupMenu = PMSiapTerFlh
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -14
                  TitleFont.Name = 'Tahoma'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Empresa'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MonAgeEqi'
                      Title.Caption = 'Eq. Agen.'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MonEntCtr'
                      Title.Caption = 'Contratante'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MonEntCtt'
                      Title.Caption = 'Contato'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MonEntPag'
                      Title.Caption = 'Pagante'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MonNumCtr'
                      Title.Caption = 'N'#250'm. contrato'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MonCondPg'
                      Title.Caption = 'Cond.Pg'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'MonCrtEmi'
                      Title.Caption = 'Cart. emi.'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DataSincOS'
                      Title.Caption = 'Dt Sinc. OS'
                      Width = 60
                      Visible = True
                    end>
                end
              end
              object TabSheet35: TTabSheet
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Config. gerais'
                ImageIndex = 6
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel43: TPanel
                  Left = 0
                  Top = 0
                  Width = 1331
                  Height = 553
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object GroupBox11: TGroupBox
                    Left = 0
                    Top = 0
                    Width = 1331
                    Height = 95
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    Caption = ' OSs: '
                    TabOrder = 0
                    object Panel44: TPanel
                      Left = 2
                      Top = 19
                      Width = 1327
                      Height = 74
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      BevelOuter = bvNone
                      TabOrder = 0
                      object Label35: TLabel
                        Left = 10
                        Top = 10
                        Width = 435
                        Height = 17
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 
                          'Lista de valores para custo de produtos nas receitas / monitoram' +
                          'entos:'
                      end
                      object DBEdit47: TDBEdit
                        Left = 10
                        Top = 31
                        Width = 70
                        Height = 25
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        DataField = 'LstCusPrd'
                        DataSource = DsSiapTerCad
                        TabOrder = 0
                      end
                      object DBEdit48: TDBEdit
                        Left = 78
                        Top = 31
                        Width = 598
                        Height = 25
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        DataField = 'NO_LSTCUSPRD'
                        DataSource = DsSiapTerCad
                        TabOrder = 1
                      end
                    end
                  end
                end
              end
              object TsFotos: TTabSheet
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'N.C.T. / Fotos'
                ImageIndex = 7
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel35: TPanel
                  Left = 0
                  Top = 0
                  Width = 1331
                  Height = 553
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Panel36: TPanel
                    Left = 0
                    Top = 0
                    Width = 1331
                    Height = 553
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Splitter3: TSplitter
                      Left = 0
                      Top = 504
                      Width = 1331
                      Height = 7
                      Cursor = crVSplit
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alBottom
                      ExplicitTop = 485
                      ExplicitWidth = 1326
                    end
                    object Panel37: TPanel
                      Left = 0
                      Top = 0
                      Width = 1331
                      Height = 504
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alClient
                      ParentBackground = False
                      TabOrder = 0
                      object Panel39: TPanel
                        Left = 1
                        Top = 1
                        Width = 1329
                        Height = 502
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        BevelOuter = bvNone
                        TabOrder = 0
                        object ImgFoto: TImage
                          Left = 792
                          Top = 0
                          Width = 537
                          Height = 291
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alClient
                          Proportional = True
                          Stretch = True
                          ExplicitWidth = 531
                          ExplicitHeight = 272
                        end
                        object Splitter4: TSplitter
                          Left = 786
                          Top = 0
                          Width = 6
                          Height = 291
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          ExplicitHeight = 272
                        end
                        object PG_NCT_CAC: TPageControl
                          Left = 0
                          Top = 291
                          Width = 1329
                          Height = 211
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          ActivePage = TabSheet29
                          Align = alBottom
                          TabOrder = 0
                          object TabSheet29: TTabSheet
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Texto livre'
                            ExplicitLeft = 0
                            ExplicitTop = 0
                            ExplicitWidth = 0
                            ExplicitHeight = 0
                            object DBMemo2: TDBMemo
                              Left = 0
                              Top = 0
                              Width = 1321
                              Height = 179
                              Margins.Left = 4
                              Margins.Top = 4
                              Margins.Right = 4
                              Margins.Bottom = 4
                              Align = alClient
                              DataField = 'Observ'
                              DataSource = DsCunsImgCab
                              ScrollBars = ssVertical
                              TabOrder = 0
                            end
                          end
                          object TabSheet39: TTabSheet
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'N.C.T.'
                            ImageIndex = 1
                            ExplicitLeft = 0
                            ExplicitTop = 0
                            ExplicitWidth = 0
                            ExplicitHeight = 0
                            object Panel40: TPanel
                              Left = 0
                              Top = 0
                              Width = 1321
                              Height = 179
                              Margins.Left = 4
                              Margins.Top = 4
                              Margins.Right = 4
                              Margins.Bottom = 4
                              Align = alClient
                              BevelOuter = bvNone
                              ParentBackground = False
                              TabOrder = 0
                              object Label32: TLabel
                                Left = 5
                                Top = 5
                                Width = 221
                                Height = 17
                                Margins.Left = 4
                                Margins.Top = 4
                                Margins.Right = 4
                                Margins.Bottom = 4
                                Caption = 'N.C.T. (N'#227'o Conformidade T'#233'cnica):'
                              end
                              object Label34: TLabel
                                Left = 5
                                Top = 84
                                Width = 57
                                Height = 17
                                Margins.Left = 4
                                Margins.Top = 4
                                Margins.Right = 4
                                Margins.Bottom = 4
                                Caption = 'C.A.C. ():'
                              end
                              object DBMemo3: TDBMemo
                                Left = 5
                                Top = 26
                                Width = 802
                                Height = 59
                                Margins.Left = 4
                                Margins.Top = 4
                                Margins.Right = 4
                                Margins.Bottom = 4
                                DataField = 'NO_NCT'
                                DataSource = DsCunsImgCab
                                ScrollBars = ssVertical
                                TabOrder = 0
                              end
                              object DBMemo4: TDBMemo
                                Left = 5
                                Top = 105
                                Width = 802
                                Height = 58
                                Margins.Left = 4
                                Margins.Top = 4
                                Margins.Right = 4
                                Margins.Bottom = 4
                                DataField = 'TxtCAC'
                                DataSource = DsCunsImgCab
                                ScrollBars = ssVertical
                                TabOrder = 1
                              end
                            end
                          end
                        end
                        object DBGCunsImgCab: TdmkDBGridZTO
                          Left = 0
                          Top = 0
                          Width = 786
                          Height = 291
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alLeft
                          DataSource = DsCunsImgCab
                          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
                          PopupMenu = PMCunsImgCab
                          TabOrder = 1
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -14
                          TitleFont.Name = 'Tahoma'
                          TitleFont.Style = []
                          RowColors = <>
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'Ordem'
                              Width = 36
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'Nome'
                              Width = 170
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_DEPENDENCIA'
                              Title.Caption = 'Local -> Depend'#234'ncia'
                              Width = 206
                              Visible = True
                            end
                            item
                              Expanded = False
                              FieldName = 'NO_STATUS'
                              Title.Caption = 'Status'
                              Width = 128
                              Visible = True
                            end>
                        end
                      end
                    end
                    object Memo1: TMemo
                      Left = 0
                      Top = 511
                      Width = 1331
                      Height = 42
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alBottom
                      Font.Charset = ANSI_CHARSET
                      Font.Color = 4196863
                      Font.Height = -17
                      Font.Name = 'Tahoma'
                      Font.Style = [fsBold]
                      ParentFont = False
                      ReadOnly = True
                      ScrollBars = ssVertical
                      TabOrder = 1
                    end
                  end
                end
              end
            end
          end
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 418
            Height = 585
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 1
            object Splitter1: TSplitter
              Left = 0
              Top = 148
              Width = 418
              Height = 7
              Cursor = crVSplit
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              ExplicitTop = 379
            end
            object Splitter2: TSplitter
              Left = 0
              Top = 141
              Width = 418
              Height = 7
              Cursor = crVSplit
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              MinSize = 1
            end
            object GroupBox3: TGroupBox
              Left = 0
              Top = 0
              Width = 418
              Height = 141
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              Caption = ' Contratantes deste cliente: '
              TabOrder = 0
              object LaContratanteAviso: TLabel
                Left = 2
                Top = 19
                Width = 449
                Height = 18
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Caption = 'Preencher apenas se o contratante for diferente da entidade'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clGreen
                Font.Height = -15
                Font.Name = 'Tahoma'
                Font.Style = [fsBold]
                ParentFont = False
              end
              object DBGCunsIts: TDBGrid
                Left = 2
                Top = 37
                Width = 414
                Height = 102
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsCunsIts
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'Tahoma'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'CunsGru'
                    Title.Caption = 'Entidade'
                    Width = 46
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_CUNSGRU'
                    Title.Caption = 'Nome do Contratante'
                    Width = 229
                    Visible = True
                  end>
              end
            end
            object GroupBox4: TGroupBox
              Left = 0
              Top = 155
              Width = 418
              Height = 231
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Caption = ' Lugares (Im'#243'veis deste cliente:) '
              TabOrder = 1
              object DBGSiapTerCad: TDBGrid
                Left = 2
                Top = 19
                Width = 414
                Height = 210
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsSiapTerCad
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'Tahoma'
                TitleFont.Style = []
                OnCellClick = DBGSiapTerCadCellClick
                OnEnter = DBGSiapTerCadEnter
                OnMouseUp = DBGSiapTerCadMouseUp
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'C'#243'digo'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Width = 234
                    Visible = True
                  end>
              end
            end
            object GroupBox7: TGroupBox
              Left = 0
              Top = 386
              Width = 418
              Height = 199
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alBottom
              Caption = ' M'#243'veis e ve'#237'culos deste cliente: '
              TabOrder = 2
              object DBGMovAmovCad: TDBGrid
                Left = 2
                Top = 19
                Width = 414
                Height = 178
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsMovAmovCad
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'Tahoma'
                TitleFont.Style = []
                OnCellClick = DBGMovAmovCadCellClick
                OnEnter = DBGMovAmovCadEnter
                OnMouseUp = DBGMovAmovCadMouseUp
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Codigo'
                    Title.Caption = 'C'#243'digo'
                    Width = 40
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Width = 234
                    Visible = True
                  end>
              end
            end
          end
        end
      end
      object TabSheet33: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Dados do cliente'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel41: TPanel
          Left = 0
          Top = 0
          Width = 1757
          Height = 585
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox10: TGroupBox
            Left = 0
            Top = 0
            Width = 1757
            Height = 64
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = ' Miscel'#226'nea: '
            TabOrder = 0
            object Panel42: TPanel
              Left = 2
              Top = 19
              Width = 1753
              Height = 43
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object DBCheckBox1: TDBCheckBox
                Left = 10
                Top = 5
                Width = 493
                Height = 22
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 
                  'Imprime dados informativos secund'#225'rios dos locais na ordem de ex' +
                  'ecu'#231#227'o.'
                DataField = 'ImpImaInfo'
                DataSource = DsCunsCad
                TabOrder = 0
                ValueChecked = '1'
                ValueUnchecked = '0'
              end
            end
          end
          object Panel30: TPanel
            Left = 0
            Top = 64
            Width = 1757
            Height = 63
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            ParentBackground = False
            ParentColor = True
            TabOrder = 1
            object Label20: TLabel
              Left = 5
              Top = 0
              Width = 113
              Height = 17
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Atividade principal:'
              FocusControl = DBEdit40
            end
            object Label21: TLabel
              Left = 340
              Top = 0
              Width = 106
              Height = 17
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Primeiro contato:'
              FocusControl = DBEdit41
            end
            object Label22: TLabel
              Left = 214
              Top = 0
              Width = 81
              Height = 17
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Respons'#225'vel:'
              FocusControl = DBEdit42
            end
            object Label23: TLabel
              Left = 565
              Top = 0
              Width = 86
              Height = 17
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Data contato:'
              FocusControl = DBEdit43
            end
            object DBEdit40: TDBEdit
              Left = 3
              Top = 22
              Width = 204
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NO_AtivPrinc'
              DataSource = DsCunsCad
              TabOrder = 0
            end
            object DBEdit41: TDBEdit
              Left = 340
              Top = 21
              Width = 220
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NO_HowFind'
              DataSource = DsCunsCad
              TabOrder = 1
            end
            object DBEdit42: TDBEdit
              Left = 214
              Top = 21
              Width = 121
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NO_Account'
              DataSource = DsCunsCad
              TabOrder = 2
            end
            object DBEdit43: TDBEdit
              Left = 565
              Top = 21
              Width = 89
              Height = 25
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'DataCon'
              DataSource = DsCunsCad
              TabOrder = 3
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Dados da entidade '
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PainelDados: TPanel
          Left = 0
          Top = 0
          Width = 1757
          Height = 585
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          Color = clDefault
          ParentBackground = False
          TabOrder = 0
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 1757
            Height = 366
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            Color = clHotLight
            ParentBackground = False
            TabOrder = 0
            object PnShowEnd: TPanel
              Left = 0
              Top = 0
              Width = 1757
              Height = 392
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object PCShowGer: TPageControl
                Left = 0
                Top = 0
                Width = 1757
                Height = 392
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                ActivePage = TabSheet4
                Align = alClient
                TabOrder = 0
                object TabSheet4: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = ' Cadastro '
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object PnShow2PF: TPanel
                    Left = 0
                    Top = 0
                    Width = 1749
                    Height = 360
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    BevelOuter = bvNone
                    Color = clSkyBlue
                    ParentBackground = False
                    TabOrder = 0
                    Visible = False
                    object Panel15: TPanel
                      Left = 0
                      Top = 0
                      Width = 1749
                      Height = 33
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alTop
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object Label28: TLabel
                        Left = 10
                        Top = 5
                        Width = 48
                        Height = 17
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Apelido:'
                      end
                      object Label33: TLabel
                        Left = 434
                        Top = 5
                        Width = 29
                        Height = 17
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'CPF:'
                      end
                      object Label36: TLabel
                        Left = 628
                        Top = 5
                        Width = 23
                        Height = 17
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'RG:'
                      end
                      object Label37: TLabel
                        Left = 811
                        Top = 5
                        Width = 93
                        Height = 17
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = #211'rg'#227'o emissor:'
                      end
                      object Label39: TLabel
                        Left = 1015
                        Top = 5
                        Width = 105
                        Height = 17
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Data da emiss'#227'o:'
                      end
                      object dmkDBEdit3: TdmkDBEdit
                        Left = 110
                        Top = 0
                        Width = 320
                        Height = 24
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        DataField = 'Apelido'
                        DataSource = DsEntidades
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -15
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 1
                        QryCampo = 'Apelido'
                        UpdCampo = 'Apelido'
                        UpdType = utYes
                        Alignment = taLeftJustify
                      end
                      object dmkDBEdit5: TdmkDBEdit
                        Left = 476
                        Top = 0
                        Width = 146
                        Height = 24
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        DataField = 'PCPF_TXT'
                        DataSource = DsEntidades
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -15
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 0
                        QryCampo = 'CPF'
                        UpdCampo = 'CPF'
                        UpdType = utYes
                        Alignment = taLeftJustify
                      end
                      object dmkDBEdit6: TdmkDBEdit
                        Left = 659
                        Top = 0
                        Width = 147
                        Height = 24
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        DataField = 'RG'
                        DataSource = DsEntidades
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -15
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 2
                        QryCampo = 'RG'
                        UpdCampo = 'RG'
                        UpdType = utYes
                        Alignment = taLeftJustify
                      end
                      object dmkDBEdit7: TdmkDBEdit
                        Left = 905
                        Top = -1
                        Width = 105
                        Height = 24
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        DataField = 'SSP'
                        DataSource = DsEntidades
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -15
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 3
                        QryCampo = 'SSP'
                        UpdCampo = 'SSP'
                        UpdType = utYes
                        Alignment = taLeftJustify
                      end
                      object dmkDBEdit34: TdmkDBEdit
                        Left = 1140
                        Top = -1
                        Width = 147
                        Height = 24
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        DataField = 'DATARG_TXT'
                        DataSource = DsEntidades
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -15
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 4
                        QryCampo = 'SSP'
                        UpdCampo = 'SSP'
                        UpdType = utYes
                        Alignment = taCenter
                      end
                    end
                    object PCShowRes: TPageControl
                      Left = 0
                      Top = -72
                      Width = 1749
                      Height = 432
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      ActivePage = TabSheet6
                      Align = alBottom
                      TabOrder = 1
                      object TabSheet6: TTabSheet
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = ' Residencial '
                        ExplicitLeft = 0
                        ExplicitTop = 0
                        ExplicitWidth = 0
                        ExplicitHeight = 0
                        object Panel16: TPanel
                          Left = 0
                          Top = 0
                          Width = 1741
                          Height = 400
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alClient
                          BevelOuter = bvNone
                          ParentBackground = False
                          TabOrder = 0
                          object Label40: TLabel
                            Left = 10
                            Top = 10
                            Width = 30
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'CEP:'
                          end
                          object Label41: TLabel
                            Left = 10
                            Top = 42
                            Width = 102
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Tipo logradouro:'
                          end
                          object Label42: TLabel
                            Left = 350
                            Top = 42
                            Width = 132
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Nome do logradouro:'
                            FocusControl = dmkDBEdit10
                          end
                          object Label43: TLabel
                            Left = 936
                            Top = 42
                            Width = 54
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'N'#250'mero:'
                            FocusControl = dmkDBEdit11
                          end
                          object Label44: TLabel
                            Left = 10
                            Top = 99
                            Width = 40
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Bairro:'
                            FocusControl = dmkDBEdit12
                          end
                          object Label45: TLabel
                            Left = 424
                            Top = 99
                            Width = 46
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Cidade:'
                            FocusControl = dmkDBEdit13
                          end
                          object Label46: TLabel
                            Left = 837
                            Top = 99
                            Width = 21
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'UF:'
                          end
                          object Label47: TLabel
                            Left = 879
                            Top = 99
                            Width = 28
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Pa'#237's:'
                          end
                          object Label48: TLabel
                            Left = 1057
                            Top = 42
                            Width = 91
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Complemento:'
                            FocusControl = dmkDBEdit17
                          end
                          object Label49: TLabel
                            Left = 10
                            Top = 152
                            Width = 68
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Refer'#234'ncia:'
                            FocusControl = dmkDBEdit18
                          end
                          object Label119: TLabel
                            Left = 10
                            Top = 204
                            Width = 46
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Cidade:'
                          end
                          object Label120: TLabel
                            Left = 811
                            Top = 204
                            Width = 28
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Pa'#237's:'
                          end
                          object Label123: TLabel
                            Left = 1135
                            Top = 152
                            Width = 68
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Telefone 1:'
                          end
                          object dmkDBEdit8: TdmkDBEdit
                            Left = 105
                            Top = 5
                            Width = 94
                            Height = 24
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'PCEP_TXT'
                            DataSource = DsEntidades
                            Font.Charset = DEFAULT_CHARSET
                            Font.Color = clWindowText
                            Font.Height = -15
                            Font.Name = 'MS Sans Serif'
                            Font.Style = []
                            ParentFont = False
                            TabOrder = 0
                            QryCampo = 'PCEP'
                            UpdCampo = 'PCEP'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object dmkDBEdit9: TdmkDBEdit
                            Left = 84
                            Top = 63
                            Width = 261
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'NOMEPLOGRAD'
                            DataSource = DsEntidades
                            TabOrder = 2
                            QryCampo = 'PLograd'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object dmkDBEdit10: TdmkDBEdit
                            Left = 350
                            Top = 63
                            Width = 581
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'PRua'
                            DataSource = DsEntidades
                            TabOrder = 3
                            QryCampo = 'PRua'
                            UpdCampo = 'PRua'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object dmkDBEdit11: TdmkDBEdit
                            Left = 936
                            Top = 63
                            Width = 115
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'PNUMERO_TXT'
                            DataSource = DsEntidades
                            TabOrder = 4
                            QryCampo = 'PNumero'
                            UpdCampo = 'PNumero'
                            UpdType = utYes
                            Alignment = taCenter
                          end
                          object dmkDBEdit12: TdmkDBEdit
                            Left = 10
                            Top = 120
                            Width = 408
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'PBairro'
                            DataSource = DsEntidades
                            TabOrder = 6
                            QryCampo = 'PBairro'
                            UpdCampo = 'PBairro'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object dmkDBEdit13: TdmkDBEdit
                            Left = 424
                            Top = 120
                            Width = 408
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'PCidade'
                            DataSource = DsEntidades
                            TabOrder = 7
                            QryCampo = 'PCidade'
                            UpdCampo = 'PCidade'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object dmkDBEdit14: TdmkDBEdit
                            Left = 837
                            Top = 120
                            Width = 37
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'NOMEPUF'
                            DataSource = DsEntidades
                            TabOrder = 8
                            QryCampo = 'PUF'
                            UpdCampo = 'PUF'
                            UpdType = utYes
                            Alignment = taCenter
                          end
                          object dmkDBEdit15: TdmkDBEdit
                            Left = 879
                            Top = 120
                            Width = 403
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'PPais'
                            DataSource = DsEntidades
                            TabOrder = 9
                            QryCampo = 'PPais'
                            UpdCampo = 'PPais'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object dmkDBEdit16: TdmkDBEdit
                            Left = 10
                            Top = 63
                            Width = 74
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'PLograd'
                            DataSource = DsEntidades
                            TabOrder = 1
                            QryCampo = 'PLograd'
                            UpdCampo = 'PLograd'
                            UpdType = utYes
                            Alignment = taRightJustify
                          end
                          object dmkDBEdit17: TdmkDBEdit
                            Left = 1057
                            Top = 63
                            Width = 225
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'PCompl'
                            DataSource = DsEntidades
                            TabOrder = 5
                            QryCampo = 'PCompl'
                            UpdCampo = 'PCompl'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object dmkDBEdit18: TdmkDBEdit
                            Left = 10
                            Top = 173
                            Width = 1121
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'PEndeRef'
                            DataSource = DsEntidades
                            TabOrder = 10
                            QryCampo = 'PEndeRef'
                            UpdCampo = 'PEndeRef'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object DBEdit10: TDBEdit
                            Left = 10
                            Top = 225
                            Width = 74
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'PCodMunici'
                            DataSource = DsEntidades
                            TabOrder = 11
                          end
                          object DBEdit11: TDBEdit
                            Left = 84
                            Top = 225
                            Width = 723
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'NO_DTB_PMUNICI'
                            DataSource = DsEntidades
                            TabOrder = 12
                          end
                          object DBEdit12: TDBEdit
                            Left = 811
                            Top = 225
                            Width = 73
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'PCodiPais'
                            DataSource = DsEntidades
                            TabOrder = 13
                          end
                          object DBEdit13: TDBEdit
                            Left = 884
                            Top = 225
                            Width = 399
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'NO_BACEN_PPAIS'
                            DataSource = DsEntidades
                            TabOrder = 14
                          end
                          object DBEdit16: TDBEdit
                            Left = 1135
                            Top = 173
                            Width = 147
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'PTE1_TXT'
                            DataSource = DsEntidades
                            TabOrder = 15
                          end
                        end
                      end
                    end
                  end
                  object PnShow2Pj: TPanel
                    Left = 0
                    Top = 0
                    Width = 1749
                    Height = 360
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    BevelOuter = bvNone
                    Color = clSkyBlue
                    ParentBackground = False
                    TabOrder = 1
                    Visible = False
                    object Panel18: TPanel
                      Left = 0
                      Top = 0
                      Width = 1749
                      Height = 33
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alTop
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 0
                      object Label51: TLabel
                        Left = 10
                        Top = 5
                        Width = 94
                        Height = 17
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Nome Fantasia:'
                      end
                      object Label52: TLabel
                        Left = 905
                        Top = 5
                        Width = 37
                        Height = 17
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'CNPJ:'
                      end
                      object Label53: TLabel
                        Left = 1098
                        Top = 5
                        Width = 25
                        Height = 17
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'I.E.:'
                      end
                      object dmkDBEdit20: TdmkDBEdit
                        Left = 110
                        Top = 0
                        Width = 791
                        Height = 24
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        DataField = 'Fantasia'
                        DataSource = DsEntidades
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -15
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 1
                        QryCampo = 'Fantasia'
                        UpdCampo = 'Fantasia'
                        UpdType = utYes
                        Alignment = taLeftJustify
                      end
                      object dmkDBEdit21: TdmkDBEdit
                        Left = 947
                        Top = 0
                        Width = 148
                        Height = 24
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        DataField = 'CNPJ_TXT'
                        DataSource = DsEntidades
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -15
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 0
                        QryCampo = 'CNPJ'
                        UpdCampo = 'CNPJ'
                        UpdType = utYes
                        Alignment = taLeftJustify
                      end
                      object dmkDBEdit22: TdmkDBEdit
                        Left = 1140
                        Top = 0
                        Width = 148
                        Height = 24
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        DataField = 'IE_TXT'
                        DataSource = DsEntidades
                        Font.Charset = DEFAULT_CHARSET
                        Font.Color = clWindowText
                        Font.Height = -15
                        Font.Name = 'MS Sans Serif'
                        Font.Style = []
                        ParentFont = False
                        TabOrder = 2
                        QryCampo = 'IE'
                        UpdCampo = 'IE'
                        UpdType = utYes
                        Alignment = taLeftJustify
                      end
                    end
                    object PCShowCom: TPageControl
                      Left = 0
                      Top = -84
                      Width = 1749
                      Height = 444
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      ActivePage = TabSheet5
                      Align = alBottom
                      TabOrder = 1
                      object TabSheet5: TTabSheet
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = ' Comercial '
                        ExplicitLeft = 0
                        ExplicitTop = 0
                        ExplicitWidth = 0
                        ExplicitHeight = 0
                        object Panel19: TPanel
                          Left = 0
                          Top = 0
                          Width = 1741
                          Height = 412
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Align = alClient
                          BevelOuter = bvNone
                          ParentBackground = False
                          TabOrder = 0
                          object Label54: TLabel
                            Left = 10
                            Top = 10
                            Width = 30
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'CEP:'
                          end
                          object Label55: TLabel
                            Left = 10
                            Top = 42
                            Width = 102
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Tipo logradouro:'
                          end
                          object Label56: TLabel
                            Left = 350
                            Top = 42
                            Width = 132
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Nome do logradouro:'
                            FocusControl = dmkDBEdit25
                          end
                          object Label57: TLabel
                            Left = 936
                            Top = 42
                            Width = 54
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'N'#250'mero:'
                            FocusControl = dmkDBEdit26
                          end
                          object Label58: TLabel
                            Left = 10
                            Top = 99
                            Width = 40
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Bairro:'
                            FocusControl = dmkDBEdit27
                          end
                          object Label59: TLabel
                            Left = 424
                            Top = 99
                            Width = 46
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Cidade:'
                            FocusControl = dmkDBEdit28
                          end
                          object Label60: TLabel
                            Left = 837
                            Top = 99
                            Width = 21
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'UF:'
                          end
                          object Label61: TLabel
                            Left = 879
                            Top = 99
                            Width = 28
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Pa'#237's:'
                          end
                          object Label62: TLabel
                            Left = 1057
                            Top = 42
                            Width = 91
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Complemento:'
                            FocusControl = dmkDBEdit32
                          end
                          object Label63: TLabel
                            Left = 466
                            Top = 152
                            Width = 68
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Refer'#234'ncia:'
                            FocusControl = dmkDBEdit33
                          end
                          object Label117: TLabel
                            Left = 10
                            Top = 204
                            Width = 46
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Cidade:'
                          end
                          object Label118: TLabel
                            Left = 811
                            Top = 204
                            Width = 28
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Pa'#237's:'
                          end
                          object Label121: TLabel
                            Left = 1240
                            Top = 204
                            Width = 27
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Filial:'
                            Visible = False
                          end
                          object Label122: TLabel
                            Left = 1135
                            Top = 152
                            Width = 68
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'Telefone 1:'
                          end
                          object Label126: TLabel
                            Left = 10
                            Top = 258
                            Width = 342
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'CNAE (Classifica'#231#227'o Nacional de Atividades Econ'#244'micas):'
                          end
                          object Label131: TLabel
                            Left = 314
                            Top = 152
                            Width = 27
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'I.M.:'
                          end
                          object Label132: TLabel
                            Left = 162
                            Top = 152
                            Width = 49
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'I.E.S.T.:'
                          end
                          object Label133: TLabel
                            Left = 10
                            Top = 152
                            Width = 25
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'I.E.:'
                          end
                          object Label136: TLabel
                            Left = 10
                            Top = 309
                            Width = 218
                            Height = 17
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            Caption = 'CRT (C'#243'digo da Regime Tribut'#225'rio):'
                          end
                          object dmkDBEdit23: TdmkDBEdit
                            Left = 105
                            Top = 5
                            Width = 94
                            Height = 24
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'ECEP_TXT'
                            DataSource = DsEntidades
                            Font.Charset = DEFAULT_CHARSET
                            Font.Color = clWindowText
                            Font.Height = -15
                            Font.Name = 'MS Sans Serif'
                            Font.Style = []
                            ParentFont = False
                            TabOrder = 0
                            QryCampo = 'ECEP'
                            UpdCampo = 'ECEP'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object dmkDBEdit24: TdmkDBEdit
                            Left = 84
                            Top = 63
                            Width = 261
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'NOMEELOGRAD'
                            DataSource = DsEntidades
                            TabOrder = 2
                            QryCampo = 'ELograd'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object dmkDBEdit25: TdmkDBEdit
                            Left = 350
                            Top = 63
                            Width = 581
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'ERua'
                            DataSource = DsEntidades
                            TabOrder = 3
                            QryCampo = 'ERua'
                            UpdCampo = 'ERua'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object dmkDBEdit26: TdmkDBEdit
                            Left = 936
                            Top = 63
                            Width = 115
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'ENUMERO_TXT'
                            DataSource = DsEntidades
                            TabOrder = 4
                            QryCampo = 'ENumero'
                            UpdCampo = 'ENumero'
                            UpdType = utYes
                            Alignment = taCenter
                          end
                          object dmkDBEdit27: TdmkDBEdit
                            Left = 10
                            Top = 120
                            Width = 408
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'EBairro'
                            DataSource = DsEntidades
                            TabOrder = 6
                            QryCampo = 'EBairro'
                            UpdCampo = 'EBairro'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object dmkDBEdit28: TdmkDBEdit
                            Left = 424
                            Top = 120
                            Width = 408
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'ECidade'
                            DataSource = DsEntidades
                            TabOrder = 7
                            QryCampo = 'ECidade'
                            UpdCampo = 'ECidade'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object dmkDBEdit29: TdmkDBEdit
                            Left = 837
                            Top = 120
                            Width = 37
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'NOMEEUF'
                            DataSource = DsEntidades
                            TabOrder = 8
                            QryCampo = 'EUF'
                            UpdCampo = 'EUF'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object dmkDBEdit30: TdmkDBEdit
                            Left = 879
                            Top = 120
                            Width = 403
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'EPais'
                            DataSource = DsEntidades
                            TabOrder = 9
                            QryCampo = 'EPais'
                            UpdCampo = 'EPais'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object dmkDBEdit31: TdmkDBEdit
                            Left = 10
                            Top = 63
                            Width = 74
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'ELograd'
                            DataSource = DsEntidades
                            TabOrder = 1
                            QryCampo = 'ELograd'
                            UpdCampo = 'ELograd'
                            UpdType = utYes
                            Alignment = taRightJustify
                          end
                          object dmkDBEdit32: TdmkDBEdit
                            Left = 1057
                            Top = 63
                            Width = 225
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'ECompl'
                            DataSource = DsEntidades
                            TabOrder = 5
                            QryCampo = 'ECompl'
                            UpdCampo = 'ECompl'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object dmkDBEdit33: TdmkDBEdit
                            Left = 466
                            Top = 173
                            Width = 665
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'EEndeRef'
                            DataSource = DsEntidades
                            TabOrder = 10
                            QryCampo = 'EEndeRef'
                            UpdCampo = 'EEndeRef'
                            UpdType = utYes
                            Alignment = taLeftJustify
                          end
                          object DBEdit6: TDBEdit
                            Left = 84
                            Top = 225
                            Width = 723
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'NO_DTB_EMUNICI'
                            DataSource = DsEntidades
                            TabOrder = 11
                          end
                          object DBEdit7: TDBEdit
                            Left = 884
                            Top = 225
                            Width = 352
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'NO_BACEN_EPAIS'
                            DataSource = DsEntidades
                            TabOrder = 12
                          end
                          object DBEdit8: TDBEdit
                            Left = 811
                            Top = 225
                            Width = 73
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'ECodiPais'
                            DataSource = DsEntidades
                            TabOrder = 13
                          end
                          object DBEdit9: TDBEdit
                            Left = 10
                            Top = 225
                            Width = 74
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'ECodMunici'
                            DataSource = DsEntidades
                            TabOrder = 14
                          end
                          object DBEdit14: TDBEdit
                            Left = 1240
                            Top = 225
                            Width = 42
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'Filial'
                            DataSource = DsEntidades
                            TabOrder = 15
                          end
                          object DBEdit15: TDBEdit
                            Left = 1135
                            Top = 173
                            Width = 147
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'ETE1_TXT'
                            DataSource = DsEntidades
                            TabOrder = 16
                          end
                          object DBEdit17: TDBEdit
                            Left = 10
                            Top = 279
                            Width = 74
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'CNAE'
                            DataSource = DsEntidades
                            TabOrder = 17
                          end
                          object DBEdit18: TDBEdit
                            Left = 84
                            Top = 279
                            Width = 1198
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'CNAE_Nome'
                            DataSource = DsEntidades
                            TabOrder = 18
                          end
                          object DBEdit19: TDBEdit
                            Left = 162
                            Top = 173
                            Width = 147
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'IEST'
                            DataSource = DsEntidades
                            TabOrder = 19
                          end
                          object DBEdit20: TDBEdit
                            Left = 314
                            Top = 173
                            Width = 146
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'NIRE'
                            DataSource = DsEntidades
                            TabOrder = 20
                          end
                          object DBEdit21: TDBEdit
                            Left = 10
                            Top = 173
                            Width = 147
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'IE_TXT'
                            DataSource = DsEntidades
                            TabOrder = 21
                          end
                          object Memo2: TMemo
                            Left = 89
                            Top = 330
                            Width = 1194
                            Height = 27
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            ReadOnly = True
                            TabOrder = 22
                          end
                          object DBEdCRT: TDBEdit
                            Left = 10
                            Top = 330
                            Width = 74
                            Height = 25
                            Margins.Left = 4
                            Margins.Top = 4
                            Margins.Right = 4
                            Margins.Bottom = 4
                            DataField = 'CRT'
                            DataSource = DsEntidades
                            TabOrder = 23
                          end
                        end
                      end
                    end
                  end
                end
                object TabSheet7: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = ' Tipo '
                  ImageIndex = 1
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object Panel13: TPanel
                    Left = 0
                    Top = 0
                    Width = 1749
                    Height = 360
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Label128: TLabel
                      Left = 5
                      Top = 124
                      Width = 125
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Carteira preferencial:'
                    end
                    object Label138: TLabel
                      Left = 5
                      Top = 178
                      Width = 176
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'C'#243'd. participante SPED EFD:'
                    end
                    object Label140: TLabel
                      Left = 235
                      Top = 178
                      Width = 64
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'SUFRAMA:'
                    end
                    object GroupBox1: TGroupBox
                      Left = 5
                      Top = 5
                      Width = 832
                      Height = 111
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = ' Tipo de cadastro: '
                      TabOrder = 0
                      object CkCliente1: TDBCheckBox
                        Left = 10
                        Top = 21
                        Width = 199
                        Height = 22
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Cliente'
                        DataField = 'Cliente1'
                        DataSource = DsEntidades
                        TabOrder = 0
                        ValueChecked = 'V'
                        ValueUnchecked = 'F'
                      end
                      object CkFornece1: TDBCheckBox
                        Left = 214
                        Top = 21
                        Width = 199
                        Height = 22
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Fornecedor'
                        DataField = 'Fornece1'
                        DataSource = DsEntidades
                        TabOrder = 4
                        ValueChecked = 'V'
                        ValueUnchecked = 'F'
                      end
                      object CkFornece2: TDBCheckBox
                        Left = 214
                        Top = 42
                        Width = 199
                        Height = 22
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Transportador'
                        DataField = 'Fornece2'
                        DataSource = DsEntidades
                        TabOrder = 5
                        ValueChecked = 'V'
                        ValueUnchecked = 'F'
                      end
                      object CkFornece3: TDBCheckBox
                        Left = 214
                        Top = 63
                        Width = 199
                        Height = 22
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Vendedor'
                        DataField = 'Fornece3'
                        DataSource = DsEntidades
                        TabOrder = 6
                        ValueChecked = 'V'
                        ValueUnchecked = 'F'
                      end
                      object CkTerceiro: TDBCheckBox
                        Left = 622
                        Top = 21
                        Width = 199
                        Height = 22
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Terceiro'
                        DataField = 'Terceiro'
                        DataSource = DsEntidades
                        TabOrder = 12
                        ValueChecked = 'V'
                        ValueUnchecked = 'F'
                      end
                      object CkFornece4: TDBCheckBox
                        Left = 214
                        Top = 84
                        Width = 199
                        Height = 22
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Fornece 4'
                        DataField = 'Fornece4'
                        DataSource = DsEntidades
                        TabOrder = 7
                        ValueChecked = 'V'
                        ValueUnchecked = 'F'
                        Visible = False
                      end
                      object CkCliente2: TDBCheckBox
                        Left = 10
                        Top = 42
                        Width = 199
                        Height = 22
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Cliente 2'
                        DataField = 'Cliente2'
                        DataSource = DsEntidades
                        TabOrder = 1
                        ValueChecked = 'V'
                        ValueUnchecked = 'F'
                        Visible = False
                      end
                      object CkFornece5: TDBCheckBox
                        Left = 418
                        Top = 21
                        Width = 199
                        Height = 22
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Fornece 5'
                        DataField = 'Fornece5'
                        DataSource = DsEntidades
                        TabOrder = 8
                        ValueChecked = 'V'
                        ValueUnchecked = 'F'
                        Visible = False
                      end
                      object CkFornece6: TDBCheckBox
                        Left = 418
                        Top = 42
                        Width = 199
                        Height = 22
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Fornece 6'
                        DataField = 'Fornece6'
                        DataSource = DsEntidades
                        TabOrder = 9
                        ValueChecked = 'V'
                        ValueUnchecked = 'F'
                        Visible = False
                      end
                      object CkCliente3: TDBCheckBox
                        Left = 10
                        Top = 63
                        Width = 199
                        Height = 22
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Cliente 3'
                        DataField = 'Cliente3'
                        DataSource = DsEntidades
                        TabOrder = 2
                        ValueChecked = 'V'
                        ValueUnchecked = 'F'
                        Visible = False
                      end
                      object CkCliente4: TDBCheckBox
                        Left = 10
                        Top = 84
                        Width = 199
                        Height = 22
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Cliente 4'
                        DataField = 'Cliente4'
                        DataSource = DsEntidades
                        TabOrder = 3
                        ValueChecked = 'V'
                        ValueUnchecked = 'F'
                        Visible = False
                      end
                      object CkFornece8: TDBCheckBox
                        Left = 418
                        Top = 84
                        Width = 199
                        Height = 22
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Fornece 8'
                        DataField = 'Fornece6'
                        DataSource = DsEntidades
                        TabOrder = 11
                        ValueChecked = 'V'
                        ValueUnchecked = 'F'
                        Visible = False
                      end
                      object CkFornece7: TDBCheckBox
                        Left = 418
                        Top = 63
                        Width = 199
                        Height = 22
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Caption = 'Fornece 7'
                        DataField = 'Fornece5'
                        DataSource = DsEntidades
                        TabOrder = 10
                        ValueChecked = 'V'
                        ValueUnchecked = 'F'
                        Visible = False
                      end
                    end
                    object dmkDBEdit57: TdmkDBEdit
                      Left = 5
                      Top = 145
                      Width = 832
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'NOMECARTPREF'
                      DataSource = DsEntidades
                      TabOrder = 1
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit58: TdmkDBEdit
                      Left = 5
                      Top = 199
                      Width = 225
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'COD_PART'
                      DataSource = DsEntidades
                      TabOrder = 2
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit59: TdmkDBEdit
                      Left = 235
                      Top = 199
                      Width = 225
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'SUFRAMA'
                      DataSource = DsEntidades
                      TabOrder = 3
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                  end
                end
                object TabSheet11: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = ' Endere'#231'o de cobran'#231'a '
                  ImageIndex = 2
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object Panel6: TPanel
                    Left = 0
                    Top = 0
                    Width = 1749
                    Height = 360
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Label64: TLabel
                      Left = 10
                      Top = 10
                      Width = 30
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'CEP:'
                    end
                    object Label65: TLabel
                      Left = 10
                      Top = 42
                      Width = 102
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Tipo logradouro:'
                    end
                    object Label66: TLabel
                      Left = 350
                      Top = 42
                      Width = 132
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Nome do logradouro:'
                      FocusControl = dmkDBEdit37
                    end
                    object Label67: TLabel
                      Left = 936
                      Top = 42
                      Width = 54
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'N'#250'mero:'
                      FocusControl = dmkDBEdit38
                    end
                    object Label68: TLabel
                      Left = 10
                      Top = 99
                      Width = 40
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Bairro:'
                      FocusControl = dmkDBEdit39
                    end
                    object Label69: TLabel
                      Left = 424
                      Top = 99
                      Width = 46
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Cidade:'
                      FocusControl = dmkDBEdit40
                    end
                    object Label70: TLabel
                      Left = 837
                      Top = 99
                      Width = 21
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'UF:'
                    end
                    object Label71: TLabel
                      Left = 879
                      Top = 99
                      Width = 28
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Pa'#237's:'
                    end
                    object Label72: TLabel
                      Left = 1057
                      Top = 42
                      Width = 91
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Complemento:'
                      FocusControl = dmkDBEdit44
                    end
                    object Label73: TLabel
                      Left = 10
                      Top = 152
                      Width = 68
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Refer'#234'ncia:'
                      FocusControl = dmkDBEdit45
                    end
                    object Label116: TLabel
                      Left = 10
                      Top = 204
                      Width = 46
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Cidade:'
                    end
                    object dmkDBEdit35: TdmkDBEdit
                      Left = 105
                      Top = 5
                      Width = 94
                      Height = 24
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'CCEP_TXT'
                      DataSource = DsEntidades
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 0
                      QryCampo = 'PCEP'
                      UpdCampo = 'PCEP'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit36: TdmkDBEdit
                      Left = 84
                      Top = 63
                      Width = 261
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'NOMECLOGRAD'
                      DataSource = DsEntidades
                      TabOrder = 2
                      QryCampo = 'PLograd'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit37: TdmkDBEdit
                      Left = 350
                      Top = 63
                      Width = 581
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'CRua'
                      DataSource = DsEntidades
                      TabOrder = 3
                      QryCampo = 'PRua'
                      UpdCampo = 'PRua'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit38: TdmkDBEdit
                      Left = 936
                      Top = 63
                      Width = 115
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'CNUMERO_TXT'
                      DataSource = DsEntidades
                      TabOrder = 4
                      QryCampo = 'PNumero'
                      UpdCampo = 'PNumero'
                      UpdType = utYes
                      Alignment = taCenter
                    end
                    object dmkDBEdit39: TdmkDBEdit
                      Left = 10
                      Top = 120
                      Width = 408
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'CBairro'
                      DataSource = DsEntidades
                      TabOrder = 6
                      QryCampo = 'PBairro'
                      UpdCampo = 'PBairro'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit40: TdmkDBEdit
                      Left = 424
                      Top = 120
                      Width = 408
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'CCidade'
                      DataSource = DsEntidades
                      TabOrder = 7
                      QryCampo = 'PCidade'
                      UpdCampo = 'PCidade'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit41: TdmkDBEdit
                      Left = 837
                      Top = 120
                      Width = 37
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'NOMECUF'
                      DataSource = DsEntidades
                      TabOrder = 8
                      QryCampo = 'PUF'
                      UpdCampo = 'PUF'
                      UpdType = utYes
                      Alignment = taCenter
                    end
                    object dmkDBEdit42: TdmkDBEdit
                      Left = 879
                      Top = 120
                      Width = 403
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'CPais'
                      DataSource = DsEntidades
                      TabOrder = 9
                      QryCampo = 'PPais'
                      UpdCampo = 'PPais'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit43: TdmkDBEdit
                      Left = 10
                      Top = 63
                      Width = 74
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'CLograd'
                      DataSource = DsEntidades
                      TabOrder = 1
                      QryCampo = 'PLograd'
                      UpdCampo = 'PLograd'
                      UpdType = utYes
                      Alignment = taRightJustify
                    end
                    object dmkDBEdit44: TdmkDBEdit
                      Left = 1057
                      Top = 63
                      Width = 225
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'CCompl'
                      DataSource = DsEntidades
                      TabOrder = 5
                      QryCampo = 'PCompl'
                      UpdCampo = 'PCompl'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit45: TdmkDBEdit
                      Left = 10
                      Top = 173
                      Width = 1272
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'CEndeRef'
                      DataSource = DsEntidades
                      TabOrder = 10
                      QryCampo = 'PEndeRef'
                      UpdCampo = 'PEndeRef'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object DBEdit4: TDBEdit
                      Left = 10
                      Top = 225
                      Width = 75
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'CCodMunici'
                      DataSource = DsEntidades
                      TabOrder = 11
                    end
                    object DBEdit5: TDBEdit
                      Left = 89
                      Top = 225
                      Width = 1194
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'NO_DTB_CMUNICI'
                      DataSource = DsEntidades
                      TabOrder = 12
                    end
                  end
                end
                object TabSheet12: TTabSheet
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = ' Endere'#231'o de entrega '
                  ImageIndex = 3
                  ExplicitLeft = 0
                  ExplicitTop = 0
                  ExplicitWidth = 0
                  ExplicitHeight = 0
                  object Panel14: TPanel
                    Left = 0
                    Top = 0
                    Width = 1749
                    Height = 360
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    BevelOuter = bvNone
                    ParentBackground = False
                    TabOrder = 0
                    object Label74: TLabel
                      Left = 214
                      Top = 10
                      Width = 30
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'CEP:'
                    end
                    object Label75: TLabel
                      Left = 10
                      Top = 42
                      Width = 102
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Tipo logradouro:'
                    end
                    object Label76: TLabel
                      Left = 350
                      Top = 42
                      Width = 132
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Nome do logradouro:'
                      FocusControl = dmkDBEdit48
                    end
                    object Label77: TLabel
                      Left = 936
                      Top = 42
                      Width = 54
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'N'#250'mero:'
                      FocusControl = dmkDBEdit49
                    end
                    object Label78: TLabel
                      Left = 10
                      Top = 99
                      Width = 40
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Bairro:'
                      FocusControl = dmkDBEdit50
                    end
                    object Label79: TLabel
                      Left = 424
                      Top = 99
                      Width = 46
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Cidade:'
                      FocusControl = dmkDBEdit51
                    end
                    object Label80: TLabel
                      Left = 837
                      Top = 99
                      Width = 21
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'UF:'
                    end
                    object Label81: TLabel
                      Left = 879
                      Top = 99
                      Width = 28
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Pa'#237's:'
                    end
                    object Label82: TLabel
                      Left = 1057
                      Top = 42
                      Width = 91
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Complemento:'
                      FocusControl = dmkDBEdit55
                    end
                    object Label83: TLabel
                      Left = 10
                      Top = 152
                      Width = 68
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Refer'#234'ncia:'
                      FocusControl = dmkDBEdit56
                    end
                    object Label114: TLabel
                      Left = 10
                      Top = 10
                      Width = 37
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'CNPJ:'
                      FocusControl = DBEdit1
                    end
                    object Label115: TLabel
                      Left = 10
                      Top = 204
                      Width = 46
                      Height = 17
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Cidade:'
                    end
                    object dmkDBEdit46: TdmkDBEdit
                      Left = 251
                      Top = 5
                      Width = 94
                      Height = 24
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'LCEP_TXT'
                      DataSource = DsEntidades
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clWindowText
                      Font.Height = -15
                      Font.Name = 'MS Sans Serif'
                      Font.Style = []
                      ParentFont = False
                      TabOrder = 0
                      QryCampo = 'PCEP'
                      UpdCampo = 'PCEP'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit47: TdmkDBEdit
                      Left = 84
                      Top = 63
                      Width = 261
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'NOMELLOGRAD'
                      DataSource = DsEntidades
                      TabOrder = 2
                      QryCampo = 'PLograd'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit48: TdmkDBEdit
                      Left = 350
                      Top = 63
                      Width = 581
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'LRua'
                      DataSource = DsEntidades
                      TabOrder = 3
                      QryCampo = 'PRua'
                      UpdCampo = 'PRua'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit49: TdmkDBEdit
                      Left = 936
                      Top = 63
                      Width = 115
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'LNUMERO_TXT'
                      DataSource = DsEntidades
                      TabOrder = 4
                      QryCampo = 'PNumero'
                      UpdCampo = 'PNumero'
                      UpdType = utYes
                      Alignment = taCenter
                    end
                    object dmkDBEdit50: TdmkDBEdit
                      Left = 10
                      Top = 120
                      Width = 408
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'LBairro'
                      DataSource = DsEntidades
                      TabOrder = 6
                      QryCampo = 'PBairro'
                      UpdCampo = 'PBairro'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit51: TdmkDBEdit
                      Left = 424
                      Top = 120
                      Width = 408
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'LCidade'
                      DataSource = DsEntidades
                      TabOrder = 7
                      QryCampo = 'PCidade'
                      UpdCampo = 'PCidade'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit52: TdmkDBEdit
                      Left = 837
                      Top = 120
                      Width = 37
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'NOMELUF'
                      DataSource = DsEntidades
                      TabOrder = 8
                      QryCampo = 'PUF'
                      UpdCampo = 'PUF'
                      UpdType = utYes
                      Alignment = taCenter
                    end
                    object dmkDBEdit53: TdmkDBEdit
                      Left = 879
                      Top = 120
                      Width = 403
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'LPais'
                      DataSource = DsEntidades
                      TabOrder = 9
                      QryCampo = 'PPais'
                      UpdCampo = 'PPais'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit54: TdmkDBEdit
                      Left = 10
                      Top = 63
                      Width = 74
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'LLograd'
                      DataSource = DsEntidades
                      TabOrder = 1
                      QryCampo = 'PLograd'
                      UpdCampo = 'PLograd'
                      UpdType = utYes
                      Alignment = taRightJustify
                    end
                    object dmkDBEdit55: TdmkDBEdit
                      Left = 1057
                      Top = 63
                      Width = 225
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'LCompl'
                      DataSource = DsEntidades
                      TabOrder = 5
                      QryCampo = 'PCompl'
                      UpdCampo = 'PCompl'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object dmkDBEdit56: TdmkDBEdit
                      Left = 10
                      Top = 173
                      Width = 1273
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'LEndeRef'
                      DataSource = DsEntidades
                      TabOrder = 10
                      QryCampo = 'PEndeRef'
                      UpdCampo = 'PEndeRef'
                      UpdType = utYes
                      Alignment = taLeftJustify
                    end
                    object DBEdit1: TDBEdit
                      Left = 52
                      Top = 5
                      Width = 148
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'L_CNPJ_TXT'
                      DataSource = DsEntidades
                      TabOrder = 11
                    end
                    object DBRadioGroup1: TDBRadioGroup
                      Left = 1088
                      Top = 204
                      Width = 195
                      Height = 48
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = ' Entregar neste endere'#231'o?: '
                      Columns = 2
                      DataField = 'L_Ativo'
                      DataSource = DsEntidades
                      Items.Strings = (
                        'N'#227'o'
                        'Sim')
                      ParentBackground = True
                      TabOrder = 12
                      Values.Strings = (
                        '0'
                        '1')
                    end
                    object DBEdit2: TDBEdit
                      Left = 10
                      Top = 225
                      Width = 75
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'LCodMunici'
                      DataSource = DsEntidades
                      TabOrder = 13
                    end
                    object DBEdit3: TDBEdit
                      Left = 89
                      Top = 225
                      Width = 995
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      DataField = 'NO_DTB_LMUNICI'
                      DataSource = DsEntidades
                      TabOrder = 14
                    end
                  end
                end
              end
            end
          end
          object PCXtraGer: TPageControl
            Left = 0
            Top = 391
            Width = 1757
            Height = 194
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            ActivePage = TabSheet16
            Align = alBottom
            TabOrder = 1
            object TabSheet9: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Contatos '
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object PnContatos: TPanel
                Left = 0
                Top = 0
                Width = 1749
                Height = 162
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object DBGEntiContat: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 492
                  Height = 162
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alLeft
                  DataSource = DsEntiContat
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -14
                  TitleFont.Name = 'Tahoma'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Nome'
                      Width = 200
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NOME_CARGO'
                      Title.Caption = 'Cargo'
                      Width = 140
                      Visible = True
                    end>
                end
                object Panel10: TPanel
                  Left = 492
                  Top = 0
                  Width = 1257
                  Height = 162
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  BevelOuter = bvNone
                  Caption = 'Panel10'
                  TabOrder = 1
                  object DBGEntiMail: TDBGrid
                    Left = 366
                    Top = 0
                    Width = 891
                    Height = 162
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    DataSource = DsEntiMail
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -14
                    TitleFont.Name = 'Tahoma'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Ordem'
                        Width = 39
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NOMEETC'
                        Title.Caption = 'Tipo de e-mail'
                        Width = 84
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'EMail'
                        Title.Caption = 'E-mail'
                        Width = 496
                        Visible = True
                      end>
                  end
                  object DBGEntiTel: TDBGrid
                    Left = 0
                    Top = 0
                    Width = 366
                    Height = 162
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alLeft
                    DataSource = DsEntiTel
                    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                    TabOrder = 1
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -14
                    TitleFont.Name = 'Tahoma'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'NOMEETC'
                        Title.Caption = 'Tipo de telefone'
                        Width = 84
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'TEL_TXT'
                        Title.Caption = 'Telefone'
                        Width = 120
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Ramal'
                        Width = 37
                        Visible = True
                      end>
                  end
                end
              end
            end
            object TabSheet10: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Atributos '
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object GradeDefAtr: TdmkDBGrid
                Left = 0
                Top = 0
                Width = 1749
                Height = 162
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'CODUSU_CAD'
                    Title.Caption = 'C'#243'digo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOME_CAD'
                    Title.Caption = 'Atributo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CODUSU_ITS'
                    Title.Caption = 'C'#243'digo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOME_ITS'
                    Title.Caption = 'Descri'#231#227'o do item do atributo'
                    Width = 236
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CTRL_ATR'
                    Title.Caption = 'ID'
                    Visible = True
                  end>
                Color = clWindow
                DataSource = DsEntDefAtr
                Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'Tahoma'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'CODUSU_CAD'
                    Title.Caption = 'C'#243'digo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOME_CAD'
                    Title.Caption = 'Atributo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CODUSU_ITS'
                    Title.Caption = 'C'#243'digo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOME_ITS'
                    Title.Caption = 'Descri'#231#227'o do item do atributo'
                    Width = 236
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'CTRL_ATR'
                    Title.Caption = 'ID'
                    Visible = True
                  end>
              end
            end
            object TabSheet15: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Respons'#225'veis '
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object DBGEntiRespon: TDBGrid
                Left = 0
                Top = 0
                Width = 1749
                Height = 162
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsEntiRespon
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'Tahoma'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'OrdemLista'
                    Title.Caption = 'Ordem lista'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Nome'
                    Width = 214
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOME_CARGO'
                    Title.Caption = 'Cargo'
                    Width = 112
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_ASSINA'
                    Title.Caption = 'Assina'
                    Width = 36
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MandatoIni_TXT'
                    Title.Caption = 'Ini. mandato'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'MandatoFim_TXT'
                    Title.Caption = 'Fim mandato'
                    Width = 56
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Observ'
                    Title.Caption = 'Observa'#231#245'es'
                    Width = 900
                    Visible = True
                  end>
              end
            end
            object TabSheet16: TTabSheet
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Dados Banc'#225'rios'
              ImageIndex = 3
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object DBGEntiCtas: TDBGrid
                Left = 0
                Top = 0
                Width = 1749
                Height = 162
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                DataSource = DsEntiCtas
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -14
                TitleFont.Name = 'Tahoma'
                TitleFont.Style = []
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Banco'
                    Width = 53
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEBANCO'
                    Title.Caption = 'Banco'
                    Width = 278
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Agencia'
                    Title.Caption = 'Ag'#234'ncia'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DAC_A'
                    Title.Caption = '[A]'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ContaCor'
                    Title.Caption = 'Conta'
                    Width = 73
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DAC_C'
                    Title.Caption = '[C]'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'DAC_AC'
                    Title.Caption = '[AC]'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'ContaTip'
                    Title.Caption = 'Tipo'
                    Visible = True
                  end>
              end
            end
          end
        end
      end
      object TabSheet32: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Financeiro'
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnFinancas: TPanel
          Left = 0
          Top = 0
          Width = 1757
          Height = 585
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
        end
      end
      object TabSheet36: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Provid'#234'ncias'
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnOSPrvGer: TPanel
          Left = 0
          Top = 0
          Width = 1757
          Height = 585
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
        end
      end
      object TabSheet37: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'OSs'
        ImageIndex = 6
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnOSs: TPanel
          Left = 0
          Top = 0
          Width = 1757
          Height = 585
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object DBGPsq: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 1757
            Height = 585
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsOSCab
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            RowColors = <>
            OnDblClick = DBGPsqDblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'OS'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Estatus_TXT'
                Title.Caption = 'Status'
                Width = 130
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_EMPRESA'
                Title.Caption = 'Empresa'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_ENTCONTRAT'
                Title.Caption = 'Contratante (EP)'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AF_ENTCONTRAT'
                Title.Caption = 'Contratante (FA)'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_ENTPAGANTE'
                Title.Caption = 'Pagante (EP)'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'AF_ENTPAGANTE'
                Title.Caption = 'Pagante (FA)'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_ENTICONTAT'
                Title.Caption = 'Contato'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtaContat'
                Title.Caption = 'Contato'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtaExePrv'
                Title.Caption = 'Prev. Exec.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtaExeFim'
                Title.Caption = 'Fim execu'#231#227'o'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'InvalTotal'
                Title.Caption = '$ N'#227'o aprov.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorTotal'
                Title.Caption = '$ Aprovado'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OrcamTotal'
                Title.Caption = '$ Or'#231'ado'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_OPERACOES'
                Title.Caption = 'Opera'#231#245'es'
                Width = 180
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_MUL_SRV'
                Title.Caption = 'Multi servi'#231'os'
                Width = 300
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_AgeEqiCab'
                Title.Caption = 'Equipe de agentes'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_WOW_GEROU'
                Title.Caption = 'Como Gerou'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OSFlhGrCab'
                Title.Caption = 'Grupo filhas'
                Width = 80
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OSFlhGrIts'
                Title.Caption = 'ID Filha'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_LUGAR'
                Title.Caption = 'Lugar'
                Width = 300
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_USER_ALT'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataAlt'
                Title.Caption = 'Alterado em'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_USER_ALT'
                Title.Caption = 'Alterado Por'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DataCad'
                Title.Caption = 'Inserido em'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_USER_CAD'
                Title.Caption = 'Inserido por'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_SOH_INICIAL'
                Title.Caption = 'S'#243' inicial'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_ST_PIP_AD_PRG'
                Title.Caption = 'Sit.adi.perg.PMV'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'OSFlhUltGe'
                Title.Caption = #218'ltima filha'
                Visible = True
              end>
          end
        end
      end
      object Diário: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Di'#225'rio'
        ImageIndex = 7
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnDiarioGer2: TPanel
          Left = 0
          Top = 0
          Width = 1757
          Height = 585
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
        end
      end
      object Contratos: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Contratos'
        ImageIndex = 8
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnContratos: TPanel
          Left = 0
          Top = 0
          Width = 1757
          Height = 585
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object dmkDBGridZTO2: TdmkDBGridZTO
            Left = 0
            Top = 0
            Width = 1757
            Height = 585
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsContratos
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -14
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            RowColors = <>
            OnDblClick = dmkDBGridZTO2DblClick
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'Contrato'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Versao'
                Title.Caption = 'Vers'#227'o'
                Width = 42
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 260
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DContrato'
                Title.Caption = 'Dta Contrato'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DInstal'
                Title.Caption = 'In'#237'cio vig'#234'ncia'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtaPrxRenw'
                Title.Caption = 'Pr'#243'x. renov.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DtaCntrFim'
                Title.Caption = 'Data rescis'#227'o'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValorMes'
                Title.Caption = '$ Mensal'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TXT_STATUS'
                Title.Caption = 'Status'
                Width = 117
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'LugarNome'
                Title.Caption = 'Lugar'
                Width = 164
                Visible = True
              end>
          end
        end
      end
      object TabSheet38: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Documentos'
        ImageIndex = 9
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PnDocsCab: TPanel
          Left = 0
          Top = 0
          Width = 1757
          Height = 585
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
        end
      end
      object TabSheet28: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'PMVs'
        ImageIndex = 9
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGPMVs: TdmkDBGridZTO
          Left = 0
          Top = 22
          Width = 1757
          Height = 563
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsPMVs
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
          PopupMenu = PMPMVs
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          RowColors = <>
          OnCellClick = DBGPMVsCellClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PerioDd'
              Title.Caption = 'Periodo dd'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DdPostero'
              Title.Caption = 'Interv. dd'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_EQUI'
              Title.Caption = 'Equipamento'
              Width = 240
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_DEPENDENCIA'
              Title.Caption = 'Depend'#234'ncia'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OSMonCab'
              Title.Caption = 'F'#243'rm. instal.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DtaAquis_TXT'
              Title.Caption = 'Data aquis.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_LISTA'
              Title.Caption = 'Lista de perguntas'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NO_MOTDESAT'
              Title.Caption = 'Motivo de desativa'#231#227'o'
              Visible = True
            end>
        end
        object Panel45: TPanel
          Left = 0
          Top = 0
          Width = 1757
          Height = 22
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          Caption = 
            'Clique na c'#233'lula da coluna "F'#243'rm.instal." para abrir a OS de ins' +
            'tala'#231#227'o do PMV correspondente.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentBackground = False
          ParentFont = False
          TabOrder = 1
        end
      end
      object TSWeb: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Web'
        ImageIndex = 10
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel46: TPanel
          Left = 0
          Top = 0
          Width = 1757
          Height = 63
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object BtWUsers: TBitBtn
            Tag = 160
            Left = 5
            Top = 5
            Width = 157
            Height = 53
            Cursor = crHandPoint
            Hint = 'Inclui novo registro'
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Senhas web'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtWUsersClick
          end
        end
        object dmkDBGrid2: TdmkDBGrid
          Left = 0
          Top = 63
          Width = 1757
          Height = 522
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Perfil_TXT'
              Title.Caption = 'Perfil'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PersonalName'
              Title.Caption = 'Nome'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Username'
              Title.Caption = 'Usu'#225'rio'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Email'
              Title.Caption = 'E-mail'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsWUsers
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Perfil_TXT'
              Title.Caption = 'Perfil'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PersonalName'
              Title.Caption = 'Nome'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Username'
              Title.Caption = 'Usu'#225'rio'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Email'
              Title.Caption = 'E-mail'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Ativo'
              Visible = True
            end>
        end
      end
    end
    object GroupBox12: TGroupBox
      Left = 0
      Top = 756
      Width = 1765
      Height = 52
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Caption = '  Arquivos e/ou diret'#243'rios n'#227'o localizados: '
      TabOrder = 3
      object MeANL: TMemo
        Left = 2
        Top = 19
        Width = 1761
        Height = 31
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = 4196863
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        ScrollBars = ssVertical
        TabOrder = 0
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1765
    Height = 68
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 1703
      Top = 0
      Width = 62
      Height = 68
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 16
        Width = 42
        Height = 42
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 340
      Height = 68
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 10
        Width = 53
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 60
        Top = 10
        Width = 52
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 115
        Top = 10
        Width = 52
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 170
        Top = 10
        Width = 52
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 225
        Top = 10
        Width = 52
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object BtEntidade: TBitBtn
        Tag = 122
        Left = 281
        Top = 10
        Width = 52
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 5
        OnClick = BtEntidadeClick
      end
    end
    object GB_M: TGroupBox
      Left = 340
      Top = 0
      Width = 356
      Height = 68
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 12
        Width = 317
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -35
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 12
        Top = 14
        Width = 317
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -35
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 13
        Width = 317
        Height = 40
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cadastro de Clientes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -35
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GroupBox2: TGroupBox
      Left = 1472
      Top = 0
      Width = 231
      Height = 68
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 3
      object SbSelfGer2: TBitBtn
        Tag = 10045
        Left = 5
        Top = 10
        Width = 53
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbSelfGer2Click
      end
      object SbProvidencias: TBitBtn
        Tag = 430
        Left = 60
        Top = 10
        Width = 52
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbProvidenciasClick
      end
      object BtOSs: TBitBtn
        Tag = 539
        Left = 115
        Top = 10
        Width = 52
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtOSsClick
      end
      object BtDiarioGer2: TBitBtn
        Tag = 398
        Left = 170
        Top = 10
        Width = 52
        Height = 53
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BtDiarioGer2Click
      end
    end
    object GBAvisos1: TGroupBox
      Left = 696
      Top = 0
      Width = 776
      Height = 68
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Caption = ' Avisos: '
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 4
      object Panel4: TPanel
        Left = 2
        Top = 19
        Width = 772
        Height = 47
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 17
          Top = 3
          Width = 679
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Clique com o bot'#227'o esquerdo do mouse na grade desejada para gere' +
            'ciar o item desta grade!'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 16
          Top = 1
          Width = 679
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Clique com o bot'#227'o esquerdo do mouse na grade desejada para gere' +
            'ciar o item desta grade!'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -17
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 404
    Top = 508
  end
  object QrCunsCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCunsCadBeforeOpen
    AfterOpen = QrCunsCadAfterOpen
    BeforeClose = QrCunsCadBeforeClose
    AfterScroll = QrCunsCadAfterScroll
    SQL.Strings = (
      'SELECT ati.Nome NO_AtivPrinc, how.Nome NO_HowFind, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOME' +
        'ENTIDADE,'
      'IF(acc.Tipo=0, RazaoSocial, acc.Nome) NO_Account, cun.* '
      'FROM cunscad cun'
      'LEFT JOIN atividades ati ON ati.Codigo=cun.AtivPrinc'
      'LEFT JOIN howfounded how ON how.Codigo=cun.HowFind'
      'LEFT JOIN entidades acc ON acc.Codigo=cun.Account'
      'WHERE cun.Codigo > 0')
    Left = 348
    Top = 508
    object QrCunsCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCunsCadAtivPrinc: TIntegerField
      FieldName = 'AtivPrinc'
    end
    object QrCunsCadHowFind: TIntegerField
      FieldName = 'HowFind'
    end
    object QrCunsCadAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrCunsCadDataCon: TDateField
      FieldName = 'DataCon'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCunsCadNO_AtivPrinc: TWideStringField
      FieldName = 'NO_AtivPrinc'
      Size = 60
    end
    object QrCunsCadNO_HowFind: TWideStringField
      FieldName = 'NO_HowFind'
      Size = 60
    end
    object QrCunsCadNO_Account: TWideStringField
      FieldName = 'NO_Account'
      Size = 100
    end
    object QrCunsCadNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrCunsCadImpImaInfo: TSmallintField
      FieldName = 'ImpImaInfo'
    end
  end
  object DsCunsCad: TDataSource
    DataSet = QrCunsCad
    Left = 376
    Top = 508
  end
  object QrCunsIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CUNSGRU,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, cui.* '
      'FROM cunsits cui'
      'LEFT JOIN entidades ent ON ent.Codigo=cui.CunsGru'
      'WHERE cui.CunsSub =:P0'
      'ORDER BY NO_CUNSGRU'
      '')
    Left = 488
    Top = 508
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCunsItsNO_CUNSGRU: TWideStringField
      FieldName = 'NO_CUNSGRU'
      Size = 100
    end
    object QrCunsItsCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrCunsItsCunsGru: TIntegerField
      FieldName = 'CunsGru'
    end
    object QrCunsItsCunsSub: TIntegerField
      FieldName = 'CunsSub'
    end
    object QrCunsItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object DsCunsIts: TDataSource
    DataSet = QrCunsIts
    Left = 524
    Top = 540
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 492
    Top = 300
    object Inclui1: TMenuItem
      Caption = '&Inclui / Edita dados da entidade'
      OnClick = Inclui1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera dados complementares'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntidadesBeforeClose
    AfterScroll = QrEntidadesAfterScroll
    OnCalcFields = QrEntidadesCalcFields
    SQL.Strings = (
      'SELECT ent.*, car.Nome NOMECARTPREF, cna.Nome CNAE_Nome,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOME' +
        'ENTIDADE,'
      
        'CASE WHEN acm.Tipo=0 THEN acm.RazaoSocial ELSE acm.Nome END NOME' +
        'ACCOUNT,'
      
        'eng.Nome NOMEENTIGRUPO, mot.Descricao NOMEMOTIVO, emp.RazaoSocia' +
        'l NOMEEMPRESA,'
      
        'euf.Nome NOMEEUF, puf.nome NOMEPUF, cuf.Nome NOMECUF, luf.nome N' +
        'OMELUF, '
      'nuf.Nome NOMENUF, elo.Nome NOMEELOGRAD, plo.Nome NOMEPLOGRAD,'
      'clo.Nome NOMECLOGRAD, llo.Nome NOMELLOGRAD, lec.Nome NOMEECIVIL,'
      'mue.Nome NO_DTB_EMUNICI, mup.Nome NO_DTB_PMUNICI,'
      'muc.Nome NO_DTB_CMUNICI, mul.Nome NO_DTB_LMUNICI,'
      'pae.Nome NO_BACEN_EPAIS, pap.Nome NO_BACEN_PPAIS,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECompl ELSE ent.PCompl END COMPLEN' +
        'T'
      'FROM entidades ent'
      'LEFT JOIN entigrupos eng  ON eng.Codigo=ent.Grupo'
      'LEFT JOIN entidades acm   ON acm.Codigo=ent.Account'
      'LEFT JOIN entidades emp   ON emp.Codigo=ent.Empresa'
      'LEFT JOIN motivose mot    ON mot.Codigo=ent.Motivo'
      'LEFT JOIN ufs euf         ON euf.Codigo=ent.EUF'
      'LEFT JOIN ufs puf         ON puf.Codigo=ent.PUF'
      'LEFT JOIN ufs cuf         ON cuf.Codigo=ent.CUF'
      'LEFT JOIN ufs luf         ON luf.Codigo=ent.LUF'
      'LEFT JOIN ufs nuf         ON nuf.Codigo=ent.UFNatal'
      'LEFT JOIN listalograd elo ON elo.Codigo=ent.ELograd'
      'LEFT JOIN listalograd plo ON plo.Codigo=ent.PLograd'
      'LEFT JOIN listalograd clo ON clo.Codigo=ent.CLograd'
      'LEFT JOIN listalograd llo ON llo.Codigo=ent.LLograd'
      'LEFT JOIN listaecivil lec ON lec.Codigo=ent.EstCivil'
      'LEFT JOIN carteiras   car ON car.Codigo=ent.CartPref'
      
        'LEFT JOIN locbugstall.dtb_munici mue ON mue.Codigo=ent.ECodMunic' +
        'i'
      
        'LEFT JOIN locbugstall.dtb_munici mup ON mup.Codigo=ent.PCodMunic' +
        'i'
      
        'LEFT JOIN locbugstall.dtb_munici muc ON muc.Codigo=ent.CCodMunic' +
        'i'
      
        'LEFT JOIN locbugstall.dtb_munici mul ON mul.Codigo=ent.LCodMunic' +
        'i'
      'LEFT JOIN locbugstall.bacen_pais pae ON pae.Codigo=ent.ECodiPais'
      'LEFT JOIN locbugstall.bacen_pais pap ON pap.Codigo=ent.PCodiPais'
      'LEFT JOIN locbugstall.cnae21cad cna ON cna.CodAlf=ent.CNAE'
      'WHERE ent.Codigo=:P0')
    Left = 432
    Top = 508
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEntidadesCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'entidades.CodUsu'
      Required = True
    end
    object QrEntidadesRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Origin = 'entidades.RazaoSocial'
      Required = True
      Size = 100
    end
    object QrEntidadesFantasia: TWideStringField
      FieldName = 'Fantasia'
      Origin = 'entidades.Fantasia'
      Required = True
      Size = 60
    end
    object QrEntidadesRespons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Required = True
      Size = 60
    end
    object QrEntidadesRespons2: TWideStringField
      FieldName = 'Respons2'
      Origin = 'entidades.Respons2'
      Required = True
      Size = 60
    end
    object QrEntidadesPai: TWideStringField
      FieldName = 'Pai'
      Origin = 'entidades.Pai'
      Required = True
      Size = 60
    end
    object QrEntidadesMae: TWideStringField
      FieldName = 'Mae'
      Origin = 'entidades.Mae'
      Required = True
      Size = 60
    end
    object QrEntidadesCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'entidades.CNPJ'
      Size = 18
    end
    object QrEntidadesIE: TWideStringField
      FieldName = 'IE'
      Origin = 'entidades.IE'
      Size = 15
    end
    object QrEntidadesNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'entidades.Nome'
      Required = True
      Size = 100
    end
    object QrEntidadesApelido: TWideStringField
      FieldName = 'Apelido'
      Origin = 'entidades.Apelido'
      Required = True
      Size = 60
    end
    object QrEntidadesCPF: TWideStringField
      FieldName = 'CPF'
      Origin = 'entidades.CPF'
      Size = 18
    end
    object QrEntidadesRG: TWideStringField
      FieldName = 'RG'
      Origin = 'entidades.RG'
      Size = 15
    end
    object QrEntidadesERua: TWideStringField
      DisplayWidth = 60
      FieldName = 'ERua'
      Origin = 'entidades.ERua'
      Size = 60
    end
    object QrEntidadesECompl: TWideStringField
      DisplayWidth = 60
      FieldName = 'ECompl'
      Origin = 'entidades.ECompl'
      Size = 60
    end
    object QrEntidadesEBairro: TWideStringField
      DisplayWidth = 60
      FieldName = 'EBairro'
      Origin = 'entidades.EBairro'
      Size = 60
    end
    object QrEntidadesECidade: TWideStringField
      DisplayWidth = 60
      FieldName = 'ECidade'
      Origin = 'entidades.ECidade'
      Size = 60
    end
    object QrEntidadesEUF: TSmallintField
      FieldName = 'EUF'
      Origin = 'entidades.EUF'
      Required = True
    end
    object QrEntidadesEPais: TWideStringField
      DisplayWidth = 60
      FieldName = 'EPais'
      Origin = 'entidades.EPais'
      Size = 60
    end
    object QrEntidadesETe1: TWideStringField
      FieldName = 'ETe1'
      Origin = 'entidades.ETe1'
    end
    object QrEntidadesETe2: TWideStringField
      FieldName = 'ETe2'
      Origin = 'entidades.Ete2'
    end
    object QrEntidadesETe3: TWideStringField
      FieldName = 'ETe3'
      Origin = 'entidades.Ete3'
    end
    object QrEntidadesECel: TWideStringField
      FieldName = 'ECel'
      Origin = 'entidades.ECel'
    end
    object QrEntidadesEFax: TWideStringField
      FieldName = 'EFax'
      Origin = 'entidades.EFax'
    end
    object QrEntidadesEEMail: TWideStringField
      FieldName = 'EEMail'
      Origin = 'entidades.EEmail'
      Size = 100
    end
    object QrEntidadesEContato: TWideStringField
      FieldName = 'EContato'
      Origin = 'entidades.EContato'
      Size = 60
    end
    object QrEntidadesENatal: TDateField
      FieldName = 'ENatal'
      Origin = 'entidades.ENatal'
    end
    object QrEntidadesPRua: TWideStringField
      DisplayWidth = 60
      FieldName = 'PRua'
      Origin = 'entidades.PRua'
      Size = 60
    end
    object QrEntidadesPCompl: TWideStringField
      DisplayWidth = 60
      FieldName = 'PCompl'
      Origin = 'entidades.PCompl'
      Size = 60
    end
    object QrEntidadesPBairro: TWideStringField
      DisplayWidth = 60
      FieldName = 'PBairro'
      Origin = 'entidades.PBairro'
      Size = 60
    end
    object QrEntidadesPCidade: TWideStringField
      DisplayWidth = 60
      FieldName = 'PCidade'
      Origin = 'entidades.PCidade'
      Size = 60
    end
    object QrEntidadesPUF: TSmallintField
      FieldName = 'PUF'
      Origin = 'entidades.PUF'
      Required = True
    end
    object QrEntidadesPPais: TWideStringField
      DisplayWidth = 60
      FieldName = 'PPais'
      Origin = 'entidades.PPais'
      Size = 60
    end
    object QrEntidadesPTe1: TWideStringField
      FieldName = 'PTe1'
      Origin = 'entidades.PTe1'
    end
    object QrEntidadesPTe2: TWideStringField
      FieldName = 'PTe2'
      Origin = 'entidades.Pte2'
    end
    object QrEntidadesPTe3: TWideStringField
      FieldName = 'PTe3'
      Origin = 'entidades.Pte3'
    end
    object QrEntidadesPCel: TWideStringField
      FieldName = 'PCel'
      Origin = 'entidades.PCel'
    end
    object QrEntidadesPFax: TWideStringField
      FieldName = 'PFax'
      Origin = 'entidades.PFax'
    end
    object QrEntidadesPEMail: TWideStringField
      FieldName = 'PEMail'
      Origin = 'entidades.PEmail'
      Size = 100
    end
    object QrEntidadesPContato: TWideStringField
      FieldName = 'PContato'
      Origin = 'entidades.PContato'
      Size = 60
    end
    object QrEntidadesPNatal: TDateField
      FieldName = 'PNatal'
      Origin = 'entidades.PNatal'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEntidadesSexo: TWideStringField
      FieldName = 'Sexo'
      Origin = 'entidades.Sexo'
      Required = True
      Size = 1
    end
    object QrEntidadesResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Origin = 'entidades.Responsavel'
      Size = 60
    end
    object QrEntidadesProfissao: TWideStringField
      FieldName = 'Profissao'
      Origin = 'entidades.Profissao'
      Size = 60
    end
    object QrEntidadesCargo: TWideStringField
      FieldName = 'Cargo'
      Origin = 'entidades.Cargo'
      Size = 60
    end
    object QrEntidadesRecibo: TSmallintField
      FieldName = 'Recibo'
      Origin = 'entidades.Recibo'
      Required = True
    end
    object QrEntidadesDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Origin = 'entidades.DiaRecibo'
      Required = True
    end
    object QrEntidadesAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Origin = 'entidades.AjudaEmpV'
      Required = True
    end
    object QrEntidadesAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Origin = 'entidades.AjudaEmpP'
      Required = True
    end
    object QrEntidadesCliente1: TWideStringField
      FieldName = 'Cliente1'
      Origin = 'entidades.Cliente1'
      Size = 1
    end
    object QrEntidadesCliente2: TWideStringField
      FieldName = 'Cliente2'
      Origin = 'entidades.Cliente2'
      Size = 1
    end
    object QrEntidadesCliente3: TWideStringField
      FieldName = 'Cliente3'
      Origin = 'entidades.Cliente3'
      Size = 1
    end
    object QrEntidadesCliente4: TWideStringField
      FieldName = 'Cliente4'
      Origin = 'entidades.Cliente4'
      Size = 1
    end
    object QrEntidadesFornece1: TWideStringField
      FieldName = 'Fornece1'
      Origin = 'entidades.Fornece1'
      Size = 1
    end
    object QrEntidadesFornece2: TWideStringField
      FieldName = 'Fornece2'
      Origin = 'entidades.Fornece2'
      Size = 1
    end
    object QrEntidadesFornece3: TWideStringField
      FieldName = 'Fornece3'
      Origin = 'entidades.Fornece3'
      Size = 1
    end
    object QrEntidadesFornece4: TWideStringField
      FieldName = 'Fornece4'
      Origin = 'entidades.Fornece4'
      Size = 1
    end
    object QrEntidadesFornece5: TWideStringField
      FieldName = 'Fornece5'
      Origin = 'entidades.Fornece5'
      Size = 1
    end
    object QrEntidadesFornece6: TWideStringField
      FieldName = 'Fornece6'
      Origin = 'entidades.Fornece6'
      Size = 1
    end
    object QrEntidadesFornece7: TWideStringField
      FieldName = 'Fornece7'
      Origin = 'entidades.Fornece7'
      Size = 1
    end
    object QrEntidadesFornece8: TWideStringField
      FieldName = 'Fornece8'
      Origin = 'entidades.Fornece8'
      Size = 1
    end
    object QrEntidadesTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Origin = 'entidades.Terceiro'
      Size = 1
    end
    object QrEntidadesCadastro: TDateField
      FieldName = 'Cadastro'
      Origin = 'entidades.Cadastro'
    end
    object QrEntidadesInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Origin = 'entidades.Informacoes'
      Size = 255
    end
    object QrEntidadesLogo: TBlobField
      FieldName = 'Logo'
      Origin = 'entidades.Logo'
      Size = 4
    end
    object QrEntidadesVeiculo: TIntegerField
      FieldName = 'Veiculo'
      Origin = 'entidades.Veiculo'
    end
    object QrEntidadesMensal: TWideStringField
      FieldName = 'Mensal'
      Origin = 'entidades.Mensal'
      Size = 1
    end
    object QrEntidadesObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      Origin = 'entidades.Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrEntidadesTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrEntidadesLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'entidades.Lk'
    end
    object QrEntidadesNOMEENTIDADE: TWideStringField
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrEntidadesNOMEEUF: TWideStringField
      FieldName = 'NOMEEUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesNOMEPUF: TWideStringField
      FieldName = 'NOMEPUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesPCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPTE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE2_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPTE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PTE3_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesPFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE2_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesETE3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE3_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesECEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesEFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesECEP: TIntegerField
      FieldName = 'ECEP'
      Origin = 'entidades.ECEP'
    end
    object QrEntidadesPCEP: TIntegerField
      FieldName = 'PCEP'
      Origin = 'entidades.PCEP'
    end
    object QrEntidadesGrupo: TIntegerField
      FieldName = 'Grupo'
      Origin = 'entidades.Grupo'
    end
    object QrEntidadesDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'entidades.DataAlt'
    end
    object QrEntidadesUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'entidades.UserCad'
    end
    object QrEntidadesUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'entidades.UserAlt'
    end
    object QrEntidadesCRua: TWideStringField
      FieldName = 'CRua'
      Origin = 'entidades.CRua'
      Size = 60
    end
    object QrEntidadesCCompl: TWideStringField
      FieldName = 'CCompl'
      Origin = 'entidades.CCompl'
      Size = 60
    end
    object QrEntidadesCBairro: TWideStringField
      FieldName = 'CBairro'
      Origin = 'entidades.CBairro'
      Size = 60
    end
    object QrEntidadesCCidade: TWideStringField
      FieldName = 'CCidade'
      Origin = 'entidades.CCidade'
      Size = 60
    end
    object QrEntidadesCUF: TSmallintField
      FieldName = 'CUF'
      Origin = 'entidades.CUF'
      Required = True
    end
    object QrEntidadesCCEP: TIntegerField
      FieldName = 'CCEP'
      Origin = 'entidades.CCEP'
    end
    object QrEntidadesCPais: TWideStringField
      FieldName = 'CPais'
      Origin = 'entidades.CPais'
    end
    object QrEntidadesCTel: TWideStringField
      FieldName = 'CTel'
      Origin = 'entidades.CTel'
    end
    object QrEntidadesCFax: TWideStringField
      FieldName = 'CFax'
      Origin = 'entidades.CFax'
    end
    object QrEntidadesCCel: TWideStringField
      FieldName = 'CCel'
      Origin = 'entidades.CCel'
    end
    object QrEntidadesCContato: TWideStringField
      FieldName = 'CContato'
      Origin = 'entidades.CContato'
      Size = 60
    end
    object QrEntidadesLRua: TWideStringField
      FieldName = 'LRua'
      Origin = 'entidades.LRua'
      Size = 60
    end
    object QrEntidadesLCompl: TWideStringField
      FieldName = 'LCompl'
      Origin = 'entidades.LCompl'
      Size = 60
    end
    object QrEntidadesLBairro: TWideStringField
      FieldName = 'LBairro'
      Origin = 'entidades.LBairro'
      Size = 60
    end
    object QrEntidadesLCidade: TWideStringField
      FieldName = 'LCidade'
      Origin = 'entidades.LCidade'
      Size = 60
    end
    object QrEntidadesLUF: TSmallintField
      FieldName = 'LUF'
      Origin = 'entidades.LUF'
      Required = True
    end
    object QrEntidadesLCEP: TIntegerField
      FieldName = 'LCEP'
      Origin = 'entidades.LCEP'
    end
    object QrEntidadesLPais: TWideStringField
      FieldName = 'LPais'
      Origin = 'entidades.LPais'
    end
    object QrEntidadesLTel: TWideStringField
      FieldName = 'LTel'
      Origin = 'entidades.LTel'
    end
    object QrEntidadesLFax: TWideStringField
      FieldName = 'LFax'
      Origin = 'entidades.LFax'
    end
    object QrEntidadesLCel: TWideStringField
      FieldName = 'LCel'
      Origin = 'entidades.LCel'
    end
    object QrEntidadesLContato: TWideStringField
      FieldName = 'LContato'
      Origin = 'entidades.LContato'
      Size = 60
    end
    object QrEntidadesComissao: TFloatField
      FieldName = 'Comissao'
      Origin = 'entidades.Comissao'
      DisplayFormat = '0.000000'
    end
    object QrEntidadesDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'entidades.DataCad'
      Required = True
    end
    object QrEntidadesECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesPCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PCEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesCCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CCEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesLCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LCEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrEntidadesNOMECAD2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECAD2'
      Size = 100
      Calculated = True
    end
    object QrEntidadesNOMEALT2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEALT2'
      Size = 100
      Calculated = True
    end
    object QrEntidadesSituacao: TSmallintField
      FieldName = 'Situacao'
      Origin = 'entidades.Situacao'
    end
    object QrEntidadesNivel: TWideStringField
      FieldName = 'Nivel'
      Origin = 'entidades.Nivel'
      Size = 1
    end
    object QrEntidadesCTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CTEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesCFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesCCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CCEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesLTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LTEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesLFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LFAX_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesLCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LCEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesNOMEENTIGRUPO: TWideStringField
      FieldName = 'NOMEENTIGRUPO'
      Origin = 'entigrupos.Nome'
      Size = 100
    end
    object QrEntidadesNOMECUF: TWideStringField
      FieldName = 'NOMECUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesNOMELUF: TWideStringField
      FieldName = 'NOMELUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesAccount: TIntegerField
      FieldName = 'Account'
      Origin = 'entidades.Account'
    end
    object QrEntidadesNOMEACCOUNT: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEACCOUNT'
      Size = 100
    end
    object QrEntidadesLogo2: TBlobField
      FieldName = 'Logo2'
      Origin = 'entidades.Logo2'
      Size = 4
    end
    object QrEntidadesELograd: TSmallintField
      FieldName = 'ELograd'
      Origin = 'entidades.ELograd'
      Required = True
    end
    object QrEntidadesPLograd: TSmallintField
      FieldName = 'PLograd'
      Origin = 'entidades.PLograd'
      Required = True
    end
    object QrEntidadesConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Origin = 'entidades.ConjugeNome'
      Size = 35
    end
    object QrEntidadesConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
      Origin = 'entidades.ConjugeNatal'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEntidadesNome1: TWideStringField
      FieldName = 'Nome1'
      Origin = 'entidades.Nome1'
      Size = 30
    end
    object QrEntidadesNatal1: TDateField
      FieldName = 'Natal1'
      Origin = 'entidades.Natal1'
    end
    object QrEntidadesNome2: TWideStringField
      FieldName = 'Nome2'
      Origin = 'entidades.Nome2'
      Size = 30
    end
    object QrEntidadesNatal2: TDateField
      FieldName = 'Natal2'
      Origin = 'entidades.Natal2'
    end
    object QrEntidadesNome3: TWideStringField
      FieldName = 'Nome3'
      Origin = 'entidades.Nome3'
      Size = 30
    end
    object QrEntidadesNatal3: TDateField
      FieldName = 'Natal3'
      Origin = 'entidades.Natal3'
    end
    object QrEntidadesCreditosI: TIntegerField
      FieldName = 'CreditosI'
      Origin = 'entidades.CreditosI'
      Required = True
    end
    object QrEntidadesCreditosL: TIntegerField
      FieldName = 'CreditosL'
      Origin = 'entidades.CreditosL'
      Required = True
    end
    object QrEntidadesCreditosD: TDateField
      FieldName = 'CreditosD'
      Origin = 'entidades.CreditosD'
    end
    object QrEntidadesCreditosU: TDateField
      FieldName = 'CreditosU'
      Origin = 'entidades.CreditosU'
    end
    object QrEntidadesCreditosV: TDateField
      FieldName = 'CreditosV'
      Origin = 'entidades.CreditosV'
    end
    object QrEntidadesMotivo: TIntegerField
      FieldName = 'Motivo'
      Origin = 'entidades.Motivo'
      Required = True
    end
    object QrEntidadesQuantI1: TIntegerField
      FieldName = 'QuantI1'
      Origin = 'entidades.QuantI1'
      Required = True
    end
    object QrEntidadesQuantI2: TIntegerField
      FieldName = 'QuantI2'
      Origin = 'entidades.QuantI2'
      Required = True
    end
    object QrEntidadesQuantI3: TIntegerField
      FieldName = 'QuantI3'
      Origin = 'entidades.QuantI3'
      Required = True
    end
    object QrEntidadesQuantI4: TIntegerField
      FieldName = 'QuantI4'
      Origin = 'entidades.QuantI4'
      Required = True
    end
    object QrEntidadesAgenda: TWideStringField
      FieldName = 'Agenda'
      Origin = 'entidades.Agenda'
      Required = True
      Size = 1
    end
    object QrEntidadesSenhaQuer: TWideStringField
      FieldName = 'SenhaQuer'
      Origin = 'entidades.SenhaQuer'
      Required = True
      Size = 1
    end
    object QrEntidadesSenha1: TWideStringField
      FieldName = 'Senha1'
      Origin = 'entidades.Senha1'
      Size = 6
    end
    object QrEntidadesNatal4: TDateField
      FieldName = 'Natal4'
      Origin = 'entidades.Natal4'
    end
    object QrEntidadesNome4: TWideStringField
      FieldName = 'Nome4'
      Origin = 'entidades.Nome4'
      Size = 30
    end
    object QrEntidadesNOMEMOTIVO: TWideStringField
      FieldName = 'NOMEMOTIVO'
      Origin = 'motivose.Descricao'
      Size = 50
    end
    object QrEntidadesCreditosF2: TFloatField
      FieldName = 'CreditosF2'
      Origin = 'entidades.CreditosF2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesNOMEELOGRAD: TWideStringField
      FieldName = 'NOMEELOGRAD'
      Origin = 'listalograd.Nome'
      Size = 10
    end
    object QrEntidadesNOMEPLOGRAD: TWideStringField
      FieldName = 'NOMEPLOGRAD'
      Origin = 'listalograd.Nome'
      Size = 10
    end
    object QrEntidadesCLograd: TSmallintField
      FieldName = 'CLograd'
      Origin = 'entidades.CLograd'
      Required = True
    end
    object QrEntidadesLLograd: TSmallintField
      FieldName = 'LLograd'
      Origin = 'entidades.LLograd'
      Required = True
    end
    object QrEntidadesNOMECLOGRAD: TWideStringField
      FieldName = 'NOMECLOGRAD'
      Origin = 'listalograd.Nome'
      Size = 10
    end
    object QrEntidadesNOMELLOGRAD: TWideStringField
      FieldName = 'NOMELLOGRAD'
      Origin = 'listalograd.Nome'
      Size = 10
    end
    object QrEntidadesQuantN1: TFloatField
      FieldName = 'QuantN1'
      Origin = 'entidades.QuantN1'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesQuantN2: TFloatField
      FieldName = 'QuantN2'
      Origin = 'entidades.QuantN2'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesLimiCred: TFloatField
      FieldName = 'LimiCred'
      Origin = 'entidades.LimiCred'
      DisplayFormat = '#,###,##0.00'
    end
    object QrEntidadesDesco: TFloatField
      FieldName = 'Desco'
      Origin = 'entidades.Desco'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrEntidadesCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
      Origin = 'entidades.CasasApliDesco'
      Required = True
    end
    object QrEntidadesCPF_Pai: TWideStringField
      FieldName = 'CPF_Pai'
      Origin = 'entidades.CPF_Pai'
      Size = 18
    end
    object QrEntidadesSSP: TWideStringField
      FieldName = 'SSP'
      Origin = 'entidades.SSP'
      Size = 10
    end
    object QrEntidadesCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Origin = 'entidades.CidadeNatal'
      Size = 30
    end
    object QrEntidadesCPF_PAI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_PAI_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesUFNatal: TSmallintField
      FieldName = 'UFNatal'
      Origin = 'entidades.UFNatal'
      Required = True
    end
    object QrEntidadesNOMENUF: TWideStringField
      FieldName = 'NOMENUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEntidadesFatorCompra: TFloatField
      FieldName = 'FatorCompra'
      Origin = 'entidades.FatorCompra'
      DisplayFormat = '#,###,###0.0000'
    end
    object QrEntidadesAdValorem: TFloatField
      FieldName = 'AdValorem'
      Origin = 'entidades.AdValorem'
      DisplayFormat = '#,###,###0.0000'
    end
    object QrEntidadesDMaisC: TIntegerField
      FieldName = 'DMaisC'
      Origin = 'entidades.DMaisC'
      DisplayFormat = '0'
    end
    object QrEntidadesDMaisD: TIntegerField
      FieldName = 'DMaisD'
      Origin = 'entidades.DMaisD'
      DisplayFormat = '0'
    end
    object QrEntidadesDataRG: TDateField
      FieldName = 'DataRG'
      Origin = 'entidades.DataRG'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrEntidadesNacionalid: TWideStringField
      FieldName = 'Nacionalid'
      Origin = 'entidades.Nacionalid'
      Size = 15
    end
    object QrEntidadesEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'entidades.Empresa'
    end
    object QrEntidadesNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Origin = 'entidades.RazaoSocial'
      Size = 100
    end
    object QrEntidadesFormaSociet: TWideStringField
      FieldName = 'FormaSociet'
      Origin = 'entidades.FormaSociet'
    end
    object QrEntidadesSimples: TSmallintField
      FieldName = 'Simples'
      Origin = 'entidades.Simples'
      Required = True
    end
    object QrEntidadesAtividade: TWideStringField
      FieldName = 'Atividade'
      Origin = 'entidades.Atividade'
      Size = 50
    end
    object QrEntidadesEstCivil: TSmallintField
      FieldName = 'EstCivil'
      Origin = 'entidades.EstCivil'
      Required = True
    end
    object QrEntidadesNOMEECIVIL: TWideStringField
      FieldName = 'NOMEECIVIL'
      Origin = 'listaecivil.Nome'
      Required = True
      Size = 10
    end
    object QrEntidadesCPF_Conjuge: TWideStringField
      FieldName = 'CPF_Conjuge'
      Origin = 'entidades.CPF_Conjuge'
      Size = 18
    end
    object QrEntidadesCBE: TIntegerField
      FieldName = 'CBE'
      Origin = 'entidades.CBE'
    end
    object QrEntidadesSCB: TIntegerField
      FieldName = 'SCB'
      Origin = 'entidades.SCB'
    end
    object QrEntidadesCPF_Resp1: TWideStringField
      FieldName = 'CPF_Resp1'
      Origin = 'entidades.CPF_Resp1'
      Size = 18
    end
    object QrEntidadesCPF_Resp1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_Resp1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesENumero: TIntegerField
      FieldName = 'ENumero'
      Origin = 'entidades.ENumero'
    end
    object QrEntidadesPNumero: TIntegerField
      FieldName = 'PNumero'
      Origin = 'entidades.PNumero'
    end
    object QrEntidadesCNumero: TIntegerField
      FieldName = 'CNumero'
      Origin = 'entidades.CNumero'
    end
    object QrEntidadesLNumero: TIntegerField
      FieldName = 'LNumero'
      Origin = 'entidades.LNumero'
    end
    object QrEntidadesBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'entidades.Banco'
      DisplayFormat = '000'
    end
    object QrEntidadesAgencia: TWideStringField
      DisplayWidth = 4
      FieldName = 'Agencia'
      Origin = 'entidades.Agencia'
      Size = 11
    end
    object QrEntidadesContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Origin = 'entidades.ContaCorrente'
      Size = 15
    end
    object QrEntidadesCartPref: TIntegerField
      FieldName = 'CartPref'
      Origin = 'entidades.CartPref'
      Required = True
    end
    object QrEntidadesNOMECARTPREF: TWideStringField
      FieldName = 'NOMECARTPREF'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrEntidadesRolComis: TIntegerField
      FieldName = 'RolComis'
      Origin = 'entidades.RolComis'
      Required = True
    end
    object QrEntidadesFilial: TIntegerField
      FieldName = 'Filial'
      Origin = 'entidades.Filial'
      Required = True
    end
    object QrEntidadesIEST: TWideStringField
      FieldName = 'IEST'
      Origin = 'entidades.IEST'
    end
    object QrEntidadesQuantN3: TFloatField
      FieldName = 'QuantN3'
      Origin = 'entidades.QuantN3'
    end
    object QrEntidadesTempA: TFloatField
      FieldName = 'TempA'
      Origin = 'entidades.TempA'
    end
    object QrEntidadesTempD: TFloatField
      FieldName = 'TempD'
      Origin = 'entidades.TempD'
    end
    object QrEntidadesPAtividad: TIntegerField
      FieldName = 'PAtividad'
      Origin = 'entidades.PAtividad'
    end
    object QrEntidadesEAtividad: TIntegerField
      FieldName = 'EAtividad'
      Origin = 'entidades.EAtividad'
    end
    object QrEntidadesPCidadeCod: TIntegerField
      FieldName = 'PCidadeCod'
      Origin = 'entidades.PCidadeCod'
    end
    object QrEntidadesECidadeCod: TIntegerField
      FieldName = 'ECidadeCod'
      Origin = 'entidades.ECidadeCod'
    end
    object QrEntidadesPPaisCod: TIntegerField
      FieldName = 'PPaisCod'
      Origin = 'entidades.PPaisCod'
    end
    object QrEntidadesEPaisCod: TIntegerField
      FieldName = 'EPaisCod'
      Origin = 'entidades.EPaisCod'
    end
    object QrEntidadesAntigo: TWideStringField
      FieldName = 'Antigo'
      Origin = 'entidades.Antigo'
    end
    object QrEntidadesCUF2: TWideStringField
      FieldName = 'CUF2'
      Origin = 'entidades.CUF2'
      Size = 2
    end
    object QrEntidadesContab: TWideStringField
      FieldName = 'Contab'
      Origin = 'entidades.Contab'
    end
    object QrEntidadesMSN1: TWideStringField
      FieldName = 'MSN1'
      Origin = 'entidades.MSN1'
      Size = 255
    end
    object QrEntidadesPastaTxtFTP: TWideStringField
      FieldName = 'PastaTxtFTP'
      Origin = 'entidades.PastaTxtFTP'
      Size = 8
    end
    object QrEntidadesPastaPwdFTP: TWideStringField
      FieldName = 'PastaPwdFTP'
      Origin = 'entidades.PastaPwdFTP'
      Size = 100
    end
    object QrEntidadesProtestar: TSmallintField
      FieldName = 'Protestar'
      Origin = 'entidades.Protestar'
      Required = True
    end
    object QrEntidadesMultaCodi: TSmallintField
      FieldName = 'MultaCodi'
      Origin = 'entidades.MultaCodi'
      Required = True
    end
    object QrEntidadesMultaDias: TSmallintField
      FieldName = 'MultaDias'
      Origin = 'entidades.MultaDias'
      Required = True
    end
    object QrEntidadesMultaValr: TFloatField
      FieldName = 'MultaValr'
      Origin = 'entidades.MultaValr'
      Required = True
    end
    object QrEntidadesMultaPerc: TFloatField
      FieldName = 'MultaPerc'
      Origin = 'entidades.MultaPerc'
      Required = True
    end
    object QrEntidadesMultaTiVe: TSmallintField
      FieldName = 'MultaTiVe'
      Origin = 'entidades.MultaTiVe'
      Required = True
    end
    object QrEntidadesJuroSacado: TFloatField
      FieldName = 'JuroSacado'
      Origin = 'entidades.JuroSacado'
      Required = True
    end
    object QrEntidadesCPMF: TFloatField
      FieldName = 'CPMF'
      Origin = 'entidades.CPMF'
      Required = True
    end
    object QrEntidadesCorrido: TIntegerField
      FieldName = 'Corrido'
      Origin = 'entidades.Corrido'
      Required = True
    end
    object QrEntidadesCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'entidades.CliInt'
      Required = True
    end
    object QrEntidadesAltDtPlaCt: TDateField
      FieldName = 'AltDtPlaCt'
      Origin = 'entidades.AltDtPlaCt'
      Required = True
    end
    object QrEntidadesAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'entidades.AlterWeb'
      Required = True
    end
    object QrEntidadesAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'entidades.Ativo'
      Required = True
    end
    object QrEntidadesEEndeRef: TWideStringField
      FieldName = 'EEndeRef'
      Origin = 'entidades.EEndeRef'
      Size = 100
    end
    object QrEntidadesPEndeRef: TWideStringField
      FieldName = 'PEndeRef'
      Origin = 'entidades.PEndeRef'
      Size = 100
    end
    object QrEntidadesCEndeRef: TWideStringField
      FieldName = 'CEndeRef'
      Origin = 'entidades.CEndeRef'
      Size = 100
    end
    object QrEntidadesLEndeRef: TWideStringField
      FieldName = 'LEndeRef'
      Origin = 'entidades.LEndeRef'
      Size = 100
    end
    object QrEntidadesPNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PNUMERO_TXT'
      Size = 11
      Calculated = True
    end
    object QrEntidadesENUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENUMERO_TXT'
      Size = 11
      Calculated = True
    end
    object QrEntidadesCNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNUMERO_TXT'
      Size = 11
      Calculated = True
    end
    object QrEntidadesLNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LNUMERO_TXT'
      Size = 11
      Calculated = True
    end
    object QrEntidadesIE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IE_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesDATARG_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATARG_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntidadesECodMunici: TIntegerField
      FieldName = 'ECodMunici'
      Origin = 'entidades.ECodMunici'
    end
    object QrEntidadesPCodMunici: TIntegerField
      FieldName = 'PCodMunici'
      Origin = 'entidades.PCodMunici'
    end
    object QrEntidadesCCodMunici: TIntegerField
      FieldName = 'CCodMunici'
      Origin = 'entidades.CCodMunici'
    end
    object QrEntidadesLCodMunici: TIntegerField
      FieldName = 'LCodMunici'
      Origin = 'entidades.LCodMunici'
    end
    object QrEntidadesCNAE: TWideStringField
      FieldName = 'CNAE'
      Origin = 'entidades.CNAE'
      Size = 7
    end
    object QrEntidadesSUFRAMA: TWideStringField
      FieldName = 'SUFRAMA'
      Origin = 'entidades.SUFRAMA'
      Size = 9
    end
    object QrEntidadesECodiPais: TIntegerField
      FieldName = 'ECodiPais'
      Origin = 'entidades.ECodiPais'
    end
    object QrEntidadesPCodiPais: TIntegerField
      FieldName = 'PCodiPais'
      Origin = 'entidades.PCodiPais'
    end
    object QrEntidadesL_CNPJ: TWideStringField
      FieldName = 'L_CNPJ'
      Origin = 'entidades.L_CNPJ'
      Size = 14
    end
    object QrEntidadesL_Ativo: TSmallintField
      FieldName = 'L_Ativo'
      Origin = 'entidades.L_Ativo'
    end
    object QrEntidadesL_CNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'L_CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesNO_DTB_EMUNICI: TWideStringField
      FieldName = 'NO_DTB_EMUNICI'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrEntidadesNO_DTB_PMUNICI: TWideStringField
      FieldName = 'NO_DTB_PMUNICI'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrEntidadesNO_DTB_CMUNICI: TWideStringField
      FieldName = 'NO_DTB_CMUNICI'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrEntidadesNO_DTB_LMUNICI: TWideStringField
      FieldName = 'NO_DTB_LMUNICI'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrEntidadesNO_BACEN_EPAIS: TWideStringField
      FieldName = 'NO_BACEN_EPAIS'
      Origin = 'bacen_pais.Nome'
      Size = 100
    end
    object QrEntidadesNO_BACEN_PPAIS: TWideStringField
      FieldName = 'NO_BACEN_PPAIS'
      Origin = 'bacen_pais.Nome'
      Size = 100
    end
    object QrEntidadesCNAE_Nome: TWideStringField
      FieldName = 'CNAE_Nome'
      Size = 255
    end
    object QrEntidadesNIRE: TWideStringField
      FieldName = 'NIRE'
      Origin = 'entidades.NIRE'
      Size = 15
    end
    object QrEntidadesIEST_TXT: TWideStringField
      DisplayWidth = 30
      FieldKind = fkCalculated
      FieldName = 'IEST_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntidadesURL: TWideStringField
      FieldName = 'URL'
      Origin = 'entidades.URL'
      Size = 60
    end
    object QrEntidadesCRT: TSmallintField
      FieldName = 'CRT'
    end
    object QrEntidadesCOD_PART: TWideStringField
      FieldName = 'COD_PART'
      Size = 60
    end
    object QrEntidadesCOMPLENT: TWideStringField
      FieldName = 'COMPLENT'
      Size = 60
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 460
    Top = 508
  end
  object DsEntDefAtr: TDataSource
    DataSet = QrEntDefAtr
    Left = 600
    Top = 536
  end
  object QrEntDefAtr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT eda.EntAtrCad, eda.EntAtrIts,'
      'eda.Controle CTRL_ATR, eai.Controle CTRL_ITS,'
      'eai.CodUsu CODUSU_ITS, eai.Nome NOME_ITS,'
      'eac.CodUsu CODUSU_CAD, eac.Nome NOME_CAD'
      'FROM entdefatr eda'
      'LEFT JOIN entatrits eai ON eai.Controle=eda.EntAtrIts'
      'LEFT JOIN entatrcad eac ON eac.Codigo=eai.Codigo'
      'WHERE eda.Entidade=:P0'#13
      '/*'
      #10
      'AND eac.Nome LIKE :P1'
      'AND eai.Nome LIKE :P2'
      '*/')
    Left = 572
    Top = 536
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntDefAtrEntAtrCad: TIntegerField
      FieldName = 'EntAtrCad'
      Required = True
    end
    object QrEntDefAtrEntAtrIts: TIntegerField
      FieldName = 'EntAtrIts'
      Required = True
    end
    object QrEntDefAtrCTRL_ATR: TIntegerField
      FieldName = 'CTRL_ATR'
      Required = True
    end
    object QrEntDefAtrCTRL_ITS: TIntegerField
      FieldName = 'CTRL_ITS'
      Required = True
    end
    object QrEntDefAtrCODUSU_ITS: TIntegerField
      FieldName = 'CODUSU_ITS'
    end
    object QrEntDefAtrNOME_ITS: TWideStringField
      FieldName = 'NOME_ITS'
      Size = 50
    end
    object QrEntDefAtrCODUSU_CAD: TIntegerField
      FieldName = 'CODUSU_CAD'
      Required = True
    end
    object QrEntDefAtrNOME_CAD: TWideStringField
      FieldName = 'NOME_CAD'
      Size = 30
    end
  end
  object DsEntiTel: TDataSource
    DataSet = QrEntiTel
    Left = 544
    Top = 536
  end
  object QrEntiTel: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntiTelCalcFields
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, emt.Conta, '
      'emt.Telefone, emt.EntiTipCto, emt.Ramal'
      'FROM entitel emt'
      'LEFT JOIN entitipcto etc ON etc.Codigo=emt.EntiTipCto'#13
      'WHERE emt.Controle=:P0')
    Left = 516
    Top = 536
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiTelNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Origin = 'entitipcto.Nome'
      Size = 30
    end
    object QrEntiTelConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'entitel.Conta'
      Required = True
    end
    object QrEntiTelTelefone: TWideStringField
      FieldName = 'Telefone'
      Origin = 'entitel.Telefone'
    end
    object QrEntiTelEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Origin = 'entitel.EntiTipCto'
      Required = True
    end
    object QrEntiTelTEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntiTelRamal: TWideStringField
      FieldName = 'Ramal'
      Origin = 'entitel.Ramal'
    end
  end
  object DsEntiMail: TDataSource
    DataSet = QrEntiMail
    Left = 488
    Top = 536
  end
  object QrEntiMail: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT etc.Nome NOMEETC, ema.Conta, '
      'ema.EMail, ema.EntiTipCto, ema.Ordem'
      'FROM entimail ema'
      'LEFT JOIN entitipcto etc ON etc.Codigo=ema.EntiTipCto'#13
      'WHERE ema.Controle=:P0'
      'ORDER BY ema.Ordem, ema.Conta'#10)
    Left = 460
    Top = 536
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiMailNOMEETC: TWideStringField
      FieldName = 'NOMEETC'
      Size = 30
    end
    object QrEntiMailConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEntiMailEMail: TWideStringField
      FieldName = 'EMail'
      Required = True
      Size = 255
    end
    object QrEntiMailEntiTipCto: TIntegerField
      FieldName = 'EntiTipCto'
      Required = True
    end
    object QrEntiMailOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsEntiRespon: TDataSource
    DataSet = QrEntiRespon
    Left = 432
    Top = 536
  end
  object QrEntiRespon: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEntiResponCalcFields
    SQL.Strings = (
      'SELECT ere.*, eca.Nome NOME_CARGO ,'
      'ELT(ere.Assina+1, "N'#227'o", "Sim", "???") NO_ASSINA'
      'FROM entirespon ere'
      'LEFT JOIN enticargos eca ON eca.Codigo=ere.Cargo'
      'WHERE ere.Codigo=:P0'
      ''
      ''
      ''
      '')
    Left = 404
    Top = 536
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiResponCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiResponControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntiResponNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrEntiResponCargo: TIntegerField
      FieldName = 'Cargo'
      Required = True
    end
    object QrEntiResponAssina: TSmallintField
      FieldName = 'Assina'
      Required = True
    end
    object QrEntiResponOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Required = True
    end
    object QrEntiResponObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrEntiResponNOME_CARGO: TWideStringField
      FieldName = 'NOME_CARGO'
      Size = 30
    end
    object QrEntiResponNO_ASSINA: TWideStringField
      FieldName = 'NO_ASSINA'
      Size = 3
    end
    object QrEntiResponMandatoIni: TDateField
      FieldName = 'MandatoIni'
    end
    object QrEntiResponMandatoFim: TDateField
      FieldName = 'MandatoFim'
    end
    object QrEntiResponMandatoIni_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MandatoIni_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntiResponMandatoFim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MandatoFim_TXT'
      Size = 10
      Calculated = True
    end
  end
  object DsEntiContat: TDataSource
    DataSet = QrEntiContat
    Left = 376
    Top = 536
  end
  object QrEntiContat: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrEntiContatBeforeClose
    AfterScroll = QrEntiContatAfterScroll
    SQL.Strings = (
      'SELECT eco.Controle, eco.Nome, eco.Cargo,'
      'eca.Nome NOME_CARGO '
      'FROM enticontat eco'
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo'
      'WHERE eco.Codigo=:P0'#10
      '')
    Left = 348
    Top = 536
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiContatControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntiContatNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrEntiContatCargo: TIntegerField
      FieldName = 'Cargo'
      Required = True
    end
    object QrEntiContatNOME_CARGO: TWideStringField
      FieldName = 'NOME_CARGO'
      Size = 30
    end
  end
  object QrEntiCtas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cta.*, ban.Nome NOMEBANCO'
      'FROM entictas cta'
      'LEFT JOIN entidades ent ON ent.Codigo=cta.Codigo'
      'LEFT JOIN bancos ban ON ban.Codigo=cta.Banco'
      'WHERE cta.Codigo=:P0'
      'ORDER BY cta.Ordem')
    Left = 628
    Top = 536
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiCtasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiCtasControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntiCtasOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrEntiCtasBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrEntiCtasAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrEntiCtasContaCor: TWideStringField
      FieldName = 'ContaCor'
      Size = 10
    end
    object QrEntiCtasContaTip: TWideStringField
      FieldName = 'ContaTip'
      Size = 10
    end
    object QrEntiCtasDAC_A: TWideStringField
      FieldName = 'DAC_A'
      Size = 1
    end
    object QrEntiCtasDAC_C: TWideStringField
      FieldName = 'DAC_C'
      Size = 1
    end
    object QrEntiCtasDAC_AC: TWideStringField
      FieldName = 'DAC_AC'
      Size = 1
    end
    object QrEntiCtasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEntiCtasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEntiCtasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEntiCtasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEntiCtasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEntiCtasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEntiCtasAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEntiCtasNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
  end
  object DsEntiCtas: TDataSource
    DataSet = QrEntiCtas
    Left = 656
    Top = 536
  end
  object QrSiapTerCad: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSiapTerCadAfterOpen
    BeforeClose = QrSiapTerCadBeforeClose
    AfterScroll = QrSiapTerCadAfterScroll
    SQL.Strings = (
      'SELECT llo.Nome NOMELOGRAD, gcp.Nome NO_LSTCUSPRD, stc.*'
      'FROM siaptercad stc'
      'LEFT JOIN listalograd llo ON llo.Codigo=stc.SLograd'
      'LEFT JOIN gracusprc gcp ON gcp.Codigo=stc.LstCusPrd'
      'WHERE stc.Cliente=:P0'
      '')
    Left = 348
    Top = 564
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSiapTerCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapTerCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSiapTerCadCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSiapTerCadSLograd: TSmallintField
      FieldName = 'SLograd'
    end
    object QrSiapTerCadSRua: TWideStringField
      FieldName = 'SRua'
      Size = 60
    end
    object QrSiapTerCadSNumero: TIntegerField
      FieldName = 'SNumero'
    end
    object QrSiapTerCadSCompl: TWideStringField
      FieldName = 'SCompl'
      Size = 60
    end
    object QrSiapTerCadSBairro: TWideStringField
      FieldName = 'SBairro'
      Size = 60
    end
    object QrSiapTerCadSCidade: TWideStringField
      FieldName = 'SCidade'
      Size = 60
    end
    object QrSiapTerCadSUF: TWideStringField
      FieldName = 'SUF'
      Size = 3
    end
    object QrSiapTerCadSCEP: TIntegerField
      FieldName = 'SCEP'
    end
    object QrSiapTerCadSPais: TWideStringField
      FieldName = 'SPais'
      Size = 60
    end
    object QrSiapTerCadSEndeRef: TWideStringField
      FieldName = 'SEndeRef'
      Size = 100
    end
    object QrSiapTerCadSCodMunici: TIntegerField
      FieldName = 'SCodMunici'
    end
    object QrSiapTerCadSCodiPais: TIntegerField
      FieldName = 'SCodiPais'
    end
    object QrSiapTerCadM2Constru: TFloatField
      FieldName = 'M2Constru'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSiapTerCadM2NaoBuild: TFloatField
      FieldName = 'M2NaoBuild'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSiapTerCadM2Terreno: TFloatField
      FieldName = 'M2Terreno'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSiapTerCadM2Total: TFloatField
      FieldName = 'M2Total'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSiapTerCadNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrSiapTerCadSTe1: TWideStringField
      FieldName = 'STe1'
    end
    object QrSiapTerCadNO_LSTCUSPRD: TWideStringField
      FieldName = 'NO_LSTCUSPRD'
      Size = 50
    end
    object QrSiapTerCadLstCusPrd: TIntegerField
      FieldName = 'LstCusPrd'
    end
    object QrSiapTerCadPdrMntsMon: TIntegerField
      FieldName = 'PdrMntsMon'
    end
    object QrSiapTerCadLatitude: TFloatField
      FieldName = 'Latitude'
    end
    object QrSiapTerCadLongitude: TFloatField
      FieldName = 'Longitude'
    end
    object QrSiapTerCadRotaLatLon: TIntegerField
      FieldName = 'RotaLatLon'
    end
  end
  object DsSiapTerCad: TDataSource
    DataSet = QrSiapTerCad
    Left = 376
    Top = 564
  end
  object QrSiapImaCad: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSiapImaCadAfterOpen
    BeforeClose = QrSiapImaCadBeforeClose
    AfterClose = QrSiapImaCadAfterClose
    AfterScroll = QrSiapImaCadAfterScroll
    SQL.Strings = (
      'SELECT obj.Nome NO_OBJETO, fin.Nome NO_FINALID, '
      'tpc.Nome NO_TPCONSTRU, sic.* '
      'FROM siapimacad sic'
      'LEFT JOIN objetos obj ON obj.Codigo=sic.Objeto'
      'LEFT JOIN finalidads fin ON fin.Codigo=sic.Finalidade'
      'LEFT JOIN tpconstrus tpc ON tpc.Codigo=sic.TpConstru'
      'WHERE sic.SiapImaTer=:P0')
    Left = 404
    Top = 564
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSiapImaCadTpConstru: TIntegerField
      FieldName = 'TpConstru'
    end
    object QrSiapImaCadSCompl2: TWideStringField
      FieldName = 'SCompl2'
      Size = 60
    end
    object QrSiapImaCadNO_OBJETO: TWideStringField
      FieldName = 'NO_OBJETO'
      Size = 60
    end
    object QrSiapImaCadNO_FINALID: TWideStringField
      FieldName = 'NO_FINALID'
      Size = 60
    end
    object QrSiapImaCadNO_TPCONSTRU: TWideStringField
      FieldName = 'NO_TPCONSTRU'
      Size = 60
    end
    object QrSiapImaCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapImaCadSiapImaTer: TIntegerField
      FieldName = 'SiapImaTer'
    end
    object QrSiapImaCadObjeto: TIntegerField
      FieldName = 'Objeto'
    end
    object QrSiapImaCadFinalidade: TIntegerField
      FieldName = 'Finalidade'
    end
    object QrSiapImaCadM2Constru: TFloatField
      FieldName = 'M2Constru'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSiapImaCadM2NaoBuild: TFloatField
      FieldName = 'M2NaoBuild'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSiapImaCadM2Terreno: TFloatField
      FieldName = 'M2Terreno'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrSiapImaCadM2Total: TFloatField
      FieldName = 'M2Total'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsSiapImaCad: TDataSource
    DataSet = QrSiapImaCad
    Left = 432
    Top = 564
  end
  object QrSiapImaDep: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSiapImaDepAfterOpen
    BeforeClose = QrSiapImaDepBeforeClose
    SQL.Strings = (
      'SELECT dep.Nome NO_DEPENDENCI, sid.* '
      'FROM siapimadep sid'
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci'
      'WHERE sid.Codigo=:P0'
      'ORDER BY NO_DEPENDENCI'
      '')
    Left = 460
    Top = 564
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSiapImaDepNO_DEPENDENCI: TWideStringField
      FieldName = 'NO_DEPENDENCI'
      Size = 60
    end
    object QrSiapImaDepCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapImaDepControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSiapImaDepDependenci: TIntegerField
      FieldName = 'Dependenci'
    end
    object QrSiapImaDepMLarg: TFloatField
      FieldName = 'MLarg'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSiapImaDepMComp: TFloatField
      FieldName = 'MComp'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrSiapImaDepMAltu: TFloatField
      FieldName = 'MAltu'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
  end
  object DsSiapImaDep: TDataSource
    DataSet = QrSiapImaDep
    Left = 488
    Top = 564
  end
  object QrSiapImaCdi: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Nome NO_CARACTERIS, cdi.* '
      'FROM siapimacdi cdi'
      'LEFT JOIN Caracteris car ON car.Codigo=cdi.Caracteris'
      'WHERE cdi.Codigo=:P0'
      'ORDER BY NO_CARACTERIS')
    Left = 516
    Top = 564
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSiapImaCdiNO_CARACTERIS: TWideStringField
      FieldName = 'NO_CARACTERIS'
      Size = 60
    end
    object QrSiapImaCdiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapImaCdiControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSiapImaCdiCaracteris: TIntegerField
      FieldName = 'Caracteris'
    end
  end
  object DsSiapImaCdi: TDataSource
    DataSet = QrSiapImaCdi
    Left = 544
    Top = 564
  end
  object QrSiapImaCav: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Nome NO_CARACTERIS, cav.* '
      'FROM siapimacav cav'
      'LEFT JOIN Caracteris car ON car.Codigo=cav.Caracteris'
      'WHERE cav.Codigo=:P0'
      'ORDER BY NO_CARACTERIS')
    Left = 572
    Top = 564
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSiapImaCavNO_CARACTERIS: TWideStringField
      FieldName = 'NO_CARACTERIS'
      Size = 60
    end
    object QrSiapImaCavCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapImaCavControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSiapImaCavCaracteris: TIntegerField
      FieldName = 'Caracteris'
    end
  end
  object DsSiapImaCav: TDataSource
    DataSet = QrSiapImaCav
    Left = 600
    Top = 564
  end
  object QrSiapImaRes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT res.Nome NO_RESIDENTE, '
      'cui.Nome NO_CUIDADO, sir.*'
      'FROM siapimares sir'
      'LEFT JOIN residentes res ON res.Codigo=sir.Residente'
      'LEFT JOIN cuidados cui ON cui.Codigo=sir.Cuidado'
      'WHERE sir.Codigo=:P0')
    Left = 628
    Top = 564
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSiapImaResNO_RESIDENTE: TWideStringField
      FieldName = 'NO_RESIDENTE'
      Size = 60
    end
    object QrSiapImaResNO_CUIDADO: TWideStringField
      FieldName = 'NO_CUIDADO'
      Size = 60
    end
    object QrSiapImaResCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapImaResControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSiapImaResResidente: TIntegerField
      FieldName = 'Residente'
    end
    object QrSiapImaResNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrSiapImaResAnoNatal: TIntegerField
      FieldName = 'AnoNatal'
    end
    object QrSiapImaResCuidado: TIntegerField
      FieldName = 'Cuidado'
    end
    object QrSiapImaResObservacao: TWideStringField
      FieldName = 'Observacao'
      Size = 255
    end
  end
  object DsSiapImaRes: TDataSource
    DataSet = QrSiapImaRes
    Left = 656
    Top = 564
  end
  object QrSiapImaCui: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cui.Nome NO_CUIDADO, sic.*'
      'FROM siapimacui sic'
      'LEFT JOIN cuidados cui ON '
      '  cui.Codigo=sic.Cuidado'
      'WHERE sic.Codigo=:P0')
    Left = 348
    Top = 592
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSiapImaCuiNO_CUIDADO: TWideStringField
      FieldName = 'NO_CUIDADO'
      Size = 60
    end
    object QrSiapImaCuiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapImaCuiControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSiapImaCuiCuidado: TIntegerField
      FieldName = 'Cuidado'
    end
  end
  object DsSiapImaCui: TDataSource
    DataSet = QrSiapImaCui
    Left = 376
    Top = 592
  end
  object QrSiapImaAti: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ati.Nome NO_ATIVIDADE, sia.*'
      'FROM siapimaati sia'
      'LEFT JOIN atividades ati ON '
      '  ati.Codigo=sia.Atividade'
      'WHERE sia.Codigo=:P0')
    Left = 404
    Top = 592
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSiapImaAtiNO_ATIVIDADE: TWideStringField
      FieldName = 'NO_ATIVIDADE'
      Size = 60
    end
    object QrSiapImaAtiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapImaAtiControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSiapImaAtiAtividade: TIntegerField
      FieldName = 'Atividade'
    end
  end
  object DsSiapImaAti: TDataSource
    DataSet = QrSiapImaAti
    Left = 432
    Top = 592
  end
  object QrSiapImaCxa: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSiapImaCxaAfterOpen
    BeforeClose = QrSiapImaCxaBeforeClose
    AfterScroll = QrSiapImaCxaAfterScroll
    SQL.Strings = (
      'SELECT cxm.Nome NO_MATERIAL, '
      'cxf.Nome NOME_FORMA, six.*'
      'FROM siapimacxa six'
      'LEFT JOIN cxamaters cxm ON '
      '  cxm.Codigo=six.MatersCxa'
      'LEFT JOIN cxaformas cxf ON '
      '  cxf.Codigo=six.FormasCxa'
      'WHERE six.Codigo=:P0')
    Left = 460
    Top = 592
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSiapImaCxaNO_MATERIAL: TWideStringField
      FieldName = 'NO_MATERIAL'
      Size = 60
    end
    object QrSiapImaCxaNOME_FORMA: TWideStringField
      FieldName = 'NOME_FORMA'
      Size = 60
    end
    object QrSiapImaCxaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapImaCxaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSiapImaCxaMatersCxa: TIntegerField
      FieldName = 'MatersCxa'
    end
    object QrSiapImaCxaFormasCxa: TIntegerField
      FieldName = 'FormasCxa'
    end
    object QrSiapImaCxaVolumeL: TFloatField
      FieldName = 'VolumeL'
    end
    object QrSiapImaCxaLocal: TWideStringField
      FieldName = 'Local'
      Size = 255
    end
    object QrSiapImaCxaAcesso: TWideStringField
      FieldName = 'Acesso'
      Size = 255
    end
    object QrSiapImaCxaMedidas: TWideStringField
      FieldName = 'Medidas'
      Size = 255
    end
  end
  object DsSiapImaCxa: TDataSource
    DataSet = QrSiapImaCxa
    Left = 488
    Top = 592
  end
  object PMCunsIts: TPopupMenu
    OnPopup = PMCunsItsPopup
    Left = 520
    Top = 300
    object AdicionaContratante1: TMenuItem
      Caption = '&Adicionar um Contratante'
      OnClick = AdicionaContratante1Click
    end
    object RetiraContratante1: TMenuItem
      Caption = '&Retirar o contratante selecionado'
      OnClick = RetiraContratante1Click
    end
  end
  object PMSiapTerCad: TPopupMenu
    OnPopup = PMSiapTerCadPopup
    Left = 548
    Top = 300
    object IncluiTerCad1: TMenuItem
      Caption = '&Inclui novo im'#243'vel'
      OnClick = IncluiTerCad1Click
    end
    object AlteraTerCad1: TMenuItem
      Caption = '&Altera o im'#243'vel selcionado'
      OnClick = AlteraTerCad1Click
    end
    object ExcluiterCad1: TMenuItem
      Caption = '&Exclui o im'#243'vel selecionado'
      OnClick = ExcluiterCad1Click
    end
  end
  object PMSiapImaCad: TPopupMenu
    OnPopup = PMSiapImaCadPopup
    Left = 604
    Top = 300
    object IncluiSiapImaCad1: TMenuItem
      Caption = '&Inclui novo local de aplica'#231#227'o'
      OnClick = IncluiSiapImaCad1Click
    end
    object AlteraSiapImaCad1: TMenuItem
      Caption = '&Altera o local de aplica'#231#227'o selecionado'
      OnClick = AlteraSiapImaCad1Click
    end
    object ExcluiSiapImaCad1: TMenuItem
      Caption = '&Exclui o local de aplica'#231#227'o selecionado'
      OnClick = ExcluiSiapImaCad1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Duplicaritem1: TMenuItem
      Caption = 'Duplicar (copiar) local selecionado'
      OnClick = Duplicaritem1Click
    end
  end
  object PMSiapImaDep: TPopupMenu
    OnPopup = PMSiapImaDepPopup
    Left = 632
    Top = 300
    object IncluiSiapImaDep1: TMenuItem
      Caption = '&Inclui nova depend'#234'ncia'
      OnClick = IncluiSiapImaDep1Click
    end
    object AlteraSiapImaDep1: TMenuItem
      Caption = '&Altera a depend'#234'ncia selecionada'
      OnClick = AlteraSiapImaDep1Click
    end
    object ExcluiSiapImaDep1: TMenuItem
      Caption = '&Exclui a depend'#234'ncia selecionada'
      OnClick = ExcluiSiapImaDep1Click
    end
  end
  object PMSiapImaCdi: TPopupMenu
    OnPopup = PMSiapImaCdiPopup
    Left = 660
    Top = 300
    object IncluiSiapImaCdi1: TMenuItem
      Caption = '&Inclui nova caracter'#237'stica'
      OnClick = IncluiSiapImaCdi1Click
    end
    object AlteraSiapImaCdi1: TMenuItem
      Caption = 'Altera a caracter'#237'stica selecionada'
      OnClick = AlteraSiapImaCdi1Click
    end
    object ExcluiSiapImaCdi1: TMenuItem
      Caption = '&Exclui a caracter'#237'stica selecionada'
      OnClick = ExcluiSiapImaCdi1Click
    end
  end
  object PMSiapImaCav: TPopupMenu
    OnPopup = PMSiapImaCavPopup
    Left = 688
    Top = 300
    object IncluiSiapImaCav1: TMenuItem
      Caption = '&Inclui nova caracter'#237'stica de '#225'rea vicinal'
      OnClick = IncluiSiapImaCav1Click
    end
    object AlteraSiapImaCav1: TMenuItem
      Caption = '&Altera a caracter'#237'stica de '#225'rea vicinal selecionada'
      OnClick = AlteraSiapImaCav1Click
    end
    object ExcluiSiapImaCav1: TMenuItem
      Caption = '&Exclui a caracter'#237'stica de '#225'rea vicinal selecionada'
      OnClick = ExcluiSiapImaCav1Click
    end
  end
  object PMSiapImaRes: TPopupMenu
    OnPopup = PMSiapImaResPopup
    Left = 716
    Top = 300
    object IncluiSiapImaRes1: TMenuItem
      Caption = 'Inclui novo residente'
      OnClick = IncluiSiapImaRes1Click
    end
    object AlteraSiapImaRes1: TMenuItem
      Caption = 'Altera residente selecionado'
      OnClick = AlteraSiapImaRes1Click
    end
    object ExcluiSiapImaRes1: TMenuItem
      Caption = 'Exclui residente selecionado'
      OnClick = ExcluiSiapImaRes1Click
    end
  end
  object PMSiapImaCui: TPopupMenu
    OnPopup = PMSiapImaCuiPopup
    Left = 744
    Top = 300
    object IncluiSiapImaCui1: TMenuItem
      Caption = '&Inclui novo cuidado'
      OnClick = IncluiSiapImaCui1Click
    end
    object AlteraSiapImaCui1: TMenuItem
      Caption = '&Altera cuidado selecionado'
      OnClick = AlteraSiapImaCui1Click
    end
    object ExcluiSiapImaCui1: TMenuItem
      Caption = '&Exclui cuidado selecionado'
      OnClick = ExcluiSiapImaCui1Click
    end
  end
  object PMSiapImaAti: TPopupMenu
    OnPopup = PMSiapImaAtiPopup
    Left = 772
    Top = 300
    object IncluiSiapImaAti1: TMenuItem
      Caption = '&Inclui nova atividade'
      OnClick = IncluiSiapImaAti1Click
    end
    object AlteraSiapImaAti1: TMenuItem
      Caption = '&Altera a atividade selecionada'
      OnClick = AlteraSiapImaAti1Click
    end
    object ExcluiSiapImaAti1: TMenuItem
      Caption = '&Exclui a atividade selecionada'
      OnClick = ExcluiSiapImaAti1Click
    end
  end
  object PMSiapImaCxa: TPopupMenu
    OnPopup = PMSiapImaCxaPopup
    Left = 800
    Top = 300
    object IncluiSiapImaCxa1: TMenuItem
      Caption = '&Inclui nova caixa d`'#225'gua'
      OnClick = IncluiSiapImaCxa1Click
    end
    object AlteraSiapImaCxa1: TMenuItem
      Caption = '&Altera a caixa d`'#225'gua selecionada'
      OnClick = AlteraSiapImaCxa1Click
    end
    object ExcluiSiapImaCxa1: TMenuItem
      Caption = '&Exclui a caixa d`'#225'gua selecionada'
      OnClick = ExcluiSiapImaCxa1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object DuplicSiapImaCxa1: TMenuItem
      Caption = '&Duplicar este cadastro'
      OnClick = DuplicSiapImaCxa1Click
    end
  end
  object QrAtrSICxDef: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts,'
      'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,'
      'cad.Nome NO_CAD, its.Nome NO_ITS'
      'FROM atrsicxdef def'
      'LEFT JOIN atrsicxits its ON its.Controle=def.AtrIts'
      'LEFT JOIN atrsicxcad cad ON cad.Codigo=def.AtrCad'
      'WHERE def.ID_Sorc=:P0'
      'ORDER BY NO_CAD, NO_ITS')
    Left = 516
    Top = 592
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAtrSICxDefID_Item: TIntegerField
      FieldName = 'ID_Item'
    end
    object QrAtrSICxDefID_Sorc: TIntegerField
      FieldName = 'ID_Sorc'
    end
    object QrAtrSICxDefAtrCad: TIntegerField
      FieldName = 'AtrCad'
    end
    object QrAtrSICxDefAtrIts: TIntegerField
      FieldName = 'AtrIts'
    end
    object QrAtrSICxDefCU_CAD: TIntegerField
      FieldName = 'CU_CAD'
      Required = True
    end
    object QrAtrSICxDefCU_ITS: TIntegerField
      FieldName = 'CU_ITS'
      Required = True
    end
    object QrAtrSICxDefNO_CAD: TWideStringField
      FieldName = 'NO_CAD'
      Size = 30
    end
    object QrAtrSICxDefNO_ITS: TWideStringField
      FieldName = 'NO_ITS'
      Size = 50
    end
  end
  object DsAtrSICxDef: TDataSource
    DataSet = QrAtrSICxDef
    Left = 544
    Top = 592
  end
  object PMAtrSICxDef: TPopupMenu
    Left = 828
    Top = 300
    object IncluiAtrSICxDef1: TMenuItem
      Caption = '&Inclui um novo atributo'
      OnClick = IncluiAtrSICxDef1Click
    end
    object AlteraAtrSICxDef1: TMenuItem
      Caption = '&Altera o atributo selecionado'
      OnClick = AlteraAtrSICxDef1Click
    end
    object ExcluiAtrSICxDef1: TMenuItem
      Caption = '&Exclui o atributo selecionado'
      OnClick = ExcluiAtrSICxDef1Click
    end
  end
  object QrAtrSICdDef: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts,'
      'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,'
      'cad.Nome NO_CAD, its.Nome NO_ITS'
      'FROM atrsicddef def'
      'LEFT JOIN atrsicdits its ON its.Controle=def.AtrIts'
      'LEFT JOIN atrsicdcad cad ON cad.Codigo=def.AtrCad'
      'WHERE def.ID_Sorc=:P0'
      'ORDER BY NO_CAD, NO_ITS')
    Left = 572
    Top = 592
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAtrSICdDefID_Item: TIntegerField
      FieldName = 'ID_Item'
    end
    object QrAtrSICdDefID_Sorc: TIntegerField
      FieldName = 'ID_Sorc'
    end
    object QrAtrSICdDefAtrCad: TIntegerField
      FieldName = 'AtrCad'
    end
    object QrAtrSICdDefAtrIts: TIntegerField
      FieldName = 'AtrIts'
    end
    object QrAtrSICdDefCU_CAD: TIntegerField
      FieldName = 'CU_CAD'
      Required = True
    end
    object QrAtrSICdDefCU_ITS: TIntegerField
      FieldName = 'CU_ITS'
    end
    object QrAtrSICdDefNO_CAD: TWideStringField
      FieldName = 'NO_CAD'
      Size = 30
    end
    object QrAtrSICdDefNO_ITS: TWideStringField
      FieldName = 'NO_ITS'
      Size = 50
    end
  end
  object DsAtrSICdDef: TDataSource
    DataSet = QrAtrSICdDef
    Left = 600
    Top = 592
  end
  object PMAtrSICdDef: TPopupMenu
    Left = 856
    Top = 300
    object IncluiAtrSICdDef1: TMenuItem
      Caption = '&Inclui um novo atributo'
      OnClick = IncluiAtrSICdDef1Click
    end
    object AlteraAtrSICdDef1: TMenuItem
      Caption = '&Altera o atributo selecionado'
      OnClick = AlteraAtrSICdDef1Click
    end
    object ExcluiAtrSICdDef1: TMenuItem
      Caption = '&Exclui o atributo selecionado'
      OnClick = ExcluiAtrSICdDef1Click
    end
  end
  object QrAtividades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM atividades'
      'ORDER BY Nome')
    Left = 328
    Top = 368
    object QrAtividadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAtividadesNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsAtividades: TDataSource
    DataSet = QrAtividades
    Left = 356
    Top = 368
  end
  object QrHowFounded: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM howfounded'
      'ORDER BY Nome')
    Left = 384
    Top = 368
    object QrHowFoundedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrHowFoundedNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsHowFounded: TDataSource
    DataSet = QrHowFounded
    Left = 412
    Top = 368
  end
  object QrAccounts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM entidades'
      'WHERE Nome <> '#39#39' '
      'ORDER BY Nome')
    Left = 440
    Top = 368
    object QrAccountsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAccountsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsAccounts: TDataSource
    DataSet = QrAccounts
    Left = 468
    Top = 368
  end
  object PMQuery: TPopupMenu
    Left = 256
    Top = 124
    object PeloNome1: TMenuItem
      Caption = '&Razao Social / Nome'
      OnClick = PeloNome1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CNPJ1: TMenuItem
      Caption = '&CNPJ'
      OnClick = CNPJ1Click
    end
    object Dados1: TMenuItem
      Caption = '&Dados'
      OnClick = Dados1Click
    end
  end
  object QrSubCli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_SUBCLI,'
      'IF(ent.Tipo=0, ent.Fantasia, ent.Apelido) FANTASIA,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) CNPJ_CPF, cug.*'
      'FROM cunsits cug'
      'LEFT JOIN entidades ent ON ent.Codigo=cug.CunsSub'
      'WHERE cug.Cunsgru =:P0'
      'ORDER BY NO_SUBCLI'
      '')
    Left = 544
    Top = 508
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSubCliNO_SUBCLI: TWideStringField
      FieldName = 'NO_SUBCLI'
      Size = 100
    end
    object QrSubCliCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrSubCliCunsGru: TIntegerField
      FieldName = 'CunsGru'
    end
    object QrSubCliCunsSub: TIntegerField
      FieldName = 'CunsSub'
    end
    object QrSubCliControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSubCliFANTASIA: TWideStringField
      FieldName = 'FANTASIA'
      Size = 60
    end
  end
  object DsSubCli: TDataSource
    DataSet = QrSubCli
    Left = 572
    Top = 508
  end
  object QrMovAmovCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrMovAmovCadBeforeClose
    AfterScroll = QrMovAmovCadAfterScroll
    SQL.Strings = (
      'SELECT mac.*'
      'FROM movamovcad mac'
      'WHERE Cliente=:P0')
    Left = 992
    Top = 40
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMovAmovCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMovAmovCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrMovAmovCadTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrMovAmovCadCliente: TIntegerField
      FieldName = 'Cliente'
    end
  end
  object DsMovAmovCad: TDataSource
    DataSet = QrMovAmovCad
    Left = 1020
    Top = 40
  end
  object QrAtrAMovDef: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts,'
      'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,'
      'cad.Nome NO_CAD, its.Nome NO_ITS'
      'FROM atramovdef def'
      'LEFT JOIN atramovits its ON its.Controle=def.AtrIts'
      'LEFT JOIN atramovcad cad ON cad.Codigo=def.AtrCad'
      'WHERE def.ID_Sorc=:P0'
      'ORDER BY NO_CAD, NO_ITS')
    Left = 992
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAtrAMovDefID_Item: TIntegerField
      FieldName = 'ID_Item'
    end
    object QrAtrAMovDefID_Sorc: TIntegerField
      FieldName = 'ID_Sorc'
    end
    object QrAtrAMovDefAtrCad: TIntegerField
      FieldName = 'AtrCad'
    end
    object QrAtrAMovDefAtrIts: TIntegerField
      FieldName = 'AtrIts'
    end
    object QrAtrAMovDefCU_CAD: TIntegerField
      FieldName = 'CU_CAD'
      Required = True
    end
    object QrAtrAMovDefCU_ITS: TIntegerField
      FieldName = 'CU_ITS'
    end
    object QrAtrAMovDefNO_CAD: TWideStringField
      FieldName = 'NO_CAD'
      Size = 30
    end
    object QrAtrAMovDefNO_ITS: TWideStringField
      FieldName = 'NO_ITS'
      Size = 50
    end
  end
  object DsAtrAMovDef: TDataSource
    DataSet = QrAtrAMovDef
    Left = 1020
    Top = 68
  end
  object PMAtrAMovDef: TPopupMenu
    Left = 884
    Top = 300
    object IncluiAtrAMovDef1: TMenuItem
      Caption = '&Inclui um novo atributo'
      OnClick = IncluiAtrAMovDef1Click
    end
    object AlteraAtrAMovDef1: TMenuItem
      Caption = '&Altera o atributo selecionado'
      OnClick = AlteraAtrAMovDef1Click
    end
    object ExcluiAtrAMovDef1: TMenuItem
      Caption = '&Exclui o atributo selecionado'
      OnClick = ExcluiAtrAMovDef1Click
    end
  end
  object PMMovAmovCad: TPopupMenu
    OnPopup = PMSiapTerCadPopup
    Left = 576
    Top = 300
    object Incluinovomvelautomvel1: TMenuItem
      Caption = '&Inclui novo m'#243'vel / ve'#237'culo'
      OnClick = Incluinovomvelautomvel1Click
    end
    object Alteramvelautomvel1: TMenuItem
      Caption = '&Altera m'#243'vel / ve'#237'culo'
      OnClick = Alteramvelautomvel1Click
    end
    object Excluimvelautomvel1: TMenuItem
      Caption = '&Exclui m'#243'vel / ve'#237'culo'
      OnClick = Excluimvelautomvel1Click
    end
  end
  object QrAT_Dep: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(sic.M2Constru > 0, '
      'SUM(sid.MLarg * sid.MComp) / sic.M2Constru * 100, 0) PERC_TOT,'
      'SUM(sid.MLarg * sid.MComp) AREA,'
      'SUM(sid.MLArg * sid.MComp * sid.MAltu) VOLUME'
      'FROM siapimadep sid'
      'LEFT JOIN siapimacad sic ON sic.Codigo=sid.Codigo'
      'WHERE sid.Codigo>0')
    Left = 628
    Top = 592
    object QrAT_DepPERC_TOT: TFloatField
      FieldName = 'PERC_TOT'
      DisplayFormat = '0.0000'
    end
    object QrAT_DepAREA: TFloatField
      FieldName = 'AREA'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrAT_DepVOLUME: TFloatField
      FieldName = 'VOLUME'
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsAT_Dep: TDataSource
    DataSet = QrAT_Dep
    Left = 656
    Top = 592
  end
  object QrCunsImgCab: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCunsImgCabAfterOpen
    BeforeClose = QrCunsImgCabBeforeClose
    AfterScroll = QrCunsImgCabAfterScroll
    SQL.Strings = (
      'SELECT dep.Nome NO_DEPENDENCIA, cis.Nome NO_STATUS, '
      'nct.Nome NO_NCT, nct.TxtCAC, cic.* '
      'FROM cunsimgcab cic '
      'LEFT JOIN cunsimgsit cis ON cis.Codigo=cic.Status '
      'LEFT JOIN siapimadep sid ON sid.Controle=cic.Dependencia'
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci'
      'LEFT JOIN cunsimgnct nct ON nct.Codigo=cic.CodiNCT'
      'WHERE cic.Codigo>0 ')
    Left = 640
    Top = 664
    object QrCunsImgCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCunsImgCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCunsImgCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCunsImgCabStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrCunsImgCabCaminho: TWideStringField
      FieldName = 'Caminho'
      Size = 255
    end
    object QrCunsImgCabNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrCunsImgCabDependencia: TIntegerField
      FieldName = 'Dependencia'
    end
    object QrCunsImgCabNO_DEPENDENCIA: TWideStringField
      FieldName = 'NO_DEPENDENCIA'
      Size = 60
    end
    object QrCunsImgCabCodiNCT: TIntegerField
      FieldName = 'CodiNCT'
    end
    object QrCunsImgCabObserv: TWideMemoField
      FieldName = 'Observ'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCunsImgCabAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrCunsImgCabNO_NCT: TWideStringField
      FieldName = 'NO_NCT'
      Size = 255
    end
    object QrCunsImgCabTxtCAC: TWideStringField
      FieldName = 'TxtCAC'
      Size = 255
    end
    object QrCunsImgCabOrdem: TIntegerField
      FieldName = 'Ordem'
    end
  end
  object DsCunsImgCab: TDataSource
    DataSet = QrCunsImgCab
    Left = 668
    Top = 664
  end
  object PMCunsImgCab: TPopupMenu
    OnPopup = PMCunsImgCabPopup
    Left = 696
    Top = 664
    object IncluinovaFoto1: TMenuItem
      Caption = '&Inclui nova foto'
      OnClick = IncluinovaFoto1Click
    end
    object AlteraFotoatual1: TMenuItem
      Caption = '&Altera foto atual'
      OnClick = AlteraFotoatual1Click
    end
    object Excluifotoatual1: TMenuItem
      Caption = '&Exclui foto atual'
      OnClick = Excluifotoatual1Click
    end
  end
  object QrCunsImgCmt_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT txg.Texto TXT_GENERIC, cic.* '
      'FROM cunsimgcmt cic'
      'LEFT JOIN txtgeneric txg ON txg.Codigo=cic.TxtGeneric'
      'WHERE cic.Controle>0 ')
    Left = 640
    Top = 692
    object QrCunsImgCmt_TXT_GENERIC: TWideMemoField
      FieldName = 'TXT_GENERIC'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCunsImgCmt_Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCunsImgCmt_Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCunsImgCmt_Conta: TIntegerField
      FieldName = 'Conta'
    end
    object QrCunsImgCmt_TxtGeneric: TIntegerField
      FieldName = 'TxtGeneric'
    end
    object QrCunsImgCmt_Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCunsImgCmt_: TDataSource
    DataSet = QrCunsImgCmt_
    Left = 668
    Top = 692
  end
  object PMCunsImgCmt_: TPopupMenu
    OnPopup = PMCunsImgCmt_Popup
    Left = 696
    Top = 692
    object IncluiNovoTexto1: TMenuItem
      Caption = '&Inclui novo texto'
      OnClick = IncluiNovoTexto1Click
    end
    object AlteraTextoAtual1: TMenuItem
      Caption = '&Altera texto selecionado'
      OnClick = AlteraTextoAtual1Click
    end
    object ExcluiTextoAtual1: TMenuItem
      Caption = '&Exclui texto selecionado'
      OnClick = ExcluiTextoAtual1Click
    end
  end
  object CSTabSheetChamou: TdmkCompoStore
    Left = 24
    Top = 12
  end
  object QrAtrSTCdDef: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts + 0.000 ' +
        'ATRITS, '
      'cad.CodUsu CU_CAD, its.CodUsu CU_ITS,  "" AtrTxt,'
      'cad.Nome NO_CAD, its.Nome NO_ITS, cad.AtrTyp'
      'FROM atrstcddef def'
      'LEFT JOIN atrstcdits its ON its.Controle=def.AtrIts '
      'LEFT JOIN atrstcdcad cad ON cad.Codigo=def.AtrCad '
      'WHERE def.ID_Sorc>0 '
      ' '
      'UNION  '
      ' '
      'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, 0 ATRITS, '
      'cad.CodUsu CU_CAD, 0 CU_ITS, def.AtrTxt AtrTxt,'
      'cad.Nome NO_CAD, def.AtrTxt NO_ITS, cad.AtrTyp '
      'FROM atrstcdtxt def '
      'LEFT JOIN atrstcdcad cad ON cad.Codigo=def.AtrCad '
      'WHERE def.ID_Sorc>0 '
      ' '
      'ORDER BY NO_CAD, NO_ITS '
      '')
    Left = 732
    Top = 664
    object QrAtrSTCdDefID_Item: TIntegerField
      FieldName = 'ID_Item'
      Required = True
    end
    object QrAtrSTCdDefID_Sorc: TIntegerField
      FieldName = 'ID_Sorc'
      Required = True
    end
    object QrAtrSTCdDefAtrCad: TIntegerField
      FieldName = 'AtrCad'
      Required = True
    end
    object QrAtrSTCdDefATRITS: TFloatField
      FieldName = 'ATRITS'
      Required = True
    end
    object QrAtrSTCdDefCU_CAD: TIntegerField
      FieldName = 'CU_CAD'
    end
    object QrAtrSTCdDefCU_ITS: TLargeintField
      FieldName = 'CU_ITS'
    end
    object QrAtrSTCdDefAtrTxt: TWideStringField
      FieldName = 'AtrTxt'
      Size = 255
    end
    object QrAtrSTCdDefNO_CAD: TWideStringField
      FieldName = 'NO_CAD'
      Size = 30
    end
    object QrAtrSTCdDefNO_ITS: TWideStringField
      FieldName = 'NO_ITS'
      Size = 255
    end
    object QrAtrSTCdDefAtrTyp: TSmallintField
      FieldName = 'AtrTyp'
    end
  end
  object DsAtrSTCdDef: TDataSource
    DataSet = QrAtrSTCdDef
    Left = 760
    Top = 664
  end
  object PMAtrSTCdDef: TPopupMenu
    OnPopup = PMAtrSTCdDefPopup
    Left = 916
    Top = 300
    object IncluiAtrSTCdDef1: TMenuItem
      Caption = '&Inclui um novo atributo'
      OnClick = IncluiAtrSTCdDef1Click
    end
    object AlteraAtrSTCdDef1: TMenuItem
      Caption = '&Altera o atributo selecionado'
      OnClick = AlteraAtrSTCdDef1Click
    end
    object ExcluiAtrSTCdDef1: TMenuItem
      Caption = '&Exclui o atributo selecionado'
      OnClick = ExcluiAtrSTCdDef1Click
    end
  end
  object PMImprime: TPopupMenu
    Left = 20
    Top = 164
    object FichadoLugar1: TMenuItem
      Caption = 'Ficha do Lugar'
      OnClick = FichadoLugar1Click
    end
    object NCTCAC1: TMenuItem
      Caption = 'N.C.T. selecionados'
      OnClick = NCTCAC1Click
    end
  end
  object frxCAD_SUBCL_001_001_A: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41383.503409583300000000
    ReportOptions.LastChange = 41383.503409583300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Picture1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <ImgNCTExiste> = True then'
      '    Picture1.LoadFromFile(<ImgNCTPath>);  '
      'end;'
      ''
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      
        '  MeSloganFooter1.Text := <SloganFooter>;                       ' +
        '            '
      '  //'
      '  if <LogoTitRExiste> = True then'
      '  begin              '
      '    PictureTitR1.LoadFromFile(<LogoTitRPath>);'
      '    //PictureTitR2.LoadFromFile(<LogoTitRPath>);'
      '  end;                  '
      '  //'
      '  if <LogoTitLExiste> = True then'
      '  begin              '
      '    PictureTitL1.LoadFromFile(<LogoTitLPath>);'
      '   // PictureTitL2.LoadFromFile(<LogoTitLPath>);'
      '  end;'
      'end.')
    OnGetValue = frxCAD_SUBCL_001_001_AGetValue
    Left = 32
    Top = 232
    Datasets = <
      item
        DataSet = frxDsCIC_Imp
        DataSetName = 'frxDsCIC_Imp'
      end
      item
      end
      item
        DataSet = Dmod.frxDsOpcoesBugs
        DataSetName = 'frxDsOpcoesBugs'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 230.551330000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 151.181200000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Line_PH_01: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 132.283550000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 18.897650000000000000
          Top = 132.283550000000000000
          Width = 132.283486540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 529.134200000000000000
          Top = 132.283550000000000000
          Width = 132.283486540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Top = 181.417440000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ENTIDADE]')
          ParentFont = False
        end
        object PictureTitR1: TfrxPictureView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 151.181102360000000000
          Height = 113.385826770000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Stretched = False
          HightQuality = True
          Transparent = True
          TransparentColor = clWhite
        end
        object PictureTitL1: TfrxPictureView
          Top = 18.897650000000000000
          Width = 151.181102360000000000
          Height = 113.385826770000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Stretched = False
          HightQuality = True
          Transparent = True
          TransparentColor = clWhite
        end
        object Rich1: TfrxRichView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 113.385826770000000000
          DataField = 'TxtTitRelC'
          DataSet = Dmod.frxDsOpcoesFili
          DataSetName = 'frxDsOpcoesBugs'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C205461686F6D613B7D7D0D0A7B5C2A5C67656E657261746F7220
            52696368656432302031302E302E31363239397D5C766965776B696E64345C75
            6331200D0A5C706172645C66305C667331365C7061720D0A7D0D0A00}
        end
        object Memo2: TfrxMemoView
          Top = 154.960730000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 68.031530240000000000
        Top = 929.764380000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Top = 52.913420000000200000
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Top = 52.913420000000200000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSloganFooter1: TfrxMemoView
          Top = 30.236240000000180000
          Width = 676.535870000000000000
          Height = 22.677170240000000000
          OnBeforePrint = 'MeSloganFooter1OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FALTA_SETAR_SLOGAN_FOOTER]!')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Top = 7.559060000000045000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'Legenda: N.C.T. : N'#227'o Conformidade T'#233'cnica - C.A.C. : Comunicado' +
              ' de A'#231#227'o Corretiva.')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 559.370415590000000000
        Top = 309.921460000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCIC_Imp
        DataSetName = 'frxDsCIC_Imp'
        RowCount = 0
        Stretched = True
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_FC_NCT] N'#186' [frxDsCIC_Imp."Ordem"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Foto: [frxDsCIC_Imp."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 37.795300000000000000
          Top = 37.795300000000000000
          Width = 604.724800000000000000
          Height = 340.157700000000000000
          OnBeforePrint = 'Picture1OnBeforePrint'
          Center = True
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo4: TfrxMemoView
          Top = 385.512060000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente: [VARF_ENTIDADE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Top = 423.307360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              'Local: [frxDsCIC_Imp."NO_LOCAL"] : [frxDsCIC_Imp."NO_DEPENDENCIA' +
              '"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Top = 442.205010000000000000
          Width = 680.315400000000000000
          Height = 22.677155590000000000
          StretchMode = smMaxHeight
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_DADOS_REL]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Top = 404.409710000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Lugar: [frxDsCIC_Imp."NO_STC"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrCIC_Imp: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT dep.Nome NO_DEPENDENCIA, cis.Nome NO_STATUS,  '
      'nct.Nome NO_NCT, nct.TxtCAC, sic.SCompl2 NO_LOCAL, cic.*  '
      'FROM _cuns_img_cab cic  '
      'LEFT JOIN bugstrol.cunsimgsit cis ON cis.Codigo=cic.Status  '
      
        'LEFT JOIN bugstrol.siapimadep sid ON sid.Controle=cic.Dependenci' +
        'a '
      'LEFT JOIN bugstrol.dependenci dep ON dep.Codigo=sid.Dependenci '
      'LEFT JOIN bugstrol.siapimacad sic ON sic.Codigo=sid.Codigo'
      'LEFT JOIN bugstrol.cunsimgnct nct ON nct.Codigo=cic.CodiNCT '
      'WHERE cic.Codigo>0  ')
    Left = 60
    Top = 232
    object QrCIC_ImpCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCIC_ImpControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCIC_ImpNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCIC_ImpStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrCIC_ImpCaminho: TWideStringField
      FieldName = 'Caminho'
      Size = 255
    end
    object QrCIC_ImpNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrCIC_ImpDependencia: TIntegerField
      FieldName = 'Dependencia'
    end
    object QrCIC_ImpNO_DEPENDENCIA: TWideStringField
      FieldName = 'NO_DEPENDENCIA'
      Size = 60
    end
    object QrCIC_ImpCodiNCT: TIntegerField
      FieldName = 'CodiNCT'
    end
    object QrCIC_ImpObserv: TWideMemoField
      FieldName = 'Observ'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCIC_ImpAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrCIC_ImpNO_NCT: TWideStringField
      FieldName = 'NO_NCT'
      Size = 255
    end
    object QrCIC_ImpTxtCAC: TWideStringField
      FieldName = 'TxtCAC'
      Size = 255
    end
    object QrCIC_ImpNO_LOCAL: TWideStringField
      FieldName = 'NO_LOCAL'
      Size = 100
    end
    object QrCIC_ImpOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrCIC_ImpNO_STC: TWideStringField
      FieldName = 'NO_STC'
      Size = 100
    end
  end
  object frxDsCIC_Imp: TfrxDBDataset
    UserName = 'frxDsCIC_Imp'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Nome=Nome'
      'Status=Status'
      'Caminho=Caminho'
      'NO_STATUS=NO_STATUS'
      'Dependencia=Dependencia'
      'NO_DEPENDENCIA=NO_DEPENDENCIA'
      'CodiNCT=CodiNCT'
      'Observ=Observ'
      'Aplicacao=Aplicacao'
      'NO_NCT=NO_NCT'
      'TxtCAC=TxtCAC'
      'NO_LOCAL=NO_LOCAL'
      'Ordem=Ordem'
      'NO_STC=NO_STC')
    DataSet = QrCIC_Imp
    BCDToCurrency = False
    Left = 88
    Top = 232
  end
  object QrMixRel: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT SUM(IF(Aplicacao=0,1,0)) FC,'
      'SUM(IF(Aplicacao=1,1,0)) NCT'
      'FROM _cuns_img_cab')
    Left = 116
    Top = 232
    object QrMixRelFC: TFloatField
      FieldName = 'FC'
    end
    object QrMixRelNCT: TFloatField
      FieldName = 'NCT'
    end
  end
  object QrSTCAgeEve: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSTCAgeEveCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM agendaeve'
      'WHERE Local=1309')
    Left = 832
    Top = 412
    object QrSTCAgeEveCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSTCAgeEveQuestaoTyp: TIntegerField
      FieldName = 'QuestaoTyp'
    end
    object QrSTCAgeEveQuestaoCod: TIntegerField
      FieldName = 'QuestaoCod'
    end
    object QrSTCAgeEveQuestaoExe: TIntegerField
      FieldName = 'QuestaoExe'
    end
    object QrSTCAgeEveInicio: TDateTimeField
      FieldName = 'Inicio'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrSTCAgeEveTermino: TDateTimeField
      FieldName = 'Termino'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrSTCAgeEveEmprEnti: TIntegerField
      FieldName = 'EmprEnti'
    end
    object QrSTCAgeEveTerceiro: TIntegerField
      FieldName = 'Terceiro'
    end
    object QrSTCAgeEveLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrSTCAgeEveCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrSTCAgeEveCption: TSmallintField
      FieldName = 'Cption'
    end
    object QrSTCAgeEveNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSTCAgeEveNotas: TWideStringField
      FieldName = 'Notas'
      Size = 255
    end
    object QrSTCAgeEveFatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
    end
    object QrSTCAgeEveLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSTCAgeEveDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSTCAgeEveDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSTCAgeEveUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSTCAgeEveUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSTCAgeEveAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrSTCAgeEveAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrSTCAgeEveTEXTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEXTO'
      Size = 512
      Calculated = True
    end
    object QrSTCAgeEveNO_FATOGERADR: TWideStringField
      FieldName = 'NO_FATOGERADR'
      Size = 60
    end
  end
  object DsSTCAgeEve: TDataSource
    DataSet = QrSTCAgeEve
    Left = 860
    Top = 412
  end
  object PMSTCAgeEve: TPopupMenu
    Left = 816
    Top = 132
    object Excluiragendamento1: TMenuItem
      Caption = 'Excluir agendamento'
      OnClick = Excluiragendamento1Click
    end
  end
  object PMSiapTerFlh: TPopupMenu
    OnPopup = PMSiapTerFlhPopup
    Left = 356
    Top = 300
    object IncluiDsSiapTerFlh1: TMenuItem
      Caption = '&Inclui nova empresa'
      OnClick = IncluiDsSiapTerFlh1Click
    end
    object AlteraDsSiapTerFlh1: TMenuItem
      Caption = '&Altera empresa selecionada'
      OnClick = AlteraDsSiapTerFlh1Click
    end
    object ExcluiDsSiapTerFlh1: TMenuItem
      Caption = '&Remove empresa selecionada'
      OnClick = ExcluiDsSiapTerFlh1Click
    end
  end
  object QrSiapTerFlh: TmySQLQuery
    Database = Dmod.MyDB
    Left = 84
    Top = 400
    object QrSiapTerFlhCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapTerFlhControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSiapTerFlhEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSiapTerFlhMonAgeEqi: TIntegerField
      FieldName = 'MonAgeEqi'
    end
    object QrSiapTerFlhMonEntCtt: TIntegerField
      FieldName = 'MonEntCtt'
    end
    object QrSiapTerFlhMonNumCtr: TIntegerField
      FieldName = 'MonNumCtr'
    end
    object QrSiapTerFlhMonEntCtr: TIntegerField
      FieldName = 'MonEntCtr'
    end
    object QrSiapTerFlhMonEntPag: TIntegerField
      FieldName = 'MonEntPag'
    end
    object QrSiapTerFlhMonCondPg: TIntegerField
      FieldName = 'MonCondPg'
    end
    object QrSiapTerFlhMonCrtEmi: TIntegerField
      FieldName = 'MonCrtEmi'
    end
    object QrSiapTerFlhDataSincOS: TDateField
      FieldName = 'DataSincOS'
      OnGetText = QrSiapTerFlhDataSincOSGetText
    end
    object QrSiapTerFlhTrajetDist: TIntegerField
      FieldName = 'TrajetDist'
    end
    object QrSiapTerFlhTrajetKm_h: TSmallintField
      FieldName = 'TrajetKm_h'
    end
    object QrSiapTerFlhMinHAgeExe: TTimeField
      FieldName = 'MinHAgeExe'
    end
  end
  object DsSiapTerFlh: TDataSource
    DataSet = QrSiapTerFlh
    Left = 112
    Top = 400
  end
  object QrOSCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT  aec.Nome NO_AgeEqiCab, msv.Nome NO_MUL_SRV, '
      'CONCAT(osc.Estatus, " - ", sta.Nome) Estatus_TXT, '
      'cun.Nome NO_LUGAR, '
      'osc.*, '
      'IF(en1.Tipo=0, en1.RazaoSocial, en1.Nome) NO_ENTIDADE, '
      'IF(en2.Tipo=0, en2.RazaoSocial, en2.Nome) NO_ENTCONTRAT, '
      'IF(en3.Tipo=0, en3.RazaoSocial, en3.Nome) NO_ENTPAGANTE, '
      'IF(en1.Tipo=0, en1.Fantasia, en1.Apelido) AF_ENTIDADE, '
      'IF(en2.Tipo=0, en2.Fantasia, en2.Apelido) AF_ENTCONTRAT, '
      'IF(en3.Tipo=0, en3.Fantasia, en3.Apelido) AF_ENTPAGANTE, '
      'enc.Nome NO_ENTICONTAT,'
      'se1.Login NO_USER_CAD, '
      'se2.Login NO_USER_ALT, '
      'ELT(osc.HowGerou+1, "Manual",'
      '"Clone", "OS M'#227'e", "?????") NO_WOW_GEROU,'
      'ELT(osc.PosGerou+1, "N'#227'o", '
      '"Sim", "???") NO_POS_GEROU, '
      'ELT(osc.SohInicial+1, "N'#227'o", '
      '"Sim", "???") NO_SOH_INICIAL, '
      'ELT(osc.StPipAdPrg+1, "N'#227'o iniciado", '
      '"Em inclus'#227'o", "Finalizado", "?????") '
      'NO_ST_PIP_AD_PRG'
      ' '
      'FROM oscab osc '
      'LEFT JOIN entidades en1 ON en1.Codigo=osc.Entidade '
      'LEFT JOIN entidades en2 ON en2.Codigo=osc.EntContrat '
      'LEFT JOIN enticontat enc ON enc.Controle=osc.EntiContat '
      'LEFT JOIN estatusoss sta ON sta.Codigo=osc.Estatus '
      'LEFT JOIN mulservico msv ON msv.Codigo=osc.MulServico '
      'LEFT JOIN ageeqicab aec ON aec.Codigo=osc.AgeEqiCab '
      'LEFT JOIN entidades en3 ON en3.Codigo=osc.EntPagante '
      'LEFT JOIN siaptercad cun ON cun.Codigo=osc.SiapTerCad '
      'LEFT JOIN senhas se1 ON se1.Numero=osc.UserCad'
      'LEFT JOIN senhas se2 ON se2.Numero=osc.UserAlt'
      'WHERE osc.Empresa=1'
      'AND osc.Entidade=1208'
      'ORDER BY Codigo DESC'
      '')
    Left = 140
    Top = 304
    object QrOSCabNO_AgeEqiCab: TWideStringField
      FieldName = 'NO_AgeEqiCab'
      Size = 255
    end
    object QrOSCabNO_MUL_SRV: TWideStringField
      FieldName = 'NO_MUL_SRV'
      Size = 60
    end
    object QrOSCabEstatus_TXT: TWideStringField
      FieldName = 'Estatus_TXT'
      Size = 74
    end
    object QrOSCabNO_LUGAR: TWideStringField
      FieldName = 'NO_LUGAR'
      Size = 100
    end
    object QrOSCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSCabEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrOSCabEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrOSCabSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
    end
    object QrOSCabEstatus: TIntegerField
      FieldName = 'Estatus'
    end
    object QrOSCabFatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
    end
    object QrOSCabDtaContat: TDateTimeField
      FieldName = 'DtaContat'
    end
    object QrOSCabDtaVisPrv: TDateTimeField
      FieldName = 'DtaVisPrv'
    end
    object QrOSCabDtaVisExe: TDateTimeField
      FieldName = 'DtaVisExe'
    end
    object QrOSCabDtaExePrv: TDateTimeField
      FieldName = 'DtaExePrv'
    end
    object QrOSCabDtaExeIni: TDateTimeField
      FieldName = 'DtaExeIni'
    end
    object QrOSCabDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
    end
    object QrOSCabDtaLibFat: TDateTimeField
      FieldName = 'DtaLibFat'
    end
    object QrOSCabDtaFimFat: TDateTimeField
      FieldName = 'DtaFimFat'
    end
    object QrOSCabValorServi: TFloatField
      FieldName = 'ValorServi'
    end
    object QrOSCabValorDesco: TFloatField
      FieldName = 'ValorDesco'
    end
    object QrOSCabValorOutrs: TFloatField
      FieldName = 'ValorOutrs'
    end
    object QrOSCabValorTotal: TFloatField
      FieldName = 'ValorTotal'
    end
    object QrOSCabDdsPosVda: TIntegerField
      FieldName = 'DdsPosVda'
    end
    object QrOSCabEntiContat: TIntegerField
      FieldName = 'EntiContat'
    end
    object QrOSCabNumContrat: TIntegerField
      FieldName = 'NumContrat'
    end
    object QrOSCabEntPagante: TIntegerField
      FieldName = 'EntPagante'
    end
    object QrOSCabEntContrat: TIntegerField
      FieldName = 'EntContrat'
    end
    object QrOSCabCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
    end
    object QrOSCabCartEmis: TIntegerField
      FieldName = 'CartEmis'
    end
    object QrOSCabSerNF: TWideStringField
      FieldName = 'SerNF'
      Size = 3
    end
    object QrOSCabNumNF: TIntegerField
      FieldName = 'NumNF'
    end
    object QrOSCabLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSCabDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSCabDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSCabUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSCabAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOSCabInvalServi: TFloatField
      FieldName = 'InvalServi'
    end
    object QrOSCabInvalDesco: TFloatField
      FieldName = 'InvalDesco'
    end
    object QrOSCabInvalOutrs: TFloatField
      FieldName = 'InvalOutrs'
    end
    object QrOSCabInvalTotal: TFloatField
      FieldName = 'InvalTotal'
    end
    object QrOSCabOrcamServi: TFloatField
      FieldName = 'OrcamServi'
    end
    object QrOSCabOrcamDesco: TFloatField
      FieldName = 'OrcamDesco'
    end
    object QrOSCabOrcamOutrs: TFloatField
      FieldName = 'OrcamOutrs'
    end
    object QrOSCabOrcamTotal: TFloatField
      FieldName = 'OrcamTotal'
    end
    object QrOSCabValiDdOrca: TIntegerField
      FieldName = 'ValiDdOrca'
    end
    object QrOSCabOperacao: TIntegerField
      FieldName = 'Operacao'
    end
    object QrOSCabExeTxtCli1: TIntegerField
      FieldName = 'ExeTxtCli1'
    end
    object QrOSCabValorPre: TFloatField
      FieldName = 'ValorPre'
    end
    object QrOSCabObsGaranti: TWideMemoField
      FieldName = 'ObsGaranti'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSCabExeTxtCli2: TIntegerField
      FieldName = 'ExeTxtCli2'
    end
    object QrOSCabFimVisPrv: TDateTimeField
      FieldName = 'FimVisPrv'
    end
    object QrOSCabFimVisExe: TDateTimeField
      FieldName = 'FimVisExe'
    end
    object QrOSCabFimExePrv: TDateTimeField
      FieldName = 'FimExePrv'
    end
    object QrOSCabGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrOSCabNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrOSCabOpcao: TIntegerField
      FieldName = 'Opcao'
    end
    object QrOSCabOptado: TSmallintField
      FieldName = 'Optado'
    end
    object QrOSCabHowGerou: TSmallintField
      FieldName = 'HowGerou'
    end
    object QrOSCabPosGerou: TSmallintField
      FieldName = 'PosGerou'
    end
    object QrOSCabOSFlhUltGe: TIntegerField
      FieldName = 'OSFlhUltGe'
    end
    object QrOSCabMulServico: TLargeintField
      FieldName = 'MulServico'
    end
    object QrOSCabAgeEqiCab: TIntegerField
      FieldName = 'AgeEqiCab'
    end
    object QrOSCabOSFlhGrCab: TIntegerField
      FieldName = 'OSFlhGrCab'
    end
    object QrOSCabOSFlhGrIts: TIntegerField
      FieldName = 'OSFlhGrIts'
    end
    object QrOSCabSohInicial: TIntegerField
      FieldName = 'SohInicial'
    end
    object QrOSCabStPipAdPrg: TSmallintField
      FieldName = 'StPipAdPrg'
    end
    object QrOSCabNO_EMPRESA: TWideStringField
      FieldName = 'NO_EMPRESA'
      Size = 100
    end
    object QrOSCabNO_ENTCONTRAT: TWideStringField
      FieldName = 'NO_ENTCONTRAT'
      Size = 100
    end
    object QrOSCabNO_ENTPAGANTE: TWideStringField
      FieldName = 'NO_ENTPAGANTE'
      Size = 100
    end
    object QrOSCabAF_ENTCONTRAT: TWideStringField
      FieldName = 'AF_ENTCONTRAT'
      Size = 60
    end
    object QrOSCabAF_ENTPAGANTE: TWideStringField
      FieldName = 'AF_ENTPAGANTE'
      Size = 60
    end
    object QrOSCabNO_ENTICONTAT: TWideStringField
      FieldName = 'NO_ENTICONTAT'
      Size = 30
    end
    object QrOSCabNO_OPERACOES: TWideStringField
      FieldName = 'NO_OPERACOES'
      Size = 255
    end
    object QrOSCabLstUplWeb: TDateTimeField
      FieldName = 'LstUplWeb'
    end
    object QrOSCabLCPUsed: TIntegerField
      FieldName = 'LCPUsed'
    end
    object QrOSCabConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrOSCabObsExecuta: TWideMemoField
      FieldName = 'ObsExecuta'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSCabNO_USER_CAD: TWideStringField
      FieldName = 'NO_USER_CAD'
      Size = 30
    end
    object QrOSCabNO_USER_ALT: TWideStringField
      FieldName = 'NO_USER_ALT'
      Size = 30
    end
    object QrOSCabNO_WOW_GEROU: TWideStringField
      FieldName = 'NO_WOW_GEROU'
      Size = 6
    end
    object QrOSCabNO_POS_GEROU: TWideStringField
      FieldName = 'NO_POS_GEROU'
      Size = 3
    end
    object QrOSCabNO_SOH_INICIAL: TWideStringField
      FieldName = 'NO_SOH_INICIAL'
      Size = 3
    end
    object QrOSCabNO_ST_PIP_AD_PRG: TWideStringField
      FieldName = 'NO_ST_PIP_AD_PRG'
      Size = 12
    end
  end
  object DsOSCab: TDataSource
    DataSet = QrOSCab
    Left = 172
    Top = 304
  end
  object QrContratos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ctr.Codigo, ctr.Nome,  '
      
        'ctr.DContrato, ctr.DInstal, ctr.DVencimento, ctr.DtaPrxRenw, ctr' +
        '.DtaCntrFim,  '
      'ctr.Status, ctr.ValorMes, ctr.LugarNome, ctr.Versao , '
      
        'ELT(IF(DtaCntrFim > "1900-01-01", 3, ctr.Status)+1, "Cadastrado"' +
        ', "Nao Contratado", "Cobntratado") TXT_STATUS '
      'FROM Contratos ctr '
      'LEFT JOIN entidades en1 ON en1.Codigo= ctr.Contratada '
      'WHERE Contratante=1350 '
      ' ')
    Left = 140
    Top = 348
    object QrContratosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContratosNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrContratosDContrato: TDateField
      FieldName = 'DContrato'
    end
    object QrContratosDInstal: TDateField
      FieldName = 'DInstal'
    end
    object QrContratosDVencimento: TDateField
      FieldName = 'DVencimento'
    end
    object QrContratosDtaPrxRenw: TDateField
      FieldName = 'DtaPrxRenw'
    end
    object QrContratosDtaCntrFim: TDateField
      FieldName = 'DtaCntrFim'
    end
    object QrContratosStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrContratosValorMes: TFloatField
      FieldName = 'ValorMes'
    end
    object QrContratosLugarNome: TWideStringField
      FieldName = 'LugarNome'
      Size = 100
    end
    object QrContratosVersao: TFloatField
      FieldName = 'Versao'
    end
    object QrContratosTXT_STATUS: TWideStringField
      FieldName = 'TXT_STATUS'
      Size = 14
    end
  end
  object DsContratos: TDataSource
    DataSet = QrContratos
    Left = 168
    Top = 348
  end
  object QrPMVs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mot.Nome NO_MOTDESAT, gg1.Nome NO_EQUI,'
      'plc.Nome NO_LISTA, dep.Nome NO_DEPENDENCIA,'
      'IF(DtaAquis <= "30/12/1899", "", DtaAquis) DtaAquis_TXT,'
      'IF(DtaDesativ<= "30/12/1899", "", DtaDesativ) DtaDesativ_TXT,'
      'cad.*'
      'FROM pipcad cad'
      'LEFT JOIN gragrux ggx ON ggx.Controle=cad.Equipamento'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN motdesativ mot ON mot.Codigo=cad.MotDesativ'
      'LEFT JOIN prglstcab plc ON plc.Codigo=cad.PrgLstCab'
      'LEFT JOIN siapimadep sid ON sid.Controle=cad.Dependenci'
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci'
      'LEFT JOIN osmoncab omc ON omc.Conta=cad.OsMonCab'
      'LEFT JOIN oscab osc ON osc.Codigo=omc.Codigo'
      'WHERE osc.Entidade=1433'
      'ORDER BY Nome')
    Left = 744
    Top = 184
    object QrPMVsNO_MOTDESAT: TWideStringField
      FieldName = 'NO_MOTDESAT'
      Size = 60
    end
    object QrPMVsNO_EQUI: TWideStringField
      FieldName = 'NO_EQUI'
      Size = 120
    end
    object QrPMVsNO_LISTA: TWideStringField
      FieldName = 'NO_LISTA'
      Size = 100
    end
    object QrPMVsNO_DEPENDENCIA: TWideStringField
      FieldName = 'NO_DEPENDENCIA'
      Size = 60
    end
    object QrPMVsDtaAquis_TXT: TWideStringField
      FieldName = 'DtaAquis_TXT'
      Required = True
      Size = 10
    end
    object QrPMVsDtaDesativ_TXT: TWideStringField
      FieldName = 'DtaDesativ_TXT'
      Required = True
      Size = 10
    end
    object QrPMVsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPMVsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPMVsEquipamento: TIntegerField
      FieldName = 'Equipamento'
    end
    object QrPMVsOSMonCab: TIntegerField
      FieldName = 'OSMonCab'
    end
    object QrPMVsMotDesativ: TIntegerField
      FieldName = 'MotDesativ'
    end
    object QrPMVsDtaAquis: TDateField
      FieldName = 'DtaAquis'
    end
    object QrPMVsDtaDesativ: TDateField
      FieldName = 'DtaDesativ'
    end
    object QrPMVsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPMVsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPMVsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPMVsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPMVsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPMVsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPMVsAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPMVsPrgLstCab: TIntegerField
      FieldName = 'PrgLstCab'
    end
    object QrPMVsDependenci: TIntegerField
      FieldName = 'Dependenci'
    end
    object QrPMVsOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrPMVsReordem: TIntegerField
      FieldName = 'Reordem'
    end
    object QrPMVsDdPostero: TIntegerField
      FieldName = 'DdPostero'
    end
    object QrPMVsPerioDd: TIntegerField
      FieldName = 'PerioDd'
    end
  end
  object DsPMVs: TDataSource
    DataSet = QrPMVs
    Left = 744
    Top = 232
  end
  object PMPMVs: TPopupMenu
    OnPopup = PMPMVsPopup
    Left = 200
    Top = 136
    object AlteraPMVatual1: TMenuItem
      Caption = 'Alterar &PMV atual'
      OnClick = AlteraPMVatual1Click
    end
    object Alterarintervalospsteros1: TMenuItem
      Caption = 'Alterar campo '#250'nico'
      object Intervalospsteros1: TMenuItem
        Caption = 'Intervalos p'#243'steros'
        OnClick = Intervalospsteros1Click
      end
      object Periododemonitoramento1: TMenuItem
        Caption = 'Per'#237'odo de monitoramento'
        OnClick = Periododemonitoramento1Click
      end
    end
  end
  object QrSiapImaSVG: TmySQLQuery
    Database = Dmod.MyDB
    Left = 72
    Top = 536
    object QrSiapImaSVGCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrSiapImaSVGControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrSiapImaSVGNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrSiapImaSVGNoArq: TWideStringField
      FieldName = 'NoArq'
      Size = 255
    end
    object QrSiapImaSVGRotarGraus: TIntegerField
      FieldName = 'RotarGraus'
    end
    object QrSiapImaSVGNO_RotarGraus: TWideStringField
      FieldName = 'NO_RotarGraus'
      Size = 5
    end
  end
  object DsSiapImaSVG: TDataSource
    DataSet = QrSiapImaSVG
    Left = 72
    Top = 584
  end
  object PMSiapImaSVG: TPopupMenu
    OnPopup = PMSiapImaSVGPopup
    Left = 944
    Top = 300
    object Carreganovoarquivo1: TMenuItem
      Caption = '&Carrega novo arquivo'
      OnClick = Carreganovoarquivo1Click
    end
    object Recarrega1: TMenuItem
      Caption = 'Recarrega arquivo &Croqui'
      OnClick = Recarrega1Click
    end
    object Alteraadescrio1: TMenuItem
      Caption = '&Altera outros dados'
      OnClick = Alteraadescrio1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Visualizarcroqui1: TMenuItem
      Caption = '&Visualizar croqui'
      OnClick = Visualizarcroqui1Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Excluiarquivoatual1: TMenuItem
      Caption = '&Remover croqui atual'
      OnClick = Excluiarquivoatual1Click
    end
  end
  object QrWUsers: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT wus.Codigo, wpe.Nome Perfil_TXT, '
      'wus.PersonalName, wus.Username, '
      'wus.Email, wus.Ativo'
      'FROM wusers wus'
      'LEFT JOIN wperfis wpe ON wpe.Codigo = wus.Perfil'
      'WHERE Entidade=:P0')
    Left = 404
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrWUsersCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWUsersPerfil_TXT: TWideStringField
      FieldName = 'Perfil_TXT'
      Size = 50
    end
    object QrWUsersPersonalName: TWideStringField
      FieldName = 'PersonalName'
      Size = 32
    end
    object QrWUsersUsername: TWideStringField
      FieldName = 'Username'
      Size = 100
    end
    object QrWUsersEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrWUsersAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object DsWUsers: TDataSource
    DataSet = QrWUsers
    Left = 432
    Top = 196
  end
end
