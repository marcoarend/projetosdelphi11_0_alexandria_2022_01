// ini Delphi 28 Alexandria
//{$I dmk.inc}
// fim Delphi 28 Alexandria
unit MyListas;

interface

uses
  System.Generics.Collections,
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, DB, (*DBTables,*) mysqlDBTables, UnMyLinguas,
  UnInternalConsts, dmkGeral, UnDmkProcFunc, UnDmkEnums;

type
  TMyListas = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    (*
    function CriaListaImpDOS(FImpDOS: TStringList): Boolean;
    function CriaListaUserSets(FUserSets: TStringList): Boolean;
    *)
    //
    function CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
    function CriaListaTabelas(Database: TmySQLDatabase; Lista:
             TList<TTabelas>): Boolean;
    //function CriaListaTabelas(Database: TmySQLDatabase; FTabelas: TStringList): Boolean;
    function CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
    function CriaListaIndices(TabelaBase, TabelaNome: String;
             FLIndices: TList<TIndices>): Boolean;
    function CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
             var TemControle: TTemControle): Boolean;
    function CriaListaQeiLnk(TabelaCria: String; FLQeiLnk: TList<TQeiLnk>): Boolean;
    function CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function ExcluiTab: Boolean;
    function ExcluiReg: Boolean;
    function ExcluiIdx: Boolean;
    procedure VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
              DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ConfiguracoesIniciais(UsaCoresRel: integer; AppIDtxt: String);
  end;

const
  CO_VERSAO = 1809171324;
  CO_SIGLA_APP = 'BUGS';
  //
  CO_DMKID_APP = 24;
  CO_GRADE_APP = False;
  CO_VLOCAL = True;
  //
  CO_TabLctA = 'lct'+'0001a';
  CO_EXTRA_LCT_003 = False; // True somente para Credito2
  //

var
  MyList: TMyListas;
  FRCampos  : TCampos;
  FRIndices : TIndices;

   _ArrClieSets: array[01..32] of String;
   _MaxClieSets: Integer;
   _ArrFornSets: array[01..32] of String;
   _MaxFornSets: Integer;

implementation

uses MyDBCheck, Module, ModuleGeral, UMySQLModule, Geral_TbTerc, UnAll_Tabs,
  UnPerfJan_Tabs, UnAnotacoes_Tabs, UnEnti_Tabs, UnIBGE_DTB_Tabs, UnCuns_Tabs,
  UnDiario_Tabs, UnBina_Tabs, UnAgenda_Tabs, UnOS_Tabs, UnProtocol_Tabs,
  UnCNAB_Tabs, UnBloq_Tabs, UnContrat_Tabs, FPMin_Tabs, FPMax_Tabs, UnUMedi_Tabs,
  UnBugMbl_Tabs, Mail_Tabs, UnTextos_Tabs, Arquivos_Tabs, WUsers_Tabs, UnCRO_Tabs,
  UnGrade_Tabs, NFe_Tabs, NFSe_Tabs, NFSe_TbTerc, SPED_EFD_Tabs, SINTEGRA_Tabs,
  UnBugs_Tabs, UnFinance_Tabs, UnImprime_Tabs, UnMoedas_Tabs, UnFiscal_Tabs,
  UnGFat_Tabs, UnEmpresas_Tabs, UnCheques_Tabs, UnChekLst_Tabs, UnDocsCab_Tabs,
  UnPraz_Tabs, UnEntities, UnBloqGerl_Tabs, UnNotificacoes_Tabs, WGerl_Tabs,
  Web_Tabs;

function TMyListas.CriaListaTabelas(Database: TmySQLDatabase; Lista:
 TList<TTabelas>): Boolean;
  procedure TabelasPorCliente((*Tab: TTabsCli*));
  var
    TbCI: String;
    CliInt: Integer;
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SHOW TABLES');
    Dmod.QrAux.SQL.Add('FROM ' + TMeuDB);
    Dmod.QrAux.SQL.Add('LIKE "entidades"');
    UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT cliint');
      Dmod.QrAux.SQL.Add('FROM entidades');
      Dmod.QrAux.SQL.Add('WHERE cliint <> 0');
      UMyMod.AbreQuery(Dmod.QrAux, Dmod.MyDB);
      //
      while not Dmod.QrAux.Eof do
      begin
        CliInt := Dmod.QrAux.FieldByName('cliint').AsInteger;
        { N�o permitir n�meros negativos para filial!
        if CliInt < 0 then
          TbCI := '_' + FormatFloat('000', CliInt )
        else
          TbCI := FormatFloat('0000', CliInt);
        }
        if CliInt > 0 then
        begin
          TbCI := FormatFloat('0000', CliInt);
          //
          MyLinguas.AdTbLst(FTabelas, False, Lowercase('lct' + TbCI + 'A'), Lowercase(LAN_CTOS));
          MyLinguas.AdTbLst(FTabelas, False, Lowercase('lct' + TbCI + 'B'), Lowercase(LAN_CTOS));
          MyLinguas.AdTbLst(FTabelas, False, Lowercase('lct' + TbCI + 'D'), Lowercase(LAN_CTOS));
        end;
        //
        Dmod.QrAux.Next;
      end;
    end;
  end;
begin
  Result := True;
  try
    if Database = Dmod.MyDB then
    begin
      All_Tabs.CarregaListaTabelas(FTabelas);
      Anotacoes_Tabs.CarregaListaTabelas(FTabelas);
      Enti_Tabs.CarregaListaTabelas(FTabelas);
      Cuns_Tabs.CarregaListaTabelas(FTabelas);
      OS_Tabs.CarregaListaTabelas(FTabelas);
      Bugs_Tabs.CarregaListaTabelas(Database, FTabelas);
      PerfJan_Tabs.CarregaListaTabelas(FTabelas);
      Fiscal_Tabs.CarregaListaTabelas(FTabelas);
      Cheques_Tabs.CarregaListaTabelas(FTabelas);
      GFat_Tabs.CarregaListaTabelas(FTabelas);
      Empresas_Tabs.CarregaListaTabelas(FTabelas);
      Finance_Tabs.CarregaListaTabelas(FTabelas);
      Finance_Tabs.ComplementaListaComLcts(Lista);
      ////CashTb.CarregaListaTabelasCashier(FTabelas);
      //CashTb.ComplementaListaComLcts(Lista);
      NFe_Tb.CarregaListaTabelas(Database, FTabelas);
      Imprime_Tabs.CarregaListaTabelas(FTabelas);
      DocsCab_Tabs.CarregaListaTabelas(FTabelas);
      ChekLst_Tabs.CarregaListaTabelas(FTabelas);
      Moedas_Tabs.CarregaListaTabelas(FTabelas);
      Grade_Tabs.CarregaListaTabelas(FTabelas);
      SPEDEFD_Tb.CarregaListaTabelas(Database, FTabelas);
      SINTEGRA_Tb.CarregaListaTabelas(FTabelas);
      Diario_Tabs.CarregaListaTabelas(FTabelas);
      Bina_Tabs.CarregaListaTabelas(FTabelas);
      Bloq_Tabs.CarregaListaTabelas(FTabelas);
      BloqGerl_Tabs.CarregaListaTabelas(Database, FTabelas);
      CNAB_Tabs.CarregaListaTabelas(Database, FTabelas);
      Protocol_Tabs.CarregaListaTabelas(Database, FTabelas);
      Contrat_Tabs.CarregaListaTabelas(FTabelas);
      UnFPMin_Tabs.CarregaListaTabelas(Lista);
      UnNFSe_Tabs.CarregaListaTabelas(Database.Databasename, FTabelas);
      Arquivos_Tb.CarregaListaTabelas(Database, Lista);
      Textos_Tabs.CarregaListaTabelas(Database, Lista);
      //UnFPMax_Tabs.CarregaListaTabelas(Lista);
      Agenda_Tabs.CarregaListaTabelas(FTabelas);
      Mail_Tb.CarregaListaTabelas(Lista);
      CRO_Tabs.CarregaListaTabelas(FTabelas);
      UMedi_Tabs.CarregaListaTabelas(Database, FTabelas);
      Praz_Tabs.CarregaListaTabelas(Database, FTabelas);
      Notificacoes_Tabs.CarregaListaTabelas(FTabelas);
      Web_Tb.CarregaListaTabelas(Database.DatabaseName, Lista, CO_DMKID_APP);
      //
      MyLinguas.AdTbLst(Lista, False, 'controle', '');
      //
      //
    end else
    if Database = DModG.AllID_DB then
    begin
      UnGeral_TbTerc.CarregaListaTabelas(FTabelas);
      IBGE_DTB_Tabs.CarregaListaTabelas(FTabelas);
      UnNFSe_TbTerc.CarregaListaTabelas(Lista);
      NFe_Tb.CarregaListaTabelas(Database, FTabelas);
      SPEDEFD_Tb.CarregaListaTabelas(Database, FTabelas);
    end else
    if Database = Dmod.MyDBn then
    begin
      BugMbl_Tabs.CarregaListaTabelas(FTabelas);
      Arquivos_Tb.CarregaListaTabelas(Database, Lista);
      Textos_Tabs.CarregaListaTabelas(Database, Lista);
      WUsers_Tb.CarregaListaTabelas(Database, Lista, CO_DMKID_APP);
      WGerl_Tb.CarregaListaTabelas(Database, Lista, CO_DMKID_APP);
      Web_Tb.CarregaListaTabelas(Database.DatabaseName, Lista, CO_DMKID_APP);
      Bugs_Tabs.CarregaListaTabelas(Database, FTabelas);
      Protocol_Tabs.CarregaListaTabelas(Database, Lista);
      //
      MyLinguas.AdTbLst(Lista, False, LowerCase('arreits'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('bacen_pais'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('carteiras'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('cnab_cfg'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('dtb_munici'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('emailconta'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('enticliint'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('entidades'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wentidades'), 'entidades');
      MyLinguas.AdTbLst(Lista, False, Lowercase('enticargos'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('enticonent'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('enticontat'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wenticonta'), 'enticontat');
      MyLinguas.AdTbLst(Lista, False, Lowercase('entimail'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wentimail'), 'entimail');
      MyLinguas.AdTbLst(Lista, False, Lowercase('entitel'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('wentitel'), 'entitel');
      MyLinguas.AdTbLst(Lista, False, LowerCase('entitipcto'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('feriados'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('listalograd'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('nfsenfscab'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('preemail'), '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('preemmsg'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('prev'), '');
      MyLinguas.AdTbLst(Lista, False, LowerCase('bloopcoes'), '');
      //
      TabelasPorCliente();
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaTabelasLocais(Lista: TList<TTabelas>): Boolean;
begin
  Result := True;
  try
    Finance_Tabs.CarregaListaTabelasLocais(Lista);
    //CashTb.CarregaListaTabelasLocaisCashier(Lista);
    //FtabelasLocais.Add('');
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
(*
const
teste =
'{\\rtf1\\ansi\\ansicpg1252\\deff0\\deflang1046{\\fonttbl{\\f0\\fnil\\fcharset0 Calibri;}}' +
'{\\*\\generator Msftedit 5.41.21.2509;}\\viewkind4\\uc1\\pard\\sa200\\sl276\\slmult1\\lang22\\f0\\fs22 Linha 1\\par' +
'Linha 2\\par' +
'Linha 3\\par' +
'Imagem:\\par' +
'\\pard\\sa200\\sl240\\slmult1{\\pict\\wmetafile8\\picw1693\\pich847\\picwgoal960\\pichgoal480' +
'0100090000034a06000000002106000000000400000003010800050000000b0200000000050000' +
'000c0220004000030000001e000400000007010400040000000701040021060000410b2000cc00' +
'200040000000000020004000000000002800000040000000200000000100080000000000000000' +
'000000000000000000000000000000000000000000ffffff00ff00ff006b84e7002142de00a9a9' +
'a9007f7f7f005273de008494ef00c6cef7000029d60098989800b9b9b900dedede006b6b6b0073' +
'8ce7001039de0094a5ef00dee7ff006b84ef007384e700adadad0077777700c1c1c100eeeeee00' +
'425ade003152de001842de004a63e7007b8cef00ced6ff00d6e7ff00bdceff00c6d6ff00cedeff' +
'001839de0090909000878787007b7b7b00b5b5b500e6e6e600eaeaea00e2e2e2000829de004263' +
'e7007b94ef00a5b5f700b5bdf700dedeff00d6deff00bdc6ff00b5c6ff00b5bdff00adc6ff008c' +
'9cef005a73de007373730093939300cecece00d6d6d600dadada00bdbdbd009c9c9c006b7be700' +
'c6ceff00adbdff00a5bdff00a5b5ff009cb5ff00294ade00d2d2d200cdcdcd00838383004a6be7' +
'009cadff0094adff008cadff008ca5ff009cadf700395ade00c9c9c900c5c5c5008b8b8b00a5a5' +
'a500adb5e70094a5ff0084a5ff00849cff007b9cff007b94ff00cacaca00e7e7e700ceced60039' +
'52ce007394ff00738cff006b8cff00b5adb500f7ffff006b84ff006384ff00b1b1b100fbfbfb00' +
'9c9cb500c6c6d600e7e7ff00636bd600395ae700637bff005273ff00526bff004a6bff004a73ff' +
'005a73ff00a5adff001031de00a8a8a800f3f3f300a3a3a300acacac00a5adad00fff7f700e7e7' +
'ef001839c600849cf7005a7bff00425ae700ebebeb006f6f6f00a59cb500bdb5ce00e7def7006b' +
'7bce002942de008c9cff004263ff00425aff00395aff008494ff003952e7005263d600a0a0a000' +
'9ca5a500efefef00314abd00637bef003963ff00315aff007b94f700637bf7000829d6005a6bc6' +
'00a5a5bd00adadbd00949cd6007b8cff00294aff00526bef003952ef000021de001031ce002942' +
'c6005a63c600b3b3b300a5a5ad004a63bd004263ef00214aff003152ff005a73f7000831de00b5' +
'b5ce00dedef700bdbdde006b7bf7004a63ff00425aef00d6ced6006373c6003152e7004252bd00' +
'8c8c8c00c6bdd6009494ad00d6d6ef00cecee7002139c6004a63ef00394ac600c6c6c6007b8cce' +
'00efe7ef008c94b500ada5ad00deded600eff7f700c6bdc600c6c6ce00adadb500c6cece00cec6' +
'ce0000000000000000000000000000000000000000000000000000000000000000000000000000' +
'000000000000000000000000000000000000000000000000000000000000000000000000000000' +
'000000000000000000000000000000000000000000000000000000000000000000000000000000' +
'000000000000000000000000000000000000000000000000000000000000000000000000000000' +
'000000000000000000000000000000000000000000000000000000000000000000000000000000' +
'000000000000000000000000000000000000000000000000000000020202020202020202020202' +
'0202273a020202020202020202020202020202020202020202020202020202020202273a020202' +
'0202020202020202020202020202020202020202020202020202153d53c4020202020202020202' +
'02020202020202020202020202020202020202153d531702020202020202020202020202020202' +
'0202020202020202020202c4278f5b530202020202020202020202020202020202020202020202' +
'0202020217278f5b53020202020202020202020202020202020202020202020202020202530d8f' +
'c83a020202020202020202020202020202020202020202020202020202530d8f5a3a0202020202' +
'020202020202020202020202020202020202020202273d8f5b5302020202020202020202020202' +
'0202020202020202020202020202273d8f5b530202020202020202020202020202020202020202' +
'02020202020202535b8f3d3a020202020202020202020202020202020202020202020202020202' +
'535b8f3d3a020202020202020202020202020202020202020202020202020215c78f0d27020202' +
'020202020202020202020202020202020202020202020202155a8f0d2702020202020202020202' +
'02020202020202020202020202020202c5158f8fc6020202020202020202020202020202020202' +
'0202020202020202025a158f8f6502020202020202020202020202020202020202020202020202' +
'0202c1c2c33bc4020202020202020202020202020202020202020202020202020202053c753b17' +
'02020202020202020202020202020202020202020202020202023d278f8f530202020202020202' +
'020202020202020202020202020202020202023d278f8f53020202020202020202020202020202' +
'020202020202020202020202535bbfc06102020202020202020202020202020202020202020202' +
'0202020202535b7f8d65020202020202020202020202020202020202020202020202020227bd8f' +
'be1b0a020202020202020202020202020202020202020202020202020227bd8f76260e02020202' +
'020202020202020202020202020202020202020202b6b7b8b9baa9bbbc02020202020202020202' +
'0202020202020202020202020202508d2a3c38743e060202020202020202020202020202020202' +
'0202020202020215b18fb2b35959a6b40202020202020202020202020202020202020202020202' +
'15468f39b53d3d0b0602020202020202020202020202020202020202020202ab67acad2baeaf9c' +
'63b0a10202020202020202020202020202020202020202021774294738657639270b1602020202' +
'0202020202020202020202020202020202a40d8fa5a657a7a7a859a9aa02020202020202020202' +
'02020202020202020202050d8f480b1724240b3d74380202020202020202020202020202020202' +
'02029899839a739b879c9c9c9c9b5f9d9e04239f9fa0a1a202020202020202020202652729a316' +
'3d8d393939393d0c8d3906268080801624020202020202020202028e8f8f90914d929393899393' +
'926357864d868694952c969702020202020202028d8f8f1605513e0b0b3e0b0b3e271751515151' +
'0c153980240202020202020281828384858687888989898988898989898989886f63868a8b8c02' +
'02020202027417293e48518d8d3e3e3e3e8d3e3e3e3e3e3e8d7627511724390202020202027879' +
'7a7b7c576d6f6f6f6f6f6f6f6f6d6f706f70706d7d57417e02020202020205667f803d17747676' +
'7676767676767476767676767477173b3902020202026768696a6b4a6c6d6e6d6f6e6e6e6e6e6e' +
'706e6e6f6e716c5f72730202020202743a753e2447657474747674747474747476747476747765' +
'0c461602020202026101620a414a5e6063646464646464646464646464635f5933130302020202' +
'026501660e3b470c2727656565656565656565656565270c3d3c150502020202275b015c5d3632' +
'4d5759595e5f5f5e5f5f5f605f5f5f5e57432f2b0202020202275b0146483d0d51173d3d0c0c0c' +
'0c0c0c0c270c0c0c0c17463b38020202020202530d53540421354b554d4d4d56574d5757585959' +
'57574b211c02020202020202530d535a062a3b505051515117175117173d3d3d1717502a0b0202' +
'0202020202023d02020349314041434444444a4a4b4b4c4d4d4d4a204e4f02020202020202023d' +
'0202050b292a3b464747474747505051515151470d50520202020202020202020202023f2c4031' +
'224020333335414243444444421e450202020202020202020202020205392a29282a0d3c3c3b3b' +
'46464747474628480202020202020202020202020202082b2c2d2e2f3012303131323334352236' +
'3702020202020202020202020202020c3839273a3b18181829290d3c3c3b283d3e020202020202' +
'020202020202020202020203191a0a0a1b1c1d1e1f202122230202020202020202020202020202' +
'020202020524250e0e260b2728290d2a2826020202020202020202020202020202020202020202' +
'020202020f10111212131402020202020202020202020202020202020202020202020202151617' +
'181815150202020202020202020202020202020202020202020202020202020708090a02020202' +
'0202020202020202020202020202020202020202020202020b0c0d0e0202020202020202020202' +
'020202020202020202020202020202020202030402020202020202020202020202020202020202' +
'020202020202020202020205060202020202020202020202020202020202020202020202020202' +
'020202020202020202020202020202020202020202020202020202020202020202020202020202' +
'0202020202020202040000002701ffff030000000000' +
'}\\par' +
'\\pard\\sa200\\sl276\\slmult1 Linha 4\\par' +
'Linha 5\\par' +
'\\par' +
'}';
*)
begin
  Result := True;
  try
{
    if Uppercase(Tabela) = Uppercase('?????') then
    begin
      FListaSQL.Add('Codigo|Numero|Nome|Texto');
      FListaSQL.Add('1|999|"Teste Imagem"|"'+teste+'"');
    end else
}
    All_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Anotacoes_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Enti_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Cuns_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    OS_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Empresas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    GFat_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Fiscal_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    ChekLst_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    DocsCab_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Bugs_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    PerfJan_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Finance_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    Cheques_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Moedas_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Imprime_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    //CashTb.CarregaListaSQLCashier(Tabela, FListaSQL);
    IBGE_DTB_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    NFe_Tb.CarregaListaSQL(Tabela, FListaSQL);
    Grade_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    SPEDEFD_Tb.CarregaListaSQL(Tabela, FListaSQL);
    SINTEGRA_Tb.CarregaListaSQL(Tabela, FListaSQL);
    Diario_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Bina_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Bloq_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    BloqGerl_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    CNAB_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Protocol_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Contrat_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    UnFPMin_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    //UnFPMax_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    UnGeral_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
    UnNFSe_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Arquivos_Tb.CarregaListaSQL(Tabela, FListaSQL);
    Textos_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    WUsers_Tb.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    WGerl_Tb.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    Web_Tb.CarregaListaSQL(Dmod.MyDB, Tabela, FListaSQL, CO_DMKID_APP);
    Agenda_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    //
    UnNFSe_TbTerc.CarregaListaSQL(Tabela, FListaSQL);
    //
    BugMbl_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Mail_Tb.CarregaListaSQL(Tabela, FListaSQL);
    //
    CRO_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    UMedi_Tabs.CarregaListaSQL(Tabela, FListaSQL, CO_DMKID_APP);
    Praz_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    Notificacoes_Tabs.CarregaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Contas') then
    begin
      (*FListaSQL.Add('-129,"Compra de mercadorias diversas"');
      FListaSQL.Add('-130,"Frete de mercadorias"');
      FListaSQL.Add('-131,"Compra de filmes"');
      FListaSQL.Add('-132,"Frete de filmes"');
      FListaSQL.Add('-133,"Inv�lido"');
      FListaSQL.Add('-134,"Venda e/ou loca��o"');*)
    end;
    All_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Anotacoes_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Enti_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Cuns_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Cheques_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    OS_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Empresas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    GFat_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    DocsCab_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Fiscal_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Moedas_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Finance_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    ChekLst_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Imprime_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Bugs_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    PerfJan_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Diario_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Bina_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Bloq_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    BloqGerl_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    CNAB_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Protocol_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Contrat_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnFPMin_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnNFSe_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Arquivos_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    WUsers_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    WGerl_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    Web_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    //UnFPMax_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Agenda_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    //
    UnNFSe_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
    UnGeral_TbTerc.ComplementaListaSQL(Tabela, FListaSQL);
    //
    BugMbl_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Mail_Tb.ComplementaListaSQL(Tabela, FListaSQL);
    //
    CRO_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UMedi_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Praz_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    Notificacoes_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String;
  FLIndices: TList<TIndices>): Boolean;
begin
  Result := True;
  try
{
    if Uppercase(TabelaBase) = Uppercase('????') then
    begin
      New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);
      //
    end else
}
    begin
      All_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Anotacoes_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Enti_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Cuns_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      OS_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Empresas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Cheques_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      ChekLst_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Fiscal_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      DocsCab_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Bugs_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Imprime_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      GFat_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      PerfJan_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Finance_Tabs.CarregaListaFRIndices(TabelaBase, TabelaNome, FRIndices, FLIndices);
      //CashTb.CarregaListaFRIndicesCashier(TabelaBase, TabelaNome, FRIndices, FLindices);
      IBGE_DTB_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      NFe_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Moedas_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Grade_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      SPEDEFD_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      SINTEGRA_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Diario_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Bina_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Bloq_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      BloqGerl_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      CNAB_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Protocol_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Contrat_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnFPMin_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      //UnFPMax_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnGeral_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnNFSe_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Arquivos_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      Textos_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      WUsers_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      WGerl_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Web_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Agenda_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      //
      UnNFSe_TbTerc.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      //
      BugMbl_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Mail_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      //
      CRO_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UMedi_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Praz_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      Notificacoes_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
    end;
  except
    raise;
    Result := False;
  end;
end;

procedure TMyListas.ConfiguracoesIniciais(UsaCoresRel: integer;
  AppIDtxt: String);
begin
  dmkPF.ConfigIniApp(UsaCoresRel);
  if Uppercase(AppIDtxt) = 'BUGSTROL' then
  begin
    Entities.ConfiguracoesIniciaisEntidades(CO_DMKID_APP);
  end else
    Geral.MensagemBox(
    'Database para configura��es de "CheckBox" n�o definidos!', 'Aviso!',
    MB_OK+MB_ICONWARNING)

end;

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList<TCampos>;
  var TemControle: TTemControle): Boolean;
begin
  try
    if Uppercase(Tabela) = Uppercase('controle') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
    end else
    begin
      All_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Anotacoes_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Enti_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Cuns_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      OS_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Cheques_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Empresas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Bugs_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      PerfJan_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Fiscal_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      ChekLst_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      GFat_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Moedas_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      DocsCab_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Imprime_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Finance_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //CashTb.CarregaListaFRCamposCashier(Tabela, FRCampos, FLCampos, TemControle);
      IBGE_DTB_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      NFe_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Grade_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      SPEDEFD_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, temControle);
      SINTEGRA_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Diario_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Bina_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Bloq_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      BloqGerl_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      CNAB_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Protocol_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Contrat_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnFPMin_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //UnFPMax_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnGeral_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnNFSe_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Agenda_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //
      UnNFSe_TbTerc.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Arquivos_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Textos_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      WUsers_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle, CO_DMKID_APP);
      WGerl_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle, CO_DMKID_APP);
      Web_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, CO_DMKID_APP, TemControle);
      //
      BugMbl_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Mail_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      //
      CRO_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UMedi_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Praz_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      Notificacoes_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    end;
    All_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Anotacoes_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Enti_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Cuns_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    OS_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Cheques_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    GFat_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    ChekLst_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    DocsCab_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Empresas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Fiscal_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Bugs_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Imprime_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    PerfJan_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Moedas_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Finance_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //CashTb.CompletaListaFRCamposCashier(Tabela, FRCampos, FLCampos);
    Grade_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Diario_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Bina_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Bloq_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    BloqGerl_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    CNAB_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Protocol_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Contrat_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnFPMin_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //UnFPMax_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    NFe_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnNFSe_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Agenda_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    UnNFSe_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Arquivos_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Textos_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    WUsers_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    WGerl_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Web_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnGeral_TbTerc.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    BugMbl_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Mail_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    CRO_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UMedi_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Praz_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Notificacoes_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    //
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.ExcluiTab: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiReg: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiIdx: Boolean;
begin
  Result := True;
end;

(*
function TMyListas.CriaListaUserSets(FUserSets: TStringList): Boolean;
begin
  Result := True;
  try
    FUserSets.Add('Edi��o de Itens de Mercadoria;C�digos Fiscais');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Representante');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Vendedor');
    FUserSets.Add('Edi��o de Itens de Mercadoria;IPI');
    FUserSets.Add('Edi��o de Itens de Mercadoria;ICMS');
    //
  except
    raise;
    Result := False;
  end;
end;
*)

(*
function TMyListas.CriaListaImpDOS(FImpDOS: TStringList): Boolean;
begin
  Result := True;
  try
    FImpDOS.Add('1001;01;NF (Topo)');
    FImpDOS.Add('1002;01;Sa�da [X]');
    FImpDOS.Add('1003;01;Entrada [X]');
    FImpDOS.Add('1004;01;Data emiss�o');
    FImpDOS.Add('1005;01;Data entra/sai');
    FImpDOS.Add('1006;01;C�digo CFOP');
    FImpDOS.Add('1007;01;Descri��o CFOP');
    FImpDOS.Add('1008;01;Base c�lculo ICMS');
    FImpDOS.Add('1009;01;Valor ICMS');
    FImpDOS.Add('1010;01;Base c�lc. ICMS subst.');
    FImpDOS.Add('1011;01;Valor ICMS subst.');
    FImpDOS.Add('1012;01;Valor frete');
    FImpDOS.Add('1013;01;Valor seguro');
    FImpDOS.Add('1014;01;Outras desp. aces.');
    FImpDOS.Add('1015;01;Valor total IPI');
    FImpDOS.Add('1016;01;Valor total produtos');
    FImpDOS.Add('1017;01;Valor total servicos');
    FImpDOS.Add('1018;01;Valor total nota');
    FImpDOS.Add('1019;01;Placa ve�culo');
    FImpDOS.Add('1020;01;UF placa ve�culo');
    FImpDOS.Add('1021;01;Vol. Quantidade');
    FImpDOS.Add('1022;01;Vol. Esp�cie');
    FImpDOS.Add('1023;01;Vol. Marca');
    FImpDOS.Add('1024;01;Vol. N�mero');
    FImpDOS.Add('1025;01;Vol. kg bruto');
    FImpDOS.Add('1026;01;Vol. kg l�quido');
    FImpDOS.Add('1027;01;Dados adicionais');
    FImpDOS.Add('1028;01;Frete por conta de ...');
    FImpDOS.Add('1029;01;Desconto especial');
    FImpDOS.Add('1030;01;NF (rodap�)');
    //
    FImpDOS.Add('2001;02;Nome ou Raz�o Social');
    FImpDOS.Add('2002;02;CNPJ ou CPF');
    FImpDOS.Add('2003;02;Endere�o');
    FImpDOS.Add('2004;02;Bairro');
    FImpDOS.Add('2005;02;CEP');
    FImpDOS.Add('2006;02;Cidade');
    FImpDOS.Add('2007;02;Telefone');
    FImpDOS.Add('2008;02;UF');
    FImpDOS.Add('2009;02;I.E. ou RG');
    FImpDOS.Add('2010;02;I.E.S.T.');
    //
    FImpDOS.Add('6001;06;Nome ou Raz�o Social');
    FImpDOS.Add('6002;06;CNPJ ou CPF');
    FImpDOS.Add('6003;06;Endere�o');
    FImpDOS.Add('6004;06;Bairro');
    FImpDOS.Add('6005;06;CEP');
    FImpDOS.Add('6006;06;Cidade');
    FImpDOS.Add('6007;06;Telefone');
    FImpDOS.Add('6008;06;UF');
    FImpDOS.Add('6009;06;I.E. ou RG');
    FImpDOS.Add('6010;06;I.E.S.T.');
    //
    FImpDOS.Add('3001;03;Descri��o');
    FImpDOS.Add('3002;03;Classifica��o Fiscal');
    FImpDOS.Add('3003;03;Situa��o Tribut�ria');
    FImpDOS.Add('3004;03;Unidade');
    FImpDOS.Add('3005;03;Quantidade');
    FImpDOS.Add('3006;03;Valor unit�rio');
    FImpDOS.Add('3007;03;Valor total');
    FImpDOS.Add('3008;03;Aliquota ICMS');
    FImpDOS.Add('3009;03;Aliquota IPI');
    FImpDOS.Add('3010;03;Valor IPI');
    FImpDOS.Add('3011;03;CFOP');
    FImpDOS.Add('3012;03;Refer�ncia');
    //
    FImpDOS.Add('4001;04;Descri��o');
    FImpDOS.Add('4002;04;Classifica��o Fiscal');
    FImpDOS.Add('4003;04;Situa��o Tribut�ria');
    FImpDOS.Add('4004;04;Unidade');
    FImpDOS.Add('4005;04;Quantidade');
    FImpDOS.Add('4006;04;Valor unit�rio');
    FImpDOS.Add('4007;04;Valor total');
    FImpDOS.Add('4008;04;Aliquota ICMS');
    FImpDOS.Add('4009;04;Aliquota IPI');
    FImpDOS.Add('4010;04;Valor IPI');
    FImpDOS.Add('4011;04;CFOP');
    FImpDOS.Add('4012;04;Refer�ncia');
    //
    FImpDOS.Add('5001;05;Parcela');
    FImpDOS.Add('5002;05;Valor');
    FImpDOS.Add('5003;05;Vencimento');
    //
  except
    raise;
    Result := False;
  end;
end;
*)

procedure TMyListas.VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  //
end;

procedure TMyListas.VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

procedure TMyListas.ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
  DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

function TMyListas.CriaListaJanelas(FLJanelas: TList<TJanelas>): Boolean;
begin
  //
  // ATD-TELEF-000 :: BINA
  New(FRJanelas);
  FRJanelas.ID        := 'ATD-TELEF-000';
  FRJanelas.Nome      := 'FmDirectTerminal';
  FRJanelas.Descricao := 'BINA';
  FLJanelas.Add(FRJanelas);
  //
  // ATD-TELEF-001 :: Atendimento
  New(FRJanelas);
  FRJanelas.ID        := 'ATD-TELEF-001';
  FRJanelas.Nome      := 'SAC_01';
  FRJanelas.Descricao := 'Atendimento';
  FLJanelas.Add(FRJanelas);
  //
  // ATD-TELEF-002 :: Interlocu��o
  New(FRJanelas);
  FRJanelas.ID        := 'ATD-TELEF-002';
  FRJanelas.Nome      := 'SAC_01_Add';
  FRJanelas.Descricao := 'Interlocu��o';
  FLJanelas.Add(FRJanelas);
  //
  // ATD-TELEF-003 :: Entidade
  New(FRJanelas);
  FRJanelas.ID        := 'ATD-TELEF-003';
  FRJanelas.Nome      := 'SAC_01_ER0';
  FRJanelas.Descricao := 'Entidade';
  FLJanelas.Add(FRJanelas);
  //
  // ATD-TELEF-004 :: Pr�-Atendimento
  New(FRJanelas);
  FRJanelas.ID        := 'ATD-TELEF-004';
  FRJanelas.Nome      := 'SAC_01_Pre';
  FRJanelas.Descricao := 'Pr�-Atendimento';
  FLJanelas.Add(FRJanelas);
  //
  // ATD-TELEF-005 :: Terreno
  New(FRJanelas);
  FRJanelas.ID        := 'ATD-TELEF-005';
  FRJanelas.Nome      := 'SAC_01_STC';
  FRJanelas.Descricao := 'Terreno';
  FLJanelas.Add(FRJanelas);
  //
  // GSM-TELEF-001 :: Envio de SMS
  New(FRJanelas);
  FRJanelas.ID        := 'GSM-TELEF-001';
  FRJanelas.Nome      := 'FmGSM_Serial';
  FRJanelas.Descricao := 'Envio de SMS';
  FLJanelas.Add(FRJanelas);
  //
  //
  All_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Anotacoes_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Enti_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Fiscal_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Cuns_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Cheques_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Empresas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  DocsCab_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  ChekLst_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  OS_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  GFat_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Imprime_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Moedas_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Bugs_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  PerfJan_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  IBGE_DTB_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  NFe_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Finance_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Grade_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  SPEDEFD_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  SINTEGRA_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Diario_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Bina_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Bloq_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  BloqGerl_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  CNAB_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Protocol_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Contrat_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnFPMin_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //UnFPMax_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnGeral_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnNFSe_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Agenda_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  UnNFSe_TbTerc.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Arquivos_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Textos_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  WUsers_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  WGerl_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Web_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  BugMbl_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Mail_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  CRO_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UMedi_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Praz_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  Notificacoes_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  //
  Result := True;
end;

function TMyListas.CriaListaQeiLnk(TabelaCria: String;
  FLQeiLnk: TList<TQeiLnk>): Boolean;
begin
  Result := True;
  try
(*
    if Uppercase(TabelaCria) = Uppercase('VSEntiMP') then
    begin
      New(FRQeiLnk);
      FRQeiLnk.AskrTab       := TabelaCria;
      FRQeiLnk.AskrCol       := 'Codigo';
      FRQeiLnk.RplyTab       := '';
      FRQeiLnk.RplyCol       := '';
      FRQeiLnk.Purpose       := TItemTuplePurpose.itpUSGSrvrInc;
      FRQeiLnk.Seq_in_pref   := 1;
      FLQeiLnk.Add(FRQeiLnk);
      //
      New(FRQeiLnk);
      FRQeiLnk.AskrTab       := TabelaCria;
      FRQeiLnk.AskrCol       := 'MPVImpOpc';
      FRQeiLnk.RplyTab       := 'MPVImpOpc';
      FRQeiLnk.RplyCol       := 'Codigo';
      FRQeiLnk.Purpose       := TItemTuplePurpose.itpRelatUsrSrv;
      FRQeiLnk.Seq_in_pref   := 1;
      FLQeiLnk.Add(FRQeiLnk);
      //
      //
    //end else if Uppercase(TabelaBase) = Uppercase('?) then
    //begin
    end else
*)
    begin
      All_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //ERPSinc_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Enti_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      PerfJan_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Finance_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      IBGE_DTB_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      NFe_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //CMPT_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Grade_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      SPEDEFD_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //SINTEGRA_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Mail_Tb.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      UnGeral_TbTerc.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //
      Imprime_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Praz_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
  	  Moedas_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Fiscal_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      GFat_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //GPed_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      CNAB_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Anotacoes_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      Empresas_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //Tributos_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      UMedi_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //ConsumoGerl_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //Consumo_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //VS_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
      //PQ_Tabs.CarregaListaFRQeiLnk(TabelaCria, FRQeiLnk, FLQeiLnk);
    end;
  except
    raise;
    Result := False;
  end;
end;

end.
