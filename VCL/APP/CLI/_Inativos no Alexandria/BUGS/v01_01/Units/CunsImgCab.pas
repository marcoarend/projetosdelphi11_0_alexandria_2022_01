unit CunsImgCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, UnInternalConsts, dmkMemo, ComCtrls, dmkRadioGroup,
  dmkImage, UnDmkEnums;

type
  TFmCunsImgCab = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    DBEdCodigo: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrStatus: TmySQLQuery;
    DsStatus: TDataSource;
    QrStatusCodigo: TIntegerField;
    QrStatusNome: TWideStringField;
    EdNO_ENT: TdmkEdit;
    QrDependencias: TmySQLQuery;
    QrDependenciasNO_COMPLETO: TWideStringField;
    QrDependenciasControle: TIntegerField;
    DsDependencias: TDataSource;
    QrCunsImgNCT: TmySQLQuery;
    DsCunsImgNCT: TDataSource;
    QrCunsImgNCTCodigo: TIntegerField;
    QrCunsImgNCTNome: TWideStringField;
    Panel3: TPanel;
    Label6: TLabel;
    EdControle: TdmkEdit;
    Label7: TLabel;
    EdNome: TdmkEdit;
    Label2: TLabel;
    EdCaminho: TdmkEdit;
    SBCaminho: TSpeedButton;
    Label1: TLabel;
    EdDependencia: TdmkEditCB;
    CBDependencia: TdmkDBLookupComboBox;
    PageControl1: TPageControl;
    Panel5: TPanel;
    CBStatus: TdmkDBLookupComboBox;
    SbStatus: TSpeedButton;
    EdStatus: TdmkEditCB;
    Label5: TLabel;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    RGAplicacao: TdmkRadioGroup;
    Panel6: TPanel;
    Label8: TLabel;
    SbCunsImgNCT: TSpeedButton;
    EdCodiNCT: TdmkEditCB;
    CBCodiNCT: TdmkDBLookupComboBox;
    MeObserv: TdmkMemo;
    EdOrdem: TdmkEdit;
    Label9: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbDependenciaClick(Sender: TObject);
    procedure SBCaminhoClick(Sender: TObject);
    procedure SbStatusClick(Sender: TObject);
    procedure SbCunsImgNCTClick(Sender: TObject);
    procedure RGAplicacaoClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenQrITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
    //
    procedure ReopenDependencias(Cliente: Integer);
  end;

  var
  FmCunsImgCab: TFmCunsImgCab;

implementation

uses UnMyObjects, Module, UMySQLModule, MyDBCheck, DmkDAC_PF, UnAppListas,
Principal, CfgCadLista, ModOS;

{$R *.DFM}

procedure TFmCunsImgCab.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Status, Dependencia, CodiNCT, Aplicacao, Ordem: Integer;
  Nome, Caminho, Observ: String;
begin
  if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  Nome        := EdNome.Text;
  Caminho     := EdCaminho.Text;
  Status      := EdStatus.ValueVariant;
  Dependencia := EdDependencia.ValueVariant;
  //
  Aplicacao   := RGAplicacao.ItemIndex;
  Observ      := MeObserv.Text;
  CodiNCT     := EdCodiNCT.ValueVariant;
  //
  Ordem       := EdOrdem.ValueVariant;
  //
  Codigo      := Geral.IMV(DBEdCodigo.Text);
  Controle    := EdControle.ValueVariant;
  Controle    := UMyMod.BPGS1I32('cunsimgcab', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'cunsimgcab', False, [
  'Codigo', 'Nome', 'Status',
  'Caminho', 'Dependencia', 'CodiNCT',
  'Observ', 'Aplicacao', 'Ordem'], [
  'Controle'], [
  Codigo, Nome, Status,
  Caminho, Dependencia, CodiNCT,
  Observ, Aplicacao, Ordem], [
  Controle], True) then
  begin
    ReopenQrITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType            := stIns;
      EdControle.ValueVariant    := 0;
      EdStatus.ValueVariant      := 0;
      CBStatus.KeyValue          := Null;
      EdDependencia.ValueVariant := 0;
      CBDependencia.KeyValue     := Null;
      EdCaminho.ValueVariant     := '';
      EdNome.ValueVariant        := '';
      //
      EdOrdem.ValueVariant       := DmModOS.ProximaOrdemCunsImgCab(Codigo);
      //
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmCunsImgCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCunsImgCab.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmCunsImgCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrStatus, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrCunsImgNCT, Dmod.MyDB);
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmCunsImgCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCunsImgCab.ReopenQrITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    FQrIts.Open;
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmCunsImgCab.ReopenDependencias(Cliente: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDependencias, Dmod.MyDB, [
  'SELECT CONCAT(stc.Nome, " (", sic.SCompl2, ") ", dep.Nome) NO_COMPLETO,',
  'stc.Nome NO_LUGAR, sic.SCompl2 NO_LOCAL, ',
  'dep.Nome NO_DEP, sid.Controle, sid.Dependenci ',
  'FROM siapimadep sid',
  'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci',
  'LEFT JOIN siapimacad sic ON sic.Codigo=sid.Codigo',
  'LEFT JOIN siaptercad stc ON stc.Codigo=sic.SiapImaTer',
  'WHERE stc.Cliente=' + Geral.FF0(Cliente),
  '']);
  // colocoar no LocF7SQL
  CBDependencia.LocF7SQLMasc := CO_JOKE_SQL;
  CBDependencia.LocF7SQLText.Text := Geral.ATS([
  'SELECT sid.Controle _Codigo, ',
  'CONCAT(stc.Nome, " (", sic.SCompl2, ") ", dep.Nome) _Nome ',
  'FROM siapimadep sid ',
  'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci ',
  'LEFT JOIN siapimacad sic ON sic.Codigo=sid.Codigo ',
  'LEFT JOIN siaptercad stc ON stc.Codigo=sic.SiapImaTer ',
  'WHERE stc.Cliente=' + Geral.FF0(Cliente),
  'AND dep.Nome LIKE "%' + CO_JOKE_SQL + '%"',
  '']);
end;

procedure TFmCunsImgCab.RGAplicacaoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := RGAplicacao.ItemIndex;
end;

procedure TFmCunsImgCab.SBCaminhoClick(Sender: TObject);
var
  Host, SDir, RDir, DirDest: String;
  Empresa: Integer;
begin
  Host := Dmod.QrOpcoesBugsCunsIPv4.Value;
  SDir := Dmod.QrOpcoesBugsCunsSDir.Value;
  Empresa := 0;
  RDir := '';
  //
  if MyObjects.TentaDefinirDiretorio(Host, SDir, Empresa, RDir, LaAviso1,
    LaAviso2, False, nil, DirDest) then
  begin
    MyObjects.DefineArquivo2(Self, EdCaminho, DirDest, EdCaminho.Text, ppInfo_Ini, True);
    if Pos(DirDest, EdCaminho.Text) = 1 then
    begin
      EdCaminho.Text := Copy(EdCaminho.Text, Length(DirDest) + 1);
    end;
  end;
end;

procedure TFmCunsImgCab.SbCunsImgNCTClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormCunsImgNCT(EdCodiNCT.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(
      EdCodiNCT, CBCodiNCT, QrCunsImgNCT, VAR_CADASTRO);
end;

procedure TFmCunsImgCab.SbDependenciaClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  UnCfgCadLista.MostraCadItemGrupo(Dmod.MyDB, 'CunsImgSit',
    AppListas.ListaNiveisSituacoesFotosClientes(), ncGerlSeq1);
  UMyMod.SetaCodigoPesquisado(EdDependencia, CBStatus, QrStatus, VAR_CADASTRO);
end;

procedure TFmCunsImgCab.SbStatusClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  UnCfgCadLista.MostraCadItemGrupo(Dmod.MyDB, 'CunsImgSit',
    AppListas.ListaNiveisSituacoesFotosClientes(), ncGerlSeq1);
  UMyMod.SetaCodigoPesquisado(EdStatus, CBStatus, QrStatus, VAR_CADASTRO);
end;

end.
