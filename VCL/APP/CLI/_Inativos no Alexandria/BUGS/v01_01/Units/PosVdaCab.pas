unit PosVdaCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, dmkGeral,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt, UMySQLModule,
  UnInternalConsts2, mySQLDbTables, UnMySQLCuringa, dmkPermissoes, dmkEdit,
  dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup, Menus, Grids, DBGrids,
  UnAppListas, UnDmkProcFunc, UnDmkEnums, UnProjGroup_Consts;

type
  TFmPosVdaCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdCodigo: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    QrPosVdaCab: TmySQLQuery;
    DsPosVdaCab: TDataSource;
    QrPosVdaIts: TmySQLQuery;
    DsPosVdaIts: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    DGDados: TDBGrid;
    QrPosVdaItsCodigo: TIntegerField;
    QrPosVdaItsControle: TIntegerField;
    QrPosVdaItsDias: TIntegerField;
    QrPosVdaItsAplicacao: TIntegerField;
    QrPosVdaItsAplicID: TIntegerField;
    QrPosVdaCabCodigo: TIntegerField;
    QrPosVdaCabNome: TWideStringField;
    QrPosVdaItsNO_AplicID: TWideStringField;
    QrPosVdaItsNO_Aplicacao: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrPosVdaCabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPosVdaCabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure QrPosVdaCabAfterScroll(DataSet: TDataSet);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraPosVdaIts(SQLType: TSQLType);

  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
    procedure ReopenPosVdaIts(Controle: Integer);

  end;

var
  FmPosVdaCab: TFmPosVdaCab;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, MyDBCheck, DmkDAC_PF, PosVdaIts;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmPosVdaCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmPosVdaCab.MostraPosVdaIts(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmPosVdaIts, FmPosVdaIts, afmoNegarComAviso) then
  begin
    FmPosVdaIts.ImgTipo.SQLType := SQLType;
    FmPosVdaIts.FQrCab := QrPosVdaCab;
    FmPosVdaIts.FDsCab := DsPosVdaCab;
    FmPosVdaIts.FQrIts := QrPosVdaIts;
    if SQLType = stIns then
      //
    else
    begin
      FmPosVdaIts.EdControle.ValueVariant := QrPosVdaItsControle.Value;
      FmPosVdaIts.EdDias.ValueVariant := QrPosVdaItsDias.Value;
      FmPosVdaIts.RGAplicacao.ItemIndex := QrPosVdaItsAplicacao.Value;
      FmPosVdaIts.EdAplicID.ValueVariant := QrPosVdaItsAplicID.Value;
      FmPosVdaIts.CBAplicID.KeyValue := QrPosVdaItsAplicID.Value;
    end;
    FmPosVdaIts.ShowModal;
    FmPosVdaIts.Destroy;
  end;
end;

procedure TFmPosVdaCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrPosVdaCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrPosVdaCab, QrPosVdaIts);
end;

procedure TFmPosVdaCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrPosVdaCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrPosVdaIts);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrPosVdaIts);
end;

procedure TFmPosVdaCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPosVdaCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmPosVdaCab.DefParams;
begin
  VAR_GOTOTABELA := 'posvdacab';
  VAR_GOTOMYSQLTABLE := QrPosVdaCab;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM posvdacab');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmPosVdaCab.ItsAltera1Click(Sender: TObject);
begin
  MostraPosVdaIts(stUpd);
end;

procedure TFmPosVdaCab.CabExclui1Click(Sender: TObject);
begin
  Geral.MB_Aviso('A��o n�o implementada! Solicite � Dermatek:' + sLineBreak +
  Caption + sLineBreak + TMenuItem(Sender).Name);
end;

procedure TFmPosVdaCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmPosVdaCab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmPosVdaCab.ItsExclui1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'posvdaits', 'Controle', QrPosVdaItsControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrPosVdaIts,
      QrPosVdaItsControle, QrPosVdaItsControle.Value);
    ReopenPosVdaIts(Controle);
  end;
end;

procedure TFmPosVdaCab.ReopenPosVdaIts(Controle: Integer);
var
  Acao: String;
begin
  Acao := dmkPF.ArrayToTexto('pvi.Aplicacao', 'NO_Aplicacao', pvPos, True,
    sListaAcaoPosVda);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrPosVdaIts, Dmod.MyDB, [
  'SELECT IF(Aplicacao=2, pre.Nome, "") NO_AplicID, ',
//  'ELT(Aplicacao+1, "Nenhuma", "Telefonar", "Enviar email") NO_Aplicacao, ',
  Acao,
  'pvi.* ',
  'FROM posvdaits pvi ',
  'LEFT JOIN preemail pre ON pre.Codigo=pvi.AplicID ',
  'WHERE pvi.Codigo=' + Geral.FF0(QrPosVdaCabCodigo.Value),
  'ORDER BY pvi.Dias, pvi.Aplicacao ',
  '']);
  //
  QrPosVdaIts.Locate('Controle', Controle, []);
end;


procedure TFmPosVdaCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmPosVdaCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmPosVdaCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmPosVdaCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmPosVdaCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmPosVdaCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPosVdaCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrPosVdaCabCodigo.Value;
  Close;
end;

procedure TFmPosVdaCab.ItsInclui1Click(Sender: TObject);
begin
  MostraPosVdaIts(stIns);
end;

procedure TFmPosVdaCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrPosVdaCab, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'posvdacab');
end;

procedure TFmPosVdaCab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BPGS1I32('posvdacab', 'Codigo', '', '',
    tsPos, ImgTipo.SQLType, QrPosVdaCabCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita, 'posvdacab',
  Codigo, Dmod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    if FSeq = 1 then Close;
  end;
end;

procedure TFmPosVdaCab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := EdCodigo.ValueVariant;
  if ImgTipo.SQLType = stIns then
    UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'posvdacab', Codigo);
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'posvdacab', 'Codigo');
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmPosVdaCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmPosVdaCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmPosVdaCab.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  DGDados.Align := alClient;
  CriaOForm;
  FSeq := 0;
end;

procedure TFmPosVdaCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrPosVdaCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPosVdaCab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmPosVdaCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrPosVdaCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmPosVdaCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmPosVdaCab.QrPosVdaCabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmPosVdaCab.QrPosVdaCabAfterScroll(DataSet: TDataSet);
begin
  ReopenPosVdaIts(0);
end;

procedure TFmPosVdaCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrPosVdaCabCodigo.Value <> FCabIni then Geral.MB_Aviso(
    'Grupo n�o localizado!');
    FLocIni := True;
  end;
end;

procedure TFmPosVdaCab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrPosVdaCabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'posvdacab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmPosVdaCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPosVdaCab.CabInclui1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrPosVdaCab, [PnDados],
    [PnEdita], EdNome, ImgTipo, 'posvdacab');
end;

procedure TFmPosVdaCab.QrPosVdaCabBeforeOpen(DataSet: TDataSet);
begin
  QrPosVdaCabCodigo.DisplayFormat := FFormatFloat;
end;

end.

