unit SiapImaDep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, dmkMemo, Variants, UnDmkEnums;

type
  TFmSiapImaDep = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    CkContinuar: TCheckBox;
    QrDependenci: TmySQLQuery;
    DsDependenci: TDataSource;
    CBDependenci: TdmkDBLookupComboBox;
    SBDependenci: TSpeedButton;
    EdDependenci: TdmkEditCB;
    LaPrompt: TLabel;
    QrDependenciCodigo: TIntegerField;
    QrDependenciNome: TWideStringField;
    EdMLarg: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdMComp: TdmkEdit;
    Label3: TLabel;
    EdMAltu: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBDependenciClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodiAtual, FCtrlAtual: Integer;
    FQrySiapImaDep: TmySQLQuery;
  end;

  var
  FmSiapImaDep: TFmSiapImaDep;

implementation

uses UnMyObjects, Module, UMySQLModule, CfgCadLista, Principal;

{$R *.DFM}

procedure TFmSiapImaDep.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Dependenci: Integer;
  //M2Area,
  MLarg, MComp, MAltu: Double;
begin
  Codigo         := FCodiAtual;
  //Controle       := FCtrlAtual;
  //
  Dependenci     := EdDependenci.ValueVariant;
  MLarg          := EdMLarg.ValueVariant;
  MComp          := EdMComp.ValueVariant;
  MAltu          := EdMAltu.ValueVariant;
  //
  Controle :=
    UMyMod.BPGS1I32('siapimadep', 'Controle', '', '', tsPos, ImgTipo.SQLType, FCtrlAtual);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'siapimadep', False, [
  'Codigo', 'Dependenci',
  'MLarg', 'MComp', 'MAltu'], [
  'Controle'], [
  Codigo, Dependenci,
  MLarg, MComp, MAltu], [
  Controle], True) then
  begin
    if FQrySiapImaDep <> nil then
    begin
      FQrySiapImaDep.Close;
      FQrySiapImaDep.Open;
      FQrySiapImaDep.Locate('Controle', Controle, []);
    end;
    if CkContinuar.Checked then
    begin
      EdDependenci.ValueVariant := 0;
      CBDependenci.KeyValue := Null;
      //
      EdMLarg.ValueVariant := 0;
      EdMComp.ValueVariant := 0;
      EdMAltu.ValueVariant := 0;
      //
      EdDependenci.SetFocus;
    end else Close;
  end;
end;

procedure TFmSiapImaDep.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSiapImaDep.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSiapImaDep.FormCreate(Sender: TObject);
begin
  QrDependenci.Open;
end;

procedure TFmSiapImaDep.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSiapImaDep.SBDependenciClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormDependenci();
  UMyMod.SetaCodigoPesquisado(EdDependenci, CBDependenci, QrDependenci, VAR_CADASTRO);
end;

end.
