unit UnOSApp_PF;

interface

uses
  Windows, SysUtils, Classes, DB, mySQLDbTables, Forms,
  DmkGeral, DmkDAC_PF, UnAppListas, DBGrids, Variants,
  UnInternalConsts, UnDmkProcFunc, frxClass, frxDBSet, UnBugs_Tabs, Menus,
  dmkDBLookupComboBox, dmkEditCB, UnDmkEnums, UnProjGroup_Consts;

type
  TTipoMonit = (istosfrmflhcb, istosmoncab);
  TMostraFormOSCab2IDSrc = (mfoisPesq, mfoisGrupo, mfoisCodigo, mfoisServico,
    mfoisOSMonCab);
  TOSBugsDuplic = (dupNenhum, dupPorLugar, dupPorOpcao, dupAmbos);
  TUnOSApp_PF = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
{
    procedure AdicionaPIPsAtivosEmOS(QrOSPipMon: TmySQLQuery;
              OS, Cliente, Lugar: Integer);
}
    procedure AdicionaPMVsEmOSsFuturas(SrvControle, SiapTerCad: Integer;
              DtaExeFim: TDateTime; EstaOS: Integer);
    procedure AtualizaExtenDdOS(Codigo: Integer; ResetaPosGerou: Boolean);
    procedure AtualizaPercentualFormulaExecutado(Conta, Controle: Integer);
    procedure AtualizaPercentualServicoExecutado(Controle: Integer);
    procedure ChecaOSStaDeStatusDefinido(Codigo, Status: Integer);
    function  ContinuaSemDefinicaoDeParzoDePagamento(Proforma: Integer;
              Avisa: Boolean): Boolean;
    procedure CopiaItensDaFormulaBase(Servico: TGraBugsServi;
              Formula, Codigo, Controle, Conta: Integer; Monitora: Boolean);
    procedure DefineTemProdutoDiluente(BugsServi: TGraBugsServi;
              Qry: TmySQLQuery);
    //function  EquipeDeAgentes(Codigo: Integer; _Qry0, _Qry2: TmySQLQuery): Boolean;
    procedure DiasMinEMaxNextExtenMonit(const OSCab: Integer; var DiasMin,
              DiasMax: Integer);
    function  FiltroGrade(const Servico: TGraBugsServi; const Material:
              TGraBugsMater; var Filtro: String): Boolean;
    procedure CriaItensDeAgentes(OSCab, AgeEqiCab: Integer);
    function  IncluiOSMonPipAtual(OSCab, SiapterCad, OSFMCbMae,
              PipCad, Ordem, PrgLstCab: Integer): Boolean;
    procedure InsAltOSCabAlv(SQLType: TSQLType; QrSelect, QrInsUpd: TmySQLQuery;
              Codigo: Integer);
    //
    procedure MostraFormOSCab2(MostraFormOSCab2IDSrcIDSrc: TMostraFormOSCab2IDSrc;
              IDIndex: Integer);
    procedure MostraFormOSPrvAdd(SQLType: TSQLType; Codigo, Controle: Integer;
              QrOSPrv: TmySQLQuery;
              Ins_Cliente, Ins_Contato, Ins_Agente, Ins_FormContat: Integer);
    procedure MostraFormOSPrz(SQLType: TSQLType; QrOSCab, QrOsPrz: TmySQLQuery;
              ValorIni, ValorFim, Porcento: Double);
    procedure MostraFormOSCabXtr(SQLType: TSQLType; QrOSCab, QrOsCabXtr: TmySQLQuery);
    procedure MostraFormOSPrvSel(SQLType: TSQLType; Codigo: Integer; QrOSPrv:
              TmySQLQuery; Cliente: Integer);
    //
    function  NomeDeOperacao(ConjuntoDeOperacoes: Integer): String;
    function  OSOrigDesnecessaria(FatoGeradr: Integer): Boolean;
    procedure OSRapida();
    //function  PesquisaOSOrigem(const Empresa, Cliente, Lugar, FatoGeradr: Integer;
    //          var OSOrigem: Integer): Boolean;
    function  RatificaConsumoOS_PreAnalise(Nivel: TNivelRatifConsumoOS;
              IDNivel, FatID: Integer; Pergunta: Boolean): Boolean;
    procedure RecalculaValoresDeCondicoes(Codigo: Integer);
    //
    procedure ReopenEntiContat(QrEntiContat: TmySQLQuery; SubCliente,
              Contratante, Pagante: Integer);
    procedure ReopenEquipAplic(Qry: TmySQLQuery; Servico: TGraBugsServi);
    procedure ReopenEntiMail(QrEntiMail: TmySQLQuery; EntiContat, Conta: Integer);
    procedure ReopenEntiTel(QrEntiTel: TmySQLQuery; EntiContat, Conta: Integer);
    procedure ReopenLctFatRef(QrLctFatRef: TmySQLQuery; Controle, Conta: Integer);
    procedure ReopenLocOS_Idx(QrLocOS: TmySQLQuery; Codigo: Integer);
    procedure ReopenLocOS_Mul(QrLocOS: TmySQLQuery; Grupo, Opcao, SiapTerCad: Integer);
    procedure ReopenOSAge(QrOSAge: TmySQLQuery; Codigo, Controle: Integer);
    procedure ReopenOSAlv(QrOSAlv: TmySQLQuery; Controle, Conta: Integer;
              Database: TmySQLDatabase);
    procedure ReopenOSCabAlv(QrOSCAbAlv: TmySQLQuery; Codigo, Controle: Integer);
    procedure ReopenOSCabXtr(QrOSCAbXtr: TmySQLQuery; Codigo, Controle: Integer);
    procedure ReopenOSCab(QrOSCab: TmySQLQuery; Codigo: Integer);
    procedure ReopenOSChk(QrOSChk: TmySQLQuery; Codigo, Controle: Integer);
    procedure ReopenOSCxa(QrOSCxa: TmySQLQuery; Codigo, Controle: Integer);
    procedure ReopenOSCxI(QrOSCxI: TmySQLQuery; Codigo, Controle: Integer);
    procedure ReopenOSExtMon(QrOSExtMon: TmySQLQuery; Codigo, Controle: Integer);
    procedure ReopenOSFrmAbr(QrOSFrmAbr: TmySQLQuery; Conta, IDIts: Integer);
    procedure ReopenOSFrmCab(QrOSFrmCab: TmySQLQuery; Controle, Conta: Integer);
    procedure ReopenOSFrmDep(QrOSFrmDep: TmySQLQuery; Conta, IDIts: Integer);
    procedure ReopenOSFrmRec(QrOSFrmRec: TmySQLQuery; Conta, IDIts: Integer);
    procedure ReopenOSFrmEvo(QrOSFrmEvo: TmySQLQuery; Conta, IDIts: Integer);
    procedure ReopenOSFrmFlhCb(QrOSFrmFlhCb: TmySQLQuery; Conta, IDIts: Integer);
    procedure ReopenOSFrmFlhDd(QrOSFrmFlhDd: TmySQLQuery; IDIts, IDIt2: Integer);
    procedure ReopenOSMonCab(QrOSMonCab: TmySQLQuery; Controle, Conta: Integer);
    //procedure _Reopen_OS_Mon_Dep_(_Qr_OS_Mon_Dep_: TmySQLQuery; Conta, IDIts: Integer);
    procedure ReopenOSMonRec(QrOSMonRec: TmySQLQuery; Conta, IDIts: Integer);
    procedure ReopenOSMonPipDd(QrOSMonPipDd: TmySQLQuery; Conta, IDIts: Integer);
    //procedure ReopenOSOriPsq(QrPsq: TmySQLQuery;
    //          Empresa, Entidade, SiapTerCad, FatoGeradr, OSOrigem: Integer;
    //          IncluiOSOrigem: Boolean);
    procedure ReopenOSEPI(Query: TmySQLQuery; Tabela: String; Conta, IDIts: Integer);
    procedure ReopenOSOSCxaAtrib(QrOSCxaAtrib: TmySQLQuery; Caixa: Integer);
    procedure ReopenOSPipMon(QrOSPipMon: TmySQLQuery; Codigo, Controle: Integer);
    procedure ReopenOSPipIts(QrOSPipIts: TmySQLQuery; Controle, Conta: Integer);
    procedure ReopenOSPos(QrOSPos: TmySQLQuery; Codigo, Controle: Integer);
    procedure ReopenOSPrz(QrOSPrz: TmySQLQuery; Codigo, Controle: Integer);
    procedure ReopenOSSrv(QrOSSrv: TmySQLQuery; Codigo,  Controle: Integer);
    procedure ReopenOSSta(QrOSSta: TmySQLQuery; Codigo,  Controle: Integer);
    procedure ReopenOSPrv(QrOSPrv: TmySQLQuery; Codigo, Controle: Integer);
    procedure ReopenSiapTerCad(QrSiapTerCad: TmySQLQuery; Cliente, Codigo:
              Integer; EdSiapTerCad: TdmkEditCB; CBSiapTerCad:
              TdmkDBLookupComboBox);
    //
    function  StartaInclusaoOS(Cliente, Grupo: Integer; var Lugar, Opcao:
              Integer; OQueSel: TOSBugsDuplic): Boolean;
    function  TemProvidenciaOrfa(Cliente: Integer): Boolean;
    procedure TotalValorOS(Codigo: Integer);
    function  TotalValorServico(Calc, Info, Desc: Double): Double;
    function  VeSeCriaOSsFuturas(Codigo: Integer): Boolean;
    procedure ZeraDiluente(BugsServi: TGraBugsServi; Receita: Integer;
              TempTb: String = '');
    function  AlteraDataDaUltimaEmissoDeFormulasFilhas(Data: TDateTime;
              ID: Integer; TipoMonit: TTipoMonit): Boolean;
    procedure IncluiEPI(QueryEPI: TmySQLQuery; Codigo, Controle, Conta: Integer;
              Tabela: String);
    procedure ExcluiEPI(QueryEPI: TmySQLQuery; DBGFormulEPI: TDBGrid;
              Tabela: String; Conta: Integer);
  end;

var
  OSApp_PF: TUnOSApp_PF;


implementation

uses Module, ModuleGeral, MyDBCheck, UMySQLModule, OsPrz, OSCabAlv, ModOS,
  OSNew, OSCabXtr, OSPrvAdd, OSPrvSel, ContratPsq1, OSOriPsq, OSMonCabFut,
  UnMyObjects, OSPsq, Principal, OSCab2, OSRapida;

{ TUnOSApp_PF }

{
procedure TUnOSApp_PF.AdicionaPIPsAtivosEmOS(QrOSPipMon: TmySQLQuery;
OS, Cliente, Lugar: Integer);
  procedure InsereItemAtual();
  const
    MotDesativ = 0;
    DtaDesativ = '0000-00-00';
  var
    PipCad, PrgLstCab, Controle, Codigo: Integer;
  begin
    Codigo    := OS;
    PipCad    := DModG.QrSelCodsNivel1.Value;
    PrgLstCab := DModG.QrSelCodsNivel2.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrLo2PIP, Dmod.MyDB, [
    'SELECT * ',
    'FROM ospipmon ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND PipCad=' + Geral.FF0(PipCad),
    '']);
    if DmModOS.QrLo2PIP.RecordCount = 0 then
    begin
      Controle := UMyMod.BPGS1I32('ospipmon', 'Controle', '', '', tsPos, stIns, 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ospipmon', False, [
      'Codigo', 'PipCad', 'PrgLstCab'], [
      'Controle'], [
      Codigo, PipCad, PrgLstCab], [
      Controle], True) then
      begin
      end;
      //
    end;
  end;
const
  Aviso  = '...';
  Titulo = 'Sele��o de PIPs a Monitorar';
  Prompt = 'Seleciones os PIPs dispon�veis:';
  //Campo  = 'Descricao';
  //
  DestTab = 'ospipmon';
  DestMaster = 'Codigo';
  DestDetail = 'Controle';

  DestSelec1 = 'PipCad';
  SorcField  = 'PipCad';
  ExcluiAnteriores = False;
var
  ValrMaster: Integer;
begin
  ValrMaster := OS;
  if DBCheck.EscolheCodigosMultiplos_0(
  Aviso, Titulo, Prompt, nil, 'Ativo', 'Nivel1', 'Nome', [
  'DELETE FROM _selcods_; ',
  'INSERT INTO _selcods_ ',
  'SELECT pip.Codigo Nivel1, pip.PrgLstCab Nivel2, ',
  '0 Nivel3, 0 Nivel4, 0 Nivel5, pip.Nome, 0 Ativo ',
  'FROM ' + TMeuDB + '.pipcad pip ',
  'LEFT JOIN ' + TMeuDB + '.osmoncab omc ON omc.Conta=pip.OSMonCab ',
  'LEFT JOIN ' + TMeuDB + '.oscab    osc ON osc.Codigo=omc.Codigo ',
  'WHERE pip.MotDesativ=0 ',
  'AND pip.DtaDesativ < "1900-01-01" ',
  'AND osc.Entidade=' + Geral.FF0(Cliente),
  'AND osc.SiapTerCad=' + Geral.FF0(Lugar),
  ''],[
  'SELECT * FROM _selcods_ ',
  'ORDER BY Nome;',
  ''], (*DestTab, DestMaster, DestDetail, DestSelec1, ValrMaster,
  DmModOS.QrOSAge, SorcField, ExcluiAnteriores,*) Dmod.QrUpd) then
  begin
    DModG.QrSelCods.First;
    while not DModG.QrSelCods.Eof do
    begin
      if DModG.QrSelCodsAtivo.Value = 1 then
        InsereItemAtual();
      DModG.QrSelCods.Next;
    end;
    //
    UnOSApp_PF.ReopenOSPIPMon(QrOSPipMon, OS, 0);
  end;
end;
}

procedure TUnOSApp_PF.AdicionaPMVsEmOSsFuturas(SrvControle, SiapTerCad: Integer;
  DtaExeFim: TDateTime; EstaOS: Integer);
begin
  if DtaExeFim > 2 then
  begin
    if DBCheck.CriaFm(TFmOSMonCabFut, FmOSMonCabFut, afmoNegarComAviso) then
    begin
      FmOSMonCabFut.ReopenNovosPMVs(SrvControle);
      FmOSMonCabFut.ReopenOSsFuturas(SiapTerCad, DtaExeFim, EstaOS);
      //
      FmOSMonCabFut.ShowModal;
      FmOSMonCabFut.Destroy;
    end;
  end else
    Geral.MB_Aviso('Data final de execu��o n�o informada! ' + sLineBreak +
    'N�o ser� poss�vel adicionar novos PMVs em OSs de monitoramento futuras!' +
    sLineBreak + 'Encerre a OS e tente novamente!');
end;

function TUnOSApp_PF.AlteraDataDaUltimaEmissoDeFormulasFilhas(Data: TDateTime;
  ID: Integer; TipoMonit: TTipoMonit): Boolean;
var
  Dta: TDateTime;
  EmisUltDta, Tabela, IdCampo: String;
begin
  Result := False;
  Dta    := Data;
  //
  if not DBCheck.ObtemData(Data, Data, 0) then
    Exit;
  //
  EmisUltDta := Geral.FDT(Data, 1);
  //
  case TipoMonit of
    istosfrmflhcb:
    begin
      Tabela  := 'osfrmflhcb';
      IdCampo := 'IDIts';
    end;
    istosmoncab:
    begin
      Tabela  := 'osmoncab';
      IdCampo := 'Conta';
    end;
  end;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False,
    ['EmisUltDta'], [IdCampo], [EmisUltDta], [ID], True)
  then
    Result := True;
end;

procedure TUnOSApp_PF.AtualizaExtenDdOS(Codigo: Integer; ResetaPosGerou: Boolean);
const
  PosGerou= 0;
var
  //ExtenDd, DiasMin, DiasMax, PosGerou: Integer;
  Qry: TmySQLQuery;
  DtaExeFim, DtaFinal: TDateTime;
  TotalDd: Integer;
  s: String;
  //
  procedure AtualizaTabela(Tabela: String);
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
    'UPDATE ' + Tabela + ' SET ',
    //'ExtenDd = ' + Geral.FF0(TotalDd) + ' - PerioDd ',
    'ExtenDd=IF(' + s + '<PerioDd, 0, ' + s + '-PerioDd) ',
    //
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
  end;
  procedure ReabreEmisStatus(Tabela: String);
  begin
    UnDmkDAC_PF.ExecutaMySQLQuery0(Qry, Dmod.MyDB, [
    'UPDATE ' + Tabela + ' SET ',
    'EmisStatus=3 ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
  end;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DtaExeFim ',
    'FROM oscab ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    DtaExeFim := Trunc(Qry.FieldByName('DtaExeFim').AsDateTime);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(DtaFinal) DtaFinal ',
    'FROM osextmon ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    DtaFinal := Trunc(Qry.FieldByName('DtaFinal').AsDateTime);
    //
    TotalDd := Trunc(DtaFinal - DtaExeFim);
    s := Geral.FF0(TotalDd);
    //
    AtualizaTabela('osmoncab');
    AtualizaTabela('osfrmflhcb');
    if ResetaPosGerou then
    begin
      ReabreEmisStatus('osmoncab');
      ReabreEmisStatus('osfrmflhcb');
    end;
  finally
    Qry.Free;
  end;
(*
  Errado!
  UnOSApp_PF.DiasMinEMaxNextExtenMonit(Codigo, DiasMin, DiasMax);
  ExtenDd := DiasMax;
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osmoncab', False, [
  'ExtenDd'], ['Codigo'], [
  ExtenDd], [Codigo], True);
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osfrmflhcb', False, [
  'ExtenDd'], ['Codigo'], [
  ExtenDd], [Codigo], True);
  //
*)
  if ResetaPosGerou then
  begin
    //PosGerou := 0;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
    'PosGerou'], ['Codigo'], [
    PosGerou], [Codigo], True);
  end;
end;

procedure TUnOSApp_PF.AtualizaPercentualFormulaExecutado(Conta, Controle: Integer);
var
  PercFeito: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrLastEvo, Dmod.MyDB, [
  'SELECT Codigo, PercFeito ',
  'FROM osfrmevo ',
  'WHERE Conta=' + Geral.FF0(Conta),
  'ORDER BY DataHora DESC ',
  'LIMIT 1 ',
  '']);
  PercFeito := DmModOS.QrLastEvoPercFeito.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osfrmcab', False, [
  'PercFeito'], [
  'Conta'], [
  PercFeito], [
  Conta], True) then
    AtualizaPercentualServicoExecutado(Controle);
end;

procedure TUnOSApp_PF.AtualizaPercentualServicoExecutado(Controle: Integer);
var
  TudoFeitoA: Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrEvoFrm, Dmod.MyDB, [
  'SELECT COUNT(PercFeito) ITENS, ',
  'SUM(PercFeito) SUMPERC, ',
  'SUM(PercFeito) / COUNT(PercFeito) MEDIA ',
  'FROM osfrmcab ',
  'WHERE Controle=' + Geral.FF0(Controle),
  '']);
  if DmModOS.QrEvoFrmMEDIA.Value > 99.99999999 then
    TudoFeitoA := 1
  else
    TudoFeitoA := 0;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ossrv', False, [
  'TudoFeitoA'], [
  'Controle'], [
  TudoFeitoA], [
  Controle], False) then
  begin
     { ELIMIDADO E COLOCADO CALCFIELD
     // Atualizar TudoFeitoT
     UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
     'UPDATE ossrv SET ',
     'TudoFeitoT=TudoFeitoA+TudoFeitoM ',
     'WHERE Controle=' + Geral.FF0(Controle),
     '']);
     // Corrigir caso TudoFeitoT resultou em 2 (ou mais)
     UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
     'UPDATE ossrv SET ',
     'TudoFeitoT=1',
     'WHERE Controle=' + Geral.FF0(Controle),
     'AND TudoFeitoT>1',
     '']);
     }
  end;
end;

procedure TUnOSApp_PF.ChecaOSStaDeStatusDefinido(Codigo, Status: Integer);
var
  Qry: TmySQLQuery;
  Controle: Integer;
begin
  Qry := TmySQLQuery.Create(Dmod);
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM ossta ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'AND Status=' + Geral.FF0(Status),
  '']);
  //
  if Qry.RecordCount = 0 then
  begin
    Controle := UMyMod.BPGS1I32_Reaproveita('ossta', 'Controle', '', '', tsDef, stIns, 0);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ossta', False, [
    'Codigo', 'Status'(*, 'Feito'*)], [
    'Controle'], [
    Codigo, Status(*, Feito*)], [
    Controle], True);
  end;
end;

function TUnOSApp_PF.ContinuaSemDefinicaoDeParzoDePagamento(Proforma: Integer;
  Avisa: Boolean): Boolean;
var
  Qry: TmySQLQuery;
  Msg: String;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT cab.Codigo, prz.Controle',
    'FROM oscab cab',
    'LEFT JOIN osprz prz ON prz.Codigo=cab.Codigo',
    '  AND prz.Escolhido=1',
    'WHERE cab.Grupo=' + Geral.FF0(Proforma),
    'AND prz.Codigo IS NULL',
    '']);
    if Qry.RecordCount > 0 then
    begin
      if Qry.RecordCount = 1 then
        Msg := 'Existe um localizador sem forma de pagamento definida!'
      else
        Msg := 'Existem ' + Geral.FF0(Qry.RecordCount) +
          'localizadores sem forma de pagamento definida!';
      //
      Result := Geral.MB_Pergunta(Msg + #13#10 +
      'Deseja continuar assim mesmo?') = ID_YES;
    end else
      Result := True;
  except
    Qry.Free;
  end;
end;

procedure TUnOSApp_PF.CopiaItensDaFormulaBase(Servico: TGraBugsServi; Formula,
  Codigo, Controle, Conta: Integer; Monitora: Boolean);
const
  NONE = '???';
var
  Tabela, TabIts: String;
  //
  procedure InsereItemAbr();
  var
    IDIts, Abrangicie: Integer;
  begin
    IDIts :=   UMyMod.BPGS1I32(Tabela, 'IDIts', '', '', tsDef, stIns, 0);
    Abrangicie     := DmModOS.QrFormulIAAbrangicie.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False, [
    'Codigo', 'Controle', 'Conta',
    'Abrangicie'], [
    'IDIts'], [
    Codigo, Controle, Conta,
    Abrangicie], [
    IDIts], True);
  end;
  //
  function InsereItemFiCb(): Integer;
  var
    IDIts, Formula, PerioDd, DdPostero: Integer;
  begin
    Result    := 0;
    //
    IDIts     := UMyMod.BPGS1I32(Tabela, 'IDIts', '', '', tsDef, stIns, 0);
    Formula   := DmModOS.QrFormulFiCbFormula.Value;
    PerioDd   := DmModOS.QrFormulFiCbPerioDd.Value;
    DdPostero := DmModOS.QrFormulFiCbDdPostero.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False, [
    'Codigo', 'Controle', 'Conta',
    'Formula', 'PerioDd', 'DdPostero'], [
    'IDIts'], [
    Codigo, Controle, Conta,
    Formula, PerioDd, DdPostero], [
    IDIts], True) then
      Result := IDIts;
    //
  end;
  //
  procedure InsereItemFiDd(IDIts: Integer);
  var
    IDIt2, Ordem, Dias: Integer;
  begin
    IDIt2 := UMyMod.BPGS1I32(TabIts, 'IDIt2', '', '', tsDef, stIns, 0);
    Ordem := DmModOS.QrFormulFiDdOrdem.Value;
    Dias  := DmModOS.QrFormulFiDdDias.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, TabIts, False, [
    'Codigo', 'Controle', 'Conta',
    'IDIts', 'Ordem', 'Dias'], [
    'IDIt2'], [
    Codigo, Controle, Conta,
    IDIts, Ordem, Dias], [
    IDIt2], True);
  end;
  //
  procedure InsereItemRec();
  var
    IDIts, GraGruX, Ordem(*, Reordem*): Integer;
    PrvQtd(*, PrvPrc, PrvVal, UsoQtd, UsoPrc, UsoVal, UsoDec, UsoTot*): Double;
    EhDiluente: Byte;
  begin
    IDIts :=   UMyMod.BPGS1I32(Tabela, 'IDIts', '', '', tsDef, stIns, 0);
    GraGruX        := DmModOS.QrFormulIFGraGruX.Value;
    PrvQtd         := DmModOS.QrFormulIFPrvQtd.Value;
    Ordem          := DmModOS.QrFormulIFOrdem.Value;
    EhDiluente     := DmModOS.QrFormulIFEhDiluente.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False, [
    'Codigo', 'Controle', 'Conta',
    'GraGruX', 'PrvQtd', (*'PrvPrc',
    'PrvVal', 'UsoQtd', 'UsoPrc',
    'UsoVal', 'UsoDec', 'UsoTot',*)
    'Ordem'(*, 'Reordem'*), 'EhDiluente'], [
    'IDIts'], [
    Codigo, Controle, Conta,
    GraGruX, PrvQtd, (*PrvPrc,
    PrvVal, UsoQtd, UsoPrc,
    UsoVal, UsoDec, UsoTot,*)
    Ordem(*, Reordem*), EhDiluente], [
    IDIts], True);
  end;
  //
  procedure InsereItemEPI();
  var
    IDIts, GraGruX: Integer;
  begin
    IDIts   := UMyMod.BPGS1I32(Tabela, 'IDIts', '', '', tsDef, stIns, 0);
    GraGruX := DmModOS.QrFormulEPIGraGruX.Value;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False,
      ['Codigo', 'Controle', 'Conta', 'GraGruX'], ['IDIts'],
      [Codigo, Controle, Conta, GraGruX], [IDIts], True);
  end;
var
  Pai: Integer;
begin
  case Servico of
    gbsAplica: Tabela := 'osfrmrec';
    gbsMonitora: Tabela := 'osmonrec';
    else
    begin
      Tabela := NONE;
      Geral.MB_Aviso('Tabela de itens de receita n�o definida!');
      //Result := False;
      Exit;
    end;
  end;
  if (Tabela <> NONE) and (Tabela <> '') then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrFormulIF, Dmod.MyDB, [
    'SELECT ofr.* ',
    'FROM formulif ofr ',
    'WHERE ofr.Codigo=' + Geral.FF0(Formula),
    'ORDER BY Ordem ',
    '']);
    DmModOS.QrFormulIF.First;
    while not DmModOS.QrFormulIF.Eof do
    begin
      InsereItemRec();
      //
      DmModOS.QrFormulIF.Next;
    end;
  end;

  case Servico of
    gbsAplica: Tabela := 'osfrmepi';
    gbsMonitora: Tabela := 'osmonepi';
    else
    begin
      Tabela := NONE;
      Geral.MB_Aviso('Tabela de itens de receita n�o definida!');
      //Result := False;
      Exit;
    end;
  end;
  if (Tabela <> NONE) and (Tabela <> '') then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrFormulEPI, Dmod.MyDB, [
    'SELECT epi.* ',
    'FROM formulepi epi ',
    'WHERE epi.Codigo=' + Geral.FF0(Formula),
    '']);
    DmModOS.QrFormulEPI.First;
    while not DmModOS.QrFormulEPI.Eof do
    begin
      InsereItemEPI();
      //
      DmModOS.QrFormulEPI.Next;
    end;
  end;


  //
  case Servico of
    gbsAplica: Tabela := 'osfrmabr';
    gbsMonitora: Tabela := '';
    else
    begin
      Tabela := NONE;
      Geral.MB_Aviso('Tabela de itens de abrang�ncia n�o definida!');
      //Result := False;
      Exit;
    end;
  end;
  if (Tabela <> NONE) and (Tabela <> '') then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrFormulIA, Dmod.MyDB, [
    'SELECT ofa.* ',
    'FROM formulia ofa ',
    'WHERE ofa.Codigo=' + Geral.FF0(Formula),
    '']);
    DmModOS.QrFormulIA.First;
    while not DmModOS.QrFormulIA.Eof do
    begin
      InsereItemAbr();
      //
      DmModOS.QrFormulIA.Next;
    end;
  end;

  //
  if Monitora then
  begin
    case Servico of
      gbsAplica:
      begin
        Tabela := 'osfrmflhcb';
        TabIts := 'osfrmflhdd';
      end;
      //gbsMonitora: Tabela := '';
      else
      begin
        Tabela := NONE;
        Geral.MB_Aviso('Tabela de receitas filhas n�o definida!');
        Geral.MB_Aviso('Tabela de intervalos de monitoramento n�o definida!');
        //Result := False;
        Exit;
      end;
    end;
    if (Tabela <> NONE) and (Tabela <> '') then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrFormulFiCb, Dmod.MyDB, [
      'SELECT ffc.* ',
      'FROM formulficb ffc ',
      'WHERE ffc.Codigo=' + Geral.FF0(Formula),
      //'ORDER BY Ordem, Controle ',
      'ORDER BY Controle ',
      '']);
      DmModOS.QrFormulFiCb.First;
      while not DmModOS.QrFormulFiCb.Eof do
      begin
        Pai := InsereItemFiCb();
        //
        UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrFormulFiDd, Dmod.MyDB, [
        'SELECT ffd.* ',
        'FROM formulfidd ffd ',
        'WHERE ffd.Controle=' + Geral.FF0(DmModOS.QrFormulFiCbControle.Value),
        'ORDER BY Ordem, Controle ',
        '']);
        DmModOS.QrFormulFiDd.First;
        while not DmModOS.QrFormulFiDd.Eof do
        begin
          InsereItemFiDd(Pai);
          //
          DmModOS.QrFormulFiDd.Next;
        end;
        //
        DmModOS.QrFormulFiCb.Next;
      end;
    end;
  end;
end;

procedure TUnOSApp_PF.CriaItensDeAgentes(OSCab, AgeEqiCab: Integer);
var
  Responsa, Agente, Controle, Codigo: Integer;
begin
  Codigo := OSCab;
  UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrAgeEqiIts, Dmod.MyDB, [
  'SELECT aei.Controle, aei.Entidade, aei.EhLider',
  'FROM ageeqiits aei ',
  'WHERE aei.Codigo=' + Geral.FF0(AgeEqiCab),
  '']);
  DmModOS.QrAgeEqiIts.First;
  while not DmModOS.QrAgeEqiIts.Eof do
  begin
    Responsa := DmModOS.QrAgeEqiItsEhLider.Value;
    Agente   := DmModOS.QrAgeEqiItsEntidade.Value;
    //
    Controle := UMyMod.BPGS1I32('osage', 'Controle', '', '', tsPos, stIns, 0);
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'osage', False, [
    'Codigo', 'Agente', 'Responsa'], [
    'Controle'], [
    Codigo, Agente, Responsa], [
    Controle], True);
    //
    DmModOS.QrAgeEqiIts.Next;
  end;
end;

procedure TUnOSApp_PF.DefineTemProdutoDiluente(BugsServi: TGraBugsServi;
  Qry: TmySQLQuery);
const
  Diluente = CO_COD_BUGS_DILUENTE_003;
var
  Tabela, Campo: String;
  IDTab: Integer;
begin
  case BugsServi of
    gbsAplica: Tabela := 'osfrmcab';
    gbsMonitora: Tabela := 'osmoncab';
    gbsAplEMon: Tabela := 'formulas';
    else Tabela := '???';
  end;
  case BugsServi of
    gbsAplica: Campo := 'Conta';
    gbsMonitora: Campo := 'Conta';
    gbsAplEMon: Campo := 'Codigo';
    else Campo := '???';
  end;
  IDTab := Qry.FieldByName(Campo).AsInteger;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
  'Diluente'], [Campo], [
  Diluente], [IDTab], True) then
  begin
    UnDmkDAC_PF.AbreQuery(Qry, Qry.Database);
    Qry.Locate(Campo, IDTab, []);
  end;
end;

procedure TUnOSApp_PF.DiasMinEMaxNextExtenMonit(const OSCab: Integer; var DiasMin,
DiasMax: Integer);
var
  Qry: TmySQLQuery;
  OS: String;
begin
  DiasMin := 0;
  DiasMax := 0;
  Qry   := TmySQLQuery.Create(Dmod);
  OS := Geral.FF0(OSCab);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo, Controle, Conta, PerioDd, ExtenDd, ',
    '(PerioDd + ExtenDd + 0.000) TotalDd ',
    'FROM osmoncab ',
    'WHERE Codigo=' + OS,
    ' ',
    'UNION ',
    ' ',
    'SELECT Codigo, Controle, Conta, PerioDd, ExtenDd, ',
    '(PerioDd + ExtenDd + 0.000) TotalDd ',
    'FROM osfrmflhcb ',
    'WHERE Codigo=' + OS,
    ' ',
    'ORDER BY TotalDd ',
    '']);
    //
    Qry.First;
    DiasMin := Trunc(Qry.FieldByName('TotalDd').AsFloat);
    Qry.Next;
    DiasMax := Trunc(Qry.FieldByName('TotalDd').AsFloat);
  finally
    Qry.Free;
  end;
end;

{
function TUnOSApp_PF.EquipeDeAgentes(Codigo: Integer; _Qry0, _Qry2: TmySQLQuery): Boolean;
var
  Qry0, Qry2: TmySQLQuery;
  AgeEqiCab: Integer;
begin
  if _Qry0 <> nil then
    Qry0 := _Qry0
  else
  begin
    Qry0 := TmySQLQuery.Create(Dmod);
    UnDmkDAC_PF.AbreMySQLQuery0(Qry0, Dmod.MyDB, [
    'SELECT Codigo, SUM(Entidade) SUM_AGENTES ',
    'FROM ageeqiits ',
    'GROUP BY Codigo ',
    'ORDER BY SUM_AGENTES ',
    '']);
  end;
  try
    //
    if _Qry2 <> nil then
      Qry2 := _Qry2
    else
      Qry2 := TmySQLQuery.Create(Dmod);
    //
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry2, Dmod.MyDB, [
      'SELECT SUM(Agente) SUM_AGENTES ',
      'FROM osage ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
      if Qry0.Locate('SUM_AGENTES', Qry2.FieldByName('SUM_AGENTES').AsFloat, []) then
      begin
        AgeEqiCab := Qry0.FieldByName('Codigo').AsInteger;
        Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
        'AgeEqiCab'], [
        'Codigo'], [
        AgeEqiCab], [
        Codigo], True);
      end else
        Result := False;
    finally
      if _Qry2 = nil then
        Qry2.Free;
    end;
  finally
    if _Qry0 = nil then
      Qry0.Free;
  end;
end;
}

function TUnOSApp_PF.FiltroGrade(const Servico: TGraBugsServi;
  const Material: TGraBugsMater; var Filtro: String): Boolean;
var
  GraNiv: Integer;
  GraCod, Txt1, Txt2, TxtA: String;
begin
  Filtro := '';
  //Result := False;
  //
  if Servico <> gbsIndef then
  begin
    TxtA := '';
    Txt1 := '';
    Txt2 := '';
    //
    case Material of
      //gbmIndef: ;
      //gbmEquiEProd: ;
      gbmEquipam: GraNiv := Dmod.QrOpcoesBugsGraNivEqAp.Value;
      gbmProduto: GraNiv := Dmod.QrOpcoesBugsGraNivPrAp.Value;
      else GraNiv := 0;
    end;
    if GraNiv > 0 then
    begin
      case Material of
        //gbmIndef: ;
        //gbmEquiEProd: ;
        gbmEquipam: GraCod := Geral.FF0(Dmod.QrOpcoesBugsGraCodEqAp.Value);
        gbmProduto: GraCod := Geral.FF0(Dmod.QrOpcoesBugsGraCodPrAp.Value);
        else GraNiv := 0;
      end;
      case GraNiv of
        //0: Txt := '';
        1: Txt1 := ' gg1.Nivel1=' + GraCod;
        2: Txt1 := ' gg1.Nivel2=' + GraCod;
        3: Txt1 := ' gg1.Nivel3=' + GraCod;
        4: Txt1 := ' gg1.Nivel4=' + GraCod;
        5: Txt1 := ' gg1.Nivel5=' + GraCod;
        6: Txt1 := ' gg1.PrdGrupTip=' + GraCod;
      end;
    end;
    //
    case Material of
      //gbmIndef: ;
      //gbmEquiEProd: ;
      gbmEquipam: GraNiv := Dmod.QrOpcoesBugsGraNivEqMo.Value;
      gbmProduto: GraNiv := Dmod.QrOpcoesBugsGraNivPrMo.Value;
      else GraNiv := 0;
    end;
    if GraNiv > 0 then
    begin
      case Material of
        //gbmIndef: ;
        //gbmEquiEProd: ;
        gbmEquipam: GraCod := Geral.FF0(Dmod.QrOpcoesBugsGraCodEqMo.Value);
        gbmProduto: GraCod := Geral.FF0(Dmod.QrOpcoesBugsGraCodPrMo.Value);
        else GraNiv := 0;
      end;
      case GraNiv of
        //0: Txt := '';
        1: Txt2 := ' gg1.Nivel1=' + GraCod;
        2: Txt2 := ' gg1.Nivel2=' + GraCod;
        3: Txt2 := ' gg1.Nivel3=' + GraCod;
        4: Txt2 := ' gg1.Nivel4=' + GraCod;
        5: Txt2 := ' gg1.Nivel5=' + GraCod;
        6: Txt2 := ' gg1.PrdGrupTip=' + GraCod;
      end;
    end;
    //
    case Servico of
      gbsIndef: TxtA := 'gg1.Nivel1 <> 0';
      gbsAplEMon:
      begin
        if (Txt1 <> '') and (Txt2 <> '') then
          TxtA := '((' + Txt1 + ') OR (' + Txt2 + '))'
        else if Txt1 <> '' then
          TxtA := Txt1
        else if Txt2 <> '' then
          TxtA := Txt2
        else
          TxtA := 'gg1.Nivel1 = 0';
      end;
      gbsAplica:
      begin
        if Txt1 <> '' then
          TxtA := Txt1
        else if Txt2 <> '' then
          TxtA := Txt2
        else
          TxtA := 'gg1.Nivel1 = 0';
      end;
      gbsMonitora:
      begin
        if Txt2 <> '' then
          TxtA := Txt2
        else
          TxtA := 'gg1.Nivel1 = 0';
      end;
    end;
    Filtro := TxtA;
  end;
  Result := Trim(Filtro) <> '';
  //
  if not Result then
  begin
    Geral.MensagemBox('Fitro de grade n�o definido!' + #13#10 +
    'Defina em op��es espec�ficas!', 'Aviso', MB_OK+MB_ICONWARNING);
    //
    //  Ter certeza que n�o trar� nenhum registro!
    Filtro := '((gg1.PrdGrupTip=0) AND (gg1.PrdGrupTip<>0))'
  end;
end;

procedure TUnOSApp_PF.IncluiEPI(QueryEPI: TmySQLQuery; Codigo, Controle,
  Conta: Integer; Tabela: String);
var
  IDIts: Integer;
  GraGruX: Variant;
begin
  GraGruX := DBCheck.EscolheCodigoUnico('...',
              'F�rmulas Bases',
              'Informe o produto: [F7 para pesquisar]', nil, nil, 'Descricao', 0, [
              'SELECT ggx.Controle Codigo, gg1.Nome Descricao',
              'FROM gragrux ggx ',
              'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
              'WHERE gg1.GraTabApp = 0 ',
              'AND gg1.Ativo = 1 ',
              'AND ggx.Controle NOT IN ',
              '( ',
              'SELECT GraGruX ',
              'FROM ' + Tabela,
              'WHERE Conta=' + Geral.FF0(Conta),
              ') ',
              'ORDER BY gg1.Nome ',
              ''], Dmod.MyDB, True);
  //
  if (GraGruX <> Null) and (Codigo <> 0) and (Controle <> 0) and (Conta <> 0) then
  begin
    IDIts := UMyMod.BPGS1I32(Tabela, 'IDIts', '', '', tsPos, stIns, 0);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, Tabela, False,
      ['GraGrux', 'Codigo', 'Controle', 'Conta'], ['IDIts'],
      [GraGruX, Codigo, Controle, Conta], [IDIts], True) then
    begin
      ReopenOSEPI(QueryEPI, Tabela, Conta, 0);
    end;
  end;
end;

procedure TUnOSApp_PF.ExcluiEPI(QueryEPI: TmySQLQuery;
  DBGFormulEPI: TDBGrid; Tabela: String; Conta: Integer);
begin
  if DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QueryEPI, DBGFormulEPI,
    Tabela, ['IDIts'], ['IDIts'], istPergunta, '') = ID_YES then
  begin
    ReopenOSEPI(QueryEPI, Tabela, Conta, 0);
  end;
end;

function TUnOSApp_PF.IncluiOSMonPipAtual(OSCab, SiapterCad, OSFMCbMae,
PipCad, Ordem, PrgLstCab: Integer): Boolean;
(*
  / n�o tem como fazer asim!
  / se houver adi��o de PIP em outra OS, ter� grande chance de haver datas
  / desencontradas de monitoramento!
*)
var
  Codigo, Controle, Reordem: Integer;
begin
  Codigo         := OSCab;
  Controle       := 0;
{
  PipCad         := QrLocPipCodigo.Value;
  Ordem          := QrLocPip.RecNo;
  PrgLstCab      := QrLocPipPrgLstCab.Value;
}
  Reordem        := Ordem;
  //
  Controle := UMyMod.BPGS1I32('ospipmon', 'Controle', '', '', tsPos, stIns, 0);
  Result := UMyMod.SQLIns_ON_DUPLICATE_KEY(Dmod.QrUpd, 'ospipmon', False, [
  'Codigo', 'PipCad', 'Ordem',
  'Reordem', 'PrgLstCab', 'OSFMCbMae'], [
  'Controle'], [
  'Controle'], [
  Codigo, PipCad, Ordem,
  Reordem, PrgLstCab, OSFMCbMae], [
  Controle], [
  Controle], True, 'Insercoes');
end;

procedure TUnOSApp_PF.InsAltOSCabAlv(SQLType: TSQLType; QrSelect, QrInsUpd: TmySQLQuery;
  Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOSCabAlv, FmOSCabAlv, afmoNegarComAviso) then
  begin
    FmOSCabAlv.ImgTipo.SQLType := SQLType;
    FmOSCabAlv.FTabela := 'OSCabAlv';
    FmOSCabAlv.FQrSelect := QrSelect;
    FmOSCabAlv.FQrInsere := QrInsUpd;
    FmOSCabAlv.FCodigo := Codigo;
    //
    //
    if SQLType = stUpd then
    begin
      // N�o tem!
    end;
    FmOSCabAlv.ShowModal;
    FmOSCabAlv.Destroy;
  end;
end;

procedure TUnOSApp_PF.MostraFormOSPrvAdd(SQLType: TSQLType; Codigo,
  Controle: Integer; QrOSPrv: TmySQLQuery;
  Ins_Cliente, Ins_Contato, Ins_Agente, Ins_FormContat: Integer);
begin
  if DBCheck.CriaFm(TFmOSPrvAdd, FmOSPrvAdd, afmoNegarComAviso) then
  begin
    FmOSPrvAdd.ImgTipo.SQLType := SQLType;
    FmOSPrvAdd.FCodigo := Codigo;
    FmOSPrvAdd.FControle := Controle;
    //
    if SQLType = stUpd then
    begin
      FmOSPrvAdd.TPDtHrContat.Date := QrOSPrv.FieldByName('DtHrContat').AsDateTime;
      FmOSPrvAdd.EdDtHrContat.ValueVariant := QrOSPrv.FieldByName('DtHrContat').AsDateTime;
      //
      FmOSPrvAdd.EdFormContat.ValueVariant := QrOSPrv.FieldByName('FormContat').AsInteger;
      FmOSPrvAdd.CBFormContat.KeyValue := QrOSPrv.FieldByName('FormContat').AsInteger;
      //
      FmOSPrvAdd.EdCliente.ValueVariant := QrOSPrv.FieldByName('Cliente').AsInteger;
      FmOSPrvAdd.CBCliente.KeyValue := QrOSPrv.FieldByName('Cliente').AsInteger;
      //
      FmOSPrvAdd.EdContato.ValueVariant := QrOSPrv.FieldByName('Contato').AsInteger;
      FmOSPrvAdd.CBContato.KeyValue := QrOSPrv.FieldByName('Contato').AsInteger;
      //
      FmOSPrvAdd.EdAgente.ValueVariant := QrOSPrv.FieldByName('Agente').AsInteger;
      FmOSPrvAdd.CBAgente.KeyValue := QrOSPrv.FieldByName('Agente').AsInteger;
      //
      FmOSPrvAdd.MeNome.Text := QrOSPrv.FieldByName('Nome').AsString;
      //
      FmOSPrvAdd.EdPrvStatus.ValueVariant := QrOSPrv.FieldByName('PrvStatus').AsInteger;
      FmOSPrvAdd.CBPrvStatus.KeyValue := QrOSPrv.FieldByName('PrvStatus').AsInteger;
      //
    end else
    begin
      FmOSPrvAdd.TPDtHrContat.Date := Date;
      FmOSPrvAdd.EdDtHrContat.ValueVariant := Time;
      //
      //
      if Ins_FormContat <> 0 then
      begin
        FmOSPrvAdd.EdFormContat.ValueVariant := Ins_FormContat;
        FmOSPrvAdd.CBFormContat.KeyValue     := Ins_FormContat;
      end;
      //
      if Ins_Cliente <> 0 then
      begin
        FmOSPrvAdd.EdCliente.ValueVariant := Ins_Cliente;
        FmOSPrvAdd.CBCliente.KeyValue     := Ins_Cliente;
      end;
      //
      if Ins_Contato <> 0 then
      begin
        FmOSPrvAdd.EdContato.ValueVariant := Ins_Contato;
        FmOSPrvAdd.CBContato.KeyValue     := Ins_Contato;
      end;
      //
      if Ins_Agente <> 0 then
      begin
        FmOSPrvAdd.EdAgente.ValueVariant := Ins_Agente;
        FmOSPrvAdd.CBAgente.KeyValue     := Ins_Agente;
      end;
      //
    end;
    FmOSPrvAdd.ShowModal;
    FmOSPrvAdd.Destroy;
  end;
end;

procedure TUnOSApp_PF.MostraFormOSCab2(MostraFormOSCab2IDSrcIDSrc:
  TMostraFormOSCab2IDSrc; IDIndex: Integer);
const
  Atual = 0;
  Disposicao = dispIndefinido;
  ReabreOSCab = True;
var
  SQL, LJ: String;
  Form: TForm;
  Grupo, Opcao, Lugar, OSCab: Integer;
begin
  SQL := '';
  LJ := '';
  Form := MyObjects.FormTDICria(
    TFmOSCab2, FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
  if IDIndex <> 0 then
  begin
    case MostraFormOSCab2IDSrcIDSrc of
      mfoisGrupo: SQL := 'WHERE cab.Grupo=' + Geral.FF0(IDIndex);
      mfoisCodigo: SQL := 'WHERE cab.Codigo=' + Geral.FF0(IDIndex);
      mfoisOSMonCab:
      begin
        LJ  := 'LEFT JOIN osmoncab omc ON omc.Codigo=cab.Codigo';
        SQL := 'WHERE omc.conta=' + Geral.FF0(IDIndex);
      end;
      mfoisPesq:
      begin
        MyObjects.FormTDICria(
          TFmOSPsq, FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
        Exit;
      end
      else
      begin
        Geral.MB_Aviso('Defina o item de pesquisa "Por:"!');
        Exit;
      end;
    end;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrLoc, Dmod.MyDB, [
    'SELECT DISTINCT cab.Codigo, cab.Grupo, cab.Opcao, cab.SiapTerCad ',
    'FROM oscab cab',
    LJ,
    SQL,
    '']);
    if DmModOS.QrLoc.RecordCount > 0 then
    begin
      Grupo := DmModOS.QrLocGrupo.Value;
      Opcao := DmModOS.QrLocOpcao.Value;
      Lugar := DmModOS.QrLocSiapTerCad.Value;
      OSCab := DmModOS.QrLocCodigo.Value;
      //
      TFmOSCab2(Form).LocCod(
        Atual, Grupo, Disposicao, Lugar, Opcao, ReabreOSCab, OSCab);
    end;
  end;
end;

procedure TUnOSApp_PF.MostraFormOSCabXtr(SQLType: TSQLType; QrOSCab,
  QrOsCabXtr: TmySQLQuery);
begin
  if DBCheck.CriaFm(TFmOSCabXtr, FmOSCabXtr, afmoNegarComAviso) then
  begin
    FmOSCabXtr.ImgTipo.SQLType := SQLType;
    FmOSCabXtr.FQrInsUpd := QrOSCabXtr;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(FmOSCabXtr.QrOSCab, Dmod.MyDB, [
    'SELECT cab.*,  fge.Nome NO_FatoGeradr, ',
    'sta.Nome NO_ESTATUS, stc.Nome NO_SiapTerCad, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
    'FROM oscab cab ',
    'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade ',
    'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr ',
    'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus ',
    'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad ',
    ' ',
    'WHERE cab.Codigo=' + Geral.FF0(QrOSCab.FieldByName('Codigo').AsInteger),
    '']);
    //
    if SQLType = stUpd then
    begin
      FmOSCabXtr.EdControle.ValueVariant := QrOSCabXtr.FieldByName('Controle').AsInteger;
      FmOSCabXtr.TPDtHrIni.Date := QrOSCabXtr.FieldByName('DtHrIni').AsDateTime;
      FmOSCabXtr.EdDtHrIni.ValueVariant := QrOSCabXtr.FieldByName('DtHrIni').AsDateTime;
      FmOSCabXtr.TPDtHrFim.Date := QrOSCabXtr.FieldByName('DtHrFim').AsDateTime;
      FmOSCabXtr.EdDtHrFim.ValueVariant := QrOSCabXtr.FieldByName('DtHrFim').AsDateTime;
    end;
    FmOSCabXtr.ShowModal;
    FmOSCabXtr.Destroy;
    //
    UnDmkDAC_PF.AbreQuery(QrOSCab, Dmod.MyDB);
  end;
end;

procedure TUnOSApp_PF.MostraFormOSPrz(SQLType: TSQLType; QrOSCab,
  QrOsPrz: TmySQLQuery; ValorIni, ValorFim, Porcento: Double);
begin
  if DBCheck.CriaFm(TFmOsPrz, FmOsPrz, afmoNegarComAviso) then
  begin
    FmOsPrz.ImgTipo.SQLType := SQLType;
    FmOsPrz.FQrInsUpd := QrOsPrz;
    //
    FmOsPrz.FValorIni := ValorIni;
    FmOsPrz.FValorFim := ValorFim;
    FmOsPrz.FPorcento := Porcento;
{
    FmOsPrz.QrOSCab.Close;
    FmOsPrz.QrOSCab.SQL.Text := QrOSCab.SQL.Text;
    FmOsPrz.QrOSCab.Params := QrOSCab.Params;
    UMyMod.AbreQuery(FmOsPrz.QrOSCab, Dmod.MyDB);
}
    UnDmkDAC_PF.AbreMySQLQuery0(FmOsPrz.QrOSCab, Dmod.MyDB, [
    'SELECT cab.*,  fge.Nome NO_FatoGeradr, ',
    'sta.Nome NO_ESTATUS, stc.Nome NO_SiapTerCad, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT ',
    'FROM oscab cab ',
    'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade ',
    'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr ',
    'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus ',
    'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad ',
    ' ',
    'WHERE cab.Codigo=' + Geral.FF0(QrOSCab.FieldByName('Codigo').AsInteger),
    '']);
    //
    if SQLType = stUpd then
    begin
      FmOsPrz.EdControle.ValueVariant := QrOsPrz.FieldByName('Controle').AsInteger;
      //FmOsPrz.EdCondicao.ValueVariant := QrOsPrz.FieldByName('Condicao').AsInteger;
      //FmOsPrz.CBCondicao.KeyValue     := QrOsPrz.FieldByName('Condicao').AsInteger;
      FmOSPrz.VuCondicao.ValueVariant := QrOsPrz.FieldByName('Condicao').AsInteger;
      FmOsPrz.EdDescoPer.ValueVariant := QrOsPrz.FieldByName('DescoPer').AsFloat;
      FmOsPrz.CkEscolhido.Checked     := QrOsPrz.FieldByName('Escolhido').AsInteger = 1;
    end else
    begin
      FmOsPrz.CkEscolhido.Checked := True;
    end;
    FmOsPrz.ShowModal;
    FmOsPrz.Destroy;
    //
    RecalculaValoresDeCondicoes(QrOSCab.FieldByName('Codigo').AsInteger);
    //
    //FQrOSCab.Close;
    UnDmkDAC_PF.AbreQuery(QrOSCab, Dmod.MyDB);
  end;
end;

procedure TUnOSApp_PF.MostraFormOSPrvSel(SQLType: TSQLType; Codigo: Integer;
  QrOSPrv: TmySQLQuery; Cliente: Integer);
begin
  if DBCheck.CriaFm(TFmOSPrvSel, FmOSPrvSel, afmoNegarComAviso) then
  begin
    FmOSPrvSel.ImgTipo.SQLType := SQLType;
    FmOSPrvSel.FCodigo := Codigo;
    FmOSPrvSel.FQrOSPrv := QrOSPrv;

    FmOSPrvSel.ReopenOSPrv(Cliente);

    FmOSPrvSel.ShowModal;
    FmOSPrvSel.Destroy;
  end;
end;

function TUnOSApp_PF.NomeDeOperacao(ConjuntoDeOperacoes: Integer): String;
begin
  Result := sListaOperacoesBugstrol[ConjuntoDeOperacoes];
  {
  case ConjuntoDeOperacoes of
    0: Result := 'INDEFINIDO!';
    1: Result := 'Dedetiza��o';
    2: Result := 'Limpeza de Caixa d''�gua';
    3: Result := 'MISTA (DL)';
    4: Result := 'Monitoramento';
    5: Result := 'MISTA (DM)';
    6: Result := 'MISTA (LM)';
    7: Result := 'COMPLETA';
    else Result := '#?????#';
  end;
  }
end;

function TUnOSApp_PF.OSOrigDesnecessaria(FatoGeradr: Integer): Boolean;
begin
  Result := FatoGeradr in ([CO_COD_FatoGeradr_INDEF, CO_COD_FatoGeradr_DEPRECADO, CO_COD_FatoGeradr_1aAcao]);
end;

procedure TUnOSApp_PF.OSRapida();
begin
  if DBCheck.CriaFm(TFmOSRapida, FmOSRapida, afmoNegarComAviso) then
  begin
    {
    FmOSRapida.ImgTipo.SQLType := SQLType;
    FmOSRapida.FCodigo := Codigo;
    FmOSRapida.FQrOSPrv := QrOSPrv;

    FmOSRapida.ReopenOSPrv(Cliente);
    }
    //Application.CreateForm(TFmOSRapida, FmOSRapida);
    FmOSRapida.ShowModal;
    FmOSRapida.Destroy;
  end;
end;

{
function TUnOSApp_PF.PesquisaOSOrigem(const Empresa, Cliente, Lugar, FatoGeradr: Integer;
  var OSOrigem: Integer): Boolean;
const
  IncluiOSOrigem = False;
  OSOrigALocalizar = 0;
begin
  Result := False;
  OSOrigem := 0;
  //
  if OSOrigDesnecessaria(FatoGeradr) then
  begin
    Geral.MB_Aviso('"Fato gerador (Motivo)" indefinido!');
    Exit;
  end;
  if Empresa = 0 then
  begin
    Geral.MB_Aviso('Informe a empresa!');
    Exit;
  end;
  if Cliente = 0 then
  begin
    Geral.MB_Aviso('Informe o cliente!');
    Exit;
  end;
  if Lugar = 0 then
  begin
    Geral.MB_Aviso('Informe o lugar!');
    Exit;
  end;
  if DBCheck.CriaFm(TFmOSOriPsq, FmOSOriPsq, afmoNegarComAviso) then
  begin
    FmOSOriPsq.ImgTipo.SQLType := stPsq;
    UnOSApp_PF.ReopenOSOriPsq(FmOSOriPsq.QrOSCab, Empresa, Cliente, Lugar,
      CO_COD_FatoGeradr_1aAcao, OSOrigALocalizar, IncluiOSOrigem);
    FmOSOriPsq.ShowModal;
    OSOrigem := FmOSOriPsq.FLocalizador;
(*
    if OSOrigem <> 0 then
      EdOSOrigem.ValueVariant := OSOrigem;
*)
    Result := OSOrigem <> 0;
    FmOSOriPsq.Destroy;
  end;
end;
}

procedure TUnOSApp_PF.ReopenEquipAplic(Qry: TmySQLQuery; Servico: TGraBugsServi);
var
  SQL_W: String;
begin
  //FiltroGrade(Servico, gbmEquipam, SQL_W);
  Qry.Close;
  //
  Qry.FieldByName('Nome').Size := 512;
  Qry.FieldByName('Nome').DisplayWidth := 512;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT ggx.Controle, GraTabApp, gg1.Nivel1, CONCAT(gg1.Nome, " [",',
  'gta.Nome, "]") Nome',
  'FROM gragrux ggx ',
  'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
  'LEFT JOIN gratabapp gta ON gta.Codigo=gg1.GraTabApp',
  'WHERE gg1.GraTabApp IN (' + Geral.FF0(CO_COD_GraTabApp_GraG1EqAp) + ', ' + Geral.FF0(CO_COD_GraTabApp_GraG1EqMo) + ')',
  //'WHERE ' + SQL_W,
  'ORDER BY gg1.Nome',
  '']);
end;

function TUnOSApp_PF.RatificaConsumoOS_PreAnalise(Nivel: TNivelRatifConsumoOS;
  IDNivel, FatID: Integer; Pergunta: Boolean): Boolean;
var
  SQL_WHERE, SQL_Union, SQL_FRM, SQL_MON: String;
begin
  Result := False;
  case Nivel of
    //nrcosNenhum=0,
    //nrcosProduto=1,
    nrcosFormula(*=2*): SQL_WHERE := 'WHERE ofr.Conta=' + Geral.FF0(IDNivel);
    nrcosServico(*=3*): SQL_WHERE := 'WHERE srv.Controle=' + Geral.FF0(IDNivel);
    nrcosLocalizador(*=4*): SQL_WHERE := 'WHERE srv.Codigo=' + Geral.FF0(IDNivel);
    else
    begin
      SQL_WHERE := 'WHERE ???';
      Geral.MB_Erro('Nivel n�o implementado na procedure "UnOSApp_PF.RatificaConsumoOS_PreAnalise()"');
      Exit;
    end;
  end;
  SQL_Union := '';
  SQL_FRM := '';
  SQL_MON := '';
  if FatID = 0 then
    SQL_Union := 'UNION ' + sLineBreak + '' + sLineBreak;
  //
  if (FatID = 0) or (FatID = VAR_FATID_4101) then
    SQL_FRM := Geral.ATS([
    'SELECT srv.Codigo Localizador, ',
    'srv.Controle IDServico, des.Nome NO_DesServico, des.Sigla NO_SIGLA, ',
    '1 Tabela, "Aplica��o" TipoFormula, ',
    'ofc.Conta IDFormula, frm.Nome NO_FORMULA, ',
    'IF((srv.TudoFeitoM OR srv.TudoFeitoA), 1, ',
    '  IF(ofc.PercFeito>99.99, 1,  0)) REALIZADO, ',
    'med.Sigla SIGLAUNIDMED, gg1.Nome NO_GG1, ',
    'ELT(RatifUso+1, "N", "S", "?") NO_RatifUso ',
    'FROM osfrmrec ofr ',
    'LEFT JOIN osfrmcab ofc ON ofc.Conta=ofr.Conta ',
    'LEFT JOIN ossrv srv ON srv.Controle=ofc.Controle ',
    'LEFT JOIN gragrux ggx on ggx.Controle=ofr.GraGruX ',
    'LEFT JOIN gragru1 gg1 on gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed med on med.Codigo=gg1.UnidMed ',
    'LEFT JOIN desservico des ON des.Codigo=srv.DesServico ',
    'LEFT JOIN formulas frm ON frm.Codigo=ofc.Formula ',
    SQL_WHERE,
    'AND ',
    'IF((srv.TudoFeitoM OR srv.TudoFeitoA), 1, ',
    '  IF(ofc.PercFeito>99.99, 1,  0)) = 0 ',
    ' ']);
  if (FatID = 0) or (FatID = VAR_FATID_4102) then
    SQL_MON := Geral.ATS([
    'SELECT srv.Codigo Localizador, ',
    'srv.Controle IDServico, des.Nome NO_DesServico, des.Sigla NO_SIGLA, ',
    '2 Tabela, "Monitoramento" TipoFormula, ',
    'ofc.Conta IDFormula, frm.Nome NO_FORMULA, ',
    'IF((srv.TudoFeitoM OR srv.TudoFeitoA), 1,0) REALIZADO, ',
    'med.Sigla SIGLAUNIDMED, gg1.Nome NO_GG1, ',
    'ELT(RatifUso+1, "N", "S", "?") NO_RatifUso ',
    'FROM osmonrec ofr ',
    'LEFT JOIN osmoncab ofc ON ofc.Conta=ofr.Conta ',
    'LEFT JOIN ossrv srv ON srv.Controle=ofc.Controle ',
    'LEFT JOIN gragrux ggx on ggx.Controle=ofr.GraGruX ',
    'LEFT JOIN gragru1 gg1 on gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed med on med.Codigo=gg1.UnidMed ',
    'LEFT JOIN desservico des ON des.Codigo=srv.DesServico ',
    'LEFT JOIN formulas frm ON frm.Codigo=ofc.Formula ',
    SQL_WHERE,
    'AND ',
    'IF((srv.TudoFeitoM OR srv.TudoFeitoA), 1, 0) = 0 ',
    '']);
  UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrNaoExeRat, Dmod.MyDB, [
  SQL_FRM,
  SQL_Union,
  SQL_MON,
  '']);
  //Geral.MB_SQL(nil, DmModOS.QrNaoExeRat);
  if DmModOS.QrNaoExeRat.RecordCount > 0 then
  begin
    Result := False;
    if Geral.MB_Pergunta('Existem ' + Geral.FF0(DmModOS.QrNaoExeRat.RecordCount) +
    ' itens que n�o ser�o ratificados por falta de confirma��o de realiza��o de sua f�rmula!' +
    sLineBreak + 'Deseja visualizar o relat�rio?') = ID_YES then
    begin
      MyObjects.frxDefineDataSets(DmModOS.frxGER_OSERV_001_ModOS_03, [
        DModG.frxDsDono,
        DmModOS.frxDsNaoExeRat
        ]);
      DmModOS.frxGER_OSERV_001_ModOS_03.Variables['VARF_DATA'] := DmodG.ObtemAgora();
      MyObjects.frxMostra(DmModOS.frxGER_OSERV_001_ModOS_03,
        'Produtos sem f�rmula executada');
    end;
    if Pergunta then
      Result := Geral.MB_Pergunta('Deseja continuar assim mesmo?') = ID_YES;
  end else
    Result := True;
end;

procedure TUnOSApp_PF.RecalculaValoresDeCondicoes(Codigo: Integer);
begin
//
end;

procedure TUnOSApp_PF.ReopenEntiContat(QrEntiContat: TmySQLQuery; SubCliente,
  Contratante, Pagante: Integer);
var
  //CodIn, SQL,
  SQL1, SQL2, SQL3: String;
{
  procedure AddItemCodIn(Entidade: Integer);
  begin
    if Entidade <> 0 then
    begin
      if CodIn <> '' then CodIn := CodIn + ', ';
      CodIn := CodIn + Geral.FF0(SubCliente);
    end;
  end;
}
  Primo: Integer;
begin
  try
(*
    if (SubCliente <> 0) and (Contratante <> 0) then
      SQL := 'WHERE ece.Codigo=' + Geral.FF0(SubCliente) +
     //2013-08-25 - Muito demorado!!!
     //  ' OR eco.Codigo=' + Geral.FF0(Contratante)
       ' OR ece.Codigo=' + Geral.FF0(Contratante)
    else
    if (SubCliente <> 0) then
      SQL := 'WHERE ece.Codigo=' + Geral.FF0(SubCliente)
    else
    if (Contratante <> 0) then
      SQL := 'WHERE ece.Codigo=' + Geral.FF0(Contratante)
    else
      SQL := 'WHERE ece.Codigo=-999999999';
    //
*)
{
    // 2013-10-16
    CodIn := '';
    AddItemCodIn(SubCliente);
    AddItemCodIn(Contratante);
    AddItemCodIn(Pagante);
    if CodIn <> '' then
      SQL := 'WHERE ece.Codigo IN (' + CodIn + ')'
    else
      SQL := 'WHERE ece.Codigo=-999999999';
    // FIM 2013-10-16
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrEntiContat, Dmod.MyDB, [
    'SELECT eco.Controle, eco.Nome ',
    'FROM enticontat eco ',
    'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
    'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo ',
    SQL,
    '']);
    //
}
    if SubCliente <> 0 then
      Primo := SubCliente
    else
      Primo := -999999999;
    SQL1 := Geral.ATS([
    'SELECT DISTINCT eco.Controle, ',
    'CONCAT(eco.Nome, " [CL]") Nome ',
    'FROM enticontat eco ',
    'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
    'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo ',
    'WHERE ece.Codigo=' + Geral.FF0(Primo),
    '']);

    if (Contratante <> 0) and (Contratante <> Primo) then
      SQL2 := Geral.ATS([
      'UNION',
      '',
      'SELECT DISTINCT eco.Controle, ',
      'CONCAT(eco.Nome, " [CO]") Nome ',
      'FROM enticontat eco ',
      'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo ',
      'WHERE ece.Codigo=' + Geral.FF0(Contratante),
      ''])
    else
      SQL2 := '';

    if (Pagante <> 0) and (Pagante <> Contratante) and (Pagante <> Primo) then
      SQL3 := Geral.ATS([
      'UNION',
      '',
      'SELECT DISTINCT eco.Controle, ',
      'CONCAT(eco.Nome, " [PG]") Nome ',
      'FROM enticontat eco ',
      'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo ',
      'WHERE ece.Codigo=' + Geral.FF0(Pagante),
      ''])
    else
      SQL3 := '';

    UnDmkDAC_PF.AbreMySQLQuery0(QrEntiContat, Dmod.MyDB, [
    SQL1,
    SQL2,
    SQL3,
    'ORDER BY Nome ',
    '']);
  except
    Geral.MB_Aviso('Erro: UnOSApp_PF.ReopenEntiContat()');
    raise;
  end;
end;

procedure TUnOSApp_PF.ReopenEntiMail(QrEntiMail: TmySQLQuery; EntiContat,
  Conta: Integer);
begin
  QrEntiMail.Close;
  QrEntiMail.Params[0].AsInteger := EntiContat;
  UMyMod.AbreQuery(QrEntiMail, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrEntiMail.Locate('Conta', Conta, []);
end;

procedure TUnOSApp_PF.ReopenEntiTel(QrEntiTel: TmySQLQuery; EntiContat,
  Conta: Integer);
begin
  QrEntiTel.Close;
  QrEntiTel.Params[0].AsInteger := EntiContat;
  UMyMod.AbreQuery(QrEntiTel, DMod.MyDB);
  //
  if Conta <> 0 then
    QrEntiTel.Locate('Conta', Conta, []);
end;

procedure TUnOSApp_PF.ReopenLctFatRef(QrLctFatRef: TmySQLQuery; Controle,
  Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLctFatRef, Dmod.MyDB, [
  'SELECT lfr.* ',
  'FROM lctfatref lfr ',
  'WHERE lfr.Controle=' + Geral.FF0(Controle),
  '']);
  //
  QrLctFatRef.Locate('Conta', Conta, []);
end;

procedure TUnOSApp_PF.ReopenLocOS_Idx(QrLocOS: TmySQLQuery; Codigo: Integer);
begin
  QrLocOS.Close;
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocOS, Dmod.MyDB, [
  'SELECT Codigo, Grupo, Opcao, SiapTerCad ',
  'FROM oscab ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TUnOSApp_PF.ReopenLocOS_Mul(QrLocOS: TmySQLQuery; Grupo, Opcao, SiapTerCad: Integer);
begin
  QrLocOS.Close;
  UnDmkDAC_PF.AbreMySQLQuery0(QrLocOS, Dmod.MyDB, [
  'SELECT Codigo, Grupo, Opcao, SiapTerCad ',
  'FROM oscab ',
  'WHERE Grupo=' + Geral.FF0(Grupo),
  'AND Opcao=' + Geral.FF0(Opcao),
  'AND SiapTerCad=' + Geral.FF0(SiapTerCad),
  '']);
end;

procedure TUnOSApp_PF.ReopenOSAge(QrOSAge:TmySQLQuery; Codigo, Controle: Integer);
begin
  QrOSAge.Close;
  QrOSAge.Params[0].AsInteger := Codigo; //FmOSCab.QrOSCabCodigo.Value
  UMyMod.AbreQuery(QrOSAge, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrOSAge.Locate('Controle', Controle, []);
end;

procedure TUnOSApp_PF.ReopenOSAlv(QrOSAlv: TmySQLQuery; Controle, Conta: Integer;
  Database: TmySQLDatabase);
begin
  QrOSAlv.Close;
  if QrOSAlv.Params.Count > 0 then
    QrOSAlv.Params[0].AsInteger := Controle;
  UMyMod.AbreQuery(QrOSAlv, Database);
  //
  if Conta <> 0 then
    QrOSAlv.Locate('Conta', Conta, []);
end;

procedure TUnOSApp_PF.ReopenOSCab(QrOSCab: TmySQLQuery; Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSCab, Dmod.MyDB, [
  'SELECT eco.Nome NO_ENTICONTAT, cab.*,  ',
  'fge.Nome NO_FatoGeradr, sta.Nome NO_ESTATUS, sta.Execucao StatusExecucao, ',
  'stc.Nome NO_SiapTerCad, stc.LstCusPrd, ',
  'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
  'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Tel_ENT, ',
  'IF(pag.Tipo=0, pag.RazaoSocial, pag.Nome) NO_PAG, ',
  'IF(ctr.Tipo=0, ctr.RazaoSocial, ctr.Nome) NO_CTR, ',
  'car.Nome NO_CART, ppc.Nome NO_PRZ, ',
  'tg1.Nome NO_ExeTxtCli1, tg2.Nome NO_ExeTxtCli2, ',
  'aec.Nome NO_AgeEqiCab, moc.Nome NO_MobiliCad, ',
  'sta.AgeCorIni, sta.AgeCorFon, eci.CodEnti, ',
  'IF(ven.Tipo=0, ven.RazaoSocial, ven.Nome) NO_Vendedor, cab.MapPMV ',
  'FROM oscab cab ',
  'LEFT JOIN enticliint eci ON eci.CodFilial=cab.Empresa',
  'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade ',
  'LEFT JOIN entidades pag ON pag.Codigo=cab.EntPagante ',
  'LEFT JOIN entidades ctr ON ctr.Codigo=cab.EntContrat ',
  'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr ',
  'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus ',
  'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad ',
  'LEFT JOIN carteiras car ON car.Codigo=cab.CartEmis ',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=cab.CondicaoPg ',
  'LEFT JOIN enticontat eco ON eco.Controle=cab.EntiContat ',
  'LEFT JOIN txtgeneric tg1 ON tg1.Codigo=cab.ExeTxtCli1  ',
  'LEFT JOIN txtgeneric tg2 ON tg2.Codigo=cab.ExeTxtCli2  ',
  'LEFT JOIN ageeqicab aec ON aec.Codigo=cab.AgeEqiCab  ',
  'LEFT JOIN mobilicad moc ON moc.Codigo=cab.MobiliCad ',
  'LEFT JOIN entidades ven ON ven.Codigo=cab.Vendedor ',
  'WHERE cab.Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TUnOSApp_PF.ReopenOSCabAlv(QrOSCAbAlv: TmySQLQuery; Codigo,
  Controle: Integer);
begin
  QrOSCabAlv.Close;
  QrOSCabAlv.Params[0].AsInteger := Codigo;
  UMyMod.AbreQuery(QrOSCabAlv, Dmod.MyDB);
  //
  if Controle <> 0 then
    QrOSCabAlv.Locate('Controle', Controle, []);
end;

procedure TUnOSApp_PF.ReopenOSCabXtr(QrOSCAbXtr: TmySQLQuery; Codigo,
  Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSCAbXtr, Dmod.MyDB, [
  'SELECT * ',
  'FROM oscabxtr ',
  'WHERE Codigo=' + Geral.FF0(Codigo),
  'ORDER BY DtHrIni DESC ',
  '']);
  //
  if Controle <> 0 then
    QrOSCabXtr.Locate('Controle', Controle, []);
end;

procedure TUnOSApp_PF.ReopenOSChk(QrOSChk: TmySQLQuery; Codigo, Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSChk, Dmod.MyDB, [
  'SELECT ELT(chk.Feito+1, "N�O", "SIM", "???") NO_FEITO, ',
  'chi.Nome NO_ITEM, chk.* ',
  'FROM oschk chk ',
  'LEFT JOIN cheklstits chi ON chi.Controle=chk.ChekLstIts ',
  'WHERE chk.Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TUnOSApp_PF.ReopenOSCxa(QrOSCxa: TmySQLQuery; Codigo, Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSCxa, Dmod.MyDB, [
  'SELECT cxa.*, cxm.Nome NO_MATERIAL, ',
  'cxf.Nome NOME_FORMA, ',
  'six.MatersCxa, six.FormasCxa, six.VolumeL, ',
  'six.Local, six.Acesso, six.Medidas ',
  'FROM oscxa cxa ',
  'LEFT JOIN siapimacxa six ON six.Controle=cxa.Caixa ',
  'LEFT JOIN cxamaters cxm ON ',
  '  cxm.Codigo=six.MatersCxa ',
  'LEFT JOIN cxaformas cxf ON ',
  '  cxf.Codigo=six.FormasCxa ',
  'WHERE cxa.Codigo=' + Geral.FF0(Codigo),
  '']);
  //
  if Controle <> 0 then
    QrOSCxa.Locate('Controle', Controle, []);
end;

procedure TUnOSApp_PF.ReopenOSCxI(QrOSCxI: TmySQLQuery; Codigo,
  Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSCxI, Dmod.MyDB, [
  'SELECT ELT(cxi.Aplicacao, "OR�AM", "C.EXE", "AMBOS")',
  'NO_APLICACAO, cxi.* ',
  'FROM oscxi cxi ',
  'WHERE cxi.Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TUnOSApp_PF.ReopenOSEPI(Query: TmySQLQuery; Tabela: String; Conta,
  IDIts: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT epi.Codigo, epi.Controle, epi.Controle, epi.Conta, epi.IDIts, ',
    'ggx.Controle GraGruX, gg1.GraTabApp, gg1.Nivel1, gg1.Nome ',
    'FROM ' + Tabela + ' epi ',
    'LEFT JOIN gragrux ggx ON ggx.Controle = epi.GraGruX ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'WHERE epi.Conta=' + Geral.FF0(Conta),
    'AND gg1.GraTabApp = 0 ',
    'AND gg1.Ativo = 1 ',
    'ORDER BY gg1.Nome ',
    '']);
  //
  if IDIts <> 0 then
    Query.Locate('IDIts', IDIts, []);
end;

procedure TUnOSApp_PF.ReopenOSExtMon(QrOSExtMon: TmySQLQuery; Codigo,
  Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSExtMon, Dmod.MyDB, [
  'SELECT oem.* ',
  'FROM osextmon oem ',
  'WHERE oem.Codigo=' + Geral.FF0(Codigo),
  // Nao Mudar!!!
  'ORDER BY oem.DtaFinal DESC ',
  '']);
  //
  if Controle <> 0 then
    QrOSExtMon.Locate('Controle', Controle, []);
end;

procedure TUnOSApp_PF.ReopenOSFrmAbr(QrOSFrmAbr: TmySQLQuery; Conta, IDIts: Integer);
begin
  QrOSFrmAbr.Close;
  QrOSFrmAbr.Params[0].AsInteger := Conta;
  UMyMod.AbreQuery(QrOSFrmAbr, Dmod.MyDB);
  //
  if IDIts <> 0 then
    QrOSFrmAbr.Locate('IDIts', IDIts, []);
end;

procedure TUnOSApp_PF.ReopenOSFrmCab(QrOSFrmCab: TmySQLQuery; Controle, Conta: Integer);
begin
  QrOSFrmCab.Close;
  QrOSFrmCab.Params[0].AsInteger := Controle;
  UMyMod.AbreQuery(QrOSFrmCab, Dmod.MyDB);
  //
  if Conta <> 0 then
    QrOSFrmCab.Locate('Conta', Conta, []);
end;

procedure TUnOSApp_PF.ReopenOSFrmFlhCb(QrOSFrmFlhCb: TmySQLQuery; Conta,
  IDIts: Integer);
begin
  QrOSFrmFlhCb.Close;
  QrOSFrmFlhCb.Params[0].AsInteger := Conta;
  UMyMod.AbreQuery(QrOSFrmFlhCb, Dmod.MyDB);
  //
  if IDIts <> 0 then
    QrOSFrmFlhCb.Locate('IDIts', IDIts, []);
end;

procedure TUnOSApp_PF.ReopenOSFrmFlhDd(QrOSFrmFlhDd: TmySQLQuery; IDIts,
IDIt2: Integer);
begin
  QrOSFrmFlhDd.Close;
  QrOSFrmFlhDd.Params[0].AsInteger := IDIts;
  UMyMod.AbreQuery(QrOSFrmFlhDd, Dmod.MyDB);
  //
  if IDIt2 <> 0 then
    QrOSFrmFlhDd.Locate('IDIt2', IDIt2, []);
end;

procedure TUnOSApp_PF.ReopenOSFrmDep(QrOSFrmDep: TmySQLQuery; Conta,
  IDIts: Integer);
begin
  QrOSFrmDep.Close;
  QrOSFrmDep.Params[0].AsInteger := Conta;
  UMyMod.AbreQuery(QrOSFrmDep, Dmod.MyDB);
  //
  if IDIts <> 0 then
    QrOSFrmDep.Locate('IDIts', IDIts, []);
end;

procedure TUnOSApp_PF.ReopenOSFrmEvo(QrOSFrmEvo: TmySQLQuery; Conta,
  IDIts: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSFrmEvo, Dmod.MyDB, [
  'SELECT * ',
  'FROM osfrmevo ',
  'WHERE Conta=' + Geral.FF0(Conta),
  'ORDER BY DataHora DESC ',
  '']);
  //
  if IDIts <> 0 then
    QrOSFrmEvo.Locate('IDIts', IDIts, []);
end;

procedure TUnOSApp_PF.ReopenOSFrmRec(QrOSFrmRec: TmySQLQuery; Conta, IDIts: Integer);
begin
  QrOSFrmRec.Close;
  QrOSFrmRec.Params[0].AsInteger := Conta;
  UMyMod.AbreQuery(QrOSFrmRec, Dmod.MyDB);
  //
  if IDIts <> 0 then
    QrOSFrmRec.Locate('IDIts', IDIts, []);
end;

procedure TUnOSApp_PF.ReopenOSMonCab(QrOSMonCab: TmySQLQuery; Controle, Conta: Integer);
begin
{
  QrOSMonCab.Close;
  QrOSMonCab.Params[0].AsInteger := Controle;
  UMyMod.AbreQuery(QrOSMonCab, Dmod.MyDB);
}
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSMonCab, Dmod.MyDB, [
  'SELECT plc.Nome NO_prglstcab, dep.Nome NO_DEPENDENCI, ',
  'ofc.*, mon.Nome NO_FORMULA,  ',
  'gg1.Nome NO_EquipAplic, pip.Nome NO_PIP, ',
  'med.CodUsu CU_UNIDMED, ',
  'med.SIGLA, med.Nome NO_UNIDMED ',
  'FROM osmoncab ofc ',
  'LEFT JOIN formulas mon ON mon.Codigo=ofc.Formula ',
  'LEFT JOIN gragrux ggx ON ggx.Controle=ofc.EquipAplic ',
  'LEFT JOIN gragru1 gg1 ON ggx.Gragru1=gg1.Nivel1 ',
  'LEFT JOIN pipcad  pip ON pip.Codigo=ofc.PipCad ',
  'LEFT JOIN unidmed med ON med.Codigo=ofc.UnidMed ',
  'LEFT JOIN prglstcab plc ON plc.Codigo=pip.PrgLstCab ',
  'LEFT JOIN siapimadep sid ON sid.Controle=pip.Dependenci ',
  'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci ',
  'WHERE ofc.Controle=' + Geral.FF0(Controle),
  '']);
  //
  if Conta <> 0 then
    QrOSMonCab.Locate('Conta', Conta, []);
end;

procedure TUnOSApp_PF.ReopenOSMonPipDd(QrOSMonPipDd: TmySQLQuery; Conta,
  IDIts: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSMonPipDd, Dmod.MyDB, [
  'SELECT * ',
  'FROM osmonpipdd',
  'WHERE Conta=' + Geral.FF0(Conta),
  'ORDER BY Ordem, IDIts',
  '']);
  //
  if IDIts <> 0 then
    QrOSMonPipDd.Locate('IDIts', IDIts, []);
end;

{
procedure TUnOSApp_PF._Reopen_OS_Mon_Dep_(_Qr_OS_Mon_Dep_: TmySQLQuery; Conta,
  IDIts: Integer);
begin
  _Qr_OS_Mon_Dep_.Close;
  _Qr_OS_Mon_Dep_.Params[0].AsInteger := Conta;
  UMyMod.AbreQuery(_Qr_OS_Mon_Dep_, Dmod.MyDB);
  //
  if IDIts <> 0 then
    _Qr_OS_Mon_Dep_.Locate('IDIts', IDIts, []);
end;
}

procedure TUnOSApp_PF.ReopenOSMonRec(QrOSMonRec: TmySQLQuery; Conta,
  IDIts: Integer);
begin
  QrOSMonRec.Close;
  QrOSMonRec.Params[0].AsInteger := Conta;
  UMyMod.AbreQuery(QrOSMonRec, Dmod.MyDB);
  //
  if IDIts <> 0 then
    QrOSMonRec.Locate('IDIts', IDIts, []);
end;

(*
procedure TUnOSApp_PF.ReopenOSOriPsq(QrPsq: TmySQLQuery; Empresa, Entidade,
  SiapTerCad, FatoGeradr, OSOrigem: Integer; IncluiOSOrigem: Boolean);
var
  SQL: String;
begin
  if IncluiOSOrigem then
    SQL := 'AND Codigo=' + Geral.FF0(OSOrigem)
  else
    SQL := '';
  UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, Dmod.MyDB, [
  'SELECT Codigo, Grupo ',
  'FROM oscab ',
  'WHERE FatoGeradr=' + Geral.FF0(FatoGeradr),
  'AND Empresa=' + Geral.FF0(Empresa),
  'AND Entidade=' + Geral.FF0(Entidade),
  'AND SiapterCad=' + Geral.FF0(SiapTerCad),
  SQL,
  'ORDER BY Grupo DESC, Codigo DESC ',
  '']);
end;
*)

procedure TUnOSApp_PF.ReopenOSOSCxaAtrib(QrOSCxaAtrib: TmySQLQuery; Caixa: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSCxaAtrib, Dmod.MyDB, [
  'SELECT def.ID_Item, def.ID_Sorc, def.AtrCad, def.AtrIts, ',
  'cad.CodUsu CU_CAD, its.CodUsu CU_ITS, ',
  'cad.Nome NO_CAD, its.Nome NO_ITS ',
  'FROM atrsicxdef def ',
  'LEFT JOIN atrsicxits its ON its.Controle=def.AtrIts ',
  'LEFT JOIN atrsicxcad cad ON cad.Codigo=def.AtrCad ',
  //'WHERE def.ID_Sorc=' + Geral.FF0(QrOSCxaCaixa.Value),
  'WHERE def.ID_Sorc=' + Geral.FF0(Caixa),
  'ORDER BY NO_CAD, NO_ITS ',
  '']);
end;

procedure TUnOSApp_PF.ReopenOSPipIts(QrOSPipIts: TmySQLQuery; Controle,
  Conta: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPipIts, Dmod.MyDB, [
{
  'SELECT pcp.Nome NO_PERGUNTA, pcp.Sigla, opi.* ',
  'FROM ospipits opi ',
  'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta ',
  'WHERE opi.Controle=' + Geral.FF0(Controle),
  'ORDER BY opi.Controle, opi.SobreOrd, opi.Ordem, opi.SubOrdem',
}
  'SELECT pcp.Sigla, pcp.Nome NO_PERGUNTA, ',
  'IF(Respondido<>1, "", ',
  '  ELT(Funcoes, ',
  '  ELT(RespBin + 1, pb0.Nome, pb1.Nome, "   "),',
  '  TRIM(TRAILING ",000" FROM REPLACE(RespQtd, ".", ",")), ',
  '  IF(ppr.GraGruX IS NULL, "Sem adi��o / substitui��o", CONCAT(',
  '    TRIM(TRAILING ",000" FROM REPLACE(ppr.UsoQtd, ".", ",")),',
  '    " unidades da isca ID ", ppr.GraGruX)),',
  '  pai.Nome,',
  '  LEFT(RespTxtLvr, 512))) RESPOSTA, ',
  'opi.*, ppr.IDIts + 0.000 IDIts, ppr.SMI_IDCtrl + 0.000 SMI_IDCtrl',
  'FROM ospipits opi ',
  'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta ',
  '',
  'LEFT JOIN prgatrits pai ON pai.Controle=opi.RespAtrIts',
  'LEFT JOIN ospipitspr ppr ON ppr.Conta=opi.Conta',
  'LEFT JOIN prgbincad pb0 ON pb0.Codigo=opi.BinarCad0',
  'LEFT JOIN prgbincad pb1 ON pb1.Codigo=opi.BinarCad1',
  'WHERE opi.Controle=' + Geral.FF0(Controle),
  'ORDER BY opi.Controle, opi.SuperOrd, opi.SuperSub, ',
  '  opi.SobreOrd, opi.Ordem, opi.SubOrdem',
  '']);
  //

  //
  if Conta <> 0 then
    QrOSPipIts.Locate('Conta', Conta, []);
end;

procedure TUnOSApp_PF.ReopenOSPipMon(QrOSPipMon: TmySQLQuery; Codigo,
  Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPipMon, Dmod.MyDB, [
  'SELECT plc.Nome NO_PrgLstCab, pip.Nome NO_PIP, opm.* ',
  'FROM ospipmon opm ',
  'LEFT JOIN pipcad pip ON pip.Codigo=opm.PipCad ',
  'LEFT JOIN prglstcab plc ON plc.Codigo=opm.PrgLstCab',
  'WHERE opm.Codigo=' + Geral.FF0(Codigo),
  '']);
  //
  if Controle <> 0 then
    QrOSPipMon.Locate('Controle', Controle, []);
end;

procedure TUnOSApp_PF.ReopenOSPos(QrOSPos: TmySQLQuery; Codigo,
  Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPos, Dmod.MyDB, [
  'SELECT IF(Aplicacao=2, pre.Nome, "") NO_AplicID,  ',
  'ELT(Aplicacao+1, "Nenhum", "Visita (O.S.)", "Envio de Email") NO_Aplicacao,  ',
  'osp.*  ',
  'FROM ospos osp ',
  'LEFT JOIN preemail pre ON pre.Codigo=osp.AplicID ',
  'WHERE osp.Codigo=' + Geral.FF0(Codigo),
  'ORDER BY osp.Dias ',
  ' ']);
  //
  if Controle <> 0 then
    QrOSPos.Locate('Controle', Controle, []);
end;

procedure TUnOSApp_PF.ReopenOSPrv(QrOSPrv: TmySQLQuery; Codigo,
  Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPrv, Dmod.MyDB, [
  'SELECT ops.Nome NO_PrvStatus, prv.*, ',
  'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NO_Cliente, ',
  'IF(age.Tipo=0, age.RazaoSocial, age.Nome) NO_Agente, ',
  'eco.Nome NO_Contato, fct.Nome NO_FormContat ',
  'FROM osprv prv ',
  'LEFT JOIN entidades cli ON cli.Codigo=prv.Cliente ',
  'LEFT JOIN entidades age ON age.Codigo=prv.Agente ',
  'LEFT JOIN enticontat eco ON eco.Controle=prv.Contato ',
  'LEFT JOIN formcontat fct ON fct.Codigo=prv.FormContat ',
  'LEFT JOIN osprvsta ops ON ops.Codigo=prv.PrvStatus ',
  'WHERE prv.Codigo=' + Geral.FF0(Codigo),
  '']);
  //
  if Controle <> 0 then
    QrOSPrv.Locate('Codigo', Controle, []);
end;

procedure TUnOSApp_PF.ReopenOSPrz(QrOSPrz: TmySQLQuery; Codigo,
  Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPrz, Dmod.MyDB, [
  'SELECT opz.Parcelas, ',
  'opz.Codigo, opz.Controle, opz.Condicao, ',
  'opz.DescoPer, opz.Escolhido, ppc.Nome NO_CONDICAO ',
  'FROM osprz opz ',
  'LEFT JOIN pediprzcab ppc ON ppc.Codigo=opz.Condicao ',
  'WHERE opz.Codigo=' + Geral.FF0(Codigo),
  ' ']);
  //
  if Controle <> 0 then
    QrOSPrz.Locate('Codigo', Controle, []);
end;

procedure TUnOSApp_PF.ReopenOSSrv(QrOSSrv: TmySQLQuery; Codigo,
  Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSSrv, Dmod.MyDB, [
  'SELECT des.Nome NO_DesServico, ',
  'des.Sigla NO_SIGLA, IF(srv.TudoFeitoA=1, 1,',
  'IF(srv.TudoFeitoM=1, 1, 0)) + 0.000 TUDOFEITO,',
  'srv.* ',
  'FROM ossrv srv ',
  'LEFT JOIN desservico des ON des.Codigo=srv.DesServico ',
  'WHERE srv.Codigo=' + Geral.FF0(Codigo),
  ' ']);
  //
  if Controle <> 0 then
    QrOSSrv.Locate('Controle', Controle, []);
end;

procedure TUnOSApp_PF.ReopenOSSta(QrOSSta: TmySQLQuery; Codigo,
  Controle: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSSta, Dmod.MyDB, [
  'SELECT eos.Nome NO_STATUS, oss.* ',
  'FROM ossta oss ',
  'LEFT JOIN estatusoss eos ON eos.Codigo=oss.Status ',
  'WHERE oss.Codigo=' + Geral.FF0(Codigo),
  'ORDER BY eos.Ordem, eos.Codigo ',
  ' ']);
  //
  if Controle <> 0 then
    QrOSSta.Locate('Controle', Controle, []);
end;

procedure TUnOSApp_PF.ReopenSiapTerCad(QrSiapTerCad: TmySQLQuery; Cliente,
  Codigo: Integer; EdSiapTerCad: TdmkEditCB; CBSiapTerCad: TdmkDBLookupComboBox);
begin
  EdSiapTerCad.ValueVariant := 0;
  CBSiapTerCad.KeyValue := 0;
  //
  if Cliente <> 0 then
    UnDmkDAC_PF.AbreMySQLQuery0(QrSiapTerCad, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM siaptercad ',
    'WHERE Cliente=' + Geral.FF0(Cliente),
    ''])
  else
    QrSiapTerCad.Close;
  //
  if (Codigo <> 0) and (QrSiapTerCad.State <> dsInactive) then
  begin
    if QrSiapTerCad.Locate('Codigo', Codigo, []) then
    begin
      EdSiapTerCad.ValueVariant := Codigo;
      CBSiapTerCad.KeyValue := Codigo;
    end;
  end;
end;

function TUnOSApp_PF.StartaInclusaoOS(Cliente, Grupo: Integer; var Lugar,
  Opcao: Integer; OQueSel: TOSBugsDuplic): Boolean;
begin
  Result := False;
  if OQueSel = dupNenhum then
  begin
    // Fica o lugar e a op��o com os mesmos valores!
    Result := True;
    Exit;
  end else
  begin
    if DBCheck.CriaFm(TFmOSNew, FmOSNew, afmoNegarComAviso) then
    begin
      FmOSNew.FGrupo := Grupo;
      FmOSNew.FCliente := Cliente;
      FmOSNew.FOQueSel := OQueSel;
      ReopenSiapTerCad(FmOSNew.QrSiapTerCad, Cliente, 0,
        FmOSNew.EdSiapTerCad, FmOSNew.CBSiapTerCad);
      //
      FmOSNew.EdOpcao.ValueVariant := Opcao;
      FmOSNew.EdSiapTerCad.ValueVariant := Lugar;
      FmOSNew.CBSiapTerCad.KeyValue := Lugar;
      case OQueSel of
        dupAmbos: ; // Habilita tudo
        dupPorLugar: FmOSNew.EdOpcao.Visible := False;
        dupPorOpcao:
        begin
          FmOSNew.EdSiapTerCad.Enabled := False;
          FmOSNew.CBSiapTerCad.Enabled := False;
        end;
        //dupNenhum: Feito acima! Ficam os mesmos valores!
      end;
      FmOSNew.ShowModal;
      //
      //
      Result := FmOSNew.FConfirmou;
      if Result then
      begin
        case OQueSel of
          dupAmbos:
          begin
            Lugar := FmOSNew.EdSiapTerCad.ValueVariant;
            Opcao := FmOSNew.EdOpcao.ValueVariant;
          end;
          dupPorLugar:
          begin
            Lugar := FmOSNew.EdSiapTerCad.ValueVariant;
          end;
          dupPorOpcao:
          begin
            Opcao := FmOSNew.EdOpcao.ValueVariant;
          end;
        end;
      end else
      begin
        Lugar := 0;
        Opcao := 0;
      end;
      FmOSNew.Destroy;
    end;
  end;
end;

function TUnOSApp_PF.TemProvidenciaOrfa(Cliente: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDMkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT COUNT(*) Itens ',
    'FROM osprv ',
    'WHERE Cliente=' + Geral.FF0(Cliente),
    'AND Codigo=0 ',
    ' ']);
    //
    Result := Qry.FieldByName('Itens').AsInteger > 0;
  finally
    Qry.Free;
  end;
end;

procedure TUnOSApp_PF.TotalValorOS(Codigo: Integer);
const
  NoTab = '? ? ?';
  procedure ReabreSoma(Tab, Filtro: String);
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrTotalOS, Dmod.MyDB, [
    'SELECT SUM(ValCalc) ValCalc, SUM(ValInfo) ValInfo, ',
    'SUM(ValDesc) ValDesc, SUM(ValTota) ValTota ',
    'FROM  ' + Tab,
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND Autorizado' + Filtro + '1',
    '']);
  end;
var
  DescoPerce,
  ValorTotal, ValorServi, ValorDesco: Double;
  InvalTotal, InvalServi, InvalDesco: Double;
  OrcamTotal, OrcamServi, OrcamDesco: Double;
  MulServico, Versao: String;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrEscolhOSPrz, Dmod.MyDB, [
  'SELECT DescoPer ',
  'FROM osprz ',
  'WHERE Escolhido=1 ',
  'AND Codigo=' + Geral.FF0(Codigo),
  '']);
  //
  DescoPerce := DmModOS.QrEscolhOSPrzDescoPer.Value / 100;
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE ossrv ',
  'SET ValDesc=(ValCalc+ValInfo)*' +
  Geral.FFT_Dot(DescoPerce, 6, siPositivo) + ', ',
  'ValTota=(ValCalc+ValInfo)*' +
  Geral.FFT_Dot(1 - DescoPerce, 6, siPositivo),
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  //
  DescoPerce := DmModOS.QrEscolhOSPrzDescoPer.Value / 100;
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'UPDATE oscxa ',
  'SET ValDesc=(ValCalc+ValInfo)*' +
  Geral.FFT_Dot(DescoPerce, 6, siPositivo) + ', ',
  'ValTota=(ValCalc+ValInfo)*' +
  Geral.FFT_Dot(1 - DescoPerce, 6, siPositivo),
  'WHERE Codigo=' + Geral.FF0(Codigo),
  '']);
  //
  ValorTotal := 0;
  ValorServi := 0;
  ValorDesco := 0;
  InvalTotal := 0;
  InvalServi := 0;
  InvalDesco := 0;
  //OrcamTotal := 0;
  //OrcamServi := 0;
  //OrcamDesco := 0;
  //
  ReabreSoma('ossrv', '=');
  //
  ValorServi     := ValorServi + DmModOS.QrTotalOSValCalc.Value + DmModOS.QrTotalOSValInfo.Value;
  ValorDesco     := ValorDesco + DmModOS.QrTotalOSValDesc.Value;
  ValorTotal     := ValorTotal + DmModOS.QrTotalOSValTota.Value;
  //
  ReabreSoma('ossrv', '<>');
  //
  InvalServi     := InvalServi + DmModOS.QrTotalOSValCalc.Value + DmModOS.QrTotalOSValInfo.Value;
  InvalDesco     := InvalDesco + DmModOS.QrTotalOSValDesc.Value;
  InvalTotal     := InvalTotal + DmModOS.QrTotalOSValTota.Value;
  //
  ReabreSoma('oscxa', '=');
  //
  ValorServi     := ValorServi + DmModOS.QrTotalOSValCalc.Value + DmModOS.QrTotalOSValInfo.Value;
  ValorDesco     := ValorDesco + DmModOS.QrTotalOSValDesc.Value;
  ValorTotal     := ValorTotal + DmModOS.QrTotalOSValTota.Value;
  //
  ReabreSoma('oscxa', '<>');
  //
  InvalServi     := InvalServi + DmModOS.QrTotalOSValCalc.Value + DmModOS.QrTotalOSValInfo.Value;
  InvalDesco     := InvalDesco + DmModOS.QrTotalOSValDesc.Value;
  InvalTotal     := InvalTotal + DmModOS.QrTotalOSValTota.Value;
  //
  //
  //
  OrcamServi     := InvalServi + ValorServi;
  OrcamDesco     := InvalDesco + ValorDesco;
  OrcamTotal     := InvalTotal + ValorTotal;
  //
  //
  try
    //SHOW VARIABLES LIKE 'version'
    Versao := DModG.ObtemVersaoMySQLServer();
    if Copy(Versao, 1, 3) = '4.1' then
      UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrMSrv, Dmod.MyDB, [
      'SELECT DISTINCT ',
      // N�o funciona no MySQL "4.1.13a-nt"  mas funciona no "5.5.21"
      //'SUM(DISTINCT(POWER(2, DesServico-1))) MulServico ',
      'SUM(/*DISTINCT*/(POWER(2, DesServico-1))) MulServico ',
      'FROM ossrv ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'GROUP BY Codigo ',
      ''])
    else
      UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrMSrv, Dmod.MyDB, [
      'SELECT DISTINCT ',
      // N�o funciona no MySQL "4.1.13a-nt"  mas funciona no "5.5.21"
      'SUM(DISTINCT(POWER(2, DesServico-1))) MulServico ',
      //'SUM(/*DISTINCT*/(POWER(2, DesServico-1))) MulServico ',
      'FROM ossrv ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      'GROUP BY Codigo ',
      '']);
    MulServico := Geral.FFI(DmModOS.QrMSrvMulServico.Value);
    //
  except
    Geral.MB_Erro('N�o foi poss�vel definir os multi servi�os!');
  end;
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
  'ValorTotal', 'ValorServi', 'ValorDesco',
  'InvalTotal', 'InvalServi', 'InvalDesco',
  'OrcamTotal', 'OrcamServi', 'OrcamDesco',
  'MulServico'], ['Codigo'], [
  ValorTotal, ValorServi, ValorDesco,
  InvalTotal, InvalServi, InvalDesco,
  OrcamTotal, OrcamServi, OrcamDesco,
  MulServico], [Codigo], True);
end;

function TUnOSApp_PF.TotalValorServico(Calc, Info, Desc: Double): Double;
begin
  Result := Calc + Info - Desc;
  if Result <= - 0.01 then
    Geral.MensagemBox('Aviso!' + #13#10 +
    'Valor negativo para total de servi�o: $ ' + Geral.FFT(Result, 2, siNegativo),
    'Aviso', MB_OK+MB_ICONWARNING);
end;

function TUnOSApp_PF.VeSeCriaOSsFuturas(Codigo: Integer): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  Qry := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT DtaExeFim, PosGerou ',
    'FROM oscab ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    '']);
    //
    // Verifica se j� criou as OSs futuras
    if Qry.FieldByName('PosGerou').AsInteger = 1 then
      Exit;
    //
    // Verifica se terminou a execu��o da OS
    if Qry.FieldByName('DtaExeFim').AsDateTime < 2 then
      Exit;

    // Verifica se j� confirmou o status CO_BUG_STATUS_0700_EXE_FINA = 0700 - Execu��o finalizada
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Status ',
    'FROM ossta ',
    'WHERE Codigo=' + Geral.FF0(Codigo),
    'AND Status=' + Geral.FF0(CO_BUG_STATUS_0700_EXE_FINA),
    '']);
    if Qry.RecordCount = 0 then
      Exit;

    ////
      
    Result := True;
    //
  finally
    Qry.Free;
  end;
end;

procedure TUnOSApp_PF.ZeraDiluente(BugsServi: TGraBugsServi; Receita: Integer;
TempTb: String = '');
const
  EhDiluente = 0;
var
  Tabela, IDReceita: String;
  QrUpd: TmySQLQuery;
begin
  IdReceita := 'Conta';
  case BugsServi of
    gbsAplica: Tabela := 'osfrmrec';
    gbsMonitora: Tabela := 'osmonrec';
    gbsMonMulti: Tabela := TempTb;
    gbsAplEMon: Tabela := 'formulif';
    else Tabela := '???';
  end;
  case BugsServi of
    gbsAplica: IdReceita := 'Conta';
    gbsMonitora: IdReceita := 'Conta';
    gbsMonMulti: IdReceita := 'Conta'; // ???
    gbsAplEMon: IdReceita := 'Codigo';
    else Tabela := '???';
  end;
  case BugsServi of
    gbsAplica, gbsMonitora, gbsAplEMon: QrUpd := Dmod.QrUpd;
    gbsMonMulti: QrUpd := DModG.QrUpdPID1;
    else QrUpd := nil;
  end;
  //
  // Cuidado!! "Conta" n�o � o ID da tabela!
  UMyMod.SQLInsUpd(QrUpd, stUpd, Tabela, False, [
  'EhDiluente'], [IdReceita], [EhDiluente], [Receita], True);
end;

end.

