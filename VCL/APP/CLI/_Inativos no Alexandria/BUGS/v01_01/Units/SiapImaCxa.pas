unit SiapImaCxa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  Menus, dmkMemo, dmkImage, UnDmkEnums;

type
  TFmSiapImaCxa = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    Panel8: TPanel;
    Label16: TLabel;
    EdLocal: TdmkEdit;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    QrCxaMaters: TmySQLQuery;
    DsMatersCxa: TDataSource;
    QrCxaFormas: TmySQLQuery;
    DsFormasCxa: TDataSource;
    EdMatersCxa: TdmkEditCB;
    Label1: TLabel;
    CBMatersCxa: TdmkDBLookupComboBox;
    Label5: TLabel;
    EdFormasCxa: TdmkEditCB;
    CBFormasCxa: TdmkDBLookupComboBox;
    SbObjeto: TSpeedButton;
    SbFinalidade: TSpeedButton;
    Label8: TLabel;
    EdMedidas: TdmkEdit;
    Label9: TLabel;
    EdVolumeL: TdmkEdit;
    MeAcesso: TdmkMemo;
    Label4: TLabel;
    Label2: TLabel;
    EdControle: TdmkEdit;
    Label3: TLabel;
    QrCxaMatersCodigo: TIntegerField;
    QrCxaMatersNome: TWideStringField;
    QrCxaFormasCodigo: TIntegerField;
    QrCxaFormasNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbObjetoClick(Sender: TObject);
    procedure SbFinalidadeClick(Sender: TObject);
  private
    { Private declarations }
    //FSCEP: String;
  public
    { Public declarations }
    FForm: TForm;
  end;

  var
  FmSiapImaCxa: TFmSiapImaCxa;

implementation

uses UnMyObjects, Module, EntiCEP, MyDBCheck, UnCEP, CunsCad, UMySQLModule,
  Principal;

{$R *.DFM}

procedure TFmSiapImaCxa.BtOKClick(Sender: TObject);
var
  Local, Acesso, Medidas: String;
  Codigo, Controle, MatersCxa, FormasCxa: Integer;
  VolumeL: Double;
begin
  Codigo         := EdCodigo.ValueVariant;
  Controle       := EdControle.ValueVariant;
  MatersCxa      := EdMatersCxa.ValueVariant;
  FormasCxa      := EdFormasCxa.ValueVariant;
  VolumeL        := EdVolumeL.ValueVariant;
  Local          := EdLocal.Text;
  Acesso         := MeAcesso.Text;
  Medidas        := EdMedidas.Text;
  //
  Controle := UMyMod.BPGS1I32(
    'siapimacxa', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'siapimacxa', False, [
  'Codigo', 'MatersCxa', 'FormasCxa',
  'VolumeL', 'Local', 'Acesso',
  'Medidas'], [
  'Controle'], [
  Codigo, MatersCxa, FormasCxa,
  VolumeL, Local, Acesso,
  Medidas], [
  Controle], True) then
  begin
    TFmCunsCad(FForm).ReopenSiapImaCxa(Controle);
    Close;
  end;
end;

procedure TFmSiapImaCxa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSiapImaCxa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSiapImaCxa.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  QrCxaFormas.Open;
  QrCxaMaters.Open;
end;

procedure TFmSiapImaCxa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSiapImaCxa.SbFinalidadeClick(Sender: TObject);
begin
  FmPrincipal.MostraFormCxaFormas();
  UMyMod.SetaCodigoPesquisado(EdFormasCxa, CBFormasCxa, QrCxaFormas, VAR_CADASTRO);
end;

procedure TFmSiapImaCxa.SbObjetoClick(Sender: TObject);
begin
  FmPrincipal.MostraFormCxaMaters();
  UMyMod.SetaCodigoPesquisado(EdMatersCxa, CBMatersCxa, QrCxaMaters, VAR_CADASTRO);
end;

end.
