unit OSCabEncerr;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, dmkEdit,
  dmkEditDateTimePicker, dmkEditCB, dmkDBLookupComboBox, mySQLDbTables;

type
  TFmOSCabEncerr = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox3: TGroupBox;
    Label18: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    TPDtaVisExe: TdmkEditDateTimePicker;
    EdDtaVisExe: TdmkEdit;
    TPFimVisExe: TdmkEditDateTimePicker;
    EdFimVisExe: TdmkEdit;
    GroupBox4: TGroupBox;
    Label20: TLabel;
    Label21: TLabel;
    Label51: TLabel;
    TPDtaExeIni: TdmkEditDateTimePicker;
    TPDtaExeFim: TdmkEditDateTimePicker;
    EdDtaExeIni: TdmkEdit;
    EdDtaExeFim: TdmkEdit;
    Label14: TLabel;
    CBEstatus: TdmkDBLookupComboBox;
    EdEstatus: TdmkEditCB;
    QrEstatusOSs: TmySQLQuery;
    QrEstatusOSsCodigo: TIntegerField;
    QrEstatusOSsNome: TWideStringField;
    DsEstatusOSs: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure TPDtaVisExeExit(Sender: TObject);
    procedure EdDtaVisExeExit(Sender: TObject);
    procedure TPDtaExeIniExit(Sender: TObject);
    procedure EdDtaExeIniExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FOSCab2: TForm;
    FCodigo: Integer;
    FDtaVisPrv: TDateTime;
  end;

  var
  FmOSCabEncerr: TFmOSCabEncerr;

implementation

uses UnMyObjects, Module, OSCab2, UMySQLModule;

{$R *.DFM}

procedure TFmOSCabEncerr.BtOKClick(Sender: TObject);
var
  Status: Integer;
  DtaVisExe, FimVisExe, DtaExeIni, DtaExeFim: String;
begin
  DtaVisExe := Geral.FDT(TPDtaVisExe.Date, 1) + ' ' + EdDtaVisExe.Text + ':00';
  FimVisExe := Geral.FDT(TPFimVisExe.Date, 1) + ' ' + EdFimVisExe.Text + ':00';
  DtaExeIni := Geral.FDT(TPDtaExeIni.Date, 1) + ' ' + EdDtaExeIni.Text + ':00';
  DtaExeFim := Geral.FDT(TPDtaExeFim.Date, 1) + ' ' + EdDtaExeFim.Text + ':00';
  Status    := EdEstatus.ValueVariant;
  //
  if MyObjects.FIC(FCodigo = 0, nil, 'ID da OS n�o definido!') then Exit;
  if MyObjects.FIC(Status = 0, EdEstatus, 'Status n�o definido!') then Exit;
  if MyObjects.FIC((FDtaVisPrv > 2) and (Geral.STD_109(DtaVisExe) < 2)
    and (Geral.STD_109(FimVisExe) < 2), nil, 'Informe a data e hora da vistoria!') then
    Exit;
  if TFmOSCab2(FOSCab2).ImpedePelaDataFinalDeExecucao(Geral.STD_109(DtaExeFim)) then Exit;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False,
    ['Estatus', 'DtaVisExe', 'FimVisExe', 'DtaExeIni', 'DtaExeFim'], ['Codigo'],
    [Status, DtaVisExe, FimVisExe, DtaExeIni, DtaExeFim], [FCodigo], True)
  then
    Close;
end;

procedure TFmOSCabEncerr.BtSaidaClick(Sender: TObject);
begin
  FCodigo := 0;
  Close;
end;

procedure TFmOSCabEncerr.EdDtaExeIniExit(Sender: TObject);
begin
  if (EdDtaExeFim.Text = '00:00') and (TPDtaExeIni.Date > 2) then
    EdDtaExeFim.ValueVariant := Geral.STT(EdDtaExeIni.Text, '01:00:00', False);
end;

procedure TFmOSCabEncerr.EdDtaVisExeExit(Sender: TObject);
begin
  if (EdFimVisExe.Text = '00:00') and (TPDtaVisExe.Date > 2) then
    EdFimVisExe.ValueVariant := Geral.STT(EdDtaVisExe.Text, '01:00:00', False);
end;

procedure TFmOSCabEncerr.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSCabEncerr.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UMyMod.AbreQuery(QrEstatusOSs, Dmod.MyDB);
end;

procedure TFmOSCabEncerr.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSCabEncerr.TPDtaExeIniExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (TPDtaExeFim.Date < 2) then
    TPDtaExeFim.Date := TPDtaExeIni.Date;
  //
  if (EdDtaExeIni.Text = '00:00') and (EdDtaExeFim.Text = '01:00') then
    EdDtaExeFim.ValueVariant := '00:00';
end;

procedure TFmOSCabEncerr.TPDtaVisExeExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (TPFimVisExe.Date < 2) then
    TPFimVisExe.Date := TPDtaVisExe.Date;
  //
  if (EdDtaVisExe.Text = '00:00') and (EdFimVisExe.Text = '01:00') then
    EdFimVisExe.ValueVariant := '00:00';
end;

end.
