unit OSGarantCls;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, UnDmkEnums, Variants,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, mySQLDbTables, dmkDBGridZTO;

type
  TFmOSGarantCls = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrMotPosGar: TmySQLQuery;
    QrMotPosGarCodigo: TIntegerField;
    QrMotPosGarNome: TWideStringField;
    DsMotPosGar: TDataSource;
    Label12: TLabel;
    EdEncerraMot: TdmkEditCB;
    CBEncerraMot: TdmkDBLookupComboBox;
    SBChekLstCab: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBChekLstCabClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    //FTabela,
    FFldCtrl: String;
    //FValCtrl: array of Integer;
    FDBG: TDmkDBGridZTO;
    FQry: TmySQLQuery;
  end;

  var
  FmOSGarantCls: TFmOSGarantCls;

implementation

uses UnMyObjects, Module, CfgCadLista, UMySQLModule, DmkDAC_PF, ModuleGeral;

{$R *.DFM}

procedure TFmOSGarantCls.BtOKClick(Sender: TObject);
var
  EncerraDta: String;
  EncerraMot, EncerraUsu: Integer;
  //
  procedure EncerraAtual();
  var
    Tabela: String;
    ValCtrl: Integer;
  begin
    Tabela  := FQry.FieldByName('Tabela').AsString;
    ValCtrl := FQry.FieldByName('Controle').AsInteger;
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
    'EncerraMot', 'EncerraDta', 'EncerraUsu'], [
    FFldCtrl], [
    EncerraMot, EncerraDta, EncerraUsu], [
    ValCtrl], True);
  end;
var
  I: Integer;
begin
  EncerraMot     := EdEncerraMot.ValueVariant;
  EncerraDta     := Geral.FDT(DModG.ObtemAgora(), 109);
  EncerraUsu     := VAR_USUARIO;
  //
  if MyObjects.FIC(EncerraMot = 0, EdEncerraMot, 'Informe um motivo!') then
    Exit;
  if FDBG.SelectedRows.Count > 1 then
  begin
    if Geral.MB_Pergunta('Confirma o encerramento dos ' + Geral.FF0(
    FDBG.SelectedRows.Count) + ' itens selecionados?') = ID_YES then
    begin
      with FDBG.DataSource.DataSet do
      for I := 0 to FDBG.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(FDBG.SelectedRows.Items[i]));
        EncerraAtual();
      end;
      Close;
    end;
  end else begin
    if Geral.MB_Pergunta('Confirma o encerramento do item selecionado?') = ID_YES then
    begin
      EncerraAtual();
      Close;
    end;
  end;
end;

procedure TFmOSGarantCls.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSGarantCls.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSGarantCls.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrMotPosGar, Dmod.MyDB);
end;

procedure TFmOSGarantCls.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSGarantCls.SBChekLstCabClick(Sender: TObject);
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'motposgar', 60, ncGerlSeq1,
  'Motivos de P�s Garantia',
  [], False, Null, [], [], False);
  UMyMod.SetaCodigoPesquisado(EdEncerraMot, CBEncerraMot, QrMotPosGar,
    VAR_CADASTRO, 'Codigo');
end;

end.
