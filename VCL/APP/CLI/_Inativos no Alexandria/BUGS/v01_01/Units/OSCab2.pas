unit OSCab2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2, TypInfo,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  Menus, Grids, DBGrids, UnDmkProcFunc, dmkDBLookupComboBox, dmkEditCB,
  ComCtrls, dmkEditDateTimePicker, dmkValUsu, frxClass, frxDBSet, dmkDBGrid,
  dmkLabelRotate, UnBugs_Tabs, dmkCheckGroup, dmkMemo, math, dmkCompoStore,
  Variants, UnOSApp_PF, UnContratUnit, DBCGrids, dmkDBGridZTO, UnDmkEnums,
  UnProjGroup_Consts, dmkCheckBox;

type
  TNivelExclusao = (nivexclProforma, nivexclOSLugar, nivexclOSOpcao,
  nivexclOSCabAll, nivexclOSCabUni,
  nivexclServicosAll, nivexclServicosUni, // N�o usa nivexclServicosUni
  nivexclDedetsAll, nivexclDedetsUni,
  nivexclCaixasAll, nivexclCaixasUni,
  nivexclMonitsAll, nivexclMonitsUni);
  THackDBGrid = class(TDBGrid);
  TFmOSCab2 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    QrOSCab: TmySQLQuery;
    DsOSCab: TDataSource;
    PMOSSrv: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMOSCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    QrOSCabCodigo: TIntegerField;
    QrOSCabEntidade: TIntegerField;
    QrOSCabEstatus: TIntegerField;
    QrOSCabFatoGeradr: TIntegerField;
    QrOSCabDtaVisPrv: TDateTimeField;
    QrOSCabDtaVisExe: TDateTimeField;
    QrOSCabDtaExePrv: TDateTimeField;
    QrOSCabDtaExeIni: TDateTimeField;
    QrOSCabDtaExeFim: TDateTimeField;
    QrOSCabLk: TIntegerField;
    QrOSCabDataCad: TDateField;
    QrOSCabDataAlt: TDateField;
    QrOSCabUserCad: TIntegerField;
    QrOSCabUserAlt: TIntegerField;
    QrOSCabAlterWeb: TSmallintField;
    QrOSCabAtivo: TSmallintField;
    QrOSCabNO_FatoGeradr: TWideStringField;
    QrOSCabNO_ESTATUS: TWideStringField;
    QrOSCabNO_ENT: TWideStringField;
    QrOSCabSiapTerCad: TIntegerField;
    QrOSCabNO_SiapTerCad: TWideStringField;
    QrOSCabDtaContat: TDateTimeField;
    QrOSCabTXTVisPrv: TWideStringField;
    QrOSCabTXTContat: TWideStringField;
    QrOSCabTXTVisExe: TWideStringField;
    QrOSCabTXTExePrv: TWideStringField;
    QrOSCabTXTExeIni: TWideStringField;
    QrOSCabTXTExeFim: TWideStringField;
    QrOSCabValorTotal: TFloatField;
    QrOSCabDdsPosVda: TIntegerField;
    PMOSFrmCab: TPopupMenu;
    ItsInclui4: TMenuItem;
    ItsAltera4: TMenuItem;
    ItsExclui4: TMenuItem;
    QrOSCabEntiContat: TIntegerField;
    QrOSCabNumContrat: TIntegerField;
    QrOSCabEntPagante: TIntegerField;
    QrOSCabEntContrat: TIntegerField;
    PMOSCabAlv: TPopupMenu;
    ExcluiOSCabAlv1: TMenuItem;
    Cabealho4: TMenuItem;
    Itens4: TMenuItem;
    QrOSCabEmpresa: TIntegerField;
    PMOSMonCab: TPopupMenu;
    Cabecalho5: TMenuItem;
    ItsInclui5: TMenuItem;
    ItsAltera5: TMenuItem;
    ItsExclui5: TMenuItem;
    Itens5: TMenuItem;
    QrOSCabValorServi: TFloatField;
    QrOSCabValorDesco: TFloatField;
    IncluiOSCabAlv1: TMenuItem;
    QrOSCabNO_PAG: TWideStringField;
    QrOSCabNO_CTR: TWideStringField;
    QrOSCabDtaLibFat: TDateTimeField;
    QrOSCabDtaFimFat: TDateTimeField;
    QrOSCabValorOutrs: TFloatField;
    QrOSCabCondicaoPg: TIntegerField;
    QrOSCabCartEmis: TIntegerField;
    QrOSCabNO_CART: TWideStringField;
    QrOSCabNO_PRZ: TWideStringField;
    QrOSCabSerNF: TWideStringField;
    QrOSCabNumNF: TIntegerField;
    PMFat: TPopupMenu;
    Gerabloqueto1: TMenuItem;
    N4: TMenuItem;
    Excluifaturamento1: TMenuItem;
    Fatura1: TMenuItem;
    Abrangncia1: TMenuItem;
    QrOSCabNO_ENTICONTAT: TWideStringField;
    QrOSCabTel_ENT: TWideStringField;
    QrOSCabTXTTel_ENT: TWideStringField;
    PMOSPos: TPopupMenu;
    IncluiC3: TMenuItem;
    AlteraC3: TMenuItem;
    ExcluiC3: TMenuItem;
    QrOSCabInvalServi: TFloatField;
    QrOSCabInvalDesco: TFloatField;
    QrOSCabInvalOutrs: TFloatField;
    QrOSCabInvalTotal: TFloatField;
    QrOSCabOrcamServi: TFloatField;
    QrOSCabOrcamDesco: TFloatField;
    QrOSCabOrcamOutrs: TFloatField;
    QrOSCabOrcamTotal: TFloatField;
    QrOSCabValiDdOrca: TIntegerField;
    QrOSCabOperacao: TIntegerField;
    PMOSCxa: TPopupMenu;
    ItsInclui2: TMenuItem;
    ItsAltera2: TMenuItem;
    ItsExclui2: TMenuItem;
    QrOSCabExeTxtCli2: TIntegerField;
    QrOSCabNO_ExeTxtCli2: TWideStringField;
    PMOSPrz: TPopupMenu;
    OSPrz1_Inclui: TMenuItem;
    OSPrz1_Altera: TMenuItem;
    OSPrz1_Exclui: TMenuItem;
    QrOSCabValorPre: TFloatField;
    GBOperacao: TGroupBox;
    LaOperacaoA: TLabel;
    LaOperacaoB: TLabel;
    LaOperacaoC: TLabel;
    QrOSCabNO_OPERACAO: TWideStringField;
    QrOSCabObsExecuta: TWideMemoField;
    PMOSPipMon: TPopupMenu;
    AdicionaPIPsativos1: TMenuItem;
    PMOSCxI: TPopupMenu;
    ItsInclui6: TMenuItem;
    ItsAltera6: TMenuItem;
    ItsExclui6: TMenuItem;
    PMOSAge: TPopupMenu;
    IncluiOSAge1: TMenuItem;
    RemoveOSAge1: TMenuItem;
    QrOSCabNO_ExeTxtCli1: TWideStringField;
    QrOSCabExeTxtCli1: TIntegerField;
    QrOSCabFimVisPrv: TDateTimeField;
    QrOSCabFimVisExe: TDateTimeField;
    QrOSCabFimExePrv: TDateTimeField;
    PnForm: TPanel;
    PnQrys: TPanel;
    PnDados: TPanel;
    PCGeral: TPageControl;
    TabSheet1: TTabSheet;
    PCOpera: TPageControl;
    TabSheet4: TTabSheet;
    PnDedetizacao: TPanel;
    TabSheet5: TTabSheet;
    PnCaixaDAgua: TPanel;
    PageControl3: TPageControl;
    GroupBox22: TGroupBox;
    Panel27: TPanel;
    DBGOSCxaAtrib: TdmkDBGrid;
    Panel11: TPanel;
    GroupBox23: TGroupBox;
    Panel28: TPanel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    dmkLabelRotate1: TdmkLabelRotate;
    DBEdNOME_FORMA: TDBEdit;
    DBEdNO_MATERIAL: TDBEdit;
    DBEdVolumeL: TDBEdit;
    DBEdMedidas: TDBEdit;
    DBMeAcesso: TDBMemo;
    GroupBox24: TGroupBox;
    Splitter5: TSplitter;
    DBGOSCxI: TDBGrid;
    DBMeOSCxI: TDBMemo;
    Panel13: TPanel;
    Splitter6: TSplitter;
    DBGOSCxa: TdmkDBGridZTO;
    DBMeOSCxa: TDBMemo;
    TabSheet6: TTabSheet;
    PnMonitoramento: TPanel;
    DBGOSPipMon: TDBGrid;
    TabSheet2: TTabSheet;
    Panel9: TPanel;
    GroupBox15: TGroupBox;
    GroupBox16: TGroupBox;
    DBGLctFatRef: TDBGrid;
    TabSheet3: TTabSheet;
    GroupBox18: TGroupBox;
    Splitter3: TSplitter;
    DBGEntiTel: TDBGrid;
    DBGEntiMail: TDBGrid;
    GroupBox19: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtOSCab: TBitBtn;
    BtOSSrv: TBitBtn;
    BtOSFrmCab: TBitBtn;
    BtOSAlv: TBitBtn;
    BtOSMonCab: TBitBtn;
    BtFat: TBitBtn;
    PnEdita: TPanel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrLctFatRef: TmySQLQuery;
    QrLctFatRefPARCELA: TIntegerField;
    QrLctFatRefCodigo: TIntegerField;
    QrLctFatRefControle: TIntegerField;
    QrLctFatRefConta: TIntegerField;
    QrLctFatRefLancto: TLargeintField;
    QrLctFatRefValor: TFloatField;
    QrLctFatRefVencto: TDateField;
    DsLctFatRef: TDataSource;
    QrOSAlv: TmySQLQuery;
    QrOSAlvCodigo: TIntegerField;
    QrOSAlvControle: TIntegerField;
    QrOSAlvConta: TIntegerField;
    QrOSAlvNO_Praga_Z: TWideStringField;
    QrOSAlvPraga_Z: TIntegerField;
    DsOSAlv: TDataSource;
    QrOSSrv: TmySQLQuery;
    QrOSSrvNO_DesServico: TWideStringField;
    QrOSSrvCodigo: TIntegerField;
    QrOSSrvControle: TIntegerField;
    QrOSSrvDesServico: TIntegerField;
    QrOSSrvLk: TIntegerField;
    QrOSSrvDataCad: TDateField;
    QrOSSrvDataAlt: TDateField;
    QrOSSrvUserCad: TIntegerField;
    QrOSSrvUserAlt: TIntegerField;
    QrOSSrvAlterWeb: TSmallintField;
    QrOSSrvAtivo: TSmallintField;
    QrOSSrvGarantiaDd: TIntegerField;
    QrOSSrvHrEvacuar: TIntegerField;
    QrOSSrvHrExecutar: TFloatField;
    QrOSSrvValCalc: TFloatField;
    QrOSSrvValInfo: TFloatField;
    QrOSSrvValDesc: TFloatField;
    QrOSSrvValTota: TFloatField;
    QrOSSrvAutorizado: TSmallintField;
    QrOSSrvVAL_CALCeINFO: TFloatField;
    QrOSSrvAUTORIZADO_BOOL: TBooleanField;
    QrOSSrvNO_SIGLA: TWideStringField;
    QrOSSrvDetalhes: TWideMemoField;
    DsOSSrv: TDataSource;
    QrOSCxa: TmySQLQuery;
    QrOSCxaCodigo: TIntegerField;
    QrOSCxaControle: TIntegerField;
    QrOSCxaCaixa: TIntegerField;
    QrOSCxaGarantiaDd: TIntegerField;
    QrOSCxaHrEvacuar: TIntegerField;
    QrOSCxaHrExecutar: TFloatField;
    QrOSCxaValCalc: TFloatField;
    QrOSCxaValInfo: TFloatField;
    QrOSCxaValDesc: TFloatField;
    QrOSCxaValTota: TFloatField;
    QrOSCxaAutorizado: TSmallintField;
    QrOSCxaLk: TIntegerField;
    QrOSCxaDataCad: TDateField;
    QrOSCxaDataAlt: TDateField;
    QrOSCxaUserCad: TIntegerField;
    QrOSCxaUserAlt: TIntegerField;
    QrOSCxaAlterWeb: TSmallintField;
    QrOSCxaAtivo: TSmallintField;
    QrOSCxaNO_MATERIAL: TWideStringField;
    QrOSCxaNOME_FORMA: TWideStringField;
    QrOSCxaMatersCxa: TIntegerField;
    QrOSCxaFormasCxa: TIntegerField;
    QrOSCxaVolumeL: TFloatField;
    QrOSCxaLocal: TWideStringField;
    QrOSCxaAcesso: TWideStringField;
    QrOSCxaMedidas: TWideStringField;
    QrOSCxaChekLstCab: TIntegerField;
    QrOSCxaDetalhes: TWideMemoField;
    DsOSCxa: TDataSource;
    QrOSCxI: TmySQLQuery;
    QrOSCxICodigo: TIntegerField;
    QrOSCxIControle: TIntegerField;
    QrOSCxIFotoCxa: TWideStringField;
    QrOSCxIDetalhes: TWideMemoField;
    QrOSCxILk: TIntegerField;
    QrOSCxIDataCad: TDateField;
    QrOSCxIDataAlt: TDateField;
    QrOSCxIUserCad: TIntegerField;
    QrOSCxIUserAlt: TIntegerField;
    QrOSCxIAlterWeb: TSmallintField;
    QrOSCxIAtivo: TSmallintField;
    QrOSCxIAplicacao: TIntegerField;
    QrOSCxINO_APLICACAO: TWideStringField;
    DsOSCxI: TDataSource;
    QrOSFrmDep: TmySQLQuery;
    QrOSFrmDepCodigo: TIntegerField;
    QrOSFrmDepControle: TIntegerField;
    QrOSFrmDepConta: TIntegerField;
    QrOSFrmDepTabela: TSmallintField;
    QrOSFrmDepCadastro: TIntegerField;
    QrOSFrmDepLk: TIntegerField;
    QrOSFrmDepDataCad: TDateField;
    QrOSFrmDepDataAlt: TDateField;
    QrOSFrmDepUserCad: TIntegerField;
    QrOSFrmDepUserAlt: TIntegerField;
    QrOSFrmDepAlterWeb: TSmallintField;
    QrOSFrmDepAtivo: TSmallintField;
    QrOSFrmDepSIGLA: TWideStringField;
    QrOSFrmDepNO_Campo: TWideStringField;
    QrOSFrmDepIDIts: TIntegerField;
    DsOSFrmDep: TDataSource;
    QrOSFrmCab: TmySQLQuery;
    QrOSFrmCabCodigo: TIntegerField;
    QrOSFrmCabControle: TIntegerField;
    QrOSFrmCabConta: TIntegerField;
    QrOSFrmCabNome: TWideStringField;
    QrOSFrmCabFormula: TIntegerField;
    QrOSFrmCabEquipAplic: TIntegerField;
    QrOSFrmCabQtdTot: TFloatField;
    QrOSFrmCabQtdQSP: TFloatField;
    QrOSFrmCabCusTot: TFloatField;
    QrOSFrmCabNO_FORMULA: TWideStringField;
    QrOSFrmCabNO_EquipAplic: TWideStringField;
    QrOSFrmCabDiluente: TSmallintField;
    QrOSFrmCabTipoAplica: TIntegerField;
    QrOSFrmCabUnidMed: TIntegerField;
    QrOSFrmCabCU_UNIDMED: TIntegerField;
    QrOSFrmCabSIGLA: TWideStringField;
    QrOSFrmCabNO_UNIDMED: TWideStringField;
    DsOSFrmCab: TDataSource;
    QrOSFrmRec: TmySQLQuery;
    QrOSFrmRecNO_GG1: TWideStringField;
    QrOSFrmRecCodigo: TIntegerField;
    QrOSFrmRecControle: TIntegerField;
    QrOSFrmRecConta: TIntegerField;
    QrOSFrmRecIDIts: TIntegerField;
    QrOSFrmRecGraGruX: TIntegerField;
    QrOSFrmRecPrvQtd: TFloatField;
    QrOSFrmRecPrvPrc: TFloatField;
    QrOSFrmRecPrvVal: TFloatField;
    QrOSFrmRecUsoQtd: TFloatField;
    QrOSFrmRecUsoPrc: TFloatField;
    QrOSFrmRecUsoVal: TFloatField;
    QrOSFrmRecUsoDec: TFloatField;
    QrOSFrmRecUsoTot: TFloatField;
    QrOSFrmRecOrdem: TIntegerField;
    QrOSFrmRecReordem: TIntegerField;
    DsOSFrmRec: TDataSource;
    QrOSFrmAbr: TmySQLQuery;
    QrOSFrmAbrNO_ABRANGE: TWideStringField;
    QrOSFrmAbrCodigo: TIntegerField;
    QrOSFrmAbrControle: TIntegerField;
    QrOSFrmAbrConta: TIntegerField;
    QrOSFrmAbrIDIts: TIntegerField;
    QrOSFrmAbrAbrangicie: TIntegerField;
    DsOSFrmAbr: TDataSource;
    QrOSMonCab: TmySQLQuery;
    QrOSMonCabCodigo: TIntegerField;
    QrOSMonCabControle: TIntegerField;
    QrOSMonCabConta: TIntegerField;
    QrOSMonCabNome: TWideStringField;
    QrOSMonCabFormula: TIntegerField;
    QrOSMonCabEquipAplic: TIntegerField;
    QrOSMonCabQtdTot: TFloatField;
    QrOSMonCabQtdQSP: TFloatField;
    QrOSMonCabCusTot: TFloatField;
    QrOSMonCabNO_FORMULA: TWideStringField;
    QrOSMonCabNO_EquipAplic: TWideStringField;
    QrOSMonCabPipCad: TIntegerField;
    QrOSMonCabNO_PIP: TWideStringField;
    QrOSMonCabDiluente: TIntegerField;
    QrOSMonCabTipoAplica: TIntegerField;
    QrOSMonCabUnidMed: TIntegerField;
    QrOSMonCabCU_UNIDMED: TIntegerField;
    QrOSMonCabSIGLA: TWideStringField;
    QrOSMonCabNO_UNIDMED: TWideStringField;
    DsOSMonCab: TDataSource;
    _Qr_OS_Mon_Dep_: TmySQLQuery;
    _Qr_OS_Mon_Dep_Codigo: TIntegerField;
    _Qr_OS_Mon_Dep_Controle: TIntegerField;
    _Qr_OS_Mon_Dep_Conta: TIntegerField;
    _Qr_OS_Mon_Dep_Tabela: TSmallintField;
    _Qr_OS_Mon_Dep_Cadastro: TIntegerField;
    _Qr_OS_Mon_Dep_Lk: TIntegerField;
    _Qr_OS_Mon_Dep_DataCad: TDateField;
    _Qr_OS_Mon_Dep_DataAlt: TDateField;
    _Qr_OS_Mon_Dep_UserCad: TIntegerField;
    _Qr_OS_Mon_Dep_UserAlt: TIntegerField;
    _Qr_OS_Mon_Dep_AlterWeb: TSmallintField;
    _Qr_OS_Mon_Dep_Ativo: TSmallintField;
    _Qr_OS_Mon_Dep_SIGLA: TWideStringField;
    _Qr_OS_Mon_Dep_NO_Campo: TWideStringField;
    _Qr_OS_Mon_Dep_IDIts: TIntegerField;
    _Ds_OS_Mon_Dep_: TDataSource;
    QrOSMonRec: TmySQLQuery;
    QrOSMonRecNO_GG1: TWideStringField;
    QrOSMonRecCodigo: TIntegerField;
    QrOSMonRecControle: TIntegerField;
    QrOSMonRecConta: TIntegerField;
    QrOSMonRecIDIts: TIntegerField;
    QrOSMonRecGraGruX: TIntegerField;
    QrOSMonRecPrvQtd: TFloatField;
    QrOSMonRecPrvPrc: TFloatField;
    QrOSMonRecPrvVal: TFloatField;
    QrOSMonRecUsoQtd: TFloatField;
    QrOSMonRecUsoPrc: TFloatField;
    QrOSMonRecUsoVal: TFloatField;
    QrOSMonRecUsoDec: TFloatField;
    QrOSMonRecUsoTot: TFloatField;
    QrOSMonRecOrdem: TIntegerField;
    QrOSMonRecReordem: TIntegerField;
    DsOSMonRec: TDataSource;
    QrOSPos: TmySQLQuery;
    QrOSPosCodigo: TIntegerField;
    QrOSPosControle: TIntegerField;
    QrOSPosDias: TIntegerField;
    DsOSPos: TDataSource;
    QrOSPrz: TmySQLQuery;
    QrOSPrzCodigo: TIntegerField;
    QrOSPrzControle: TIntegerField;
    QrOSPrzCondicao: TIntegerField;
    QrOSPrzNO_CONDICAO: TWideStringField;
    QrOSPrzDescoPer: TFloatField;
    QrOSPrzEscolhido: TSmallintField;
    DsOSPrz: TDataSource;
    QrOSAge: TmySQLQuery;
    QrOSAgeNO_AGENTE: TWideStringField;
    QrOSAgeCodigo: TIntegerField;
    QrOSAgeControle: TIntegerField;
    QrOSAgeAgente: TIntegerField;
    QrOSAgeResponsa: TSmallintField;
    DsOSAge: TDataSource;
    QrOSCabAlv: TmySQLQuery;
    QrOSCabAlvCodigo: TIntegerField;
    QrOSCabAlvControle: TIntegerField;
    QrOSCabAlvOrdem: TSmallintField;
    QrOSCabAlvLk: TIntegerField;
    QrOSCabAlvDataCad: TDateField;
    QrOSCabAlvDataAlt: TDateField;
    QrOSCabAlvUserCad: TIntegerField;
    QrOSCabAlvUserAlt: TIntegerField;
    QrOSCabAlvAlterWeb: TSmallintField;
    QrOSCabAlvAtivo: TSmallintField;
    QrOSCabAlvPraga_A: TIntegerField;
    QrOSCabAlvPraga_Z: TIntegerField;
    QrOSCabAlvNO_NIVEL: TWideStringField;
    QrOSCabAlvNO_PRAGA: TWideStringField;
    DsOSCabAlv: TDataSource;
    QrEntiMail: TmySQLQuery;
    QrEntiMailNOMEETC: TWideStringField;
    QrEntiMailConta: TIntegerField;
    QrEntiMailEMail: TWideStringField;
    QrEntiMailEntiTipCto: TIntegerField;
    QrEntiMailOrdem: TIntegerField;
    DsEntiMail: TDataSource;
    QrEntiTel: TmySQLQuery;
    QrEntiTelNOMEETC: TWideStringField;
    QrEntiTelConta: TIntegerField;
    QrEntiTelTelefone: TWideStringField;
    QrEntiTelEntiTipCto: TIntegerField;
    QrEntiTelTEL_TXT: TWideStringField;
    QrEntiTelRamal: TWideStringField;
    DsEntiTel: TDataSource;
    QrOSPipMon: TmySQLQuery;
    QrOSPipMonNO_PIP: TWideStringField;
    QrOSPipMonCodigo: TIntegerField;
    QrOSPipMonControle: TIntegerField;
    QrOSPipMonPipCad: TIntegerField;
    DsOSPipMon: TDataSource;
    QrOSCxaAtrib: TmySQLQuery;
    QrOSCxaAtribID_Item: TIntegerField;
    QrOSCxaAtribID_Sorc: TIntegerField;
    QrOSCxaAtribAtrCad: TIntegerField;
    QrOSCxaAtribAtrIts: TIntegerField;
    QrOSCxaAtribCU_CAD: TIntegerField;
    QrOSCxaAtribCU_ITS: TIntegerField;
    QrOSCxaAtribNO_CAD: TWideStringField;
    QrOSCxaAtribNO_ITS: TWideStringField;
    DsOSCxaAtrib: TDataSource;
    QrSiapTerCad: TmySQLQuery;
    QrSiapTerCadCodigo: TIntegerField;
    QrSiapTerCadNome: TWideStringField;
    DsSiapterCad: TDataSource;
    QrEntiContat: TmySQLQuery;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    DsEntiContat: TDataSource;
    QrFatoGeradr: TmySQLQuery;
    QrFatoGeradrCodigo: TIntegerField;
    QrFatoGeradrNome: TWideStringField;
    QrFatoGeradrNeedOS_Ori: TSmallintField;
    DsFatoGeradr: TDataSource;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNO_ENT: TWideStringField;
    DsEntidades: TDataSource;
    QrEstatusOSs: TmySQLQuery;
    QrEstatusOSsCodigo: TIntegerField;
    QrEstatusOSsNome: TWideStringField;
    DsEstatusOSs: TDataSource;
    QrEntPagante: TmySQLQuery;
    QrEntPaganteCodigo: TIntegerField;
    QrEntPaganteNO_ENT: TWideStringField;
    QrEntContrat: TmySQLQuery;
    QrEntContratCodigo: TIntegerField;
    QrEntContratNO_ENT: TWideStringField;
    DsEntContrat: TDataSource;
    DsEntPagante: TDataSource;
    QrExeTxtCli1: TmySQLQuery;
    QrExeTxtCli1Codigo: TIntegerField;
    QrExeTxtCli1Nome: TWideStringField;
    QrExeTxtCli2: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsExeTxtCli2: TDataSource;
    DsExeTxtCli1: TDataSource;
    QrAgentes: TmySQLQuery;
    QrAgentesCodigo: TIntegerField;
    QrAgentesNome: TWideStringField;
    DsAgentes: TDataSource;
    QrPediPrzCab: TmySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    DsPediPrzCab: TDataSource;
    CSTabSheetChamou: TdmkCompoStore;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel6: TPanel;
    Label24: TLabel;
    Label33: TLabel;
    DBEdit17: TDBEdit;
    DBEdit23: TDBEdit;
    QrOSChk: TmySQLQuery;
    DsOSChk: TDataSource;
    TabSheet7: TTabSheet;
    DBGOSChk: TDBGrid;
    QrOSChkNO_ITEM: TWideStringField;
    QrOSChkCodigo: TIntegerField;
    QrOSChkControle: TIntegerField;
    QrOSChkChekLstIts: TIntegerField;
    PMOSChk: TPopupMenu;
    Incluiitens1: TMenuItem;
    Removeitens1: TMenuItem;
    RemovePIPs1: TMenuItem;
    PnLugarOpcao: TPanel;
    Panel15: TPanel;
    DBGLugar: TDBGrid;
    DBGOpcao: TDBGrid;
    QrLugares: TmySQLQuery;
    DsLugares: TDataSource;
    QrGrupos: TmySQLQuery;
    DsGrupos: TDataSource;
    QrGruposGrupo: TIntegerField;
    QrLugaresSiapTerCad: TIntegerField;
    QrLugaresNome: TWideStringField;
    SpLugarOpcao: TSplitter;
    BtAlterna: TBitBtn;
    N1: TMenuItem;
    Duplica1: TMenuItem;
    ReplicaLugarOpo1: TMenuItem;
    N2: TMenuItem;
    QrOSCabOptado: TSmallintField;
    QrOSCabGrupo: TIntegerField;
    QrOSCabNumero: TIntegerField;
    QrOSCabOpcao: TIntegerField;
    QrOpcoes: TmySQLQuery;
    DsOpcoes: TDataSource;
    QrOpcoesOpcao: TIntegerField;
    Comsegurana1: TMenuItem;
    Incondicional1: TMenuItem;
    PMOSImp: TPopupMenu;
    Cadastrodecliente1: TMenuItem;
    Fichadolugar1: TMenuItem;
    Fichasdevistoria1: TMenuItem;
    Oramento1: TMenuItem;
    Fichadeexecuo1: TMenuItem;
    ComprovantedeExecues1: TMenuItem;
    Preencherosdadosdoslugares1: TMenuItem;
    Dadosdoslugaresembranco1: TMenuItem;
    Adiodenovooramento1: TMenuItem;
    Adiodenovolugar1: TMenuItem;
    QrReplica: TmySQLQuery;
    QrReplicaGrupo: TIntegerField;
    QrReplicaOpcao: TIntegerField;
    QrReplicaSiapTerCad: TIntegerField;
    QrReplicaCodigo: TIntegerField;
    Label49: TLabel;
    DBEdit37: TDBEdit;
    TabSheet8: TTabSheet;
    QrDiarioAdd: TmySQLQuery;
    DsDiarioAdd: TDataSource;
    DBGDiarioAdd: TDBGrid;
    QrDiarioAddCodigo: TIntegerField;
    QrDiarioAddDepto: TIntegerField;
    QrDiarioAddData: TDateField;
    QrDiarioAddHora: TTimeField;
    QrDiarioAddInterloctr: TIntegerField;
    QrDiarioAddPrintOS: TIntegerField;
    QrDiarioAddNome: TWideStringField;
    DBMemo1: TDBMemo;
    QrDiarioAddIMP: TWideStringField;
    PMDiarioAdd: TPopupMenu;
    Sim1: TMenuItem;
    No1: TMenuItem;
    QrFaltaToImp: TmySQLQuery;
    QrFaltaToImpITENS: TLargeintField;
    N3: TMenuItem;
    Diriodestaempresacomestecliente1: TMenuItem;
    PMPesqNum: TPopupMenu;
    Proforma1: TMenuItem;
    Localizador1: TMenuItem;
    QrOSChkFeito: TSmallintField;
    QrOSChkNO_FEITO: TWideStringField;
    N5: TMenuItem;
    Novodilogo1: TMenuItem;
    TabSheet9: TTabSheet;
    QrOSSta: TmySQLQuery;
    DsOSSta: TDataSource;
    QrOSStaNO_STATUS: TWideStringField;
    QrOSStaCodigo: TIntegerField;
    QrOSStaControle: TIntegerField;
    QrOSStaStatus: TIntegerField;
    QrOSStaFeito: TSmallintField;
    DBGOSSta: TDBGrid;
    PMOSSta: TPopupMenu;
    Adicionastatus1: TMenuItem;
    QrOSPrzVAL_COM_DESCO: TFloatField;
    QrOSPrzParcelas: TIntegerField;
    QrOSPrzVAL_PARCELA: TFloatField;
    DBGOSPrz: TDBGrid;
    QrOSMonRecValCliDd: TIntegerField;
    QrOSMonRecEhDiluente: TSmallintField;
    Frmulaoatual1: TMenuItem;
    odasformulaesdesteservio1: TMenuItem;
    Servioatual1: TMenuItem;
    odosservios1: TMenuItem;
    Somenteseusservios1: TMenuItem;
    odaOS1: TMenuItem;
    odas1: TMenuItem;
    Atual1: TMenuItem;
    Atual2: TMenuItem;
    odos1: TMenuItem;
    Atual3: TMenuItem;
    odos2: TMenuItem;
    Alteraproformadolocalizador1: TMenuItem;
    DBGOSPos: TDBGrid;
    QrOSPosNO_AplicID: TWideStringField;
    QrOSPosNO_Aplicacao: TWideStringField;
    QrOSPosAplicacao: TIntegerField;
    QrOSPosAplicID: TIntegerField;
    QrOSPosEmitido: TIntegerField;
    QrOSPosExecutado: TIntegerField;
    QrOSPosExeCodi: TIntegerField;
    QrOSPosExeCtrl: TIntegerField;
    Incluiporprcadastro1: TMenuItem;
    Incluialertacomercial1: TMenuItem;
    QrOSFrmEvo: TmySQLQuery;
    DsOSFrmEvo: TDataSource;
    DsOSCabXtr: TDataSource;
    QrOSCabXtr: TmySQLQuery;
    QrOSFrmEvoCodigo: TIntegerField;
    QrOSFrmEvoControle: TIntegerField;
    QrOSFrmEvoConta: TIntegerField;
    QrOSFrmEvoIDIts: TIntegerField;
    QrOSFrmEvoDataHora: TDateTimeField;
    QrOSFrmEvoPercFeito: TFloatField;
    QrOSFrmEvoTexto: TWideStringField;
    PMOSFrmEvo: TPopupMenu;
    Incluievoluodeaplicao1: TMenuItem;
    Alteraevoluodeaplicao1: TMenuItem;
    Excluievoluodeaplicao1: TMenuItem;
    QrOSCabXtrCodigo: TIntegerField;
    QrOSCabXtrControle: TIntegerField;
    QrOSCabXtrDtHrIni: TDateTimeField;
    QrOSCabXtrDtHrFim: TDateTimeField;
    DBGOSCabXtr: TDBGrid;
    PMOSCabXtr: TPopupMenu;
    Incluiitemdeescalonamento1: TMenuItem;
    Alteraitemdeescalonamento1: TMenuItem;
    Excluiitemdeescalonamento1: TMenuItem;
    QrOSFrmCabPercFeito: TFloatField;
    QrOSSrvTudoFeitoA: TSmallintField;
    QrOSSrvTudoFeitoM: TSmallintField;
    QrOSSrvTUDOFEITO: TFloatField;
    QrOSFrmCabCALC_FEITO: TFloatField;
    TabSheet10: TTabSheet;
    DBGOSPrv: TDBGrid;
    QrOSPrv: TmySQLQuery;
    QrOSPrvCodigo: TIntegerField;
    QrOSPrvControle: TIntegerField;
    QrOSPrvDtHrContat: TDateTimeField;
    QrOSPrvFormContat: TIntegerField;
    QrOSPrvCliente: TIntegerField;
    QrOSPrvContato: TIntegerField;
    QrOSPrvAgente: TIntegerField;
    QrOSPrvNome: TWideStringField;
    QrOSPrvNO_Cliente: TWideStringField;
    QrOSPrvNO_Agente: TWideStringField;
    QrOSPrvNO_Contato: TWideStringField;
    QrOSPrvNO_FormContat: TWideStringField;
    DsOSPrv: TDataSource;
    PMOSPrv: TPopupMenu;
    Adicionasolicitaesdeprovidncias1: TMenuItem;
    Removeasolicitaesdeprovidnciaselecionada1: TMenuItem;
    Monitoramento1: TMenuItem;
    QrOSPipMonPrgLstCab: TIntegerField;
    QrOSPrzORC_COM_DESCO: TFloatField;
    QrOSPrzORC_PARCELA: TFloatField;
    QrOSPrzINV_COM_DESCO: TFloatField;
    QrOSPrzINV_PARCELA: TFloatField;
    QrOSSrvMoniDdTotl: TIntegerField;
    QrOSSrvMoniDdIntv: TIntegerField;
    PMOSFrmFlhCb: TPopupMenu;
    QrOSFrmFlhCb: TmySQLQuery;
    DsOSFrmFlhCb: TDataSource;
    QrOSFrmFlhDd: TmySQLQuery;
    DsOSFrmFlhDd: TDataSource;
    QrOSFrmFlhCbCodigo: TIntegerField;
    QrOSFrmFlhCbControle: TIntegerField;
    QrOSFrmFlhCbConta: TIntegerField;
    QrOSFrmFlhCbIDIts: TIntegerField;
    QrOSFrmFlhCbFormula: TIntegerField;
    QrOSFrmFlhCbPeriodd: TIntegerField;
    QrOSFrmFlhCbLk: TIntegerField;
    QrOSFrmFlhCbDataCad: TDateField;
    QrOSFrmFlhCbDataAlt: TDateField;
    QrOSFrmFlhCbUserCad: TIntegerField;
    QrOSFrmFlhCbUserAlt: TIntegerField;
    QrOSFrmFlhCbAlterWeb: TSmallintField;
    QrOSFrmFlhCbAtivo: TSmallintField;
    QrOSFrmFlhDdCodigo: TIntegerField;
    QrOSFrmFlhDdControle: TIntegerField;
    QrOSFrmFlhDdConta: TIntegerField;
    QrOSFrmFlhDdIDIts: TIntegerField;
    QrOSFrmFlhDdIDIt2: TIntegerField;
    QrOSFrmFlhDdOrdem: TIntegerField;
    QrOSFrmFlhDdDias: TIntegerField;
    QrOSFrmFlhDdLk: TIntegerField;
    QrOSFrmFlhDdDataCad: TDateField;
    QrOSFrmFlhDdDataAlt: TDateField;
    QrOSFrmFlhDdUserCad: TIntegerField;
    QrOSFrmFlhDdUserAlt: TIntegerField;
    QrOSFrmFlhDdAlterWeb: TSmallintField;
    QrOSFrmFlhDdAtivo: TSmallintField;
    QrOSFrmFlhCbNO_FORMULA: TWideStringField;
    PMOSFrmFlhDd: TPopupMenu;
    IncluiIntervalo1: TMenuItem;
    Alteraintervalo1: TMenuItem;
    ExcluiIntervalo1: TMenuItem;
    Incluifrmulafilha1: TMenuItem;
    Alterafrmulafilha1: TMenuItem;
    Excluifrmulafilha1: TMenuItem;
    QrOSFrmFlhCbDdPostero: TIntegerField;
    QrOSFrmFlhCbEmisStatus: TSmallintField;
    QrOSFrmFlhCbEmisUltDta: TDateField;
    QrAgeEqiCab: TmySQLQuery;
    IntegerField2: TIntegerField;
    DsAgeEqiCab: TDataSource;
    QrAgeEqiCabNome: TWideStringField;
    QrOSCabAgeEqiCab: TIntegerField;
    QrOSCabNO_AgeEqiCab: TWideStringField;
    QrOSMonCabPerioDd: TIntegerField;
    QrOSMonCabDdPostero: TIntegerField;
    QrOSMonCabNO_prglstcab: TWideStringField;
    QrOSMonCabNO_DEPENDENCI: TWideStringField;
    PMOSMonPipDd: TPopupMenu;
    IncluiIntervalo2: TMenuItem;
    AlteraIntervalo2: TMenuItem;
    ExcluiIntervalo2: TMenuItem;
    QrOSMonPipDd: TmySQLQuery;
    DsOSMonPipDd: TDataSource;
    QrOSMonPipDdCodigo: TIntegerField;
    QrOSMonPipDdControle: TIntegerField;
    QrOSMonPipDdConta: TIntegerField;
    QrOSMonPipDdIDIts: TIntegerField;
    QrOSMonPipDdOrdem: TIntegerField;
    QrOSMonPipDdDias: TIntegerField;
    QrOSMonPipDdLk: TIntegerField;
    QrOSMonPipDdDataCad: TDateField;
    QrOSMonPipDdDataAlt: TDateField;
    QrOSMonPipDdUserCad: TIntegerField;
    QrOSMonPipDdUserAlt: TIntegerField;
    QrOSMonPipDdAlterWeb: TSmallintField;
    QrOSMonPipDdAtivo: TSmallintField;
    QrOSPipMonNO_PrgLstCab: TWideStringField;
    QrOSPipMonOrdem: TIntegerField;
    QrOSPipMonReordem: TIntegerField;
    Cabealho1: TMenuItem;
    PIP1: TMenuItem;
    N6: TMenuItem;
    Gerarperguntas1: TMenuItem;
    QrOSCabSohInicial: TSmallintField;
    PB1: TProgressBar;
    QrOSCabStPipAdPrg: TSmallintField;
    QrOSPipIts: TmySQLQuery;
    DsOSPipIts: TDataSource;
    QrOSPipItsNO_PERGUNTA: TWideStringField;
    QrOSPipItsCodigo: TIntegerField;
    QrOSPipItsControle: TIntegerField;
    QrOSPipItsConta: TLargeintField;
    QrOSPipItsPrgLstIts: TIntegerField;
    QrOSPipItsOrdem: TIntegerField;
    QrOSPipItsSubOrdem: TIntegerField;
    QrOSPipItsTabela: TSmallintField;
    QrOSPipItsGraGruX: TIntegerField;
    QrOSPipItsFuncoes: TSmallintField;
    QrOSPipItsDependente: TSmallintField;
    QrOSPipItsPrgAtrCad: TIntegerField;
    QrOSPipItsFiliacao: TIntegerField;
    QrOSPipItsRelacao: TIntegerField;
    QrOSPipItsNivel: TIntegerField;
    QrOSPipItsNivSeq: TIntegerField;
    QrOSPipItsPergunta: TIntegerField;
    QrOSPipItsBinarCad0: TIntegerField;
    QrOSPipItsBinarCad1: TIntegerField;
    QrOSPipItsLk: TIntegerField;
    QrOSPipItsDataCad: TDateField;
    QrOSPipItsDataAlt: TDateField;
    QrOSPipItsUserCad: TIntegerField;
    QrOSPipItsUserAlt: TIntegerField;
    QrOSPipItsAlterWeb: TSmallintField;
    QrOSPipItsAtivo: TSmallintField;
    DBGOSPipIts: TdmkDBGridZTO;
    QrOSPipItsSigla: TWideStringField;
    Responderperguntas1: TMenuItem;
    Excluirperguntasgeradas1: TMenuItem;
    Atual4: TMenuItem;
    odasdoPIPSelecionado1: TMenuItem;
    odasdetodosPIPs1: TMenuItem;
    QrOSPipItsPrgLstCab: TIntegerField;
    QrOSPipItsSobreOrd: TIntegerField;
    QrOSPipItsTabIdx: TLargeintField;
    QrOSPipItsRESPOSTA: TWideStringField;
    QrOSPipItsAcaoPrd: TSmallintField;
    QrOSPipItsRespondido: TSmallintField;
    QrOSPipItsRespBin: TSmallintField;
    QrOSPipItsRespQtd: TFloatField;
    QrOSPipItsRespAtrCad: TIntegerField;
    QrOSPipItsRespAtrIts: TIntegerField;
    QrOSPipItsRespAtrTxt: TWideStringField;
    QrOSPipItsRespTxtLvr: TWideMemoField;
    QrOSPipItsRespPrdBin: TSmallintField;
    N7: TMenuItem;
    Ratificaconsumosprevistos1: TMenuItem;
    N8: TMenuItem;
    Ratificaconsumosprevistos2: TMenuItem;
    Aplicaes1: TMenuItem;
    Monitoramentos1: TMenuItem;
    ApliceMonit1: TMenuItem;
    odaOS2: TMenuItem;
    N9: TMenuItem;
    Ratificaodeconsumosprevistos1: TMenuItem;
    QrOSFrmRecNO_RatifUso: TWideStringField;
    QrOSFrmRecEhDiluente: TSmallintField;
    QrOSFrmRecUsoCusUni: TFloatField;
    QrOSFrmRecUsoCusTot: TFloatField;
    QrOSFrmRecRatifUso: TSmallintField;
    QrOSMonRecNO_RatifUso: TWideStringField;
    QrOSMonRecRatifUso: TSmallintField;
    QrNaoRatif: TmySQLQuery;
    QrOSCabHowGerou: TSmallintField;
    QrOSCabPosGerou: TSmallintField;
    QrOSCabOSFlhUltGe: TIntegerField;
    QrOSCabMulServico: TLargeintField;
    QrOSCabOSFlhGrCab: TIntegerField;
    QrOSCabOSFlhGrIts: TIntegerField;
    QrOSCabLstCusPrd: TIntegerField;
    QrOSMonRecUsoCusUni: TFloatField;
    QrOSMonRecUsoCusTot: TFloatField;
    QrOSMonRecDesativado: TSmallintField;
    Alteraodeconsumorealizado1: TMenuItem;
    Alteraodeconsumorealizado2: TMenuItem;
    QrSemCus: TmySQLQuery;
    QrSemCusGraGruX: TIntegerField;
    QrSemCusControle: TIntegerField;
    QrSemCusNome: TWideStringField;
    frxGER_OSERV_001_Err_01: TfrxReport;
    frxDsSemCus: TfrxDBDataset;
    Visualizarbloquetos1: TMenuItem;
    QrPreQtd: TmySQLQuery;
    QrPreQtdNO_PIP: TWideStringField;
    QrPreQtdNO_Pergunta: TWideStringField;
    QrPreQtdRespQtd: TFloatField;
    QrPreQtdConta: TLargeintField;
    QrPreQtdCodigo: TIntegerField;
    QrPreQtdPipCad: TIntegerField;
    QrPreQtdFiliacao: TIntegerField;
    QrPreQtdPrgLstIts: TIntegerField;
    TSLocalizador: TTabSheet;
    GBDados: TGroupBox;
    Panel7: TPanel;
    Label7: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label001: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    Label56: TLabel;
    DBEdit15: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdSiapTerCad: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    GroupBox6: TGroupBox;
    Label23: TLabel;
    DBEdit16: TDBEdit;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label48: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    GroupBox21: TGroupBox;
    Label26: TLabel;
    Label27: TLabel;
    Label37: TLabel;
    Label32: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit38: TDBEdit;
    Panel12: TPanel;
    Splitter4: TSplitter;
    GroupBox10: TGroupBox;
    DBGOSAge: TDBGrid;
    GroupBox25: TGroupBox;
    DBGOSCabAlv: TDBGrid;
    QrOSFrmRecSIGLAUNIDMED: TWideStringField;
    QrOSMonRecSIGLAUNIDMED: TWideStringField;
    QrOSFrmCabIndicUso: TSmallintField;
    N10: TMenuItem;
    UploadNuvem1: TMenuItem;
    QrOSCabLstUplWeb: TDateTimeField;
    Panel16: TPanel;
    Label16: TLabel;
    DBEdit39: TDBEdit;
    Label57: TLabel;
    N11: TMenuItem;
    N12: TMenuItem;
    ImprimeQRCode1: TMenuItem;
    QrOSCxaTudoFeito: TSmallintField;
    DBGrid1: TDBGrid;
    QrOSPrn: TmySQLQuery;
    DsOSPrn: TDataSource;
    QrOSPrnCodigo: TIntegerField;
    QrOSPrnControle: TIntegerField;
    QrOSPrnRelatorio: TIntegerField;
    QrOSPrnDtHrIni: TDateTimeField;
    QrOSPrnDtHrFim: TDateTimeField;
    QrOSPrnLk: TIntegerField;
    QrOSPrnDataCad: TDateField;
    QrOSPrnDataAlt: TDateField;
    QrOSPrnUserCad: TIntegerField;
    QrOSPrnUserAlt: TIntegerField;
    QrOSPrnAlterWeb: TSmallintField;
    QrOSPrnAtivo: TSmallintField;
    QrOSPrnNO_Relatorio: TWideStringField;
    Agentes1: TMenuItem;
    N13: TMenuItem;
    Adicionaagentes1: TMenuItem;
    Retiraagente1: TMenuItem;
    N14: TMenuItem;
    N15: TMenuItem;
    Adicionanovacondiodepagamento1: TMenuItem;
    Editaacondiodepagamentoatual1: TMenuItem;
    Removecondiaodepagamentoatual1: TMenuItem;
    QrOSCabConta: TIntegerField;
    N16: TMenuItem;
    Recalcula1: TMenuItem;
    QrOSSrvTITULO_FORMULAS: TWideStringField;
    Panel22: TPanel;
    Label58: TLabel;
    DBEdit40: TDBEdit;
    Label59: TLabel;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    Label60: TLabel;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    Label61: TLabel;
    DBEdit46: TDBEdit;
    Label62: TLabel;
    DBEdit47: TDBEdit;
    AdicionaPMVsemOSsFuturas1: TMenuItem;
    N17: TMenuItem;
    AdicionanovosPMVsemOSsfuturas1: TMenuItem;
    Selecionados1: TMenuItem;
    N18: TMenuItem;
    Alteradatadaltimaemissodefrmulasfilhas1: TMenuItem;
    QrOSStaDataCad: TDateField;
    QrOSStaDataAlt: TDateField;
    QrOSStaUserCad: TIntegerField;
    QrOSStaUserAlt: TIntegerField;
    QrMobiliCad: TmySQLQuery;
    DsMobiliCad: TDataSource;
    QrMobiliCadCodigo: TIntegerField;
    QrMobiliCadNome: TWideStringField;
    QrOSCabLCPUsed: TIntegerField;
    QrOSCabDtIniMonGr: TDateField;
    QrOSCabMobiliCad: TIntegerField;
    QrOSCabNO_MobiliCad: TWideStringField;
    DBEdit48: TDBEdit;
    DBEdit49: TDBEdit;
    Label64: TLabel;
    N19: TMenuItem;
    CadastrodoPMV1: TMenuItem;
    QrOSPrvNO_PrvStatus: TWideStringField;
    QrLoc: TmySQLQuery;
    PMOSPipIts: TPopupMenu;
    este1: TMenuItem;
    QrOSPipItsIDIts: TFloatField;
    QrOSPipItsSMI_IDCtrl: TFloatField;
    TabSheet14: TTabSheet;
    DBGrid2: TDBGrid;
    QrOSExtMon: TmySQLQuery;
    DsOSExtMon: TDataSource;
    QrOSExtMonCodigo: TIntegerField;
    QrOSExtMonControle: TIntegerField;
    QrOSExtMonDtaFinal: TDateField;
    QrOSExtMonPerioDd: TIntegerField;
    QrOSExtMonTotalDd: TIntegerField;
    QrOSExtMonLk: TIntegerField;
    QrOSExtMonDataCad: TDateField;
    QrOSExtMonDataAlt: TDateField;
    QrOSExtMonUserCad: TIntegerField;
    QrOSExtMonUserAlt: TIntegerField;
    QrOSExtMonAlterWeb: TSmallintField;
    QrOSExtMonAtivo: TSmallintField;
    PMOSExtMon: TPopupMenu;
    Incluimonitoramentoextra1: TMenuItem;
    Alteramonitoramentoextra1: TMenuItem;
    Excluimonitoramentoextra1: TMenuItem;
    BtCunsCad: TBitBtn;
    BtNFSePesq: TBitBtn;
    Panel23: TPanel;
    BtAlteraVct: TBitBtn;
    Panel8: TPanel;
    Splitter7: TSplitter;
    GroupBox7: TGroupBox;
    LaServicoAviso: TLabel;
    DBGOSSrv: TDBGrid;
    GroupBox9: TGroupBox;
    DBGOSAlv: TDBGrid;
    PCDedetizacao: TPageControl;
    TabSheet15: TTabSheet;
    Panel10: TPanel;
    DBText2: TDBText;
    GroupBox11: TGroupBox;
    GroupBox12: TGroupBox;
    Splitter2: TSplitter;
    Panel20: TPanel;
    DBGOSFrmAbr: TDBGrid;
    DBGOSFrmEvo: TDBGrid;
    Panel21: TPanel;
    DBGOSFrmCab: TDBGrid;
    GroupBox8: TGroupBox;
    DBGOSFrmDep: TDBGrid;
    GroupBox17: TGroupBox;
    DBGOSFrmFlhCb: TDBGrid;
    DBGOSFrmFlhDd: TDBGrid;
    TabSheet16: TTabSheet;
    Panel14: TPanel;
    Panel19: TPanel;
    DBText1: TDBText;
    GroupBox13: TGroupBox;
    Splitter8: TSplitter;
    DBGOSMonCab: TDBGrid;
    GroupBox14: TGroupBox;
    GroupBox20: TGroupBox;
    DBGOSMonPipDd: TDBGrid;
    Encerra1: TMenuItem;
    N20: TMenuItem;
    N21: TMenuItem;
    AlteradatadaltimaemissodeOSsfilhas1: TMenuItem;
    QrOSMonCabEmisUltDta: TDateField;
    N22: TMenuItem;
    QrOSCabAgeCorIni: TIntegerField;
    QrOSCabAgeCorFon: TIntegerField;
    QrOSCabCodEnti: TIntegerField;
    QrOSMonRecFracio: TSmallintField;
    QrOSFrmRecFracio: TSmallintField;
    QrOSCabStatusExecucao: TIntegerField;
    QrEstatusOSsAgeCorIni: TIntegerField;
    QrEstatusOSsAgeCorFon: TIntegerField;
    TabSheet11: TTabSheet;
    BtCroquGer: TBitBtn;
    QrVendedores: TmySQLQuery;
    IntegerField3: TIntegerField;
    StringField2: TWideStringField;
    DsVendedores: TDataSource;
    Label66: TLabel;
    DBEdit50: TDBEdit;
    DBEdit51: TDBEdit;
    QrOSCabVendedor: TIntegerField;
    QrOSCabNO_Vendedor: TWideStringField;
    Cadastrodeperguntas1: TMenuItem;
    N23: TMenuItem;
    Listadeperguntas1: TMenuItem;
    N24: TMenuItem;
    Cadastrodeagentes1: TMenuItem;
    RGDBMapPMV: TDBRadioGroup;
    QrOSCabMapPMV: TSmallintField;
    FAES1: TMenuItem;
    TabSheet12: TTabSheet;
    Panel24: TPanel;
    Splitter1: TSplitter;
    GroupBox27: TGroupBox;
    Panel25: TPanel;
    MeDBLauParTec: TDBMemo;
    GroupBox28: TGroupBox;
    Splitter13: TSplitter;
    Panel26: TPanel;
    dmkLabelRotate8: TdmkLabelRotate;
    MeDBLauInfesInt: TDBMemo;
    Panel29: TPanel;
    dmkLabelRotate9: TdmkLabelRotate;
    MeDBLauInfesExt: TDBMemo;
    Panel47: TPanel;
    BtLaudoIns: TBitBtn;
    BtLaudoDel: TBitBtn;
    QrOSCabLauInfesInt: TWideMemoField;
    QrOSCabLauInfesExt: TWideMemoField;
    QrOSCabLauParTec: TWideMemoField;
    PMLaudoDel: TPopupMenu;
    Graudeinfestaodaspragasevetores1: TMenuItem;
    Parecerdoreponsveltcnico1: TMenuItem;
    reainterna1: TMenuItem;
    reaexterna1: TMenuItem;
    ScrollBox2: TScrollBox;
    GBEdita: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label34: TLabel;
    Label36: TLabel;
    Label38: TLabel;
    Label25: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label47: TLabel;
    Label50: TLabel;
    Label54: TLabel;
    Label35: TLabel;
    Label55: TLabel;
    SpeedButton5: TSpeedButton;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    Label63: TLabel;
    SpeedButton9: TSpeedButton;
    SpeedButton10: TSpeedButton;
    SpeedButton11: TSpeedButton;
    SpeedButton12: TSpeedButton;
    Label65: TLabel;
    SBVendedor: TSpeedButton;
    EdCodigo: TdmkEdit;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    EdSiapTerCad: TdmkEditCB;
    CBSiapTerCad: TdmkDBLookupComboBox;
    EdEstatus: TdmkEditCB;
    CBEstatus: TdmkDBLookupComboBox;
    EdFatoGeradr: TdmkEditCB;
    CBFatoGeradr: TdmkDBLookupComboBox;
    GroupBox3: TGroupBox;
    Label17: TLabel;
    Label18: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    TPDtaVisPrv: TdmkEditDateTimePicker;
    TPDtaVisExe: TdmkEditDateTimePicker;
    EdDtaVisPrv: TdmkEdit;
    EdDtaVisExe: TdmkEdit;
    TPFimVisPrv: TdmkEditDateTimePicker;
    EdFimVisPrv: TdmkEdit;
    TPFimVisExe: TdmkEditDateTimePicker;
    EdFimVisExe: TdmkEdit;
    GroupBox4: TGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label51: TLabel;
    TPDtaExePrv: TdmkEditDateTimePicker;
    TPDtaExeIni: TdmkEditDateTimePicker;
    TPDtaExeFim: TdmkEditDateTimePicker;
    EdDtaExePrv: TdmkEdit;
    EdDtaExeIni: TdmkEdit;
    EdDtaExeFim: TdmkEdit;
    TPFimExePrv: TdmkEditDateTimePicker;
    EdFimExePrv: TdmkEdit;
    GroupBox5: TGroupBox;
    Label22: TLabel;
    TPDtaContat: TdmkEditDateTimePicker;
    EdDtaContat: TdmkEdit;
    EdEntiContat: TdmkEditCB;
    CBEntiContat: TdmkDBLookupComboBox;
    EdEntContrat: TdmkEditCB;
    CBEntContrat: TdmkDBLookupComboBox;
    EdEntPagante: TdmkEditCB;
    CBEntPagante: TdmkDBLookupComboBox;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    EdValiDdOrca: TdmkEdit;
    EdExeTxtCli1: TdmkEditCB;
    CBExeTxtCli1: TdmkDBLookupComboBox;
    CGOperacao: TdmkCheckGroup;
    EdExeTxtCli2: TdmkEditCB;
    CBExeTxtCli2: TdmkDBLookupComboBox;
    EdOpcao: TdmkEdit;
    EdNumContrat: TdmkEdit;
    EdAgeEqiCab: TdmkEditCB;
    CBAgeEqiCab: TdmkDBLookupComboBox;
    EdMobiliCad: TdmkEditCB;
    CBMobiliCad: TdmkDBLookupComboBox;
    CkPosGerou: TdmkCheckBox;
    EdEstatusCor: TdmkEdit;
    EdVendedor: TdmkEditCB;
    CBVendedor: TdmkDBLookupComboBox;
    RGMapPMV: TdmkRadioGroup;
    GBObservacoes: TGroupBox;
    PnObsExecuta: TPanel;
    dmkLabelRotate2: TdmkLabelRotate;
    MeObsExecuta: TdmkMemo;
    PnObsGaranti: TPanel;
    dmkLabelRotate3: TdmkLabelRotate;
    MeObsGaranti: TdmkMemo;
    TabSheet19: TTabSheet;
    GridPanel1: TGridPanel;
    Panel17: TPanel;
    dmkLabelRotate4: TdmkLabelRotate;
    DBMemo3: TDBMemo;
    Panel18: TPanel;
    dmkLabelRotate5: TdmkLabelRotate;
    DBMemo2: TDBMemo;
    PCFrmIts: TPageControl;
    TabSheet13: TTabSheet;
    DBGOSFrmRec: TDBGrid;
    TabSheet17: TTabSheet;
    PCMonIts: TPageControl;
    TabSheet18: TTabSheet;
    DBGOSMonRec: TDBGrid;
    TabSheet20: TTabSheet;
    Panel30: TPanel;
    BtOSFrmEPI: TBitBtn;
    DBGOSFrmEPI: TDBGrid;
    Panel31: TPanel;
    BtOSMonEPI: TBitBtn;
    DBGOSMonEPI: TDBGrid;
    PMOSFrmEPI: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    MenuItem1: TMenuItem;
    Cadatrodeprodutos1: TMenuItem;
    PMOSMonEPI: TPopupMenu;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    QrOSFrmEPI: TmySQLQuery;
    IntegerField4: TIntegerField;
    StringField3: TWideStringField;
    IntegerField5: TIntegerField;
    QrOSFrmEPIGraGruX: TIntegerField;
    DsOSFrmEPI: TDataSource;
    QrOSMonEPI: TmySQLQuery;
    IntegerField6: TIntegerField;
    StringField4: TWideStringField;
    IntegerField7: TIntegerField;
    IntegerField8: TIntegerField;
    DsOSMonEPI: TDataSource;
    QrOSFrmEPICodigo: TIntegerField;
    QrOSFrmEPIConta: TIntegerField;
    QrOSFrmEPIIDIts: TIntegerField;
    QrOSMonEPICodigo: TIntegerField;
    QrOSMonEPIConta: TIntegerField;
    QrOSMonEPIIDIts: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure BtOSCabClick(Sender: TObject);
    procedure BtOSSrvClick(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMOSCabPopup(Sender: TObject);
    procedure PMOSSrvPopup(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure EdFatoGeradrChange(Sender: TObject);
    procedure PMOSFrmCabPopup(Sender: TObject);
    procedure ItsInclui4Click(Sender: TObject);
    procedure ItsAltera4Click(Sender: TObject);
    procedure BtOSFrmCabClick(Sender: TObject);
    procedure QrOSCabAfterScroll(DataSet: TDataSet);
    procedure QrOSCabBeforeOpen(DataSet: TDataSet);
    procedure QrOSCabCalcFields(DataSet: TDataSet);
    procedure PMOSCabAlvPopup(Sender: TObject);
    procedure ExcluiOSCabAlv1Click(Sender: TObject);
    procedure QrOSCabBeforeClose(DataSet: TDataSet);
    procedure BtOSAlvClick(Sender: TObject);
    procedure Itens4Click(Sender: TObject);
    procedure BtOSMonCabClick(Sender: TObject);
    procedure PMOSMonCabPopup(Sender: TObject);
    procedure Itens5Click(Sender: TObject);
    procedure ItsExclui4Click(Sender: TObject);
    procedure DBGOSSrvDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGOSSrvMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSFrmDepMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSAlvMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSFrmCabMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSFrmRecMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSMonCabMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSMonRecMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IncluiOSCabAlv1Click(Sender: TObject);
    procedure DBGOSPrzMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSPrzDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    function frxGER_OSERV_001_001UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure SbImprimeClick(Sender: TObject);
    procedure Fatura1Click(Sender: TObject);
    procedure BtFatClick(Sender: TObject);
    procedure Excluifaturamento1Click(Sender: TObject);
    procedure Gerabloqueto1Click(Sender: TObject);
    procedure PMFatPopup(Sender: TObject);
    procedure EdNumContratKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGOSFrmAbrMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Abrangncia1Click(Sender: TObject);
    procedure DBGOSPosMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IncluiC3Click(Sender: TObject);
    procedure AlteraC3Click(Sender: TObject);
    procedure ExcluiC3Click(Sender: TObject);
    {
    procedure _DBG_OS_Mon_Dep_MouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    }
    procedure ItsInclui2Click(Sender: TObject);
    procedure ItsAltera2Click(Sender: TObject);
    procedure PMOSCxaPopup(Sender: TObject);
    procedure DBGOSCxaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ItsExclui2Click(Sender: TObject);
    procedure OSPrz1_IncluiClick(Sender: TObject);
    procedure OSPrz1_AlteraClick(Sender: TObject);
    procedure OSPrz1_ExcluiClick(Sender: TObject);
    procedure PMOSPrzPopup(Sender: TObject);
    procedure QrOSCabAfterClose(DataSet: TDataSet);
    procedure CGOperacaoClick(Sender: TObject);
    procedure DBGOSPipMonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AdicionaPIPsativos1Click(Sender: TObject);
    procedure ItsInclui6Click(Sender: TObject);
    procedure ItsAltera6Click(Sender: TObject);
    procedure ItsExclui6Click(Sender: TObject);
    procedure DBGOSCxIMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PMOSCxIPopup(Sender: TObject);
    procedure DBGOSCabAlvMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSAgeMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PMOSAgePopup(Sender: TObject);
    procedure IncluiOSAge1Click(Sender: TObject);
    procedure RemoveOSAge1Click(Sender: TObject);
    procedure DBGOSAgeDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGOSAgeColEnter(Sender: TObject);
    procedure DBGOSAgeColExit(Sender: TObject);
    procedure DBGOSSrvColEnter(Sender: TObject);
    procedure DBGOSSrvColExit(Sender: TObject);
    procedure EdEntidadeEnter(Sender: TObject);
    procedure DBGOSCxaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TPDtaVisPrvExit(Sender: TObject);
    procedure EdDtaVisPrvExit(Sender: TObject);
    procedure TPDtaVisExeExit(Sender: TObject);
    procedure EdDtaVisExeExit(Sender: TObject);
    procedure TPDtaExePrvExit(Sender: TObject);
    procedure EdDtaExePrvExit(Sender: TObject);
    procedure TPDtaExeIniExit(Sender: TObject);
    procedure EdDtaExeIniExit(Sender: TObject);
    procedure QrLctFatRefCalcFields(DataSet: TDataSet);
    procedure QrOSSrvAfterScroll(DataSet: TDataSet);
    procedure QrOSSrvBeforeClose(DataSet: TDataSet);
    procedure QrOSSrvCalcFields(DataSet: TDataSet);
    procedure QrOSCxaAfterScroll(DataSet: TDataSet);
    procedure QrOSCxaBeforeClose(DataSet: TDataSet);
    procedure QrOSFrmCabAfterScroll(DataSet: TDataSet);
    procedure QrOSFrmCabBeforeClose(DataSet: TDataSet);
    procedure QrOSMonCabAfterScroll(DataSet: TDataSet);
    procedure QrOSMonCabBeforeClose(DataSet: TDataSet);
    procedure QrEntiTelCalcFields(DataSet: TDataSet);
    procedure QrEntidadesBeforeClose(DataSet: TDataSet);
    procedure QrOSCabBeforeScroll(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure Incluiitens1Click(Sender: TObject);
    procedure DBGOSChkMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Removeitens1Click(Sender: TObject);
    procedure RemovePIPs1Click(Sender: TObject);
    procedure QrGruposBeforeClose(DataSet: TDataSet);
    procedure SpLugarOpcaoMoved(Sender: TObject);
    procedure QrLugaresAfterScroll(DataSet: TDataSet);
    procedure QrOpcoesAfterScroll(DataSet: TDataSet);
    procedure DBGLugarMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ReplicaLugarOpo1Click(Sender: TObject);
    procedure BtAlternaClick(Sender: TObject);
    procedure QrLugaresBeforeClose(DataSet: TDataSet);
    procedure QrOpcoesBeforeClose(DataSet: TDataSet);
    procedure Comsegurana1Click(Sender: TObject);
    procedure Cadastrodecliente1Click(Sender: TObject);
    procedure Fichasdevistoria1Click(Sender: TObject);
    procedure Oramento1Click(Sender: TObject);
    procedure Fichadeexecuo1Click(Sender: TObject);
    procedure ComprovantedeExecues1Click(Sender: TObject);
    procedure Preencherosdadosdoslugares1Click(Sender: TObject);
    procedure Dadosdoslugaresembranco1Click(Sender: TObject);
    procedure Duplica1Click(Sender: TObject);
    procedure Adiodenovooramento1Click(Sender: TObject);
    procedure Adiodenovolugar1Click(Sender: TObject);
    procedure QrGruposAfterOpen(DataSet: TDataSet);
    procedure QrGruposAfterScroll(DataSet: TDataSet);
    procedure DBGDiarioAddMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Sim1Click(Sender: TObject);
    procedure No1Click(Sender: TObject);
    procedure DBGDiarioAddDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure Diriodestaempresacomestecliente1Click(Sender: TObject);
    procedure DBEdSiapTerCadDblClick(Sender: TObject);
    procedure DBEdit4DblClick(Sender: TObject);
    procedure Proforma1Click(Sender: TObject);
    procedure Localizador1Click(Sender: TObject);
    procedure DBGOSChkDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGOSChkCellClick(Column: TColumn);
    procedure Novodilogo1Click(Sender: TObject);
    procedure DBGOSStaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Adicionastatus1Click(Sender: TObject);
    procedure QrOSPrzCalcFields(DataSet: TDataSet);
    procedure Frmulaoatual1Click(Sender: TObject);
    procedure odasformulaesdesteservio1Click(Sender: TObject);
    procedure Servioatual1Click(Sender: TObject);
    procedure odosservios1Click(Sender: TObject);
    procedure Somenteseusservios1Click(Sender: TObject);
    procedure odaOS1Click(Sender: TObject);
    procedure Atual1Click(Sender: TObject);
    procedure odas1Click(Sender: TObject);
    procedure Atual2Click(Sender: TObject);
    procedure odos1Click(Sender: TObject);
    procedure Atual3Click(Sender: TObject);
    procedure odos2Click(Sender: TObject);
    procedure Alteraproformadolocalizador1Click(Sender: TObject);
    procedure Incluiporprcadastro1Click(Sender: TObject);
    procedure DBGOSFrmEvoMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Incluievoluodeaplicao1Click(Sender: TObject);
    procedure Alteraevoluodeaplicao1Click(Sender: TObject);
    procedure Excluievoluodeaplicao1Click(Sender: TObject);
    procedure DBGOSFrmEvoDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGOSCabXtrMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Incluiitemdeescalonamento1Click(Sender: TObject);
    procedure Alteraitemdeescalonamento1Click(Sender: TObject);
    procedure Excluiitemdeescalonamento1Click(Sender: TObject);
    procedure DBGOSFrmCabDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QrOSFrmCabCalcFields(DataSet: TDataSet);
    procedure Adicionasolicitaesdeprovidncias1Click(Sender: TObject);
    procedure Removeasolicitaesdeprovidnciaselecionada1Click(Sender: TObject);
    procedure DBGOSPrvMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Monitoramento1Click(Sender: TObject);
{
    procedure DBGOSFrmDd_sMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
}
    procedure IncluiIntervalo1Click(Sender: TObject);
    procedure DBGOSFrmFlhCbMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure QrOSFrmFlhCbBeforeClose(DataSet: TDataSet);
    procedure QrOSFrmFlhCbAfterScroll(DataSet: TDataSet);
    procedure Incluifrmulafilha1Click(Sender: TObject);
    procedure Alterafrmulafilha1Click(Sender: TObject);
    procedure Excluifrmulafilha1Click(Sender: TObject);
    procedure Alteraintervalo1Click(Sender: TObject);
    procedure ExcluiIntervalo1Click(Sender: TObject);
    procedure DBGOSFrmFlhDdMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PMOSFrmFlhCbPopup(Sender: TObject);
    procedure PMOSFrmFlhDdPopup(Sender: TObject);
    procedure EdOSOrigemKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure IncluiIntervalo2Click(Sender: TObject);
    procedure AlteraIntervalo2Click(Sender: TObject);
    procedure ExcluiIntervalo2Click(Sender: TObject);
    procedure PMOSMonPipDdPopup(Sender: TObject);
    procedure Cabealho1Click(Sender: TObject);
    procedure PIP1Click(Sender: TObject);
    procedure Gerarperguntas1Click(Sender: TObject);
    procedure PMOSPipMonPopup(Sender: TObject);
    procedure QrOSPipMonBeforeClose(DataSet: TDataSet);
    procedure QrOSPipMonAfterScroll(DataSet: TDataSet);
    procedure Responderperguntas1Click(Sender: TObject);
    procedure Atual4Click(Sender: TObject);
    procedure odasdoPIPSelecionado1Click(Sender: TObject);
    procedure odasdetodosPIPs1Click(Sender: TObject);
    procedure Ratificaconsumosprevistos1Click(Sender: TObject);
    procedure Aplicaes1Click(Sender: TObject);
    procedure Monitoramentos1Click(Sender: TObject);
    procedure ApliceMonit1Click(Sender: TObject);
    procedure odaOS2Click(Sender: TObject);
    procedure Ratificaodeconsumosprevistos1Click(Sender: TObject);
    procedure Alteraodeconsumorealizado1Click(Sender: TObject);
    procedure Alteraodeconsumorealizado2Click(Sender: TObject);
    procedure Visualizarbloquetos1Click(Sender: TObject);
    procedure DBGOSAgeCellClick(Column: TColumn);
    procedure EdEntContratKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBEntContratKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdEntPaganteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure UploadNuvem1Click(Sender: TObject);
    procedure QrOSCabLstUplWebGetText(Sender: TField; var Text: string;
      DisplayText: Boolean);
    procedure EdEntContratChange(Sender: TObject);
    procedure EdEntPaganteChange(Sender: TObject);
    procedure ImprimeQRCode1Click(Sender: TObject);
    procedure Retiraagente1Click(Sender: TObject);
    procedure Adicionaagentes1Click(Sender: TObject);
    procedure Adicionanovacondiodepagamento1Click(Sender: TObject);
    procedure Editaacondiodepagamentoatual1Click(Sender: TObject);
    procedure Removecondiaodepagamentoatual1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
    procedure DBGOSCxaColEnter(Sender: TObject);
    procedure DBGOSCxaCellClick(Column: TColumn);
    procedure DBGOSSrvCellClick(Column: TColumn);
    procedure DBGOSPrzCellClick(Column: TColumn);
    procedure DBGOSPrzColEnter(Sender: TObject);
    procedure DBEdit21DblClick(Sender: TObject);
    procedure DBEdit25DblClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure Recalcula1Click(Sender: TObject);
    procedure DBGOSFrmCabColEnter(Sender: TObject);
    procedure DBGOSFrmCabColExit(Sender: TObject);
    procedure ItsInclui5Click(Sender: TObject);
    procedure AdicionaPMVsemOSsFuturas1Click(Sender: TObject);
    procedure AdicionanovosPMVsemOSsfuturas1Click(Sender: TObject);
    procedure Selecionados1Click(Sender: TObject);
    procedure Alteradatadaltimaemissodefrmulasfilhas1Click(Sender: TObject);
    procedure CadastrodoPMV1Click(Sender: TObject);
    procedure este1Click(Sender: TObject);
    procedure Incluimonitoramentoextra1Click(Sender: TObject);
    procedure Alteramonitoramentoextra1Click(Sender: TObject);
    procedure Excluimonitoramentoextra1Click(Sender: TObject);
    procedure PMOSExtMonPopup(Sender: TObject);
    procedure DBGOSFrmCabCellClick(Column: TColumn);
    procedure BtCunsCadClick(Sender: TObject);
    procedure BtNFSePesqClick(Sender: TObject);
    procedure SpeedButton9Click(Sender: TObject);
    procedure SpeedButton10Click(Sender: TObject);
    procedure SpeedButton11Click(Sender: TObject);
    procedure SpeedButton12Click(Sender: TObject);
    procedure CBEntPaganteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtAlteraVctClick(Sender: TObject);
    procedure Encerra1Click(Sender: TObject);
    procedure PMOSFrmEvoPopup(Sender: TObject);
    procedure AlteradatadaltimaemissodeOSsfilhas1Click(Sender: TObject);
    procedure EdSiapTerCadEnter(Sender: TObject);
    procedure CBSiapTerCadEnter(Sender: TObject);
    procedure EdEstatusChange(Sender: TObject);
    procedure BtCroquGerClick(Sender: TObject);
    procedure SBVendedorClick(Sender: TObject);
    procedure Cadastrodeperguntas1Click(Sender: TObject);
    procedure Listadeperguntas1Click(Sender: TObject);
    procedure Cadastrodeagentes1Click(Sender: TObject);
    procedure FAES1Click(Sender: TObject);
    procedure BtLaudoInsClick(Sender: TObject);
    procedure reainterna1Click(Sender: TObject);
    procedure reaexterna1Click(Sender: TObject);
    procedure Parecerdoreponsveltcnico1Click(Sender: TObject);
    procedure BtLaudoDelClick(Sender: TObject);
    procedure PMLaudoDelPopup(Sender: TObject);
    procedure BtOSMonEPIClick(Sender: TObject);
    procedure BtOSFrmEPIClick(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure Cadatrodeprodutos1Click(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure PMOSFrmEPIPopup(Sender: TObject);
    procedure PMOSMonEPIPopup(Sender: TObject);
    //procedure ExcluiIntervalo1Click(Sender: TObject);
  private
    FOPIPI_PreQtd: String;
    FNaoReabreVariantes: Boolean;
    FPos_PCGeral, FPos_PCOpera: Integer;
    FDisposicao: TDisposicao;
    //
    procedure AdicionaAgente();
    procedure AdicionaServico(IniLoop: Boolean);
    procedure RemoveAgente();
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure DefParams;
    procedure Va(Para: TVaiPara);

    procedure FechaOS();

    procedure GuardaPosicoes();
    procedure RedefinePosicoes();

    procedure LocalizaOS(Lugar, Opcao, Codigo: Integer);

    procedure MostraOSCxa(SQLType: TSQLType);
    procedure MostraOSCxI(SQLType: TSQLType);
    procedure MostraOSSrv(SQLType: TSQLType);
    procedure MostraOSAlv(SQLType: TSQLType);
    procedure MostraOSFrmCab(SQLType: TSQLType);
    procedure MostraOSFrmRec();
    procedure MostraOSFrmAbr(SQLType: TSQLType);
    procedure MostraOSFrmDep(SQLType: TSQLType);
    procedure MostraOSFrmEvo(SQLType: TSQLType);
    procedure MostraOSExtMon(SQLType: TSQLType);
    procedure MostraOSFrmFlhCb(SQLType: TSQLType);
    procedure MostraOSFrmFlhDd(SQLType: TSQLType);
    procedure MostraOSMonCabUni(SQLType: TSQLType);
    procedure MostraOSMonCabMul();
    procedure MostraOSMonRec();
    procedure MostraOSMonPipDd(SQLType: TSQLType);
    //procedure _Mostra_OS_Mon_Dep_(SQLType: TSQLType);
    procedure MostraOSPos(SQLType: TSQLType);
    procedure MostraOSPsq();
    procedure MostraPreSrv();
    //
    procedure MostraFormOSCabFat(SQLType: TSQLType);
    procedure MostraFormEntidade(Sender: TObject);
    //
    procedure ReopenVariantes(Disposicao: TDisposicao; Lugar, Opcao: Integer;
              ReabreOSCab: Boolean; Codigo: Integer);
    procedure TravaOForm();
    //
    function  CondicaoPGSelecionado(): Integer;
    //
    procedure VeSeReabreOSCab(Disposicao: TDisposicao; Forca: Boolean;
              Codigo: Integer);
    //
    function  DefineAgenteControlador(var Agente: Integer): Boolean;
    function  DuplicaLocalizadorAtual(const Grupo, Numero: Integer; var
              SiapTerCad, Opcao, Codigo: Integer;  OQueSel: TOSBugsDuplic;
              HowGerou: Integer; HabilitaForm: Boolean):
              Boolean;
    //
    procedure ReopenOpcoes(Grupo, Lugar, Opcao: Integer);
    procedure ReopenLugares(Grupo, Opcao, Lugar: Integer);
    procedure ReopenDiarioAdd(Codigo: Integer);
    //
    procedure SetaImprimivelItemConversa(Imprime: Boolean);
    procedure MostraOSImp(Aba: Integer; Cfg1: Variant);

    procedure MostraFormCunsCad();

    procedure ExcluiOSFrmCab(Quais: TSelType);
    procedure ExcluiOSMonCab(Quais: TSelType);
    procedure ExclusaoIncondicional(Nivel: TNivelExclusao);

    procedure LocalizaLocalizador(Codigo: Integer);
    procedure OSsFuturas();

    function  PodeResponderPerguntas(): Boolean;

    function  ProximaOrdemFrmFlhDd(): Integer;
    function  ProximaOrdemMonPipDd(): Integer;

    procedure ReopenContato();

    procedure RatificaConsumo(TabMae, Tabela, Campo: String; Indice, FatID: Integer);

    procedure ReopenOSPrn(Controle: Integer);

    procedure ConfiguraCorStatus(AgeCorIni, AgeCorFon: Integer; Edit1, Edit2: TEdit);
    procedure MostraOSAltRec(FatID: Integer);
    procedure MostraFormOSCabLaudo();
    procedure RemoveOSLaudo(Campo: String);
    procedure VerificaRespons();
    procedure AtualizaRespons();
  public
    { Public declarations }
    FAbaQueChamou,
    FSeq, FCabIni, FOSSrv: Integer;
    FLocIni: Boolean;
    //
    function  ImpedePelaDataFinalDeExecucao(DtaExeFim: TDateTime): Boolean;
    //
    procedure ExcluiPerguntasERespostas(Campo: String; Indice: Int64);
    procedure LocCod(Atual, Grupo: Integer; Disposicao: TDisposicao;
              Lugar, Opcao: Integer; ReabreOSCab: Boolean; OSCab: Integer);
  end;

var
  FmOSCab2: TFmOSCab2;

implementation

uses UnMyObjects, Module, ModOS, MyDBCheck, DmkDAC_PF, ModuleGeral, CfgCadLista,
  OSSrv, OSAlv, OSFrmCab, OSFrmRec, OSMonRec, OSPsq, MyVCLSkin, OsPrz, OSImp2,
  OSCabFat, ModuleFatura, ModuleFin, ModuleBloGeren, (*ContratPsq,*) OSCxI,
  OSFrmAbr, OSPos, OSFrmDep, (*_OS_Mon_Dep_,*) OSCxa, Bugstrol_Dmk, UnAppListas,
  ModAgenda, Principal, MyGlyfs, OSChk, Entidade2, DiarioAdd2, UnDiario_Tabs,
  OSMonCabIns, OSMonCabUpd, GetValor, OSFrmEvo, OSPrvSel, OSFrmFlhCb,
  OSFrmFlhDd(*, OSOriPsq*), OSMonPipDd, OSPipIts, OSAltRec, CreateBugs,
  UnBloquetos, OPIPI_PreQtd, OSSrvPre, OSExtMon, UnOSAll_PF, NFSe_PF_0000,
  OSCabEncerr, OSCabLaudo, ModProd, UnGrade_Jan;

{$R *.DFM}

const
  FFormatFloat = '00000';

resourcestring
  sTudoFeito = 'TudoFeito';
  sCalcFeito = 'CALC_FEITO';

procedure TFmOSCab2.Listadeperguntas1Click(Sender: TObject);
begin
  UnCfgCadLista.MostraCadRegistroSimples(Dmod.MyDB, DmModOS.TbPrgCadPrg,
    DmModOS.DsPrgCadPrg, ncGerlSeq1, 'Cadastro de Perguntas');
end;

procedure TFmOSCab2.MostraFormOSCabLaudo();
var
  Codigo, Grupo, Lugar, Opcao: Integer;
begin
  if (QrOSCab.State <> dsInactive) and (QrOSCab.RecordCount > 0) then
  begin
    Codigo := QrOSCabCodigo.Value;
    Grupo  := QrOSCabGrupo.Value;
    Lugar  := QrOSCabSiapTerCad.Value;
    Opcao  := QrOSCabOpcao.Value;
    //
    if DBCheck.CriaFm(TFmOSCabLaudo, FmOSCabLaudo, afmoNegarComAviso) then
    begin
      FmOSCabLaudo.FCodigo            := Codigo;
      FmOSCabLaudo.MeLauInfesInt.Text := QrOSCabLauInfesInt.Value;
      FmOSCabLaudo.MeLauInfesExt.Text := QrOSCabLauInfesExt.Value;
      FmOSCabLaudo.MeLauParTec.Text   := QrOSCabLauParTec.Value;
      //
      FmOSCabLaudo.ShowModal;
      FmOSCabLaudo.Destroy;
      //
      LocCod(Grupo, Grupo, dispIndefinido, Lugar, Opcao, True, Codigo);
      PCGeral.ActivePageIndex := 1;
      PCOpera.ActivePageIndex := 5;
    end;
  end;
end;

procedure TFmOSCab2.RemoveOSLaudo(Campo: String);
var
  Codigo, Grupo, Lugar, Opcao: Integer;
begin
  if MyObjects.FIC(Campo = '', nil,
    'Campo n�o informado na fun��o: "TFmOSCab2.RemoveOSLaudo"!')
  then
    Exit;
  //
  Codigo := QrOSCabCodigo.Value;
  Grupo  := QrOSCabGrupo.Value;
  Lugar  := QrOSCabSiapTerCad.Value;
  Opcao  := QrOSCabOpcao.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False,
    [Campo], ['Codigo'], [''], [Codigo], True) then
  begin
    LocCod(Grupo, Grupo, dispIndefinido, Lugar, Opcao, True, Codigo);
    PCGeral.ActivePageIndex := 1;
    PCOpera.ActivePageIndex := 5;
  end;
end;

procedure TFmOSCab2.Localizador1Click(Sender: TObject);
var
  Num : String;
  Cod : Integer;
  //Continua, Passa: Boolean;
begin
  Num := InputBox(VAR_GOTOTABELA, 'Digite o c�digo.', '' );
  Cod := Geral.IMV(Num);
  if Cod <> 0 then
    LocalizaLocalizador(Cod);
end;

procedure TFmOSCab2.LocalizaLocalizador(Codigo: Integer);
begin
  OSApp_PF.ReopenLocOS_Idx(DmModOS.QrLocOS, Codigo);
  LocCod(DmModOS.QrLocOSGrupo.Value, DmModOS.QrLocOSGrupo.Value, FDisposicao,
    DmModOS.QrLocOSSiapTerCad.Value, DmModOS.QrLocOSOpcao.Value, True,
    DmModOS.QrLocOSCodigo.Value);
    if QrOSCabCodigo.Value <> Codigo then Geral.MensagemBox(
  'A OS ' + Geral.FF0(FCabIni) + 'n�o foi localizada!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmOSCab2.LocalizaOS(Lugar, Opcao, Codigo: Integer);
begin
  case FDisposicao of
    dispIndefinido,
    dispPorLugar:
    begin
      if Lugar = 0 then
        Exit;
      QrLugares.Locate('SiapTerCad', Lugar, []);
      QrOpcoes.Locate('Opcao', Opcao, []);
    end;
    dispPorOpcao:
    begin
      if Opcao = 0 then
        Exit;
      QrOpcoes.Locate('Opcao', Opcao, []);
      QrLugares.Locate('SiapTerCad', Lugar, []);
    end;
  end;
  QrOSCab.Locate('Codigo', Codigo, []);
end;

procedure TFmOSCab2.LocCod(Atual, Grupo: Integer; Disposicao: TDisposicao;
Lugar, Opcao: Integer; ReabreOSCab: Boolean; OSCab: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Grupo);
  //
  ReopenVariantes(
    Disposicao, Lugar, Opcao, ReabreOSCab, OSCab);
end;

procedure TFmOSCab2.MenuItem2Click(Sender: TObject);
begin
  OSApp_PF.IncluiEPI(QrOSMonEPI, QrOSMonCabCodigo.Value,
    QrOSMonCabControle.Value, QrOSMonCabConta.Value, 'osmonepi');
end;

procedure TFmOSCab2.MenuItem3Click(Sender: TObject);
begin
  OSApp_PF.ExcluiEPI(QrOSMonEPI, DBGOSMonEPI, 'osmonepi', QrOSMonCabConta.Value);
end;

procedure TFmOSCab2.MenuItem5Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruN(0);
end;

procedure TFmOSCab2.Monitoramento1Click(Sender: TObject);
begin
  MostraOSImp(5, Null);
end;

procedure TFmOSCab2.Monitoramentos1Click(Sender: TObject);
const
  FatID = VAR_FATID_4102;
  Pergunta = True;
begin
  if OSApp_PF.RatificaConsumoOS_PreAnalise(nrcosFormula, QrOSSrvControle.Value,
  FatID, Pergunta) then
  begin
    if Geral.MB_Pergunta(
    'Confirma a ratifica��o dos consumos ainda n�o ratificados das f�rmulas de monitoramento deste servi�o?') =
    ID_YES then
    begin
      RatificaConsumo('osmoncab', 'osmonrec', 'Controle', QrOSSrvControle.Value, VAR_FATID_4102);
      //
      OSApp_PF.ReopenOSSrv(QrOSSrv, QrOSCabCodigo.Value, 0);
    end;
  end;
end;

procedure TFmOSCab2.MostraFormCunsCad();
var
  CunsCad: Integer;
begin
  if QrOSCab.State <> dsInactive then
    CunsCad := QrOSCabEntidade.Value
  else
    CunsCad := 0;
  FmPrincipal.MostraFormCunsCad(True, CunsCad, 0);
end;

procedure TFmOSCab2.MostraFormEntidade(Sender: TObject);
{
var
  Entidade: Integer;
begin
  //
  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2);
}
var
  Form: TForm;
  Entidade: Integer;
begin
  Entidade := Geral.IMV(Geral.SoNumero_TT(TDBEdit(Sender).Text));
{
  if QrOSCab.State <> dsInactive then
    Entidade := QrOSCabEntidade.Value
  else
    Entidade := 0;
  //
}
  Form := MyObjects.FormTDICria(TFmEntidade2,
    FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
  TFmEntidade2(Form).LocCod(Entidade, Entidade);
end;

procedure TFmOSCab2.MostraFormOSCabFat(SQLType: TSQLType);
var
  CondicaoPG: Integer;
begin
  //fazer abertura e encerramento de faturamento!
  //ver outros campos necess�rios para faturamento como carteira etc.

{
  if (QrLctFatRef.State <> dsInactive) and
  (QrLctFatRef.RecordCount > 0) then
    SQLType := stUpd
  else
    SQLType := stIns;
  //
}
// 2013-06-14
  //SQLType := stUpd;
// FIM 2013-06-14
  //
  if DBCheck.CriaFm(TFmOSCabFat, FmOSCabFat, afmoNegarComAviso) then
  begin
    FmOSCabFat.ImgTipo.SQLType := SQLType;
    FmOSCabFat.FQrCab := QrOSCab;
    FmOSCabFat.FDsCab := DsOSCab;
    FmOSCabFat.FQrIts := QrLctFatRef;

    FmOSCabFat.DBEdCodigo.DataSource  := DsOSCab;
    FmOSCabFat.DBEdEmpresa.DataSource := DsOSCab;
    FmOSCabFat.DBEdNO_EMP.DataSource  := DsOSCab;
    FmOSCabFat.DBEdPagante.DataSource := DsOSCab;
    FmOSCabFat.DBEdNO_PAG.DataSource  := DsOSCab;

    FmOSCabFat.FEmpresa := QrOSCabEmpresa.Value;
    FmOSCabFat.FCliente := QrOSCabEntPagante.Value;
    //FmOSCabFat.FDataAbriu := QrLocCConDtHrEmi.Value;

    // N�o h� faturamento parcial: Controle e c�digo s�o o mesmo n�mero
    FmOSCabFat.FEncerra := True;
    FmOSCabFat.EdControle.ValueVariant := QrOSCabCodigo.Value;
    // FIM N�o h� faturamento parcial!

    FmOSCabFat.EdValorServi.ValueVariant := QrOSCabValorServi.Value;
    FmOSCabFat.EdValorOutrs.ValueVariant := QrOSCabValorOutrs.Value;
    FmOSCabFat.EdValorDesco.ValueVariant := QrOSCabValorDesco.Value;

    DModG.ReopenParamsEmp(VAR_LIB_EMPRESA_SEL);

    if SQLType = stIns then
    begin
      FmOSCabFat.TPDataFat.Date := Trunc(Date);
      FmOSCabFat.EdHoraFat.Text := FormatDateTime('hh:nn:ss', Now());
      //
      CondicaoPG := CondicaoPGSelecionado();
      UMyMod.SetaCodUsuDeCodigo(FmOSCabFat.EdCondicaoPG,
        FmOSCabFat.CBCondicaoPG, DmFatura.QrPediPrzCab, CondicaoPG);
      //
      FmOSCabFat.EdConta.ValueVariant := DModG.QrParamsEmpCtaServico.Value;
      FmOSCabFat.CBConta.KeyValue     := DModG.QrParamsEmpCtaServico.Value;
    end else
    begin
      //
      UMyMod.SetaCodUsuDeCodigo(FmOSCabFat.EdCondicaoPG,
        FmOSCabFat.CBCondicaoPG, DmFatura.QrPediPrzCab, QrOSCabCondicaoPG.Value);

      FmOSCabFat.EdCartEmis.ValueVariant := QrOSCabCartEmis.Value;
      FmOSCabFat.CBCartEmis.KeyValue     := QrOSCabCartEmis.Value;
      FmOSCabFat.EdConta.ValueVariant    := QrOSCabConta.Value;
      FmOSCabFat.CBConta.KeyValue        := QrOSCabConta.Value;

      //FmOSCabFat.EdValorTotal.ValueVariant := QrOSCabValorTotal.Value;

      FmOSCabFat.EdSerNF.ValueVariant := QrOSCabSerNF.Value;
      FmOSCabFat.EdNumNF.ValueVariant := QrOSCabNumNF.Value;

      FmOSCabFat.TPDataFat.Date := Trunc(QrOSCabDtaFimFat.Value);
      FmOSCabFat.EdHoraFat.Text := FormatDateTime('hh:nn:ss', QrOSCabDtaFimFat.Value);
    end;
    FmOSCabFat.ShowModal;
    FmOSCabFat.Destroy;
  end;
end;

procedure TFmOSCab2.MostraOSAltRec(FatID: Integer);
var
  UsoCusUni: Double;
begin
  if FatID = VAR_FATID_4101 then
  begin
    if DBCheck.CriaFm(TFmOSAltRec, FmOSAltRec, afmoNegarComAviso) then
    begin
      FmOSAltRec.ImgTipo.SQLType := stUpd;

      FmOSAltRec.FQry    := QrOSFrmRec;
      FmOSAltRec.FTabela := 'osfrmrec';
      FmOSAltRec.FFatID  := VAR_FATID_4101;

      FmOSAltRec.EdUsoQtd.DecimalSize    := QrOSFrmRecFracio.Value;
      FmOSAltRec.EdUsoCusUni.DecimalSize := Dmod.FFmtPrc;
      FmOSAltRec.EdUsoCusTot.DecimalSize := Dmod.FFmtPrc;

      FmOSAltRec.EdCodigo.ValueVariant    := QrOSFrmRecCodigo.Value;
      FmOSAltRec.EdControle.ValueVariant  := QrOSFrmRecControle.Value;
      FmOSAltRec.EdConta.ValueVariant     := QrOSFrmRecConta.Value;
      FmOSAltRec.EdIDIts.ValueVariant     := QrOSFrmRecIDIts.Value;
      FmOSAltRec.EdGraGruX.ValueVariant   := QrOSFrmRecGraGruX.Value;

      FmOSAltRec.EdNO_Serv.ValueVariant   := QrOSSrvNO_DesServico.Value;
      FmOSAltRec.EdNO_Frm.ValueVariant    := QrOSFrmCabNO_FORMULA.Value;
      FmOSAltRec.EdNO_GG1.ValueVariant    := QrOSFrmRecNO_GG1.Value;

      FmOSAltRec.EdUsoQtd.ValueVariant    := QrOSFrmRecUsoQtd.Value;

      UsoCusUni := QrOSFrmRecUsoCusUni.Value;

      if UsoCusUni = 0 then
        UsoCusUni := DmProd.ObtemPrecoDeCusto(QrOSCabCodEnti.Value,
                       QrOSFrmRecGraGruX.Value, QrOSCabLstCusPrd.Value);

      FmOSAltRec.EdUsoCusUni.ValueVariant := UsoCusUni;

      FmOSAltRec.ShowModal;
      FmOSAltRec.Destroy;
    end;
  end else
  if FatID = VAR_FATID_4102 then
  begin
    if DBCheck.CriaFm(TFmOSAltRec, FmOSAltRec, afmoNegarComAviso) then
    begin
      FmOSAltRec.ImgTipo.SQLType := stUpd;

      FmOSAltRec.FQry    := QrOSMonRec;
      FmOSAltRec.FTabela := 'osmonrec';
      FmOSAltRec.FFatID  := VAR_FATID_4102;

      FmOSAltRec.EdUsoQtd.DecimalSize    := QrOSMonRecFracio.Value;
      FmOSAltRec.EdUsoCusUni.DecimalSize := Dmod.FFmtPrc;
      FmOSAltRec.EdUsoCusTot.DecimalSize := Dmod.FFmtPrc;

      FmOSAltRec.EdCodigo.ValueVariant    := QrOSMonRecCodigo.Value;
      FmOSAltRec.EdControle.ValueVariant  := QrOSMonRecControle.Value;
      FmOSAltRec.EdConta.ValueVariant     := QrOSMonRecConta.Value;
      FmOSAltRec.EdIDIts.ValueVariant     := QrOSMonRecIDIts.Value;
      FmOSAltRec.EdGraGruX.ValueVariant   := QrOSMonRecGraGruX.Value;

      FmOSAltRec.EdNO_Serv.ValueVariant   := QrOSSrvNO_DesServico.Value;
      FmOSAltRec.EdNO_Frm.ValueVariant    := QrOSMonCabNO_FORMULA.Value;
      FmOSAltRec.EdNO_GG1.ValueVariant    := QrOSMonRecNO_GG1.Value;

      FmOSAltRec.EdUsoQtd.ValueVariant    := QrOSMonRecUsoQtd.Value;

      UsoCusUni := QrOSMonRecUsoCusUni.Value;

      if UsoCusUni = 0 then
        UsoCusUni := DmProd.ObtemPrecoDeCusto(QrOSCabCodEnti.Value,
                       QrOSMonRecGraGruX.Value, QrOSCabLstCusPrd.Value);

      FmOSAltRec.EdUsoCusUni.ValueVariant := UsoCusUni;
      //
      FmOSAltRec.ShowModal;
      FmOSAltRec.Destroy;
    end;
  end else
  begin
    Geral.MB_Aviso('FatID n�o implementado!');
    Exit;
  end;

end;

procedure TFmOSCab2.MostraOSAlv(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSAlv, FmOSAlv, afmoNegarComAviso) then
  begin
    FmOSAlv.ImgTipo.SQLType := SQLType;
    FmOSAlv.FTabela := 'osalv';
    FmOSAlv.FQrOSAlv := QrOSAlv;
    FmOSAlv.FDatabase := Dmod.MyDB;
    FmOSAlv.FQrInsere := Dmod.QrUpd;
    FmOSAlv.FCodigo := QrOSCabCodigo.Value;
    FmOSAlv.FControle := QrOSSrvControle.Value;
    //                   
    FmOSAlv.QrOSSrv.Close;
    FmOSAlv.QrOSSrv.SQL.Text := QrOSSrv.SQL.Text;
    FmOSAlv.QrOSSrv.Params := QrOSSrv.Params;
    UMyMod.AbreQuery(FmOSAlv.QrOSSrv, Dmod.MyDB);
    //
    { // N�o tem!
    if SQLType = stUpd then
    begin
      // N�o tem!
    end;
    }
    FmOSAlv.ShowModal;
    FmOSAlv.Destroy;
  end;
end;

procedure TFmOSCab2.MostraOSCxa(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSCxa, FmOSCxa, afmoNegarComAviso) then
  begin
    FmOSCxa.ImgTipo.SQLType := SQLType;
    FmOSCxa.FQrInsUpd := QrOSCxa;
    //
    FmOSCxa.QrOSCab.Close;
    FmOSCxa.QrOSCab.SQL.Text := QrOSCab.SQL.Text;
    FmOSCxa.QrOSCab.Params := QrOSCab.Params;
    UMyMod.AbreQuery(FmOSCxa.QrOSCab, Dmod.MyDB);
    //
    if SQLType = stUpd then
    begin
      FmOSCxa.EdControle.ValueVariant := QrOSCxaControle.Value;
      FmOSCxa.EdCaixa.ValueVariant := QrOSCxaCaixa.Value;
      FmOSCxa.CBCaixa.KeyValue := QrOSCxaCaixa.Value;
      FmOSCxa.EdGarantiaDd.ValueVariant := QrOSCxaGarantiaDd.Value;
      FmOSCxa.EdHrEvacuar.ValueVariant := QrOSCxaHrEvacuar.Value;
      FmOSCxa.EdHrExecutar.ValueVariant := QrOSCxaHrExecutar.Value;
      //
      FmOSCxa.EdValCalc.ValueVariant := QrOSCxaValCalc.Value;
      FmOSCxa.EdValInfo.ValueVariant := QrOSCxaValInfo.Value;
      //FmOSCxa.EdValDesc.ValueVariant := QrOSCxaValDesc.Value;
      FmOSCxa.EdValTota.ValueVariant := QrOSCxaValTota.Value;
      FmOSCxa.EdChekLstCab.ValueVariant := QrOSCxaChekLstCab.Value;
      //
      FmOSCxa.RGAutorizado.ItemIndex := QrOSCxaAutorizado.Value;
      FmOSCxa.MeDetalhes.Text        := QrOSCxaDetalhes.Value;
      //
    end;
    FmOSCxa.FCriou := True;
    //FmOSCxa.CalculaTotal();
    FmOSCxa.ShowModal;
    FmOSCxa.Destroy;
    //
    LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
      QrOSCabCodigo.Value);
  end;
end;

procedure TFmOSCab2.MostraOSCxI(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSCxI, FmOSCxI, afmoNegarComAviso) then
  begin
    FmOSCxI.ImgTipo.SQLType := SQLType;
    FmOSCxI.FQrInsUpd := QrOSCxI;
    //
    FmOSCxI.QrOSCab.Close;
    FmOSCxI.QrOSCab.SQL.Text := QrOSCab.SQL.Text;
    FmOSCxI.QrOSCab.Params := QrOSCab.Params;
    UMyMod.AbreQuery(FmOSCxI.QrOSCab, Dmod.MyDB);
    //
    if SQLType = stUpd then
    begin
      FmOSCxI.EdControle.ValueVariant := QrOSCxIControle.Value;
      FmOSCxI.EdFotoCxa.ValueVariant  := QrOSCxIFotoCxa.Value;
      FmOSCxI.MeDetalhes.Text         := QrOSCxIDetalhes.Value;
      FmOSCxI.CGAplicacao.Value       := QrOSCxIAplicacao.Value;
      //
    end else
    begin
      FmOSCxI.CGAplicacao.Value := 0;
    end;
    FmOSCxI.FCriou := True;
    //FmOSCxI.CalculaTotal();
    FmOSCxI.ShowModal;
    FmOSCxI.Destroy;
    //
    LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
      QrOSCabCodigo.Value);
  end;
end;

procedure TFmOSCab2.MostraOSExtMon(SQLType: TSQLType);
const
  KindData = 0;
var
  DiasMin, DiasMax: Integer;
  DtaExeFim: TDateTime;
  Dias: Integer;
begin
  if MyObjects.FIC(QrOSCabNumContrat.Value > 0, nil, 'Esta OS � de contrato!' +
  sLinebreak + 'Gerencie o Monitoramento no contrato!') then
    Exit;
  if MyObjects.FIC(QrOSCabDtaExeFim.Value < 2, nil,
  'Data de encerramento inv�lida!') then
    Exit;
  //
  OSApp_PF.DiasMinEMaxNextExtenMonit(QrOSCabCodigo.Value, DiasMin, DiasMax);
  //
  if DBCheck.CriaFm(TFmOSExtMon, FmOSExtMon, afmoNegarComAviso) then
  begin
    FmOSExtMon.ImgTipo.SQLType := SQLType;
    FmOSExtMon.FQrCab  := QrOSCab;
    FmOSExtMon.FDsCab  := DsOSCab;
    FmOSExtMon.FQrIts  := QrOSExtMon;
    FmOSExtMon.FAtuDiasMax := DiasMax;
    FmOSExtMon.FMinData := QrOSCabDtaExeFim.Value + DiasMin + 1;
    FmOSExtMon.FDtaExeFim := QrOSCabDtaExeFim.Value;
    if SQLType = stIns then
    begin
      FmOSExtMon.TPDtaFinal.Date := FmOSExtMon.FMinData + 89;
    end else
    begin
      FmOSExtMon.EdControle.ValueVariant  := QrOSExtMonControle.Value;
      //
      FmOSExtMon.TPDtaFinal.Date        := QrOSExtMonDtaFinal.Value;
      FmOSExtMon.EdPerioDd.ValueVariant := QrOSExtMonPerioDd.Value;
      FmOSExtMon.EdTotalDd.ValueVariant := QrOSExtMonTotalDd.Value;
    end;
    FmOSExtMon.CalculaDataEDias(KindData);
    FmOSExtMon.ShowModal;
    FmOSExtMon.Destroy;
  end;
  LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
    QrOSCabCodigo.Value);
end;

procedure TFmOSCab2.MostraOSFrmDep(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSFrmDep, FmOSFrmDep, afmoNegarComAviso) then
  begin
    FmOSFrmDep.ImgTipo.SQLType := SQLType;
    FmOSFrmDep.FQrOSFrmDep := QrOSFrmDep;
    FmOSFrmDep.FCodigo := QrOSCabCodigo.Value;
    FmOSFrmDep.FControle := QrOSSrvControle.Value;
    FmOSFrmDep.FConta := QrOSFrmCabConta.Value;
    FmOSFrmDep.FSiapTerCad := QrOSCabSiapTerCad.Value;
    FmOSFrmDep.FEntidade := QrOSCabEntidade.Value;
    //
    FmOSFrmDep.QrOSSrv.Close;
    FmOSFrmDep.QrOSSrv.SQL.Text := QrOSSrv.SQL.Text;
    FmOSFrmDep.QrOSSrv.Params := QrOSSrv.Params;
    UMyMod.AbreQuery(FmOSFrmDep.QrOSSrv, Dmod.MyDB);
    //
    //
    FmOSFrmDep.QrOSFrmCab.Close;
    FmOSFrmDep.QrOSFrmCab.SQL.Text := QrOSFrmCab.SQL.Text;
    FmOSFrmDep.QrOSFrmCab.Params := QrOSFrmCab.Params;
    UMyMod.AbreQuery(FmOSFrmDep.QrOSFrmCab, Dmod.MyDB);
    FmOSFrmDep.QrOSFrmCab.Locate('Conta', QrOSFrmCabConta.Value, []);
    //
    if SQLType = stUpd then
    begin
      // N�o tem!
    end;
    FmOSFrmDep.ShowModal;
    FmOSFrmDep.Destroy;
  end;
end;

procedure TFmOSCab2.MostraOSFrmEvo(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSFrmEvo, FmOSFrmEvo, afmoNegarComAviso) then
  begin
    FmOSFrmEvo.ImgTipo.SQLType := SQLType;
    FmOSFrmEvo.FQrOSSrv := QrOSSrv;
    FmOSFrmEvo.FQrOSFrmCab := QrOSFrmCab;
    FmOSFrmEvo.FQrOSFrmEvo := QrOSFrmEvo;
    FmOSFrmEvo.FCodigo := QrOSFrmCabCodigo.Value;
    FmOSFrmEvo.FControle := QrOSFrmCabControle.Value;
    FmOSFrmEvo.FConta := QrOSFrmCabConta.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(FmOSFrmEvo.QrOSFrmCab, Dmod.MyDB, [
    'SELECT Codigo, Controle, Conta, Nome ',
    'FROM osfrmcab ',
    'WHERE Conta=' + Geral.FF0(QrOSFrmCabConta.Value),
    '']);
    //
    if SQLType = stUpd then
    begin
      FmOSFrmEvo.FIDIts := QrOSFrmEvoIDIts.Value;
      //
      FmOSFrmEvo.TPDataHora.Date := QrOSFrmEvoDataHora.Value;
      FmOSFrmEvo.EdDataHora.ValueVariant := QrOSFrmEvoDataHora.Value;
      FmOSFrmEvo.EdPercFeito.ValueVariant := QrOSFrmEvoPercFeito.Value;
      FmOSFrmEvo.MeTexto.Text := QrOSFrmEvoTexto.Value;
    end
    else
      FmOSFrmEvo.FIDIts := 0;
    FmOSFrmEvo.ShowModal;
    FmOSFrmEvo.Destroy;
  end;
end;

procedure TFmOSCab2.MostraOSFrmFlhCb(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSFrmFlhCb, FmOSFrmFlhCb, afmoNegarComAviso) then
  begin
    FmOSFrmFlhCb.ImgTipo.SQLType := SQLType;
    FmOSFrmFlhCb.FQrCab          := QrOSFrmCab;
    FmOSFrmFlhCb.FDsCab          := DsOSFrmCab;
    FmOSFrmFlhCb.FQrIts          := QrOSFrmFlhCb;
    //
    if SQLType = stIns then
    begin
      FmOSFrmFlhCb.EdFormula.ValueVariant := QrOSFrmCabFormula.Value;
      FmOSFrmFlhCb.CBFormula.KeyValue     := QrOSFrmCabFormula.Value;
    end else
    begin
      FmOSFrmFlhCb.EdIDIts.ValueVariant     := QrOSFrmFlhCbIdIts.Value;
      //
      FmOSFrmFlhCb.EdFormula.ValueVariant   := QrOSFrmFlhCbFormula.Value;
      FmOSFrmFlhCb.CBFormula.KeyValue       := QrOSFrmFlhCbFormula.Value;
      FmOSFrmFlhCb.EdPerioDd.ValueVariant   := QrOSFrmFlhCbPerioDd.Value;
      FmOSFrmFlhCb.EdDdPostero.ValueVariant := QrOSFrmFlhCbDdPostero.Value;
      FmOSFrmFlhCb.RGEmisStatus.ItemIndex   := QrOSFrmFlhCbEmisStatus.Value;
    end;
    FmOSFrmFlhCb.ShowModal;
    FmOSFrmFlhCb.Destroy;
  end;
end;

procedure TFmOSCab2.MostraOSFrmFlhDd(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSFrmFlhDd, FmOSFrmFlhDd, afmoNegarComAviso) then
  begin
    FmOSFrmFlhDd.ImgTipo.SQLType := SQLType;
    FmOSFrmFlhDd.FQrCab := QrOSFrmFlhCb;
    FmOSFrmFlhDd.FDsCab := DsOSFrmFlhCb;
    FmOSFrmFlhDd.FQrIts := QrOSFrmFlhDd;
    if SQLType = stIns then
    begin
      FmOSFrmFlhDd.EdOrdem.ValueVariant := ProximaOrdemFrmFlhDd();
    end else
    begin
      FmOSFrmFlhDd.EdIDIt2.ValueVariant := QrOSFrmFlhDdIdIt2.Value;
      //
      FmOSFrmFlhDd.EdOrdem.ValueVariant := QrOSFrmFlhDdOrdem.Value;
      FmOSFrmFlhDd.EdDias.ValueVariant  := QrOSFrmFlhDdDias.Value;
    end;
    FmOSFrmFlhDd.ShowModal;
    FmOSFrmFlhDd.Destroy;
  end;
end;

procedure TFmOSCab2.MostraOSFrmCab(SQLType: TSQLType);
var
  Conta: Integer;
begin
  //Conta := 0;
  if DBCheck.CriaFm(TFmOSFrmCab, FmOSFrmCab, afmoNegarComAviso) then
  begin
    FmOSFrmCab.ImgTipo.SQLType := SQLType;
    FmOSFrmCab.FQrOSFrmCab := QrOSFrmCab;
    FmOSFrmCab.FControle := QrOSSrvControle.Value;
    FmOSFrmCab.FConta := QrOSFrmCabConta.Value;
    FmOSFrmCab.FCodigo     := QrOSCabCodigo.Value;
    //
    FmOSFrmCab.QrOSSrv.Close;
    FmOSFrmCab.QrOSSrv.SQL.Text := QrOSSrv.SQL.Text;
    FmOSFrmCab.QrOSSrv.Params := QrOSSrv.Params;
    UMyMod.AbreQuery(FmOSFrmCab.QrOSSrv, Dmod.MyDB);
    //
{
    FmOSFrmCab.GbEditaB.Visible :=
      // Primeira a��o de execu��o
      (QrOSCabFatoGeradr.Value = CO_COD_FatoGeradr_1aAcao)
      (* e que n�o tenha contrato
      and (QrOSCabNumContrat.Value = 0)*);
}
    if SQLType = stUpd then
    begin
      FmOSFrmCab.EdConta.ValueVariant := QrOSFrmCabConta.Value;
      FmOSFrmCab.EdNome.Text := QrOSFrmCabNome.Value;
      //
      FmOSFrmCab.EdFormula.ValueVariant := QrOSFrmCabFormula.Value;
      FmOSFrmCab.CBFormula.KeyValue := QrOSFrmCabFormula.Value;
      //
      FmOSFrmCab.EdEquipAplic.ValueVariant := QrOSFrmCabEquipAplic.Value;
      FmOSFrmCab.CBEquipAplic.KeyValue := QrOSFrmCabEquipAplic.Value;
      //
      FmOSFrmCab.EdTipoAplica.ValueVariant := QrOSFrmCabTipoAplica.Value;
      FmOSFrmCab.CBTipoAplica.KeyValue := QrOSFrmCabTipoAplica.Value;
      //
      FmOSFrmCab.EdQtdTot.ValueVariant := QrOSFrmCabQtdTot.Value;
      FmOSFrmCab.RGDiluente.ItemIndex := QrOSFrmCabDiluente.Value;
      FmOSFrmCab.RGIndicUso.ItemIndex := QrOSFrmCabIndicUso.Value;
      //FmOSFrmCab.EdQtdQSP.ValueVariant := QrOSFrmCabQtdQSP.Value;
      //
      FmOSFrmCab.EdSigla.Text              := QrOSFrmCabSIGLA.Value;
      FmOSFrmCab.EdUnidMed.ValueVariant    := QrOSFrmCabCU_UnidMed.Value;
      FmOSFrmCab.CBUnidMed.KeyValue        := QrOSFrmCabCU_UnidMed.Value;
    end;
    FmOSFrmCab.ShowModal;
    Conta := FmOSFrmCab.FConta;
    FmOSFrmCab.Destroy;
    //
    if (Conta <> 0) and (QrOSFrmCab.State <> dsInactive)
    and (QrOSFrmCabConta.Value = Conta) then
     Itens4Click(Self);
  end;
end;

procedure TFmOSCab2.MostraOSFrmRec();
begin
  if DBCheck.CriaFm(TFmOSFrmRec, FmOSFrmRec, afmoNegarComAviso) then
  begin
    //FmOSFrmRec.ImgTipo.SQLType := SQLType;
    FmOSFrmRec.FQrOSFrmRec := QrOSFrmRec;
    FmOSFrmRec.FQrOSFrmCab := QrOSFrmCab;
    //FmOSFrmRec.FQrOSSrv := QrOSSrv;
    FmOSFrmRec.FCodigo := QrOSCabCodigo.Value;
    FmOSFrmRec.FControle := QrOSSrvControle.Value;
    FmOSFrmRec.FConta := QrOSFrmCabConta.Value;
    //
    FmOSFrmRec.QrOSSrv.Close;
    FmOSFrmRec.QrOSSrv.SQL.Text := QrOSSrv.SQL.Text;
    FmOSFrmRec.QrOSSrv.Params := QrOSSrv.Params;
    UMyMod.AbreQuery(FmOSFrmRec.QrOSSrv, Dmod.MyDB);
    //
    FmOSFrmRec.QrOSFrmCab.Close;
    FmOSFrmRec.QrOSFrmCab.SQL.Text := QrOSFrmCab.SQL.Text;
    FmOSFrmRec.QrOSFrmCab.Params := QrOSFrmCab.Params;
    UMyMod.AbreQuery(FmOSFrmRec.QrOSFrmCab, Dmod.MyDB);
    FmOSFrmRec.QrOSFrmCab.Locate('Conta', QrOSFrmCabConta.Value, []);
    //

    FmOSFrmRec.ShowModal;
    FmOSFrmRec.Destroy;
  end;
end;

procedure TFmOSCab2.MostraOSFrmAbr(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSFrmAbr, FmOSFrmAbr, afmoNegarComAviso) then
  begin
    FmOSFrmAbr.ImgTipo.SQLType := SQLType;
    FmOSFrmAbr.FQrOSFrmAbr := QrOSFrmAbr;
    FmOSFrmAbr.FCodigo := QrOSFrmCabCodigo.Value;
    FmOSFrmAbr.FControle := QrOSFrmCabControle.Value;
    FmOSFrmAbr.FConta := QrOSFrmCabConta.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(FmOSFrmAbr.QrOSFrmCab, Dmod.MyDB, [
    'SELECT Codigo, Controle, Conta, Nome ',
    'FROM osfrmcab ',
    'WHERE Conta=' + Geral.FF0(QrOSFrmCabCodigo.Value),
    '']);
    //
    { // N�o tem!
    if SQLType = stUpd then
    begin
      // N�o tem!
    end;
    }
    FmOSFrmAbr.ShowModal;
    FmOSFrmAbr.Destroy;
  end;
end;

procedure TFmOSCab2.MostraOSMonCabMul();
const
  SQLType = stIns;
var
  //Conta: Integer;
  PeriodoDias: Integer;
begin
  //Conta := 0;
  if DBCheck.CriaFm(TFmOSMonCabIns, FmOSMonCabIns, afmoNegarComAviso) then
  begin
    FmOSMonCabIns.ImgTipo.SQLType := SQLType;
    FmOSMonCabIns.FQrOSMonCab := QrOSMonCab;
    FmOSMonCabIns.FQrOSMonRec := QrOSMonRec;
    FmOSMonCabIns.FCodigo     := QrOSCabCodigo.Value;
    FmOSMonCabIns.FControle   := QrOSSrvControle.Value;
    //FmOSMonCabIns.FPipCad     := QrOSMonCabPipCad.Value;
    FmOSMonCabIns.FSiapterCad := QrOSCabSiapTerCad.Value;
    FmOSMonCabIns.FEntidade   := QrOSCabEntidade.Value;
    FmOSMonCabIns.FDtaExeFim  := QrOSCabDtaExeFim.Value;
    //
    if SQLType = stIns then
    begin
      PeriodoDias := OSAll_PF.ObtemPeriodoDiasContrato(QrOSCabNumContrat.Value);
      //
      FmOSMonCabIns.EdPerioDd.ValueVariant := PeriodoDias;
    end;
    //
    FmOSMonCabIns.QrOSSrv.Close;
    FmOSMonCabIns.QrOSSrv.SQL.Text := QrOSSrv.SQL.Text;
    FmOSMonCabIns.QrOSSrv.Params := QrOSSrv.Params;
    UMyMod.AbreQuery(FmOSMonCabIns.QrOSSrv, Dmod.MyDB);
    //
    FmOSMonCabIns.ShowModal;
    //Conta := FmOSMonCabIns.FConta;
    FmOSMonCabIns.Destroy;
    //
    //
    OSApp_PF.ReopenOSMonRec(QrOSMonRec, QrOSMonCabConta.Value, 0);
  end;
end;

procedure TFmOSCab2.MostraOSMonCabUni(SQLType: TSQLType);
var
  Conta: Integer;
begin
  //Conta := 0;
  if DBCheck.CriaFm(TFmOSMonCabUpd, FmOSMonCabUpd, afmoNegarComAviso) then
  begin
    FmOSMonCabUpd.ImgTipo.SQLType := SQLType;
    FmOSMonCabUpd.FQrOSMonCab := QrOSMonCab;
    FmOSMonCabUpd.FQrOSMonRec := QrOSMonRec;
    FmOSMonCabUpd.FCodigo     := QrOSCabCodigo.Value;
    FmOSMonCabUpd.FControle   := QrOSSrvControle.Value;
    FmOSMonCabUpd.FPipCad     := QrOSMonCabPipCad.Value;
    FmOSMonCabUpd.FSiapterCad := QrOSCabSiapTerCad.Value;
    FmOSMonCabUpd.FEntidade   := QrOSCabEntidade.Value;
    //
    FmOSMonCabUpd.QrOSSrv.Close;
    FmOSMonCabUpd.QrOSSrv.SQL.Text := QrOSSrv.SQL.Text;
    FmOSMonCabUpd.QrOSSrv.Params := QrOSSrv.Params;
    UMyMod.AbreQuery(FmOSMonCabUpd.QrOSSrv, Dmod.MyDB);
    //
    if SQLType = stUpd then
    begin
      FmOSMonCabUpd.EdConta.ValueVariant := QrOSMonCabConta.Value;
      FmOSMonCabUpd.EdNome.Text := QrOSMonCabNome.Value;
      //
      FmOSMonCabUpd.EdFormula.ValueVariant := QrOSMonCabFormula.Value;
      FmOSMonCabUpd.CBFormula.KeyValue := QrOSMonCabFormula.Value;
      //
      FmOSMonCabUpd.EdEquipAplic.ValueVariant := QrOSMonCabEquipAplic.Value;
      FmOSMonCabUpd.CBEquipAplic.KeyValue := QrOSMonCabEquipAplic.Value;
      //
      FmOSMonCabUpd.EdTipoAplica.ValueVariant := QrOSMonCabTipoAplica.Value;
      FmOSMonCabUpd.CBTipoAplica.KeyValue := QrOSMonCabTipoAplica.Value;
      //
      FmOSMonCabUpd.EdQtdTot.ValueVariant := QrOSMonCabQtdTot.Value;
      FmOSMonCabUpd.EdQtdQSP.ValueVariant := QrOSMonCabQtdQSP.Value;
      //
      FmOSMonCabUpd.EdPipCad.ValueVariant := QrOSMonCabPipCad.Value;
      FmOSMonCabUpd.CBPipCad.KeyValue     := QrOSMonCabPipCad.Value;
      //
      FmOSMonCabUpd.RGDiluente.ItemIndex := QrOSMonCabDiluente.Value;
      //
      FmOSMonCabUpd.EdPerioDd.ValueVariant := QrOSMonCabPerioDd.Value;
      FmOSMonCabUpd.EdDdPostero.ValueVariant := QrOSMonCabDdPostero.Value;
      //
      //
      FmOSMonCabUpd.EdSigla.Text              := QrOSMonCabSIGLA.Value;
      FmOSMonCabUpd.EdUnidMed.ValueVariant    := QrOSMonCabCU_UnidMed.Value;
      FmOSMonCabUpd.CBUnidMed.KeyValue        := QrOSMonCabCU_UnidMed.Value;
    end;
    FmOSMonCabUpd.ShowModal;
    Conta := FmOSMonCabUpd.FConta;
    FmOSMonCabUpd.Destroy;
    //
    if (Conta <> 0) and (QrOSMonCab.State <> dsInactive)
    and (QrOSMonCabConta.Value = Conta) then
     Itens5Click(Self);
  end;
end;

procedure TFmOSCab2.MostraOSMonPipDd(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSMonPipDd, FmOSMonPipDd, afmoNegarComAviso) then
  begin
    FmOSMonPipDd.ImgTipo.SQLType := SQLType;
    FmOSMonPipDd.FQrCab := QrOSMonCab;
    FmOSMonPipDd.FDsCab := DsOSMonCab;
    FmOSMonPipDd.FQrIts := QrOSMonPipDd;
    if SQLType = stIns then
    begin
      FmOSMonPipDd.EdOrdem.ValueVariant := ProximaOrdemMonPipDd();
    end else
    begin
      FmOSMonPipDd.EdIDIts.ValueVariant := QrOSMonPipDdIdIts.Value;
      //
      FmOSMonPipDd.EdOrdem.ValueVariant := QrOSMonPipDdOrdem.Value;
      FmOSMonPipDd.EdDias.ValueVariant  := QrOSMonPipDdDias.Value;
    end;
    FmOSMonPipDd.ShowModal;
    FmOSMonPipDd.Destroy;
  end;
end;

{
procedure TFmOSCab2._Mostra_OS_Mon_Dep_(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(T_Fm_OS_Mon_Dep_, _Fm_OS_Mon_Dep_, afmoNegarComAviso) then
  begin
    _Fm_OS_Mon_Dep_.ImgTipo.SQLType := SQLType;
    _Fm_OS_Mon_Dep_.F_Qr_OS_Mon_Dep_ := _Qr_OS_Mon_Dep_;
    _Fm_OS_Mon_Dep_.FCodigo := QrOSCabCodigo.Value;
    _Fm_OS_Mon_Dep_.FControle := QrOSSrvControle.Value;
    _Fm_OS_Mon_Dep_.FConta := QrOSMonCabConta.Value;
    _Fm_OS_Mon_Dep_.FSiapTerCad := QrOSCabSiapTerCad.Value;
    _Fm_OS_Mon_Dep_.FEntidade := QrOSCabEntidade.Value;
    //
    _Fm_OS_Mon_Dep_.QrOSSrv.Close;
    _Fm_OS_Mon_Dep_.QrOSSrv.SQL.Text := QrOSSrv.SQL.Text;
    _Fm_OS_Mon_Dep_.QrOSSrv.Params := QrOSSrv.Params;
    UMyMod.AbreQuery(_Fm_OS_Mon_Dep_.QrOSSrv, Dmod.MyDB);
    //
    //
    _Fm_OS_Mon_Dep_.QrOSMonCab.Close;
    _Fm_OS_Mon_Dep_.QrOSMonCab.SQL.Text := QrOSMonCab.SQL.Text;
    _Fm_OS_Mon_Dep_.QrOSMonCab.Params := QrOSMonCab.Params;
    UMyMod.AbreQuery(_Fm_OS_Mon_Dep_.QrOSMonCab, Dmod.MyDB);
    _Fm_OS_Mon_Dep_.QrOSMonCab.Locate('Conta', QrOSMonCabConta.Value, []);
    //
    if SQLType = stUpd then
    begin
      // N�o tem!
    end;
    _Fm_OS_Mon_Dep_.ShowModal;
    _Fm_OS_Mon_Dep_.Destroy;
  end;
end;
}

procedure TFmOSCab2.MostraOSMonRec();
begin
  if DBCheck.CriaFm(TFmOSMonRec, FmOSMonRec, afmoNegarComAviso) then
  begin
    //FmOSMonRec.ImgTipo.SQLType := SQLType;
    FmOSMonRec.FQrOSMonRec := QrOSMonRec;
    FmOSMonRec.FQrOSMonCab := QrOSMonCab;
    FmOSMonRec.FCodigo     := QrOSCabCodigo.Value;
    FmOSMonRec.FControle   := QrOSSrvControle.Value;
    FmOSMonRec.FConta      := QrOSMonCabConta.Value;
    FmOSMonRec.FDiluente   := QrOSMonCabDiluente.Value;
    //
    FmOSMonRec.QrOSSrv.Close;
    FmOSMonRec.QrOSSrv.SQL.Text := QrOSSrv.SQL.Text;
    FmOSMonRec.QrOSSrv.Params   := QrOSSrv.Params;
    UMyMod.AbreQuery(FmOSMonRec.QrOSSrv, Dmod.MyDB);
    //
    FmOSMonRec.QrOSMonCab.Close;
    FmOSMonRec.QrOSMonCab.SQL.Text := QrOSMonCab.SQL.Text;
    FmOSMonRec.QrOSMonCab.Params   := QrOSMonCab.Params;
    UMyMod.AbreQuery(FmOSMonRec.QrOSMonCab, Dmod.MyDB);
    FmOSMonRec.QrOSMonCab.Locate('Conta', QrOSMonCabConta.Value, []);
    //
    FmOSMonRec.ShowModal;
    FmOSMonRec.Destroy;
  end;
end;

procedure TFmOSCab2.MostraOSPos(SQLType: TSQLType);
begin
{
  if DBCheck.CriaFm(TFmOSPos, FmOSPos, afmoNegarComAviso) then
  begin
    FmOSPos.ImgTipo.SQLType := SQLType;
    //FmOSPos.FQrOSPos := QrOSPos;
    //
    FmOSPos.EdCodigo.ValueVariant := QrOSCabCodigo.Value;
    //
    if SQLType = stUpd then
    begin
      FmOSPos.EdControle.ValueVariant := QrOSPosControle.Value;
      FmOSPos.EdDias.ValueVariant     := QrOSPosDias.Value;
      FmOSPos.RGAplicacao.ItemIndex   := QrOSPosAplicacao.Value;
      FmOSPos.EdAplicID.ValueVariant  := QrOSPosAplicID.Value;
      FmOSPos.CBAplicID.KeyValue      := QrOSPosAplicID.Value;
    end;
    FmOSPos.ShowModal;
    FmOSPos.Destroy;
    //
    OSApp_PF.ReopenOSPos(QrOSPos, QrOSCabCodigo.Value, 0);
  end;
}
  if DmModOS.MostraOSPos(SQLType,
  QrOSCabCodigo.Value,
  QrOSPosControle.Value,
  QrOSPosDias.Value,
  QrOSPosAplicacao.Value,
  QrOSPosAplicID.Value) then
    OSApp_PF.ReopenOSPos(QrOSPos, QrOSCabCodigo.Value, 0);
end;

procedure TFmOSCab2.MostraOSPsq();
var
  Grupo, Opcao, Lugar, OSCab: Integer;
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmOSPsq, FmOSPsq, afmoNegarComAviso) then
  begin
    if (QrOSCab.State <> dsInactive) and (QrOSCab.RecordCount > 0) then
    begin
      FmOSPsq.EdEmpresa.ValueVariant := QrOSCabEmpresa.Value;
      FmOSPsq.CBEmpresa.KeyValue     := QrOSCabEmpresa.Value;
    end;
    FmOSPsq.ShowModal;
    Grupo := FmOSPsq.FGrupo;
    Opcao := FmOSPsq.FOpcao;
    Lugar := FmOSPsq.FLugar;
    OSCab := FmOSPsq.FOSCab;
    FmOSPsq.Destroy;
    //
    if Grupo <> 0 then
      LocCod(Grupo, Grupo, FDisposicao, Lugar, Opcao, True, OSCab);
    if (QrOSCab.State = dsInactive) or (QrOSCabCodigo.Value = 0) then
      OSApp_PF.ReopenOSCab(QrOSCab, OSCab);
  end;
end;

procedure TFmOSCab2.MostraOSSrv(SQLType: TSQLType);
var
  Codigo: Integer;
begin
  Codigo := QrOSCabCodigo.Value;
  //
  if DBCheck.CriaFm(TFmOsSrv, FmOsSrv, afmoNegarComAviso) then
  begin
    FmOsSrv.ImgTipo.SQLType := SQLType;
    FmOsSrv.FQrInsUpd := QrOsSrv;
    //
    FmOsSrv.QrOSCab.Close;
    FmOsSrv.QrOSCab.SQL.Text := QrOSCab.SQL.Text;
    FmOsSrv.QrOSCab.Params := QrOSCab.Params;
    UMyMod.AbreQuery(FmOsSrv.QrOSCab, Dmod.MyDB);
    //
    if SQLType = stUpd then
    begin
      FmOsSrv.EdControle.ValueVariant   := QrOSSrvControle.Value;
      FmOsSrv.EdDesServico.ValueVariant := QrOSSrvDesServico.Value;
      FmOsSrv.CBDesServico.KeyValue     := QrOSSrvDesServico.Value;
      FmOsSrv.EdGarantiaDd.ValueVariant := QrOSSrvGarantiaDd.Value;
      FmOsSrv.EdHrEvacuar.ValueVariant  := QrOSSrvHrEvacuar.Value;
      FmOsSrv.EdHrExecutar.ValueVariant := QrOSSrvHrExecutar.Value;
      //
      FmOsSrv.EdValCalc.ValueVariant    := QrOSSrvValCalc.Value;
      FmOsSrv.EdValInfo.ValueVariant    := QrOSSrvValInfo.Value;
      FmOsSrv.EdValDesc.ValueVariant    := QrOSSrvValDesc.Value;
      FmOsSrv.EdValTota.ValueVariant    := QrOSSrvValTota.Value;
      //
      FmOsSrv.RGAutorizado.ItemIndex    := QrOSSrvAutorizado.Value;
      FmOSSrv.MeDetalhes.Text           := QrOSSrvDetalhes.Value;
      //
      FmOsSrv.EdMoniDdTotl.ValueVariant := QrOSSrvMoniDdTotl.Value;
      FmOSSrv.EdMoniDdIntv.ValueVariant := QrOSSrvMoniDdIntv.Value;
      //
      FmOsSrv.RGTudoFeitoM.ItemIndex    := QrOSSrvTudoFeitoM.Value;
      //
    end;
    FmOsSrv.FCriou := True;
    //FmOsSrv.CalculaTotal();
    FmOsSrv.ShowModal;
    FmOsSrv.Destroy;
    PCGeral.ActivePageIndex := 1;
    //
    LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
      QrOSCabCodigo.Value);
    //
    if SQLType = stUpd then
    begin
      if Codigo = QrOSCabCodigo.Value then
        OSsFuturas()
      else
        Geral.MB_Aviso('Localizador n�o confere! [1] Avise a DERMATEK');
    end;
  end;
end;

procedure TFmOSCab2.MostraPreSrv();
const
  SQLType = stIns;
var
  Codigo: Integer;
begin
  Codigo := QrOSCabCodigo.Value;
  //
  if DBCheck.CriaFm(TFmOSSrvPre, FmOSSrvPre, afmoNegarComAviso) then
  begin
    FmOSSrvPre.ImgTipo.SQLType := SQLType;
    FmOSSrvPre.FOSCab          := QrOSCabCodigo.Value;
    FmOSSrvPre.FQrOsSrv        := QrOsSrv;
    FmOSSrvPre.FQrOSCab        := QrOSCab;
    FmOSSrvPre.FQrOSFrmCab     := QrOSFrmCab;
    FmOSSrvPre.FQrOSFrmRec     := QrOSFrmRec;
    FmOSSrvPre.FQrOSMonCab     := QrOSMonCab;
    FmOSSrvPre.FQrOSMonRec     := QrOSMonRec;
    //
    FmOSSrvPre.ShowModal;
    FmOSSrvPre.Destroy;
    //
    /////////// Copiado do FmOSSrv /////////////////////////////////////////////
    PCGeral.ActivePageIndex := 1;
    LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
      QrOSCabCodigo.Value);
    if SQLType = stUpd then
    begin
      if Codigo = QrOSCabCodigo.Value then
        OSsFuturas()
      else
        Geral.MB_Aviso('Localizador n�o confere! [2] Avise a DERMATEK');
    end;
    /////////// FIM Copiado do FmOSSrv /////////////////////////////////////////
  end;
end;

procedure TFmOSCab2.No1Click(Sender: TObject);
begin
  SetaImprimivelItemConversa(False);
end;

procedure TFmOSCab2.Novodilogo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmDiarioAdd2, FmDiarioAdd2, afmoNegarComAviso) then
  begin
    FmDiarioAdd2.ImgTipo.SQLType := stIns;
    FmDiarioAdd2.FGenero := CO_DIARIO_ADD_GENERO_ORD_SRV;
    FmDiarioAdd2.FDepto  := QrOSCabCodigo.Value;
    FmDiarioAdd2.FQry    := QrDiarioAdd;
    //
    FmDiarioAdd2.EdCliInt.ValueVariant := QrOSCabEmpresa.Value;
    FmDiarioAdd2.CBCliInt.KeyValue := QrOSCabEmpresa.Value;
    //
    FmDiarioAdd2.EdEntidade1.ValueVariant := QrOSCabEntidade.Value;
    FmDiarioAdd2.CBEntidade1.KeyValue     := QrOSCabEntidade.Value;
    //
    FmDiarioAdd2.EdTerceiro01.ValueVariant := QrOSCabEntContrat.Value;
    FmDiarioAdd2.CBTerceiro01.KeyValue := QrOSCabEntContrat.Value;
    //
    FmDiarioAdd2.LaEntidade1.Caption := '[Sub]Cliente:';
    FmDiarioAdd2.LaEntidade2.Caption := 'COntratante:';

    FmDiarioAdd2.ShowModal;
    FmDiarioAdd2.Destroy;
    //
    ReopenDiarioAdd(0);
  end;
end;

procedure TFmOSCab2.odaOS1Click(Sender: TObject);
begin
  ExclusaoIncondicional(nivexclOSCabUni);
end;

procedure TFmOSCab2.odaOS2Click(Sender: TObject);
const
  FatID = 0;
  Pergunta = True;
begin
  if OSApp_PF.RatificaConsumoOS_PreAnalise(nrcosLocalizador, QrOSCabCodigo.Value,
  FatID, Pergunta) then
  begin
    if Geral.MB_Pergunta(
    'Confirma a ratifica��o dos consumos ainda n�o ratificados de todas f�rmulas do localizador atual?') =
    ID_YES then
    begin
      RatificaConsumo('osfrmcab', 'osfrmrec', 'Codigo', QrOSCabCodigo.Value, VAR_FATID_4101);
      RatificaConsumo('osmoncab', 'osmonrec', 'Codigo', QrOSCabCodigo.Value, VAR_FATID_4102);
      //
      OSApp_PF.ReopenOSCab(QrOSCab, QrOSCabCodigo.Value);
    end;
  end;
end;

procedure TFmOSCab2.odas1Click(Sender: TObject);
begin
  ExclusaoIncondicional(nivexclcaixasAll);
  //
  //TotalValorOS(QrOSCabCodigo.Value, gboCaixa);
  OSApp_PF.TotalValorOS(QrOSCabCodigo.Value);
  LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
    QrOSCabCodigo.Value);
end;

procedure TFmOSCab2.odasdetodosPIPs1Click(Sender: TObject);
const
  StPipAdPrg = CO_StPipAdPrg_NONE;
var
  Grupo, Codigo, Controle: Integer;
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o de todas pergutas e respostas de todos PIPs?') = ID_YES then
  begin
    Grupo    := QrGruposGrupo.Value;
    Codigo   := QrOSCabCodigo.Value;
    Controle := QrOSPipMonControle.Value;
    ExcluiPerguntasERespostas('Codigo', Codigo);
    //
    if Geral.MB_Pergunta(
    'Deseja reverter o status de perguntas geradas para n�o geradas?') = ID_YES then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
      'StPipAdPrg'], [
      'Codigo'], [
      StPipAdPrg], [
      Codigo], True);
    end;
    LocCod(Grupo, Grupo, FDisposicao, 0, 0, True,
      Codigo);
    OSApp_PF.ReopenOSPipMon(QrOSPipMon, Codigo, Controle);
  end;
end;

procedure TFmOSCab2.odasdoPIPSelecionado1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o de todas pergutas e respostas do PIP selecionado?') = ID_YES then
  begin
    ExcluiPerguntasERespostas('Controle', QrOSPipMonControle.Value);
    OSApp_PF.ReopenOSPipMon(QrOSPipMon, QrOSCabCodigo.Value, QrOSPipMonControle.Value);
  end;
end;

procedure TFmOSCab2.odasformulaesdesteservio1Click(Sender: TObject);
begin
  ExcluiOSMonCab(istTodos);
end;

procedure TFmOSCab2.odos1Click(Sender: TObject);
begin
(* Primeiros excluir perguntas e suas respostas!
  ExclusaoIncondicional(nivexclMonitsAll);
*)
end;

procedure TFmOSCab2.odos2Click(Sender: TObject);
begin
  ExcluiOSFrmCab(istTodos);
end;

procedure TFmOSCab2.odosservios1Click(Sender: TObject);
begin
  ExclusaoIncondicional(nivexclDedetsAll);
  //TotalValorOS(QrOSCabCodigo.Value, gboServi);
  OSApp_PF.TotalValorOS(QrOSCabCodigo.Value);
  LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
    QrOSCabCodigo.Value);
end;

procedure TFmOSCab2.Oramento1Click(Sender: TObject);
begin
  MostraOSImp(3, Null);
end;

procedure TFmOSCab2.OSPrz1_AlteraClick(Sender: TObject);
begin
  //OSApp_PF.MostraFormOSPrz(stUpd, QrOSCab, QrOSPrz);
  OSApp_PF.MostraFormOSPrz(
    stUpd, QrOSCab, QrOSPrz, QrOSCabOrcamTotal.Value, 0, 0);
end;

procedure TFmOSCab2.OSPrz1_ExcluiClick(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da condi��o de pagamento "' +
  QrOSPrzNO_CONDICAO.Value + '"?') =
  ID_YES then DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSPrz, 'OSPrz',
  ['Controle'], ['Controle'], True);
end;

procedure TFmOSCab2.OSPrz1_IncluiClick(Sender: TObject);
begin
  //OSApp_PF.MostraFormOSPrz(stIns, QrOSCab, QrOSPrz);
  OSApp_PF.MostraFormOSPrz(
    stIns, QrOSCab, QrOSPrz, QrOSCabOrcamTotal.Value, 0, 0);
end;

procedure TFmOSCab2.OSsFuturas();
var
  Lugar, Opcao, Codigo, HowGerou: Integer;
begin
  if OSApp_PF.VeSeCriaOSsFuturas(QrOSCabCodigo.Value) then
  begin
    {
    QrOSPos.First;
    while not QrOSPos.Eof do
    begin
      if QrOSPosAplicacao.Value = CO_OS_POS_APLICACAO_VISI_OS then
      begin
        Lugar  := QrOSCabSiapTerCad.Value;
        Opcao  := QrOSCabOpcao.Value;
        Codigo := 0;
        HowGerou := CO_OS_HOWGEROU_OS_AUTO_PREVENT;
        if DuplicaLocalizadorAtual(QrGruposGrupo.Value, QrOSCabNumero.Value, Lugar,
        Opcao, Codigo, dupAmbos, HowGerou, True) then
        begin
          (*
          ******************* Parei Aqui *******************************************

          Ver com o Eslauco como ir� criar as OSs j� que pode haver servi�os n�o
          realizados! Colocar uma data de finaliza��o? Ou entender que ao finalizar
          a OS o servi�o n�o mais ser� feito?

          **************************************************************************
          *)
          (*
          LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
            Codigo);
            //
          LocalizaOS(Lugar, Opcao, Codigo);
          if OSApp_PF.TemProvidenciaOrfa(QrOSCAbEntidade.Value) then
            OSApp_PF.MostraFormOSPrvSel(stIns, Codigo, nil, QrOSCAbEntidade.Value);
          *)
        end;
        //
      end;
      QrOSPos.Next;
    }
  end
end;

function TFmOSCab2.ImpedePelaDataFinalDeExecucao(DtaExeFim: TDateTime): Boolean;
  procedure MostraAviso1();
  begin
    Geral.MB_Aviso('Condi��o de pagamento n�o definida!');
  end;
  procedure MostraAviso2();
  begin
    Geral.MB_Aviso('Data final de execu��o inv�lida!' + sLineBreak +
      'Data final de execu��o deve ficar em branco!' + sLineBreak +
      'Existem ' + Geral.FF0(QrNaoRatif.RecordCount) +
      ' itens de consumo n�o ratificados!');
  end;
var
  OSCab: String;
  Condicao, Erros: Integer;
begin
  if DtaExeFim > 2 then
  begin
    Erros := 0;
    OSCab := Geral.FF0(QrOSCabCodigo.Value);
    //
    Condicao := DmModOS.CondicaoDePagamentoSelecionada(QrOSCabCodigo.Value);
    if Condicao = 0 then
      Erros := Erros + 1;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrNaoRatif, Dmod.MyDB, [
    'SELECT 1 Tabela, IDIts ',
    'FROM osmonrec ofr ',
    'LEFT JOIN osmoncab ofc ON ofc.Conta=ofr.Conta ',
    'WHERE ofr.Codigo=' + OSCab,
    'AND ofr.RatifUso=0 ',
    'AND ofc.IndicUso=' + Geral.FF0(CO_FRM_INDICUSO_001_COD_UsoDefinitivo),
    ' ',
    'UNION  ',
    ' ',
    'SELECT 2 Tabela, IDIts ',
    'FROM osfrmrec ofr ',
    'LEFT JOIN osfrmcab ofc ON ofc.Conta=ofr.Conta ',
    'WHERE ofr.Codigo=' + OSCab,
    'AND ofr.RatifUso=0 ',
    'AND ofc.IndicUso=' + Geral.FF0(CO_FRM_INDICUSO_001_COD_UsoDefinitivo),
    '']);
    //
    if QrNaoRatif.RecordCount > 0 then
      Erros := Erros + 2;
    //
    Result := Erros > 0;
    //
    case Erros of
      1: MostraAviso1();
      2: MostraAviso2();
      3:
      begin
        MostraAviso1();
        MostraAviso2();
      end;
   end;
  end else
    Result := False;
end;

procedure TFmOSCab2.ImprimeQRCode1Click(Sender: TObject);
begin
  if not DmModOS.ImprimeQRCodePMVs(QrOSMonCab, DBGOSMonCab) then
   // nada;
end;

procedure TFmOSCab2.Inclui1Click(Sender: TObject);
begin
  OSApp_PF.IncluiEPI(QrOSFrmEPI, QrOSFrmCabCodigo.Value, QrOSFrmCabControle.Value,
    QrOSFrmCabConta.Value, 'osfrmepi');
end;

procedure TFmOSCab2.IncluiC3Click(Sender: TObject);
begin
  MostraOSPos(stIns);
end;

procedure TFmOSCab2.Incluievoluodeaplicao1Click(Sender: TObject);
begin
  MostraOSFrmEvo(stIns);
end;

procedure TFmOSCab2.Incluifrmulafilha1Click(Sender: TObject);
begin
  MostraOSFrmFlhCb(stIns);
end;

procedure TFmOSCab2.IncluiIntervalo1Click(Sender: TObject);
begin
  MostraOSFrmFlhDd(stIns);
end;

procedure TFmOSCab2.IncluiIntervalo2Click(Sender: TObject);
begin
  MostraOSMonPipDd(stIns);
end;

procedure TFmOSCab2.Incluiitemdeescalonamento1Click(Sender: TObject);
begin
  OSApp_PF.MostraFormOSCabXtr(stIns, QrOSCab, QrOSCabXtr);
end;

procedure TFmOSCab2.Incluiitens1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOSChk, FmOSChk, afmoNegarComAviso) then
  begin
    FmOSChk.FQrOSChk := QrOSChk;
    FmOSChk.FOSCab   := QrOSCabCodigo.Value;
    FmOSChk.ShowModal;
    FmOSChk.Destroy;
  end;
end;

procedure TFmOSCab2.Incluimonitoramentoextra1Click(Sender: TObject);
begin
  MostraOSExtMon(stIns);
end;

procedure TFmOSCab2.ItsInclui6Click(Sender: TObject);
begin
  MostraOSCxI(stIns);
end;

procedure TFmOSCab2.IncluiOSAge1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'XXX-XXXXX-004 :: Sele��o de Agentes';
  Prompt = 'Seleciones os agentes operacionais:';
  //Campo  = 'Descricao';
  //
  DestTab = 'osage';
  DestMaster = 'Codigo';
  DestDetail = 'Controle';
  DestSelec1 = 'Agente';
  SorcField  = 'Agente';
  ExcluiAnteriores = True;
var
  OSCab, ValrMaster: Integer;
begin
  ValrMaster := QrOSCabCodigo.Value;
  OSCab      := QrOSCabCodigo.Value;
  //
  if DBCheck.EscolheCodigosMultiplos_A(
    Aviso, Titulo, Prompt, nil, 'Ativo', 'Nivel1', 'Nome', [
    'DELETE FROM _selcods_; ',
    'INSERT INTO _selcods_ ',
    'SELECT Codigo Nivel1, 0 Nivel2, ',
    '0 Nivel3, 0 Nivel4, 0 Nivel5, Nome, 0 Ativo ',
    'FROM ' + TMeuDB + '.entidades',
    'WHERE Nome <> "" ',
    'AND ' + VAR_FP_FUNCION,
    'AND Ativo = 1 ',
    ''],[
    'SELECT * FROM _selcods_ ',
    'ORDER BY Nome;',
    ''], DestTab, [DestMaster], DestDetail, DestSelec1, [ValrMaster],
    QrOSAge, SorcField, ExcluiAnteriores, Dmod.QrUpd) then
  begin
    DmModOS.MD5_AtualizaCheckSumOSAge(QrOSCabCodigo.Value);
    OSApp_PF.ReopenOSCab(QrOSCab, OSCab);
    OSApp_PF.ReopenOSAge(QrOSAge, QrOSCabCodigo.Value, 0);
    //
    VerificaRespons;
  end;
end;

procedure TFmOSCab2.IncluiOSCabAlv1Click(Sender: TObject);
begin
  OSApp_PF.InsAltOSCabAlv(stIns, QrOSCabAlv, Dmod.QrUpd, QrOSCabCodigo.Value);
end;

procedure TFmOSCab2.Incluiporprcadastro1Click(Sender: TObject);
const
  Aviso   = '...';
  Titulo  = 'XXX-XXXXX-002 :: Inclui v�rios p�s venda';
  Prompt  = 'Informe o grupo de p�s venda:';
  Campo   = 'Descricao';
var
  Res: Variant;
  Codigo, Controle, Dias, Aplicacao, AplicID: Integer;
  Qry: TmySQLQuery;
begin
  Controle := 0;
  Res := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil, nil, Campo, 0, [
  'SELECT Codigo Codigo, Nome ' + Campo,
  'FROM posvdacab ',
  'WHERE Codigo > 0 ',
  'ORDER BY ' + Campo,
  ''], Dmod.MyDB, False);
  if (Res <> Null) and (Res <> 0) then
  begin
    Screen.Cursor := crHourGlass;
    try
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Incluindo itens de p�s venda');
      Qry := TmySQLQuery.Create(Dmod);
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT * ',
      'FROM posvdaits ',
      'WHERE Codigo=' + Geral.FF0(Res),
      'ORDER BY Dias ',
      '']);
      if Qry.RecordCount = 0 then
      begin
        Geral.MB_AViso('O cadastro selecionado n�o possui nenhum item!!');
        Exit;
      end;
      Codigo := QrOSCabCodigo.Value;
      Qry.First;
      while not Qry.Eof do
      begin
        Dias      := Qry.FieldByName('Dias').AsInteger;
        Aplicacao := Qry.FieldByName('Aplicacao').AsInteger;
        AplicID   := Qry.FieldByName('AplicID').AsInteger;
        //
        Controle := UMyMod.BPGS1I32('ospos', 'Controle', '', '',
          tsDef, stIns, Controle);
        //
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ospos', False, [
        'Codigo', 'Dias', 'Aplicacao',
        'AplicID'], [
        'Controle'], [
        Codigo, Dias, Aplicacao,
        AplicID], [
        Controle], True) then
        //
        Qry.Next;
      end;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      OSApp_PF.ReopenOSPos(QrOSPos, QrOSCabCodigo.Value, 0);
    finally
      Screen.Cursor := crDefault;
    end;  
  end;
end;

procedure TFmOSCab2.PMOSCabPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOSCab);
  MyObjects.HabilitaMenuItemCabUpd(Alteraproformadolocalizador1, QrOSCab);
  //
  MyObjects.HabilitaMenuItemCabUpd(CabExclui1, QrOSCab);
  MyObjects.HabilitaMenuItemCabUpd(Duplica1, QrOSCab);
  MyObjects.HabilitaMenuItemCabUpd(ReplicaLugarOpo1, QrOSCab);
  MyObjects.HabilitaMenuItemCabUpd(Adiodenovooramento1, QrOSCab);
  MyObjects.HabilitaMenuItemCabUpd(Agentes1, QrOSCab);
  MyObjects.HabilitaMenuItemCabUpd(UploadNuvem1, QrOSCab);
  //
  Enab := (QrOSCab.State <> dsInactive) and (QrOSCab.RecordCount > 0);
  //
  Encerra1.Enabled := Enab;
  //
  // Gambiarra para n�o criar outra procedure!
  MyObjects.HabilitaMenuItemCabDel(Incluiporprcadastro1, QrOSCab, QrOSPos);
  //
  (*
  Permitir o altera para caso precise reabrir
  if CabAltera1.Enabled = True then
    CabAltera1.Enabled := NotEncerr;
  *)
end;

procedure TFmOSCab2.Parecerdoreponsveltcnico1Click(Sender: TObject);
begin
  RemoveOSLaudo('LauParTec');
end;

procedure TFmOSCab2.PIP1Click(Sender: TObject);
var
  Conta: Integer;
begin
  Conta := QrOSMonCabConta.Value;
  //
  FmPrincipal.MostraFormPipRapido(QrOSMonCabPipCad.Value, stUpd,
    (*QrOSCabSiapTerCad.Value,*) QrOSCabEntidade.Value, 0, False);
  //
  OSApp_PF.ReopenOSMonCab(QrOSMonCab, QrOSSrvControle.Value, Conta);
end;

procedure TFmOSCab2.PMFatPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := not ((QrLctFatRef.State <> dsInactive) and (QrLctFatRef.RecordCount > 0));
  Enab2 := (QrOSCab.State <> dsInactive) and (QrOSCab.RecordCount > 0);
  Enab3 := (QrOSPrz.State <> dsInactive) and (QrOSPrz.RecordCount > 0);

  Fatura1.Enabled            := Enab2 and Enab and Enab3;
  Excluifaturamento1.Enabled := Enab2 and (not Enab);

  Enab := (QrLctFatRef.State <> dsInactive) and (QrLctFatRef.RecordCount > 0);

  Gerabloqueto1.Enabled        := Enab and (not UBloquetos.VerificaSeBoletoExiste(QrLctFatRefLancto.Value));
  Visualizarbloquetos1.Enabled := Enab;

  MyObjects.HabilitaMenuItemItsIns(Adicionanovacondiodepagamento1, QrOSCab);
  MyObjects.HabilitaMenuItemItsUpd(Editaacondiodepagamentoatual1, QrOSPrz);
  MyObjects.HabilitaMenuItemItsDel(Removecondiaodepagamentoatual1, QrOSPrz);
end;

procedure TFmOSCab2.PMLaudoDelPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrOSCab.State <> dsInactive) and (QrOSCab.RecordCount > 0);
  //
  reainterna1.Enabled               := Enab and (QrOSCabLauInfesInt.Value <> '');
  reaexterna1.Enabled               := Enab and (QrOSCabLauInfesExt.Value <> '');
  Parecerdoreponsveltcnico1.Enabled := Enab and (QrOSCabLauParTec.Value <> '');
end;

procedure TFmOSCab2.PMOSSrvPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOSCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, QrOSSrv);
  (*MyObjects.HabilitaMenuItemCabDelC1I3(ItsExclui1, QrOSSrv,
  QrOSAlv, QrOSFrmCab, QrOSMonCab);*)
  MyObjects.HabilitaMenuItemItsDel(ItsExclui1, QrOSSrv);
  //
  MyObjects.HabilitaMenuItemItsDel(Recalcula1, QrOSSrv);
end;

function TFmOSCab2.PodeResponderPerguntas(): Boolean;
  function FaltaPreQtd(): Boolean;
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrPreQtd, Dmod.MyDB, [
    'SELECT opi.Codigo, opi.Conta, opi.Filiacao, ',
    'opm.PipCad, pip.Nome NO_PIP, ',
    'pcp.Nome NO_Pergunta, opi.RespQtd, opi.PrgLstIts ',
    'FROM ospipits opi ',
    'LEFT JOIN ospipmon  opm ON opm.Controle=opi.Controle ',
    'LEFT JOIN pipcad    pip ON pip.Codigo=opm.PipCad  ',
    'LEFT JOIN prgcadprg pcp ON pcp.Codigo=opi.Pergunta ',
    'WHERE opi.Codigo=' + Geral.FF0(QrOscabCodigo.Value),
    'AND opi.LupForma=' + Geral.FF0(CO_LupForma_COD_PorLoop),
    'AND opi.LupInfVzs=0 ',
    'ORDER BY NO_PIP, NO_Pergunta ',
    '']);
    Result := QrPreQtd.RecordCount > 0;
  end;
var
  Codigo, Filiacao, PipCad, OSCab, Empresa, LstCusPrd, LCPUsed: Integer;
  Lista, NO_Emp: String;
var
  NO_PIP, NO_Pergunta: String;
  RespQtd, PrgLstIts: Integer;
  Conta: Double;
begin
  Result := False;
  //
  if FaltaPreQtd() then
  begin
    FOPIPI_PreQtd :=
      UnCreateBugs.RecriaTempTableNovo(ntrtt_OPIPI_PreQtd, DmodG.QrUpdPID1, False);
    //
    QrPreQtd.First;
    while not QrPreQtd.Eof do
    begin
      Codigo         := QrPreQtdCodigo.Value;
      Filiacao       := QrPreQtdFiliacao.Value;
      PipCad         := QrPreQtdPipCad.Value;
      Conta          := QrPreQtdConta.Value;
      NO_PIP         := QrPreQtdNO_PIP.Value;
      NO_Pergunta    := QrPreQtdNO_Pergunta.Value;
      RespQtd        := 0;
      PrgLstIts      := QrPreQtdPrgLstIts.VAlue;

      //
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, FOPIPI_PreQtd, False, [
      'Codigo', 'Filiacao', 'PipCad',
      'Conta', 'NO_PIP', 'NO_Pergunta',
      'RespQtd', 'PrgLstIts'], [
      ], [
      Codigo, Filiacao, PipCad,
      Conta, NO_PIP, NO_Pergunta,
      RespQtd, PrgLstIts], [
      ], False);
      //
      QrPreQtd.Next;
    end;
    //
    if DBCheck.CriaFm(TFmOPIPI_PreQtd, FmOPIPI_PreQtd, afmoNegarComAviso) then
    begin
      FmOPIPI_PreQtd.ShowModal;
      FmOPIPI_PreQtd.Destroy;
    end;
  end;
  if FaltaPreQtd() then
    Exit;
  //
  OsCab := QrOsCabCodigo.Value;
  Empresa := DModG.ObtemEntidadeDeFilial(QrOsCabEmpresa.Value);
  //
  LstCusPrd := QrOSCabLstCusPrd.Value;
  if LstCusPrd = 0 then
      LstCusPrd := Dmod.QrOpcoesBugsLstCusPrd.Value;
  if LstCusPrd = 0 then
  begin
    Geral.MB_Aviso(
    'Lista de custos n�o definida para o lugar nem nas opc��es espec�ficas!');
    Exit;
  end else
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrSemCus, Dmod.MyDB, [
    'SELECT DISTINCT opi.GraGruX, ggv.Controle, gg1.Nome  ',
    'FROM ospipits opi   ',
    'LEFT JOIN gragruval ggv ON ggv.GraGruX=opi.GraGruX  ',
    '  AND ggv.GraCusPrc=' + Geral.FF0(LstCusPrd),
    '  AND Entidade=' + Geral.FF0(Empresa),
    'LEFT JOIN gragrux ggx ON ggx.Controle=opi.GraGruX  ',
    'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1  ',
    'WHERE opi.GraGruX <> 0  ',
    'AND opi.Codigo=' + Geral.FF0(OSCab),
    'AND opi.Respondido=0   ',
    'AND ggv.Controle IS NULL  ',
    'ORDER BY Nome  ',
    '']);
    //
    if QrSemCus.RecordCount > 0 then
    begin
      Lista := UnDmkDAC_PF.ObtemNomeDeCodigo(Dmod.MyDB, 'gracusprc', LstCusPrd);
      NO_Emp := UnDmkDAC_PF.ObtemNomeDeCodigo(Dmod.MyDB, 'entidades', Empresa,
        'Codigo', 'RazaoSocial');
      frxGER_OSERV_001_Err_01.Variables['VARF_EMPRESA'] :=
        QuotedStr(Geral.FF0(Empresa) + ' - ' + NO_Emp);
      frxGER_OSERV_001_Err_01.Variables['VARF_LISTA_VALORES'] :=
        QuotedStr(Geral.FF0(LstCusPrd) + ' - ' + Lista);
      frxGER_OSERV_001_Err_01.Variables['VARF_DATA'] := Date;
      //
      MyObjects.frxDefineDataSets(frxGER_OSERV_001_Err_01, [
      DmodG.frxDsDono,
      frxDsSemCus
      ]);
      MyObjects.frxMostra(frxGER_OSERV_001_Err_01,
        'Produtos sem pre�o de custo definido');
      //
      Exit;
    end;
  end;
  //
  if LstCusPrd <> 0 then
  begin
    Codigo := OSCab;
    LCPUsed := LstCusPrd;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
    'LCPUsed'], ['Codigo'], [LCPUsed], [Codigo], True) then
      Result := True;
  end;
end;

procedure TFmOSCab2.PMOSAgePopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiOSAge1, QrOSCab);
  //MyObjects.HabilitaMenuItemItsUpd(EditaOSAge1, QrOSAge);
  MyObjects.HabilitaMenuItemItsDel(RemoveOSAge1, QrOSAge);
end;

procedure TFmOSCab2.PMOSCabAlvPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiOSCabAlv1, QrOSCab);
  //MyObjects.HabilitaMenuItemItsUpd(AlteraOSCabAlv1, QrOSCabAlv);
  MyObjects.HabilitaMenuItemItsDel(ExcluiOSCabAlv1, QrOSCabAlv);
end;

procedure TFmOSCab2.PMOSCxaPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui2, QrOSCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera2, QrOSCxa);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui2, QrOSCxa);
end;

procedure TFmOSCab2.PMOSCxIPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui6, QrOSCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera6, QrOSCxI);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui6, QrOSCxI);
end;

procedure TFmOSCab2.PMOSExtMonPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluimonitoramentoextra1, QrOSCab);
  MyObjects.HabilitaMenuItemItsUpd(Alteramonitoramentoextra1, QrOSExtMon);
  MyObjects.HabilitaMenuItemItsUpd(Excluimonitoramentoextra1, QrOSExtMon);
  Alteramonitoramentoextra1.Enabled := Alteramonitoramentoextra1.Enabled and
    (QrOSExtMon.recNo = 1);
end;

procedure TFmOSCab2.PMOSFrmCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui4, QrOSSrv);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera4, QrOSFrmCab);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui4, QrOSFrmCab);
  //MyObjects.HabilitaMenuItemCabDelC1I2(ItsExclui4, QrOSFrmCab, QrOSFrmRec, QrOSFrmDep);
  //
  MyObjects.HabilitaMenuItemItsIns(Itens4, QrOSFrmCab);
  //
  MyObjects.HabilitaMenuItemItsIns(Ratificaodeconsumosprevistos1, QrOSFrmRec);
  MyObjects.HabilitaMenuItemItsIns(Alteraodeconsumorealizado1, QrOSFrmRec);
end;

procedure TFmOSCab2.PMOSFrmEPIPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Inclui1, QrOSFrmCab);
  MyObjects.HabilitaMenuItemItsIns(Cadatrodeprodutos1, QrOSFrmCab);
  MyObjects.HabilitaMenuItemItsDel(Exclui1, QrOSFrmEPI);
end;

procedure TFmOSCab2.PMOSFrmEvoPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrOSCab.State <> dsInactive) and (QrOSCab.RecordCount > 0);
  Enab2 := (QrOSFrmEvo.State <> dsInactive) and (QrOSFrmEvo.RecordCount > 0);
  //
  Incluievoluodeaplicao1.Enabled := Enab;
  Alteraevoluodeaplicao1.Enabled := Enab and Enab2;
  Excluievoluodeaplicao1.Enabled := Enab and Enab2;
end;

procedure TFmOSCab2.PMOSFrmFlhCbPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluifrmulafilha1, QrOSFrmCab);
  MyObjects.HabilitaMenuItemItsUpd(Alterafrmulafilha1, QrOSFrmFlhCb);
  MyObjects.HabilitaMenuItemCabDel(Excluifrmulafilha1, QrOSFrmFlhCb, QrOSFrmFlhDd);
  MyObjects.HabilitaMenuItemItsUpd(Alteradatadaltimaemissodefrmulasfilhas1, QrOSFrmFlhCb);
end;

procedure TFmOSCab2.PMOSFrmFlhDdPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluiintervalo1, QrOSFrmFlhCb);
  MyObjects.HabilitaMenuItemItsUpd(Alteraintervalo1, QrOSFrmFlhDd);
  MyObjects.HabilitaMenuItemItsDel(Excluiintervalo1, QrOSFrmFlhDd);
end;

procedure TFmOSCab2.PMOSMonCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui5, QrOSSrv);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera5, QrOSMonCab);
  MyObjects.HabilitaMenuItemItsUpd(Alteraodeconsumorealizado2, QrOSMonRec);
  MyObjects.HabilitaMenuItemItsIns(Cabecalho5, QrOSSrv);

  //MyObjects.HabilitaMenuItemCabDelC1I2(ItsExclui5, QrOSMonCab, QrOSMonRec, _Qr_OS_Mon_Dep_);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui5, QrOSMonCab);
  //
  MyObjects.HabilitaMenuItemItsIns(Itens5, QrOSMonCab);
  //
  PIP1.Enabled := (QrOSMonCab.State <> dsInactive) and
    (QrOSMonCab.RecordCount > 0) and (QrOSMonCabPipCad.Value <> 0);
  //
  MyObjects.HabilitaMenuItemItsUpd(ImprimeQRCode1, QrOSMonCab);
end;

procedure TFmOSCab2.PMOSMonEPIPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(MenuItem2, QrOSMonCab);
  MyObjects.HabilitaMenuItemItsIns(MenuItem5, QrOSMonCab);
  MyObjects.HabilitaMenuItemItsDel(MenuItem3, QrOSMonEPI);
end;

procedure TFmOSCab2.PMOSMonPipDdPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(Incluiintervalo2, QrOSMonCab);
  MyObjects.HabilitaMenuItemItsUpd(Alteraintervalo2, QrOSMonPipDd);
  MyObjects.HabilitaMenuItemItsDel(Excluiintervalo2, QrOSMonPipDd);
end;

procedure TFmOSCab2.PMOSPipMonPopup(Sender: TObject);
begin
  AdicionaPIPsativos1.Enabled := False;
  MyObjects.HabilitaMenuItemCabDel(RemovePIPs1, QrOSPipMon, QrOSPipIts);
  MyObjects.HabilitaMenuItemItsUpd(Gerarperguntas1, QrOSPipMon);
  MyObjects.HabilitaMenuItemItsUpd(Cadastrodeperguntas1, QrOSPipMon);
  //
  Gerarperguntas1.Enabled := Gerarperguntas1.Enabled and
    (QrOsCabStPipAdPrg.Value < 2);
end;

procedure TFmOSCab2.PMOSPrzPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(OSPrz1_Inclui, QrOSCab);
  MyObjects.HabilitaMenuItemItsUpd(OSPrz1_Altera, QrOSPrz);
  MyObjects.HabilitaMenuItemItsDel(OSPrz1_Exclui, QrOSPrz);
end;

procedure TFmOSCab2.Preencherosdadosdoslugares1Click(Sender: TObject);
begin
  MostraOSImp(1, True);
end;

procedure TFmOSCab2.Proforma1Click(Sender: TObject);
{
var
  Lugar, Opcao, Codigo: Integer;
  ReabreOSCab: Boolean;
}
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGruposGrupo.Value, LaRegistro.Caption);
  //
  if (QrLugares.State = dsInactive) or (QrLugares.RecordCount = 0)
  or (QrOpcoes.State = dsInactive) or (Qropcoes.RecordCount = 0) then
      ReopenVariantes(FDisposicao, 0, 0, True, 0);
  //
  if (QrOSCab.State = dsInactive) or (QrOSCabCodigo.Value = 0) then
    VeSeReabreOSCab(FDisposicao, True, 0);
end;

function TFmOSCab2.ProximaOrdemFrmFlhDd(): Integer;
const
  Incremento = 1;
  Base       = 0;
var
  Codigo, Controle, Conta, IDIts: Integer;
begin
  Codigo   := QrOSFrmFlhCbCodigo.Value;
  Controle := QrOSFrmFlhCbControle.Value;
  Conta    := QrOSFrmFlhCbConta.Value;
  IDIts    := QrOSFrmFlhCbIDIts.Value;
  //
  Result := UnDmkDAC_PF.ObtemProximaOrdem(Dmod.MyDB, 'osfrmflhdd', 'Ordem',
            vpLast, dmktfInteger, Incremento, Base, ['Codigo', 'Controle',
            'Conta', 'IDIts'], [Codigo, Controle, Conta, IDIts])
end;

function TFmOSCab2.ProximaOrdemMonPipDd(): Integer;
const
  Incremento = 1;
  Base       = 0;
var
  Codigo, Controle, Conta(*, IDIts*): Integer;
begin
  Codigo   := QrOSMonCabCodigo.Value;
  Controle := QrOSMonCabControle.Value;
  Conta    := QrOSMonCabConta.Value;
  //IDIts    := QrOSMonPipCbIDIts.Value;
  //
  Result := UnDmkDAC_PF.ObtemProximaOrdem(Dmod.MyDB, 'osmonpipdd', 'Ordem',
            vpLast, dmktfInteger, Incremento, Base, ['Codigo', 'Controle',
            'Conta'(*, IDIts*)], [Codigo, Controle, Conta(*IDIts*)])
end;

procedure TFmOSCab2.QrEntidadesBeforeClose(DataSet: TDataSet);
begin
  QrSiapTerCad.Close;
  QrEntiContat.Close;
end;

procedure TFmOSCab2.QrEntiTelCalcFields(DataSet: TDataSet);
begin
  QrEntiTelTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntiTelTelefone.Value);
end;

procedure TFmOSCab2.QrGruposAfterOpen(DataSet: TDataSet);
begin
  QrGruposGrupo.DisplayFormat := FFormatFloat;
end;

procedure TFmOSCab2.QrGruposAfterScroll(DataSet: TDataSet);
begin
  ReopenDiarioAdd(0);
end;

procedure TFmOSCab2.QrGruposBeforeClose(DataSet: TDataSet);
begin
  QrOpcoes.Close;
  QrLugares.Close;
  QrDiarioAdd.Close;
end;

procedure TFmOSCab2.QrLctFatRefCalcFields(DataSet: TDataSet);
begin
  QrLctFatRefPARCELA.Value := QrLctFatRef.RecNo;
end;

procedure TFmOSCab2.QrLugaresAfterScroll(DataSet: TDataSet);
begin
  if FDisposicao = dispPorLugar then
    ReopenOpcoes(QrGruposGrupo.Value, QrLugaresSiapTerCad.Value, 0);
  //
  VeSeReabreOSCab(dispPorLugar, True, 0);
end;

procedure TFmOSCab2.QrLugaresBeforeClose(DataSet: TDataSet);
begin
  FechaOS();
end;

procedure TFmOSCab2.QrOpcoesAfterScroll(DataSet: TDataSet);
begin
  if FDisposicao = dispPorOpcao then
    ReopenLugares(QrGruposGrupo.Value, QrOpcoesOpcao.Value, 0);
  //
  VeSeReabreOSCab(dispPorOpcao, True, 0);
end;

procedure TFmOSCab2.QrOpcoesBeforeClose(DataSet: TDataSet);
begin
  FechaOS();
end;

procedure TFmOSCab2.QrOSCabAfterClose(DataSet: TDataSet);
begin
  MyObjects.Entitula(GBOperacao, [LaOperacaoA, LaOperacaoB, LaOperacaoC],
  [], '', False, taLeftJustify, 2, 10, 20);
end;

procedure TFmOSCab2.QrOSCabAfterScroll(DataSet: TDataSet);
begin
  TSLocalizador.Caption := 'Lclz: ' + Geral.FFN(QrOSCabCodigo.Value, 6);
  RedefinePosicoes();
  //
  PnDedetizacao.Visible    := Geral.IntInConjunto(1, QrOSCabOperacao.Value) or
                              Geral.IntInConjunto(4, QrOSCabOperacao.Value);
  PnCaixaDAgua.Visible     := Geral.IntInConjunto(2, QrOSCabOperacao.Value);
  PnMonitoramento.Visible  := Geral.IntInConjunto(4, QrOSCabOperacao.Value);
  //
  OSApp_PF.ReopenOSAge(QrOSAge, QrOSCabCodigo.Value, 0);
  OSApp_PF.ReopenOSSrv(QrOSSrv, QrOSCabCodigo.Value, 0);
  OSApp_PF.ReopenOSCabAlv(QrOSCabAlv, QrOSCabCodigo.Value, 0);
  OSApp_PF.ReopenOSPrz(QrOSPrz, QrOSCabCodigo.Value, 0);
  OSApp_PF.ReopenOSPos(QrOSPos, QrOSCabCodigo.Value, 0);
  OSApp_PF.ReopenLctFatRef(QrLctFatRef, QrOSCabCodigo.Value, 0);
  OSApp_PF.ReopenEntiMail(QrEntiMail, QrOSCabEntiContat.Value, 0);
  OSApp_PF.ReopenEntiTel(QrEntiTel, QrOSCabEntiContat.Value, 0);
  OSApp_PF.ReopenOSCxa(QrOSCxa, QrOSCabCodigo.Value, 0);
  OSApp_PF.ReopenOSPipMon(QrOSPipMon, QrOSCabCodigo.Value, 0);
  OSApp_PF.ReopenOSCxI(QrOSCxI, QrOSCabCodigo.Value, 0);
  OSApp_PF.ReopenOSChk(QrOSChk, QrOSCabCodigo.Value, 0);
  OSApp_PF.ReopenOSSta(QrOSSta, QrOSCabCodigo.Value, 0);
  OSApp_PF.ReopenOSCabXtr(QrOSCabXtr, QrOSCabCodigo.Value, 0);
  OSApp_PF.ReopenOSPrv(QrOSPrv, QrOSCabCodigo.Value, 0);
  OSApp_PF.ReopenOSExtMon(QrOSExtMon, QrOSCabCodigo.Value, 0);
  ReopenOSPrn(0);
  //
  ConfiguraCorStatus(QrOSCabAgeCorIni.Value, QrOSCabAgeCorFon.Value, TEdit(DBEdit6), TEdit(DBEdit5));
  ConfiguraCorStatus(QrOSCabAgeCorIni.Value, QrOSCabAgeCorFon.Value, TEdit(DBEdit44), TEdit(DBEdit45));
  //
  MyObjects.Entitula(GBOperacao, [LaOperacaoA, LaOperacaoB, LaOperacaoC],
    [], ' ' + QrOSCabNO_OPERACAO.Value, False, taLeftJustify, 2, 10, 20);
end;

procedure TFmOSCab2.QrOSCabBeforeClose(DataSet: TDataSet);
begin
  GuardaPosicoes();
  //
  QrOSSrv.Close;
  QrOSAge.Close;
  QrOSCabAlv.Close;
  QrOSPrz.Close;
  QrOSPos.Close;
  QrLctFatRef.Close;
  QrEntiMail.Close;
  QrEntiTel.Close;
  QrOSCxa.Close;
  QrOSCxI.Close;
  QrOSPipMon.Close;
  QrOSChk.Close;
  QrOSSta.Close;
  QrOSCabXtr.Close;
  QrOSPrv.Close;
  QrOSPrn.Close;
  QrOSExtMon.Close;
  QrOSPrn.Close;
  //
  PnDedetizacao.Visible := False;
  PnCaixaDAgua.Visible := False;
  //
  ConfiguraCorStatus(0, 0, TEdit(DBEdit6), TEdit(DBEdit5));
  //
  TSLocalizador.Caption := 'Localizador';
end;

procedure TFmOSCab2.QrOSCabBeforeOpen(DataSet: TDataSet);
begin
  QrOSCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmOSCab2.QrOSCabBeforeScroll(DataSet: TDataSet);
begin
  GuardaPosicoes();
end;

procedure TFmOSCab2.QrOSCabCalcFields(DataSet: TDataSet);
begin
  QrOSCabTXTContat.Value := Geral.FDT(QrOSCabDtaContat.Value, 0, True);
  //
  QrOSCabTXTVisPrv.Value := Geral.FDT(QrOSCabDtaVisPrv.Value, 0, True);
  QrOSCabTXTVisExe.Value := Geral.FDT(QrOSCabDtaVisExe.Value, 0, True);
  //
  QrOSCabTXTExePrv.Value := Geral.FDT(QrOSCabDtaExePrv.Value, 0, True);
  QrOSCabTXTExeIni.Value := Geral.FDT(QrOSCabDtaExeIni.Value, 0, True);
  QrOSCabTXTExeFim.Value := Geral.FDT(QrOSCabDtaExeFim.Value, 0, True);
  //
  QrOSCabTXTTel_ENT.Value := Geral.FormataTelefone_TT_Curto(QrOSCabTel_ENT.Value);
  //
  QrOSCabNO_OPERACAO.Value := OSApp_PF.NomeDeOperacao(QrOSCabOperacao.Value);
end;

procedure TFmOSCab2.QrOSCabLstUplWebGetText(Sender: TField; var Text: string;
  DisplayText: Boolean);
begin
  Text := Geral.FDT(QrOSCabLstUplWeb.Value, 106);
end;

procedure TFmOSCab2.QrOSCxaAfterScroll(DataSet: TDataSet);
begin
  OSApp_PF.ReopenOSOSCxaAtrib(QrOSCxaAtrib, QrOSCxaCaixa.Value);
end;

procedure TFmOSCab2.QrOSCxaBeforeClose(DataSet: TDataSet);
begin
  QrOSCxaAtrib.Close;
end;

procedure TFmOSCab2.QrOSFrmCabAfterScroll(DataSet: TDataSet);
begin
  OSApp_PF.ReopenOSFrmRec(QrOSFrmRec, QrOSFrmCabConta.Value, 0);
  OSApp_PF.ReopenOSFrmAbr(QrOSFrmAbr, QrOSFrmCabConta.Value, 0);
  OSApp_PF.ReopenOSFrmDep(QrOSFrmDep, QrOSFrmCabConta.Value, 0);
  OSApp_PF.ReopenOSFrmDep(QrOSFrmEvo, QrOSFrmCabConta.Value, 0);
  OSApp_PF.ReopenOSEPI(QrOSFrmEPI, 'osfrmepi', QrOSFrmCabConta.Value, 0);
  OSApp_PF.ReopenOSFrmFlhCb(QrOSFrmFlhCb, QrOSFrmCabConta.Value, 0);
end;

procedure TFmOSCab2.QrOSFrmCabBeforeClose(DataSet: TDataSet);
begin
  QrOSFrmRec.Close;
  QrOSFrmAbr.Close;
  QrOSFrmDep.Close;
  QrOSFrmEvo.Close;
  QrOSFrmEPI.Close;
  QrOSFrmFlhCb.Close;
end;

procedure TFmOSCab2.QrOSFrmCabCalcFields(DataSet: TDataSet);
begin
  if QrOSSrvTUDOFEITO.Value = 1 then
    QrOSFrmCabCALC_FEITO.Value := 100
  else
    QrOSFrmCabCALC_FEITO.Value := QrOSFrmCabPercFeito.Value;
end;

procedure TFmOSCab2.QrOSFrmFlhCbAfterScroll(DataSet: TDataSet);
begin
  OSApp_PF.ReopenOSFrmFlhDd(QrOSFrmFlhDd, QrOSFrmFlhCbIDIts.Value, 0);
end;

procedure TFmOSCab2.QrOSFrmFlhCbBeforeClose(DataSet: TDataSet);
begin
  QrOSFrmFlhDd.Close;
end;

procedure TFmOSCab2.QrOSMonCabAfterScroll(DataSet: TDataSet);
begin
  OSApp_PF.ReopenOSEPI(QrOSMonEPI, 'osmonepi', QrOSMonCabConta.Value, 0);
  OSApp_PF.ReopenOSMonRec(QrOSMonRec, QrOSMonCabConta.Value, 0);
  OSApp_PF.ReopenOSMonPipDd(QrOSMonPipDd, QrOSMonCabConta.Value, 0);
{
  OSApp_PF._Reopen_OS_Mon_Dep(_Qr_OS_Mon_Dep_, QrOSMonCabConta.Value, 0);
}
end;

procedure TFmOSCab2.QrOSMonCabBeforeClose(DataSet: TDataSet);
begin
  QrOSMonEPI.Close;
  QrOSMonRec.Close;
  QrOSMonPipDd.Close;
  //_Qr_OS_Mon_Dep_.Close;
end;

procedure TFmOSCab2.QrOSPipMonAfterScroll(DataSet: TDataSet);
begin
  OSApp_PF.ReopenOSPipIts(QrOSPipIts, QrOSPipMonControle.Value, 0);
end;

procedure TFmOSCab2.QrOSPipMonBeforeClose(DataSet: TDataSet);
begin
  QrOSPipIts.Close;
end;

procedure TFmOSCab2.QrOSPrzCalcFields(DataSet: TDataSet);
var
  Valor, Orcam, Inval, Fator: Double;
  Parcelas: Integer;
begin
  Parcelas := QrOSPrzParcelas.Value;
  Fator := 1 - (QrOSPrzDescoPer.Value / 100);
  //
  Orcam := QrOSCabOrcamServi.Value; // > Valor total do orcamento, mesmo se nao confirmado!
  Inval := QrOSCabInvalServi.Value; // > Valor total nao confirmado para execucao!
  Valor := QrOSCabValorServi.Value; // > Valor total confirmado para execucao!
  //
  QrOSPrzORC_COM_DESCO.Value := Orcam * Fator;
  QrOSPrzINV_COM_DESCO.Value := Inval * Fator;
  QrOSPrzVAL_COM_DESCO.Value := Valor * Fator;
  //
  if Parcelas > 0 then
  begin
    QrOSPrzORC_PARCELA.Value := QrOSPrzORC_COM_DESCO.Value / Parcelas;
    QrOSPrzINV_PARCELA.Value := QrOSPrzINV_COM_DESCO.Value / Parcelas;
    QrOSPrzVAL_PARCELA.Value := QrOSPrzVAL_COM_DESCO.Value / Parcelas;
  end else
  begin
    QrOSPrzORC_PARCELA.Value := 0;
    QrOSPrzINV_PARCELA.Value := 0;
    QrOSPrzVAL_PARCELA.Value := 0;
  end;
end;

procedure TFmOSCab2.QrOSSrvAfterScroll(DataSet: TDataSet);
begin
  OSApp_PF.ReopenOSAlv(QrOSAlv, QrOSSrvControle.Value, 0, Dmod.MyDB);
  OSApp_PF.ReopenOSFrmCab(QrOSFrmCab, QrOSSrvControle.Value, 0);
  OSApp_PF.ReopenOSMonCab(QrOSMonCab, QrOSSrvControle.Value, 0);
end;

procedure TFmOSCab2.QrOSSrvBeforeClose(DataSet: TDataSet);
begin
  QrOSAlv.Close;
  QrOSFrmCab.Close;
  QrOSMonCab.Close;
end;

procedure TFmOSCab2.QrOSSrvCalcFields(DataSet: TDataSet);
var
  Autorizado, TudoFeito: String;
begin
  QrOSSrvVAL_CALCeINFO.Value := QrOSSrvValCalc.Value + QrOSSrvValInfo.Value;
  QrOSSrvAUTORIZADO_BOOL.Value := Geral.IntToBool(QrOSSrvAutorizado.Value);
  //
  if QrOSSrvAUTORIZADO_BOOL.Value then
    Autorizado := 'Autorizado'
  else
    Autorizado := 'N�O Autorizado';
  //
  if QrOSSrvTudoFeito.Value = 1 then
    TudoFeito  := 'Executado'
  else
    TudoFeito  := 'N�O Executado';
  QrOSSrvTITULO_FORMULAS.Value := '   ' + Geral.FF0(QrOSSrvControle.Value) +
    ' - ' + QrOSSrvNO_DesServico.Value + ' ($ ' + Geral.FFT(QrOSSrvValTota.Value,
    2, siPositivo) + ') ' + Autorizado + ' - ' + TudoFeito;
end;

procedure TFmOSCab2.RatificaConsumo(TabMae, Tabela, Campo: String; Indice, FatID: Integer);
var
  GraCusPrc, Empresa: Integer;
begin
  GraCusPrc := QrOSCabLstCusPrd.Value;
  Empresa   := DModG.ObtemEntidadeDeFilial(QrOSCabEmpresa.Value);
  DmModOS.RatificaConsumo(TabMae, Tabela, Campo, Indice, GraCusPrc, Empresa, FatID);
end;

procedure TFmOSCab2.Ratificaconsumosprevistos1Click(Sender: TObject);
const
  FatID = VAR_FATID_4102;
  Pergunta = True;
begin
  if OSApp_PF.RatificaConsumoOS_PreAnalise(nrcosFormula, QrOSSrvControle.Value,
  FatID, Pergunta) then
  begin
    if Geral.MB_Pergunta(
    'Confirma a ratifica��o dos consumos ainda n�o ratificados desta f�rmula de monitoramento?') =
    ID_YES then
    begin
      RatificaConsumo('osmoncab', 'osmonrec', 'Conta', QrOSMonRecConta.Value, VAR_FATID_4102);
      //
      OSApp_PF.ReopenOSMonRec(QrOSMonRec, QrOSMonCabConta.Value, 0);
    end;
  end;
end;

procedure TFmOSCab2.Ratificaodeconsumosprevistos1Click(Sender: TObject);
const
  FatID = VAR_FATID_4101;
  Pergunta = True;
begin
  if OSApp_PF.RatificaConsumoOS_PreAnalise(nrcosFormula, QrOSSrvControle.Value,
  FatID, Pergunta) then
  begin
    if Geral.MB_Pergunta(
    'Confirma a ratifica��o dos consumos ainda n�o ratificados da f�rmula de aplica��o selecionada?') =
    ID_YES then
    begin
      RatificaConsumo('osfrmcab', 'osfrmrec', 'Conta', QrOSFrmCabConta.Value, VAR_FATID_4101);
      //
      OSApp_PF.ReopenOSFrmCab(QrOSFrmCab, QrOSSrvControle.Value, 0);
    end;
  end;
end;

procedure TFmOSCab2.reaexterna1Click(Sender: TObject);
begin
  RemoveOSLaudo('LauInfesExt');
end;

procedure TFmOSCab2.reainterna1Click(Sender: TObject);
begin
  RemoveOSLaudo('LauInfesInt');
end;

procedure TFmOSCab2.Recalcula1Click(Sender: TObject);
var
  Conta, Controle: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Controle := QrOSSrvControle.Value;
    QrOSFrmCab.First;
    while not QrOSFrmCab.Eof do
    begin
      Conta := QrOSFrmCabConta.Value;
      OSApp_PF.AtualizaPercentualFormulaExecutado(Conta, Controle);
      //
      QrOSFrmCab.Next;
    end;
    //OSApp_PF.AtualizaPercentualServicoExecutado(Controle);
    OSApp_PF.ReopenOSSrv(QrOSSrv, QrOSCabCodigo.Value, Controle);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSCab2.RedefinePosicoes();
var
  Idx: Single;
  Err: Boolean;
begin
  PCGeral.ActivePageIndex := FPos_PCGeral;
  PCOpera.ActivePageIndex := FPos_PCOpera;
  Idx := Power(2, FPos_PCOpera);
{
  case FPos_PCOpera of
    0: Idx := 1;
    1: Idx := 2;
    2: Idx := 4;
  end;
}
  Err := not Geral.IntInConjunto(Trunc(Idx), QrOSCabOperacao.Value);
  if Err then
  begin
    if Geral.IntInConjunto(1, QrOSCabOperacao.Value) then
      PCOpera.ActivePageIndex := 0;
    if Geral.IntInConjunto(2, QrOSCabOperacao.Value) then
      PCOpera.ActivePageIndex := 1;
    if Geral.IntInConjunto(4, QrOSCabOperacao.Value) then
      PCOpera.ActivePageIndex := 2;
  end;
end;

procedure TFmOSCab2.RemoveAgente;
var
  OSCab, Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a retirada do agente selecionado?',
  'osage', 'Controle', QrOSAgeControle.Value, Dmod.MyDB) = ID_YES then
  begin
    OSCab := QrOSCabCodigo.Value;
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOSAge,
    QrOSAgeControle, QrOSAgeControle.Value);
    DmModOS.MD5_AtualizaCheckSumOSAge(QrOSCabCodigo.Value);
    //
    OSApp_PF.ReopenOSCab(QrOSCab, OSCab);
    OSApp_PF.ReopenOSAge(QrOSAge, QrOSCabCodigo.Value, Controle);
  end;
end;

procedure TFmOSCab2.Removeasolicitaesdeprovidnciaselecionada1Click(
  Sender: TObject);
const
  Codigo = 0;
var
  Controle: Integer;
begin
  if Geral.MB_Pergunta(
  'Confirma a retirada da solicita��o de provid�ncia selecionada?') =
  ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOSPrv,
      QrOSPrvControle, QrOSPrvControle.Value);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osprv', False, [
    'Codigo'], [
    'Controle'], [
    Codigo], [
    Controle], True) then
        OSApp_PF.ReopenOSPrv(QrOSPrv, QrOSCabCodigo.Value, Controle);
  end;
end;

procedure TFmOSCab2.Removecondiaodepagamentoatual1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da condi��o de pagamento "' +
    QrOSPrzNO_CONDICAO.Value + '"?') =
    ID_YES then DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSPrz, 'OSPrz',
    ['Controle'], ['Controle'], True);
end;

procedure TFmOSCab2.Removeitens1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrOSChk, DBGOSChk,
  'oschk', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmOSCab2.RemoveOSAge1Click(Sender: TObject);
var
  OSCab, Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a retirada do agente selecionado?',
    'osage', 'Controle', QrOSAgeControle.Value, Dmod.MyDB) = ID_YES then
  begin
    OSCab    := QrOSCabCodigo.Value;
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOSAge, QrOSAgeControle, QrOSAgeControle.Value);
    //
    DmModOS.MD5_AtualizaCheckSumOSAge(QrOSCabCodigo.Value);
    //
    OSApp_PF.ReopenOSCab(QrOSCab, OSCab);
    OSApp_PF.ReopenOSAge(QrOSAge, QrOSCabCodigo.Value, Controle);
    //
    VerificaRespons;
  end;
end;

procedure TFmOSCab2.RemovePIPs1Click(Sender: TObject);
begin
{  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrOSPIPMon, DBGOSChk,
  'ospipmon', ['Controle'], ['Controle'], istPergunta, '');
}end;

procedure TFmOSCab2.ReopenContato();
var
  Ctr, Cli, Pag, STC: Integer;
begin
  Cli := EdEntidade.ValueVariant;
  Ctr := EdEntContrat.ValueVariant;
  Pag := EdEntPagante.ValueVariant;
  STC := EdSiapTerCad.ValueVariant;
  OSApp_PF.ReopenSiapTerCad(QrSiapTerCad, Cli, STC, EdSiapTerCad, CBSiapTerCad);
  OSApp_PF.ReopenEntiContat(QrEntiContat, Cli, Ctr, Pag);
end;

procedure TFmOSCab2.ReopenDiarioAdd(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrDiarioAdd, Dmod.MyDB, [
  'SELECT Codigo, Depto, Data, Hora,  ',
  'Interloctr, PrintOS, ',
  'ELT(PrintOS + 2, " ", "N", "S", "?") IMP, Nome',
  'FROM diarioadd ',
  'WHERE Genero=' + Geral.FF0(CO_DIARIO_ADD_GENERO_ORD_SRV),
  'AND Depto IN ( ',
  '     SELECT Codigo ',
  '     FROM oscab  ',
  '     WHERE Grupo=' + Geral.FF0(QrGruposGrupo.Value) + ') ',
  'ORDER BY Codigo DESC ',
  '']);
  //
  if Codigo <> 0 then
    QrDiarioAdd.Locate('Codigo', Codigo, []);
end;

procedure TFmOSCab2.ReopenLugares(Grupo, Opcao, Lugar: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrLugares, Dmod.MyDB, [
  'SELECT DISTINCT osc.SiapTerCad, stc.Nome ',
  'FROM oscab osc ',
  'LEFT JOIN siaptercad stc ON stc.Codigo=osc.SiapTerCad ',
  'WHERE osc.Grupo=' + Geral.FF0(QrGruposGrupo.Value),
  'AND osc.Opcao=' + Geral.FF0(QrOpcoesOpcao.Value),
  'ORDER BY SiapTerCad ',
  '']);
  //
  QrLugares.Locate('SiapTerCad', Lugar, []);
end;

procedure TFmOSCab2.ReopenOpcoes(Grupo, Lugar, Opcao: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrOpcoes, Dmod.MyDB, [
  'SELECT DISTINCT Opcao ',
  'FROM oscab ',
  'WHERE Grupo=' + Geral.FF0(Grupo),
  'AND SiapTerCad=' + Geral.FF0(Lugar),
  'ORDER BY Opcao ',
  '']);
  //
  QrOpcoes.Locate('Opcao', Opcao, []);
end;

procedure TFmOSCab2.ReopenOSPrn(Controle: Integer);
var
  Impressoes: String;
begin
  Impressoes := dmkPF.ArrayToTexto('prn.Relatorio', 'NO_Relatorio', pvNo, True,
    sListaImpressoesOS);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSPrn, Dmod.MyDB, [
  'SELECT prn.*, ',
  Impressoes,
  'FROM osprn prn ',
  'WHERE prn.Codigo=' + Geral.FF0(QrOSCabCodigo.Value),
  'ORDER BY DtHrIni DESC ',
  '']);
  //
  QrOSPrn.Locate('Controle', Controle, []);
end;

procedure TFmOSCab2.ReopenVariantes(Disposicao: TDisposicao; Lugar,
  Opcao: Integer; ReabreOSCab: Boolean; Codigo: Integer);

  procedure ReabrePorOpcao();
  begin
    DBGOpcao.Align := alLeft;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrOpcoes, Dmod.MyDB, [
    'SELECT DISTINCT Opcao ',
    'FROM oscab ',
    'WHERE Opcao <> 0 ',
    'AND Grupo=' + Geral.FF0(QrGruposGrupo.Value),
    //'AND SiapTerCad=' + Geral.FF0(Lugar),
    'ORDER BY Opcao ',
    '']);
    if Opcao <> 0 then
      QrOpcoes.Locate('Opcao', Opcao, []);
    ReopenLugares(QrGruposGrupo.Value, QrOpcoesOpcao.Value, Lugar);
  end;

  procedure ReabrePorLugar();
  begin
    DBGOpcao.Align := alRight;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrLugares, Dmod.MyDB, [
    'SELECT DISTINCT osc.SiapTerCad, stc.Nome ',
    'FROM oscab osc ',
    'LEFT JOIN siaptercad stc ON stc.Codigo=osc.SiapTerCad ',
    'WHERE osc.SiapTerCad <> 0 ',
    'AND osc.Grupo=' + Geral.FF0(QrGruposGrupo.Value),
    //'AND osc.Opcao=' + Geral.FF0(Lugar),
    'ORDER BY SiapTerCad ',
    '']);
    QrLugares.Locate('SiapTerCad', Lugar, []);
    ReopenOpcoes(QrGruposGrupo.Value, QrLugaresSiapTerCad.Value, Opcao);
  end;

begin
  FNaoReabreVariantes := True;
  try
    QrOpcoes.Close;
    QrLugares.Close;
    //
    FDisposicao := Disposicao;
    if FDisposicao = dispIndefinido then
      FDisposicao := dispPorLugar;
    case FDisposicao of
      //dispIndefinido: ReabrePorLugar();
      dispPorLugar  : ReabrePorLugar();
      dispPorOpcao  : ReabrePorOpcao();
    end;
    //
    FNaoReabreVariantes := False;
    if QrOSCab.State = dsInactive then
      VeSeReabreOSCab(FDisposicao, True, Codigo);
  finally
    FNaoReabreVariantes := False;
  end;
end;

procedure TFmOSCab2.ReplicaLugarOpo1Click(Sender: TObject);
var
  Lugar, Opcao, Codigo, HowGerou: Integer;
begin
  Lugar  := 0;
  Opcao  := 0;
  Codigo := 0;
  HowGerou := CO_OS_HOWGEROU_OS_CLONANDO_OS;
  if DuplicaLocalizadorAtual(QrGruposGrupo.Value, QrOSCabNumero.Value, Lugar,
  Opcao, Codigo, dupAmbos, HowGerou, True) then
  begin
    LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
      Codigo);
      //
    LocalizaOS(Lugar, Opcao, Codigo);
    if OSApp_PF.TemProvidenciaOrfa(QrOSCabEntidade.Value) then
      OSApp_PF.MostraFormOSPrvSel(stIns, Codigo, nil, QrOSCAbEntidade.Value);
  end;
end;

procedure TFmOSCab2.Responderperguntas1Click(Sender: TObject);
var
(*  Codigo, Filiacao, PipCad,*)
  OSCab, Empresa, LstCusPrd: Integer;
(*  Lista, NO_Emp: String;
var
  NO_PIP, NO_Pergunta: String;
  RespQtd, PrgLstIts: Integer;
  Conta: Double;
*)
begin
  if not PodeResponderPerguntas() then
    Exit;
  //
  OsCab := QrOsCabCodigo.Value;
  Empresa := DModG.ObtemEntidadeDeFilial(QrOsCabEmpresa.Value);
  LstCusPrd := QrOSCabLstCusPrd.Value;
  //
  if DBCheck.CriaFm(TFmOSPipIts, FmOSPipIts, afmoNegarComAviso) then
  begin
    FmOSPipIts.ImgTipo.SQLType := stUpd;
    FmOSPipIts.FOSCab := OSCab;
    FmOSPipIts.FEmpresa := Empresa;
    FmOSPipIts.FLstCusPrd := LstCusPrd;
    FmOSPipIts.ReopenOSPipIts(0);
    if QrOSPipIts.RecordCount > 0 then
      FmOSPipIts.ShowModal
    else
      Geral.MB_Aviso('N�o existe pergunta n�o respondida!');
    FmOSPipIts.Destroy;
    //
    OSApp_PF.ReopenOSPipMon(QrOSPipMon, QrOSCabCodigo.Value, 0);
  end;
end;

procedure TFmOSCab2.Retiraagente1Click(Sender: TObject);
begin
  RemoveAgente();
end;

procedure TFmOSCab2.Exclui1Click(Sender: TObject);
begin
  OSApp_PF.ExcluiEPI(QrOSFrmEPI, DBGOSFrmEPI, 'osfrmepi', QrOSFrmCabConta.Value);
end;

procedure TFmOSCab2.ExcluiC3Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do alerta selecionado?',
  'ospos', 'Controle', QrOSPosControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOSPos,
    QrOSPosControle, QrOSPosControle.Value);
    OSApp_PF.ReopenOSPos(QrOSPos, QrOSCabCodigo.Value, Controle);
  end;
end;

procedure TFmOSCab2.Excluievoluodeaplicao1Click(Sender: TObject);
var
  Conta, Controle: Integer;
begin
  if Geral.MB_Pergunta(
    'Confirma a exclus�o do item de evolu��o de aplica��o "' +
    Geral.FF0(QrOSFrmEvoIDIts.Value) + '"?') = ID_YES then
  begin
    Conta    := QrOSFrmEvoConta.Value;
    Controle := QrOSFrmEvoControle.Value;
    if DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSFrmEvo, 'OSFrmEvo',
    ['IDIts'], ['IDIts'], True) then
    //
    OSApp_PF.AtualizaPercentualFormulaExecutado(Conta, Controle);
    OSApp_PF.ReopenOSSrv(QrOSSrv, QrOSCabCodigo.Value, Controle);
    QrOSFrmCab.Locate('Conta', Conta, []);
  end;
end;

procedure TFmOSCab2.Excluifaturamento1Click(Sender: TObject);
var
  Filial, Quitados, FatNum: Integer;
  TabLctA: String;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  FatNum := QrOSCAbCodigo.Value;
  //Filial := DmFatura.ObtemFilialDeEntidade(QrOSCabEmpresa.Value);
  Filial := QrOSCabEmpresa.Value;
  TabLctA  := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  //
  Quitados := DModFin.FaturamentosQuitados(
    TabLctA, VAR_FATID_4001, FatNum);
  //
  if Quitados > 0 then
  begin
    Geral.MensagemBox('Existem ' + Geral.FF0(Quitados) +
    ' itens j� quitados que impedem a exclus�o deste faturamento',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Geral.MensagemBox(
  'Confirma a exclus�o do faturamento e de TODOS seus lan�amentos financeiros atrelados?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    if UBloquetos.DesfazerBoletosFatID(QrLoc, Dmod.MyDB, VAR_FATID_4001,
      FatNum, TabLctA) then
    begin
      Screen.Cursor := crHourGlass;
      try
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
        'DELETE FROM ' + TabLctA,
        'WHERE FatID=' + Geral.FF0(VAR_FATID_4001),
        'AND FatNum=' + Geral.FF0(FatNum),
        '']);
        //
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
        'DELETE FROM lctfatref',
        'WHERE Controle=' + Geral.FF0(FatNum),
        '']);
        //
        (* N�o tem m�ltiplos faturamentos na mesma OS!
        UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
        'DELETE FROM ? ',
        'WHERE Controle=' + Geral.FF0(FatNum),
        '']);
        *)
        //

        OSApp_PF.ReopenLctFatRef(QrLctFatRef, FatNum, 0);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmOSCab2.Excluifrmulafilha1Click(Sender: TObject);
var
  Itens, IDIts: Integer;
  Qry: TmySQLQuery;
begin
(*
////////////////////////////////////////////////////////////////////////////////
Antes de permitir excluir ver se ja nao tem formulas filhas geradas!!!
////////////////////////////////////////////////////////////////////////////////
*)
  Qry := TmySQLQuery.Create(Dmod.MyDB);
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT COUNT(*) ITENS ',
  'FROM osfrmcab ',
  'WHERE OSFFCbMae=' + Geral.FF0(QrOSFrmFlhCbIDIts.Value),
  '']);
  Itens := Qry.FieldByName('ITENS').AsInteger;
  if Itens > 0 then
  begin
    Geral.MB_Aviso(
    'Esta f�rmula filha n�o pode ser exclu�da pois j� possui itens gerados!');
  end else
  begin
    if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
    'osfrmflhcb', 'IDIts', QrOSFrmFlhCbIDIts.Value, Dmod.MyDB) = ID_YES then
    begin
      IDIts := GOTOy.LocalizaPriorNextIntQr(QrOSFrmFlhCb,
        QrOSFrmFlhCbIDIts, QrOSFrmFlhCbIDIts.Value);
      OSApp_PF.ReopenOSFrmFlhCb(QrOSFrmFlhCb, QrOSFrmFlhCbConta.Value, IDIts);
    end;
  end;
end;

procedure TFmOSCab2.ExcluiIntervalo1Click(Sender: TObject);
var
  IDIt2: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'osfrmflhdd', 'IDIt2', QrOSFrmFlhDdIDIt2.Value, Dmod.MyDB) = ID_YES then
  begin
    IDIt2 := GOTOy.LocalizaPriorNextIntQr(QrOSFrmFlhDd,
      QrOSFrmFlhDdIDIt2, QrOSFrmFlhDdIDIt2.Value);
    OSApp_PF.ReopenOSFrmFlhDd(QrOSFrmFlhDd, QrOSFrmFlhCbIDIts.Value, IDIt2);
  end;
end;

procedure TFmOSCab2.ExcluiIntervalo2Click(Sender: TObject);
var
  IDIts: Integer;
begin
  if UMyMod.ExcluiRegistroInt1('Confirma a exclus�o do item selecionado?',
  'osmonpipdd', 'IDIts', QrOSMonPipDdIDIts.Value, Dmod.MyDB) = ID_YES then
  begin
    IDIts := GOTOy.LocalizaPriorNextIntQr(QrOSMonPipDd,
      QrOSMonPipDdIDIts, QrOSMonPipDdIDIts.Value);
    OSApp_PF.ReopenOSMonPipDd(QrOSMonPipDd, QrOSMonCabConta.Value, IDIts);
  end;
end;

procedure TFmOSCab2.Excluiitemdeescalonamento1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do item de escalonamento "' +
  Geral.FF0(QrOSCabXtrControle.Value) + '"?') =
  ID_YES then DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSCabXtr, 'OSCabXtr',
  ['Controle'], ['Controle'], True);
end;

procedure TFmOSCab2.Excluimonitoramentoextra1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do monitoramento extra?' +
  sLineBreak +
  'As OSs Filhas que porventura foram geradas N�O ser�o exclu�das!') =
  ID_YES then DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSExtMon, 'OSExtMon',
  ['Controle'], ['Controle'], True);
  //
  OSApp_PF.AtualizaExtenDdOS(QrOSCabCodigo.Value, False);
  LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
    QrOSCabCodigo.Value);
end;

procedure TFmOSCab2.ItsExclui6Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a retirada da foto da caixa d`�gua " ' +
  QrOSCxIFotoCxa.Value + '"?') =
  ID_YES then DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSCxI, 'OSCxI',
  ['Controle'], ['Controle'], True);
  //
  LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
    QrOSCabCodigo.Value);
end;

procedure TFmOSCab2.ExcluiOSCabAlv1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a retirada da praga alvo selecionada?',
  'oscabalv', 'Controle', QrOSCabAlvControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(QrOSCabAlv,
    QrOSCabAlvControle, QrOSCabAlvControle.Value);
    OSApp_PF.ReopenOSCabAlv(QrOSCabAlv, QrOSCabCodigo.Value, Controle);
  end;
end;

procedure TFmOSCab2.ExcluiOSFrmCab(Quais: TSelType);
var
  Continua: Integer;
  IdxFld, Esk: String;
begin
  case Quais of
    istAtual: IdxFld := 'Conta';
    istTodos: IdxFld := 'Controle';
    else IdxFld := '??fld??';
  end;
  case Quais of
    istAtual: Esk := 'A formula��o de aplica��o ID ' + Geral.FF0(QrOSFrmCabConta.Value);
    istTodos: Esk := 'O servi�o de dedetiza��o ID ' + Geral.FF0(QrOSFrmCabControle.Value);
    else IdxFld := '??fld??';
  end;
  if (QrOSFrmRec.RecordCount > 0) or (QrOSFrmDep.RecordCount > 0) then
  begin
    Continua := Geral.MB_Pergunta(Esk +
    ' possui produtos e comodos/bens que tamb�m dever�o ser exclu�dos!'
    + sLineBreak + 'Deseja continuar assim mesmo?');
    if Continua = ID_YES then
    begin

      // Exclui TODOS registros de dias de intervalo de monitoramento de f�rmulas filhas!
      if (QrOSFrmFlhDd.State <> dsInactive) and (QrOSFrmFlhDd.RecordCount > 0) then
      DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSFrmFlhDd, 'OSFrmFlhDd',
      [IdxFld], [IdxFld], True);

      // Exclui TODOS registros de receitas filhas da mesma receita m�e de dedetiza��o!
      if (QrOSFrmFlhCb.State <> dsInactive) and (QrOSFrmFlhCb.RecordCount > 0) then
      DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSFrmFlhCb, 'OSFrmFlhCb',
      [IdxFld], [IdxFld], True);

      // Exclui TODOS registros de depend�ncias da mesma receita de dedetiza��o!
      if (QrOSFrmDep.State <> dsInactive) and (QrOSFrmDep.RecordCount > 0) then
      DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSFrmDep, 'OSFrmDep',
      [IdxFld], [IdxFld], True);

      // Exclui TODOS registros de ites de receita da mesma receita de dedetiza��o!
      if (QrOSFrmRec.State <> dsInactive) and (QrOSFrmRec.RecordCount > 0) then
      DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSFrmRec, 'OSFrmRec',
      [IdxFld], [IdxFld], True);
    end;
  end else
    Continua := Geral.MB_Pergunta(
    'Confirma a exclus�o da formula��o de dedetiza��o ID ' +
      Geral.FF0(QrOSFrmCabConta.Value) + '?');
  //
  if Continua = ID_YES then
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSFrmCab, 'OSFrmCab',
    [IdxFld], [IdxFld], True);
  //
{
  ******************************************************************************
  ** O Campo no PipCad n�o � OSFrmCab mas sim OSMonCab.                       **
  ******************************************************************************

  // Atualiza PIPs pelo OSFrmCab mesmo se n�o excluir
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE pipcad pip ',
    'LEFT JOIN osfrmcab omc ON omc.Conta=pip.OsFrmCab ',
    'SET pip.OSFrmCab=0 ',
    'WHERE omc.Conta IS NULL  ',
    'AND pip.OSFrmCab <> 0 ',
    '']);


  finally
    Screen.Cursor := crDefault;
  end;
}
end;

procedure TFmOSCab2.ExcluiOSMonCab(Quais: TSelType);
var
  Continua: Integer;
  IdxFld, Esk: String;
begin
  case Quais of
    istAtual: IdxFld := 'Conta';
    istTodos: IdxFld := 'Controle';
    else IdxFld := '??fld??';
  end;
  case Quais of
    istAtual: Esk := 'A formula��o de monitoramento ID ' + Geral.FF0(QrOSMonCabConta.Value);
    istTodos: Esk := 'O servi�o de monitoramento ID ' + Geral.FF0(QrOSMonCabControle.Value);
    else IdxFld := '??fld??';
  end;
  if (QrOSMonRec.RecordCount > 0) (*or (_Qr_OS_Mon_Dep_.RecordCount > 0)*) then
  begin
    Continua := Geral.MB_Pergunta(Esk +
    ' possui produtos e comodos/bens que tamb�m dever�o ser exclu�dos!'
    + sLineBreak + 'Deseja continuar assim mesmo?');
    if Continua = ID_YES then
    begin

{
      // Exclui TODOS registros de depend�ncias da mesma receita de monitoramento!
      if _Qr_OS_Mon_Dep_.RecordCount > 0 then
      DBCheck.ExcluiRegistro(Dmod.QrUpd, _Qr_OS_Mon_Dep_, '_OS_Mon_Dep_',
      [IdxFld], [IdxFld], True);
}
      // Exclui TODOS registros de ites de receita da mesma receita de monitoramento!
      if QrOSMonRec.RecordCount > 0 then
      DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSMonRec, 'OSMonRec',
      [IdxFld], [IdxFld], True);

      // Exclui TODOS registros de intervalos de monitoramentos iniciais de PIPs da mesma receita de monitoramento!
      if QrOSMonPipDd.RecordCount > 0 then
      DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSMonPipDd, 'OSMonPipDd',
      [IdxFld], [IdxFld], True);

    end;
  end else
    Continua := Geral.MB_Pergunta(
    'Confirma a exclus�o da formula��o de monitoramento ID ' +
      Geral.FF0(QrOSMonCabConta.Value) + '?');
  //
  if Continua = ID_YES then
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSMonCab, 'OSMonCab',
    [IdxFld], [IdxFld], True);
  //
  // Atualiza PIPs pelo OSMonCab mesmo se n�o excluir
  Screen.Cursor := crHourGlass;
  try
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'UPDATE pipcad pip ',
    'LEFT JOIN osmoncab omc ON omc.Conta=pip.OsMonCab ',
    'SET pip.OSMonCab=0 ',
    'WHERE omc.Conta IS NULL  ',
    'AND pip.OSMonCab <> 0 ',
    '']);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSCab2.ExcluiPerguntasERespostas(Campo: String; Indice: Int64);
begin
{
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'DELETE FROM ospipitspr ',
  'WHERE ' + Campo + '=' + Geral.FI64(Indice),
  '']);
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'DELETE FROM ospipits ',
  'WHERE ' + Campo + '=' + Geral.FI64(Indice),
  '']);
}
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'INSERT INTO livre2 (Tabela, Campo, Codigo, Vezes, AlterWeb, Ativo) ',
  'SELECT "ospipitspr" Tabela, "IDIts" Campo, ',
  'IDIts Codigo, 1 Vezes, 0 AlterWeb, 0 Ativo ',
  'FROM ospipitspr ',
  'WHERE ' + Campo + '=' + Geral.FI64(Indice),
  ';',
  'DELETE FROM ospipitspr ',
  'WHERE ' + Campo + '=' + Geral.FI64(Indice),
  '']);
  //
  UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
  'INSERT INTO livre2 (Tabela, Campo, Codigo, Vezes, AlterWeb, Ativo) ',
  'SELECT "ospipits" Tabela, "Conta" Campo, ',
  'Conta Codigo, 1 Vezes, 0 AlterWeb, 0 Ativo ',
  'FROM ospipits ',
  'WHERE ' + Campo + '=' + Geral.FI64(Indice),
  ';',
  'DELETE FROM ospipits ',
  'WHERE ' + Campo + '=' + Geral.FI64(Indice),
  '']);
  //
end;

procedure TFmOSCab2.ExclusaoIncondicional(Nivel: TNivelExclusao);
var
  Indice: String;
  Campo: String;
  function StrDel(Tabela: String): String;
  begin
    Result := 'DELETE FROM ' + Lowercase(Tabela) +
    ' WHERE ' + Campo + '=' + Indice + ';';
  end;
var
  SQL: String;
begin
  OSApp_PF.ReopenLctFatRef(QrLctFatRef, QrOSCabCodigo.Value, 0);
  //
  if (QrLctFatRef.RecordCount > 0) and (Nivel <= nivexclOSCabUni) then
  begin
    Geral.MB_Aviso('Exclus�o cancelada! OS j� possui faturamento!');
    Exit;
  end;
  if QrOSCabCodigo.Value <> 0 then
  begin
    if Geral.MB_Pergunta('Tem certeza que deseja excluir tudo do localizador selecionado?') =
    ID_YES then
    begin
      if not DBCheck.LiberaPelaSenhaBoss() then
        Exit;
      //
      SQL := '';
      case Nivel of
        {
        nivexclProforma
        nivexclOSLugar,
        nivexclOSOpcao,
        nivexclOSCabAll,
        }
        nivexclOSCabUni     : Campo := 'Codigo';
        nivexclServicosAll  : Campo := 'Codigo';
        //nivexclServicosUni  : Campo := 'Controle';
        nivexclDedetsAll    : Campo := 'Codigo';
        nivexclDedetsUni    : Campo := 'Controle';
        nivexclCaixasAll    : Campo := 'Codigo';
        nivexclCaixasUni    : Campo := 'Controle';
        nivexclMonitsAll    : Campo := 'Codigo';
        nivexclMonitsUni    : Campo := 'Controle';
        else Campo := '??fld??';
      end;
      case Nivel of
        {
        nivexclProforma
        nivexclOSLugar,
        nivexclOSOpcao,
        nivexclOSCabAll,
        }
        nivexclOSCabUni     : Indice := Geral.FF0(QrOSCabCodigo.Value);
        nivexclServicosAll  : Indice := Geral.FF0(QrOSCabCodigo.Value);
        //nivexclServicosUni  : ???
        nivexclDedetsAll    : Indice := Geral.FF0(QrOSCabCodigo.Value);
        nivexclDedetsUni    : Indice := Geral.FF0(QrOSSrvControle.Value);
        nivexclCaixasAll    : Indice := Geral.FF0(QrOSCabCodigo.Value);
        nivexclCaixasUni    : Indice := Geral.FF0(QrOSCxaControle.Value);
        nivexclMonitsAll    : Indice := Geral.FF0(QrOSCabCodigo.Value);
        nivexclMonitsUni    : Indice := Geral.FF0(QrOSPipMonControle.Value);
        else Indice := '??Valr??';
      end;
      if (Nivel <= nivexclServicosUni) or
      (Nivel in ([nivexclDedetsAll, nivexclDedetsUni] )) then
      begin
        SQL := SQL + Geral.ATS([
        //StrDel('_OS_Mon_Dep_'),
        StrDel('OSFrmFlhCb'),
        StrDel('OSFrmFlhDd'),
        StrDel('OSMonPipDd'),
        StrDel('OSFrmEvo'),
        StrDel('OSFrmDep'),
        StrDel('OSMonRec'),
        StrDel('OSFrmRec'),
        StrDel('OSMonCab'),
        StrDel('OSFrmAbr'),
        StrDel('OSFrmCab'),
        StrDel('OSSrv')
        ]);
      end;
      if (Nivel <= nivexclServicosUni) or
      (Nivel in ([nivexclCaixasAll, nivexclCaixasUni] )) then
      begin
        SQL := SQL + Geral.ATS([
        StrDel('OSCxI'),
        StrDel('OSCxa')
        ]);
      end;
      if (Nivel <= nivexclServicosUni) or
      (Nivel in ([nivexclMonitsAll, nivexclMonitsUni] )) then
      begin
        SQL := SQL + Geral.ATS([
        StrDel('ospipits'),
        StrDel('ospipitspr'),
        StrDel('OSPipMon')
        ]);
      end;
      if Nivel <= nivexclOSCabUni then
      begin
        SQL := SQL + Geral.ATS([
        StrDel('OSChk'),
        StrDel('OSAge'),
        StrDel('OSAlv'),
        StrDel('OSCabAlv'),
        //StrDel(''),
        StrDel('OSPos'),
        StrDel('OSPrz'),
        StrDel('oscabxtr'),
        StrDel('ossta'),
        ///... colocar outros aqui, antes do OSCab!
        StrDel('OSCab')
        ]);
        if QrOSCabCodigo.Value <> 0 then
        begin

          // Zerar codigo:
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osprv', False, [
          'Codigo'], ['Codigo'], [0], [QrOSCabCodigo.Value], True);

          // Zerar Depto quando Genero=CO_DIARIO_ADD_GENERO_ORD_SRV
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'diarioadd', False, [
          'Depto'], ['Depto', 'Genero'], [
          0], [QrOSCabCodigo.Value, CO_DIARIO_ADD_GENERO_ORD_SRV], True);

        end;
      end;
      //Geral.MB_Aviso(SQL);
      if UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      SQL,
      '']) then
      LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
        QrOSCabCodigo.Value);
    end;
  end;
end;

procedure TFmOSCab2.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGruposGrupo.Value, LaRegistro.Caption[2]);
  //
  ReopenVariantes(FDisposicao, 0, 0, True, 0);
end;

procedure TFmOSCab2.VeSeReabreOSCab(Disposicao: TDisposicao; Forca: Boolean;
  Codigo: Integer);
var
  Continua: Boolean;
  OSCab: Integer;
begin
  Continua := (Disposicao = FDisposicao) or Forca;
  if Continua then
  begin
    QrOSCab.Close;
    //Continua := (QrOpcoesOpcao.Value <> 0) and (QrLugaresSiapTerCad.Value <> 0);
    Continua := (FNaoReabreVariantes = False);
    //Continua := True;
    if Continua then
    begin
      if Codigo <> 0 then
        OSCab := Codigo
      else
      begin
        OSApp_PF.ReopenLocOS_Mul(DmModOS.QrLocOS, QrGruposGrupo.Value,
        QrOpcoesOpcao.Value, QrLugaresSiapTerCad.Value);
        OSCab := DmModOS.QrLocOSCodigo.Value;
      end;
      //
      if (QrOSCab.State = dsInactive) or (Codigo <> 0) or
      (DmModOS.QrLocOSCodigo.Value <> QrOSCabCodigo.Value) then
        OSApp_PF.ReopenOSCab(QrOSCab, OSCab);
    end;
  end;
end;

procedure TFmOSCab2.Visualizarbloquetos1Click(Sender: TObject);
begin
  UBloquetos.MostraBloGeren(1, VAR_FATID_4001, 0, QrOSCabCodigo.Value,
    FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOSCab2.DefParams;
begin
  VAR_GOTOTABELA := 'oscab';
  VAR_GOTOMYSQLTABLE := QrGrupos;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := 'Grupo';
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

{

  VAR_SQLx.Add('SELECT eco.Nome NO_ENTICONTAT, cab.*, ');
  VAR_SQLx.Add('fge.Nome NO_FatoGeradr,');
  VAR_SQLx.Add('sta.Nome NO_ESTATUS, stc.Nome NO_SiapTerCad,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Tel_ENT,');
  VAR_SQLx.Add('IF(pag.Tipo=0, pag.RazaoSocial, pag.Nome) NO_PAG,');
  VAR_SQLx.Add('IF(ctr.Tipo=0, ctr.RazaoSocial, ctr.Nome) NO_CTR,');
  VAR_SQLx.Add('car.Nome NO_CART, ppc.Nome NO_PRZ, ');
  VAR_SQLx.Add('tg1.Nome NO_ExeTxtCli1, tg2.Nome NO_ExeTxtCli2 ');
  VAR_SQLx.Add('FROM oscab cab');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade');
  VAR_SQLx.Add('LEFT JOIN entidades pag ON pag.Codigo=cab.EntPagante');
  VAR_SQLx.Add('LEFT JOIN entidades ctr ON ctr.Codigo=cab.EntContrat');
  VAR_SQLx.Add('LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr');
  VAR_SQLx.Add('LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus');
  VAR_SQLx.Add('LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad');
  VAR_SQLx.Add('LEFT JOIN carteiras car ON car.Codigo=cab.CartEmis');
  VAR_SQLx.Add('LEFT JOIN pediprzcab ppc ON ppc.Codigo=cab.CondicaoPg');
  VAR_SQLx.Add('LEFT JOIN enticontat eco ON eco.Controle=cab.EntiContat');
  VAR_SQLx.Add('LEFT JOIN txtgeneric tg1 ON tg1.Codigo=cab.ExeTxtCli1 ');
  VAR_SQLx.Add('LEFT JOIN txtgeneric tg2 ON tg2.Codigo=cab.ExeTxtCli2 ');
  //
  VAR_SQL1.Add('WHERE cab.Codigo=:P0');
  //
  //VAR_SQL2.Add('WHERE cab.CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) LIKE :P0');
  //
}

  VAR_SQLx.Add('SELECT DISTINCT cab.Grupo');
  VAR_SQLx.Add('FROM oscab cab');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade');
  //
  VAR_SQL1.Add('WHERE cab.Grupo=:P0');
  //
  //VAR_SQL2.Add('WHERE cab.CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) LIKE :P0');
  //

end;

procedure TFmOSCab2.Diriodestaempresacomestecliente1Click(Sender: TObject);
var
  Assunto, Empresa, Entidade,
  Terceiro, Interlocutor, Acao, UserGenero, Depto: Integer;
  TemUserGenero, SohInterloctrRelacionado: Boolean; DataIni, DataFim: TDateTime;
  ParteTexto: String;
begin
  Assunto                  := 0;
  Empresa                  := QrOSCabEmpresa.Value;
  Entidade                 := QrOSCabEntidade.Value;
  Terceiro                 := 0;
  Interlocutor             := 0;
  Acao                     := 0;
  UserGenero               := 0;
  Depto                    := 0;
  TemUserGenero            := False;
  SohInterloctrRelacionado := True;
  DataIni                  := 0;
  DataFim                  := 0;
  ParteTexto               := '';
  //
  FmPrincipal.MostraFormDiarioGer2(Assunto, Empresa, Entidade, Terceiro,
    Interlocutor, Acao, UserGenero, Depto, TemUserGenero,
    SohInterloctrRelacionado, DataIni, DataFim, ParteTexto, True);
end;

procedure TFmOSCab2.Duplica1Click(Sender: TObject);
var
  Grupo, Numero, Lugar, Opcao, Codigo, HowGerou: Integer;
begin
  if Geral.MB_Pergunta('Confirma a replica��o de toda proforma atual?') = ID_YES then
  begin;
    try
      UnDmkDAC_PF.AbreMySQLQuery0(QrReplica, Dmod.MyDB, [
      'SELECT  Grupo, Opcao, SiapTerCad, Codigo ',
      'FROM oscab ',
      'WHERE Grupo <> 0  ',
      'AND Grupo=' + Geral.FF0(QrGruposGrupo.Value),
      'ORDER BY Opcao, SiapTerCad ',
      '']);
      //
      Grupo := UMyMod.BPGS1I32('oscab', 'Grupo', '', '', tsDef, stIns, 0);
      Numero := Grupo;
      HowGerou := CO_OS_HOWGEROU_OS_CLONANDO_OS;
      QrReplica.First;
      while not QrReplica.Eof do
      begin
        OSApp_PF.ReopenOSCab(QrOSCab, QrReplicaCodigo.Value);
        //
        Lugar  := QrOSCabSiapTerCad.Value;
        Opcao  := QrOSCabOpcao.Value;
        Codigo := 0;
        //
        if DuplicaLocalizadorAtual(
          Grupo, Numero, Lugar, Opcao, Codigo, dupNenhum,
          HowGerou, False) then ;
        //
        QrReplica.Next;
      end;
      //
      LocCod(Grupo, Grupo, FDisposicao, 0, 0, True, Codigo);
      LocalizaOS(Lugar, Opcao, Codigo);
      //
      if OSApp_PF.TemProvidenciaOrfa(QrOSCabEntidade.Value) then
        OSApp_PF.MostraFormOSPrvSel(stIns, Codigo, nil, QrOSCabEntidade.Value);
    finally
      Screen.Cursor := crDefault;
      PnForm.Enabled := True;
    end;
  end
end;

function TFmOSCab2.DuplicaLocalizadorAtual(const Grupo, Numero: Integer; var
SiapterCad, Opcao, Codigo: Integer; OQueSel: TOSBugsDuplic; HowGerou: Integer;
HabilitaForm: Boolean): Boolean;
const
  PosGerou   = 0;
  OSFlhUltGe = 0;
  OSFlhGrCab = 0;
  OSFlhGrIts = 0;
  SohInicial = 0;
  StPipAdPrg = 0;
  LstUplWeb  = 0;
  UsoQtd     = 0.00;
  UsoPrc     = 0.00;
  UsoVal     = 0.00;
  UsoDec     = 0.00;
  UsoTot     = 0.00;
  UsoCusUni  = 0.00;
  UsoCusTot  = 0.00;
  RatifUso   = 0;
  PercFeito  = 0.00;
  OSFFCbMae  = 0;
  TudoFeitoM = 0;
  TudoFeitoA = 0;
var
  //Texto: String;
  Controle, Conta, IDIts, IDIt2: Integer;
  CopiaDep: Boolean;
  //
  HowEstav, HowOrige: Integer;
  //
begin
  Screen.Cursor := crHourGlass;
  // Evitar interfer�ncia do usu�rio nas grids (tabelas) durante o processo de duplica��o
  PnForm.Enabled := False;
  try
    Result := False;
    if OSApp_PF.StartaInclusaoOS(QrOSCabEntidade.Value, QrGruposGrupo.Value,
    SiapTerCad, Opcao, OQueSel) then
    begin
      CopiaDep := SiapTerCad = QrOSCabSiapTerCad.Value;
      //
      Codigo := UMyMod.BPGS1I32('oscab', 'Codigo', '', '', tsDef, stIns, 0);
      //
      if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'oscab', TMeuDB,
      ['Codigo'], [QrOSCabCodigo.Value],
      ['SiapTerCad', 'Opcao', 'Codigo',
      'Grupo', 'Numero', 'HowGerou',
      'PosGerou', 'OSFlhUltGe', 'OSFlhGrCab',
      'OSFlhGrIts', 'SohInicial', 'StPipAdPrg',
      'LstUplWeb'
      ],
      [SiapTerCad, Opcao, Codigo,
      Grupo, Numero, HowGerou,
      PosGerou, OSFlhUltGe, OSFlhGrCab,
      OSFlhGrIts, SohInicial, StPipAdPrg,
      LstUplWeb],
      '', True, LaAviso1, LaAviso2) then
      begin
        // OSCabAlv
        QrOSCabAlv.First;
        while not QrOSCabAlv.Eof do
        begin
          Controle  := UMyMod.BPGS1I32('oscabalv', 'Controle', '', '', tsDef, stIns, 0);
          //
          UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'oscabalv', TMeuDB,
          ['Controle'], [QrOSCabAlvControle.Value],
          ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
          //
          QrOSCabAlv.Next;
        end;
        // OSAge
        QrOSAge.First;
        while not QrOSAge.Eof do
        begin
          Controle  := UMyMod.BPGS1I32('osage', 'Controle', '', '', tsDef, stIns, 0);
          //
          UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'osage', TMeuDB,
          ['Controle'], [QrOSAgeControle.Value],
          ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
          //
          QrOSAge.Next;
        end;
        // OSChk
        QrOSChk.First;
        while not QrOSChk.Eof do
        begin
          Controle  := UMyMod.BPGS1I32('oschk', 'Controle', '', '', tsDef, stIns, 0);
          //
          UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'oschk', TMeuDB,
          ['Controle'], [QrOSChkControle.Value],
          ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
          //
          QrOSChk.Next;
        end;
        // OSSrv
        QrOSSrv.First;
        while not QrOSSrv.Eof do
        begin
          Controle := UMyMod.BPGS1I32('ossrv', 'Controle', '', '', tsDef, stIns, 0);
          HowOrige := 0;
          if HowGerou = CO_OS_HOWGEROU_OS_AUTO_PREVENT then
          begin
            HowOrige := QrOSSrvControle.Value;
            if (QrOSSrvTudoFeitoA.Value = 1)
            or (QrOSSrvTudoFeitoM.Value = 1) then
              HowEstav := CO_SRV_HOW_ESTAVA_SIM_EXEC (*2*)
            else
              HowEstav := CO_SRV_HOW_ESTAVA_NAO_EXEC (*1*);
          end else
            HowEstav := CO_SRV_HOW_ESTAVA_NAO_INFO(*0*);
          //
          if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'ossrv', TMeuDB,
          ['Controle'], [QrOSSrvControle.Value],
          ['Codigo', 'Controle', 'HowEstav',
          'HowOrige', 'TudoFeitoM', 'TudoFeitoA'],
          [Codigo, Controle, HowEstav,
          HowOrige, TudoFeitoM, TudoFeitoA],
          '', True, LaAviso1, LaAviso2) then
          begin
            // OSAlv
            QrOSAlv.First;
            while not QrOSAlv.Eof do
            begin
              Conta := UMyMod.BPGS1I32_Reaproveita('osalv', 'Conta', '', '', tsDef, stIns, 0);
              UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'osalv', TMeuDB,
              ['Conta'], [QrOSAlvConta.Value],
              ['Codigo', 'Controle', 'Conta'],
              [Codigo, Controle, Conta],
              '', True, LaAviso1, LaAviso2);
              //
              QrOSAlv.Next;
            end;
            // OSFrmCab
            QrOSFrmCab.First;
            while not QrOSFrmCab.Eof do
            begin
              Conta := UMyMod.BPGS1I32('osfrmcab', 'Conta', '', '', tsDef, stIns, 0);
              if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'osfrmcab', TMeuDB,
              ['Conta'], [
              QrOSFrmCabConta.Value],
              ['Codigo', 'Controle', 'Conta',
              'PercFeito', 'OSFFCbMae'], [
              Codigo, Controle, Conta,
              PercFeito, OSFFCbMae],
              '', True, LaAviso1, LaAviso2) then
              begin
                // OSFrmRec
                QrOSFrmRec.First;
                //
                while not QrOSFrmRec.Eof do
                begin
                  IDIts := UMyMod.BPGS1I32('osfrmrec', 'IDIts', '', '', tsDef, stIns, 0);
                  UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'osfrmrec', TMeuDB,
                  ['IDIts'], [QrOSFrmRecIDIts.Value],
                  ['Codigo', 'Controle', 'Conta',
                  'IDIts', 'UsoQtd', 'UsoPrc',
                  'UsoVal', 'UsoDec', 'UsoTot',
                  'UsoCusUni', 'UsoCusTot',
                  'RatifUso'
(* OS Dmk 2577 : Quantidade prevista (PrvQtd) n�o duplica? N�o vi nada de errado!
PrvQtd, PrvPrc, PrvVal, > Duplicando pois n�o est�o na rela��o de exclus�o!
Ordem, Reordem, EhDiluente, NumLote, NumLaudo], [*)
                  ],
                  [Codigo, Controle, Conta,
                  IDIts, UsoQtd, UsoPrc,
                  UsoVal, UsoDec, UsoTot,
                  UsoCusUni, UsoCusTot,
                  RatifUso

                  ],
                  '', True, LaAviso1, LaAviso2);
                  //
                  QrOSFrmRec.Next;
                end;
                // OSFrmDep
                if CopiaDep then
                begin
                QrOSFrmDep.First;
                  while not QrOSFrmDep.Eof do
                  begin
                    IDIts := UMyMod.BPGS1I32('osfrmdep', 'IDIts', '', '', tsDef, stIns, 0);
                    UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'osfrmdep', TMeuDB,
                    ['IDIts'], [QrOSFrmDepIDIts.Value],
                    ['Codigo', 'Controle', 'Conta', 'IDIts'],
                    [Codigo, Controle, Conta, IDIts],
                    '', True, LaAviso1, LaAviso2);
                    //
                    QrOSFrmDep.Next;
                  end;
                end;
                // OSFrmAbr
                QrOSFrmAbr.First;
                while not QrOSFrmAbr.Eof do
                begin
                  IDIts := UMyMod.BPGS1I32_Reaproveita('osfrmabr', 'IDIts', '', '', tsDef, stIns, 0);
                  UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'osfrmabr', TMeuDB,
                  ['IDIts'], [QrOSFrmAbrIDIts.Value],
                  ['Codigo', 'Controle', 'Conta', 'IDIts'],
                  [Codigo, Controle, Conta, IDIts],
                  '', True, LaAviso1, LaAviso2);
                  //
                  QrOSFrmAbr.Next;
                end;
                // OSFrmFlhCb
                QrOSFrmFlhCb.First;
                while not QrOSFrmFlhCb.Eof do
                begin
                  IDIts := UMyMod.BPGS1I32('osfrmflhcb', 'IDIts', '', '', tsDef, stIns, 0);
                  UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'osfrmflhcb', TMeuDB,
                  ['IDIts'], [QrOSFrmFlhCbIDIts.Value],
                  ['Codigo', 'Controle', 'Conta', 'IDIts'],
                  [Codigo, Controle, Conta, IDIts],
                  '', True, LaAviso1, LaAviso2);
                  //
                  QrOSFrmFlhDd.First;
                  while not QrOSFrmFlhDd.Eof do
                  begin
                    IDIt2 := UMyMod.BPGS1I32('osfrmflhdd', 'IDIt2', '', '', tsDef, stIns, 0);
                    UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'osfrmflhdd', TMeuDB,
                    ['IDIt2'], [QrOSFrmFlhDdIDIt2.Value],
                    ['Codigo', 'Controle', 'Conta', 'IDIts', 'IDIt2'],
                    [Codigo, Controle, Conta, IDIts, IDIt2],
                    '', True, LaAviso1, LaAviso2);
                    //
                    QrOSFrmFlhDd.Next;
                  end;
                  //
                  QrOSFrmFlhCb.Next;
                end;
              end;
              //
              QrOSFrmCab.Next;
            end;
            // OSMonCab
{
            QrOSMonCab.First;
            while not QrOSMonCab.Eof do
            begin
              Conta := UMyMod.BPGS1I32('osmoncab', 'Conta', '', '', tsDef, stIns, 0);
              if UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'osmoncab', TMeuDB,
              ['Conta'], [QrOSMonCabConta.Value],
              ['Codigo', 'Controle', 'Conta'], [Codigo, Controle, Conta],
              '', True, LaAviso1, LaAviso2) then
              begin
                // OSMonRec
                QrOSMonRec.First;
                while not QrOSMonRec.Eof do
                begin
                  IDIts := UMyMod.BPGS1I32('osmonrec', 'IDIts', '', '', tsDef, stIns, 0);
                  UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'osmonrec', TMeuDB,
                  ['IDIts'], [QrOSMonRecIDIts.Value],
                  ['Codigo', 'Controle', 'Conta', 'IDIts'],
                  [Codigo, Controle, Conta, IDIts],
                  '', True, LaAviso1, LaAviso2);
                  //
                  QrOSMonRec.Next;
                end;
                // _OS_Mon_Dep_
                if CopiaDep then
                begin
                  _Qr_OS_Mon_Dep_.First;
                  while not _Qr_OS_Mon_Dep_.Eof do
                  begin
                    IDIts := UMyMod.BPGS1I32('_OS_Mon_Dep_', 'IDIts', '', '', tsDef, stIns, 0);
                    UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, '_OS_Mon_Dep_', TMeuDB,
                    ['IDIts'], [_Qr_OS_Mon_Dep_IDIts.Value],
                    ['Codigo', 'Controle', 'Conta', 'IDIts'],
                    [Codigo, Controle, Conta, IDIts],
                    '', True, LaAviso1, LaAviso2);
                    //
                    _Qr_OS_Mon_Dep_.Next;
                  end;
                end;
              end;
              //
              QrOSMonCab.Next;
            end;
}
          end;
          //
          QrOSSrv.Next;
        end;
        // OSCxa
        QrOSCxa.First;
        while not QrOSCxa.Eof do
        begin
          Controle  := UMyMod.BPGS1I32('oscxa', 'Controle', '', '', tsDef, stIns, 0);
          //
          UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'oscxa', TMeuDB,
          ['Controle'], [QrOSCxaControle.Value],
          ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
          //
          QrOSCxa.Next;
        end;
        // OSCxI
        QrOSCxI.First;
        while not QrOSCxI.Eof do
        begin
          Controle  := UMyMod.BPGS1I32('oscxi', 'Controle', '', '', tsDef, stIns, 0);
          //
          UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'oscxi', TMeuDB,
          ['Controle'], [QrOSCxIControle.Value],
          ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
          //
          QrOSCxI.Next;
        end;
        // OSPipMon
        QrOSPipMon.First;
        while not QrOSPipMon.Eof do
        begin
          Controle  := UMyMod.BPGS1I32('ospipmon', 'Controle', '', '', tsDef, stIns, 0);
          //
          UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'ospipmon', TMeuDB,
          ['Controle'], [QrOSPipMonControle.Value],
          ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
          //
          QrOSPipMon.Next;
        end;
        // OSPrz
        QrOSPrz.First;
        while not QrOSPrz.Eof do
        begin
          Controle  := UMyMod.BPGS1I32('osprz', 'Controle', '', '', tsDef, stIns, 0);
          //
          UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'osprz', TMeuDB,
          ['Controle'], [QrOSPrzControle.Value],
          ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
          //
          QrOSPrz.Next;
        end;
        // OSPos
        QrOSPos.First;
        while not QrOSPos.Eof do
        begin
          Controle  := UMyMod.BPGS1I32('ospos', 'Controle', '', '', tsDef, stIns, 0);
          //
          UnDmkDAC_PF.InsereRegistrosPorCopia(DModG.QrUpdPID1, 'ospos', TMeuDB,
          ['Controle'], [QrOSPosControle.Value],
          ['Codigo', 'Controle'], [Codigo, Controle], '', True, LaAviso1, LaAviso2);
          //
          QrOSPos.Next;
        end;
      end;
      //
      Result := True;
    end;
    // N�O FAZER!!
    {
    LctFatRef(QrLctFatRef, QrOSCabCodigo.Value, 0);
    EntiMail(QrEntiMail, QrOSCabEntiContat.Value, 0);
    EntiTel(QrEntiTel, QrOSCabEntiContat.Value, 0);
    }
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    PnForm.Enabled := HabilitaForm;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOSCab2.EdDtaExeIniExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns)
  and (EdDtaExeFim.Text = '00:00')
  and (TPDtaExeIni.Date > 2) then
    EdDtaExeFim.ValueVariant := Geral.STT(EdDtaExeIni.Text, '01:00:00', False);
end;

procedure TFmOSCab2.EdDtaExePrvExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns)
  and (EdFimExePrv.Text = '00:00')
  and (TPDtaExePrv.Date > 2) then
    EdFimExePrv.ValueVariant := Geral.STT(EdDtaExePrv.Text, '01:00:00', False);
end;

procedure TFmOSCab2.EdDtaVisExeExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns)
  and (EdFimVisExe.Text = '00:00')
  and (TPDtaVisExe.Date > 2) then
    EdFimVisExe.ValueVariant := Geral.STT(EdDtaVisExe.Text, '01:00:00', False);
end;

procedure TFmOSCab2.EdDtaVisPrvExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns)
  and (EdFimVisPrv.Text = '00:00')
  and (TPDtaVisPrv.Date > 2) then
    EdFimVisPrv.ValueVariant := Geral.STT(EdDtaVisPrv.Text, '01:00:00', False);
end;

procedure TFmOSCab2.EdEntContratChange(Sender: TObject);
begin
  ReopenContato();
end;

procedure TFmOSCab2.EdEntContratKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  OSAll_PF.CopiaCliente(Key, EdEntidade, EdEntContrat, CBEntContrat);
end;

procedure TFmOSCab2.EdEntidadeChange(Sender: TObject);
begin
  ReopenContato();
end;

procedure TFmOSCab2.EdEntidadeEnter(Sender: TObject);
begin
  try
    if EdEmpresa.ValueVariant = 0 then
      EdEmpresa.SetFocus;
  except
    ;
  end;
end;

procedure TFmOSCab2.EdEntPaganteChange(Sender: TObject);
begin
  ReopenContato();
end;

procedure TFmOSCab2.EdEntPaganteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  OSAll_PF.CopiaCliente(Key, EdEntidade, EdEntPagante, CBEntPagante);
end;

procedure TFmOSCab2.EdEstatusChange(Sender: TObject);
var
  Status: Integer;
begin
  Status := EdEstatus.ValueVariant;
  //
  if Status = 0 then
    ConfiguraCorStatus(0, 0, TEdit(EdEstatusCor), nil)
  else
    ConfiguraCorStatus(QrEstatusOSsAgeCorIni.Value, QrEstatusOSsAgeCorFon.Value,
      TEdit(EdEstatusCor), nil);
end;

procedure TFmOSCab2.EdFatoGeradrChange(Sender: TObject);
{
var
  Habilita: Boolean;
}
begin
{ // 2013-06-29 - N�o usa mais.
  Habilita := (EdFatoGeradr.ValueVariant <> 0) and (QrFatoGeradrNeedOS_Ori.Value = 1);
  LaOSOrigem.Enabled := Habilita;
  EdOSOrigem.Enabled := Habilita;
  if not Habilita then
    EdOSOrigem.ValueVariant := 0;
}
end;

procedure TFmOSCab2.Editaacondiodepagamentoatual1Click(Sender: TObject);
begin
  OSApp_PF.MostraFormOSPrz(stUpd, QrOSCab, QrOSPrz, QrOSCabOrcamTotal.Value, 0, 0);
end;

procedure TFmOSCab2.ItsAltera2Click(Sender: TObject);
begin
  MostraOSCxa(stUpd);
end;

procedure TFmOSCab2.EdNumContratKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
{
var
  Entidade: Integer;
begin
  if Key = VK_F4 then
  begin
    if DBCheck.CriaFm(TFmContratPsq, FmContratPsq, afmoNegarComAviso) then
    begin
      Entidade    := DModG.ObtemEntidadeDeFilial(QrOSCabEmpresa.Value);
      //
      FmContratPsq.EdContratada.ValueVariant  := Entidade;
      FmContratPsq.CBContratada.KeyValue      := Entidade;
      (* 2013-06-23
      FmContratPsq.EdContratante.ValueVariant := QrOSCabEntidade.Value;
      FmContratPsq.CBContratante.KeyValue     := QrOSCabEntidade.Value;
      *)
      FmContratPsq.EdContratante.ValueVariant := EdEntidade.ValueVariant;
      FmContratPsq.CBContratante.KeyValue     := EdEntidade.ValueVariant;
      // FIM 2013-06-23
      FmContratPsq.ShowModal;
      if FmContratPsq.FContrato <> 0 then
        EdNumContrat.ValueVariant := FmContratPsq.FContrato;
      FmContratPsq.Destroy;
    end;
  end;
}
var
  Empresa, Contratante, Contrato: Integer;
begin
  if Key = VK_F4 then
  begin
    Empresa     := DModG.ObtemEntidadeDeFilial(QrOSCabEmpresa.Value);
    Contratante := EdEntidade.ValueVariant;
    //
      if ContratUnit.PesquisaNumeroDoContrato(Empresa, Contratante, Contrato) then
    EdNumContrat.ValueVariant := Contrato;
  end;
  if Key = VK_DELETE then
  begin
    EdNumContrat.ValueVariant := 0;
  end;
end;

procedure TFmOSCab2.EdOSOrigemKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
(*
var
  OSOrigem, Empresa, Cliente, Lugar, FatoGeradr: Integer;
*)
begin
////////////////////////////////////////////////////////////////////////////////
///  Movido da tabela OSCAb para OSFrmCab, OSMonCab e OSPipMon (chaves primarias de origem)
///  ///////////////////////////////////////////////////////////////////////////
(*
  if Key = VK_F4 then
  begin
    FatoGeradr := EdFatoGeradr.ValueVariant;
    Empresa    := EdEmpresa.ValueVariant;
    Cliente    := EdEntidade.ValueVariant;
    Lugar      := EdSiapterCad.ValueVariant;
    FatoGeradr := EdFatoGeradr.ValueVariant;
    //
    if OSApp_PF.PesquisaOSOrigem(Empresa, Cliente, Lugar, FatoGeradr, OSOrigem) then
      EdOSOrigem.ValueVariant := OSOrigem;
  end;
*)
end;

procedure TFmOSCab2.EdSiapTerCadEnter(Sender: TObject);
begin
  ReopenContato();
end;

procedure TFmOSCab2.Encerra1Click(Sender: TObject);
var
  Reabre: Boolean;
  Status, Grupo, Codigo: Integer;
  DtaVisPrv, FimVisPrv, DtaExePrv, FimExePrv: TDateTime;
begin
  if (QrOSCab.State <> dsInactive) and (QrOSCab.RecordCount > 0) then
  begin
    Grupo     := QrOSCabGrupo.Value;
    Codigo    := QrOSCabCodigo.Value;
    DtaVisPrv := QrOSCabDtaVisPrv.Value;
    FimVisPrv := QrOSCabFimVisPrv.Value;
    DtaExePrv := QrOSCabDtaExePrv.Value;
    FimExePrv := QrOSCabFimExePrv.Value;
    Status    := QrOSCabEstatus.Value;
    //
    if DBCheck.CriaFm(TFmOSCabEncerr, FmOSCabEncerr, afmoNegarComAviso) then
    begin
      FmOSCabEncerr.FCodigo    := Codigo;
      FmOSCabEncerr.FDtaVisPrv := QrOSCabDtaVisPrv.Value;
      FmOSCabEncerr.FOSCab2    := TFmOSCab2(Self);
      //

      if FmOSCabEncerr.QrEstatusOSs.Locate('Codigo', Status, []) then
      begin
        FmOSCabEncerr.EdEstatus.ValueVariant := Status;
        FmOSCabEncerr.CBEstatus.KeyValue     := Status;
      end;
      //
      if DtaVisPrv > 2 then
      begin
        FmOSCabEncerr.TPDtaVisExe.Date         := DtaVisPrv;
        FmOSCabEncerr.EdDtaVisExe.ValueVariant := DtaVisPrv;
      end;
      if FimVisPrv > 2 then
      begin
        FmOSCabEncerr.TPFimVisExe.Date         := FimVisPrv;
        FmOSCabEncerr.EdFimVisExe.ValueVariant := FimVisPrv;
      end;
      if DtaExePrv > 2 then
      begin
        FmOSCabEncerr.TPDtaExeIni.Date         := DtaExePrv;
        FmOSCabEncerr.EdDtaExeIni.ValueVariant := DtaExePrv;
      end;
      if FimExePrv > 2 then
      begin
        FmOSCabEncerr.TPDtaExeFim.Date         := FimExePrv;
        FmOSCabEncerr.EdDtaExeFim.ValueVariant := FimExePrv;
      end;
      //
      FmOSCabEncerr.ShowModal;
      //
      if FmOSCabEncerr.FCodigo <> 0 then
        Reabre := True      else
        Reabre := False;
      //
      FmOSCabEncerr.Destroy;
      //
      if Reabre then
        LocCod(Grupo, Grupo, FDisposicao, 0, 0, True, Codigo);
    end;
  end;
end;

procedure TFmOSCab2.este1Click(Sender: TObject);
var
(*  Codigo, Filiacao, PipCad,*)
  OSCab, Empresa, LstCusPrd: Integer;
(*  Lista, NO_Emp: String;
var
  NO_PIP, NO_Pergunta: String;
  RespQtd, PrgLstIts: Integer;
  Conta: Double;
*)
begin
  if QrOSPipItsRespondido.Value = 1 then
  begin
    if not PodeResponderPerguntas() then
      Exit;
    //
    OsCab := QrOsCabCodigo.Value;
    Empresa := DModG.ObtemEntidadeDeFilial(QrOsCabEmpresa.Value);
    LstCusPrd := QrOSCabLstCusPrd.Value;
    //
    if DBCheck.CriaFm(TFmOSPipIts, FmOSPipIts, afmoNegarComAviso) then
    begin
      FmOSPipIts.ImgTipo.SQLType := stIns;
      FmOSPipIts.FOSCab := OSCab;
      FmOSPipIts.FEmpresa := Empresa;
      FmOSPipIts.FLstCusPrd := LstCusPrd;
      FmOSPipIts.ReopenOSPipIts(QrOSPipItsConta.Value);
      //FmOSPipIts.FIDIts      := Trunc(QrOSPipItsIDIts.Value);
      //FmOSPipIts.FSMI_IDCtrl := Trunc(QrOSPipItsSMI_IDCtrl.Value);
      //FmOSPipIts.FDataHoraOrig := Trunc(QrOSPipItsSMI_IDCtrl.Value);
      if QrOSPipIts.RecordCount > 0 then
      begin
        FmOSPipIts.ReopenOSPipItsPr(FmOSPipIts.QrAntOPIP);
        if FmOSPipIts.QrAntOPIPSMI_IDCtrl.Value <> 0 then
          FmOSPipIts.ReopenSMIA(FmOSPipIts.QrAntOPIPSMI_IDCtrl.Value);
        //
        FmOSPipIts.ShowModal;
        OSApp_PF.ReopenOSPipIts(QrOSPipIts, QrOSPipMonControle.Value,
          QrOSPipItsConta.Value);
      end else
        Geral.MB_Aviso('Pergunta n�o localizada!');
      FmOSPipIts.Destroy;
    end;
  end else
    Geral.MB_Aviso('Esta pergunta n�o est� respondida!');
end;

procedure TFmOSCab2.Itens4Click(Sender: TObject);
begin
  MostraOSFrmRec();
end;

procedure TFmOSCab2.Itens5Click(Sender: TObject);
begin
  MostraOSMonRec();
end;

procedure TFmOSCab2.ItsAltera1Click(Sender: TObject);
begin
  MostraOSSrv(stUpd);
end;

procedure TFmOSCab2.ItsAltera4Click(Sender: TObject);
begin
  MostraOSFrmCab(stUpd);
end;

procedure TFmOSCab2.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOSCab2.ItsExclui2Click(Sender: TObject);
begin
{
  if Geral.MensagemBox('Confirma a retirada da caixa d`�gua " ' +
  QrOSCxaLocal.Value + '"?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSCxa, 'OSCxa',
  ['Controle'], ['Controle'], True);
}
end;

procedure TFmOSCab2.ItsExclui4Click(Sender: TObject);
begin
{
  if Geral.MensagemBox('Confirma a exclus�o da formula��o de aplica��o "' +
  QrOSFrmCabNome.Value + '"?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSFrmCab, 'OSFrmCab',
  ['Conta'], ['Conta'], True);
}
end;

procedure TFmOSCab2.Dadosdoslugaresembranco1Click(Sender: TObject);
begin
  MostraOSImp(1, False);
end;

procedure TFmOSCab2.DBGLugarMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
{
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMLugar, DBGLugar, X, Y);
object PMLugar: TPopupMenu
  Left = 52
  Top = 224
end
}
end;

procedure TFmOSCab2.DBGOSAgeCellClick(Column: TColumn);
begin
  if Lowercase(Column.FieldName) = Lowercase('Responsa') then
    AtualizaRespons;
end;

procedure TFmOSCab2.DBGOSAgeColEnter(Sender: TObject);
begin
  if DBGOSAge.Columns[THackDBGrid(DBGOSAge).Col -1].FieldName = CO_FldRespo then
    DBGOSAge.Options := DBGOSAge.Options - [dgEditing] else
    DBGOSAge.Options := DBGOSAge.Options + [dgEditing];
end;

procedure TFmOSCab2.DBGOSAgeColExit(Sender: TObject);
begin
  DBGOSAge.Options := DBGOSAge.Options - [dgEditing];
end;

procedure TFmOSCab2.DBGOSAgeDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = CO_FldRespo then
    MeuVCLSkin.DrawGrid(DBGOSAge, Rect, 1, QrOSAgeResponsa.Value);
end;

procedure TFmOSCab2.DBGOSAgeMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSAge, DBGOSAge, X,Y);
  *)
end;

procedure TFmOSCab2.DBGOSAlvMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    BtOSAlvClick(BtOSAlv);
end;

procedure TFmOSCab2.DBGOSCabAlvMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSCabAlv, DBGOSCabAlv, X,Y);
  *)
end;

procedure TFmOSCab2.DBGOSCabXtrMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSCabXtr, DBGOSCabXtr, X, Y);
  *)
end;

procedure TFmOSCab2.DBGOSChkCellClick(Column: TColumn);
var
  Feito, Controle: Integer;
begin
  if (Column.FieldName = 'NO_FEITO') then
  begin
    Controle := QrOSChkControle.Value;
    if QrOSChkFeito.Value = 0 then
      Feito := 1
    else
      Feito := 0;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oschk', False, [
    'Feito'], ['Controle'], [
    Feito], [Controle], True) then
    //
    OSApp_PF.ReopenOSChk(QrOSChk, QrOSCabCodigo.Value, Controle);
  end;
end;

procedure TFmOSCab2.DBGOSChkDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if (Column.FieldName = 'NO_FEITO') then
  begin
    if      QrOSChkFeito.Value = 0 then Cor := clRed
    else if QrOSChkFeito.Value = 1 then Cor := clBlue
    else Cor := clGray;
    MyObjects.DesenhaTextoEmDBGrid(
      TDbGrid(DBGOSChk), Rect, Cor, clWhite,
      Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TFmOSCab2.DBGOSChkMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSChk, DBGOSChk, X, Y);
  *)
end;

procedure TFmOSCab2.DBGOSCxaCellClick(Column: TColumn);
var
  Autorizado, TudoFeito, Controle: Integer;
begin
  if Column.FieldName = sTudoFeito then
  begin
    if QrOSCxaTudoFeito.Value = 0 then
      TudoFeito := 1
    else
      TudoFeito := 0;
    //
    Controle := QrOSCxaControle.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscxa', False, [
    'TudoFeito'], [
    'Controle'], [
    TudoFeito], [
    Controle], True) then
      OSApp_PF.ReopenOSCxa(QrOSCxa, QrOSCabCodigo.Value, QrOSCxaControle.Value);
  end;

  //

  if Uppercase(Column.FieldName) = Uppercase(CO_FldAutrz) then
  begin
    if QrOSCxaAutorizado.Value = 0 then
      Autorizado := 1
    else
      Autorizado := 0;
    //
    Controle := QrOSCxaControle.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscxa', False, [
    'Autorizado'], [
    'Controle'], [
    Autorizado], [
    Controle], True) then
      OSApp_PF.ReopenOSCxa(QrOSCxa, QrOSCabCodigo.Value, QrOSCxaControle.Value);
  end;
end;

procedure TFmOSCab2.DBGOSCxaColEnter(Sender: TObject);
var
  FldName: String;
begin
  FldName := Uppercase(DBGOSCxa.Columns[TStringGrid(DBGOSCxa).Col - 1].FieldName);
  if (FldName = Uppercase(sTudoFeito)) or (FldName = Uppercase(CO_FldAutrz)) then
    DBGOSCxa.Options := DBGOSCxa.Options - [dgEditing]
  else
    DBGOSCxa.Options := DBGOSCxa.Options + [dgEditing]
end;

procedure TFmOSCab2.DBGOSCxaDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Autorizado' then
    //MeuVCLSkin.DrawGrid(DBGOSCxa, Rect, 1, QrOSCxaAutorizado.Value);
    MeuVCLSkin.DrawGrid(TDBGrid(DBGOSCxa), Rect, 1, QrOSCxaAutorizado.Value);
end;

procedure TFmOSCab2.DBGOSCxaMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //if Button = mbRight then
    //MyObjects.MostraPopOnControlXY(PMOSCxa, DBGOSCxa, X, Y);
end;

procedure TFmOSCab2.DBGOSCxIMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //if Button = mbRight then
    //MyObjects.MostraPopOnControlXY(PMOSCxI, DBGOSCxI, X, Y);
end;

procedure TFmOSCab2.DBGOSFrmFlhCbMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //if Button = mbRight then
    //MyObjects.MostraPopOnControlXY(PMOSFrmFlhCb, DBGOSFrmFlhCb, X, Y);
end;

procedure TFmOSCab2.DBGOSFrmDepMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
  begin
    if MyObjects.FIC((QrOSFrmCab.State = dsInactive)
    or (QrOSFrmCab.RecordCount = 0), nil, 'Informe uma aplica��o!') then
      Exit;
    //
    MostraOSFrmDep(stIns);
  end;
end;

procedure TFmOSCab2.DBGOSFrmEvoDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'PercFeito' then
    MeuVCLSkin.DrawGridPercent(DBGOSFrmEvo, Rect, 1, QrOSFrmEvoPercFeito.Value,
    100);
end;

procedure TFmOSCab2.DBGOSFrmEvoMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //if Button = mbRight then
    //MyObjects.MostraPopOnControlXY(PMOSFrmEvo, DBGOSFrmEvo, X, Y);
end;

procedure TFmOSCab2.DBGOSFrmAbrMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //if Button = mbRight then
    //MyObjects.MostraPopOnControlXY(PMOSFrmCab, DBGOSFrmAbr, X, Y);
end;

procedure TFmOSCab2.DBGOSFrmCabCellClick(Column: TColumn);
var
  Autorizado, TudoFeitoM, Controle: Integer;
begin
  if Uppercase(Column.FieldName) = Uppercase(sCalcFeito) then
  begin
    // Fazer o que se eh por percentual? 2015-01-05
  end;
end;

procedure TFmOSCab2.DBGOSFrmCabColEnter(Sender: TObject);
var
  Campo: String;
begin
  Campo := Lowercase(DBGOSFrmCab.Columns[THackDBGrid(DBGOSFrmCab).Col -1].FieldName);
  if Campo = Lowercase('CALC_FEITO') then
    DBGOSFrmCab.Options := DBGOSFrmCab.Options - [dgEditing] else
    DBGOSFrmCab.Options := DBGOSFrmCab.Options + [dgEditing];
end;

procedure TFmOSCab2.DBGOSFrmCabColExit(Sender: TObject);
begin
  DBGOSFrmCab.Options := DBGOSFrmCab.Options - [dgEditing];
end;

procedure TFmOSCab2.DBGOSFrmCabDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Checked: Integer;
begin
  if Column.FieldName = 'CALC_FEITO' then
  begin
  {
      MeuVCLSkin.DrawGridPercent(DBGOSFrmCab, Rect, 1, QrOSFrmCabCALC_FEITO.Value,
      100);
  }
    if QrOSFrmCabCALC_FEITO.Value < 0.01 then
      Checked := 0
    else
    if QrOSFrmCabCALC_FEITO.Value > 99.99 then
      Checked := 2
    else
      Checked := 1;
    MeuVCLSkin.DrawCheck2(DBGOSFrmCab.Canvas, Rect, Checked, dmkrc0a2);
  end;
end;

procedure TFmOSCab2.DBGOSFrmCabMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //if Button = mbRight then
    //MyObjects.MostraPopOnControlXY(PMOSFrmCab, DBGOSFrmCab, X, Y);
end;

procedure TFmOSCab2.DBGOSFrmRecMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //if Button = mbRight then
    //MyObjects.MostraPopOnControlXY(PMOSFrmCab, DBGOSFrmRec, X, Y);
end;

procedure TFmOSCab2.DBGOSMonCabMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //if Button = mbRight then
    //MyObjects.MostraPopOnControlXY(PMOSMonCab, DBGOSMonCab, X, Y);
end;

{
procedure TFmOSCab2._DBG_OS_Mon_Dep_MouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
  begin
    if MyObjects.FIC((QrOSMonCab.State = dsInactive)
    or (QrOSMonCab.RecordCount = 0), nil, 'Informe um monitoramento!') then
      Exit;
    //
    _Mostra_OS_Mon_Dep_stIns);
  end;
end;
}

procedure TFmOSCab2.DBGOSMonRecMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //if Button = mbRight then
    //MyObjects.MostraPopOnControlXY(PMOSMonCab, DBGOSMonRec, X, Y);
end;

procedure TFmOSCab2.DBGOSSrvCellClick(Column: TColumn);
var
  Autorizado, TudoFeitoM, Controle: Integer;
begin
  if Uppercase(Column.FieldName) = Uppercase(sTudoFeito) then
  begin
    //                               0  1
    if Trunc(QrOSSrvTudoFeitoM.Value) <> (QrOSSrvTUDOFEITO.Value) then
      Geral.MB_Aviso(
      'Registros de progresso de aplica��es deste servi�o interferem nesta defini��o!'
      + sLineBreak +
      'Execute o item de servi�o "Recalcula progresso de aplica��es" e tente novamente!'
      + sLineBreak + 'Verifique tamb�m os itens de progresso de aplica��es!')
    else
    begin
      if QrOSSrvTudoFeitoM.Value = 0 then
        TudoFeitoM := 1
      else
        TudoFeitoM := 0;
      //
      Controle := QrOSSrvControle.Value;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ossrv', False, [
      'TudoFeitoM'], [
      'Controle'], [
      TudoFeitoM], [
      Controle], True) then
        OSApp_PF.ReopenOSSrv(QrOSSrv, QrOSCabCodigo.Value, QrOSSrvControle.Value);
    end;
  end;

  //

  if Uppercase(Column.FieldName) = Uppercase(CO_FldAutrz) then
  begin
    if QrOSSrvAutorizado.Value = 0 then
      Autorizado := 1
    else
      Autorizado := 0;
    //
    Controle := QrOSSrvControle.Value;
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ossrv', False, [
    'Autorizado'], [
    'Controle'], [
    Autorizado], [
    Controle], True) then
      OSApp_PF.ReopenOSSrv(QrOSSrv, QrOSCabCodigo.Value, QrOSSrvControle.Value);
  end;
end;

procedure TFmOSCab2.DBGOSSrvColEnter(Sender: TObject);
var
  Campo: String;
begin
  Campo := Lowercase(DBGOSSrv.Columns[THackDBGrid(DBGOSSrv).Col -1].FieldName);
  if (Campo = Lowercase(CO_FldAutrz)) or (Campo = Lowercase(CO_FldTudoFeito)) then
    DBGOSSrv.Options := DBGOSSrv.Options - [dgEditing] else
    DBGOSSrv.Options := DBGOSSrv.Options + [dgEditing];
end;

procedure TFmOSCab2.DBGOSSrvColExit(Sender: TObject);
begin
  DBGOSSrv.Options := DBGOSSrv.Options - [dgEditing];
end;

procedure TFmOSCab2.DBGOSSrvDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Autorizado' then
    MeuVCLSkin.DrawGrid(DBGOSSrv, Rect, 1, QrOSSrvAutorizado.Value);
  if Column.FieldName = 'TUDOFEITO' then
    MeuVCLSkin.DrawGrid(DBGOSSrv, Rect, 1, Trunc(QrOSSrvTUDOFEITO.Value));
end;

procedure TFmOSCab2.DBGOSSrvMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //if Button = mbRight then
    //MyObjects.MostraPopOnControlXY(PMOSSrv, DBGOSSrv, X, Y);
end;

procedure TFmOSCab2.DBGOSStaMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSSta, DBGOSSta, X, Y);
  *)
end;

procedure TFmOSCab2.DBGOSFrmFlhDdMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //if Button = mbRight then
    //MyObjects.MostraPopOnControlXY(PMOSFrmFlhDd, DBGOSFrmFlhDd, X, Y);
end;

procedure TFmOSCab2.DBGOSPrvMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSPrv, DBGOSPrv, X, Y);
  *)
end;

procedure TFmOSCab2.DBEdit21DblClick(Sender: TObject);
begin
  MostraFormEntidade(Sender);
end;

procedure TFmOSCab2.DBEdit25DblClick(Sender: TObject);
begin
  MostraFormEntidade(Sender);
end;

procedure TFmOSCab2.DBEdit4DblClick(Sender: TObject);
var
  Form: TForm;
  Entidade: Integer;
begin
  if QrOSCab.State <> dsInactive then
    Entidade := QrOSCabEntidade.Value
  else
    Entidade := 0;
  //
  Form := MyObjects.FormTDICria(TFmEntidade2,
    FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1);
  TFmEntidade2(Form).LocCod(Entidade, Entidade);
end;

procedure TFmOSCab2.DBEdSiapTerCadDblClick(Sender: TObject);
begin
  MostraFormCunsCad();
end;

procedure TFmOSCab2.DBGDiarioAddDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Txt, Bak: TColor;
begin
  if (Column.FieldName = 'IMP') then
  begin
    Txt := clWindowText;
    Bak := clWindow;
    case QrDiarioAddPrintOS.Value of
      -1: Bak := clFuchsia;
      0: Txt := clRed;
      1: Txt := clBlue;
    end;
    MyObjects.DesenhaTextoEmDBGrid(DBGDiarioAdd, Rect,
      Txt, Bak, Column.Alignment, Column.Field.DisplayText);
  end;
end;

procedure TFmOSCab2.DBGDiarioAddMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMDiarioAdd, DBGDiarioAdd, X, Y);
  *)
end;

procedure TFmOSCab2.DBGOSPipMonMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  //if Button = mbRight then
    //MyObjects.MostraPopOnControlXY(PMOSPipMon, DBGOSPipMon, X, Y);
end;

procedure TFmOSCab2.DBGOSPosMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSPos, DBGOSPos, X, Y);
  *)
end;

procedure TFmOSCab2.DBGOSPrzCellClick(Column: TColumn);
var
  Escolhido, Codigo, Controle, Grupo: Integer;
begin
  if Column.FieldName = CO_FldEscolhido then
  begin
    if QrOSPrzEscolhido.Value = 0 then
      Escolhido := 1
    else
      Escolhido := 0;
    //
    Grupo    := QrGruposGrupo.Value;
    Codigo   := QrOSPrzCodigo.Value;
    Controle := QrOSPrzControle.Value;
    //
    // Definir todas as condi��es da OS como n�o escolhidas
    if Escolhido = 1 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osprz', False, [
      'Escolhido'], ['Codigo'], [0], [Codigo], True);
    end;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osprz', False, [
    'Escolhido'], [
    'Controle'], [
    Escolhido
    ], [
    Controle], True) then
    begin
      OSApp_PF.TotalValorOS(Codigo);
      LocCod(Grupo, Grupo, FDisposicao, 0, 0, True, Codigo);
      OSApp_PF.ReopenOSPrz(QrOSPrz, Codigo, Controle);
    end;
  end;
end;

procedure TFmOSCab2.DBGOSPrzColEnter(Sender: TObject);
begin
  if DBGOSPrz.Columns[THackDBGrid(DBGOSPrz).Col -1].FieldName = CO_FldEscolhido then
    DBGOSPrz.Options := DBGOSPrz.Options - [dgEditing]
  else
    DBGOSPrz.Options := DBGOSPrz.Options + [dgEditing];
end;

procedure TFmOSCab2.DBGOSPrzDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Escolhido' then
    MeuVCLSkin.DrawGrid(DBGOSPrz, Rect, 1, QrOSPrzEscolhido.Value);
end;

procedure TFmOSCab2.DBGOSPrzMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  (*
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSPrz, DBGOSPrz, X,Y);
  *)
end;

function TFmOSCab2.DefineAgenteControlador(var Agente: Integer): Boolean;
const
  Aviso  = '...';
  Titulo = 'XXX-XXXXX-002 :: Sele��o de Agente';
  Prompt = 'Informe o agente pr�-escalado:';
  Campo  = 'Descricao';
var
  SQL: String;
  Res: Variant;
begin
  SQL := Geral.ATS([
  'SELECT Codigo, Nome Descricao ',
  'FROM entidades ',
  'WHERE Nome <> "" ',
  'AND ' + VAR_FP_FUNCION,
  'ORDER BY Nome ',
  '']);
  //
  Res := DBCheck.EscolheCodigoUnico(Aviso, Titulo, Prompt, nil,
          nil, Campo, 0, [SQL], Dmod.MyDB, False);
  if Res <> Null then
    Agente := Res
  else
    Agente := 0;
  Result := Agente <> 0;
end;

procedure TFmOSCab2.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOSCab2.SpeedButton10Click(Sender: TObject);
var
  ExeTxtCli1: Integer;
begin
  ExeTxtCli1   := EdExeTxtCli1.ValueVariant;
  VAR_CADASTRO := 0;

  FmPrincipal.MostraFormTxtGeneric(ExeTxtCli1);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdExeTxtCli1, CBExeTxtCli1, QrExeTxtCli1, VAR_CADASTRO);

    EdExeTxtCli1.SetFocus;
  end;
end;

procedure TFmOSCab2.SpeedButton11Click(Sender: TObject);
var
  ExeTxtCli2: Integer;
begin
  VAR_CADASTRO := 0;
  ExeTxtCli2   := EdExeTxtCli2.ValueVariant;

  FmPrincipal.MostraFormTxtGeneric(ExeTxtCli2);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdExeTxtCli2, CBExeTxtCli2, QrExeTxtCli2, VAR_CADASTRO);

    EdExeTxtCli2.SetFocus;
  end;
end;

procedure TFmOSCab2.SpeedButton12Click(Sender: TObject);
var
  MobiliCad: Integer;
begin
  VAR_CADASTRO := 0;
  MobiliCad    := EdMobiliCad.ValueVariant;

  FmPrincipal.MostraMobiliCad(MobiliCad);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdMobiliCad, CBMobiliCad, QrMobiliCad, VAR_CADASTRO);

    EdMobiliCad.SetFocus;
  end;
end;

procedure TFmOSCab2.SBVendedorClick(Sender: TObject);
var
  Vendedor: Integer;
begin
  VAR_CADASTRO := 0;
  Vendedor     := EdVendedor.ValueVariant;

  FmPrincipal.CadastroEntidades(Vendedor, False);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdVendedor, CBVendedor, QrVendedores, VAR_CADASTRO);
    EdVendedor.SetFocus;
  end;
end;

procedure TFmOSCab2.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOSCab2.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOSCab2.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOSCab2.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOSCab2.SpeedButton5Click(Sender: TObject);
var
  Entidade: Integer;
begin
  VAR_CADASTRO := 0;
  Entidade     := EdEntidade.ValueVariant;

  FmPrincipal.CadastroEntidades(Entidade, False);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdEntidade, CBEntidade, QrEntidades, VAR_CADASTRO);

    UMyMod.AbreQuery(QrEntContrat, Dmod.MyDB);
    UMyMod.AbreQuery(QrEntPagante, Dmod.MyDB);
    UMyMod.AbreQuery(QrEntiContat, Dmod.MyDB);

    EdEntidade.SetFocus;
  end;
end;

procedure TFmOSCab2.SpeedButton6Click(Sender: TObject);
var
  EntContrat: Integer;
begin
  VAR_CADASTRO := 0;
  EntContrat   := EdEntContrat.ValueVariant;

  FmPrincipal.CadastroEntidades(EntContrat, False);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdEntContrat, CBEntContrat, QrEntContrat, VAR_CADASTRO);

    UMyMod.AbreQuery(QrEntidades, Dmod.MyDB);
    UMyMod.AbreQuery(QrEntPagante, Dmod.MyDB);
    UMyMod.AbreQuery(QrEntiContat, Dmod.MyDB);

    EdEntContrat.SetFocus;
  end;
end;

procedure TFmOSCab2.SpeedButton7Click(Sender: TObject);
var
  Entidade: Integer;
begin
  VAR_CADASTRO := 0;
  Entidade     := EdEntPagante.ValueVariant;

  FmPrincipal.CadastroEntidades(Entidade, False);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdEntPagante, CBEntPagante, QrEntPagante, VAR_CADASTRO);

    UMyMod.AbreQuery(QrEntidades, Dmod.MyDB);
    UMyMod.AbreQuery(QrEntContrat, Dmod.MyDB);
    UMyMod.AbreQuery(QrEntiContat, Dmod.MyDB);

    EdEntPagante.SetFocus;
  end;
end;

procedure TFmOSCab2.SpeedButton8Click(Sender: TObject);
var
  Cliente, SiapTerCad: Integer;
begin
  VAR_CADASTRO := 0;
  Cliente      := EdEntidade.ValueVariant;
  SiapTerCad   := EdSiapTerCad.ValueVariant;

  FmPrincipal.MostraFormCunsCad(False, Cliente, SiapTerCad);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdSiapTerCad, CBSiapTerCad, QrSiapterCad, VAR_CADASTRO);

    EdSiapTerCad.SetFocus;
  end;
end;

procedure TFmOSCab2.SpeedButton9Click(Sender: TObject);
var
  Entidade: Integer;
begin
  Entidade     := EdEntidade.ValueVariant;
  VAR_CADASTRO := 0;

  FmPrincipal.CadastroEntidades(Entidade, False);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdEntiContat, CBEntiContat, QrEntiContat, VAR_CADASTRO, 'Controle');

    EdEntiContat.SetFocus;
  end;
end;

procedure TFmOSCab2.SpLugarOpcaoMoved(Sender: TObject);
begin
  Geral.WriteAppKeyLM('LugarOpcao', Application.Title, SpLugarOpcao.Left, ktInteger);
end;

procedure TFmOSCab2.TPDtaExeIniExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (TPDtaExeFim.Date < 2) then
    TPDtaExeFim.Date := TPDtaExeIni.Date;
  //
  if (EdDtaExeIni.Text = '00:00') and (EdDtaExeFim.Text = '01:00') then
    EdDtaExeFim.ValueVariant := '00:00';
end;

procedure TFmOSCab2.TPDtaExePrvExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (TPFimExePrv.Date < 2) then
    TPFimExePrv.Date := TPDtaExePrv.Date;
  //
  if (EdDtaExePrv.Text = '00:00') and (EdFimExePrv.Text = '01:00') then
    EdFimExePrv.ValueVariant := '00:00';
end;

procedure TFmOSCab2.TPDtaVisExeExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (TPFimVisExe.Date < 2) then
    TPFimVisExe.Date := TPDtaVisExe.Date;
  //
  if (EdDtaVisExe.Text = '00:00') and (EdFimVisExe.Text = '01:00') then
    EdFimVisExe.ValueVariant := '00:00';
end;

procedure TFmOSCab2.TPDtaVisPrvExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (TPFimVisPrv.Date < 2) then
    TPFimVisPrv.Date := TPDtaVisPrv.Date;
  //
  if (EdDtaVisPrv.Text = '00:00') and (EdFimVisPrv.Text = '01:00') then
    EdFimVisPrv.ValueVariant := '00:00';
end;

procedure TFmOSCab2.TravaOForm();
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  PnLugarOpcao.Visible := True;
  GOTOy.BotoesSb(ImgTipo.SQLType, TFmOSCab2(Self));
  //
  MyObjects.Entitula(GBOperacao, [LaOperacaoA, LaOperacaoB, LaOperacaoC],
  [], ' ' + QrOSCabNO_OPERACAO.Value, False, taLeftJustify, 2, 10, 20);
end;

procedure TFmOSCab2.UploadNuvem1Click(Sender: TObject);
begin
  if not PodeResponderPerguntas() then
    Exit;
  //
  DmModOS.UpLoadWeb_OS(QrOSCabCodigo.Value, ulaNada, LaAviso1, LaAviso2);
end;

procedure TFmOSCab2.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSCab2.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOSCabCodigo.Value;
  //
  if TFmOSCab2(Self).Owner is TApplication then
    Close
  else
    MyObjects.FormTDIFecha(Self, TTabSheet(CSTabSheetChamou.Component));
end;

procedure TFmOSCab2.ItsInclui4Click(Sender: TObject);
begin
  MostraOSFrmCab(stIns);
end;

procedure TFmOSCab2.ItsInclui5Click(Sender: TObject);
begin
(*
   Adiciona �nico
   Desativado em 2014-04-23
  MostraOSMonCabUni(stIns);
  //
  Deixado apenas adi��o de m�ltiplos
*)
  MostraOSMonCabMul();
end;

procedure TFmOSCab2.CabAltera1Click(Sender: TObject);
begin
  ConfiguraCorStatus(QrEstatusOSsAgeCorIni.Value, QrEstatusOSsAgeCorFon.Value,
    TEdit(EdEstatusCor), nil);
  //
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOSCab, [PnDados],
    [PnEdita], EdEntidade, ImgTipo, 'oscab');
  // Esta tirando o valor quando faz antes do cliente!
  EdSiapTerCad.ValueVariant := QrOSCabSiapTerCad.Value;
  CBSiapTerCad.KeyValue     := QrOSCabSiapTerCad.Value;
  //
  RGMapPMV.ItemIndex := QrOSCabMapPMV.Value + 1;
  //
  EdAgeEqiCab.Enabled  := False;
  CBAgeEqiCab.Enabled  := False;
  PnLugarOpcao.Visible := False;
end;

procedure TFmOSCab2.Cabealho1Click(Sender: TObject);
begin
  MostraOSMonCabUni(stUpd);
end;

procedure TFmOSCab2.BtConfirmaClick(Sender: TObject);
  function TemFormulasAplicacao(Codigo: Integer): Boolean;
  var
    Qry: TmySQLQuery;
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT COUNT(*) Itens ',
      'FROM osfrmcab ',
      'WHERE Codigo=' + Geral.FF0(Codigo),
      '']);
      //
      Result := Qry.FieldByName('Itens').AsInteger > 0;
    finally
      Qry.Free;
    end;
  end;
const
  IncluiOSOrigem = True;
var
  //DdsPosVda,
  DtaContat, DtaVisPrv, DtaVisExe, DtaExePrv, DtaExeIni,
  DtaExeFim, ObsGaranti, ObsExecuta, FimVisPrv, FimVisExe, FimExePrv: String;

  //
  //GarantiaDd, HrEvacuar, HrExecutar, CondicaoPG,
  Codigo, Entidade, SiapTerCad, Estatus, FatoGeradr, (*OSOrigem,*) EntiContat,
  NumContrat, EntPagante, EntContrat, Empresa, ValiDdOrca, Operacao,
  ExeTxtCli1, ExeTxtCli2, Vendedor: Integer;
  Inicio, Termino: TDateTime;
  Nome, Notas: String;
  Cor: Integer;
  Cption: Byte;
  Grupo, Numero, Opcao, (*Optado,*) Agente, Controle, AgeEqiCab, Responsa,
  Status, MobiliCad, PosGerou, MapPMV: Integer;
  Incluindo: Boolean;
begin
  Incluindo := (ImgTipo.SQLType = stIns)
    or ((QrOSSrv.State <> dsInactive) and (QrOSSrv.RecordCount = 0));
  (* 2013-06-29
  if ImgTipo.SQLType = stIns then
  begin
    if not DefineAgenteControlador(Agente) then
      Exit;
  end;
  FIM 2013-06-29 *)
  Codigo         := QrOSCabCodigo.Value;
  Empresa        := EdEmpresa.ValueVariant;
  Entidade       := EdEntidade.ValueVariant;
  SiapTerCad     := EdSiapTerCad.ValueVariant;
  Estatus        := EdEstatus.ValueVariant;
  FatoGeradr     := EdFatoGeradr.ValueVariant;
  //OSOrigem       := EdOSOrigem.ValueVariant;
  MobiliCad      := EdMobiliCad.ValueVariant;
  DtaContat      := Geral.FDT(TPDtaContat.Date, 1) + ' ' + EdDtaContat.Text + ':00';
  DtaVisPrv      := Geral.FDT(TPDtaVisPrv.Date, 1) + ' ' + EdDtaVisPrv.Text + ':00';
  DtaVisExe      := Geral.FDT(TPDtaVisExe.Date, 1) + ' ' + EdDtaVisExe.Text + ':00';
  DtaExePrv      := Geral.FDT(TPDtaExePrv.Date, 1) + ' ' + EdDtaExePrv.Text + ':00';
  DtaExeIni      := Geral.FDT(TPDtaExeIni.Date, 1) + ' ' + EdDtaExeIni.Text + ':00';
  DtaExeFim      := Geral.FDT(TPDtaExeFim.Date, 1) + ' ' + EdDtaExeFim.Text + ':00';
  FimVisPrv      := Geral.FDT(TPFimVisPrv.Date, 1) + ' ' + EdFimVisPrv.Text + ':00';
  FimVisExe      := Geral.FDT(TPFimVisExe.Date, 1) + ' ' + EdFimVisExe.Text + ':00';
  FimExePrv      := Geral.FDT(TPFimExePrv.Date, 1) + ' ' + EdFimExePrv.Text + ':00';
  {
  DdsPosVda      := EdDdsPosVda.ValueVariant;
  GarantiaDd     := EdGarantiaDD.ValueVariant;
  HrEvacuar      := EdHrEvacuar.ValueVariant;
  HrExecutar     := EdHrExecutar.ValueVariant;
  CondicaoPG     := EdCondicaoPG.ValueVariant;
  }
  EntiContat     := EdEntiContat.ValueVariant;
  NumContrat     := EdNumContrat.ValueVariant;
  EntPagante     := EdEntPagante.ValueVariant;
  EntContrat     := EdEntContrat.ValueVariant;
  //
  ValiDdOrca     := EdValiDdOrca.ValueVariant;
  Operacao       := CGOperacao.Value;
  ExeTxtCli1     := EdExeTxtCli1.ValueVariant;
  ExeTxtCli2     := EdExeTxtCli2.ValueVariant;
  ObsGaranti     := MeObsGaranti.Text;
  ObsExecuta     := MeObsExecuta.Text;
  //
  AgeEqiCab      := EdAgeEqiCab.ValueVariant;
  //
  Vendedor       := EdVendedor.ValueVariant;
  //
  MapPMV         := RGMapPMV.ItemIndex - 1;
  //
  if MyObjects.FIC(AgeEqiCab = 0, EdAgeEqiCab, 'Informe a equipe de agentes!') then
    Exit;
  if MyObjects.FIC(MobiliCad = 0, EdMobiliCad, 'Informe a forma de mobilidade!') then
    Exit;
  if MyObjects.FIC(MapPMV < 0, RGMapPMV, 'Informe o mapeamento de iscas!') then
    Exit;

  //  OS de origem
{
  if MyObjects.FIC((OSOrigem = 0)
  and (not (OSApp_PF.OSOrigDesnecessaria(FatoGeradr))),
  EdOSOrigem, 'Informe a OS de origem!') then
    Exit;
  if MyObjects.FIC((OSOrigem <> 0)
  and (OSApp_PF.OSOrigDesnecessaria(FatoGeradr)),
  EdOSOrigem, 'OS de origem desnecess�ria!') then
    Exit;
  OSApp_PF.ReopenOSOriPsq(DmModOS.QrOSOriPsq, Empresa, Entidade, SiapTerCad,
    CO_COD_FatoGeradr_1aAcao, OSOrigem, IncluiOSOrigem);
}
  //

  // FIM OS de origem

  //
  Inicio     := Geral.STD_109(DtaVisPrv);
  Termino    := Geral.STD_109(FimVisPrv);
  if DmModAgenda.DesistePorqueNaoVaiAgendar(Inicio, Termino, 'Vistoria')
  {
  or ((ImgTipo.SQLType = stUpd) and (DmModAgenda.ObtemDeAgendaEve_Codigo(
  qagOSBgstrl, aagVistoria, Codigo) = 0))} then
    Exit;
  //
  Inicio  := Geral.STD_109(DtaExePrv);
  Termino := Geral.STD_109(FimExePrv);
  if DmModAgenda.DesistePorqueNaoVaiAgendar(Inicio, Termino, 'Execu��o')
  {
  or ((ImgTipo.SQLType = stUpd) and (DmModAgenda.ObtemDeAgendaEve_Codigo(
  qagOSBgstrl, aagExecucao, Codigo) = 0))} then
    Exit;
  //
  if MyObjects.FIC(Operacao = 0, CGOperacao, 'Informe a opera��o!') then
    Exit;
  if ImgTipo.SQLType = stUpd then
  begin
    {
    if MyObjects.FIC((QrOSSrv.RecordCount > 0) and
    (Geral.IntInConjunto(1, Operacao) = False), CGOperacao,
    'A opera��o dedetiza��o deve ser marcada pois j� existe item para ela!') then
      Exit;
    }
    if (QrOSSrv.RecordCount > 0) and
    (Geral.IntInConjunto(1, Operacao) = False) then
    begin
      if (*(Geral.IntInConjunto(4, Operacao) = False)
      and*) TemFormulasAplicacao(Codigo) then
      begin
         Geral.MB_Aviso(
         'A opera��o dedetiza��o deve ser marcada pois j� existe item para ela!');
         Exit;
      end;
    end;
    //
    if MyObjects.FIC((QrOSCxa.RecordCount > 0) and
    (Geral.IntInConjunto(2, Operacao) = False), CGOperacao,
    'A opera��o limpeza de caixa d''�gua deve ser marcada pois j� existe item para ela!') then
      Exit;
    if MyObjects.FIC((QrOSPipMon.RecordCount > 0) and
    (Geral.IntInConjunto(4, Operacao) = False), CGOperacao,
    'A opera��o monitoramento deve ser marcada pois j� existe item para ela!') then
      Exit;
  end;
  if ImpedePelaDataFinalDeExecucao(Geral.STD_109(DtaExeFim)) then
    Exit;
  //
  // 2013-07-18
(*
  //2013-06-29
  if ImgTipo.SQLType = stIns then
  begin
    if not DefineAgenteControlador(Agente) then
      Exit;
  end;
  //FIM 2013-06-29
*)
  // FIM 2013-07-18
  //Grupo := QrGruposGrupo.Value;
  Grupo     := QrOSCabGrupo.Value;
  Grupo     := UMyMod.BPGS1I32('oscab', 'Grupo', '', '', tsDef, ImgTipo.SQLType, Grupo);
  Numero    := Grupo; // N�o usa para nada! S� para imprimir como n�mero da OS!
  Opcao     := EdOpcao.ValueVariant;
  PosGerou  := Geral.BoolToInt(CkPosGerou.Checked);

  Codigo := UMyMod.BPGS1I32('oscab', 'Codigo', '', '', tsDef, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'oscab', False, [
  'Empresa',
  'Entidade', 'SiapTerCad', 'Estatus',
  'FatoGeradr', (*'OSOrigem',*) 'DtaContat',
  'DtaVisPrv', 'DtaVisExe', 'DtaExePrv',
  'DtaExeIni', 'DtaExeFim', (*'GarantiaDd',
  'HrEvacuar', 'HrExecutar', *'ValorTotal',*
  'CondicaoPG', 'DdsPosVda',*) 'EntiContat',
  'NumContrat', 'EntPagante', 'EntContrat',
  'ValiDdOrca', 'Operacao', 'ExeTxtCli1',
  'ExeTxtCli2', 'ObsGaranti', 'ObsExecuta',
  'FimVisPrv', 'FimVisExe', 'FimExePrv',
  'Grupo', 'Numero', 'Opcao',
  'AgeEqiCab', 'MobiliCad', 'PosGerou',
  'Vendedor', 'MapPMV'], [
  'Codigo'], [
  Empresa,
  Entidade, SiapTerCad, Estatus,
  FatoGeradr, (*OSOrigem,*) DtaContat,
  DtaVisPrv, DtaVisExe, DtaExePrv,
  DtaExeIni, DtaExeFim, (*GarantiaDd,
  HrEvacuar, HrExecutar, *ValorTotal,*
  CondicaoPG, DdsPosVda,*) EntiContat,
  NumContrat, EntPagante, EntContrat,
  ValiDdOrca, Operacao, ExeTxtCli1,
  ExeTxtCli2, ObsGaranti, ObsExecuta,
  FimVisPrv, FimVisExe, FimExePrv,
  Grupo, Numero, Opcao,
  AgeEqiCab, MobiliCad, PosGerou,
  Vendedor, MapPMV], [
  Codigo], True) then
  begin
    if ImgTipo.SQLType = stIns then
    begin
// 2013-07-18
{
      Controle := UMyMod.BPGS1I32('osage', 'Controle', '', '', tsPos, stIns, 0);
      //
      UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'osage', False, [
      'Codigo', 'Agente'(*, 'Responsa'*)], [
      'Controle'], [
      Codigo, Agente(*, Responsa*)], [
      Controle], True);
}
      OSApp_PF.CriaItensDeAgentes(Codigo, AgeEqiCab);
// FIM 2013-07-18
    end;
    // Vistoria
    Inicio  := Geral.STD_109(DtaVisPrv);
    Termino := Geral.STD_109(FimVisPrv);
    Nome    := '';
    Notas   := '';
    Cor     := 0;
    Cption  := 0;
    Status  := Estatus;
    //
    DmModAgenda.AtualizaCompromissoNaAgenda(qagOSBgstrl, aagVistoria, Codigo,
    Inicio, Termino, Empresa, Entidade, SiapTerCad, FatoGeradr, Nome, Notas,
    Cor, Cption, Status);
    // Execu��o
    Inicio  := Geral.STD_109(DtaExePrv);
    Termino := Geral.STD_109(FimExePrv);
    Nome    := '';
    Notas   := '';
    Cor     := 0;
    Cption  := 0;
    DmModAgenda.AtualizaCompromissoNaAgenda(qagOSBgstrl, aagExecucao, Codigo,
    Inicio, Termino, Empresa, Entidade, SiapTerCad, FatoGeradr, Nome, Notas,
    Cor, Cption, Status);
    //
    OSAll_PF.AtualizaCadastroCunsIts(EntContrat, Entidade);
    OSApp_PF.TotalValorOS(Codigo);
    if OSApp_PF.TemProvidenciaOrfa(Entidade) then
      OSApp_PF.MostraFormOSPrvSel(stIns, Codigo, QrOSPrv, Entidade);
    OSApp_PF.ChecaOSStaDeStatusDefinido(Codigo, Estatus);


    // Deve ser por �ltimo pois depende de outros dados acima!
    if ImgTipo.SQLType = stUpd then
    begin
    {
      QrOSSrv.DisableControls;
      try
        QrOSSrv.First;
        while not QrOSSrv.Eof do
        begin
    }
          OSsFuturas();
    {
          //
          QrOSSrv.Next;
        end;
      finally
        QrOSSrv.EnableControls;
      end;
    }
    end;
    ///////////// FIM DAS VERIFICACOES /////////////////////////////////////////
    LocCod(Grupo, Grupo, FDisposicao, 0, 0, True, Codigo);
    if FSeq = 1 then
      Close
    else
    begin
      TravaOForm();
      if Incluindo then
        //MostraOSSrv(stIns);
        AdicionaServico(True);
    end
  end;
end;

procedure TFmOSCab2.BtCroquGerClick(Sender: TObject);
var
  Aba: Boolean;
  Cliente, SiapTerCad: Integer;
begin
  if (QrOSCab.State <> dsInactive) and (QrOSCab.RecordCount > 0) then
  begin
    if TFmOSCab2(Self).Owner is TApplication then
      Aba := False
    else
      Aba := True;
    //
    Cliente    := QrOSCabEntidade.Value;
    SiapTerCad := QrOSCabSiapTerCad.Value;
    //
    FmPrincipal.MostraFormCunsCad(Aba, Cliente, SiapTerCad, True, 0, False, True);
  end;
end;

procedure TFmOSCab2.BtCunsCadClick(Sender: TObject);
var
  Aba: Boolean;
  Cliente: Integer;
begin
  if TFmOSCab2(Self).Owner is TApplication then
    Aba := False
  else
    Aba := True;
  //
  if (QrOSCab.State <> dsInactive) and (QrOSCab.RecordCount > 0) then
    Cliente := QrOSCabEntidade.Value
  else
    Cliente := 0;
  //
  FmPrincipal.MostraFormCunsCad(Aba, Cliente, 0);
end;

procedure TFmOSCab2.BtDesisteClick(Sender: TObject);
begin
  TravaOForm();
end;

procedure TFmOSCab2.BtFatClick(Sender: TObject);
begin
  PCGeral.ActivePageIndex := 2;
  //
  MyObjects.MostraPopUpDeBotao(PMFat, BtFat);
end;

procedure TFmOSCab2.BtLaudoDelClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLaudoDel, BtLaudoDel);
end;

procedure TFmOSCab2.BtLaudoInsClick(Sender: TObject);
begin
  MostraFormOSCabLaudo;
end;

procedure TFmOSCab2.BtNFSePesqClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_NFSePesq(False, nil, nil);
end;

procedure TFmOSCab2.BtOSSrvClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOSSrv, BtOSSrv);
end;

procedure TFmOSCab2.BtOSAlvClick(Sender: TObject);
begin
  case PCGeral.ActivePageIndex of
    1:
    begin
      if MyObjects.FIC((QrOSSrv.State = dsInactive)
        or (QrOSSrv.RecordCount = 0), nil, 'Informe um servi�o!')
      then
        Exit;
      //
      MostraOSAlv(stIns);
    end;
    else
    begin
      PCGeral.ActivePageIndex := 0;
      //
      MyObjects.MostraPopUpDeBotao(PMOSCabAlv, BtOSAlv);
    end;
  end;
end;

procedure TFmOSCab2.BtOSFrmCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOSFrmCab, BtOSFrmCab);
end;

procedure TFmOSCab2.BtOSFrmEPIClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOSFrmEPI, BtOSFrmEPI);
end;

procedure TFmOSCab2.BtOSMonCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOSMonCab, BtOSMonCab);
end;

procedure TFmOSCab2.BtOSMonEPIClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOSMonEPI, BtOSMonEPI);
end;

procedure TFmOSCab2.Abrangncia1Click(Sender: TObject);
begin
  if MyObjects.FIC((QrOSFrmCab.State = dsInactive)
  or (QrOSFrmCab.RecordCount = 0), nil,
  'Informe uma formula��o de aplica��o!') then
    Exit;
  //
  MostraOSFrmAbr(stIns);
end;

procedure TFmOSCab2.ItsInclui1Click(Sender: TObject);
begin
  AdicionaServico(True);
end;

procedure TFmOSCab2.ItsInclui2Click(Sender: TObject);
begin
  MostraOSCxa(stIns);
end;

procedure TFmOSCab2.AdicionaAgente;
const
  Aviso  = '...';
  Titulo = 'XXX-XXXXX-004 :: Sele��o de Agentes';
  Prompt = 'Seleciones os agentes operacionais:';
  //Campo  = 'Descricao';
  //
  DestTab = 'osage';
  DestMaster = 'Codigo';
  DestDetail = 'Controle';
  DestSelec1 = 'Agente';
  SorcField  = 'Agente';
  ExcluiAnteriores = True;
var
  OSCab, ValrMaster: Integer;
begin
  ValrMaster := QrOSCabCodigo.Value;
  OSCab := QrOSCabCodigo.Value;
  if DBCheck.EscolheCodigosMultiplos_A(
  Aviso, Titulo, Prompt, nil, 'Ativo', 'Nivel1', 'Nome', [
  'DELETE FROM _selcods_; ',
  'INSERT INTO _selcods_ ',
  'SELECT Codigo Nivel1, 0 Nivel2, ',
  '0 Nivel3, 0 Nivel4, 0 Nivel5, Nome, 0 Ativo ',
  'FROM ' + TMeuDB + '.entidades',
  'WHERE Nome <> "" ',
  'AND ' + VAR_FP_FUNCION,
  ''],[
  'SELECT * FROM _selcods_ ',
  'ORDER BY Nome;',
  ''], DestTab, [DestMaster], DestDetail, DestSelec1, [ValrMaster],
  QrOSAge, SorcField, ExcluiAnteriores, Dmod.QrUpd) then
  begin
    DmModOS.MD5_AtualizaCheckSumOSAge(QrOSCabCodigo.Value);
    OSApp_PF.ReopenOSCab(QrOSCab, OSCab);
    OSApp_PF.ReopenOSAge(QrOSAge, QrOSCabCodigo.Value, 0);
  end;
end;

procedure TFmOSCab2.Adicionaagentes1Click(Sender: TObject);
begin
  AdicionaAgente();
end;

procedure TFmOSCab2.Adicionanovacondiodepagamento1Click(Sender: TObject);
begin
  OSApp_PF.MostraFormOSPrz(stIns, QrOSCab, QrOSPrz, 0, 0, 0);
end;

procedure TFmOSCab2.AdicionanovosPMVsemOSsfuturas1Click(Sender: TObject);
begin
  OSApp_PF.AdicionaPMVsEmOSsFuturas(QrOSSrvControle.Value,
    QrOSCabSiapTerCad.Value, QrOSCabDtaExeFim.Value, QrOSCabCodigo.Value);
end;

procedure TFmOSCab2.AdicionaPIPsativos1Click(Sender: TObject);
begin
{ Ver o que fazer! Adicionar como � adicionado na inclus�o das OSs autom�ticas!
  OSApp_PF.AdicionaPIPsAtivosEmOS(QrOSPipMon,
    QrOSCabCodigo.Value, QrOSCabEntidade.Value, QrOSCabSiapTerCad.Value);
  //OSApp_PF.ReopenOSPipMon(QrOSPipMon, QrOSCabCodigo.Value, 0);
}
end;

procedure TFmOSCab2.AdicionaPMVsemOSsFuturas1Click(Sender: TObject);
begin
  OSApp_PF.AdicionaPMVsEmOSsFuturas(QrOSSrvControle.Value,
    QrOSCabSiapTerCad.Value, QrOSCabDtaExeFim.Value, QrOSCabCodigo.Value);
end;

procedure TFmOSCab2.AdicionaServico(IniLoop: Boolean);
var
  AdicaoServico: Byte;
  Contratante, Codigo, Controle: Integer;
begin
  if IniLoop then
    AdicaoServico := MyObjects.SelRadioGroup('Adi��o de Servi�os',
    'Como deseja adicionar os servi�os?',
    ['N�o quero adicionar agora',
    'Agora, manualmente',
    'Agora, pr�-configurado'], -1)
  else
    AdicaoServico := MyObjects.SelRadioGroup('Adi��o de Servi�os',
    'Deseja adicionar mais um servi�o?',
    ['N�o quero mais adicionar',
    'Mais um manualmente',
    'Mais um pr�-configurado'], -1);
  //
  case AdicaoServico of
    1: MostraOSSrv(stIns);
    2: MostraPreSrv();
    else Exit;
  end;
  AdicionaServico(False);
end;

procedure TFmOSCab2.Adicionasolicitaesdeprovidncias1Click(Sender: TObject);
begin
  OSApp_PF.MostraFormOSPrvSel(stIns, QrOSCabCodigo.Value, QrOSPrv, QrOSCabEntidade.Value);
end;

procedure TFmOSCab2.Adicionastatus1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'XXX-XXXXX-004 :: Sele��o de Status';
  Prompt = 'Seleciones os finalizados:';
  //Campo  = 'Descricao';
  //
  DestTab = 'ossta';
  DestMaster = 'Codigo';
  DestDetail = 'Controle';
  DestSelec1 = 'Status';
  SorcField  = 'Status';
  ExcluiAnteriores = True;
var
  ValrMaster: Integer;
begin
  ValrMaster := QrOSCabCodigo.Value;
  if DBCheck.EscolheCodigosMultiplos_A(
  Aviso, Titulo, Prompt, nil, 'Ativo', 'Nivel1', 'Nome', [
  'DELETE FROM _selcods_; ',
  'INSERT INTO _selcods_ ',
  'SELECT Codigo Nivel1, Ordem Nivel2, ',
  '0 Nivel3, 0 Nivel4, 0 Nivel5, Nome, 0 Ativo ',
  'FROM ' + TMeuDB + '.estatusoss',
  'WHERE Nome <> "" ',
  ''],[
  'SELECT * FROM _selcods_ ',
  'ORDER BY Nivel2, Nivel1, Nome;',
  ''], DestTab, [DestMaster], DestDetail, DestSelec1, [ValrMaster],
  QrOSSta, SorcField, ExcluiAnteriores, Dmod.QrUpd) then
    OSApp_PF.ReopenOSSta(QrOSSta, QrOSCabCodigo.Value, 0);
end;

procedure TFmOSCab2.Adiodenovolugar1Click(Sender: TObject);
begin
// N�o precisa???!!!
end;

procedure TFmOSCab2.Adiodenovooramento1Click(Sender: TObject);
var
  Grupo, Numero, Lugar, Opcao, Codigo, HowGerou: Integer;
begin
  if Geral.MB_Pergunta(
  'Confirma a cria��o de um novo or�amento nesta proforma copiando o or�amento atual?')
  = ID_YES then
  begin;
    try
      Grupo  := QrGruposGrupo.Value;
      Numero := QrOSCabNumero.Value;
      Opcao  := 0;
      Lugar  := 0;
      Codigo := 0;
      HowGerou := CO_OS_HOWGEROU_OS_CLONANDO_OS;
      if OSApp_PF.StartaInclusaoOS(QrOSCabEntidade.Value, Grupo, Lugar, Opcao,
      dupPorOpcao) then
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrReplica, Dmod.MyDB, [
        'SELECT  Grupo, Opcao, SiapTerCad, Codigo ',
        'FROM oscab ',
        'WHERE Grupo <> 0  ',
        'AND Grupo=' + Geral.FF0(QrGruposGrupo.Value),
        'AND Opcao=' + Geral.FF0(QrOpcoesOpcao.Value),
        'ORDER BY Opcao, SiapTerCad ',
        '']);
        //
        QrReplica.First;
        while not QrReplica.Eof do
        begin
          OSApp_PF.ReopenOSCab(QrOSCab, QrReplicaCodigo.Value);
          //
          Lugar  := QrOSCabSiapTerCad.Value;
          if DuplicaLocalizadorAtual(
            Grupo, Numero, Lugar, Opcao, Codigo, dupNenhum, HowGerou, False) then ;
          //
          QrReplica.Next;
        end;
        //
        LocCod(Grupo, Grupo, FDisposicao, 0, 0, True, Codigo);
        LocalizaOS(Lugar, Opcao, Codigo);
      end;
      //
    finally
      Screen.Cursor := crDefault;
      PnForm.Enabled := True;
    end;
  end
end;

procedure TFmOSCab2.AlteraC3Click(Sender: TObject);
begin
  MostraOSPos(stUpd);
end;

procedure TFmOSCab2.Alteradatadaltimaemissodefrmulasfilhas1Click(
  Sender: TObject);
begin
  if OSApp_PF.AlteraDataDaUltimaEmissoDeFormulasFilhas(QrOSFrmFlhCbEmisUltDta.Value,
    QrOSFrmFlhCbIDIts.Value, istosfrmflhcb)
  then
    OSApp_PF.ReopenOSFrmFlhCb(QrOSFrmFlhCb, QrOSFrmCabConta.Value, QrOSFrmFlhCbIDIts.Value);
end;

procedure TFmOSCab2.AlteradatadaltimaemissodeOSsfilhas1Click(Sender: TObject);
begin
  if OSApp_PF.AlteraDataDaUltimaEmissoDeFormulasFilhas(QrOSMonCabEmisUltDta.Value,
    QrOSMonCabConta.Value, istosmoncab)
  then
    OSApp_PF.ReopenOSMonCab(QrOSMonCab, QrOSSrvControle.Value, QrOSMonCabConta.Value);
end;

procedure TFmOSCab2.Alteraevoluodeaplicao1Click(Sender: TObject);
begin
  MostraOSFrmEvo(stUpd);
end;

procedure TFmOSCab2.Alterafrmulafilha1Click(Sender: TObject);
begin
  MostraOSFrmFlhCb(stUpd);
end;

procedure TFmOSCab2.Alteraintervalo1Click(Sender: TObject);
begin
  MostraOSFrmFlhDd(stUpd);
end;

procedure TFmOSCab2.AlteraIntervalo2Click(Sender: TObject);
begin
  MostraOSMonPipDd(stUpd);
end;

procedure TFmOSCab2.Alteraitemdeescalonamento1Click(Sender: TObject);
begin
  OSApp_PF.MostraFormOSCabXtr(stUpd, QrOSCab, QrOSCabXtr);
end;

procedure TFmOSCab2.Alteramonitoramentoextra1Click(Sender: TObject);
begin
  //  Nao fazer!!
  // Se, e quando fazer, ver a qustao de datas minimas, maximas, dias etc.
  //MostraOSExtMon(stUpd);
end;

procedure TFmOSCab2.Alteraodeconsumorealizado1Click(Sender: TObject);
begin
  MostraOSAltRec(VAR_FATID_4101);
end;

procedure TFmOSCab2.Alteraodeconsumorealizado2Click(Sender: TObject);
begin
  MostraOSAltRec(VAR_FATID_4102);
end;

procedure TFmOSCab2.Alteraproformadolocalizador1Click(Sender: TObject);
var
  MaxGru, Grupo, Codigo, Numero: Integer;
  Qry: TmySQLQuery;
  Cod: Variant;
begin
  Qry := TmySQLQuery.Create(Dmod);
  try
    Grupo := QrGruposGrupo.Value;
    Qry.DataBase := DMod.MyDB;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT MAX(Grupo) Grupo ',
    'FROM oscab ',
    '']);
    MaxGru := Qry.FieldByName('Grupo').AsInteger;
    // permitir usar o pr�ximo?
    if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfInteger, Grupo, 0, 0,
    '', '', True, 'Proforma', 'N�mero da proforma: ', 0, Cod) then
    begin
      Grupo  := Cod;
      Codigo := QrOSCabCodigo.Value;
      Numero := Grupo;
      if (Grupo <= MaxGru) or (Grupo > 0) then
      begin
        if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
        'Grupo', 'Numero'], ['Codigo'], [
        Grupo, Numero], [Codigo], True) then
        LocalizaLocalizador(Codigo);
      end else
        Geral.MB_Aviso('N�mero inv�lido para proforma!' + sLineBreak +
        'Informe um valor entre 1 e ' + Geral.FF0(MaxGru));
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmOSCab2.Aplicaes1Click(Sender: TObject);
const
  FatID = VAR_FATID_4101;
  Pergunta = True;
begin
  if OSApp_PF.RatificaConsumoOS_PreAnalise(nrcosFormula, QrOSSrvControle.Value,
  FatID, Pergunta) then
  begin
    if Geral.MB_Pergunta(
    'Confirma a ratifica��o dos consumos ainda n�o ratificados das f�rmulas de aplica��o deste servi�o?') =
    ID_YES then
    begin
      RatificaConsumo('osfrmcab', 'osfrmrec', 'Controle', QrOSSrvControle.Value, VAR_FATID_4101);
      //
      OSApp_PF.ReopenOSSrv(QrOSSrv, QrOSCabCodigo.Value, 0);
    end;
  end;
end;

procedure TFmOSCab2.ApliceMonit1Click(Sender: TObject);
const
  FatID = 0;
  Pergunta = True;
begin
  if OSApp_PF.RatificaConsumoOS_PreAnalise(nrcosFormula, QrOSSrvControle.Value,
  FatID, Pergunta) then
  begin
    if Geral.MB_Pergunta(
    'Confirma a ratifica��o dos consumos ainda n�o ratificados de todas f�rmulas deste servi�o?') =
    ID_YES then
    begin
      RatificaConsumo('osfrmcab', 'osfrmrec', 'Controle', QrOSSrvControle.Value, VAR_FATID_4101);
      RatificaConsumo('osmoncab', 'osmonrec', 'Controle', QrOSSrvControle.Value, VAR_FATID_4102);
      //
      OSApp_PF.ReopenOSSrv(QrOSSrv, QrOSCabCodigo.Value, 0);
    end;
  end;
end;

procedure TFmOSCab2.Atual1Click(Sender: TObject);
begin
  ExclusaoIncondicional(nivexclcaixasUni);
  //
  //TotalValorOS(QrOSCabCodigo.Value, gboCaixa);
  OSApp_PF.TotalValorOS(QrOSCabCodigo.Value);
  LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
    QrOSCabCodigo.Value);
end;

procedure TFmOSCab2.Atual2Click(Sender: TObject);
begin
  ExclusaoIncondicional(nivexclMonitsUni);
end;

procedure TFmOSCab2.Atual3Click(Sender: TObject);
begin
  ExcluiOSFrmCab(istAtual);
end;

procedure TFmOSCab2.Atual4Click(Sender: TObject);
begin
  if Geral.MB_Pergunta(
  'Confirma a exclus�o da perguta selecionada e de sua resposta?') = ID_YES then
  begin
    ExcluiPerguntasERespostas('Conta', QrOSPipItsConta.Value);
    OSApp_PF.ReopenOSPipMon(QrOSPipMon, QrOSCabCodigo.Value, QrOSPipMonControle.Value);
  end;
end;

procedure TFmOSCab2.VerificaRespons;
begin
  if (QrOSAge.State <> dsInactive) and (QrOSAge.RecordCount > 0) then
  begin
    if not QrOSAge.Locate('Responsa', 1, []) then
    begin
      QrOSAge.First;
      AtualizaRespons;
    end;
  end;
end;

procedure TFmOSCab2.AtualizaRespons;
var
  Codigo, Controle, Agente, Responsa: Integer;
begin
  if (QrOSAge.State <> dsInactive) and (QrOSAge.RecordCount > 0) then
  begin
    Screen.Cursor := crHourGlass;
    try
      Codigo   := QrOSAgeCodigo.Value;
      Controle := QrOSAgeControle.Value;
      Responsa := 0;
      //
      // Zerar todos "Responsa" da OS
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osage', False,
        ['Responsa'], ['Codigo'], [Responsa], [Codigo], True) then
      begin
        // Ativar "Responsa" do Agente selecionado
        Responsa := 1;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'osage', False,
          ['Responsa'], ['Controle'], [Responsa], [Controle], True);
      end;
      OSApp_PF.ReopenOSAge(QrOSAge, QrOSCabCodigo.Value, Controle);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmOSCab2.ItsAltera6Click(Sender: TObject);
begin
  MostraOSCxI(stUpd);
end;

procedure TFmOSCab2.BtAlteraVctClick(Sender: TObject);
var
  Data: TDateTime;
  Lancto: Largeint;
  Conta, Filial: Integer;
  TabLctA: String;
  Enab, Enab2: Boolean;
begin
  Enab  := (QrOSCab.State <> dsInactive) and (QrOSCab.RecordCount > 0);
  Enab2 := (QrLctFatRef.State <> dsInactive) and (QrLctFatRef.RecordCount > 0);

  if Enab and Enab2 then
  begin
    Lancto  := QrLctFatRef.FieldByName('Lancto').AsLargeInt;
    Filial  := QrOSCabEmpresa.Value;
    TabLctA := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
    Conta   := QrLctFatRef.FieldByName('Conta').AsInteger;

    if UBloquetos.VerificaSeFaturaEstaPaga(TabLctA, Lancto) then
    begin
      Geral.MB_Aviso('A parcela ID n�mero ' + Geral.FFI(Conta) + ' n�o pode ser editada!' +
        sLineBreak + 'Motivo: H� pagamento(s) lan�ado(s) nela!');
      Exit;
    end;
    if UBloquetos.VerificaSeBoletoExiste(Lancto) then
    begin
      Geral.MB_Aviso('A parcela ID n�mero ' + Geral.FFI(Conta) + ' n�o pode ser editada!' +
        sLineBreak + 'Motivo: Existe boleto gerado para ela!');
      Exit;
    end;
    //
    if not DBCheck.ObtemData(QrLctFatRef.FieldByName('Vencto').AsDateTime, Data, 0) then
      Exit;
    //
    if Geral.MB_Pergunta('Confirma a altera��o do vencimento para a data '
      + Geral.FDT(Data, 2) + '?') = ID_YES then
    begin
      if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, TabLctA, False,
        ['Vencimento'], ['Controle'], [Geral.FDT(Data, 1)],
        [Lancto], True) then
      begin
        Geral.MB_Aviso('Falha ao alterar lan�amento financeiro ID n�mero: '
          + Geral.FF0(Lancto));
        Exit;
      end else
      begin
        if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'lctfatref', False,
          ['Vencto'], ['Conta'], [Geral.FDT(Data, 1)], [Conta], True) then
        begin
          Geral.MB_Aviso('Falha ao alterar parcela do faturamento ID n�mero: '
            + Geral.FF0(Conta));
          Exit;
        end;
      end;
      OSApp_PF.ReopenLctFatRef(QrLctFatRef, QrOSCabCodigo.Value, Conta);
    end;
  end;
end;

procedure TFmOSCab2.BtAlternaClick(Sender: TObject);
var
  Lugar, Opcao, OSCab: Integer;
begin
  Lugar := QrLugaresSiapTerCad.Value;
  Opcao := QrOpcoesOpcao.Value;
  OSCab := QrOSCabCodigo.Value;
  case FDisposicao of
    dispPorLugar,
    dispIndefinido: ReopenVariantes(dispPorOpcao, Lugar, Opcao, True, OSCab);
    else ReopenVariantes(dispPorLugar, Lugar, Opcao, True, OSCab);
  end;
end;

procedure TFmOSCab2.BtOSCabClick(Sender: TObject);
begin
  PCGeral.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMOSCab, BtOSCab);
end;

procedure TFmOSCab2.FormCreate(Sender: TObject);
var
  MEsq: Integer;
begin
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FNaoReabreVariantes  := False;
  //
  FDisposicao  := dispIndefinido;
  FPos_PCGeral := 0;
  FPos_PCOpera := 0;
  FOSSrv       := 0;
  //
  ImgTipo.SQLType               := stLok;
  PCGeral.Align                 := alClient;
  PCGeral.ActivePageIndex       := 0;
  PCDedetizacao.ActivePageIndex := 0;
  PCFrmIts.ActivePageIndex      := 0;
  PCMonIts.ActivePageIndex      := 0;
  PnDedetizacao.Align           := alClient;
  PnCaixaDAgua.Align            := alClient;
  ScrollBox2.Align              := alClient;
  //
  CriaOForm;
  FSeq := 0;
  //
  DBGOSPrv.PopupMenu      := PMOSPrv;
  DBGOSSta.PopupMenu      := PMOSSta;
  DBGOSCabXtr.PopupMenu   := PMOSCabXtr;
  DBGDiarioAdd.PopupMenu  := PMDiarioAdd;
  DBGOSChk.PopupMenu      := PMOSChk;
  DBGOSPos.PopupMenu      := PMOSPos;
  DBGOSPrz.PopupMenu      := PMOSPrz;
  DBGOSSrv.PopupMenu      := PMOSSrv;
  DBGOSFrmCab.PopupMenu   := PMOSFrmCab;
  DBGOSFrmRec.PopupMenu   := PMOSFrmCab;
  DBGOSFrmAbr.PopupMenu   := PMOSFrmCab;
  DBGOSFrmEvo.PopupMenu   := PMOSFrmEvo;
  DBGOSFrmFlhCb.PopupMenu := PMOSFrmFlhCb;
  DBGOSFrmFlhDd.PopupMenu := PMOSFrmFlhDd;
  DBGOSMonCab.PopupMenu   := PMOSMonCab;
  DBGOSMonPipDd.PopupMenu := PMOSMonPipDd;
  DBGOSMonRec.PopupMenu   := PMOSMonCab;
  DBGOSCxa.PopupMenu      := PMOSCxa;
  DBGOSCxI.PopupMenu      := PMOSCxI;
  DBGOSPipMon.PopupMenu   := PMOSPipMon;
  DBGOSPipIts.PopupMenu   := PMOSPipIts;
  DBGrid2.PopupMenu       := PMOSExtMon;
  DBGOSCabAlv.PopupMenu   := PMOSCabAlv;
  DBGOSAge.PopupMenu      := PMOSAge;
  DBGOSFrmEPI.PopupMenu   := PMOSFrmEPI;
  DBGOSMonEPI.PopupMenu   := PMOSMonEPI;
  //
  DBGOSCabAlv.DataSource := DsOSCabAlv;
  DBGOSAge.DataSource := DsOSAge;
  //DBG.DataSource := Ds;
  //
  CBEntidade.ListSource   := DsEntidades;
  CBEstatus.ListSource    := DsEstatusOSs;
  CBSiapTerCad.ListSource := DsSiapTerCad;
  CBEnticontat.ListSource := DsEntiContat;
  CBFatoGeradr.ListSource := DsFatoGeradr;
  CBVendedor.ListSource   := DsVendedores;
  //CBCondicaoPg.ListSource := DsPediPrzCab;
  CBEntContrat.ListSource := DsEntContrat;
  CBEntPagante.ListSource := DsEntPagante;
  CBExeTxtCli1.ListSource := DsExeTxtCli1;
  CBExeTxtCli2.ListSource := DsExeTxtCli2;
  //
  UMyMod.ReAbreQuery(QrEntidades, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(QrEstatusOSs, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(QrSiapTerCad, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(QrFatoGeradr, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(QrEntContrat, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(QrEntPagante, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(QrExeTxtCli1, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(QrExeTxtCli2, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(QrAgentes, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(QrAgeEqiCab, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(QrVendedores, Dmod.MyDB, [], '');
  //
  DBGOSSrv.DataSource      := DsOSSrv;
  DBGOSAlv.DataSource      := DsOSAlv;
  DBGOSFrmCab.DataSource   := DsOSFrmCab;
  DBGOSFrmRec.DataSource   := DsOSFrmRec;
  DBGOSFrmEPI.DataSource   := DsOSFrmEPI;
  DBGOSFrmDep.DataSource   := DsOSFrmDep;
  DBGOSFrmAbr.DataSource   := DsOSFrmAbr;
  DBGOSMonCab.DataSource   := DsOSMonCab;
  DBGOSMonRec.DataSource   := DsOSMonRec;
  DBGOSMonEPI.DataSource   := DsOSMonEPI;
  DBGOSMonPipDd.DataSource := DsOSMonPipDd;
  //_DBG_OS_Mon_Dep_.DataSource   := _Ds_OS_Mon_Dep_;
  DBGOSPos.DataSource      := DsOSPos;
  DBGLctFatRef.DataSource  := DsLctFatRef;
  DBGEntiMail.DataSource   := DsEntiMail;
  DBGEntiTel.DataSource    := DsEntiTel;
  DBGOSCxa.DataSource      := DsOSCxa;
  DBGOSCxaAtrib.DataSource := DsOSCxaAtrib;
  DBGOSPipMon.DataSource   := DsOSPipMon;
  DBGOSCxI.DataSource      := DsOSCxI;
  //
  DBEdNOME_FORMA.DataSource  := DsOSCxa;
  DBEdNO_MATERIAL.DataSource := DsOSCxa;
  DBEdVolumeL.DataSource     := DsOSCxa;
  DBEdMedidas.DataSource     := DsOSCxa;
  DBMeAcesso.DataSource      := DsOSCxa;
  DBMeOSCxI.DataSource       := DsOSCxI;
  DBMeOSCxa.DataSource       := DsOSCxa;
  //
  MyObjects.ConfiguraCheckGroup(CGOperacao, sListaOperacoesBugs, 3, 0, False);
  //
  UnDmkDAC_PF.AbreQuery(QrMobiliCad, Dmod.MyDB);
  UnDmkDAC_PF.AbreMySQLQuery0(QrPediPrzCab, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome, MaxDesco, ',
  'JurosMes, Parcelas, Percent1, Percent2 ',
  'FROM pediprzcab ',
  //'WHERE Aplicacao & :P0 <> 0 ',
  'ORDER BY Nome ',
  '']);
  //
  MEsq := Geral.ReadAppKeyLM('LugarOpcao', Application.Title, ktInteger, 173);
  if MEsq < 90 then
    PnLugarOpcao.Width := 90
  else
    PnLugarOpcao.Width := MEsq;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  //
  Adiodenovolugar1.Visible := False; //N�o usa
end;

procedure TFmOSCab2.SbNumeroClick(Sender: TObject);
begin
  DefParams;
  MyObjects.MostraPopUpDeBotao(PMPesqNum, SbNumero);
end;

procedure TFmOSCab2.SbImprimeClick(Sender: TObject);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrFaltaToImp, Dmod.MyDB, [
    'SELECT COUNT(Codigo) ITENS',
    'FROM diarioadd ',
    'WHERE PrintOS=-1',
    'AND Depto IN ( ',
    '     SELECT Codigo ',
    '     FROM oscab  ',
    '     WHERE Grupo=' + Geral.FF0(QrGruposGrupo.Value) + ') ',
    'ORDER BY Codigo DESC ',
    '']);
  //
  if QrFaltaToImpITENS.Value > 0 then
  begin
    Geral.MB_Aviso('CUIDADO!! Falta definir impress�o de ' +
      Geral.FF0(QrFaltaToImpITENS.Value) + ' itens de interlocu��es!');
    //
    PCGeral.ActivePageIndex := 5
  end;  
  //
  MyObjects.MostraPopUpDeBotao(PMOSImp, SbImprime);
end;

procedure TFmOSCab2.MostraOSImp(Aba: Integer; Cfg1: Variant);
begin
  if QrOSAge.RecordCount = 0 then
    Geral.MB_Aviso('Informe pelo menos um agente!')
  else begin
    if DBCheck.CriaFm(TFmOsImp2, FmOsImp2, afmoNegarComAviso) then
    begin
      //FmOsImp2.EdOSCab.ValueVariant  := QrOSCabCodigo.Value;
      FmOsImp2.FCliente              := QrOSCabEntidade.Value;
      FmOsImp2.FContratante          := QrOSCabEntContrat.Value;
      FmOsImp2.FPagante              := QrOSCabEntPagante.Value;
      //
      FmOsImp2.EdGrupo.ValueVariant  := QrGruposGrupo.Value;
      FmOsImp2.EdCliCod.ValueVariant := QrOSCabEntidade.Value;
      FmOsImp2.EdCliNom.Text         := QrOSCabNO_ENT.Value;
      FmOsImp2.EdTel_ENT.Text        := Geral.FormataTelefone_TT_Curto(QrOSCabTXTTel_ENT.Value);
      FmOsImp2.PageControl1.ActivePageIndex := Aba;
      case Aba of
        1: FmOsImp2.CkLugar.Checked := Cfg1;
      end;
      //
      FmOsImp2.ShowModal;
      FmOsImp2.Destroy;
      //
      ReopenOSPrn(QrOSPrnControle.Value);
    end;
  end;
end;

procedure TFmOSCab2.SbNomeClick(Sender: TObject);
begin
  DefParams;
  MostraOSPsq();
end;

procedure TFmOSCab2.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrGruposGrupo.Value, LaRegistro.Caption);
end;

procedure TFmOSCab2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOSCab2.FAES1Click(Sender: TObject);
begin
  MostraOSImp(7, Null);
end;

procedure TFmOSCab2.Fatura1Click(Sender: TObject);
begin
  if Myobjects.FIC(QrOSCabEntPagante.Value = 0, nil,
  'Faturamento n�o realizado! Pagante n�o informado!') then
    Exit;
  MostraFormOSCabFat(stIns);
end;

procedure TFmOSCab2.FechaOS();
begin
  if QrOSCab.State <> dsInactive then
    QrOSCab.Close;
end;

procedure TFmOSCab2.Fichadeexecuo1Click(Sender: TObject);
begin
  if not
    OSApp_PF.ContinuaSemDefinicaoDeParzoDePagamento(QrGruposGrupo.Value, True)
    then
      Exit;
  //
  if OSApp_PF.TemProvidenciaOrfa(QrOSCabEntidade.Value) then
    OSApp_PF.MostraFormOSPrvSel(stIns, QrOSCabCodigo.Value, QrOSPrv,
    QrOSCAbEntidade.Value);
  //
  MostraOSImp(4, Null);
end;

procedure TFmOSCab2.Fichasdevistoria1Click(Sender: TObject);
begin
  MostraOSImp(2, Null);
end;

procedure TFmOSCab2.SbQueryClick(Sender: TObject);
begin
  DefParams;
  MostraOSPsq();
end;

procedure TFmOSCab2.Selecionados1Click(Sender: TObject);
  procedure ExcluiAtual();
  begin
    ExcluiPerguntasERespostas('Controle', QrOSPipMonControle.Value);
    //
    UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
    'INSERT INTO livre2 (Tabela, Campo, Codigo, Vezes, AlterWeb, Ativo) ',
    'SELECT "ospipmon" Tabela, "Controle" Campo, ',
    'Controle Codigo, 1 Vezes, 0 AlterWeb, 0 Ativo ',
    'FROM ospipmon ',
    'WHERE Controle=' + Geral.FI64(QrOSPipMonControle.Value),
    ';',
    'DELETE FROM ospipmon ',
    'WHERE Controle=' + Geral.FI64(QrOSPipMonControle.Value),
    '']);
  end;
var
  I: Integer;
begin
  if Geral.MB_Pergunta('Remove os PMVs selecionados?') = ID_YES then
  begin
    if DBGOSPipMon.SelectedRows.Count > 1 then
    begin
      with DBGOSPipMon.DataSource.DataSet do
      for I:= 0 to DBGOSPipMon.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGOSPipMon.SelectedRows.Items[i]));
        ExcluiAtual();
      end;
    end else
      ExcluiAtual();
    //
    QrOSPipMon.Next;
    OSApp_PF.ReopenOSPipMon(QrOSPipMon, QrOSCabCodigo.Value, QrOSPipMonControle.Value);
  end;
end;

procedure TFmOSCab2.Servioatual1Click(Sender: TObject);
begin
{
  if Geral.MensagemBox('Confirma a exclus�o do servi�o " ' +
  QrOSSrvNO_DesServico.Value + '"?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then
    DBCheck.ExcluiRegistro(Dmod.QrUpd, QrOSSrv, 'OSSrv', ['Controle'], ['Controle'], True);
  //
}
  ExclusaoIncondicional(nivexclDedetsUni);
  //TotalValorOS(QrOSCabCodigo.Value, gboServi);
  OSApp_PF.TotalValorOS(QrOSCabCodigo.Value);
  LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
    QrOSCabCodigo.Value);
end;

procedure TFmOSCab2.SetaImprimivelItemConversa(Imprime: Boolean);
var
  Continua: Boolean;
  PrintOS, Codigo: Integer;
begin
  Continua :=  QrDiarioAddPrintOS.Value = -1;
  if not Continua then
    Continua := Imprime <> Geral.IntToBool(QrDiarioAddPrintOS.Value);
  if Continua then
  begin
    PrintOS := Geral.BoolToInt(Imprime);
    Codigo  := QrDiarioAddCodigo.Value;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'diarioadd', False, [
    'PrintOS'], ['Codigo'], [
    PrintOS], [Codigo], True) then
      ReopenDiarioAdd(Codigo);
  end;
end;

procedure TFmOSCab2.Sim1Click(Sender: TObject);
begin
  SetaImprimivelItemConversa(True);
end;

procedure TFmOSCab2.Somenteseusservios1Click(Sender: TObject);
begin
  ExclusaoIncondicional(nivexclServicosAll);
end;

procedure TFmOSCab2.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSCab2.FormShow(Sender: TObject);
begin
  FmMyGlyfs.DefineGlyfsTDI(FmPrincipal.sd1, Sender);
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    OSApp_PF.ReopenLocOS_Idx(DmModOS.QrLocOS, FCabIni);
    LocCod(DmModOS.QrLocOSGrupo.Value, DmModOS.QrLocOSGrupo.Value, FDisposicao,
      DmModOS.QrLocOSSiapTerCad.Value, DmModOS.QrLocOSOpcao.Value, True,
      DmModOS.QrLocOSCodigo.Value);
    if QrOSCabCodigo.Value <> FCabIni then Geral.MensagemBox(
    'A OS ' + Geral.FF0(FCabIni) + 'n�o foi localizada!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
  if FOSSrv <> 0 then
  begin
    PCGeral.ActivePageIndex := 1;
    PCOpera.ActivePageIndex := 0;
    //
    QrOSSrv.Locate('Controle', FOSSrv, []);
  end;
end;

procedure TFmOSCab2.Frmulaoatual1Click(Sender: TObject);
begin
  ExcluiOSMonCab(istAtual);
end;

function TFmOSCab2.frxGER_OSERV_001_001UserFunction(const MethodName: string;
  var Params: Variant): Variant;
{
var
  Tabela: String;
  Coluna, IdxFld, Ultima, MaxCol, QtdReg: Integer;
}
begin
{
  if AnsiCompareText(MethodName, 'CorMemoMatrix') = 0 then
  begin
    Tabela := Params[0];
    Coluna := Params[1];
    IdxFld := Params[2];
    //
    Result := clFuchsia;
    //
    if Tabela = 'osdep' then
    begin
      MaxCol := CO_IMP_COLS_DEP;
      QtdReg := QrOSDep.RecordCount;
    end else
    if Tabela = 'osalv' then
    begin
      MaxCol := CO_IMP_COLS_ALV;
      QtdReg := QrOSAlv.RecordCount;
    end;
    Ultima := QtdReg mod MaxCol;
    //
    if Ultima < MaxCol then
    begin
      case IdxFld of
        1: Result := CorMedia;
        2: Result := CorFraca;
        3: Result := CorFundo;
      end;
    end else
      Result := clBlack;
  end;
}
end;

procedure TFmOSCab2.Gerabloqueto1Click(Sender: TObject);
var
  Entidade, Filial: Integer;
  TabLctA: String;
begin
  try
    Screen.Cursor         := crHourGlass;
    Gerabloqueto1.Enabled := False;
    //
    Filial   := QrOSCabEmpresa.Value;
    Entidade := DmFatura.ObtemEntidadeDeFilial(Filial);
    TabLctA  := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
    //
    UBloquetos.BloquetosParcelasByFatNum(Entidade, VAR_FatID_4001,
      QrOSCabCodigo.Value, QrOSCabCodigo.Value, TabLctA, True, PB1);
  finally
    Gerabloqueto1.Enabled := True;
    Screen.Cursor         := crDefault;
  end;
end;

procedure TFmOSCab2.Gerarperguntas1Click(Sender: TObject);
var
  OSCab, Controle: Integer;
  Data: TDateTime;
begin
  PB1.Visible := True;
  //
  OSCab    := QrOSCabCodigo.Value;
  Controle := QrOSPipMonControle.Value;
  Data     := QrOSCabDtaExeIni.Value;
  //
  DmModOS.GeraPerguntasPIPs(QrOsCabCodigo.Value, QrOsCabSohInicial.Value,
    QrOsCabSiapTerCad.Value, Data, PB1, LaAviso1, LaAviso2);
  PB1.Visible := False;
  //
  OSApp_PF.ReopenOSCab(QrOSCab, OSCab);
  QrOSPipMon.Locate('Controle', Controle, []);
end;

procedure TFmOSCab2.GuardaPosicoes();
begin
  FPos_PCGeral := PCGeral.ActivePageIndex;
  FPos_PCOpera := PCOpera.ActivePageIndex;
end;

procedure TFmOSCab2.CabInclui1Click(Sender: TObject);
var
  Data: TDateTime;
  Hora: TTime;
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  ConfiguraCorStatus(0, 0, TEdit(EdEstatusCor), nil);
  //
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOSCab, [PnDados],
    [PnEdita], EdEntidade, ImgTipo, 'oscab');

  DModG.ObtemAgora();
  DModG.ObtemDataHora(Data, Hora);

  EdEmpresa.ValueVariant    := DModG.QrFiliLogFilial.Value;
  CBEmpresa.KeyValue        := DModG.QrFiliLogFilial.Value;
  TPDtaContat.Date          := Data;
  EdDtaContat.Text          := FormatDateTime('hh:nn', Hora);
  CGOperacao.Value          := 0;
  EdExeTxtCli1.ValueVariant := Dmod.QrOpcoesBugsOSPrvETC.Value;
  CBExeTxtCli1.KeyValue     := Dmod.QrOpcoesBugsOSPrvETC.Value;
  EdExeTxtCli2.ValueVariant := Dmod.QrOpcoesBugsOSPrvOTC.Value;
  CBExeTxtCli2.KeyValue     := Dmod.QrOpcoesBugsOSPrvOTC.Value;
  EdAgeEqiCab.Enabled       := False;
  CBAgeEqiCab.Enabled       := False;
  EdAgeEqiCab.ValueVariant  := -1;
  CBAgeEqiCab.KeyValue      := -1;
  PnLugarOpcao.Visible      := False;
end;

procedure TFmOSCab2.Cadastrodeagentes1Click(Sender: TObject);
var
  Agen: Integer;
begin
  if (QrOSAge.State <> dsInactive) and (QrOSAge.RecordCount > 0) then
    Agen := QrOSAgeAgente.Value
  else
    Agen := 0;
  //
  DmodG.CadastroDeEntidade(Agen, fmcadEntidade2, fmcadEntidade2, False, True,
    FmPrincipal.PageControl1, FmPrincipal.AdvToolBarPager1, False, uetFornece2);
end;

procedure TFmOSCab2.Cadastrodecliente1Click(Sender: TObject);
begin
  MostraOSImp(0, Null);
end;

procedure TFmOSCab2.Cadastrodeperguntas1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrOSPipMonPrgLstCab.Value;
  //
  FmPrincipal.MostraFormPrgLstCab(Codigo, 0);
end;

procedure TFmOSCab2.CadastrodoPMV1Click(Sender: TObject);
begin
  FmPrincipal.MostraFormPipCad(QrOSPipMonPipCad.Value);
end;

procedure TFmOSCab2.Cadatrodeprodutos1Click(Sender: TObject);
begin
  Grade_Jan.MostraFormGraGruN(0);
end;

procedure TFmOSCab2.CBEntContratKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  OSAll_PF.CopiaCliente(Key, EdEntidade, EdEntContrat, CBEntContrat);
end;

procedure TFmOSCab2.CBEntPaganteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  OSAll_PF.CopiaCliente(Key, EdEntidade, EdEntPagante, CBEntPagante);
end;

procedure TFmOSCab2.CBSiapTerCadEnter(Sender: TObject);
begin
  ReopenContato();
end;

procedure TFmOSCab2.CGOperacaoClick(Sender: TObject);
begin
  MyObjects.Entitula(GBOperacao, [LaOperacaoA, LaOperacaoB, LaOperacaoC],
  [], ' ' + OSApp_PF.NomeDeOperacao(CGOperacao.Value), False, taLeftJustify, 2, 10, 20);
end;

procedure TFmOSCab2.ComprovantedeExecues1Click(Sender: TObject);
begin
  MostraOSImp(6, Null);
end;

procedure TFmOSCab2.Comsegurana1Click(Sender: TObject);
var
  I, N: Integer;
  DS: TDataSource;
  Qr: TmySQLQuery;
begin
  N := 0;
  for I := 0 to ComponentCount - 1 do
  begin
    if ((Components[I] is TCustomDBGrid)) then
    begin
      if (TCustomDBGrid(Components[i]).DataSource <> DsOSCab) then
      begin
        DS := TCustomDBGrid(Components[i]).DataSource;
        if DS <> nil then
        begin
          Qr := TmySQLQuery(DS.DataSet);
          if Qr <> nil then
          begin
            if Qr.State <> dsInactive then
            begin
              if (Qr.Name = 'QrLugares')
              or (Qr.Name = 'QrOpcoes')
              or (Qr.Name = 'QrEntiTel')
              or (Qr.Name = 'QrEntiMail') then
                // nada
              else
              if Qr.RecordCount > 0 then
                N := N + 1;
            end;
          end;
        end;
      end;
    end;
  end;
  if N > 0 then
    Geral.MB_Aviso('Exclus�o cancelada! Existem ' + Geral.FF0(N) +
     ' tabelas com registros inseridos para esta ordem de servi�o!')
  else begin
    if UMyMod.ExcluiRegistroInt1(
    'Confirma a exclus�o da OS atual?',
    'oscab', 'Codigo', QrOSCabCodigo.Value, Dmod.MyDB) = ID_YES then
  LocCod(QrGruposGrupo.Value, QrGruposGrupo.Value, FDisposicao, 0, 0, True,
    QrOSCabCodigo.Value);
  end;
end;

function TFmOSCab2.CondicaoPGSelecionado(): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Condicao ',
  'FROM osprz ',
  'WHERE Codigo=' + Geral.FF0(QrOSCabCodigo.Value),
  'AND Escolhido=1',
  ' ',
  '']);
  //
  Result := Dmod.QrAux.FieldByName('Condicao').AsInteger;
end;

procedure TFmOSCab2.ConfiguraCorStatus(AgeCorIni, AgeCorFon: Integer; Edit1,
  Edit2: TEdit);
begin
  if AgeCorIni = 0 then
  begin
    if Edit1 <> nil then
      Edit1.Color := clWindow;
    if Edit2 <> nil then
      Edit2.Color := clWindow;
  end else
  begin
    if Edit1 <> nil then
      Edit1.Color := AgeCorIni;
    if Edit2 <> nil then
      Edit2.Color := AgeCorIni;
  end;
  if AgeCorFon = 0 then
  begin
    if Edit1 <> nil then
      Edit1.Font.Color := clWindowText;
    if Edit2 <> nil then
      Edit2.Font.Color := clWindowText;
  end else
  begin
    if Edit1 <> nil then
      Edit1.Font.Color := AgeCorFon;
    if Edit2 <> nil then
      Edit2.Font.Color := AgeCorFon;
  end;
end;

{
object Label16: TLabel
  Left = 916
  Top = 56
  Width = 78
  Height = 13
  Caption = 'OS Origem [F4]:'
  Color = clBtnFace
  ParentColor = False
end
object EdOSOrigem: TdmkEdit
  Left = 916
  Top = 72
  Width = 80
  Height = 21
  Alignment = taRightJustify
  ReadOnly = True
  TabOrder = 12
  FormatType = dmktfInteger
  MskType = fmtNone
  DecimalSize = 0
  LeftZeros = 0
  NoEnterToTab = False
  NoForceUppercase = False
  ForceNextYear = False
  DataFormat = dmkdfShort
  HoraFormat = dmkhfShort
  Texto = '0'
  QryCampo = 'OSOrigem'
  UpdCampo = 'OSOrigem'
  UpdType = utYes
  Obrigatorio = False
  PermiteNulo = False
  ValueVariant = 0
  OnKeyDown = EdOSOrigemKeyDown
end
}

//nao permitir data final de termino da os caso haja consumos nao ratificados!

//Usar PosGerou como forma de cancelar a emissao de oss filhas

end.

