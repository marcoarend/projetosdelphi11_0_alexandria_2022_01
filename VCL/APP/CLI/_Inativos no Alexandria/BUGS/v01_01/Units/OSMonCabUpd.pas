unit OSMonCabUpd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, AdvObj, BaseGrid, AdvGrid, DBAdvGrid, UnAppListas,
  dmkRadioGroup, UnBugs_Tabs, UnDmkEnums, UnProjGroup_Consts;

type
  TFmOSMonCabUpd = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    GBDados: TGroupBox;
    Label7: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit15: TDBEdit;
    QrOSSrv: TmySQLQuery;
    QrOSSrvNO_DesServico: TWideStringField;
    QrOSSrvCodigo: TIntegerField;
    QrOSSrvControle: TIntegerField;
    QrOSSrvDesServico: TIntegerField;
    QrOSSrvLk: TIntegerField;
    QrOSSrvDataCad: TDateField;
    QrOSSrvDataAlt: TDateField;
    QrOSSrvUserCad: TIntegerField;
    QrOSSrvUserAlt: TIntegerField;
    QrOSSrvAlterWeb: TSmallintField;
    QrOSSrvAtivo: TSmallintField;
    DsOSSrv: TDataSource;
    QrFormulas: TmySQLQuery;
    DsFormulas: TDataSource;
    GBEdita: TGroupBox;
    Label1: TLabel;
    Label9: TLabel;
    SbFormulas: TSpeedButton;
    Label3: TLabel;
    EdConta: TdmkEdit;
    EdNome: TdmkEdit;
    CBFormula: TdmkDBLookupComboBox;
    EdFormula: TdmkEditCB;
    Label2: TLabel;
    EdEquipAplic: TdmkEditCB;
    CBEquipAplic: TdmkDBLookupComboBox;
    SbEquipAplic: TSpeedButton;
    EdQtdTot: TdmkEdit;
    Label6: TLabel;
    EdQtdQSP: TdmkEdit;
    Label8: TLabel;
    QrFormulasCodigo: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrEquipAplic: TmySQLQuery;
    DsEquipAplic: TDataSource;
    QrEquipAplicControle: TIntegerField;
    QrEquipAplicNome: TWideStringField;
    Label10: TLabel;
    EdPipCad: TdmkEditCB;
    CBPipCad: TdmkDBLookupComboBox;
    SBPIPCad: TSpeedButton;
    QrPIPs: TmySQLQuery;
    DsPIPs: TDataSource;
    QrPIPsCodigo: TIntegerField;
    QrPIPsNome: TWideStringField;
    QrPIPsEquipamento: TIntegerField;
    QrPIPsOSMonCab: TIntegerField;
    QrTipoAplica: TmySQLQuery;
    QrTipoAplicaCodigo: TIntegerField;
    QrTipoAplicaNome: TWideStringField;
    DsTipoAplica: TDataSource;
    Label11: TLabel;
    EdTipoAplica: TdmkEditCB;
    CBTipoAplica: TdmkDBLookupComboBox;
    SBTipoAplica: TSpeedButton;
    RGDiluente: TdmkRadioGroup;
    QrFormulasEquipAplic: TIntegerField;
    QrFormulasQtdTot: TFloatField;
    QrFormulasQtdQSP: TFloatField;
    QrFormulasCusTot: TFloatField;
    QrFormulasAplicacao: TIntegerField;
    QrFormulasDiluente: TSmallintField;
    QrFormulasTipoAplica: TIntegerField;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    Label12: TLabel;
    EdUnidMed: TdmkEditCB;
    EdSigla: TdmkEdit;
    CBUnidMed: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    QrFormulasCU_UNIDMED: TIntegerField;
    QrFormulasSIGLA: TWideStringField;
    Label13: TLabel;
    EdPerioDd: TdmkEdit;
    Label14: TLabel;
    EdDdPostero: TdmkEdit;
    Label15: TLabel;
    Label16: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbFormulasClick(Sender: TObject);
    procedure SbEquipAplicClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBPIPCadClick(Sender: TObject);
    procedure SBTipoAplicaClick(Sender: TObject);
    procedure EdFormulaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FSIapTerCad, FEntidade,
    FPipCad, FCodigo, FControle, FConta: Integer;
    FQrOSMonCab, FQrOSMonRec: TmySQLQuery;
    //
    procedure ReopenPIPs(Codigo: Integer);
  end;

  var
  FmOSMonCabUpd: TFmOSMonCabUpd;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModuleGeral,
  Principal, ModProd, UnidMed, MyDBCheck, UnOSApp_PF;

{$R *.DFM}

procedure TFmOSMonCabUpd.BtOKClick(Sender: TObject);
const
  IndicUso = CO_FRM_INDICUSO_001_COD_UsoDefinitivo;
var
  Nome: String;
  Codigo, Controle, Conta, Formula, EquipAplic, PipCad, TipoAplica,
    Diluente, UnidMed, PerioDd, DdPostero: Integer;
  //CusTot,
  QtdTot, QtdQSP: Double;
begin
  Codigo         := FCodigo;
  Controle       := FControle;
  Nome           := EdNome.Text;
  Formula        := EdFormula.ValueVariant;
  EquipAplic     := EdEquipAplic.ValueVariant;
  QtdTot         := EdQtdTot.ValueVariant;
  QtdQSP         := EdQtdQSP.ValueVariant;
  PipCad         := EdPipCad.ValueVariant;
  PerioDd        := EdPerioDd.ValueVariant;
  DdPostero      := EdDdPostero.ValueVariant;
  //CusTot         := ;
  TipoAplica     := EdTipoAplica.ValueVariant;
  Diluente       := RGDiluente.ItemIndex;
  if EdUnidMed.ValueVariant <> 0 then
    UnidMed := QrUnidMedCodigo.Value
  else
    Unidmed := 0;
  //
  if MyObjects.FIC(PipCad = 0, EdPipCad, 'Defina o PIP!') then
    Exit;
  //
  if MyObjects.FIC(PerioDd = 0, EdPerioDd,
  'Defina o "Per�odo dias" mesmo para monitoramentos contratuais!' + sLineBreak +
  'Isso evitar� uma verifica��o demorada ao criar OSs filhas de monitoramento!') then
    Exit;
  //
  Conta := EdConta.ValueVariant;
  Conta := UMyMod.BPGS1I32('osmoncab', 'Conta', '', '', tsDef,
    ImgTipo.SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'osmoncab', False, [
  'Codigo', 'Controle', 'Nome',
  'Formula', 'EquipAplic', 'QtdTot',
  'QtdQSP'(*, 'CusTot'*), 'pipcad',
  'TipoAplica', 'Diluente', 'UnidMed',
  'PerioDd', 'DdPostero', 'IndicUso'], [
  'Conta'], [
  Codigo, Controle, Nome,
  Formula, EquipAplic, QtdTot,
  QtdQSP(*, CusTot*), PipCad,
  TipoAplica, Diluente, UnidMed,
  PerioDd, DdPostero, IndicUso], [
  Conta], True) then
  begin
    OSApp_PF.ReopenOSMonCab(FQrOSMonCab, Controle, Conta);
    if ImgTipo.SQLType = stIns then
      FConta := Conta;
    if PipCad <> 0 then
      Dmod.AtualizaPIP(PipCad);
    Close;
  end;
end;

procedure TFmOSMonCabUpd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSMonCabUpd.EdFormulaChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    EdQtdTot.ValueVariant := QrFormulasQtdTot.Value;
    EdEquipAplic.ValueVariant := QrFormulasEquipAplic.Value;
    CBEquipAplic.KeyValue := QrFormulasEquipAplic.Value;
    EdTipoAplica.ValueVariant := QrFormulasTipoAplica.Value;
    CBTipoAplica.KeyValue := QrFormulasTipoAplica.Value;
    RGDiluente.ItemIndex := QrFormulasDiluente.Value;
    EdSigla.Text := QrFormulasSIGLA.Value;
    EdUnidMed.ValueVariant := QrFormulasCU_UnidMed.Value;
    CBUnidMed.KeyValue := QrFormulasCU_UnidMed.Value;
  end;
end;

procedure TFmOSMonCabUpd.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  EdNome.Text := CBFormula.Text;
end;

procedure TFmOSMonCabUpd.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    DmProd.PesquisaPorSigla(False, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmOSMonCabUpd.EdSiglaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmOSMonCabUpd.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);
end;

procedure TFmOSMonCabUpd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSMonCabUpd.FormCreate(Sender: TObject);
begin
  FConta := 0;
  UnDMkDAC_PF.AbreMySQLQuery0(QrFormulas, Dmod.MyDB, [
  'SELECT frm.*, med.CodUsu CU_UNIDMED, med.SIGLA ',
  'FROM formulas frm ',
  'LEFT JOIN unidmed med ON med.Codigo=frm.UnidMed ',
  'WHERE frm.Aplicacao & 2 ',
  'ORDER BY frm.Nome ',
  '']);
  UMyMod.AbreQuery(QrTipoAplica, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  OSApp_PF.ReopenEquipAplic(QrEquipAplic, gbsMonitora);
end;

procedure TFmOSMonCabUpd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSMonCabUpd.FormShow(Sender: TObject);
begin
  // Precisa do SQLType
  ReopenPIPs(0);
end;

procedure TFmOSMonCabUpd.ReopenPIPs(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPIPs, Dmod.MyDB, [
  'SELECT * ',
  'FROM pipcad ',
  'WHERE (OSMonCab=0 ',
  //'AND MotDesativ=0 ',
  'AND MotInutili=0 ',
  'AND (DtaInutili < "1900-01-01" ',
  '  OR DtaInutili IS NULL)) ',
  Geral.ATS_If(ImgTipo.SQLType = stUpd, [
    'OR Codigo=' + Geral.FF0(FPipCad)]),
  'ORDER BY Nome ',
  '']);
end;

procedure TFmOSMonCabUpd.SbEquipAplicClick(Sender: TObject);
begin
  //FmPrincipal.MostraFormGraG1EqMo();
  FmPrincipal.MostraFormGraG1Equi(gbsMonitora, 0);
  UMyMod.SetaCodigoPesquisado(EdEquipAplic, CBEquipAplic, QrEquipAplic,
    VAR_CADASTRO, 'Controle');
end;

procedure TFmOSMonCabUpd.SbFormulasClick(Sender: TObject);
var
  Formula: Integer;
begin
  VAR_CADASTRO := 0;
  Formula      := EdFormula.ValueVariant;
  //
  FmPrincipal.MostraFormFormulas(FQrOSMonRec, 0, Formula);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdFormula, CBFormula, QrFormulas, VAR_CADASTRO);
    EdFormula.SetFocus;
  end;
end;

procedure TFmOSMonCabUpd.SBPIPCadClick(Sender: TObject);
var
  Equipamento: Integer;
begin
  VAR_CADASTRO := 0;
  Equipamento  := EdEquipAplic.ValueVariant;
  //
  FmPrincipal.MostraFormPipRapido(0, stIns, FEntidade, Equipamento, False);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdPipCad, CBPipCad, QrPIPs, VAR_CADASTRO, 'Codigo');
    EdPipCad.SetFocus;
  end;
end;

procedure TFmOSMonCabUpd.SBTipoAplicaClick(Sender: TObject);
begin
  FmPrincipal.MostraFormTipoAplica(0);
  UMyMod.SetaCodigoPesquisado(EdTipoAplica, CBTipoAplica, QrTipoAplica, VAR_CADASTRO);
end;

procedure TFmOSMonCabUpd.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO,
      'Codigo', 'CodUsu');
    end;
  end;
end;

end.
