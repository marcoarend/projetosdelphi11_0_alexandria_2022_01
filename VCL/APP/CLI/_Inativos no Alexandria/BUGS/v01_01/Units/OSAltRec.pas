unit OSAltRec;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkEdit, mySQLDbTables, UnDmkEnums;

type
  TFmOSAltRec = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    EdCodigo: TdmkEdit;
    Label1: TLabel;
    EdControle: TdmkEdit;
    Label2: TLabel;
    EdConta: TdmkEdit;
    Label3: TLabel;
    EdGraGruX: TdmkEdit;
    Label4: TLabel;
    EdNO_Serv: TdmkEdit;
    EdNO_Frm: TdmkEdit;
    EdNO_GG1: TdmkEdit;
    Panel6: TPanel;
    Label5: TLabel;
    EdUsoQtd: TdmkEdit;
    Label6: TLabel;
    EdUsoCusUni: TdmkEdit;
    EdUsoCusTot: TdmkEdit;
    Label7: TLabel;
    Label8: TLabel;
    EdIDIts: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdUsoQtdChange(Sender: TObject);
    procedure EdUsoCusUniChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FTabela: String;
    FFatID: Integer;
    FQry: TmySQLQuery;
  end;

  var
  FmOSAltRec: TFmOSAltRec;

implementation

uses UnMyObjects, Module, ModProd, UMySQLModule, DmkDAC_PF, ModOS, ModuleGeral;

{$R *.DFM}

procedure TFmOSAltRec.BtOKClick(Sender: TObject);
var
  IDIts: Integer;
  UsoQtd, UsoCusUni, UsoCusTot: Double;
begin
  IDIts     := EdIDIts.ValueVariant;
  UsoQtd    := EdUsoQtd.ValueVariant;
  UsoCusUni := EdUsoCusUni.ValueVariant;
  UsoCusTot := EdUsoCusTot.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, FTabela, False, [
    (*'GraGruX', 'PrvQtd', 'PrvPrc',
    'PrvVal',*) 'UsoQtd', (*'UsoPrc',
    'UsoVal', 'UsoDec', 'UsoTot',
    'Ordem', 'Reordem', 'ValCliDd',
    'EhDiluente',*) 'UsoCusUni', 'UsoCusTot'(*,
    'Desativado', 'RatifUso'*)], [
    'IDIts'], [
    (*GraGruX, PrvQtd, PrvPrc,
    PrvVal,*) UsoQtd, (*UsoPrc,
    UsoVal, UsoDec, UsoTot,
    Ordem, Reordem, ValCliDd,
    EhDiluente,*) UsoCusUni, UsoCusTot(*,
    Desativado, RatifUso*)], [
    IDIts], True) then
  begin
    DmModOS.BaixaInsumoDeOS(FFatID, IDIts, DModG.ObtemAgora());
    UnDmkDAC_PF.AbreQuery(FQry, Dmod.MyDB);
    FQry.Locate('IDIts', IDIts, []);
    //
    Close;
  end;
end;

procedure TFmOSAltRec.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSAltRec.EdUsoCusUniChange(Sender: TObject);
begin
  DmProd.CalculaCustoProduto(EdUsoQtd, EdUsoCusUni, EdUsoCusTot);
end;

procedure TFmOSAltRec.EdUsoQtdChange(Sender: TObject);
begin
  DmProd.CalculaCustoProduto(EdUsoQtd, EdUsoCusUni, EdUsoCusTot);
end;

procedure TFmOSAltRec.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSAltRec.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmOSAltRec.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
