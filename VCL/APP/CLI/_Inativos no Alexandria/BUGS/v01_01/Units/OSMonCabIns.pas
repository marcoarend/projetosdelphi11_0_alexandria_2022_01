unit OSMonCabIns;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, AdvObj, BaseGrid, AdvGrid, DBAdvGrid, Menus,
  dmkRadioGroup, dmkDBGridDAC, dmkDBGridZTO, UnBugs_Tabs, UnDmkEnums,
  UnProjGroup_Consts, dmkValUsu;

type
  THackDBGrid = class(TDBGrid);
  TFmOSMonCabIns = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    QrOSSrv: TmySQLQuery;
    QrOSSrvNO_DesServico: TWideStringField;
    QrOSSrvCodigo: TIntegerField;
    QrOSSrvControle: TIntegerField;
    QrOSSrvDesServico: TIntegerField;
    QrOSSrvLk: TIntegerField;
    QrOSSrvDataCad: TDateField;
    QrOSSrvDataAlt: TDateField;
    QrOSSrvUserCad: TIntegerField;
    QrOSSrvUserAlt: TIntegerField;
    QrOSSrvAlterWeb: TSmallintField;
    QrOSSrvAtivo: TSmallintField;
    DsOSSrv: TDataSource;
    QrFormulas: TmySQLQuery;
    DsFormulas: TDataSource;
    QrFormulasCodigo: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrEquipAplic: TmySQLQuery;
    DsEquipAplic: TDataSource;
    QrEquipAplicControle: TIntegerField;
    QrEquipAplicNome: TWideStringField;
    QrPIPs: TmySQLQuery;
    DsPIPs: TDataSource;
    QrPIPsCodigo: TIntegerField;
    QrPIPsNome: TWideStringField;
    QrTipoAplica: TmySQLQuery;
    QrTipoAplicaCodigo: TIntegerField;
    QrTipoAplicaNome: TWideStringField;
    DsTipoAplica: TDataSource;
    QrFormulasEquipAplic: TIntegerField;
    QrFormulasQtdTot: TFloatField;
    QrFormulasQtdQSP: TFloatField;
    QrFormulasCusTot: TFloatField;
    QrFormulasAplicacao: TIntegerField;
    QrFormulasDiluente: TSmallintField;
    QrFormulasTipoAplica: TIntegerField;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    QrFormulasCU_UNIDMED: TIntegerField;
    QrFormulasSIGLA: TWideStringField;
    Panel5: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox2: TGroupBox;
    GBEdita: TGroupBox;
    Label1: TLabel;
    Label9: TLabel;
    SbFormulas: TSpeedButton;
    Label3: TLabel;
    EdConta: TdmkEdit;
    EdNome: TdmkEdit;
    CBFormula: TdmkDBLookupComboBox;
    EdFormula: TdmkEditCB;
    RGDiluente: TdmkRadioGroup;
    GBDados: TGroupBox;
    Label7: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit15: TDBEdit;
    Panel6: TPanel;
    GroupBox1: TGroupBox;
    DBGPIPs: TdmkDBGridDAC;
    Panel8: TPanel;
    CBEquipAplic: TdmkDBLookupComboBox;
    SbEquipAplic: TSpeedButton;
    EdEquipAplic: TdmkEditCB;
    Label2: TLabel;
    EdQtdQSP: TdmkEdit;
    Label8: TLabel;
    EdQtdTot: TdmkEdit;
    Label6: TLabel;
    SpeedButton5: TSpeedButton;
    CBUnidMed: TdmkDBLookupComboBox;
    EdSigla: TdmkEdit;
    EdUnidMed: TdmkEditCB;
    Label12: TLabel;
    SBTipoAplica: TSpeedButton;
    CBTipoAplica: TdmkDBLookupComboBox;
    EdTipoAplica: TdmkEditCB;
    Label11: TLabel;
    Panel9: TPanel;
    BtPIPs: TBitBtn;
    Memo1: TMemo;
    PMPIPs: TPopupMenu;
    InclusodePIPnico1: TMenuItem;
    InclusodemultiPIPs1: TMenuItem;
    QrPIPsAtivo: TSmallintField;
    BtTudo: TBitBtn;
    BtNenhum: TBitBtn;
    QrSel: TmySQLQuery;
    QrSelCodigo: TIntegerField;
    QrSelNome: TWideStringField;
    QrSelAtivo: TSmallintField;
    GroupBox3: TGroupBox;
    DBGrid1: TDBGrid;
    DsOSMonRec: TDataSource;
    QrGGXs: TmySQLQuery;
    QrGGXsGraGru1: TIntegerField;
    QrGGXsControle: TIntegerField;
    QrGGXsNO_PRD_TAM_COR: TWideStringField;
    DsGGxs: TDataSource;
    TbOSMonRec: TmySQLTable;
    TbOSMonRecNO_GGX: TWideStringField;
    TbOSMonRecCodigo: TIntegerField;
    TbOSMonRecControle: TIntegerField;
    TbOSMonRecConta: TIntegerField;
    TbOSMonRecIDIts: TIntegerField;
    TbOSMonRecGraGruX: TIntegerField;
    TbOSMonRecPrvQtd: TFloatField;
    TbOSMonRecPrvPrc: TFloatField;
    TbOSMonRecPrvVal: TFloatField;
    TbOSMonRecUsoQtd: TFloatField;
    TbOSMonRecUsoPrc: TFloatField;
    TbOSMonRecUsoVal: TFloatField;
    TbOSMonRecUsoDec: TFloatField;
    TbOSMonRecUsoTot: TFloatField;
    TbOSMonRecOrdem: TIntegerField;
    TbOSMonRecReordem: TIntegerField;
    TbOSMonRecValCliDd: TIntegerField;
    TbOSMonRecEhDiluente: TSmallintField;
    QrIts: TmySQLQuery;
    QrItsCodigo: TIntegerField;
    QrItsControle: TIntegerField;
    QrItsConta: TIntegerField;
    QrItsIDIts: TIntegerField;
    QrItsGraGruX: TIntegerField;
    QrItsPrvQtd: TFloatField;
    QrItsPrvPrc: TFloatField;
    QrItsPrvVal: TFloatField;
    QrItsUsoQtd: TFloatField;
    QrItsUsoPrc: TFloatField;
    QrItsUsoVal: TFloatField;
    QrItsUsoDec: TFloatField;
    QrItsUsoTot: TFloatField;
    QrItsOrdem: TIntegerField;
    QrItsReordem: TIntegerField;
    QrItsValCliDd: TIntegerField;
    QrItsEhDiluente: TSmallintField;
    Label10: TLabel;
    EdPerioDd: TdmkEdit;
    Label13: TLabel;
    EdDdPostero: TdmkEdit;
    Label14: TLabel;
    TbDef_DMP: TmySQLTable;
    DsDef_DMP: TDataSource;
    TbDef_DMPOrdem: TIntegerField;
    TbDef_DMPDias: TIntegerField;
    GroupBox4: TGroupBox;
    dmkDBGridZTO1: TdmkDBGridZTO;
    QrDMP: TmySQLQuery;
    QrDMPOrdem: TIntegerField;
    QrDMPDias: TIntegerField;
    QrGGXsNO_MED: TWideStringField;
    QrGGXsSigla: TWideStringField;
    TbOSMonRecSigla: TWideStringField;
    VUUnidMed: TdmkValUsu;
    QrEquipAplicNivel1: TIntegerField;
    N1: TMenuItem;
    EditaPMVselecionado1: TMenuItem;
    LaServicoAviso: TLabel;
    Label15: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbFormulasClick(Sender: TObject);
    procedure SbEquipAplicClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBTipoAplicaClick(Sender: TObject);
    procedure EdFormulaChange(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
    procedure InclusodePIPnico1Click(Sender: TObject);
    procedure InclusodemultiPIPs1Click(Sender: TObject);
    procedure EdEquipAplicChange(Sender: TObject);
    procedure BtPIPsClick(Sender: TObject);
    procedure BtTudoClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure EdFormulaExit(Sender: TObject);
    procedure EdFormulaEnter(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1ColEnter(Sender: TObject);
    procedure DBGrid1ColExit(Sender: TObject);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CBFormulaExit(Sender: TObject);
    procedure EditaPMVselecionado1Click(Sender: TObject);
    procedure PMPIPsPopup(Sender: TObject);
  private
    { Private declarations }
    FFormula: Integer;
    F_Sel_PIP, F_Def_OMR, F_Def_DMP: String;
    //
    procedure ReInserePipDisponiveis();
    procedure AtivaTodos(Ativo: Integer);
    procedure ReInsereOSMonRec();
    procedure InsereOSMonEPI(Codigo, Controle, Conta: Integer);
    procedure ReopenOSMonRec();
  public
    { Public declarations }
    FSiapTerCad, FEntidade,
    FCodigo, FControle, FConta: Integer;
    FQrOSMonCab, FQrOSMonRec: TmySQLQuery;
    FDtaExeFim: TDateTime;
    //
    procedure ReopenPIPs(Codigo: Integer);

  end;

  var
  FmOSMonCabIns: TFmOSMonCabIns;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModuleGeral, CreateBugs,
Principal, ModProd, UnGrade_Jan, MyDBCheck, UnOSApp_PF, GraGruN, UnAppListas, MyVCLSkin,
MeuDBUses, PipCad;

{$R *.DFM}

procedure TFmOSMonCabIns.AtivaTodos(Ativo: Integer);
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'UPDATE ' + F_Sel_PIP,
  'SET Ativo=' + FormatFloat('0', Ativo),
  '']);
  ReopenPIPs(QrPIPsCodigo.Value);
  //
  Screen.Cursor := crDefault;
end;

procedure TFmOSMonCabIns.BtNenhumClick(Sender: TObject);
begin
  AtivaTodos(0);
end;

procedure TFmOSMonCabIns.BtOKClick(Sender: TObject);
  const
    IndicUso = CO_FRM_INDICUSO_001_COD_UsoDefinitivo;
  //
  function InsereItemMonRecAtual(Codigo, Controle, Conta: Integer): Boolean;
  var
    IDIts, GraGruX, Ordem, Reordem, ValCliDd, EhDiluente: Integer;
    PrvQtd, PrvPrc, PrvVal, UsoQtd, UsoPrc, UsoVal, UsoDec, UsoTot: Double;
  begin
    GraGruX        := QrItsGraGruX.Value;
    PrvQtd         := QrItsPrvQtd.Value;
    PrvPrc         := QrItsPrvPrc.Value;
    PrvVal         := QrItsPrvVal.Value;
    UsoQtd         := QrItsUsoQtd.Value;
    UsoPrc         := QrItsUsoPrc.Value;
    UsoVal         := QrItsUsoVal.Value;
    UsoDec         := QrItsUsoDec.Value;
    UsoTot         := QrItsUsoTot.Value;
    Ordem          := (*QrItsOrdem .Value*) QrIts.RecNo;
    Reordem        := QrItsReordem.Value;
    ValCliDd       := QrItsValCliDd.Value;
    EhDiluente     := QrItsEhDiluente.Value;
      //
    IDIts := UMyMod.BPGS1I32('osmonrec', 'IDIts', '', '', tsPos, stIns, 0);
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'osmonrec', False, [
    'Codigo', 'Controle', 'Conta',
    'GraGruX', 'PrvQtd', 'PrvPrc',
    'PrvVal', 'UsoQtd', 'UsoPrc',
    'UsoVal', 'UsoDec', 'UsoTot',
    'Ordem', 'Reordem', 'ValCliDd',
    'EhDiluente'], [
    'IDIts'], [
    Codigo, Controle, Conta,
    GraGruX, PrvQtd, PrvPrc,
    PrvVal, UsoQtd, UsoPrc,
    UsoVal, UsoDec, UsoTot,
    Ordem, Reordem, ValCliDd,
    EhDiluente], [
    IDIts], True);
  end;
  //
  function InsereItemMonPipDdAtual(Codigo, Controle, Conta: Integer): Boolean;
{
  var
    IDIts, GraGruX, Ordem, Reordem, ValCliDd, EhDiluente: Integer;
    PrvQtd, PrvPrc, PrvVal, UsoQtd, UsoPrc, UsoVal, UsoDec, UsoTot: Double;
  begin
    GraGruX        := QrItsGraGruX.Value;
    PrvQtd         := QrItsPrvQtd.Value;
    PrvPrc         := QrItsPrvPrc.Value;
    PrvVal         := QrItsPrvVal.Value;
    UsoQtd         := QrItsUsoQtd.Value;
    UsoPrc         := QrItsUsoPrc.Value;
    UsoVal         := QrItsUsoVal.Value;
    UsoDec         := QrItsUsoDec.Value;
    UsoTot         := QrItsUsoTot.Value;
    Ordem          := (*QrItsOrdem .Value*) QrIts.RecNo;
    Reordem        := QrItsReordem.Value;
    ValCliDd       := QrItsValCliDd.Value;
    EhDiluente     := QrItsEhDiluente.Value;
      //
    IDIts := UMyMod.BPGS1I32('osmonrec', 'IDIts', '', '', tsPos, stIns, 0);
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'osmonrec', False, [
    'Codigo', 'Controle', 'Conta',
    'GraGruX', 'PrvQtd', 'PrvPrc',
    'PrvVal', 'UsoQtd', 'UsoPrc',
    'UsoVal', 'UsoDec', 'UsoTot',
    'Ordem', 'Reordem', 'ValCliDd',
    'EhDiluente'], [
    'IDIts'], [
    Codigo, Controle, Conta,
    GraGruX, PrvQtd, PrvPrc,
    PrvVal, UsoQtd, UsoPrc,
    UsoVal, UsoDec, UsoTot,
    Ordem, Reordem, ValCliDd,
    EhDiluente], [
    IDIts], True);

}

  var
    IDIts, Ordem, Dias: Integer;
  begin
    IDIts          := 0;
    Ordem          := QrDMPOrdem.Value;
    Dias           := QrDMPDias.Value;
    //
    IDIts := UMyMod.BPGS1I32('osmonpipdd', 'IDIts', '', '', tsPos, stIns, IDIts);
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'osmonpipdd', False, [
    'Codigo', 'Controle', 'Conta',
    'Ordem', 'Dias'], [
    'IDIts'], [
    Codigo, Controle, Conta,
    Ordem, Dias], [
    IDIts], True);
  end;
  //
var
  Nome: String;
  Codigo, Controle, Conta, Formula, EquipAplic, PipCad, TipoAplica,
    Diluente, UnidMed, PerioDd, DdPostero: Integer;
  //CusTot,
  QtdTot, QtdQSP: Double;
  Data: TDateTime;
begin
  TbOSMonRec.First;
  while not TbOSMonRec.Eof do
  begin
    if Geral.MB_Pergunta('Confirma a quantidade de ' + FloatToStr(
      TbOSMonRecPrvQtd.Value) + ' ' + TbOSMonRecSigla.Value +
      ' por PMV para o item "' + TbOSMonRecNO_GGX.Value + '"') <> ID_YES
    then
      Exit;
    //
    TbOSMonRec.Next;
  end;
  //
  if MyObjects.FIC(TRIM(EdNome.Text) = '', EdNome, 'Informe uma descri�ao!') then
    Exit;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Preparando inclus�o');
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSel, DModG.MyPID_DB, [
    'SELECT * ',
    'FROM ' + F_Sel_PIP,
    'WHERE Ativo=1',
    'ORDER BY Codigo',
    '']);
  UnDmkDAC_PF.AbreMySQLQuery0(QrIts, DModG.MyPID_DB, [
    'SELECT * FROM ' + F_Def_OMR,
    '']);
    UnDmkDAC_PF.AbreMySQLQuery0(QrDMP, DModG.MyPID_DB, [
    'SELECT * FROM ' + F_Def_DMP,
    'ORDER BY Ordem',
    '']);
  if QrSel.RecordCount = 0 then
  begin
    Geral.MB_Aviso('Nenhum PIP foi selecionado!');
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    Exit;
  end;
  if QrDMP.RecordCount = 0 then
  begin
    if Geral.MB_Pergunta('Nenhum per�odo inicial de monitoramento foi selecionado!' +
      sLineBreak + 'Deseja continuar assim mesmo?') <> ID_YES then
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      Exit;
    end;
  end;
  //
  PerioDd := EdPerioDd.ValueVariant;
  if MyObjects.FIC(PerioDd = 0, EdPerioDd,
    'Defina o "Per�odo dias" mesmo para monitoramentos contratuais!' + sLineBreak +
    'Isso evitar� uma verifica��o demorada ao criar OSs filhas de monitoramento!')
  then
    Exit;
  //
  Screen.Cursor := crHourGlass;
  try
    Codigo         := FCodigo;
    Controle       := FControle;
    Nome           := EdNome.Text;
    Formula        := EdFormula.ValueVariant;
    EquipAplic     := EdEquipAplic.ValueVariant;
    QtdTot         := EdQtdTot.ValueVariant;
    QtdQSP         := EdQtdQSP.ValueVariant;
    TipoAplica     := EdTipoAplica.ValueVariant;
    Diluente       := RGDiluente.ItemIndex;
    DdPostero      := EdDdPostero.ValueVariant;
    //
    if EdUnidMed.ValueVariant <> 0 then
      UnidMed := QrUnidMedCodigo.Value
    else
      Unidmed := 0;
    //CusTot         := ;
    Conta := EdConta.ValueVariant;
    //
    QrSel.First;
    while not QrSel.Eof do
    begin
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Incluindo PIP ' +
        Geral.FF0(QrSel.RecNo) + ' de ' + Geral.FF0(QrSel.RecordCount));
      //
      PipCad := QrSelCodigo.Value;
      //
      Conta := UMyMod.BPGS1I32('osmoncab', 'Conta', '', '', tsDef,
        ImgTipo.SQLType, Conta);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'osmoncab', False, [
      'Codigo', 'Controle', 'Nome',
      'Formula', 'EquipAplic', 'QtdTot',
      'QtdQSP'(*, 'CusTot'*), 'pipcad',
      'TipoAplica', 'Diluente', 'UnidMed',
      'PerioDd', 'DdPostero', 'IndicUso'], [
      'Conta'], [
      Codigo, Controle, Nome,
      Formula, EquipAplic, QtdTot,
      QtdQSP(*, CusTot*), PipCad,
      TipoAplica, Diluente, UnidMed,
      PerioDd, DdPostero, IndicUso], [
      Conta], True) then
      begin
        if PipCad <> 0 then
          Dmod.AtualizaPIP(PipCad);
        //
        QrIts.First;
        while not QrIts.Eof do
        begin
          if not InsereItemMonRecAtual(Codigo, Controle, Conta) then
            Exit;
          //
          QrIts.Next;
        end;
        //
        QrDMP.First;
        while not QrDMP.Eof do
        begin
          if not InsereItemMonPipDdAtual(Codigo, Controle, Conta) then
            Exit;
          //
          QrDMP.Next;
        end;
      end;
      //
      QrSel.Next;
    end;
    InsereOSMonEPI(Codigo, Controle, Conta);
    OSApp_PF.ReopenOSMonCab(FQrOSMonCab, Controle, Conta);
    if ImgTipo.SQLType = stIns then
      FConta := Conta;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    (* Aparentemente n�o usa aqui
    if FDtaExeFim > 2 then
      Data := 2
    else
      Data := Date;
    OSApp_PF.AdicionaPMVsEmOSsFuturas(QrOSSrvControle.Value,
      FSiapTerCad, Data, QrOSSrvCodigo.Value);
    *)
  finally
    Screen.Cursor := crDefault;
    Close;
  end;
end;

procedure TFmOSMonCabIns.BtPIPsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPIPs, BtPIPs);
end;

procedure TFmOSMonCabIns.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSMonCabIns.BtTudoClick(Sender: TObject);
begin
  AtivaTodos(1);
end;

procedure TFmOSMonCabIns.CBFormulaExit(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    if EdNome.ValueVariant = '' then
      EdNome.ValueVariant := CBFormula.Text;
  end;
end;

procedure TFmOSMonCabIns.DBGrid1CellClick(Column: TColumn);
const
  Conta = 0; // ID do OSMonCab que n�o foi definido ainda!
var
  EhDiluente, AcaoSobreDil: Byte;
  UsaDil: String;
  Continua: Boolean;
begin
  if Column.FieldName = CO_FldEhDil then
  begin
    if TbOSMonRecEhDiluente.Value = 0 then
    begin
      if RGDiluente.ItemIndex <> CO_COD_BUGS_DILUENTE_003 then
      begin
        UsaDil := '"' + sListaDiluidoresBugs[CO_COD_BUGS_DILUENTE_003] + '"';
        AcaoSobreDil := MyObjects.SelRadioGroup('O Que Fazer?',
        'O tipo de diluente difere de ' + UsaDil +  '. O que fazer?',
        ['Manter do jeito que est�', 'Mudar para '+ UsaDil, 'Desistir'], 1);
        case AcaoSobreDil of
          0: Continua := True;
          1:
          begin
            Continua := True;
            //OSApp_PF.DefineTemProdutoDiluente(gbsMonitora, FQrOSMonCab(*DmModOS.QrOSMonCab*));
            RGDiluente.ItemIndex := CO_COD_BUGS_DILUENTE_003;
          end;
          else Continua := False;
        end;
        if not Continua then
          Exit;
      end;
      OSApp_PF.ZeraDiluente(gbsMonMulti, Conta, F_Def_OMR);
      EhDiluente := 1;
    end else
      EhDiluente := 0;
    //
    TbOSMonRec.Edit;
    TbOSMonRecEhDiluente.Value := EhDiluente;
    TbOSMonRec.Post;
    //
    TbOSMonRec.Refresh;
  end;
end;

procedure TFmOSMonCabIns.DBGrid1ColEnter(Sender: TObject);
begin
  if DBGrid1.Columns[THackDBGrid(DBGrid1).Col -1].FieldName = CO_FldEhDil then
    DBGrid1.Options := DBGrid1.Options - [dgEditing] else
    DBGrid1.Options := DBGrid1.Options + [dgEditing];
end;

procedure TFmOSMonCabIns.DBGrid1ColExit(Sender: TObject);
begin
  DBGrid1.Options := DBGrid1.Options - [dgEditing];
end;

procedure TFmOSMonCabIns.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = CO_FldEhDil then
    MeuVCLSkin.DrawGrid(DBGrid1, Rect, 1, TbOSMonRecEhDiluente.Value);
end;

procedure TFmOSMonCabIns.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Ordem, IDIts: Integer;
begin
  if key=VK_INSERT then
  begin
    if DBGrid1.ReadOnly = False then
    begin
      Ordem := TbOSMonRecOrdem.Value-1;
      TbOSMonRec.Insert;
      TbOSMonRecOrdem.Value := Ordem;
      key := 0;
    end;
  end else
  if key=VK_F4 then
  begin
    {$IfNDef SemGraGruN}
    //if ImgTipo.SQLType in ([stIns, stUpd]) then
    begin
      VAR_CADASTRO := 0;
      //FmPrincipal.CadastroPQ(TbOSMonRecProduto.Value);
      if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
      begin
        FmGraGruN.ShowModal;
        FmGraGruN.Destroy;
        // Parei Aqui
        // Como gerar o VAR_CADASTRO?
      end;
      if VAR_CADASTRO <> 0 then
      begin
        Screen.Cursor := crHourGlass;
        try
          QrGGXs.Close;
          QrGGXs.Open;
          //
          TbOSMonRec.Edit;
          TbOSMonRecGraGruX.Value := VAR_CADASTRO;
          TbOSMonRec.Post;
        finally
          Screen.Cursor := crDefault;
        end;
      end;
    end;
    {$EndIf}
  end else
  if key=VK_F7 then
  begin
    VAR_CADASTRO := 0;
    FmMeuDBUses.PesquisaNome('gragrux', '', '');
    if VAR_CADASTRO <> 0 then
    begin
      IDIts := TbOSMonRecIDIts.Value;
      TbOSMonRec.Edit;
      TbOSMonRecGraGruX.Value := VAR_CADASTRO;
      if IDIts <> 0 then
      begin
        TbOSMonRec.Post;
        //TbOSMonRec.Locate('IDIts', IDIts, []);
      end;
    end;
  end;
end;

procedure TFmOSMonCabIns.EdEquipAplicChange(Sender: TObject);
begin
  ReInserePipDisponiveis();
end;

procedure TFmOSMonCabIns.EdFormulaChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    EdQtdTot.ValueVariant := QrFormulasQtdTot.Value;
    EdEquipAplic.ValueVariant := QrFormulasEquipAplic.Value;
    CBEquipAplic.KeyValue := QrFormulasEquipAplic.Value;
    EdTipoAplica.ValueVariant := QrFormulasTipoAplica.Value;
    CBTipoAplica.KeyValue := QrFormulasTipoAplica.Value;
    RGDiluente.ItemIndex := QrFormulasDiluente.Value;
    EdSigla.Text := QrFormulasSIGLA.Value;
    EdUnidMed.ValueVariant := QrFormulasCU_UnidMed.Value;
    CBUnidMed.KeyValue := QrFormulasCU_UnidMed.Value;
  end;
  if EdFormula.Focused = False then
    ReInsereOSMonRec();
end;

procedure TFmOSMonCabIns.EdFormulaEnter(Sender: TObject);
begin
  FFormula := EdFormula.ValueVariant;
end;

procedure TFmOSMonCabIns.EdFormulaExit(Sender: TObject);
begin
  if FFormula <> EdFormula.ValueVariant then
  begin
    FFormula := EdFormula.ValueVariant;
    ReInsereOSMonRec();
  end;
end;

procedure TFmOSMonCabIns.EditaPMVselecionado1Click(Sender: TObject);
begin
  if (QrPIPs.State <> dsInactive) and (QrPIPs.RecordCount > 0) then
  begin
    FmPrincipal.MostraFormPipCad(QrPIPsCodigo.Value, FEntidade);
    //
    ReInserePipDisponiveis();
  end;
end;

procedure TFmOSMonCabIns.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  EdNome.Text := CBFormula.Text;
end;

procedure TFmOSMonCabIns.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    DmProd.PesquisaPorSigla(False, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmOSMonCabIns.EdSiglaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmOSMonCabIns.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);
end;

procedure TFmOSMonCabIns.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSMonCabIns.FormCreate(Sender: TObject);
var
  FiltroNivel: String;
begin
  FFormula   := 0;
  FConta     := 0;
  FDtaExeFim := 0;
  //
  Label14.Visible := False;
  //
  UnDMkDAC_PF.AbreMySQLQuery0(QrFormulas, Dmod.MyDB, [
    'SELECT frm.*, med.CodUsu CU_UNIDMED, med.SIGLA ',
    'FROM formulas frm ',
    'LEFT JOIN unidmed med ON med.Codigo=frm.UnidMed ',
    'WHERE frm.Aplicacao & 2 ',
    'ORDER BY frm.Nome ',
    '']);
  UMyMod.AbreQuery(QrTipoAplica, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  OSApp_PF.FiltroGrade(gbsMonitora, gbmProduto, FiltroNivel);
  UnDmkDAC_PF.AbreMySQLQuery0(QrGGXs, DMod.MyDB, [
    'SELECT ggx.GraGru1, ggx.Controle, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR, med.Nome NO_MED, med.Sigla ',
    'FROM gragrux ggx ',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN unidmed  med ON med.Codigo=gg1.UnidMed',
    // 2013-08-03
    //'WHERE ' + FiltroNivel,
    // FIM 2013-08-03
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
    '']);
  OSApp_PF.ReopenEquipAplic(QrEquipAplic, gbsMonitora);
  //
  F_Sel_PIP :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Sel_PIP, DmodG.QrUpdPID1, False);
  DBGPIPs.SQLTable := F_Sel_PIP;
  F_Def_OMR :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Def_OMR, DmodG.QrUpdPID1, False);
  F_Def_DMP :=
    UnCreateBugs.RecriaTempTableNovo(ntrtt_Def_DMP, DmodG.QrUpdPID1, False);
  //
  TbOSMonRec.Close;
  TbOSMonRec.Database := DModG.MyPID_DB;
  TbOSMonRec.TableName := F_Def_OMR;
  //
  TbDef_DMP.Close;
  TbDef_DMP.Database := DModG.MyPID_DB;
  TbDef_DMP.TableName := F_Def_DMP;
  TbDef_DMP.Open;
end;

procedure TFmOSMonCabIns.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSMonCabIns.InclusodemultiPIPs1Click(Sender: TObject);
var
  Equipamento: Integer;
begin
  Equipamento := EdEquipAplic.ValueVariant;
  //
  FmPrincipal.MostraFormPipVarios(stIns, Equipamento);
  //Parei aqui!
  //UMyMod.SetaCodigoPesquisado(EdPipCad, CBPipCad, QrPIPs, VAR_CADASTRO, 'Codigo');
  ReInserePipDisponiveis();
end;

procedure TFmOSMonCabIns.InclusodePIPnico1Click(Sender: TObject);
var
  Equipamento: Integer;
begin
  Equipamento := EdEquipAplic.ValueVariant;
  //
  FmPrincipal.MostraFormPipRapido(0, stIns, FEntidade, Equipamento, False);
  //Parei aqui!
  //UMyMod.SetaCodigoPesquisado(EdPipCad, CBPipCad, QrPIPs, VAR_CADASTRO, 'Codigo');
  ReInserePipDisponiveis();
end;

procedure TFmOSMonCabIns.PMPIPsPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrPIPs.State <> dsInactive) and (QrPIPs.RecordCount > 0);
  //
  EditaPMVselecionado1.Enabled := Enab;
end;

procedure TFmOSMonCabIns.ReInserePipDisponiveis();
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
  'DELETE FROM '+ F_Sel_PIP +';',
  'INSERT INTO '+ F_Sel_PIP,
  'SELECT pip.Codigo, pip.Nome, 0 Ativo',
  'FROM ' + TMeuDB + '.pipcad pip',
  'WHERE pip.Equipamento=' + Geral.FF0(EdEquipAplic.ValueVariant),
  'AND (OSMonCab=0 ',
  'AND MotInutili=0 ',
  'AND (DtaInutili < "1900-01-01" ',
  '  OR DtaInutili IS NULL)) ',
  '']);
  //
  ReopenPIPs(0);
end;

procedure TFmOSMonCabIns.InsereOSMonEPI(Codigo, Controle, Conta: Integer);
var
  IDIts, Formula, GraGruX: Integer;
  Qry: TmySQLQuery;
begin
  Formula := EdFormula.ValueVariant;
  Qry     := TmySQLQuery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT epi.* ',
      'FROM formulepi epi ',
      'WHERE epi.Codigo=' + Geral.FF0(Formula),
      '']);
    if Qry.RecordCount > 0 then
    begin
      while not Qry.Eof do
      begin
        IDIts   := UMyMod.BPGS1I32('osmonepi', 'IDIts', '', '', tsDef, stIns, 0);
        GraGruX := Qry.FieldByName('GraGruX').AsInteger;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'osmonepi', False,
          ['Codigo', 'Controle', 'Conta', 'GraGruX'], ['IDIts'],
          [Codigo, Controle, Conta, GraGruX], [IDIts], True);
        //
        Qry.Next;
      end;
    end;
  finally
    Qry.Free;
  end;
end;

procedure TFmOSMonCabIns.ReInsereOSMonRec();
begin
  UnDmkDAC_PF.ExecutaMySQLQuery0(DModG.QrUpdPID1, DModG.MyPID_DB, [
(*
  'DELETE FROM _def_omr;',
  'INSERT INTO _def_omr',
  'SELECT',
  'lif.Codigo, lif.Controle, 0 Conta, ',
  '0 IDIts, lif.GraGruX, lif.PrvQtd, ',
  'lif.PrvPrc, lif.PrvVal, lif.UsoQtd, ',
  'lif.UsoPrc, lif.UsoVal, lif.UsoDec, ',
  'lif.UsoTot, lif.Ordem, lif.Reordem, ',
  '0 ValCliDd, lif.EhDiluente, ',
  'lif.Lk, lif.DataCad, lif.DataAlt, ',
  'lif.UserCad, lif.UserAlt, lif.AlterWeb, ',
  'ggx.GraGru1, gg1.Nome, ',
  'lif.Ativo ',
  'FROM ' + TMeuDB + '.formulif lif',
  'LEFT JOIN ' + TMeuDB + '.gragrux ggx ON ggx.Controle=lif.GraGruX',
  'LEFT JOIN ' + TMeuDB + '.gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1',
*)

  'DELETE FROM _def_omr;',
  'INSERT INTO _def_omr',
  'SELECT',
  'Codigo, Controle, 0 Conta, ',
  '0 IDIts, GraGruX, PrvQtd, ',
  'PrvPrc, PrvVal, UsoQtd, ',
  'UsoPrc, UsoVal, UsoDec, ',
  'UsoTot, Ordem, Reordem, ',
  '0 ValCliDd, EhDiluente, ',
  // Compatibilidade
  'Lk, DataCad, DataAlt, ',
  'UserCad, UserAlt, AlterWeb, ',
  // FIM Compatibilidade
  'Ativo ',
  'FROM ' + TMeuDB + '.formulif',
  'WHERE Codigo=' + Geral.FF0(EdFormula.ValueVariant),
  '']);
  //
  //Geral.MB_SQL(TForm(DModG), DModG.QrUpdPID1);
  //
  ReopenOSMonRec();
end;

procedure TFmOSMonCabIns.ReopenOSMonRec();
begin
  TbOSMonRec.Close;
  TbOSMonRec.Open;
end;

procedure TFmOSMonCabIns.ReopenPIPs(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPIPs, DModG.MyPID_DB, [
  'SELECT * ',
  'FROM ' + F_Sel_PIP,
  'ORDER BY Nome',
  '']);
end;

procedure TFmOSMonCabIns.SbEquipAplicClick(Sender: TObject);
var
  EquipAplic, Nivel1: Integer;
begin
  VAR_CADASTRO := 0;
  EquipAplic   := EdEquipAplic.ValueVariant;

  if EquipAplic <> 0 then
    Nivel1 := QrEquipAplicNivel1.Value
  else
    Nivel1 := 0;

  Grade_Jan.MostraFormGraGruN(Nivel1);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdEquipAplic, CBEquipAplic, QrEquipAplic, VAR_CADASTRO, 'Controle');
    EdEquipAplic.SetFocus;
  end;
  (*
  //FmPrincipal.MostraFormGraG1EqMo();
  FmPrincipal.MostraFormGraG1Equi(gbsMonitora, 0);
  *)
end;

procedure TFmOSMonCabIns.SbFormulasClick(Sender: TObject);
var
  Formula: Integer;
begin
  VAR_CADASTRO := 0;
  Formula      := EdFormula.ValueVariant;
  //
  FmPrincipal.MostraFormFormulas(FQrOSMonRec, Formula, 0);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdFormula, CBFormula, QrFormulas, VAR_CADASTRO);
    //
    UMyMod.AbreQuery(QrTipoAplica, Dmod.MyDB);
    UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
    OSApp_PF.ReopenEquipAplic(QrEquipAplic, gbsMonitora);
    //
    EdFormula.SetFocus;
  end;
end;

procedure TFmOSMonCabIns.SBTipoAplicaClick(Sender: TObject);
var
  TipoAplica: Integer;
begin
  VAR_CADASTRO := 0;
  TipoAplica   := EdTipoAplica.ValueVariant;
  //
  FmPrincipal.MostraFormTipoAplica(TipoAplica);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdTipoAplica, CBTipoAplica, QrTipoAplica, VAR_CADASTRO);
    EdTipoAplica.SetFocus;
  end;
end;

procedure TFmOSMonCabIns.SpeedButton5Click(Sender: TObject);
var
  UnidMed: Integer;
begin
  VAR_CADASTRO := 0;
  UnidMed      := VUUnidMed.ValueVariant;
  //
  Grade_Jan.MostraFormUnidMed(UnidMed);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO, 'Codigo', 'CodUsu');
    EdUnidMed.SetFocus;
  end;
end;

end.
