unit PosVdaIts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, dmkRadioGroup, UnAppListas, UnDmkEnums,
  UnProjGroup_Consts;

type
  TFmPosVdaIts = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    CBAplicID: TdmkDBLookupComboBox;
    EdAplicID: TdmkEditCB;
    Label1: TLabel;
    SBAplicID: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    EdDias: TdmkEdit;
    RGAplicacao: TdmkRadioGroup;
    QrPreEmail: TmySQLQuery;
    DsPreEmail: TDataSource;
    QrPreEmailCodigo: TIntegerField;
    QrPreEmailNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SBAplicIDClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenPosVdaIts(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmPosVdaIts: TFmPosVdaIts;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
  Principal;

{$R *.DFM}

procedure TFmPosVdaIts.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Dias, Aplicacao, AplicID: Integer;
begin
  Dias           := EdDias.ValueVariant;
  Aplicacao      := RGAplicacao.ItemIndex;
  AplicID        := EdAplicID.ValueVariant;
  //
  if Aplicacao = 2 then
    if MyObjects.FIC(AplicID = 0, EdAplicID, 'Informe o Pr� Email!') then
      Exit;
  Codigo   := Geral.IMV(DBEdCodigo.Text);
  Controle := EdControle.ValueVariant;
  Controle := UMyMod.BPGS1I32('posvdaits', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'posvdaits', False, [
  'Codigo', 'Dias', 'Aplicacao',
  'AplicID'], [
  'Controle'], [
  Codigo, Dias, Aplicacao,
  AplicID], [
  Controle], True) then
  begin
    ReopenPosVdaIts(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdAplicID.ValueVariant   := 0;
      CBAplicID.KeyValue       := Null;
      RGAplicacao.ItemIndex    := 0;
      EdDias.ValueVariant      := '';
      EdDias.SetFocus;
    end else Close;
  end;
end;

procedure TFmPosVdaIts.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPosVdaIts.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmPosVdaIts.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  MyObjects.ConfiguraRadioGroup(RGAplicacao, sListaAcaoPosVda, 3, 0);
  //
  UnDmkDAC_PF.AbreQuery(QrPreEmail, Dmod.MyDB);
end;

procedure TFmPosVdaIts.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPosVdaIts.ReopenPosVdaIts(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    FQrIts.Open;
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmPosVdaIts.SBAplicIDClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormPreEmail(EdAplicID.ValueVariant);
  if VAR_CADASTRO <> 0 then
    UMyMod.SetaCodigoPesquisado(EdAplicID, CBAplicID, QrPreEmail, VAR_CADASTRO);
end;

end.
