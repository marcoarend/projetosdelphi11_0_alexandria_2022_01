unit OSCxa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, dmkRadioGroup, UnBugs_Tabs, dmkMemo, dmkLabelRotate,
  UnDmkEnums;

type
  TFmOSCxa = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    QrOSCab: TmySQLQuery;
    QrOSCabCodigo: TIntegerField;
    QrOSCabEntidade: TIntegerField;
    QrOSCabEstatus: TIntegerField;
    QrOSCabFatoGeradr: TIntegerField;
    QrOSCabDtaContat: TDateTimeField;
    QrOSCabDtaVisPrv: TDateTimeField;
    QrOSCabDtaVisExe: TDateTimeField;
    QrOSCabDtaExePrv: TDateTimeField;
    QrOSCabDtaExeIni: TDateTimeField;
    QrOSCabDtaExeFim: TDateTimeField;
    QrOSCabLk: TIntegerField;
    QrOSCabDataCad: TDateField;
    QrOSCabDataAlt: TDateField;
    QrOSCabUserCad: TIntegerField;
    QrOSCabUserAlt: TIntegerField;
    QrOSCabAlterWeb: TSmallintField;
    QrOSCabAtivo: TSmallintField;
    QrOSCabNO_FatoGeradr: TWideStringField;
    QrOSCabNO_ESTATUS: TWideStringField;
    QrOSCabNO_ENT: TWideStringField;
    QrOSCabSiapTerCad: TIntegerField;
    QrOSCabNO_SiapTerCad: TWideStringField;
    QrOSCabTXTVisPrv: TWideStringField;
    QrOSCabTXTContat: TWideStringField;
    QrOSCabTXTVisExe: TWideStringField;
    QrOSCabTXTExePrv: TWideStringField;
    QrOSCabTXTExeIni: TWideStringField;
    QrOSCabTXTExeFim: TWideStringField;
    DsOSCab: TDataSource;
    GBDados: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit15: TDBEdit;
    QrSiapImaCxa: TmySQLQuery;
    DsSiapImaCxa: TDataSource;
    Label2: TLabel;
    EdCaixa: TdmkEditCB;
    CBCaixa: TdmkDBLookupComboBox;
    EdControle: TdmkEdit;
    Label1: TLabel;
    CkContinuar: TCheckBox;
    Label25: TLabel;
    EdGarantiaDd: TdmkEdit;
    EdHrEvacuar: TdmkEdit;
    Label26: TLabel;
    Label27: TLabel;
    EdHrExecutar: TdmkEdit;
    Label6: TLabel;
    EdValCalc: TdmkEdit;
    EdValInfo: TdmkEdit;
    Label8: TLabel;
    Label10: TLabel;
    EdValDesc_: TdmkEdit;
    EdValTota: TdmkEdit;
    Label11: TLabel;
    RGAutorizado: TdmkRadioGroup;
    QrSiapImaCxaControle: TIntegerField;
    QrSiapImaCxaLocal: TWideStringField;
    Label12: TLabel;
    EdChekLstCab: TdmkEditCB;
    CBChekLstCab: TdmkDBLookupComboBox;
    SBChekLstCab: TSpeedButton;
    QrChekLstCab: TmySQLQuery;
    QrChekLstCabCodigo: TIntegerField;
    QrChekLstCabNome: TWideStringField;
    DsChekLstCab: TDataSource;
    SbCaixa: TSpeedButton;
    MeDetalhes: TdmkMemo;
    dmkLabelRotate1: TdmkLabelRotate;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdValCalcChange(Sender: TObject);
    procedure EdValInfoChange(Sender: TObject);
    procedure EdValDesc_Change(Sender: TObject);
    procedure SBChekLstCabClick(Sender: TObject);
    procedure SbCaixaClick(Sender: TObject);
    procedure DBEdit2Change(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FQrInsUpd: TmySQLQuery;
    FCriou: Boolean;
    //
    procedure CalculaTotal();
  end;

  var
  FmOSCxa: TFmOSCxa;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, UnChekLstJan, MyDBCheck,
  CunsCad, UnOSApp_PF, UnDmkProcFunc;

{$R *.DFM}

procedure TFmOSCxa.BtOKClick(Sender: TObject);
var
  GarantiaDd, HrEvacuar, Codigo, Controle, Caixa,
  Autorizado, ChekLstCab: Integer;
  HrExecutar, ValCalc, ValInfo, ValDesc, ValTota: Double;
  Detalhes: String;
begin
  Codigo     := QrOSCabCodigo.Value;
  Controle   := EdControle.ValueVariant;
  Caixa      := EdCaixa.ValueVariant;
  GarantiaDd := EdGarantiaDd.ValueVariant;
  HrEvacuar  := EdHrEvacuar.ValueVariant;
  HrExecutar := EdHrExecutar.ValueVariant;
  //
  ValCalc := EdValCalc.ValueVariant;
  ValInfo := EdValInfo.ValueVariant;
  //ValDesc := EdValDesc.ValueVariant;
  ValTota := EdValTota.ValueVariant;
  //
  Autorizado := RGAutorizado.ItemIndex;
  Detalhes   := MeDetalhes.Text;
  //
  ChekLstCab := EdChekLstCab.ValueVariant;

  if MyObjects.FIC(Caixa = 0, EdCaixa, 'Informe o servi�o!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('oscxa', 'Controle', '', '',
    tsDef, ImgTipo.SQLType, Controle);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'oscxa', False, [
  'Codigo', 'Caixa', 'GarantiaDd',
  'HrEvacuar', 'HrExecutar', 'ValCalc',
  'ValInfo', (*'ValDesc',*) 'ValTota',
  'Autorizado', 'ChekLstCab', 'Detalhes'
  ], [
  'Controle'], [
  Codigo, Caixa, GarantiaDd,
  HrEvacuar, HrExecutar, ValCalc,
  ValInfo, (*ValDesc,*) ValTota,
  Autorizado, ChekLstCab, Detalhes
  ], [
  Controle], True) then
  begin
    //DmModOS.TotalValorOS(Codigo, gboCaixa);
    OSApp_PF.TotalValorOS(Codigo);
    if FQrInsUpd <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(FQrInsUpd, Dmod.MyDB);
      FQrInsUpd.Locate('Controle', Controle, []);
    end;
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType := stIns;
      EdControle.ValueVariant := 0;
      EdCaixa.ValueVariant := 0;
      CBCaixa.KeyValue := 0;
      EdGarantiaDd.ValueVariant := 0;
      EdHrEvacuar.ValueVariant := 0;
      EdHrExecutar.ValueVariant := 0;
      //EdValDesc.ValueVariant := 0; // Deve ser antes para n�o dar aviso de valor negativo
      EdValCalc.ValueVariant := 0;
      EdValInfo.ValueVariant := 0;
      EdValTota.ValueVariant := 0;
      RGAutorizado.ItemIndex := 0;
      MeDetalhes.Text := '';
      EdCaixa.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmOSCxa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSCxa.CalculaTotal();
var
  Calc, Info, Desc, Tota: Double;
begin
  if FCriou = False then
    Exit;
  //
  Calc := EdValCalc.ValueVariant;
  Info := EdValInfo.ValueVariant;
  Desc := 0; //EdValDesc.ValueVariant;
  Tota := OSApp_PF.TotalValorServico(Calc, Info, Desc);
  EdValTota.ValueVariant := Tota;
end;

procedure TFmOSCxa.DBEdit2Change(Sender: TObject);
begin
  //
{
  UnDmkDAC_PF.AbreMySQLQuery0(QrSiapImaCxa, Dmod.MyDB, [
  'SELECT six.Controle, six.Local ',
  'FROM siapimacxa six ',
  'WHERE six.Codigo=' + Geral.FF0(QrOSCabSiapTerCad.Value),
  '']);
}
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSiapImaCxa, Dmod.MyDB, [
  'SELECT six.Controle, six.Local ',
  'FROM siapimacxa six ',
  'LEFT JOIN siapimacad sic ON sic.Codigo=six.Codigo ',
  'WHERE sic.SiapImaTer=' + Geral.FF0(QrOSCabSiapTerCad.Value),
  '']);
end;

procedure TFmOSCxa.EdValCalcChange(Sender: TObject);
begin
  CalculaTotal();
end;

procedure TFmOSCxa.EdValDesc_Change(Sender: TObject);
begin
  CalculaTotal();
end;

procedure TFmOSCxa.EdValInfoChange(Sender: TObject);
begin
  CalculaTotal();
end;

procedure TFmOSCxa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSCxa.FormCreate(Sender: TObject);
begin
  FCriou := False;
  //
  UnDmkDAC_PF.AbreQuery(QrChekLstCab, Dmod.MyDB);
end;

procedure TFmOSCxa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSCxa.SbCaixaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCunsCad, FmCunsCad, afmoNegarComAviso) then
  begin
    FmCunsCad.LocCod(QrOSCabEntidade.Value,QrOSCabEntidade.Value);
    FmCunsCad.ShowModal;
    FmCunsCad.Destroy;
    //
    QrChekLstCab.Close;
    QrChekLstCab.Open;
  end;
end;

procedure TFmOSCxa.SBChekLstCabClick(Sender: TObject);
begin
  ChekLstJan.MostraChekLstCab();
  UMyMod.SetaCodigoPesquisado(EdChekLstCab, CBChekLstCab, QrChekLstCab,
    VAR_CADASTRO, 'Codigo');
end;

end.
