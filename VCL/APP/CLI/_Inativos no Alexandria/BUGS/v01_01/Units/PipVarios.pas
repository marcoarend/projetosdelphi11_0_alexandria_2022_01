unit PipVarios;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmPipVarios = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    QrEquipAplic: TmySQLQuery;
    QrEquipAplicControle: TIntegerField;
    QrEquipAplicNome: TWideStringField;
    DsEquipAplic: TDataSource;
    PnGeral: TPanel;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    SbEquipAplic: TSpeedButton;
    TPDtaAquis: TdmkEditDateTimePicker;
    EdEquipamento: TdmkEditCB;
    CBEquipamento: TdmkDBLookupComboBox;
    GroupBox3: TGroupBox;
    Panel6: TPanel;
    Label4: TLabel;
    EdQtd: TdmkEdit;
    Label3: TLabel;
    EdPre: TdmkEdit;
    EdFmt: TdmkEdit;
    Label5: TLabel;
    EdSuf: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    EdPrimeiro: TdmkEdit;
    Label8: TLabel;
    EdIni: TdmkEdit;
    QrPsq: TmySQLQuery;
    QrPsqITENS: TLargeintField;
    CkMinusculas: TCheckBox;
    Label9: TLabel;
    EdPrgLstCab: TdmkEditCB;
    CBPrgLstCab: TdmkDBLookupComboBox;
    SBPrgLstCab: TSpeedButton;
    QrPrgLstCab: TmySQLQuery;
    QrPrgLstCabCodigo: TIntegerField;
    QrPrgLstCabNome: TWideStringField;
    DsPrgLstCab: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbEquipAplicClick(Sender: TObject);
    procedure EdPreChange(Sender: TObject);
    procedure EdIniChange(Sender: TObject);
    procedure EdFmtChange(Sender: TObject);
    procedure EdSufChange(Sender: TObject);
    procedure CkMinusculasClick(Sender: TObject);
    procedure SBPrgLstCabClick(Sender: TObject);
  private
    { Private declarations }
    procedure CalculaPrimeiro();
    function  CalculaNomePIP(Seq: Integer): String;
  public
    { Public declarations }
  end;

  var
  FmPipVarios: TFmPipVarios;

implementation

uses UnMyObjects, Module, ModOS, Principal, UMySQLModule, UnBugs_Tabs, DmkDAC_PF,
UnOSApp_PF;

{$R *.DFM}

procedure TFmPipVarios.BtOKClick(Sender: TObject);
var
  Nome, DtaAquis(*, DtaDesativ*), Err: String;
  Codigo, Equipamento(*, OSMonCab, MotDesativ*), I, N, Ini, Fim, PrgLstCab: Integer;
  Continua: Boolean;
begin
  Codigo := 0;
  if ImgTipo.SQLType <> stIns then
  begin
    Geral.MB_ERRO('SQLType inv�lido! Avise a DERMATEK!');
    Exit;
  end;
  //
  PrgLstCab := EdPrgLstCab.ValueVariant;
  if MyObjects.FIC(PrgLstCab = 0, EdPrgLstCab, 'Informe a lista de perguntas!') then
    Exit;
  //
  Ini := EdIni.ValueVariant;
  Fim := Ini + EdQtd.ValueVariant - 1;
  N := 0;
  Err := '';
  for I := Ini to Fim do
  begin
    Nome := CalculaNomePIP(I);
    if Length(Nome) > 30 then
      N := N + 1;
    UnDmkDAC_PF.AbreMySQLQuery0(QrPsq, Dmod.MyDB, [
    'SELECT COUNT(*) ITENS ',
    'FROM pipcad ',
    'WHERE Nome="' + Nome + '" ',
    '']);
    if QrPsqITENS.Value > 0 then
      Err := Err + ', "' + Nome + '"';
  end;
  if N > 0 then
  begin
    Continua := False;
    Geral.MB_Aviso('Inclus�o cancelada! Existem ' + IntToStr(N) +
    ' itens cujo nome tem mais de 30 caracteres!');
  end
  else
  if Err <> '' then
  begin
    Err :=
    'Os PIPs abaixo ser�o duplicados pois j� existem! Deseja continuar assim mesmo?' +
    sLineBreak + Copy(Err, 3);
    Continua := Geral.MB_Pergunta(Err) = ID_YES;
  end else
    Continua := True;
  if not Continua then
    Exit;
  //
  DtaAquis     := Geral.FDT(TPDtaAquis.Date, 1);
  Equipamento  := EdEquipamento.ValueVariant;
  for I := Ini to Fim do
  begin
    Nome := CalculaNomePIP(I);
    //
    Codigo := UMyMod.BPGS1I32('pipcad', 'Codigo', '', '',
      tsPos, ImgTipo.SQLType, 0);
    if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'pipcad', False, [
    'Nome', 'Equipamento', (*'OSMonCab',
    'MotDesativ',*) 'DtaAquis'(*, 'DtaDesativ'*),
    'PrgLstCab'], [
    'Codigo'], [
    Nome, Equipamento, (*OSMonCab,
    MotDesativ,*) DtaAquis(*, DtaDesativ*),
    PrgLstCab], [
    Codigo], True) then
    begin
      //LocCod(Codigo, Codigo);
    end;
  end;
  VAR_CADASTRO := Codigo;
  Close;
end;

procedure TFmPipVarios.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmPipVarios.CalculaNomePIP(Seq: Integer): String;
begin
  Result :=
    Trim(EdPre.Text) + Geral.FFN(Seq, EdFmt.ValueVariant) + Trim(EdSuf.Text);
end;

procedure TFmPipVarios.CalculaPrimeiro();
begin
  EdPrimeiro.Text := CalculaNomePIP(EdIni.ValueVariant);
end;

procedure TFmPipVarios.CkMinusculasClick(Sender: TObject);
var
  CC: TEditCharCase;
begin
  if CkMinusculas.Checked then
    CC := ecNormal
  else
    CC := ecUpperCase;
  //
  EdPre.CharCase := CC;
  EdSuf.CharCase := CC;
  //
  CalculaPrimeiro();
end;

procedure TFmPipVarios.EdFmtChange(Sender: TObject);
begin
  CalculaPrimeiro();
end;

procedure TFmPipVarios.EdIniChange(Sender: TObject);
begin
  CalculaPrimeiro();
end;

procedure TFmPipVarios.EdPreChange(Sender: TObject);
begin
  CalculaPrimeiro();
end;

procedure TFmPipVarios.EdSufChange(Sender: TObject);
begin
  CalculaPrimeiro();
end;

procedure TFmPipVarios.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmPipVarios.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrPrgLstCab, Dmod.MyDB);
  OSApp_PF.ReopenEquipAplic(QrEquipAplic, gbsMonitora);
end;

procedure TFmPipVarios.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPipVarios.SbEquipAplicClick(Sender: TObject);
begin
  FmPrincipal.MostraFormGraG1Equi(gbsMonitora, 0);
  UMyMod.SetaCodigoPesquisado(EdEquipamento, CBEquipamento, QrEquipAplic,
    VAR_CADASTRO, 'Controle');
end;

procedure TFmPipVarios.SBPrgLstCabClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormPrgLstCab(0, 0);
  UMyMod.SetaCodigoPesquisado(EdPrgLstCab, CBPrgLstCab, QrPrgLstCab, VAR_CADASTRO);
end;

end.
