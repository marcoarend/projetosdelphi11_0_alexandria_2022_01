unit OSPrvAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, dmkMemo, dmkEdit, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, mySQLDbTables, Variants, UnDmkEnums;

type
  TFmOSPrvAdd = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    Label52: TLabel;
    Label3: TLabel;
    TPDtHrContat: TdmkEditDateTimePicker;
    EdDtHrContat: TdmkEdit;
    EdFormContat: TdmkEditCB;
    CBFormContat: TdmkDBLookupComboBox;
    SbFormContat: TSpeedButton;
    Label1: TLabel;
    QrFormContat: TmySQLQuery;
    QrFormContatCodigo: TIntegerField;
    QrFormContatNome: TWideStringField;
    DsFormContat: TDataSource;
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNO_ENT: TWideStringField;
    DsEntidades: TDataSource;
    QrEntiContat: TmySQLQuery;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    DsEntiContat: TDataSource;
    GroupBox3: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    CBCliente: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    EdContato: TdmkEditCB;
    CBContato: TdmkDBLookupComboBox;
    BtContatos: TBitBtn;
    QrAgentes: TmySQLQuery;
    QrAgentesCodigo: TIntegerField;
    QrAgentesNome: TWideStringField;
    DsAgentes: TDataSource;
    Panel5: TPanel;
    Label2: TLabel;
    MeNome: TdmkMemo;
    Label6: TLabel;
    EdAgente: TdmkEditCB;
    CBAgente: TdmkDBLookupComboBox;
    CkContinuar: TCheckBox;
    Label7: TLabel;
    EdPrvStatus: TdmkEditCB;
    CBPrvStatus: TdmkDBLookupComboBox;
    SbPrvStatus: TSpeedButton;
    QrOSPrvStatus: TmySQLQuery;
    DsOSPrvStatus: TDataSource;
    QrOSPrvStatusCodigo: TIntegerField;
    QrOSPrvStatusNome: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbFormContatClick(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure BtContatosClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbPrvStatusClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo,
    FControle: Integer;
  end;

  var
  FmOSPrvAdd: TFmOSPrvAdd;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, CfgCadLista, ModuleGeral,
Principal;

{$R *.DFM}

procedure TFmOSPrvAdd.BtContatosClick(Sender: TObject);
begin
  EdCliente.ValueVariant := 0;
  CBCliente.KeyValue     := Null;
  DModG.ReopenEntiContat(QrEntiContat, 0, True);
end;

procedure TFmOSPrvAdd.BtOKClick(Sender: TObject);
var
  Nome, DtHrContat: String;
  Codigo, Controle, FormContat, Cliente, Contato, Agente, PrvStatus: Integer;
begin
  Codigo         := FCodigo;
  Controle       := FControle;
  DtHrContat     := Geral.FDT(TPDtHrContat.Date, 1) + ' ' + EdDtHrContat.Text;
  FormContat     := EdFormContat.ValueVariant;
  Cliente        := EdCliente.ValueVariant;
  Contato        := EdContato.ValueVariant;
  Agente         := EdAgente.ValueVariant;
  Nome           := MeNome.Text;
  PrvStatus      := EdPrvStatus.ValueVariant;
  //
  Controle := UMyMod.BPGS1I32('osprv', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'osprv', False, [
  'Codigo', 'DtHrContat', 'FormContat',
  'Cliente', 'Contato', 'Agente',
  'Nome', 'PrvStatus'], [
  'Controle'], [
  Codigo, DtHrContat, FormContat,
  Cliente, Contato, Agente,
  Nome, PrvStatus], [
  Controle], False) then
  begin
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType := stIns;
      FCodigo   := 0;
      FControle := 0;
      //
      TpDtHrContat.Date := Date;
      EdDtHrContat.ValueVariant := Time;
      MeNome.Text := '';
      //
      MeNome.SetFocus;
    end
    else
      Close;
  end;
end;

procedure TFmOSPrvAdd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSPrvAdd.EdClienteChange(Sender: TObject);
begin
  DModG.ReopenEntiContat(QrEntiContat, EdCliente.ValueVariant, False);
end;

procedure TFmOSPrvAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSPrvAdd.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FCodigo   := 0;
  FControle := 0;
  UMyMod.AbreQuery(QrFormContat, DMod.MyDB);
  UMyMod.AbreQuery(QrEntidades, DMod.MyDB);
  UMyMod.AbreQuery(QrAgentes, DMod.MyDB);
  UMyMod.AbreQuery(QrOSPrvStatus, DMod.MyDB);
end;

procedure TFmOSPrvAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSPrvAdd.SbFormContatClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnCfgCadLista.MostraCadLista(QrFormContat.Database, 'formcontat',
  QrFormContatNome.Size, ncGerlSeq1, 'Cadastro de Formas de Contato',
  [], False, Null, [], [], False);
  //
  UMyMod.SetaCodigoPesquisado(
    EdFormContat, CBFormContat, QrFormContat, VAR_CADASTRO);
end;

procedure TFmOSPrvAdd.SbPrvStatusClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
(*
  UnCfgCadLista.MostraCadLista(QrOSPrvStatus.Database, 'osprvstatus',
  QrOsPrvStatusNome.Size, ncGerlSeq1, 'Cadastro de Formas de Contato', [], False, Null);
*)
  FmPrincipal.MostraFormStatusProvidencias();
  //
  UMyMod.SetaCodigoPesquisado(
    EdPrvStatus, CBPrvStatus, QrOSPrvStatus, VAR_CADASTRO);
end;

end.
