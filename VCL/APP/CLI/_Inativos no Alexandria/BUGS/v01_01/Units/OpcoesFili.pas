unit OpcoesFili;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnDmkProcFunc, UnGOTOy, UnInternalConsts,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,
  dmkGeral, dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, dmkValUsu,
  dmkDBLookupComboBox, dmkEditCB, dmkDBEdit, ComCtrls, Grids, DBGrids, Menus,
  MyDBCheck, Variants, dmkCheckBox, dmkMemo, XMLDoc, xmldom, XMLIntf, Principal,
  dmkEditDateTimePicker, dmkImage, UnDmkEnums, dmkRichEdit;

type
  TFmOpcoesFili = class(TForm)
    PainelDados: TPanel;
    DsOpcoesFili: TDataSource;
    QrOpcoesFili: TmySQLQuery;
    PainelEdita: TPanel;
    PnEditCab: TPanel;
    PnDataCab: TPanel;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdCodigo: TdmkDBEdit;
    DBEdit1: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    QrOpcoesFiliNOMEFILIAL: TWideStringField;
    QrOpcoesFiliFilial: TIntegerField;
    QrOpcoesFiliEntidade: TIntegerField;
    QrOpcoesFiliCodigo: TIntegerField;
    PMFilial: TPopupMenu;
    Adicionafilial1: TMenuItem;
    Alterafilialatual1: TMenuItem;
    Retirafilialatual1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel47: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtFilial: TBitBtn;
    QrOpcoesFiliCNPJCPF_FILIAL: TWideStringField;
    QrOpcoesFiliNumLicOper: TWideStringField;
    QrOpcoesFiliEmergencia: TWideStringField;
    GroupBox1: TGroupBox;
    Panel4: TPanel;
    Label5: TLabel;
    GroupBox2: TGroupBox;
    Panel6: TPanel;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    DBEdit7: TDBEdit;
    QrOpcoesFiliCarimboEmp: TWideStringField;
    GroupBox3: TGroupBox;
    Panel7: TPanel;
    Label12: TLabel;
    DBEdit10: TDBEdit;
    QrOpcoesFiliAgenDGrCab: TIntegerField;
    QrAgenGrCabDia: TmySQLQuery;
    QrAgenGrCabDiaCodigo: TIntegerField;
    QrAgenGrCabDiaNome: TWideStringField;
    QrAgenGrCabDiaColLarg: TIntegerField;
    QrAgenGrCabDiaInterMinut: TIntegerField;
    QrAgenGrCabDiaCorMul: TIntegerField;
    DsAgenGrCabDia: TDataSource;
    GroupBox4: TGroupBox;
    Panel8: TPanel;
    Label17: TLabel;
    QrOpcoesFiliNO_GRCAB_D: TWideStringField;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    QrEntidades: TmySQLQuery;
    IntegerField1: TIntegerField;
    QrEntidadesNome: TWideStringField;
    DsEntidades: TDataSource;
    QrOpcoesFiliRespTec: TIntegerField;
    QrOpcoesFiliRespTecNome: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    Label13: TLabel;
    EdNumLicOper: TdmkEdit;
    Label4: TLabel;
    EdEmergencia: TdmkEdit;
    Label19: TLabel;
    EdCarimboEmp: TdmkEdit;
    SbCarimboEmp: TSpeedButton;
    GroupBox9: TGroupBox;
    Panel16: TPanel;
    Label14: TLabel;
    SpeedButton5: TSpeedButton;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    Label18: TLabel;
    EdAgenDGrCab: TdmkEditCB;
    CbAgenDGrCab: TdmkDBLookupComboBox;
    SbAgenDGrCab: TSpeedButton;
    Label10: TLabel;
    EdImgTitRelR: TdmkEdit;
    Label11: TLabel;
    EdImgTitRelL: TdmkEdit;
    Label15: TLabel;
    RETxtTitRelC: TdmkRichEdit;
    SbTxtTitRelC: TSpeedButton;
    SbImgTitRelL: TSpeedButton;
    SbImgTitRelR: TSpeedButton;
    QrOpcoesFiliImgTitRelR: TWideStringField;
    QrOpcoesFiliImgTitRelL: TWideStringField;
    QrOpcoesFiliTxtTitRelC: TWideMemoField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOpcoesFiliAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOpcoesFiliBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtFilialClick(Sender: TObject);
    procedure Adicionafilial1Click(Sender: TObject);
    procedure Alterafilialatual1Click(Sender: TObject);
    procedure PMFilialPopup(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SbCarimboEmpClick(Sender: TObject);
    procedure SbAgenDGrCabClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SbImgTitRelRClick(Sender: TObject);
    procedure SbImgTitRelLClick(Sender: TObject);
    procedure SbTxtTitRelCClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmOpcoesFili: TFmOpcoesFili;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, ModuleGeral, MyListas, DmkDAC_PF, UnAgendaGerAll,
  Textos;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOpcoesFili.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOpcoesFili.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOpcoesFiliCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOpcoesFili.DefParams;
begin
  VAR_GOTOTABELA := 'OpcoesFili';
  VAR_GOTOMYSQLTABLE := QrOpcoesFili;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT agc.Nome NO_GRCAB_D,');
  VAR_SQLx.Add('IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NOMEFILIAL,');
  VAR_SQLx.Add('IF(ent.Tipo=0,ent.CNPJ,ent.CPF) CNPJCPF_FILIAL,');
  VAR_SQLx.Add('ent.Filial, ent.Codigo Entidade, ');
  VAR_SQLx.Add('IF(ass.Nome<>"", ass.Nome, ent.Nome) RespTecNome,');
  VAR_SQLx.Add('ass.Dados RespTecDocu, ass.Imagem RespTec1, fil.*');
  VAR_SQLx.Add('FROM entidades ent');
  VAR_SQLx.Add('LEFT JOIN opcoesfili fil ON fil.Codigo=ent.Codigo');
  VAR_SQLx.Add('LEFT JOIN agengrcab agc ON agc.Codigo=fil.AgenDGrCab');
  VAR_SQLx.Add('LEFT JOIN entiassina ass ON ass.Entidade=fil.RespTec');
  VAR_SQLx.Add(' ');
  if CO_DMKID_APP IN [4, 5] then
    VAR_SQLx.Add('WHERE ent.CliInt <> 0')
  else
    VAR_SQLx.Add('WHERE ent.Codigo < -10');
  //
  VAR_SQL1.Add('AND fil.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND IF(ent.Tipo=0,ent.RazaoSocial,Nome) Like :P0');
  //
end;

procedure TFmOpcoesFili.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmOpcoesFili.PMFilialPopup(Sender: TObject);
begin
  Alterafilialatual1.Enabled :=
    (QrOpcoesFili.State <> dsInactive) and (QrOpcoesFili.RecordCount > 0);
end;

procedure TFmOpcoesFili.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOpcoesFili.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOpcoesFili.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOpcoesFili.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOpcoesFili.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOpcoesFili.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOpcoesFili.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOpcoesFili.SpeedButton5Click(Sender: TObject);
var
  Entidade: Integer;
begin
  VAR_CADASTRO := 0;
  Entidade     := EdEntidade.ValueVariant;
  //
  DModG.CadastroDeEntidade(Entidade, fmcadEntidade2, fmcadEntidade2);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdEntidade, CBEntidade, QrEntidades, VAR_CADASTRO);
    //
    EdEntidade.SetFocus;
  end;
end;

procedure TFmOpcoesFili.SbAgenDGrCabClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  AgendaGerAll.MostraFormAgenGrCab(EdAgenDGrCab.ValueVariant);
  if VAR_CADASTRO > 0 then
    UMyMod.SetaCodigoPesquisado(
      EdAgenDGrCab, CBAgenDGrCab, QrAgenGrCabDia, VAR_CADASTRO);
end;

procedure TFmOpcoesFili.SbCarimboEmpClick(Sender: TObject);
begin
  MyObjects.DefineArquivo1(Self, EdCarimboEmp);
end;

procedure TFmOpcoesFili.Adicionafilial1Click(Sender: TObject);
var
  Novas: Integer;
begin
  DModG.QrFiliaisOF.Close;
  UMyMod.AbreQuery(DModG.QrFiliaisOF, Dmod.MyDB, 'TFmOpcoesFili.Adicionafilial1Click()');
  if DModG.QrFiliaisOF.RecordCount > 0 then
  begin
    Novas := 0;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO opcoesfili SET Codigo=:P0');
    while not DModG.QrFiliaisOF.Eof do
    begin
      if (DModG.QrFiliaisOFEmpresa.Value = 0) (*and (DModG.QrFiliaisOFEntidade.Value < 0)*)  then
      begin
        Novas := Novas + 1;
        Dmod.QrUpd.Params[0].AsInteger := DModG.QrFiliaisOFEntidade.Value;
        Dmod.QrUpd.ExecSQL;
      end;
      //
      DModG.QrFiliaisOF.Next;
    end;
    case Novas of
      0: Application.MessageBox('N�o foi localizado nenhuma nova empresa!',
         'Informa��o', MB_OK+MB_ICONINFORMATION);
      1: Application.MessageBox('Foi adicionada uma nova empresa!',
         'Informa��o', MB_OK+MB_ICONINFORMATION);
      else Application.MessageBox(PChar('Foram adicionadas ' + IntToStr(Novas) +
         ' novas empresas!'), 'Informa��o', MB_OK+MB_ICONINFORMATION);
    end;
  end else Application.MessageBox('N�o foi localizado nenhuma empresa!',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmOpcoesFili.Alterafilialatual1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrOpcoesFili, [PainelDados],
  [PainelEdita], nil, ImgTipo, 'OpcoesFili');
end;

procedure TFmOpcoesFili.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOpcoesFiliCodigo.Value;
  Close;
end;

procedure TFmOpcoesFili.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  (*
  ImgTitRelR, ImgTitRelL: String;
  TxtTitRelC: AnsiString;
  *)
  //Emergencia, RespTecNome, RespTecDocu, NumLicOper: String;
begin
  {
  NumLicOper     := EdNumLicOper.ValueVariant;
  Emergencia     := EdEmergencia.ValueVariant;
  RespTecNome    := EdRespTecNome.ValueVariant;
  RespTecDocu    := EdRespTecDocu.ValueVariant;
  }
  //
  Codigo := QrOpcoesFiliCodigo.Value;
  (*
  ImgTitRelR := EdImgTitRelR.ValueVariant;
  ImgTitRelL := EdImgTitRelL.ValueVariant;
  TxtTitRelC := MyObjects.ObtemTextoRichEdit(Self, RETxtTitRelC);
  *)
  //
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, FmOpcoesFili, PainelEdita,
    'OpcoesFili', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], ImgTipo, True) then
  begin
    LocCod(Codigo, Codigo);
    Dmod.QrOpcoesFili.Close;
    // Se for apenas uma empresa! -> no caso dos Syn... � mais que uma!
    if pos(',', VAR_LIB_EMPRESAS) = 0 then
      Dmod.ReopenOpcoesFiliPelaEmpresa(Geral.IMV(VAR_LIB_EMPRESAS));
  end;
end;

procedure TFmOpcoesFili.BtDesisteClick(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := QrOpcoesFiliCodigo.Value;
  MostraEdicao(0, stLok, Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'OpcoesFili', 'Codigo');
end;

procedure TFmOpcoesFili.BtFilialClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFilial, BtFilial);
end;

procedure TFmOpcoesFili.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  CriaOForm;
  //
  UnDmkDAC_PF.AbreQuery(QrAgenGrCabDia, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrEntidades, Dmod.MyDB);
  //
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.DefineTextoRichEdit(RETxtTitRelC, QrOpcoesFiliTxtTitRelC.Value);
end;

procedure TFmOpcoesFili.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOpcoesFiliCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOpcoesFili.SbImgTitRelLClick(Sender: TObject);
begin
  MyObjects.DefineArquivo1(Self, EdImgTitRelL);
end;

procedure TFmOpcoesFili.SbImgTitRelRClick(Sender: TObject);
begin
  MyObjects.DefineArquivo1(Self, EdImgTitRelR);
end;

procedure TFmOpcoesFili.SbImprimeClick(Sender: TObject);
begin
//
end;

procedure TFmOpcoesFili.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOpcoesFili.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrOpcoesFiliCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmOpcoesFili.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOpcoesFili.QrOpcoesFiliAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOpcoesFili.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOpcoesFili.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOpcoesFiliCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'OpcoesFili', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOpcoesFili.SbTxtTitRelCClick(Sender: TObject);
(*
var
  MemoryStream: TMemoryStream;
*)
begin
  //
  Application.CreateForm(TFmTextos, FmTextos);
  FmTextos.FAtributesSize   := 10;
  FmTextos.FAtributesName   := 'Arial';
  FmTextos.FSaveSit         := 3;
  FmTextos.FRichEdit        := RETxtTitRelC;
  FmTextos.LBItensMD.Sorted := False;
  with FmTextos.LBItensMD.Items do
  begin
    //Add('[MES_ANO ] Per�odo (m�s e ano) do ???);
    //Add('[MES_ANO ] Per�odo (m�s e ano) do ???);
    //Add('[MES_ANO ] Per�odo (m�s e ano) do ???);
  end;
  FmTextos.LBItensMD.Sorted := True;
  //
  MyObjects.TextoEntreRichEdits(RETxtTitRelC, FmTextos.Editor);
  //
  FmTextos.ShowModal;
  FmTextos.Destroy;
end;

procedure TFmOpcoesFili.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOpcoesFili.QrOpcoesFiliBeforeOpen(DataSet: TDataSet);
begin
  QrOpcoesFiliCodigo.DisplayFormat := FFormatFloat;
end;

end.

