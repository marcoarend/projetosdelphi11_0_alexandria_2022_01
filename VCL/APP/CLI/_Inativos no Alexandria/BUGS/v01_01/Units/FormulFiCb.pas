unit FormulFiCb;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmFormulFiCb = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    DBEdCodigo: TdmkDBEdit;
    Label5: TLabel;
    DBEdNome: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdControle: TdmkEdit;
    Label6: TLabel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrFormulas: TmySQLQuery;
    DsFormulas: TDataSource;
    EdFormula: TdmkEditCB;
    CBFormula: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrFormulasCodigo: TIntegerField;
    QrFormulasNome: TWideStringField;
    EdPerioDd: TdmkEdit;
    Label2: TLabel;
    Label4: TLabel;
    EdDdPostero: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenFormulFiCb(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmFormulFiCb: TFmFormulFiCb;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
Formulas;

{$R *.DFM}

procedure TFmFormulFiCb.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Formula, Periodd, DdPostero: Integer;
begin
  Codigo         := Geral.IMV(DBEdCodigo.Text);
  Controle       := EdControle.ValueVariant;
  Formula        := EdFormula.ValueVariant;
  Periodd        := EdPerioDd.ValueVariant;
  DdPostero      := EdDdPostero.ValueVariant;
  //
  Controle := UMyMod.BPGS1I32('formulficb', 'Controle', '', '', tsPos, ImgTipo.SQLType, Controle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'formulficb', False, [
  'Codigo', 'Formula', 'Periodd',
  'DdPostero'], [
  'Controle'], [
  Codigo, Formula, Periodd,
  DdPostero], [
  Controle], True) then
  begin
    ReopenFormulFiCb(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdControle.ValueVariant  := 0;
      EdFormula.ValueVariant   := 0;
      CbFormula.KeyValue       := Null;
      EdPerioDd.ValueVariant   := 0;
      //
      EdFormula.SetFocus;
    end else Close;
  end;
end;

procedure TFmFormulFiCb.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFormulFiCb.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource := FDsCab;
  DBEdNome.DataSource := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmFormulFiCb.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  UnDmkDAC_PF.AbreQuery(QrFormulas, Dmod.MyDB);
end;

procedure TFmFormulFiCb.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFormulFiCb.ReopenFormulFiCb(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    FQrIts.Open;
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

end.
