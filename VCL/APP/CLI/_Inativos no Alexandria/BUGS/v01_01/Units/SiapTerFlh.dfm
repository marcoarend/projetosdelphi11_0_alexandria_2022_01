object FmSiapTerFlh: TFmSiapTerFlh
  Left = 339
  Top = 185
  Caption = 'CAD-SUBCL-012 :: Padr'#245'es de OSs Filhas'
  ClientHeight = 688
  ClientWidth = 614
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 614
    Height = 64
    Align = alTop
    Caption = ' Dados do cabe'#231'alho:'
    Enabled = False
    TabOrder = 5
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label3: TLabel
      Left = 72
      Top = 20
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object DBEdCodigo: TdmkDBEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taRightJustify
    end
    object DBEdNome: TDBEdit
      Left = 72
      Top = 36
      Width = 529
      Height = 21
      TabStop = False
      Color = clWhite
      DataField = 'Nome'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 614
    Height = 61
    Align = alTop
    Caption = ' Dados do item: '
    TabOrder = 0
    object Label6: TLabel
      Left = 12
      Top = 16
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object Label25: TLabel
      Left = 96
      Top = 16
      Width = 44
      Height = 13
      Caption = 'Empresa:'
      Color = clBtnFace
      ParentColor = False
    end
    object EdControle: TdmkEdit
      Left = 12
      Top = 32
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdEmpresa: TdmkEditCB
      Left = 96
      Top = 32
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Empresa'
      UpdCampo = 'Empresa'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdEmpresaChange
      DBLookupComboBox = CBEmpresa
      IgnoraDBLookupComboBox = False
    end
    object CBEmpresa: TdmkDBLookupComboBox
      Left = 152
      Top = 32
      Width = 449
      Height = 21
      KeyField = 'Filial'
      ListField = 'NOMEFILIAL'
      ListSource = DModG.DsEmpresas
      TabOrder = 2
      dmkEditCB = EdEmpresa
      QryCampo = 'Empresa'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 614
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 566
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 518
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 279
        Height = 32
        Caption = 'Padr'#245'es de OSs Filhas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 279
        Height = 32
        Caption = 'Padr'#245'es de OSs Filhas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 279
        Height = 32
        Caption = 'Padr'#245'es de OSs Filhas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 574
    Width = 614
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    ExplicitTop = 497
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 610
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 618
    Width = 614
    Height = 70
    Align = alBottom
    TabOrder = 2
    ExplicitTop = 541
    object PnSaiDesis: TPanel
      Left = 468
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 466
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object Panel5: TPanel
    Left = 0
    Top = 173
    Width = 614
    Height = 401
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitHeight = 324
    object Label36: TLabel
      Left = 8
      Top = 5
      Width = 79
      Height = 13
      Caption = 'Contratante: [F4]'
      Color = clBtnFace
      ParentColor = False
    end
    object Label2: TLabel
      Left = 528
      Top = 85
      Width = 64
      Height = 13
      Caption = 'Contrato [F4]:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label8: TLabel
      Left = 8
      Top = 45
      Width = 64
      Height = 13
      Caption = 'Pagante: [F4]'
      Color = clBtnFace
      ParentColor = False
    end
    object Label9: TLabel
      Left = 8
      Top = 85
      Width = 40
      Height = 13
      Caption = 'Contato:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label55: TLabel
      Left = 8
      Top = 125
      Width = 92
      Height = 13
      Caption = 'Equipe de agentes:'
      Color = clBtnFace
      ParentColor = False
    end
    object Label10: TLabel
      Left = 344
      Top = 125
      Width = 119
      Height = 13
      Caption = 'Condi'#231#227'o de pagamento:'
    end
    object Label13: TLabel
      Left = 8
      Top = 165
      Width = 39
      Height = 13
      Caption = 'Carteira:'
    end
    object Label38: TLabel
      Left = 8
      Top = 212
      Width = 457
      Height = 13
      AutoSize = False
      Caption = 
        'Data inicial para gera'#231#227'o de OSs autom'#225'ticas (OSs com data anter' +
        'ior n'#227'o geram OSs filhas):'
    end
    object SbDataSincOS: TSpeedButton
      Left = 581
      Top = 208
      Width = 21
      Height = 21
      Caption = '?'
      OnClick = SbDataSincOSClick
    end
    object Label49: TLabel
      Left = 444
      Top = 292
      Width = 64
      Height = 13
      Caption = 'Distancia km:'
    end
    object Label28: TLabel
      Left = 8
      Top = 292
      Width = 222
      Height = 13
      Caption = 'Hor'#225'rio m'#237'nimo de agendamento de execu'#231#227'o:'
    end
    object Label1: TLabel
      Left = 112
      Top = 352
      Width = 257
      Height = 13
      AutoSize = False
      Caption = 'Data de refer'#234'ncia para gera'#231#227'o de OSs autom'#225'ticas*:'
    end
    object EdMonEntCtr: TdmkEditCB
      Left = 8
      Top = 21
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MonEntCtr'
      UpdCampo = 'MonEntCtr'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdMonEntCtrChange
      OnKeyDown = EdMonEntCtrKeyDown
      DBLookupComboBox = CBMonEntCtr
      IgnoraDBLookupComboBox = False
    end
    object CBMonEntCtr: TdmkDBLookupComboBox
      Left = 64
      Top = 21
      Width = 537
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NO_ENT'
      ListSource = DsEntContrat
      TabOrder = 1
      dmkEditCB = EdMonEntCtr
      QryCampo = 'MonEntCtr'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdMonNumCtr: TdmkEdit
      Left = 528
      Top = 101
      Width = 72
      Height = 21
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 6
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MonNumCtr'
      UpdCampo = 'MonNumCtr'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnKeyDown = EdMonNumCtrKeyDown
    end
    object EdMonEntPag: TdmkEditCB
      Left = 8
      Top = 61
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MonEntPag'
      UpdCampo = 'MonEntPag'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdMonEntPagChange
      OnKeyDown = EdMonEntPagKeyDown
      DBLookupComboBox = CBMonEntPag
      IgnoraDBLookupComboBox = False
    end
    object CBMonEntPag: TdmkDBLookupComboBox
      Left = 64
      Top = 61
      Width = 537
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NO_ENT'
      ListSource = DsEntPagante
      TabOrder = 3
      dmkEditCB = EdMonEntPag
      QryCampo = 'MonEntPag'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdMonEntCtt: TdmkEditCB
      Left = 8
      Top = 101
      Width = 44
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MonEntCtt'
      UpdCampo = 'MonEntCtt'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBMonEntCtt
      IgnoraDBLookupComboBox = False
    end
    object CBMonEntCtt: TdmkDBLookupComboBox
      Left = 52
      Top = 101
      Width = 473
      Height = 21
      KeyField = 'Controle'
      ListField = 'Nome'
      ListSource = DsEntiContat
      TabOrder = 5
      dmkEditCB = EdMonEntCtt
      QryCampo = 'MonEntCtt'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdMonAgeEqi: TdmkEditCB
      Left = 8
      Top = 141
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MonAgeEqi'
      UpdCampo = 'MonAgeEqi'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CbMonAgeEqi
      IgnoraDBLookupComboBox = False
    end
    object CbMonAgeEqi: TdmkDBLookupComboBox
      Left = 63
      Top = 141
      Width = 278
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsAgeEqiCab
      TabOrder = 8
      dmkEditCB = EdMonAgeEqi
      QryCampo = 'MonAgeEqi'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdMonCondPg: TdmkEditCB
      Left = 344
      Top = 141
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'MonCondPg'
      UpdCampo = 'MonCondPg'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBMonCondPg
      IgnoraDBLookupComboBox = False
    end
    object CBMonCondPg: TdmkDBLookupComboBox
      Left = 404
      Top = 141
      Width = 197
      Height = 21
      KeyField = 'CodUsu'
      ListField = 'Nome'
      ListSource = DmFatura.DsPediPrzCab
      TabOrder = 10
      dmkEditCB = EdMonCondPg
      QryCampo = 'MonCondPg'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdMonCrtEmi: TdmkEditCB
      Left = 8
      Top = 181
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Cargo'
      UpdCampo = 'MonCrtEmi'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBMonCrtEmi
      IgnoraDBLookupComboBox = False
    end
    object CBMonCrtEmi: TdmkDBLookupComboBox
      Left = 68
      Top = 181
      Width = 533
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DmFatura.DsCartEmis
      TabOrder = 12
      dmkEditCB = EdMonCrtEmi
      QryCampo = 'MonCrtEmi'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object TPDataSincOS: TdmkEditDateTimePicker
      Left = 466
      Top = 208
      Width = 112
      Height = 21
      Date = 0.487372523137310100
      Time = 0.487372523137310100
      TabOrder = 13
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      QryCampo = 'DataSincOS'
      UpdCampo = 'DataSincOS'
      UpdType = utYes
    end
    object EdTrajetDist: TdmkEdit
      Left = 520
      Top = 288
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 14
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Mediakmh5'
      UpdCampo = 'Mediakmh5'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object RGTrajetKm_h: TdmkRadioGroup
      Left = 8
      Top = 232
      Width = 593
      Height = 49
      Caption = ' Padr'#227'o de velocidade m'#233'dia da filial ao cliente:'
      Columns = 5
      ItemIndex = 2
      Items.Strings = (
        'Muito lento'
        'Lento'
        'Normal'
        'R'#225'pido'
        'Muito r'#225'pido')
      TabOrder = 15
      UpdType = utYes
      OldValor = 0
    end
    object EdMinHAgeExe: TdmkEdit
      Left = 244
      Top = 288
      Width = 69
      Height = 21
      TabOrder = 16
      FormatType = dmktfTime
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '00:00'
      QryCampo = 'MinHAgeExe'
      UpdCampo = 'MinHAgeExe'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object QrEntPagante: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades '
      'WHERE Codigo IN ('
      '     SELECT Codigo'
      '     FROM cunscad'
      ')'
      'ORDER BY NO_ENT')
    Left = 464
    Top = 48
    object QrEntPaganteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntPaganteNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntPagante: TDataSource
    DataSet = QrEntPagante
    Left = 492
    Top = 48
  end
  object DsEntContrat: TDataSource
    DataSet = QrEntContrat
    Left = 436
    Top = 48
  end
  object QrEntContrat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'IF(Tipo=0, RazaoSocial, Nome) NO_ENT'
      'FROM entidades '
      'WHERE Codigo IN ('
      '     SELECT Codigo'
      '     FROM cunscad'
      ')'
      'ORDER BY NO_ENT')
    Left = 408
    Top = 48
    object QrEntContratCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntContratNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
  end
  object DsEntiContat: TDataSource
    DataSet = QrEntiContat
    Left = 380
    Top = 48
  end
  object QrEntiContat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT eco.Controle, eco.Nome'
      'FROM enticontat eco'
      'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle'
      'LEFT JOIN enticargos eca ON eca.Codigo=eco.Cargo'
      'WHERE ece.Codigo=:P0')
    Left = 352
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiContatControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEntiContatNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsAgeEqiCab: TDataSource
    DataSet = QrAgeEqiCab
    Left = 324
    Top = 48
  end
  object QrAgeEqiCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ageeqicab '
      'ORDER BY Nome'
      ' ')
    Left = 296
    Top = 48
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAgeEqiCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
end
