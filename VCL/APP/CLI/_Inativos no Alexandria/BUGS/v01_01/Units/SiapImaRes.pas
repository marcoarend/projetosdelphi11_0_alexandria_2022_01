unit SiapImaRes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, dmkMemo, Variants, UnDmkEnums;

type
  TFmSiapImaRes = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    CkContinuar: TCheckBox;
    QrResidentes: TmySQLQuery;
    DsResisdentes: TDataSource;
    CBResidente: TdmkDBLookupComboBox;
    EdNome: TdmkEdit;
    LaOrdem: TLabel;
    SBResidente: TSpeedButton;
    EdResidente: TdmkEditCB;
    LaPrompt: TLabel;
    QrResidentesCodigo: TIntegerField;
    QrResidentesNome: TWideStringField;
    EdAnoNatal: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdCuidado: TdmkEditCB;
    CBCuidado: TdmkDBLookupComboBox;
    SBCuidado: TSpeedButton;
    QrCuidados: TmySQLQuery;
    DsCuidados: TDataSource;
    QrCuidadosCodigo: TIntegerField;
    QrCuidadosNome: TWideStringField;
    MeObservacao: TdmkMemo;
    Label3: TLabel;
    Label4: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBResidenteClick(Sender: TObject);
    procedure SBCuidadoClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodiAtual, FCtrlAtual: Integer;
    FQrySiapImaRes: TmySQLQuery;
  end;

  var
  FmSiapImaRes: TFmSiapImaRes;

implementation

uses UnMyObjects, Module, UMySQLModule, CfgCadLista, MyDBCheck;

{$R *.DFM}

procedure TFmSiapImaRes.BtOKClick(Sender: TObject);
var
  Nome, Observacao: String;
  Codigo, Controle, Residente, AnoNatal, Cuidado: Integer;
begin
  Codigo         := FCodiAtual;
  //Controle       := FCtrlAtual;
  Residente      := EdResidente.ValueVariant;
  Nome           := EdNome.Text;
  AnoNatal       := EdAnoNatal.ValueVariant;
  Cuidado        := EdCuidado.ValueVariant;
  Observacao     := MeObservacao.Text;
  //
  Controle :=
    UMyMod.BPGS1I32('siapimares', 'Controle', '', '', tsPos, ImgTipo.SQLType, FCtrlAtual);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'siapimares', False, [
  'Codigo', 'Residente', 'Nome',
  'AnoNatal', 'Cuidado', 'Observacao'], [
  'Controle'], [
  Codigo, Residente, Nome,
  AnoNatal, Cuidado, Observacao], [
  Controle], True) then
  begin
    if FQrySiapImaRes <> nil then
    begin
      FQrySiapImaRes.Close;
      FQrySiapImaRes.Open;
      FQrySiapImaRes.Locate('Controle', Controle, []);
    end;
    if CkContinuar.Checked then
    begin
      EdResidente.ValueVariant := 0;
      CBResidente.KeyValue := Null;
      //
      EdNome.Text := '';
      EdAnoNatal.ValueVariant := 0;
      EdCuidado.ValueVariant := 0;
      CBCuidado.KeyValue := Null;
      //
      MeObservacao.Text := '';
      //
      EdResidente.SetFocus;
    end else Close;
  end;
end;

procedure TFmSiapImaRes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmSiapImaRes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmSiapImaRes.FormCreate(Sender: TObject);
begin
  QrResidentes.Open;
  QrCuidados.Open;
end;

procedure TFmSiapImaRes.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Idade: String;
  //DataSel: TDateTime;
  Ano, Mes, Dia: Word;
begin
  if Key = VK_F4 then
  begin
    {
    if not DBCheck.ObtemData(Date, DataSel, 0) then Exit;
    DecodeDate(DataSel, Ano, Mes, Dia);
    }
    Idade := '0';
    if InputQuery('Idade', 'Informe a idade', Idade) then
    begin
      DecodeDate(Date, Ano, Mes, Dia);
      Ano := Ano - Geral.IMV(Idade);
      EdAnoNatal.ValueVariant := Ano;
    end;
  end;
end;

procedure TFmSiapImaRes.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmSiapImaRes.SBCuidadoClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnCfgCadLista.MostraCadLista(QrCuidados.Database, 'cuidados',
  QrCuidadosNome.Size, ncGerlSeq1, 'Cadastro de Cuidados',
  [], False, Null, [], [], False);
  //
  UMyMod.SetaCodigoPesquisado(EdCuidado, CBCuidado, QrCuidados, VAR_CADASTRO);
end;

procedure TFmSiapImaRes.SBResidenteClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  UnCfgCadLista.MostraCadLista(QrResidentes.Database, 'residentes',
  QrResidentesNome.Size, ncGerlSeq1, 'Cadastro de Residentes',
  [], False, Null, [], [], False);
  //
  UMyMod.SetaCodigoPesquisado(EdResidente, CBResidente, QrResidentes, VAR_CADASTRO);
end;

end.

