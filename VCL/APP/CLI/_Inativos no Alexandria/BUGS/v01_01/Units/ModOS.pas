unit ModOS;

interface

uses DBGrids, Graphics, SysUtils, Classes, UITypes, Generics.Collections,
  UnMyQRCode, QRCode, frxClass, frxDBSet, Data.DB, mySQLDbTables, Windows,
  DmkGeral, DmkDAC_PF, UnAppListas, UnInternalConsts, UnDmkProcFunc, UnBugs_Tabs,
  Forms, Controls, StdCtrls, ComCtrls, Menus, UnDmkEnums, UnProjGroup_Consts,
  AdvToolBar;

type
  TDmModOS = class(TDataModule)
    QrEntidades: TmySQLQuery;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNO_ENT: TWideStringField;
    DsEntidades: TDataSource;
    QrSiapTerCad: TmySQLQuery;
    QrSiapTerCadCodigo: TIntegerField;
    QrSiapTerCadNome: TWideStringField;
    DsSiapterCad: TDataSource;
    QrFatoGeradr: TmySQLQuery;
    QrFatoGeradrCodigo: TIntegerField;
    QrFatoGeradrNome: TWideStringField;
    QrFatoGeradrNeedOS_Ori: TSmallintField;
    DsFatoGeradr: TDataSource;
    QrPediPrzCab: TmySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzCabMaxDesco: TFloatField;
    QrPediPrzCabJurosMes: TFloatField;
    QrPediPrzCabParcelas: TIntegerField;
    QrPediPrzCabPercent1: TFloatField;
    QrPediPrzCabPercent2: TFloatField;
    DsPediPrzCab: TDataSource;
    QrOSSrv: TmySQLQuery;
    QrOSSrvNO_DesServico: TWideStringField;
    QrOSSrvCodigo: TIntegerField;
    QrOSSrvControle: TIntegerField;
    QrOSSrvDesServico: TIntegerField;
    QrOSSrvLk: TIntegerField;
    QrOSSrvDataCad: TDateField;
    QrOSSrvDataAlt: TDateField;
    QrOSSrvUserCad: TIntegerField;
    QrOSSrvUserAlt: TIntegerField;
    QrOSSrvAlterWeb: TSmallintField;
    QrOSSrvAtivo: TSmallintField;
    DsOSSrv: TDataSource;
    QrOSFrmCab: TmySQLQuery;
    DsOSFrmCab: TDataSource;
    QrEntiContat: TmySQLQuery;
    DsEntiContat: TDataSource;
    QrEntContrat: TmySQLQuery;
    DsEntContrat: TDataSource;
    QrEntPagante: TmySQLQuery;
    DsEntPagante: TDataSource;
    QrEstatusOSs: TmySQLQuery;
    QrEstatusOSsCodigo: TIntegerField;
    QrEstatusOSsNome: TWideStringField;
    DsEstatusOSs: TDataSource;
    QrEntPaganteCodigo: TIntegerField;
    QrEntPaganteNO_ENT: TWideStringField;
    QrEntContratCodigo: TIntegerField;
    QrEntContratNO_ENT: TWideStringField;
    QrEntiContatControle: TIntegerField;
    QrEntiContatNome: TWideStringField;
    QrOSPrz: TmySQLQuery;
    DsOSPrz: TDataSource;
    QrOSCabAlv: TmySQLQuery;
    DsOSCabAlv: TDataSource;
    QrOSSrvGarantiaDd: TIntegerField;
    QrOSSrvHrEvacuar: TIntegerField;
    QrOSSrvHrExecutar: TFloatField;
    QrOSAlv: TmySQLQuery;
    DsOSAlv: TDataSource;
    QrOSAlvCodigo: TIntegerField;
    QrOSAlvControle: TIntegerField;
    QrOSAlvConta: TIntegerField;
    QrOSFrmRec: TmySQLQuery;
    DsOSFrmRec: TDataSource;
    QrOSFrmCabCodigo: TIntegerField;
    QrOSFrmCabControle: TIntegerField;
    QrOSFrmCabConta: TIntegerField;
    QrOSFrmCabNome: TWideStringField;
    QrOSFrmCabFormula: TIntegerField;
    QrOSFrmCabEquipAplic: TIntegerField;
    QrOSFrmCabQtdTot: TFloatField;
    QrOSFrmCabQtdQSP: TFloatField;
    QrOSFrmCabCusTot: TFloatField;
    QrOSFrmCabNO_FORMULA: TWideStringField;
    QrOSFrmCabNO_EquipAplic: TWideStringField;
    QrOSFrmRecNO_GG1: TWideStringField;
    QrOSFrmRecCodigo: TIntegerField;
    QrOSFrmRecControle: TIntegerField;
    QrOSFrmRecConta: TIntegerField;
    QrOSFrmRecIDIts: TIntegerField;
    QrOSFrmRecGraGruX: TIntegerField;
    QrOSFrmRecPrvQtd: TFloatField;
    QrOSFrmRecPrvPrc: TFloatField;
    QrOSFrmRecPrvVal: TFloatField;
    QrOSFrmRecUsoQtd: TFloatField;
    QrOSFrmRecUsoPrc: TFloatField;
    QrOSFrmRecUsoVal: TFloatField;
    QrOSFrmRecUsoDec: TFloatField;
    QrOSFrmRecUsoTot: TFloatField;
    QrOSFrmRecOrdem: TIntegerField;
    QrOSFrmRecReordem: TIntegerField;
    QrOSMonCab: TmySQLQuery;
    QrOSMonRec: TmySQLQuery;
    DsOSMonCab: TDataSource;
    DsOSMonRec: TDataSource;
    QrOSMonCabCodigo: TIntegerField;
    QrOSMonCabControle: TIntegerField;
    QrOSMonCabConta: TIntegerField;
    QrOSMonCabNome: TWideStringField;
    QrOSMonCabFormula: TIntegerField;
    QrOSMonCabEquipAplic: TIntegerField;
    QrOSMonCabQtdTot: TFloatField;
    QrOSMonCabQtdQSP: TFloatField;
    QrOSMonCabCusTot: TFloatField;
    QrOSMonCabNO_FORMULA: TWideStringField;
    QrOSMonCabNO_EquipAplic: TWideStringField;
    QrOSMonRecNO_GG1: TWideStringField;
    QrOSMonRecCodigo: TIntegerField;
    QrOSMonRecControle: TIntegerField;
    QrOSMonRecConta: TIntegerField;
    QrOSMonRecIDIts: TIntegerField;
    QrOSMonRecGraGruX: TIntegerField;
    QrOSMonRecPrvQtd: TFloatField;
    QrOSMonRecPrvPrc: TFloatField;
    QrOSMonRecPrvVal: TFloatField;
    QrOSMonRecUsoQtd: TFloatField;
    QrOSMonRecUsoPrc: TFloatField;
    QrOSMonRecUsoVal: TFloatField;
    QrOSMonRecUsoDec: TFloatField;
    QrOSMonRecUsoTot: TFloatField;
    QrOSMonRecOrdem: TIntegerField;
    QrOSMonRecReordem: TIntegerField;
    QrTotalOS: TmySQLQuery;
    QrTotalOSValCalc: TFloatField;
    QrTotalOSValInfo: TFloatField;
    QrTotalOSValDesc: TFloatField;
    QrTotalOSValTota: TFloatField;
    QrOSSrvValCalc: TFloatField;
    QrOSSrvValInfo: TFloatField;
    QrOSSrvValDesc: TFloatField;
    QrOSSrvValTota: TFloatField;
    QrOSSrvAutorizado: TSmallintField;
    QrOSSrvVAL_CALCeINFO: TFloatField;
    QrOSAlvNO_Praga_Z: TWideStringField;
    QrOSAlvPraga_Z: TIntegerField;
    QrOSCabAlvCodigo: TIntegerField;
    QrOSCabAlvControle: TIntegerField;
    QrOSCabAlvOrdem: TSmallintField;
    QrOSCabAlvLk: TIntegerField;
    QrOSCabAlvDataCad: TDateField;
    QrOSCabAlvDataAlt: TDateField;
    QrOSCabAlvUserCad: TIntegerField;
    QrOSCabAlvUserAlt: TIntegerField;
    QrOSCabAlvAlterWeb: TSmallintField;
    QrOSCabAlvAtivo: TSmallintField;
    QrOSCabAlvPraga_A: TIntegerField;
    QrOSCabAlvPraga_Z: TIntegerField;
    QrOSCabAlvNO_NIVEL: TWideStringField;
    QrOSCabAlvNO_PRAGA: TWideStringField;
    QrOSSrvAUTORIZADO_BOOL: TBooleanField;
    QrOSPrzCodigo: TIntegerField;
    QrOSPrzControle: TIntegerField;
    QrOSPrzCondicao: TIntegerField;
    QrOSPrzNO_CONDICAO: TWideStringField;
    QrOSPrzDescoPer: TFloatField;
    TbDesServico: TmySQLTable;
    TbDesServicoCodigo: TIntegerField;
    TbDesServicoNome: TWideStringField;
    DsDesServico: TDataSource;
    TbDesServicoSigla: TWideStringField;
    QrOSSrvNO_SIGLA: TWideStringField;
    QrOSPrzEscolhido: TSmallintField;
    QrSTC: TmySQLQuery;
    QrSTCNOMELOGRAD: TWideStringField;
    QrSTCCodigo: TIntegerField;
    QrSTCNome: TWideStringField;
    QrSTCCliente: TIntegerField;
    QrSTCSLograd: TSmallintField;
    QrSTCSRua: TWideStringField;
    QrSTCSNumero: TIntegerField;
    QrSTCSCompl: TWideStringField;
    QrSTCSBairro: TWideStringField;
    QrSTCSCidade: TWideStringField;
    QrSTCSUF: TWideStringField;
    QrSTCSCEP: TIntegerField;
    QrSTCSPais: TWideStringField;
    QrSTCSEndeRef: TWideStringField;
    QrSTCSCodMunici: TIntegerField;
    QrSTCSCodiPais: TIntegerField;
    QrSTCSTe1: TWideStringField;
    QrSTCM2Constru: TFloatField;
    QrSTCM2NaoBuild: TFloatField;
    QrSTCM2Terreno: TFloatField;
    QrSTCM2Total: TFloatField;
    QrLctFatRef: TmySQLQuery;
    QrLctFatRefPARCELA: TIntegerField;
    QrLctFatRefCodigo: TIntegerField;
    QrLctFatRefControle: TIntegerField;
    QrLctFatRefConta: TIntegerField;
    QrLctFatRefLancto: TLargeintField;
    QrLctFatRefValor: TFloatField;
    QrLctFatRefVencto: TDateField;
    DsLctFatRef: TDataSource;
    QrOSFrmAbr: TmySQLQuery;
    DsOSFrmAbr: TDataSource;
    QrOSFrmAbrNO_ABRANGE: TWideStringField;
    QrOSFrmAbrCodigo: TIntegerField;
    QrOSFrmAbrControle: TIntegerField;
    QrOSFrmAbrConta: TIntegerField;
    QrOSFrmAbrIDIts: TIntegerField;
    QrOSFrmAbrAbrangicie: TIntegerField;
    QrEntiMail: TmySQLQuery;
    QrEntiMailNOMEETC: TWideStringField;
    QrEntiMailConta: TIntegerField;
    QrEntiMailEMail: TWideStringField;
    QrEntiMailEntiTipCto: TIntegerField;
    QrEntiMailOrdem: TIntegerField;
    DsEntiMail: TDataSource;
    QrEntiTel: TmySQLQuery;
    QrEntiTelNOMEETC: TWideStringField;
    QrEntiTelConta: TIntegerField;
    QrEntiTelTelefone: TWideStringField;
    QrEntiTelEntiTipCto: TIntegerField;
    QrEntiTelTEL_TXT: TWideStringField;
    QrEntiTelRamal: TWideStringField;
    DsEntiTel: TDataSource;
    QrOSPos: TmySQLQuery;
    DsOSPos: TDataSource;
    QrOSPosCodigo: TIntegerField;
    QrOSPosControle: TIntegerField;
    QrOSPosDias: TIntegerField;
    QrOSFrmDep: TmySQLQuery;
    DsOSFrmDep: TDataSource;
    QrOSFrmDepCodigo: TIntegerField;
    QrOSFrmDepControle: TIntegerField;
    QrOSFrmDepConta: TIntegerField;
    QrOSFrmDepTabela: TSmallintField;
    QrOSFrmDepCadastro: TIntegerField;
    QrOSFrmDepLk: TIntegerField;
    QrOSFrmDepDataCad: TDateField;
    QrOSFrmDepDataAlt: TDateField;
    QrOSFrmDepUserCad: TIntegerField;
    QrOSFrmDepUserAlt: TIntegerField;
    QrOSFrmDepAlterWeb: TSmallintField;
    QrOSFrmDepAtivo: TSmallintField;
    QrOSFrmDepSIGLA: TWideStringField;
    QrOSFrmDepNO_Campo: TWideStringField;
    QrOSFrmDepIDIts: TIntegerField;
    QrOSCxa: TmySQLQuery;
    DsOSCxa: TDataSource;
    QrOSCxaAtrib: TmySQLQuery;
    QrOSCxaAtribID_Item: TIntegerField;
    QrOSCxaAtribID_Sorc: TIntegerField;
    QrOSCxaAtribAtrCad: TIntegerField;
    QrOSCxaAtribAtrIts: TIntegerField;
    QrOSCxaAtribCU_CAD: TIntegerField;
    QrOSCxaAtribCU_ITS: TIntegerField;
    QrOSCxaAtribNO_CAD: TWideStringField;
    QrOSCxaAtribNO_ITS: TWideStringField;
    DsOSCxaAtrib: TDataSource;
    QrOSCxaCodigo: TIntegerField;
    QrOSCxaControle: TIntegerField;
    QrOSCxaCaixa: TIntegerField;
    QrOSCxaGarantiaDd: TIntegerField;
    QrOSCxaHrEvacuar: TIntegerField;
    QrOSCxaHrExecutar: TFloatField;
    QrOSCxaValCalc: TFloatField;
    QrOSCxaValInfo: TFloatField;
    QrOSCxaValDesc: TFloatField;
    QrOSCxaValTota: TFloatField;
    QrOSCxaAutorizado: TSmallintField;
    QrOSCxaLk: TIntegerField;
    QrOSCxaDataCad: TDateField;
    QrOSCxaDataAlt: TDateField;
    QrOSCxaUserCad: TIntegerField;
    QrOSCxaUserAlt: TIntegerField;
    QrOSCxaAlterWeb: TSmallintField;
    QrOSCxaAtivo: TSmallintField;
    QrOSCxaNO_MATERIAL: TWideStringField;
    QrOSCxaNOME_FORMA: TWideStringField;
    QrOSCxaMatersCxa: TIntegerField;
    QrOSCxaFormasCxa: TIntegerField;
    QrOSCxaVolumeL: TFloatField;
    QrOSCxaLocal: TWideStringField;
    QrOSCxaAcesso: TWideStringField;
    QrOSCxaMedidas: TWideStringField;
    QrFormulIF: TmySQLQuery;
    DsFormulIF: TDataSource;
    QrFormulIA: TmySQLQuery;
    DsFormulIA: TDataSource;
    QrFormulIFCodigo: TIntegerField;
    QrFormulIFGraGruX: TIntegerField;
    QrFormulIFPrvQtd: TFloatField;
    QrFormulIFPrvPrc: TFloatField;
    QrFormulIFPrvVal: TFloatField;
    QrFormulIFUsoQtd: TFloatField;
    QrFormulIFUsoPrc: TFloatField;
    QrFormulIFUsoVal: TFloatField;
    QrFormulIFUsoDec: TFloatField;
    QrFormulIFUsoTot: TFloatField;
    QrFormulIFOrdem: TIntegerField;
    QrFormulIFReordem: TIntegerField;
    QrFormulIACodigo: TIntegerField;
    QrFormulIAControle: TIntegerField;
    QrFormulIAAbrangicie: TIntegerField;
    QrFormulIFControle: TIntegerField;
    DsExeTxtCli1: TDataSource;
    QrExeTxtCli1: TmySQLQuery;
    QrExeTxtCli1Codigo: TIntegerField;
    QrExeTxtCli1Nome: TWideStringField;
    QrOSCxaChekLstCab: TIntegerField;
    QrAgentes: TmySQLQuery;
    QrAgentesCodigo: TIntegerField;
    QrAgentesNome: TWideStringField;
    DsAgentes: TDataSource;
    QrOSCxaDetalhes: TWideMemoField;
    QrOSSrvDetalhes: TWideMemoField;
    QrOSMonCabPipCad: TIntegerField;
    QrOSMonCabNO_PIP: TWideStringField;
    QrOSPipMon: TmySQLQuery;
    DsOSPipMon: TDataSource;
    QrOSPipMonNO_PIP: TWideStringField;
    QrOSPipMonCodigo: TIntegerField;
    QrOSPipMonControle: TIntegerField;
    QrOSPipMonPipCad: TIntegerField;
    QrPIPs: TmySQLQuery;
    QrPIPsCodigo: TIntegerField;
    QrOSCxI: TmySQLQuery;
    QrOSCxICodigo: TIntegerField;
    QrOSCxIControle: TIntegerField;
    QrOSCxIFotoCxa: TWideStringField;
    QrOSCxIDetalhes: TWideMemoField;
    QrOSCxILk: TIntegerField;
    QrOSCxIDataCad: TDateField;
    QrOSCxIDataAlt: TDateField;
    QrOSCxIUserCad: TIntegerField;
    QrOSCxIUserAlt: TIntegerField;
    QrOSCxIAlterWeb: TSmallintField;
    QrOSCxIAtivo: TSmallintField;
    DsOSCxI: TDataSource;
    QrOSFrmCabDiluente: TSmallintField;
    QrOSFrmCabTipoAplica: TIntegerField;
    QrOSMonCabTipoAplica: TIntegerField;
    QrOSMonCabDiluente: TIntegerField;
    DsOSAge: TDataSource;
    QrOSAge: TmySQLQuery;
    QrOSAgeNO_AGENTE: TWideStringField;
    QrOSAgeCodigo: TIntegerField;
    QrOSAgeControle: TIntegerField;
    QrOSAgeAgente: TIntegerField;
    QrOSAgeResponsa: TSmallintField;
    QrFormulIFEhDiluente: TSmallintField;
    QrOSMonCabUnidMed: TIntegerField;
    QrOSMonCabCU_UNIDMED: TIntegerField;
    QrOSMonCabSIGLA: TWideStringField;
    QrOSMonCabNO_UNIDMED: TWideStringField;
    QrOSFrmCabUnidMed: TIntegerField;
    QrOSFrmCabCU_UNIDMED: TIntegerField;
    QrOSFrmCabSIGLA: TWideStringField;
    QrOSFrmCabNO_UNIDMED: TWideStringField;
    QrOSCxIAplicacao: TIntegerField;
    QrOSCxINO_APLICACAO: TWideStringField;
    QrLo2PIP: TmySQLQuery;
    QrLo2PIPCodigo: TIntegerField;
    QrLo2PIPControle: TIntegerField;
    QrLo2PIPPipCad: TIntegerField;
    QrLo2PIPOrdem: TIntegerField;
    QrLo2PIPReordem: TIntegerField;
    QrLocCGCS: TmySQLQuery;
    QrLocCGCSControle: TIntegerField;
    QrExeTxtCli2: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsExeTxtCli2: TDataSource;
    QrEscolhOSPrz: TmySQLQuery;
    QrEscolhOSPrzDescoPer: TFloatField;
    QrAgeEmOS: TmySQLQuery;
    QrAgeEmOSControle: TIntegerField;
    QrOSChk: TmySQLQuery;
    QrOSChkNO_ITEM: TWideStringField;
    QrOSChkCodigo: TIntegerField;
    QrOSChkControle: TIntegerField;
    QrOSChkChekLstIts: TIntegerField;
    DsOSChk: TDataSource;
    QrLocOS: TmySQLQuery;
    QrLocOSGrupo: TIntegerField;
    QrLocOSOpcao: TIntegerField;
    QrLocOSSiapTerCad: TIntegerField;
    QrLocOSCodigo: TIntegerField;
    QrLastEvo: TmySQLQuery;
    QrLastEvoPercFeito: TFloatField;
    QrEvoFrm: TmySQLQuery;
    QrEvoFrmITENS: TLargeintField;
    QrEvoFrmSUMPERC: TFloatField;
    QrEvoFrmMEDIA: TFloatField;
    QrFormulFiCb: TmySQLQuery;
    DsFormulFiCb: TDataSource;
    QrOSOriPsq: TmySQLQuery;
    QrOSOriPsqCodigo: TIntegerField;
    QrOSOriPsqGrupo: TIntegerField;
    QrFormulFiDd: TmySQLQuery;
    DsFormulFiDd: TDataSource;
    QrFormulFiCbCodigo: TIntegerField;
    QrFormulFiCbControle: TIntegerField;
    QrFormulFiCbFormula: TIntegerField;
    QrFormulFiCbPeriodd: TIntegerField;
    QrFormulFiDdCodigo: TIntegerField;
    QrFormulFiDdControle: TIntegerField;
    QrFormulFiDdConta: TIntegerField;
    QrFormulFiDdOrdem: TIntegerField;
    QrFormulFiDdDias: TIntegerField;
    QrFrmFil: TmySQLQuery;
    QrFrmFilITENS: TLargeintField;
    QrNulServico: TmySQLQuery;
    QrNulServicoCodigo: TFloatField;
    QrDesServico: TmySQLQuery;
    QrDesServicoCodigo: TIntegerField;
    QrDesServicoNome: TWideStringField;
    QrDesServicoSigla: TWideStringField;
    QrMSrv: TmySQLQuery;
    QrMSrvMulServico: TFloatField;
    QrAgeEqiIts: TmySQLQuery;
    QrAgeEqiItsControle: TIntegerField;
    QrAgeEqiItsEntidade: TIntegerField;
    QrAgeEqiItsEhLider: TSmallintField;
    QrFormulFiCbDdPostero: TIntegerField;
    QrOSMonCabNO_prglstcab: TWideStringField;
    QrOSMonCabNO_DEPENDENCI: TWideStringField;
    QrOSMonCabPerioDd: TIntegerField;
    QrOSMonCabDdPostero: TIntegerField;
    QrPIPCad: TmySQLQuery;
    QrPIPCadCodigo: TIntegerField;
    QrPIPCadNome: TWideStringField;
    QrPIPCadEquipamento: TIntegerField;
    QrPIPCadOSMonCab: TIntegerField;
    QrPIPCadMotDesativ: TIntegerField;
    QrPIPCadDtaAquis: TDateField;
    QrPIPCadDtaDesativ: TDateField;
    QrPIPCadPrgLstCab: TIntegerField;
    QrPIPCadDependenci: TIntegerField;
    QrOsPM: TmySQLQuery;
    QrOsPMCodigo: TIntegerField;
    QrOsPMControle: TIntegerField;
    QrOsPMPipCad: TIntegerField;
    QrOsPMOrdem: TIntegerField;
    QrOsPMReordem: TIntegerField;
    QrOsPMPrgLstCab: TIntegerField;
    QrOsPMOSFMCbMae: TIntegerField;
    QrOsPMInsercoes: TIntegerField;
    QrLocPip: TmySQLQuery;
    QrLocPipCodigo: TIntegerField;
    QrLocPipPrgLstCab: TIntegerField;
    QrLocPipNome: TWideStringField;
    QrLocPipEquipamento: TIntegerField;
    QrLocPipOSMonCab: TIntegerField;
    QrLocPipMotDesativ: TIntegerField;
    QrLocPipDtaAquis: TDateField;
    QrLocPipDtaDesativ: TDateField;
    QrLocPipDependenci: TIntegerField;
    QrLocPipOrdem: TIntegerField;
    QrGgxPip: TmySQLQuery;
    QrGgxPipCodigo: TIntegerField;
    QrGgxPipControle: TIntegerField;
    QrGgxPipConta: TLargeintField;
    QrGgxPipIDIts: TLargeintField;
    QrGgxPipOrdem: TIntegerField;
    QrGgxPipGraGruX: TIntegerField;
    QrGgxPipUsoQtd: TFloatField;
    QrGgxPipValCliDd: TIntegerField;
    QrGgxPipTabela: TLargeintField;
    QrPrgLstIts: TmySQLQuery;
    QrPrgLstItsCodigo: TIntegerField;
    QrPrgLstItsControle: TIntegerField;
    QrPrgLstItsNome: TWideStringField;
    QrPrgLstItsFuncoes: TSmallintField;
    QrPrgLstItsDependente: TSmallintField;
    QrPrgLstItsPrgAtrCad: TIntegerField;
    QrPrgLstItsLk: TIntegerField;
    QrPrgLstItsDataCad: TDateField;
    QrPrgLstItsDataAlt: TDateField;
    QrPrgLstItsUserCad: TIntegerField;
    QrPrgLstItsUserAlt: TIntegerField;
    QrPrgLstItsAlterWeb: TSmallintField;
    QrPrgLstItsAtivo: TSmallintField;
    QrPrgLstItsOrdem: TIntegerField;
    QrPrgLstItsFiliacao: TIntegerField;
    QrPrgLstItsRelacao: TIntegerField;
    TbPrgCadPrg: TmySQLTable;
    DsPrgCadPrg: TDataSource;
    TbPrgCadPrgCodigo: TIntegerField;
    TbPrgCadPrgNome: TWideStringField;
    TbPrgCadPrgSigla: TWideStringField;
    QrPrgLstItsNivel: TIntegerField;
    QrPrgLstItsNivSeq: TIntegerField;
    QrPrgLstItsPergunta: TIntegerField;
    QrPrgLstItsBinarCad0: TIntegerField;
    QrPrgLstItsBinarCad1: TIntegerField;
    QrPrgLstItsAcaoPrd: TSmallintField;
    QrUsoRatif: TmySQLQuery;
    QrUsoRatifIDIts: TIntegerField;
    QrUsoRatifGraGruX: TIntegerField;
    QrUsoRatifPrvQtd: TFloatField;
    QrUsoRatifCtrlGGV: TIntegerField;
    QrUsoRatifCustoPreco: TFloatField;
    QrErrRatif: TmySQLQuery;
    frxGER_OSERV_001_ModOS_01: TfrxReport;
    QrErrRatifGraGruX: TIntegerField;
    QrErrRatifGraGru1: TIntegerField;
    QrErrRatifNO_PRD_TAM_COR: TWideStringField;
    QrPrgLstItsBinario0: TWideStringField;
    QrPrgLstItsBinario1: TWideStringField;
    QrPrgLstItsLupForma: TSmallintField;
    QrPrgLstItsLupQtdVzs: TSmallintField;
    QrAge: TmySQLQuery;
    QrAgeAgente: TIntegerField;
    QrGgxPipDtaExeIni: TDateTimeField;
    QrPrzSel: TmySQLQuery;
    QrPrzSelCondicao: TIntegerField;
    QrPrzSelControle: TIntegerField;
    QrUL_OSCab: TmySQLQuery;
    QrUL_OSCabCodigo: TIntegerField;
    QrUL_OSCabGrupo: TIntegerField;
    QrUL_OSCabOpcao: TIntegerField;
    QrUL_OSCabEmpresa: TIntegerField;
    QrUL_OSCabEntidade: TIntegerField;
    QrUL_OSCabSiapTerCad: TIntegerField;
    QrUL_OSCabEstatus: TIntegerField;
    QrUL_OSCabFatoGeradr: TIntegerField;
    QrUL_OSCabDtaContat: TDateTimeField;
    QrUL_OSCabDtaVisPrv: TDateTimeField;
    QrUL_OSCabFimVisPrv: TDateTimeField;
    QrUL_OSCabDtaExePrv: TDateTimeField;
    QrUL_OSCabFimVisExe: TDateTimeField;
    QrUL_OSCabDtaVisExe: TDateTimeField;
    QrUL_OSCabFimExePrv: TDateTimeField;
    QrUL_OSCabDtaExeIni: TDateTimeField;
    QrUL_OSCabDtaExeFim: TDateTimeField;
    QrUL_OSCabOrcamServi: TFloatField;
    QrUL_OSCabOrcamDesco: TFloatField;
    QrUL_OSCabOrcamOutrs: TFloatField;
    QrUL_OSCabOrcamTotal: TFloatField;
    QrUL_OSCabInvalServi: TFloatField;
    QrUL_OSCabInvalDesco: TFloatField;
    QrUL_OSCabInvalOutrs: TFloatField;
    QrUL_OSCabInvalTotal: TFloatField;
    QrUL_OSCabValorServi: TFloatField;
    QrUL_OSCabValorDesco: TFloatField;
    QrUL_OSCabValorOutrs: TFloatField;
    QrUL_OSCabValorTotal: TFloatField;
    QrUL_OSCabEntiContat: TIntegerField;
    QrUL_OSCabNumContrat: TIntegerField;
    QrUL_OSCabEntPagante: TIntegerField;
    QrUL_OSCabEntContrat: TIntegerField;
    QrUL_OSCabCondicaoPg: TIntegerField;
    QrUL_OSCabOperacao: TIntegerField;
    QrUL_OSCabObsExecuta: TWideMemoField;
    QrUL_OSCabExeTxtCli1: TIntegerField;
    QrUL_OSCabExeTxtCli2: TIntegerField;
    QrUL_OSCabMulServico: TLargeintField;
    QrUL_OSCabAgeEqiCab: TIntegerField;
    QrUL_OSCabNO_ENTICONTAT: TWideStringField;
    QrUL_OSCabNO_FatoGeradr: TWideStringField;
    QrUL_OSCabNO_ESTATUS: TWideStringField;
    QrUL_OSCabNO_SiapTerCad: TWideStringField;
    QrUL_OSCabLstCusPrd: TIntegerField;
    QrUL_OSCabNO_ENT: TWideStringField;
    QrUL_OSCabTel_ENT: TWideStringField;
    QrUL_OSCabNO_PAG: TWideStringField;
    QrUL_OSCabNO_CTR: TWideStringField;
    QrUL_OSCabNO_CART: TWideStringField;
    QrUL_OSCabNO_PRZ: TWideStringField;
    QrUL_OSCabNO_ExeTxtCli1: TWideStringField;
    QrUL_OSCabNO_ExeTxtCli2: TWideStringField;
    QrUL_OSCabNO_AgeEqiCab: TWideStringField;
    QrUL_OSPipMon: TmySQLQuery;
    QrUL_OSPipMonNO_PrgLstCab: TWideStringField;
    QrUL_OSPipMonNO_PIP: TWideStringField;
    QrUL_OSPipMonCodigo: TIntegerField;
    QrUL_OSPipMonControle: TIntegerField;
    QrUL_OSPipMonPipCad: TIntegerField;
    QrUL_OSPipMonOrdem: TIntegerField;
    QrUL_OSPipMonReordem: TIntegerField;
    QrUL_OSPipMonLk: TIntegerField;
    QrUL_OSPipMonDataCad: TDateField;
    QrUL_OSPipMonDataAlt: TDateField;
    QrUL_OSPipMonUserCad: TIntegerField;
    QrUL_OSPipMonUserAlt: TIntegerField;
    QrUL_OSPipMonAlterWeb: TSmallintField;
    QrUL_OSPipMonAtivo: TSmallintField;
    QrUL_OSPipMonPrgLstCab: TIntegerField;
    QrUL_OSPipMonOSFMCbMae: TIntegerField;
    QrUL_OSPipMonInsercoes: TIntegerField;
    QrEqMo: TmySQLQuery;
    QrEqMoNaoUsaPrdt: TSmallintField;
    QrImpQrcPmv: TmySQLQuery;
    QrImpQrcPmvCodigo: TIntegerField;
    QrImpQrcPmvControle: TIntegerField;
    QrImpQrcPmvConta: TIntegerField;
    QrImpQrcPmvPipCad: TIntegerField;
    QrImpQrcPmvNO_PIP: TWideStringField;
    QrImpQrcPmvNO_EquipAplic: TWideStringField;
    frxDsImpQrcPmv: TfrxDBDataset;
    frxGER_OSERV_001_ModOS_02: TfrxReport;
    QrUL_OSCabLCPUsed: TIntegerField;
    QrUL_OSCabObsGaranti: TWideMemoField;
    frxDsErrRatif: TfrxDBDataset;
    QrLoc: TmySQLQuery;
    QrLocCodigo: TIntegerField;
    QrLocGrupo: TIntegerField;
    QrLocOpcao: TIntegerField;
    QrLocSiapTerCad: TIntegerField;
    QrPIPCadLk: TIntegerField;
    QrPIPCadDataCad: TDateField;
    QrPIPCadDataAlt: TDateField;
    QrPIPCadUserCad: TIntegerField;
    QrPIPCadUserAlt: TIntegerField;
    QrPIPCadAlterWeb: TSmallintField;
    QrPIPCadAtivo: TSmallintField;
    QrPIPCadOrdem: TIntegerField;
    QrPIPCadReordem: TIntegerField;
    QrPIPCadDtaInutili: TDateField;
    QrPIPCadMotInutili: TIntegerField;
    QrBxaFrm: TmySQLQuery;
    QrBxaFrmCodigo: TIntegerField;
    QrBxaFrmControle: TIntegerField;
    QrBxaFrmGraGruX: TIntegerField;
    QrUsoRatifCodigo: TIntegerField;
    QrUsoRatifControle: TIntegerField;
    QrUsoRatifConta: TIntegerField;
    QrBxaFrmEntidade: TIntegerField;
    QrBxaFrmCodEnti: TIntegerField;
    QrBxaFrmStqCenCad: TIntegerField;
    QrBxaFrmUsoQtd: TFloatField;
    QrBxaFrmUsoCusTot: TFloatField;
    QrBxaFrmConta: TFloatField;
    QrBxaFrmIDIts: TFloatField;
    QrBxaFrmSMI_IDCtrl: TFloatField;
    QrOSDesat: TmySQLQuery;
    QrOSDesatDtaDesativ: TDateField;
    QrOSDesatNome: TWideStringField;
    QrOSDesatControle: TIntegerField;
    QrDadosCab: TmySQLQuery;
    QrDadosCabEstatus: TIntegerField;
    QrDadosCabNO_STATUS: TWideStringField;
    QrDadosSrv: TmySQLQuery;
    QrDadosFrmCab: TmySQLQuery;
    QrDadosFrmDep: TmySQLQuery;
    QrDadosSrvNO_SERVICO: TWideStringField;
    QrDadosSrvControle: TIntegerField;
    QrDadosFrmCabNO_FORMULA: TWideStringField;
    QrDadosFrmCabConta: TIntegerField;
    QrDadosFrmDepNOME: TWideStringField;
    QrNaoExeRat: TmySQLQuery;
    QrNaoExeRatLocalizador: TIntegerField;
    QrNaoExeRatIDServico: TIntegerField;
    QrNaoExeRatNO_DesServico: TWideStringField;
    QrNaoExeRatNO_SIGLA: TWideStringField;
    QrNaoExeRatTabela: TLargeintField;
    QrNaoExeRatTipoFormula: TWideStringField;
    QrNaoExeRatIDFormula: TIntegerField;
    QrNaoExeRatNO_FORMULA: TWideStringField;
    QrNaoExeRatREALIZADO: TLargeintField;
    QrNaoExeRatSIGLAUNIDMED: TWideStringField;
    QrNaoExeRatNO_GG1: TWideStringField;
    QrNaoExeRatNO_RatifUso: TWideStringField;
    frxDsNaoExeRat: TfrxDBDataset;
    frxGER_OSERV_001_ModOS_03: TfrxReport;
    QrOSCab: TmySQLQuery;
    QrOSCabCodigo: TIntegerField;
    QrOSCabEmpresa: TIntegerField;
    QrOSCabEntidade: TIntegerField;
    QrOSCabSiapTerCad: TIntegerField;
    QrOSCabEstatus: TIntegerField;
    QrOSCabFatoGeradr: TIntegerField;
    QrOSCabDtaContat: TDateTimeField;
    QrOSCabDtaVisPrv: TDateTimeField;
    QrOSCabDtaVisExe: TDateTimeField;
    QrOSCabDtaExePrv: TDateTimeField;
    QrOSCabDtaExeIni: TDateTimeField;
    QrOSCabDtaExeFim: TDateTimeField;
    QrOSCabValorTotal: TFloatField;
    QrOSCabLk: TIntegerField;
    QrOSCabDataCad: TDateField;
    QrOSCabDataAlt: TDateField;
    QrOSCabUserCad: TIntegerField;
    QrOSCabUserAlt: TIntegerField;
    QrOSCabAlterWeb: TSmallintField;
    QrOSCabAtivo: TSmallintField;
    QrOSCabDdsPosVda: TIntegerField;
    QrOSCabEntiContat: TIntegerField;
    QrOSCabNumContrat: TIntegerField;
    QrOSCabEntPagante: TIntegerField;
    QrOSCabEntContrat: TIntegerField;
    QrOSCabOperacao: TIntegerField;
    QrOSCabValorPre: TFloatField;
    QrOSCabFimVisPrv: TDateTimeField;
    QrOSCabFimExePrv: TDateTimeField;
    QrOSCabGrupo: TIntegerField;
    QrOSCabNumero: TIntegerField;
    QrOSCabOpcao: TIntegerField;
    QrOSCabAgeEqiCab: TIntegerField;
    QrOSCabDtaLibFat: TDateTimeField;
    QrOSCabDtaFimFat: TDateTimeField;
    QrOSCabValorServi: TFloatField;
    QrOSCabValorDesco: TFloatField;
    QrOSCabValorOutrs: TFloatField;
    QrOSCabCondicaoPg: TIntegerField;
    QrOSCabCartEmis: TIntegerField;
    QrOSCabSerNF: TWideStringField;
    QrOSCabNumNF: TIntegerField;
    QrOSCabInvalServi: TFloatField;
    QrOSCabInvalDesco: TFloatField;
    QrOSCabInvalOutrs: TFloatField;
    QrOSCabInvalTotal: TFloatField;
    QrOSCabOrcamServi: TFloatField;
    QrOSCabOrcamDesco: TFloatField;
    QrOSCabOrcamOutrs: TFloatField;
    QrOSCabOrcamTotal: TFloatField;
    QrOSCabValiDdOrca: TIntegerField;
    QrOSCabExeTxtCli1: TIntegerField;
    QrOSCabObsGaranti: TWideMemoField;
    QrOSCabObsExecuta: TWideMemoField;
    QrOSCabExeTxtCli2: TIntegerField;
    QrOSCabFimVisExe: TDateTimeField;
    QrOSCabOptado: TSmallintField;
    QrOSCabHowGerou: TSmallintField;
    QrOSCabPosGerou: TSmallintField;
    QrOSCabOSFlhUltGe: TIntegerField;
    QrOSCabMulServico: TLargeintField;
    QrOSCabOSFlhGrCab: TIntegerField;
    QrOSCabOSFlhGrIts: TIntegerField;
    QrOSCabSohInicial: TIntegerField;
    QrOSCabStPipAdPrg: TSmallintField;
    QrOSCabLstUplWeb: TDateTimeField;
    QrFormulEPI: TmySQLQuery;
    DsFormulEPI: TDataSource;
    QrFormulEPICodigo: TIntegerField;
    QrFormulEPIControle: TIntegerField;
    QrFormulEPIGraGruX: TIntegerField;
    procedure QrOSSrvAfterScroll(DataSet: TDataSet);
    procedure QrOSSrvBeforeClose(DataSet: TDataSet);
    procedure QrEntidadesBeforeClose(DataSet: TDataSet);
    procedure QrOSFrmCabBeforeClose(DataSet: TDataSet);
    procedure QrOSFrmCabAfterScroll(DataSet: TDataSet);
    procedure QrOSMonCabAfterScroll(DataSet: TDataSet);
    procedure QrOSMonCabBeforeClose(DataSet: TDataSet);
    procedure QrOSSrvCalcFields(DataSet: TDataSet);
    procedure QrLctFatRefCalcFields(DataSet: TDataSet);
    procedure QrEntiTelCalcFields(DataSet: TDataSet);
    procedure QrOSCxaBeforeClose(DataSet: TDataSet);
    procedure QrOSCxaAfterScroll(DataSet: TDataSet);
    procedure frxGER_OSERV_001_ModOS_02GetValue(const VarName: string;
      var Value: Variant);
    procedure frxGER_OSERV_001_ModOS_03ClickObject(Sender: TfrxView;
      Button: TMouseButton; Shift: TShiftState; var Modified: Boolean);
    procedure frxGER_OSERV_001_ModOS_01ClickObject(Sender: TfrxView;
      Button: TMouseButton; Shift: TShiftState; var Modified: Boolean);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FLstCusPrd: Integer;
  public
    { Public declarations }
    //FQrOsCab: TmysqlQuery; // >> QrOSPrz
    function  EntidadeNoAvulso(Entidade, QuestaoCod: Integer): Integer;
    function  IDAgenteNaOS(Entidade, QuestaoCod: Integer): Integer;
    function  ImprimeQRCodePMVs(const Query: TmySQLQuery; DBGrid:
              TDBGrid(*; var Quais: TSelType*)): Boolean;
    function  NomeDiluente_GG1(Diluente, GraGru1: Integer): String;
    function  NomeDiluente_GGX(Diluente, GraGruX: Integer; AvisaErro: Boolean): String;
    function  ProximaOrdemCunsImgCab(Codigo: Integer): Integer;
    procedure ReopenSTC_Unico(Codigo: Integer);
    function  VerificaFormulasFilhas(MostraForm, Pergunta: Boolean;
              AbrirEmAba: Boolean = False; PageControl: TPageControl = nil;
              AdvToolBarPager: TAdvToolBarPager = nil): Integer;
    procedure VerificaMulServico(GBAvisos1: TGroupBox;
              LaAviso1, LaAviso2: TLabel; PB1: TProgressBar);
    function  SQL_Filtr(): String;
    function  DataSincOS_1Acao(const Empresa, Entidade, SiapTerCad: Integer;
              var Data: TDateTime): Boolean;
    procedure GeraPerguntasPIPs(OSCab, SohInicial, SiapTerCad: Integer;
              Data: TDateTime; PB: TProgressBar; LaAviso1, LaAviso2: TLabel);
    procedure RatificaConsumo(TabMae, Tabela, Campo: String; Indice, GraCusPrc,
              Empresa, FatID: Integer);
    procedure MD5_VerificaMD5DeGrupoDeAgentes();
    function  MD5_AtualizaCheckSumOSAge(OSCab: Integer): Integer;
    procedure MD5_AtualizaCheckSumAgeEqiCab(Codigo: Integer);
    function  MD5_ObtemCheckSumAgentes(Tabela, FldID, FldAge: String;
              Codigo: Integer; Database: TmySQLDatabase): String;
    function  MD5_ObtemAgeEqiCab(Tabela, FldID, FldAge: String;
              Codigo: Integer; Database: TmySQLDatabase): Integer;
    function  MostraOSPos(SQLType: TSQLType; Codigo, Controle, Dias,
              Aplicacao, AplicID: Integer): Boolean;
    procedure ReopenGgxPIP(Qry: TmySQLQuery; SiapTerCad: Integer; Ativo: Boolean;
              TXT_Ordem: String);
    procedure ReativarPIPemOS(SiapTerCad: Integer);
    procedure SelecionaPipAtivoDeOS(SiapTerCad: Integer);
    function  CondicaoDePagamentoSelecionada(OSCab: Integer): Integer;
    function  UpLoadWeb_OS(Codigo: Integer; Abertos: FUploadAbertos;
              LaAviso1, LaAviso2: TLabel): Boolean;
    procedure BaixaInsumoDeOS(FatIDEspecifico, IDIts: Integer;
              MinhaDataHora: TDateTime);
    function  GarantiasAVencer(Hoje: TDateTime; MostraForm: Boolean): Integer;
    function  DadosOSBgstrl(Codigo: Integer): String;
  end;

var
  DmModOS: TDmModOS;

const
  CO_IMP_COLS_AGE = 1;
  CO_IMP_COLS_CAB_ALV = 6;
  CO_IMP_COLS_SRV = 1;
  CO_IMP_COLS_ALV = 4;
  CO_IMP_COLS_FRM_CAB = 1;
  CO_IMP_COLS_FRM_REC = 1;//2;
  CO_IMP_COLS_FRM_ABR = 1;//4;
  CO_IMP_COLS_FRM_DEP = 1;
  CO_IMP_COLS_MON_CAB = 1;
  CO_IMP_COLS_MON_REC = 2;
  CO_IMP_COLS_MON_DEP = 1;
  CO_IMP_COLS_PRZ = 2;
  CO_IMP_COLS_CXA = 1;
  //
  CO_IMP_ENTI_TEL = 1;
  CO_IMP_ENTI_EMA = 1;
  //
  CO_IMP_COLS_SMOVCAD = 1;
  CO_IMP_COLS_SMOVDEF = 1;
  CO_IMP_COLS_SIMACAD = 1;
  CO_IMP_COLS_SIMADEP = 2;
  CO_IMP_COLS_SIMACDI = 3;
  CO_IMP_COLS_SIMACAV = 3;
  CO_IMP_COLS_SIMARES = 1;
  CO_IMP_COLS_SIMACUI = 2;
  CO_IMP_COLS_SIMAATI = 2;
  CO_IMP_COLS_SIMACXA = 1;
  CO_IMP_COLS_SICxDef = 1;
  CO_IMP_COLS_SICdDef = 1;

implementation

uses Module, UMySQLModule, CfgCadLista, OSCabAlv, MyDBCheck, UnMyObjects,
  OSPos, UnOSApp_PF, OSFlhGer, ModuleGeral, ReativaPIPemOS, PipRapido,
  ModuleNFe_0000, OSGarantRnw, UnDmkWeb_Jan, MyListas, Principal, UnGrade_Jan;

{$R *.dfm}

procedure TDmModOS.BaixaInsumoDeOS(FatIDEspecifico, IDIts: Integer;
MinhaDataHora: TDateTime);
var
  Tabela: String;
  //
  procedure InsereItemStqMov(SMI_IDCtrl: Integer; DataHora: String; Tipo,
    OriCodi, OriCtrl, OriCnta, OriPart, Empresa, StqCenCad, GraGruX, QuemUsou,
    Baixa: Integer; Qtde, Pecas, Peso, AreaM2, AreaP2, CustoAll: Double);
  var
    IDCtrl: Integer;
    SQLType: TSQLType;
  begin
    if SMI_IDCtrl = 0 then
      SQLType := stIns
    else
      SQLType := stUpd;
    //
    IDCtrl := UMyMod.BuscaEmLivreY_Def('stqmovitsa', 'IDCtrl', SQLType, SMI_IDCtrl);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'stqmovitsa', False, [
      'DataHora', 'Tipo', 'OriCodi',
      'OriCtrl', 'OriCnta', 'OriPart',
      'Empresa', 'StqCenCad', 'GraGruX',
      'Qtde', 'Pecas', 'Peso',
      'AreaM2', 'AreaP2', (*'FatorClas',*)
      'QuemUsou', (*'Retorno', 'ParTipo',
      'ParCodi', 'DebCtrl', 'SMIMultIns',*)
      'CustoAll', (*'ValorAll', 'GrupoBal',*)
      'Baixa'(*, 'AntQtde', 'UnidMed',
      'ValiStq'*)], [
      'IDCtrl'], [
      DataHora, Tipo, OriCodi,
      OriCtrl, OriCnta, OriPart,
      Empresa, StqCenCad, GraGruX,
      Qtde, Pecas, Peso,
      AreaM2, AreaP2, (*FatorClas,*)
      QuemUsou, (*Retorno, ParTipo,
      ParCodi, DebCtrl, SMIMultIns,*)
      CustoAll,(* ValorAll, GrupoBal,*)
      Baixa(*, AntQtde, UnidMed,
      ValiStq*)], [
      IDCtrl], False) then
    begin
      if SQLType = stIns then
      begin
        UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
          'UPDATE ' + Tabela,
          'SET SMI_IDCtrl=' + Geral.FF0(IDCtrl),
          'WHERE IDIts=' + Geral.FF0(IDIts),
          '']);
      end;
    end;
  end;
var
  GraGruX, StqCenCad, SMI_IDCtrl,
  //
  //Cliente,
  Tipo, OriCodi, OriCtrl, OriCnta, OriPart, Empresa, QuemUsou, Baixa: Integer;
  Qtde, Pecas, Peso, AreaM2, AreaP2, CustoAll: Double;
  DataHora: String;
  //
  SQL_RatifUso: String;
begin
  //Criado campo nas tabelas > stqmovitsa.IDCtrl >>> tabxxx.SMI_IDCtrl

  (*osfrmrec  *) (*VAR_FATID_4101 = 4101; // Bugstrol -> Baixa na OS (tabela osfrmrec)*)
  (*osmomrec  *) (*VAR_FATID_4102 = 4102; // Bugstrol -> Baixa na OS (tabela osmomrec)*)
  (*ospipitspr*) (*VAR_FATID_4103 = 4103; // Bugstrol -> Baixa na OS por adicao (tabela ospipitspr)*)
  (*ospipitspr*) (*VAR_FATID_4104 = 4104; // Bugstrol -> Baixa na OS por substituicao (tabela ospipitspr)*)

  // osfrmrec  -  VAR_FATID_4101
  //if (FatIDEspecifico = VAR_FATID_4101) or (FatIDEspecifico = VAR_FATID_4102) then
  begin
    case FatIDEspecifico of
      VAR_FATID_4101: Tabela := 'osfrmrec';
      VAR_FATID_4102: Tabela := 'osmonrec';
      VAR_FATID_4103: Tabela := 'ospipitspr';
      VAR_FATID_4104: Tabela := 'ospipitspr';
      else
      begin
        Tabela := ' ???? ';
        Geral.MB_Erro('Tabela n�o definida em "DmModOS.BaixaInsumoDeOS()"');
      end;
    end;
    if (FatIDEspecifico = VAR_FATID_4103)
    or (FatIDEspecifico = VAR_FATID_4104) then
      SQL_RatifUso := ''
    else
      SQL_RatifUso := 'AND ofr.RatifUso=1 ';
    //
    UnDMkDAC_PF.AbreMySQLQuery0(QrBxaFrm, Dmod.MyDB, [
      'SELECT ofr.Codigo, ofr.Controle, ',
      'ofr.Conta + 0.000 Conta, ofr.IDIts + 0.000 IDIts, ',
      'ofr.GraGruX, ofr.UsoQtd, ofr.UsoCusTot, ofr.SMI_IDCtrl + 0.000 SMI_IDCtrl, ',
      'cab.Entidade, eci.CodEnti, moc.StqCenCad ',
      'FROM ' + Tabela + ' ofr ',
      'LEFT JOIN oscab cab ON cab.Codigo=ofr.Codigo ',
      'LEFT JOIN mobilicad moc ON moc.Codigo=cab.MobiliCad ',
      'LEFT JOIN enticliint eci ON eci.CodCliInt=cab.Empresa ',
      'WHERE ofr.IDIts=' + Geral.FF0(IDIts),
      SQL_RatifUso,
{
      'AND ofr.Codigo=' + Geral.FF0(Codigo),
      'AND ofr.Controle=' + Geral.FF0(Controle),
      'AND ofr.Conta=' + Geral.FF0(Conta),
}
      '']);
    //
    QrBxaFrm.First;
    while not QrBxaFrm.Eof do
    begin
      Tipo           := FatIDEspecifico;
      OriCodi        := QrBxaFrmCodigo.Value;
      OriCtrl        := QrBxaFrmControle.Value;
      OriCnta        := Trunc(QrBxaFrmConta.Value);
      OriPart        := Trunc(QrBxaFrmIDIts.Value);
      ///
      Empresa        := Trunc(QrBxaFrmCodEnti.Value);
      //Cliente        := Trunc(QrBxaFrmEntidade.Value);
      ///
      StqCenCad      := QrBxaFrmStqCenCad.Value;
      GraGruX        := QrBxaFrmGraGruX.Value;
      Qtde           := QrBxaFrmUsoQtd.Value;
      Pecas          := 0;
      Peso           := 0;
      AreaM2         := 0;
      AreaP2         := 0;
      //FatorClas      := 0;
      QuemUsou       := Empresa;
      //Retorno        := ;
      //ParTipo        := ;  // Usados para relacionar com outra tabela, mas o
      //ParCodi        := ;  // relacionamento neste caso j� existe acima.
      //DebCtrl        := ;  ???
      //SMIMultIns     := ;
      CustoAll       := QrBxaFrmUsoCusTot.Value;
      //ValorAll       := ;  Informar este valor como pre�o de venda se o eslauco desejar
      //GrupoBal       := ;
      Baixa          := -1;
      //AntQtde        := ;
      //UnidMed        := ;
      //ValiStq        := ;
        ///
      //DataHora       := Geral.FDT(DModG.ObtemAgora(), 109);
      DataHora       := Geral.FDT(MinhaDataHora, 109);
      //IDCtrl         := ;

      SMI_IDCtrl := Trunc(QrBxaFrmSMI_IDCtrl.Value);
      //
      InsereItemStqMov(SMI_IDCtrl, DataHora, Tipo, OriCodi, OriCtrl,
        OriCnta, OriPart, Empresa, StqCenCad, GraGruX, QuemUsou, Baixa,
        Qtde, Pecas, Peso, AreaM2, AreaP2, CustoAll);
      //
      QrBxaFrm.Next;
    end;
  end;
end;

function TDmModOS.CondicaoDePagamentoSelecionada(OSCab: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPrzSel, Dmod.MyDB, [
  'SELECT Controle, Condicao ',
  'FROM osprz ',
  'WHERE Codigo=' + Geral.FF0(OSCab),
  'AND Escolhido=1',
  ' ']);
  //
  Result := QrPrzSelCondicao.Value;
end;

function TDmModOS.DadosOSBgstrl(Codigo: Integer): String;
begin
  Result := 'O.S. ' + Geral.FFT(Codigo, 0, siNegativo) + '. Status ';
  UnDmkDAC_PF.AbreMySQLQuery0(QrDadosCab, Dmod.MyDB, [
  'SELECT cab.Estatus, sta.Nome NO_STATUS ',
  'FROM oscab cab ',
  'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus ',
  'WHERE cab.Codigo=' + Geral.FF0(Codigo),
  '']);
  Result := Result + Geral.FF0(QrDadosCabEstatus.Value) + ': ' +
    QrDadosCabNO_STATUS.Value + '. ';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDadosSrv, Dmod.MyDB, [
  'SELECT srv.Controle, des.Nome NO_SERVICO ',
  'FROM ossrv srv ',
  'LEFT JOIN desservico des ON des.Codigo=srv.Desservico ',
  'WHERE srv.Codigo=' + Geral.FF0(Codigo),
  '']);
  QrDadosSrv.First;
  while not QrDadosSrv.Eof do
  begin
    Result := Result +
      sLineBreak + '  Servi�o: ' + QrDadosSrvNO_SERVICO.Value + '.';
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrDadosFrmCab, Dmod.MyDB, [
    'SELECT ofc.Conta, frm.Nome NO_FORMULA ',
    'FROM osfrmcab ofc ',
    'LEFT JOIN formulas frm ON frm.Codigo=ofc.Formula ',
    'WHERE ofc.Controle=' + Geral.FF0(QrDadosSrvControle.Value),
    '']);
    QrDadosFrmCab.First;
    while not QrDadosFrmCab.Eof do
    begin
      Result := Result + sLineBreak + '    F�rmula: '  +
        QrDadosFrmCabNO_FORMULA.Value + '.' + sLineBreak + '      Locais: ';
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrDadosFrmDep, Dmod.MyDB, [
      'SELECT DISTINCT CONCAT(sic.SCompl2, "->", det.Nome) NOME ',
      'FROM osfrmdep osd',
      'LEFT JOIN siapimadep sid ON sid.Controle=osd.Cadastro',
      'LEFT JOIN siapimacad sic ON sic.Codigo=sid.Codigo  ',
      'LEFT JOIN siaptercad sit ON sit.Codigo=sic.SiapImaTer  ',
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci  ',
      'LEFT JOIN dependtype det ON det.Codigo=dep.DependType  ',
      'WHERE osd.Conta=' + Geral.FF0(QrDadosFrmCabConta.Value),
      ' ',
      'UNION ',
      ' ',
      'SELECT DISTINCT CONCAT(ELT(mac.Tipo, "Movel", "Automovel",  ',
      '"Animal", "Outro", "???"), "->", mac.Nome) NOME ',
      'FROM osfrmdep osd',
      'LEFT JOIN movamovcad mac ON mac.Codigo=osd.Cadastro',
      'WHERE osd.Conta=' + Geral.FF0(QrDadosFrmCabConta.Value),
      ' ',
      'ORDER BY NOME ', // colocar os nomes brancos por primeiro
      '']);
      //
      //Geral.MB_Info(QrDadosFrmDep.SQL.Text);
      QrDadosFrmDep.First;
      while not QrDadosFrmDep.Eof do
      begin
        if Trim(QrDadosFrmDepNOME.Value) <> '' then
        begin
          Result := Result + QrDadosFrmDepNOME.Value;
          if QrDadosFrmDep.RecNo < QrDadosFrmDep.RecordCount then
            Result := Result + ', '
          else
            Result := Result + '.';
        end;
        QrDadosFrmDep.Next;
      end;
      QrDadosFrmCab.Next;
    end;
    //
    QrDadosSrv.Next;
  end;
  Result := Result + sLineBreak;
end;

procedure TDmModOS.DataModuleCreate(Sender: TObject);
begin
  FLstCusPrd := 0;
end;

function TDmModOS.DataSincOS_1Acao(const Empresa, Entidade, SiapTerCad: Integer;
  var Data: TDateTime): Boolean;
var
  Qry: TmySQLQuery;
begin
  Result := False;
  //
  if Geral.MB_Pergunta('Deseja relamente definir a data de refer�ncia pela ' +
  sLineBreak + 'data de encerramento da primeira OS de primeira a��o ' +
  sLineBreak + 'deste cliente / lugar?') = ID_YES then
  begin
    Qry := TmySQLQuery.Create(Dmod);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT DtaExeFim ',
      'FROM oscab ',
      'WHERE FatoGeradr=' + Geral.FF0(CO_COD_FatoGeradr_1aAcao), //20
      'AND Empresa=' + Geral.FF0(Empresa),
      'AND Entidade=' + Geral.FF0(Entidade),
      'AND SiapTerCad=' + Geral.FF0(SiapTerCad),
      'AND DtaExeFim > "1900-01-01" ',
      'ORDER BY DtaExeFim ',
      '']);
      //
      Data := Qry.FieldByName('DtaExeFim').AsDateTime;
      //
      Result := Data > 2;
    finally
      Qry.Free;
    end;
  end;
end;

function TDmModOS.EntidadeNoAvulso(Entidade, QuestaoCod: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgeEmOS, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM agendaent ',
  'WHERE Codigo=' + Geral.FF0(QuestaoCod),
  'AND Entidade=' + Geral.FF0(Entidade),
  '']);
  //
  Result := QrAgeEmOSControle.Value;
end;

procedure TDmModOS.frxGER_OSERV_001_ModOS_01ClickObject(Sender: TfrxView;
  Button: TMouseButton; Shift: TShiftState; var Modified: Boolean);
var
  Nivel1: Integer;
begin
  Nivel1 := 0;
  //
  if (Sender.Name = 'Memo7') or (Sender.Name = 'Memo11') then
    Nivel1 := Geral.IMV(Sender.TagStr);
  //
  if Nivel1 <> 0 then
  begin
    if frxGER_OSERV_001_ModOS_01.Preview <> nil then
      TForm(frxGER_OSERV_001_ModOS_01.Preview.Parent).Close;
    //
    Grade_Jan.MostraFormGraGruN(Nivel1, FLstCusPrd);
  end;
end;

procedure TDmModOS.frxGER_OSERV_001_ModOS_02GetValue(const VarName: string;
  var Value: Variant);
var
  frxBmp: TfrxPictureView;
  Bmp: Graphics.TBitmap;
  PipCad: String;
begin
  if VarName = 'VARF_OBTEM_BMP' then
  begin
    PipCad := Geral.FF0(QrImpQrcPmvPipCad.Value);
    Bmp :=
      MyQRCode.GeraBmpQRCode(PipCad, (*ImageWidth*)400, (*ImageHeight*)400,
      (*Encoding*) QRCode.AUTO,
      (*Versao*)1, (*FNC1Mode*)QRCode.NO, (*ECLevel*)QRCode.LEVEL_L,
      (*moduleWidth*)8, (*marginPixels*)10, (*applicationIndicator*)-1,
      (*StructuredAppend*)False, (*StructuredAppendCounter*)0,
      (*StructuredAppendIndex*)0, (*AutoConfigurate*) True);
    frxBmp := frxGER_OSERV_001_ModOS_02.FindObject('Picture1') as TfrxPictureView;
    frxBmp.Picture.Bitmap.Assign(Bmp);
    //
    Value := True;
  end;
end;

procedure TDmModOS.frxGER_OSERV_001_ModOS_03ClickObject(Sender: TfrxView;
  Button: TMouseButton; Shift: TShiftState; var Modified: Boolean);
var
  IDServico: Integer;
begin
  IDServico := 0;
  //
  if (Sender.Name = 'Memo5') or (Sender.Name = 'Memo6') then
    IDServico := Geral.IMV(Sender.TagStr);
  //
  if IDServico <> 0 then
  begin
    if frxGER_OSERV_001_ModOS_03.Preview <> nil then
      TForm(frxGER_OSERV_001_ModOS_03.Preview.Parent).Close;
    //
    FmPrincipal.MostraOSCabByOSSrv(IDServico);
  end;
end;

function TDmModOS.GarantiasAVencer(Hoje: TDateTime;
  MostraForm: Boolean): Integer;
var
  DataI, DataF: String;
  Qry: TmySQLQuery;
begin
  Result := 0;
  DataI := Geral.FDT(Hoje, 1);
  DataF := Geral.FDT(Hoje + 30, 1);
  //
  Qry := TmySQLQuery.Create(Dmod);
  try
    if MostraForm = False then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT COUNT(*) ITENS',
      'FROM ossrv its',
      'LEFT JOIN oscab cab ON cab.Codigo=its.Codigo',
      'WHERE DATE_ADD(cab.DtaExeini, INTERVAL its.GarantiaDd DAY)',
      '  BETWEEN "' + DataI + '" AND "' + DataF + '" ',
      '',
      'UNION',
      '',
      'SELECT COUNT(*) ITENS',
      'FROM oscxa its',
      'LEFT JOIN oscab cab ON cab.Codigo=its.Codigo',
      'WHERE DATE_ADD(cab.DtaExeini, INTERVAL its.GarantiaDd DAY)',
      '  BETWEEN "' + DataI + '" AND "' + DataF + '" ',
      '']);
      Result := Result + Qry.FieldByName('ITENS').AsInteger;
      Qry.Next;
      Result := Result + Qry.FieldByName('ITENS').AsInteger;
    end else
      Result := 1;
    //
    if MostraForm and (Result > 0) then
    begin
      if DBCheck.CriaFm(TFmOSGarantRnw, FmOSGarantRnw, afmoNegarComAviso) then
      begin
        FmOSGarantRnw.FDataI := DataI;
        FmOSGarantRnw.FDataF := DataF;
        FmOSGarantRnw.ReopenGarantias();
        FmOSGarantRnw.ShowModal;
        FmOSGarantRnw.Destroy;
      end;
    end;
    //
{
SELECT cab.DtaExeIni, srv.GarantiaDd,
DATE_ADD(cab.DtaExeIni, INTERVAL srv.GarantiaDd DAY) DtGarantia
FROM ossrv srv
LEFT JOIN oscab cab ON cab.Codigo=srv.Codigo
WHERE DATE_ADD(cab.DtaExeini, INTERVAL srv.GarantiaDd DAY)
  BETWEEN "2014-06-29" AND "2014-07-29"
ORDER BY DtGarantia
}
{
SELECT cab.DtaExeIni, cxa.GarantiaDd,
DATE_ADD(cab.DtaExeIni, INTERVAL cxa.GarantiaDd DAY) DtGarantia
FROM oscxa cxa
LEFT JOIN oscab cab ON cab.Codigo=cxa.Codigo
WHERE DATE_ADD(cab.DtaExeini, INTERVAL cxa.GarantiaDd DAY)
  BETWEEN "2014-06-29" AND "2014-08-29"
}
  finally
    Qry.Free;
  end;
end;

procedure TDmModOS.GeraPerguntasPIPs(OSCab, SohInicial, SiapTerCad: Integer;
Data: TDateTime; PB: TProgressBar; LaAviso1, LaAviso2: TLabel);
  function InserePerguntaPipIts(SuperOrd, SobreOrd, SubOrdem, Tabela, GraGruX: Integer): Boolean;
  const
    SuperSub = 0;
  var
    Codigo, Controle, PrgLstCab, PrgLstIts, Ordem, Funcoes, Dependente,
    PrgAtrCad, Filiacao, Relacao, Nivel, NivSeq, Pergunta, BinarCad0, BinarCad1,
    AcaoPrd, LupForma, LupQtdVzs: Integer;
    Conta, TabIdx: Int64;
  begin
    //
    Codigo         := QrOsPMCodigo.Value;
    Controle       := QrOsPMControle.Value;;
    Conta          := 0;
    PrgLstCab      := QrPrgLstItsCodigo.Value;
    PrgLstIts      := QrPrgLstItsControle.Value;
    Ordem          := QrPrgLstItsOrdem.Value;
    //SubOrdem       := ;
    //Tabela         := ;
    //GraGruX        := ;
    Funcoes        := QrPrgLstItsFuncoes.Value;
    Dependente     := QrPrgLstItsDependente.Value;
    PrgAtrCad      := QrPrgLstItsPrgAtrCad.Value;
    Filiacao       := QrPrgLstItsFiliacao.Value;
    Relacao        := QrPrgLstItsRelacao.Value;
    Nivel          := QrPrgLstItsNivel.Value;
    NivSeq         := QrPrgLstItsNivSeq.Value;
    Pergunta       := QrPrgLstItsPergunta.Value;
    BinarCad0      := QrPrgLstItsBinarCad0.Value;
    BinarCad1      := QrPrgLstItsBinarCad1.Value;
    AcaoPrd        := QrPrgLstItsAcaoPrd.Value;
    TabIdx         := QrGgxPipIDIts.Value;
    LupForma       := QrPrgLstItsLupForma.Value;
    LupQtdVzs      := QrPrgLstItsLupQtdVzs.Value;
    //
    Conta := UMyMod.BPGS1I64_Reaproveita('ospipits', 'Conta', '', '', tsPos, stIns, Conta);
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ospipits', False, [
    'Codigo', 'Controle', 'PrgLstCab',
    'PrgLstIts', 'SuperOrd', 'SobreOrd',
    'Ordem', 'SubOrdem', 'Tabela',
    'GraGruX', 'Funcoes', 'Dependente',
    'PrgAtrCad', 'Filiacao', 'Relacao',
    'Nivel', 'NivSeq', 'Pergunta',
    'BinarCad0', 'BinarCad1', 'AcaoPrd',
    'TabIdx', 'LupForma', 'LupQtdVzs',
    'SuperSub'], [
    'Conta'], [
    Codigo, Controle, PrgLstCab,
    PrgLstIts, SuperOrd, SobreOrd,
    Ordem, SubOrdem, Tabela,
    GraGruX, Funcoes, Dependente,
    PrgAtrCad, Filiacao, Relacao,
    Nivel, NivSeq, Pergunta,
    BinarCad0, BinarCad1, AcaoPrd,
    TabIdx, LupForma, LupQtdVzs,
    SuperSub], [
    Conta], True);
  end;
  //
var
  StPipAdPrg, Codigo, OSFMCbMae, PipCad, Ordem, PrgLstCab, SubOrdem, Tabela,
  GraGruX, SuperPai, SuperOrd, SobreOrd, LoopiOrd, UltimOrd: Integer;
  SuperLup: Boolean;
  AntUsoQtd: Double;
  AntUsoGGX, AntUsoVCD: Integer;
  DtTxt, CtrlTXT: String;
begin
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Verificando PIP p�steros');
  //
  Codigo := OSCab;
  StPipAdPrg := CO_StPipAdPrg_INIT; // 1
  if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
  'StPipAdPrg'], [
  'Codigo'], [
  StPipAdPrg], [
  Codigo], True) then
    Exit;
  // Adicionar PIPs instalados depois da OS inicial!
  // Desmarcado em 2014-04-22
{
  if SohInicial = 0 then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrLocPip, Dmod.MyDB, [
    'SELECT DISTINCT pip.*, ',
    // 2014-04-22
    'omc.PerioDd, omc.DdPostero ',
    // FIM 2014-04-22
    'FROM pipcad pip ',
    'LEFT JOIN osmoncab omc ON omc.PipCad=pip.Codigo ',
    'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo ',
    'WHERE cab.SiapTerCad=' + Geral.FF0(SiapTerCad),
    'AND pip.DtaDesativ < "1900-01-01" ',
    'AND ',
    '(',
    '  NOT pip.Codigo IN ',
    '  (',
    '    SELECT PipCad',
    '    FROM ospipmon',
    '    WHERE Codigo=' + Geral.FF0(OSCab),
    '  )',
    ')',
    '']);
    //
    QrLocPIP.First;
    while not QrLocPIP.Eof do
    begin
(*
      // 2014-04-22 Visto com Eslauco!!
      Dias := Trunc(QrNewF_MDtaOSCalc.Value) - Trunc(DEP);
      DdRota := Dias mod QrLocPipDdPostero.Value;
      if DdRota < Dmod.QrOpcoesBugsDdRotaFlh.Value then
      // FIM 2014-04-22
*)
      begin
        // Aqui pode dar problema se n�o estiver bem setado!
        // O problema vai acontecer na atualiza��o da OSMonCab.Conta!
        // > Seta em procedure TDmod.AtualizaPIP(Codigo: Integer);
        OSFMCbMae := QrLocPIPOSMonCab.Value;
        //
        //
        PipCad    := QrLocPipCodigo.Value;
        Ordem     := QrLocPipOrdem.Value;
        PrgLstCab := QrLocPipPrgLstCab.Value;
        //
        OSApp_PF.IncluiOSMonPipAtual(OSCab, SiapterCad, OSFMCbMae, PipCad,
          Ordem, PrgLstCab);
        //
      end;
      QrLocPIP.Next;
    end;
  end;
}
  //
  DtTxt := Geral.FDT(Data, 1);
  UnDmkDAC_PF.AbreMySQLQuery0(QrOSDesat, Dmod.MyDB, [
  'SELECT opm.Controle, pip.Nome, pip.DtaDesativ ',
  'FROM ospipmon opm ',
  'LEFT JOIN pipcad pip ON pip.Codigo=opm.PipCad ',
  'WHERE opm.Codigo=' + Geral.FF0(OSCab),
  'AND (pip.DtaDesativ>"1900-01-01" ',
  '         AND ',
  '         pip.DtaDesativ<="' + DtTxt + '" ',
  ' ) ',
  '']);
  //
  //Geral.MB_SQL(QrOSDesat.SQL.Text);
  QrOSDesat.First;
  while not QrOSDesat.Eof do
  begin
    if Geral.MB_Pergunta('O PMV "' + QrOSDesatNome.Value +
    '" foi desativado em ' + Geral.FDT(QrOSDesatDtaDesativ.Value, 3) + '.' +
    sLineBreak + 'Deseja remov�-lo da OS?') = ID_YES then
    begin
      CtrlTXT := Geral.FF0(QrOSDesatControle.Value);
      //
      UnDmkDAC_PF.ExecutaMySQLQuery0(Dmod.QrUpd, Dmod.MyDB, [
      'DELETE FROM ospipmon WHERE Controle=' + CtrlTXT + '; ',
      'DELETE FROM ospipits WHERE Controle=' + CtrlTXT + '; ',
      'DELETE FROM ospipitspr WHERE Controle=' + CtrlTXT + '; ',
      '']);
    end;
    //
    QrOSDesat.Next;
  end;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrOsPM, Dmod.MyDB, [
  'SELECT * ',
  'FROM ospipmon ',
  'WHERE Codigo=' + Geral.FF0(OSCab),
  '']);
  //
  PB.Position := 0;
  PB.Max := QrOsPM.RecordCount;
  QrOsPM.First;
  SuperLup := False;
  SuperPai := 0;
  SuperOrd := 1;
  SobreOrd := 0;
  while not QrOsPM.Eof do
  begin
    UltimOrd := CO_PrgLstItsRelacao_COD_N_I;
    LoopiOrd := 1;
    //
    MyObjects.Informa2EUpdPB(PB, LaAviso1, LaAviso2, True,
    'Adicionando perguntas ao PIP ID = ' + Geral.FF0(QrOsPMPipCad.Value));
    //
    // ver fontes de produtos (GraGruX) das tabelas osmoncab e ospipitspr
    // para perguntas relativas aos produtos
    ReopenGgxPIP(QrGgxPip, SiapTerCad, True,
      'ORDER BY Codigo, Controle, Conta, IDIts, Ordem');
    AntUsoQtd := QrGgxPipUsoQtd.Value;
    AntUsoGGX := QrGgxPipGraGruX.Value;
    AntUsoVCD := QrGgxPipValCliDd.Value;
{
    UnDmkDAC_PF.AbreMySQLQuery0(QrGgxPip, Dmod.MyDB, [
    'SELECT omr.Codigo, omr.Controle, omr.Conta, omr.IDIts,',
    'omr.Ordem, omr.GraGruX, omr.UsoQtd, omr.ValCliDd, ',
    'cab.DtaExeIni, ',
    Geral.FF0(CO_OSPipIts_OSMONREC) + ' Tabela', // 2
    'FROM osmonrec omr',
    'LEFT JOIN osmoncab omc ON omc.Conta=omr.Conta',
    'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo',
    'WHERE omc.PipCad=' + Geral.FF0(QrOsPMPipCad.Value),
    'AND omr.Desativado=0',
    'AND cab.SiapTerCad=' + Geral.FF0(SiapTerCad),
    '',
    'UNION',
    '',
    'SELECT omr.Codigo, omr.Controle, omr.Conta, omr.IDIts,',
    'omc.Ordem, omr.GraGruX, omr.UsoQtd, omr.ValCliDd, ',
    'cab.DtaExeIni, ',
    Geral.FF0(CO_OSPipIts_OSPIPITSPR) + ' Tabela', // 3
    'FROM ospipitspr omr',
    'LEFT JOIN ospipmon omc ON omc.Controle=omr.Controle',
    'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo',
    'WHERE omc.PipCad=' + Geral.FF0(QrOsPMPipCad.Value),
    'AND omr.Desativado=0',
    'AND cab.SiapTerCad=' + Geral.FF0(SiapTerCad),
    '',
    '',
    'ORDER BY Codigo, Controle,',
    'Conta, IDIts, Ordem',
    '']);
}
    //
    if QrGgxPip.RecordCount = 0 then
    begin
      ReativarPIPemOS(SiapTerCad);
      //
      ReopenGgxPIP(QrGgxPip, SiapTerCad, True,
        'ORDER BY Codigo, Controle, Conta, IDIts, Ordem');
    end;
    case QrGgxPip.RecordCount of
      0:
      begin
        AntUsoQtd := 0;
        AntUsoGGX := 0;
        AntUsoVCD := 0;
        UnDmkDAC_PF.AbreMySQLQuery0(QrEqMo, Dmod.MyDB, [
        'SELECT emo.NaoUsaPrdt ',
        'FROM pipcad cad ',
        'LEFT JOIN gragrux ggx ON ggx.Controle=cad.Equipamento ',
        'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1 ',
        'LEFT JOIN grag1eqmo emo ON emo.Nivel1=gg1.Nivel1 ',
        'WHERE cad.Codigo=' + Geral.FF0(QrOsPMPipCad.Value),
        '']);
        if QrEqMoNaoUsaPrdt.Value <> 1 then
        Geral.MB_Erro(
        'N�o foi poss�vel obter a �ltima OS v�lida para consumo de produto(s) do PMV: ' +
        'ID = ' + Geral.FF0(QrOsPMPipCad.Value) + sLineBreak +
        'Caso n�o queira mais ver esta mensagem, altere o cadastro do equipamento do PMV');
      end;
      1: ; // Nada
      else // Mais de um
      begin
        SelecionaPipAtivoDeOS(SiapTerCad);
        //
        ReopenGgxPIP(QrGgxPip, SiapTerCad, True,
          'ORDER BY Codigo, Controle, Conta, IDIts, Ordem');
        AntUsoQtd := QrGgxPipUsoQtd.Value;
        AntUsoGGX := QrGgxPipGraGruX.Value;
        AntUsoVCD := QrGgxPipValCliDd.Value;
      end;
    end;
    // Atualizar AntUsoQtd (QrGgxPipUsoQtd.Value) e AntUsoGGX e AntUsoVCD;
    UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ospipmon', False, [
    'AntUsoQtd', 'AntUsoGGX', 'AntUsoVCD'
    ], ['Controle'], [
    AntUsoQtd, AntUsoGGX, AntUsoVCD
    ], [QrOSPMControle.Value], True);
    //
    // Incluir perguntas
    PrgLstCab := QrOsPMPrgLstCab.Value;
    UnDmkDAC_PF.AbreMySQLQuery0(QrPrgLstIts, Dmod.MyDB, [
    'SELECT * ',
    'FROM prglstits ',
    'WHERE Codigo=' + Geral.FF0(PrgLstCab),
    'ORDER BY Ordem, Controle',
    '']);
    //
    QrPrgLstIts.First;
    while not QrPrgLstIts.Eof do
    begin
      case QrPrgLstItsRelacao.Value of
             // Pergunta relativa ao PIP
        (*1*)CO_PrgLstItsRelacao_COD_PIP   :
        begin
          SobreOrd  := SobreOrd + 1;
          //
          SubOrdem  := 0;
          Tabela    := CO_OSPipIts_SEM_TAB;
          GraGruX   := 0;
          InserePerguntaPipIts(SuperOrd, SobreOrd, SubOrdem, Tabela, GraGruX);
          LoopiOrd := 0;
        end;
             // pergunta relativa as iscas do PIP
        (*2*)CO_PrgLstItsRelacao_COD_Iscas :
        begin
          SubOrdem  := 0;
          SobreOrd  := SobreOrd - LoopiOrd;
          //Tabela    := CO_OSPipIts_OSPIPITSPR;
          Tabela    := QrGGXPipTabela.Value;
          //GraGruX  := 0;
          //
          QrGgxPip.First;
          while not QrGgxPip.Eof do
          begin
            SobreOrd := SobreOrd + 1;
            SubOrdem := SubOrdem + 1;
            //Tabela   := CO_OSPipIts_OSPIPITSPR;
            GraGruX  := QrGgxPipGraGruX.Value;
            //
            InserePerguntaPipIts(SuperOrd, SobreOrd, SubOrdem, Tabela, GraGruX);
            //
            QrGgxPip.Next;
          end;
          LoopiOrd := QrGgxPip.RecordCount;
        end;
        else
               // Nada
        //(*0*)CO_PrgLstItsRelacao_COD_N_I   :
           Geral.MB_Aviso('Pergunta sem objeto aplic�vel: "' +
           QrPrgLstItsNome.Value + '"');
      end;
      //
      UltimOrd := QrPrgLstItsRelacao.Value;
      //
      if QrPrgLstItsLupForma.Value = CO_LupForma_COD_PorLoop then
      begin
        SuperLup := True;
        SuperPai := QrPrgLstItsControle.Value;
        SuperOrd := SuperOrd + 1;
      end else
      begin
        if SuperLup and (SuperPai <> QrPrgLstItsFiliacao.Value) then
        begin
          SuperLup := False;
          SuperPai := 0;
          SuperOrd := SuperOrd + 1;
        end;
      end;
      QrPrgLstIts.Next;
    end;
    //
    QrOsPM.Next;
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, 'Atualizando OS');
  Codigo := OSCab;
  StPipAdPrg := CO_StPipAdPrg_FINI; // 2
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
  'StPipAdPrg'], [
  'Codigo'], [
  StPipAdPrg], [
  Codigo], True);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  Geral.MB_Info('Inclus�o de perguntas finalizadas!');
end;

function TDmModOS.IDAgenteNaOS(Entidade, QuestaoCod: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrAgeEmOS, Dmod.MyDB, [
  'SELECT Controle ',
  'FROM osage ',
  'WHERE Codigo=' + Geral.FF0(QuestaoCod),
  'AND Agente=' + Geral.FF0(Entidade),
  '']);
  //
  Result := QrAgeEmOSControle.Value;
end;

function TDmModOS.ImprimeQRCodePMVs(const Query: TmySQLQuery; DBGrid:
TDBGrid(*; var Quais: TSelType*)): Boolean;
var
  WHR_SQL: String;
  //
  procedure AbreSQLeImprime();
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(QrImpQrcPmv, Dmod.MyDB, [
    'SELECT /*plc.Nome NO_prglstcab, */',
    'ofc.Codigo, ofc.Controle, ofc.Conta, ofc.PipCad,',
    'pip.Nome NO_PIP, gg1.Nome NO_EquipAplic',
    '/* , mon.Nome NO_FORMULA,  ',
    'med.CodUsu CU_UNIDMED, ',
    'med.SIGLA, med.Nome NO_UNIDMED */',
    'FROM osmoncab ofc ',
    '/*',
    'LEFT JOIN formulas mon ON mon.Codigo=ofc.Formula ',
    '*/',
    'LEFT JOIN gragrux ggx ON ggx.Controle=ofc.EquipAplic ',
    'LEFT JOIN gragru1 gg1 ON ggx.Gragru1=gg1.Nivel1 ',
    'LEFT JOIN pipcad  pip ON pip.Codigo=ofc.PipCad ',
    '/*',
    'LEFT JOIN unidmed med ON med.Codigo=ofc.UnidMed ',
    'LEFT JOIN prglstcab plc ON plc.Codigo=pip.PrgLstCab ',
    'LEFT JOIN siapimadep sid ON sid.Controle=pip.Dependenci ',
    'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci ',
    '*/',
    WHR_SQL,
    '']);
    MyObjects.frxMostra(frxGER_OSERV_001_ModOS_02, 'QrCode');
  end;
var
  q: TSelType;
  n: Integer;
  Itens: String;
begin
  Result := DBCheck.Quais_Selecionou(Query, DBGrid, q);
  if not Result then Exit;
  //
  WHR_SQL := 'WHERE ???';
  case q of
    istAtual:
      WHR_SQL := 'WHERE ofc.Conta=' +
      Geral.FF0(Query.FieldByName('Conta').AsInteger);
    istSelecionados:
    begin
      Itens := '';
      with DBGrid.DataSource.DataSet do
      for n := 0 to DBGrid.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGrid.SelectedRows.Items[n]));
        Itens := Itens + ', ' + Geral.FF0(Query.FieldByName('Conta').AsInteger);
      end;
      Itens := Copy(Itens, 3);
      WHR_SQL := 'WHERE ofc.Conta IN (' + Itens + ') ';
    end;
    istTodos:
      WHR_SQL := 'WHERE ofc.Controle=' +
      Geral.FF0(Query.FieldByName('Controle').AsInteger);
  end;
  AbreSQLeImprime();
end;

procedure TDmModOS.MD5_AtualizaCheckSumAgeEqiCab(Codigo: Integer);
var
  MD5: String;
begin
  MD5 := MD5_ObtemCheckSumAgentes('ageeqiits', 'Codigo', 'Entidade', Codigo,
    Dmod.MyDB);
  //
  UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ageeqicab', False, [
  'MD5'], ['Codigo'], [MD5], [Codigo], True);
end;

function TDmModOS.MD5_AtualizaCheckSumOSAge(OSCab: Integer): Integer;

  function CriaNovoAgeEqiCab(MD5: String): Integer;
  var
    Nome: String;
    Codigo, Controle, Entidade, EhLider: Integer;
  begin
    Codigo := UMyMod.BPGS1I32('ageeqicab', 'Codigo', '', '', tsPos, stIns, 0);
    Nome   := DModG.ConcatenaPrimeirosNomes('osage', 'Codigo', 'Agente', OSCab);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ageeqicab', False, [
    'Nome', 'MD5'], [
    'Codigo'], [
    Nome, MD5], [
    Codigo], True) then
    begin
      Result := Codigo;
      //
      UnDmkDAC_PF.AbreMySQLQuery0(QrAge, Dmod.MyDB, [
        'SELECT Agente ',
        'FROM osage ',
        'WHERE Codigo=' + Geral.FF0(OSCab),
        '']);
      EhLider := 0;
      //
      QrAge.First;
      while not QrAge.Eof do
      begin
        Entidade := QrAgeAgente.Value;
        //
        Controle := UMyMod.BPGS1I32('ageeqiits', 'Controle', '', '', tsPos, stIns, 0);
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'ageeqiits', False, [
        'Codigo', 'Entidade', 'EhLider'], [
        'Controle'], [
        Codigo, Entidade, EhLider], [
        Controle], True);
        //
        QrAge.Next;
      end;
    end else
      Result := 0;
  end;

var
  Qry: TmySQLQuery;
  MD5: String;
  AgeEqiCab, Codigo: Integer;
begin
  Result    := 0;
  AgeEqiCab := MD5_ObtemAgeEqiCab('osage', 'Codigo', 'Agente', OSCab, Dmod.MyDB);
  {
  MD5 := MD5_ObtemCheckSumAgentes('osage', 'Codigo', 'Agente', OSCab);
  //
  if MD5 <> '' then
  begin
    Qry := TmySQLQuery.Create(Self);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM ageeqicab ',
      'WHERE MD5="' + MD5 + '" ',
      '']);
      AgeEqiCab := Qry.FieldByName('Codigo').AsInteger;
     /
      if AgeEqiCab = 0 then
        AgeEqiCab := CriaNovoAgeEqiCab(MD5);
      //
    finally
      Qry.Free;
    end;
  end;}
  if AgeEqiCab = 0 then
  begin
    MD5 := MD5_ObtemCheckSumAgentes('osage', 'Codigo', 'Agente', OSCab, Dmod.MyDB);
    if MD5 <> '' then
      AgeEqiCab := CriaNovoAgeEqiCab(MD5);
  end;
  //
  Codigo := OSCab;
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
  'AgeEqiCab'], ['Codigo'], [AgeEqiCab], [Codigo], True) then
    Result := AgeEqiCab;
end;

function TDmModOS.MD5_ObtemAgeEqiCab(Tabela, FldID, FldAge: String;
  Codigo: Integer; Database: TmySQLDatabase): Integer;
var
  MD5: String;
  Qry: TmySQLQuery;
begin
  Result := 0;
  MD5 := MD5_ObtemCheckSumAgentes(Tabela, FldID, FldAge, Codigo, Database);
  //
  if MD5 <> '' then
  begin
    Qry := TmySQLQuery.Create(Self);
    try
      UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
      'SELECT Codigo ',
      'FROM ageeqicab ',
      'WHERE MD5="' + MD5 + '" ',
      '']);
      Result := Qry.FieldByName('Codigo').AsInteger;
    finally
      Qry.Free;
    end;
  end;
end;

function TDmModOS.MD5_ObtemCheckSumAgentes(Tabela, FldID, FldAge: String;
  Codigo: Integer; Database: TmySQLDatabase): String;
var
  Texto: String;
begin
  Result := '';
  Texto := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrAge, Database, [
  'SELECT ' + FldAge + ' Agente ',
  'FROM ' + Tabela,
  'WHERE ' + FldID + '=' + Geral.FF0(Codigo),
  'ORDER BY Agente']);
  QrAge.First;
  while not QrAge.Eof do
  begin
    Texto := Texto + ',' + Geral.FF0(QrAgeAgente.Value);
    //
    QrAge.Next;
  end;
  //
  if Texto <> '' then
  begin
    Texto := Copy(Texto, 2);
    Result := DModG.MD5_por_SQL(Texto);
  end;
end;

procedure TDmModOS.MD5_VerificaMD5DeGrupoDeAgentes();
var
  Qry: TmySQLQuery;
  MD5: String;
  Codigo: Integer;
begin
  Qry := TmySQLQuery.Create(Self);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SELECT Codigo ',
    'FROM ageeqicab ',
    'WHERE MD5="" ',
    '']);
    //
    Qry.First;
    while not Qry.Eof do
    begin
      Codigo := Qry.FieldByName('Codigo').AsInteger;
      //
      MD5_AtualizaCheckSumAgeEqiCab(Codigo);
      //
      Qry.Next;
    end;
  finally
    Qry.Free;
  end;

end;

function TDmModOS.MostraOSPos(SQLType: TSQLType; Codigo, Controle, Dias,
Aplicacao, AplicID: Integer): Boolean;
begin
  if DBCheck.CriaFm(TFmOSPos, FmOSPos, afmoNegarComAviso) then
  begin
    FmOSPos.ImgTipo.SQLType := SQLType;
    //
    FmOSPos.EdCodigo.ValueVariant := Codigo;
    //
    if SQLType = stUpd then
    begin
      FmOSPos.EdControle.ValueVariant := Controle;
      FmOSPos.EdDias.ValueVariant     := Dias;
      FmOSPos.RGAplicacao.ItemIndex   := Aplicacao;
      FmOSPos.EdAplicID.ValueVariant  := AplicID;
      FmOSPos.CBAplicID.KeyValue      := AplicID;
    end;
    FmOSPos.ShowModal;
    FmOSPos.Destroy;
    //
    Result := True;
  end;
end;

function TDmModOS.NomeDiluente_GG1(Diluente, GraGru1: Integer): String;
var
  QrLoc: TmySQLQuery;
begin
  if Diluente <> CO_COD_BUGS_DILUENTE_003 then
    Result := sListaDiluidoresBugs[Diluente]
  else begin
    QrLoc := TmySQLQuery.Create(Dmod);
    QrLoc.Close;
    QrLoc.Database := Dmod.MyDB;
    UnDmkDAC_PF.AbreMySQLQuery0(Qrloc, Dmod.MyDB, [
    'SELECT Nome ',
    'FROM gragru1 ',
    'WHERE Nivel1=' + Geral.FF0(GraGru1),
    '']);
    //
    Result := QrLoc.FieldByName('Nome').AsString;
  end;
end;

function TDmModOS.NomeDiluente_GGX(Diluente, GraGruX: Integer; AvisaErro: Boolean): String;
var
  QrLoc: TmySQLQuery;
begin
  if Diluente <> CO_COD_BUGS_DILUENTE_003 then
    Result := sListaDiluidoresBugs[Diluente]
  else begin
    if GraGruX <> 0 then
    begin
      QrLoc := TmySQLQuery.Create(Dmod);
      QrLoc.Close;
      QrLoc.Database := Dmod.MyDB;
      UnDmkDAC_PF.AbreMySQLQuery0(Qrloc, Dmod.MyDB, [
      'SELECT gg1.Nome ',
      'FROM gragru1 gg1',
      'LEFT JOIN gragrux ggx ON ggx.GraGru1=gg1.Nivel1',
      'WHERE ggx.Controle=' + Geral.FF0(GraGruX),
      '']);
      //
      Result := QrLoc.FieldByName('Nome').AsString;
    end else
    begin
      if AvisaErro then
        Geral.MB_Aviso('Reduzido n�o informado para diluente!');
      Result := 'N�o informado';
    end;
  end;
end;

function TDmModOS.ProximaOrdemCunsImgCab(Codigo: Integer): Integer;
const
  Incremento = 1;
  Base       = 0;
begin
  Result := UnDmkDAC_PF.ObtemProximaOrdem(Dmod.MyDB, 'cunsimgcab', 'Ordem',
            vpLast, dmktfInteger, Incremento, Base, ['Codigo'], [Codigo]);
end;

procedure TDmModOS.QrEntidadesBeforeClose(DataSet: TDataSet);
begin
  QrSiapTerCad.Close;
  QrEntiContat.Close;
end;

procedure TDmModOS.QrEntiTelCalcFields(DataSet: TDataSet);
begin
  QrEntiTelTEL_TXT.Value := Geral.FormataTelefone_TT(QrEntiTelTelefone.Value);
end;

procedure TDmModOS.QrLctFatRefCalcFields(DataSet: TDataSet);
begin
  QrLctFatRefPARCELA.Value := QrLctFatRef.RecNo;
end;

procedure TDmModOS.QrOSCxaAfterScroll(DataSet: TDataSet);
begin
  OSApp_PF.ReopenOSOSCxaAtrib(QrOSCxaAtrib, QrOSCxaCaixa.Value);
end;

procedure TDmModOS.QrOSCxaBeforeClose(DataSet: TDataSet);
begin
  QrOSCxaAtrib.Close;
end;

procedure TDmModOS.QrOSFrmCabAfterScroll(DataSet: TDataSet);
begin
  OSApp_PF.ReopenOSFrmRec(QrOSFrmRec, QrOSFrmCabConta.Value, 0);
  OSApp_PF.ReopenOSFrmAbr(QrOSFrmAbr, QrOSFrmCabConta.Value, 0);
  OSApp_PF.ReopenOSFrmDep(QrOSFrmDep, QrOSFrmCabConta.Value, 0);
end;

procedure TDmModOS.QrOSFrmCabBeforeClose(DataSet: TDataSet);
begin
  QrOSFrmRec.Close;
  QrOSFrmAbr.Close;
  QrOSFrmDep.Close;
end;

procedure TDmModOS.QrOSMonCabAfterScroll(DataSet: TDataSet);
begin
  OSApp_PF.ReopenOSMonRec(QrOSMonRec, QrOSMonCabConta.Value, 0);
  //OSApp_PF._Reopen_OS_Mon_Dep_(_Qr_OS_Mon_Dep_, QrOSMonCabConta.Value, 0);
end;

procedure TDmModOS.QrOSMonCabBeforeClose(DataSet: TDataSet);
begin
  QrOSMonRec.Close;
  //Qr_OS_Mon_Dep_.Close;
end;

procedure TDmModOS.QrOSSrvAfterScroll(DataSet: TDataSet);
begin
  OSApp_PF.ReopenOSAlv(QrOSAlv, QrOSSrvControle.Value, 0, Dmod.MyDB);
  OSApp_PF.ReopenOSFrmCab(QrOSFrmCab, QrOSSrvControle.Value, 0);
  OSApp_PF.ReopenOSMonCab(QrOSMonCab, QrOSSrvControle.Value, 0);
end;

procedure TDmModOS.QrOSSrvBeforeClose(DataSet: TDataSet);
begin
  QrOSAlv.Close;
  QrOSFrmCab.Close;
  QrOSMonCab.Close;
end;

procedure TDmModOS.QrOSSrvCalcFields(DataSet: TDataSet);
begin
  QrOSSrvVAL_CALCeINFO.Value := QrOSSrvValCalc.Value + QrOSSrvValInfo.Value;
  QrOSSrvAUTORIZADO_BOOL.Value := Geral.IntToBool(QrOSSrvAutorizado.Value);
end;

{ 2014-01-06
procedure TDmModOS.RatificaConsumo(TabMae, Tabela, Campo: String; Indice, GraCusPrc,
Empresa, FatID: Integer);
var
  RatifUso, IDIts, ErrItens, LstCusPrd: Integer;
  UsoQtd, UsoCusUni, UsoCusTot: Double;
  Lista, NO_Emp: String;
  //
  Codigo, Controle, Conta: Integer;
begin
  if GraCusPrc <> 0 then
    LstCusPrd := GraCusPrc
  else
    LstCusPrd := Dmod.QrOpcoesBugsLstCusPrd.Value;
  if LstCusPrd = 0 then
  begin
    Geral.MB_Aviso('Tabela de pre�o n�o definida para o lugar selecionado!' +
    sLineBreak +
    'Defina a tabela no cadastro do lugar ou nas op��es espec�ficas do aplicativo');
    Exit;
  end;
  RatifUso  := 1;
  ErrItens := 0;
  UnDmkDAC_PF.AbreMySQLQuery0(QrUsoRatif, Dmod.MyDB, [
  'SELECT omr.Codigo, omr.Controle, omr.Conta, ',
  'omr.IDIts, omr.GraGruX, omr.PrvQtd, ',
  //'cab.Entidade, eci.CodEnti, ',
  'ggv.Controle CtrlGGV, ggv.CustoPreco  ',
  'FROM ' + LowerCase(Tabela) + ' omr',
  'LEFT JOIN ' + LowerCase(TabMae) + ' ofc ON ofc.Conta=omr.Conta ',
  //'LEFT JOIN oscab cab ON cab.Codigo=omr.Codigo ',
  //'LEFT JOIN enticliint eci ON eci.CodCliInt=cab.Empresa ',
  'LEFT JOIN gragruval ggv ON ggv.GraGruX=omr.GraGruX',
  '     AND ggv.GraCusPrc=' + Geral.FF0(LstCusPrd),
  '     AND ggv.Entidade=' + Geral.FF0(Empresa),
  'WHERE omr.RatifUso=0',
  'AND omr.' + Campo + '=' + Geral.FF0(Indice),
  'AND ofc.IndicUso=' + Geral.FF0(CO_FRM_INDICUSO_001_COD_UsoDefinitivo),
  '']);
  //
  QrUsoRatif.First;
  while not QrUsoRatif.Eof do
  begin
    if QrUsoRatifCtrlGGV.Value = 0 then
      ErrItens := ErrItens + 1
    else
    begin
      UsoQtd    := QrUsoRatifPrvQtd.Value;
      UsoCusUni := QrUsoRatifCustoPreco.Value;
      UsoCusTot := UsoQtd * UsoCusUni;
      IDIts     := QrUsoRatifIDIts.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
      'UsoQtd', (*'UsoPrc',
      'UsoVal', 'UsoDec', 'UsoTot',*)
      'UsoCusUni', 'UsoCusTot',
      'RatifUso'], [
      'IDIts'], [
      UsoQtd, (*UsoPrc,
      UsoVal, UsoDec, UsoTot,*)
      UsoCusUni, UsoCusTot,
      RatifUso], [
      IDIts], True) then
      begin
        Codigo   := QrUsoRatifCodigo.Value;
        Controle := QrUsoRatifControle.Value;
        Conta    := QrUsoRatifConta.Value;
        BaixaInsumoDeOS(FatID, IDIts, DModG.ObtemAgora());
      end;
    end;
    QrUsoRatif.Next;
  end;
  //
  if ErrItens > 0 then
  begin
    UnDMkDAC_PF.AbreMySQLQuery0(QrErrRatif, Dmod.MyDB, [
    'SELECT DISTINCT omr.GraGruX, ggx.GraGru1, CONCAT(gg1.Nome, ',
    'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
    'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
    'NO_PRD_TAM_COR ',
    'FROM ' + LowerCase(Tabela) + ' omr',
    'LEFT JOIN gragrux ggx ON ggx.Controle=omr.GraGruX',
    'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
    'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
    'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
    'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
    'LEFT JOIN gragruval ggv ON ggv.GraGruX=ggx.Controle',
    '     AND ggv.GraCusPrc=' + Geral.FF0(LstCusPrd),
    '     AND ggv.Entidade=' + Geral.FF0(Empresa),
    'WHERE omr.RatifUso=0',
    'AND omr.' + Campo + '=' + Geral.FF0(Indice),
    'AND ggv.Controle IS NULL',
    'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
    '']);
    //
    Lista := UnDmkDAC_PF.ObtemNomeDeCodigo(Dmod.MyDB, 'gracusprc', LstCusPrd);
    NO_Emp := UnDmkDAC_PF.ObtemNomeDeCodigo(Dmod.MyDB, 'entidades', Empresa,
      'Codigo', 'RazaoSocial');
    frxGER_OSERV_001_ModOS_01.Variables['VARF_EMPRESA'] :=
      QuotedStr(Geral.FF0(Empresa) + ' - ' + NO_Emp);
    frxGER_OSERV_001_ModOS_01.Variables['VARF_LISTA_VALORES'] :=
      QuotedStr(Geral.FF0(LstCusPrd) + ' - ' + Lista);
    frxGER_OSERV_001_ModOS_01.Variables['VARF_DATA'] := Date;
    //
    MyObjects.frxDefineDataSets(frxGER_OSERV_001_ModOS_01, [
    DmodG.frxDsDono,
    frxDsErrRatif
    ]);
    MyObjects.frxMostra(frxGER_OSERV_001_ModOS_01,
      'Produtos sem pre�o de custo definido');
  end;
  //////////////////  Estoque///////////////////////////////////////////////////
  ///  Fazer aqui porque baixa todos abertos!
  if Tabela = 'osfrmrec' then
    FatID := VAR_FATID_4101
  else
  if Tabela = 'osmonrec' then
    FatID := VAR_FATID_4102
  else
    Geral.MB_Erro('Aten��o! O estoque poder� ficar desatualizado!' + sLineBreak +
    'A tabela "' + Tabela + '" n�o foi implementada para baixa de estoque!');
  //////////////////////////////////////////////////////////////////////////////
end;
}

procedure TDmModOS.RatificaConsumo(TabMae, Tabela, Campo: String; Indice, GraCusPrc,
Empresa, FatID: Integer);
var
  RatifUso, IDIts, ErrItens, LstCusPrd: Integer;
  UsoQtd, UsoCusUni, UsoCusTot: Double;
  Lista, NO_Emp, SQL_Elimin_Nao_Feito: String;
  //
  Codigo, Controle, Conta: Integer;
begin
  if GraCusPrc <> 0 then
    LstCusPrd := GraCusPrc
  else
    LstCusPrd := Dmod.QrOpcoesBugsLstCusPrd.Value;
  //
  if LstCusPrd = 0 then
  begin
    Geral.MB_Aviso('Tabela de pre�o n�o definida para o lugar selecionado!' +
      sLineBreak +
      'Defina a tabela no cadastro do lugar ou nas op��es espec�ficas do aplicativo');
    Exit;
  end;
  case FatID of
    VAR_FATID_4101: SQL_Elimin_Nao_Feito := Geral.ATS(['AND ',
                    'IF((srv.TudoFeitoM OR srv.TudoFeitoA), 1, ',
                    'IF(ofc.PercFeito>99.99, 1,  0)) = 1 ']);
    VAR_FATID_4102: SQL_Elimin_Nao_Feito := Geral.ATS(['AND ',
                    'IF((srv.TudoFeitoM OR srv.TudoFeitoA), 1, 0) = 1 ']);
    else
    begin
      SQL_Elimin_Nao_Feito := '';
      Geral.MB_Erro('"FatID" n�o definido na ratifica��o de consumo!');
      Exit;
    end;
  end;
  RatifUso := 1;
  ErrItens := 0;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrUsoRatif, Dmod.MyDB, [
    'SELECT omr.Codigo, omr.Controle, omr.Conta, ',
    'omr.IDIts, omr.GraGruX, omr.PrvQtd, ',
    //'cab.Entidade, eci.CodEnti, ',
    'ggv.Controle CtrlGGV, ggv.CustoPreco  ',
    'FROM ' + LowerCase(Tabela) + ' omr',
    'LEFT JOIN ' + LowerCase(TabMae) + ' ofc ON ofc.Conta=omr.Conta ',
    'LEFT JOIN ossrv srv ON srv.Controle=ofc.Controle ',
    //'LEFT JOIN oscab cab ON cab.Codigo=omr.Codigo ',
    //'LEFT JOIN enticliint eci ON eci.CodCliInt=cab.Empresa ',
    'LEFT JOIN gragruval ggv ON ggv.GraGruX=omr.GraGruX',
    '     AND ggv.GraCusPrc=' + Geral.FF0(LstCusPrd),
    '     AND ggv.Entidade=' + Geral.FF0(Empresa),
    'WHERE omr.RatifUso=0',
    'AND omr.' + Campo + '=' + Geral.FF0(Indice),
    (*
    Mudado em: 20160810 => Aparentemente este campo n�o usa e quando marca uso indefinido ele n�o considera
    'AND ofc.IndicUso=' + Geral.FF0(CO_FRM_INDICUSO_001_COD_UsoDefinitivo),
    *)
    SQL_Elimin_Nao_Feito,
    '']);
  //
  QrUsoRatif.First;
  while not QrUsoRatif.Eof do
  begin
    if QrUsoRatifCtrlGGV.Value = 0 then
      ErrItens := ErrItens + 1
    else
    begin
      UsoQtd    := QrUsoRatifPrvQtd.Value;
      UsoCusUni := QrUsoRatifCustoPreco.Value;
      UsoCusTot := UsoQtd * UsoCusUni;
      IDIts     := QrUsoRatifIDIts.Value;
      //
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, Tabela, False, [
        'UsoQtd', (*'UsoPrc',
        'UsoVal', 'UsoDec', 'UsoTot',*)
        'UsoCusUni', 'UsoCusTot',
        'RatifUso'], [
        'IDIts'], [
        UsoQtd, (*UsoPrc,
        UsoVal, UsoDec, UsoTot,*)
        UsoCusUni, UsoCusTot,
        RatifUso], [
        IDIts], True) then
      begin
        Codigo   := QrUsoRatifCodigo.Value;
        Controle := QrUsoRatifControle.Value;
        Conta    := QrUsoRatifConta.Value;
        BaixaInsumoDeOS(FatID, IDIts, DModG.ObtemAgora());
      end;
    end;
    QrUsoRatif.Next;
  end;
  //
  if ErrItens > 0 then
  begin
    UnDMkDAC_PF.AbreMySQLQuery0(QrErrRatif, Dmod.MyDB, [
      'SELECT DISTINCT omr.GraGruX, ggx.GraGru1, CONCAT(gg1.Nome, ',
      'IF(gti.PrintTam=0, "", CONCAT(" ", gti.Nome)), ',
      'IF(gcc.PrintCor=0,"", CONCAT(" ", gcc.Nome))) ',
      'NO_PRD_TAM_COR ',
      'FROM ' + LowerCase(Tabela) + ' omr',
      'LEFT JOIN gragrux ggx ON ggx.Controle=omr.GraGruX',
      'LEFT JOIN gragruc    ggc ON ggc.Controle=ggx.GraGruC ',
      'LEFT JOIN gracorcad  gcc ON gcc.Codigo=ggc.GraCorCad ',
      'LEFT JOIN gratamits  gti ON gti.Controle=ggx.GraTamI ',
      'LEFT JOIN gragru1    gg1 ON gg1.Nivel1=ggx.GraGru1 ',
      'LEFT JOIN gragruval ggv ON ggv.GraGruX=ggx.Controle',
      '     AND ggv.GraCusPrc=' + Geral.FF0(LstCusPrd),
      '     AND ggv.Entidade=' + Geral.FF0(Empresa),
      'WHERE omr.RatifUso=0',
      'AND omr.' + Campo + '=' + Geral.FF0(Indice),
      'AND ggv.Controle IS NULL',
      'ORDER BY NO_PRD_TAM_COR, ggx.Controle ',
      '']);
    //
    Lista      := UnDmkDAC_PF.ObtemNomeDeCodigo(Dmod.MyDB, 'gracusprc', LstCusPrd);
    NO_Emp     := UnDmkDAC_PF.ObtemNomeDeCodigo(Dmod.MyDB, 'entidades', Empresa, 'Codigo', 'RazaoSocial');
    FLstCusPrd := LstCusPrd;
    //
    frxGER_OSERV_001_ModOS_01.Variables['VARF_EMPRESA'] :=
      QuotedStr(Geral.FF0(Empresa) + ' - ' + NO_Emp);
    frxGER_OSERV_001_ModOS_01.Variables['VARF_LISTA_VALORES'] :=
      QuotedStr(Geral.FF0(LstCusPrd) + ' - ' + Lista);
    frxGER_OSERV_001_ModOS_01.Variables['VARF_DATA'] := Date;
    //
    MyObjects.frxDefineDataSets(frxGER_OSERV_001_ModOS_01, [
      DmodG.frxDsDono,
      frxDsErrRatif
      ]);
    MyObjects.frxMostra(frxGER_OSERV_001_ModOS_01,
      'Produtos sem pre�o de custo definido');
  end;
  //////////////////  Estoque  /////////////////////////////////////////////////
  ///  Fazer aqui porque baixa todos abertos!
  if Tabela = 'osfrmrec' then
    FatID := VAR_FATID_4101
  else
  if Tabela = 'osmonrec' then
    FatID := VAR_FATID_4102
  else
    Geral.MB_Erro('Aten��o! O estoque poder� ficar desatualizado!' + sLineBreak +
    'A tabela "' + Tabela + '" n�o foi implementada para baixa de estoque!');
  //////////////////////////////////////////////////////////////////////////////
end;
// FIM 2014-01-06

procedure TDmModOS.ReativarPIPemOS(SiapTerCad: Integer);
begin
  if DBCheck.CriaFm(TFmReativaPIPemOS, FmReativaPIPemOS, afmoNegarComAviso) then
  begin
    ReopenGgxPIP(FmReativaPIPemOS.QrGgxPip, SiapTerCad, False,
      'ORDER BY Codigo DESC, Controle DESC , Conta DESC, IDIts DESC, Ordem DESC');
    //
    // Somente se houver consumo de produto
    // Placa de monitoramento n�o tem!!!
    if FmReativaPIPemOS.QrGgxPip.RecordCount > 0 then
    begin
      FmReativaPIPemOS.FBgstDoPip := gstDoPipAtivaSelec;
      if Dmod.QrOpcoesBugsAutReatPIP.Value = 0 then
        FmReativaPIPemOS.ShowModal
      else
        FmReativaPIPemOS.BtOKClick(FmReativaPIPemOS)
    end;
    FmReativaPIPemOS.Destroy;
  end;
end;

procedure TDmModOS.ReopenGgxPIP(Qry: TmySQLQuery; SiapTerCad: Integer; Ativo: Boolean;
TXT_Ordem: String);
var
  SQL_TXT: String;
begin
  if Ativo then
    SQL_TXT := 'AND omr.Desativado=0'
  else
    SQL_TXT := '';
  //
  UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
  'SELECT omr.Codigo, omr.Controle, omr.Conta, omr.IDIts,',
  'omr.Ordem, omr.GraGruX, omr.UsoQtd, omr.ValCliDd, ',
  'cab.DtaExeIni, ',
  Geral.FF0(CO_OSPipIts_OSMONREC) + ' Tabela', // 2
  'FROM osmonrec omr',
  'LEFT JOIN osmoncab omc ON omc.Conta=omr.Conta',
  'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo',
  'WHERE omc.PipCad=' + Geral.FF0(QrOsPMPipCad.Value),
  SQL_TXT,
  'AND cab.SiapTerCad=' + Geral.FF0(SiapTerCad),
  '',
  'UNION',
  '',
  'SELECT omr.Codigo, omr.Controle, omr.Conta, omr.IDIts,',
  'omc.Ordem, omr.GraGruX, omr.UsoQtd, omr.ValCliDd, ',
  'cab.DtaExeIni, ',
  Geral.FF0(CO_OSPipIts_OSPIPITSPR) + ' Tabela', // 3
  'FROM ospipitspr omr',
  'LEFT JOIN ospipmon omc ON omc.Controle=omr.Controle',
  'LEFT JOIN oscab cab ON cab.Codigo=omc.Codigo',
  'WHERE omc.PipCad=' + Geral.FF0(QrOsPMPipCad.Value),
  SQL_TXT,
  'AND cab.SiapTerCad=' + Geral.FF0(SiapTerCad),
  '',
  '',
  TXT_Ordem,
  '']);
  //
end;

procedure TDmModOS.ReopenSTC_Unico(Codigo: Integer);
begin
  QrSTC.Close;
  UnDmkDAC_PF.AbreMySQLQuery0(QrSTC, Dmod.MyDB, [
  'SELECT llo.Nome NOMELOGRAD, stc.* ',
  'FROM siaptercad stc ',
  'LEFT JOIN listalograd llo ON llo.Codigo=stc.SLograd ',
  'WHERE stc.Codigo=' + Geral.FF0(Codigo),
  '']);
end;

procedure TDmModOS.SelecionaPipAtivoDeOS(SiapTerCad: Integer);
begin
  if DBCheck.CriaFm(TFmReativaPIPemOS, FmReativaPIPemOS, afmoNegarComAviso) then
  begin
    ReopenGgxPIP(FmReativaPIPemOS.QrGgxPip, SiapTerCad, False,
      'ORDER BY Codigo DESC, Controle DESC , Conta DESC, IDIts DESC, Ordem DESC');
    //
    FmReativaPIPemOS.FBgstDoPip := gstDoPipDesativaNaoSelec;
    if FmReativaPIPemOS.QrGgxPip.RecordCount > 0 then
    begin
      if Dmod.QrOpcoesBugsAutReatPIP.Value = 0 then
        FmReativaPIPemOS.ShowModal
      else
        FmReativaPIPemOS.BtOKClick(FmReativaPIPemOS);
    end else
      FmReativaPIPemOS.ShowModal;
    //
    FmReativaPIPemOS.Destroy;
  end;
end;

function TDmModOS.SQL_Filtr(): String;
begin
  Result := Geral.ATS([
  'WHERE xxx.Ativo > 0 ',
  'AND xxx.EmisUltDta < SYSDATE()  ',
  'AND cab.DtaExeFim > "1900-01-01"  ',
  'AND cab.FatoGeradr=' + Geral.FF0(CO_COD_FatoGeradr_1aAcao),
  'AND cab.PosGerou=0  ', // 2014-07-13
  'AND  ',
  '(  ',
  '     ( ',
  '          cab.NumContrat=0  ',
  '          AND ',
  '          xxx.EmisStatus IN (' +
  Geral.FF0(CO_FORMUL_FILHA_MONIT_EmisStat_ABERTO) + ', ' + // 0
  Geral.FF0(CO_FORMUL_FILHA_MONIT_EmisStat_REABERTO) + ') ', // 3
  '     ) ',
  '     OR  ',
  '     (  ',
  '          cab.NumContrat<>0 ',
  '          AND ',
  '          ctr.DtaCntrFim < "1900-01-01" ',
  '          AND ',
  '          xxx.EmisStatus <> ' + Geral.FF0(CO_FORMUL_FILHA_MONIT_EmisStat_TOTAL), // 2
  '     )  ',
  ')  ',
  '']);
end;

function TDmModOS.UpLoadWeb_OS(Codigo: Integer; Abertos: FUploadAbertos;
LaAviso1, LaAviso2: TLabel): Boolean;
(*
var
  CodTxt: String;
  DtaContat, DtaVisPrv, FimVisPrv, DtaExePrv, FimVisExe, DtaVisExe, FimExePrv,
  DtaExeIni, DtaExeFim, ObsGaranti, ObsExecuta, NO_ENTICONTAT, NO_FatoGeradr,
  NO_ESTATUS, NO_SiapTerCad, NO_ENT, Tel_ENT, NO_PAG, NO_CTR, NO_CART, NO_PRZ,
  NO_ExeTxtCli1, NO_ExeTxtCli2, NO_AgeEqiCab: String;
  Evolucao, Grupo, Opcao, Empresa, Entidade, SiapTerCad, Estatus,
  FatoGeradr, EntiContat, NumContrat, EntPagante, EntContrat, CondicaoPg,
  Operacao, ExeTxtCli1, ExeTxtCli2, AgeEqiCab, LstCusPrd, LCPUsed: Integer;
  OrcamServi, OrcamDesco, OrcamOutrs, OrcamTotal, InvalServi, InvalDesco,
  InvalOutrs, InvalTotal, ValorServi, ValorDesco, ValorOutrs, ValorTotal,
  MulServico: Double;
  // OSPipMon
  NO_PrgLstCab, NO_PIP, LstUplWeb: String;
  Controle, PipCad, Ordem, Reordem, PrgLstCab, OSFMCbMae, Insercoes: Integer;
  QrExeWeb: TmySQLQuery;
*)
begin
  (*
  QrExeWeb := TmySQLQuery.Create(Dmod);
  try
    QrExeWeb.Database := Dmod.MyDBn;
    //
    CodTxt := Geral.FF0(Codigo);
    UnDmkDAC_PF.ExecutaMySQLQuery0(nil, Dmod.MyDBn, [
    'DELETE FROM oswcab WHERE Codigo=' + CodTxt  + ';',
    'DELETE FROM oswpipmon WHERE Codigo=' + CodTxt  + ';',
    'DELETE FROM ospipits WHERE Codigo=' + CodTxt  + ';',
    'DELETE FROM ospipitspr WHERE Codigo=' + CodTxt  + ';',
    '']);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrUL_OSCab, Dmod.MyDB, [
    'SELECT cab.Codigo, cab.Grupo, cab.Opcao, cab.Empresa, ',
    'cab.Entidade, cab.SiapTerCad, cab.Estatus, cab.FatoGeradr, ',
    'cab.DtaContat, ',
    'cab.DtaVisPrv, cab.FimVisPrv, ',
    'cab.DtaExePrv, cab.FimVisExe, ',
    'cab.DtaVisExe, cab.FimExePrv, ',
    'cab.DtaExeIni, cab.DtaExeFim, ',
    ' ',
    'cab.OrcamServi, cab.OrcamDesco, cab.OrcamOutrs, cab.OrcamTotal, ',
    'cab.InvalServi, cab.InvalDesco, cab.InvalOutrs, cab.InvalTotal, ',
    'cab.ValorServi, cab.ValorDesco, cab.ValorOutrs, cab.ValorTotal, ',
    'cab.EntiContat, cab.NumContrat, cab.EntPagante, cab.EntContrat, ',
    'cab.CondicaoPg, cab.Operacao, ',
    'cab.ObsGaranti, cab.ObsExecuta, ',
    'cab.ExeTxtCli1, cab.ExeTxtCli2, ',
    'cab.MulServico, cab.AgeEqiCab, ',
    ' ',
    'eco.Nome NO_ENTICONTAT, ',
    'fge.Nome NO_FatoGeradr, sta.Nome NO_ESTATUS, ',
    'stc.Nome NO_SiapTerCad, stc.LstCusPrd, ',
    'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT, ',
    'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Tel_ENT, ',
    'IF(pag.Tipo=0, pag.RazaoSocial, pag.Nome) NO_PAG, ',
    'IF(ctr.Tipo=0, ctr.RazaoSocial, ctr.Nome) NO_CTR, ',
    'car.Nome NO_CART, ppc.Nome NO_PRZ, ',
    'tg1.Nome NO_ExeTxtCli1, tg2.Nome NO_ExeTxtCli2, ',
    'aec.Nome NO_AgeEqiCab, cab.LCPUsed ',
    ' ',
    'FROM oscab cab ',
    'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade ',
    'LEFT JOIN entidades pag ON pag.Codigo=cab.EntPagante ',
    'LEFT JOIN entidades ctr ON ctr.Codigo=cab.EntContrat ',
    'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr ',
    'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus ',
    'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad ',
    'LEFT JOIN carteiras car ON car.Codigo=cab.CartEmis ',
    'LEFT JOIN pediprzcab ppc ON ppc.Codigo=cab.CondicaoPg ',
    'LEFT JOIN enticontat eco ON eco.Controle=cab.EntiContat ',
    'LEFT JOIN txtgeneric tg1 ON tg1.Codigo=cab.ExeTxtCli1 ',
    'LEFT JOIN txtgeneric tg2 ON tg2.Codigo=cab.ExeTxtCli2 ',
    'LEFT JOIN ageeqicab aec ON aec.Codigo=cab.AgeEqiCab ',
    ' ',
    'WHERE cab.Codigo=' + Geral.FF0(Codigo),
    '']);
    //
    //Codigo         := QrUL_OSCabCodigo.Value;
    Evolucao       := CO_OSWCAB_EVOLUCAO_00000_COD_UPLD_DSKTP_CLOUD_INI;
    Grupo          := QrUL_OSCabGrupo.Value;
    Opcao          := QrUL_OSCabOpcao.Value;
    Empresa        := QrUL_OSCabEmpresa.Value;
    Entidade       := QrUL_OSCabEntidade.Value;
    SiapTerCad     := QrUL_OSCabSiapTerCad.Value;
    Estatus        := QrUL_OSCabEstatus.Value;
    FatoGeradr     := QrUL_OSCabFatoGeradr.Value;
    DtaContat      := Geral.FDT(QrUL_OSCabDtaContat.Value, 109);
    DtaVisPrv      := Geral.FDT(QrUL_OSCabDtaVisPrv.Value, 109);
    FimVisPrv      := Geral.FDT(QrUL_OSCabFimVisPrv.Value, 109);
    DtaExePrv      := Geral.FDT(QrUL_OSCabDtaExePrv.Value, 109);
    FimVisExe      := Geral.FDT(QrUL_OSCabFimVisExe.Value, 109);
    DtaVisExe      := Geral.FDT(QrUL_OSCabDtaVisExe.Value, 109);
    FimExePrv      := Geral.FDT(QrUL_OSCabFimExePrv.Value, 109);
    DtaExeIni      := Geral.FDT(QrUL_OSCabDtaExeIni.Value, 109);
    DtaExeFim      := Geral.FDT(QrUL_OSCabDtaExeFim.Value, 109);
    OrcamServi     := QrUL_OSCabOrcamServi.Value;
    OrcamDesco     := QrUL_OSCabOrcamDesco.Value;
    OrcamOutrs     := QrUL_OSCabOrcamOutrs.Value;
    OrcamTotal     := QrUL_OSCabOrcamTotal.Value;
    InvalServi     := QrUL_OSCabInvalServi.Value;
    InvalDesco     := QrUL_OSCabInvalDesco.Value;
    InvalOutrs     := QrUL_OSCabInvalOutrs.Value;
    InvalTotal     := QrUL_OSCabInvalTotal.Value;
    ValorServi     := QrUL_OSCabValorServi.Value;
    ValorDesco     := QrUL_OSCabValorDesco.Value;
    ValorOutrs     := QrUL_OSCabValorOutrs.Value;
    ValorTotal     := QrUL_OSCabValorTotal.Value;
    EntiContat     := QrUL_OSCabEntiContat.Value;
    NumContrat     := QrUL_OSCabNumContrat.Value;
    EntPagante     := QrUL_OSCabEntPagante.Value;
    EntContrat     := QrUL_OSCabEntContrat.Value;
    CondicaoPg     := QrUL_OSCabCondicaoPg.Value;
    Operacao       := QrUL_OSCabOperacao.Value;
    ObsGaranti     := QrUL_OSCabObsGaranti.Value;
    ObsExecuta     := QrUL_OSCabObsExecuta.Value;
    ExeTxtCli1     := QrUL_OSCabExeTxtCli1.Value;
    ExeTxtCli2     := QrUL_OSCabExeTxtCli2.Value;
    MulServico     := QrUL_OSCabMulServico.Value;
    AgeEqiCab      := QrUL_OSCabAgeEqiCab.Value;
    NO_ENTICONTAT  := QrUL_OSCabNO_ENTICONTAT.Value;
    NO_FatoGeradr  := QrUL_OSCabNO_FatoGeradr.Value;
    NO_ESTATUS     := QrUL_OSCabNO_ESTATUS.Value;
    NO_SiapTerCad  := QrUL_OSCabNO_SiapTerCad.Value;
    LstCusPrd      := QrUL_OSCabLstCusPrd.Value;
    NO_ENT         := QrUL_OSCabNO_ENT.Value;
    Tel_ENT        := QrUL_OSCabTel_ENT.Value;
    NO_PAG         := QrUL_OSCabNO_PAG.Value;
    NO_CTR         := QrUL_OSCabNO_CTR.Value;
    NO_CART        := QrUL_OSCabNO_CART.Value;
    NO_PRZ         := QrUL_OSCabNO_PRZ.Value;
    NO_ExeTxtCli1  := QrUL_OSCabNO_ExeTxtCli1.Value;
    NO_ExeTxtCli2  := QrUL_OSCabNO_ExeTxtCli1.Value;
    NO_AgeEqiCab   := QrUL_OSCabNO_AgeEqiCab.Value;
    LCPUsed        := QrUL_OSCabLCPUsed.Value;

    //
    if UMyMod.SQLInsUpd(QrExeWeb, stIns, 'oswcab', False, [
    'Evolucao', 'Grupo', 'Opcao',
    'Empresa', 'Entidade', 'SiapTerCad',
    'Estatus', 'FatoGeradr', 'DtaContat',
    'DtaVisPrv', 'FimVisPrv', 'DtaExePrv',
    'FimVisExe', 'DtaVisExe', 'FimExePrv',
    'DtaExeIni', 'DtaExeFim', 'OrcamServi',
    'OrcamDesco', 'OrcamOutrs', 'OrcamTotal',
    'InvalServi', 'InvalDesco', 'InvalOutrs',
    'InvalTotal', 'ValorServi', 'ValorDesco',
    'ValorOutrs', 'ValorTotal', 'EntiContat',
    'NumContrat', 'EntPagante', 'EntContrat',
    'CondicaoPg', 'Operacao',
    'ObsGaranti', 'ObsExecuta',
    'ExeTxtCli1', 'ExeTxtCli2', 'MulServico',
    'AgeEqiCab', 'NO_ENTICONTAT', 'NO_FatoGeradr',
    'NO_ESTATUS', 'NO_SiapTerCad', 'LstCusPrd',
    'NO_ENT', 'Tel_ENT', 'NO_PAG',
    'NO_CTR', 'NO_CART', 'NO_PRZ',
    'NO_ExeTxtCli1', 'NO_ExeTxtCli2', 'NO_AgeEqiCab',
    'LCPUsed'], [
    'Codigo'], [
    Evolucao, Grupo, Opcao,
    Empresa, Entidade, SiapTerCad,
    Estatus, FatoGeradr, DtaContat,
    DtaVisPrv, FimVisPrv, DtaExePrv,
    FimVisExe, DtaVisExe, FimExePrv,
    DtaExeIni, DtaExeFim, OrcamServi,
    OrcamDesco, OrcamOutrs, OrcamTotal,
    InvalServi, InvalDesco, InvalOutrs,
    InvalTotal, ValorServi, ValorDesco,
    ValorOutrs, ValorTotal, EntiContat,
    NumContrat, EntPagante, EntContrat,
    CondicaoPg, Operacao,
    ObsGaranti, ObsExecuta,
    ExeTxtCli1, ExeTxtCli2, MulServico,
    AgeEqiCab, NO_ENTICONTAT, NO_FatoGeradr,
    NO_ESTATUS, NO_SiapTerCad, LstCusPrd,
    NO_ENT, Tel_ENT, NO_PAG,
    NO_CTR, NO_CART, NO_PRZ,
    NO_ExeTxtCli1, NO_ExeTxtCli2, NO_AgeEqiCab,
    LCPUsed], [
    Codigo], True) then
    begin
      UnDmkDAC_PF.AbreMySQLQuery0(QrUL_OSPipMon, Dmod.MyDB, [
      'SELECT plc.Nome NO_PrgLstCab, pip.Nome NO_PIP, opm.* ',
      'FROM ospipmon opm ',
      'LEFT JOIN pipcad pip ON pip.Codigo=opm.PipCad ',
      'LEFT JOIN prglstcab plc ON plc.Codigo=opm.PrgLstCab ',
      ' ',
      'WHERE opm.Codigo=' + Geral.FF0(Codigo),
      '']);
      //
      QrUL_OSPipMon.First;
      while not QrUL_OSPipMon.Eof do
      begin
        //Codigo         := ;
        Controle       := QrUL_OSPipMonControle.Value;
        PipCad         := QrUL_OSPipMonPipCad.Value;
        Ordem          := QrUL_OSPipMonOrdem.Value;
        Reordem        := QrUL_OSPipMonReordem.Value;
        PrgLstCab      := QrUL_OSPipMonPrgLstCab.Value;
        OSFMCbMae      := QrUL_OSPipMonOSFMCbMae.Value;
        Insercoes      := QrUL_OSPipMonInsercoes.Value;
        NO_PrgLstCab   := QrUL_OSPipMonNO_PrgLstCab.Value;
        NO_PIP         := QrUL_OSPipMonNO_PIP.Value;
        //
        //if
        UMyMod.SQLInsUpd(QrExeWeb, stIns, 'oswpipmon', False, [
        'Codigo', 'PipCad', 'Ordem',
        'Reordem', 'PrgLstCab', 'OSFMCbMae',
        'Insercoes', 'NO_PrgLstCab', 'NO_PIP'], [
        'Controle'], [
        Codigo, PipCad, Ordem,
        Reordem, PrgLstCab, OSFMCbMae,
        Insercoes, NO_PrgLstCab, NO_PIP], [
        Controle], True);
        //
        QrUL_OSPipMon.Next;
      end;
      //
      DmkWeb_Jan.MostraWSincro(CO_DMKID_APP, CO_Versao,
        DModG.QrMasterHabilModulos.Value, DmodG.QrOpcoesGerl, Dmod.MyDBn,
        Dmod.MyDB, DModG.QrControle.FieldByName('DtaSincro').AsDateTime, TMeuDB,
        VAR_AllID_DB_NOME, nil, ['ospipits', 'osage'], VAR_VERIFI_DB_CANCEL);
      //
      // No final ...
      LstUplWeb := Geral.FDT(DModG.ObtemAgora(), 109);
      Evolucao := CO_OSWCAB_EVOLUCAO_01000_COD_UPLD_DSKTP_CLOUD_FIM;
      if UMyMod.SQLInsUpd(QrExeWeb, stUpd, 'oswcab', False, [
      'Evolucao', 'LstUplWeb'], ['Codigo'], [Evolucao, LstUplWeb], [
      Codigo], True) then
      begin
        UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'oscab', False, [
        'LstUplWeb'], ['Codigo'], [LstUplWeb], [
        Codigo], True);
      end;
    end;
    Geral.MB_Info('Upload do localizador n�mero ' + Geral.FF0(Codigo) +
      ' finalizado com sucesso!');
  finally
    QrExeWeb.Free;
  end;
  *)
  Geral.MB_Aviso('Tempor�riamente indispon�vel!');
end;

function TDmModOS.VerificaFormulasFilhas(MostraForm, Pergunta: Boolean;
  AbrirEmAba: Boolean = False; PageControl: TPageControl = nil;
  AdvToolBarPager: TAdvToolBarPager = nil): Integer;
var
  Txt: String;
  Itens: Integer;
  Qry: TmySQLQuery;
  Continua: Boolean;
begin
  Result := 0;
////////////////////////////////////////////////////////////////////////////////
///   REMOVER NO FUTURO - QUANDO TIVER CRIADO �NDICE UNIQUE1
  Qry := TmySQLQUery.Create(Dmod);
  try
    UnDmkDAC_PF.AbreMySQLQuery0(Qry, Dmod.MyDB, [
    'SHOW INDEX ',
    'FROM ospipmon ',
    '']);
    //
    if Qry.RecordCount < 2 then
    begin
      Geral.MB_Erro('�ndice �nico n�o criado para a tabela "ospipmon"');
      //
      Exit;
    end;
  finally
    Qry.Free;
  end;
///  FIM Provis�rio
////////////////////////////////////////////////////////////////////////////////
  UnDmkDAC_PF.AbreMySQLQuery0(QrFrmFil, Dmod.MyDB, [
  'SELECT COUNT(xxx.IDIts) ITENS ',
  'FROM osfrmflhcb xxx ',
  'LEFT JOIN oscab cab ON cab.Codigo=xxx.Codigo ',
  'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat ',
  SQL_Filtr(),
  ' ',
  'UNION ',
  ' ',
  'SELECT COUNT(xxx.Conta) ITENS  ',
  'FROM osmoncab xxx  ',
  'LEFT JOIN oscab cab ON cab.Codigo=xxx.Codigo  ',
  'LEFT JOIN contratos ctr ON ctr.Codigo=cab.NumContrat  ',
  '  ',
  SQL_Filtr(),
  '']);
  Itens := 0;
  //Geral.MB_Aviso(QrFrmFil.SQL.Text);
  QrFrmFil.First;
  while not QrFrmFil.Eof do
  begin
    Itens := Itens + QrFrmFilITENS.Value;
    QrFrmFil.Next;
  end;
  if Itens > 0 then
  begin
    if (Dmod.QrOpcoesBugsFlhAgeEqi.Value = 0)
    or (Dmod.QrOpcoesBugsFlhEntCtt.Value = 0)
    or (Dmod.QrOpcoesBugsFlhNumCtr.Value = 0)
    or (Dmod.QrOpcoesBugsFlhEntCtr.Value = 0)
    or (Dmod.QrOpcoesBugsFlhEntPag.Value = 0)
    or (Dmod.QrOpcoesBugsFlhCondPg.Value = 0)
    or (Dmod.QrOpcoesBugsFlhCrtEmi.Value = 0)
    or (Dmod.QrOpcoesBugsFlhPrePrg.Value = 0) then
    begin
      if Geral.MB_Pergunta(
      '� necess�io configurar a forma de gera��o de OS de monitoramento filhas!'
      + sLineBreak + 'Deseja faz�-lo agora?') = ID_YES then
          Dmod.MostraFormOpcoesBugs(5);
      //
      Exit;
    end;
    //
    Result := Itens;
    //
    if MostraForm then
    begin
      if Itens = 1 then
        Txt :=
        'Existe um item dependente de gera��o de OS filha de monitoramento. '
        + 'Deseja verificar a gera��o agora?'
      else
        Txt := 'Existem ' + Geral.FF0(Itens) +
        ' itens dependentes de gera��o de OSs filhas de monitoramento. '
        + 'Deseja verificar a gera��o agora?';
      if Pergunta then
        Continua := Geral.MB_Pergunta(Txt) = ID_YES
      else
        Continua := True;
      if Continua then
      begin
        if (not AbrirEmAba) then
        begin
          if DBCheck.CriaFm(TFmOSFlhGer, FmOSFlhGer, afmoNegarComAviso) then
          begin
            FmOSFlhGer.ShowModal;
            FmOSFlhGer.Destroy;
          end;
        end else
          MyObjects.FormTDICria(TFmOSFlhGer, PageControl, AdvToolBarPager, False, True);
      end;
    end;
  end else
    if MostraForm then
      Geral.MB_Info('N�o h� f�rmulas filhas a serem geradas!');
end;

procedure TDmModOS.VerificaMulServico(GBAvisos1: TGroupBox;
LaAviso1, LaAviso2: TLabel; PB1: TProgressBar);
var
  Conjunto: Double;
  Nome: String;
  Codigo: Integer;
  Visivel: Boolean;
begin
  Screen.Cursor := crHourGlass;
  //
  if GBAvisos1 <> nil then
  begin
    Visivel := GBAvisos1.Visible;
    GBAvisos1.Visible := True;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Atualizando conjuntos de servi�os');
  UnDmkDAC_PF.AbreQuery(QrNulServico, Dmod.MyDB);
  if PB1 <> nil then
  begin
    PB1.Position := 0;
    PB1.Max := QrNulServico.RecordCount;
  end;
  QrNulServico.First;
  while not QrNulServico.Eof do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
    'Atualizando conjunto: ' + Geral.FFI(QrNulServicoCodigo.Value));
    Conjunto := QrNulServicoCodigo.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrDesServico, Dmod.MyDB, [
    'SELECT des.codigo, des.Nome, des.Sigla ',
    'FROM desservico des ',
    'WHERE ' + Geral.FFT(Conjunto, 0, siPositivo) + ' & POWER(2, des.Codigo-1) ',
    'ORDER BY Sigla ',
    '']);
    //
    Nome := '';
    QrDesServico.First;
    while not QrDesServico.Eof do
    begin
      Nome := Nome + ', ' + QrDesServicoSigla.Value;
      //
      QrDesServico.Next;
    end;
    if Nome <> '' then
    Nome := Copy(Nome, 3);
    //
    Codigo := Geral.IMV(Geral.FFI(Conjunto));
    //
    UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'mulservico', False, [
    'Nome'], [
    'Codigo'], [
    Nome], [
    Codigo], True);
    //
    QrNulServico.Next;
  end;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  //
  Screen.Cursor := crDefault;
end;

(*
object Qr_OS_Mon_Dep_: TmySQLQuery
  Database = Dmod.MyDB
  SQL.Strings = (
    'SELECT osd.*, ELT(osd.Tabela, "I", "M", "?") SIGLA,'
    'ELT(osd.Tabela, dpd.Nome, mac.Nome, "? ? ?") NO_Campo'
    'FROM _OS_Mon_Dep_ osd'
    'LEFT JOIN siapimadep sid ON sid.Controle=osd.Cadastro'
    'LEFT JOIN dependenci dpd ON dpd.Codigo=sid.Dependenci'
    'LEFT JOIN movamovcad mac ON mac.Codigo=osd.Cadastro'
    'WHERE osd.Conta=:P0'
    '')
  Left = 40
  Top = 388
  ParamData = <
    item
      DataType = ftUnknown
      Name = 'P0'
      ParamType = ptUnknown
    end>
  object Qr_OS_Mon_Dep_Codigo: TIntegerField
    FieldName = 'Codigo'
  end
  object Qr_OS_Mon_Dep_Controle: TIntegerField
    FieldName = 'Controle'
  end
  object Qr_OS_Mon_Dep_Conta: TIntegerField
    FieldName = 'Conta'
  end
  object Qr_OS_Mon_Dep_Tabela: TSmallintField
    FieldName = 'Tabela'
  end
  object Qr_OS_Mon_Dep_Cadastro: TIntegerField
    FieldName = 'Cadastro'
  end
  object Qr_OS_Mon_Dep_Lk: TIntegerField
    FieldName = 'Lk'
  end
  object Qr_OS_Mon_Dep_DataCad: TDateField
    FieldName = 'DataCad'
  end
  object Qr_OS_Mon_Dep_DataAlt: TDateField
    FieldName = 'DataAlt'
  end
  object Qr_OS_Mon_Dep_UserCad: TIntegerField
    FieldName = 'UserCad'
  end
  object Qr_OS_Mon_Dep_UserAlt: TIntegerField
    FieldName = 'UserAlt'
  end
  object Qr_OS_Mon_Dep_AlterWeb: TSmallintField
    FieldName = 'AlterWeb'
  end
  object Qr_OS_Mon_Dep_Ativo: TSmallintField
    FieldName = 'Ativo'
  end
  object Qr_OS_Mon_Dep_SIGLA: TWideStringField
    FieldName = 'SIGLA'
    Size = 1
  end
  object Qr_OS_Mon_Dep_NO_Campo: TWideStringField
    FieldName = 'NO_Campo'
    Size = 100
  end
  object Qr_OS_Mon_Dep_IDIts: TIntegerField
    FieldName = 'IDIts'
  end
end
object Ds_OS_Mon_Dep_: TDataSource
  DataSet = Qr_OS_Mon_Dep_
  Left = 108
  Top = 388
end
*)

end.
