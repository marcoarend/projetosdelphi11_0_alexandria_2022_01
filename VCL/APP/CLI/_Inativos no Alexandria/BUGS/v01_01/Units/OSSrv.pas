unit OSSrv;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnInternalConsts,
  dmkImage, Mask, dmkRadioGroup, UnBugs_Tabs, dmkMemo, UnDmkEnums;

type
  TFmOSSrv = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    Panel5: TPanel;
    QrOSCab: TmySQLQuery;
    QrOSCabCodigo: TIntegerField;
    QrOSCabEntidade: TIntegerField;
    QrOSCabEstatus: TIntegerField;
    QrOSCabFatoGeradr: TIntegerField;
    QrOSCabDtaContat: TDateTimeField;
    QrOSCabDtaVisPrv: TDateTimeField;
    QrOSCabDtaVisExe: TDateTimeField;
    QrOSCabDtaExePrv: TDateTimeField;
    QrOSCabDtaExeIni: TDateTimeField;
    QrOSCabDtaExeFim: TDateTimeField;
    QrOSCabLk: TIntegerField;
    QrOSCabDataCad: TDateField;
    QrOSCabDataAlt: TDateField;
    QrOSCabUserCad: TIntegerField;
    QrOSCabUserAlt: TIntegerField;
    QrOSCabAlterWeb: TSmallintField;
    QrOSCabAtivo: TSmallintField;
    QrOSCabNO_FatoGeradr: TWideStringField;
    QrOSCabNO_ESTATUS: TWideStringField;
    QrOSCabNO_ENT: TWideStringField;
    QrOSCabSiapTerCad: TIntegerField;
    QrOSCabNO_SiapTerCad: TWideStringField;
    QrOSCabTXTVisPrv: TWideStringField;
    QrOSCabTXTContat: TWideStringField;
    QrOSCabTXTVisExe: TWideStringField;
    QrOSCabTXTExePrv: TWideStringField;
    QrOSCabTXTExeIni: TWideStringField;
    QrOSCabTXTExeFim: TWideStringField;
    DsOSCab: TDataSource;
    GBDados: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit15: TDBEdit;
    QrDesServico: TmySQLQuery;
    DsDesServico: TDataSource;
    QrDesServicoCodigo: TIntegerField;
    QrDesServicoNome: TWideStringField;
    Label2: TLabel;
    EdDesServico: TdmkEditCB;
    CBDesServico: TdmkDBLookupComboBox;
    EdControle: TdmkEdit;
    Label1: TLabel;
    CkContinuar: TCheckBox;
    Label25: TLabel;
    EdGarantiaDd: TdmkEdit;
    EdHrEvacuar: TdmkEdit;
    Label26: TLabel;
    Label27: TLabel;
    EdHrExecutar: TdmkEdit;
    Label6: TLabel;
    EdValCalc: TdmkEdit;
    EdValInfo: TdmkEdit;
    Label8: TLabel;
    Label10: TLabel;
    EdValDesc: TdmkEdit;
    EdValTota: TdmkEdit;
    Label11: TLabel;
    RGAutorizado: TdmkRadioGroup;
    MeDetalhes: TdmkMemo;
    GBMonitoramento: TGroupBox;
    Label12: TLabel;
    EdMoniDdTotl: TdmkEdit;
    RGTudoFeitoM: TdmkRadioGroup;
    EdMoniDdIntv: TdmkEdit;
    Label13: TLabel;
    SpeedButton6: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdValCalcChange(Sender: TObject);
    procedure EdValInfoChange(Sender: TObject);
    procedure EdValDescChange(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FQrInsUpd: TmySQLQuery;
    FConfirmou, FCriou: Boolean;
    //
    procedure CalculaTotal();
  end;

  var
  FmOSSrv: TFmOSSrv;

implementation

uses UnMyObjects, Module, UMySQLModule, UnOSApp_PF, DmkDAC_PF, Principal;

{$R *.DFM}

procedure TFmOSSrv.BtOKClick(Sender: TObject);
var
  GarantiaDd, HrEvacuar, Codigo, Controle, DesServico,
  Autorizado, MoniDdTotl, MoniDdIntv, TudoFeitoM: Integer;
  HrExecutar, ValCalc, ValInfo, ValDesc, ValTota: Double;
  Detalhes: String;
  SemQtd: Boolean;
begin
  Codigo     := QrOSCabCodigo.Value;
  Controle   := EdControle.ValueVariant;
  DesServico := EdDesServico.ValueVariant;
  GarantiaDd := EdGarantiaDd.ValueVariant;
  HrEvacuar  := EdHrEvacuar.ValueVariant;
  HrExecutar := EdHrExecutar.ValueVariant;
  //
  ValCalc := EdValCalc.ValueVariant;
  ValInfo := EdValInfo.ValueVariant;
  ValDesc := EdValDesc.ValueVariant;
  ValTota := EdValTota.ValueVariant;
  //
  Autorizado := RGAutorizado.ItemIndex;
  Detalhes   := MeDetalhes.Text;
  //
  MoniDdTotl   := EdMoniDdTotl.ValueVariant;
  MoniDdIntv   := EdMoniDdIntv.ValueVariant;
  //
  TudoFeitoM := RGTudoFeitoM.ItemIndex;
  //
  if MyObjects.FIC(DesServico = 0, EdDesServico, 'Informe o servi�o!') then
    Exit;
  //
  SemQtd := (MoniDdTotl <> 0) and (MoniDdIntv = 0);
  if MyObjects.FIC(SemQtd, EdDesServico,
  'Informe o intervalo do monitoramento!') then
    Exit;
  //
  Controle := UMyMod.BPGS1I32('ossrv', 'Controle', '', '',
    tsDef, ImgTipo.SQLType, Controle);
  // Necessita para retorno ao fechar a janela!
  EdControle.ValueVariant := Controle;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'ossrv', False,
    ['Codigo', 'DesServico', 'GarantiaDd',
    'HrEvacuar', 'HrExecutar', 'ValCalc',
    'ValInfo', 'ValDesc', 'ValTota',
    'Autorizado', 'Detalhes',
    'MoniDdTotl', 'MoniDdIntv',
    'TudoFeitoM'],
    ['Controle'],
    [Codigo, DesServico, GarantiaDd,
    HrEvacuar, HrExecutar, ValCalc,
    ValInfo, ValDesc, ValTota,
    Autorizado, Detalhes,
    MoniDdTotl, MoniDdIntv,
    TudoFeitoM],
    [Controle], True) then
  begin
    FConfirmou := True;
    //DmModOS.TotalValorOS(Codigo, gboServi);
    OSApp_PF.TotalValorOS(Codigo);
    if ImgTipo.SQLType = stUpd then
      OSApp_PF.VeSeCriaOSsFuturas(Codigo);
    if FQrInsUpd <> nil then
    begin
      UnDmkDAC_PF.AbreQuery(FQrInsUpd, Dmod.MyDB);
      FQrInsUpd.Locate('Controle', Controle, []);
    end;
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType := stIns;
      EdControle.ValueVariant := 0;
      EdDesServico.ValueVariant := 0;
      CBDesServico.KeyValue := 0;
      EdGarantiaDd.ValueVariant := 0;
      EdHrEvacuar.ValueVariant := 0;
      EdHrExecutar.ValueVariant := 0;
      EdValDesc.ValueVariant := 0; // Deve ser antes para n�o dar aviso de valor negativo
      EdValCalc.ValueVariant := 0;
      EdValInfo.ValueVariant := 0;
      EdValTota.ValueVariant := 0;
      RGAutorizado.ItemIndex := 0;
    end else
      Close;
  end;
end;

procedure TFmOSSrv.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSSrv.CalculaTotal();
var
  Calc, Info, Desc, Tota: Double;
begin
  if FCriou = False then
    Exit;
  //
  Calc := EdValCalc.ValueVariant;
  Info := EdValInfo.ValueVariant;
  Desc := EdValDesc.ValueVariant;
  Tota := OSApp_PF.TotalValorServico(Calc, Info, Desc);
  EdValTota.ValueVariant := Tota;
end;

procedure TFmOSSrv.EdValCalcChange(Sender: TObject);
begin
  CalculaTotal();
end;

procedure TFmOSSrv.EdValDescChange(Sender: TObject);
begin
  CalculaTotal();
end;

procedure TFmOSSrv.EdValInfoChange(Sender: TObject);
begin
  CalculaTotal();
end;

procedure TFmOSSrv.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSSrv.FormCreate(Sender: TObject);
begin
  FConfirmou := False;
  FCriou := False;
  UnDMkDAC_PF.AbreQuery(QrDesServico, Dmod.MyDB);
  //
  //N�o usa
  GBMonitoramento.Visible := False;
end;

procedure TFmOSSrv.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSSrv.SpeedButton6Click(Sender: TObject);
var
  DesServico: Integer;
begin
  VAR_CADASTRO := 0;
  DesServico   := EdDesServico.ValueVariant;

  FmPrincipal.MostraServicoDesServico(DesServico);

  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdDesServico, CBDesServico, QrDesServico, VAR_CADASTRO);

    EdDesServico.SetFocus;
  end;
end;

(*
object LaOSOrigem: TLabel
  Left = 468
  Top = 60
  Width = 54
  Height = 13
  Caption = 'OS Origem:'
  Color = clBtnFace
  Enabled = False
  ParentColor = False
end
object DBEdit9: TDBEdit
  Left = 524
  Top = 56
  Width = 56
  Height = 21
  TabStop = False
  DataField = 'OSOrigem'
  DataSource = DsOSCab
  TabOrder = 9
end

*)
end.
