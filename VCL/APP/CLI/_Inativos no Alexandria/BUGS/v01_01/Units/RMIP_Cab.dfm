object FmRMIP_Cab: TFmRMIP_Cab
  Left = 339
  Top = 185
  Caption = 'IMP-ReMIP-001 :: RMIP'
  ClientHeight = 976
  ClientWidth = 1241
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 1182
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 1123
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 82
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'RMIP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 82
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'RMIP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 82
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'RMIP'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 802
    Width = 1241
    Height = 88
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel10: TPanel
      Left = 2
      Top = 18
      Width = 1236
      Height = 67
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object ProgressBar1: TProgressBar
        Left = 0
        Top = 46
        Width = 1236
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 890
    Width = 1241
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 2
    object Panel12: TPanel
      Left = 2
      Top = 18
      Width = 1236
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label3: TLabel
        Left = 784
        Top = 0
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tempo:'
      end
      object PnSaiDesis: TPanel
        Left = 1113
        Top = 0
        Width = 123
        Height = 65
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 3
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 5
        Top = 4
        Width = 111
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object EdTempo: TdmkEdit
        Left = 784
        Top = 17
        Width = 74
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfLong
        HoraFormat = dmkhfLong
        Texto = '00:00:00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object BtEnviar: TBitBtn
        Tag = 553
        Left = 123
        Top = 4
        Width = 111
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Enviar'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtEnviarClick
      end
      object BtConfigura: TBitBtn
        Tag = 256
        Left = 241
        Top = 4
        Width = 111
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Config.'
        NumGlyphs = 2
        TabOrder = 4
        OnClick = BtConfiguraClick
      end
      object Button1: TButton
        Left = 591
        Top = 15
        Width = 92
        Height = 31
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Button1'
        TabOrder = 5
        OnClick = Button1Click
      end
      object Button2: TButton
        Left = 684
        Top = 15
        Width = 93
        Height = 31
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Button2'
        TabOrder = 6
        OnClick = Button2Click
      end
      object EdWebRes: TdmkEdit
        Left = 862
        Top = 17
        Width = 237
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInt64
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CkDoLocalize: TCheckBox
        Left = 369
        Top = 20
        Width = 134
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Localizar imagens.'
        Checked = True
        State = cbChecked
        TabOrder = 8
      end
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 59
    Width = 1241
    Height = 743
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 3
    object TabSheet1: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'RMIP'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 1233
        Height = 712
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        ExplicitWidth = 1231
        ExplicitHeight = 705
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 1233
          Height = 712
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitWidth = 1231
          ExplicitHeight = 705
          object Panel4: TPanel
            Left = 0
            Top = 0
            Width = 1233
            Height = 607
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            ExplicitWidth = 1231
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 1233
              Height = 607
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              ExplicitWidth = 1231
              object Panel5: TPanel
                Left = 0
                Top = 0
                Width = 684
                Height = 607
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 0
                object Panel8: TPanel
                  Left = 0
                  Top = 0
                  Width = 684
                  Height = 415
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label11: TLabel
                    Left = 15
                    Top = 236
                    Width = 122
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Parecer do relat'#243'rio:'
                  end
                  object Label4: TLabel
                    Left = 15
                    Top = 286
                    Width = 166
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Respons'#225'vel pelo relat'#243'rio:'
                  end
                  object Panel2: TPanel
                    Left = 0
                    Top = 0
                    Width = 684
                    Height = 55
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label1: TLabel
                      Left = 15
                      Top = 5
                      Width = 58
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Empresa:'
                    end
                    object EdEmpresa: TdmkEditCB
                      Left = 15
                      Top = 25
                      Width = 54
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInt64
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      OnChange = EdEmpresaChange
                      OnExit = EdEmpresaExit
                      DBLookupComboBox = CBEmpresa
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBEmpresa: TdmkDBLookupComboBox
                      Left = 69
                      Top = 25
                      Width = 607
                      Height = 24
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      KeyField = 'Filial'
                      ListField = 'NOMEFILIAL'
                      ListSource = DModG.DsEmpresas
                      TabOrder = 1
                      dmkEditCB = EdEmpresa
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                  object Panel14: TPanel
                    Left = 0
                    Top = 55
                    Width = 684
                    Height = 56
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 1
                    object Label7: TLabel
                      Left = 15
                      Top = 5
                      Width = 44
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Cliente:'
                    end
                    object EdCliente: TdmkEditCB
                      Left = 15
                      Top = 25
                      Width = 54
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInt64
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      OnChange = EdClienteChange
                      OnExit = EdClienteExit
                      DBLookupComboBox = CBCliente
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBCliente: TdmkDBLookupComboBox
                      Left = 69
                      Top = 25
                      Width = 607
                      Height = 24
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      KeyField = 'Codigo'
                      ListField = 'NO_ENT'
                      ListSource = DsClientes
                      TabOrder = 1
                      dmkEditCB = EdCliente
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                  object Panel15: TPanel
                    Left = 0
                    Top = 111
                    Width = 684
                    Height = 67
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 2
                    object Label2: TLabel
                      Left = 522
                      Top = 15
                      Width = 118
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Data de impress'#227'o:'
                    end
                    object GroupBox4: TGroupBox
                      Left = 0
                      Top = 0
                      Width = 512
                      Height = 67
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alLeft
                      Caption = ' Per'#237'odo da pesquisa: '
                      TabOrder = 0
                      ExplicitHeight = 68
                      object Panel16: TPanel
                        Left = 2
                        Top = 18
                        Width = 508
                        Height = 47
                        Margins.Left = 4
                        Margins.Top = 4
                        Margins.Right = 4
                        Margins.Bottom = 4
                        Align = alClient
                        BevelOuter = bvNone
                        TabOrder = 0
                        object Label8: TLabel
                          Left = 11
                          Top = 15
                          Width = 69
                          Height = 16
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Caption = 'Data inicial:'
                        end
                        object Label9: TLabel
                          Left = 238
                          Top = 15
                          Width = 59
                          Height = 16
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Caption = 'Data final:'
                        end
                        object TPIni: TdmkEditDateTimePicker
                          Left = 94
                          Top = 10
                          Width = 137
                          Height = 24
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Date = 41704.706008634260000000
                          Time = 41704.706008634260000000
                          TabOrder = 0
                          ReadOnly = False
                          DefaultEditMask = '!99/99/99;1;_'
                          AutoApplyEditMask = True
                          UpdType = utYes
                          DatePurpose = dmkdpNone
                        end
                        object TPFim: TdmkEditDateTimePicker
                          Left = 320
                          Top = 10
                          Width = 138
                          Height = 24
                          Margins.Left = 4
                          Margins.Top = 4
                          Margins.Right = 4
                          Margins.Bottom = 4
                          Date = 41704.706008634260000000
                          Time = 41704.706008634260000000
                          TabOrder = 1
                          ReadOnly = False
                          DefaultEditMask = '!99/99/99;1;_'
                          AutoApplyEditMask = True
                          UpdType = utYes
                          DatePurpose = dmkdpNone
                        end
                      end
                    end
                    object TPData: TdmkEditDateTimePicker
                      Left = 522
                      Top = 34
                      Width = 138
                      Height = 24
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Date = 40375.706008634260000000
                      Time = 40375.706008634260000000
                      TabOrder = 1
                      ReadOnly = False
                      DefaultEditMask = '!99/99/99;1;_'
                      AutoApplyEditMask = True
                      UpdType = utYes
                      DatePurpose = dmkdpNone
                    end
                  end
                  object Panel17: TPanel
                    Left = 0
                    Top = 178
                    Width = 684
                    Height = 60
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 3
                    object Label10: TLabel
                      Left = 15
                      Top = 5
                      Width = 113
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'T'#237'tulo do Relat'#243'rio:'
                    end
                    object EdTitulo: TEdit
                      Left = 15
                      Top = 25
                      Width = 661
                      Height = 21
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      TabOrder = 0
                      Text = 'RMIP'
                    end
                  end
                  object EdParecer: TdmkEditCB
                    Left = 15
                    Top = 256
                    Width = 54
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    TabOrder = 4
                    FormatType = dmktfInt64
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBParecer
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBParecer: TdmkDBLookupComboBox
                    Left = 69
                    Top = 256
                    Width = 607
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    KeyField = 'Codigo'
                    ListField = 'Nome'
                    ListSource = DsPareceres
                    TabOrder = 5
                    dmkEditCB = EdParecer
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object CkPaginar: TdmkCheckBox
                    Left = 20
                    Top = 347
                    Width = 129
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Paginar as folhas.'
                    Checked = True
                    State = cbChecked
                    TabOrder = 6
                    UpdType = utYes
                    ValCheck = #0
                    ValUncheck = #0
                    OldValor = #0
                  end
                  object EdEntiRespon: TdmkEditCB
                    Left = 15
                    Top = 305
                    Width = 54
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    TabOrder = 7
                    FormatType = dmktfInt64
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBEntiRespon
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBEntiRespon: TdmkDBLookupComboBox
                    Left = 69
                    Top = 305
                    Width = 607
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    KeyField = 'Controle'
                    ListField = 'Nome'
                    ListSource = DsResponsaveis
                    TabOrder = 8
                    dmkEditCB = EdEntiRespon
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                  object CkPizzaUltima: TdmkCheckBox
                    Left = 158
                    Top = 347
                    Width = 237
                    Height = 21
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Pizza s'#243' da '#250'lima revis'#227'o dos PMVs.'
                    TabOrder = 9
                    UpdType = utYes
                    ValCheck = #0
                    ValUncheck = #0
                    OldValor = #0
                  end
                end
                object Panel6: TPanel
                  Left = 0
                  Top = 415
                  Width = 684
                  Height = 192
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  BevelOuter = bvLowered
                  TabOrder = 1
                  object PnSub0: TPanel
                    Left = 1
                    Top = 1
                    Width = 682
                    Height = 20
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 0
                    object LaSub0B: TLabel
                      Left = 1
                      Top = 2
                      Width = 15
                      Height = 19
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = '...'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clSilver
                      Font.Height = -17
                      Font.Name = 'Arial'
                      Font.Style = []
                      ParentFont = False
                      Transparent = True
                    end
                    object LaSub0A: TLabel
                      Left = 0
                      Top = 1
                      Width = 15
                      Height = 19
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = '...'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clRed
                      Font.Height = -17
                      Font.Name = 'Arial'
                      Font.Style = []
                      ParentFont = False
                      Transparent = True
                    end
                  end
                  object PnSub1: TPanel
                    Left = 1
                    Top = 21
                    Width = 682
                    Height = 42
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 1
                    object LaSub1A: TLabel
                      Left = 1
                      Top = 2
                      Width = 15
                      Height = 19
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = '...'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clSilver
                      Font.Height = -17
                      Font.Name = 'Arial'
                      Font.Style = []
                      ParentFont = False
                      Transparent = True
                    end
                    object LaSub1B: TLabel
                      Left = 0
                      Top = 1
                      Width = 15
                      Height = 19
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = '...'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clRed
                      Font.Height = -17
                      Font.Name = 'Arial'
                      Font.Style = []
                      ParentFont = False
                      Transparent = True
                    end
                    object PBSub1: TProgressBar
                      Left = 0
                      Top = 21
                      Width = 682
                      Height = 21
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alBottom
                      TabOrder = 0
                    end
                  end
                  object PnSub2: TPanel
                    Left = 1
                    Top = 63
                    Width = 682
                    Height = 42
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 2
                    object LaSub2B: TLabel
                      Left = 1
                      Top = 2
                      Width = 15
                      Height = 19
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = '...'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clSilver
                      Font.Height = -17
                      Font.Name = 'Arial'
                      Font.Style = []
                      ParentFont = False
                      Transparent = True
                    end
                    object LaSub2A: TLabel
                      Left = 0
                      Top = 1
                      Width = 15
                      Height = 19
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = '...'
                      Font.Charset = DEFAULT_CHARSET
                      Font.Color = clRed
                      Font.Height = -17
                      Font.Name = 'Arial'
                      Font.Style = []
                      ParentFont = False
                      Transparent = True
                    end
                    object PBSub2: TProgressBar
                      Left = 0
                      Top = 21
                      Width = 682
                      Height = 21
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alBottom
                      TabOrder = 0
                    end
                  end
                end
              end
              object Panel9: TPanel
                Left = 684
                Top = 0
                Width = 549
                Height = 607
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                ExplicitWidth = 547
                object CGResumido: TdmkCheckGroup
                  Left = 0
                  Top = 410
                  Width = 549
                  Height = 197
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alBottom
                  Caption = ' Itens a serem impressos: '
                  Items.Strings = (
                    'Demonstrativo de receitas no per'#237'odo'
                    'Demonstrativo de despesas no per'#237'odo'
                    'Inadimpl'#234'ncia de unidades'
                    'Resumo do movimento nas contas correntes'
                    'Saldos das contas controladas'
                    'Pend'#234'ncias de mensalidades pr'#233'-estipuladas'
                    'Resumo geral de saldos')
                  TabOrder = 0
                  Visible = False
                  UpdType = utYes
                  Value = 0
                  OldValor = 0
                  ExplicitWidth = 546
                end
                object PnListaCompl: TPanel
                  Left = 0
                  Top = 0
                  Width = 549
                  Height = 410
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  ExplicitWidth = 546
                  object Panel11: TPanel
                    Left = 0
                    Top = 0
                    Width = 549
                    Height = 75
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 0
                    ExplicitWidth = 546
                    object RGOpcoesListas: TRadioGroup
                      Left = 0
                      Top = 0
                      Width = 208
                      Height = 75
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Align = alLeft
                      Caption = ' Op'#231#245'es de itens de relat'#243'rios: '
                      ItemIndex = 0
                      Items.Strings = (
                        'Pr'#233'-cadastrados'
                        'Todos')
                      TabOrder = 0
                      OnClick = RGOpcoesListasClick
                    end
                    object BtOrdem: TBitBtn
                      Left = 441
                      Top = 17
                      Width = 110
                      Height = 49
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = '&Muda ordem'
                      Enabled = False
                      NumGlyphs = 2
                      TabOrder = 1
                      OnClick = BtOrdemClick
                    end
                    object BtTudo: TBitBtn
                      Tag = 127
                      Left = 214
                      Top = 17
                      Width = 111
                      Height = 49
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = '&Todos'
                      NumGlyphs = 2
                      TabOrder = 2
                      OnClick = BtTudoClick
                    end
                    object BtNenhum: TBitBtn
                      Tag = 128
                      Left = 327
                      Top = 17
                      Width = 111
                      Height = 49
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = '&Nenhum'
                      NumGlyphs = 2
                      TabOrder = 3
                      OnClick = BtNenhumClick
                    end
                  end
                  object DBGLista: TdmkDBGridZTO
                    Left = 0
                    Top = 130
                    Width = 549
                    Height = 280
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    DataSource = DsLista
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                    TabOrder = 1
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -14
                    TitleFont.Name = 'MS Sans Serif'
                    TitleFont.Style = []
                    RowColors = <>
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Ordem'
                        Width = 38
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_REL'
                        Title.Caption = 'Descri'#231#227'o'
                        Width = 328
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ItemRel'
                        Title.Caption = 'ID rel.'
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'Controle'
                        Visible = True
                      end>
                  end
                  object PnCfgCad: TPanel
                    Left = 0
                    Top = 75
                    Width = 549
                    Height = 55
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alTop
                    BevelOuter = bvNone
                    TabOrder = 2
                    ExplicitWidth = 546
                    object Label6: TLabel
                      Left = 0
                      Top = 5
                      Width = 83
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Configura'#231#227'o:'
                    end
                    object EdCfgCab: TdmkEditCB
                      Left = 0
                      Top = 25
                      Width = 54
                      Height = 25
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInt64
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ValMin = '-2147483647'
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                      OnChange = EdCfgCabChange
                      DBLookupComboBox = CBCfgCab
                      IgnoraDBLookupComboBox = False
                      AutoSetIfOnlyOneReg = setregOnlyManual
                    end
                    object CBCfgCab: TdmkDBLookupComboBox
                      Left = 54
                      Top = 25
                      Width = 494
                      Height = 24
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      KeyField = 'Codigo'
                      ListField = 'Nome'
                      ListSource = DsRMIPCfgCad
                      TabOrder = 1
                      dmkEditCB = EdCfgCab
                      UpdType = utYes
                      LocF7SQLMasc = '$#'
                    end
                  end
                end
              end
            end
          end
          object GroupBox8: TGroupBox
            Left = 0
            Top = 607
            Width = 1233
            Height = 105
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Caption = '  Arquivos e/ou diret'#243'rios n'#227'o localizados: '
            TabOrder = 1
            ExplicitWidth = 1231
            ExplicitHeight = 98
            object Memo1: TMemo
              Left = 2
              Top = 18
              Width = 1226
              Height = 82
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Font.Charset = ANSI_CHARSET
              Font.Color = 4196863
              Font.Height = -17
              Font.Name = 'Tahoma'
              Font.Style = [fsBold]
              ParentFont = False
              ReadOnly = True
              ScrollBars = ssVertical
              TabOrder = 0
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Navegador'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object WebBrowser1: TWebBrowser
        Left = 0
        Top = 0
        Width = 1231
        Height = 709
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        OnProgressChange = WebBrowser1ProgressChange
        ControlData = {
          4C0000005A670000883B00000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126209000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
    end
    object Imagem: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Imagem'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Image1: TImage
        Left = 0
        Top = 0
        Width = 1231
        Height = 742
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
      end
    end
  end
  object QrLista: TmySQLQuery
    Database = Dmod.MyDBn
    AfterOpen = QrListaAfterOpen
    BeforeClose = QrListaBeforeClose
    SQL.Strings = (
      'SELECT * '
      'FROM _rmip_'
      'ORDER BY Ordem, Controle')
    Left = 560
    Top = 176
    object QrListaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrListaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrListaItemRel: TIntegerField
      FieldName = 'ItemRel'
    end
    object QrListaOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrListaAtivo: TIntegerField
      FieldName = 'Ativo'
    end
    object QrListaNO_REL: TWideStringField
      FieldName = 'NO_REL'
      Size = 255
    end
  end
  object DsLista: TDataSource
    DataSet = QrLista
    Left = 560
    Top = 224
  end
  object frxIMP_ReMIP_001_001: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39947.906630057870000000
    ReportOptions.LastChange = 39947.906630057870000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxIMP_ReMIP_001_001GetValue
    Left = 124
    Top = 12
    Datasets = <
      item
      end
      item
      end
      item
      end
      item
        DataSet = DModFin.frxDsCreditos
        DataSetName = 'frxDsCreditos'
      end
      item
        DataSet = DModG.frxDsCST
        DataSetName = 'frxDsCST'
      end
      item
      end
      item
      end
      item
        DataSet = DModFin.frxDsDebitos
        DataSetName = 'frxDsDebitos'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
      end
      item
      end
      item
      end
      item
        DataSet = DModFin.frxDsExtratos
        DataSetName = 'frxDsExtratos'
      end
      item
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
      end
      item
        DataSet = DModFin.frxDsPendAll
        DataSetName = 'frxDsPendAll'
      end
      item
      end
      item
      end
      item
        DataSet = DModFin.frxDsPendSum
        DataSetName = 'frxDsPendSum'
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
      end
      item
        DataSet = DModFin.frxDsResumo
        DataSetName = 'frxDsResumo'
      end
      item
        DataSet = DModFin.frxDsSaldoA
        DataSetName = 'frxDsSaldoA'
      end
      item
        DataSet = DModFin.frxDsSaldosNiv
        DataSetName = 'frxDsSaldosNiv'
      end
      item
        DataSet = DModG.frxDsSCE
        DataSetName = 'frxDsSCE'
      end
      item
        DataSet = DModFin.frxDsSdoCtas
        DataSetName = 'frxDsSdoCtas'
      end
      item
        DataSet = DModFin.frxDsSdoExcl
        DataSetName = 'frxDsSdoExcl'
      end
      item
        DataSet = DModFin.frxDsSNG
        DataSetName = 'frxDsSNG'
      end
      item
        DataSet = DModFin.frxDsSTCP
        DataSetName = 'frxDsSTCP'
      end
      item
      end
      item
      end
      item
        DataSet = DModFin.frxDsTotalSaldo
        DataSetName = 'frxDsTotalSaldo'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
    end
  end
  object QrSels: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT COUNT(*) Itens'
      'FROM cfgrel'
      'WHERE Ativo=1')
    Left = 600
    Top = 176
    object QrSelsItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrRMIPCfgCad: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM rmipcfgcad'
      'ORDER BY Nome')
    Left = 656
    Top = 177
    object QrRMIPCfgCadCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRMIPCfgCadNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsRMIPCfgCad: TDataSource
    DataSet = QrRMIPCfgCad
    Left = 656
    Top = 225
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, CONCAT('
      'IF(ent.RazaoSocial <> "", CONCAT(ent.RazaoSocial, " {R} "), ""),'
      'IF(ent.Fantasia <> "", CONCAT(ent.Fantasia, " {F} "), ""),'
      'IF(ent.Nome <> "", CONCAT(ent.Nome, " {N} "), ""),'
      'IF(ent.Apelido <> "", CONCAT(ent.Apelido, " {A} "), "")'
      ') NO_ENT,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_CLIENTE'
      'FROM cunscad cun'
      'LEFT JOIN entidades ent ON ent.Codigo=cun.Codigo'
      'WHERE cun.Codigo <> 0'
      'ORDER BY NO_ENT')
    Left = 728
    Top = 176
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 255
    end
    object QrClientesNO_CLIENTE: TWideStringField
      FieldName = 'NO_CLIENTE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 728
    Top = 224
  end
  object frxChartObject1: TfrxChartObject
    Left = 816
    Top = 336
  end
  object frxIMP_ReMIP_001_R002: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39947.656532893520000000
    ReportOptions.LastChange = 39947.656532893520000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxIMP_ReMIP_001_R002GetValue
    Left = 268
    Top = 56
    Datasets = <
      item
        DataSet = frxDsEntiRespon
        DataSetName = 'frxDsEntiRespon'
      end
      item
        DataSet = frxDsTxtGeneric
        DataSetName = 'frxDsTxtGeneric'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object PageHeader8: TfrxPageHeader
        FillType = ftBrush
        Height = 64.252010000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo128: TfrxMemoView
          Left = 60.472480000000000000
          Top = 37.795300000000000000
          Width = 680.315400000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_RelTitu]')
          ParentFont = False
        end
      end
      object MasterData8: TfrxMasterData
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 143.622140000000000000
        Width = 793.701300000000000000
        RowCount = 1
        Stretched = True
        object Rich1: TfrxRichView
          Left = 56.692950000000010000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          DataField = 'Texto'
          DataSet = frxDsTxtGeneric
          DataSetName = 'frxDsTxtGeneric'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C204D532053616E732053657269663B7D7D0D0A7B5C2A5C67656E
            657261746F722052696368656432302031302E302E31363239397D5C76696577
            6B696E64345C756331200D0A5C706172645C66305C667331365C7061720D0A7D
            0D0A00}
        end
      end
      object MasterData9: TfrxMasterData
        FillType = ftBrush
        Height = 79.370130000000000000
        Top = 188.976500000000000000
        Width = 793.701300000000000000
        DataSet = frxDsEntiRespon
        DataSetName = 'frxDsEntiRespon'
        RowCount = 0
        object Memo136: TfrxMemoView
          Left = 56.692950000000000000
          Top = 49.133890000000010000
          Width = 37.795300000000000000
          Height = 30.236220472440940000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo137: TfrxMemoView
          Left = 94.488250000000000000
          Top = 49.133890000000010000
          Width = 253.228510000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '[frxDsEntiRespon."Nome"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo142: TfrxMemoView
          Left = 347.716760000000000000
          Top = 49.133890000000010000
          Width = 37.795300000000000000
          Height = 30.236220470000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            'Cargo:')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo147: TfrxMemoView
          Left = 385.512060000000000000
          Top = 49.133890000000010000
          Width = 207.874150000000000000
          Height = 30.236220470000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '[frxDsEntiRespon."NOME_CARGO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo150: TfrxMemoView
          Left = 502.677490000000000000
          Top = 30.236240000000010000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Ciente em:                 /                 /               ')
          ParentFont = False
          WordWrap = False
        end
        object Memo1: TfrxMemoView
          Left = 593.386210000000000000
          Top = 49.133890000000010000
          Width = 143.622140000000000000
          Height = 30.236220470000000000
          DataField = 'Observ'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '[frxDsEntiRespon."Observ"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxIMP_ReMIP_001_R001: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39947.621174710650000000
    ReportOptions.LastChange = 41176.779509965280000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '(* Falta fazer! Copiado do CashBal                              ' +
        '                                         '
      '  if <LogoBalanceteExiste> = True then'
      '      Picture1.LoadFromFile(<LogoBalancetePath>);'
      '  //Linha 1                               '
      '  Memo1.Font.Name := <VARF_CpaLin1FonNom>;'
      '  Memo1.Font.Size := <VARF_CpaLin1FonTam>;'
      '  Memo1.Top       := <VARF_CpaLin1MrgSup>;'
      '  Memo1.Height    := <VARF_CpaLin1AltLin>;'
      
        '  Memo1.HAlign    := <VARF_CpaLin1AliTex>;                      ' +
        '                                   '
      '  //Linha 2                                  '
      '  Memo2.Font.Name := <VARF_CpaLin2FonNom>;'
      '  Memo2.Font.Size := <VARF_CpaLin2FonTam>;'
      '  Memo2.Top       := <VARF_CpaLin2MrgSup>;'
      '  Memo2.Height    := <VARF_CpaLin2AltLin>;'
      
        '  Memo2.HAlign    := <VARF_CpaLin2AliTex>;                      ' +
        '                                   '
      '  //Linha 3                                  '
      '  Memo3.Font.Name := <VARF_CpaLin3FonNom>;'
      '  Memo3.Font.Size := <VARF_CpaLin3FonTam>;'
      '  Memo3.Top       := <VARF_CpaLin3MrgSup>;'
      '  Memo3.Height    := <VARF_CpaLin3AltLin>;'
      '  Memo3.HAlign    := <VARF_CpaLin3AliTex>;'
      '  //Logo'
      '  Picture1.Top    := <VARF_CpaImgMrgSup>;'
      
        '  Picture1.Left   := <VARF_CpaImgMrgEsq>;                       ' +
        '       '
      '  Picture1.Height := <VARF_CpaImgAlt>;'
      
        '  Picture1.Width  := <VARF_CpaImgLar>;                          ' +
        '                            '
      '  if <VARF_CpaImgStre> = 0 then'
      '    Picture1.Stretched := False'
      '  else'
      
        '    Picture1.Stretched := True;                                 ' +
        '                                            '
      '  if <VARF_CpaImgProp> = 0 then'
      '    Picture1.KeepAspectRatio := False'
      '  else                  '
      '    Picture1.KeepAspectRatio := True;        '
      '  if <VARF_CpaImgTran> = 0 then'
      '    Picture1.Transparent := False'
      '  else                  '
      '    Picture1.Transparent := True;        '
      
        '  Picture1.TransparentColor := <VARF_CpaImgTranCol>;            ' +
        '                                             '
      '*)'
      'end.')
    OnGetValue = frxIMP_ReMIP_001_R001GetValue
    Left = 268
    Top = 12
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object Memo1: TfrxMemoView
        Align = baCenter
        Left = 18.897649999999970000
        Top = 75.590600000000000000
        Width = 755.906000000000000000
        Height = 113.385900000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -32
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[VARF_NO_CLIENTE]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo3: TfrxMemoView
        Left = 18.897650000000000000
        Top = 944.882500000000000000
        Width = 755.906000000000000000
        Height = 132.283550000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -32
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[VARF_PERIODO_TXT]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo2: TfrxMemoView
        Left = 18.897650000000000000
        Top = 188.976500000000000000
        Width = 755.906000000000000000
        Height = 75.590600000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[VARF_RelTitu]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Picture1: TfrxPictureView
        Left = 56.692950000000010000
        Top = 264.567100000000000000
        Width = 680.315400000000000000
        Height = 680.315400000000000000
        HightQuality = True
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo4: TfrxMemoView
        Left = 18.897650000000000000
        Top = 37.795300000000000000
        Width = 755.906000000000000000
        Height = 37.795300000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[VARF_EMPRESA]')
        ParentFont = False
        VAlign = vaCenter
      end
    end
  end
  object frxReport1: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41811.423735983800000000
    ReportOptions.LastChange = 41811.423735983800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  Picture1.Visible := <VARF_OBTEM_BMP>;  '
      'end.')
    OnGetValue = frxReport1GetValue
    Left = 232
    Top = 276
    Datasets = <>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object Memo1: TfrxMemoView
        Left = 15.118120000000000000
        Top = 15.118120000000000000
        Width = 287.244280000000000000
        Height = 18.897650000000000000
        Memo.UTF8W = (
          'imagem n'#227'o carregada!!')
      end
      object Picture1: TfrxPictureView
        Left = 3.779530000000000000
        Top = 3.779530000000000000
        Width = 710.551640000000000000
        Height = 1035.591220000000000000
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
    end
  end
  object QrTxtGeneric: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM txtgeneric'
      'WHERE Aplicacao & 8')
    Left = 808
    Top = 176
    object QrTxtGenericCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTxtGenericNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrTxtGenericTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrTxtGenericAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
  end
  object frxDsTxtGeneric: TfrxDBDataset
    UserName = 'frxDsTxtGeneric'
    CloseDataSource = False
    DataSet = QrTxtGeneric
    BCDToCurrency = False
    Left = 808
    Top = 224
  end
  object QrResponsaveis: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM txtgeneric'
      'WHERE Aplicacao & 8')
    Left = 896
    Top = 176
    object QrResponsaveisControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrResponsaveisNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsResponsaveis: TDataSource
    DataSet = QrResponsaveis
    Left = 896
    Top = 224
  end
  object QrEntiRespon: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ere.*, eca.Nome NOME_CARGO ,'
      'ELT(ere.Assina+1, "N'#227'o", "Sim", "???") NO_ASSINA,'
      'IF(ent.Tipo=0, ent.ETe1, PTe1) Te1,'
      'IF(ent.Tipo=0, ent.ECel, PCel) Cel,'
      'IF(ent.Tipo=0, ent.EEmail, PEmail) Email'
      'FROM entirespon ere'
      'LEFT JOIN enticargos eca ON eca.Codigo=ere.Cargo'
      'LEFT JOIN entidades ent ON ent.Codigo = ere.Entidade'
      'WHERE ere.Codigo=:P0'
      ''
      ''
      ''
      '')
    Left = 560
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiResponCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEntiResponControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEntiResponNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrEntiResponCargo: TIntegerField
      FieldName = 'Cargo'
      Required = True
    end
    object QrEntiResponAssina: TSmallintField
      FieldName = 'Assina'
      Required = True
    end
    object QrEntiResponOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Required = True
    end
    object QrEntiResponObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrEntiResponNOME_CARGO: TWideStringField
      FieldName = 'NOME_CARGO'
      Size = 30
    end
    object QrEntiResponNO_ASSINA: TWideStringField
      FieldName = 'NO_ASSINA'
      Size = 3
    end
    object QrEntiResponMandatoIni: TDateField
      FieldName = 'MandatoIni'
    end
    object QrEntiResponMandatoFim: TDateField
      FieldName = 'MandatoFim'
    end
    object QrEntiResponMandatoIni_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MandatoIni_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntiResponMandatoFim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MandatoFim_TXT'
      Size = 10
      Calculated = True
    end
    object QrEntiResponTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrEntiResponCel: TWideStringField
      FieldName = 'Cel'
    end
    object QrEntiResponEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrEntiResponEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrEntiResponTe1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Te1_TXT'
      Size = 30
      Calculated = True
    end
    object QrEntiResponCel_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Cel_TXT'
      Size = 30
      Calculated = True
    end
  end
  object frxDsEntiRespon: TfrxDBDataset
    UserName = 'frxDsEntiRespon'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Nome=Nome'
      'Cargo=Cargo'
      'Assina=Assina'
      'OrdemLista=OrdemLista'
      'Observ=Observ'
      'NOME_CARGO=NOME_CARGO'
      'NO_ASSINA=NO_ASSINA'
      'MandatoIni=MandatoIni'
      'MandatoFim=MandatoFim'
      'MandatoIni_TXT=MandatoIni_TXT'
      'MandatoFim_TXT=MandatoFim_TXT'
      'Te1=Te1'
      'Cel=Cel'
      'Email=Email'
      'Entidade=Entidade'
      'Te1_TXT=Te1_TXT'
      'Cel_TXT=Cel_TXT')
    DataSet = QrEntiRespon
    BCDToCurrency = False
    Left = 560
    Top = 328
  end
  object QrPareceres: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM txtgeneric'
      'WHERE Aplicacao & 8')
    Left = 896
    Top = 272
    object QrPareceresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPareceresNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsPareceres: TDataSource
    DataSet = QrPareceres
    Left = 896
    Top = 320
  end
end
