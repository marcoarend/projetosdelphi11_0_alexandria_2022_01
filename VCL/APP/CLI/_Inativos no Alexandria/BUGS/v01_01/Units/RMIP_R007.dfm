object FmRMIP_R007: TFmRMIP_R007
  Left = 0
  Top = 0
  Caption = 'FmRMIP_R007'
  ClientHeight = 168
  ClientWidth = 199
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Qr007OSCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT eco.Nome NO_ENTICONTAT, cab.*,'
      'fge.Nome NO_FatoGeradr,'
      'sta.Nome NO_ESTATUS, stc.Nome NO_SiapTerCad,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Tel_ENT,'
      'IF(pag.Tipo=0, pag.RazaoSocial, pag.Nome) NO_PAG,'
      'IF(ctr.Tipo=0, ctr.RazaoSocial, ctr.Nome) NO_CTR,'
      'car.Nome NO_CART, ppc.Nome NO_PRZ,'
      'tg1.Nome NO_ExeTxtCli1, moc.Nome NO_MobiliCad'
      'FROM oscab cab'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN entidades pag ON pag.Codigo=cab.EntPagante'
      'LEFT JOIN entidades ctr ON ctr.Codigo=cab.EntContrat'
      'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr'
      'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus'
      'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad'
      'LEFT JOIN carteiras car ON car.Codigo=cab.CartEmis'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=cab.CondicaoPg'
      'LEFT JOIN enticontat eco ON eco.Controle=cab.EntiContat'
      'LEFT JOIN txtgeneric tg1 ON tg1.Codigo=cab.ExeTxtCli1'
      'LEFT JOIN mobilicad moc ON moc.Codigo=cab.MobiliCad'
      ''
      'WHERE cab.Codigo>0')
    Left = 32
    Top = 4
    object Qr007OSCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'oscab.Codigo'
    end
    object Qr007OSCabEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'oscab.Entidade'
    end
    object Qr007OSCabEstatus: TIntegerField
      FieldName = 'Estatus'
      Origin = 'oscab.Estatus'
    end
    object Qr007OSCabFatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
      Origin = 'oscab.FatoGeradr'
    end
    object Qr007OSCabDtaContat: TDateTimeField
      FieldName = 'DtaContat'
      Origin = 'oscab.DtaContat'
    end
    object Qr007OSCabDtaVisPrv: TDateTimeField
      FieldName = 'DtaVisPrv'
      Origin = 'oscab.DtaVisPrv'
    end
    object Qr007OSCabDtaVisExe: TDateTimeField
      FieldName = 'DtaVisExe'
      Origin = 'oscab.DtaVisExe'
    end
    object Qr007OSCabDtaExePrv: TDateTimeField
      FieldName = 'DtaExePrv'
      Origin = 'oscab.DtaExePrv'
    end
    object Qr007OSCabDtaExeIni: TDateTimeField
      FieldName = 'DtaExeIni'
      Origin = 'oscab.DtaExeIni'
    end
    object Qr007OSCabDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
      Origin = 'oscab.DtaExeFim'
    end
    object Qr007OSCabLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'oscab.Lk'
    end
    object Qr007OSCabDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'oscab.DataCad'
    end
    object Qr007OSCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'oscab.UserCad'
    end
    object Qr007OSCabDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'oscab.DataAlt'
    end
    object Qr007OSCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'oscab.UserAlt'
    end
    object Qr007OSCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'oscab.AlterWeb'
    end
    object Qr007OSCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'oscab.Ativo'
    end
    object Qr007OSCabNO_FatoGeradr: TWideStringField
      FieldName = 'NO_FatoGeradr'
      Origin = 'fatogeradr.Nome'
    end
    object Qr007OSCabNO_ESTATUS: TWideStringField
      FieldName = 'NO_ESTATUS'
      Origin = 'estatusoss.Nome'
      Size = 60
    end
    object Qr007OSCabNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object Qr007OSCabSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
      Origin = 'oscab.SiapTerCad'
    end
    object Qr007OSCabNO_SiapTerCad: TWideStringField
      FieldName = 'NO_SiapTerCad'
      Origin = 'siaptercad.Nome'
      Size = 100
    end
    object Qr007OSCabTXTVisPrv: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTVisPrv'
      Calculated = True
    end
    object Qr007OSCabTXTContat: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTContat'
      Calculated = True
    end
    object Qr007OSCabTXTVisExe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTVisExe'
      Calculated = True
    end
    object Qr007OSCabTXTExePrv: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTExePrv'
      Calculated = True
    end
    object Qr007OSCabTXTExeIni: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTExeIni'
      Calculated = True
    end
    object Qr007OSCabTXTExeFim: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTExeFim'
      Calculated = True
    end
    object Qr007OSCabDdsPosVda: TIntegerField
      FieldName = 'DdsPosVda'
      Origin = 'oscab.DdsPosVda'
      DisplayFormat = '00'
    end
    object Qr007OSCabEntiContat: TIntegerField
      FieldName = 'EntiContat'
      Origin = 'oscab.EntiContat'
    end
    object Qr007OSCabNumContrat: TIntegerField
      FieldName = 'NumContrat'
      Origin = 'oscab.NumContrat'
    end
    object Qr007OSCabEntPagante: TIntegerField
      FieldName = 'EntPagante'
      Origin = 'oscab.EntPagante'
    end
    object Qr007OSCabEntContrat: TIntegerField
      FieldName = 'EntContrat'
      Origin = 'oscab.EntContrat'
    end
    object Qr007OSCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'oscab.Empresa'
    end
    object Qr007OSCabNO_PAG: TWideStringField
      FieldName = 'NO_PAG'
      Size = 100
    end
    object Qr007OSCabNO_CTR: TWideStringField
      FieldName = 'NO_CTR'
      Size = 100
    end
    object Qr007OSCabDtaLibFat: TDateTimeField
      FieldName = 'DtaLibFat'
      Origin = 'oscab.DtaLibFat'
    end
    object Qr007OSCabDtaFimFat: TDateTimeField
      FieldName = 'DtaFimFat'
      Origin = 'oscab.DtaFimFat'
    end
    object Qr007OSCabCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
      Origin = 'oscab.CondicaoPg'
    end
    object Qr007OSCabCartEmis: TIntegerField
      FieldName = 'CartEmis'
      Origin = 'oscab.CartEmis'
    end
    object Qr007OSCabNO_CART: TWideStringField
      FieldName = 'NO_CART'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object Qr007OSCabNO_PRZ: TWideStringField
      FieldName = 'NO_PRZ'
      Origin = 'pediprzcab.Nome'
      Size = 50
    end
    object Qr007OSCabSerNF: TWideStringField
      FieldName = 'SerNF'
      Origin = 'oscab.SerNF'
      Size = 3
    end
    object Qr007OSCabNumNF: TIntegerField
      FieldName = 'NumNF'
      Origin = 'oscab.NumNF'
    end
    object Qr007OSCabNO_ENTICONTAT: TWideStringField
      FieldName = 'NO_ENTICONTAT'
      Origin = 'enticontat.Nome'
      Size = 30
    end
    object Qr007OSCabTel_ENT: TWideStringField
      FieldName = 'Tel_ENT'
    end
    object Qr007OSCabTXTTel_ENT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTTel_ENT'
      Size = 30
      Calculated = True
    end
    object Qr007OSCabValorServi: TFloatField
      FieldName = 'ValorServi'
      Origin = 'oscab.ValorServi'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr007OSCabValorDesco: TFloatField
      FieldName = 'ValorDesco'
      Origin = 'oscab.ValorDesco'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr007OSCabValorOutrs: TFloatField
      FieldName = 'ValorOutrs'
      Origin = 'oscab.ValorOutrs'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr007OSCabInvalServi: TFloatField
      FieldName = 'InvalServi'
      Origin = 'oscab.InvalServi'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr007OSCabValorTotal: TFloatField
      FieldName = 'ValorTotal'
      Origin = 'oscab.ValorTotal'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr007OSCabInvalDesco: TFloatField
      FieldName = 'InvalDesco'
      Origin = 'oscab.InvalDesco'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr007OSCabInvalOutrs: TFloatField
      FieldName = 'InvalOutrs'
      Origin = 'oscab.InvalOutrs'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr007OSCabInvalTotal: TFloatField
      FieldName = 'InvalTotal'
      Origin = 'oscab.InvalTotal'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr007OSCabOrcamServi: TFloatField
      FieldName = 'OrcamServi'
      Origin = 'oscab.OrcamServi'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr007OSCabOrcamDesco: TFloatField
      FieldName = 'OrcamDesco'
      Origin = 'oscab.OrcamDesco'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr007OSCabOrcamOutrs: TFloatField
      FieldName = 'OrcamOutrs'
      Origin = 'oscab.OrcamOutrs'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr007OSCabOrcamTotal: TFloatField
      FieldName = 'OrcamTotal'
      Origin = 'oscab.OrcamTotal'
      DisplayFormat = '#,###,##0.00'
    end
    object Qr007OSCabValiDdOrca: TIntegerField
      FieldName = 'ValiDdOrca'
      Origin = 'oscab.ValiDdOrca'
    end
    object Qr007OSCabOperacao: TIntegerField
      FieldName = 'Operacao'
      Origin = 'oscab.Operacao'
    end
    object Qr007OSCabExeTxtCli1: TIntegerField
      FieldName = 'ExeTxtCli1'
      Origin = 'oscab.ExeTxtCli1'
    end
    object Qr007OSCabNO_ExeTxtCli1: TWideStringField
      FieldName = 'NO_ExeTxtCli1'
      Size = 100
    end
    object Qr007OSCabExeTxtCli2: TIntegerField
      FieldName = 'ExeTxtCli2'
    end
    object Qr007OSCabNO_ExeTxtCli2: TWideStringField
      FieldName = 'NO_ExeTxtCli2'
      Size = 100
    end
    object Qr007OSCabValorPre: TFloatField
      FieldName = 'ValorPre'
    end
    object Qr007OSCabNO_OPERACAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_OPERACAO'
      Size = 255
      Calculated = True
    end
    object Qr007OSCabObsGaranti: TWideMemoField
      FieldName = 'ObsGaranti'
      BlobType = ftWideMemo
      Size = 4
    end
    object Qr007OSCabObsExecuta: TWideMemoField
      FieldName = 'ObsExecuta'
      BlobType = ftWideMemo
      Size = 4
    end
    object Qr007OSCabFimVisPrv: TDateTimeField
      FieldName = 'FimVisPrv'
    end
    object Qr007OSCabFimVisExe: TDateTimeField
      FieldName = 'FimVisExe'
    end
    object Qr007OSCabFimExePrv: TDateTimeField
      FieldName = 'FimExePrv'
    end
    object Qr007OSCabOptado: TSmallintField
      FieldName = 'Optado'
    end
    object Qr007OSCabGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object Qr007OSCabNumero: TIntegerField
      FieldName = 'Numero'
    end
    object Qr007OSCabOpcao: TIntegerField
      FieldName = 'Opcao'
    end
    object Qr007OSCabAgeEqiCab: TIntegerField
      FieldName = 'AgeEqiCab'
    end
    object Qr007OSCabNO_AgeEqiCab: TWideStringField
      FieldName = 'NO_AgeEqiCab'
      Size = 255
    end
    object Qr007OSCabSohInicial: TSmallintField
      FieldName = 'SohInicial'
    end
    object Qr007OSCabStPipAdPrg: TSmallintField
      FieldName = 'StPipAdPrg'
    end
    object Qr007OSCabHowGerou: TSmallintField
      FieldName = 'HowGerou'
    end
    object Qr007OSCabPosGerou: TSmallintField
      FieldName = 'PosGerou'
    end
    object Qr007OSCabOSFlhUltGe: TIntegerField
      FieldName = 'OSFlhUltGe'
    end
    object Qr007OSCabMulServico: TLargeintField
      FieldName = 'MulServico'
    end
    object Qr007OSCabOSFlhGrCab: TIntegerField
      FieldName = 'OSFlhGrCab'
    end
    object Qr007OSCabOSFlhGrIts: TIntegerField
      FieldName = 'OSFlhGrIts'
    end
    object Qr007OSCabLstCusPrd: TIntegerField
      FieldName = 'LstCusPrd'
    end
    object Qr007OSCabLstUplWeb: TDateTimeField
      FieldName = 'LstUplWeb'
    end
    object Qr007OSCabConta: TIntegerField
      FieldName = 'Conta'
    end
    object Qr007OSCabLCPUsed: TIntegerField
      FieldName = 'LCPUsed'
    end
    object Qr007OSCabDtIniMonGr: TDateField
      FieldName = 'DtIniMonGr'
    end
    object Qr007OSCabMobiliCad: TIntegerField
      FieldName = 'MobiliCad'
    end
    object Qr007OSCabNO_MobiliCad: TWideStringField
      FieldName = 'NO_MobiliCad'
      Size = 100
    end
  end
  object frxDs007OSCab: TfrxDBDataset
    UserName = 'frxDs007OSCab'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Entidade=Entidade'
      'Estatus=Estatus'
      'FatoGeradr=FatoGeradr'
      'DtaContat=DtaContat'
      'DtaVisPrv=DtaVisPrv'
      'DtaVisExe=DtaVisExe'
      'DtaExePrv=DtaExePrv'
      'DtaExeIni=DtaExeIni'
      'DtaExeFim=DtaExeFim'
      'Lk=Lk'
      'DataCad=DataCad'
      'UserCad=UserCad'
      'DataAlt=DataAlt'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'NO_FatoGeradr=NO_FatoGeradr'
      'NO_ESTATUS=NO_ESTATUS'
      'NO_ENT=NO_ENT'
      'SiapTerCad=SiapTerCad'
      'NO_SiapTerCad=NO_SiapTerCad'
      'TXTVisPrv=TXTVisPrv'
      'TXTContat=TXTContat'
      'TXTVisExe=TXTVisExe'
      'TXTExePrv=TXTExePrv'
      'TXTExeIni=TXTExeIni'
      'TXTExeFim=TXTExeFim'
      'DdsPosVda=DdsPosVda'
      'EntiContat=EntiContat'
      'NumContrat=NumContrat'
      'EntPagante=EntPagante'
      'EntContrat=EntContrat'
      'Empresa=Empresa'
      'NO_PAG=NO_PAG'
      'NO_CTR=NO_CTR'
      'DtaLibFat=DtaLibFat'
      'DtaFimFat=DtaFimFat'
      'CondicaoPg=CondicaoPg'
      'CartEmis=CartEmis'
      'NO_CART=NO_CART'
      'NO_PRZ=NO_PRZ'
      'SerNF=SerNF'
      'NumNF=NumNF'
      'NO_ENTICONTAT=NO_ENTICONTAT'
      'Tel_ENT=Tel_ENT'
      'TXTTel_ENT=TXTTel_ENT'
      'ValorServi=ValorServi'
      'ValorDesco=ValorDesco'
      'ValorOutrs=ValorOutrs'
      'InvalServi=InvalServi'
      'ValorTotal=ValorTotal'
      'InvalDesco=InvalDesco'
      'InvalOutrs=InvalOutrs'
      'InvalTotal=InvalTotal'
      'OrcamServi=OrcamServi'
      'OrcamDesco=OrcamDesco'
      'OrcamOutrs=OrcamOutrs'
      'OrcamTotal=OrcamTotal'
      'ValiDdOrca=ValiDdOrca'
      'Operacao=Operacao'
      'ExeTxtCli1=ExeTxtCli1'
      'NO_ExeTxtCli1=NO_ExeTxtCli1'
      'ExeTxtCli2=ExeTxtCli2'
      'NO_ExeTxtCli2=NO_ExeTxtCli2'
      'ValorPre=ValorPre'
      'NO_OPERACAO=NO_OPERACAO'
      'ObsGaranti=ObsGaranti'
      'ObsExecuta=ObsExecuta'
      'FimVisPrv=FimVisPrv'
      'FimVisExe=FimVisExe'
      'FimExePrv=FimExePrv'
      'Optado=Optado'
      'Grupo=Grupo'
      'Numero=Numero'
      'Opcao=Opcao'
      'AgeEqiCab=AgeEqiCab'
      'NO_AgeEqiCab=NO_AgeEqiCab'
      'SohInicial=SohInicial'
      'StPipAdPrg=StPipAdPrg'
      'HowGerou=HowGerou'
      'PosGerou=PosGerou'
      'OSFlhUltGe=OSFlhUltGe'
      'MulServico=MulServico'
      'OSFlhGrCab=OSFlhGrCab'
      'OSFlhGrIts=OSFlhGrIts'
      'LstCusPrd=LstCusPrd'
      'LstUplWeb=LstUplWeb'
      'Conta=Conta'
      'LCPUsed=LCPUsed'
      'DtIniMonGr=DtIniMonGr'
      'MobiliCad=MobiliCad'
      'NO_MobiliCad=NO_MobiliCad')
    DataSet = Qr007OSCab
    BCDToCurrency = False
    Left = 32
    Top = 52
  end
  object frxReport007A: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 38101.979964398100000000
    ReportOptions.LastChange = 41410.866535844900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxReport007AGetValue
    Left = 32
    Top = 100
    Datasets = <
      item
        DataSet = frxDs007OSCab
        DataSetName = 'frxDs007OSCab'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      Columns = 1
      ColumnWidth = 190.000000000000000000
      ColumnPositions.Strings = (
        '0')
      object DadosMestre1: TfrxMasterData
        Height = 75.590600000000000000
        Top = 200.315090000000000000
        Width = 680.315400000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDs007OSCab
        DataSetName = 'frxDs007OSCab'
        RowCount = 0
        Stretched = True
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Shape = skRoundRectangle
        end
        object Memo1: TfrxMemoView
          Width = 71.811026060000000000
          Height = 20.787401570000000000
          ShowHint = False
          DataField = 'DtaExeIni'
          DataSet = frxDs007OSCab
          DataSetName = 'frxDs007OSCab'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs007OSCab."DtaExeIni"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo2: TfrxMemoView
          Left = 71.811021180000000000
          Width = 71.811026060000000000
          Height = 20.787401570000000000
          ShowHint = False
          DataField = 'DtaExeFim'
          DataSet = frxDs007OSCab
          DataSetName = 'frxDs007OSCab'
          DisplayFormat.FormatStr = 'dd/mm/yy hh:mm'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs007OSCab."DtaExeFim"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 143.622042360000000000
          Width = 268.346454250000000000
          Height = 20.787401570000000000
          ShowHint = False
          DataField = 'NO_SiapTerCad'
          DataSet = frxDs007OSCab
          DataSetName = 'frxDs007OSCab'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs007OSCab."NO_SiapTerCad"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
        object Memo4: TfrxMemoView
          Left = 0.000048820000000000
          Top = 20.787404020000000000
          Width = 226.771738980000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Execu'#231#227'o')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 226.881928820000000000
          Top = 20.787404020000000000
          Width = 453.543538980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DataField = 'NO_FatoGeradr'
          DataSet = frxDs007OSCab
          DataSetName = 'frxDs007OSCab'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs007OSCab."NO_FatoGeradr"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 3.779578820000000000
          Top = 37.795278029999990000
          Width = 672.756278980000000000
          Height = 37.795278030000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[VARF_DESCRI_ATIVI]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 411.968818820000000000
          Width = 268.346454250000000000
          Height = 20.787401570000000000
          ShowHint = False
          DataField = 'NO_ENT'
          DataSet = frxDs007OSCab
          DataSetName = 'frxDs007OSCab'
          DisplayFormat.DecimalSeparator = '.'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDs007OSCab."NO_ENT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaBottom
        end
      end
      object PageHeader4: TfrxPageHeader
        Height = 120.944884330000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_EMPRESA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 105.826840000000000000
          Top = 18.897650000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_DATA]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 574.488560000000000000
          Top = 18.897650000000000000
          Width = 98.267716540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Top = 64.252010000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo: [VARF_PERIODO]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Top = 83.149660000000000000
          Width = 71.811026060000000000
          Height = 17.007874020000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'In'#237'cio')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 71.811021180000000000
          Top = 83.149660000000000000
          Width = 71.811026060000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'T'#233'rmino')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 143.622042360000000000
          Top = 83.149660000000000000
          Width = 268.346454250000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Local')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Top = 100.157480310000000000
          Width = 226.771738980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Atividade')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 226.881928820000000000
          Top = 100.157480310000000000
          Width = 453.543538980000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Pauta')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 411.968770000000000000
          Top = 83.149660000000000000
          Width = 268.346454250000000000
          Height = 17.007874020000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = '.'
          DisplayFormat.FormatStr = '#,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Terceiro')
          ParentFont = False
        end
      end
      object PageFooter2: TfrxPageFooter
        Height = 15.118110240000000000
        Top = 362.834880000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CODI_FRX]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        Height = 3.779530000000000000
        Top = 298.582870000000000000
        Width = 680.315400000000000000
        RowCount = 1
      end
    end
  end
end
