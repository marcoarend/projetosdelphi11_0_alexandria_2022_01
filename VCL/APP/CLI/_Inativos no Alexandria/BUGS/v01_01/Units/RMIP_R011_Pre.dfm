object FmRMIP_R011_Pre: TFmRMIP_R011_Pre
  Left = 339
  Top = 185
  Caption = 'MIP-Relat-011 :: NCTs'
  ClientHeight = 629
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 912
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 65
        Height = 32
        Caption = 'NCTs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 65
        Height = 32
        Caption = 'NCTs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 65
        Height = 32
        Caption = 'NCTs'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 467
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 467
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 467
        Align = alClient
        TabOrder = 0
        object Panel37: TPanel
          Left = 2
          Top = 15
          Width = 1004
          Height = 450
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Panel39: TPanel
            Left = 1
            Top = 1
            Width = 1002
            Height = 448
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 0
            object ImgFoto: TImage
              Left = 808
              Top = 0
              Width = 194
              Height = 287
              Align = alClient
              Proportional = True
              Stretch = True
              ExplicitLeft = 369
              ExplicitTop = 8
              ExplicitWidth = 633
              ExplicitHeight = 244
            end
            object Splitter4: TSplitter
              Left = 805
              Top = 0
              Height = 287
              ExplicitLeft = 632
              ExplicitTop = 92
              ExplicitHeight = 100
            end
            object PG_NCT_CAC: TPageControl
              Left = 0
              Top = 287
              Width = 1002
              Height = 161
              ActivePage = TabSheet39
              Align = alBottom
              TabOrder = 0
              object TabSheet29: TTabSheet
                Caption = 'Texto livre'
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 798
                ExplicitHeight = 0
                object DBMemo2: TDBMemo
                  Left = 0
                  Top = 0
                  Width = 994
                  Height = 133
                  Align = alClient
                  DataField = 'Observ'
                  DataSource = DsCunsImgCab
                  ScrollBars = ssVertical
                  TabOrder = 0
                  ExplicitWidth = 798
                end
              end
              object TabSheet39: TTabSheet
                Caption = 'N.C.T.'
                ImageIndex = 1
                ExplicitLeft = 0
                ExplicitTop = 0
                ExplicitWidth = 0
                ExplicitHeight = 0
                object Panel40: TPanel
                  Left = 0
                  Top = 0
                  Width = 994
                  Height = 133
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Label32: TLabel
                    Left = 4
                    Top = 4
                    Width = 173
                    Height = 13
                    Caption = 'N.C.T. (N'#227'o Conformidade T'#233'cnica):'
                  end
                  object Label34: TLabel
                    Left = 4
                    Top = 64
                    Width = 42
                    Height = 13
                    Caption = 'C.A.C. ():'
                  end
                  object DBMemo3: TDBMemo
                    Left = 4
                    Top = 20
                    Width = 613
                    Height = 45
                    DataField = 'NO_NCT'
                    DataSource = DsCunsImgCab
                    ScrollBars = ssVertical
                    TabOrder = 0
                  end
                  object DBMemo4: TDBMemo
                    Left = 4
                    Top = 80
                    Width = 613
                    Height = 45
                    DataField = 'TxtCAC'
                    DataSource = DsCunsImgCab
                    ScrollBars = ssVertical
                    TabOrder = 1
                  end
                end
              end
            end
            object DBGCunsImgCab: TdmkDBGridZTO
              Left = 0
              Top = 0
              Width = 805
              Height = 287
              Align = alLeft
              DataSource = DsCunsImgCab
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect, dgTitleClick, dgTitleHotTrack]
              TabOrder = 1
              TitleFont.Charset = ANSI_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              RowColors = <>
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NO_STC'
                  Title.Caption = 'Lugar'
                  Width = 200
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Ordem'
                  Width = 36
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 170
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_DEPENDENCIA'
                  Title.Caption = 'Local -> Depend'#234'ncia'
                  Width = 206
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_STATUS'
                  Title.Caption = 'Status'
                  Width = 128
                  Visible = True
                end>
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 515
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 559
    Width = 1008
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 862
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 860
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrCunsImgCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT dep.Nome NO_DEPENDENCIA, cis.Nome NO_STATUS, '
      'nct.Nome NO_NCT, nct.TxtCAC, cic.* '
      'FROM cunsimgcab cic '
      'LEFT JOIN cunsimgsit cis ON cis.Codigo=cic.Status '
      'LEFT JOIN siapimadep sid ON sid.Controle=cic.Dependencia'
      'LEFT JOIN dependenci dep ON dep.Codigo=sid.Dependenci'
      'LEFT JOIN cunsimgnct nct ON nct.Codigo=cic.CodiNCT'
      'WHERE cic.Codigo>0 ')
    Left = 72
    Top = 124
    object QrCunsImgCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCunsImgCabControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCunsImgCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCunsImgCabStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrCunsImgCabCaminho: TWideStringField
      FieldName = 'Caminho'
      Size = 255
    end
    object QrCunsImgCabNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrCunsImgCabDependencia: TIntegerField
      FieldName = 'Dependencia'
    end
    object QrCunsImgCabNO_DEPENDENCIA: TWideStringField
      FieldName = 'NO_DEPENDENCIA'
      Size = 60
    end
    object QrCunsImgCabCodiNCT: TIntegerField
      FieldName = 'CodiNCT'
    end
    object QrCunsImgCabObserv: TWideMemoField
      FieldName = 'Observ'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCunsImgCabAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrCunsImgCabNO_NCT: TWideStringField
      FieldName = 'NO_NCT'
      Size = 255
    end
    object QrCunsImgCabTxtCAC: TWideStringField
      FieldName = 'TxtCAC'
      Size = 255
    end
    object QrCunsImgCabOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrCunsImgCabNO_STC: TWideStringField
      FieldName = 'NO_STC'
      Size = 100
    end
  end
  object DsCunsImgCab: TDataSource
    DataSet = QrCunsImgCab
    Left = 100
    Top = 124
  end
  object frxReport011A: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 41383.503409583300000000
    ReportOptions.LastChange = 41383.503409583300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure Picture1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <ImgNCTExiste> = True then'
      '    Picture1.LoadFromFile(<ImgNCTPath>);  '
      'end;'
      ''
      'begin'
      
        '  MeVARF_CODI_FRX.Text := Copy(Report.Name, 4, Length(Report.Nam' +
        'e));'
      
        '  MeSloganFooter1.Text := <SloganFooter>;                       ' +
        '            '
      '  //'
      '  if <LogoTitRExiste> = True then'
      '  begin              '
      '    PictureTitR1.LoadFromFile(<LogoTitRPath>);'
      '    //PictureTitR2.LoadFromFile(<LogoTitRPath>);'
      '  end;                  '
      '  //'
      '  if <LogoTitLExiste> = True then'
      '  begin              '
      '    PictureTitL1.LoadFromFile(<LogoTitLPath>);'
      '   // PictureTitL2.LoadFromFile(<LogoTitLPath>);'
      '  end;'
      'end.')
    Left = 32
    Top = 232
    Datasets = <
      item
        DataSet = frxDsCIC_Imp
        DataSetName = 'frxDsCIC_Imp'
      end
      item
      end
      item
        DataSet = Dmod.frxDsOpcoesBugs
        DataSetName = 'frxDsOpcoesBugs'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        Height = 230.551330000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape3: TfrxShapeView
          Width = 680.315400000000000000
          Height = 151.181200000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo46: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Line_PH_01: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo47: TfrxMemoView
          Left = 151.181200000000000000
          Top = 132.283550000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 18.897650000000000000
          Top = 132.283550000000000000
          Width = 132.283486540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy hh:nn:ss'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Now]')
          ParentFont = False
          VAlign = vaCenter
        end
        object MePagina: TfrxMemoView
          Left = 529.134200000000000000
          Top = 132.283550000000000000
          Width = 132.283486540000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Top = 181.417440000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_ENTIDADE]')
          ParentFont = False
        end
        object PictureTitR1: TfrxPictureView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 151.181102360000000000
          Height = 113.385826770000000000
          ShowHint = False
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Stretched = False
          HightQuality = True
          Transparent = True
          TransparentColor = clWhite
        end
        object PictureTitL1: TfrxPictureView
          Top = 18.897650000000000000
          Width = 151.181102360000000000
          Height = 113.385826770000000000
          ShowHint = False
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Stretched = False
          HightQuality = True
          Transparent = True
          TransparentColor = clWhite
        end
        object Rich1: TfrxRichView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 113.385826770000000000
          ShowHint = False
          DataField = 'TxtTitRelC'
          DataSet = Dmod.frxDsOpcoesFili
          DataSetName = 'frxDsOpcoesBugs'
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C205461686F6D613B7D7D0D0A7B5C2A5C67656E657261746F7220
            526963686564323020362E322E393230307D5C766965776B696E64345C756331
            200D0A5C706172645C66305C667331365C7061720D0A7D0D0A00}
        end
        object Memo2: TfrxMemoView
          Top = 154.960730000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_TITULO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 68.031530240000000000
        Top = 929.764380000000000000
        Width = 680.315400000000000000
        object Memo93: TfrxMemoView
          Top = 52.913420000000200000
          Width = 676.535870000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
        object MeVARF_CODI_FRX: TfrxMemoView
          Left = 359.055350000000000000
          Top = 52.913420000000200000
          Width = 321.260050000000000000
          Height = 13.228346460000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -8
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'VARF_CODI_FRX')
          ParentFont = False
          VAlign = vaCenter
        end
        object MeSloganFooter1: TfrxMemoView
          Top = 30.236240000000180000
          Width = 676.535870000000000000
          Height = 22.677170240000000000
          OnBeforePrint = 'MeSloganFooter1OnBeforePrint'
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FALTA_SETAR_SLOGAN_FOOTER]!')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Top = 7.559060000000045000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            
              'Legenda: N.C.T. : N'#227'o Conformidade T'#233'cnica - C.A.C. : Comunicado' +
              ' de A'#231#227'o Corretiva.')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 559.370415590000000000
        Top = 309.921460000000000000
        Width = 680.315400000000000000
        DataSet = frxDsCIC_Imp
        DataSetName = 'frxDsCIC_Imp'
        RowCount = 0
        Stretched = True
        object Memo1: TfrxMemoView
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_FC_NCT] N'#186' [frxDsCIC_Imp."Ordem"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Foto: [frxDsCIC_Imp."Nome"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 37.795300000000000000
          Top = 37.795300000000000000
          Width = 604.724800000000000000
          Height = 340.157700000000000000
          OnBeforePrint = 'Picture1OnBeforePrint'
          ShowHint = False
          Center = True
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo4: TfrxMemoView
          Top = 385.512060000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente: [VARF_ENTIDADE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Top = 423.307360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              'Local: [frxDsCIC_Imp."NO_LOCAL"] : [frxDsCIC_Imp."NO_DEPENDENCIA' +
              '"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Top = 442.205010000000000000
          Width = 680.315400000000000000
          Height = 22.677155590000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_DADOS_REL]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Top = 404.409710000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Lugar: [frxDsCIC_Imp."NO_STC"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrCIC_Imp: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT dep.Nome NO_DEPENDENCIA, cis.Nome NO_STATUS,  '
      'nct.Nome NO_NCT, nct.TxtCAC, sic.SCompl2 NO_LOCAL, cic.*  '
      'FROM _cuns_img_cab cic  '
      'LEFT JOIN bugstrol.cunsimgsit cis ON cis.Codigo=cic.Status  '
      
        'LEFT JOIN bugstrol.siapimadep sid ON sid.Controle=cic.Dependenci' +
        'a '
      'LEFT JOIN bugstrol.dependenci dep ON dep.Codigo=sid.Dependenci '
      'LEFT JOIN bugstrol.siapimacad sic ON sic.Codigo=sid.Codigo'
      'LEFT JOIN bugstrol.cunsimgnct nct ON nct.Codigo=cic.CodiNCT '
      'WHERE cic.Codigo>0  ')
    Left = 60
    Top = 232
    object QrCIC_ImpCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCIC_ImpControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCIC_ImpNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCIC_ImpStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrCIC_ImpCaminho: TWideStringField
      FieldName = 'Caminho'
      Size = 255
    end
    object QrCIC_ImpNO_STATUS: TWideStringField
      FieldName = 'NO_STATUS'
      Size = 60
    end
    object QrCIC_ImpDependencia: TIntegerField
      FieldName = 'Dependencia'
    end
    object QrCIC_ImpNO_DEPENDENCIA: TWideStringField
      FieldName = 'NO_DEPENDENCIA'
      Size = 60
    end
    object QrCIC_ImpCodiNCT: TIntegerField
      FieldName = 'CodiNCT'
    end
    object QrCIC_ImpObserv: TWideMemoField
      FieldName = 'Observ'
      Required = True
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCIC_ImpAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrCIC_ImpNO_NCT: TWideStringField
      FieldName = 'NO_NCT'
      Size = 255
    end
    object QrCIC_ImpTxtCAC: TWideStringField
      FieldName = 'TxtCAC'
      Size = 255
    end
    object QrCIC_ImpNO_LOCAL: TWideStringField
      FieldName = 'NO_LOCAL'
      Size = 100
    end
    object QrCIC_ImpOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrCIC_ImpNO_STC: TWideStringField
      FieldName = 'NO_STC'
      Size = 100
    end
  end
  object frxDsCIC_Imp: TfrxDBDataset
    UserName = 'frxDsCIC_Imp'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Nome=Nome'
      'Status=Status'
      'Caminho=Caminho'
      'NO_STATUS=NO_STATUS'
      'Dependencia=Dependencia'
      'NO_DEPENDENCIA=NO_DEPENDENCIA'
      'CodiNCT=CodiNCT'
      'Observ=Observ'
      'Aplicacao=Aplicacao'
      'NO_NCT=NO_NCT'
      'TxtCAC=TxtCAC'
      'NO_LOCAL=NO_LOCAL'
      'Ordem=Ordem'
      'NO_STC=NO_STC')
    DataSet = QrCIC_Imp
    BCDToCurrency = False
    Left = 88
    Top = 232
  end
  object QrMixRel: TmySQLQuery
    Database = Dmod.ZZDB
    SQL.Strings = (
      'SELECT SUM(IF(Aplicacao=0,1,0)) FC,'
      'SUM(IF(Aplicacao=1,1,0)) NCT'
      'FROM _cuns_img_cab')
    Left = 116
    Top = 232
    object QrMixRelFC: TFloatField
      FieldName = 'FC'
    end
    object QrMixRelNCT: TFloatField
      FieldName = 'NCT'
    end
  end
end
