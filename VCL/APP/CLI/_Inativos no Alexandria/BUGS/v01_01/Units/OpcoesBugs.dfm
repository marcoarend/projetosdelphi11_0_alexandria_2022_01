object FmOpcoesBugs: TFmOpcoesBugs
  Left = 339
  Top = 185
  Caption = 'FER-OPCAO-002 :: Op'#231#245'es Espec'#237'ficas do Aplicativo'
  ClientHeight = 832
  ClientWidth = 977
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 977
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 918
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 859
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 476
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Op'#231#245'es Espec'#237'ficas do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 476
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Op'#231#245'es Espec'#237'ficas do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 476
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Op'#231#245'es Espec'#237'ficas do Aplicativo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 977
    Height = 633
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 977
      Height = 633
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      ActivePage = TabSheet8
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Grade de Produtos '
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 969
          Height = 602
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 969
            Height = 346
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            TabOrder = 0
            object GBEquipamentos: TGroupBox
              Left = 2
              Top = 18
              Width = 480
              Height = 326
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              Caption = ' Filtros de EQUIPAMENTOS: '
              TabOrder = 0
              object GroupBox2: TGroupBox
                Left = 2
                Top = 18
                Width = 476
                Height = 148
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Caption = ' Filtro de equipamentos de APLICA'#199#195'O:  '
                TabOrder = 0
                object RGGraNivEqAp: TdmkRadioGroup
                  Left = 2
                  Top = 18
                  Width = 472
                  Height = 71
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  Caption = ' Nivel: '
                  Columns = 4
                  ItemIndex = 0
                  Items.Strings = (
                    'Sem filtro'
                    'Nivel 1'
                    'Nivel 2'
                    'Nivel 3'
                    'Nivel 4'
                    'Nivel 5'
                    'Tipo')
                  TabOrder = 0
                  OnClick = RGGraNivEqApClick
                  QryCampo = 'GraNivEqAp'
                  UpdCampo = 'GraNivEqAp'
                  UpdType = utYes
                  OldValor = 0
                end
                object Panel5: TPanel
                  Left = 2
                  Top = 89
                  Width = 472
                  Height = 57
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label1: TLabel
                    Left = 10
                    Top = 5
                    Width = 81
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Item do N'#237'vel:'
                  end
                  object EdGraCodEqAp: TdmkEditCB
                    Left = 10
                    Top = 25
                    Width = 69
                    Height = 25
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryCampo = 'GraCodEqAp'
                    UpdCampo = 'GraCodEqAp'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBGraCodEqAp
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBGraCodEqAp: TdmkDBLookupComboBox
                    Left = 79
                    Top = 25
                    Width = 379
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    KeyField = 'CODNIV'
                    ListField = 'Nome'
                    ListSource = DsEqAp
                    TabOrder = 1
                    dmkEditCB = EdGraCodEqAp
                    QryCampo = 'GraCodEqAp'
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
              end
              object GroupBox3: TGroupBox
                Left = 2
                Top = 166
                Width = 476
                Height = 148
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Caption = ' Filtro de equipamentos de MONITORAMENTO: '
                TabOrder = 1
                object RGGraNivEqMo: TdmkRadioGroup
                  Left = 2
                  Top = 18
                  Width = 472
                  Height = 71
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  Caption = ' Nivel: '
                  Columns = 4
                  ItemIndex = 0
                  Items.Strings = (
                    'Sem filtro'
                    'Nivel 1'
                    'Nivel 2'
                    'Nivel 3'
                    'Nivel 4'
                    'Nivel 5'
                    'Tipo')
                  TabOrder = 0
                  OnClick = RGGraNivEqMoClick
                  QryCampo = 'GraNivEqMo'
                  UpdCampo = 'GraNivEqMo'
                  UpdType = utYes
                  OldValor = 0
                end
                object Panel6: TPanel
                  Left = 2
                  Top = 89
                  Width = 472
                  Height = 57
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label2: TLabel
                    Left = 10
                    Top = 5
                    Width = 81
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Item do N'#237'vel:'
                  end
                  object EdGraCodEqMo: TdmkEditCB
                    Left = 10
                    Top = 25
                    Width = 69
                    Height = 25
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryCampo = 'GraCodEqMo'
                    UpdCampo = 'GraCodEqMo'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBGraCodEqMo
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBGraCodEqMo: TdmkDBLookupComboBox
                    Left = 79
                    Top = 25
                    Width = 379
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    KeyField = 'CODNIV'
                    ListField = 'Nome'
                    ListSource = DsEqMo
                    TabOrder = 1
                    dmkEditCB = EdGraCodEqMo
                    QryCampo = 'GraCodEqMo'
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
              end
            end
            object GBProdutos: TGroupBox
              Left = 482
              Top = 18
              Width = 485
              Height = 326
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Caption = ' Filtros de PRODUTOS: '
              TabOrder = 1
              object GroupBox5: TGroupBox
                Left = 2
                Top = 18
                Width = 481
                Height = 148
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Caption = ' Filtro de produtos de APLICA'#199#195'O:  '
                TabOrder = 0
                object RGGraNivPrAp: TdmkRadioGroup
                  Left = 2
                  Top = 18
                  Width = 477
                  Height = 71
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  Caption = ' Nivel: '
                  Columns = 4
                  ItemIndex = 0
                  Items.Strings = (
                    'Sem filtro'
                    'Nivel 1'
                    'Nivel 2'
                    'Nivel 3'
                    'Nivel 4'
                    'Nivel 5'
                    'Tipo')
                  TabOrder = 0
                  OnClick = RGGraNivPrApClick
                  QryCampo = 'GraNivPrAp'
                  UpdCampo = 'GraNivPrAp'
                  UpdType = utYes
                  OldValor = 0
                end
                object Panel7: TPanel
                  Left = 2
                  Top = 89
                  Width = 477
                  Height = 57
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label3: TLabel
                    Left = 10
                    Top = 5
                    Width = 81
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Item do N'#237'vel:'
                  end
                  object EdGraCodPrAp: TdmkEditCB
                    Left = 10
                    Top = 25
                    Width = 69
                    Height = 25
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryCampo = 'GraCodPrAp'
                    UpdCampo = 'GraCodPrAp'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBGraCodPrAp
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBGraCodPrAp: TdmkDBLookupComboBox
                    Left = 79
                    Top = 25
                    Width = 379
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    KeyField = 'CODNIV'
                    ListField = 'Nome'
                    ListSource = DsPrAp
                    TabOrder = 1
                    dmkEditCB = EdGraCodPrAp
                    QryCampo = 'GraCodPrAp'
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
              end
              object GroupBox6: TGroupBox
                Left = 2
                Top = 166
                Width = 481
                Height = 148
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Caption = ' Filtro de produtos de MONITORAMENTO: '
                TabOrder = 1
                object RGGraNivPrMo: TdmkRadioGroup
                  Left = 2
                  Top = 18
                  Width = 477
                  Height = 71
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alTop
                  Caption = ' Nivel: '
                  Columns = 4
                  ItemIndex = 0
                  Items.Strings = (
                    'Sem filtro'
                    'Nivel 1'
                    'Nivel 2'
                    'Nivel 3'
                    'Nivel 4'
                    'Nivel 5'
                    'Tipo')
                  TabOrder = 0
                  OnClick = RGGraNivPrMoClick
                  QryCampo = 'GraNivPrMo'
                  UpdCampo = 'GraNivPrMo'
                  UpdType = utYes
                  OldValor = 0
                end
                object Panel8: TPanel
                  Left = 2
                  Top = 89
                  Width = 477
                  Height = 57
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  BevelOuter = bvNone
                  TabOrder = 1
                  object Label4: TLabel
                    Left = 10
                    Top = 5
                    Width = 81
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Item do N'#237'vel:'
                  end
                  object EdGraCodPrMo: TdmkEditCB
                    Left = 10
                    Top = 25
                    Width = 69
                    Height = 25
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Alignment = taRightJustify
                    TabOrder = 0
                    FormatType = dmktfInteger
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ValMin = '-2147483647'
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryCampo = 'GraCodPrMo'
                    UpdCampo = 'GraCodPrMo'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0
                    ValWarn = False
                    DBLookupComboBox = CBGraCodPrMo
                    IgnoraDBLookupComboBox = False
                    AutoSetIfOnlyOneReg = setregOnlyManual
                  end
                  object CBGraCodPrMo: TdmkDBLookupComboBox
                    Left = 79
                    Top = 25
                    Width = 379
                    Height = 24
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    KeyField = 'CODNIV'
                    ListField = 'Nome'
                    ListSource = DsPrMo
                    TabOrder = 1
                    dmkEditCB = EdGraCodPrMo
                    QryCampo = 'GraCodPrMo'
                    UpdType = utYes
                    LocF7SQLMasc = '$#'
                  end
                end
              end
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Taxonomia'
        ImageIndex = 1
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 969
          Height = 169
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object GroupBox4: TGroupBox
            Left = 0
            Top = 0
            Width = 969
            Height = 169
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Caption = ' Fonte das imagens das esp'#233'cies: '
            TabOrder = 0
            object Panel10: TPanel
              Left = 2
              Top = 18
              Width = 965
              Height = 149
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label5: TLabel
                Left = 177
                Top = 5
                Width = 247
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Diret'#243'rio raiz das imagens das esp'#233'cies: '
              end
              object Label6: TLabel
                Left = 10
                Top = 5
                Width = 160
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'IP do servidor de imagens:'
              end
              object SbImgSDir: TSpeedButton
                Left = 911
                Top = 25
                Width = 26
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SbImgSDirClick
              end
              object EdImgSDir: TdmkEdit
                Left = 177
                Top = 25
                Width = 730
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'ImgSDir'
                UpdCampo = 'ImgSDir'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdImgIPv4: TdmkEdit
                Left = 10
                Top = 25
                Width = 154
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'ImgIPv4'
                UpdCampo = 'ImgIPv4'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Relat'#243'rios 1'
        ImageIndex = 2
        object Panel12: TPanel
          Left = 0
          Top = 65
          Width = 969
          Height = 537
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object GroupBox7: TGroupBox
            Left = 0
            Top = 0
            Width = 969
            Height = 80
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = ' Reposit'#243'rio dos contratos em Microsof Word e relat'#243'rios *.fr3: '
            TabOrder = 0
            object Panel13: TPanel
              Left = 2
              Top = 18
              Width = 965
              Height = 60
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label8: TLabel
                Left = 177
                Top = 5
                Width = 81
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Diret'#243'rio raiz: '
              end
              object Label9: TLabel
                Left = 10
                Top = 5
                Width = 86
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'IP do servidor:'
              end
              object SbContrtSDir: TSpeedButton
                Left = 916
                Top = 25
                Width = 26
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SbContrtSDirClick
              end
              object EdContrtSDir: TdmkEdit
                Left = 177
                Top = 25
                Width = 735
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'ContrtSDir'
                UpdCampo = 'ContrtSDir'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdContrtIPv4: TdmkEdit
                Left = 10
                Top = 25
                Width = 154
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'ContrtIPv4'
                UpdCampo = 'ContrtIPv4'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object GroupBox9: TGroupBox
            Left = 0
            Top = 80
            Width = 969
            Height = 80
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = ' Reposit'#243'rio das fotos de caixas d'#39#225'gua: '
            TabOrder = 1
            object Panel15: TPanel
              Left = 2
              Top = 18
              Width = 965
              Height = 60
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label13: TLabel
                Left = 177
                Top = 5
                Width = 81
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Diret'#243'rio raiz: '
              end
              object Label14: TLabel
                Left = 10
                Top = 5
                Width = 86
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'IP do servidor:'
              end
              object SbCxaSDir: TSpeedButton
                Left = 916
                Top = 25
                Width = 26
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SbCxaSDirClick
              end
              object EdCxaSDir: TdmkEdit
                Left = 177
                Top = 25
                Width = 735
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'CxaSDir'
                UpdCampo = 'CxaSDir'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCxaIPv4: TdmkEdit
                Left = 10
                Top = 25
                Width = 154
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'CxaIPv4'
                UpdCampo = 'CxaIPv4'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
        end
        object Panel11: TPanel
          Left = 0
          Top = 0
          Width = 969
          Height = 65
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label7: TLabel
            Left = 10
            Top = 10
            Width = 93
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Slogan rodap'#233':'
          end
          object EdSloganFoot: TdmkEdit
            Left = 10
            Top = 30
            Width = 936
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'SloganFoot'
            UpdCampo = 'SloganFoot'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
      object TabSheet4: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Relat'#243'rios 2'
        ImageIndex = 3
        object Panel16: TPanel
          Left = 0
          Top = 0
          Width = 969
          Height = 602
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel20: TPanel
            Left = 0
            Top = 0
            Width = 969
            Height = 119
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox10: TGroupBox
              Left = 15
              Top = 5
              Width = 927
              Height = 80
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = ' Cor de impress'#227'o de produtos na ficha de execu'#231#227'o: '
              TabOrder = 0
              object Label15: TLabel
                Left = 15
                Top = 25
                Width = 74
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Cor do texto:'
              end
              object Label16: TLabel
                Left = 197
                Top = 25
                Width = 79
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Cor do fundo:'
              end
              object CBCorImpPrdTxt: TColorBox
                Left = 15
                Top = 44
                Width = 178
                Height = 22
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
                TabOrder = 0
              end
              object CBCorImpPrdBkg: TColorBox
                Left = 197
                Top = 44
                Width = 178
                Height = 22
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                DefaultColorColor = clWhite
                Selected = clWhite
                Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
                TabOrder = 1
              end
              object CkCorImpPrdBld: TdmkCheckBox
                Left = 384
                Top = 49
                Width = 124
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Fonte em negrito.'
                Checked = True
                Enabled = False
                State = cbChecked
                TabOrder = 2
                UpdType = utYes
                ValCheck = '1'
                ValUncheck = '0'
                OldValor = #0
              end
            end
            object CkInfoImoMov: TdmkCheckBox
              Left = 15
              Top = 94
              Width = 927
              Height = 20
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 
                'Informar (I/M) para im'#243'vel / mov'#237'vel nas impress'#245'es relativas a ' +
                'OSs.'
              TabOrder = 1
              QryCampo = 'InfoImoMov'
              UpdCampo = 'InfoImoMov'
              UpdType = utYes
              ValCheck = '1'
              ValUncheck = '0'
              OldValor = #0
            end
          end
          object CkInfQdr2Dep: TdmkCheckBox
            Left = 15
            Top = 118
            Width = 927
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 
              'Mostrar 2'#186' quadro de locais de controle de pragas no preview do ' +
              'or'#231'amento.'
            TabOrder = 1
            QryCampo = 'InfQdr2Dep'
            UpdCampo = 'InfQdr2Dep'
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
          object CkAgeNomAnts: TdmkCheckBox
            Left = 15
            Top = 143
            Width = 927
            Height = 21
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Mostrar o nome do cliente antes da O.S. na agenda.'
            TabOrder = 2
            QryCampo = 'AgeNomAnts'
            UpdCampo = 'AgeNomAnts'
            UpdType = utYes
            ValCheck = '1'
            ValUncheck = '0'
            OldValor = #0
          end
          object GroupBox14: TGroupBox
            Left = 15
            Top = 172
            Width = 927
            Height = 179
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = ' Cor de impress'#227'o de PMVs X Perguntas de monitoramento: '
            TabOrder = 3
            object Label22: TLabel
              Left = 15
              Top = 30
              Width = 168
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Cor das bordas das c'#233'lulas:'
            end
            object Label23: TLabel
              Left = 15
              Top = 59
              Width = 323
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Cor de fundo das c'#233'lulas descritivas (t'#237'tulos e objetos):'
            end
            object Label24: TLabel
              Left = 15
              Top = 89
              Width = 278
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Cor de fundo das c'#233'lulas responsivas in'#243'cuas:'
            end
            object Label25: TLabel
              Left = 15
              Top = 118
              Width = 436
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 
                'Cor de fundo das c'#233'lulas responsivas das perguntas relativas aos' +
                ' PMVs:'
            end
            object Label26: TLabel
              Left = 15
              Top = 148
              Width = 424
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 
                'Cor de fundo das c'#233'lulas responsivas das perguntas relativas as ' +
                'iscas:'
            end
            object CBCros1BordB: TColorBox
              Left = 443
              Top = 25
              Width = 179
              Height = 22
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
              TabOrder = 0
            end
            object CBCros1Per1B: TColorBox
              Left = 443
              Top = 54
              Width = 179
              Height = 22
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DefaultColorColor = clWhite
              Selected = clWhite
              Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
              TabOrder = 1
            end
            object CBCros1Res1B: TColorBox
              Left = 443
              Top = 84
              Width = 179
              Height = 22
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DefaultColorColor = clWhite
              Selected = clWhite
              Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
              TabOrder = 2
            end
            object CBCros1Res2B: TColorBox
              Left = 443
              Top = 113
              Width = 179
              Height = 22
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DefaultColorColor = clWhite
              Selected = clWhite
              Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
              TabOrder = 3
            end
            object CBCros1Res3B: TColorBox
              Left = 443
              Top = 143
              Width = 179
              Height = 22
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DefaultColorColor = clWhite
              Selected = clWhite
              Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
              TabOrder = 4
            end
            object BtSugestoes: TBitBtn
              Tag = 10126
              Left = 630
              Top = 25
              Width = 148
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Sugest'#245'es'
              NumGlyphs = 2
              TabOrder = 5
              OnClick = BtSugestoesClick
            end
          end
        end
      end
      object TabSheet5: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Miscel'#226'nea'
        ImageIndex = 4
        object Panel17: TPanel
          Left = 0
          Top = 0
          Width = 969
          Height = 602
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox11: TGroupBox
            Left = 0
            Top = 0
            Width = 969
            Height = 80
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = ' Reposit'#243'rio das imagens relativas a entidades: '
            TabOrder = 0
            object Panel18: TPanel
              Left = 2
              Top = 18
              Width = 965
              Height = 60
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object Label17: TLabel
                Left = 177
                Top = 5
                Width = 81
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Diret'#243'rio raiz: '
              end
              object Label18: TLabel
                Left = 10
                Top = 5
                Width = 86
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'IP do servidor:'
              end
              object SbCunsSDir: TSpeedButton
                Left = 916
                Top = 25
                Width = 26
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '...'
                OnClick = SbCunsSDirClick
              end
              object EdCunsSDir: TdmkEdit
                Left = 177
                Top = 25
                Width = 735
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'CunsSDir'
                UpdCampo = 'CunsSDir'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCunsIPv4: TdmkEdit
                Left = 10
                Top = 25
                Width = 154
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                QryCampo = 'CunsIPv4'
                UpdCampo = 'CunsIPv4'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
            end
          end
          object RGIdxBDAnt: TdmkRadioGroup
            Left = 0
            Top = 80
            Width = 969
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = ' Banco de dados anterior (de terceiros): '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'N'#227'o tem'
              'FireBird')
            TabOrder = 1
            QryCampo = 'IdxBDAnt'
            UpdCampo = 'IdxBDAnt'
            UpdType = utYes
            OldValor = 0
          end
          object Panel22: TPanel
            Left = 0
            Top = 139
            Width = 969
            Height = 69
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 2
            object GroupBox12: TGroupBox
              Left = 0
              Top = 0
              Width = 346
              Height = 69
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              Caption = ' A'#231#245'es iniciais do aplicativo: '
              TabOrder = 0
              object Panel19: TPanel
                Left = 2
                Top = 18
                Width = 342
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object CkSWTAgenda: TdmkCheckBox
                  Left = 10
                  Top = 15
                  Width = 331
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Criar janela em aba da agenda ao iniciar aplicativo.'
                  Checked = True
                  State = cbChecked
                  TabOrder = 0
                  QryCampo = 'SWTAgenda'
                  UpdCampo = 'SWTAgenda'
                  UpdType = utYes
                  ValCheck = '1'
                  ValUncheck = '0'
                  OldValor = #0
                end
              end
            end
            object GroupBox16: TGroupBox
              Left = 346
              Top = 0
              Width = 623
              Height = 69
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Caption = ' Janela principal: '
              TabOrder = 1
              object Panel27: TPanel
                Left = 2
                Top = 18
                Width = 619
                Height = 49
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object CkATBAutoExp: TdmkCheckBox
                  Left = 10
                  Top = 15
                  Width = 391
                  Height = 21
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 'Expandir / contrair as  guias de bot'#245'es automaticamente.'
                  Checked = True
                  State = cbChecked
                  TabOrder = 0
                  QryCampo = 'ATBAutoExp'
                  UpdCampo = 'ATBAutoExp'
                  UpdType = utYes
                  ValCheck = '1'
                  ValUncheck = '0'
                  OldValor = #0
                end
              end
            end
          end
          object GroupBox18: TGroupBox
            Left = 0
            Top = 208
            Width = 969
            Height = 129
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = ' Agenda: '
            TabOrder = 3
            object Label41: TLabel
              Left = 15
              Top = 30
              Width = 328
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Cor das bordas dos compromissos sem atrelamemnto:'
            end
            object Label42: TLabel
              Left = 15
              Top = 59
              Width = 314
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Cor das bordas dos compromissos atrelados a OSs:'
            end
            object CBAgeCorAvul: TColorBox
              Left = 443
              Top = 25
              Width = 179
              Height = 22
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Selected = clYellow
              Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
              TabOrder = 0
            end
            object CBAgeCorBgst: TColorBox
              Left = 443
              Top = 54
              Width = 179
              Height = 22
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Selected = clAqua
              Style = [cbStandardColors, cbExtendedColors, cbSystemColors, cbCustomColor, cbPrettyNames, cbCustomColors]
              TabOrder = 1
            end
          end
          object GroupBox20: TGroupBox
            Left = 0
            Top = 337
            Width = 969
            Height = 73
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            Caption = ' Di'#225'rio: '
            TabOrder = 4
            object Panel31: TPanel
              Left = 2
              Top = 18
              Width = 965
              Height = 53
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label39: TLabel
                Left = 10
                Top = 5
                Width = 197
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Assunto padr'#227'o para p'#243's-venda:'
              end
              object Label40: TLabel
                Left = 463
                Top = 4
                Width = 254
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Forma de contato padr'#227'o para p'#243's-venda:'
              end
              object EdAssPosVda: TdmkEditCB
                Left = 10
                Top = 25
                Width = 69
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'AssPosVda'
                UpdCampo = 'AssPosVda'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBAssPosVda
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBAssPosVda: TdmkDBLookupComboBox
                Left = 79
                Top = 25
                Width = 379
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'CodUsu'
                ListField = 'Nome'
                ListSource = DsDiarioAss
                TabOrder = 1
                dmkEditCB = EdAssPosVda
                QryCampo = 'AssPosVda'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdFrmCPosVda: TdmkEditCB
                Left = 463
                Top = 25
                Width = 39
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'FrmCPosVda'
                UpdCampo = 'FrmCPosVda'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBFrmCPosVda
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBFrmCPosVda: TdmkDBLookupComboBox
                Left = 502
                Top = 25
                Width = 233
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsFormContat
                TabOrder = 3
                dmkEditCB = EdFrmCPosVda
                QryCampo = 'FrmCPosVda'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
          object GroupBox19: TGroupBox
            Left = 0
            Top = 410
            Width = 969
            Height = 192
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            Caption = ' SP (Solicita'#231#227'o de provid'#234'ncias): '
            TabOrder = 5
            object Panel36: TPanel
              Left = 2
              Top = 18
              Width = 965
              Height = 172
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label31: TLabel
                Left = 20
                Top = 30
                Width = 180
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Status padr'#227'o ao atrelar a OS:'
              end
              object CkOSPrvStaUsa: TdmkCheckBox
                Left = 20
                Top = 5
                Width = 208
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Mudar status ao atrelar OS.'
                TabOrder = 0
                QryCampo = 'OSPrvStaUsa'
                UpdCampo = 'OSPrvStaUsa'
                UpdType = utYes
                ValCheck = #0
                ValUncheck = #0
                OldValor = #0
              end
              object EdOSPrvStaCod: TdmkEditCB
                Left = 20
                Top = 49
                Width = 69
                Height = 26
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'OSPrvStaCod'
                UpdCampo = 'OSPrvStaCod'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBOSPrvStaCod
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBOSPrvStaCod: TdmkDBLookupComboBox
                Left = 89
                Top = 49
                Width = 277
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOSPrvStatus
                TabOrder = 2
                dmkEditCB = EdOSPrvStaCod
                QryCampo = 'OSPrvStaCod'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
        end
      end
      object TabSheet8: TTabSheet
        Caption = 'OSs'
        ImageIndex = 7
        object Label47: TLabel
          Left = 10
          Top = 8
          Width = 479
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Texto para cliente na impress'#227'o de execu'#231#227'o: (cadastrado em text' +
            'os gen'#233'ricos)'
          Color = clBtnFace
          ParentColor = False
        end
        object Label32: TLabel
          Left = 10
          Top = 57
          Width = 484
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Texto para cliente na impress'#227'o do or'#231'amento: (cadastrado em tex' +
            'tos gen'#233'ricos)'
          Color = clBtnFace
          ParentColor = False
        end
        object SpeedButton1: TSpeedButton
          Left = 914
          Top = 77
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SpeedButton1Click
        end
        object SpeedButton10: TSpeedButton
          Left = 914
          Top = 28
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SpeedButton10Click
        end
        object Label33: TLabel
          Left = 10
          Top = 108
          Width = 484
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Texto para cliente na impress'#227'o do or'#231'amento: (cadastrado em tex' +
            'tos gen'#233'ricos)'
          Color = clBtnFace
          ParentColor = False
        end
        object SpeedButton2: TSpeedButton
          Left = 914
          Top = 128
          Width = 26
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '...'
          OnClick = SpeedButton2Click
        end
        object EdOSPrvETC: TdmkEditCB
          Left = 10
          Top = 28
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'OSPrvETC'
          UpdCampo = 'OSPrvETC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBOSPrvETC
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBOSPrvETC: TdmkDBLookupComboBox
          Left = 78
          Top = 28
          Width = 834
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsExeTxtCli1
          TabOrder = 1
          dmkEditCB = EdOSPrvETC
          QryCampo = 'OSPrvETC'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdOSPrvOTC: TdmkEditCB
          Left = 10
          Top = 77
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'OSPrvOTC'
          UpdCampo = 'OSPrvOTC'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBOSPrvOTC
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBOSPrvOTC: TdmkDBLookupComboBox
          Left = 78
          Top = 77
          Width = 834
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsExeTxtCli2
          TabOrder = 3
          dmkEditCB = EdOSPrvOTC
          QryCampo = 'OSPrvOTC'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdTxtFAES: TdmkEditCB
          Left = 10
          Top = 128
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 4
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          QryCampo = 'TxtFAES'
          UpdCampo = 'TxtFAES'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBTxtFAES
          IgnoraDBLookupComboBox = False
          AutoSetIfOnlyOneReg = setregOnlyManual
        end
        object CBTxtFAES: TdmkDBLookupComboBox
          Left = 78
          Top = 128
          Width = 834
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsTxtFAES
          TabOrder = 5
          dmkEditCB = EdTxtFAES
          QryCampo = 'TxtFAES'
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CkDataGarantiaFaes: TdmkCheckBox
          Left = 10
          Top = 161
          Width = 239
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Mostrar data da garantia na FAES'
          Checked = True
          State = cbChecked
          TabOrder = 6
          UpdType = utYes
          ValCheck = '1'
          ValUncheck = '0'
          OldValor = #0
        end
      end
      object TabSheet6: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'OSs filhas'
        ImageIndex = 5
        object Panel28: TPanel
          Left = 0
          Top = 0
          Width = 969
          Height = 602
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel23: TPanel
            Left = 0
            Top = 0
            Width = 969
            Height = 86
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox15: TGroupBox
              Left = 0
              Top = 0
              Width = 469
              Height = 86
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alLeft
              Caption = ' Transfer'#234'ncia de trabalhos de dias n'#227'o '#250'teis: '
              TabOrder = 0
              object RGDdRotaSab: TdmkRadioGroup
                Left = 2
                Top = 18
                Width = 183
                Height = 66
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' S'#225'bado: '
                Columns = 2
                ItemIndex = 3
                Items.Strings = (
                  'Sexta'
                  'S'#225'bado'
                  'Domingo'
                  'Segunda')
                TabOrder = 0
                QryCampo = 'DdRotaSab'
                UpdCampo = 'DdRotaSab'
                UpdType = utYes
                OldValor = 0
              end
              object RgDdRotaDom: TdmkRadioGroup
                Left = 185
                Top = 18
                Width = 182
                Height = 66
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = ' Domingo: '
                Columns = 2
                ItemIndex = 3
                Items.Strings = (
                  'Sexta'
                  'S'#225'bado'
                  'Domingo'
                  'Segunda')
                TabOrder = 1
                QryCampo = 'DdRotaDom'
                UpdCampo = 'DdRotaDom'
                UpdType = utYes
                OldValor = 0
              end
              object RGDdRotaFer: TdmkRadioGroup
                Left = 367
                Top = 18
                Width = 100
                Height = 66
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                Caption = ' Feriados: '
                ItemIndex = 1
                Items.Strings = (
                  'Adiantar'
                  'Postergar')
                TabOrder = 2
                QryCampo = 'DdRotaFer'
                UpdCampo = 'DdRotaFer'
                UpdType = utYes
                OldValor = 0
              end
            end
            object Panel21: TPanel
              Left = 469
              Top = 0
              Width = 500
              Height = 86
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object GroupBox13: TGroupBox
                Left = 0
                Top = 0
                Width = 500
                Height = 55
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                Caption = ' OSs filhas: '
                TabOrder = 0
                object Panel35: TPanel
                  Left = 2
                  Top = 18
                  Width = 496
                  Height = 35
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object Label19: TLabel
                    Left = 10
                    Top = 7
                    Width = 264
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Intervalo em dias para agrupamento de rota:'
                  end
                  object EdDdRotaFlh: TdmkEdit
                    Left = 281
                    Top = 2
                    Width = 98
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    TabOrder = 0
                    FormatType = dmktfString
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '0'
                    QryCampo = 'DdRotaFlh'
                    UpdCampo = 'DdRotaFlh'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = '0'
                    ValWarn = False
                  end
                end
              end
              object CkAutReatPIP: TdmkCheckBox
                Left = 10
                Top = 59
                Width = 459
                Height = 21
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 
                  'Auto reativa'#231#227'o de PMVs em gera'#231#227'o de perguntas de monitoramento' +
                  '.'
                TabOrder = 1
                QryCampo = 'AutReatPIP'
                UpdCampo = 'AutReatPIP'
                UpdType = utYes
                ValCheck = '1'
                ValUncheck = '0'
                OldValor = #0
              end
            end
          end
          object Panel24: TPanel
            Left = 0
            Top = 86
            Width = 969
            Height = 466
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Panel25: TPanel
              Left = 0
              Top = 0
              Width = 969
              Height = 55
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label20: TLabel
                Left = 10
                Top = 5
                Width = 118
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 'Status inicial da OS:'
              end
              object Label21: TLabel
                Left = 468
                Top = 30
                Width = 430
                Height = 16
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = 
                  'Dias extras de garantia dos servi'#231'os de OS preventiva (soma ao i' +
                  'nterv.):'
              end
              object EdOSPrvSta: TdmkEditCB
                Left = 10
                Top = 25
                Width = 69
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                QryCampo = 'OSPrvSta'
                UpdCampo = 'OSPrvSta'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                DBLookupComboBox = CBOSPrvSta
                IgnoraDBLookupComboBox = False
                AutoSetIfOnlyOneReg = setregOnlyManual
              end
              object CBOSPrvSta: TdmkDBLookupComboBox
                Left = 79
                Top = 25
                Width = 379
                Height = 24
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsEstatusOSs
                TabOrder = 1
                dmkEditCB = EdOSPrvSta
                QryCampo = 'OSPrvSta'
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdOSPrvXtraD: TdmkEdit
                Left = 906
                Top = 25
                Width = 34
                Height = 25
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '5'
                QryCampo = 'OSPrvXtraD'
                UpdCampo = 'OSPrvXtraD'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 5
                ValWarn = False
              end
            end
            object Panel29: TPanel
              Left = 0
              Top = 55
              Width = 969
              Height = 138
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 1
              object RGFlhAgeEqi: TdmkRadioGroup
                Left = 0
                Top = 0
                Width = 118
                Height = 138
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = 'Equipe Agentes: '
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'Deixar vazio'
                  'Pelo Lugar'
                  'OS inicial')
                TabOrder = 0
                QryCampo = 'FlhAgeEqi'
                UpdCampo = 'FlhAgeEqi'
                UpdType = utYes
                OldValor = 0
              end
              object RGFlhEntCtt: TdmkRadioGroup
                Left = 118
                Top = 0
                Width = 118
                Height = 138
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = 'Contato do cliente: '
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'Deixar vazio'
                  'Pelo Lugar'
                  'OS inicial')
                TabOrder = 1
                QryCampo = 'FlhEntCtt'
                UpdCampo = 'FlhEntCtt'
                UpdType = utYes
                OldValor = 0
              end
              object RGFlhNumCtr: TdmkRadioGroup
                Left = 236
                Top = 0
                Width = 118
                Height = 138
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = 'N'#250'm. do contrato:'
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'Deixar vazio'
                  'Pelo Lugar'
                  'OS inicial')
                TabOrder = 2
                QryCampo = 'FlhNumCtr'
                UpdCampo = 'FlhNumCtr'
                UpdType = utYes
                OldValor = 0
              end
              object RGFlhEntCtr: TdmkRadioGroup
                Left = 354
                Top = 0
                Width = 119
                Height = 138
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = 'Contratante:'
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'Deixar vazio'
                  'Pelo Lugar'
                  'OS inicial')
                TabOrder = 3
                QryCampo = 'FlhEntCtr'
                UpdCampo = 'FlhEntCtr'
                UpdType = utYes
                OldValor = 0
              end
              object RGFlhEntPag: TdmkRadioGroup
                Left = 473
                Top = 0
                Width = 118
                Height = 138
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = 'Pagante:'
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'Deixar vazio'
                  'Pelo Lugar'
                  'OS inicial')
                TabOrder = 4
                QryCampo = 'FlhEntPag'
                UpdCampo = 'FlhEntPag'
                UpdType = utYes
                OldValor = 0
              end
              object RGFlhCondPg: TdmkRadioGroup
                Left = 591
                Top = 0
                Width = 118
                Height = 138
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = 'Condi'#231#227'o pagto:'
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'Deixar vazio'
                  'Pelo Lugar'
                  'OS inicial')
                TabOrder = 5
                QryCampo = 'FlhCondPg'
                UpdCampo = 'FlhCondPg'
                UpdType = utYes
                OldValor = 0
              end
              object RGFlhCrtEmi: TdmkRadioGroup
                Left = 709
                Top = 0
                Width = 118
                Height = 138
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = 'Carteira emis'#227'o:'
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'Deixar vazio'
                  'Pelo Lugar'
                  'OS inicial')
                TabOrder = 6
                QryCampo = 'FlhCrtEmi'
                UpdCampo = 'FlhCrtEmi'
                UpdType = utYes
                OldValor = 0
              end
              object RGFlhPrePrg: TdmkRadioGroup
                Left = 827
                Top = 0
                Width = 118
                Height = 138
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alLeft
                Caption = 'Pragas pr'#233'-atend.:'
                ItemIndex = 0
                Items.Strings = (
                  'Indefinido'
                  'Deixar vazio'
                  'Pelo Lugar'
                  'OS inicial')
                TabOrder = 7
                QryCampo = 'FlhPrePrg'
                UpdCampo = 'FlhPrePrg'
                UpdType = utYes
                OldValor = 0
              end
            end
            object GroupBox17: TGroupBox
              Left = 0
              Top = 193
              Width = 969
              Height = 273
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alClient
              Caption = ' OSs: '
              TabOrder = 2
              object Panel44: TPanel
                Left = 2
                Top = 18
                Width = 965
                Height = 57
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alTop
                BevelOuter = bvNone
                TabOrder = 0
                object Label27: TLabel
                  Left = 10
                  Top = 5
                  Width = 421
                  Height = 16
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 
                    'Lista de valores para custo de produtos nas receitas / monitoram' +
                    'entos:'
                end
                object Label43: TLabel
                  Left = 586
                  Top = 20
                  Width = 276
                  Height = 32
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Caption = 
                    'Tempo padr'#227'o de monitoramento em minutos para os clientes sem co' +
                    'nfigura'#231#227'o espec'#237'fica:'
                  WordWrap = True
                end
                object EdLstCusPrd: TdmkEditCB
                  Left = 10
                  Top = 23
                  Width = 65
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 0
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ValMin = '-2147483647'
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '0'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 0
                  ValWarn = False
                  DBLookupComboBox = CBLstCusPrd
                  IgnoraDBLookupComboBox = False
                  AutoSetIfOnlyOneReg = setregOnlyManual
                end
                object CBLstCusPrd: TdmkDBLookupComboBox
                  Left = 79
                  Top = 23
                  Width = 498
                  Height = 24
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  KeyField = 'CodUsu'
                  ListField = 'Nome'
                  ListSource = DsGraCusPrc
                  TabOrder = 1
                  dmkEditCB = EdLstCusPrd
                  UpdType = utYes
                  LocF7SQLMasc = '$#'
                end
                object EdPdrMntsMon: TdmkEdit
                  Left = 901
                  Top = 23
                  Width = 45
                  Height = 26
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Alignment = taRightJustify
                  TabOrder = 2
                  FormatType = dmktfInteger
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = True
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  Texto = '30'
                  QryCampo = 'PdrMntsMon'
                  UpdCampo = 'PdrMntsMon'
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = 30
                  ValWarn = False
                end
              end
              object Panel32: TPanel
                Left = 2
                Top = 75
                Width = 965
                Height = 196
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 1
                object Panel33: TPanel
                  Left = 0
                  Top = 0
                  Width = 223
                  Height = 196
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alLeft
                  BevelOuter = bvNone
                  TabOrder = 0
                  object Label28: TLabel
                    Left = 5
                    Top = 20
                    Width = 197
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'Hor'#225'rio m'#237'nimo de agendamento'
                  end
                  object Label30: TLabel
                    Left = 5
                    Top = 44
                    Width = 81
                    Height = 16
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Caption = 'de execu'#231#227'o:'
                  end
                  object EdMinHAgeExe: TdmkEdit
                    Left = 118
                    Top = 39
                    Width = 85
                    Height = 26
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    TabOrder = 0
                    FormatType = dmktfTime
                    MskType = fmtNone
                    DecimalSize = 0
                    LeftZeros = 0
                    NoEnterToTab = False
                    NoForceUppercase = False
                    ForceNextYear = False
                    DataFormat = dmkdfShort
                    HoraFormat = dmkhfShort
                    Texto = '00:00'
                    QryCampo = 'MinHAgeExe'
                    UpdCampo = 'MinHAgeExe'
                    UpdType = utYes
                    Obrigatorio = False
                    PermiteNulo = False
                    ValueVariant = 0.000000000000000000
                    ValWarn = False
                  end
                end
                object GroupBox21: TGroupBox
                  Left = 223
                  Top = 0
                  Width = 742
                  Height = 196
                  Margins.Left = 4
                  Margins.Top = 4
                  Margins.Right = 4
                  Margins.Bottom = 4
                  Align = alClient
                  Caption = ' Velocidade m'#233'dia de deslocamento (em km/h):'
                  TabOrder = 1
                  object Panel34: TPanel
                    Left = 2
                    Top = 18
                    Width = 738
                    Height = 176
                    Margins.Left = 4
                    Margins.Top = 4
                    Margins.Right = 4
                    Margins.Bottom = 4
                    Align = alClient
                    BevelOuter = bvNone
                    TabOrder = 0
                    object Label44: TLabel
                      Left = 10
                      Top = 0
                      Width = 67
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Muito lento:'
                    end
                    object Label45: TLabel
                      Left = 113
                      Top = 0
                      Width = 36
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Lento:'
                    end
                    object Label46: TLabel
                      Left = 217
                      Top = 0
                      Width = 47
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Normal:'
                    end
                    object Label48: TLabel
                      Left = 320
                      Top = 0
                      Width = 48
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'R'#225'pido:'
                    end
                    object Label49: TLabel
                      Left = 423
                      Top = 0
                      Width = 77
                      Height = 16
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Caption = 'Muito r'#225'pido:'
                    end
                    object EdMediakmh1: TdmkEdit
                      Left = 10
                      Top = 20
                      Width = 98
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Alignment = taRightJustify
                      TabOrder = 0
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'Mediakmh1'
                      UpdCampo = 'Mediakmh1'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object EdMediakmh2: TdmkEdit
                      Left = 113
                      Top = 20
                      Width = 99
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Alignment = taRightJustify
                      TabOrder = 1
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'Mediakmh2'
                      UpdCampo = 'Mediakmh2'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object EdMediakmh3: TdmkEdit
                      Left = 217
                      Top = 20
                      Width = 98
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Alignment = taRightJustify
                      TabOrder = 2
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'Mediakmh3'
                      UpdCampo = 'Mediakmh3'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object EdMediakmh4: TdmkEdit
                      Left = 320
                      Top = 20
                      Width = 98
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Alignment = taRightJustify
                      TabOrder = 3
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'Mediakmh4'
                      UpdCampo = 'Mediakmh4'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                    object EdMediakmh5: TdmkEdit
                      Left = 423
                      Top = 20
                      Width = 99
                      Height = 26
                      Margins.Left = 4
                      Margins.Top = 4
                      Margins.Right = 4
                      Margins.Bottom = 4
                      Alignment = taRightJustify
                      TabOrder = 4
                      FormatType = dmktfInteger
                      MskType = fmtNone
                      DecimalSize = 0
                      LeftZeros = 0
                      NoEnterToTab = False
                      NoForceUppercase = False
                      ForceNextYear = False
                      DataFormat = dmkdfShort
                      HoraFormat = dmkhfShort
                      Texto = '0'
                      QryCampo = 'Mediakmh5'
                      UpdCampo = 'Mediakmh5'
                      UpdType = utYes
                      Obrigatorio = False
                      PermiteNulo = False
                      ValueVariant = 0
                      ValWarn = False
                    end
                  end
                end
              end
            end
          end
          object Panel37: TPanel
            Left = 0
            Top = 552
            Width = 969
            Height = 50
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 2
          end
        end
      end
      object TabSheet7: TTabSheet
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Web'
        ImageIndex = 6
        object Panel30: TPanel
          Left = 0
          Top = 0
          Width = 969
          Height = 602
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label29: TLabel
            Left = 10
            Top = 10
            Width = 322
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'String de seguran'#231'a (deve ser igual a p'#225'gina da web):'
          end
          object EdSecuritStr: TdmkEdit
            Left = 10
            Top = 32
            Width = 536
            Height = 26
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = True
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 692
    Width = 977
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 973
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 746
    Width = 977
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 796
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
    object PnSaiDesis: TPanel
      Left = 798
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object QrEqAp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, Nivel1 CODNIV'
      'FROM gragru1'
      'ORDER BY Nome')
    Left = 628
    Top = 12
    object QrEqApNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrEqApCODNIV: TIntegerField
      FieldName = 'CODNIV'
    end
  end
  object DsEqAp: TDataSource
    DataSet = QrEqAp
    Left = 656
    Top = 12
  end
  object QrEqMo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, Nivel1 CODNIV'
      'FROM gragru1'
      'ORDER BY Nome')
    Left = 572
    Top = 12
    object QrEqMoNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object QrEqMoCODNIV: TIntegerField
      FieldName = 'CODNIV'
    end
  end
  object DsEqMo: TDataSource
    DataSet = QrEqMo
    Left = 600
    Top = 12
  end
  object QrPrAp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, Nivel1 CODNIV'
      'FROM gragru1'
      'ORDER BY Nome')
    Left = 460
    Top = 12
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object IntegerField1: TIntegerField
      FieldName = 'CODNIV'
    end
  end
  object DsPrAp: TDataSource
    DataSet = QrPrAp
    Left = 488
    Top = 12
  end
  object QrPrMo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, Nivel1 CODNIV'
      'FROM gragru1'
      'ORDER BY Nome')
    Left = 516
    Top = 12
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
    object IntegerField2: TIntegerField
      FieldName = 'CODNIV'
    end
  end
  object DsPrMo: TDataSource
    DataSet = QrPrMo
    Left = 544
    Top = 12
  end
  object QrEstatusOSs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM estatusoss'
      'ORDER BY Codigo')
    Left = 684
    Top = 12
    object QrEstatusOSsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEstatusOSsNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsEstatusOSs: TDataSource
    DataSet = QrEstatusOSs
    Left = 712
    Top = 12
  end
  object DsExeTxtCli1: TDataSource
    DataSet = QrExeTxtCli1
    Left = 712
    Top = 56
  end
  object QrExeTxtCli1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM txtgeneric'
      'WHERE Aplicacao & 1'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 684
    Top = 56
    object QrExeTxtCli1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrExeTxtCli1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object PMSugestoes: TPopupMenu
    Left = 652
    Top = 428
    object Brancoepreto1: TMenuItem
      Caption = '&Branco e preto'
      OnClick = Brancoepreto1Click
    end
    object LaranjaAzuleverde1: TMenuItem
      Caption = '&Laranja, Azul e verde'
      OnClick = LaranjaAzuleverde1Click
    end
  end
  object QrGraCusPrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome'
      'FROM gracusprc'
      'ORDER BY Nome')
    Left = 684
    Top = 100
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 712
    Top = 100
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdLstCusPrd
    Panel = Panel44
    QryCampo = 'LstCusPrd'
    UpdCampo = 'LstCusPrd'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 536
    Top = 128
  end
  object QrDiarioAss: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, Aplicacao'
      'FROM diarioass'
      'ORDER BY Nome')
    Left = 684
    Top = 148
    object QrDiarioAssCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrDiarioAssCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrDiarioAssNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrDiarioAssAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
  end
  object DsDiarioAss: TDataSource
    DataSet = QrDiarioAss
    Left = 712
    Top = 148
  end
  object dmkValUsu2: TdmkValUsu
    dmkEditCB = EdAssPosVda
    Panel = Panel2
    QryCampo = 'AssPosVda'
    UpdCampo = 'AssPosVda'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 612
    Top = 128
  end
  object QrFormContat: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM formcontat'
      'ORDER BY Nome')
    Left = 528
    Top = 272
    object QrFormContatCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormContatNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsFormContat: TDataSource
    DataSet = QrFormContat
    Left = 556
    Top = 272
  end
  object QrOSPrvStatus: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM osprvsta'
      'ORDER BY Nome')
    Left = 588
    Top = 56
    object QrOSPrvStatusCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSPrvStatusNome: TWideStringField
      DisplayWidth = 60
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsOSPrvStatus: TDataSource
    DataSet = QrOSPrvStatus
    Left = 616
    Top = 56
  end
  object QrExeTxtCli2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM txtgeneric'
      'WHERE Aplicacao & 2'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 652
    Top = 480
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField3: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsExeTxtCli2: TDataSource
    DataSet = QrExeTxtCli2
    Left = 680
    Top = 480
  end
  object QrTxtFAES: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM txtgeneric'
      'WHERE Aplicacao & 16'
      'AND Ativo=1'
      'ORDER BY Nome')
    Left = 292
    Top = 344
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField4: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsTxtFAES: TDataSource
    DataSet = QrTxtFAES
    Left = 320
    Top = 344
  end
end
