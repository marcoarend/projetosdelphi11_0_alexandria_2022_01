object FmFormulIA: TFmFormulIA
  Left = 339
  Top = 185
  Caption = 'FRM-APLIC-003 :: Abrang'#234'ncia de F'#243'rmula de Aplica'#231#227'o'
  ClientHeight = 673
  ClientWidth = 970
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 970
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 911
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 852
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 540
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Abrang'#234'ncia de F'#243'rmula de Aplica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 540
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Abrang'#234'ncia de F'#243'rmula de Aplica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 540
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Abrang'#234'ncia de F'#243'rmula de Aplica'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 59
    Width = 970
    Height = 474
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 970
      Height = 474
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 0
        Width = 970
        Height = 474
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 0
        object dmkDBGrid1: TdmkDBGrid
          Left = 2
          Top = 98
          Width = 965
          Height = 373
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              ReadOnly = True
              Title.Caption = 'A'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 555
              Visible = True
            end>
          Color = clWindow
          DataSource = Ds_OS_Frm_Abr
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = dmkDBGrid1CellClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Ativo'
              ReadOnly = True
              Title.Caption = 'A'
              Width = 17
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 555
              Visible = True
            end>
        end
        object GBDados: TGroupBox
          Left = 2
          Top = 18
          Width = 965
          Height = 80
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          ParentBackground = False
          TabOrder = 1
          object Panel6: TPanel
            Left = 2
            Top = 18
            Width = 960
            Height = 60
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 0
            object Label10: TLabel
              Left = 10
              Top = 5
              Width = 68
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'ID F'#243'rmula:'
              FocusControl = DBEdit1
            end
            object Label11: TLabel
              Left = 158
              Top = 5
              Width = 65
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Descri'#231#227'o:'
              FocusControl = DBEdit2
            end
            object Label13: TLabel
              Left = 10
              Top = 34
              Width = 83
              Height = 16
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = 'Equipamento:'
              FocusControl = DBEdit4
            end
            object DBEdit1: TDBEdit
              Left = 84
              Top = 0
              Width = 69
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Codigo'
              DataSource = DsFormulas
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 226
              Top = 0
              Width = 725
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'Nome'
              DataSource = DsFormulas
              TabOrder = 1
            end
            object DBEdit4: TDBEdit
              Left = 94
              Top = 30
              Width = 857
              Height = 21
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              DataField = 'NO_EQUIAPLIC'
              DataSource = DsFormulas
              TabOrder = 2
            end
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 533
    Width = 970
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 965
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 587
    Width = 970
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 790
      Top = 18
      Width = 177
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 15
        Top = 4
        Width = 147
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 18
      Width = 788
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 15
        Top = 5
        Width = 147
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object BtTodos: TBitBtn
        Tag = 127
        Left = 166
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Todos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtTodosClick
      end
      object BtNenhum: TBitBtn
        Tag = 128
        Left = 281
        Top = 5
        Width = 110
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Nenhum'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtNenhumClick
      end
      object BitBtn1: TBitBtn
        Tag = 10108
        Left = 394
        Top = 5
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Abrang'#234'ncia'
        NumGlyphs = 2
        TabOrder = 3
        OnClick = BitBtn1Click
      end
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 10
    OnTimer = Timer1Timer
    Left = 332
    Top = 96
  end
  object Qr_OS_Frm_Abr: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * FROM _os_frm_abr')
    Left = 276
    Top = 96
    object Qr_OS_Frm_AbrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object Qr_OS_Frm_AbrNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object Qr_OS_Frm_AbrAtivo: TSmallintField
      FieldName = 'Ativo'
      MaxValue = 1
    end
  end
  object Ds_OS_Frm_Abr: TDataSource
    DataSet = Qr_OS_Frm_Abr
    Left = 304
    Top = 96
  end
  object QrOFA: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT * FROM _os_frm_abr')
    Left = 360
    Top = 96
    object QrOFAAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOFACodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOFANome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object QrFormulas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gg1.Nome NO_EQUIAPLIC, frm.* '
      'FROM formulas frm'
      'LEFT JOIN gragrux ggx ON ggx.Controle = frm.EquipAplic'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'WHERE frm.Codigo=:P0'
      '')
    Left = 8
    Top = 5
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFormulasNO_EQUIAPLIC: TWideStringField
      FieldName = 'NO_EQUIAPLIC'
      Size = 120
    end
    object QrFormulasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrFormulasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrFormulasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrFormulasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrFormulasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrFormulasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrFormulasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrFormulasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrFormulasEquipAplic: TIntegerField
      FieldName = 'EquipAplic'
    end
    object QrFormulasQtdTot: TFloatField
      FieldName = 'QtdTot'
    end
    object QrFormulasQtdQSP: TFloatField
      FieldName = 'QtdQSP'
    end
    object QrFormulasCusTot: TFloatField
      FieldName = 'CusTot'
    end
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 36
    Top = 5
  end
end
