object FmOSRec: TFmOSRec
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-005 :: Ordem de Servi'#231'o - Produtos'
  ClientHeight = 341
  ClientWidth = 597
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 597
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 549
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 501
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 351
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 351
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 351
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 597
    Height = 179
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    ExplicitHeight = 174
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 597
      Height = 179
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      ExplicitHeight = 174
      object GroupBox2: TGroupBox
        Left = 0
        Top = 37
        Width = 597
        Height = 142
        Align = alClient
        TabOrder = 0
        ExplicitHeight = 137
        object Panel5: TPanel
          Left = 2
          Top = 15
          Width = 593
          Height = 125
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          ExplicitHeight = 120
          object Label2: TLabel
            Left = 96
            Top = 4
            Width = 40
            Height = 13
            Caption = 'Produto:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label1: TLabel
            Left = 16
            Top = 4
            Width = 54
            Height = 13
            Caption = 'ID Produto:'
            Color = clBtnFace
            Enabled = False
            ParentColor = False
          end
          object Label3: TLabel
            Left = 96
            Top = 48
            Width = 58
            Height = 13
            Caption = 'Quantidade:'
          end
          object Label6: TLabel
            Left = 180
            Top = 48
            Width = 31
            Height = 13
            Caption = 'Pre'#231'o:'
          end
          object Label8: TLabel
            Left = 264
            Top = 48
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object Label9: TLabel
            Left = 348
            Top = 48
            Width = 49
            Height = 13
            Caption = 'Desconto:'
          end
          object Label10: TLabel
            Left = 432
            Top = 48
            Width = 27
            Height = 13
            Caption = 'Total:'
          end
          object EdGraGruX: TdmkEditCB
            Left = 96
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'GraGruX'
            UpdCampo = 'GraGruX'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBGraGruX
            IgnoraDBLookupComboBox = False
          end
          object CBGraGruX: TdmkDBLookupComboBox
            Left = 152
            Top = 20
            Width = 425
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            TabOrder = 2
            dmkEditCB = EdGraGruX
            QryCampo = 'GraGruX'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CkContinuar: TCheckBox
            Left = 16
            Top = 100
            Width = 213
            Height = 17
            Caption = 'Continuar inserindo ap'#243's a confirma'#231#227'o.'
            TabOrder = 9
          end
          object EdConta: TdmkEdit
            Left = 16
            Top = 20
            Width = 76
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Conta'
            UpdCampo = 'Conta'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdPrvQtd: TdmkEdit
            Left = 96
            Top = 64
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'PrvQtd'
            UpdCampo = 'PrvQtd'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdPrvQtdChange
          end
          object EdPrvPrc: TdmkEdit
            Left = 180
            Top = 64
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            QryCampo = 'PrvPrc'
            UpdCampo = 'PrvPrc'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdPrvPrcChange
          end
          object EdPrvVal: TdmkEdit
            Left = 264
            Top = 64
            Width = 80
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'PrvVal'
            UpdCampo = 'PrvVal'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdPrvValChange
          end
          object CkEhQSP: TdmkCheckBox
            Left = 16
            Top = 64
            Width = 57
            Height = 17
            Caption = #201' QSP.'
            TabOrder = 3
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
          object EdPrvDes: TdmkEdit
            Left = 348
            Top = 64
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'PrvDes'
            UpdCampo = 'PrvDes'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdPrvDesChange
          end
          object EdPrvTot: TdmkEdit
            Left = 432
            Top = 64
            Width = 80
            Height = 21
            TabStop = False
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 8
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            QryCampo = 'PrvTot'
            UpdCampo = 'PrvTot'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
      end
      object GBDados: TGroupBox
        Left = 0
        Top = 0
        Width = 597
        Height = 37
        Align = alTop
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 1
        object Label7: TLabel
          Left = 8
          Top = 12
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label4: TLabel
          Left = 232
          Top = 12
          Width = 39
          Height = 13
          Caption = 'Servi'#231'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label5: TLabel
          Left = 108
          Top = 12
          Width = 53
          Height = 13
          Caption = 'ID Servi'#231'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object DBEdit6: TDBEdit
          Left = 276
          Top = 8
          Width = 32
          Height = 21
          DataField = 'DesServico'
          DataSource = DsOSSrv
          TabOrder = 1
        end
        object DBEdit5: TDBEdit
          Left = 308
          Top = 8
          Width = 272
          Height = 21
          DataField = 'NO_DesServico'
          DataSource = DsOSSrv
          TabOrder = 2
        end
        object DBEdit8: TDBEdit
          Left = 164
          Top = 8
          Width = 64
          Height = 21
          DataField = 'Controle'
          DataSource = DsOSSrv
          TabOrder = 3
        end
        object DBEdit15: TDBEdit
          Left = 48
          Top = 8
          Width = 56
          Height = 21
          DataField = 'Codigo'
          DataSource = DsOSSrv
          TabOrder = 0
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 227
    Width = 597
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    ExplicitTop = 222
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 593
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 271
    Width = 597
    Height = 70
    Align = alBottom
    TabOrder = 3
    ExplicitTop = 266
    object PnSaiDesis: TPanel
      Left = 451
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 449
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object QrOSSrv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT des.Nome NO_DesServico, srv.* '
      'FROM ossrv srv'
      'LEFT JOIN desservico des ON des.Codigo=srv.DesServico'
      'WHERE srv.Codigo=:P0')
    Left = 12
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSSrvNO_DesServico: TWideStringField
      FieldName = 'NO_DesServico'
      Size = 60
    end
    object QrOSSrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSSrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSSrvDesServico: TIntegerField
      FieldName = 'DesServico'
    end
    object QrOSSrvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSSrvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSSrvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSSrvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSSrvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSSrvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSSrvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsOSSrv: TDataSource
    DataSet = QrOSSrv
    Left = 40
    Top = 12
  end
  object QrGraGruX: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT ggx.Controle GraGruX, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1,' +
        ' '
      'gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad, '
      'HowBxaEstq, GerBxaEstq, gtc.Nome NOMEGRATAMCAD, '
      'gtc.CodUsu CODUSUGRATAMCAD, gg1.CST_A, gg1.CST_B, '
      'gg1.UnidMed, gg1.NCM, gg1.Peso, gg1.FatorClas,'
      'unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED, '
      'unm.Nome NOMEUNIDMED '
      'FROM gragrux ggx'
      'LEFT JOIN gragru1 gg1 ON gg1.Nivel1=ggx.GraGru1'
      'LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad'
      'LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed'
      'LEFT JOIN prdgruptip pgt ON pgt.Codigo=gg1.PrdGrupTip'
      '/*WHERE pgt.TipPrd=1 */'
      'ORDER BY gg1.Nome')
    Left = 436
    Top = 12
    object QrGraGruXGraGruX: TIntegerField
      FieldName = 'GraGruX'
    end
    object QrGraGruXNivel3: TIntegerField
      FieldName = 'Nivel3'
    end
    object QrGraGruXNivel2: TIntegerField
      FieldName = 'Nivel2'
    end
    object QrGraGruXNivel1: TIntegerField
      FieldName = 'Nivel1'
    end
    object QrGraGruXNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraGruXPrdGrupTip: TIntegerField
      FieldName = 'PrdGrupTip'
    end
    object QrGraGruXGraTamCad: TIntegerField
      FieldName = 'GraTamCad'
    end
    object QrGraGruXHowBxaEstq: TSmallintField
      FieldName = 'HowBxaEstq'
    end
    object QrGraGruXGerBxaEstq: TSmallintField
      FieldName = 'GerBxaEstq'
    end
    object QrGraGruXNOMEGRATAMCAD: TWideStringField
      FieldName = 'NOMEGRATAMCAD'
      Size = 50
    end
    object QrGraGruXCODUSUGRATAMCAD: TIntegerField
      FieldName = 'CODUSUGRATAMCAD'
      Required = True
    end
    object QrGraGruXCST_A: TSmallintField
      FieldName = 'CST_A'
    end
    object QrGraGruXCST_B: TSmallintField
      FieldName = 'CST_B'
    end
    object QrGraGruXUnidMed: TIntegerField
      FieldName = 'UnidMed'
    end
    object QrGraGruXNCM: TWideStringField
      FieldName = 'NCM'
      Size = 10
    end
    object QrGraGruXPeso: TFloatField
      FieldName = 'Peso'
    end
    object QrGraGruXFatorClas: TIntegerField
      FieldName = 'FatorClas'
    end
    object QrGraGruXSIGLAUNIDMED: TWideStringField
      FieldName = 'SIGLAUNIDMED'
      Size = 6
    end
    object QrGraGruXCODUSUUNIDMED: TIntegerField
      FieldName = 'CODUSUUNIDMED'
      Required = True
    end
    object QrGraGruXNOMEUNIDMED: TWideStringField
      FieldName = 'NOMEUNIDMED'
      Size = 30
    end
  end
  object DsGraGruX: TDataSource
    DataSet = QrGraGruX
    Left = 464
    Top = 12
  end
end
