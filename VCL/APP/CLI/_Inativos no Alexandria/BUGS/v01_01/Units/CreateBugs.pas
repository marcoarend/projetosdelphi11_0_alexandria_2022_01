unit CreateBugs;

interface

uses
  StdCtrls, ExtCtrls, Windows, Messages, SysUtils, Classes, Graphics, Controls,
  Forms, Dialogs, Menus, UnMsgInt, UnInternalConsts, Db, DbCtrls,
  UnInternalConsts2, ComCtrls, Registry, mySQLDbTables,(* DbTables,*) dmkGeral;

type
  TNomeTabRecriaTempTableBugs = (ntrtt_Cuns_Img_Cab,
  ntrtt_OS_Alv_, ntrtt_OS_Dep_, ntrtt_OS_Frm_Abr,
  ntrtt_OS_Loc_, ntrtt_OS_Imp_,
  //
  ntrtt_Psq_OSCabAlv, ntrtt_Psq_OSAlv, ntrtt_Psq_OSAge,
  //
  ntrtt_Imp_EntiTel, ntrtt_Imp_EntiEma,
  //
  ntrtt_Imp_OSCab, ntrtt_Imp_OSCabAlv, Srv, ntrtt_Imp_OSSrv, ntrtt_Imp_OSAlv,
  ntrtt_Imp_OSFrmCab, ntrtt_Imp_OSFrmRec, ntrtt_Imp_OSFrmAbr, ntrtt_Imp_OSFrmDep,
  ntrtt_Imp_OSMonCab, ntrtt_Imp_OSMonRec, (*ntrtt_Imp__OS_Mon_Dep_,*) ntrtt_Imp_OSPrz,
  ntrtt_Imp_OSCxa, ntrtt_Imp_OSTox, ntrtt_Imp_OSToz, ntrtt_Imp_OSAge,
  //
  ntrtt_Imp_STerCad, ntrtt_Imp_SImaCad, ntrtt_Imp_SImaDep,
  ntrtt_Imp_SImaCdi, ntrtt_Imp_SImaCav, ntrtt_Imp_SImaRes, ntrtt_Imp_SImaCui,
  ntrtt_Imp_SImaAti, ntrtt_Imp_SImaCxa, ntrtt_Imp_SICxDef, ntrtt_Imp_SICdDef,
  ntrtt_Imp_SMovCad, ntrtt_Imp_SMovDef,
  ntrtt_Imp_OSPipML, ntrtt_Imp_OSPipMI,
  //
  ntrtt_Sel_PIP, ntrtt_Def_OMR, ntrtt_Def_DMP,
  //
  ntrtt_MoniNewOSs,
  //
  ntrtt_STC_sem_Emp,
  //
  ntrtt_OPIPI_PreQtd,
  //
  ntrtt_ProtoAddIts,
  //
  ntrtt_RMIP, ntrtt_RMIP_004Atr, ntrtt_RMIP_006Atr, ntrtt_RMIP_010Uso,
  ntrtt_RMIP_014Atr, ntrtt_RMIP_015Atr,
  //
  //ntrtt_FiltroPragas
  //
  ntrtt_OSProlepse
  );
  //
  TAcaoCreateBugs = (acDrop, acCreate, acFind);
  TCreateBugs = class(TObject)

  private
    { Private declarations }
    procedure Cria_ntrtt_OS_Imp_(Qry: TmySQLQuery);

    procedure Cria_ntrtt_Cuns_Img_Cab(Qry: TmySQLQuery);

    procedure Cria_ntrtt_OS_Alv_(Qry: TmySQLQuery);
    procedure Cria_ntrtt_OS_Dep_(Qry: TmySQLQuery);
    procedure Cria_ntrtt_OS_Loc_(Qry: TmySQLQuery);
    procedure Cria_ntrtt_OS_Frm_Abr(Qry: TmySQLQuery);
    //
    procedure Cria_ntrtt_Imp_EntiTel(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_EntiEma(Qry: TmySQLQuery);
    //
    procedure Cria_ntrtt_Imp_OSCab(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_OSAge(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_OSCabAlv(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_OSSrv(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_OSAlv(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_OSFrmCab(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_OSFrmRec(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_OSFrmAbr(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_OSFrmDep(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_OSMonCab(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_OSMonRec(Qry: TmySQLQuery);
    //procedure Cria_ntrtt_Imp__OS_Mon_Dep_(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_OSPrz(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_OSCxa(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_OSTox(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_OSToz(Qry: TmySQLQuery);
    //
    procedure Cria_ntrtt_Psq_OSAge(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Psq_OSAlv(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Psq_OSCabAlv(Qry: TmySQLQuery);

    //
    procedure Cria_ntrtt_Imp_OSPipML(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_OSPipMI(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_SMovCad(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_SMovDef(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_STerCad(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_SImaCad(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_SImaDep(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_SImaCdi(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_SImaCav(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_SImaRes(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_SImaCui(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_SImaAti(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_SImaCxa(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_SICxDef(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Imp_SICdDef(Qry: TmySQLQuery);
    //
    procedure Cria_ntrtt_Sel_PIP(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Def_OMR(Qry: TmySQLQuery);
    procedure Cria_ntrtt_Def_DMP(Qry: TmySQLQuery);
    procedure Cria_ntrtt_MoniNewOSs(Qry: TmySQLQuery);
    //
    procedure Cria_ntrtt_STC_sem_Emp(Qry: TmySQLQuery);
    //
    procedure Cria_ntrtt_OPIPI_PreQtd(Qry: TmySQLQuery);
    //
    procedure Cria_ntrtt_ProtoAddIts(Qry: TmySQLQuery);
    //
    procedure Cria_ntrtt_RMIP(Qry: TmySQLQuery);
    procedure Cria_ntrtt_RMIP_004Atr(Qry: TmySQLQuery);
    procedure Cria_ntrtt_RMIP_006Atr(Qry: TmySQLQuery);
    procedure Cria_ntrtt_RMIP_010Uso(Qry: TmySQLQuery);
    //
    //procedure Cria_ntrtt_FiltroPragas(Qry: TmySQLQuery);
    //
    procedure Cria_ntrtt_OSProlepse(Qry: TmySQLQuery);
    //
  public
    { Public declarations }
    function RecriaTempTableNovo(Tabela: TNomeTabRecriaTempTableBugs;
             Qry: TmySQLQuery; UniqueTableName: Boolean = False; Repeticoes:
             Integer = 1; NomeTab: String = ''): String;
  end;

var
  UnCreateBugs: TCreateBugs;

implementation

uses UnMyObjects, Module, ModuleGeral;


procedure TCreateBugs.Cria_ntrtt_Imp_SICdDef(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  ID_Item              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  ID_Sorc              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  AtrCad               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  AtrIts               int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_SICxDef(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  ID_Item              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  ID_Sorc              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  AtrCad               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  AtrIts               int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_SMovCad(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(100)          DEFAULT "?"          ,');
  Qry.SQL.Add('  Tipo                 tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Cliente              int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_SMovDef(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  ID_Item              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  ID_Sorc              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  AtrCad               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  AtrIts               int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Cuns_Img_Cab(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                           varchar(50)  NOT NULL DEFAULT "?"          ,');
  Qry.SQL.Add('  Status                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Caminho                        varchar(255) NOT NULL DEFAULT "?"          ,');
  Qry.SQL.Add('  Dependencia                    int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  CodiNCT                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Observ                         text                                       ,');
  Qry.SQL.Add('  Aplicacao                      int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ordem                          int(11)      NOT NULL DEFAULT "0"          ,');
  {
  Qry.SQL.Add('  Lk                             int(11)       ,');
  Qry.SQL.Add('  DataCad                        date          ,');
  Qry.SQL.Add('  DataAlt                        date          ,');
  Qry.SQL.Add('  UserCad                        int(11)       ,');
  Qry.SQL.Add('  UserAlt                        int(11)       ,');
  Qry.SQL.Add('  AlterWeb                       tinyint(1)    ,');
  }
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Def_OMR(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IDIts                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PrvQtd                         double(15,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  PrvPrc                         double(15,6) NOT NULL DEFAULT "0.000000"   ,');
  Qry.SQL.Add('  PrvVal                         double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  UsoQtd                         double(15,3) NOT NULL DEFAULT "0.0000"     ,');
  Qry.SQL.Add('  UsoPrc                         double(15,6) NOT NULL DEFAULT "0.000000"   ,');
  Qry.SQL.Add('  UsoVal                         double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  UsoDec                         double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  UsoTot                         double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Ordem                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Reordem                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  ValCliDd                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  EhDiluente                     tinyint(1)   NOT NULL DEFAULT "0"          ,');
  //  Compatibilidade
  Qry.SQL.Add('  Lk                             int(11)       ,');
  Qry.SQL.Add('  DataCad                        date          ,');
  Qry.SQL.Add('  DataAlt                        date          ,');
  Qry.SQL.Add('  UserCad                        int(11)       ,');
  Qry.SQL.Add('  UserAlt                        int(11)       ,');
  Qry.SQL.Add('  AlterWeb                       tinyint(1)    ,');
  //  FIM Compatibilidade
(*
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_GG1                         varchar(255)                               ,');
*)
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

{
procedure TCreateBugs.Cria_ntrtt_FiltroPragas(Qry: TmySQLQuery);
begin
/
  Qry.SQL.Add('  CodPraga_A                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  CodPraga_Z                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NomPraga_A                     varchar(60)                                ,');
  Qry.SQL.Add('  NomPraga_Z                     varchar(60)                                ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "1"         ,');
  Qry.SQL.Add('  PRIMARY KEY (Controle)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;
}

procedure TCreateBugs.Cria_ntrtt_Def_DMP(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Ordem                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Dias                           int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PRIMARY KEY (Ordem)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_EntiEma(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_CONTATO           varchar(30)                                ,');
  Qry.SQL.Add('  NO_CARGO             varchar(30)                                ,');
  Qry.SQL.Add('  DtaNatal             date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  NOMEETC              varchar(30)                                ,');
  Qry.SQL.Add('  EMail                varchar(255)                               ,');
  Qry.SQL.Add('  NATAL_TXT            varchar(10)                                ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_Entitel(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_CONTATO           varchar(30)                                ,');
  Qry.SQL.Add('  NO_CARGO             varchar(30)                                ,');
  Qry.SQL.Add('  DtaNatal             date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  NOMEETC              varchar(30)                                ,');
  Qry.SQL.Add('  Telefone             varchar(20)                                ,');
  Qry.SQL.Add('  Ramal                varchar(20)                                ,');
  Qry.SQL.Add('  NATAL_TXT            varchar(10)                                ,');
{
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Telefone             varchar(20)                                ,');
  Qry.SQL.Add('  EntiTipCto           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ramal                varchar(20)                                ,');
  Qry.SQL.Add('  DiarioAdd            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NOMEETC              varchar(30)                                ,');
}
  Qry.SQL.Add('  TEL_TXT              varchar(40)                                ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSAlv(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Praga_Z              int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSCab(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Entidade             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SiapTerCad           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Estatus              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  FatoGeradr           int(11)      NOT NULL DEFAULT "0"          ,');
  //Qry.SQL.Add('  OSOrigem             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DtaContat            datetime     NOT NULL DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  DtaVisPrv            datetime     NOT NULL DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  DtaVisExe            datetime     NOT NULL DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  DtaExePrv            datetime     NOT NULL DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  DtaExeIni            datetime     NOT NULL DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  DtaExeFim            datetime     NOT NULL DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  DdsPosVda            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  EntiContat           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NumContrat           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  EntPagante           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  EntContrat           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  ValorTotal           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ValorServi           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ValorDesco           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  InvalTotal           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  InvalServi           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  InvalDesco           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  OrcamTotal           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  OrcamServi           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  OrcamDesco           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ValiDdOrca           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  ExeTxtCli1           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  ExeTxtCli2           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Operacao             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  ValorPre             double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ObsGaranti           text                                       ,');
  Qry.SQL.Add('  ObsExecuta           text                                       ,');
  //
  Qry.SQL.Add('  LauInfesInt          text                                       ,');
  Qry.SQL.Add('  LauInfesExt          text                                       ,');
  Qry.SQL.Add('  LauParTec            text                                       ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  MapPMV               tinyint(1)   NOT NULL DEFAULT "-1"         ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSAge(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ordem                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Agente               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Responsa             int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSCabAlv(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ordem                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Praga_A              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Praga_Z              int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSCxa(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Caixa                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  GarantiaDd                     int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  HrEvacuar                      int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  HrExecutar                     double(15,10) NOT NULL DEFAULT "0.0000000000",');
  Qry.SQL.Add('  ValCalc                        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ValInfo                        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ValDesc                        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ValTota                        double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Autorizado                     tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  ChekLstCab                     int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Detalhes                       text                                       ,');
  Qry.SQL.Add('  TudoFeito                      tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"                    ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"                    ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSFrmCab(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(60)                       ,');
  Qry.SQL.Add('  Formula              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  EquipAplic           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  QtdTot               double(15,3) NOT NULL DEFAULT "0.000"       ,');
  Qry.SQL.Add('  QtdQSP               double(15,3) NOT NULL DEFAULT "0.000"       ,');
  Qry.SQL.Add('  CusTot               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  PercFeito            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSFrmDep(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IDIts                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Tabela               tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Cadastro             int(11)      NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  SeqLug               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SeqLoc               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SeqTyp               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SeqDep               int(11)      NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  TotLug               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TotLoc               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TotTyp               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TotDep               int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSFrmRec(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IDIts                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  GraGruX              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PrvQtd               double(15,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  PrvPrc               double(15,6)                               ,');
  Qry.SQL.Add('  PrvVal               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  UsoQtd               double(15,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  UsoPrc               double(15,6)                               ,');
  Qry.SQL.Add('  UsoVal               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  UsoDec               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  UsoTot               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Ordem                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Reordem              int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSFrmAbr(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IDIts                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Abrangicie           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(60)                                ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSMonCab(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(60)                                ,');
  Qry.SQL.Add('  Formula              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  EquipAplic           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  QtdTot               double(15,3) NOT NULL DEFAULT "0.000"       ,');
  Qry.SQL.Add('  QtdQSP               double(15,3) NOT NULL DEFAULT "0.000"       ,');
  Qry.SQL.Add('  CusTot               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

(*
procedure TCreateBugs.Cria_ntrtt_Imp__OS_Mon_Dep_(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IDIts                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Tabela               tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Cadastro             int(11)      NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  SeqLug               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SeqLoc               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SeqTyp               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SeqDep               int(11)      NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  TotLug               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TotLoc               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TotTyp               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TotDep               int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;
*)

procedure TCreateBugs.Cria_ntrtt_Imp_OSMonRec(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IDIts                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  GraGruX              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PrvQtd               double(15,3) NOT NULL DEFAULT "0.000"       ,');
  Qry.SQL.Add('  PrvPrc               double(15,6) NOT NULL DEFAULT "0.000000"       ,');
  Qry.SQL.Add('  PrvVal               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  UsoQtd               double(15,3) NOT NULL DEFAULT "0.000"       ,');
  Qry.SQL.Add('  UsoPrc               double(15,6) NOT NULL DEFAULT "0.000000"       ,');
  Qry.SQL.Add('  UsoVal               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  UsoDec               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  UsoTot               double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Ordem                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Reordem              int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSPipML(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(255)                               ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSPipMI(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  NO_PIP                         varchar(30)                      ,');
  Qry.SQL.Add('  NO_ITEM                        varchar(25)                      ,');
  Qry.SQL.Add('  Sigla                          varchar(100)                     ,');
  Qry.SQL.Add('  Tabela                         tinyint(1)   NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  Conta                          bigint(20)   NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  SuperOrd                       int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  SuperSub                       int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  Ordem                          int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  SubOrdem                       int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  SobreOrd                       int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  PrgLstCab                      int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  PrgLstIts                      int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  SiapImaCad                     int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  SiapImaDep                     int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  NO_LOCAL                       varchar(60)                      ,');
  Qry.SQL.Add('  NO_DEPENDENCI                  varchar(60)                      ,');
  {
  Qry.SQL.Add('  NO_PERGUNTA                    varchar(100)                     ,');
  Qry.SQL.Add('  TabIdx                         bigint(20)   NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  Funcoes                        tinyint(1)   NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  Dependente                     tinyint(1)   NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  PrgAtrCad                      int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  Filiacao                       int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  Relacao                        int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  Nivel                          int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  NivSeq                         int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  Pergunta                       int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  BinarCad0                      int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  BinarCad1                      int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  AcaoPrd                        tinyint(1)   NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  Respondido                     tinyint(1)   NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  RespBin                        tinyint(1)   NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  RespQtd                        double(15,3) NOT NULL DEFAULT "0.000",');
  Qry.SQL.Add('  RespAtrCad                     int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  RespAtrIts                     int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  RespAtrTxt                     varchar(60)                      ,');
  Qry.SQL.Add('  RespTxtLvr                     mediumtext                       ,');
  Qry.SQL.Add('  RespPrdBin                     tinyint(1)   NOT NULL DEFAULT "0",');
}
  Qry.SQL.Add('  LupForma                       tinyint(1)   NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  LupQtdVzs                      tinyint(3)   NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  LupSeqRsp                      int(11)      NOT NULL DEFAULT "0",');
  //
  Qry.SQL.Add('  PERGUNTA                       varchar(255)                     ,');
  Qry.SQL.Add('  RESPOSTA                       Text                             ,');
  //
  //
  Qry.SQL.Add('  DtaPutPMV                      datetime                         ,');
  Qry.SQL.Add('  ValCliDd                       int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  DtaValPMV                      datetime                         ,');
  //
  Qry.SQL.Add('  Relacao                        int(11)      NOT NULL DEFAULT "0",');
  //
  Qry.SQL.Add('  Seq                            int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  MyOrd                          int(11)      NOT NULL DEFAULT "0",');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0" ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSPrz(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Condicao             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DescoPer             double(15,6) NOT NULL DEFAULT "0.000000"       ,');
  Qry.SQL.Add('  BaseVal              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  DescoVal             double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  TotaVal              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Escolhido            tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Parcelas             int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSSrv(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DesServico           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  GarantiaDd           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  HrEvacuar            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  HrExecutar           double(15,10) NOT NULL DEFAULT "0.0000000000",');
  Qry.SQL.Add('  ValCalc              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ValInfo              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ValDesc              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  ValTota              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Autorizado           tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  MoniDdTotl           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  MoniDdIntv           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TudoFeitoM           tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TudoFeitoA           tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSTox(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  ApMo                           tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IDIts                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TIPO                           varchar(2)                         ,');
  Qry.SQL.Add('  NO_Diluente                    varchar(120)                       ,');
  Qry.SQL.Add('  GraGru1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  GraGruX                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PrgLstCab                      int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_EQUIP                       varchar(120)                               ,');
  Qry.SQL.Add('  NO_TIPOAPLICA                  varchar(60)                                ,');
  Qry.SQL.Add('  QtdTot                         double(15,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  SIGLA_TOT                      varchar(30)                                ,');
  Qry.SQL.Add('  Diluente                       tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  UsoQtd                         double(15,3) NOT NULL DEFAULT "0.000"      ,');
  Qry.SQL.Add('  EhDiluente                     tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  RegMinSaud                     varchar(60)                                ,');
  Qry.SQL.Add('  FoneCIT                        varchar(18)                                ,');
  Qry.SQL.Add('  FoneCEATOX                     varchar(18)                                ,');
  Qry.SQL.Add('  Toxicidade                     varchar(255)                               ,');
  Qry.SQL.Add('  Concentrac                     varchar(255)                               ,');
  Qry.SQL.Add('  AcaoToxica                     varchar(255)                               ,');
  Qry.SQL.Add('  Antidoto                       varchar(255)                               ,');
  Qry.SQL.Add('  NO_GG1                         varchar(120)                               ,');
  Qry.SQL.Add('  Referencia                     varchar(25)                                ,');
  Qry.SQL.Add('  SIGLA_USO                      varchar(30)                                ,');
  Qry.SQL.Add('  NO_MARCA                       varchar(60)                                ,');
  Qry.SQL.Add('  NO_FABRICA                     varchar(60)                                ,');
  Qry.SQL.Add('  Observacao                     text                                       ,');
  Qry.SQL.Add('  NumLote                        varchar(60)                                ,');
  Qry.SQL.Add('  NumLaudo                       varchar(60)                                ,');
  Qry.SQL.Add('  FormaApresen                   varchar(15)                                ,');
  //
  Qry.SQL.Add('  MyOrd                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_OSToz(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  ApMo                           tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IDIts                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PRINCIPIO_ATIVO                varchar(120)                               ,');
  Qry.SQL.Add('  GRUPO_QUIMICO                  varchar(120)                               ,');
  //
  Qry.SQL.Add('  MyOrd                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_SImaCad(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SiapImaTer           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Objeto               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Finalidade           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  TpConstru            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SCompl2              varchar(60)                                 ,');
  Qry.SQL.Add('  M2Constru            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  M2NaoBuild           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  M2Terreno            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  M2Total              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_SImaCav(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Caracteris           int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_SImaCdi(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Caracteris           int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_SImaCui(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Cuidado              int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_SImaDep(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Dependenci           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  MLarg                double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  MComp                double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  MAltu                double(15,2) NOT NULL DEFAULT "0.00"       ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_SImaRes(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Residente            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(60)                                 ,');
  Qry.SQL.Add('  AnoNatal             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Cuidado              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Observacao           varchar(255)                               ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_SImaAti(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Atividade            int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_STerCad(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(100)                               ,');
  Qry.SQL.Add('  Cliente              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SLograd              tinyint(2)                                 ,');
  Qry.SQL.Add('  SRua                 varchar(60)                                 ,');
  Qry.SQL.Add('  SNumero              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SCompl               varchar(60)                                 ,');
  Qry.SQL.Add('  SBairro              varchar(60)                                 ,');
  Qry.SQL.Add('  SCidade              varchar(60)                                 ,');
  Qry.SQL.Add('  SUF                  char(3)      NOT NULL DEFAULT "0"                      ,');
  Qry.SQL.Add('  SCEP                 int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SPais                varchar(60)                                 ,');
  Qry.SQL.Add('  SEndeRef             varchar(100)                               ,');
  Qry.SQL.Add('  SCodMunici           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SCodiPais            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  STe1                 varchar(20)                                ,');
  Qry.SQL.Add('  M2Constru            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  M2NaoBuild           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  M2Terreno            double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  M2Total              double(15,2) NOT NULL DEFAULT "0.00"       ,');
  //
  Qry.SQL.Add('  LugInfo              int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Imp_SImaCxa(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  MatersCxa            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  FormasCxa            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  VolumeL              double(15,3) NOT NULL DEFAULT "0.000"       ,');
  Qry.SQL.Add('  Local                varchar(255)                               ,');
  Qry.SQL.Add('  Acesso               varchar(255)                               ,');
  Qry.SQL.Add('  Medidas              varchar(255)                               ,');
  //
  Qry.SQL.Add('  MyOrd                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_MoniNewOSs(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Tabela               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SubTab               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle             int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IDIts                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IDIt2                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  IDItZ                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ordem                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Dias                 int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  EmisUltDta           date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  DesServico           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  HrEvacuar            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  HrExecutar           double(15,2) NOT NULL DEFAULT "0.00"       ,');
  Qry.SQL.Add('  Formula              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PrgLstCab            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PerioDd              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  ExtenDd              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DdPostero            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NumContrat           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DtaExeFim            date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  DtaCntrFim           date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  DtaOSCalc            date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  DtaOSUtil            date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  DtaOSReal            date         NOT NULL DEFAULT "0000-00-00" ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Cliente              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SiapTerCad           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  EmisStatus           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  GrupoOS              int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  SeqNovaOS            int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  DiaSemTxt            varchar(20)           DEFAULT "???"        ,');
  Qry.SQL.Add('  TitTipoTab           varchar(20)           DEFAULT "???"        ,');
  Qry.SQL.Add('  TitItemTab           varchar(60)  NOT NULL DEFAULT "???"        ,');
  Qry.SQL.Add('  MinHAgeExe           time         NOT NULL DEFAULT "00:00:00"   ,');
  Qry.SQL.Add('  DtInicial            date         NOT NULL DEFAULT "0000-00-00" ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PRIMARY KEY (IDItZ)');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');

  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_OPIPI_PreQtd(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Filiacao                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PrgLstIts                      int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  PipCad                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                          bigint(20)   NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  NO_PIP                         varchar(30)                                ,');
  Qry.SQL.Add('  NO_Pergunta                    varchar(100)                               ,');
  Qry.SQL.Add('  RespQtd                        int(11)      NOT NULL DEFAULT "0"          ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_OSProlepse(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  PelaEmisUltDta                 varchar(3)   NOT NULL                      ,');
  Qry.SQL.Add('  PelaDtaExeFim                  varchar(3)   NOT NULL                      ,');
  Qry.SQL.Add('  PeloFatoGeradr20               varchar(3)   NOT NULL                      ,');
  Qry.SQL.Add('  PeloPosGerou0                  varchar(3)   NOT NULL                      ,');
  Qry.SQL.Add('  PelaFormula                    varchar(7)   NOT NULL                      ,');
  Qry.SQL.Add('  Empresa                        int(11)                                    ,');
  Qry.SQL.Add('  Grupo                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Opcao                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Conta                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  DdPostero                      int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_CLIENTE                     varchar(100)                               ,');
  Qry.SQL.Add('  NO_EmisStatus                  varchar(7)                                 ,');
  Qry.SQL.Add('  NO_STC                         varchar(100)                               ,');
  Qry.SQL.Add('  CO_FORMULA                     int(11)                                    ,');
  Qry.SQL.Add('  NO_FORMULA                     varchar(60)                                ,');
  Qry.SQL.Add('  Entidade                       int(11)                                    ,');
  Qry.SQL.Add('  SiapTerCad                     int(11)                                    ,');
  Qry.SQL.Add('  DtaExeFim                      datetime     NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  NumContrat                     int(11)                                    ,');
  Qry.SQL.Add('  DtaPrxRenw                     date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  EmisUltDta                     date         NOT NULL  DEFAULT "0000-00-00",');
  Qry.SQL.Add('  PerioDd                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ExtenDd                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  EmisStatus                     tinyint(4)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  PosGerou                       tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Tabela                         tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  SIGLA_TAB                      varchar(3)   NOT NULL                      ,');
  //
  Qry.SQL.Add('  IDSuperior                     int(11)      NOT NULL  DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_OS_Alv_(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  o2Gru                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  c2Gru                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  n2Gru                varchar(60)                                 ,');
  Qry.SQL.Add('  a2Gru                tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  o1Cab                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  c1Cab                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  n1Cab                varchar(60)                                 ,');
  Qry.SQL.Add('  a1Cab                tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_OS_Dep_(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  o4Ter                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  c4Ter                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  n4Ter                varchar(100)                               ,');
  Qry.SQL.Add('  a4Ter                tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  o3Loc                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  c3Loc                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  n3Loc                varchar(60)                                 ,');
  Qry.SQL.Add('  a3Loc                tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  o2Typ                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  c2Typ                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  n2Typ                varchar(60)                                 ,');
  Qry.SQL.Add('  a2Typ                tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  o1Dep                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  c1Dep                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  n1Dep                varchar(60)                                 ,');
  Qry.SQL.Add('  a1Dep                tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  Tabela               tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_OS_Frm_Abr(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(60)  NOT NULL DEFAULT "?"          ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_OS_Loc_(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  o5Ent                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  c5Ent                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  n5Ent                varchar(100)                               ,');
  Qry.SQL.Add('  a5Ent                tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  o4Ter                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  c4Ter                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  n4Ter                varchar(100)                               ,');
  Qry.SQL.Add('  a4Ter                tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  o3Loc                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  c3Loc                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  n3Loc                varchar(60)                                 ,');
  Qry.SQL.Add('  a3Loc                tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  Tabela               tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_ProtoAddIts(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Session                        datetime     DEFAULT "0000-00-00 00:00:00" ,');
  Qry.SQL.Add('  ID_Cod1                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  ID_Cod2                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  ID_Cod3                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  ID_Cod4                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  ID_Txt1                        varchar(255)                               ,');
  Qry.SQL.Add('  ID_Txt2                        varchar(255)                               ,');
  Qry.SQL.Add('  ID_Txt3                        varchar(255)                               ,');
  Qry.SQL.Add('  ID_Txt4                        varchar(255)                               ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Psq_OSAge(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Agente                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Responsa                       tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  Lk                             int(11)       ,');
  Qry.SQL.Add('  DataCad                        date          ,');
  Qry.SQL.Add('  DataAlt                        date          ,');
  Qry.SQL.Add('  UserCad                        int(11)       ,');
  Qry.SQL.Add('  UserAlt                        int(11)       ,');
  Qry.SQL.Add('  AlterWeb                       tinyint(1)    ,');

  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Psq_OSAlv(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Conta                          int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Praga_Z                        int(11)      NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  Lk                             int(11)       ,');
  Qry.SQL.Add('  DataCad                        date          ,');
  Qry.SQL.Add('  DataAlt                        date          ,');
  Qry.SQL.Add('  UserCad                        int(11)       ,');
  Qry.SQL.Add('  UserAlt                        int(11)       ,');
  Qry.SQL.Add('  AlterWeb                       tinyint(1)    ,');

  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Psq_OSCabAlv(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Praga_A                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Praga_Z                        int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Ordem                          tinyint(1)   NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  Lk                             int(11)       ,');
  Qry.SQL.Add('  DataCad                        date          ,');
  Qry.SQL.Add('  DataAlt                        date          ,');
  Qry.SQL.Add('  UserCad                        int(11)       ,');
  Qry.SQL.Add('  UserAlt                        int(11)       ,');
  Qry.SQL.Add('  AlterWeb                       tinyint(1)    ,');

  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_RMIP(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Nome                           varchar(60)  NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Titulo                         varchar(255) NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  ItemRel                        int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Ordem                          int(11)      NOT NULL  DEFAULT "0"         ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_RMIP_004Atr(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  ITENS                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Respondido                     tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RespAtrCad                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RespAtrIts                     int(11)      NOT NULL  DEFAULT "0"         ,');
  //Qry.SQL.Add('  DtaExeFim                      datetime               DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  DtaExeFim                      date         DEFAULT "0000-00-00"          ,');
  Qry.SQL.Add('  Resposta                       varchar(255)                               ,');
  Qry.SQL.Add('  CorPizza                       int(11)                                    ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_RMIP_006Atr(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  ITENS                          int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Respondido                     tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RespAtrCad                     int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  RespAtrIts                     int(11)      NOT NULL  DEFAULT "0"         ,');
  //Qry.SQL.Add('  DtaExeFim                      datetime               DEFAULT "0000-00-00 00:00:00",');
  Qry.SQL.Add('  DtaExeFim                      date         DEFAULT "0000-00-00"          ,');
  Qry.SQL.Add('  Resposta                       varchar(255)                               ,');
  Qry.SQL.Add('  CorPizza                       int(11)                                    ,');
  Qry.SQL.Add('  PipCad                         int(11)                                    ,');
  Qry.SQL.Add('  SiapImaDep                     int(11)                                    ,');
  Qry.SQL.Add('  Dependenci                     int(11)                                    ,');
  Qry.SQL.Add('  NO_DEPENDENCI                  varchar(60)                                ,');
  //
  Qry.SQL.Add('  Ativo                          tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_RMIP_010Uso(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  ApMo                           tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Codigo                         int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Controle                       int(11)      NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  Conta                          bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  IDIts                          bigint(20)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  GraGru1                        int(11)                                    ,');
  Qry.SQL.Add('  GraGruX                        int(11)                                    ,');
  Qry.SQL.Add('  QtdTot                         double(15,3)                               ,');
  Qry.SQL.Add('  Diluente                       tinyint(1)                                 ,');
  Qry.SQL.Add('  UsoQtd                         double(15,3) NOT NULL  DEFAULT "0.000"     ,');
  Qry.SQL.Add('  EhDiluente                     tinyint(1)   NOT NULL  DEFAULT "0"         ,');
  Qry.SQL.Add('  NO_GG1                         varchar(120)                               ,');
  Qry.SQL.Add('  SIGLA_USO                      varchar(6)                                  ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_Sel_PIP(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Codigo               int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Nome                 varchar(30)                                ,');
  //
  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"           ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_STC_sem_Emp(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  SiapTerCad           int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Empresa              int(11)      NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

procedure TCreateBugs.Cria_ntrtt_OS_Imp_(Qry: TmySQLQuery);
begin
  Qry.SQL.Add('  Grupo                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Lugar                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  Opcao                int(11)      NOT NULL DEFAULT "0"          ,');
  Qry.SQL.Add('  OSCab                int(11)      NOT NULL DEFAULT "0"          ,');

  Qry.SQL.Add('  NO_Lugar             varchar(100)          DEFAULT "?"          ,');

  Qry.SQL.Add('  Ativo                tinyint(1)   NOT NULL DEFAULT "0"          ');
  Qry.SQL.Add(') CHARACTER SET latin1 COLLATE latin1_swedish_ci');
  Qry.ExecSQL;
end;

function TCreateBugs.RecriaTempTableNovo(
  Tabela: TNomeTabRecriaTempTableBugs; Qry: TmySQLQuery; UniqueTableName:
  Boolean = False; Repeticoes: Integer = 1; NomeTab: String = ''): String;
var
  Nome, TabNo: String;
  P: Integer;
begin
  TabNo := '';
  if NomeTab = '' then
  begin
    case Tabela of
      ntrtt_OS_Imp_:        Nome := Lowercase('_OS_Imp_');

      ntrtt_Cuns_Img_Cab:   Nome := Lowercase('_Cuns_Img_Cab');

      ntrtt_OS_Alv_:        Nome := Lowercase('_OS_Alv_');
      ntrtt_OS_Dep_:        Nome := Lowercase('_OS_Dep_');
      ntrtt_OS_Loc_:        Nome := Lowercase('_OS_Loc_');
      ntrtt_OS_Frm_Abr:     Nome := Lowercase('_OS_Frm_Abr');
      //
      ntrtt_Imp_EntiEma:    Nome := Lowercase('_Imp_EntiEma');
      ntrtt_Imp_entiTel:    Nome := Lowercase('_Imp_EntiTel');
      //
      ntrtt_Imp_OSCab:      Nome := Lowercase('_Imp_OSCab');
      ntrtt_Imp_OSAge:      Nome := Lowercase('_Imp_OSAge');
      ntrtt_Imp_OSCabAlv:   Nome := Lowercase('_Imp_OSCabAlv');
      ntrtt_Imp_OSSrv:      Nome := Lowercase('_Imp_OSSrv');
      ntrtt_Imp_OSAlv:      Nome := Lowercase('_Imp_OSAlv');
      ntrtt_Imp_OSFrmCab:   Nome := Lowercase('_Imp_OSFrmCab');
      ntrtt_Imp_OSFrmRec:   Nome := Lowercase('_Imp_OSFrmRec');
      ntrtt_Imp_OSFrmAbr:   Nome := Lowercase('_Imp_OSFrmAbr');
      ntrtt_Imp_OSFrmDep:   Nome := Lowercase('_Imp_OSFrmDep');
      ntrtt_Imp_OSMonCab:   Nome := Lowercase('_Imp_OSMonCab');
      ntrtt_Imp_OSMonRec:   Nome := Lowercase('_Imp_OSMonRec');
      //ntrtt_Imp__OS_Mon_Dep_:   Nome := Lowercase('_Imp__OS_Mon_Dep_');
      ntrtt_Imp_OSPrz:      Nome := Lowercase('_Imp_OSPrz');
      ntrtt_Imp_OSCxa:      Nome := Lowercase('_Imp_OSCxa');
      ntrtt_Imp_OSTox:      Nome := Lowercase('_Imp_OSTox');
      ntrtt_Imp_OSToz:      Nome := Lowercase('_Imp_OSToz');
      //
      ntrtt_Imp_OSPipML:    Nome := Lowercase('_Imp_OSPipML');
      ntrtt_Imp_OSPipMI:    Nome := Lowercase('_Imp_OSPipMI');
      ntrtt_Imp_SMovCad:    Nome := Lowercase('_Imp_SMovCad');
      ntrtt_Imp_SMovDef:    Nome := Lowercase('_Imp_SMovDef');
      ntrtt_Imp_STerCad:    Nome := Lowercase('_Imp_STerCad');
      ntrtt_Imp_SImaCad:    Nome := Lowercase('_Imp_SImaCad');
      ntrtt_Imp_SImaDep:    Nome := Lowercase('_Imp_SImaDep');
      ntrtt_Imp_SImaCdi:    Nome := Lowercase('_Imp_SImaCdi');
      ntrtt_Imp_SImaCav:    Nome := Lowercase('_Imp_SImaCav');
      ntrtt_Imp_SImaRes:    Nome := Lowercase('_Imp_SImaRes');
      ntrtt_Imp_SImaCui:    Nome := Lowercase('_Imp_SImaCui');
      ntrtt_Imp_SImaAti:    Nome := Lowercase('_Imp_SImaAti');
      ntrtt_Imp_SImaCxa:    Nome := Lowercase('_Imp_SImaCxa');
      ntrtt_Imp_SICxDef:    Nome := Lowercase('_Imp_SICxDef');
      ntrtt_Imp_SICdDef:    Nome := Lowercase('_Imp_SICdDef');
      //
      ntrtt_Psq_OSAge:      Nome := Lowercase('_Psq_OSAge');
      ntrtt_Psq_OSAlv:      Nome := Lowercase('_Psq_OSAlv');
      ntrtt_Psq_OSCabAlv:   Nome := Lowercase('_Psq_OSCabAlv');
      //
      ntrtt_Sel_PIP:        Nome := Lowercase('_Sel_PIP');
      ntrtt_Def_OMR:        Nome := Lowercase('_Def_OMR');
      ntrtt_Def_DMP:        Nome := Lowercase('_Def_DMP');
      //
      ntrtt_MoniNewOSs:     Nome := Lowercase('_MoniNewOSs');
      ntrtt_STC_sem_Emp:    Nome := Lowercase('_STC_sem_Emp');
      //
      ntrtt_OPIPI_PreQtd:   Nome := Lowercase('_OPIPI_PreQtd');
      //
      ntrtt_ProtoAddIts:    Nome := Lowercase('_ProtoAddIts');
      //
      ntrtt_RMIP:           Nome := Lowercase('_RMIP_');
      ntrtt_RMIP_004Atr:    Nome := Lowercase('_RMIP_004Atr_');
      ntrtt_RMIP_006Atr:    Nome := Lowercase('_RMIP_006Atr_');
      ntrtt_RMIP_010Uso:    Nome := Lowercase('_RMIP_010Uso_');
      ntrtt_RMIP_014Atr:    Nome := Lowercase('_RMIP_014Atr_');
      ntrtt_RMIP_015Atr:    Nome := Lowercase('_RMIP_015Atr_');
      ntrtt_OSProlepse:     Nome := LowerCase('_OSProlepse_');
      //
      //ntrtt_FiltroPragas:   Nome := Lowercase('_FiltroPragas');
      // ...
      else Nome := '';
    end;
  end else
    Nome := Lowercase(NomeTab);
  //
  if Nome = '' then
  begin
    Geral.MB_Erro(
    'Tabela tempor�ria sem nome definido! (RecriaTemTableNovo)');
    Result := '';
    Exit;
  end;
  if UniqueTableName then
  begin
    TabNo := '_' + FormatFloat('0', VAR_USUARIO) + '_' + Nome;
    //  caso for Master ou Admin (n�meros negativos)
    P := pos('-', TabNo);
    if P > 0 then
      TabNo[P] := '_';
  end else TabNo := Nome;
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('DROP TABLE IF EXISTS ' + TabNo);
  Qry.ExecSQL;
  //
  Qry.Close;
  Qry.SQL.Clear;
  Qry.SQL.Add('CREATE TABLE ' + TabNo +' (');
  //
  case Tabela of
    ntrtt_OS_Imp_:          Cria_ntrtt_OS_Imp_(Qry);

    ntrtt_Cuns_Img_Cab:     Cria_ntrtt_Cuns_Img_Cab(Qry);

    ntrtt_OS_Alv_:          Cria_ntrtt_OS_Alv_(Qry);
    ntrtt_OS_Dep_:          Cria_ntrtt_OS_Dep_(Qry);
    ntrtt_OS_Loc_:          Cria_ntrtt_OS_Loc_(Qry);
    ntrtt_OS_Frm_Abr:       Cria_ntrtt_OS_Frm_Abr(Qry);
    //
    ntrtt_Imp_EntiEma:      Cria_ntrtt_Imp_EntiEma(Qry);
    ntrtt_Imp_EntiTel:      Cria_ntrtt_Imp_EntiTel(Qry);
    //
    ntrtt_Imp_OSCab:        Cria_ntrtt_Imp_OSCab(Qry);
    ntrtt_Imp_OSAge:        Cria_ntrtt_Imp_OSAge(Qry);
    ntrtt_Imp_OSCabAlv:     Cria_ntrtt_Imp_OSCabAlv(Qry);
    ntrtt_Imp_OSSrv:        Cria_ntrtt_Imp_OSSrv(Qry);
    ntrtt_Imp_OSAlv:        Cria_ntrtt_Imp_OSAlv(Qry);
    ntrtt_Imp_OSFrmCab:     Cria_ntrtt_Imp_OSFrmCab(Qry);
    ntrtt_Imp_OSFrmRec:     Cria_ntrtt_Imp_OSFrmRec(Qry);
    ntrtt_Imp_OSFrmAbr:     Cria_ntrtt_Imp_OSFrmAbr(Qry);
    ntrtt_Imp_OSFrmDep:     Cria_ntrtt_Imp_OSFrmDep(Qry);
    ntrtt_Imp_OSMonCab:     Cria_ntrtt_Imp_OSMonCab(Qry);
    ntrtt_Imp_OSMonRec:     Cria_ntrtt_Imp_OSMonRec(Qry);
    //ntrtt_Imp__OS_Mon_Dep_:     Cria_ntrtt_Imp__OS_Mon_Dep_(Qry);
    ntrtt_Imp_OSPrz:        Cria_ntrtt_Imp_OSPrz(Qry);
    ntrtt_Imp_OSCxa:        Cria_ntrtt_Imp_OSCxa(Qry);
    ntrtt_Imp_OSTox:        Cria_ntrtt_Imp_OSTox(Qry);
    ntrtt_Imp_OSToz:        Cria_ntrtt_Imp_OSToz(Qry);
    //
    ntrtt_Imp_OSPipML:      Cria_ntrtt_Imp_OSPipML(Qry);
    ntrtt_Imp_OSPipMI:      Cria_ntrtt_Imp_OSPipMI(Qry);
    ntrtt_Imp_SMovCad:      Cria_ntrtt_Imp_SMovCad(Qry);
    ntrtt_Imp_SMovDef:      Cria_ntrtt_Imp_SMovDef(Qry);
    ntrtt_Imp_STerCad:      Cria_ntrtt_Imp_STerCad(Qry);
    ntrtt_Imp_SImaCad:      Cria_ntrtt_Imp_SImaCad(Qry);
    ntrtt_Imp_SImaDep:      Cria_ntrtt_Imp_SImaDep(Qry);
    ntrtt_Imp_SImaCdi:      Cria_ntrtt_Imp_SImaCdi(Qry);
    ntrtt_Imp_SImaCav:      Cria_ntrtt_Imp_SImaCav(Qry);
    ntrtt_Imp_SImaRes:      Cria_ntrtt_Imp_SImaRes(Qry);
    ntrtt_Imp_SImaCui:      Cria_ntrtt_Imp_SImaCui(Qry);
    ntrtt_Imp_SImaAti:      Cria_ntrtt_Imp_SImaAti(Qry);
    ntrtt_Imp_SImaCxa:      Cria_ntrtt_Imp_SImaCxa(Qry);
    ntrtt_Imp_SICxDef:      Cria_ntrtt_Imp_SICxDef(Qry);
    ntrtt_Imp_SICdDef:      Cria_ntrtt_Imp_SICdDef(Qry);
    //
    ntrtt_Psq_OSAge:        Cria_ntrtt_Psq_OSAge(Qry);
    ntrtt_Psq_OSAlv:        Cria_ntrtt_Psq_OSAlv(Qry);
    ntrtt_Psq_OSCabAlv:     Cria_ntrtt_Psq_OSCabAlv(Qry);
    //
    ntrtt_Sel_PIP:          Cria_ntrtt_Sel_PIP(Qry);
    ntrtt_Def_OMR:          Cria_ntrtt_Def_OMR(Qry);
    ntrtt_Def_DMP:          Cria_ntrtt_Def_DMP(Qry);
    //
    ntrtt_MoniNewOSs:       Cria_ntrtt_MoniNewOSs(Qry);
    //
    ntrtt_STC_sem_Emp:      Cria_ntrtt_STC_sem_Emp(Qry);
    //
    ntrtt_OPIPI_PreQtd:     Cria_ntrtt_OPIPI_PreQtd(Qry);
    //
    ntrtt_ProtoAddIts:      Cria_ntrtt_ProtoAddIts(Qry);
    //
    ntrtt_RMIP:             Cria_ntrtt_RMIP(Qry);
    ntrtt_RMIP_004Atr:      Cria_ntrtt_RMIP_004Atr(Qry);
    ntrtt_RMIP_006Atr:      Cria_ntrtt_RMIP_006Atr(Qry);
    ntrtt_RMIP_010Uso:      Cria_ntrtt_RMIP_010Uso(Qry);
    ntrtt_RMIP_014Atr:      Cria_ntrtt_RMIP_004Atr(Qry);  // Copia do 004
    ntrtt_RMIP_015Atr:      Cria_ntrtt_RMIP_006Atr(Qry);  // Copia do 006
    ntrtt_OSProlepse:       Cria_ntrtt_OSProlepse(Qry);
    //
    //ntrtt_FiltroPragas:     Cria_ntrtt_FiltroPragas(Qry);
    //...
    //
    else Geral.MB_Erro('N�o foi poss�vel criar a tabela tempor�ria "' +
    Nome + '" por falta de implementa��o!');
  end;
  Result := TabNo;
end;

end.

