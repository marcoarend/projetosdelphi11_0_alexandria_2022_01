program Bugsurvey;

uses
  ExceptionLog,
  Forms,
  Windows,
  Messages,
  Dialogs,
  dmkGeral,
  MyListas in 'MyListas.pas',
  Principal in 'Principal.pas' {FmPrincipal},
  ZCF2 in '..\..\..\..\Outros\Geral\Units\ZCF2.pas',
  UnMsgInt in '..\..\..\..\Outros\Geral\Units\UnMsgInt.pas',
  UnInternalConsts2 in '..\..\..\..\Outros\Geral\Units\UnInternalConsts2.pas',
  UnInternalConsts in '..\..\..\..\Outros\Geral\Units\UnInternalConsts.pas',
  UnMyObjects in '..\..\..\..\Outros\Geral\Units\UnMyObjects.pas',
  MyGlyfs in '..\..\..\..\Outros\Geral\Forms\MyGlyfs.pas' {FmMyGlyfs},
  soapserver in '..\..\..\..\Outros\WebService\soapserver.pas',
  MyLinguas in '..\..\..\..\Outros\Geral\Units\MyLinguas.pas',
  ResIntStrings in '..\..\..\..\Outros\Geral\Units\ResIntStrings.pas',
  UnMyVCLref in '..\..\..\..\Outros\Geral\Units\UnMyVCLref.pas',
  NovaVersao in '..\..\..\..\Outros\Geral\Forms\NovaVersao.pas' {FmNovaVersao},
  LinkRankSkin in '..\..\..\..\Outros\Geral\Forms\LinkRankSkin.pas' {FmLinkRankSkin},
  CoresVCLSkin in '..\..\..\Auxiliares\DermaDB\CoresVCLSkin.pas' {FmCoresVCLSkin},
  MeuFrx in '..\..\..\..\Outros\Print\Geral\MeuFrx.pas' {FmMeuFrx},
  Calculadora in '..\..\..\..\Outros\Geral\Forms\Calculadora.pas' {FmCalculadora},
  UnDmkWeb in '..\..\..\..\Outros\Web\UnDmkWeb.pas',
  ZForge in '..\..\..\..\Outros\WinZIP\ZForge.pas' {FmZForge},
  ABOUT in '..\..\..\..\Outros\Geral\Forms\ABOUT.pas' {AboutBox},
  SelOnStringGrid in '..\..\..\..\Outros\Geral\Forms\SelOnStringGrid.pas' {FmSelOnStringGrid},
  UnDmkProcFunc in '..\..\..\..\Geral\Units\UnDmkProcFunc.pas',
  dmkSOAP in '..\..\..\..\Outros\WebService\dmkSOAP.pas',
  SelRadioGroup in '..\..\..\..\Geral\SelRadioGroup.pas' {FmSelRadioGroup},
  Module in 'Module.pas' {Dmod: TDataModule},
  UnGOTOy in '..\..\..\..\Outros\MySQL\UnGOTOy.pas',
  dmkDAC_PF in '..\..\..\..\Outros\MySQL\dmkDAC_PF.pas',
  UMySQLModule in '..\..\..\..\Outros\MySQL\UMySQLModule.pas',
  ModuleGeral in '..\..\..\..\Outros\MySQL\ModuleGeral.pas' {DModG: TDataModule},
  MyDBCheck in '..\..\..\..\Outros\MySQL\MyDBCheck.pas',
  QuaisItens in '..\..\..\..\Outros\Geral\Forms\QuaisItens.pas' {FmQuaisItens},
  GetData in '..\..\..\..\Outros\Geral\Forms\GetData.pas' {FmGetData},
  Senha in '..\..\..\..\Outros\Geral\Forms\Senha.pas' {FmSenha},
  UCreateEDrop in '..\..\..\..\Geral\Units\UCreateEDrop.pas',
  SelCod in '..\..\..\..\Outros\Geral\Forms\SelCod.pas' {FmSelCod},
  SenhaEspecial in '..\..\..\..\Outros\Geral\Forms\SenhaEspecial.pas' {FmSenhaEspecial},
  ConfJanela in '..\..\..\..\Outros\Geral\Forms\ConfJanela.pas' {FmConfJanela},
  GModule in '..\..\..\..\Outros\MySQL\GModule.pas' {GMod: TDataModule},
  UCreate in '..\..\..\..\Outros\MySQL\UCreate.pas',
  UnLock_Dmk in '..\..\..\Auxiliares\UnLock\UnLock_Dmk.pas' {FmUnLock_Dmk},
  UnitMD5 in '..\..\..\..\Outros\Encrypt\UnitMD5.pas',
  ZStepCodUni in '..\..\..\..\Outros\ZStepCod\ZStepCodUni.pas' {FmZStepCodUni},
  UnLock_MLA in '..\..\..\..\Outros\Geral\Forms\UnLock_MLA.pas' {FmUnLock_MLA},
  Terminais in '..\..\..\..\Outros\Geral\Forms\Terminais.pas' {FmTerminais},
  PCsNaNet in '..\..\..\..\Outros\internet\PCsNaNet.pas' {FmPCsNaNet},
  VerifiDB in '..\..\..\..\Outros\MySQL\VerifiDB.pas' {FmVerifiDB},
  Servidor in '..\..\..\..\Outros\MySQL\Servidor.pas' {FmServidor},
  Backup3 in '..\..\..\..\Outros\MySQL\Backup3.pas' {FmBackup3},
  SenhaBoss in '..\..\..\..\Outros\Geral\Forms\SenhaBoss.pas' {FmSenhaBoss},
  InstallMySQL41 in '..\..\..\..\Outros\MySQL\InstallMySQL41.pas' {FmInstallMySQL41},
  NomeX in '..\..\..\..\Outros\Geral\Forms\NomeX.pas' {FmNomeX},
  ServiceManager in '..\..\..\..\Outros\MySQL\ServiceManager.pas',
  ServicoManager in '..\..\..\..\Outros\MySQL\ServicoManager.pas' {FmServicoManager},
  ServerConnect in '..\..\..\..\Outros\MySQL\ServerConnect.pas' {FmServerConnect},
  MyInis in '..\..\..\..\Outros\MySQL\MyInis.pas' {FmMyInis},
  Bugsurvey_Dmk in 'Bugsurvey_Dmk.pas' {FmBugsurvey_Dmk},
  CustomFR3Imp in '..\..\..\..\Outros\Print\Custom\CustomFR3Imp.pas' {FmCustomFR3Imp},
  CustomFR3 in '..\..\..\..\Outros\Print\Custom\CustomFR3.pas' {FmCustomFR3},
  UnMySQLCuringa in '..\..\..\..\Outros\MySQL\UnMySQLCuringa.pas',
  Curinga in '..\..\..\..\Outros\Geral\Forms\Curinga.pas' {FmCuringa},
  PesqDescri in '..\..\..\..\Outros\Geral\Forms\PesqDescri.pas' {FmPesqDescri},
  cfgAtributos in '..\..\..\..\Geral\Atributos\cfgAtributos.pas',
  AtrDef in '..\..\..\..\Geral\Atributos\AtrDef.pas' {FmAtrDef},
  AtrCad in '..\..\..\..\Geral\Atributos\AtrCad.pas' {FmAtrCad},
  AtrIts in '..\..\..\..\Geral\Atributos\AtrIts.pas' {FmAtrIts},
  MeuDBUses in '..\..\..\..\Outros\MySQL\MeuDBUses.pas' {FmMeuDBUses},
  PesqESel in '..\..\..\..\Outros\Geral\Forms\PesqESel.pas' {FmPesqESel},
  PesqNome in '..\..\..\..\Outros\Geral\Forms\PesqNome.pas' {FmPesqNome},
  PerfJan_Tabs in '..\..\..\..\Outros\PerfJan\PerfJan_Tabs.pas',
  DBReplicaParte in '..\..\..\..\Outros\MySQL\Outros\DBReplicaParte.pas' {FmDBReplicaParte},
  Bugs_Tabs in '..\Bugs_Tabs.pas',
  Matriz in '..\..\..\..\Outros\Empresa\Matriz.pas' {FmMatriz},
  OSCab in '..\OSCab.pas' {FmOSCab},
  PediPrzCab1 in '..\..\..\..\Grade\PediPrzCab1.pas' {FmPediPrzCab1},
  PediPrzIts1 in '..\..\..\..\Grade\PediPrzIts1.pas' {FmPediPrzIts1},
  MasterSelFilial in '..\..\..\..\Outros\Empresa\MasterSelFilial.pas' {FmMasterSelFilial},
  Grade_Tabs in '..\..\..\..\Grade\Grade_Tabs.pas',
  OSSrv in '..\OSSrv.pas' {FmOSSrv},
  OSDep in '..\OSDep.pas' {FmOSDep},
  OSRec in '..\OSRec.pas' {FmOSRec},
  ModOS in '..\ModOS.pas' {DmModOS: TDataModule},
  CashTabs in '..\..\..\..\Financas\Units\CashTabs.pas',
  CreateBugs in '..\CreateBugs.pas',
  OSAlv in '..\OSAlv.pas' {FmOSAlv},
  OSFrmCab in '..\OSFrmCab.pas' {FmOSFrmCab},
  OSFrmRec in '..\OSFrmRec.pas' {FmOSFrmRec},
  CadLista in '..\..\..\..\Outros\MySQL\CadLista.pas' {FmCadLista},
  CfgCadLista in '..\..\..\..\Outros\MySQL\CfgCadLista.pas',
  OSMonCab in '..\OSMonCab.pas' {FmOSMonCab},
  OSMonRec in '..\OSMonRec.pas' {FmOSMonRec},
  OSPsq in '..\OSPsq.pas' {FmOSPsq};

{$R *.res}

const
  NomeForm = 'TFmBugsurvey_Dmk';
var
  Form : PChar;
begin
  Form := PChar(NomeForm);
  if OpenMutex(MUTEX_ALL_ACCESS, False, Form) = 0 then
  begin
    CreateMutex(nil, False, Form);
    Application.Initialize;
    Application.Name := 'Bugsurvey';
    Application.Title := 'Bugsurvey';
    try
      Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmMyGlyfs, FmMyGlyfs);
  Application.CreateForm(TFmLinkRankSkin, FmLinkRankSkin);
  Application.CreateForm(TGMod, GMod);
  Application.CreateForm(TFmMeuDBUses, FmMeuDBUses);
  Application.CreateForm(TFmBugsurvey_Dmk, FmBugsurvey_Dmk);
  Application.CreateForm(TDmModOS, DmModOS);
  except
      Application.Terminate;
      Exit;
    end;
    Application.Run;
  end
  else
  begin
    Geral.MensagemBox('O Aplicativo j� est� em uso.', 'Erro',
    MB_OK+MB_ICONERROR);
    SetForeGroundWindow(FindWindow(NomeForm, Form));
  end;
end.
