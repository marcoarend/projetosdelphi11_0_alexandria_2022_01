unit MyListas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Dialogs, Forms,
  ExtCtrls, ComCtrls, StdCtrls, DB, (*DBTables,*) mysqlDBTables, UnMyLinguas,
  UnInternalConsts, UnMLAGeral, dmkGeral, UnDmkProcFunc;

type
  TMyListas = class(TObject)
  private
    { Private declarations }

  public
    { Public declarations }
    function CriaListaImpDOS(FImpDOS: TStringList): Boolean;
    function CriaListaUserSets(FUserSets: TStringList): Boolean;
    //
    function CriaListaJanelas(FLJanelas: TList): Boolean;
    function CriaListaTabelas(Database: TmySQLDatabase; Lista:
             TList): Boolean;
    //function CriaListaTabelas(Database: TmySQLDatabase; FTabelas: TStringList): Boolean;
    function CriaListaTabelasLocais(Lista: TList): Boolean;
    function CriaListaIndices(TabelaBase, TabelaNome: String; FLIndices: TList):
             Boolean;
    function CriaListaCampos(Tabela: String; FLCampos: TList; var TemControle: TTemControle): Boolean;
    function CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function CriaListaReplicacoes(Database: TmySQLDatabase; Lista: TList): Boolean;
    function CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
    function ExcluiTab: Boolean;
    function ExcluiReg: Boolean;
    function ExcluiIdx: Boolean;
    procedure VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
              DataBase: TmySQLDatabase; Memo: TMemo);
    procedure ConfiguracoesIniciais(UsaCoresRel: integer; AppIDtxt: String);
  end;

const
  CO_VERSAO = 1205281121;
  CO_SIGLA_APP = 'BUGY';
  CO_DMKID_APP = 26;
  CO_GRADE_APP = False;
  CO_VLOCAL = False;
  CO_EXTRA_LCT_003 = False; // True somente para Credito2

var
  MyList: TMyListas;
  FRCampos  : TCampos;
  FRIndices : TIndices;
  FRJanelas : TJanelas;
  //
  _ArrClieSets: array[01..32] of String;
  _MaxClieSets: Integer;
  _ArrFornSets: array[01..32] of String;
  _MaxFornSets: Integer;

implementation

uses CashTabs, PerfJan_Tabs, Bugs_Tabs, Grade_Tabs;

function TMyListas.CriaListaTabelas(Database: TmySQLDatabase; Lista:
 TList): Boolean;
begin
  Result := True;
  try
    if Database = Dmod.MyDB then
    begin
      // N�o ativar! N�o usa todas tabelas!
      //CashTb.CarregaListaTabelas(FTabelas);

      // N�o ativar! N�o usa todas tabelas!
      //Grade_Tb.CarregaListaTabelas(FTabelas);

      UnBugs_Tabs.CarregaListaTabelas(Lista);
      UnPerfJan_Tabs.CarregaListaTabelas(Lista);
      //
      MyLinguas.AdTbLst(Lista, False, 'controle', '');
      /////////////////////////
      MyLinguas.AdTbLst(Lista, False, 'EntiContat', '');
      /////////////////////////
      MyLinguas.AdTbLst(Lista, False, 'PediPrzCab', '');
      // Ver o que fazer
      MyLinguas.AdTbLst(Lista, False, 'entidades', '');
      MyLinguas.AdTbLst(Lista, False, Lowercase('Master'), '');
    end;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaTabelasLocais(Lista: TList): Boolean;
begin
  Result := True;
  try
    //.>CashTb.CarregaListaTabelasLocaisCashier(Lista);
    //FtabelasLocais.Add('');
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    {if Uppercase(Tabela) = Uppercase('Bancos') then
    begin
      FListaSQL.Add('Codigo,Nome');
      FListaSQL.Add('0,""');
    end else }
    CashTb.CarregaListaSQLCashier(Tabela, FListaSQL);
    Grade_Tb.CarregaListaSQL(Tabela, FListaSQL);
    UnBugs_Tabs.CarregaListaSQL(Tabela, FListaSQL);
    UnPerfJan_Tabs.CarregaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CompletaListaSQL(Tabela: String; FListaSQL: TStringList): Boolean;
begin
  Result := True;
  try
    if Uppercase(Tabela) = Uppercase('Contas') then
    begin
      (*FListaSQL.Add('-129,"Compra de mercadorias diversas"');
      FListaSQL.Add('-130,"Frete de mercadorias"');
      FListaSQL.Add('-131,"Compra de filmes"');
      FListaSQL.Add('-132,"Frete de filmes"');
      FListaSQL.Add('-133,"Inv�lido"');
      FListaSQL.Add('-134,"Venda e/ou loca��o"');*)
    end;
    UnBugs_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
    UnPerfJan_Tabs.ComplementaListaSQL(Tabela, FListaSQL);
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaIndices(TabelaBase, TabelaNome: String; FLIndices: TList): Boolean;
begin
  Result := True;
  try
    if Uppercase(TabelaBase) = Uppercase('XXXX') then
    begin
      {New(FRIndices);
      FRIndices.Non_unique    := 0;
      FRIndices.Key_name      := 'PRIMARY';
      FRIndices.Seq_in_index  := 1;
      FRIndices.Column_name   := 'Codigo';
      FLIndices.Add(FRIndices);}
      //
    end else
    begin
      CashTb.CarregaListaFRIndicesCashier(TabelaBase, TabelaNome, FRIndices, FLindices);
      Grade_Tb.CarregaListaFRIndices(TabelaBase, FRIndices, FLindices);
      UnBugs_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
      UnPerfJan_Tabs.CarregaListaFRIndices(TabelaBase, FRIndices, FLIndices);
    end;
  except
    raise;
    Result := False;
  end;
end;

procedure TMyListas.ConfiguracoesIniciais(UsaCoresRel: integer;
  AppIDtxt: String);
begin
  dmkPF.ConfigIniApp(UsaCoresRel);
  if Uppercase(AppIDtxt) = 'BUGSURVEY' then
  begin
    //n�o h� outro al�m de cliente1, fornece1 e terceiro;
  end else  
    Geral.MensagemBox(
    'Database para configura��es de "CheckBox" n�o definidos!', 'Aviso!',
    MB_OK+MB_ICONWARNING)

end;

function TMyListas.CriaListaCampos(Tabela: String; FLCampos: TList;
var TemControle: TTemControle): Boolean;
begin
  try
    if Uppercase(Tabela) = Uppercase('controle') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
{      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Versao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Dono';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '-11';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNPJ';
      FRCampos.Tipo       := 'varchar(18)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ContraSenha';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MeuLogoPath';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
}
    end else
    if Uppercase(Tabela) = Uppercase('entidades') then
    begin
      TemControle := TemControle + cTemControleSim;
      //
      New(FRCampos);
      FRCampos.Field      := 'Codigo';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CodUsu';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Tipo';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Filial';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNPJ';
      FRCampos.Tipo       := 'varchar(18)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CPF';
      FRCampos.Tipo       := 'varchar(18)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'RazaoSocial';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Nome';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CliInt';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
    end else
    if Uppercase(Tabela) = Uppercase('Master') then
    begin
      TemControle := TemControle + cTemControleNao;
      //
      New(FRCampos);
      FRCampos.Field      := 'SolicitaSenha';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '1';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Em';
      FRCampos.Tipo       := 'varchar(100)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'CNPJ';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Monitorar';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Licenca';
      FRCampos.Tipo       := 'varchar(50)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Distorcao';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataI';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DataF';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Hoje';
      FRCampos.Tipo       := 'date';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Hora';
      FRCampos.Tipo       := 'time';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MasLogin';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MasSenha';
      FRCampos.Tipo       := 'varchar(30)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'MasAtivar';
      FRCampos.Tipo       := 'char(1)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := 'F';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'Limite';
      FRCampos.Tipo       := 'tinyint(2)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'SitSenha';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
{
      //
      New(FRCampos);
      FRCampos.Field      := 'Dono';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '-11';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
}
      //
      New(FRCampos);
      FRCampos.Field      := 'ChekF';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'DiasExtra';
      FRCampos.Tipo       := 'int(11)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'ChekMonit';
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LicTel';
      FRCampos.Tipo       := 'varchar(40)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LicMail';
      FRCampos.Tipo       := 'varchar(255)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'LicInfo'; // dados de licen�a gerada por contato (sem net)
      FRCampos.Tipo       := 'varchar(32)';
      FRCampos.Null       := 'YES';
      FRCampos.Key        := '';
      FRCampos.Default    := '';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
      New(FRCampos);
      FRCampos.Field      := 'UsaAccMngr';
      FRCampos.Tipo       := 'tinyint(1)';
      FRCampos.Null       := '';
      FRCampos.Key        := '';
      FRCampos.Default    := '0';
      FRCampos.Extra      := '';
      FLCampos.Add(FRCampos);
      //
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    end else
    begin
      CashTb.CarregaListaFRCamposCashier(Tabela, FRCampos, FLCampos, TemControle);
      Grade_Tb.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnPerfJan_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
      UnBugs_Tabs.CarregaListaFRCampos(Tabela, FRCampos, FLCampos, TemControle);
    end;
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
    CashTb.CompletaListaFRCamposCashier(Tabela, FRCampos, FLCampos);
    Grade_Tb.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnPerfJan_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    UnBugs_Tabs.CompletaListaFRCampos(Tabela, FRCampos, FLCampos);
    Result := True;
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.ExcluiTab: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiReg: Boolean;
begin
  Result := True;
end;

function TMyListas.ExcluiIdx: Boolean;
begin
  Result := True;
end;

function TMyListas.CriaListaUserSets(FUserSets: TStringList): Boolean;
begin
  Result := True;
  try
    FUserSets.Add('Edi��o de Itens de Mercadoria;C�digos Fiscais');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Representante');
    FUserSets.Add('Edi��o de Itens de Mercadoria;Comiss�o de Vendedor');
    FUserSets.Add('Edi��o de Itens de Mercadoria;IPI');
    FUserSets.Add('Edi��o de Itens de Mercadoria;ICMS');
    //
  except
    raise;
    Result := False;
  end;
end;

function TMyListas.CriaListaImpDOS(FImpDOS: TStringList): Boolean;
begin
  Result := True;
  try
    FImpDOS.Add('1001;01;NF (Topo)');
    FImpDOS.Add('1002;01;Sa�da [X]');
    FImpDOS.Add('1003;01;Entrada [X]');
    FImpDOS.Add('1004;01;Data emiss�o');
    FImpDOS.Add('1005;01;Data entra/sai');
    FImpDOS.Add('1006;01;C�digo CFOP');
    FImpDOS.Add('1007;01;Descri��o CFOP');
    FImpDOS.Add('1008;01;Base c�lculo ICMS');
    FImpDOS.Add('1009;01;Valor ICMS');
    FImpDOS.Add('1010;01;Base c�lc. ICMS subst.');
    FImpDOS.Add('1011;01;Valor ICMS subst.');
    FImpDOS.Add('1012;01;Valor frete');
    FImpDOS.Add('1013;01;Valor seguro');
    FImpDOS.Add('1014;01;Outras desp. aces.');
    FImpDOS.Add('1015;01;Valor total IPI');
    FImpDOS.Add('1016;01;Valor total produtos');
    FImpDOS.Add('1017;01;Valor total servicos');
    FImpDOS.Add('1018;01;Valor total nota');
    FImpDOS.Add('1019;01;Placa ve�culo');
    FImpDOS.Add('1020;01;UF placa ve�culo');
    FImpDOS.Add('1021;01;Vol. Quantidade');
    FImpDOS.Add('1022;01;Vol. Esp�cie');
    FImpDOS.Add('1023;01;Vol. Marca');
    FImpDOS.Add('1024;01;Vol. N�mero');
    FImpDOS.Add('1025;01;Vol. kg bruto');
    FImpDOS.Add('1026;01;Vol. kg l�quido');
    FImpDOS.Add('1027;01;Dados adicionais');
    FImpDOS.Add('1028;01;Frete por conta de ...');
    FImpDOS.Add('1029;01;Desconto especial');
    FImpDOS.Add('1030;01;NF (rodap�)');
    //
    FImpDOS.Add('2001;02;Nome ou Raz�o Social');
    FImpDOS.Add('2002;02;CNPJ ou CPF');
    FImpDOS.Add('2003;02;Endere�o');
    FImpDOS.Add('2004;02;Bairro');
    FImpDOS.Add('2005;02;CEP');
    FImpDOS.Add('2006;02;Cidade');
    FImpDOS.Add('2007;02;Telefone');
    FImpDOS.Add('2008;02;UF');
    FImpDOS.Add('2009;02;I.E. ou RG');
    FImpDOS.Add('2010;02;I.E.S.T.');
    //
    FImpDOS.Add('6001;06;Nome ou Raz�o Social');
    FImpDOS.Add('6002;06;CNPJ ou CPF');
    FImpDOS.Add('6003;06;Endere�o');
    FImpDOS.Add('6004;06;Bairro');
    FImpDOS.Add('6005;06;CEP');
    FImpDOS.Add('6006;06;Cidade');
    FImpDOS.Add('6007;06;Telefone');
    FImpDOS.Add('6008;06;UF');
    FImpDOS.Add('6009;06;I.E. ou RG');
    FImpDOS.Add('6010;06;I.E.S.T.');
    //
    FImpDOS.Add('3001;03;Descri��o');
    FImpDOS.Add('3002;03;Classifica��o Fiscal');
    FImpDOS.Add('3003;03;Situa��o Tribut�ria');
    FImpDOS.Add('3004;03;Unidade');
    FImpDOS.Add('3005;03;Quantidade');
    FImpDOS.Add('3006;03;Valor unit�rio');
    FImpDOS.Add('3007;03;Valor total');
    FImpDOS.Add('3008;03;Aliquota ICMS');
    FImpDOS.Add('3009;03;Aliquota IPI');
    FImpDOS.Add('3010;03;Valor IPI');
    FImpDOS.Add('3011;03;CFOP');
    FImpDOS.Add('3012;03;Refer�ncia');
    //
    FImpDOS.Add('4001;04;Descri��o');
    FImpDOS.Add('4002;04;Classifica��o Fiscal');
    FImpDOS.Add('4003;04;Situa��o Tribut�ria');
    FImpDOS.Add('4004;04;Unidade');
    FImpDOS.Add('4005;04;Quantidade');
    FImpDOS.Add('4006;04;Valor unit�rio');
    FImpDOS.Add('4007;04;Valor total');
    FImpDOS.Add('4008;04;Aliquota ICMS');
    FImpDOS.Add('4009;04;Aliquota IPI');
    FImpDOS.Add('4010;04;Valor IPI');
    FImpDOS.Add('4011;04;CFOP');
    FImpDOS.Add('4012;04;Refer�ncia');
    //
    FImpDOS.Add('5001;05;Parcela');
    FImpDOS.Add('5002;05;Valor');
    FImpDOS.Add('5003;05;Vencimento');
    //
  except
    raise;
    Result := False;
  end;
end;

procedure TMyListas.VerificaOutrosAntes(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  //
end;

procedure TMyListas.VerificaOutrosDepois(DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

procedure TMyListas.ModificaDadosDeIndicesAlterados(Indice, Tabela: String;
  DataBase: TmySQLDatabase; Memo: TMemo);
begin
  // Nada
end;

function TMyListas.CriaListaJanelas(FLJanelas: TList): Boolean;
begin
  //
  Result := True;
  //
  // REP-MySQL-001 :: Replicar Parcialmente Base de Dados
  New(FRJanelas);
  FRJanelas.ID        := 'REP-MySQL-001';
  FRJanelas.Nome      := 'FmDBReplicaParte';
  FRJanelas.Descricao := 'Replicar Parcialmente Base de Dados';
  FLJanelas.Add(FRJanelas);
  //
  Grade_Tb.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnBugs_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
  UnPerfJan_Tabs.CompletaListaFRJanelas(FRJanelas, FLJanelas);
end;

function TMyListas.CriaListaReplicacoes(Database: TmySQLDatabase;
  Lista: TList): Boolean;
begin
  Result := True;
  try
    if Database = Dmod.MyDB then
    begin
      MyLinguas.AdTbLst(Lista, False, 'Entidades' , '');
      //CashTb.CarregaListaReplicacoes(Lista);
      MyLinguas.AdTbLst(Lista, False, 'CunsCad'   , '');
      MyLinguas.AdTbLst(Lista, False, 'CunsIts'   , '');
      //
      MyLinguas.AdTbLst(Lista, False, 'SiapTerCad', '');
      MyLinguas.AdTbLst(Lista, False, 'SiapImaCad', '');
      MyLinguas.AdTbLst(Lista, False, 'SiapImaCav', '');
      MyLinguas.AdTbLst(Lista, False, 'SiapImaCdi', '');
      MyLinguas.AdTbLst(Lista, False, 'SiapImaRes', '');
      MyLinguas.AdTbLst(Lista, False, 'SiapImaCui', '');
      MyLinguas.AdTbLst(Lista, False, 'SiapImaAti', '');
      MyLinguas.AdTbLst(Lista, False, 'SiapImaDep', '');
      MyLinguas.AdTbLst(Lista, False, 'SiapImaCxa', '');
      //
      //
      MyLinguas.AdTbLst(Lista, False, 'Atividades', '');
      MyLinguas.AdTbLst(Lista, False, 'Caracteris', '');
      MyLinguas.AdTbLst(Lista, False, 'CxaMaters' , '');
      MyLinguas.AdTbLst(Lista, False, 'CxaFormas' , '');
      MyLinguas.AdTbLst(Lista, False, 'Cuidados'  , '');
      MyLinguas.AdTbLst(Lista, False, 'Dependenci', '');
      MyLinguas.AdTbLst(Lista, False, 'DependType', '');
      MyLinguas.AdTbLst(Lista, False, 'EstatusOSs', '');
      MyLinguas.AdTbLst(Lista, False, 'Finalidads', '');
      MyLinguas.AdTbLst(Lista, False, 'HowFounded', '');
      MyLinguas.AdTbLst(Lista, False, 'Objetos'   , '');
      MyLinguas.AdTbLst(Lista, False, 'Residentes', '');
      MyLinguas.AdTbLst(Lista, False, 'TpConstrus', '');
      //
      MyLinguas.AdTbLst(Lista, False, 'Praga_A', '');
      MyLinguas.AdTbLst(Lista, False, 'DesServico', '');
      MyLinguas.AdTbLst(Lista, False, 'FatoGeradr', '');
      MyLinguas.AdTbLst(Lista, False, 'Abrangicie', '');
      //
      //
      MyLinguas.AdTbLst(Lista, False, 'AtrSICdCad', '');
      MyLinguas.AdTbLst(Lista, False, 'AtrSICdIts', '');
      MyLinguas.AdTbLst(Lista, False, 'AtrSICdDef', '');
      //
      MyLinguas.AdTbLst(Lista, False, 'AtrSICxCad', '');
      MyLinguas.AdTbLst(Lista, False, 'AtrSICxIts', '');
      MyLinguas.AdTbLst(Lista, False, 'AtrSICxDef', '');

      /////////////////////////
      MyLinguas.AdTbLst(Lista, False, 'PediPrzCab', '');

      /////////////////////////
      MyLinguas.AdTbLst(Lista, False, 'EntiContat', '');

    end;
  except
    raise;
    Result := False;
  end;
end;

end.
