object Dmod: TDmod
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  Height = 499
  Width = 799
  object MyDB: TmySQLDatabase
    DatabaseName = 'bugsurvey'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=bugsurvey'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    Left = 12
    Top = 3
  end
  object QrAux: TmySQLQuery
    Database = MyDB
    Left = 176
    Top = 364
  end
  object QrUpd: TmySQLQuery
    Database = MyDB
    Left = 176
    Top = 320
  end
  object QrMas: TmySQLQuery
    Database = MyDB
    Left = 148
    Top = 320
  end
  object MyLocDatabase: TmySQLDatabase
    DatabaseName = 'locbugsy'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=locbugsy'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    Left = 156
    Top = 7
  end
  object QrControle: TmySQLQuery
    Database = MyDB
    AfterOpen = QrControleAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM Controle')
    Left = 12
    Top = 287
    object QrControleLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'controle.Lk'
    end
    object QrControleDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'controle.DataCad'
    end
    object QrControleDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'controle.DataAlt'
    end
    object QrControleUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'controle.UserCad'
    end
    object QrControleUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'controle.UserAlt'
    end
    object QrControleCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'controle.Codigo'
    end
    object QrControleAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'controle.AlterWeb'
    end
    object QrControleAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'controle.Ativo'
    end
    object QrControleDono: TIntegerField
      FieldName = 'Dono'
    end
    object QrControleContraSenha: TWideStringField
      DisplayWidth = 50
      FieldName = 'ContraSenha'
      Size = 50
    end
    object QrControleMeuLogoPath: TWideStringField
      FieldName = 'MeuLogoPath'
      Size = 255
    end
  end
  object QrUpdU: TmySQLQuery
    Database = MyDB
    Left = 12
    Top = 47
  end
  object ZZDB: TmySQLDatabase
    DatabaseName = 'bugstrol'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1'
      'DatabaseName=bugstrol')
    Left = 96
    Top = 3
  end
  object QrIdx: TmySQLQuery
    Database = MyDB
    Left = 120
    Top = 320
  end
  object QrSQL: TmySQLQuery
    Database = MyDB
    Left = 120
    Top = 364
  end
  object QrNTV: TmySQLQuery
    Database = MyLocDatabase
    Left = 60
    Top = 4
  end
  object QrNTI: TmySQLQuery
    Database = MyLocDatabase
    Left = 60
    Top = 48
  end
  object QrPriorNext: TmySQLQuery
    Database = MyDB
    Left = 148
    Top = 364
  end
  object QrTerminais: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM Terminais')
    Left = 292
    Top = 228
    object QrTerminaisIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminaisLicenca: TWideStringField
      FieldName = 'Licenca'
      Size = 50
    end
    object QrTerminaisTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrTerminal: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM Terminais'
      'WHERE IP=:P0')
    Left = 292
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerminalIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminalTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrMaster: TmySQLQuery
    Database = MyDB
    AfterOpen = QrMasterAfterOpen
    OnCalcFields = QrMasterCalcFields
    SQL.Strings = (
      'SELECT ma.Em, te.Tipo, te.Logo,  te.Logo2,cm.Dono, cm.Versao, '
      'cm.CNPJ, te.IE, te.ECidade, uf.Nome NOMEUF, te.EFax,'
      
        'te.ERua, (te.ENumero+0.000) ENumero, te.EBairro, te.ECEP, te.ECo' +
        'mpl,'
      'te.EContato, te.ECel, te.ETe1, te.ETe2, te.ETe3, te.EPais,'
      'te.Respons1, te.Respons2, ma.Limite, ma.SolicitaSenha,'
      'ma.UsaAccMngr'
      'FROM Entidades te, Controle cm, Ufs uf, Master ma'
      'WHERE te.Codigo=cm.Dono'
      
        'AND ((te.CNPJ=cm.CNPJ AND te.Tipo=0) OR (te.CPF=cm.CNPJ AND te.T' +
        'ipo=1))'
      'AND uf.Codigo=te.EUF'
      '')
    Left = 12
    Top = 143
    object QrMasterCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterEm: TWideStringField
      FieldName = 'Em'
      Origin = 'master.Em'
      Required = True
      Size = 100
    end
    object QrMasterTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrMasterDono: TIntegerField
      FieldName = 'Dono'
      Origin = 'controle.Dono'
      Required = True
    end
    object QrMasterVersao: TIntegerField
      FieldName = 'Versao'
      Origin = 'controle.Versao'
      Required = True
    end
    object QrMasterCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'controle.CNPJ'
      Required = True
      Size = 18
    end
    object QrMasterLimite: TSmallintField
      FieldName = 'Limite'
      Origin = 'master.Limite'
      Required = True
    end
    object QrMasterSolicitaSenha: TIntegerField
      FieldName = 'SolicitaSenha'
      Origin = 'master.SolicitaSenha'
    end
    object QrMasterUsaAccMngr: TSmallintField
      FieldName = 'UsaAccMngr'
    end
  end
  object frxDsMaster: TfrxDBDataset
    UserName = 'frxDsMaster'
    CloseDataSource = False
    DataSet = QrMaster
    BCDToCurrency = False
    Left = 60
    Top = 144
  end
  object QrAgora: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 56
    Top = 95
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
    object QrAgoraAGORA: TDateTimeField
      FieldName = 'AGORA'
      Required = True
    end
  end
  object QrOpcoesBugs: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM opcoesbugs')
    Left = 728
    Top = 424
    object QrOpcoesBugsGraNivEqAp: TSmallintField
      FieldName = 'GraNivEqAp'
    end
    object QrOpcoesBugsGraCodEqAp: TIntegerField
      FieldName = 'GraCodEqAp'
    end
    object QrOpcoesBugsGraNivEqMo: TSmallintField
      FieldName = 'GraNivEqMo'
    end
    object QrOpcoesBugsGraCodEqMo: TIntegerField
      FieldName = 'GraCodEqMo'
    end
  end
end
