unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnMLAGeral, ExtCtrls, Menus, Grids, DBGrids, Db, (*DBTables,*) TypInfo, StdCtrls,
  ZCF2, ToolWin, ComCtrls, UnInternalConsts, ExtDlgs, jpeg, Buttons,
  WinSkinStore, WinSkinData, Mask, DBCtrls, Tabs, DockTabSet, AdvToolBar,
  AdvGlowButton, AdvMenus, dmkGeral, AdvToolBarStylers,
  AdvShapeButton, AdvPreviewMenu, AdvOfficeHint, dmkDBGrid, dmkEdit,
  UnMyObjects, CfgCadLista;

type
  TcpCalc = (cpJurosMes, cpMulta);
  TFmPrincipal = class(TForm)
    OpenPictureDialog1: TOpenPictureDialog;
    Timer1: TTimer;
    sd1: TSkinData;
    SkinStore1: TSkinStore;
    StatusBar: TStatusBar;
    PMGeral: TPopupMenu;
    Entidades2: TMenuItem;
    N1: TMenuItem;
    Reabrirtabelas1: TMenuItem;
    TimerIdle: TTimer;
    OpenDialog1: TOpenDialog;
    AdvToolBarPager1: TAdvToolBarPager;
    AdvToolBarPager12: TAdvPage;
    AdvToolBarPager13: TAdvPage;
    AdvToolBar2: TAdvToolBar;
    AGBCadImporta: TAdvGlowButton;
    AdvToolBar4: TAdvToolBar;
    AGBOS: TAdvGlowButton;
    AdvToolBar9: TAdvToolBar;
    AdvPMImagem: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    Estilo2: TMenuItem;
    Centralizado1: TMenuItem;
    ManterProporo1: TMenuItem;
    ManterAltura1: TMenuItem;
    ManterLargura1: TMenuItem;
    Nenhum1: TMenuItem;
    Espichar1: TMenuItem;
    Repetido1: TMenuItem;
    AdvPMMenuCor: TAdvPopupMenu;
    Padro3: TMenuItem;
    Office20071: TMenuItem;
    Dermatek1: TMenuItem;
    Preto1: TMenuItem;
    Azul1: TMenuItem;
    Cinza1: TMenuItem;
    Verde1: TMenuItem;
    Prscia1: TMenuItem;
    WhidbeyStyle1: TMenuItem;
    WindowsXP1: TMenuItem;
    AdvToolBarOfficeStyler1: TAdvToolBarOfficeStyler;
    AdvShapeButton1: TAdvShapeButton;
    AdvQuickAccessToolBar1: TAdvQuickAccessToolBar;
    ATB_VerificaDB: TAdvToolBarButton;
    ATB_Atualiza: TAdvToolBarButton;
    ATB_Login: TAdvToolBarButton;
    AdvPreviewMenu1: TAdvPreviewMenu;
    AdvToolBar11: TAdvToolBar;
    AGBMenuAdv: TAdvGlowMenuButton;
    AdvOfficeHint1: TAdvOfficeHint;
    AGBEmpresa: TAdvGlowButton;
    Automatico1: TMenuItem;
    Panel1: TPanel;
    Panel16: TPanel;
    LaAviso1: TLabel;
    PB1: TProgressBar;
    AGBTheme: TAdvGlowButton;
    AGBAtualiza: TAdvGlowButton;
    AdvToolBar1: TAdvToolBar;
    AGBBackup: TAdvGlowButton;
    Memo3: TMemo;
    Image1: TImage;
    ImgPrincipal: TImage;
    AGBValidaXML: TAdvGlowButton;
    AdvToolBar3: TAdvToolBar;
    AGBSobre: TAdvGlowButton;
    ATB_Backup: TAdvToolBarButton;
    ImgAbout: TImage;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TimerIdleTimer(Sender: TObject);
    procedure AGBAtualizaClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure Padro3Click(Sender: TObject);
    procedure ATB_AtualizaClick(Sender: TObject);
    procedure Automatico1Click(Sender: TObject);
    procedure AGBCadImportaClick(Sender: TObject);
    procedure AGBOSClick(Sender: TObject);
    procedure AGBThemeClick(Sender: TObject);
    procedure Escolher2Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure AGBEmpresaClick(Sender: TObject);
    procedure AGBValidaXMLClick(Sender: TObject);
    procedure AGBBackupClick(Sender: TObject);
    procedure AGBSobreClick(Sender: TObject);
    procedure ATB_LoginClick(Sender: TObject);
    procedure ATB_BackupClick(Sender: TObject);
    procedure ATB_VerificaDBClick(Sender: TObject);
  private
    { Private declarations }
    FALiberar: Boolean;
    //
    procedure SkinMenu(Index: integer);
    //function  Backup_BD_XML(): String;

  public
    { Public declarations }
    procedure AcoesIniciaisDoAplicativo;
    procedure AppIdle(Sender: TObject; var Done: Boolean);
    procedure ShowHint(Sender: TObject);

    procedure SelecionaImagemdefundo;
    procedure AjustaEstiloImagem(Index: integer);
    procedure MostraVerifiDB();
    procedure MostraMatriz();
    procedure MostraFormGraGruN();
    procedure MostraFormFormulas();

    function  VerificaNovasVersoes(): Integer;
    //procedure PerguntaBackup();
    procedure ReCaptionComponentesDeForm(Form: TForm);
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses MyListas, Module, MyGlyfs, Bugsurvey_Dmk, LinkRankSkin, NovaVersao,
  ZipForge, UnDmkWeb, ZForge, ABOUT, DBReplicaParte, VerifiDB, MyDBCheck,
  Matriz, OSCab;

const
  FAltLin = CO_POLEGADA / 2.16;

{$R *.DFM}

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  APP_LIBERADO := True;
  MyObjects.CorIniComponente();
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Geral.VersaoTxt2006(CO_VERSAO) <>
    Geral.FileVerInfo(Application.ExeName, 3 (*Versao*)) then
    Geral.MensagemBox('Vers�o difere do arquivo',
    'Aviso', MB_OK+MB_ICONWARNING);
  if not FALiberar then Timer1.Enabled := True;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  MenuStyle: Integer;
  Imagem(*, Avisa*): String;
begin
  VAR_ForcaBigIntNeg := True;
  VAR_DERMA_AD := 2;
  //
  FM_MASTER := 'V';
  VAR_LOGIN := 'MASTER';
  //
  Geral.DefineFormatacoes();
  AdvToolBarPager1.ActivePageIndex := 0;
  //
  VAR_CAMINHOSKINPADRAO := 'C:\Dermatek\Skins\VCLSkin\Office 2007.skn';
  VAR_TYPE_LOG := ttlFiliLog;
  VAR_USA_TAG_BITBTN := True;
  VAR_SERVIDOR3 := 3;
  VAR_STLOGIN       := StatusBar.Panels[01];
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  VAR_STTERMINAL    := StatusBar.Panels[05];
  VAR_STDATALICENCA := StatusBar.Panels[07];
  //VAR_STAVISOS      := StatusBar.Panels[09];
  VAR_SKINUSANDO    := StatusBar.Panels[09];
  VAR_STDATABASES   := StatusBar.Panels[11];
  VAR_TIPOSPRODM_TXT := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  VAR_APP := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;
  Application.OnMessage := MyObjects.FormMsg;
  Imagem := Geral.ReadAppKeyCU(
    'ImagemFundo', Application.Title, ktString, VAR_APP+'Fundo.jpg');
  if FileExists(Imagem) then
  begin
    ImgPrincipal.Picture.LoadFromFile(Imagem);
    AjustaEstiloImagem(ImgPrincipal.Tag);
  end;
  //
  MenuStyle := Geral.ReadAppKeyCU('MenuStyle', Application.Title,
    ktInteger, Integer(AdvToolBarOfficeStyler1.Style));
  SkinMenu(MenuStyle);
  //
  VAR_CAD_POPUP := PMGeral;
  //////////////////////////////////////////////////////////////////////////////
  AjustaEstiloImagem(-1);
  //
  Application.OnHint      := ShowHint;
  Application.OnException := MyObjects.MostraErro;
  Application.OnMessage   := MyObjects.FormMsg;
  Application.OnIdle      := AppIdle;
end;

procedure TFmPrincipal.SelecionaImagemdefundo;
begin
  if OpenPictureDialog1.Execute then
  begin
    ImgPrincipal.Picture.LoadFromFile(OpenpictureDialog1.FileName);
    Geral.WriteAppKeyCU('ImagemFundo', Application.Title, OpenPictureDialog1.FileName, ktString);
    AjustaEstiloImagem(ImgPrincipal.Tag);
  end;
end;

procedure TFmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //PerguntaBackup();
  Application.Terminate;
end;

procedure TFmPrincipal.AGBEmpresaClick(Sender: TObject);
begin
  MostraMatriz();
end;

procedure TFmPrincipal.AGBCadImportaClick(Sender: TObject);
var
  IP: String;
begin
  IP := Geral.ReadAppKey('IPServer', 'bugstrol', ktString, CO_VAZIO, HKEY_LOCAL_MACHINE);
  if MyObjects.CriaForm_AcessoTotal(TFmDBReplicaParte, FmDBReplicaParte) then
  begin
    FmDBReplicaParte.EdSorcDatabaseName.Text := 'bugstrol';
    FmDBReplicaParte.EdSorcHost.Text := VAR_IP;
    FmDBReplicaParte.EdSorcPorta.ValueVariant := VAR_PORTA;
    //
    FmDBReplicaParte.EdDestDatabaseName.Text := 'bugsurvey';
    FmDBReplicaParte.EdDestHost.Text := IP;
    FmDBReplicaParte.EdDestPorta.ValueVariant := VAR_PORTA;
    //
    if FmDBReplicaParte.EdSorcHost.Text = '' then
      FmDBReplicaParte.EdSorcHost.Text := '127.0.0.1';
    if FmDBReplicaParte.EdSorcPorta.ValueVariant = 0 then
      FmDBReplicaParte.EdSorcPorta.ValueVariant := 3306;
    //
    if FmDBReplicaParte.EdDestHost.Text = '' then
      FmDBReplicaParte.EdDestHost.Text := '127.0.0.1';
    if FmDBReplicaParte.EdDestPorta.ValueVariant = 0 then
      FmDBReplicaParte.EdDestPorta.ValueVariant := 3306;
    //
    FmDBReplicaParte.ShowModal;
    FmDBReplicaParte.Destroy;
  end;
end;

procedure TFmPrincipal.AGBSobreClick(Sender: TObject);
begin
  Geral.ShowAboutBox(CO_VERSAO, ImgAbout, Application.Title,
    'Aplicativo auxiliar para gera��o de or�amento.');
end;

procedure TFmPrincipal.AGBOSClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOSCab, FmOSCab, afmoSoBoss) then
  begin
    FmOSCab.ShowModal;
    FmOSCab.Destroy;
  end;
end;

procedure TFmPrincipal.AGBValidaXMLClick(Sender: TObject);
begin
{
  if MyObjects.CriaForm_AcessoTotal(TFmValidaXML_001, FmValidaXML_001)  then
  begin
    FmValidaXML_001.ShowModal;
    FmValidaXML_001.Destroy;
  end;
}
end;

procedure TFmPrincipal.AGBAtualizaClick(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.AGBBackupClick(Sender: TObject);
begin
  //Backup_BD_XML();
end;

procedure TFmPrincipal.ATB_AtualizaClick(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.ATB_BackupClick(Sender: TObject);
begin
  //Backup_BD_XML();
end;

procedure TFmPrincipal.ATB_LoginClick(Sender: TObject);
begin
  Geral.MensagemBox('Este aplicativo n�o possui prote��o por senha!',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmPrincipal.ATB_VerificaDBClick(Sender: TObject);
begin
  MostraVerifiDB();
end;

procedure TFmPrincipal.AGBThemeClick(Sender: TObject);
begin
  FmLinkRankSkin.Show;
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmBugsurvey_Dmk.Show;
  Enabled := False;
  MyObjects.Informa2(FmBugsurvey_Dmk.LaAviso1, FmBugsurvey_Dmk.LaAviso2, True, 'Carregando m�dulo de dados');
  FmBugsurvey_Dmk.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo de dados', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  FmBugsurvey_Dmk.Refresh;
  //FmBugsurvey_Dmk.Timer1.Enabled := True;
end;

procedure TFmPrincipal.Padro3Click(Sender: TObject);
begin
  Geral.WriteAppKeyCU('MenuStyle', Application.Title,
    TMenuItem(Sender).Tag, ktInteger);
  SkinMenu(TMenuItem(Sender).Tag);
end;

{
procedure TFmPrincipal.PerguntaBackup();
begin
  if Dmod.FMudouXML then
  begin
    if Geral.MensagemBox(
    '� altamente recomend�vel fazer um backup (c�pia) dos dados!' + #13#10 +
    'Deseja faz�-lo agora?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      Backup_BD_XML();
  end;
end;
}

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm);
begin
  // N�o usa ainda!
end;

procedure TFmPrincipal.AjustaEstiloImagem(Index: integer);
{
var
 Indice: Integer;
}
begin
{
  if Index >= 0 then Indice := Index else
  Indice := Geral.ReadAppKey('Estio_ImgPrincipal', Application.Title,
    ktInteger, 1, HKEY_LOCAL_MACHINE);
  case Indice of
    00: ImgPrincipal.Style := sbAutosize;
    01: ImgPrincipal.Style := sbCenter;
    02: ImgPrincipal.Style := sbKeepAspRatio;
    03: ImgPrincipal.Style := sbKeepHeight;
    04: ImgPrincipal.Style := sbKeepWidth;
    05: ImgPrincipal.Style := sbNone;
    06: ImgPrincipal.Style := sbStretch;
    07: ImgPrincipal.Style := sbTile;
    else ImgPrincipal.Style := sbCenter;
  end;
  ImgPrincipal.Tag       := Indice;
  Automatico1.Checked    := Indice = 00;
  Centralizado1.Checked  := Indice = 01;
  ManterProporo1.Checked := Indice = 02;
  ManterAltura1.Checked  := Indice = 03;
  ManterLargura1.Checked := Indice = 04;
  Nenhum1.Checked        := Indice = 05;
  Espichar1.Checked      := Indice = 06;
  Repetido1.Checked      := Indice = 07;
  //
}
  ImgPrincipal.Invalidate;
end;

procedure TFmPrincipal.AppIdle(Sender: TObject; var Done: Boolean);
begin
  (*
  TimerIdle.Enabled := False;
  TimerIdle.Enabled := True;
  *)
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
end;

procedure TFmPrincipal.MenuItem1Click(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal.MenuItem2Click(Sender: TObject);
begin
  Geral.WriteAppKeyCU('ImagemFundo', Application.Title, '', ktString);
  Geral.MensagemBox(
  '� necess�rio reexecutar o aplicativo para que a altera��o tenha efeito!',
  'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmPrincipal.MostraFormFormulas();
begin
  UnCfgCadLista.MostraCadLista(Dmod.MyDB, 'formulas', 60, ncGerlSeq1,
  'F�rmulas', [], False, Null);
end;

procedure TFmPrincipal.MostraFormGraGruN();
begin
{$IfNDef SemGraGruN}
  if DBCheck.CriaFm(TFmGraGruN, FmGraGruN, afmoNegarComAviso) then
  begin
    FmGraGruN.ShowModal;
    FmGraGruN.Destroy;
  end;
{$EndIf}
end;

procedure TFmPrincipal.MostraMatriz();
begin
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoBoss) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
  end;
end;

procedure TFmPrincipal.MostraVerifiDB();
begin
  if DBCheck.CriaFm(TFmVerifiDB, FmVerifiDB, afmoNegarComAviso) then
  begin
    FmVerifiDB.ShowModal;
    FmVerifiDB.Destroy;
  end;
end;

procedure TFmPrincipal.SkinMenu(Index: integer);
{var
  Indice : Integer;}
begin
  {if Index >= 0 then
    Indice := Index
  else
    Indice := Geral.ReadAppKey('MenuStyle', Application.Title,
      ktInteger, 1, HKEY_LOCAL_MACHINE);}
  case Index of
    0: AdvToolBarOfficeStyler1.Style := bsOffice2003Blue;
    1: AdvToolBarOfficeStyler1.Style := bsOffice2003Classic;
    2: AdvToolBarOfficeStyler1.Style := bsOffice2003Olive;
    3: AdvToolBarOfficeStyler1.Style := bsOffice2003Silver;
    4: AdvToolBarOfficeStyler1.Style := bsOffice2007Luna;
    5: AdvToolBarOfficeStyler1.Style := bsOffice2007Obsidian;
    6: AdvToolBarOfficeStyler1.Style := bsOffice2007Silver;
    7: AdvToolBarOfficeStyler1.Style := bsOfficeXP;
    8: AdvToolBarOfficeStyler1.Style := bsWhidbeyStyle;
    9: AdvToolBarOfficeStyler1.Style := bsWindowsXP;
  end;
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo;
begin
  //DModG.MyPID_DB_Cria();
end;

procedure TFmPrincipal.FormResize(Sender: TObject);
begin
  AjustaEstiloImagem(ImgPrincipal.Tag);
end;

function TFmPrincipal.VerificaNovasVersoes(): Integer;
begin
{ TODO :        CRIAR C�digo para APLICATIVO!!! }
{
  DmkWeb.VerificaAtualizacaoVersao2(TFmNovaVersao, FmNovaVersao, True, True,
  'Bugsurvey', 'Bugsurvey', CO_VERSAO, CO_DMKID_APP, Memo3, dtExec);
}
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
{
var
  Dia: Integer;
}
begin
{
  TimerIdle.Enabled := False;
  Dia := Geral.ReadAppKey('VeriNetVersao', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if (Dia > 2) and (Dia < Int(Date)) then
  begin
    if VerificaNovasVersoes < 1 then Application.Terminate;
  end else Application.Terminate;
}
end;

procedure TFmPrincipal.Automatico1Click(Sender: TObject);
begin
  Geral.WriteAppKey('Estio_ImgPrincipal', Application.Title,
    TMenuItem(Sender).Tag, ktInteger, HKEY_LOCAL_MACHINE);
  AjustaEstiloImagem(TMenuItem(Sender).Tag);
end;

{
function TFmPrincipal.Backup_BD_XML(): String;
var
  MyCursor: TCursor;
  Arq: String;
begin
  MyCursor := Screen.Cursor;
  if MyObjects.CriaForm_AcessoTotal(TFmZForge, FmZForge) then
  begin
    FmZForge.Show;
    Arq := 'Backup_' + FormatDateTime('YYYYMMDD"_"HHNNSS', Now());
    FmZForge.ZipaArquivo(1, FAppDirIO, FAppDirBD,
      Arq, '', False, False);
    FmZForge.Destroy;
    Result := FAppDirIO + Arq;
  end;
  Screen.Cursor := MyCursor;
  if MyObjects.CriaForm_AcessoTotal(TFmBackupDB_XML, FmBackupDB_XML)  then
  begin
    FmBackupDB_XML.ShowModal;
    FmBackupDB_XML.Destroy;
  end;
end;
}

procedure TFmPrincipal.Escolher2Click(Sender: TObject);
begin

end;


{
procedure Antialiasing(Image: TImage; Percent: Integer);
type
  TRGBTripleArray = array[0..32767] of TRGBTriple;
  PRGBTripleArray = ^TRGBTripleArray;
var
  SL, SL2: PRGBTripleArray;
  l, m, p: Integer;
  R, G, B: TColor;
  R1, R2, G1, G2, B1, B2: Byte;
begin
  with Image.Canvas do
  begin
    Brush.Style  := bsClear;
    Pixels[1, 1] := Pixels[1, 1];
    for l := 0 to Image.Height - 1 do
    begin
      SL := Image.Picture.Bitmap.ScanLine[l];
      for p := 1 to Image.Width - 1 do
      begin
        R1 := SL[p].rgbtRed;
        G1 := SL[p].rgbtGreen;
        B1 := SL[p].rgbtBlue;

        // Left
        if (p < 1) then m := Image.Width
        else
          m := p - 1;
        R2 := SL[m].rgbtRed;
        G2 := SL[m].rgbtGreen;
        B2 := SL[m].rgbtBlue;
        if (R1 <> R2) or (G1 <> G2) or (B1 <> B2) then
        begin
          R := Round(R1 + (R2 - R1) * 50 / (Percent + 50));
          G := Round(G1 + (G2 - G1) * 50 / (Percent + 50));
          B := Round(B1 + (B2 - B1) * 50 / (Percent + 50));
          SL[m].rgbtRed := R;
          SL[m].rgbtGreen := G;
          SL[m].rgbtBlue := B;
        end;

        //Right
        if (p > Image.Width - 2) then m := 0
        else
          m := p + 1;
        R2 := SL[m].rgbtRed;
        G2 := SL[m].rgbtGreen;
        B2 := SL[m].rgbtBlue;
        if (R1 <> R2) or (G1 <> G2) or (B1 <> B2) then
        begin
          R := Round(R1 + (R2 - R1) * 50 / (Percent + 50));
          G := Round(G1 + (G2 - G1) * 50 / (Percent + 50));
          B := Round(B1 + (B2 - B1) * 50 / (Percent + 50));
          SL[m].rgbtRed := R;
          SL[m].rgbtGreen := G;
          SL[m].rgbtBlue := B;
        end;

        if (l < 1) then m := Image.Height - 1
        else
          m := l - 1;
        //Over
        SL2 := Image.Picture.Bitmap.ScanLine[m];
        R2  := SL2[p].rgbtRed;
        G2  := SL2[p].rgbtGreen;
        B2  := SL2[p].rgbtBlue;
        if (R1 <> R2) or (G1 <> G2) or (B1 <> B2) then
        begin
          R := Round(R1 + (R2 - R1) * 50 / (Percent + 50));
          G := Round(G1 + (G2 - G1) * 50 / (Percent + 50));
          B := Round(B1 + (B2 - B1) * 50 / (Percent + 50));
          SL2[p].rgbtRed := R;
          SL2[p].rgbtGreen := G;
          SL2[p].rgbtBlue := B;
        end;

        if (l > Image.Height - 2) then m := 0
        else
          m := l + 1;
        //Under
        SL2 := Image.Picture.Bitmap.ScanLine[m];
        R2  := SL2[p].rgbtRed;
        G2  := SL2[p].rgbtGreen;
        B2  := SL2[p].rgbtBlue;
        if (R1 <> R2) or (G1 <> G2) or (B1 <> B2) then
        begin
          R := Round(R1 + (R2 - R1) * 50 / (Percent + 50));
          G := Round(G1 + (G2 - G1) * 50 / (Percent + 50));
          B := Round(B1 + (B2 - B1) * 50 / (Percent + 50));
          SL2[p].rgbtRed := R;
          SL2[p].rgbtGreen := G;
          SL2[p].rgbtBlue := B;
        end;
      end;
    end;
  end;
end;


//Example:
procedure TForm1.Button1Click(Sender: TObject);
begin
  Antialiasing(Image1, 80);
end;

}

end.
