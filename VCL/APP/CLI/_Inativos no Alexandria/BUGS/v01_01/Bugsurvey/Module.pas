unit Module;

interface

uses
  Windows, Forms, Dialogs, SysUtils, Classes, DB, mySQLDbTables, dmkGeral,
  frxClass, frxDBSet;

type
  TDmod = class(TDataModule)
    MyDB: TmySQLDatabase;
    QrAux: TmySQLQuery;
    QrUpd: TmySQLQuery;
    QrMas: TmySQLQuery;
    MyLocDatabase: TmySQLDatabase;
    QrControle: TmySQLQuery;
    QrControleLk: TIntegerField;
    QrControleDataCad: TDateField;
    QrControleDataAlt: TDateField;
    QrControleUserCad: TIntegerField;
    QrControleUserAlt: TIntegerField;
    QrControleCodigo: TIntegerField;
    QrControleAlterWeb: TSmallintField;
    QrControleAtivo: TSmallintField;
    QrUpdU: TmySQLQuery;
    ZZDB: TmySQLDatabase;
    QrIdx: TmySQLQuery;
    QrSQL: TmySQLQuery;
    QrNTV: TmySQLQuery;
    QrNTI: TmySQLQuery;
    QrPriorNext: TmySQLQuery;
    QrTerminais: TmySQLQuery;
    QrTerminaisIP: TWideStringField;
    QrTerminaisLicenca: TWideStringField;
    QrTerminaisTerminal: TIntegerField;
    QrTerminal: TmySQLQuery;
    QrTerminalIP: TWideStringField;
    QrTerminalTerminal: TIntegerField;
    QrMaster: TmySQLQuery;
    QrMasterCNPJ_TXT: TWideStringField;
    QrMasterEm: TWideStringField;
    QrMasterTipo: TSmallintField;
    QrMasterDono: TIntegerField;
    QrMasterVersao: TIntegerField;
    QrMasterCNPJ: TWideStringField;
    QrMasterLimite: TSmallintField;
    QrMasterSolicitaSenha: TIntegerField;
    QrMasterUsaAccMngr: TSmallintField;
    frxDsMaster: TfrxDBDataset;
    QrControleDono: TIntegerField;
    QrControleContraSenha: TWideStringField;
    QrControleMeuLogoPath: TWideStringField;
    QrAgora: TmySQLQuery;
    QrAgoraANO: TLargeintField;
    QrAgoraMES: TLargeintField;
    QrAgoraDIA: TLargeintField;
    QrAgoraHORA: TLargeintField;
    QrAgoraMINUTO: TLargeintField;
    QrAgoraSEGUNDO: TLargeintField;
    QrAgoraAGORA: TDateTimeField;
    QrOpcoesBugs: TmySQLQuery;
    QrOpcoesBugsGraNivEqAp: TSmallintField;
    QrOpcoesBugsGraCodEqAp: TIntegerField;
    QrOpcoesBugsGraNivEqMo: TSmallintField;
    QrOpcoesBugsGraCodEqMo: TIntegerField;
    procedure QrControleAfterOpen(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure QrMasterAfterOpen(DataSet: TDataSet);
    procedure QrMasterCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    procedure ReopenMaster();
  public
    function  TabelasQueNaoQueroCriar(): String;
    procedure ReopenControle();
    procedure ReopenOpcoesBugs();
    { Public declarations }
  end;

var
  Dmod: TDmod;

implementation

uses UnInternalConsts, Principal, Servidor, UnGOTOy, Bugsurvey_Dmk, VerifiDB,
  MyListas, ModuleGeral, DmkDAC_PF, UMySQLModule;

{$R *.dfm}

{ TDmod }

procedure TDmod.DataModuleCreate(Sender: TObject);
var
  Versao, VerZero, Resp: Integer;
  BD: String;
  Verifica: Boolean;
begin
  if MyDB.Connected then
    Geral.MensagemBox('MyDB est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  if MyLocDataBase.Connected then
    Geral.MensagemBox('MyLocDataBase est� connectado antes da configura��o!',
    'Aviso!', MB_OK+MB_ICONWARNING);
  VAR_PORTA := Geral.ReadAppKeyCU('Porta', 'Dermatek', ktInteger, 3306);
  Geral.WriteAppKeyCU('Porta', 'Dermatek', VAR_PORTA, ktInteger);
  VAR_BDSENHA := 'wkljweryhvbirt';
  if not GOTOy.OMySQLEstaInstalado(FmBugsurvey_Dmk.LaAviso1,
    FmBugsurvey_Dmk.LaAviso2, FmBugsurvey_Dmk.ProgressBar1) then
  begin
    raise EAbort.Create('');
    Exit;
  end;
  /////////////////////////////////////////
  if GOTOy.SenhaDesconhecida then
  begin
    raise EAbort.Create('Senha desconhecida');
    Exit;
  end;
  ///////////////////////////////////////////////////////////
  ZZDB.Host         := VAR_SQLHOST;
  ZZDB.UserName     := VAR_SQLUSER;
  ZZDB.UserPassword := VAR_BDSENHA;
  ZZDB.Port         := VAR_PORTA;
  try
    ZZDB.Connected := True;
  except
    ZZDB.UserPassword := '852456';
    try
      ZZDB.Connected := True;
      QrUpd.Database := ZZDB;
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('UPDATE User SET Password=PASSWORD("'+VAR_BDSENHA+'")');
      QrUpd.SQL.Add('');
      QrUpd.SQL.Add('WHERE User="root"');
      QrUpd.ExecSQL;
      ///////////
      QrUpd.SQL.Clear;
      QrUpd.SQL.Add('FLUSH PRIVILEGES');
      QrUpd.ExecSQL;
      ///////////
      FmPrincipal.Close;
      Application.Terminate;
      Exit;
    except
      if VAR_SERVIDOR = 2 then
        Geral.MensagemBox('Banco de dados teste n�o se conecta!',
        'ERRO', MB_OK+MB_ICONERROR);
    end;
  end;
  /////////////////////////////////////////
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  //
  VAR_SQLx := TStringList.Create;
  VAR_SQL1 := TStringList.Create;
  VAR_SQL2 := TStringList.Create;
  VAR_SQLa := TStringList.Create;
  //
  MAR_SQLx := TStringList.Create;
  MAR_SQL1 := TStringList.Create;
  MAR_SQL2 := TStringList.Create;
  MAR_SQLa := TStringList.Create;
  try
    if VAR_SERVIDOR = 1 then
    begin
      if not GetSystemMetrics(SM_NETWORK) and $01 = $01 then
      begin
      Geral.MensagemBox('M�quina cliente sem rede.', 'Erro', MB_OK+MB_ICONERROR);
        //Application.Terminate;
      end;
    end;
  except
    Application.Terminate;
    Exit;
  end;
  if VAR_APPTERMINATE then
  begin
    Application.Terminate;
    Exit;
  end;
  Geral.DefineFormatacoes;
  //
  VAR_SERVIDOR := Geral.ReadAppKey('Server', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if not (VAR_SERVIDOR in [1,2]) then
  begin
    Application.CreateForm(TFmServidor, FmServidor);
    FmServidor.ShowModal;
    FmServidor.Destroy;
  end;
  if VAR_IP = CO_VAZIO then
  begin
    VAR_IP := '127.0.0.1';
    if VAR_SERVIDOR = 1 then
    begin
      if InputQuery('IP do Servidor', 'Defina o IP do Servidor', VAR_IP) then
      begin
        Geral.WriteAppKey('IPServer', Application.Title, VAR_IP, ktString,
          HKEY_LOCAL_MACHINE);
      end else
      begin
        Application.Terminate;
        Exit;
      end;
    end;
  end;
  MyDB.UserName     := VAR_SQLUSER;
  MyDB.UserPassword := VAR_BDSENHA;
  MyDB.Host         := VAR_IP;
  MyDB.Port         := VAR_PORTA;
  MyDB.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  //
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SHOW DATABASES');
  QrAux.Open;
  BD := CO_VAZIO;
  while not QrAux.Eof do
  begin
    if Uppercase(QrAux.FieldByName('Database').AsString)=Uppercase(TMeuDB) then
      BD := TMeuDB;
    QrAux.Next;
  end;
  MyDB.Close;
  MyDB.DataBaseName := BD;
  if MyDB.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MensagemBox('O banco de dados '+TMeuDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?', 'Banco de Dados',
      MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAux.Close;
      QrAux.SQL.Clear;
      QrAux.SQL.Add('CREATE DATABASE '+TMeuDB);
      QrAux.ExecSQL;
      MyDB.DatabaseName := TMeuDB;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MensagemBox('O aplicativo ser� encerrado!', 'Banco de Dados',
      MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
  //
  GOTOy.DefinePathMySQL;
  //
  MyLocDatabase.UserName     := VAR_SQLUSER;
  MyLocDatabase.UserPassword := VAR_BDSENHA;
  MyLocDatabase.Port         := VAR_PORTA;
  MyLocDatabase.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  //
{
  QrAuxL.Close;
  QrAuxL.SQL.Clear;
  QrAuxL.SQL.Add('SHOW DATABASES');
  QrAuxL.Open;
  BD := CO_VAZIO;
  while not QrAuxL.Eof do
  begin
    if Uppercase(QrAuxL.FieldByName('Database').AsString)=Uppercase(TLocDB) then
      BD := TLocDB;
    QrAuxL.Next;
  end;
  MyLocDatabase.Close;
  MyLocDatabase.DatabaseName := BD;
  if MyLocDatabase.DataBaseName = CO_VAZIO then
  begin
    Resp := Geral.MensagemBox('O banco de dados local '+TLocDB+
      ' n�o existe e deve ser criado. Confirma a cria��o?',
      'Banco de Dados Local', MB_YESNOCANCEL+MB_ICONQUESTION);
    if Resp = ID_YES then
    begin
      QrAuxL.Close;
      QrAuxL.SQL.Clear;
      QrAuxL.SQL.Add('CREATE DATABASE '+TLocDB);
      QrAuxL.ExecSQL;
      MyLocDatabase.DatabaseName := TLocDB;
    end else if Resp = ID_CANCEL then
    begin
      Geral.MensagemBox('O aplicativo ser� encerrado!',
      'Banco de Dados Local', MB_OK+MB_ICONWARNING);
      Application.Terminate;
      Exit;
    end;
  end;
  //
}
  VerZero := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    0, HKEY_LOCAL_MACHINE);
  Versao := Geral.ReadAppKey('Versao', Application.Title, ktInteger,
    CO_VERSAO, HKEY_LOCAL_MACHINE);
  Verifica := False;
  if Versao < CO_Versao then
    Verifica := True;
  if VerZero = 0 then
  begin
    Resp := Geral.MensagemBox('N�o h� informa��o de vers�o no registro, '+
    'se esta n�o for a primeira execu��o do aplicativo selecione cancelar e '+
    'informe o respons�vel!. Caso confirme, ser� verificado a composi��o do '+
    'banco de dados. Confirma a Verifica��o?',
    'Aus�ncia de Informa��o de Vers�o no Registro',
    MB_YESNOCANCEL+MB_ICONWARNING);
    if Resp = ID_YES then
      Verifica := True
    else if Resp = ID_CANCEL then
    begin
      Application.Terminate;
      Exit;
    end;
  end;
  //
  Mylist.ConfiguracoesIniciais(1, MyDB.DatabaseName);
  //
  try
    Application.CreateForm(TDmodG, DmodG);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo de dados Geral', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
{
  try
    Application.CreateForm(TDModFin, DModFin);
  except
    Geral.MensagemBox('Imposs�vel criar Modulo Financeiro', 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
}
  if Verifica then
  begin
    Application.CreateForm(TFmVerifiDB, FmVerifiDB);
    with FmVerifiDb do
    begin
      BtSair.Enabled := False;
      FVerifi := True;
      ShowModal;
      FVerifi := False;
      Destroy;
    end;
  end;
  //
  try
    ReopenMaster(); //QrMaster.Open;
    VAR_EMPRESANOME := QrMasterEm.Value;
  except
    try
      Application.CreateForm(TFmVerifiDB, FmVerifiDB);
      with FmVerifiDb do
      begin
        BtSair.Enabled := False;
        FVerifi := True;
        ShowModal;
        FVerifi := False;
        Destroy;
      end;
    except;
      MyDB.DatabaseName := CO_VAZIO;
      raise;
    end;
  end;

  Lic_Dmk.LiberaUso5;
  ReopenOpcoesBugs();
end;

procedure TDmod.QrControleAfterOpen(DataSet: TDataSet);
begin
  VAR_MEULOGOPATH := QrControleMeuLogoPath.Value;
  if FileExists(VAR_MEULOGOPATH) then
    VAR_MEULOGOEXISTE := True
  else
    VAR_MEULOGOEXISTE := False;
end;

procedure TDmod.QrMasterAfterOpen(DataSet: TDataSet);
begin
  if Trim(QrMasterCNPJ.Value) = CO_VAZIO then
  Geral.MensagemBox('CNPJ n�o definido ou Empresa n�o definida. '+#13#10+
  'Algumas ferramentas do aplicativo poder�o ficar inacess�veis.',
  'Aviso', MB_OK+MB_ICONWARNING);
  FmPrincipal.Caption := Application.Title+'  ::  '+QrMasterEm.Value+' # '+
  QrMasterCNPJ.Value;
  //
  ReopenControle();
  //
{
  if QrControleSoMaiusculas.Value = 'V' then VAR_SOMAIUSCULAS := True;
  VAR_UFPADRAO        := QrControleUFPadrao.Value;
  VAR_CIDADEPADRAO    := QrControleCidade.Value;
  VAR_TRAVACIDADE     := QrControleTravaCidade.Value;
  VAR_MOEDA           := QrControleMoeda.Value;
  //
  VAR_CARTVEN         := QrControleCartVen.Value;
  VAR_CARTCOM         := QrControleCartCom.Value;
  VAR_CARTRES         := QrControleCartReS.Value;
  VAR_CARTDES         := QrControleCartDeS.Value;
  VAR_CARTREG         := QrControleCartReG.Value;
  VAR_CARTDEG         := QrControleCartDeG.Value;
  VAR_CARTCOE         := QrControleCartCoE.Value;
  VAR_CARTCOC         := QrControleCartCoC.Value;
  VAR_CARTEMD         := QrControleCartEmD.Value;
  VAR_CARTEMA         := QrControleCartEmA.Value;
  //
  VAR_PAPERSET        := True;
  VAR_PAPERTOP        := QrControlePaperTop.Value;
  VAR_PAPERLEF        := QrControlePaperLef.Value;
  VAR_PAPERWID        := QrControlePaperWid.Value;
  VAR_PAPERHEI        := QrControlePaperHei.Value;
  VAR_PAPERFCL        := QrControlePaperFcl.Value;
  //
}
end;

procedure TDmod.QrMasterCalcFields(DataSet: TDataSet);
begin
  {
  QrMasterTE1_TXT.Value := Geral.FormataTelefone_TT(QrMasterETe1.Value);
  QrMasterCEP_TXT.Value :=Geral.FormataCEP_NT(QrMasterECEP.Value);
  }
  QrMasterCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrMasterCNPJ.Value);
end;

procedure TDmod.ReopenControle();
begin
  QrControle.Close;
  QrControle.Open;
end;

procedure TDmod.ReopenMaster;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrMaster, Dmod.MyDB, [
  'SELECT ma.Em, te.Tipo, ',
  '/* ',
  'te.Logo, te.Logo2, ',
  'te.IE, te.ECidade, uf.Nome NOMEUF, te.EFax, ',
  'te.ERua, (te.ENumero+0.000) ENumero, te.EBairro, te.ECEP, te.ECompl, ',
  'te.EContato, te.ECel, te.ETe1, te.ETe2, te.ETe3, te.EPais, ',
  'te.Respons1, te.Respons2,*/ ',
  'ma.Limite, ma.SolicitaSenha, ',
  'ma.UsaAccMngr, cm.Dono, cm.Versao, cm.CNPJ ',
  'FROM Entidades te, Controle cm, Master ma ',
  '/*/Ufs uf*/ ',
  'WHERE te.Codigo=cm.Dono ',
  'AND ((te.CNPJ=cm.CNPJ AND te.Tipo=0) OR (te.CPF=cm.CNPJ AND te.Tipo=1)) ',
  '/*AND uf.Codigo=te.EUF*/ ',
  '']);
end;

procedure TDmod.ReopenOpcoesBugs();
begin
  UMyMod.AbreQuery(QrOpcoesBugs, MyDB);
end;

function TDmod.TabelasQueNaoQueroCriar: String;
begin
//
end;

end.
