object Form2: TForm2
  Left = 0
  Top = 0
  Caption = 'Form2'
  ClientHeight = 300
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 635
    Height = 300
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 0
    OnDrawTab = PageControl1DrawTab
    object TabSheet1: TTabSheet
      Caption = 'TabSheet1'
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 627
        Height = 272
        Align = alClient
        Caption = 'Panel1'
        TabOrder = 0
        object Label2: TLabel
          Left = 64
          Top = 28
          Width = 31
          Height = 13
          Caption = 'Label1'
        end
        object Edit2: TEdit
          Left = 64
          Top = 44
          Width = 121
          Height = 21
          TabOrder = 0
          Text = 'Edit1'
        end
        object Button1: TButton
          Left = 152
          Top = 188
          Width = 75
          Height = 25
          Caption = 'Button1'
          TabOrder = 1
          OnClick = Button1Click
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'TabSheet2'
      ImageIndex = 1
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 627
        Height = 272
        Align = alClient
        Caption = 'Panel1'
        TabOrder = 0
        object Label1: TLabel
          Left = 64
          Top = 28
          Width = 31
          Height = 13
          Caption = 'Label1'
        end
        object Edit1: TEdit
          Left = 64
          Top = 44
          Width = 121
          Height = 21
          TabOrder = 0
          Text = 'Edit1'
        end
      end
    end
  end
end
