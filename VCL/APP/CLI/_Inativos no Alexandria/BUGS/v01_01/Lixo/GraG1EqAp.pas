unit GraG1EqAp;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, ModOS, dmkDBLookupComboBox, dmkEditCB, ComCtrls, dmkRichEdit,
  Bugs_Tabs;

type
  TFmGraG1EqAp = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrGraG1EqAp: TmySQLQuery;
    DsGraG1EqAp: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdNivel1: TdmkEdit;
    Label9: TLabel;
    EdNome: TdmkEdit;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    Label2: TLabel;
    DBEdNome: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrGraG1EqApNivel1: TIntegerField;
    QrGraG1EqApNO_GG1: TWideStringField;
    QrGraG1EqApMarca: TIntegerField;
    QrGraG1EqApNO_MARCA: TWideStringField;
    QrGraG1EqApNO_FABR: TWideStringField;
    Label4: TLabel;
    EdMarca: TdmkEditCB;
    CBMarca: TdmkDBLookupComboBox;
    QrMarcas: TmySQLQuery;
    DsMarcas: TDataSource;
    QrMarcasControle: TIntegerField;
    QrMarcasNO_MARCA_FABR: TWideStringField;
    SbMarcas: TSpeedButton;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    QrGraG1EqApCPL_EXISTE: TLargeintField;
    ReObservacao: TdmkRichEdit;
    BtTexto: TBitBtn;
    DBREObservacao: TDBRichEdit;
    QrGraG1EqApObservacao: TWideMemoField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGraG1EqApAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGraG1EqApBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure SbMarcasClick(Sender: TObject);
    procedure BtTextoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    // :) Mover para private quando a estrutura da GraG1EqAp diferir da GraG1EqMo
    FGraBugsServi: TGraBugsServi;
    FTabNome,
    FFrmNome: String;
    // :) Fim Mover quando...!
    //
    procedure FiltraItensAExibir(GraBugs: TGraBugsServi);
  end;

var
  FmGraG1EqAp: TFmGraG1EqAp;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, Module, DmkDAC_PF, Principal, MyDBCheck, GraGru1;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGraG1EqAp.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGraG1EqAp.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGraG1EqApNivel1.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGraG1EqAp.DefParams;
var
  //GraNiv: Integer;
  Filtro: String;
begin
  VAR_GOTOTABELA := 'gragru1 gg1';
  VAR_GOTOMYSQLTABLE := QrGraG1EqAp;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := 'gg1.Nivel1';
  VAR_GOTONOME := 'gg1.Nome';
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;

  GOTOy.LimpaVAR_SQL;

  if FGraBugsServi <> gbsIndef then
  begin
    DmModOS.FiltroGrade(FGraBugsServi, gbmEquipam, Filtro);
    //
    VAR_SQLx.Add('SELECT gg1.Nivel1, gg1.Nome NO_GG1, cpl.Marca,');
    VAR_SQLx.Add('gfm.Nome NO_MARCA, gfc.Nome NO_FABR, ');
    VAR_SQLx.Add('IF(cpl.Nivel1 IS NULL, 0, 1) CPL_EXISTE, cpl.Observacao ');
    VAR_SQLx.Add('FROM gragru1 gg1');
    VAR_SQLx.Add('LEFT JOIN ' + FTabNome + ' cpl ON cpl.Nivel1=gg1.Nivel1');
    VAR_SQLx.Add('LEFT JOIN grafabmar gfm ON cpl.Marca=gfm.Controle');
    VAR_SQLx.Add('LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo');
    VAR_SQLx.Add('');
    VAR_SQLx.Add('WHERE ' + Filtro);
    //
    VAR_SQL1.Add('AND gg1.Nivel1=:P0');
    //
    //VAR_SQL2.Add('AND gg1.CodUsu=:P0');
    //
    VAR_SQLa.Add('AND gg1.Nome LIKE :P0');
    //
    //
    VAR_GOTOVAR1 := Filtro;
    //
  end;
end;

procedure TFmGraG1EqAp.CriaOForm;
begin
  DefineONomeDoForm;
end;

procedure TFmGraG1EqAp.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGraG1EqAp.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmGraG1EqAp.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGraG1EqAp.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGraG1EqAp.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGraG1EqAp.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGraG1EqAp.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraG1EqAp.BtTextoClick(Sender: TObject);
begin
  MyObjects.EditaTextoRichEdit(REObservacao, 10, 'Tahoma', []);
end;

procedure TFmGraG1EqAp.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrGraG1EqAp, [PnDados],
  [PnEdita], EdNome, ImgTipo, 'gragru1');
  EdNivel1.ValueVariant := QrGraG1EqApNivel1.Value;
  EdNome.Text := QrGraG1EqApNO_GG1.Value;
  EdMarca.ValueVariant := QrGraG1EqApMarca.Value;
  CBMarca.KeyValue := QrGraG1EqApMarca.Value;
end;

procedure TFmGraG1EqAp.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGraG1EqApNivel1.Value;
  Close;
end;

procedure TFmGraG1EqAp.BtConfirmaClick(Sender: TObject);
  function InsUpdG1PrAp(SQLType: TSQLType): Boolean;
  var
    Nivel1, Marca: Integer;
    Observacao: String;
  begin
    Nivel1         := EdNivel1.ValueVariant;
    Marca          := EdMarca.ValueVariant;
    Observacao     := MyObjects.ObtemTextoRichEdit(Self, ReObservacao);
    //
    Result := UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, FTabNome, False, [
    'Marca', 'Observacao'], ['Nivel1'
    ], [Marca, Observacao], [Nivel1
    ], True);
  end;
var
  Nivel1: Integer;
  Nome: String;
  SQLType: TSQLType;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descrição!') then Exit;
  //
  Nivel1 := EdNivel1.ValueVariant;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'gragru1', False, [
  'Nome'], ['Nivel1'], [Nome], [Nivel1], True) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
    'SELECT Nivel1 ',
    'FROM ' + FTabNome,
    'WHERE Nivel1=' + Geral.FF0(Nivel1),
    '']);
    if Nivel1 = Dmod.QrAux.FieldByName('Nivel1').AsInteger then
      SQLType := stUpd
    else
      SQLType := stIns;  
    if InsUpdG1PrAp(SQLType) then
    begin
      LocCod(Nivel1,Nivel1);
      ImgTipo.SQLType := stlok;
      PnDados.Visible := True;
      PnEdita.Visible := False;
    end;
  end;
end;

procedure TFmGraG1EqAp.BtDesisteClick(Sender: TObject);
var
  Nivel1: Integer;
begin
  Nivel1 := EdNivel1.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(Nivel1, Dmod.MyDB, FTabNome, 'Nivel1');
end;

procedure TFmGraG1EqAp.BtIncluiClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmGraGru1, FmGraGru1, afmoNegarComAviso) then
  begin
    FmGraGru1.ImgTipo.SQLType := stIns;
    FmGraGru1.FQrGraGru1 := QrGraG1EqAp;
    //
    FmGraGru1.ShowModal;
    FmGraGru1.Destroy;
  end;
  if VAR_CADASTRO <> 0 then
  begin
    LocCod(VAR_CADASTRO, VAR_CADASTRO);
    if (VAR_CADASTRO = QrGraG1EqApNivel1.Value) and
    (QrGraG1EqApCPL_EXISTE.Value = 0) then
      BtAlteraClick(BtAltera);
  end;
end;

procedure TFmGraG1EqAp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  ReObservacao.Align := alClient;
  DBReObservacao.Align := alClient;
  CriaOForm;
  //
  UMyMod.AbreQuery(QrMarcas, Dmod.MyDB);
end;

procedure TFmGraG1EqAp.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGraG1EqApNivel1.Value, LaRegistro.Caption);
end;

procedure TFmGraG1EqAp.SbMarcasClick(Sender: TObject);
begin
  VAR_CADASTRO2 := 0;
  FmPrincipal.MostraFormGraFabCad();
  UMyMod.SetaCodigoPesquisado(EdMarca, CBMarca, QrMarcas, VAR_CADASTRO2, 'Controle');
end;

procedure TFmGraG1EqAp.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGraG1EqAp.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrGraG1EqApNivel1.Value, LaRegistro.Caption);
end;

procedure TFmGraG1EqAp.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmGraG1EqAp.QrGraG1EqApAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGraG1EqAp.FiltraItensAExibir(GraBugs: TGraBugsServi);
begin
  FGraBugsServi := GraBugs;
  case FGraBugsServi of
    gbsAplica  : FTabNome := 'grag1eqap';
    gbsMonitora: FTabNome := 'grag1eqmo';
    else FTabNome := 'grag1eq??';
  end;
  //
  case FGraBugsServi of
    gbsAplica  : FFrmNome := 'Aplicação';
    gbsMonitora: FFrmNome := 'Monitoramento';
    else FFrmNome := '? ? ? ? ?';
  end;
  //
  DefParams;
  Va(vpLast);
end;

procedure TFmGraG1EqAp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmGraG1EqAp.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGraG1EqApNivel1.Value,
  CuringaLoc.CriaForm('Nivel1', CO_NOME, 'gragru1', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGraG1EqAp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], 'PRD-GRUPO-025 :: Equipamentos de ' + FFrmNome, True, taCenter, 2, 10, 20);
end;

procedure TFmGraG1EqAp.QrGraG1EqApBeforeOpen(DataSet: TDataSet);
begin
  QrGraG1EqApNivel1.DisplayFormat := FFormatFloat;
end;

end.

