object FmOSCab: TFmOSCab
  Left = 368
  Top = 194
  Caption = 'GER-OSERV-001 :: Ordem de Servi'#231'o'
  ClientHeight = 853
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  ParentFont = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 757
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 225
      Width = 1008
      Height = 408
      ActivePage = TabSheet7
      Align = alTop
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = ' Operacional '
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object PCOperacoes: TPageControl
          Left = 0
          Top = 0
          Width = 1000
          Height = 380
          ActivePage = TabSheet6
          Align = alClient
          TabOrder = 0
          object TabSheet4: TTabSheet
            Caption = ' Dedetiza'#231#227'o '
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object PnDedetizacao: TPanel
              Left = 0
              Top = 0
              Width = 992
              Height = 352
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              Visible = False
              object GroupBox7: TGroupBox
                Left = 0
                Top = 0
                Width = 265
                Height = 352
                Align = alLeft
                Caption = ' Servi'#231'os: '
                TabOrder = 0
                object DBGOSSrv: TDBGrid
                  Left = 2
                  Top = 15
                  Width = 261
                  Height = 335
                  Align = alClient
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'Tahoma'
                  TitleFont.Style = []
                  OnColEnter = DBGOSSrvColEnter
                  OnColExit = DBGOSSrvColExit
                  OnDrawColumnCell = DBGOSSrvDrawColumnCell
                  OnMouseUp = DBGOSSrvMouseUp
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Autorizado'
                      Title.Caption = 'A'
                      Width = 17
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Title.Caption = 'ID'
                      Width = 41
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_DesServico'
                      Title.Caption = 'Servi'#231'o'
                      Width = 100
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'ValTota'
                      Title.Caption = 'Valor'
                      Visible = True
                    end>
                end
              end
              object GroupBox9: TGroupBox
                Left = 265
                Top = 0
                Width = 160
                Height = 352
                Align = alLeft
                Caption = ' Pragas alvo: '
                TabOrder = 1
                object DBGOSAlv: TDBGrid
                  Left = 2
                  Top = 15
                  Width = 156
                  Height = 335
                  Align = alClient
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'Tahoma'
                  TitleFont.Style = []
                  OnMouseUp = DBGOSAlvMouseUp
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NO_Praga_Z'
                      Title.Caption = 'Nome'
                      Width = 120
                      Visible = True
                    end>
                end
              end
              object Panel8: TPanel
                Left = 425
                Top = 0
                Width = 567
                Height = 352
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 2
                object Splitter1: TSplitter
                  Left = 0
                  Top = 165
                  Width = 567
                  Height = 5
                  Cursor = crVSplit
                  Align = alTop
                  ExplicitTop = 129
                  ExplicitWidth = 427
                end
                object GroupBox11: TGroupBox
                  Left = 0
                  Top = 0
                  Width = 567
                  Height = 165
                  Align = alTop
                  Caption = ' Formula'#231#245'es de APLICA'#199#195'O:'
                  TabOrder = 0
                  object GroupBox12: TGroupBox
                    Left = 2
                    Top = 15
                    Width = 407
                    Height = 148
                    Align = alClient
                    Caption = ' Itens da APLICA'#199#195'O selecionada: '
                    TabOrder = 0
                    object DBGOSFrmRec: TDBGrid
                      Left = 157
                      Top = 15
                      Width = 248
                      Height = 131
                      Align = alClient
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'Tahoma'
                      TitleFont.Style = []
                      OnMouseUp = DBGOSFrmRecMouseUp
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'Ordem'
                          Title.Caption = 'Seq.'
                          Width = 26
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_GG1'
                          Title.Caption = 'Produto'
                          Width = 128
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'PrvQtd'
                          Title.Caption = 'Qtde'
                          Visible = True
                        end>
                    end
                    object Panel10: TPanel
                      Left = 2
                      Top = 15
                      Width = 155
                      Height = 131
                      Align = alLeft
                      BevelOuter = bvNone
                      ParentBackground = False
                      TabOrder = 1
                      object Splitter2: TSplitter
                        Left = 0
                        Top = 61
                        Width = 155
                        Height = 3
                        Cursor = crVSplit
                        Align = alTop
                        ExplicitTop = 85
                        ExplicitWidth = 58
                      end
                      object DBGOSFrmCab: TDBGrid
                        Left = 0
                        Top = 0
                        Width = 155
                        Height = 61
                        Align = alTop
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -11
                        TitleFont.Name = 'Tahoma'
                        TitleFont.Style = []
                        OnMouseUp = DBGOSFrmCabMouseUp
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'Nome'
                            Title.Caption = 'APLICA'#199#195'O'
                            Width = 120
                            Visible = True
                          end>
                      end
                      object GroupBox17: TGroupBox
                        Left = 0
                        Top = 64
                        Width = 155
                        Height = 67
                        Align = alClient
                        Caption = ' Abrang'#234'ncia: '
                        TabOrder = 1
                        object DBGOSFrmAbr: TDBGrid
                          Left = 2
                          Top = 15
                          Width = 151
                          Height = 50
                          Align = alClient
                          DataSource = DmModOS.DsOSFrmAbr
                          TabOrder = 0
                          TitleFont.Charset = DEFAULT_CHARSET
                          TitleFont.Color = clWindowText
                          TitleFont.Height = -11
                          TitleFont.Name = 'Tahoma'
                          TitleFont.Style = []
                          OnMouseUp = DBGOSFrmAbrMouseUp
                          Columns = <
                            item
                              Expanded = False
                              FieldName = 'NO_ABRANGE'
                              Title.Caption = 'ABRANG'#202'NCIA'
                              Width = 115
                              Visible = True
                            end>
                        end
                      end
                    end
                  end
                  object GroupBox8: TGroupBox
                    Left = 409
                    Top = 15
                    Width = 156
                    Height = 148
                    Align = alRight
                    Caption = ' Comodos / Bens: '
                    TabOrder = 1
                    object DBGOSFrmDep: TDBGrid
                      Left = 2
                      Top = 15
                      Width = 152
                      Height = 131
                      Align = alClient
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'Tahoma'
                      TitleFont.Style = []
                      OnMouseUp = DBGOSFrmDepMouseUp
                      Columns = <
                        item
                          Alignment = taCenter
                          Expanded = False
                          FieldName = 'SIGLA'
                          Title.Alignment = taCenter
                          Title.Caption = 'T'
                          Width = 16
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_Campo'
                          Title.Caption = 'Descri'#231#227'o'
                          Width = 100
                          Visible = True
                        end>
                    end
                  end
                end
                object GroupBox13: TGroupBox
                  Left = 0
                  Top = 170
                  Width = 567
                  Height = 182
                  Align = alClient
                  Caption = ' Formula'#231#245'es de MONITORAMENTO:'
                  TabOrder = 1
                  object DBGOSMonCab: TDBGrid
                    Left = 2
                    Top = 15
                    Width = 267
                    Height = 165
                    Align = alLeft
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'Tahoma'
                    TitleFont.Style = []
                    OnMouseUp = DBGOSMonCabMouseUp
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'Nome'
                        Title.Caption = 'MONITORAMENTO'
                        Width = 120
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_PIP'
                        Title.Caption = 'Refer'#234'ncia PMV'
                        Width = 107
                        Visible = True
                      end>
                  end
                  object GroupBox14: TGroupBox
                    Left = 269
                    Top = 15
                    Width = 140
                    Height = 165
                    Align = alClient
                    Caption = ' Itens do MONITORAMENTO selecionado: '
                    TabOrder = 1
                    object DBGOSMonRec: TDBGrid
                      Left = 2
                      Top = 15
                      Width = 136
                      Height = 148
                      Align = alClient
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'Tahoma'
                      TitleFont.Style = []
                      OnMouseUp = DBGOSMonRecMouseUp
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'Ordem'
                          Title.Caption = 'Seq.'
                          Width = 26
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'PrvQtd'
                          Title.Caption = 'Qtde'
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_GG1'
                          Title.Caption = 'Produto'
                          Width = 250
                          Visible = True
                        end>
                    end
                  end
                  object GroupBox20: TGroupBox
                    Left = 409
                    Top = 15
                    Width = 156
                    Height = 165
                    Align = alRight
                    Caption = ' Comodos / Bens: '
                    TabOrder = 2
                    object DBGOSMonDep: TDBGrid
                      Left = 2
                      Top = 15
                      Width = 152
                      Height = 148
                      Align = alClient
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'Tahoma'
                      TitleFont.Style = []
                      OnMouseUp = DBGOSMonDepMouseUp
                      Columns = <
                        item
                          Alignment = taCenter
                          Expanded = False
                          FieldName = 'SIGLA'
                          Title.Alignment = taCenter
                          Title.Caption = 'T'
                          Width = 16
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'NO_Campo'
                          Title.Caption = 'Descri'#231#227'o'
                          Width = 100
                          Visible = True
                        end>
                    end
                  end
                end
              end
            end
          end
          object TabSheet5: TTabSheet
            Caption = ' Caixas d'#39#225'gua  '
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object PnCaixaDAgua: TPanel
              Left = 0
              Top = 0
              Width = 992
              Height = 352
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              Visible = False
              object PageControl3: TPageControl
                Left = 0
                Top = 0
                Width = 992
                Height = 352
                Align = alClient
                TabOrder = 0
                Visible = False
              end
              object GroupBox22: TGroupBox
                Left = 0
                Top = 0
                Width = 992
                Height = 352
                Align = alClient
                Caption = ' Caixas D`'#225'gua'
                TabOrder = 1
                object Panel27: TPanel
                  Left = 2
                  Top = 15
                  Width = 988
                  Height = 335
                  Align = alClient
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 0
                  object DBGOSCxaAtrib: TdmkDBGrid
                    Left = 727
                    Top = 0
                    Width = 261
                    Height = 335
                    Align = alClient
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'CU_CAD'
                        Title.Caption = 'C'#243'digo'
                        Width = 44
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_CAD'
                        Title.Caption = 'Atributo'
                        Width = 112
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CU_ITS'
                        Title.Caption = 'C'#243'digo'
                        Width = 44
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_ITS'
                        Title.Caption = 'Descri'#231#227'o do item do atributo'
                        Width = 236
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ID_Item'
                        Title.Caption = 'ID'
                        Width = 44
                        Visible = True
                      end>
                    Color = clWindow
                    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
                    TabOrder = 0
                    TitleFont.Charset = DEFAULT_CHARSET
                    TitleFont.Color = clWindowText
                    TitleFont.Height = -11
                    TitleFont.Name = 'Tahoma'
                    TitleFont.Style = []
                    Columns = <
                      item
                        Expanded = False
                        FieldName = 'CU_CAD'
                        Title.Caption = 'C'#243'digo'
                        Width = 44
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_CAD'
                        Title.Caption = 'Atributo'
                        Width = 112
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'CU_ITS'
                        Title.Caption = 'C'#243'digo'
                        Width = 44
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'NO_ITS'
                        Title.Caption = 'Descri'#231#227'o do item do atributo'
                        Width = 236
                        Visible = True
                      end
                      item
                        Expanded = False
                        FieldName = 'ID_Item'
                        Title.Caption = 'ID'
                        Width = 44
                        Visible = True
                      end>
                  end
                  object Panel11: TPanel
                    Left = 309
                    Top = 0
                    Width = 418
                    Height = 335
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 1
                    object GroupBox23: TGroupBox
                      Left = 0
                      Top = 0
                      Width = 418
                      Height = 169
                      Align = alTop
                      Caption = ' Dados da caixa d`'#225'gua selecionada: '
                      TabOrder = 0
                      object Panel28: TPanel
                        Left = 2
                        Top = 15
                        Width = 414
                        Height = 152
                        Align = alClient
                        BevelOuter = bvNone
                        ParentBackground = False
                        TabOrder = 0
                        object Label43: TLabel
                          Left = 4
                          Top = 0
                          Width = 34
                          Height = 13
                          Caption = 'Forma:'
                          FocusControl = DBEdNOME_FORMA
                        end
                        object Label44: TLabel
                          Left = 208
                          Top = 0
                          Width = 42
                          Height = 13
                          Caption = 'Material:'
                          FocusControl = DBEdNO_MATERIAL
                        end
                        object Label45: TLabel
                          Left = 4
                          Top = 40
                          Width = 59
                          Height = 13
                          Caption = 'Vol. (Litros):'
                          FocusControl = DBEdVolumeL
                        end
                        object Label46: TLabel
                          Left = 68
                          Top = 40
                          Width = 43
                          Height = 13
                          Caption = 'Medidas:'
                          FocusControl = DBEdMedidas
                        end
                        object dmkLabelRotate1: TdmkLabelRotate
                          Left = 0
                          Top = 88
                          Width = 21
                          Height = 57
                          Angle = ag90
                          Caption = 'Acesso:'
                          Font.Charset = ANSI_CHARSET
                          Font.Color = clWindowText
                          Font.Height = -13
                          Font.Name = 'Arial'
                          Font.Style = []
                        end
                        object DBEdNOME_FORMA: TDBEdit
                          Left = 4
                          Top = 16
                          Width = 200
                          Height = 21
                          DataField = 'NOME_FORMA'
                          TabOrder = 0
                        end
                        object DBEdNO_MATERIAL: TDBEdit
                          Left = 208
                          Top = 16
                          Width = 200
                          Height = 21
                          DataField = 'NO_MATERIAL'
                          TabOrder = 1
                        end
                        object DBEdVolumeL: TDBEdit
                          Left = 4
                          Top = 56
                          Width = 60
                          Height = 21
                          DataField = 'VolumeL'
                          TabOrder = 2
                        end
                        object DBEdMedidas: TDBEdit
                          Left = 68
                          Top = 56
                          Width = 340
                          Height = 21
                          DataField = 'Medidas'
                          TabOrder = 3
                        end
                        object DBMeAcesso: TDBMemo
                          Left = 20
                          Top = 84
                          Width = 388
                          Height = 63
                          DataField = 'Acesso'
                          TabOrder = 4
                        end
                      end
                    end
                    object GroupBox24: TGroupBox
                      Left = 0
                      Top = 169
                      Width = 418
                      Height = 166
                      Align = alClient
                      Caption = ' Fotos de caixas d'#39#225'gua: '
                      TabOrder = 1
                      object Splitter5: TSplitter
                        Left = 2
                        Top = 103
                        Width = 414
                        Height = 5
                        Cursor = crVSplit
                        Align = alBottom
                        ExplicitTop = 105
                      end
                      object DBGOSCxI: TDBGrid
                        Left = 2
                        Top = 15
                        Width = 414
                        Height = 88
                        Align = alClient
                        DataSource = DmModOS.DsOSCxI
                        TabOrder = 0
                        TitleFont.Charset = DEFAULT_CHARSET
                        TitleFont.Color = clWindowText
                        TitleFont.Height = -11
                        TitleFont.Name = 'Tahoma'
                        TitleFont.Style = []
                        OnMouseUp = DBGOSCxIMouseUp
                        Columns = <
                          item
                            Expanded = False
                            FieldName = 'NO_APLICACAO'
                            Title.Caption = 'Mostrar'
                            Width = 46
                            Visible = True
                          end
                          item
                            Expanded = False
                            FieldName = 'FotoCxa'
                            Width = 331
                            Visible = True
                          end>
                      end
                      object DBMeOSCxI: TDBMemo
                        Left = 2
                        Top = 108
                        Width = 414
                        Height = 56
                        Align = alBottom
                        DataField = 'Detalhes'
                        TabOrder = 1
                      end
                    end
                  end
                  object Panel13: TPanel
                    Left = 0
                    Top = 0
                    Width = 309
                    Height = 335
                    Align = alLeft
                    BevelOuter = bvNone
                    TabOrder = 2
                    object Splitter6: TSplitter
                      Left = 0
                      Top = 231
                      Width = 309
                      Height = 5
                      Cursor = crVSplit
                      Align = alBottom
                      ExplicitTop = 0
                    end
                    object DBGOSCxa: TDBGrid
                      Left = 0
                      Top = 0
                      Width = 309
                      Height = 231
                      Align = alLeft
                      DataSource = DmModOS.DsOSCxa
                      TabOrder = 0
                      TitleFont.Charset = DEFAULT_CHARSET
                      TitleFont.Color = clWindowText
                      TitleFont.Height = -11
                      TitleFont.Name = 'Tahoma'
                      TitleFont.Style = []
                      OnDrawColumnCell = DBGOSCxaDrawColumnCell
                      OnMouseUp = DBGOSCxaMouseUp
                      Columns = <
                        item
                          Expanded = False
                          FieldName = 'Autorizado'
                          Title.Caption = 'A'
                          Width = 17
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Controle'
                          Title.Caption = 'ID'
                          Width = 40
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'Local'
                          Title.Caption = 'Local da caixa'
                          Width = 150
                          Visible = True
                        end
                        item
                          Expanded = False
                          FieldName = 'ValTota'
                          Title.Caption = 'Valor'
                          Visible = True
                        end>
                    end
                    object DBMeOSCxa: TDBMemo
                      Left = 0
                      Top = 236
                      Width = 309
                      Height = 99
                      Align = alBottom
                      DataField = 'Detalhes'
                      DataSource = DmModOS.DsOSCxa
                      TabOrder = 1
                    end
                  end
                end
              end
            end
          end
          object TabSheet6: TTabSheet
            Caption = ' Monitoramento '
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object PnMonitoramento: TPanel
              Left = 0
              Top = 0
              Width = 992
              Height = 352
              Align = alClient
              BevelOuter = bvNone
              ParentBackground = False
              TabOrder = 0
              object DBGOSPipMon: TDBGrid
                Left = 0
                Top = 0
                Width = 320
                Height = 352
                Align = alLeft
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -11
                TitleFont.Name = 'Tahoma'
                TitleFont.Style = []
                OnMouseUp = DBGOSPipMonMouseUp
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Controle'
                    Title.Caption = 'ID'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'PipCad'
                    Title.Caption = 'C'#243'digo'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NO_PIP'
                    Title.Caption = 'Refer'#234'ncia'
                    Width = 151
                    Visible = True
                  end>
              end
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Op'#231#245'es de pagamento'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel9: TPanel
          Left = 0
          Top = 0
          Width = 1000
          Height = 380
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox15: TGroupBox
            Left = 0
            Top = 0
            Width = 509
            Height = 380
            Align = alLeft
            Caption = ' Condi'#231#245'es de pagamento: '
            TabOrder = 0
            object DBGOSPrz: TDBGrid
              Left = 2
              Top = 15
              Width = 505
              Height = 363
              Align = alClient
              DataSource = DmModOS.DsOSPrz
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              OnDrawColumnCell = DBGOSPrzDrawColumnCell
              OnMouseUp = DBGOSPrzMouseUp
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Escolhido'
                  Title.Caption = 'E'
                  Width = 17
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_CONDICAO'
                  Title.Caption = 'Descri'#231#227'o da condi'#231#227'o de pagamento'
                  Width = 384
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DescoPer'
                  Title.Caption = '% Desconto'
                  Visible = True
                end>
            end
          end
          object GroupBox16: TGroupBox
            Left = 509
            Top = 0
            Width = 491
            Height = 380
            Align = alClient
            Caption = ' Parcelas do faturamento selecionado: '
            TabOrder = 1
            object DBGLctFatRef: TDBGrid
              Left = 2
              Top = 15
              Width = 487
              Height = 363
              Align = alClient
              DataSource = DmModOS.DsLctFatRef
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'Tahoma'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'PARCELA'
                  Title.Caption = 'Parc.'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Lancto'
                  Width = 82
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Valor'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencto'
                  Width = 56
                  Visible = True
                end>
            end
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Dados para p'#243's-venda'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GroupBox18: TGroupBox
          Left = 0
          Top = 0
          Width = 625
          Height = 380
          Align = alLeft
          Caption = ' Telefone e e-mails'
          TabOrder = 0
          object Splitter3: TSplitter
            Left = 2
            Top = 161
            Width = 621
            Height = 5
            Cursor = crVSplit
            Align = alTop
            ExplicitTop = 166
            ExplicitWidth = 537
          end
          object DBGEntiTel: TDBGrid
            Left = 2
            Top = 15
            Width = 621
            Height = 146
            Align = alTop
            DataSource = DmModOS.DsEntiTel
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMEETC'
                Title.Caption = 'Tipo de telefone'
                Width = 84
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'TEL_TXT'
                Title.Caption = 'Telefone'
                Width = 120
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Ramal'
                Width = 37
                Visible = True
              end>
          end
          object DBGEntiMail: TDBGrid
            Left = 2
            Top = 166
            Width = 621
            Height = 212
            Align = alClient
            DataSource = DmModOS.DsEntiMail
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMEETC'
                Title.Caption = 'Tipo de e-mail'
                Width = 84
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'EMail'
                Title.Caption = 'E-mail'
                Width = 496
                Visible = True
              end>
          end
        end
        object GroupBox19: TGroupBox
          Left = 625
          Top = 0
          Width = 375
          Height = 380
          Align = alClient
          Caption = ' Alertas comerciais e monitoramentos: '
          TabOrder = 1
          object DBGOSPos: TDBGrid
            Left = 2
            Top = 15
            Width = 371
            Height = 363
            Align = alClient
            DataSource = DmModOS.DsOSPos
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnMouseUp = DBGOSPosMouseUp
            Columns = <
              item
                Expanded = False
                FieldName = 'Dias'
                Visible = True
              end>
          end
        end
      end
      object TabSheet7: TTabSheet
        Caption = 'Particularidades da OS'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGOSChk: TDBGrid
          Left = 0
          Top = 0
          Width = 1000
          Height = 380
          Align = alClient
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = []
          OnMouseUp = DBGOSChkMouseUp
          Columns = <
            item
              Expanded = False
              FieldName = 'NO_ITEM'
              Title.Caption = 'Textos de itens de lista'
              Width = 960
              Visible = True
            end>
        end
      end
    end
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 225
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel7: TPanel
        Left = 2
        Top = 15
        Width = 785
        Height = 208
        Align = alLeft
        BevelOuter = bvNone
        ParentBackground = False
        TabOrder = 0
        object Label7: TLabel
          Left = 4
          Top = 4
          Width = 37
          Height = 13
          Caption = 'C'#243'digo:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label9: TLabel
          Left = 104
          Top = 4
          Width = 37
          Height = 13
          Caption = 'Cliente:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label4: TLabel
          Left = 576
          Top = 4
          Width = 35
          Height = 13
          Caption = 'Status:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label3: TLabel
          Left = 8
          Top = 92
          Width = 31
          Height = 13
          Caption = 'Lugar:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label5: TLabel
          Left = 8
          Top = 188
          Width = 110
          Height = 13
          Caption = 'Fato gerador (Motivo):'
          Color = clBtnFace
          ParentColor = False
        end
        object LaOSOrigem: TLabel
          Left = 296
          Top = 188
          Width = 43
          Height = 13
          Caption = '$ Previa:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label28: TLabel
          Left = 8
          Top = 116
          Width = 60
          Height = 13
          Caption = 'N'#186' contrato:'
          FocusControl = DBEdit20
        end
        object Label29: TLabel
          Left = 8
          Top = 140
          Width = 63
          Height = 13
          Caption = 'Contratante:'
          FocusControl = DBEdit21
        end
        object Label30: TLabel
          Left = 8
          Top = 164
          Width = 44
          Height = 13
          Caption = 'Pagante:'
          FocusControl = DBEdit25
        end
        object Label31: TLabel
          Left = 140
          Top = 116
          Width = 43
          Height = 13
          Caption = 'Contato:'
          FocusControl = DBEdit27
        end
        object DBEdit15: TDBEdit
          Left = 44
          Top = 0
          Width = 56
          Height = 21
          DataField = 'Codigo'
          DataSource = DsOSCab
          TabOrder = 0
        end
        object DBEdit4: TDBEdit
          Left = 144
          Top = 0
          Width = 56
          Height = 21
          DataField = 'Entidade'
          DataSource = DsOSCab
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 200
          Top = 0
          Width = 261
          Height = 21
          DataField = 'NO_ENT'
          DataSource = DsOSCab
          TabOrder = 2
        end
        object DBEdit6: TDBEdit
          Left = 612
          Top = 0
          Width = 32
          Height = 21
          DataField = 'Estatus'
          DataSource = DsOSCab
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 644
          Top = 0
          Width = 136
          Height = 21
          DataField = 'NO_ESTATUS'
          DataSource = DsOSCab
          TabOrder = 4
        end
        object DBEdit2: TDBEdit
          Left = 44
          Top = 88
          Width = 56
          Height = 21
          DataField = 'SiapTerCad'
          DataSource = DsOSCab
          TabOrder = 5
        end
        object DBEdit1: TDBEdit
          Left = 100
          Top = 88
          Width = 333
          Height = 21
          DataField = 'NO_SiapTerCad'
          DataSource = DsOSCab
          TabOrder = 6
        end
        object DBEdit8: TDBEdit
          Left = 496
          Top = 24
          Width = 28
          Height = 21
          DataField = 'FatoGeradr'
          DataSource = DsOSCab
          TabOrder = 7
        end
        object DBEdit7: TDBEdit
          Left = 116
          Top = 184
          Width = 177
          Height = 21
          DataField = 'NO_FatoGeradr'
          DataSource = DsOSCab
          TabOrder = 8
        end
        object DBEdit9: TDBEdit
          Left = 356
          Top = 184
          Width = 76
          Height = 21
          DataField = 'ValorPre'
          DataSource = DsOSCab
          TabOrder = 9
        end
        object GroupBox6: TGroupBox
          Left = 8
          Top = 24
          Width = 125
          Height = 61
          Caption = ' Data Contato: '
          TabOrder = 10
          object Label23: TLabel
            Left = 8
            Top = 16
            Width = 50
            Height = 13
            Caption = 'Realizado:'
            Color = clBtnFace
            ParentColor = False
          end
          object DBEdit16: TDBEdit
            Left = 8
            Top = 32
            Width = 112
            Height = 21
            DataField = 'TXTContat'
            DataSource = DsOSCab
            TabOrder = 0
          end
        end
        object GroupBox1: TGroupBox
          Left = 136
          Top = 24
          Width = 241
          Height = 61
          Caption = ' Data Vistoria: '
          TabOrder = 11
          object Label8: TLabel
            Left = 8
            Top = 16
            Width = 43
            Height = 13
            Caption = 'Previsto:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label10: TLabel
            Left = 124
            Top = 16
            Width = 50
            Height = 13
            Caption = 'Realizado:'
            Color = clBtnFace
            ParentColor = False
          end
          object DBEdit10: TDBEdit
            Left = 8
            Top = 32
            Width = 112
            Height = 21
            DataField = 'TXTVisPrv'
            DataSource = DsOSCab
            TabOrder = 0
          end
          object DBEdit11: TDBEdit
            Left = 124
            Top = 32
            Width = 112
            Height = 21
            DataField = 'TXTVisExe'
            DataSource = DsOSCab
            TabOrder = 1
          end
        end
        object GroupBox2: TGroupBox
          Left = 380
          Top = 24
          Width = 401
          Height = 61
          Caption = ' Data Execu'#231#227'o: '
          TabOrder = 12
          object Label11: TLabel
            Left = 8
            Top = 16
            Width = 43
            Height = 13
            Caption = 'Previsto:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label12: TLabel
            Left = 124
            Top = 16
            Width = 29
            Height = 13
            Caption = 'In'#237'cio:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label13: TLabel
            Left = 240
            Top = 16
            Width = 42
            Height = 13
            Caption = 'T'#233'rmino:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label48: TLabel
            Left = 356
            Top = 16
            Width = 32
            Height = 13
            Caption = 'Texto:'
            FocusControl = DBEdit36
          end
          object DBEdit12: TDBEdit
            Left = 8
            Top = 32
            Width = 112
            Height = 21
            DataField = 'TXTExePrv'
            DataSource = DsOSCab
            TabOrder = 0
          end
          object DBEdit13: TDBEdit
            Left = 125
            Top = 32
            Width = 112
            Height = 21
            DataField = 'TXTExeIni'
            DataSource = DsOSCab
            TabOrder = 1
          end
          object DBEdit14: TDBEdit
            Left = 241
            Top = 32
            Width = 112
            Height = 21
            DataField = 'TXTExeFim'
            DataSource = DsOSCab
            TabOrder = 2
          end
          object DBEdit36: TDBEdit
            Left = 356
            Top = 32
            Width = 36
            Height = 21
            DataField = 'ExeTxtCli1'
            DataSource = DsOSCab
            TabOrder = 3
          end
        end
        object DBEdit20: TDBEdit
          Left = 68
          Top = 112
          Width = 68
          Height = 21
          DataField = 'NumContrat'
          DataSource = DsOSCab
          TabOrder = 13
        end
        object DBEdit21: TDBEdit
          Left = 68
          Top = 136
          Width = 60
          Height = 21
          DataField = 'EntContrat'
          DataSource = DsOSCab
          TabOrder = 14
        end
        object DBEdit24: TDBEdit
          Left = 128
          Top = 136
          Width = 305
          Height = 21
          DataField = 'NO_CTR'
          DataSource = DsOSCab
          TabOrder = 15
        end
        object DBEdit25: TDBEdit
          Left = 68
          Top = 160
          Width = 60
          Height = 21
          DataField = 'EntPagante'
          DataSource = DsOSCab
          TabOrder = 16
        end
        object DBEdit26: TDBEdit
          Left = 128
          Top = 160
          Width = 305
          Height = 21
          DataField = 'NO_PAG'
          DataSource = DsOSCab
          TabOrder = 17
        end
        object DBEdit27: TDBEdit
          Left = 184
          Top = 111
          Width = 56
          Height = 21
          DataField = 'EntiContat'
          DataSource = DsOSCab
          TabOrder = 18
        end
        object DBEdit28: TDBEdit
          Left = 240
          Top = 111
          Width = 193
          Height = 21
          DataField = 'NO_ENTICONTAT'
          DataSource = DsOSCab
          TabOrder = 19
        end
        object DBEdit29: TDBEdit
          Left = 460
          Top = 0
          Width = 112
          Height = 21
          DataField = 'TXTTel_ENT'
          DataSource = DsOSCab
          TabOrder = 20
        end
        object GroupBox21: TGroupBox
          Left = 440
          Top = 88
          Width = 341
          Height = 117
          Caption = ' Valores:'
          TabOrder = 21
          object Label26: TLabel
            Left = 80
            Top = 16
            Width = 48
            Height = 13
            Caption = '$ Servi'#231'o:'
            FocusControl = DBEdit18
          end
          object Label27: TLabel
            Left = 164
            Top = 16
            Width = 58
            Height = 13
            Caption = '$ Desconto:'
            FocusControl = DBEdit19
          end
          object Label37: TLabel
            Left = 250
            Top = 16
            Width = 40
            Height = 13
            Caption = '$ Total: '
            FocusControl = DBEdit22
          end
          object Label32: TLabel
            Left = 8
            Top = 36
            Width = 51
            Height = 13
            Caption = 'Aprovado:'
            FocusControl = DBEdit18
          end
          object Label39: TLabel
            Left = 8
            Top = 60
            Width = 72
            Height = 13
            Caption = 'N'#227'o aprovado:'
            FocusControl = DBEdit30
          end
          object Label40: TLabel
            Left = 8
            Top = 84
            Width = 57
            Height = 13
            Caption = 'Or'#231'amento:'
            FocusControl = DBEdit33
          end
          object DBEdit18: TDBEdit
            Left = 80
            Top = 32
            Width = 80
            Height = 21
            DataField = 'ValorServi'
            DataSource = DsOSCab
            TabOrder = 0
          end
          object DBEdit19: TDBEdit
            Left = 164
            Top = 32
            Width = 80
            Height = 21
            DataField = 'ValorDesco'
            DataSource = DsOSCab
            TabOrder = 1
          end
          object DBEdit22: TDBEdit
            Left = 248
            Top = 32
            Width = 80
            Height = 21
            DataField = 'ValorTotal'
            DataSource = DsOSCab
            TabOrder = 2
          end
          object DBEdit30: TDBEdit
            Left = 80
            Top = 56
            Width = 80
            Height = 21
            DataField = 'InvalServi'
            DataSource = DsOSCab
            TabOrder = 3
          end
          object DBEdit31: TDBEdit
            Left = 164
            Top = 56
            Width = 80
            Height = 21
            DataField = 'InvalDesco'
            DataSource = DsOSCab
            TabOrder = 4
          end
          object DBEdit32: TDBEdit
            Left = 248
            Top = 56
            Width = 80
            Height = 21
            DataField = 'InvalTotal'
            DataSource = DsOSCab
            TabOrder = 5
          end
          object DBEdit33: TDBEdit
            Left = 80
            Top = 80
            Width = 80
            Height = 21
            DataField = 'OrcamServi'
            DataSource = DsOSCab
            TabOrder = 6
          end
          object DBEdit34: TDBEdit
            Left = 164
            Top = 80
            Width = 80
            Height = 21
            DataField = 'OrcamDesco'
            DataSource = DsOSCab
            TabOrder = 7
          end
          object DBEdit35: TDBEdit
            Left = 248
            Top = 80
            Width = 80
            Height = 21
            DataField = 'OrcamTotal'
            DataSource = DsOSCab
            TabOrder = 8
          end
        end
      end
      object Panel12: TPanel
        Left = 787
        Top = 15
        Width = 219
        Height = 208
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Splitter4: TSplitter
          Left = 0
          Top = 119
          Width = 219
          Height = 5
          Cursor = crVSplit
          Align = alBottom
          ExplicitTop = 0
        end
        object GroupBox10: TGroupBox
          Left = 0
          Top = 124
          Width = 219
          Height = 84
          Align = alBottom
          Caption = ' Agentes: '
          TabOrder = 0
          object DBGOSAge: TDBGrid
            Left = 2
            Top = 15
            Width = 215
            Height = 67
            Align = alClient
            DataSource = DmModOS.DsOSAge
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnColEnter = DBGOSAgeColEnter
            OnColExit = DBGOSAgeColExit
            OnDrawColumnCell = DBGOSAgeDrawColumnCell
            OnMouseUp = DBGOSAgeMouseUp
            Columns = <
              item
                Expanded = False
                FieldName = 'Responsa'
                Title.Caption = 'R'
                Width = 17
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_AGENTE'
                Title.Caption = 'Agente'
                Width = 160
                Visible = True
              end>
          end
        end
        object GroupBox25: TGroupBox
          Left = 0
          Top = 0
          Width = 219
          Height = 119
          Align = alClient
          Caption = ' Pragas alvo (pr'#233'-atendimento): '
          TabOrder = 1
          object DBGOSCabAlv: TDBGrid
            Left = 2
            Top = 15
            Width = 215
            Height = 102
            Align = alClient
            DataSource = DmModOS.DsOSCabAlv
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = []
            OnMouseUp = DBGOSCabAlvMouseUp
            Columns = <
              item
                Expanded = False
                FieldName = 'NO_NIVEL'
                Title.Caption = 'N'
                Width = 17
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_PRAGA'
                Title.Caption = 'Praga'
                Width = 160
                Visible = True
              end>
          end
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 693
      Width = 1008
      Height = 64
      Align = alBottom
      TabOrder = 0
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 32
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 296
        Top = 15
        Width = 710
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object Panel2: TPanel
          Left = 608
          Top = 0
          Width = 102
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
        object BtCab: TBitBtn
          Tag = 539
          Left = 4
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&OS'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCabClick
          NumGlyphs = 2
        end
        object BtIts: TBitBtn
          Left = 240
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Servi'#231'o'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          Visible = False
          OnClick = BtItsClick
          NumGlyphs = 2
        end
        object BtOSFrmCab: TBitBtn
          Left = 424
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Aplica'#231#227'o'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          Visible = False
          OnClick = BtOSFrmCabClick
          NumGlyphs = 2
        end
        object BtOSAlv: TBitBtn
          Left = 332
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Praga'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          Visible = False
          OnClick = BtOSAlvClick
          NumGlyphs = 2
        end
        object BtOSMonCab: TBitBtn
          Left = 516
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Monitora'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 5
          Visible = False
          OnClick = BtOSMonCabClick
          NumGlyphs = 2
        end
        object BtFat: TBitBtn
          Tag = 414
          Left = 126
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Faturamento'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 6
          OnClick = BtFatClick
          NumGlyphs = 2
        end
      end
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 1008
    Height = 757
    Align = alClient
    BevelOuter = bvNone
    ParentBackground = False
    TabOrder = 1
    Visible = False
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 1008
      Height = 409
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 37
        Height = 13
        Caption = 'C'#243'digo:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label2: TLabel
        Left = 432
        Top = 16
        Width = 37
        Height = 13
        Caption = 'Cliente:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label6: TLabel
        Left = 92
        Top = 56
        Width = 31
        Height = 13
        Caption = 'Lugar:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label14: TLabel
        Left = 792
        Top = 16
        Width = 35
        Height = 13
        Caption = 'Status:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label15: TLabel
        Left = 564
        Top = 56
        Width = 110
        Height = 13
        Caption = 'Fato gerador (Motivo):'
        Color = clBtnFace
        ParentColor = False
      end
      object Label16: TLabel
        Left = 924
        Top = 56
        Width = 55
        Height = 13
        Caption = 'OS Origem:'
        Color = clBtnFace
        Enabled = False
        ParentColor = False
      end
      object Label34: TLabel
        Left = 772
        Top = 184
        Width = 43
        Height = 13
        Caption = 'Contato:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label35: TLabel
        Left = 16
        Top = 56
        Width = 70
        Height = 13
        Caption = 'Contrato [F4]:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label36: TLabel
        Left = 16
        Top = 184
        Width = 63
        Height = 13
        Caption = 'Contratante:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label38: TLabel
        Left = 428
        Top = 184
        Width = 44
        Height = 13
        Caption = 'Pagante:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label25: TLabel
        Left = 76
        Top = 16
        Width = 45
        Height = 13
        Caption = 'Empresa:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label41: TLabel
        Left = 16
        Top = 232
        Width = 84
        Height = 13
        Caption = 'Valid. or'#231'amento:'
      end
      object Label42: TLabel
        Left = 72
        Top = 252
        Width = 24
        Height = 13
        Caption = 'Dias.'
      end
      object Label47: TLabel
        Left = 16
        Top = 272
        Width = 386
        Height = 13
        Caption = 
          'Texto para cliente na impress'#227'o de execu'#231#227'o: (cadastrado em text' +
          'os gen'#233'ricos)'
        Color = clBtnFace
        ParentColor = False
      end
      object Label50: TLabel
        Left = 16
        Top = 312
        Width = 391
        Height = 13
        Caption = 
          'Texto para cliente na impress'#227'o do or'#231'amento: (cadastrado em tex' +
          'tos gen'#233'ricos)'
        Color = clBtnFace
        ParentColor = False
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdEntidade: TdmkEditCB
        Left = 432
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Entidade'
        UpdCampo = 'Entidade'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdEntidadeChange
        OnEnter = EdEntidadeEnter
        DBLookupComboBox = CBEntidade
        IgnoraDBLookupComboBox = False
      end
      object CBEntidade: TdmkDBLookupComboBox
        Left = 488
        Top = 32
        Width = 301
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        TabOrder = 4
        dmkEditCB = EdEntidade
        QryCampo = 'Entidade'
        UpdType = utYes
      end
      object EdSiapTerCad: TdmkEditCB
        Left = 92
        Top = 72
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'SiapTerCad'
        UpdCampo = 'SiapTerCad'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBSiapTerCad
        IgnoraDBLookupComboBox = False
      end
      object CBSiapTerCad: TdmkDBLookupComboBox
        Left = 148
        Top = 72
        Width = 413
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        TabOrder = 9
        dmkEditCB = EdSiapTerCad
        QryCampo = 'SiapTerCad'
        UpdType = utYes
      end
      object EdEstatus: TdmkEditCB
        Left = 792
        Top = 32
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Estatus'
        UpdCampo = 'Estatus'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBEstatus
        IgnoraDBLookupComboBox = False
      end
      object CBEstatus: TdmkDBLookupComboBox
        Left = 836
        Top = 32
        Width = 160
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DmModOS.DsEstatusOSs
        TabOrder = 6
        dmkEditCB = EdEstatus
        QryCampo = 'Estatus'
        UpdType = utYes
      end
      object EdFatoGeradr: TdmkEditCB
        Left = 564
        Top = 72
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'FatoGeradr'
        UpdCampo = 'FatoGeradr'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdFatoGeradrChange
        DBLookupComboBox = CBFatoGeradr
        IgnoraDBLookupComboBox = False
      end
      object CBFatoGeradr: TdmkDBLookupComboBox
        Left = 608
        Top = 72
        Width = 313
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        TabOrder = 11
        dmkEditCB = EdFatoGeradr
        QryCampo = 'FatoGeradr'
        UpdType = utYes
      end
      object EdOSOrigem: TdmkEdit
        Left = 924
        Top = 72
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'OSOrigem'
        UpdCampo = 'OSOrigem'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object GroupBox3: TGroupBox
        Left = 246
        Top = 100
        Width = 371
        Height = 81
        Caption = ' Data Vistoria: '
        TabOrder = 14
        object Label17: TLabel
          Left = 12
          Top = 32
          Width = 43
          Height = 13
          Caption = 'Previsto:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label18: TLabel
          Left = 12
          Top = 60
          Width = 50
          Height = 13
          Caption = 'Realizado:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label52: TLabel
          Left = 64
          Top = 16
          Width = 29
          Height = 13
          Caption = 'In'#237'cio:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label53: TLabel
          Left = 216
          Top = 16
          Width = 42
          Height = 13
          Caption = 'T'#233'rmino:'
          Color = clBtnFace
          ParentColor = False
        end
        object TPDtaVisPrv: TdmkEditDateTimePicker
          Left = 64
          Top = 32
          Width = 108
          Height = 21
          Date = 0.639644131944805800
          Time = 0.639644131944805800
          TabOrder = 0
          OnExit = TPDtaVisPrvExit
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtaVisPrv'
          UpdCampo = 'DtaVisPrv'
          UpdType = utYes
        end
        object TPDtaVisExe: TdmkEditDateTimePicker
          Left = 64
          Top = 56
          Width = 108
          Height = 21
          Date = 0.639644131944805800
          Time = 0.639644131944805800
          TabOrder = 4
          OnExit = TPDtaVisExeExit
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtaVisExe'
          UpdCampo = 'DtaVisExe'
          UpdType = utYes
        end
        object EdDtaVisPrv: TdmkEdit
          Left = 172
          Top = 32
          Width = 40
          Height = 21
          TabOrder = 1
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'DtaVisPrv'
          UpdCampo = 'DtaVisPrv'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnExit = EdDtaVisPrvExit
        end
        object EdDtaVisExe: TdmkEdit
          Left = 172
          Top = 56
          Width = 40
          Height = 21
          TabOrder = 5
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'DtaVisExe'
          UpdCampo = 'DtaVisExe'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnExit = EdDtaVisExeExit
        end
        object TPFimVisPrv: TdmkEditDateTimePicker
          Left = 216
          Top = 32
          Width = 108
          Height = 21
          Date = 0.639644131944805800
          Time = 0.639644131944805800
          TabOrder = 2
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'FimVisPrv'
          UpdCampo = 'FimVisPrv'
          UpdType = utYes
        end
        object EdFimVisPrv: TdmkEdit
          Left = 324
          Top = 32
          Width = 40
          Height = 21
          TabOrder = 3
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'FimVisPrv'
          UpdCampo = 'FimVisPrv'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object TPFimVisExe: TdmkEditDateTimePicker
          Left = 216
          Top = 56
          Width = 108
          Height = 21
          Date = 0.639644131944805800
          Time = 0.639644131944805800
          TabOrder = 6
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'FimVisExe'
          UpdCampo = 'FimVisExe'
          UpdType = utYes
        end
        object EdFimVisExe: TdmkEdit
          Left = 324
          Top = 56
          Width = 40
          Height = 21
          TabOrder = 7
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'FimVisExe'
          UpdCampo = 'FimVisExe'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
      end
      object GroupBox4: TGroupBox
        Left = 624
        Top = 100
        Width = 373
        Height = 81
        Caption = ' Data Execu'#231#227'o: '
        TabOrder = 15
        object Label19: TLabel
          Left = 12
          Top = 36
          Width = 43
          Height = 13
          Caption = 'Previsto:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label20: TLabel
          Left = 64
          Top = 16
          Width = 29
          Height = 13
          Caption = 'In'#237'cio:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label21: TLabel
          Left = 216
          Top = 16
          Width = 42
          Height = 13
          Caption = 'T'#233'rmino:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label51: TLabel
          Left = 12
          Top = 60
          Width = 50
          Height = 13
          Caption = 'Realizado:'
          Color = clBtnFace
          ParentColor = False
        end
        object TPDtaExePrv: TdmkEditDateTimePicker
          Left = 64
          Top = 32
          Width = 108
          Height = 21
          Date = 0.639644131944805800
          Time = 0.639644131944805800
          TabOrder = 0
          OnExit = TPDtaExePrvExit
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtaExePrv'
          UpdCampo = 'DtaExePrv'
          UpdType = utYes
        end
        object TPDtaExeIni: TdmkEditDateTimePicker
          Left = 64
          Top = 56
          Width = 108
          Height = 21
          Date = 0.639644131944805800
          Time = 0.639644131944805800
          TabOrder = 4
          OnExit = TPDtaExeIniExit
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtaExeIni'
          UpdCampo = 'DtaExeIni'
          UpdType = utYes
        end
        object TPDtaExeFim: TdmkEditDateTimePicker
          Left = 216
          Top = 56
          Width = 108
          Height = 21
          Date = 0.639644131944805800
          Time = 0.639644131944805800
          TabOrder = 6
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtaExeFim'
          UpdCampo = 'DtaExeFim'
          UpdType = utYes
        end
        object EdDtaExePrv: TdmkEdit
          Left = 172
          Top = 32
          Width = 40
          Height = 21
          TabOrder = 1
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'DtaExePrv'
          UpdCampo = 'DtaExePrv'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnExit = EdDtaExePrvExit
        end
        object EdDtaExeIni: TdmkEdit
          Left = 172
          Top = 56
          Width = 40
          Height = 21
          TabOrder = 5
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'DtaExeIni'
          UpdCampo = 'DtaExeIni'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnExit = EdDtaExeIniExit
        end
        object EdDtaExeFim: TdmkEdit
          Left = 324
          Top = 56
          Width = 40
          Height = 21
          TabOrder = 7
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'DtaExeFim'
          UpdCampo = 'DtaExeFim'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object TPFimExePrv: TdmkEditDateTimePicker
          Left = 216
          Top = 32
          Width = 108
          Height = 21
          Date = 0.639644131944805800
          Time = 0.639644131944805800
          TabOrder = 2
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'FimExePrv'
          UpdCampo = 'FimExePrv'
          UpdType = utYes
        end
        object EdFimExePrv: TdmkEdit
          Left = 324
          Top = 32
          Width = 40
          Height = 21
          TabOrder = 3
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'FimExePrv'
          UpdCampo = 'FimExePrv'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
      end
      object GroupBox5: TGroupBox
        Left = 16
        Top = 100
        Width = 225
        Height = 81
        Caption = ' Data Contato: '
        TabOrder = 13
        object Label22: TLabel
          Left = 12
          Top = 60
          Width = 50
          Height = 13
          Caption = 'Realizado:'
          Color = clBtnFace
          ParentColor = False
        end
        object TPDtaContat: TdmkEditDateTimePicker
          Left = 68
          Top = 56
          Width = 108
          Height = 21
          Date = 0.639644131944805800
          Time = 0.639644131944805800
          TabOrder = 0
          ReadOnly = False
          DefaultEditMask = '!99/99/99;1;_'
          AutoApplyEditMask = True
          QryCampo = 'DtaContat'
          UpdCampo = 'DtaContat'
          UpdType = utYes
        end
        object EdDtaContat: TdmkEdit
          Left = 176
          Top = 56
          Width = 40
          Height = 21
          TabOrder = 1
          FormatType = dmktfTime
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '00:00'
          QryCampo = 'DtaContat'
          UpdCampo = 'DtaContat'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
      end
      object EdEntiContat: TdmkEditCB
        Left = 772
        Top = 200
        Width = 44
        Height = 21
        Alignment = taRightJustify
        TabOrder = 20
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EntiContat'
        UpdCampo = 'EntiContat'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBEntiContat
        IgnoraDBLookupComboBox = False
      end
      object CBEntiContat: TdmkDBLookupComboBox
        Left = 816
        Top = 200
        Width = 180
        Height = 21
        KeyField = 'Controle'
        ListField = 'Nome'
        ListSource = DmModOS.DsEntiContat
        TabOrder = 21
        dmkEditCB = EdEntiContat
        QryCampo = 'EntiContat'
        UpdType = utYes
      end
      object EdNumContrat: TdmkEdit
        Left = 16
        Top = 72
        Width = 72
        Height = 21
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'NumContrat'
        UpdCampo = 'NumContrat'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnKeyDown = EdNumContratKeyDown
      end
      object EdEntContrat: TdmkEditCB
        Left = 16
        Top = 200
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EntContrat'
        UpdCampo = 'EntContrat'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBEntContrat
        IgnoraDBLookupComboBox = False
      end
      object CBEntContrat: TdmkDBLookupComboBox
        Left = 72
        Top = 200
        Width = 353
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        TabOrder = 17
        dmkEditCB = EdEntContrat
        QryCampo = 'EntContrat'
        UpdType = utYes
      end
      object EdEntPagante: TdmkEditCB
        Left = 428
        Top = 200
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'EntPagante'
        UpdCampo = 'EntPagante'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBEntPagante
        IgnoraDBLookupComboBox = False
      end
      object CBEntPagante: TdmkDBLookupComboBox
        Left = 484
        Top = 200
        Width = 285
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        TabOrder = 19
        dmkEditCB = EdEntPagante
        QryCampo = 'EntPagante'
        UpdType = utYes
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 132
        Top = 32
        Width = 297
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 2
        dmkEditCB = EdEmpresa
        QryCampo = 'Empresa'
        UpdType = utYes
      end
      object EdEmpresa: TdmkEditCB
        Left = 76
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Empresa'
        UpdCampo = 'Empresa'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdEntidadeChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object EdValiDdOrca: TdmkEdit
        Left = 16
        Top = 248
        Width = 53
        Height = 21
        Alignment = taRightJustify
        TabOrder = 22
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '1'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '1'
        QryCampo = 'ValiDdOrca'
        UpdCampo = 'ValiDdOrca'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 1
      end
      object EdExeTxtCli1: TdmkEditCB
        Left = 16
        Top = 288
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 24
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ExeTxtCli1'
        UpdCampo = 'ExeTxtCli1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBExeTxtCli1
        IgnoraDBLookupComboBox = False
      end
      object CBExeTxtCli1: TdmkDBLookupComboBox
        Left = 72
        Top = 288
        Width = 925
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        TabOrder = 25
        dmkEditCB = EdExeTxtCli1
        QryCampo = 'ExeTxtCli1'
        UpdType = utYes
      end
      object CGOperacao: TdmkCheckGroup
        Left = 108
        Top = 224
        Width = 889
        Height = 45
        Caption = ' Opera'#231#227'o: '
        Columns = 2
        Items.Strings = (
          'Dedetiza'#231#227'o'
          'Caixa d'#39#225'gua')
        TabOrder = 23
        OnClick = CGOperacaoClick
        QryCampo = 'Operacao'
        UpdCampo = 'Operacao'
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
      object EdExeTxtCli2: TdmkEditCB
        Left = 16
        Top = 328
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 26
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ExeTxtCli2'
        UpdCampo = 'ExeTxtCli2'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBExeTxtCli2
        IgnoraDBLookupComboBox = False
      end
      object CBExeTxtCli2: TdmkDBLookupComboBox
        Left = 72
        Top = 328
        Width = 925
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        TabOrder = 27
        dmkEditCB = EdExeTxtCli2
        QryCampo = 'ExeTxtCli2'
        UpdType = utYes
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 694
      Width = 1008
      Height = 63
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object Panel1: TPanel
        Left = 868
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
          NumGlyphs = 2
        end
      end
    end
    object MeObsGaranti: TdmkMemo
      Left = 0
      Top = 409
      Width = 1008
      Height = 208
      Align = alTop
      TabOrder = 2
      QryCampo = 'ObsGaranti'
      UpdCampo = 'ObsGaranti'
      UpdType = utYes
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 0
        OnClick = SbImprimeClick
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 1
        OnClick = SbNovoClick
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 237
      Height = 52
      Align = alLeft
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 218
        Height = 32
        Caption = 'Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 218
        Height = 32
        Caption = 'Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 218
        Height = 32
        Caption = 'Ordem de Servi'#231'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBOperacao: TGroupBox
      Left = 453
      Top = 0
      Width = 507
      Height = 52
      Align = alClient
      TabOrder = 3
      object LaOperacaoA: TLabel
        Left = 7
        Top = 9
        Width = 45
        Height = 32
        Caption = '???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaOperacaoB: TLabel
        Left = 9
        Top = 11
        Width = 45
        Height = 32
        Caption = '???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaOperacaoC: TLabel
        Left = 8
        Top = 10
        Width = 45
        Height = 32
        Caption = '???'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 1008
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 746
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
    object Panel6: TPanel
      Left = 748
      Top = 15
      Width = 258
      Height = 27
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object Label24: TLabel
        Left = 168
        Top = 6
        Width = 45
        Height = 13
        Caption = 'Empresa:'
      end
      object Label33: TLabel
        Left = 35
        Top = 6
        Width = 77
        Height = 13
        Caption = 'Dias P'#243's Venda:'
      end
      object DBEdit17: TDBEdit
        Left = 116
        Top = 4
        Width = 41
        Height = 21
        DataField = 'Empresa'
        DataSource = DsOSCab
        TabOrder = 0
      end
      object DBEdit23: TDBEdit
        Left = 218
        Top = 2
        Width = 36
        Height = 21
        DataField = 'DdsPosVda'
        DataSource = DsOSCab
        TabOrder = 1
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtCab
    Left = 428
    Top = 64
  end
  object QrOSCab: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrOSCabBeforeOpen
    BeforeClose = QrOSCabBeforeClose
    AfterClose = QrOSCabAfterClose
    AfterScroll = QrOSCabAfterScroll
    OnCalcFields = QrOSCabCalcFields
    SQL.Strings = (
      'SELECT eco.Nome NO_ENTICONTAT, cab.*, '
      'fge.Nome NO_FatoGeradr,'
      'sta.Nome NO_ESTATUS, stc.Nome NO_SiapTerCad,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,'
      'IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Tel_ENT,'
      'IF(pag.Tipo=0, pag.RazaoSocial, pag.Nome) NO_PAG,'
      'IF(ctr.Tipo=0, ctr.RazaoSocial, ctr.Nome) NO_CTR,'
      'car.Nome NO_CART, ppc.Nome NO_PRZ, '
      'tg1.Nome NO_ExeTxtCli1'
      'FROM oscab cab'
      'LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade'
      'LEFT JOIN entidades pag ON pag.Codigo=cab.EntPagante'
      'LEFT JOIN entidades ctr ON ctr.Codigo=cab.EntContrat'
      'LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr'
      'LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus'
      'LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad'
      'LEFT JOIN carteiras car ON car.Codigo=cab.CartEmis'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=cab.CondicaoPg'
      'LEFT JOIN enticontat eco ON eco.Controle=cab.EntiContat'
      'LEFT JOIN txtgeneric tg1 ON tg1.Codigo=cab.ExeTxtCli1'
      ''
      'WHERE cab.Codigo>0')
    Left = 372
    Top = 64
    object QrOSCabCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'oscab.Codigo'
    end
    object QrOSCabEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'oscab.Entidade'
    end
    object QrOSCabEstatus: TIntegerField
      FieldName = 'Estatus'
      Origin = 'oscab.Estatus'
    end
    object QrOSCabFatoGeradr: TIntegerField
      FieldName = 'FatoGeradr'
      Origin = 'oscab.FatoGeradr'
    end
    object QrOSCabOSOrigem: TIntegerField
      FieldName = 'OSOrigem'
      Origin = 'oscab.OSOrigem'
    end
    object QrOSCabDtaContat: TDateTimeField
      FieldName = 'DtaContat'
      Origin = 'oscab.DtaContat'
    end
    object QrOSCabDtaVisPrv: TDateTimeField
      FieldName = 'DtaVisPrv'
      Origin = 'oscab.DtaVisPrv'
    end
    object QrOSCabDtaVisExe: TDateTimeField
      FieldName = 'DtaVisExe'
      Origin = 'oscab.DtaVisExe'
    end
    object QrOSCabDtaExePrv: TDateTimeField
      FieldName = 'DtaExePrv'
      Origin = 'oscab.DtaExePrv'
    end
    object QrOSCabDtaExeIni: TDateTimeField
      FieldName = 'DtaExeIni'
      Origin = 'oscab.DtaExeIni'
    end
    object QrOSCabDtaExeFim: TDateTimeField
      FieldName = 'DtaExeFim'
      Origin = 'oscab.DtaExeFim'
    end
    object QrOSCabLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'oscab.Lk'
    end
    object QrOSCabDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'oscab.DataCad'
    end
    object QrOSCabUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'oscab.UserCad'
    end
    object QrOSCabDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'oscab.DataAlt'
    end
    object QrOSCabUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'oscab.UserAlt'
    end
    object QrOSCabAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'oscab.AlterWeb'
    end
    object QrOSCabAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'oscab.Ativo'
    end
    object QrOSCabNO_FatoGeradr: TWideStringField
      FieldName = 'NO_FatoGeradr'
      Origin = 'fatogeradr.Nome'
    end
    object QrOSCabNO_ESTATUS: TWideStringField
      FieldName = 'NO_ESTATUS'
      Origin = 'estatusoss.Nome'
      Size = 60
    end
    object QrOSCabNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrOSCabSiapTerCad: TIntegerField
      FieldName = 'SiapTerCad'
      Origin = 'oscab.SiapTerCad'
    end
    object QrOSCabNO_SiapTerCad: TWideStringField
      FieldName = 'NO_SiapTerCad'
      Origin = 'siaptercad.Nome'
      Size = 100
    end
    object QrOSCabTXTVisPrv: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTVisPrv'
      Calculated = True
    end
    object QrOSCabTXTContat: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTContat'
      Calculated = True
    end
    object QrOSCabTXTVisExe: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTVisExe'
      Calculated = True
    end
    object QrOSCabTXTExePrv: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTExePrv'
      Calculated = True
    end
    object QrOSCabTXTExeIni: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTExeIni'
      Calculated = True
    end
    object QrOSCabTXTExeFim: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTExeFim'
      Calculated = True
    end
    object QrOSCabDdsPosVda: TIntegerField
      FieldName = 'DdsPosVda'
      Origin = 'oscab.DdsPosVda'
      DisplayFormat = '00'
    end
    object QrOSCabEntiContat: TIntegerField
      FieldName = 'EntiContat'
      Origin = 'oscab.EntiContat'
    end
    object QrOSCabNumContrat: TIntegerField
      FieldName = 'NumContrat'
      Origin = 'oscab.NumContrat'
    end
    object QrOSCabEntPagante: TIntegerField
      FieldName = 'EntPagante'
      Origin = 'oscab.EntPagante'
    end
    object QrOSCabEntContrat: TIntegerField
      FieldName = 'EntContrat'
      Origin = 'oscab.EntContrat'
    end
    object QrOSCabEmpresa: TIntegerField
      FieldName = 'Empresa'
      Origin = 'oscab.Empresa'
    end
    object QrOSCabNO_PAG: TWideStringField
      FieldName = 'NO_PAG'
      Size = 100
    end
    object QrOSCabNO_CTR: TWideStringField
      FieldName = 'NO_CTR'
      Size = 100
    end
    object QrOSCabDtaLibFat: TDateTimeField
      FieldName = 'DtaLibFat'
      Origin = 'oscab.DtaLibFat'
    end
    object QrOSCabDtaFimFat: TDateTimeField
      FieldName = 'DtaFimFat'
      Origin = 'oscab.DtaFimFat'
    end
    object QrOSCabCondicaoPg: TIntegerField
      FieldName = 'CondicaoPg'
      Origin = 'oscab.CondicaoPg'
    end
    object QrOSCabCartEmis: TIntegerField
      FieldName = 'CartEmis'
      Origin = 'oscab.CartEmis'
    end
    object QrOSCabNO_CART: TWideStringField
      FieldName = 'NO_CART'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrOSCabNO_PRZ: TWideStringField
      FieldName = 'NO_PRZ'
      Origin = 'pediprzcab.Nome'
      Size = 50
    end
    object QrOSCabSerNF: TWideStringField
      FieldName = 'SerNF'
      Origin = 'oscab.SerNF'
      Size = 3
    end
    object QrOSCabNumNF: TIntegerField
      FieldName = 'NumNF'
      Origin = 'oscab.NumNF'
    end
    object QrOSCabNO_ENTICONTAT: TWideStringField
      FieldName = 'NO_ENTICONTAT'
      Origin = 'enticontat.Nome'
      Size = 30
    end
    object QrOSCabTel_ENT: TWideStringField
      FieldName = 'Tel_ENT'
    end
    object QrOSCabTXTTel_ENT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TXTTel_ENT'
      Size = 30
      Calculated = True
    end
    object QrOSCabValorServi: TFloatField
      FieldName = 'ValorServi'
      Origin = 'oscab.ValorServi'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabValorDesco: TFloatField
      FieldName = 'ValorDesco'
      Origin = 'oscab.ValorDesco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabValorOutrs: TFloatField
      FieldName = 'ValorOutrs'
      Origin = 'oscab.ValorOutrs'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabInvalServi: TFloatField
      FieldName = 'InvalServi'
      Origin = 'oscab.InvalServi'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabValorTotal: TFloatField
      FieldName = 'ValorTotal'
      Origin = 'oscab.ValorTotal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabInvalDesco: TFloatField
      FieldName = 'InvalDesco'
      Origin = 'oscab.InvalDesco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabInvalOutrs: TFloatField
      FieldName = 'InvalOutrs'
      Origin = 'oscab.InvalOutrs'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabInvalTotal: TFloatField
      FieldName = 'InvalTotal'
      Origin = 'oscab.InvalTotal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabOrcamServi: TFloatField
      FieldName = 'OrcamServi'
      Origin = 'oscab.OrcamServi'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabOrcamDesco: TFloatField
      FieldName = 'OrcamDesco'
      Origin = 'oscab.OrcamDesco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabOrcamOutrs: TFloatField
      FieldName = 'OrcamOutrs'
      Origin = 'oscab.OrcamOutrs'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabOrcamTotal: TFloatField
      FieldName = 'OrcamTotal'
      Origin = 'oscab.OrcamTotal'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOSCabValiDdOrca: TIntegerField
      FieldName = 'ValiDdOrca'
      Origin = 'oscab.ValiDdOrca'
    end
    object QrOSCabOperacao: TIntegerField
      FieldName = 'Operacao'
      Origin = 'oscab.Operacao'
    end
    object QrOSCabExeTxtCli1: TIntegerField
      FieldName = 'ExeTxtCli1'
      Origin = 'oscab.ExeTxtCli1'
    end
    object QrOSCabNO_ExeTxtCli1: TWideStringField
      FieldName = 'NO_ExeTxtCli1'
      Size = 100
    end
    object QrOSCabExeTxtCli2: TIntegerField
      FieldName = 'ExeTxtCli2'
    end
    object QrOSCabNO_ExeTxtCli2: TWideStringField
      FieldName = 'NO_ExeTxtCli2'
      Size = 100
    end
    object QrOSCabValorPre: TFloatField
      FieldName = 'ValorPre'
    end
    object QrOSCabNO_OPERACAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_OPERACAO'
      Size = 255
      Calculated = True
    end
    object QrOSCabObsGaranti: TWideMemoField
      FieldName = 'ObsGaranti'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrOSCabFimVisPrv: TDateTimeField
      FieldName = 'FimVisPrv'
    end
    object QrOSCabFimVisExe: TDateTimeField
      FieldName = 'FimVisExe'
    end
    object QrOSCabFimExePrv: TDateTimeField
      FieldName = 'FimExePrv'
    end
  end
  object DsOSCab: TDataSource
    DataSet = QrOSCab
    Left = 400
    Top = 64
  end
  object PMIts: TPopupMenu
    OnPopup = PMItsPopup
    Left = 436
    Top = 568
    object ItsInclui1: TMenuItem
      Caption = '&Adiciona'
      Enabled = False
      OnClick = ItsInclui1Click
    end
    object ItsAltera1: TMenuItem
      Caption = '&Edita'
      Enabled = False
      OnClick = ItsAltera1Click
    end
    object ItsExclui1: TMenuItem
      Caption = '&Remove'
      Enabled = False
      OnClick = ItsExclui1Click
    end
  end
  object PMCab: TPopupMenu
    OnPopup = PMCabPopup
    Left = 336
    Top = 564
    object CabInclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = CabInclui1Click
    end
    object CabAltera1: TMenuItem
      Caption = '&Altera'
      Enabled = False
      OnClick = CabAltera1Click
    end
    object CabExclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
      OnClick = CabExclui1Click
    end
  end
  object PMOSFrmCab: TPopupMenu
    OnPopup = PMOSFrmCabPopup
    Left = 700
    Top = 584
    object Cabealho4: TMenuItem
      Caption = '&Cabe'#231'alho'
      object ItsInclui4: TMenuItem
        Caption = '&Adiciona'
        Enabled = False
        OnClick = ItsInclui4Click
      end
      object ItsAltera4: TMenuItem
        Caption = '&Edita'
        Enabled = False
        OnClick = ItsAltera4Click
      end
      object ItsExclui4: TMenuItem
        Caption = '&Remove'
        Enabled = False
        OnClick = ItsExclui4Click
      end
    end
    object Itens4: TMenuItem
      Caption = '&Itens'
      OnClick = Itens4Click
    end
    object Abrangncia1: TMenuItem
      Caption = '&Abrang'#234'ncia'
      OnClick = Abrangncia1Click
    end
  end
  object PMOSCabAlv: TPopupMenu
    OnPopup = PMOSCabAlvPopup
    Left = 456
    Top = 64
    object IncluiOSCabAlv1: TMenuItem
      Caption = '&Adiciona, muda ou retira pragas alvo'
      OnClick = IncluiOSCabAlv1Click
    end
    object ExcluiOSCabAlv1: TMenuItem
      Caption = '&Retira a praga alvo selecionada'
      OnClick = ExcluiOSCabAlv1Click
    end
  end
  object PMOSMonCab: TPopupMenu
    OnPopup = PMOSMonCabPopup
    Left = 792
    Top = 568
    object Cabecalho5: TMenuItem
      Caption = '&Cabe'#231'alho'
      object ItsInclui5: TMenuItem
        Caption = '&Adiciona'
        Enabled = False
        OnClick = ItsInclui5Click
      end
      object ItsAltera5: TMenuItem
        Caption = '&Edita'
        Enabled = False
        OnClick = ItsAltera5Click
      end
      object ItsExclui5: TMenuItem
        Caption = '&Remove'
        Enabled = False
        OnClick = ItsExclui5Click
      end
    end
    object Itens5: TMenuItem
      Caption = '&Itens'
      OnClick = Itens5Click
    end
  end
  object PMFat: TPopupMenu
    OnPopup = PMFatPopup
    Left = 380
    Top = 564
    object Fatura1: TMenuItem
      Caption = '&Fatura todos servi'#231'os da OS'
      OnClick = Fatura1Click
    end
    object Gerabloqueto1: TMenuItem
      Caption = 'Emite boleto(s)'
      OnClick = Gerabloqueto1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Excluifaturamento1: TMenuItem
      Caption = 'Exclui todo faturamento'
      OnClick = Excluifaturamento1Click
    end
  end
  object PMOSPos: TPopupMenu
    OnPopup = PMCabPopup
    Left = 464
    Top = 568
    object IncluiC3: TMenuItem
      Caption = '&Inclui alerta comercial'
      OnClick = IncluiC3Click
    end
    object AlteraC3: TMenuItem
      Caption = '&Altera o alerta selecionado'
      OnClick = AlteraC3Click
    end
    object ExcluiC3: TMenuItem
      Caption = '&Exclui o alerta selecionado'
      OnClick = ExcluiC3Click
    end
  end
  object PMOSCxa: TPopupMenu
    OnPopup = PMOSCxaPopup
    Left = 100
    Top = 652
    object ItsInclui2: TMenuItem
      Caption = '&Adiciona caixa d'#180#224'gua'
      OnClick = ItsInclui2Click
    end
    object ItsAltera2: TMenuItem
      Caption = '&Edita caixa d'#180#224'gua'
      Enabled = False
      OnClick = ItsAltera2Click
    end
    object ItsExclui2: TMenuItem
      Caption = '&Retira caixa d'#180#224'gua'
      Enabled = False
      OnClick = ItsExclui2Click
    end
  end
  object PMOSPrz: TPopupMenu
    OnPopup = PMOSPrzPopup
    Left = 852
    Top = 568
    object OSPrz1_Inclui: TMenuItem
      Caption = '&Adiciona nova condi'#231#227'o de pagamento'
      Enabled = False
      OnClick = OSPrz1_IncluiClick
    end
    object OSPrz1_Altera: TMenuItem
      Caption = '&Edita a condi'#231#227'o de pagamento atual'
      Enabled = False
      OnClick = OSPrz1_AlteraClick
    end
    object OSPrz1_Exclui: TMenuItem
      Caption = '&Remove condi'#231'ao de pagamento atual'
      Enabled = False
      OnClick = OSPrz1_ExcluiClick
    end
  end
  object PMOSPipMon: TPopupMenu
    Left = 896
    Top = 572
    object AdicionaPIPsativos1: TMenuItem
      Caption = '&Adiciona PMVs'
      OnClick = AdicionaPIPsativos1Click
    end
    object RemovePIPs1: TMenuItem
      Caption = '&Remove PMVs'
      OnClick = RemovePIPs1Click
    end
  end
  object PMOSCxI: TPopupMenu
    OnPopup = PMOSCxIPopup
    Left = 128
    Top = 652
    object ItsInclui6: TMenuItem
      Caption = '&Inclui foto de caixa'
      OnClick = ItsInclui6Click
    end
    object ItsAltera6: TMenuItem
      Caption = '&Alterai foto de caixa'
      OnClick = ItsAltera6Click
    end
    object ItsExclui6: TMenuItem
      Caption = '&Exclui foto de caixa'
      OnClick = ItsExclui6Click
    end
  end
  object PMOSAge: TPopupMenu
    OnPopup = PMOSAgePopup
    Left = 484
    Top = 64
    object IncluiOSAge1: TMenuItem
      Caption = '&Adiciona agente(s)'
      OnClick = IncluiOSAge1Click
    end
    object RemoveOSAge1: TMenuItem
      Caption = '&Retira agente'
      OnClick = RemoveOSAge1Click
    end
  end
  object PMOSChk: TPopupMenu
    Left = 936
    Top = 572
    object Incluiitens1: TMenuItem
      Caption = '&Inclui itens'
      OnClick = Incluiitens1Click
    end
    object Removeitens1: TMenuItem
      Caption = '&Remove itens'
      OnClick = Removeitens1Click
    end
  end
end
