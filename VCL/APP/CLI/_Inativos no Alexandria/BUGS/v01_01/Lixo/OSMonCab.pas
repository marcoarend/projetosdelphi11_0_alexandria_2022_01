unit OSMonCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, AdvObj, BaseGrid, AdvGrid, DBAdvGrid, Bugs_Tabs, dmkRadioGroup;

type
  TFmOSMonCab = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    GroupBox2: TGroupBox;
    GBDados: TGroupBox;
    Label7: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit15: TDBEdit;
    QrOSSrv: TmySQLQuery;
    QrOSSrvNO_DesServico: TWideStringField;
    QrOSSrvCodigo: TIntegerField;
    QrOSSrvControle: TIntegerField;
    QrOSSrvDesServico: TIntegerField;
    QrOSSrvLk: TIntegerField;
    QrOSSrvDataCad: TDateField;
    QrOSSrvDataAlt: TDateField;
    QrOSSrvUserCad: TIntegerField;
    QrOSSrvUserAlt: TIntegerField;
    QrOSSrvAlterWeb: TSmallintField;
    QrOSSrvAtivo: TSmallintField;
    DsOSSrv: TDataSource;
    QrFormulas: TmySQLQuery;
    DsFormulas: TDataSource;
    GBEdita: TGroupBox;
    Label1: TLabel;
    Label9: TLabel;
    SbFormulas: TSpeedButton;
    Label3: TLabel;
    EdConta: TdmkEdit;
    EdNome: TdmkEdit;
    CBFormula: TdmkDBLookupComboBox;
    EdFormula: TdmkEditCB;
    Label2: TLabel;
    EdEquipAplic: TdmkEditCB;
    CBEquipAplic: TdmkDBLookupComboBox;
    SbEquipAplic: TSpeedButton;
    EdQtdTot: TdmkEdit;
    Label6: TLabel;
    EdQtdQSP: TdmkEdit;
    Label8: TLabel;
    QrFormulasCodigo: TIntegerField;
    QrFormulasNome: TWideStringField;
    QrEquipAplic: TmySQLQuery;
    DsEquipAplic: TDataSource;
    QrEquipAplicControle: TIntegerField;
    QrEquipAplicNome: TWideStringField;
    Label10: TLabel;
    EdPipCad: TdmkEditCB;
    CBPipCad: TdmkDBLookupComboBox;
    SBPIPCad: TSpeedButton;
    QrPIPs: TmySQLQuery;
    DsPIPs: TDataSource;
    QrPIPsCodigo: TIntegerField;
    QrPIPsNome: TWideStringField;
    QrPIPsEquipamento: TIntegerField;
    QrPIPsOSMonCab: TIntegerField;
    QrTipoAplica: TmySQLQuery;
    QrTipoAplicaCodigo: TIntegerField;
    QrTipoAplicaNome: TWideStringField;
    DsTipoAplica: TDataSource;
    Label11: TLabel;
    EdTipoAplica: TdmkEditCB;
    CBTipoAplica: TdmkDBLookupComboBox;
    SBTipoAplica: TSpeedButton;
    RGDiluente: TdmkRadioGroup;
    QrFormulasEquipAplic: TIntegerField;
    QrFormulasQtdTot: TFloatField;
    QrFormulasQtdQSP: TFloatField;
    QrFormulasCusTot: TFloatField;
    QrFormulasAplicacao: TIntegerField;
    QrFormulasDiluente: TSmallintField;
    QrFormulasTipoAplica: TIntegerField;
    QrUnidMed: TmySQLQuery;
    QrUnidMedCodigo: TIntegerField;
    QrUnidMedCodUsu: TIntegerField;
    QrUnidMedSigla: TWideStringField;
    QrUnidMedNome: TWideStringField;
    DsUnidMed: TDataSource;
    Label12: TLabel;
    EdUnidMed: TdmkEditCB;
    EdSigla: TdmkEdit;
    CBUnidMed: TdmkDBLookupComboBox;
    SpeedButton5: TSpeedButton;
    QrFormulasCU_UNIDMED: TIntegerField;
    QrFormulasSIGLA: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbFormulasClick(Sender: TObject);
    procedure SbEquipAplicClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBPIPCadClick(Sender: TObject);
    procedure SBTipoAplicaClick(Sender: TObject);
    procedure EdFormulaChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure EdUnidMedChange(Sender: TObject);
    procedure EdSiglaChange(Sender: TObject);
    procedure EdSiglaExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FPipCad, FCodigo, FControle, FConta: Integer;
    FQrOSMonCab, FQrOSMonRec: TmySQLQuery;
    //
    procedure ReopenPIPs(Codigo: Integer);
  end;

  var
  FmOSMonCab: TFmOSMonCab;

implementation

uses UnMyObjects, Module, UMySQLModule, DmkDAC_PF, ModuleGeral,
  Principal, ModProd, UnidMed, MyDBCheck, OSUnit;

{$R *.DFM}

procedure TFmOSMonCab.BtOKClick(Sender: TObject);
var
  Nome: String;
  Codigo, Controle, Conta, Formula, EquipAplic, PipCad, TipoAplica,
    Diluente, UnidMed: Integer;
  //CusTot,
  QtdTot, QtdQSP: Double;
begin
  Codigo         := FCodigo;
  Controle       := FControle;
  Nome           := EdNome.Text;
  Formula        := EdFormula.ValueVariant;
  EquipAplic     := EdEquipAplic.ValueVariant;
  QtdTot         := EdQtdTot.ValueVariant;
  QtdQSP         := EdQtdQSP.ValueVariant;
  PipCad         := EdPipCad.ValueVariant;
  //CusTot         := ;
  TipoAplica     := EdTipoAplica.ValueVariant;
  Diluente       := RGDiluente.ItemIndex;
  if EdUnidMed.ValueVariant <> 0 then
    UnidMed := QrUnidMedCodigo.Value
  else
    Unidmed := 0;

  //
  Conta := EdConta.ValueVariant;
  Conta := UMyMod.BPGS1I32('osmoncab', 'Conta', '', '', tsDef,
    ImgTipo.SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'osmoncab', False, [
  'Codigo', 'Controle', 'Nome',
  'Formula', 'EquipAplic', 'QtdTot',
  'QtdQSP'(*, 'CusTot'*), 'pipcad',
  'TipoAplica', 'Diluente', 'UnidMed'], [
  'Conta'], [
  Codigo, Controle, Nome,
  Formula, EquipAplic, QtdTot,
  QtdQSP(*, CusTot*), PipCad,
  TipoAplica, Diluente, UnidMed], [
  Conta], True) then
  begin
    UnOSUnit.ReopenOSMonCab(FQrOSMonCab, Controle, Conta);
    if ImgTipo.SQLType = stIns then
      FConta := Conta;
    if PipCad <> 0 then
      Dmod.AtualizaPIP(PipCad);
    Close;
  end;
end;

procedure TFmOSMonCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSMonCab.EdFormulaChange(Sender: TObject);
begin
  if ImgTipo.SQLType = stIns then
  begin
    EdQtdTot.ValueVariant := QrFormulasQtdTot.Value;
    EdEquipAplic.ValueVariant := QrFormulasEquipAplic.Value;
    CBEquipAplic.KeyValue := QrFormulasEquipAplic.Value;
    EdTipoAplica.ValueVariant := QrFormulasTipoAplica.Value;
    CBTipoAplica.KeyValue := QrFormulasTipoAplica.Value;
    RGDiluente.ItemIndex := QrFormulasDiluente.Value;
    EdSigla.Text := QrFormulasSIGLA.Value;
    EdUnidMed.ValueVariant := QrFormulasCU_UnidMed.Value;
    CBUnidMed.KeyValue := QrFormulasCU_UnidMed.Value;
  end;
end;

procedure TFmOSMonCab.EdSiglaChange(Sender: TObject);
begin
  if EdSigla.Focused then
    DmProd.PesquisaPorSigla(False, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmOSMonCab.EdSiglaExit(Sender: TObject);
begin
  DmProd.PesquisaPorSigla(True, EdSigla, EdUnidMed, CBUnidMed);
end;

procedure TFmOSMonCab.EdUnidMedChange(Sender: TObject);
begin
  if not EdSigla.Focused then
    DmProd.PesquisaPorCodigo(EdUnidMed.ValueVariant, EdSigla);
end;

procedure TFmOSMonCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmOSMonCab.FormCreate(Sender: TObject);
begin
  FConta := 0;
  UMyMod.AbreQuery(QrFormulas, Dmod.MyDB);
  UMyMod.AbreQuery(QrTipoAplica, Dmod.MyDB);
  UnDmkDAC_PF.AbreQuery(QrUnidMed, Dmod.MyDB);
  UnOSUnit.ReabreEquipAplic(QrEquipAplic, gbsMonitora);
end;

procedure TFmOSMonCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmOSMonCab.FormShow(Sender: TObject);
begin
  // Precisa do SQLType
  ReopenPIPs(0);
end;

procedure TFmOSMonCab.ReopenPIPs(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPIPs, Dmod.MyDB, [
  'SELECT * ',
  'FROM pipcad ',
  'WHERE (OSMonCab=0 ',
  'AND MotDesativ=0 ',
  'AND (DtaDesativ < "1900-01-01" ',
  '  OR DtaDesativ IS NULL)) ',
  Geral.ATS_If(ImgTipo.SQLType = stUpd, [
    'OR Codigo=' + Geral.FF0(FPipCad)]),
  'ORDER BY Nome ',
  '']);
end;

procedure TFmOSMonCab.SbEquipAplicClick(Sender: TObject);
begin
  //FmPrincipal.MostraFormGraG1EqMo();
  FmPrincipal.MostraFormGraG1Equi(gbsMonitora);
  UMyMod.SetaCodigoPesquisado(EdEquipAplic, CBEquipAplic, QrEquipAplic,
    VAR_CADASTRO, 'Controle');
end;

procedure TFmOSMonCab.SbFormulasClick(Sender: TObject);
begin
  FmPrincipal.MostraFormFormulas(FQrOSMonRec, FConta);
  UMyMod.SetaCodigoPesquisado(EdFormula, CBFormula, QrFormulas, VAR_CADASTRO);
end;

procedure TFmOSMonCab.SBPIPCadClick(Sender: TObject);
begin
  FmPrincipal.MostraFormPipRapido(stIns);
  UMyMod.SetaCodigoPesquisado(EdPipCad, CBPipCad, QrPIPs, VAR_CADASTRO, 'Codigo');
end;

procedure TFmOSMonCab.SBTipoAplicaClick(Sender: TObject);
begin
  FmPrincipal.MostraFormTipoAplica();
  UMyMod.SetaCodigoPesquisado(EdTipoAplica, CBTipoAplica, QrTipoAplica, VAR_CADASTRO);
end;

procedure TFmOSMonCab.SpeedButton5Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmUnidMed, FmUnidMed, afmoNegarComAviso) then
  begin
    FmUnidMed.ShowModal;
    FmUnidMed.Destroy;
    if VAR_CADASTRO <> 0 then
    begin
      UMyMod.SetaCodUsuDeCodigo(EdUnidMed, CBUnidMed, QrUnidMed, VAR_CADASTRO,
      'Codigo', 'CodUsu');
    end;
  end;
end;

end.
