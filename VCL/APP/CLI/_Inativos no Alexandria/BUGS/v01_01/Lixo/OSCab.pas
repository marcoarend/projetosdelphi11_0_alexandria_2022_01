unit OSCab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  Menus, Grids, DBGrids, UnDmkProcFunc, dmkDBLookupComboBox, dmkEditCB,
  ComCtrls, dmkEditDateTimePicker, dmkValUsu, frxClass, frxDBSet, dmkDBGrid,
  dmkLabelRotate, Bugs_Tabs, dmkCheckGroup, dmkMemo;

type
  THackDBGrid = class(TDBGrid);
  TFmOSCab = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    QrOSCab: TmySQLQuery;
    DsOSCab: TDataSource;
    PMIts: TPopupMenu;
    ItsInclui1: TMenuItem;
    ItsExclui1: TMenuItem;
    ItsAltera1: TMenuItem;
    PMCab: TPopupMenu;
    CabInclui1: TMenuItem;
    CabAltera1: TMenuItem;
    CabExclui1: TMenuItem;
    BtCab: TBitBtn;
    BtIts: TBitBtn;
    QrOSCabCodigo: TIntegerField;
    QrOSCabEntidade: TIntegerField;
    QrOSCabEstatus: TIntegerField;
    QrOSCabFatoGeradr: TIntegerField;
    QrOSCabOSOrigem: TIntegerField;
    QrOSCabDtaVisPrv: TDateTimeField;
    QrOSCabDtaVisExe: TDateTimeField;
    QrOSCabDtaExePrv: TDateTimeField;
    QrOSCabDtaExeIni: TDateTimeField;
    QrOSCabDtaExeFim: TDateTimeField;
    QrOSCabLk: TIntegerField;
    QrOSCabDataCad: TDateField;
    QrOSCabDataAlt: TDateField;
    QrOSCabUserCad: TIntegerField;
    QrOSCabUserAlt: TIntegerField;
    QrOSCabAlterWeb: TSmallintField;
    QrOSCabAtivo: TSmallintField;
    QrOSCabNO_FatoGeradr: TWideStringField;
    QrOSCabNO_ESTATUS: TWideStringField;
    QrOSCabNO_ENT: TWideStringField;
    QrOSCabSiapTerCad: TIntegerField;
    QrOSCabNO_SiapTerCad: TWideStringField;
    Label1: TLabel;
    EdCodigo: TdmkEdit;
    Label2: TLabel;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    Label6: TLabel;
    EdSiapTerCad: TdmkEditCB;
    CBSiapTerCad: TdmkDBLookupComboBox;
    Label14: TLabel;
    EdEstatus: TdmkEditCB;
    CBEstatus: TdmkDBLookupComboBox;
    Label15: TLabel;
    EdFatoGeradr: TdmkEditCB;
    CBFatoGeradr: TdmkDBLookupComboBox;
    EdOSOrigem: TdmkEdit;
    Label16: TLabel;
    GroupBox3: TGroupBox;
    Label17: TLabel;
    Label18: TLabel;
    TPDtaVisPrv: TdmkEditDateTimePicker;
    TPDtaVisExe: TdmkEditDateTimePicker;
    EdDtaVisPrv: TdmkEdit;
    EdDtaVisExe: TdmkEdit;
    GroupBox4: TGroupBox;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    TPDtaExePrv: TdmkEditDateTimePicker;
    TPDtaExeIni: TdmkEditDateTimePicker;
    TPDtaExeFim: TdmkEditDateTimePicker;
    EdDtaExePrv: TdmkEdit;
    EdDtaExeIni: TdmkEdit;
    EdDtaExeFim: TdmkEdit;
    GroupBox5: TGroupBox;
    Label22: TLabel;
    TPDtaContat: TdmkEditDateTimePicker;
    EdDtaContat: TdmkEdit;
    QrOSCabDtaContat: TDateTimeField;
    QrOSCabTXTVisPrv: TWideStringField;
    QrOSCabTXTContat: TWideStringField;
    QrOSCabTXTVisExe: TWideStringField;
    QrOSCabTXTExePrv: TWideStringField;
    QrOSCabTXTExeIni: TWideStringField;
    QrOSCabTXTExeFim: TWideStringField;
    QrOSCabValorTotal: TFloatField;
    QrOSCabDdsPosVda: TIntegerField;
    Panel6: TPanel;
    BtOSFrmCab: TBitBtn;
    PMOSFrmCab: TPopupMenu;
    ItsInclui4: TMenuItem;
    ItsAltera4: TMenuItem;
    ItsExclui4: TMenuItem;
    QrOSCabEntiContat: TIntegerField;
    QrOSCabNumContrat: TIntegerField;
    QrOSCabEntPagante: TIntegerField;
    Label34: TLabel;
    EdEntiContat: TdmkEditCB;
    CBEntiContat: TdmkDBLookupComboBox;
    Label35: TLabel;
    EdNumContrat: TdmkEdit;
    Label36: TLabel;
    EdEntContrat: TdmkEditCB;
    CBEntContrat: TdmkDBLookupComboBox;
    Label38: TLabel;
    EdEntPagante: TdmkEditCB;
    CBEntPagante: TdmkDBLookupComboBox;
    QrOSCabEntContrat: TIntegerField;
    Panel7: TPanel;
    Label7: TLabel;
    DBEdit15: TDBEdit;
    Label9: TLabel;
    DBEdit4: TDBEdit;
    DBEdit3: TDBEdit;
    Label4: TLabel;
    DBEdit6: TDBEdit;
    DBEdit5: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    DBEdit8: TDBEdit;
    DBEdit7: TDBEdit;
    LaOSOrigem: TLabel;
    DBEdit9: TDBEdit;
    GroupBox6: TGroupBox;
    Label23: TLabel;
    DBEdit16: TDBEdit;
    GroupBox1: TGroupBox;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    GroupBox2: TGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    PMOSCabAlv: TPopupMenu;
    ExcluiOSCabAlv1: TMenuItem;
    BtOSAlv: TBitBtn;
    Cabealho4: TMenuItem;
    Itens4: TMenuItem;
    QrOSCabEmpresa: TIntegerField;
    Label24: TLabel;
    DBEdit17: TDBEdit;
    CBEmpresa: TdmkDBLookupComboBox;
    EdEmpresa: TdmkEditCB;
    Label25: TLabel;
    BtOSMonCab: TBitBtn;
    PMOSMonCab: TPopupMenu;
    Cabecalho5: TMenuItem;
    ItsInclui5: TMenuItem;
    ItsAltera5: TMenuItem;
    ItsExclui5: TMenuItem;
    Itens5: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel9: TPanel;
    GroupBox15: TGroupBox;
    QrOSCabValorServi: TFloatField;
    QrOSCabValorDesco: TFloatField;
    IncluiOSCabAlv1: TMenuItem;
    DBEdit20: TDBEdit;
    Label28: TLabel;
    DBGOSPrz: TDBGrid;
    Label29: TLabel;
    DBEdit21: TDBEdit;
    DBEdit24: TDBEdit;
    Label33: TLabel;
    DBEdit23: TDBEdit;
    Label30: TLabel;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    QrOSCabNO_PAG: TWideStringField;
    QrOSCabNO_CTR: TWideStringField;
    GroupBox16: TGroupBox;
    DBGLctFatRef: TDBGrid;
    QrOSCabDtaLibFat: TDateTimeField;
    QrOSCabDtaFimFat: TDateTimeField;
    QrOSCabValorOutrs: TFloatField;
    QrOSCabCondicaoPg: TIntegerField;
    QrOSCabCartEmis: TIntegerField;
    QrOSCabNO_CART: TWideStringField;
    QrOSCabNO_PRZ: TWideStringField;
    QrOSCabSerNF: TWideStringField;
    QrOSCabNumNF: TIntegerField;
    PMFat: TPopupMenu;
    Gerabloqueto1: TMenuItem;
    N4: TMenuItem;
    Excluifaturamento1: TMenuItem;
    BtFat: TBitBtn;
    Fatura1: TMenuItem;
    Abrangncia1: TMenuItem;
    Label31: TLabel;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    TabSheet3: TTabSheet;
    QrOSCabNO_ENTICONTAT: TWideStringField;
    QrOSCabTel_ENT: TWideStringField;
    DBEdit29: TDBEdit;
    QrOSCabTXTTel_ENT: TWideStringField;
    GroupBox18: TGroupBox;
    DBGEntiTel: TDBGrid;
    Splitter3: TSplitter;
    DBGEntiMail: TDBGrid;
    GroupBox19: TGroupBox;
    DBGOSPos: TDBGrid;
    PMOSPos: TPopupMenu;
    IncluiC3: TMenuItem;
    AlteraC3: TMenuItem;
    ExcluiC3: TMenuItem;
    GroupBox21: TGroupBox;
    Label26: TLabel;
    DBEdit18: TDBEdit;
    Label27: TLabel;
    DBEdit19: TDBEdit;
    Label37: TLabel;
    DBEdit22: TDBEdit;
    Label32: TLabel;
    Label39: TLabel;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    Label40: TLabel;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    QrOSCabInvalServi: TFloatField;
    QrOSCabInvalDesco: TFloatField;
    QrOSCabInvalOutrs: TFloatField;
    QrOSCabInvalTotal: TFloatField;
    QrOSCabOrcamServi: TFloatField;
    QrOSCabOrcamDesco: TFloatField;
    QrOSCabOrcamOutrs: TFloatField;
    QrOSCabOrcamTotal: TFloatField;
    Label41: TLabel;
    EdValiDdOrca: TdmkEdit;
    Label42: TLabel;
    QrOSCabValiDdOrca: TIntegerField;
    QrOSCabOperacao: TIntegerField;
    PMOSCxa: TPopupMenu;
    ItsInclui2: TMenuItem;
    ItsAltera2: TMenuItem;
    ItsExclui2: TMenuItem;
    Label47: TLabel;
    EdExeTxtCli1: TdmkEditCB;
    CBExeTxtCli1: TdmkDBLookupComboBox;
    QrOSCabExeTxtCli2: TIntegerField;
    QrOSCabNO_ExeTxtCli2: TWideStringField;
    Label48: TLabel;
    DBEdit36: TDBEdit;
    PMOSPrz: TPopupMenu;
    OSPrz1_Inclui: TMenuItem;
    OSPrz1_Altera: TMenuItem;
    OSPrz1_Exclui: TMenuItem;
    QrOSCabValorPre: TFloatField;
    PCOperacoes: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    PnCaixaDAgua: TPanel;
    PageControl3: TPageControl;
    GroupBox22: TGroupBox;
    Panel27: TPanel;
    DBGOSCxaAtrib: TdmkDBGrid;
    PnDedetizacao: TPanel;
    GroupBox7: TGroupBox;
    DBGOSSrv: TDBGrid;
    GroupBox9: TGroupBox;
    DBGOSAlv: TDBGrid;
    Panel8: TPanel;
    Splitter1: TSplitter;
    GroupBox11: TGroupBox;
    GroupBox12: TGroupBox;
    DBGOSFrmRec: TDBGrid;
    Panel10: TPanel;
    Splitter2: TSplitter;
    DBGOSFrmCab: TDBGrid;
    GroupBox17: TGroupBox;
    DBGOSFrmAbr: TDBGrid;
    GroupBox8: TGroupBox;
    DBGOSFrmDep: TDBGrid;
    GroupBox13: TGroupBox;
    DBGOSMonCab: TDBGrid;
    GroupBox14: TGroupBox;
    DBGOSMonRec: TDBGrid;
    GroupBox20: TGroupBox;
    DBGOSMonDep: TDBGrid;
    GBOperacao: TGroupBox;
    LaOperacaoA: TLabel;
    LaOperacaoB: TLabel;
    LaOperacaoC: TLabel;
    QrOSCabNO_OPERACAO: TWideStringField;
    CGOperacao: TdmkCheckGroup;
    QrOSCabObsGaranti: TWideMemoField;
    MeObsGaranti: TdmkMemo;
    TabSheet6: TTabSheet;
    PnMonitoramento: TPanel;
    DBGOSPipMon: TDBGrid;
    PMOSPipMon: TPopupMenu;
    AdicionaPIPsativos1: TMenuItem;
    Panel11: TPanel;
    GroupBox23: TGroupBox;
    Panel28: TPanel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    dmkLabelRotate1: TdmkLabelRotate;
    DBEdNOME_FORMA: TDBEdit;
    DBEdNO_MATERIAL: TDBEdit;
    DBEdVolumeL: TDBEdit;
    DBEdMedidas: TDBEdit;
    DBMeAcesso: TDBMemo;
    GroupBox24: TGroupBox;
    DBGOSCxI: TDBGrid;
    DBMeOSCxI: TDBMemo;
    PMOSCxI: TPopupMenu;
    ItsInclui6: TMenuItem;
    ItsAltera6: TMenuItem;
    ItsExclui6: TMenuItem;
    Panel12: TPanel;
    GroupBox10: TGroupBox;
    DBGOSAge: TDBGrid;
    Splitter4: TSplitter;
    GroupBox25: TGroupBox;
    DBGOSCabAlv: TDBGrid;
    PMOSAge: TPopupMenu;
    IncluiOSAge1: TMenuItem;
    RemoveOSAge1: TMenuItem;
    Splitter5: TSplitter;
    Panel13: TPanel;
    DBGOSCxa: TDBGrid;
    DBMeOSCxa: TDBMemo;
    Splitter6: TSplitter;
    Label50: TLabel;
    EdExeTxtCli2: TdmkEditCB;
    CBExeTxtCli2: TdmkDBLookupComboBox;
    QrOSCabNO_ExeTxtCli1: TWideStringField;
    QrOSCabExeTxtCli1: TIntegerField;
    TPFimVisPrv: TdmkEditDateTimePicker;
    EdFimVisPrv: TdmkEdit;
    TPFimVisExe: TdmkEditDateTimePicker;
    EdFimVisExe: TdmkEdit;
    TPFimExePrv: TdmkEditDateTimePicker;
    EdFimExePrv: TdmkEdit;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    QrOSCabFimVisPrv: TDateTimeField;
    QrOSCabFimVisExe: TDateTimeField;
    QrOSCabFimExePrv: TDateTimeField;
    TabSheet7: TTabSheet;
    PMOSChk: TPopupMenu;
    Incluiitens1: TMenuItem;
    DBGOSChk: TDBGrid;
    Removeitens1: TMenuItem;
    RemovePIPs1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
    procedure CabInclui1Click(Sender: TObject);
    procedure CabAltera1Click(Sender: TObject);
    procedure CabExclui1Click(Sender: TObject);
    procedure BtCabClick(Sender: TObject);
    procedure BtItsClick(Sender: TObject);
    procedure ItsInclui1Click(Sender: TObject);
    procedure ItsExclui1Click(Sender: TObject);
    procedure ItsAltera1Click(Sender: TObject);
    procedure PMCabPopup(Sender: TObject);
    procedure PMItsPopup(Sender: TObject);
    procedure EdEntidadeChange(Sender: TObject);
    procedure EdFatoGeradrChange(Sender: TObject);
    procedure PMOSFrmCabPopup(Sender: TObject);
    procedure ItsInclui4Click(Sender: TObject);
    procedure ItsAltera4Click(Sender: TObject);
    procedure BtOSFrmCabClick(Sender: TObject);
    procedure QrOSCabAfterScroll(DataSet: TDataSet);
    procedure QrOSCabBeforeOpen(DataSet: TDataSet);
    procedure QrOSCabCalcFields(DataSet: TDataSet);
    procedure PMOSCabAlvPopup(Sender: TObject);
    procedure ExcluiOSCabAlv1Click(Sender: TObject);
    procedure QrOSCabBeforeClose(DataSet: TDataSet);
    procedure BtOSAlvClick(Sender: TObject);
    procedure Itens4Click(Sender: TObject);
    procedure BtOSMonCabClick(Sender: TObject);
    procedure PMOSMonCabPopup(Sender: TObject);
    procedure ItsInclui5Click(Sender: TObject);
    procedure ItsAltera5Click(Sender: TObject);
    procedure Itens5Click(Sender: TObject);
    procedure ItsExclui4Click(Sender: TObject);
    procedure ItsExclui5Click(Sender: TObject);
    procedure DBGOSSrvDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGOSSrvMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSFrmDepMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSAlvMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSFrmCabMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSFrmRecMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSMonCabMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSMonRecMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IncluiOSCabAlv1Click(Sender: TObject);
    procedure DBGOSPrzMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSPrzDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    function frxGER_OSERV_001_001UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure SbImprimeClick(Sender: TObject);
    procedure Fatura1Click(Sender: TObject);
    procedure BtFatClick(Sender: TObject);
    procedure Excluifaturamento1Click(Sender: TObject);
    procedure Gerabloqueto1Click(Sender: TObject);
    procedure PMFatPopup(Sender: TObject);
    procedure EdNumContratKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGOSFrmAbrMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Abrangncia1Click(Sender: TObject);
    procedure DBGOSPosMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure IncluiC3Click(Sender: TObject);
    procedure AlteraC3Click(Sender: TObject);
    procedure ExcluiC3Click(Sender: TObject);
    procedure DBGOSMonDepMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ItsInclui2Click(Sender: TObject);
    procedure ItsAltera2Click(Sender: TObject);
    procedure PMOSCxaPopup(Sender: TObject);
    procedure DBGOSCxaMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ItsExclui2Click(Sender: TObject);
    procedure OSPrz1_IncluiClick(Sender: TObject);
    procedure OSPrz1_AlteraClick(Sender: TObject);
    procedure OSPrz1_ExcluiClick(Sender: TObject);
    procedure PMOSPrzPopup(Sender: TObject);
    procedure QrOSCabAfterClose(DataSet: TDataSet);
    procedure CGOperacaoClick(Sender: TObject);
    procedure DBGOSPipMonMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure AdicionaPIPsativos1Click(Sender: TObject);
    procedure ItsInclui6Click(Sender: TObject);
    procedure ItsAltera6Click(Sender: TObject);
    procedure ItsExclui6Click(Sender: TObject);
    procedure DBGOSCxIMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PMOSCxIPopup(Sender: TObject);
    procedure DBGOSCabAlvMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure DBGOSAgeMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure PMOSAgePopup(Sender: TObject);
    procedure IncluiOSAge1Click(Sender: TObject);
    procedure RemoveOSAge1Click(Sender: TObject);
    procedure DBGOSAgeDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGOSAgeColEnter(Sender: TObject);
    procedure DBGOSAgeColExit(Sender: TObject);
    procedure DBGOSSrvColEnter(Sender: TObject);
    procedure DBGOSSrvColExit(Sender: TObject);
    procedure EdEntidadeEnter(Sender: TObject);
    procedure DBGOSCxaDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TPDtaVisPrvExit(Sender: TObject);
    procedure EdDtaVisPrvExit(Sender: TObject);
    procedure TPDtaVisExeExit(Sender: TObject);
    procedure EdDtaVisExeExit(Sender: TObject);
    procedure TPDtaExePrvExit(Sender: TObject);
    procedure EdDtaExePrvExit(Sender: TObject);
    procedure TPDtaExeIniExit(Sender: TObject);
    procedure EdDtaExeIniExit(Sender: TObject);
    procedure DBGOSChkMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Incluiitens1Click(Sender: TObject);
    procedure Removeitens1Click(Sender: TObject);
    procedure RemovePIPs1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);

    procedure MostraOSCxa(SQLType: TSQLType);
    procedure MostraOSCxI(SQLType: TSQLType);
    procedure MostraOSSrv(SQLType: TSQLType);
    procedure MostraOSAlv(SQLType: TSQLType);
    procedure MostraOSFrmCab(SQLType: TSQLType);
    procedure MostraOSFrmRec();
    procedure MostraOSFrmAbr(SQLType: TSQLType);
    procedure MostraOSFrmDep(SQLType: TSQLType);
    procedure MostraOSMonCab(SQLType: TSQLType);
    procedure MostraOSMonRec();
    procedure MostraOSMonDep(SQLType: TSQLType);
    procedure MostraOSPos(SQLType: TSQLType);
    procedure MostraOSPsq();
    //
    procedure MostraFormOSCabFat();
    procedure TravaOForm();
    //
    function CondicaoPGSelecionado(): Integer;
  public
    { Public declarations }
    FSeq, FCabIni: Integer;
    FLocIni: Boolean;
    //
  end;

var
  FmOSCab: TFmOSCab;

implementation

uses UnMyObjects, Module, ModOS, MyDBCheck, DmkDAC_PF, ModuleGeral, CfgCadLista,
  OSSrv, OSAlv, OSFrmCab, OSFrmRec, OSMonCab, OSMonRec, OSPsq, MyVCLSkin, OsPrz,
  OSImp, OSCabFat, ModuleFatura, ModuleFin, ModuleBloGeren, ContratPsq, OSCxI,
  OSFrmAbr, OSPos, OSFrmDep, OSMonDep, OSCxa, Bugstrol_Dmk, AppListas, OSUnit,
  ModAgenda, OSChk;

{$R *.DFM}

const
  FFormatFloat = '00000';
  //

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOSCab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOSCab.MostraFormOSCabFat();
var
  CondicaoPG: Integer;
  SQLType: TSQLType;
begin
  //fazer abertura e encerramento de faturamento!
  //ver outros campos necess�rios para faturamento como carteira etc.

{
  if (DmModOS.QrLctFatRef.State <> dsInactive) and
  (DmModOS.QrLctFatRef.RecordCount > 0) then
    SQLType := stUpd
  else
    SQLType := stIns;
  //
}
  SQLType := stUpd;
  //
  if DBCheck.CriaFm(TFmOSCabFat, FmOSCabFat, afmoNegarComAviso) then
  begin
    FmOSCabFat.ImgTipo.SQLType := SQLType;
    FmOSCabFat.FQrCab := QrOSCab;
    FmOSCabFat.FDsCab := DsOSCab;
    FmOSCabFat.FQrIts := DmModOS.QrLctFatRef;

    FmOSCabFat.DBEdCodigo.DataSource := DsOSCab;
    FmOSCabFat.DBEdEmpresa.DataSource := DsOSCab;
    FmOSCabFat.DBEdNO_EMP.DataSource := DsOSCab;
    FmOSCabFat.DBEdPagante.DataSource := DsOSCab;
    FmOSCabFat.DBEdNO_PAG.DataSource := DsOSCab;

    FmOSCabFat.FEmpresa := QrOSCabEmpresa.Value;
    FmOSCabFat.FCliente := QrOSCabEntPagante.Value;
    //FmOSCabFat.FDataAbriu := QrLocCConDtHrEmi.Value;

    // N�o h� faturamento parcial: Controle e c�digo s�o o mesmo n�mero
    FmOSCabFat.FEncerra := True;
    FmOSCabFat.EdControle.ValueVariant := QrOSCabCodigo.Value;
    // FIM N�o h� faturamento parcial!


    FmOSCabFat.EdValorServi.ValueVariant := QrOSCabValorServi.Value;
    FmOSCabFat.EdValorOutrs.ValueVariant := QrOSCabValorOutrs.Value;
    FmOSCabFat.EdValorDesco.ValueVariant := QrOSCabValorDesco.Value;


    if SQLType = stIns then
    begin
      FmOSCabFat.TPDataFat.Date := Trunc(Date);
      FmOSCabFat.EdHoraFat.Text := FormatDateTime('hh:nn:ss', Now());
      //
      CondicaoPG := CondicaoPGSelecionado();
      UMyMod.SetaCodUsuDeCodigo(FmOSCabFat.EdCondicaoPG,
        FmOSCabFat.CBCondicaoPG, DmFatura.QrPediPrzCab, CondicaoPG);
    end else
    begin
      //
      UMyMod.SetaCodUsuDeCodigo(FmOSCabFat.EdCondicaoPG,
        FmOSCabFat.CBCondicaoPG, DmFatura.QrPediPrzCab, QrOSCabCondicaoPG.Value);
      FmOSCabFat.EdCartEmis.ValueVariant := QrOSCabCartEmis.Value;
      FmOSCabFat.CBCartEmis.KeyValue := QrOSCabCartEmis.Value;

      //FmOSCabFat.EdValorTotal.ValueVariant := QrOSCabValorTotal.Value;

      FmOSCabFat.EdSerNF.ValueVariant := QrOSCabSerNF.Value;
      FmOSCabFat.EdNumNF.ValueVariant := QrOSCabNumNF.Value;

      FmOSCabFat.TPDataFat.Date := Trunc(QrOSCabDtaFimFat.Value);
      FmOSCabFat.EdHoraFat.Text := FormatDateTime('hh:nn:ss', QrOSCabDtaFimFat.Value);
    end;
    FmOSCabFat.ShowModal;
    FmOSCabFat.Destroy;
  end;
end;

procedure TFmOSCab.MostraOSAlv(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSAlv, FmOSAlv, afmoNegarComAviso) then
  begin
    FmOSAlv.ImgTipo.SQLType := SQLType;
    FmOSAlv.FQrOSAlv := DmModOS.QrOSAlv;
    FmOSAlv.FCodigo := QrOSCabCodigo.Value;
    FmOSAlv.FControle := DmModOS.QrOSSrvControle.Value;
    //
    FmOSAlv.QrOSSrv.Close;
    FmOSAlv.QrOSSrv.SQL.Text := DmModOS.QrOSSrv.SQL.Text;
    FmOSAlv.QrOSSrv.Params := DmModOS.QrOSSrv.Params;
    UMyMod.AbreQuery(FmOSAlv.QrOSSrv, Dmod.MyDB);
    //
    { // N�o tem!
    if SQLType = stUpd then
    begin
      // N�o tem!
    end;
    }
    FmOSAlv.ShowModal;
    FmOSAlv.Destroy;
  end;
end;

procedure TFmOSCab.MostraOSCxa(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSCxa, FmOSCxa, afmoNegarComAviso) then
  begin
    FmOSCxa.ImgTipo.SQLType := SQLType;
    FmOSCxa.FQrInsUpd := DmModOS.QrOSCxa;
    //
    FmOSCxa.QrOSCab.Close;
    FmOSCxa.QrOSCab.SQL.Text := QrOSCab.SQL.Text;
    FmOSCxa.QrOSCab.Params := QrOSCab.Params;
    UMyMod.AbreQuery(FmOSCxa.QrOSCab, Dmod.MyDB);
    //
    if SQLType = stUpd then
    begin
      FmOSCxa.EdControle.ValueVariant := DmModOS.QrOSCxaControle.Value;
      FmOSCxa.EdCaixa.ValueVariant := DmModOS.QrOSCxaCaixa.Value;
      FmOSCxa.CBCaixa.KeyValue := DmModOS.QrOSCxaCaixa.Value;
      FmOSCxa.EdGarantiaDd.ValueVariant := DmModOS.QrOSCxaGarantiaDd.Value;
      FmOSCxa.EdHrEvacuar.ValueVariant := DmModOS.QrOSCxaHrEvacuar.Value;
      FmOSCxa.EdHrExecutar.ValueVariant := DmModOS.QrOSCxaHrExecutar.Value;
      //
      FmOSCxa.EdValCalc.ValueVariant := DmModOS.QrOSCxaValCalc.Value;
      FmOSCxa.EdValInfo.ValueVariant := DmModOS.QrOSCxaValInfo.Value;
      FmOSCxa.EdValDesc.ValueVariant := DmModOS.QrOSCxaValDesc.Value;
      FmOSCxa.EdValTota.ValueVariant := DmModOS.QrOSCxaValTota.Value;
      FmOSCxa.EdChekLstCab.ValueVariant := DmModOS.QrOSCxaChekLstCab.Value;
      //
      FmOSCxa.RGAutorizado.ItemIndex := DmModOS.QrOSCxaAutorizado.Value;
      FmOSCxa.MeDetalhes.Text        := DmModOS.QrOSCxaDetalhes.Value;
      //
    end;
    FmOSCxa.FCriou := True;
    //FmOSCxa.CalculaTotal();
    FmOSCxa.ShowModal;
    FmOSCxa.Destroy;
    //
    LocCod(QrOSCabCodigo.Value, QrOSCabCodigo.Value);
  end;
end;

procedure TFmOSCab.MostraOSCxI(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSCxI, FmOSCxI, afmoNegarComAviso) then
  begin
    FmOSCxI.ImgTipo.SQLType := SQLType;
    FmOSCxI.FQrInsUpd := DmModOS.QrOSCxI;
    //
    FmOSCxI.QrOSCab.Close;
    FmOSCxI.QrOSCab.SQL.Text := QrOSCab.SQL.Text;
    FmOSCxI.QrOSCab.Params := QrOSCab.Params;
    UMyMod.AbreQuery(FmOSCxI.QrOSCab, Dmod.MyDB);
    //
    if SQLType = stUpd then
    begin
      FmOSCxI.EdControle.ValueVariant := DmModOS.QrOSCxIControle.Value;
      FmOSCxI.EdFotoCxa.ValueVariant  := DmModOS.QrOSCxIFotoCxa.Value;
      FmOSCxI.MeDetalhes.Text         := DmModOS.QrOSCxIDetalhes.Value;
      FmOSCxI.CGAplicacao.Value       := DmModOS.QrOSCxIAplicacao.Value;
      //
    end else
    begin
      FmOSCxI.CGAplicacao.Value := 0;
    end;
    FmOSCxI.FCriou := True;
    //FmOSCxI.CalculaTotal();
    FmOSCxI.ShowModal;
    FmOSCxI.Destroy;
    //
    LocCod(QrOSCabCodigo.Value, QrOSCabCodigo.Value);
  end;
end;

procedure TFmOSCab.MostraOSFrmDep(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSFrmDep, FmOSFrmDep, afmoNegarComAviso) then
  begin
    FmOSFrmDep.ImgTipo.SQLType := SQLType;
    FmOSFrmDep.FQrOSFrmDep := DmModOS.QrOSFrmDep;
    FmOSFrmDep.FCodigo := QrOSCabCodigo.Value;
    FmOSFrmDep.FControle := DmModOS.QrOSSrvControle.Value;
    FmOSFrmDep.FConta := DmModOS.QrOSFrmCabConta.Value;
    FmOSFrmDep.FSiapTerCad := QrOSCabSiapTerCad.Value;
    FmOSFrmDep.FEntidade := QrOSCabEntidade.Value;
    //
    FmOSFrmDep.QrOSSrv.Close;
    FmOSFrmDep.QrOSSrv.SQL.Text := DmModOS.QrOSSrv.SQL.Text;
    FmOSFrmDep.QrOSSrv.Params := DmModOS.QrOSSrv.Params;
    UMyMod.AbreQuery(FmOSFrmDep.QrOSSrv, Dmod.MyDB);
    //
    //
    FmOSFrmDep.QrOSFrmCab.Close;
    FmOSFrmDep.QrOSFrmCab.SQL.Text := DmModOS.QrOSFrmCab.SQL.Text;
    FmOSFrmDep.QrOSFrmCab.Params := DmModOS.QrOSFrmCab.Params;
    UMyMod.AbreQuery(FmOSFrmDep.QrOSFrmCab, Dmod.MyDB);
    FmOSFrmDep.QrOSFrmCab.Locate('Conta', DmModOS.QrOSFrmCabConta.Value, []);
    //
    if SQLType = stUpd then
    begin
      // N�o tem!
    end;
    FmOSFrmDep.ShowModal;
    FmOSFrmDep.Destroy;
  end;
end;

procedure TFmOSCab.MostraOSFrmCab(SQLType: TSQLType);
var
  Conta: Integer;
begin
  //Conta := 0;
  if DBCheck.CriaFm(TFmOSFrmCab, FmOSFrmCab, afmoNegarComAviso) then
  begin
    FmOSFrmCab.ImgTipo.SQLType := SQLType;
    FmOSFrmCab.FQrOSFrmCab := DmModOS.QrOSFrmCab;
    FmOSFrmCab.FCodigo     := QrOSCabCodigo.Value;
    FmOSFrmCab.FControle   := DmModOS.QrOSSrvControle.Value;
    FmOSFrmCab.FConta      := DmModOS.QrOSFrmCabConta.Value;
    //
    FmOSFrmCab.QrOSSrv.Close;
    FmOSFrmCab.QrOSSrv.SQL.Text := DmModOS.QrOSSrv.SQL.Text;
    FmOSFrmCab.QrOSSrv.Params := DmModOS.QrOSSrv.Params;
    UMyMod.AbreQuery(FmOSFrmCab.QrOSSrv, Dmod.MyDB);
    //
    if SQLType = stUpd then
    begin
      FmOSFrmCab.EdConta.ValueVariant := DmModOS.QrOSFrmCabConta.Value;
      FmOSFrmCab.EdNome.Text := DmModOS.QrOSFrmCabNome.Value;
      //
      FmOSFrmCab.EdFormula.ValueVariant := DmModOS.QrOSFrmCabFormula.Value;
      FmOSFrmCab.CBFormula.KeyValue := DmModOS.QrOSFrmCabFormula.Value;
      //
      FmOSFrmCab.EdEquipAplic.ValueVariant := DmModOS.QrOSFrmCabEquipAplic.Value;
      FmOSFrmCab.CBEquipAplic.KeyValue := DmModOS.QrOSFrmCabEquipAplic.Value;
      //
      FmOSFrmCab.EdTipoAplica.ValueVariant := DmModOS.QrOSFrmCabTipoAplica.Value;
      FmOSFrmCab.CBTipoAplica.KeyValue := DmModOS.QrOSFrmCabTipoAplica.Value;
      //
      FmOSFrmCab.EdQtdTot.ValueVariant := DmModOS.QrOSFrmCabQtdTot.Value;
      FmOSFrmCab.RGDiluente.ItemIndex := DmModOS.QrOSFrmCabDiluente.Value;
      //FmOSFrmCab.EdQtdQSP.ValueVariant := DmModOS.QrOSFrmCabQtdQSP.Value;
      //
      FmOSFrmCab.EdSigla.Text              := DmModOS.QrOSFrmCabSIGLA.Value;
      FmOSFrmCab.EdUnidMed.ValueVariant    := DmModOS.QrOSFrmCabCU_UnidMed.Value;
      FmOSFrmCab.CBUnidMed.KeyValue        := DmModOS.QrOSFrmCabCU_UnidMed.Value;
    end;
    FmOSFrmCab.ShowModal;
    Conta := FmOSFrmCab.FConta;
    FmOSFrmCab.Destroy;
    //
    if (Conta <> 0) and (DmModOS.QrOSFrmCab.State <> dsInactive)
    and (DmModOS.QrOSFrmCabConta.Value = Conta) then
     Itens4Click(Self);
  end;
end;

procedure TFmOSCab.MostraOSFrmRec();
begin
  if DBCheck.CriaFm(TFmOSFrmRec, FmOSFrmRec, afmoNegarComAviso) then
  begin
    //FmOSFrmRec.ImgTipo.SQLType := SQLType;
    FmOSFrmRec.FQrOSFrmRec := DmModOS.QrOSFrmRec;
    FmOSFrmRec.FQrOSFrmCab := DmModOS.QrOSFrmCab;
    //FmOSFrmRec.FQrOSSrv := DmModOS.QrOSSrv;
    FmOSFrmRec.FCodigo := QrOSCabCodigo.Value;
    FmOSFrmRec.FControle := DmModOS.QrOSSrvControle.Value;
    FmOSFrmRec.FConta := DmModOS.QrOSFrmCabConta.Value;
    //
    FmOSFrmRec.QrOSSrv.Close;
    FmOSFrmRec.QrOSSrv.SQL.Text := DmModOS.QrOSSrv.SQL.Text;
    FmOSFrmRec.QrOSSrv.Params := DmModOS.QrOSSrv.Params;
    UMyMod.AbreQuery(FmOSFrmRec.QrOSSrv, Dmod.MyDB);
    //
    FmOSFrmRec.QrOSFrmCab.Close;
    FmOSFrmRec.QrOSFrmCab.SQL.Text := DmModOS.QrOSFrmCab.SQL.Text;
    FmOSFrmRec.QrOSFrmCab.Params := DmModOS.QrOSFrmCab.Params;
    UMyMod.AbreQuery(FmOSFrmRec.QrOSFrmCab, Dmod.MyDB);
    FmOSFrmRec.QrOSFrmCab.Locate('Conta', DmModOS.QrOSFrmCabConta.Value, []);
    //

    FmOSFrmRec.ShowModal;
    FmOSFrmRec.Destroy;
  end;
end;

procedure TFmOSCab.MostraOSFrmAbr(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSFrmAbr, FmOSFrmAbr, afmoNegarComAviso) then
  begin
    FmOSFrmAbr.ImgTipo.SQLType := SQLType;
    FmOSFrmAbr.FQrOSFrmAbr := DmModOS.QrOSFrmAbr;
    FmOSFrmAbr.FCodigo := DmModOS.QrOSFrmCabCodigo.Value;
    FmOSFrmAbr.FControle := DmModOS.QrOSFrmCabControle.Value;
    FmOSFrmAbr.FConta := DmModOS.QrOSFrmCabConta.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(FmOSFrmAbr.QrOSFrmCab, Dmod.MyDB, [
    'SELECT Codigo, Controle, Conta, Nome ',
    'FROM osfrmcab ',
    'WHERE Conta=' + Geral.FF0(DmModOS.QrOSFrmCabCodigo.Value),
    '']);
    //
    { // N�o tem!
    if SQLType = stUpd then
    begin
      // N�o tem!
    end;
    }
    FmOSFrmAbr.ShowModal;
    FmOSFrmAbr.Destroy;
  end;
end;

procedure TFmOSCab.MostraOSMonCab(SQLType: TSQLType);
var
  Conta: Integer;
begin
  //Conta := 0;
  if DBCheck.CriaFm(TFmOSMonCab, FmOSMonCab, afmoNegarComAviso) then
  begin
    FmOSMonCab.ImgTipo.SQLType := SQLType;
    FmOSMonCab.FQrOSMonCab := DmModOS.QrOSMonCab;
    FmOSMonCab.FQrOSMonRec := DmModOS.QrOSMonRec;
    FmOSMonCab.FCodigo     := QrOSCabCodigo.Value;
    FmOSMonCab.FControle   := DmModOS.QrOSSrvControle.Value;
    FmOSMonCab.FPipCad     := DmModOS.QrOSMonCabPipCad.Value;
    //
    FmOSMonCab.QrOSSrv.Close;
    FmOSMonCab.QrOSSrv.SQL.Text := DmModOS.QrOSSrv.SQL.Text;
    FmOSMonCab.QrOSSrv.Params := DmModOS.QrOSSrv.Params;
    UMyMod.AbreQuery(FmOSMonCab.QrOSSrv, Dmod.MyDB);
    //
    if SQLType = stUpd then
    begin
      FmOSMonCab.EdConta.ValueVariant := DmModOS.QrOSMonCabConta.Value;
      FmOSMonCab.EdNome.Text := DmModOS.QrOSMonCabNome.Value;
      //
      FmOSMonCab.EdFormula.ValueVariant := DmModOS.QrOSMonCabFormula.Value;
      FmOSMonCab.CBFormula.KeyValue := DmModOS.QrOSMonCabFormula.Value;
      //
      FmOSMonCab.EdEquipAplic.ValueVariant := DmModOS.QrOSMonCabEquipAplic.Value;
      FmOSMonCab.CBEquipAplic.KeyValue := DmModOS.QrOSMonCabEquipAplic.Value;
      //
      FmOSMonCab.EdTipoAplica.ValueVariant := DmModOS.QrOSMonCabTipoAplica.Value;
      FmOSMonCab.CBTipoAplica.KeyValue := DmModOS.QrOSMonCabTipoAplica.Value;
      //
      FmOSMonCab.EdQtdTot.ValueVariant := DmModOS.QrOSMonCabQtdTot.Value;
      FmOSMonCab.EdQtdQSP.ValueVariant := DmModOS.QrOSMonCabQtdQSP.Value;
      //
      FmOSMonCab.EdPipCad.ValueVariant := DmModOS.QrOSMonCabPipCad.Value;
      FmOSMonCab.CBPipCad.KeyValue     := DmModOS.QrOSMonCabPipCad.Value;
      //
      FmOSMonCab.RGDiluente.ItemIndex := DmModOS.QrOSMonCabDiluente.Value;
      //
      //
      FmOSMonCab.EdSigla.Text              := DmModOS.QrOSMonCabSIGLA.Value;
      FmOSMonCab.EdUnidMed.ValueVariant    := DmModOS.QrOSMonCabCU_UnidMed.Value;
      FmOSMonCab.CBUnidMed.KeyValue        := DmModOS.QrOSMonCabCU_UnidMed.Value;
    end;
    FmOSMonCab.ShowModal;
    Conta := FmOSMonCab.FConta;
    FmOSMonCab.Destroy;
    //
    if (Conta <> 0) and (DmModOS.QrOSMonCab.State <> dsInactive)
    and (DmModOS.QrOSMonCabConta.Value = Conta) then
     Itens5Click(Self);
  end;
end;

procedure TFmOSCab.MostraOSMonDep(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOSMonDep, FmOSMonDep, afmoNegarComAviso) then
  begin
    FmOSMonDep.ImgTipo.SQLType := SQLType;
    FmOSMonDep.FQrOSMonDep := DmModOS.QrOSMonDep;
    FmOSMonDep.FCodigo := QrOSCabCodigo.Value;
    FmOSMonDep.FControle := DmModOS.QrOSSrvControle.Value;
    FmOSMonDep.FConta := DmModOS.QrOSMonCabConta.Value;
    FmOSMonDep.FSiapTerCad := QrOSCabSiapTerCad.Value;
    FmOSMonDep.FEntidade := QrOSCabEntidade.Value;

    //
    FmOSMonDep.QrOSSrv.Close;
    FmOSMonDep.QrOSSrv.SQL.Text := DmModOS.QrOSSrv.SQL.Text;
    FmOSMonDep.QrOSSrv.Params := DmModOS.QrOSSrv.Params;
    UMyMod.AbreQuery(FmOSMonDep.QrOSSrv, Dmod.MyDB);
    //
    //
    FmOSMonDep.QrOSMonCab.Close;
    FmOSMonDep.QrOSMonCab.SQL.Text := DmModOS.QrOSMonCab.SQL.Text;
    FmOSMonDep.QrOSMonCab.Params := DmModOS.QrOSMonCab.Params;
    UMyMod.AbreQuery(FmOSMonDep.QrOSMonCab, Dmod.MyDB);
    FmOSMonDep.QrOSMonCab.Locate('Conta', DmModOS.QrOSMonCabConta.Value, []);
    //
    if SQLType = stUpd then
    begin
      // N�o tem!
    end;
    FmOSMonDep.ShowModal;
    FmOSMonDep.Destroy;
  end;
end;

procedure TFmOSCab.MostraOSMonRec();
begin
  if DBCheck.CriaFm(TFmOSMonRec, FmOSMonRec, afmoNegarComAviso) then
  begin
    //FmOSMonRec.ImgTipo.SQLType := SQLType;
    FmOSMonRec.FQrOSMonCab := DmModOS.QrOSMonCab;
    FmOSMonRec.FQrOSMonRec := DmModOS.QrOSMonRec;
    FmOSMonRec.FCodigo := QrOSCabCodigo.Value;
    FmOSMonRec.FControle := DmModOS.QrOSSrvControle.Value;
    FmOSMonRec.FConta := DmModOS.QrOSMonCabConta.Value;
    FmOSMonRec.FDiluente := DmModOS.QrOSMonCabDiluente.Value;
    //
    FmOSMonRec.QrOSSrv.Close;
    FmOSMonRec.QrOSSrv.SQL.Text := DmModOS.QrOSSrv.SQL.Text;
    FmOSMonRec.QrOSSrv.Params := DmModOS.QrOSSrv.Params;
    UMyMod.AbreQuery(FmOSMonRec.QrOSSrv, Dmod.MyDB);
    //
    FmOSMonRec.QrOSMonCab.Close;
    FmOSMonRec.QrOSMonCab.SQL.Text := DmModOS.QrOSMonCab.SQL.Text;
    FmOSMonRec.QrOSMonCab.Params := DmModOS.QrOSMonCab.Params;
    UMyMod.AbreQuery(FmOSMonRec.QrOSMonCab, Dmod.MyDB);
    FmOSMonRec.QrOSMonCab.Locate('Conta', DmModOS.QrOSMonCabConta.Value, []);
    //
    FmOSMonRec.ShowModal;
    FmOSMonRec.Destroy;
  end;
end;

procedure TFmOSCab.MostraOSPos(SQLType: TSQLType);
{
var
  Conta: Integer;
}
begin
  //Conta := 0;
  if DBCheck.CriaFm(TFmOSPos, FmOSPos, afmoNegarComAviso) then
  begin
    FmOSPos.ImgTipo.SQLType := SQLType;
    FmOSPos.FQrOSPos := DmModOS.QrOSPos;
    //
    FmOSPos.QrOSSrv.Close;
    FmOSPos.QrOSSrv.SQL.Text := DmModOS.QrOSSrv.SQL.Text;
    FmOSPos.QrOSSrv.Params := DmModOS.QrOSSrv.Params;
    UMyMod.AbreQuery(FmOSPos.QrOSSrv, Dmod.MyDB);
    //
    if SQLType = stUpd then
    begin
      FmOSPos.EdControle.ValueVariant := DmModOS.QrOSPosControle.Value;
      FmOSPos.EdDias.ValueVariant := DmModOS.QrOSPosDias.Value;
    end;
    FmOSPos.ShowModal;
    FmOSPos.Destroy;
    //
  end;
end;

procedure TFmOSCab.MostraOSPsq();
var
  Codigo: Integer;
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  //
  if DBCheck.CriaFm(TFmOSPsq, FmOSPsq, afmoNegarComAviso) then
  begin
    FmOSPsq.ShowModal;
    Codigo := FmOSPsq.FCodigo;
    FmOSPsq.Destroy;
    //
    if Codigo <> 0 then
      LocCod(QrOSCabCodigo.Value, Codigo);
  end;
end;

procedure TFmOSCab.MostraOSSrv(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmOsSrv, FmOsSrv, afmoNegarComAviso) then
  begin
    FmOsSrv.ImgTipo.SQLType := SQLType;
    FmOsSrv.FQrInsUpd := DmModOS.QrOsSrv;
    //
    FmOsSrv.QrOSCab.Close;
    FmOsSrv.QrOSCab.SQL.Text := QrOSCab.SQL.Text;
    FmOsSrv.QrOSCab.Params := QrOSCab.Params;
    UMyMod.AbreQuery(FmOsSrv.QrOSCab, Dmod.MyDB);
    //
    if SQLType = stUpd then
    begin
      FmOsSrv.EdControle.ValueVariant := DmModOS.QrOSSrvControle.Value;
      FmOsSrv.EdDesServico.ValueVariant := DmModOS.QrOSSrvDesServico.Value;
      FmOsSrv.CBDesServico.KeyValue := DmModOS.QrOSSrvDesServico.Value;
      FmOsSrv.EdGarantiaDd.ValueVariant := DmModOS.QrOSSrvGarantiaDd.Value;
      FmOsSrv.EdHrEvacuar.ValueVariant := DmModOS.QrOSSrvHrEvacuar.Value;
      FmOsSrv.EdHrExecutar.ValueVariant := DmModOS.QrOSSrvHrExecutar.Value;
      //
      FmOsSrv.EdValCalc.ValueVariant := DmModOS.QrOSSrvValCalc.Value;
      FmOsSrv.EdValInfo.ValueVariant := DmModOS.QrOSSrvValInfo.Value;
      FmOsSrv.EdValDesc.ValueVariant := DmModOS.QrOSSrvValDesc.Value;
      FmOsSrv.EdValTota.ValueVariant := DmModOS.QrOSSrvValTota.Value;
      //
      FmOsSrv.RGAutorizado.ItemIndex := DmModOS.QrOSSrvAutorizado.Value;
      FmOSSrv.MeDetalhes.Text        := DmModOS.QrOSSrvDetalhes.Value;
      //
    end;
    FmOsSrv.FCriou := True;
    //FmOsSrv.CalculaTotal();
    FmOsSrv.ShowModal;
    FmOsSrv.Destroy;
    //
    LocCod(QrOSCabCodigo.Value, QrOSCabCodigo.Value);
  end;
end;

procedure TFmOSCab.OSPrz1_AlteraClick(Sender: TObject);
begin
  UnOSUnit.MostraOSPrz(stUpd, QrOSCab, DmModOS.QrOSPrz);
end;

procedure TFmOSCab.OSPrz1_ExcluiClick(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o da condi��o de pagamento "' +
  DmModOS.QrOSPrzNO_CONDICAO.Value + '"?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then DBCheck.ExcluiRegistro(Dmod.QrUpd, DmModOS.QrOSPrz, 'OSPrz',
  ['Controle'], ['Controle'], True);
end;

procedure TFmOSCab.OSPrz1_IncluiClick(Sender: TObject);
begin
  UnOSUnit.MostraOSPrz(stIns, QrOSCab, DmModOS.QrOSPrz);
end;

procedure TFmOSCab.IncluiC3Click(Sender: TObject);
begin
  MostraOSPos(stIns);
end;

procedure TFmOSCab.Incluiitens1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOSChk, FmOSChk, afmoNegarComAviso) then
  begin
    FmOSChk.FQrOSChk := DmModOS.QrOSChk; 
    FmOSChk.FOSCab   := QrOSCabCodigo.Value;
    FmOSChk.ShowModal;
    FmOSChk.Destroy;
  end;
end;

procedure TFmOSCab.ItsInclui6Click(Sender: TObject);
begin
  MostraOSCxI(stIns);
end;

procedure TFmOSCab.IncluiOSAge1Click(Sender: TObject);
const
  Aviso  = '...';
  Titulo = 'Sele��o de Agentes';
  Prompt = 'Seleciones os agentes operacionais:';
  //Campo  = 'Descricao';
  //
  DestTab = 'osage';
  DestMaster = 'Codigo';
  DestDetail = 'Controle';
  DestSelec1 = 'Agente';
  SorcField  = 'Agente';
  ExcluiAnteriores = True;
var
  ValrMaster: Integer;
begin
  ValrMaster := QrOSCabCodigo.Value;
  if DBCheck.EscolheCodigosMultiplos_A(
  Aviso, Titulo, Prompt, nil, 'Ativo', 'Nivel1', 'Nome', [
  'DELETE FROM _selcods_; ',
  'INSERT INTO _selcods_ ',
  'SELECT Codigo Nivel1, 0 Nivel2, ',
  '0 Nivel3, 0 Nivel4, 0 Nivel5, Nome, 0 Ativo ',
  'FROM ' + TMeuDB + '.entidades',
  'WHERE Nome <> "" ',
  'AND Fornece2="V" ',
  ''],[
  'SELECT * FROM _selcods_ ',
  'ORDER BY Nome;',
  ''], DestTab, DestMaster, DestDetail, DestSelec1, ValrMaster,
  DmModOS.QrOSAge, SorcField, ExcluiAnteriores) then
    UnOSUnit.ReopenOSAge(DmModOS.QrOSAge, QrOSCabCodigo.Value, 0);
end;

procedure TFmOSCab.IncluiOSCabAlv1Click(Sender: TObject);
begin
  UnOSUnit.InsAltOSCabAlv(stIns, DmModOS.QrOSCabAlv, QrOSCabCodigo.Value);
end;

procedure TFmOSCab.PMCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemCabUpd(CabAltera1, QrOSCab);
  MyObjects.HabilitaMenuItemCabDel(CabExclui1, QrOSCab, DmModOS.QrOSSrv);
end;

procedure TFmOSCab.PMFatPopup(Sender: TObject);
begin
  Fatura1.Enabled := not ((DmModOS.QrLctFatRef.State <> dsInactive) and
                     (DmModOS.QrLctFatRef.RecordCount > 0));
  Excluifaturamento1.Enabled := not Fatura1.Enabled;
end;

procedure TFmOSCab.PMItsPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui1, QrOSCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera1, DmModOS.QrOSSrv);
  MyObjects.HabilitaMenuItemCabDelC1I3(ItsExclui1, DmModOS.QrOSSrv,
  DmModOS.QrOSAlv, DmModOS.QrOSFrmCab, DmModOS.QrOSMonCab);
end;

procedure TFmOSCab.PMOSAgePopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiOSAge1, QrOSCab);
  //MyObjects.HabilitaMenuItemItsUpd(EditaOSAge1, DmModOS.QrOSAge);
  MyObjects.HabilitaMenuItemItsDel(RemoveOSAge1, DmModOS.QrOSAge);
end;

procedure TFmOSCab.PMOSCabAlvPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(IncluiOSCabAlv1, QrOSCab);
  //MyObjects.HabilitaMenuItemItsUpd(AlteraOSCabAlv1, DmModOS.QrOSCabAlv);
  MyObjects.HabilitaMenuItemItsDel(ExcluiOSCabAlv1, DmModOS.QrOSCabAlv);
end;

procedure TFmOSCab.PMOSCxaPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui2, QrOSCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera2, DmModOS.QrOSCxa);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui2, DmModOS.QrOSCxa);
end;

procedure TFmOSCab.PMOSCxIPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui6, QrOSCab);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera6, DmModOS.QrOSCxI);
  MyObjects.HabilitaMenuItemItsDel(ItsExclui6, DmModOS.QrOSCxI);
end;

procedure TFmOSCab.PMOSFrmCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui4, DmModOS.QrOSSrv);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera4, DmModOS.QrOSFrmCab);
  MyObjects.HabilitaMenuItemCabDelC1I2(ItsExclui4, DmModOS.QrOSFrmCab, DmModOS.QrOSFrmRec, DmModOS.QrOSFrmDep);
  //
  MyObjects.HabilitaMenuItemItsIns(Itens4, DmModOS.QrOSFrmCab);
end;

procedure TFmOSCab.PMOSMonCabPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(ItsInclui5, DmModOS.QrOSSrv);
  MyObjects.HabilitaMenuItemItsUpd(ItsAltera5, DmModOS.QrOSMonCab);
  MyObjects.HabilitaMenuItemCabDelC1I2(ItsExclui5, DmModOS.QrOSMonCab, DmModOS.QrOSMonRec, DmModOS.QrOSMonDep);
  //
  MyObjects.HabilitaMenuItemItsIns(Itens5, DmModOS.QrOSMonCab);
end;

procedure TFmOSCab.PMOSPrzPopup(Sender: TObject);
begin
  MyObjects.HabilitaMenuItemItsIns(OSPrz1_Inclui, QrOSCab);
  MyObjects.HabilitaMenuItemItsUpd(OSPrz1_Altera, DmModOS.QrOSPrz);
  MyObjects.HabilitaMenuItemItsDel(OSPrz1_Exclui, DmModOS.QrOSPrz);
end;

procedure TFmOSCab.QrOSCabAfterClose(DataSet: TDataSet);
begin
  MyObjects.Entitula(GBOperacao, [LaOperacaoA, LaOperacaoB, LaOperacaoC],
  [], '', False, taLeftJustify, 2, 10, 20);
end;

procedure TFmOSCab.QrOSCabAfterScroll(DataSet: TDataSet);
begin
  PnDedetizacao.Visible    := Geral.IntInConjunto(1, QrOSCabOperacao.Value);
  PnCaixaDAgua.Visible     := Geral.IntInConjunto(2, QrOSCabOperacao.Value);
  PnMonitoramento.Visible  := Geral.IntInConjunto(4, QrOSCabOperacao.Value);
  //
  if Geral.IntInConjunto(1, QrOSCabOperacao.Value) then
    PCOperacoes.ActivePageIndex := 0;
  if Geral.IntInConjunto(2, QrOSCabOperacao.Value) then
    PCOperacoes.ActivePageIndex := 1;
  if Geral.IntInConjunto(4, QrOSCabOperacao.Value) then
    PCOperacoes.ActivePageIndex := 2;
  //
  UnOSUnit.ReopenOSAge(DmModOS.QrOSAge, QrOSCabCodigo.Value, 0);
  UnOSUnit.ReopenOSSrv(DmModOS.QrOSSrv, QrOSCabCodigo.Value, 0);
  UnOSUnit.ReopenOSCabAlv(DmModOS.QrOSCabAlv, QrOSCabCodigo.Value, 0);
  UnOSUnit.ReopenOSPrz(DmModOS.QrOSPrz, QrOSCabCodigo.Value, 0);
  UnOSUnit.ReopenOSPos(DmModOS.QrOSPos, QrOSCabCodigo.Value, 0);
  UnOSUnit.ReopenLctFatRef(DmModOS.QrLctFatRef, QrOSCabCodigo.Value, 0);
  UnOSUnit.ReopenEntiMail(DmModOS.QrEntiMail, QrOSCabEntiContat.Value, 0);
  UnOSUnit.ReopenEntiTel(DmModOS.QrEntiTel, QrOSCabEntiContat.Value, 0);
  UnOSUnit.ReopenOSCxa(DmModOS.QrOSCxa, QrOSCabCodigo.Value, 0);
  UnOSUnit.ReopenOSPipMon(DmModOS.QrOSPipMon, QrOSCabCodigo.Value, 0);
  UnOSUnit.ReopenOSCxI(DmModOS.QrOSCxI, QrOSCabCodigo.Value, 0);
  UnOSUnit.ReopenOSChk(DmModOS.QrOSChk, QrOSCabCodigo.Value, 0);
  //
  MyObjects.Entitula(GBOperacao, [LaOperacaoA, LaOperacaoB, LaOperacaoC],
  [], ' ' + QrOSCabNO_OPERACAO.Value, False, taLeftJustify, 2, 10, 20);
end;

procedure TFmOSCab.QrOSCabBeforeClose(DataSet: TDataSet);
begin
  DmModOS.QrOSSrv.Close;
  DmModOS.QrOSAge.Close;
  DmModOS.QrOSCabAlv.Close;
  DmModOS.QrOSPrz.Close;
  DmModOS.QrOSPos.Close;
  DmModOS.QrLctFatRef.Close;
  DmModOS.QrEntiMail.Close;
  DmModOS.QrEntiTel.Close;
  DmModOS.QrOSCxa.Close;
  DmModOS.QrOSCxI.Close;
  DmModOS.QrOSPipMon.Close;
  DmModOS.QrOSAge.Close;
  DmModOS.QrOSChk.Close;
  //
  //
  PnDedetizacao.Visible := False;
  PnCaixaDAgua.Visible := False;
end;

procedure TFmOSCab.QrOSCabBeforeOpen(DataSet: TDataSet);
begin
  QrOSCabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmOSCab.QrOSCabCalcFields(DataSet: TDataSet);
begin
  QrOSCabTXTContat.Value := Geral.FDT(QrOSCabDtaContat.Value, 0, True);
  //
  QrOSCabTXTVisPrv.Value := Geral.FDT(QrOSCabDtaVisPrv.Value, 0, True);
  QrOSCabTXTVisExe.Value := Geral.FDT(QrOSCabDtaVisExe.Value, 0, True);
  //
  QrOSCabTXTExePrv.Value := Geral.FDT(QrOSCabDtaExePrv.Value, 0, True);
  QrOSCabTXTExeIni.Value := Geral.FDT(QrOSCabDtaExeIni.Value, 0, True);
  QrOSCabTXTExeFim.Value := Geral.FDT(QrOSCabDtaExeFim.Value, 0, True);
  //
  QrOSCabTXTTel_ENT.Value := Geral.FormataTelefone_TT_Curto(QrOSCabTel_ENT.Value);
  //
  QrOSCabNO_OPERACAO.Value := UnOSUnit.NomeDeOperacao(QrOSCabOperacao.Value);
end;

procedure TFmOSCab.Removeitens1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DmModOS.QrOSChk, DBGOSChk,
  'oschk', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmOSCab.RemoveOSAge1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a retirada do agente selecionado?',
  'osage', 'Controle', DmModOS.QrOSAgeControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(DmModOS.QrOSAge,
    DmModOS.QrOSAgeControle, DmModOS.QrOSAgeControle.Value);
    UnOSUnit.ReopenOSAge(DmModOS.QrOSAge, QrOSCabCodigo.Value, Controle);
  end;
end;

procedure TFmOSCab.RemovePIPs1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DmModOS.QrOSPIPMon, DBGOSChk,
  'ospipmon', ['Controle'], ['Controle'], istPergunta, '');
end;

procedure TFmOSCab.ExcluiC3Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a exclus�o do alerta selecionado?',
  'ospos', 'Controle', DmModOS.QrOSPosControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(DmModOS.QrOSPos,
    DmModOS.QrOSPosControle, DmModOS.QrOSPosControle.Value);
    UnOSUnit.ReopenOSPos(DmModOS.QrOSPrz, QrOSCabCodigo.Value, Controle);
  end;
end;

procedure TFmOSCab.Excluifaturamento1Click(Sender: TObject);
var
  Filial, Quitados, FatNum: Integer;
  TabLctA: String;
begin
  FatNum := QrOSCAbCodigo.Value;
  //Filial := DmFatura.ObtemFilialDeEntidade(QrOSCabEmpresa.Value);
  Filial := QrOSCabEmpresa.Value;
  TabLctA  := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  //
  Quitados := DModFin.FaturamentosQuitados(
    TabLctA, VAR_FATID_4001, FatNum);
  //
  if Quitados > 0 then
  begin
    Geral.MensagemBox('Existem ' + Geral.FF0(Quitados) +
    ' itens j� quitados que impedem a exclus�o deste faturamento',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Geral.MensagemBox(
  'Confirma a exclus�o do faturamento e de TODOS seus lan�amentos financeiros atrelados?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
      'DELETE FROM ' + TabLctA,
      'WHERE FatID=' + Geral.FF0(VAR_FATID_4001),
      'AND FatNum=' + Geral.FF0(FatNum),
      '']);
      //
      UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
      'DELETE FROM lctfatref',
      'WHERE Controle=' + Geral.FF0(FatNum),
      '']);
      //
      (* N�o tem m�ltiplos faturamentos na mesma OS!
      UMyMod.ExecutaMySQLQuery1(Dmod.QrUpd, [
      'DELETE FROM ? ',
      'WHERE Controle=' + Geral.FF0(FatNum),
      '']);
      *)
      //
      UnOSUnit.ReopenLctFatRef(DmModOS.QrLctFatRef, FatNum, 0);
    finally
      Screen.Cursor := crDefault;
    end;
    //
  end;
end;

procedure TFmOSCab.ItsExclui6Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a retirada da foto da caixa d`�gua " ' +
  DmModOS.QrOSCxIFotoCxa.Value + '"?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then DBCheck.ExcluiRegistro(Dmod.QrUpd, DmModOS.QrOSCxI, 'OSCxI',
  ['Controle'], ['Controle'], True);
  //
  LocCod(QrOSCabCodigo.Value, QrOSCabCodigo.Value);
end;

procedure TFmOSCab.ExcluiOSCabAlv1Click(Sender: TObject);
var
  Controle: Integer;
begin
  if UMyMod.ExcluiRegistroInt1(
  'Confirma a retirada da praga alvo selecionada?',
  'oscabalv', 'Controle', DmModOS.QrOSCabAlvControle.Value, Dmod.MyDB) = ID_YES then
  begin
    Controle := GOTOy.LocalizaPriorNextIntQr(DmModOS.QrOSCabAlv,
    DmModOS.QrOSCabAlvControle, DmModOS.QrOSCabAlvControle.Value);
    UnOSUnit.ReopenOSCabAlv(DmModOS.QrOSCabAlv, QrOSCAbCodigo.Value, Controle);
  end;
end;

procedure TFmOSCab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOSCabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOSCab.DefParams;
begin
  VAR_GOTOTABELA := 'oscab';
  VAR_GOTOMYSQLTABLE := QrOSCab;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT eco.Nome NO_ENTICONTAT, cab.*, ');
  VAR_SQLx.Add('fge.Nome NO_FatoGeradr,');
  VAR_SQLx.Add('sta.Nome NO_ESTATUS, stc.Nome NO_SiapTerCad,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NO_ENT,');
  VAR_SQLx.Add('IF(ent.Tipo=0, ent.ETe1, ent.PTe1) Tel_ENT,');
  VAR_SQLx.Add('IF(pag.Tipo=0, pag.RazaoSocial, pag.Nome) NO_PAG,');
  VAR_SQLx.Add('IF(ctr.Tipo=0, ctr.RazaoSocial, ctr.Nome) NO_CTR,');
  VAR_SQLx.Add('car.Nome NO_CART, ppc.Nome NO_PRZ, ');
  VAR_SQLx.Add('tg1.Nome NO_ExeTxtCli1, tg2.Nome NO_ExeTxtCli2 ');
  VAR_SQLx.Add('FROM oscab cab');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=cab.Entidade');
  VAR_SQLx.Add('LEFT JOIN entidades pag ON pag.Codigo=cab.EntPagante');
  VAR_SQLx.Add('LEFT JOIN entidades ctr ON ctr.Codigo=cab.EntContrat');
  VAR_SQLx.Add('LEFT JOIN fatogeradr fge ON fge.Codigo=cab.FatoGeradr');
  VAR_SQLx.Add('LEFT JOIN estatusoss sta ON sta.Codigo=cab.Estatus');
  VAR_SQLx.Add('LEFT JOIN siaptercad stc ON stc.Codigo=cab.SiapTerCad');
  VAR_SQLx.Add('LEFT JOIN carteiras car ON car.Codigo=cab.CartEmis');
  VAR_SQLx.Add('LEFT JOIN pediprzcab ppc ON ppc.Codigo=cab.CondicaoPg');
  VAR_SQLx.Add('LEFT JOIN enticontat eco ON eco.Controle=cab.EntiContat');
  VAR_SQLx.Add('LEFT JOIN txtgeneric tg1 ON tg1.Codigo=cab.ExeTxtCli1 ');
  VAR_SQLx.Add('LEFT JOIN txtgeneric tg2 ON tg2.Codigo=cab.ExeTxtCli2 ');
  //
  VAR_SQL1.Add('WHERE cab.Codigo=:P0');
  //
  //VAR_SQL2.Add('WHERE cab.CodUsu=:P0');
  //
  VAR_SQLa.Add('WHERE IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) LIKE :P0');
  //
end;

procedure TFmOSCab.EdDtaExeIniExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (EdDtaExeFim.Text = '00:00') then
    EdDtaExeFim.ValueVariant := Geral.STT(EdDtaExeIni.Text, '01:00:00', False);
end;

procedure TFmOSCab.EdDtaExePrvExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (EdFimExePrv.Text = '00:00') then
    EdFimExePrv.ValueVariant := Geral.STT(EdDtaExePrv.Text, '01:00:00', False);
end;

procedure TFmOSCab.EdDtaVisExeExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (EdFimVisExe.Text = '00:00') then
    EdFimVisExe.ValueVariant := Geral.STT(EdDtaVisExe.Text, '01:00:00', False);
end;

procedure TFmOSCab.EdDtaVisPrvExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (EdFimVisPrv.Text = '00:00') then
    EdFimVisPrv.ValueVariant := Geral.STT(EdDtaVisPrv.Text, '01:00:00', False);
end;

procedure TFmOSCab.EdEntidadeChange(Sender: TObject);
var
  Cli: Integer;
begin
  Cli := EdEntidade.ValueVariant;
  UnOSUnit.ReopenSiapTerCad(DmModOS.QrSiapTerCad, Cli, 0, EdSiapTerCad, CBSiapTerCad);
  UnOSUnit.ReopenEntiContat(DmModOS.QrEntiContat, EdEntidade.ValueVariant, EdEntContrat.ValueVariant);
end;

procedure TFmOSCab.EdEntidadeEnter(Sender: TObject);
begin
  try
    if EdEmpresa.ValueVariant = 0 then
      EdEmpresa.SetFocus;
  except
    ;
  end;
end;

procedure TFmOSCab.EdFatoGeradrChange(Sender: TObject);
var
  Habilita: Boolean;
begin
  Habilita := (EdFatoGeradr.ValueVariant <> 0) and (DmModOS.QrFatoGeradrNeedOS_Ori.Value = 1);
  LaOSOrigem.Enabled := Habilita;
  EdOSOrigem.Enabled := Habilita;
  if not Habilita then
    EdOSOrigem.ValueVariant := 0;
end;

procedure TFmOSCab.ItsAltera2Click(Sender: TObject);
begin
  MostraOSCxa(stUpd);
end;

procedure TFmOSCab.EdNumContratKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Entidade: Integer;
begin
  if Key = VK_F4 then
  begin
    if DBCheck.CriaFm(TFmContratPsq, FmContratPsq, afmoNegarComAviso) then
    begin
      Entidade := DmFatura.ObtemEntidadeDeFilial(QrOSCabEmpresa.Value);
      //
      FmContratPsq.EdContratada.ValueVariant  := Entidade;
      FmContratPsq.CBContratada.KeyValue      := Entidade;
      FmContratPsq.EdContratante.ValueVariant := QrOSCabEntidade.Value;
      FmContratPsq.CBContratante.KeyValue     := QrOSCabEntidade.Value;
      //
      FmContratPsq.ShowModal;
      if FmContratPsq.FContrato <> 0 then
        EdNumContrat.ValueVariant := FmContratPsq.FContrato;
      FmContratPsq.Destroy;
    end;
  end;
end;

procedure TFmOSCab.Itens4Click(Sender: TObject);
begin
  MostraOSFrmRec();
end;

procedure TFmOSCab.Itens5Click(Sender: TObject);
begin
  MostraOSMonRec();
end;

procedure TFmOSCab.ItsAltera1Click(Sender: TObject);
begin
  MostraOSSrv(stUpd);
end;

procedure TFmOSCab.ItsAltera4Click(Sender: TObject);
begin
  MostraOSFrmCab(stUpd);
end;

procedure TFmOSCab.ItsAltera5Click(Sender: TObject);
begin
  MostraOSMonCab(stUpd);
end;

procedure TFmOSCab.CabExclui1Click(Sender: TObject);
var
  I, N: Integer;
  DS: TDataSource;
  Qr: TmySQLQuery;
begin
  N := 0;
  for I := 0 to ComponentCount - 1 do
  begin
    if ((Components[I] is TCustomDBGrid)) then
    begin
      if (TCustomDBGrid(Components[i]).DataSource <> DsOSCab) then
      begin
        DS := TCustomDBGrid(Components[i]).DataSource;
        if DS <> nil then
        begin
          Qr := TmySQLQuery(DS.DataSet);
          if Qr <> nil then
            if Qr.State <> dsInactive then
              if Qr.RecordCount > 0 then
                N := N + 1;
        end;
      end;
    end;
  end;
  if N > 0 then
    Geral.MB_Aviso('Exclus�o cancelada! Existem ' + IntToStr(N) +
     ' tabelas com registros inseridos para esta ordem de servi�o!')
  else begin
    if UMyMod.ExcluiRegistroInt1(
    'Confirma a exclus�o da OS atual?',
    'oscab', 'Codigo', QrOSCabCodigo.Value, Dmod.MyDB) = ID_YES then
      LocCod(QrOSCabCodigo.Value, QrOSCabCodigo.Value);
  end;
end;

procedure TFmOSCab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOSCab.ItsExclui1Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o do servi�o " ' +
  DmModOS.QrOSSrvNO_DesServico.Value + '"?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then DBCheck.ExcluiRegistro(Dmod.QrUpd, DmModOS.QrOSSrv, 'OSSrv',
  ['Controle'], ['Controle'], True);
  //
  //DmModOS.TotalValorOS(QrOSCabCodigo.Value, gboServi);
  UnOSUnit.TotalValorOS(QrOSCabCodigo.Value);
  LocCod(QrOSCabCodigo.Value, QrOSCabCodigo.Value);
end;

procedure TFmOSCab.ItsExclui2Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a retirada da caixa d`�gua " ' +
  DmModOS.QrOSCxaLocal.Value + '"?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then DBCheck.ExcluiRegistro(Dmod.QrUpd, DmModOS.QrOSCxa, 'OSCxa',
  ['Controle'], ['Controle'], True);
  //
  //UnOSUnit.TotalValorOS(QrOSCabCodigo.Value, gboCaixa);
  UnOSUnit.TotalValorOS(QrOSCabCodigo.Value);
  LocCod(QrOSCabCodigo.Value, QrOSCabCodigo.Value);
end;

procedure TFmOSCab.ItsExclui4Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o da formula��o de aplica��o "' +
  DmModOS.QrOSFrmCabNome.Value + '"?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then DBCheck.ExcluiRegistro(Dmod.QrUpd, DmModOS.QrOSFrmCab, 'OSFrmCab',
  ['Conta'], ['Conta'], True);
end;

procedure TFmOSCab.ItsExclui5Click(Sender: TObject);
begin
  if Geral.MensagemBox('Confirma a exclus�o da formula��o de aplica��o "' +
  DmModOS.QrOSMonCabNome.Value + '"?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
  ID_YES then DBCheck.ExcluiRegistro(Dmod.QrUpd, DmModOS.QrOSMonCab, 'OSMonCab',
  ['Conta'], ['Conta'], True);
end;

procedure TFmOSCab.DBGOSAgeColEnter(Sender: TObject);
begin
  if DBGOSAge.Columns[THackDBGrid(DBGOSAge).Col -1].FieldName = CO_FldRespo then
    DBGOSAge.Options := DBGOSAge.Options - [dgEditing] else
    DBGOSAge.Options := DBGOSAge.Options + [dgEditing];
end;

procedure TFmOSCab.DBGOSAgeColExit(Sender: TObject);
begin
  DBGOSAge.Options := DBGOSAge.Options - [dgEditing];
end;

procedure TFmOSCab.DBGOSAgeDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = CO_FldRespo then
    MeuVCLSkin.DrawGrid(DBGOSAge, Rect, 1, DmModOS.QrOSAgeResponsa.Value);
end;

procedure TFmOSCab.DBGOSAgeMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSAge, DBGOSAge, X,Y);
end;

procedure TFmOSCab.DBGOSAlvMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    BtOSAlvClick(BtOSAlv);
end;

procedure TFmOSCab.DBGOSCabAlvMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSCabAlv, DBGOSCabAlv, X,Y);
end;

procedure TFmOSCab.DBGOSCxaDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Autorizado' then
    MeuVCLSkin.DrawGrid(DBGOSCxa, Rect, 1, DmModOS.QrOSCxaAutorizado.Value);
end;

procedure TFmOSCab.DBGOSCxaMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSCxa, DBGOSCxa, X, Y);
end;

procedure TFmOSCab.DBGOSCxIMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSCxI, DBGOSCxI, X, Y);
end;

procedure TFmOSCab.DBGOSFrmDepMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
  begin
    if MyObjects.FIC((DmModOS.QrOSFrmCab.State = dsInactive)
    or (DmModOS.QrOSFrmCab.RecordCount = 0), nil, 'Informe uma aplica��o!') then
      Exit;
    //
    MostraOSFrmDep(stIns);
  end;
end;

procedure TFmOSCab.DBGOSFrmAbrMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSFrmCab, DBGOSFrmAbr, X, Y);
end;

procedure TFmOSCab.DBGOSFrmCabMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSFrmCab, DBGOSFrmCab, X, Y);
end;

procedure TFmOSCab.DBGOSFrmRecMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSFrmCab, DBGOSFrmRec, X, Y);
end;

procedure TFmOSCab.DBGOSMonCabMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSMonCab, DBGOSMonCab, X, Y);
end;

procedure TFmOSCab.DBGOSMonDepMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
  begin
    if MyObjects.FIC((DmModOS.QrOSMonCab.State = dsInactive)
    or (DmModOS.QrOSMonCab.RecordCount = 0), nil, 'Informe um monitoramento!') then
      Exit;
    //
    MostraOSMonDep(stIns);
  end;
end;

procedure TFmOSCab.DBGOSMonRecMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSMonCab, DBGOSMonRec, X, Y);
end;

procedure TFmOSCab.DBGOSSrvColEnter(Sender: TObject);
begin
  if DBGOSSrv.Columns[THackDBGrid(DBGOSSrv).Col -1].FieldName = CO_FldAutrz then
    DBGOSSrv.Options := DBGOSSrv.Options - [dgEditing] else
    DBGOSSrv.Options := DBGOSSrv.Options + [dgEditing];
end;

procedure TFmOSCab.DBGOSSrvColExit(Sender: TObject);
begin
  DBGOSSrv.Options := DBGOSSrv.Options - [dgEditing];
end;

procedure TFmOSCab.DBGOSSrvDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Autorizado' then
    MeuVCLSkin.DrawGrid(DBGOSSrv, Rect, 1, DmModOS.QrOSSrvAutorizado.Value);
end;

procedure TFmOSCab.DBGOSSrvMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMIts, DBGOSSrv, X, Y);
end;

procedure TFmOSCab.DBGOSChkMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSChk, DBGOSChk, X, Y);
end;

procedure TFmOSCab.DBGOSPipMonMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSPipMon, DBGOSPipMon, X, Y);
end;

procedure TFmOSCab.DBGOSPosMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSPos, DBGOSPos, X, Y);
end;

procedure TFmOSCab.DBGOSPrzDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'Escolhido' then
    MeuVCLSkin.DrawGrid(DBGOSPrz, Rect, 1, DmModOS.QrOSPrzEscolhido.Value);
end;

procedure TFmOSCab.DBGOSPrzMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbRight then
    MyObjects.MostraPopOnControlXY(PMOSPrz, DBGOSPrz, X,Y);
end;

procedure TFmOSCab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOSCab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOSCab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOSCab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOSCab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOSCab.TPDtaExeIniExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (TPDtaExeFim.Date < 2) then
    TPDtaExeFim.Date := TPDtaExeIni.Date;
end;

procedure TFmOSCab.TPDtaExePrvExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (TPFimExePrv.Date < 2) then
    TPFimExePrv.Date := TPDtaExePrv.Date;
end;

procedure TFmOSCab.TPDtaVisExeExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (TPFimVisExe.Date < 2) then
    TPFimVisExe.Date := TPDtaVisExe.Date;
end;

procedure TFmOSCab.TPDtaVisPrvExit(Sender: TObject);
begin
  if (ImgTipo.SQLType = stIns) and (TPFimVisPrv.Date < 2) then
    TPFimVisPrv.Date := TPDtaVisPrv.Date;
end;

procedure TFmOSCab.TravaOForm();
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  GOTOy.BotoesSb(ImgTipo.SQLType);
  //
  MyObjects.Entitula(GBOperacao, [LaOperacaoA, LaOperacaoB, LaOperacaoC],
  [], ' ' + QrOSCabNO_OPERACAO.Value, False, taLeftJustify, 2, 10, 20);
end;

procedure TFmOSCab.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOSCab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOSCabCodigo.Value;
  Close;
end;

procedure TFmOSCab.ItsInclui1Click(Sender: TObject);
begin
  MostraOSSrv(stIns);
end;

procedure TFmOSCab.ItsInclui4Click(Sender: TObject);
begin
  MostraOSFrmCab(stIns);
end;

procedure TFmOSCab.ItsInclui5Click(Sender: TObject);
begin
  MostraOSMonCab(stIns);
end;

procedure TFmOSCab.CabAltera1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrOSCab, [PnDados],
  [PnEdita], EdEntidade, ImgTipo, 'oscab');
  // Esta tirando o valor quando faz antes do cliente!
  EdSiapTerCad.ValueVariant := QrOSCabSiapTerCad.Value;
  CBSiapTerCad.KeyValue     := QrOSCabSiapTerCad.Value;
end;

procedure TFmOSCab.BtConfirmaClick(Sender: TObject);
var
  //DdsPosVda,
  DtaContat, DtaVisPrv, DtaVisExe, DtaExePrv, DtaExeIni,
  DtaExeFim, ObsGaranti, FimVisPrv, FimVisExe, FimExePrv: String;

  //
  //GarantiaDd, HrEvacuar, HrExecutar, CondicaoPG,
  Codigo, Entidade, SiapTerCad, Estatus, FatoGeradr, OSOrigem, EntiContat,
  NumContrat, EntPagante, EntContrat, Empresa, ValiDdOrca, Operacao,
  ExeTxtCli1, ExeTxtCli2: Integer;
  Inicio, Termino: TDateTime;
  Nome, Notas: String;
  Cor: Integer;
  Cption: Byte;
begin
  Codigo         := QrOSCabCodigo.Value;
  Empresa        := EdEmpresa.ValueVariant;
  Entidade       := EdEntidade.ValueVariant;
  SiapTerCad     := EdSiapTerCad.ValueVariant;
  Estatus        := EdEstatus.ValueVariant;
  FatoGeradr     := EdFatoGeradr.ValueVariant;
  OSOrigem       := EdOSOrigem.ValueVariant;
  DtaContat      := Geral.FDT(TPDtaContat.Date, 1) + ' ' + EdDtaContat.Text + ':00';
  DtaVisPrv      := Geral.FDT(TPDtaVisPrv.Date, 1) + ' ' + EdDtaVisPrv.Text + ':00';
  DtaVisExe      := Geral.FDT(TPDtaVisExe.Date, 1) + ' ' + EdDtaVisExe.Text + ':00';
  DtaExePrv      := Geral.FDT(TPDtaExePrv.Date, 1) + ' ' + EdDtaExePrv.Text + ':00';
  DtaExeIni      := Geral.FDT(TPDtaExeIni.Date, 1) + ' ' + EdDtaExeIni.Text + ':00';
  DtaExeFim      := Geral.FDT(TPDtaExeFim.Date, 1) + ' ' + EdDtaExeFim.Text + ':00';
  FimVisPrv      := Geral.FDT(TPFimVisPrv.Date, 1) + ' ' + EdFimVisPrv.Text + ':00';
  FimVisExe      := Geral.FDT(TPFimVisExe.Date, 1) + ' ' + EdFimVisExe.Text + ':00';
  FimExePrv      := Geral.FDT(TPFimExePrv.Date, 1) + ' ' + EdFimExePrv.Text + ':00';
  {
  DdsPosVda      := EdDdsPosVda.ValueVariant;
  GarantiaDd     := EdGarantiaDD.ValueVariant;
  HrEvacuar      := EdHrEvacuar.ValueVariant;
  HrExecutar     := EdHrExecutar.ValueVariant;
  CondicaoPG     := EdCondicaoPG.ValueVariant;
  }
  EntiContat     := EdEntiContat.ValueVariant;
  NumContrat     := EdNumContrat.ValueVariant;
  EntPagante     := EdEntPagante.ValueVariant;
  EntContrat     := EdEntContrat.ValueVariant;
  //
  ValiDdOrca     := EdValiDdOrca.ValueVariant;
  Operacao       := CGOperacao.Value;
  ExeTxtCli1     := EdExeTxtCli1.ValueVariant;
  ExeTxtCli2     := EdExeTxtCli2.ValueVariant;
  ObsGaranti     := MeObsGaranti.Text;
  //
  //
  Inicio  := Geral.STD_109(DtaVisPrv);
  Termino := Geral.STD_109(FimVisPrv);
  if DmModAgenda.DesistePorqueNaoVaiAgendar(Inicio, Termino, 'Vistoria') then
    Exit;
  //
  Inicio  := Geral.STD_109(DtaExePrv);
  Termino := Geral.STD_109(FimExePrv);
  if DmModAgenda.DesistePorqueNaoVaiAgendar(Inicio, Termino, 'Execu��o') then
    Exit;
  //
  if MyObjects.FIC((OSOrigem = 0) and (EdOSOrigem.Enabled), EdOSOrigem,
  'Informe a OS de origem!') then
    Exit;
  if MyObjects.FIC(Operacao = 0, CGOperacao, 'Informe a opera��o!') then
    Exit;
  if ImgTipo.SQLType = stUpd then
  begin
    if MyObjects.FIC((DmModOS.QrOSSrv.RecordCount > 0) and
    (Geral.IntInConjunto(1, Operacao) = False), CGOperacao,
    'A opera��o dedetiza��o deve ser marcada pois j� existe item para ela!') then
      Exit;
    if MyObjects.FIC((DmModOS.QrOSCxa.RecordCount > 0) and
    (Geral.IntInConjunto(2, Operacao) = False), CGOperacao,
    'A opera��o limpeza de caixa d''�gua deve ser marcada pois j� existe item para ela!') then
      Exit;
    if MyObjects.FIC((DmModOS.QrOSPipMon.RecordCount > 0) and
    (Geral.IntInConjunto(4, Operacao) = False), CGOperacao,
    'A opera��o monitoramento deve ser marcada pois j� existe item para ela!') then
      Exit;
  end;
  //
  Codigo := UMyMod.BPGS1I32('oscab', 'Codigo', '', '', tsDef,
    ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'oscab', False, [
  'Empresa',
  'Entidade', 'SiapTerCad', 'Estatus',
  'FatoGeradr', 'OSOrigem', 'DtaContat',
  'DtaVisPrv', 'DtaVisExe', 'DtaExePrv',
  'DtaExeIni', 'DtaExeFim', (*'GarantiaDd',
  'HrEvacuar', 'HrExecutar', *'ValorTotal',*
  'CondicaoPG', 'DdsPosVda',*) 'EntiContat',
  'NumContrat', 'EntPagante', 'EntContrat',
  'ValiDdOrca', 'Operacao', 'ExeTxtCli1',
  'ExeTxtCli2', 'ObsGaranti',
  'FimVisPrv', 'FimVisExe', 'FimExePrv'], [
  'Codigo'], [
  Empresa,
  Entidade, SiapTerCad, Estatus,
  FatoGeradr, OSOrigem, DtaContat,
  DtaVisPrv, DtaVisExe, DtaExePrv,
  DtaExeIni, DtaExeFim, (*GarantiaDd,
  HrEvacuar, HrExecutar, *ValorTotal,*
  CondicaoPG, DdsPosVda,*) EntiContat,
  NumContrat, EntPagante, EntContrat,
  ValiDdOrca, Operacao, ExeTxtCli1,
  ExeTxtCli2, ObsGaranti,
  FimVisPrv, FimVisExe, FimExePrv], [
  Codigo], True) then
  begin
    // Vistoria
    Inicio  := Geral.STD_109(DtaVisPrv);
    Termino := Geral.STD_109(FimVisPrv);
    Nome    := '';
    Notas   := '';
    Cor     := 0;
    Cption  := 0;
    DmModAgenda.AtualizaCompromissoNaAgenda(qagOSBgstrl, aagVistoria, Codigo,
    Inicio, Termino, Empresa, Entidade, SiapTerCad, FatoGeradr, Nome, Notas,
    Cor, Cption);
    // Execu��o
    Inicio  := Geral.STD_109(DtaExePrv);
    Termino := Geral.STD_109(FimExePrv);
    Nome    := '';
    Notas   := '';
    Cor     := 0;
    Cption  := 0;
    DmModAgenda.AtualizaCompromissoNaAgenda(qagOSBgstrl, aagExecucao, Codigo,
    Inicio, Termino, Empresa, Entidade, SiapTerCad, FatoGeradr, Nome, Notas,
    Cor, Cption);
    //
    UnOSUnit.AtualizaCadastroCunsIts(EntContrat, Entidade);
    UnOSUnit.TotalValorOS(Codigo);
    LocCod(Codigo, Codigo);
    if FSeq = 1 then
      Close
    else
      TravaOForm();
  end;
end;

procedure TFmOSCab.BtDesisteClick(Sender: TObject);
begin
  TravaOForm();
end;

procedure TFmOSCab.BtFatClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMFat, BtFat);
end;

procedure TFmOSCab.BtItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIts, BtIts);
end;

procedure TFmOSCab.BtOSAlvClick(Sender: TObject);
begin
  if MyObjects.FIC((DmModOS.QrOSSrv.State = dsInactive)
  or (DmModOS.QrOSSrv.RecordCount = 0), nil, 'Informe um servi�o!') then
    Exit;
  //
  MostraOSAlv(stIns);
end;

procedure TFmOSCab.BtOSFrmCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOSFrmCab, BtOSFrmCab);
end;

procedure TFmOSCab.BtOSMonCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOSMonCab, BtOSMonCab);
end;

procedure TFmOSCab.Abrangncia1Click(Sender: TObject);
begin
  if MyObjects.FIC((DmModOS.QrOSFrmCab.State = dsInactive)
  or (DmModOS.QrOSFrmCab.RecordCount = 0), nil,
  'Informe uma formula��o de aplica��o!') then
    Exit;
  //
  MostraOSFrmAbr(stIns);
end;

procedure TFmOSCab.ItsInclui2Click(Sender: TObject);
begin
  MostraOSCxa(stIns);
end;

procedure TFmOSCab.AdicionaPIPsativos1Click(Sender: TObject);
begin
  UnOSUnit.AdicionaPIPsAtivosEmOS(DmModOS.QrOSPipMon,
    QrOSCabCodigo.Value, QrOSCabEntidade.Value, QrOSCabSiapTerCad.Value);
  //UnOSUnit.ReopenOSPipMon(DmModOS.QrOSPipMon, QrOSCabCodigo.Value, 0);
end;

procedure TFmOSCab.AlteraC3Click(Sender: TObject);
begin
  MostraOSPos(stUpd);
end;

procedure TFmOSCab.ItsAltera6Click(Sender: TObject);
begin
  MostraOSCxI(stUpd);
end;

procedure TFmOSCab.BtCabClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCab, BtCab);
end;

procedure TFmOSCab.FormCreate(Sender: TObject);// Adicionado por .DFM > .PAS
CB?.ListSource = DModG.DsEmpresas;

begin
  ImgTipo.SQLType := stLok;
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  PnDedetizacao.Align := alClient;
  PnCaixaDAgua.Align := alClient;
  MeObsGaranti.Align := alClient;
  CriaOForm;
  FSeq := 0;
  //
  DBGOSCabAlv.DataSource := DmModOS.DsOSCabAlv;
  DBGOSAge.DataSource := DmModOS.DsOSAge;
  //DBG.DataSource := DmModOS.Ds;
  //
  CBEntidade.ListSource   := DmModOS.DsEntidades;
  CBEstatus.ListSource    := DmModOS.DsEstatusOSs;
  CBSiapTerCad.ListSource := DmModOS.DsSiapTerCad;
  CBEnticontat.ListSource := DmModOS.DsEntiContat;
  CBFatoGeradr.ListSource := DmModOS.DsFatoGeradr;
  //CBCondicaoPg.ListSource := DmModOS.DsPediPrzCab;
  CBEntContrat.ListSource := DmModOS.DsEntContrat;
  CBEntPagante.ListSource := DmModOS.DsEntPagante;
  CBExeTxtCli1.ListSource := DmModOS.DsExeTxtCli1;
  CBExeTxtCli2.ListSource := DmModOS.DsExeTxtCli2;
  //
  UMyMod.ReAbreQuery(DmModOS.QrEntidades, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(DmModOS.QrEstatusOSs, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(DmModOS.QrSiapTerCad, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(DmModOS.QrFatoGeradr, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(DmModOS.QrEntContrat, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(DmModOS.QrEntPagante, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(DmModOS.QrExeTxtCli1, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(DmModOS.QrExeTxtCli2, Dmod.MyDB, [], '');
  UMyMod.ReAbreQuery(DmModOS.QrAgentes, Dmod.MyDB, [], '');
  //
  DBGOSSrv.DataSource      := DmModOS.DsOSSrv;
  DBGOSAlv.DataSource      := DmModOS.DsOSAlv;
  DBGOSFrmCab.DataSource   := DmModOS.DsOSFrmCab;
  DBGOSFrmRec.DataSource   := DmModOS.DsOSFrmRec;
  DBGOSFrmDep.DataSource   := DmModOS.DsOSFrmDep;
  DBGOSFrmAbr.DataSource   := DmModOS.DsOSFrmAbr;
  DBGOSMonCab.DataSource   := DmModOS.DsOSMonCab;
  DBGOSMonRec.DataSource   := DmModOS.DsOSMonRec;
  DBGOSMonDep.DataSource   := DmModOS.DsOSMonDep;
  DBGOSPos.DataSource      := DmModOS.DsOSPos;
  DBGLctFatRef.DataSource  := DmModOS.DsLctFatRef;
  DBGEntiMail.DataSource   := DmModOS.DsEntiMail;
  DBGEntiTel.DataSource    := DmModOS.DsEntiTel;
  DBGOSCxa.DataSource      := DmModOS.DsOSCxa;
  DBGOSCxaAtrib.DataSource := DmModOS.DsOSCxaAtrib;
  DBGOSPipMon.DataSource   := DmModOS.DsOSPipMon;
  DBGOSCxI.DataSource      := DmModOS.DsOSCxI;
  DBGOSChk.DataSource      := DmModOS.DsOSChk;
  //
  DBEdNOME_FORMA.DataSource  := DmModOS.DsOSCxa;
  DBEdNO_MATERIAL.DataSource := DmModOS.DsOSCxa;
  DBEdVolumeL.DataSource     := DmModOS.DsOSCxa;
  DBEdMedidas.DataSource     := DmModOS.DsOSCxa;
  DBMeAcesso.DataSource      := DmModOS.DsOSCxa;
  DBMeOSCxI.DataSource       := DmModOS.DsOSCxI;
  DBMeOSCxa.DataSource       := DmModOS.DsOSCxa;
  //
  MyObjects.ConfiguraCheckGroup(CGOperacao, sListaOperacoesBugs, 3, 0, False);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(DmModOS.QrPediPrzCab, Dmod.MyDB, [
  'SELECT Codigo, CodUsu, Nome, MaxDesco, ',
  'JurosMes, Parcelas, Percent1, Percent2 ',
  'FROM pediprzcab ',
  //'WHERE Aplicacao & :P0 <> 0 ',
  'ORDER BY Nome ',
  '']);
end;

procedure TFmOSCab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOSCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOSCab.SbImprimeClick(Sender: TObject);
begin
  if DmModOS.QrOSAge.RecordCount = 0 then
    Geral.MB_Aviso('Informe pelo menos um agente!')
  else begin
    if DBCheck.CriaFm(TFmOsImp, FmOsImp, afmoSoBoss) then
    begin
      FmOsImp.EdOSCab.ValueVariant  := QrOSCabCodigo.Value;
      FmOsImp.EdCliCod.ValueVariant := QrOSCabEntidade.Value;
      FmOsImp.EdCliNom.Text         := QrOSCabNO_ENT.Value;
      FmOsImp.PageControl1.ActivePageIndex := 2;
      //
      FmOsImp.ShowModal;
      FmOsImp.Destroy;
    end;
  end;  
end;

procedure TFmOSCab.SbNomeClick(Sender: TObject);
begin
  MostraOSPsq();
end;

procedure TFmOSCab.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOSCabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOSCab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmOSCab.Fatura1Click(Sender: TObject);
begin
  if Myobjects.FIC(QrOSCabEntPagante.Value = 0, nil,
  'Faturamento n�o realizado! Pagante n�o informado!') then
    Exit;
  MostraFormOSCabFat();
end;

procedure TFmOSCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  //
  if FSeq = 1 then
    CabInclui1Click(Self)
  else
  if (not FLocIni) and (FCabIni <> 0)  then
  begin
    LocCod(FCabIni, FCabIni);
    if QrOSCabCodigo.Value <> FCabIni then Geral.MensagemBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
  if Screen.Width > 1280 then
  begin
    WindowState := wsNormal;
    Width := 1280;
    Height := 800;
  end;
end;

procedure TFmOSCab.SbQueryClick(Sender: TObject);
begin
  MostraOSPsq();
end;

procedure TFmOSCab.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

function TFmOSCab.frxGER_OSERV_001_001UserFunction(const MethodName: string;
  var Params: Variant): Variant;
{
var
  Tabela: String;
  Coluna, IdxFld, Ultima, MaxCol, QtdReg: Integer;
}
begin
{
  if AnsiCompareText(MethodName, 'CorMemoMatrix') = 0 then
  begin
    Tabela := Params[0];
    Coluna := Params[1];
    IdxFld := Params[2];
    //
    Result := clFuchsia;
    //
    if Tabela = 'osdep' then
    begin
      MaxCol := CO_IMP_COLS_DEP;
      QtdReg := DmModOS.QrOSDep.RecordCount;
    end else
    if Tabela = 'osalv' then
    begin
      MaxCol := CO_IMP_COLS_ALV;
      QtdReg := DmModOS.QrOSAlv.RecordCount;
    end;
    Ultima := QtdReg mod MaxCol;
    //
    if Ultima < MaxCol then
    begin
      case IdxFld of
        1: Result := CorMedia;
        2: Result := CorFraca;
        3: Result := CorFundo;
      end;
    end else
      Result := clBlack;
  end;
}
end;

procedure TFmOSCab.Gerabloqueto1Click(Sender: TObject);
//{$IFDEF IMP_BLOQLCT}
var
  EntiInt, Filial(*, Periodo*): Integer;
  TabLctA: String;
//{$ENDIF}
begin
  //{$IFDEF IMP_BLOQLCT}
  Gerabloqueto1.Enabled := False;
  //
  //Entidade := QrOSCabEmpresa.Value;
  //Filial := DmFatura.ObtemFilialDeEntidade(Entidade);
  Filial := QrOSCabEmpresa.Value;
  EntiInt := DmFatura.ObtemEntidadeDeFilial(Filial);
  TabLctA  := DmodG.NomeTab(TMeuDB, ntLct, False, ttA, Filial);
  //
  DBloGeren.BloquetosParcelasByFatNum(EntiInt, VAR_FatID_4001,
    QrOSCabCodigo.Value, TabLctA);
  Gerabloqueto1.Enabled := True;
  //{$ENDIF}
end;

procedure TFmOSCab.CabInclui1Click(Sender: TObject);
var
  Data: TDateTime;
  Hora: TTime;
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrOSCab, [PnDados],
  [PnEdita], EdEntidade, ImgTipo, 'oscab');
  DModG.ObtemAgora();
  DModG.ObtemDataHora(Data, Hora);
  //
  TPDtaContat.Date := Data;
  EdDtaContat.Text := FormatDateTime('hh:nn', Hora);
  CGOperacao.Value := 0;
end;

procedure TFmOSCab.CGOperacaoClick(Sender: TObject);
begin
  MyObjects.Entitula(GBOperacao, [LaOperacaoA, LaOperacaoB, LaOperacaoC],
  [], ' ' + UnOSUnit.NomeDeOperacao(CGOperacao.Value), False, taLeftJustify, 2, 10, 20);
end;

function TFmOSCab.CondicaoPGSelecionado(): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT Condicao ',
  'FROM osprz ',
  'WHERE Codigo=' + Geral.FF0(QrOSCabCodigo.Value),
  'AND Escolhido=1',
  ' ',
  '']);
  //
  Result := Dmod.QrAux.FieldByName('Condicao').AsInteger;
end;

end.

