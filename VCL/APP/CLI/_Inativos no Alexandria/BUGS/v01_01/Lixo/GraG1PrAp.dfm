object FmGraG1PrAp: TFmGraG1PrAp
  Left = 368
  Top = 194
  Caption = 'PRD-GRUPO-023 :: Produtos de ...'
  ClientHeight = 538
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 442
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitHeight = 203
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 105
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Label1: TLabel
        Left = 16
        Top = 16
        Width = 33
        Height = 13
        Caption = 'Nivel1:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 472
        Top = 60
        Width = 153
        Height = 13
        Caption = 'Registro no Minist'#233'rio da Sa'#250'de:'
        FocusControl = DBEdit1
      end
      object Label6: TLabel
        Left = 16
        Top = 60
        Width = 33
        Height = 13
        Caption = 'Marca:'
        FocusControl = DBEdit4
      end
      object Label8: TLabel
        Left = 220
        Top = 60
        Width = 53
        Height = 13
        Caption = 'Fabricante:'
        FocusControl = DBEdit5
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Nivel1'
        DataSource = DsGraG1PrAp
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 76
        Top = 32
        Width = 553
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NO_GG1'
        DataSource = DsGraG1PrAp
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBEdit1: TDBEdit
        Left = 472
        Top = 76
        Width = 157
        Height = 21
        DataField = 'RegMinSaud'
        DataSource = DsGraG1PrAp
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 16
        Top = 76
        Width = 200
        Height = 21
        DataField = 'NO_MARCA'
        DataSource = DsGraG1PrAp
        TabOrder = 2
      end
      object DBEdit5: TDBEdit
        Left = 220
        Top = 76
        Width = 249
        Height = 21
        DataField = 'NO_FABR'
        DataSource = DsGraG1PrAp
        TabOrder = 3
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 636
        Top = 16
        Width = 133
        Height = 81
        Caption = ' Imprime no or'#231'amento: '
        Columns = 2
        DataField = 'impinOrca'
        DataSource = DsGraG1PrAp
        Items.Strings = (
          'N'#227'o'
          'Sim')
        ParentBackground = True
        TabOrder = 5
        Values.Strings = (
          '0'
          '1')
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 378
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 139
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          NumGlyphs = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
          NumGlyphs = 2
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
          NumGlyphs = 2
        end
        object Panel2: TPanel
          Left = 412
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
      end
    end
    object DBREObservacao: TDBRichEdit
      Left = 0
      Top = 105
      Width = 784
      Height = 100
      Align = alTop
      DataField = 'Observacao'
      DataSource = DsGraG1PrAp
      TabOrder = 2
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 442
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitHeight = 203
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 105
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label4: TLabel
        Left = 16
        Top = 60
        Width = 94
        Height = 13
        Caption = 'Fabricante / Marca:'
      end
      object Label10: TLabel
        Left = 472
        Top = 60
        Width = 153
        Height = 13
        Caption = 'Registro no Minist'#233'rio da Sa'#250'de:'
      end
      object SbMarcas: TSpeedButton
        Left = 444
        Top = 76
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SbMarcasClick
      end
      object EdNivel1: TdmkEdit
        Left = 16
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 76
        Top = 32
        Width = 553
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object RGImpInOrca: TdmkRadioGroup
        Left = 636
        Top = 16
        Width = 133
        Height = 81
        Caption = ' Imprime no or'#231'amento: '
        Columns = 2
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 5
        UpdType = utYes
        OldValor = 0
      end
      object EdMarca: TdmkEditCB
        Left = 16
        Top = 76
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBMarca
        IgnoraDBLookupComboBox = False
      end
      object EdRegMinSaud: TdmkEdit
        Left = 472
        Top = 76
        Width = 157
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object CBMarca: TdmkDBLookupComboBox
        Left = 72
        Top = 76
        Width = 369
        Height = 21
        KeyField = 'Controle'
        ListField = 'NO_MARCA_FABR'
        ListSource = DsMarcas
        TabOrder = 3
        dmkEditCB = EdMarca
        UpdType = utYes
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 379
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 140
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object Panel1: TPanel
        Left = 644
        Top = 15
        Width = 138
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
          NumGlyphs = 2
        end
      end
      object BtTexto: TBitBtn
        Tag = 121
        Left = 332
        Top = 14
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Observa'#231#245'es'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtTextoClick
        NumGlyphs = 2
      end
    end
    object ReObservacao: TdmkRichEdit
      Left = 0
      Top = 105
      Width = 784
      Height = 124
      Align = alTop
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 2
      QryCampo = 'Observacao'
      UpdCampo = 'Observacao'
      UpdType = utYes
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 0
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 1
        OnClick = SbNovoClick
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 178
        Height = 32
        Caption = 'Produtos de ...'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 178
        Height = 32
        Caption = 'Produtos de ...'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 178
        Height = 32
        Caption = 'Produtos de ...'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrGraG1PrAp: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrGraG1PrApBeforeOpen
    AfterOpen = QrGraG1PrApAfterOpen
    SQL.Strings = (
      'SELECT gg1.Nivel1, gg1.Nome NO_GG1, cpl.RegMinSaud, cpl.Marca,'
      'cpl.impinOrca, gfm.Nome NO_MARCA, gfc.Nome NO_FABR,'
      'IF(cpl.Nivel1 IS NULL, 0, 1) CPL_EXISTE, cpl.Observacao'
      'FROM gragru1 gg1'
      'LEFT JOIN grag1prap cpl ON cpl.Nivel1=gg1.Nivel1'
      'LEFT JOIN grafabmar gfm ON cpl.Marca=gfm.Controle'
      'LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo')
    Left = 64
    Top = 64
    object QrGraG1PrApNivel1: TIntegerField
      FieldName = 'Nivel1'
      Origin = 'gragru1.Nivel1'
    end
    object QrGraG1PrApNO_GG1: TWideStringField
      FieldName = 'NO_GG1'
      Origin = 'gragru1.Nome'
      Size = 120
    end
    object QrGraG1PrApRegMinSaud: TWideStringField
      FieldName = 'RegMinSaud'
      Origin = 'grag1prap.RegMinSaud'
      Size = 60
    end
    object QrGraG1PrApMarca: TIntegerField
      FieldName = 'Marca'
      Origin = 'grag1prap.Marca'
    end
    object QrGraG1PrApimpinOrca: TSmallintField
      FieldName = 'impinOrca'
      Origin = 'grag1prap.ImpInOrca'
    end
    object QrGraG1PrApNO_MARCA: TWideStringField
      FieldName = 'NO_MARCA'
      Origin = 'grafabmar.Nome'
      Size = 60
    end
    object QrGraG1PrApNO_FABR: TWideStringField
      FieldName = 'NO_FABR'
      Origin = 'grafabcad.Nome'
      Size = 60
    end
    object QrGraG1PrApCPL_EXISTE: TLargeintField
      FieldName = 'CPL_EXISTE'
      Required = True
    end
    object QrGraG1PrApObservacao: TWideMemoField
      FieldName = 'Observacao'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsGraG1PrAp: TDataSource
    DataSet = QrGraG1PrAp
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
  object QrMarcas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gfm.Controle, CONCAT(gfm.Nome, " [", '
      'gfc.Nome, "]") NO_MARCA_FABR'
      'FROM grafabmar gfm '
      'LEFT JOIN grafabcad gfc ON gfm.Codigo=gfc.Codigo'
      'ORDER BY NO_MARCA_FABR')
    Left = 152
    Top = 64
    object QrMarcasControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMarcasNO_MARCA_FABR: TWideStringField
      FieldName = 'NO_MARCA_FABR'
      Size = 123
    end
  end
  object DsMarcas: TDataSource
    DataSet = QrMarcas
    Left = 180
    Top = 64
  end
end
