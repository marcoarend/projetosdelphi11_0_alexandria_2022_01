unit CtrTagNivX;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, dmkMemo;

type
  TFmCtrTagNivX = class(TForm)
    Panel1: TPanel;
    GroupBox1: TGroupBox;
    EdNivelAcima: TdmkEdit;
    Label5: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Panel3: TPanel;
    Label6: TLabel;
    EdCodigo: TdmkEdit;
    Label7: TLabel;
    EdNome: TdmkEdit;
    Label2: TLabel;
    EdTag: TdmkEdit;
    MeConteudo: TdmkMemo;
    EdNivSup: TdmkEdit;
    EdSupNome: TdmkEdit;
    EdOrdem: TdmkEdit;
    Label1: TLabel;
    CkContinuar: TCheckBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCtrTagNivX(Codigo: Integer);
  public
    { Public declarations }
    FQrIts: TmySQLQuery;
  end;

  var
  FmCtrTagNivX: TFmCtrTagNivX;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck;

{$R *.DFM}

procedure TFmCtrTagNivX.BtOKClick(Sender: TObject);
var
  Tabela, Nome, Tag, Conteudo: String;
  Nivel, Codigo, NivSup, NivelAcima, Ordem: Integer;
begin
  NivelAcima     := EdNivelAcima.ValueVariant;
  NivSup         := EdNivSup.ValueVariant;
  Codigo         := EdCodigo.ValueVariant;
  Nome           := EdNome.Text;
  Tag            := EdTag.Text;
  Conteudo       := MeConteudo.Text;
  Ordem          := EdOrdem.ValueVariant;

  //
  Nivel := NivelAcima -1;
  Tabela := 'ctrtagniv' + Geral.FF0(Nivel);
  if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  { Nada a ver
  if MyObjects.FIC(Trim(EdTag.Text) = '', EdTag, 'Informe uma tag!') then
    Exit;
  }
  Codigo := EdCodigo.ValueVariant;
  Codigo := UMyMod.BPGS1I32(Tabela, 'Codigo', '', '', tsPos, ImgTipo.SQLType, Codigo);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, Tabela, False, [
  'NivSup', 'Nome', 'Tag',
  'Conteudo', 'Ordem'], [
  'Codigo'], [
  NivSup, Nome, Tag,
  Conteudo, Ordem], [
  Codigo], True) then
  begin
    ReopenCtrTagNivX(Codigo);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType          := stIns;
      EdCodigo.ValueVariant    := 0;
      EdOrdem.ValueVariant     := EdOrdem.ValueVariant + 1;
      EdNome.ValueVariant      := '';
      EdTag.ValueVariant       := '';
      MeConteudo.Text          := '';
      EdNome.SetFocus;
      //
    end else Close;
  end;
end;

procedure TFmCtrTagNivX.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCtrTagNivX.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCtrTagNivX.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCtrTagNivX.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCtrTagNivX.ReopenCtrTagNivX(Codigo: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger := EdNivSup.ValueVariant;
    FQrIts.Open;
    //
    if Codigo <> 0 then
      FQrIts.Locate('Codigo', Codigo, []);
  end;
end;

end.
