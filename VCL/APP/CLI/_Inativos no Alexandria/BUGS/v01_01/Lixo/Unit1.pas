unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ActiveX, ComObj;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Button1: TButton;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    PDFCreator, MSWord: OleVariant;

    optUseAutosave, optUseAutoSaveDirectory, optAutosaveFormat, 
optPDFColorsColorModel: Integer;
    optAutosaveDirectory, optAutosaveFilename: String;

    // Security Options
    optPDFUseSecurity, optPDFHighEncryption, optPDFOwnerPass: Integer;
    optPDFDisallowCopy, optPDFDisallowModifyAnnotations, 
optPDFDisallowModifyContents, optPDFDisallowPrinting: Integer;
    optPDFAllowAssembly, optPDFAllowDegradedPrinting, optPDFAllowFillIn, 
optPDFAllowScreenReaders: Integer;
    optPDFOwnerPasswordString: String;

    procedure StorePDFOptions;
    procedure SetPDFOptions(const filename, ownerpass: String);
    procedure RestorePDFOptions;
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
const
   output_filename: String = 'C:\Teste\Teste002.pdf';
   owner_password: String = 'ownerpass';
var
   c, printJobCount: Integer;
   optPrintBackground: Boolean;
begin
   PDFCreator := CreateOLEObject('PDFCreator.clsPDFCreator');

   PDFCreator.cStart('/NoProcessingAtStartup');

   StorePDFOptions;
   SetPDFOptions(output_filename, owner_password);

   MSWord := CreateOleObject('Word.Application');
   try
      MSWord.DisplayAlerts := 0;

      MSword.Documents.Open(Edit1.Text);

      MSWord.ActiveDocument.TrackRevisions := false;
      optPrintBackground := MSWord.Options.PrintBackground;
      if not optPrintBackground then
         MSWord.Options.PrintBackground := True;
      MSWord.ActiveWindow.View.ShowRevisionsAndComments := False;
      MSWord.Options.WarnBeforeSavingPrintingSendingMarkup := false;

      printJobCount := PDFCreator.cCountOfPrintJobs;
      /// ------------------------------------------
      ///   Have PDFCreator wait for pages
      PDFCreator.cPrinterStop := true;

      MSWord.ActiveDocument.PrintoutOld;

      ///   we don't want to continue on right after telling word to print, because it can cause problems
      while MSWord.BackgroundPrintingStatus <> 0 do
      begin
         Sleep(10);
         Application.ProcessMessages;
      end;

      /// Tell PDFCreator we're done printing pages.
      PDFCreator.cPrinterStop := false;
      /// ------------------------------------------

      ///   now wait for PDFCreator to finish
      c := PDFCreator.cCountOfPrintJobs;
      while c > printJobCount do
      begin
         Sleep(10);
         Application.ProcessMessages;
         c := PDFCreator.cCountOfPrintJobs;
      end;

      MSword.ActiveDocument.Close(SaveChanges := 0);
   finally
      VarClear(MSWord);
   end;

   RestorePDFOptions;
   VarClear(PDFCreator);
end;

procedure TForm1.StorePDFOptions;
begin
   // save the current options to put back when we're finished.
   optUseAutosave := PDFCreator.cOption['UseAutosave'];
   optUseAutosaveDirectory := PDFCreator.cOption['UseAutosaveDirectory'];
   optAutosaveDirectory := PDFCreator.cOption['AutosaveDirectory'];
   optAutosaveFilename := PDFCreator.cOption['AutosaveFilename'];
   optAutosaveFormat := PDFCreator.cOption['AutosaveFormat'];
   optPDFColorsColorModel := PDFCreator.cOption['PDFColorsColorModel'];

   // security options
   optPDFUseSecurity := PDFCreator.cOption['PDFUseSecurity'];
   optPDFHighEncryption := PDFCreator.cOption['PDFHighEncryption'];
   optPDFOwnerPass := PDFCreator.cOption['PDFOwnerPass'];
   optPDFOwnerPasswordString := 
PDFCreator.cOption['PDFOwnerPasswordString'];
   optPDFDisallowCopy := PDFCreator.cOption['PDFDisallowCopy'];
   optPDFDisallowModifyAnnotations := 
PDFCreator.cOption['PDFDisallowModifyAnnotations'];
   optPDFDisallowModifyContents := 
PDFCreator.cOption['PDFDisallowModifyContents'];
   optPDFDisallowPrinting := PDFCreator.cOption['PDFDisallowPrinting'];
   optPDFAllowAssembly := PDFCreator.cOption['PDFAllowAssembly'];
   optPDFAllowDegradedPrinting := 
PDFCreator.cOption['PDFAllowDegradedPrinting'];
   optPDFAllowFillIn := PDFCreator.cOption['PDFAllowFillIn'];
   optPDFAllowScreenReaders := PDFCreator.cOption['PDFAllowScreenReaders'];
end;

procedure TForm1.SetPDFOptions(const filename, ownerpass: String);
begin
   // set the options we want, auto-save PDF with specific filename
   PDFCreator.cOption('UseAutosave') := 1;
   PDFCreator.cOption('UseAutosaveDirectory') := 1;
   PDFCreator.cOption('AutosaveDirectory') := ExtractFilePath(filename);
   PDFCreator.cOption('AutosaveFilename') := ExtractFileName(filename);
   PDFCreator.cOption('AutosaveFormat') := 0; // PDF format
   PDFCreator.cOption('PDFColorsColorModel') := 0; // RGB format

   if Trim(ownerpass) <> '' then
   begin
      PDFCreator.cOption('PDFUseSecurity') := 1;
      PDFCreator.cOption('PDFHighEncryption') := 1;
      PDFCreator.cOption('PDFOwnerPass') := 1;
      PDFCreator.cOption('PDFOwnerPasswordString') := ownerpass;
      PDFCreator.cOption('PDFDisallowCopy') := 0;
      PDFCreator.cOption('PDFDisallowModifyAnnotations') := 1;
      PDFCreator.cOption('PDFDisallowModifyContents') := 1;
      PDFCreator.cOption('PDFDisallowPrinting') := 0;
      PDFCreator.cOption('PDFAllowAssembly') := 0;
      PDFCreator.cOption('PDFAllowDegradedPrinting') := 0;
      PDFCreator.cOption('PDFAllowFillIn') := 0;
      PDFCreator.cOption('PDFAllowScreenReaders') := 0;
   end;
   PDFCreator.cSaveOptions;
end;

procedure TForm1.RestorePDFOptions;
begin
   // reset user's options
   PDFCreator.cOption('UseAutosave') := optUseAutosave;
   PDFCreator.cOption('UseAutosaveDirectory') := optUseAutosaveDirectory;
   PDFCreator.cOption('AutosaveDirectory') := optAutosaveDirectory;
   PDFCreator.cOption('AutosaveFilename') := optAutosaveFilename;
   PDFCreator.cOption('AutosaveFormat') := optAutosaveFormat;
   PDFCreator.cOption('PDFColorsColorModel') := optPDFColorsColorModel;

   // security options
   PDFCreator.cOption('PDFUseSecurity') := optPDFUseSecurity;
   PDFCreator.cOption('PDFHighEncryption') := optPDFHighEncryption;
   PDFCreator.cOption('PDFOwnerPass') := optPDFOwnerPass;
   PDFCreator.cOption('PDFOwnerPasswordString') := 
optPDFOwnerPasswordString;
   PDFCreator.cOption('PDFDisallowCopy') := optPDFDisallowCopy;
   PDFCreator.cOption('PDFDisallowModifyAnnotations') := 
optPDFDisallowModifyAnnotations;
   PDFCreator.cOption('PDFDisallowModifyContents') := 
optPDFDisallowModifyContents;
   PDFCreator.cOption('PDFDisallowPrinting') := optPDFDisallowPrinting;
   PDFCreator.cOption('PDFAllowAssembly') := optPDFAllowAssembly;
   PDFCreator.cOption('PDFAllowDegradedPrinting') := 
optPDFAllowDegradedPrinting;
   PDFCreator.cOption('PDFAllowFillIn') := optPDFAllowFillIn;
   PDFCreator.cOption('PDFAllowScreenReaders') := optPDFAllowScreenReaders;

   PDFCreator.cSaveOptions;

   Sleep(100);
end;

end.

