object FmOSMonCab: TFmOSMonCab
  Left = 339
  Top = 185
  Caption = 'GER-OSERV-008 :: Ordem de Servi'#231'o - Monitoramento'
  ClientHeight = 510
  ClientWidth = 626
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 626
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 578
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 530
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 424
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Monitoramento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 424
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Monitoramento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 424
        Height = 32
        Caption = 'Ordem de Servi'#231'o - Monitoramento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 626
    Height = 348
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 626
      Height = 348
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox2: TGroupBox
        Left = 0
        Top = 37
        Width = 626
        Height = 311
        Align = alClient
        TabOrder = 1
        object GBEdita: TGroupBox
          Left = 2
          Top = 15
          Width = 622
          Height = 238
          Align = alTop
          Color = clBtnFace
          ParentBackground = False
          ParentColor = False
          TabOrder = 0
          object Label1: TLabel
            Left = 16
            Top = 16
            Width = 14
            Height = 13
            Caption = 'ID:'
            Color = clBtnFace
            ParentColor = False
          end
          object Label9: TLabel
            Left = 16
            Top = 60
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
            Color = clBtnFace
            ParentColor = False
          end
          object SbFormulas: TSpeedButton
            Left = 588
            Top = 32
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbFormulasClick
          end
          object Label3: TLabel
            Left = 76
            Top = 16
            Width = 105
            Height = 13
            Caption = 'F'#243'rmula de refer'#234'ncia:'
          end
          object Label2: TLabel
            Left = 16
            Top = 104
            Width = 146
            Height = 13
            Caption = 'Equipamento a ser monitorado:'
          end
          object SbEquipAplic: TSpeedButton
            Left = 588
            Top = 120
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SbEquipAplicClick
          end
          object Label6: TLabel
            Left = 16
            Top = 192
            Width = 81
            Height = 13
            Caption = 'Quantidade total:'
          end
          object Label8: TLabel
            Left = 112
            Top = 192
            Width = 83
            Height = 13
            Caption = 'Quantidade QSP:'
          end
          object Label10: TLabel
            Left = 208
            Top = 192
            Width = 20
            Height = 13
            Caption = 'PMV:'
          end
          object SBPIPCad: TSpeedButton
            Left = 588
            Top = 208
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SBPIPCadClick
          end
          object Label11: TLabel
            Left = 16
            Top = 148
            Width = 88
            Height = 13
            Caption = 'Tipo de aplica'#231#227'o:'
          end
          object SBTipoAplica: TSpeedButton
            Left = 308
            Top = 164
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SBTipoAplicaClick
          end
          object Label12: TLabel
            Left = 332
            Top = 148
            Width = 117
            Height = 13
            Caption = 'Unidade de Medida [F3]:'
          end
          object SpeedButton5: TSpeedButton
            Left = 589
            Top = 163
            Width = 21
            Height = 22
            Caption = '...'
            OnClick = SpeedButton5Click
          end
          object EdConta: TdmkEdit
            Left = 16
            Top = 32
            Width = 56
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Conta'
            UpdCampo = 'Conta'
            UpdType = utInc
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdNome: TdmkEdit
            Left = 16
            Top = 76
            Width = 593
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            QryCampo = 'Nome'
            UpdCampo = 'Nome'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object CBFormula: TdmkDBLookupComboBox
            Left = 132
            Top = 32
            Width = 456
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsFormulas
            TabOrder = 2
            dmkEditCB = EdFormula
            QryCampo = 'Formula'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdFormula: TdmkEditCB
            Left = 76
            Top = 32
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'Formula'
            UpdCampo = 'Formula'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdFormulaChange
            DBLookupComboBox = CBFormula
            IgnoraDBLookupComboBox = False
          end
          object EdEquipAplic: TdmkEditCB
            Left = 16
            Top = 120
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'EquipAplic'
            UpdCampo = 'EquipAplic'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBEquipAplic
            IgnoraDBLookupComboBox = False
          end
          object CBEquipAplic: TdmkDBLookupComboBox
            Left = 72
            Top = 120
            Width = 516
            Height = 21
            KeyField = 'Controle'
            ListField = 'Nome'
            ListSource = DsEquipAplic
            TabOrder = 5
            dmkEditCB = EdEquipAplic
            QryCampo = 'EquipAplic'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdQtdTot: TdmkEdit
            Left = 16
            Top = 208
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 11
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'QtdTot'
            UpdCampo = 'QtdTot'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdQtdQSP: TdmkEdit
            Left = 112
            Top = 208
            Width = 92
            Height = 21
            Alignment = taRightJustify
            TabOrder = 12
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            QryCampo = 'QtdQSP'
            UpdCampo = 'QtdQSP'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdPipCad: TdmkEditCB
            Left = 208
            Top = 208
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 13
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'PipCad'
            UpdCampo = 'PipCad'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBPipCad
            IgnoraDBLookupComboBox = False
          end
          object CBPipCad: TdmkDBLookupComboBox
            Left = 264
            Top = 208
            Width = 325
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsPIPs
            TabOrder = 14
            dmkEditCB = EdPipCad
            QryCampo = 'PipCad'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdTipoAplica: TdmkEditCB
            Left = 16
            Top = 164
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            QryCampo = 'TipoAplica'
            UpdCampo = 'TipoAplica'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBTipoAplica
            IgnoraDBLookupComboBox = False
          end
          object CBTipoAplica: TdmkDBLookupComboBox
            Left = 72
            Top = 164
            Width = 233
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsTipoAplica
            TabOrder = 7
            dmkEditCB = EdTipoAplica
            QryCampo = 'TipoAplica'
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdUnidMed: TdmkEditCB
            Left = 332
            Top = 164
            Width = 40
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdUnidMedChange
            DBLookupComboBox = CBUnidMed
            IgnoraDBLookupComboBox = False
          end
          object EdSigla: TdmkEdit
            Left = 372
            Top = 164
            Width = 40
            Height = 21
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnChange = EdSiglaChange
            OnExit = EdSiglaExit
          end
          object CBUnidMed: TdmkDBLookupComboBox
            Left = 412
            Top = 164
            Width = 177
            Height = 21
            KeyField = 'CodUsu'
            ListField = 'Nome'
            ListSource = DsUnidMed
            TabOrder = 10
            dmkEditCB = EdUnidMed
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object RGDiluente: TdmkRadioGroup
          Left = 2
          Top = 253
          Width = 622
          Height = 48
          Align = alTop
          Caption = ' Diluente: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            'Indefinido'
            'N'#227'o tem'
            #193'gua'
            'Produto indicado')
          TabOrder = 1
          QryCampo = 'Diluente'
          UpdCampo = 'Diluente'
          UpdType = utYes
          OldValor = 0
        end
      end
      object GBDados: TGroupBox
        Left = 0
        Top = 0
        Width = 626
        Height = 37
        Align = alTop
        Color = clBtnFace
        ParentBackground = False
        ParentColor = False
        TabOrder = 0
        object Label7: TLabel
          Left = 8
          Top = 12
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label4: TLabel
          Left = 232
          Top = 12
          Width = 39
          Height = 13
          Caption = 'Servi'#231'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object Label5: TLabel
          Left = 108
          Top = 12
          Width = 53
          Height = 13
          Caption = 'ID Servi'#231'o:'
          Color = clBtnFace
          ParentColor = False
        end
        object DBEdit6: TDBEdit
          Left = 276
          Top = 8
          Width = 32
          Height = 21
          TabStop = False
          DataField = 'DesServico'
          DataSource = DsOSSrv
          TabOrder = 1
        end
        object DBEdit5: TDBEdit
          Left = 308
          Top = 8
          Width = 272
          Height = 21
          TabStop = False
          DataField = 'NO_DesServico'
          DataSource = DsOSSrv
          TabOrder = 2
        end
        object DBEdit8: TDBEdit
          Left = 164
          Top = 8
          Width = 64
          Height = 21
          TabStop = False
          DataField = 'Controle'
          DataSource = DsOSSrv
          TabOrder = 3
        end
        object DBEdit15: TDBEdit
          Left = 48
          Top = 8
          Width = 56
          Height = 21
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsOSSrv
          TabOrder = 0
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 396
    Width = 626
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 622
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 440
    Width = 626
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 480
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 478
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
    end
  end
  object QrOSSrv: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT des.Nome NO_DesServico, srv.* '
      'FROM ossrv srv'
      'LEFT JOIN desservico des ON des.Codigo=srv.DesServico'
      'WHERE srv.Codigo=:P0')
    Left = 16
    Top = 80
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOSSrvNO_DesServico: TWideStringField
      FieldName = 'NO_DesServico'
      Size = 60
    end
    object QrOSSrvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOSSrvControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrOSSrvDesServico: TIntegerField
      FieldName = 'DesServico'
    end
    object QrOSSrvLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOSSrvDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOSSrvDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOSSrvUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOSSrvUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOSSrvAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOSSrvAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsOSSrv: TDataSource
    DataSet = QrOSSrv
    Left = 44
    Top = 80
  end
  object QrFormulas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT frm.*, med.CodUsu CU_UNIDMED, med.SIGLA'
      'FROM formulas frm'
      'LEFT JOIN unidmed med ON med.Codigo=frm.UnidMed'
      'WHERE frm.Aplicacao & 2'
      'ORDER BY frm.Nome')
    Left = 464
    Top = 12
    object QrFormulasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFormulasNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
    object QrFormulasEquipAplic: TIntegerField
      FieldName = 'EquipAplic'
    end
    object QrFormulasQtdTot: TFloatField
      FieldName = 'QtdTot'
    end
    object QrFormulasQtdQSP: TFloatField
      FieldName = 'QtdQSP'
    end
    object QrFormulasCusTot: TFloatField
      FieldName = 'CusTot'
    end
    object QrFormulasAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
    object QrFormulasDiluente: TSmallintField
      FieldName = 'Diluente'
    end
    object QrFormulasTipoAplica: TIntegerField
      FieldName = 'TipoAplica'
    end
    object QrFormulasCU_UNIDMED: TIntegerField
      FieldName = 'CU_UNIDMED'
      Required = True
    end
    object QrFormulasSIGLA: TWideStringField
      FieldName = 'SIGLA'
      Size = 6
    end
  end
  object DsFormulas: TDataSource
    DataSet = QrFormulas
    Left = 492
    Top = 12
  end
  object QrEquipAplic: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM equiaplic'
      'ORDER BY Nome')
    Left = 520
    Top = 12
    object QrEquipAplicControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEquipAplicNome: TWideStringField
      FieldName = 'Nome'
      Size = 120
    end
  end
  object DsEquipAplic: TDataSource
    DataSet = QrEquipAplic
    Left = 548
    Top = 12
  end
  object QrPIPs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM pipcad'
      'WHERE OSMonCab=0'
      'AND MotDesativ=0'
      'AND (DtaDesativ < "1900-01-01" '
      '  OR DtaDesativ IS NULL)'
      'ORDER BY Nome')
    Left = 244
    Top = 104
    object QrPIPsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPIPsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPIPsEquipamento: TIntegerField
      FieldName = 'Equipamento'
    end
    object QrPIPsOSMonCab: TIntegerField
      FieldName = 'OSMonCab'
    end
  end
  object DsPIPs: TDataSource
    DataSet = QrPIPs
    Left = 272
    Top = 104
  end
  object QrTipoAplica: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM tipoaplica'
      'ORDER BY Nome')
    Left = 464
    Top = 40
    object QrTipoAplicaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTipoAplicaNome: TWideStringField
      FieldName = 'Nome'
      Size = 60
    end
  end
  object DsTipoAplica: TDataSource
    DataSet = QrTipoAplica
    Left = 492
    Top = 40
  end
  object QrUnidMed: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Sigla, Nome'
      'FROM unidmed '
      'ORDER BY Nome')
    Left = 72
    Top = 80
    object QrUnidMedCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUnidMedCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrUnidMedSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 3
    end
    object QrUnidMedNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsUnidMed: TDataSource
    DataSet = QrUnidMed
    Left = 100
    Top = 80
  end
end
