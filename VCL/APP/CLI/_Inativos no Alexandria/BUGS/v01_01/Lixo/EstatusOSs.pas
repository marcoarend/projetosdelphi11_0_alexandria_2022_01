unit EstatusOSs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, DmkDAC_PF,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, Clipbrd, Variants, dmkDBGridZTO, dmkRadioGroup,
  UnDmkListas;

type
  TFmEstatusOSs = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    TbEstatusOSs: TmySQLTable;
    DsEstatusOSs: TDataSource;
    TbEstatusOSsCodigo: TIntegerField;
    TbEstatusOSsNome: TWideStringField;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqNome: TWideStringField;
    PnDados: TPanel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    DBGrid1: TdmkDBGridZTO;
    Panel5: TPanel;
    Panel6: TPanel;
    Label1: TLabel;
    EdPesq: TEdit;
    TBTam: TTrackBar;
    DBGrid2: TDBGrid;
    LbItensMD: TListBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PnEdita: TPanel;
    GBEdita: TGroupBox;
    Label7: TLabel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel8: TPanel;
    BtDesiste: TBitBtn;
    TbEstatusOSsOrdem: TIntegerField;
    TbEstatusOSsAgeCorIni: TIntegerField;
    TbEstatusOSsAgeCorFim: TIntegerField;
    TbEstatusOSsAgeCorDir: TSmallintField;
    TbEstatusOSsAgeCorFon: TIntegerField;
    TbEstatusOSsAgeCorHin: TIntegerField;
    GroupBox14: TGroupBox;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    CBAgeCorIni: TColorBox;
    CBAgeCorFim: TColorBox;
    CBAgeCorFon: TColorBox;
    CBAgeCorHin: TColorBox;
    RGAgeCorDir: TdmkRadioGroup;
    EdOrdem: TdmkEdit;
    Label2: TLabel;
    TbEstatusOSsTXT_AgeCorDir: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure TbEstatusOSsBeforePost(DataSet: TDataSet);
    procedure TbEstatusOSsDeleting(Sender: TObject; var Allow: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure TBTamChange(Sender: TObject);
    procedure TbEstatusOSsAfterPost(DataSet: TDataSet);
    procedure LbItensMDDblClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure TbEstatusOSsAfterInsert(DataSet: TDataSet);
    procedure TbEstatusOSsNewRecord(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure TbEstatusOSsCalcFields(DataSet: TDataSet);
    procedure DBGrid1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    FPermiteExcluir: Boolean;
    FNovoCodigo: TNovoCodigo;
    FReservados: Variant;
    FSoReserva: Boolean;
    FFldID: String;
    FFldNome: String;
    FDBGInsert: Boolean;
    FModoInclusao: TdmkModoInclusao;
    //
    procedure ReabrePesquisa();
  end;

  var
  FmEstatusOSs: TFmEstatusOSs;

implementation

uses UnMyObjects, Module, UMySQLModule, CfgCadLista;

{$R *.DFM}

procedure TFmEstatusOSs.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, TmySQLQuery(TbEstatusOSs), [PnDados],
  [PnEdita], EdNome, ImgTipo, TbEstatusOSs.TableName);
  //
  CBAgeCorIni.Selected := TBEstatusOSsAgeCorIni.Value;
  CBAgeCorFim.Selected := TBEstatusOSsAgeCorFim.Value;
  CBAgeCorFon.Selected := TBEstatusOSsAgeCorFon.Value;
  CBAgeCorHin.Selected := TBEstatusOSsAgeCorHin.Value;
end;

procedure TFmEstatusOSs.BtConfirmaClick(Sender: TObject);
var
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  if UnCfgCadLista.PoeTbEmEdicao(TbEstatusOSs, ImgTipo.SQLType) then
  begin
    TbEstatusOSsNome.Value := EdNome.Text;
    TbEstatusOSsOrdem.Value := EdOrdem.ValueVariant;
    //
    TbEstatusOSsAgeCorIni.Value := CBAgeCorIni.Selected;
    TbEstatusOSsAgeCorFim.Value := CBAgeCorFim.Selected;
    TbEstatusOSsAgeCorFon.Value := CBAgeCorFon.Selected;
    TbEstatusOSsAgeCorHin.Value := CBAgeCorHin.Selected;
    //
    TbEstatusOSsAgeCorDir.Value := RGAgeCorDir.ItemIndex;
    //
    ImgTipo.SQLType := stLok;
    PnDados.Visible := True;
    PnEdita.Visible := False;
    TbEstatusOSs.Refresh;
  end;
end;

procedure TFmEstatusOSs.BtDesisteClick(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
end;

procedure TFmEstatusOSs.BtIncluiClick(Sender: TObject);
begin
  UnCfgCadLista.IncluiNovoRegistro(FModoInclusao, Self, PnEdita, [PnDados],
  [PnEdita], EdNome, ImgTipo, FReservados, TbEstatusOSs, FFldID);
  EdOrdem.ValueVariant := 999999999;
end;

procedure TFmEstatusOSs.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEstatusOSs.DBGrid1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
begin
  if Column.FieldName = 'AgeCorIni' then Cor := TbEstatusOSsAgeCorIni.Value else
  if Column.FieldName = 'AgeCorFim' then Cor := TbEstatusOSsAgeCorFim.Value else
  if Column.FieldName = 'AgeCorFon' then Cor := TbEstatusOSsAgeCorFon.Value else
  if Column.FieldName = 'AgeCorHin' then Cor := TbEstatusOSsAgeCorHin.Value else
  Exit;
  //
  MyObjects.DesenhaTextoEmDBGrid(
    TDbGrid(DBGrid1), Rect, Cor, Cor, Column.Alignment, '');
end;

procedure TFmEstatusOSs.DBGrid2DblClick(Sender: TObject);
begin
  UnCfgCadLista.LocalizaPesquisadoInt1(TbEstatusOSs, QrPesq, FFldID, True);
end;

procedure TFmEstatusOSs.EdPesqChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

procedure TFmEstatusOSs.FormActivate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  MyObjects.CorIniComponente();
  BtInclui.Visible := FReservados <> Null;
end;

procedure TFmEstatusOSs.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Dmod.PoeEmMemoryCoresStatusOS();
end;

procedure TFmEstatusOSs.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PnDados.Align := alClient;
  FNovoCodigo := ncGerlSeq1;
  FReservados := 0;
  FSoReserva := False;
  FPermiteExcluir := False;
  FModoInclusao := dmkmiUserDefine;
  FFldID := CO_CODIGO;
  FFldNome := CO_NOME;
  //
  TbEstatusOSs.TableName := 'estatusoss';
end;

procedure TFmEstatusOSs.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEstatusOSs.FormShow(Sender: TObject);
begin
  TbEstatusOSs.Open;
end;

procedure TFmEstatusOSs.LbItensMDDblClick(Sender: TObject);
begin
  UnCfgCadLista.AddVariavelEmTexto(LbItensMD, TbEstatusOSs, FFldNome, DBGrid1);
end;

procedure TFmEstatusOSs.ReabrePesquisa();
begin
  UnCfgCadLista.ReabrePesquisa(
    QrPesq, TbEstatusOSs, EdPesq, TBTam, FFldID, FFldNome);
end;

procedure TFmEstatusOSs.TbEstatusOSsAfterInsert(DataSet: TDataSet);
begin
 if FDBGInsert = false then
   TbEstatusOSs.Cancel;
end;

procedure TFmEstatusOSs.TbEstatusOSsAfterPost(DataSet: TDataSet);
begin
  VAR_CADASTRO := TbEstatusOSsCodigo.Value;
end;

procedure TFmEstatusOSs.TbEstatusOSsBeforePost(DataSet: TDataSet);
begin
  UnCfgCadLista.TbBeforePost(TbEstatusOSs, FNovoCodigo, FFldID, FReservados);
{
  if TbEstatusOSs.State = dsInsert then
  begin
    case FNovoCodigo of
      ncControle: TbEstatusOSsCodigo.Value := UMyMod.BuscaNovoCodigo_Int(
        Dmod.QrAux, TbEstatusOSs.TableName, FFldID, [], [], stIns, 0, siPositivo, nil);
      ncGerlSeq1: TbEstatusOSsCodigo.Value := UMyMod.BuscaProximoGerlSeq1Int32(
        TbEstatusOSs.TableName, FFldID, '', '', tsDef, stIns, 0, FReservados);
      else (*ncIdefinido: CtrlGeral: ;*)
      begin
        Geral.MensagemBox('Tipo de obten��o de novo c�digo indefinido!',
        'ERRO', MB_OK+MB_ICONERROR);
        Halt(0);
        Exit;
      end;
    end;
  end;
}
end;

procedure TFmEstatusOSs.TbEstatusOSsCalcFields(DataSet: TDataSet);
begin
  TbEstatusOSsTXT_AgeCorDir.Value :=
    sPlannerGradientDirection[TbEstatusOSsAgeCorDir.Value];
end;

procedure TFmEstatusOSs.TbEstatusOSsDeleting(Sender: TObject; var Allow: Boolean);
begin
  if FPermiteExcluir then
  begin
   Allow := Geral.MB_Pergunta(
   'Confirma a exclus�o do registro selecionado?') = ID_YES;
  end
  else
    Allow := UMyMod.NaoPermiteExcluirDeTable();
end;

procedure TFmEstatusOSs.TbEstatusOSsNewRecord(DataSet: TDataSet);
begin
  if FDBGInsert = False then
    TbEstatusOSs.Cancel;
end;

procedure TFmEstatusOSs.TBTamChange(Sender: TObject);
begin
  ReabrePesquisa();
end;

end.
