unit CunsImgCmt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, DB, mySQLDbTables, DBCtrls, dmkLabel, Mask,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkDBEdit, dmkGeral, Variants,
  dmkValUsu, dmkImage, UnDmkEnums;

type
  TFmCunsImgCmt = class(TForm)
    Panel1: TPanel;
    CkContinuar: TCheckBox;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    DBEdCodigo: TDBEdit;
    Label3: TLabel;
    GroupBox2: TGroupBox;
    EdConta: TdmkEdit;
    Label6: TLabel;
    EdNome: TdmkEdit;
    Label7: TLabel;
    CBTxtGeneric: TdmkDBLookupComboBox;
    EdTxtGeneric: TdmkEditCB;
    Label1: TLabel;
    SbTxtGeneric: TSpeedButton;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    QrTxtGeneric: TmySQLQuery;
    DsTxtGeneric: TDataSource;
    VU_Sel_: TdmkValUsu;
    QrTxtGenericCodigo: TIntegerField;
    QrTxtGenericNome: TWideStringField;
    DBEdControle: TDBEdit;
    Label2: TLabel;
    DBEdNome: TDBEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure SbTxtGenericClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCadastro_Com_Itens_ITS(Controle: Integer);
  public
    { Public declarations }
    FQrCab, FQrIts: TmySQLQuery;
    FDsCab: TDataSource;
  end;

  var
  FmCunsImgCmt: TFmCunsImgCmt;

implementation

uses UnMyObjects, Module, UMySQLModule, UnInternalConsts, MyDBCheck, DmkDAC_PF,
AppListas, CfgCadLista, Principal;

{$R *.DFM}

procedure TFmCunsImgCmt.BtOKClick(Sender: TObject);
var
  Codigo, Controle, Conta, Status, TxtGeneric: Integer;
  Nome: String;
begin
  if MyObjects.FIC(Trim(EdNome.Text) = '', EdNome, 'Informe uma descri��o!') then
    Exit;
  Nome       := EdNome.Text;
  TxtGeneric := EdTxtGeneric.ValueVariant;
  TxtGeneric := EdTxtGeneric.ValueVariant;
  //
  Codigo     := Geral.IMV(DBEdCodigo.Text);
  Controle   := Geral.IMV(DBEdControle.Text);
  Conta := EdConta.ValueVariant;
  Conta := UMyMod.BPGS1I32('cunsimgcmt', 'Conta', '', '', tsPos, ImgTipo.SQLType, Conta);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, 'cunsimgcmt', False, [
  'Codigo', 'Controle', 'TxtGeneric',
  'Nome'], [
  'Conta'], [
  Codigo, Controle, TxtGeneric,
  Nome], [
  Conta], True) then
  begin
    ReopenCadastro_Com_Itens_ITS(Controle);
    if CkContinuar.Checked then
    begin
      ImgTipo.SQLType           := stIns;
      EdConta.ValueVariant      := 0;
      EdTxtGeneric.ValueVariant := 0;
      CBTxtGeneric.KeyValue     := Null;
      EdNome.ValueVariant       := '';
      EdNome.SetFocus;
    end else Close;
  end;
end;

procedure TFmCunsImgCmt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCunsImgCmt.FormActivate(Sender: TObject);
begin
  DBEdCodigo.DataSource   := FDsCab;
  DBEdControle.DataSource := FDsCab;
  DBEdNome.DataSource     := FDsCab;
  MyObjects.CorIniComponente();
end;

procedure TFmCunsImgCmt.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  UnDmkDAC_PF.AbreQuery(QrTxtGeneric, Dmod.MyDB);
end;

procedure TFmCunsImgCmt.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCunsImgCmt.ReopenCadastro_Com_Itens_ITS(Controle: Integer);
begin
  if FQrIts <> nil then
  begin
    FQrIts.Close;
    if FQrIts.ParamCount > 0 then
      FQrIts.Params[0].AsInteger :=
      FQrCab.FieldByName('Codigo').AsInteger;
    FQrIts.Open;
    //
    if Controle <> 0 then
      FQrIts.Locate('Controle', Controle, []);
  end;
end;

procedure TFmCunsImgCmt.SbTxtGenericClick(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.MostraFormTxtGeneric();
  //
  UMyMod.SetaCodigoPesquisado(
    EdTxtGeneric, CBTxtGeneric, QrTxtGeneric, VAR_CADASTRO);
end;

end.
