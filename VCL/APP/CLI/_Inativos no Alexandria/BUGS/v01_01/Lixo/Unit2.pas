unit Unit2;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs,
  //
  StdCtrls, ComCtrls, ExtCtrls;

type
  THackTabControl = class(TCustomTabControl);
  TForm2 = class(TForm)
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    Label1: TLabel;
    Edit1: TEdit;
    Label2: TLabel;
    Edit2: TEdit;
    Button1: TButton;
    procedure PageControl1DrawTab(Control: TCustomTabControl; TabIndex: Integer;
      const Rect: TRect; Active: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
    //FOldPageCOntrolWndProc: TWndMethod;
    //procedure PageControlWndProc( Var Msg: TMessage );

  public
    { Public declarations }
  end;

var
  Form2: TForm2;

implementation

uses Unit3;

{$R *.dfm}

procedure TForm2.Button1Click(Sender: TObject);
var
  aForm: TForm3;
  ts :TTabSheet;
begin
  PageControl1.OwnerDraw := True;
  //
  ts := TTabSheet.Create(PageControl1);
  ts.PageControl := PageControl1;
  aForm := TForm3.Create(self);
  aForm.Parent := ts;
  aForm.Align := alClient;
  aForm.BorderStyle := bsNone;
  aForm.Show;
end;

procedure TForm2.FormCreate(Sender: TObject);
begin
{
  FoldPageControlWndProc := Pagecontrol1.WindowProc;
  Pagecontrol1.WindowProc := PageControlWndProc;
}
end;

procedure TForm2.PageControl1DrawTab(Control: TCustomTabControl;
  TabIndex: Integer; const Rect: TRect; Active: Boolean);
{
var
   iImage:  Integer;
   TabRect: TRect;
begin
     with Canvas do
     begin
          TabRect:= Rect;
          Canvas.Brush.Color := clBtnFace;
          Canvas.Font.Color := clBtnText;

          Canvas.FillRect(TabRect);
          iImage := GetImageIndex(TabIndex);
          If (iImage >=0) and Assigned(Images) Then
          begin
               SaveDC(Canvas.Handle);
               Images.Draw(Canvas, Rect.Left+4, 
                           Rect.Top+2, iImage, 
                           Pages[TabIndex].Enabled);
               RestoreDC(Canvas.Handle, -1);
               TabRect.Left := TabRect.Left+Images.Width+4;
          End;
          InflateRect(TabRect,-2,-2);
          DrawFocusRect(Rect);

          if Active and bTabBold Then
             Font.Style := [fsBold]
          else
              Font.Style := [];

          TextRect(TabRect, TabRect.Left+(TabRect.Right-
                   TabRect.Left) div 2 - (TextWidth(
                   Trim(Pages[TabIndex].Caption))) div 2, 
                   TabRect.Top, Pages[TabIndex].Caption);
     end;}
var
  R: TRect;
  Texto: String;
begin
  Texto := THackTabControl(Control).Tabs[TabIndex];
  Label1.Caption := Texto;
  Label2.Caption := Texto;
  //
  Edit1.Text := Texto;
  Edit2.Text := Texto;
  //...
  R := Rect;
  R.Top := R.Top+4;
  Control.Canvas.Font.Color := clBlue;
  Control.Canvas.TextRect(Rect, Rect.Left+4, Rect.Top+4,
                          (*PageControl1.Pages[TabIndex].Caption*) 'Teste');
end;

{
procedure TForm2.PageControlWndProc(var Msg: TMessage);
var
  canvas: TCanvas;
  x, y: Integer;
  r: TRect;
  Background: TBitmap;
begin
  if msg.Msg = WM_ERASEBKGND then
  begin
    msg.Result := 1;
    r:= pagecontrol1.clientrect;
    canvas:= TCanvas.Create;
    try
      canvas.handle := HDC(msg.wparam);
      y := 0;
      while y < r.Bottom do
      begin
        x:= 0;
        while x < r.right do
        begin
          canvas.Draw( x, y, Background );
          Inc( x, Background.Width );
        End;
        Inc( y, Background.Height );
      end;
    finally
      canvas.handle := 0;
      canvas.free;
    end;
  end
  else
    FOldPageControlWndProc( Msg );
end;
}

end.
