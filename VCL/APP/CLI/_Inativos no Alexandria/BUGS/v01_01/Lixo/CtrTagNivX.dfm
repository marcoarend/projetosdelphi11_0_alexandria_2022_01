object FmCtrTagNivX: TFmCtrTagNivX
  Left = 339
  Top = 185
  Caption = 'TAG-CONTR-999 :: N'#237'vel X de Tags de Contrato'
  ClientHeight = 442
  ClientWidth = 624
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 112
    Width = 624
    Height = 216
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 624
    Height = 64
    Align = alTop
    Caption = ' Dados do N'#205'VEL SUPERIOR:'
    Enabled = False
    TabOrder = 0
    object Label5: TLabel
      Left = 12
      Top = 20
      Width = 29
      Height = 13
      Caption = 'N'#237'vel:'
    end
    object Label4: TLabel
      Left = 72
      Top = 20
      Width = 41
      Height = 13
      Caption = 'ID Nivel:'
    end
    object Label3: TLabel
      Left = 156
      Top = 20
      Width = 133
      Height = 13
      Caption = 'Descri'#231#227'o do n'#237'vel superior:'
    end
    object EdNivelAcima: TdmkEdit
      Left = 12
      Top = 36
      Width = 56
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdNivSup: TdmkEdit
      Left = 72
      Top = 36
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'NivSup'
      UpdCampo = 'NivSup'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdSupNome: TdmkEdit
      Left = 156
      Top = 36
      Width = 445
      Height = 21
      TabStop = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 112
    Width = 624
    Height = 216
    Align = alClient
    Caption = ' Dados do item: '
    TabOrder = 1
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 42
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label6: TLabel
        Left = 8
        Top = 0
        Width = 14
        Height = 13
        Caption = 'ID:'
      end
      object Label7: TLabel
        Left = 92
        Top = 0
        Width = 31
        Height = 13
        Caption = 'Nome:'
      end
      object Label2: TLabel
        Left = 460
        Top = 0
        Width = 22
        Height = 13
        Caption = 'Tag:'
        Enabled = False
      end
      object Label1: TLabel
        Left = 564
        Top = 0
        Width = 34
        Height = 13
        Caption = 'Ordem:'
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 16
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 92
        Top = 16
        Width = 364
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdTag: TdmkEdit
        Left = 460
        Top = 16
        Width = 100
        Height = 21
        Enabled = False
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Tag'
        UpdCampo = 'Tag'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdOrdem: TdmkEdit
        Left = 564
        Top = 16
        Width = 36
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
    end
    object MeConteudo: TdmkMemo
      Left = 2
      Top = 57
      Width = 620
      Height = 157
      Align = alClient
      TabOrder = 1
      QryCampo = 'Conteudo'
      UpdCampo = 'Conteudo'
      UpdType = utYes
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 624
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 576
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 528
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 338
        Height = 32
        Caption = 'N'#237'vel X de Tags de Contrato'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 338
        Height = 32
        Caption = 'N'#237'vel X de Tags de Contrato'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 338
        Height = 32
        Caption = 'N'#237'vel X de Tags de Contrato'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 328
    Width = 624
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 620
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 372
    Width = 624
    Height = 70
    Align = alBottom
    TabOrder = 5
    object PnSaiDesis: TPanel
      Left = 478
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 476
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        TabOrder = 0
        OnClick = BtOKClick
        NumGlyphs = 2
      end
      object CkContinuar: TCheckBox
        Left = 144
        Top = 16
        Width = 117
        Height = 17
        Caption = 'Continuar inserindo.'
        Checked = True
        State = cbChecked
        TabOrder = 1
      end
    end
  end
end
