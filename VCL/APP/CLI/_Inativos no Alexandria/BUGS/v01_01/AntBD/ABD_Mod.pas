unit ABD_Mod;

interface

uses
  Windows, Forms, Messages, SysUtils, Classes, DB, mySQLDbTables, dmkGeral,
  UninternalConsts;

type
  TDmABD_Mod = class(TDataModule)
    MyABD: TmySQLDatabase;
    QrUpd: TmySQLQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure MyABD_Cria();
  end;

var
  DmABD_Mod: TDmABD_Mod;

implementation

uses Module, DmkDAC_PF;

{$R *.dfm}

{ TDmABD_Mod }

const
  FDatabase = 'zzz_ib_db_ant'; // Banco de dados interbase do Eslauco

procedure TDmABD_Mod.DataModuleCreate(Sender: TObject);
begin
  if MyABD.Connected then
    Geral.MB_Erro('MyABD est� connectado antes da configura��o!');
end;

procedure TDmABD_Mod.MyABD_Cria();
begin
  if Dmod.QrOpcoesBugsIdxBDAnt.Value = 1 then
  begin
{
  MyABD.Connected := False;
  //
  MyABD.Connected    := False;
  MyABD.DatabaseName := FDatabase;
  MyABD.Host         := DMod.MyDB.Host;
  MyABD.Port         := DMod.MyDB.Port;
  MyABD.UserName     := DMod.MyDB.UserName;
  MyABD.UserPassword := DMod.MyDB.UserPassword;
  try
    MyABD.Connected    := True;
  except
    Geral.MB_Erro(
    'N�o foi poss�vel conectar o "MyABD" pela configura��o do "MyDB":' +
    sLineBreak + sLineBreak+
    'Database: ' + FDatabase + sLineBreak +
    'Host: ' + DMod.MyDB.Host + sLineBreak +
    'Porta: ' + FormatFloat('0', DMod.MyDB.Port));
  end;
  //
}
    UnDmkDAC_PF.ConectaMyDB_DAC(MyABD, FDatabase,  DMod.MyDB.Host,
    DMod.MyDB.Port,  DMod.MyDB.UserName, DMod.MyDB.UserPassword,
    (*Desconecta*)True, (*Configura*)True, (*Conecta*)True);
  end;
end;

end.
