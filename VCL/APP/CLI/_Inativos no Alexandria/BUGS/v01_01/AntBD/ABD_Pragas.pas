unit ABD_Pragas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc, ComCtrls, UnDmkEnums;

type
  TFmABD_Pragas = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrABD_Pragas: TmySQLQuery;
    DsABD_Pragas: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdID: TdmkEdit;
    Label9: TLabel;
    EdDescricao: TdmkEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrABD_PragasAUTOINC: TIntegerField;
    QrABD_PragasID_PRAGA: TIntegerField;
    QrABD_PragasDESCRICAO: TWideStringField;
    QrABD_PragasNOME_TEC: TWideStringField;
    QrABD_PragasID_CLASS: TIntegerField;
    QrABD_PragasCARAC_PRAGA: TWideMemoField;
    QrABD_PragasPROC_ENV: TWideMemoField;
    QrABD_PragasPREV: TWideMemoField;
    QrABD_PragasINCIDENCIA: TIntegerField;
    QrABD_PragasVENENOSO: TIntegerField;
    QrABD_PragasIMAGEM: TBlobField;
    QrABD_PragasID_EMPRESA: TIntegerField;
    Panel6: TPanel;
    Label1: TLabel;
    DBEdID: TdmkDBEdit;
    Label2: TLabel;
    DBEdDescricao: TdmkDBEdit;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label4: TLabel;
    DBEdit2: TDBEdit;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    Label6: TLabel;
    DBEdit4: TDBEdit;
    Label8: TLabel;
    DBEdit5: TDBEdit;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    DBMemo1: TDBMemo;
    DBMemo2: TDBMemo;
    DBMemo3: TDBMemo;
    Panel7: TPanel;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrABD_PragasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrABD_PragasBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, ID: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmABD_Pragas: TFmABD_Pragas;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, ABD_Mod;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmABD_Pragas.LocCod(Atual, ID: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, ID);
end;

procedure TFmABD_Pragas.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrABD_PragasID_PRAGA.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmABD_Pragas.DefParams;
begin
  VAR_GOTOTABELA := 'pragas';
  VAR_GOTOMYSQLTABLE := QrABD_Pragas;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := 'ID_PRAGA';
  VAR_GOTONOME := 'DESCRICAO';
  VAR_GOTOMySQLDBNAME := DmABD_Mod.MyABD;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM pragas');
  VAR_SQLx.Add('WHERE ID_PRAGA > 0');
  //
  VAR_SQL1.Add('AND ID_PRAGA=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND DESCRICAO Like :P0');
  //
end;

procedure TFmABD_Pragas.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmABD_Pragas.QueryPrincipalAfterOpen;
begin
end;

procedure TFmABD_Pragas.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmABD_Pragas.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmABD_Pragas.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmABD_Pragas.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmABD_Pragas.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmABD_Pragas.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmABD_Pragas.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrABD_Pragas, [PnDados],
  [PnEdita], EdDESCRICAO, ImgTipo, 'pragas');
end;

procedure TFmABD_Pragas.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrABD_PragasID_PRAGA.Value;
  Close;
end;

procedure TFmABD_Pragas.BtConfirmaClick(Sender: TObject);
var
  ID: Integer;
  Nome: String;
begin
  Nome := EdDESCRICAO.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdDESCRICAO, 'Defina uma descrição!') then Exit;
  //
  ID := UMyMod.BPGS1I32('pragas', 'ID_PRAGA', '', '',
    tsPos, ImgTipo.SQLType, QrABD_PragasID_PRAGA.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'pragas', ID, DmABD_Mod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(ID, ID);
  end;
end;

procedure TFmABD_Pragas.BtDesisteClick(Sender: TObject);
var
  ID : Integer;
begin
  ID := EdID.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(ID, DmABD_Mod.MyABD, 'pragas', 'ID_PRAGA');
end;

procedure TFmABD_Pragas.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrABD_Pragas, [PnDados],
  [PnEdita], EdDESCRICAO, ImgTipo, 'pragas');
end;

procedure TFmABD_Pragas.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  //GBDados.Align := alClient;
  PageControl1.Align := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
end;

procedure TFmABD_Pragas.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrABD_PragasID_PRAGA.Value, LaRegistro.Caption);
end;

procedure TFmABD_Pragas.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmABD_Pragas.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrABD_PragasID_PRAGA.Value, LaRegistro.Caption);
end;

procedure TFmABD_Pragas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmABD_Pragas.QrABD_PragasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmABD_Pragas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmABD_Pragas.SbQueryClick(Sender: TObject);
begin
  LocCod(QrABD_PragasID_PRAGA.Value,
  CuringaLoc.CriaForm('ID_PRAGA', 'DESCRICAO', 'pragas', DmABD_Mod.MyABD, CO_VAZIO));
end;

procedure TFmABD_Pragas.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmABD_Pragas.QrABD_PragasBeforeOpen(DataSet: TDataSet);
begin
  QrABD_PragasID_PRAGA.DisplayFormat := FFormatFloat;
end;

end.

