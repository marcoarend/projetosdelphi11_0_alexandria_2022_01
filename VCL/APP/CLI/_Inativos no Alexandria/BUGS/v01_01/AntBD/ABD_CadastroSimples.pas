unit ABD_CadastroSimples;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkImage, dmkRadioGroup,
  unDmkProcFunc;

type
  TFmABD_CadastroSimples = class(TForm)
    PnDados: TPanel;
    PnEdita: TPanel;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    GBEdita: TGroupBox;
    GBDados: TGroupBox;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    QrABD_CadastroSimples: TmySQLQuery;
    DsABD_CadastroSimples: TDataSource;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    EdID: TdmkEdit;
    Label9: TLabel;
    EdDescricao: TdmkEdit;
    Label1: TLabel;
    DBEdID: TdmkDBEdit;
    Label2: TLabel;
    DBEdDescricao: TdmkDBEdit;
    GBConfirma: TGroupBox;
    BtConfirma: TBitBtn;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    QrABD_CadastroSimplesAUTOINC: TIntegerField;
    QrABD_CadastroSimplesID_PRAGA: TIntegerField;
    QrABD_CadastroSimplesDESCRICAO: TWideStringField;
    QrABD_CadastroSimplesNOME_TEC: TWideStringField;
    QrABD_CadastroSimplesID_CLASS: TIntegerField;
    QrABD_CadastroSimplesCARAC_PRAGA: TWideMemoField;
    QrABD_CadastroSimplesPROC_ENV: TWideMemoField;
    QrABD_CadastroSimplesPREV: TWideMemoField;
    QrABD_CadastroSimplesINCIDENCIA: TIntegerField;
    QrABD_CadastroSimplesVENENOSO: TIntegerField;
    QrABD_CadastroSimplesIMAGEM: TBlobField;
    QrABD_CadastroSimplesID_EMPRESA: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrABD_CadastroSimplesAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrABD_CadastroSimplesBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure BtSelecionaClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure DefParams;
    procedure LocCod(Atual, ID: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmABD_CadastroSimples: TFmABD_CadastroSimples;
const
  FFormatFloat = '00000';

implementation

uses UnMyObjects, ABD_Mod;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmABD_CadastroSimples.LocCod(Atual, ID: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, ID);
end;

procedure TFmABD_CadastroSimples.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrABD_PragasID_PRAGA.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmABD_CadastroSimples.DefParams;
begin
  VAR_GOTOTABELA := 'pragas';
  VAR_GOTOMYSQLTABLE := QrABD_Pragas;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := 'ID_PRAGA';
  VAR_GOTONOME := 'DESCRICAO';
  VAR_GOTOMySQLDBNAME := DmABD_Mod.MyABD;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM pragas');
  VAR_SQLx.Add('WHERE ID_PRAGA > 0');
  //
  VAR_SQL1.Add('AND ID_PRAGA=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND DESCRICAO Like :P0');
  //
end;

procedure TFmABD_CadastroSimples.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmABD_CadastroSimples.QueryPrincipalAfterOpen;
begin
end;

procedure TFmABD_CadastroSimples.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARAÇÕES///////////////////

procedure TFmABD_CadastroSimples.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmABD_CadastroSimples.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmABD_CadastroSimples.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmABD_CadastroSimples.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmABD_CadastroSimples.BtSelecionaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmABD_CadastroSimples.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PnEdita, QrABD_Pragas, [PnDados],
  [PnEdita], EdDESCRICAO, ImgTipo, 'pragas');
end;

procedure TFmABD_CadastroSimples.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrABD_PragasID_PRAGA.Value;
  Close;
end;

procedure TFmABD_CadastroSimples.BtConfirmaClick(Sender: TObject);
var
  ID: Integer;
  Nome: String;
begin
  Nome := EdDESCRICAO.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdDESCRICAO, 'Defina uma descrição!') then Exit;
  //
  ID := UMyMod.BPGS1I32('pragas', 'ID_PRAGA', '', '',
    tsPos, ImgTipo.SQLType, QrABD_PragasID_PRAGA.Value);
  if UMyMod.ExecSQLInsUpdPanel(ImgTipo.SQLType, Self, PnEdita,
    'pragas', ID, DmABD_Mod.QrUpd, [PnEdita], [PnDados], ImgTipo, True) then
  begin
    LocCod(ID, ID);
  end;
end;

procedure TFmABD_CadastroSimples.BtDesisteClick(Sender: TObject);
var
  ID : Integer;
begin
  ID := EdID.ValueVariant;
  ImgTipo.SQLType := stLok;
  PnDados.Visible := True;
  PnEdita.Visible := False;
  UMyMod.UpdUnlockY(ID, DmABD_Mod.MyABD, 'pragas', 'ID_PRAGA');
end;

procedure TFmABD_CadastroSimples.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PnEdita, QrABD_Pragas, [PnDados],
  [PnEdita], EdDESCRICAO, ImgTipo, 'pragas');
end;

procedure TFmABD_CadastroSimples.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  GBEdita.Align := alClient;
  GBDados.Align := alClient;
  CriaOForm;
end;

procedure TFmABD_CadastroSimples.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrABD_PragasID_PRAGA.Value, LaRegistro.Caption);
end;

procedure TFmABD_CadastroSimples.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmABD_CadastroSimples.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrABD_PragasID_PRAGA.Value, LaRegistro.Caption);
end;

procedure TFmABD_CadastroSimples.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmABD_CadastroSimples.QrABD_CadastroSimplesAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmABD_CadastroSimples.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmABD_CadastroSimples.SbQueryClick(Sender: TObject);
begin
  LocCod(QrABD_PragasID_PRAGA.Value,
  CuringaLoc.CriaForm('ID_PRAGA', 'DESCRICAO', 'pragas', DmABD_Mod.MyABD, CO_VAZIO));
end;

procedure TFmABD_CadastroSimples.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmABD_CadastroSimples.QrABD_CadastroSimplesBeforeOpen(DataSet: TDataSet);
begin
  QrABD_PragasID_PRAGA.DisplayFormat := FFormatFloat;
end;

end.

