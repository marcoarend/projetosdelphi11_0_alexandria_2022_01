object FmABD_Pragas: TFmABD_Pragas
  Left = 368
  Top = 194
  Caption = '???-?????-999 :: ABD - Pragas'
  ClientHeight = 679
  ClientWidth = 784
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnEdita: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 583
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    ExplicitHeight = 225
    object GBEdita: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 125
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 0
      object Label7: TLabel
        Left = 16
        Top = 16
        Width = 14
        Height = 13
        Caption = 'ID:'
        Color = clBtnFace
        ParentColor = False
      end
      object Label9: TLabel
        Left = 76
        Top = 16
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        Color = clBtnFace
        ParentColor = False
      end
      object EdID: TdmkEdit
        Left = 20
        Top = 32
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'ID_PRAGA'
        UpdCampo = 'ID_PRAGA'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdDescricao: TdmkEdit
        Left = 76
        Top = 32
        Width = 633
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'DESCRICAO'
        UpdCampo = 'DESCRICAO'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 520
      Width = 784
      Height = 63
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 162
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 674
        Top = 15
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PnDados: TPanel
    Left = 0
    Top = 96
    Width = 784
    Height = 583
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    ExplicitHeight = 225
    object GBDados: TGroupBox
      Left = 0
      Top = 0
      Width = 784
      Height = 105
      Align = alTop
      Color = clBtnFace
      ParentBackground = False
      ParentColor = False
      TabOrder = 1
      object Panel6: TPanel
        Left = 2
        Top = 15
        Width = 780
        Height = 90
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 12
          Top = 4
          Width = 14
          Height = 13
          Caption = 'ID:'
          FocusControl = DBEdID
        end
        object Label2: TLabel
          Left = 72
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdDescricao
        end
        object Label3: TLabel
          Left = 376
          Top = 4
          Width = 59
          Height = 13
          Caption = 'NOME_TEC'
          FocusControl = DBEdit1
        end
        object Label4: TLabel
          Left = 680
          Top = 4
          Width = 51
          Height = 13
          Caption = 'ID_CLASS'
          FocusControl = DBEdit2
        end
        object Label5: TLabel
          Left = 12
          Top = 44
          Width = 61
          Height = 13
          Caption = 'INCIDENCIA'
          FocusControl = DBEdit3
        end
        object Label6: TLabel
          Left = 156
          Top = 44
          Width = 60
          Height = 13
          Caption = 'VENENOSO'
          FocusControl = DBEdit4
        end
        object Label8: TLabel
          Left = 296
          Top = 44
          Width = 69
          Height = 13
          Caption = 'ID_EMPRESA'
          FocusControl = DBEdit5
        end
        object DBEdID: TdmkDBEdit
          Left = 12
          Top = 20
          Width = 56
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'ID_PRAGA'
          DataSource = DsABD_Pragas
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
          UpdType = utYes
          Alignment = taRightJustify
        end
        object DBEdDescricao: TdmkDBEdit
          Left = 72
          Top = 20
          Width = 300
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'DESCRICAO'
          DataSource = DsABD_Pragas
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object DBEdit1: TDBEdit
          Left = 376
          Top = 20
          Width = 300
          Height = 21
          DataField = 'NOME_TEC'
          DataSource = DsABD_Pragas
          TabOrder = 2
        end
        object DBEdit2: TDBEdit
          Left = 680
          Top = 20
          Width = 56
          Height = 21
          DataField = 'ID_CLASS'
          DataSource = DsABD_Pragas
          TabOrder = 3
        end
        object DBEdit3: TDBEdit
          Left = 12
          Top = 60
          Width = 134
          Height = 21
          DataField = 'INCIDENCIA'
          DataSource = DsABD_Pragas
          TabOrder = 4
        end
        object DBEdit4: TDBEdit
          Left = 156
          Top = 60
          Width = 134
          Height = 21
          DataField = 'VENENOSO'
          DataSource = DsABD_Pragas
          TabOrder = 5
        end
        object DBEdit5: TDBEdit
          Left = 296
          Top = 60
          Width = 134
          Height = 21
          DataField = 'ID_EMPRESA'
          DataSource = DsABD_Pragas
          TabOrder = 6
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 519
      Width = 784
      Height = 64
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 161
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 261
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 412
          Top = 0
          Width = 109
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object PageControl1: TPageControl
      Left = 0
      Top = 105
      Width = 784
      Height = 414
      ActivePage = TabSheet4
      Align = alClient
      TabOrder = 2
      ExplicitTop = 137
      ExplicitHeight = 382
      object TabSheet1: TTabSheet
        Caption = 'Caracter'#237'sticas'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 354
        object DBMemo1: TDBMemo
          Left = 0
          Top = 0
          Width = 776
          Height = 386
          Align = alClient
          DataField = 'CARAC_PRAGA'
          DataSource = DsABD_Pragas
          TabOrder = 0
          ExplicitLeft = 196
          ExplicitTop = 140
          ExplicitWidth = 185
          ExplicitHeight = 89
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Envenenamento'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 28
        ExplicitWidth = 0
        ExplicitHeight = 354
        object DBMemo2: TDBMemo
          Left = 0
          Top = 0
          Width = 776
          Height = 386
          Align = alClient
          DataField = 'PROC_ENV'
          DataSource = DsABD_Pragas
          TabOrder = 0
          ExplicitTop = 4
          ExplicitHeight = 354
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Preven'#231#227'o'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 32
        ExplicitWidth = 0
        ExplicitHeight = 354
        object DBMemo3: TDBMemo
          Left = 0
          Top = 0
          Width = 776
          Height = 386
          Align = alClient
          DataField = 'PREV'
          DataSource = DsABD_Pragas
          TabOrder = 0
          ExplicitLeft = 196
          ExplicitTop = 140
          ExplicitWidth = 185
          ExplicitHeight = 89
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Imagem'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 28
        ExplicitWidth = 0
        ExplicitHeight = 354
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 386
          Align = alClient
          BevelOuter = bvNone
          Caption = 'N'#227'o implementado!'
          TabOrder = 0
          ExplicitLeft = 320
          ExplicitTop = 292
          ExplicitWidth = 185
          ExplicitHeight = 41
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 520
      Height = 52
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 165
        Height = 32
        Caption = 'ABD - Pragas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 165
        Height = 32
        Caption = 'ABD - Pragas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 165
        Height = 32
        Caption = 'ABD - Pragas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 784
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrABD_Pragas: TmySQLQuery
    Database = DModG.AllID_DB
    BeforeOpen = QrABD_PragasBeforeOpen
    AfterOpen = QrABD_PragasAfterOpen
    SQL.Strings = (
      'SELECT *'
      'FROM pragas')
    Left = 64
    Top = 64
    object QrABD_PragasAUTOINC: TIntegerField
      FieldName = 'AUTOINC'
    end
    object QrABD_PragasID_PRAGA: TIntegerField
      FieldName = 'ID_PRAGA'
    end
    object QrABD_PragasDESCRICAO: TWideStringField
      FieldName = 'DESCRICAO'
      Size = 40
    end
    object QrABD_PragasNOME_TEC: TWideStringField
      FieldName = 'NOME_TEC'
      Size = 45
    end
    object QrABD_PragasID_CLASS: TIntegerField
      FieldName = 'ID_CLASS'
    end
    object QrABD_PragasCARAC_PRAGA: TWideMemoField
      FieldName = 'CARAC_PRAGA'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrABD_PragasPROC_ENV: TWideMemoField
      FieldName = 'PROC_ENV'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrABD_PragasPREV: TWideMemoField
      FieldName = 'PREV'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrABD_PragasINCIDENCIA: TIntegerField
      FieldName = 'INCIDENCIA'
    end
    object QrABD_PragasVENENOSO: TIntegerField
      FieldName = 'VENENOSO'
    end
    object QrABD_PragasIMAGEM: TBlobField
      FieldName = 'IMAGEM'
      Size = 4
    end
    object QrABD_PragasID_EMPRESA: TIntegerField
      FieldName = 'ID_EMPRESA'
    end
  end
  object DsABD_Pragas: TDataSource
    DataSet = QrABD_Pragas
    Left = 92
    Top = 64
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 120
    Top = 64
  end
end
