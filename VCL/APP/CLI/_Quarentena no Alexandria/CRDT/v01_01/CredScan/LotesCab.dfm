object FmLotesCab: TFmLotesCab
  Left = 339
  Top = 185
  Width = 800
  Height = 530
  Caption = 'Lotes da Web'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtBaixa: TBitBtn
      Tag = 19
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Baixa'
      TabOrder = 0
      OnClick = BtBaixaClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 680
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa�da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtRejeita: TBitBtn
      Tag = 309
      Left = 116
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Rejeita'
      TabOrder = 2
      OnClick = BtRejeitaClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TLMDPanelFill
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Bevel.StyleInner = bvFrameRaised
    Bevel.BorderColor = clWindow
    Bevel.EdgeStyle = etBump
    Bevel.Mode = bmCustom
    Caption = 'Lotes da Web'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    Transparent = True
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 788
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 400
    Align = alClient
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 1
      Top = 65
      Width = 790
      Height = 334
      Align = alClient
      DataSource = DsStatus
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Cliente'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Nome'
          Width = 380
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Title.Caption = 'Lote net'
          Width = 47
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Itens'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Total'
          Width = 73
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETIPO'
          Title.Caption = 'Tipo'
          Width = 100
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 64
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 1
      object RGStatus: TRadioGroup
        Left = 1
        Top = 1
        Width = 788
        Height = 62
        Align = alClient
        Caption = ' Status do Lote Web: '
        Columns = 5
        ItemIndex = 1
        Items.Strings = (
          'Gerando'
          'Gerado'
          'Baixado'
          'Aceito'
          'Rejeitado')
        TabOrder = 0
        OnClick = RGStatusClick
      end
    end
  end
  object QrLLC: TmySQLQuery
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome'
      
        'END NOMECLI, IF(llc.Tipo=0, "Cheque", "Duplicata") NOMETIPO, llc' +
        '.* '
      'FROM llotecab llc'
      'LEFT JOIN entidades ent ON ent.Codigo=llc.Cliente'
      'WHERE llc.Baixado=2')
    Left = 404
    Top = 156
    object QrLLCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLLCCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLLCLote: TIntegerField
      FieldName = 'Lote'
      DisplayFormat = '000000'
    end
    object QrLLCTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLLCData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLLCTotal: TFloatField
      FieldName = 'Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLLCDias: TFloatField
      FieldName = 'Dias'
    end
    object QrLLCItens: TIntegerField
      FieldName = 'Itens'
      DisplayFormat = '00'
    end
    object QrLLCBaixado: TIntegerField
      FieldName = 'Baixado'
    end
    object QrLLCCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLLCNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrLLCNOMETIPO: TWideStringField
      FieldName = 'NOMETIPO'
      Required = True
      Size = 9
    end
  end
  object QrLLI: TmySQLQuery
    SQL.Strings = (
      'SELECT * FROM lloteits'
      'WHERE Codigo=:P0')
    Left = 404
    Top = 188
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLLICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLLIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLLIComp: TIntegerField
      FieldName = 'Comp'
    end
    object QrLLIPraca: TIntegerField
      FieldName = 'Praca'
    end
    object QrLLIBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLLIAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrLLIConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrLLICheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrLLICPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLLIEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLLIBruto: TFloatField
      FieldName = 'Bruto'
    end
    object QrLLIDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrLLIValor: TFloatField
      FieldName = 'Valor'
    end
    object QrLLIEmissao: TDateField
      FieldName = 'Emissao'
    end
    object QrLLIDCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrLLIDDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrLLIVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrLLIDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrLLIDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrLLIIE: TWideStringField
      FieldName = 'IE'
      Size = 25
    end
    object QrLLIRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrLLINumero: TLargeintField
      FieldName = 'Numero'
    end
    object QrLLICompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrLLIBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrLLICidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrLLIUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrLLICEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrLLITel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrLLIUser_ID: TIntegerField
      FieldName = 'User_ID'
    end
    object QrLLIPasso: TIntegerField
      FieldName = 'Passo'
    end
  end
  object QrStatus: TmySQLQuery
    Database = FmPrincipal.DBNet
    SQL.Strings = (
      'SELECT wlc.*, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMECLI'
      'FROM wlotecab wlc'
      'LEFT JOIN entidades ent ON ent.Codigo=wlc.Cliente'
      'WHERE Baixado=:P0'
      'ORDER BY Codigo DESC')
    Left = 100
    Top = 56
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrStatusCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrStatusCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrStatusLote: TIntegerField
      FieldName = 'Lote'
      Required = True
    end
    object QrStatusTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrStatusData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrStatusTotal: TFloatField
      FieldName = 'Total'
      Required = True
    end
    object QrStatusDias: TFloatField
      FieldName = 'Dias'
      Required = True
    end
    object QrStatusItens: TIntegerField
      FieldName = 'Itens'
      Required = True
    end
    object QrStatusBaixado: TIntegerField
      FieldName = 'Baixado'
      Required = True
    end
    object QrStatusCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrStatusNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
  end
  object DsStatus: TDataSource
    DataSet = QrStatus
    Left = 128
    Top = 56
  end
end

