unit Status;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  teForm, TransEff, teTimed, teRoll, Buttons, ExtCtrls, StdCtrls, tePixelt,
  LMDControl, LMDBaseControl, LMDBaseGraphicControl, LMDBaseLabel,
  LMDCustomLabel, LMDLabel;

type
  TFmStatus = class(TForm)
    Image1: TImage;
    Button1: TButton;
    Button2: TButton;
    FormTransitions1: TFormTransitions;
    TransitionList1: TTransitionList;
    Transition1: TRollTransition;
    Transition2: TRollTransition;
    Label1: TLabel;
    Button3: TButton;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmStatus: TFmStatus;

implementation

uses Principal;

{$R *.DFM}

procedure TFmStatus.FormCreate(Sender: TObject);
var
  x, y: Integer;
begin
  y := Screen.Height;
  x := Screen.Width;
  //
  FmStatus.Left := x - FmStatus.Width  - 10;
  FmStatus.Top  := y - FmStatus.Height - 27;
  //
  //Label2.Caption := MLAGeral.FileVerInfo(Application.ExeName, 3);
end;

procedure TFmStatus.FormDestroy(Sender: TObject);
begin
  //FmPrincipal.FShowStatus := False;
end;

procedure TFmStatus.Button1Click(Sender: TObject);
begin
  {
  Application.CreateForm(TFmLotesCab, FmLotesCab);
  FmLotesCab.ShowModal;
  FmLotesCab.Destroy;
  }
end;

procedure TFmStatus.FormHide(Sender: TObject);
begin
  FmPrincipal.Hide;
end;

procedure TFmStatus.Button3Click(Sender: TObject);
begin
  Close;
end;

procedure TFmStatus.Button2Click(Sender: TObject);
begin
  FmPrincipal.BaixaLotesWeb;
end;

end.
