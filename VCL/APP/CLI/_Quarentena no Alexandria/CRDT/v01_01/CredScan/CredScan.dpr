program CredScan;

uses
  Forms,
  Principal in 'Principal.pas' {FmPrincipal},
  Status in 'Status.pas' {FmStatus},
  LotesCab in '..\CredScan2\LotesCab.pas' {FmLotesCab};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'Creditor WebScan';
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmStatus, FmStatus);
  Application.CreateForm(TFmLotesCab, FmLotesCab);
  Application.Run;
end.
