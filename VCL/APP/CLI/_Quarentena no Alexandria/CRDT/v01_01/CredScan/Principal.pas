unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, StdCtrls, Registry, ShellApi, Menus, ExtCtrls, WinSkinStore,
  WinSkinData, Db, mySQLDbTables, TransEff, teTimed, teRoll, teForm,
  dmkGeral;

type
  TFmPrincipal = class(TForm)
    PopupMenu1: TPopupMenu;
    Mostrar1: TMenuItem;
    Fechar1: TMenuItem;
    Timer1: TTimer;
    sd1: TSkinData;
    SkinStore1: TSkinStore;
    DBPri: TmySQLDatabase;
    QrPri: TmySQLQuery;
    QrPriWeb_Host: TWideStringField;
    QrPriWeb_User: TWideStringField;
    QrPriWeb_Pwd: TWideStringField;
    QrPriWeb_DB: TWideStringField;
    QrPriNewWebScan: TIntegerField;
    QrPriNewWebMins: TIntegerField;
    DBNet: TmySQLDatabase;
    QrNet: TmySQLQuery;
    QrNew: TmySQLQuery;
    QrNetCodigo: TAutoIncField;
    QrNetCliente: TIntegerField;
    QrNetLote: TIntegerField;
    QrNetTipo: TSmallintField;
    QrNetData: TDateField;
    QrNetTotal: TFloatField;
    QrNetDias: TFloatField;
    QrNetItens: TIntegerField;
    QrNetBaixado: TIntegerField;
    QrNetCPF: TWideStringField;
    QrNetNOMECLI: TWideStringField;
    QrNewCodigo: TAutoIncField;
    QrUpdN: TmySQLQuery;
    QrNewBaixado: TIntegerField;
    Qrwlc: TmySQLQuery;
    QrwlcCodigo: TAutoIncField;
    QrwlcCliente: TIntegerField;
    QrwlcLote: TIntegerField;
    QrwlcTipo: TSmallintField;
    QrwlcData: TDateField;
    QrwlcTotal: TFloatField;
    QrwlcDias: TFloatField;
    QrwlcItens: TIntegerField;
    QrwlcBaixado: TIntegerField;
    QrwlcCPF: TWideStringField;
    QrUpdL: TmySQLQuery;
    Qrwli: TmySQLQuery;
    QrwliCodigo: TIntegerField;
    QrwliControle: TAutoIncField;
    QrwliComp: TIntegerField;
    QrwliPraca: TIntegerField;
    QrwliBanco: TIntegerField;
    QrwliAgencia: TIntegerField;
    QrwliConta: TWideStringField;
    QrwliCheque: TIntegerField;
    QrwliCPF: TWideStringField;
    QrwliEmitente: TWideStringField;
    QrwliBruto: TFloatField;
    QrwliDesco: TFloatField;
    QrwliValor: TFloatField;
    QrwliEmissao: TDateField;
    QrwliDCompra: TDateField;
    QrwliDDeposito: TDateField;
    QrwliVencto: TDateField;
    QrwliDias: TIntegerField;
    QrwliDuplicata: TWideStringField;
    QrwliUser_ID: TIntegerField;
    QrwliPasso: TIntegerField;
    QrwliRua: TWideStringField;
    QrwliNumero: TLargeintField;
    QrwliCompl: TWideStringField;
    QrwliBairro: TWideStringField;
    QrwliCidade: TWideStringField;
    QrwliUF: TWideStringField;
    QrwliCEP: TIntegerField;
    QrwliTel1: TWideStringField;
    QrwliIE: TWideStringField;
    QrSumLLC: TmySQLQuery;
    QrSumLLCTotal: TFloatField;
    QrSumLLCItens: TFloatField;
    TrayIcon1: TTrayIcon;
    procedure FormCreate(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure Mostrar1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TrayIcon1Click(Sender: TObject);
  private
    { Private declarations }
    procedure ExecutaNaInicializacao(Titulo, Programa: String);
    function BuscaInfoNet(Acao: Integer): Integer;
    procedure ReopenQrNet(Quitado: integer);
  public
    { Public declarations }
    FShowStatus: Boolean;
    function RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
              TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    function AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
             Data: TDateTime; Arquivo: String): Boolean;
    procedure MostraStatus(Forca: Boolean);
    function TextoStatus(Status: Integer): String;
    function BaixaLotesWeb: Integer;
  end;


var
  FmPrincipal: TFmPrincipal;

implementation

uses Status;

{$R *.DFM}

(*procedure TFmPrincipal.WMQueryOpen(var Msg : TWMQueryOpen);
begin
  Msg.Result := 0;
end; *)

(*procedure TFmPrincipal.CreateParams(var Params: TCreateParams);
begin
  inherited CreateParams(Params);
  with Params do
    exStyle := exStyle or WS_EX_TOOLWINDOW;
end;*)

procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;

function TFmPrincipal.RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
  Result := False;
  //Ucriar.RecriaTabelaLocal(Tabela, Acao);
end;

///// FIM OBRIGAT�RIOS

procedure TFmPrincipal.ExecutaNaInicializacao(Titulo, Programa: String);
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_LOCAL_MACHINE;
    Registry.OpenKey('\Software\Microsoft\Windows\CurrentVersion\Run', False);
    Registry.WriteString(Titulo, Programa);
  finally
    Registry.Free;
  end;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
begin
  TrayIcon1.Visible := False;
{
var
  NotifyIconData : TNotifyIconData;
begin
  Width := 0;
  Height := 0;
  ExecutaNaInicializacao('Creditor WebScan', Application.ExeName);
  with NotifyIconData do begin
    cbSize := sizeof(TNotifyIconData);
    wnd := self.Handle;
    uId := 0;
    uCallbackMessage := WM_TRYICON;
    uFlags := NIF_ICON or NIF_TIP or NIF_MESSAGE;
    hIcon := Application.Icon.Handle;
    szTip := 'Creditor WebScan';
  end;
  Shell_NotifyIcon(NIM_ADD, @NotifyIconData);
}
  //
  FmPrincipal.Top := 32000;
  BuscaInfoNet(1);
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  MostraStatus(False);
end;

procedure TFmPrincipal.Fechar1Click(Sender: TObject);
begin
  if FShowStatus = False then Close;
end;

procedure TFmPrincipal.Mostrar1Click(Sender: TObject);
begin
  //FmPrincipal.Show;
  MostraStatus(True);
end;


procedure TFmPrincipal.MostraStatus(Forca: Boolean);
var
  Status: Integer;
  Mensagem: String;
begin
  Status := BuscaInfoNet(2);
  Mensagem := TextoStatus(Status);
  //
  if Forca or (Status > 5) then
  begin
    //Application.CreateForm(TFmStatus, FmStatus);
    if Status > 3 then
    begin
      FmStatus.Button1.Visible := True;
      FmStatus.Button2.Visible := True;
    end;
    FmStatus.Label1.Caption := Mensagem;
    //
    FmPrincipal.Show;
    FmStatus.Show;
    //FmStatus.ShowModal;
    //FmStatus.Destroy;
  end;
end;

function TFmPrincipal.BuscaInfoNet(Acao: Integer): Integer;
var
  Status: Integer;
begin
  Status := 0;
  try
    QrPri.Close;
    QrPri.Open;
    Status := 1;
    Timer1.Interval := 60000 * QrPriNewWebMins.Value;
    Timer1.Enabled := (QrPriNewWebScan.Value = 1);
    //
    if Acao = 2 then
    begin
      DBNet.Host         := QrPriWeb_Host.Value;
      DBNet.DatabaseName := QrPriWeb_DB.Value;
      DBNet.UserName     := QrPriWeb_User.Value;
      DBNet.UserPassword := QrPriWeb_Pwd.Value;
      DBNet.Disconnect;
      DBNet.Connect;
      Status := 2;
      //
      ReopenQrNet(1);
      //
      Status := 3;
      if QrNet.RecordCount > 0 then
      begin
        if QrNet.RecordCount =  1 then Status := 6 else Status := 7;
        QrNew.Close;
        QrNew.Open;
        if QrNew.RecordCount > 0 then
        begin
          Status := 8;
          QrUpdN.SQL.Clear;
          QrUpdN.SQL.Add('UPDATE wlotecab SET ScanStep=:P0 WHERE Codigo=:P1');
          while not QrNew.Eof do
          begin
            QrUpdN.Params[00].AsInteger := QrNewBaixado.Value;
            QrUpdN.Params[01].AsInteger := QrNewCodigo.Value;
            QrUpdN.ExecSQL;
            //
            QrNew.Next;
          end;
          Status := 9;
        end else
        begin
          if QrNet.RecordCount =  1 then Status := 4 else Status := 5;
        end;
      end;
    end;
  except
  end;
  //
  //
  Result := Status;
end;

function TFmPrincipal.TextoStatus(Status: Integer): String;
begin
  case Status of
    0: Result := ' N�o conectado [Local]';
    1: Result := ' N�o conectado [Site]';
    3: Result := ' N�o h� lote a ser baixado da web!';
    4: Result := ' H� um lote a ser baixado da web!';
    5: Result := ' H� ' + IntToStr(QrNet.RecordCount) +
      ' lotes para baixar da web!';
    6: Result :=
      ' H� um lote a ser baixado da web, mas n�o foi poss�vel verificar ele � novo!';
    7: Result := ' H� ' + IntToStr(QrNet.RecordCount) +
      ' lotes para baixar da web, mas n�o foi poss�vel verificar se eles s�o novos!';
    8: Result := ' H� ' + IntToStr(QrNet.RecordCount) +
      ' lotes para baixar da web, dos quais ' + IntToStr(QrNew.RecordCount) +
      ' s�o novos! (mas ocorreu um erro ao set�-los como reportados)';
    9: Result := ' H� ' + IntToStr(QrNet.RecordCount) +
      ' lotes para baixar da web, dos quais ' + IntToStr(QrNew.RecordCount) +
      ' s�o novos!';
    else Result := ' Status desconhecido';
  end;
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  //Timer2.Enabled := True;
end;

procedure TFmPrincipal.TrayIcon1Click(Sender: TObject);
begin
  FmStatus.Visible := True;
end;

procedure TFmPrincipal.ReopenQrNet(Quitado: integer);
begin
  Screen.Cursor := crHourGlass;
  QrNet.Close;
  QrNet.Params[0].AsInteger := Quitado;
  QrNet.Open;
  //
  Screen.Cursor := crDefault;
end;

function TFmPrincipal.BaixaLotesWeb: Integer;
var
  Baixar: Integer;
begin
  Qrwlc.Close;
  Qrwlc.Open;
  Baixar := Qrwlc.RecordCount;
  case Baixar of
    0: Application.MessageBox('N�o h� lote novo na web!', 'Informa��o',
      MB_OK+MB_ICONINFORMATION);
    1: if Application.MessageBox(PChar('Foi localizado um lote novo na web! '+
       'Deseja baix�-lo?'), 'Pergunta', MB_YESNO+MB_ICONQUESTION) <> ID_YES then
       Baixar := 0;
    else if Application.MessageBox(PChar('Foram localizados ' +
       IntToStr(Baixar) + ' lotes novos na web! Deseja baix�-los?'), 'Pergunta',
       MB_YESNO+MB_ICONQUESTION) <> ID_YES then Baixar := 0;
  end;
  if Baixar > 0 then
  begin
    Screen.Cursor := crHourGlass;
    try
      Qrwlc.First;
      while not Qrwlc.Eof do
      begin
        QrUpdL.SQL.Clear;
        QrUpdL.SQL.Add('DELETE FROM llotecab WHERE Codigo=:P0');
        QrUpdL.Params[0].AsInteger := QrwlcCodigo.Value;
        QrUpdL.ExecSQL;
        //
        QrUpdL.SQL.Clear;
        QrUpdL.SQL.Add('DELETE FROM lloteits WHERE Codigo=:P0');
        QrUpdL.Params[0].AsInteger := QrwlcCodigo.Value;
        QrUpdL.ExecSQL;
        //
        QrUpdL.SQL.Clear;
        QrUpdL.SQL.Add('INSERT INTO llotecab SET ');
        QrUpdL.SQL.Add('Codigo=:P0, Cliente=:P1, Lote=:P2, Tipo=:P3, ');
        QrUpdL.SQL.Add('Data=:P4, Total=:P5, Itens=:P6');

        QrUpdL.Params[00].AsInteger := QrwlcCodigo.Value;
        QrUpdL.Params[01].AsInteger := QrwlcCliente.Value;
        QrUpdL.Params[02].AsInteger := QrwlcLote.Value;
        QrUpdL.Params[03].AsInteger := QrwlcTipo.Value;
        QrUpdL.Params[04].AsString  := Geral.FDT(QrwlcData.Value, 1);
        QrUpdL.Params[05].AsFloat   := QrwlcTotal.Value;
        QrUpdL.Params[06].AsInteger := QrwlcItens.Value;
        QrUpdL.ExecSQL;
        //
        Qrwli.Close;
        Qrwli.Params[0].AsInteger := QrwlcCodigo.Value;
        Qrwli.Open;
        //
        Qrwli.First;
        QrUpdL.SQL.Clear;
        QrUpdL.SQL.Add('INSERT INTO lloteits SET ');
        QrUpdL.SQL.Add('Codigo=:P0, Controle=:P1, Praca=:P2, Banco=:P3, ');
        QrUpdL.SQL.Add('Agencia=:P4, Conta=:P5, Cheque=:P6, CPF=:P7, ');
        QrUpdL.SQL.Add('Emitente=:P8, Bruto=:P9, Valor=:P10, ');
        QrUpdL.SQL.Add('Vencto=:P11, Duplicata=:P12, Rua=:P13, ');
        QrUpdL.SQL.Add('Numero=:P14, Compl=:P15, Bairro=:P16, ');
        QrUpdL.SQL.Add('Cidade=:P17, UF=:P18, CEP=:P19, Tel1=:P20, ');
        QrUpdL.SQL.Add('IE=:P21, Emissao=:P22');
        QrUpdL.SQL.Add('');
        while not Qrwli.Eof do
        begin
          QrUpdL.Params[00].AsInteger := QrwliCodigo.Value;
          QrUpdL.Params[01].AsInteger := QrwliControle.Value;
          QrUpdL.Params[02].AsInteger := QrwliPraca.Value;
          QrUpdL.Params[03].AsInteger := QrwliBanco.Value;
          QrUpdL.Params[04].AsInteger := QrwliAgencia.Value;
          QrUpdL.Params[05].AsString  := QrwliConta.Value;
          QrUpdL.Params[06].AsInteger := QrwliCheque.Value;
          QrUpdL.Params[07].AsString  := QrwliCPF.Value;
          QrUpdL.Params[08].AsString  := QrwliEmitente.Value;
          QrUpdL.Params[09].AsFloat   := QrwliBruto.Value;
          QrUpdL.Params[10].AsFloat   := QrwliValor.Value;
          QrUpdL.Params[11].AsString  := Geral.FDT(QrwliVencto.Value, 1);
          QrUpdL.Params[12].AsString  := QrwliDuplicata.Value;
          QrUpdL.Params[13].AsString  := QrwliRua.Value;
          QrUpdL.Params[14].AsInteger := QrwliNumero.Value;
          QrUpdL.Params[15].AsString  := QrwliCompl.Value;
          QrUpdL.Params[16].AsString  := QrwliBairro.Value;
          QrUpdL.Params[17].AsString  := QrwliCidade.Value;
          QrUpdL.Params[18].AsString  := QrwliUF.Value;
          QrUpdL.Params[19].AsInteger := QrwliCEP.Value;
          QrUpdL.Params[20].AsString  := QrwliTel1.Value;
          QrUpdL.Params[21].AsString  := QrwliIE.Value;
          QrUpdL.Params[22].AsString  := Geral.FDT(QrwliEmissao.Value, 1);
          QrUpdL.ExecSQL;
          Qrwli.Next;
        end;
        QrSumLLC.Close;
        QrSumLLC.Params[0].AsInteger := QrwlcCodigo.Value;
        QrSumLLC.Open;
        //
        QrUpdN.SQL.Clear;
        QrUpdN.SQL.Add('UPDATE wlotecab SET Baixado=2, Total=:P0, Itens=:P1 ');
        QrUpdN.SQL.Add('WHERE Codigo=:P2');
        QrUpdN.Params[00].AsFloat   := QrSumLLCTotal.Value;
        QrUpdN.Params[01].AsInteger := Trunc(QrSumLLCItens.Value);
        QrUpdN.Params[02].AsInteger := QrwlcCodigo.Value;
        QrUpdN.ExecSQL;
        //
        QrUpdL.SQL.Clear;
        QrUpdL.SQL.Add('UPDATE llotecab SET Baixado=2, Total=:P0, Itens=:P1 ');
        QrUpdL.SQL.Add('WHERE Codigo=:P2');
        QrUpdL.Params[00].AsFloat   := QrSumLLCTotal.Value;
        QrUpdL.Params[01].AsInteger := Trunc(QrSumLLCItens.Value);
        QrUpdL.Params[02].AsInteger := QrwlcCodigo.Value;
        QrUpdL.ExecSQL;
        //
        Qrwlc.Next;
      end;
      Application.MessageBox('Download conclu�do com sucesso!', 'Informa��o',
        MB_OK+MB_ICONINFORMATION);
      Screen.Cursor := crDefault;
    finally
      Screen.Cursor := crDefault;
    end;  
  end;
  Result := Baixar;
end;

end.

