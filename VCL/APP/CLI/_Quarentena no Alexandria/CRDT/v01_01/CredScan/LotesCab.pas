unit LotesCab;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, lmdrtfbase, lmdrtfrichedit,
  LMDCustomControl, LMDCustomPanel, LMDCustomBevelPanel,
  LMDCustomParentPanel, LMDCustomPanelFill, LMDPanelFill, Grids, DBGrids,
  Db, mySQLDbTables, LMDCustomGroupBox, LMDCustomButtonGroup,
  LMDCustomCheckGroup, LMDCheckGroup;

type
  TFmLotesCab = class(TForm)
    PainelConfirma: TPanel;
    BtBaixa: TBitBtn;
    PainelTitulo: TLMDPanelFill;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DBGrid1: TDBGrid;
    QrLLC: TmySQLQuery;
    QrLLCCodigo: TIntegerField;
    QrLLCCliente: TIntegerField;
    QrLLCLote: TIntegerField;
    QrLLCTipo: TSmallintField;
    QrLLCData: TDateField;
    QrLLCTotal: TFloatField;
    QrLLCDias: TFloatField;
    QrLLCItens: TIntegerField;
    QrLLCBaixado: TIntegerField;
    QrLLCCPF: TWideStringField;
    QrLLCNOMECLI: TWideStringField;
    QrLLCNOMETIPO: TWideStringField;
    QrLLI: TmySQLQuery;
    QrLLICodigo: TIntegerField;
    QrLLIControle: TIntegerField;
    QrLLIComp: TIntegerField;
    QrLLIPraca: TIntegerField;
    QrLLIBanco: TIntegerField;
    QrLLIAgencia: TIntegerField;
    QrLLIConta: TWideStringField;
    QrLLICheque: TIntegerField;
    QrLLICPF: TWideStringField;
    QrLLIEmitente: TWideStringField;
    QrLLIBruto: TFloatField;
    QrLLIDesco: TFloatField;
    QrLLIValor: TFloatField;
    QrLLIEmissao: TDateField;
    QrLLIDCompra: TDateField;
    QrLLIDDeposito: TDateField;
    QrLLIVencto: TDateField;
    QrLLIDias: TIntegerField;
    QrLLIDuplicata: TWideStringField;
    QrLLIIE: TWideStringField;
    QrLLIRua: TWideStringField;
    QrLLINumero: TLargeintField;
    QrLLICompl: TWideStringField;
    QrLLIBairro: TWideStringField;
    QrLLICidade: TWideStringField;
    QrLLIUF: TWideStringField;
    QrLLICEP: TIntegerField;
    QrLLITel1: TWideStringField;
    QrLLIUser_ID: TIntegerField;
    QrLLIPasso: TIntegerField;
    BtRejeita: TBitBtn;
    Panel3: TPanel;
    RGStatus: TRadioGroup;
    QrStatus: TmySQLQuery;
    QrStatusCodigo: TAutoIncField;
    QrStatusCliente: TIntegerField;
    QrStatusLote: TIntegerField;
    QrStatusTipo: TSmallintField;
    QrStatusData: TDateField;
    QrStatusTotal: TFloatField;
    QrStatusDias: TFloatField;
    QrStatusItens: TIntegerField;
    QrStatusBaixado: TIntegerField;
    QrStatusCPF: TWideStringField;
    QrStatusNOMECLI: TWideStringField;
    DsStatus: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtBaixaClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtRejeitaClick(Sender: TObject);
    procedure RGStatusClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenQrStatus(Quitado: integer);
  public
    { Public declarations }
  end;

  var
  FmLotesCab: TFmLotesCab;

implementation

{$R *.DFM}

uses Principal;

procedure TFmLotesCab.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLotesCab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ReopenQrStatus(RGStatus.ItemIndex);
end;

procedure TFmLotesCab.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToLMDPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLotesCab.BtBaixaClick(Sender: TObject);
begin
  FmPrincipal.BaixaLotesWeb;
end;

procedure TFmLotesCab.DBGrid1DblClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLotesCab.BtRejeitaClick(Sender: TObject);
var
  Baixado: Integer;
begin
  Baixado := -1;
  case RGStatus.ItemIndex of
    1:
    begin
      if Application.MessageBox(PChar('Confirma a rejei��o deste lote?'+
      Chr(13)+Chr(10)+ '(A rejei��o poder� ser desfeita!)'), 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then Baixado := 4;
    end;
    4:
    begin
      if Application.MessageBox(PChar('Desfaz a rejei��o deste lote?'+
      Chr(13)+Chr(10)+ '(A rejei��o poder� ser refeita!)'), 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then Baixado := 1;
    end;
  end;
  if Baixado <> -1 then
  begin
    FmPrincipal.QrUpdN.SQL.Clear;
    FmPrincipal.QrUpdN.SQL.Add('UPDATE wlotecab SET Baixado=:P0 WHERE Codigo=:P1');
    FmPrincipal.QrUpdN.Params[0].AsInteger := Baixado;
    FmPrincipal.QrUpdN.Params[1].AsInteger := FmPrincipal.QrNetCodigo.Value;
    FmPrincipal.QrUpdN.ExecSQL;
    //
    ReopenQrStatus(RGStatus.ItemIndex);
  end else Application.MessageBox('Nenhuma a��o foi executada neste lote',
    'Aviso', MB_OK+MB_ICONEXCLAMATION);
end;

procedure TFmLotesCab.RGStatusClick(Sender: TObject);
begin
  ReopenQrStatus(RGStatus.ItemIndex);
  case RGStatus.ItemIndex of
    1:
    begin
      BtBaixa.Enabled := True;
      BtRejeita.Enabled := True;
    end;
    4:
    begin
      BtBaixa.Enabled := False;
      BtRejeita.Enabled := True;
    end;
    else
    begin
      BtBaixa.Enabled := False;
      BtRejeita.Enabled := False;
    end;
  end;
end;

procedure TFmLotesCab.ReopenQrStatus(Quitado: integer);
begin
  Screen.Cursor := crHourGlass;
  QrStatus.Close;
  QrStatus.Params[0].AsInteger := Quitado;
  QrStatus.Open;
  //
  Screen.Cursor := crDefault;
end;

end.
