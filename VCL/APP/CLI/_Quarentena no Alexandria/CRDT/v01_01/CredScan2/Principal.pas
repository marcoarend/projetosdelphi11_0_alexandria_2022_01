unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  teForm, TransEff, teTimed, teRoll, Buttons, ExtCtrls, StdCtrls, tePixelt,
  LMDControl, LMDBaseControl, LMDBaseGraphicControl, LMDBaseLabel,
  LMDCustomLabel, LMDLabel, Menus, DB, mySQLDbTables, registry, jpeg;

type
  TFmPrincipal = class(TForm)
    Image1: TImage;
    Button1: TButton;
    Button2: TButton;
    FormTransitions1: TFormTransitions;
    TransitionList1: TTransitionList;
    Transition1: TRollTransition;
    Transition2: TRollTransition;
    Label1: TLabel;
    Button3: TButton;
    Label2: TLabel;
    TrayIcon1: TTrayIcon;
    Timer1: TTimer;
    PopupMenu1: TPopupMenu;
    Mostrar1: TMenuItem;
    Fechar1: TMenuItem;
    N1: TMenuItem;
    Inicializao1: TMenuItem;
    Executarnainicializao1: TMenuItem;
    NOexecutarnainicializao1: TMenuItem;
    DBPri: TmySQLDatabase;
    QrUpdL: TmySQLQuery;
    QrUpdN: TmySQLQuery;
    DBNet: TmySQLDatabase;
    QrPri: TmySQLQuery;
    QrPriWeb_Host: TWideStringField;
    QrPriWeb_User: TWideStringField;
    QrPriWeb_Pwd: TWideStringField;
    QrPriWeb_DB: TWideStringField;
    QrPriNewWebScan: TIntegerField;
    QrPriNewWebMins: TIntegerField;
    QrNet: TmySQLQuery;
    QrNetCodigo: TAutoIncField;
    QrNetCliente: TIntegerField;
    QrNetLote: TIntegerField;
    QrNetTipo: TSmallintField;
    QrNetData: TDateField;
    QrNetTotal: TFloatField;
    QrNetDias: TFloatField;
    QrNetItens: TIntegerField;
    QrNetBaixado: TIntegerField;
    QrNetCPF: TWideStringField;
    QrNetNOMECLI: TWideStringField;
    Qrwlc: TmySQLQuery;
    QrwlcCodigo: TAutoIncField;
    QrwlcCliente: TIntegerField;
    QrwlcLote: TIntegerField;
    QrwlcTipo: TSmallintField;
    QrwlcData: TDateField;
    QrwlcTotal: TFloatField;
    QrwlcDias: TFloatField;
    QrwlcItens: TIntegerField;
    QrwlcBaixado: TIntegerField;
    QrwlcCPF: TWideStringField;
    Qrwli: TmySQLQuery;
    QrwliCodigo: TIntegerField;
    QrwliControle: TAutoIncField;
    QrwliComp: TIntegerField;
    QrwliPraca: TIntegerField;
    QrwliBanco: TIntegerField;
    QrwliAgencia: TIntegerField;
    QrwliConta: TWideStringField;
    QrwliCheque: TIntegerField;
    QrwliCPF: TWideStringField;
    QrwliEmitente: TWideStringField;
    QrwliBruto: TFloatField;
    QrwliDesco: TFloatField;
    QrwliValor: TFloatField;
    QrwliEmissao: TDateField;
    QrwliDCompra: TDateField;
    QrwliDDeposito: TDateField;
    QrwliVencto: TDateField;
    QrwliDias: TIntegerField;
    QrwliDuplicata: TWideStringField;
    QrwliUser_ID: TIntegerField;
    QrwliPasso: TIntegerField;
    QrwliRua: TWideStringField;
    QrwliNumero: TLargeintField;
    QrwliCompl: TWideStringField;
    QrwliBairro: TWideStringField;
    QrwliCidade: TWideStringField;
    QrwliUF: TWideStringField;
    QrwliCEP: TIntegerField;
    QrwliTel1: TWideStringField;
    QrwliIE: TWideStringField;
    QrNew: TmySQLQuery;
    QrNewCodigo: TAutoIncField;
    QrNewBaixado: TIntegerField;
    QrSumLLC: TmySQLQuery;
    QrSumLLCTotal: TFloatField;
    QrSumLLCItens: TFloatField;
    Timer2: TTimer;
    QrAux: TmySQLQuery;
    QrControle: TmySQLQuery;
    QrControleWeb_Host: TWideStringField;
    QrControleWeb_User: TWideStringField;
    QrControleWeb_Pwd: TWideStringField;
    QrControleWeb_DB: TWideStringField;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Timer1Timer(Sender: TObject);
    procedure Mostrar1Click(Sender: TObject);
    procedure Fechar1Click(Sender: TObject);
    procedure Executarnainicializao1Click(Sender: TObject);
    procedure NOexecutarnainicializao1Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure TrayIcon1DblClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
      FShowed, FHideFirst: Boolean;
    procedure ExecutaNaInicializacao(Executa: Boolean; Titulo, Programa: String);
    procedure ReopenQrNet(Quitado: integer);
    //
    procedure MostraStatus(Forca: Boolean);
    function TextoStatus(Status: Integer): String;
    //
  public
    { Public declarations }
    function BuscaInfoNet(Acao: Integer): Integer;
    function BaixaLotesWeb: Integer;
    function ConectaBDn(Pergunta: Boolean): Boolean;
    function FaltaInfo(ParesDeItens: Integer; Textos: array of String;
             TituloMensagem: String): Boolean;
  end;

var
  FmPrincipal: TFmPrincipal;

implementation

uses dmkGeral, LotesCab;

{$R *.DFM}

procedure GetBuildInfo(var V1, V2, V3, V4: Word);
var
VerInfoSize, VerValueSize, Dummy: DWORD;
VerInfo: Pointer;
VerValue: PVSFixedFileInfo;
begin
VerInfoSize := GetFileVersionInfoSize(Pchar(ParamStr(0)), Dummy);
GetMem(VerInfo, VerInfoSize);
GetFileVersionInfo(Pchar(ParamStr(0)), 0, VerInfoSize, VerInfo);
VerQueryValue(VerInfo, '''', Pointer(VerValue), VerValueSize);
with VerValue^ do
begin
V1 := dwFileVersionMS shr 16;
V2 := dwFileVersionMS and $FFFF;
V3 := dwFileVersionLS shr 16;
V4 := dwFileVersionLS and $FFFF;
end;
Freemem(VerInfo, VerInfoSize);
end;

function GetVersionInfo: string;
var
V1, V2, V3, V4: Word;
begin
GetBuildInfo(V1, V2, V3, V4);
Result := IntToStr(V1) + '.' + IntToStr(V2) + IntToStr(V3) + ' (Build ' + IntToStr(V4) + ')';
end;

function TFmPrincipal.FaltaInfo(ParesDeItens: Integer; Textos: array of String;
  TituloMensagem: String): Boolean;
var
  s: string;
  i: Integer;
begin
  Result := False;
  for i := 0 to ParesDeItens - 1 do
  begin
    if Trim(Textos[i * 2]) = '' then
    begin
      Result := True;
      s := Textos[(i * 2)+1];
      if Trim(s) <> '' then Application.MessageBox(PChar('Falta informar "'+s+
        '"!'), PChar(TituloMensagem), MB_OK+MB_ICONWARNING);
    end;
  end;
end;

procedure TFmPrincipal.Fechar1Click(Sender: TObject);
begin
  TrayIcon1.Visible := False;
  Close;
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  TrayIcon1.Visible := True;
  //Timer2.Enabled := True;
end;

procedure TFmPrincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if TrayIcon1.Visible then
  begin
    Action := caNone;
    Hide;
  end;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  x, y: Integer;
begin
  FShowed := True;
  y := Screen.Height;
  x := Screen.Width;
  //
  FmPrincipal.Left := x - FmPrincipal.Width  - 10;
  FmPrincipal.Top  := y - FmPrincipal.Height - 27;
  //
  Label2.Caption := Geral.FileVerInfo(Application.ExeName, 3);
  BuscaInfoNet(1);
end;

procedure TFmPrincipal.FormDestroy(Sender: TObject);
begin
  //FmPrincipal.FShowStatus := False;
end;

procedure TFmPrincipal.Mostrar1Click(Sender: TObject);
begin
  MostraStatus(True);
end;

procedure TFmPrincipal.MostraStatus(Forca: Boolean);
var
  Status: Integer;
begin
  Status := BuscaInfoNet(2);
  if Forca or (Status > 5) then
    Show;
end;

procedure TFmPrincipal.NOexecutarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(False, 'Creditor WebScan', Application.ExeName);
end;

procedure TFmPrincipal.ReopenQrNet(Quitado: integer);
begin
  Screen.Cursor := crHourGlass;
  QrNet.Close;
  QrNet.Params[0].AsInteger := Quitado;
  QrNet.Open;
  //
  Screen.Cursor := crDefault;
end;

function TFmPrincipal.TextoStatus(Status: Integer): String;
begin
  case Status of
    0: Result := ' N�o conectado [Local]';
    1: Result := ' N�o conectado [Site]';
    3: Result := ' N�o h� lote a ser baixado da web!';
    4: Result := ' H� um lote a ser baixado da web!';
    5: Result := ' H� ' + IntToStr(QrNet.RecordCount) +
      ' lotes para baixar da web!';
    6: Result :=
      ' H� um lote a ser baixado da web, mas n�o foi poss�vel verificar ele � novo!';
    7: Result := ' H� ' + IntToStr(QrNet.RecordCount) +
      ' lotes para baixar da web, mas n�o foi poss�vel verificar se eles s�o novos!';
    8: Result := ' H� ' + IntToStr(QrNet.RecordCount) +
      ' lotes para baixar da web, dos quais ' + IntToStr(QrNew.RecordCount) +
      ' s�o novos! (mas ocorreu um erro ao set�-los como reportados)';
    9: Result := ' H� ' + IntToStr(QrNet.RecordCount) +
      ' lotes para baixar da web, dos quais ' + IntToStr(QrNew.RecordCount) +
      ' s�o novos!';
    else Result := ' Status desconhecido';
  end;
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  MostraStatus(False);
end;

procedure TFmPrincipal.Timer2Timer(Sender: TObject);
begin
  if Visible then
  begin
    //EdSeg.Text := FormatDateTime('  hh:nn:ss',Now - FStartTime);
    if not FHideFirst then
    begin
      FHideFirst := True;
      Application.MainFormOnTaskBar := FShowed;
      Hide;
      //FmWetBlueMLA_BK.WindowState :=  wsMinimized;
      Application.MainFormOnTaskBar := FShowed;
      TrayIcon1.Visible := True;
    end;
  end;
end;

procedure TFmPrincipal.TrayIcon1DblClick(Sender: TObject);
begin
  MostraStatus(True);
end;

function TFmPrincipal.BaixaLotesWeb: Integer;
var
  Baixar: Integer;
begin
  // Parei Aqui Fazer conex�o do BD, se este estiver desconectado
  if not ConectaBDn(True) then
  begin
    Result := 0;
    Exit;
  end;
  Qrwlc.Close;
  Qrwlc.Open;
  Baixar := Qrwlc.RecordCount;
  case Baixar of
    0: Application.MessageBox('N�o h� lote novo na web!', 'Informa��o',
      MB_OK+MB_ICONINFORMATION);
    1: if Application.MessageBox(PChar('Foi localizado um lote novo na web! '+
       'Deseja baix�-lo?'), 'Pergunta', MB_YESNO+MB_ICONQUESTION) <> ID_YES then
       Baixar := 0;
    else if Application.MessageBox(PChar('Foram localizados ' +
       IntToStr(Baixar) + ' lotes novos na web! Deseja baix�-los?'), 'Pergunta',
       MB_YESNO+MB_ICONQUESTION) <> ID_YES then Baixar := 0;
  end;
  if Baixar > 0 then
  begin
    Screen.Cursor := crHourGlass;
    try
      Qrwlc.First;
      while not Qrwlc.Eof do
      begin
        QrUpdL.SQL.Clear;
        QrUpdL.SQL.Add('DELETE FROM llotecab WHERE Codigo=:P0');
        QrUpdL.Params[0].AsInteger := QrwlcCodigo.Value;
        QrUpdL.ExecSQL;
        //
        QrUpdL.SQL.Clear;
        QrUpdL.SQL.Add('DELETE FROM lloteits WHERE Codigo=:P0');
        QrUpdL.Params[0].AsInteger := QrwlcCodigo.Value;
        QrUpdL.ExecSQL;
        //
        QrUpdL.SQL.Clear;
        QrUpdL.SQL.Add('INSERT INTO llotecab SET ');
        QrUpdL.SQL.Add('Codigo=:P0, Cliente=:P1, Lote=:P2, Tipo=:P3, ');
        QrUpdL.SQL.Add('Data=:P4, Total=:P5, Itens=:P6');

        QrUpdL.Params[00].AsInteger := QrwlcCodigo.Value;
        QrUpdL.Params[01].AsInteger := QrwlcCliente.Value;
        QrUpdL.Params[02].AsInteger := QrwlcLote.Value;
        QrUpdL.Params[03].AsInteger := QrwlcTipo.Value;
        QrUpdL.Params[04].AsString  := Geral.FDT(QrwlcData.Value, 1);
        QrUpdL.Params[05].AsFloat   := QrwlcTotal.Value;
        QrUpdL.Params[06].AsInteger := QrwlcItens.Value;
        QrUpdL.ExecSQL;
        //
        Qrwli.Close;
        Qrwli.Params[0].AsInteger := QrwlcCodigo.Value;
        Qrwli.Open;
        //
        Qrwli.First;
        QrUpdL.SQL.Clear;
        QrUpdL.SQL.Add('INSERT INTO lloteits SET ');
        QrUpdL.SQL.Add('Codigo=:P0, Controle=:P1, Praca=:P2, Banco=:P3, ');
        QrUpdL.SQL.Add('Agencia=:P4, Conta=:P5, Cheque=:P6, CPF=:P7, ');
        QrUpdL.SQL.Add('Emitente=:P8, Bruto=:P9, Valor=:P10, ');
        QrUpdL.SQL.Add('Vencto=:P11, Duplicata=:P12, Rua=:P13, ');
        QrUpdL.SQL.Add('Numero=:P14, Compl=:P15, Bairro=:P16, ');
        QrUpdL.SQL.Add('Cidade=:P17, UF=:P18, CEP=:P19, Tel1=:P20, ');
        QrUpdL.SQL.Add('IE=:P21, Emissao=:P22');
        QrUpdL.SQL.Add('');
        //Controle = VerificaControle(QrwliCodigo.Value, QrwliControle.Value);
        while not Qrwli.Eof do
        begin
          QrUpdL.Params[00].AsInteger := QrwliCodigo.Value;
          QrUpdL.Params[01].AsInteger := QrwliControle.Value;
          QrUpdL.Params[02].AsInteger := QrwliPraca.Value;
          QrUpdL.Params[03].AsInteger := QrwliBanco.Value;
          QrUpdL.Params[04].AsInteger := QrwliAgencia.Value;
          QrUpdL.Params[05].AsString  := QrwliConta.Value;
          QrUpdL.Params[06].AsInteger := QrwliCheque.Value;
          QrUpdL.Params[07].AsString  := QrwliCPF.Value;
          QrUpdL.Params[08].AsString  := QrwliEmitente.Value;
          QrUpdL.Params[09].AsFloat   := QrwliBruto.Value;
          QrUpdL.Params[10].AsFloat   := QrwliValor.Value;
          QrUpdL.Params[11].AsString  := Geral.FDT(QrwliVencto.Value, 1);
          QrUpdL.Params[12].AsString  := QrwliDuplicata.Value;
          QrUpdL.Params[13].AsString  := QrwliRua.Value;
          QrUpdL.Params[14].AsInteger := QrwliNumero.Value;
          QrUpdL.Params[15].AsString  := QrwliCompl.Value;
          QrUpdL.Params[16].AsString  := QrwliBairro.Value;
          QrUpdL.Params[17].AsString  := QrwliCidade.Value;
          QrUpdL.Params[18].AsString  := QrwliUF.Value;
          QrUpdL.Params[19].AsInteger := QrwliCEP.Value;
          QrUpdL.Params[20].AsString  := QrwliTel1.Value;
          QrUpdL.Params[21].AsString  := QrwliIE.Value;
          QrUpdL.Params[22].AsString  := Geral.FDT(QrwliEmissao.Value, 1);
          QrUpdL.ExecSQL;
          Qrwli.Next;
        end;
        QrSumLLC.Close;
        QrSumLLC.Params[0].AsInteger := QrwlcCodigo.Value;
        QrSumLLC.Open;
        //
        QrUpdN.SQL.Clear;
        QrUpdN.SQL.Add('UPDATE wlotecab SET Baixado=2, Total=:P0, Itens=:P1 ');
        QrUpdN.SQL.Add('WHERE Codigo=:P2');
        QrUpdN.Params[00].AsFloat   := QrSumLLCTotal.Value;
        QrUpdN.Params[01].AsInteger := Trunc(QrSumLLCItens.Value);
        QrUpdN.Params[02].AsInteger := QrwlcCodigo.Value;
        QrUpdN.ExecSQL;
        //
        QrUpdL.SQL.Clear;
        QrUpdL.SQL.Add('UPDATE llotecab SET Baixado=2, Total=:P0, Itens=:P1 ');
        QrUpdL.SQL.Add('WHERE Codigo=:P2');
        QrUpdL.Params[00].AsFloat   := QrSumLLCTotal.Value;
        QrUpdL.Params[01].AsInteger := Trunc(QrSumLLCItens.Value);
        QrUpdL.Params[02].AsInteger := QrwlcCodigo.Value;
        QrUpdL.ExecSQL;
        //
        Qrwlc.Next;
      end;
      Application.MessageBox('Download conclu�do com sucesso!', 'Informa��o',
        MB_OK+MB_ICONINFORMATION);
      Screen.Cursor := crDefault;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
  Result := Baixar;
end;

function TFmPrincipal.BuscaInfoNet(Acao: Integer): Integer;
const
  Creditor = 'Creditor';
var
  BD, Servidor, IP, Senha: String;
  Status: Integer;
begin
  Label1.Caption := 'Buscando informa��es na web';
  Button1.Visible := False;
  Button2.Visible := False;
  Update;
  Application.ProcessMessages;
  //
  DBPri.DisConnect;
  Status := 0;
  try
  Servidor := Geral.ReadAppKey('Server', Creditor, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  IP := Geral.ReadAppKey('IPServer', Creditor, ktString,
    '', HKEY_LOCAL_MACHINE);
  Senha := 'wkljweryhvbirt';
  //  Conex�o na base de dados principal
  DBPri.UserPassword := Senha;
  DBPri.UserName := 'root';
  DBPri.Host := IP;
  DBPri.DatabaseName := 'mysql';// existe com certeza se estiver instalado
  //
  QrAux.Close;
  QrAux.SQL.Clear;
  QrAux.SQL.Add('SHOW DATABASES');
  QrAux.Open;
  BD := '';
  while not QrAux.Eof do
  begin
    if Uppercase(QrAux.FieldByName('Database').AsString)=Uppercase(Creditor) then
      BD := Creditor;
    QrAux.Next;
  end;
  DBPri.Close;
  DBPri.DataBaseName := BD;
  DBPri.Connect;

  //


    QrPri.Close;
    QrPri.Open;
    Status := 1;
    Timer1.Interval := 60000 * QrPriNewWebMins.Value;
    Timer1.Enabled := (QrPriNewWebScan.Value = 1);
    //
    if Acao = 2 then
    begin
      DBNet.Host         := QrPriWeb_Host.Value;
      DBNet.DatabaseName := QrPriWeb_DB.Value;
      DBNet.UserName     := QrPriWeb_User.Value;
      DBNet.UserPassword := QrPriWeb_Pwd.Value;
      DBNet.Disconnect;
      DBNet.Connect;
      Status := 2;
      //
      ReopenQrNet(1);
      //
      Status := 3;
      if QrNet.RecordCount > 0 then
      begin
        if QrNet.RecordCount =  1 then Status := 6 else Status := 7;
        QrNew.Close;
        QrNew.Open;
        if QrNew.RecordCount > 0 then
        begin
          Status := 8;
          QrUpdN.SQL.Clear;
          QrUpdN.SQL.Add('UPDATE wlotecab SET ScanStep=:P0 WHERE Codigo=:P1');
          while not QrNew.Eof do
          begin
            QrUpdN.Params[00].AsInteger := QrNewBaixado.Value;
            QrUpdN.Params[01].AsInteger := QrNewCodigo.Value;
            QrUpdN.ExecSQL;
            //
            QrNew.Next;
          end;
          Status := 9;
        end else
        begin
          if QrNet.RecordCount =  1 then Status := 4 else Status := 5;
        end;
      end;
      //
      if Status > 3 then
      begin
        Button1.Visible := True;
        Button2.Visible := True;
      end;
      Label1.Caption := TextoStatus(Status);
      //
    end else Label1.Caption := 'Inicializando...';
  except
  end;
  Result := Status;
end;

procedure TFmPrincipal.Button1Click(Sender: TObject);
begin
  Application.CreateForm(TFmLotesCab, FmLotesCab);
  FmLotesCab.ShowModal;
  FmLotesCab.Destroy;
end;

procedure TFmPrincipal.Button3Click(Sender: TObject);
begin
  Close;
end;

function TFmPrincipal.ConectaBDn(Pergunta: Boolean): Boolean;
var
  Conectar: Integer;
  BD, IP, PW, ID: String;
begin
  Result := False;
  if not DBPri.Connected then
  begin
    if Pergunta then Conectar := Application.MessageBox(PChar('A base de dados'+
    ' web est� desconectada. Deseja se conectar agora?'), 'Pergunta', MB_YESNO+
    MB_ICONQUESTION) else Conectar := ID_YES;
    if Conectar = ID_YES then
    begin
      try
        IP := QrControleWeb_Host.Value;
        ID := QrControleWeb_User.Value;
        PW := QrControleWeb_Pwd.Value;
        BD := QrControleWeb_DB.Value;
        if FaltaInfo(4,
        [IP,'Servidor',ID,'Usu�rio',PW,'Senha',BD,'Banco de dados'],
        'Conex�o ao MySQL no meu site') then
        begin
          Result := False;
          Exit;
        end;
        //
        DBPri.Connected    := False;
        DBPri.UserPassword := PW;
        DBPri.UserName     := ID;
        DBPri.Host         := IP;
        DBPri.DatabaseName := BD; // J� deve existir (configurado pelo hospedeiro)
        DBPri.Connected := True;
        Result := True;
      except
        Application.MessageBox(PChar('N�o foi poss�vel a conex�o � base de '+
        'dados web!'), 'Aviso', MB_OK+ MB_ICONWARNING);
        raise;
      end;
    end;
  end else Result := True;
end;

procedure TFmPrincipal.ExecutaNaInicializacao(Executa: Boolean; Titulo, Programa: String);
var
  Registry: TRegistry;
begin
  Registry := TRegistry.Create;
  try
    Registry.RootKey := HKEY_LOCAL_MACHINE;
    Registry.OpenKey('\Software\Microsoft\Windows\CurrentVersion\Run', False);
    if Executa then
      Registry.WriteString(Titulo, Programa)
    else
      Registry.WriteString(Titulo, '');
  finally
    Registry.Free;
  end;
end;

procedure TFmPrincipal.Executarnainicializao1Click(Sender: TObject);
begin
  ExecutaNaInicializacao(True, 'Creditor WebScan', Application.ExeName);
end;

procedure TFmPrincipal.Button2Click(Sender: TObject);
begin
  FmPrincipal.BaixaLotesWeb;
end;

end.

