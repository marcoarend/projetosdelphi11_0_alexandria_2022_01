{* TrayIconTest
 * Written by Joshua Bartlett, 2007.
 * http://talljosh.googlepages.com/ttrayicon
 * Project for example/test for TTrayIcon component.
 *
 * This program has been placed in the public domain by its author.
 * If you modify and redistribute it, please note your name and the date
 * of modification in this header comment.
 *
 *}

program CredScan2;

uses
  Forms,
  Principal in 'Principal.pas' {FmPrincipal},
  dmkGeral in 'C:\Componentes delphi 2007\Dermatek\EmUso\dmkGeral.pas',
  LotesCab in 'LotesCab.pas' {FmLotesCab};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFmPrincipal, FmPrincipal);
  Application.CreateForm(TFmLotesCab, FmLotesCab);
  Application.Run;
end.
