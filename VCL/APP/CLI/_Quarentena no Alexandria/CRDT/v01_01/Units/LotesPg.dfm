object FmLotesPG: TFmLotesPG
  Left = 334
  Top = 176
  Caption = 'Pagamento de Compra de Cr'#233'ditos'
  ClientHeight = 285
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label12: TLabel
    Left = 300
    Top = 104
    Width = 28
    Height = 13
    Caption = 'Conta'
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 189
    Align = alClient
    TabOrder = 0
    object Panel1: TPanel
      Left = 1
      Top = 97
      Width = 782
      Height = 91
      Align = alClient
      TabOrder = 1
      object Label75: TLabel
        Left = 12
        Top = 48
        Width = 39
        Height = 13
        Caption = 'Carteira:'
      end
      object Label1: TLabel
        Left = 12
        Top = 8
        Width = 27
        Height = 13
        Caption = 'Valor:'
      end
      object Label3: TLabel
        Left = 96
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object Label4: TLabel
        Left = 580
        Top = 48
        Width = 118
        Height = 13
        Caption = 'Coment'#225'rios (Descri'#231#227'o):'
      end
      object Label33: TLabel
        Left = 208
        Top = 8
        Width = 143
        Height = 13
        Caption = 'Leitura pela banda magn'#233'tica:'
      end
      object Label34: TLabel
        Left = 496
        Top = 8
        Width = 34
        Height = 13
        Caption = 'Banco:'
      end
      object Label35: TLabel
        Left = 540
        Top = 8
        Width = 42
        Height = 13
        Caption = 'Ag'#234'ncia:'
      end
      object Label37: TLabel
        Left = 592
        Top = 8
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object Label38: TLabel
        Left = 708
        Top = 8
        Width = 40
        Height = 13
        Caption = 'Cheque:'
      end
      object EdCarteira: TdmkEditCB
        Left = 12
        Top = 64
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBCarteira
        IgnoraDBLookupComboBox = False
      end
      object CBCarteira: TdmkDBLookupComboBox
        Left = 78
        Top = 64
        Width = 499
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCarteiras
        TabOrder = 8
        dmkEditCB = EdCarteira
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdValor: TdmkEdit
        Left = 12
        Top = 24
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object TPData: TDateTimePicker
        Left = 96
        Top = 24
        Width = 109
        Height = 21
        Date = 38731.525819224500000000
        Time = 38731.525819224500000000
        TabOrder = 1
      end
      object EdTxt: TdmkEdit
        Left = 580
        Top = 64
        Width = 193
        Height = 21
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdBanda1: TdmkEdit
        Left = 208
        Top = 24
        Width = 285
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 34
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdBanda1Change
      end
      object EdBanco1: TdmkEdit
        Left = 496
        Top = 24
        Width = 41
        Height = 21
        Alignment = taRightJustify
        Color = clWhite
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 3
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAgencia1: TdmkEdit
        Left = 540
        Top = 24
        Width = 49
        Height = 21
        Alignment = taRightJustify
        Color = clWhite
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 4
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdConta1: TdmkEdit
        Left = 592
        Top = 24
        Width = 113
        Height = 21
        Color = clWhite
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCheque1: TdmkEdit
        Left = 708
        Top = 24
        Width = 65
        Height = 21
        Alignment = taRightJustify
        Color = clWhite
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdCMC_7_1: TEdit
        Left = 380
        Top = 12
        Width = 37
        Height = 21
        MaxLength = 30
        TabOrder = 10
        Visible = False
        OnChange = EdCMC_7_1Change
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 96
      Align = alTop
      TabOrder = 0
      object Label2: TLabel
        Left = 12
        Top = 4
        Width = 69
        Height = 13
        Caption = 'Creditado [F4]:'
      end
      object Label5: TLabel
        Left = 564
        Top = 4
        Width = 34
        Height = 13
        Caption = 'Banco:'
      end
      object Label6: TLabel
        Left = 608
        Top = 4
        Width = 42
        Height = 13
        Caption = 'Ag'#234'ncia:'
      end
      object Label7: TLabel
        Left = 660
        Top = 4
        Width = 31
        Height = 13
        Caption = 'Conta:'
      end
      object Label10: TLabel
        Left = 448
        Top = 4
        Width = 109
        Height = 13
        Caption = 'CPF / CNPJ Creditado:'
      end
      object PnCreditado: TPanel
        Left = 1
        Top = 48
        Width = 780
        Height = 47
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 6
        Visible = False
        object Label14: TLabel
          Left = 8
          Top = 4
          Width = 28
          Height = 13
          Caption = 'Nome'
          FocusControl = DBEdit5
        end
        object Label8: TLabel
          Left = 408
          Top = 4
          Width = 20
          Height = 13
          Caption = 'CPF'
          FocusControl = DBEdit1
        end
        object Label9: TLabel
          Left = 508
          Top = 4
          Width = 31
          Height = 13
          Caption = 'Banco'
          FocusControl = DBEdit2
        end
        object Label11: TLabel
          Left = 576
          Top = 4
          Width = 39
          Height = 13
          Caption = 'Agencia'
          FocusControl = DBEdit3
        end
        object Label13: TLabel
          Left = 644
          Top = 4
          Width = 28
          Height = 13
          Caption = 'Conta'
          FocusControl = DBEdit4
        end
        object DBEdit5: TDBEdit
          Left = 8
          Top = 20
          Width = 393
          Height = 21
          DataField = 'Nome'
          DataSource = DsCreditados
          TabOrder = 0
        end
        object DBEdit1: TDBEdit
          Left = 408
          Top = 20
          Width = 94
          Height = 21
          DataField = 'CPF'
          DataSource = DsCreditados
          TabOrder = 1
        end
        object DBEdit2: TDBEdit
          Left = 508
          Top = 20
          Width = 64
          Height = 21
          DataField = 'Banco'
          DataSource = DsCreditados
          TabOrder = 2
        end
        object DBEdit3: TDBEdit
          Left = 576
          Top = 20
          Width = 64
          Height = 21
          DataField = 'Agencia'
          DataSource = DsCreditados
          TabOrder = 3
        end
        object DBEdit4: TDBEdit
          Left = 644
          Top = 20
          Width = 124
          Height = 21
          DataField = 'Conta'
          DataSource = DsCreditados
          TabOrder = 4
        end
      end
      object CBCreditado: TDBLookupComboBox
        Left = 12
        Top = 20
        Width = 433
        Height = 21
        KeyField = 'LastPaymt'
        ListField = 'Nome'
        ListSource = DsCreditados
        TabOrder = 0
        OnKeyDown = CBCreditadoKeyDown
      end
      object EdCreditado: TdmkEdit
        Left = 12
        Top = 36
        Width = 433
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnKeyDown = EdCreditadoKeyDown
      end
      object EdBanco2: TdmkEdit
        Left = 564
        Top = 20
        Width = 41
        Height = 21
        Alignment = taRightJustify
        Color = clWhite
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 3
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAgencia2: TdmkEdit
        Left = 608
        Top = 20
        Width = 49
        Height = 21
        Alignment = taRightJustify
        Color = clWhite
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 4
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdConta2: TdmkEdit
        Left = 660
        Top = 20
        Width = 113
        Height = 21
        Color = clWhite
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCPF2: TdmkEdit
        Left = 448
        Top = 20
        Width = 113
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnExit = EdCPF2Exit
      end
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 237
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 12
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object BtCheque: TBitBtn
      Tag = 21
      Left = 198
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Cheque'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtChequeClick
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 686
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtSaidaClick
    end
    object BtEnvelope: TBitBtn
      Tag = 156
      Left = 294
      Top = 4
      Width = 90
      Height = 40
      Caption = '&DOC TED'
      NumGlyphs = 2
      TabOrder = 3
      OnClick = BtEnvelopeClick
    end
    object BitBtn1: TBitBtn
      Tag = 174
      Left = 486
      Top = 4
      Width = 90
      Height = 40
      Caption = '&R'#225'pido'
      NumGlyphs = 2
      TabOrder = 4
      OnClick = BitBtn1Click
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Pagamento de Compra de Cr'#233'ditos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCarteirasCalcFields
    SQL.Strings = (
      'SELECT DISTINCT ca.*, '
      'CASE WHEN ba.Codigo=0 THEN "" ELSE ba.Nome2 END NOME2DOBANCO, '
      
        'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ELSE fo.Nome END NOMEFOR' +
        'NECEI'
      'FROM carteiras ca'
      'LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco'
      'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI'
      'WHERE ca.Codigo>0')
    Left = 116
    Top = 8
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrCarteirasInicial: TFloatField
      FieldName = 'Inicial'
    end
    object QrCarteirasBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrCarteirasID: TWideStringField
      FieldName = 'ID'
      Size = 100
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrCarteirasID_Fat: TWideStringField
      FieldName = 'ID_Fat'
      Size = 50
    end
    object QrCarteirasSaldo: TFloatField
      FieldName = 'Saldo'
    end
    object QrCarteirasEmCaixa: TFloatField
      FieldName = 'EmCaixa'
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrCarteirasPagRec: TIntegerField
      FieldName = 'PagRec'
    end
    object QrCarteirasDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
      Required = True
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
      Required = True
    end
    object QrCarteirasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCarteirasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCarteirasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCarteirasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCarteirasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCarteirasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 100
    end
    object QrCarteirasTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
      Required = True
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrCarteirasAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrCarteirasConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCarteirasCheque1: TIntegerField
      FieldName = 'Cheque1'
    end
    object QrCarteirasContato1: TWideStringField
      FieldName = 'Contato1'
      Required = True
      Size = 100
    end
    object QrCarteirasNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrCarteirasNOME2DOBANCO: TWideStringField
      FieldName = 'NOME2DOBANCO'
      Size = 100
    end
    object QrCarteirasNOMETIPODOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPODOC'
      Size = 30
      Calculated = True
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 144
    Top = 8
  end
  object QrLocCC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM carteiras'
      'WHERE TipoDoc=1'
      'AND Banco1=:P0'
      'AND Agencia1=:P1'
      'AND Conta1=:P2')
    Left = 85
    Top = 37
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrLocCCCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCCTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrLocCCNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrLocCCInicial: TFloatField
      FieldName = 'Inicial'
    end
    object QrLocCCBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLocCCID: TWideStringField
      FieldName = 'ID'
      Size = 100
    end
    object QrLocCCFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrLocCCID_Fat: TWideStringField
      FieldName = 'ID_Fat'
      Size = 50
    end
    object QrLocCCSaldo: TFloatField
      FieldName = 'Saldo'
    end
    object QrLocCCEmCaixa: TFloatField
      FieldName = 'EmCaixa'
    end
    object QrLocCCFechamento: TIntegerField
      FieldName = 'Fechamento'
    end
    object QrLocCCPrazo: TSmallintField
      FieldName = 'Prazo'
    end
    object QrLocCCPagRec: TIntegerField
      FieldName = 'PagRec'
    end
    object QrLocCCDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
    end
    object QrLocCCExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
    end
    object QrLocCCForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLocCCLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocCCDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocCCDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocCCUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocCCUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLocCCNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 100
    end
    object QrLocCCTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrLocCCBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrLocCCAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrLocCCConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrLocCCCheque1: TIntegerField
      FieldName = 'Cheque1'
    end
  end
  object QrCreditados: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCreditadosAfterScroll
    SQL.Strings = (
      'SELECT MAX(ci.LastPaymt) LastPaymt, cr.* '
      'FROM creditadosits ci'
      'LEFT JOIN creditados cr ON cr.BAC=ci.BAC'
      'WHERE ci.Cliente=:P0'
      'GROUP BY cr.Nome')
    Left = 181
    Top = 161
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCreditadosLastPaymt: TIntegerField
      FieldName = 'LastPaymt'
      Required = True
    end
    object QrCreditadosBAC: TWideStringField
      FieldName = 'BAC'
      Required = True
    end
    object QrCreditadosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCreditadosCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrCreditadosBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrCreditadosAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrCreditadosConta: TWideStringField
      FieldName = 'Conta'
    end
  end
  object DsCreditados: TDataSource
    DataSet = QrCreditados
    Left = 209
    Top = 161
  end
  object QrLocCr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(LastPaymt) LastPaymt'
      'FROM creditadosits WHERE Cliente=:P0')
    Left = 237
    Top = 161
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCrLastPaymt: TIntegerField
      FieldName = 'LastPaymt'
    end
  end
  object QrBanco1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome FROM bancos'
      'WHERE Codigo=:P0')
    Left = 341
    Top = 113
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBanco1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object frxDOC_TED: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.442033599500000000
    ReportOptions.LastChange = 39718.442033599500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxDOC_TEDGetValue
    Left = 141
    Top = 37
    Datasets = <
      item
        DataSet = frxDsCarteiras
        DataSetName = 'frxDsCarteiras'
      end
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 994.016390000000000000
        Width = 737.008350000000000000
        object Memo31: TfrxMemoView
          Left = 587.574830000000000000
          Top = 8.046380000000000000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 675.574830000000000000
          Top = 8.046380000000000000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        Height = 916.000000000000000000
        Top = 18.897650000000000000
        Width = 737.008350000000000000
        object Memo1: TfrxMemoView
          Left = 34.000000000000000000
          Top = 83.102350000000000000
          Width = 172.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'AO')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 34.000000000000000000
          Top = 109.102350000000000000
          Width = 676.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCarteiras."NOME2DOBANCO"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 34.000000000000000000
          Top = 133.102350000000000000
          Width = 676.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCarteiras."Nome2"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 34.000000000000000000
          Top = 181.102350000000000000
          Width = 676.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'A/C.: [frxDsCarteiras."Contato1"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 34.000000000000000000
          Top = 229.102350000000000000
          Width = 132.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Ref.: Solicitac'#227'o de emiss'#227'o de')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 166.000000000000000000
          Top = 229.102350000000000000
          Width = 132.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCarteiras."NOMETIPODOC"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 34.000000000000000000
          Top = 273.102350000000000000
          Width = 132.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'C/C D'#233'bito:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 166.000000000000000000
          Top = 273.102350000000000000
          Width = 132.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCarteiras."Conta1"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 34.000000000000000000
          Top = 321.102350000000000000
          Width = 676.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              'Solicito a emiss'#227'o nesta data de [frxDsCarteiras."NOMETIPODOC"] ' +
              'ao creditado a seguir:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 34.000000000000000000
          Top = 349.102350000000000000
          Width = 132.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold, fsUnderline]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsCarteiras."NOMETIPODOC"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 34.000000000000000000
          Top = 377.102350000000000000
          Width = 68.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Valor:')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 106.000000000000000000
          Top = 377.102350000000000000
          Width = 604.000000000000000000
          Height = 40.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_VALOR]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 34.000000000000000000
          Top = 421.102350000000000000
          Width = 68.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Banco:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 106.000000000000000000
          Top = 421.102350000000000000
          Width = 604.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_BANCO]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 34.000000000000000000
          Top = 445.102350000000000000
          Width = 68.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Ag'#234'ncia:')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 106.000000000000000000
          Top = 445.102350000000000000
          Width = 604.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_AGENCIA]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 34.000000000000000000
          Top = 469.102350000000000000
          Width = 68.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Conta corrente:')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 106.000000000000000000
          Top = 469.102350000000000000
          Width = 604.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CONTA]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 34.000000000000000000
          Top = 493.102350000000000000
          Width = 68.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Creditado:')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 106.000000000000000000
          Top = 493.102350000000000000
          Width = 604.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CREDITADO]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 34.000000000000000000
          Top = 517.102350000000000000
          Width = 68.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CNPJ / CPF:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 106.000000000000000000
          Top = 517.102350000000000000
          Width = 604.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VARF_CNPJ]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 2.000000000000000000
          Top = 893.102350000000000000
          Width = 720.000000000000000000
          Height = 60.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -15
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."E_CUC"] :: [frxDsDono."TE1_TXT"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 38.000000000000000000
          Top = 649.102350000000000000
          Width = 671.000000000000000000
          Height = 24.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_CIDADE_E_DATA]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 34.000000000000000000
          Top = 573.102350000000000000
          Width = 676.000000000000000000
          Height = 48.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            
              'D'#250'vidas? Entrar em contato com [frxDsDono."EContato"] - [frxDsDo' +
              'no."TE1_TXT"]  /  [frxDsDono."CEL_TXT"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 38.000000000000000000
          Top = 761.102350000000000000
          Width = 671.000000000000000000
          Height = 44.000000000000000000
          ShowHint = False
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]'
            '[frxDsDono."CNPJ_TXT"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsCarteiras: TfrxDBDataset
    UserName = 'frxDsCarteiras'
    CloseDataSource = False
    DataSet = QrCarteiras
    BCDToCurrency = False
    Left = 113
    Top = 37
  end
  object PMCheque: TPopupMenu
    OnPopup = PMChequePopup
    Left = 528
    Top = 209
    object Pertochek1: TMenuItem
      Caption = '&Pertochek'
      OnClick = Pertochek1Click
    end
    object Texto1: TMenuItem
      Caption = '&Texto'
      object Portador1: TMenuItem
        Caption = '&Portador'
        OnClick = Portador1Click
      end
      object Nominal1: TMenuItem
        Caption = '&Nominal'
        OnClick = Nominal1Click
      end
    end
  end
end
