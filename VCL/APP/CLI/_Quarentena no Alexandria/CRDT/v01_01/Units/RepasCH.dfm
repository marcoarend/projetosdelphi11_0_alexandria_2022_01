object FmRepasCH: TFmRepasCH
  Left = 368
  Top = 194
  Caption = 'Repasses de Cheques'
  ClientHeight = 649
  ClientWidth = 1008
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 601
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 552
      Width = 1006
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 584
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtDesisteClick
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 820
      Height = 196
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label75: TLabel
        Left = 120
        Top = 8
        Width = 44
        Height = 13
        Caption = 'Coligado:'
      end
      object Label3: TLabel
        Left = 460
        Top = 8
        Width = 66
        Height = 13
        Caption = 'Data repasse:'
      end
      object Label16: TLabel
        Left = 572
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Hora:'
      end
      object Label17: TLabel
        Left = 16
        Top = 52
        Width = 129
        Height = 13
        Caption = 'Configura'#231#227'o de cobran'#231'a:'
      end
      object Label18: TLabel
        Left = 492
        Top = 52
        Width = 145
        Height = 13
        Caption = 'Ag'#234'ncia/DV entrega cheques:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdColigado: TdmkEditCB
        Left = 120
        Top = 24
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBColigado
        IgnoraDBLookupComboBox = False
      end
      object CBColigado: TdmkDBLookupComboBox
        Left = 188
        Top = 24
        Width = 269
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECOLIGADO'
        ListSource = DsColigado
        TabOrder = 2
        dmkEditCB = EdColigado
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPData: TdmkEditDateTimePicker
        Left = 460
        Top = 24
        Width = 108
        Height = 21
        Date = 39796.835646099530000000
        Time = 39796.835646099530000000
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object TPHora: TDateTimePicker
        Left = 572
        Top = 24
        Width = 69
        Height = 21
        Date = 39198.000000000000000000
        Time = 39198.000000000000000000
        Kind = dtkTime
        TabOrder = 4
      end
      object CBConfigBB: TdmkDBLookupComboBox
        Left = 76
        Top = 68
        Width = 413
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsConfigs
        TabOrder = 5
        dmkEditCB = EdConfigBB
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdConfigBB: TdmkEditCB
        Left = 16
        Top = 68
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBConfigBB
        IgnoraDBLookupComboBox = False
      end
      object EdAgencRecNu: TdmkEdit
        Left = 492
        Top = 68
        Width = 113
        Height = 21
        Alignment = taRightJustify
        MaxLength = 5
        TabOrder = 7
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 5
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAgencRecDV: TdmkEdit
        Left = 604
        Top = 68
        Width = 37
        Height = 21
        Alignment = taCenter
        MaxLength = 1
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 601
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 1
      Top = 444
      Width = 1006
      Height = 5
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 349
      ExplicitWidth = 805
    end
    object Panel1: TPanel
      Left = 1
      Top = 65
      Width = 1006
      Height = 296
      Align = alTop
      Color = clMaroon
      TabOrder = 1
      object PainelItens: TPanel
        Left = 1
        Top = 84
        Width = 1004
        Height = 211
        Align = alBottom
        Color = clAppWorkSpace
        TabOrder = 0
        Visible = False
        object GradeCHs: TDBGrid
          Left = 1
          Top = 89
          Width = 1002
          Height = 121
          Align = alClient
          DataSource = DsCheques
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Lote'
              Title.Caption = 'Border'#244
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Banco'
              Title.Caption = 'Bco.'
              Width = 27
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Agencia'
              Title.Caption = 'Ag.'
              Width = 31
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conta'
              Title.Caption = 'Conta corente'
              Width = 76
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cheque'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DCompra'
              Title.Caption = 'D. Compra'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DDeposito'
              Title.Caption = 'D. Dep'#243'sito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPF_TXT'
              Title.Caption = 'CPF / CNPJ'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Width = 208
              Visible = True
            end>
        end
        object Panel4: TPanel
          Left = 1
          Top = 1
          Width = 1002
          Height = 88
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object Label36: TLabel
            Left = 8
            Top = 4
            Width = 143
            Height = 13
            Caption = 'Leitura pela banda magn'#233'tica:'
          end
          object Label11: TLabel
            Left = 8
            Top = 44
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object Label10: TLabel
            Left = 296
            Top = 4
            Width = 34
            Height = 13
            Caption = 'Banco:'
          end
          object Label12: TLabel
            Left = 340
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Ag'#234'ncia:'
          end
          object Label13: TLabel
            Left = 392
            Top = 4
            Width = 31
            Height = 13
            Caption = 'Conta:'
          end
          object Label32: TLabel
            Left = 508
            Top = 4
            Width = 40
            Height = 13
            Caption = 'Cheque:'
          end
          object Label8: TLabel
            Left = 576
            Top = 4
            Width = 67
            Height = 13
            Caption = 'CPF Emitente:'
          end
          object Label31: TLabel
            Left = 592
            Top = 44
            Width = 79
            Height = 13
            Caption = 'Nome do banco:'
            FocusControl = DBEdit5
          end
          object Label14: TLabel
            Left = 368
            Top = 44
            Width = 54
            Height = 13
            Caption = 'Taxa [F12]:'
          end
          object Label15: TLabel
            Left = 320
            Top = 44
            Width = 40
            Height = 13
            Caption = 'Border'#244':'
          end
          object EdBanda: TdmkEdit
            Left = 8
            Top = 20
            Width = 285
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 34
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnChange = EdBandaChange
            OnKeyDown = EdBandaKeyDown
          end
          object EdCliente: TdmkEditCB
            Left = 8
            Top = 60
            Width = 45
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdClienteChange
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 56
            Top = 60
            Width = 261
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMECLIENTE'
            ListSource = DsClientes
            TabOrder = 7
            dmkEditCB = EdCliente
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdBanco: TdmkEdit
            Left = 296
            Top = 20
            Width = 41
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 3
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdBancoChange
            OnExit = EdBancoExit
          end
          object EdAgencia: TdmkEdit
            Left = 340
            Top = 20
            Width = 49
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 4
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnExit = EdAgenciaExit
          end
          object EdConta: TdmkEdit
            Left = 392
            Top = 20
            Width = 113
            Height = 21
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnExit = EdContaExit
          end
          object EdCheque: TdmkEdit
            Left = 508
            Top = 20
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnExit = EdChequeExit
          end
          object EdCPF: TdmkEdit
            Left = 576
            Top = 20
            Width = 113
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnExit = EdCPFExit
          end
          object DBEdit5: TDBEdit
            Left = 588
            Top = 60
            Width = 197
            Height = 21
            TabStop = False
            DataField = 'Nome'
            DataSource = DsBanco
            TabOrder = 10
          end
          object EdTaxa: TdmkEdit
            Left = 368
            Top = 60
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 9
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnKeyDown = EdTaxaKeyDown
          end
          object CkInap: TCheckBox
            Left = 436
            Top = 60
            Width = 149
            Height = 17
            Caption = 'Mostrar cheques vencidos.'
            TabOrder = 11
            Visible = False
            OnClick = CkInapClick
          end
          object EdBordero: TdmkEdit
            Left = 320
            Top = 60
            Width = 45
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnExit = EdBorderoExit
          end
          object MeAviso: TMemo
            Left = 692
            Top = 4
            Width = 89
            Height = 49
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Lines.Strings = (
              'Ctrl + Mouse '
              'seleciona mais'
              'de 1 cheque.')
            ParentFont = False
            ReadOnly = True
            TabOrder = 12
          end
        end
      end
      object GradeRepas: TDBGrid
        Left = 1
        Top = 1
        Width = 1004
        Height = 59
        Align = alTop
        DataSource = DsRepasIts
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Banco'
            Title.Caption = 'Bco.'
            Width = 27
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Agencia'
            Title.Caption = 'Ag.'
            Width = 31
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Conta'
            Title.Caption = 'Conta corrente'
            Width = 76
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cheque'
            Width = 49
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Taxa'
            Title.Caption = '% Taxa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'JurosP'
            Title.Caption = '% Juros'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'JurosV'
            Title.Caption = '$ Juros'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIQUIDO'
            Title.Caption = 'L'#205'QUIDO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CPF_TXT'
            Title.Caption = 'CPF / CNPJ'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Emitente'
            Width = 185
            Visible = True
          end>
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 64
      Align = alTop
      Enabled = False
      TabOrder = 0
      object Panel7: TPanel
        Left = 1
        Top = 1
        Width = 704
        Height = 62
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 8
          Top = 8
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 76
          Top = 8
          Width = 44
          Height = 13
          Caption = 'Coligado:'
          FocusControl = DBEdNome
        end
        object Label5: TLabel
          Left = 388
          Top = 8
          Width = 53
          Height = 13
          Caption = 'Valor base:'
          FocusControl = DBEdit2
        end
        object Label6: TLabel
          Left = 464
          Top = 8
          Width = 37
          Height = 13
          Caption = '$ Juros:'
          FocusControl = DBEdit3
        end
        object Label7: TLabel
          Left = 540
          Top = 8
          Width = 39
          Height = 13
          Caption = 'L'#237'quido:'
          FocusControl = DBEdit4
        end
        object DBEdCodigo: TDBEdit
          Left = 8
          Top = 24
          Width = 65
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsRepas
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          MaxLength = 1
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdNome: TDBEdit
          Left = 76
          Top = 24
          Width = 309
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'NOMECOLIGADO'
          DataSource = DsRepas
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 33
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object DBEdit2: TDBEdit
          Left = 388
          Top = 24
          Width = 72
          Height = 21
          DataField = 'Total'
          DataSource = DsRepas
          TabOrder = 2
        end
        object DBEdit3: TDBEdit
          Left = 464
          Top = 24
          Width = 72
          Height = 21
          DataField = 'JurosV'
          DataSource = DsRepas
          TabOrder = 3
        end
        object DBEdit4: TDBEdit
          Left = 540
          Top = 24
          Width = 72
          Height = 21
          DataField = 'SALDO'
          DataSource = DsRepas
          TabOrder = 4
        end
        object Progress: TProgressBar
          Left = 616
          Top = 24
          Width = 85
          Height = 17
          TabOrder = 5
          Visible = False
        end
      end
      object GroupBox1: TGroupBox
        Left = 853
        Top = 1
        Width = 148
        Height = 62
        Align = alLeft
        Caption = ' '#218'ltima gera'#231#227'o: '
        TabOrder = 1
        object Label4: TLabel
          Left = 6
          Top = 16
          Width = 26
          Height = 13
          Caption = 'Data:'
          FocusControl = DBEdit1
        end
        object Label19: TLabel
          Left = 76
          Top = 16
          Width = 26
          Height = 13
          Caption = 'Hora:'
          FocusControl = DBEdit6
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 32
          Width = 64
          Height = 21
          DataField = 'MyDATAS'
          DataSource = DsRepas
          TabOrder = 0
        end
        object DBEdit6: TDBEdit
          Left = 76
          Top = 32
          Width = 64
          Height = 21
          DataField = 'HoraS'
          DataSource = DsRepas
          TabOrder = 1
        end
      end
      object GroupBox2: TGroupBox
        Left = 705
        Top = 1
        Width = 148
        Height = 62
        Align = alLeft
        Caption = ' Cria'#231#227'o: '
        TabOrder = 2
        object Label20: TLabel
          Left = 6
          Top = 16
          Width = 26
          Height = 13
          Caption = 'Data:'
          FocusControl = DBEdit7
        end
        object Label21: TLabel
          Left = 76
          Top = 16
          Width = 26
          Height = 13
          Caption = 'Hora:'
          FocusControl = DBEdit8
        end
        object DBEdit7: TDBEdit
          Left = 8
          Top = 32
          Width = 64
          Height = 21
          DataField = 'MyDATAG'
          DataSource = DsRepas
          TabOrder = 0
        end
        object DBEdit8: TDBEdit
          Left = 76
          Top = 32
          Width = 64
          Height = 21
          DataField = 'Hora'
          DataSource = DsRepas
          TabOrder = 1
        end
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 552
      Width = 1006
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object PainelConfI: TPanel
        Left = 0
        Top = 0
        Width = 1006
        Height = 48
        Align = alBottom
        TabOrder = 1
        Visible = False
        object BtConfirma2: TBitBtn
          Tag = 14
          Left = 12
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirma2Click
        end
        object BtDesiste2: TBitBtn
          Tag = 13
          Left = 596
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Sair'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtDesiste2Click
        end
        object BtExclui2: TBitBtn
          Tag = 12
          Left = 276
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Exclui'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExclui2Click
        end
      end
      object PainelControle: TPanel
        Left = 108
        Top = 16
        Width = 702
        Height = 48
        Color = clAppWorkSpace
        TabOrder = 0
        object LaRegistro: TLabel
          Left = 173
          Top = 1
          Width = 26
          Height = 13
          Caption = '[N]: 0'
        end
        object Panel5: TPanel
          Left = 1
          Top = 1
          Width = 172
          Height = 46
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object SpeedButton4: TBitBtn
            Tag = 4
            Left = 128
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Hint = 'ltimo registro'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = SpeedButton4Click
          end
          object SpeedButton3: TBitBtn
            Tag = 3
            Left = 88
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Hint = 'Pr'#243'ximo registro'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = SpeedButton3Click
          end
          object SpeedButton2: TBitBtn
            Tag = 2
            Left = 48
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Hint = 'Registro anterior'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = SpeedButton2Click
          end
          object SpeedButton1: TBitBtn
            Tag = 1
            Left = 8
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Hint = 'Primeiro registro'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = SpeedButton1Click
          end
        end
        object Panel6: TPanel
          Left = 220
          Top = 1
          Width = 481
          Height = 46
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtCheque: TBitBtn
            Tag = 146
            Left = 99
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Altera banco atual'
            Caption = '&Cheque'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtChequeClick
          end
          object BtReceitas: TBitBtn
            Tag = 143
            Left = 8
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Inclui novo banco'
            Caption = '&Lote'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtReceitasClick
          end
          object BtImportar: TBitBtn
            Tag = 147
            Left = 190
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = 'Im&porta'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtImportarClick
          end
          object Panel2: TPanel
            Left = 372
            Top = 0
            Width = 109
            Height = 46
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 3
            object BtSaida: TBitBtn
              Tag = 13
              Left = 3
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
          object BtGera: TBitBtn
            Tag = 147
            Left = 282
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Gera'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = BtGeraClick
          end
        end
      end
    end
    object Memo1: TMemo
      Left = 1
      Top = 449
      Width = 1006
      Height = 103
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      WordWrap = False
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = '                              Repasses de Cheques'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TLabel
      Left = 925
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 699
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 481
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object BtImpCalculado: TBitBtn
        Tag = 144
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImpCalculadoClick
      end
      object BtImpPesquisa: TBitBtn
        Tag = 145
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImpPesquisaClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsRepas: TDataSource
    DataSet = QrRepas
    Left = 544
    Top = 21
  end
  object QrRepas: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrRepasBeforeOpen
    AfterOpen = QrRepasAfterOpen
    AfterScroll = QrRepasAfterScroll
    OnCalcFields = QrRepasCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECOLIGADO, en.FatorCompra,'
      're.*, (re.Total-re.JurosV) SALDO'
      'FROM repas re'
      'LEFT JOIN entidades en ON en.Codigo=re.Coligado'
      'WHERE re.Codigo > 0')
    Left = 516
    Top = 21
    object QrRepasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'repas.Codigo'
    end
    object QrRepasColigado: TIntegerField
      FieldName = 'Coligado'
      Origin = 'repas.Coligado'
    end
    object QrRepasData: TDateField
      FieldName = 'Data'
      Origin = 'repas.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRepasTotal: TFloatField
      FieldName = 'Total'
      Origin = 'repas.Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepasJurosV: TFloatField
      FieldName = 'JurosV'
      Origin = 'repas.JurosV'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepasNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
    object QrRepasSALDO: TFloatField
      FieldName = 'SALDO'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepasFatorCompra: TFloatField
      FieldName = 'FatorCompra'
      Origin = 'entidades.FatorCompra'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrRepasConfigBB: TIntegerField
      FieldName = 'ConfigBB'
      Origin = 'repas.ConfigBB'
    end
    object QrRepasHora: TTimeField
      FieldName = 'Hora'
      Origin = 'repas.Hora'
      Required = True
    end
    object QrRepasDataS: TDateField
      FieldName = 'DataS'
      Origin = 'repas.DataS'
      Required = True
    end
    object QrRepasHoraS: TTimeField
      FieldName = 'HoraS'
      Origin = 'repas.HoraS'
      Required = True
    end
    object QrRepasAgencRecNu: TIntegerField
      FieldName = 'AgencRecNu'
      Origin = 'repas.AgencRecNu'
      Required = True
      DisplayFormat = '00000'
    end
    object QrRepasAgencRecDV: TWideStringField
      FieldName = 'AgencRecDV'
      Origin = 'repas.AgencRecDV'
      Required = True
      Size = 1
    end
    object QrRepasMyDATAG: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MyDATAG'
      Size = 10
      Calculated = True
    end
    object QrRepasMyDATAS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MyDATAS'
      Size = 10
      Calculated = True
    end
  end
  object QrColigado: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrRepasBeforeOpen
    AfterOpen = QrRepasAfterOpen
    AfterScroll = QrRepasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECOLIGADO'
      'FROM entidades'
      'WHERE Fornece2="V"')
    Left = 136
    Top = 65
    object QrColigadoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrColigadoNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
  end
  object DsColigado: TDataSource
    DataSet = QrColigado
    Left = 164
    Top = 65
  end
  object QrRepasIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrRepasItsAfterOpen
    AfterClose = QrRepasItsAfterClose
    OnCalcFields = QrRepasItsCalcFields
    SQL.Strings = (
      'SELECT li.Banco, li.Agencia, li.Conta, li.Cheque, '
      'li.Emitente, li.CPF, li.DDeposito, li.Vencto, '
      '(ri.Valor - ri.JurosV) LIQUIDO, li.Praca, li.Tipific, '
      'ri.* '
      'FROM repasits ri'
      'LEFT JOIN lotesits li ON li.Controle=ri.Origem'
      'WHERE ri.Codigo=:P0')
    Left = 572
    Top = 21
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRepasItsBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrRepasItsAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrRepasItsConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrRepasItsCheque: TIntegerField
      FieldName = 'Cheque'
      DisplayFormat = '000000'
    end
    object QrRepasItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrRepasItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRepasItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrRepasItsOrigem: TIntegerField
      FieldName = 'Origem'
      Required = True
    end
    object QrRepasItsDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrRepasItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrRepasItsTaxa: TFloatField
      FieldName = 'Taxa'
      Required = True
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrRepasItsJurosP: TFloatField
      FieldName = 'JurosP'
      Required = True
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrRepasItsJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrRepasItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrRepasItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrRepasItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrRepasItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrRepasItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrRepasItsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrRepasItsCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrRepasItsDDeposito: TDateField
      FieldName = 'DDeposito'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRepasItsVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRepasItsLIQUIDO: TFloatField
      FieldName = 'LIQUIDO'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrRepasItsPraca: TIntegerField
      FieldName = 'Praca'
    end
    object QrRepasItsTipific: TSmallintField
      FieldName = 'Tipific'
    end
    object QrRepasItsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrRepasItsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
  end
  object DsRepasIts: TDataSource
    DataSet = QrRepasIts
    Left = 600
    Top = 21
  end
  object QrBanco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM bancos'
      'WHERE Codigo=:P0')
    Left = 425
    Top = 205
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBancoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrBancoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBancoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBancoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBancoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBancoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBancoSite: TWideStringField
      FieldName = 'Site'
      Size = 255
    end
  end
  object DsBanco: TDataSource
    DataSet = QrBanco
    Left = 453
    Top = 205
  end
  object QrCheques: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrChequesAfterOpen
    AfterClose = QrChequesAfterClose
    OnCalcFields = QrChequesCalcFields
    SQL.Strings = (
      'SELECT lo.Lote, li.* '
      'FROM lotesits li'
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Tipo=0'
      'AND li.Repassado=0')
    Left = 720
    Top = 20
    object QrChequesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrChequesControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrChequesComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrChequesBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrChequesAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrChequesConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrChequesCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrChequesCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrChequesEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrChequesValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrChequesEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrChequesTxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrChequesTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrChequesVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrChequesVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrChequesDMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrChequesDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrChequesDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrChequesLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrChequesDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChequesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChequesUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrChequesUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrChequesDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrChequesDesco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrChequesQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrChequesBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
    end
    object QrChequesCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrChequesLote: TSmallintField
      FieldName = 'Lote'
      DisplayFormat = '0000'
    end
  end
  object DsCheques: TDataSource
    DataSet = QrCheques
    Left = 748
    Top = 20
  end
  object PMLote: TPopupMenu
    Left = 268
    Top = 408
    object Incluinovolote1: TMenuItem
      Caption = '&Inclui novo lote'
      OnClick = Incluinovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      Enabled = False
    end
  end
  object PMCheque: TPopupMenu
    Left = 372
    Top = 416
    object Adicionachequeaoloteatual1: TMenuItem
      Caption = '&Adiciona cheque ao lote atual'
      OnClick = Adicionachequeaoloteatual1Click
    end
    object Alterarepassedochequeatual1: TMenuItem
      Caption = '&Edita repasse do cheque atual'
      Enabled = False
    end
    object Retirachequedoloteatual1: TMenuItem
      Caption = '&Retira  cheque do lote atual'
      OnClick = Retirachequedoloteatual1Click
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMECLIENTE')
    Left = 368
    Top = 205
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 396
    Top = 205
  end
  object QrSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor, SUM(JurosV) JurosV'
      'FROM repasits'
      'WHERE Codigo=:P0'
      '')
    Left = 197
    Top = 65
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSumJurosV: TFloatField
      FieldName = 'JurosV'
    end
  end
  object PMImprime: TPopupMenu
    Left = 233
    Top = 65
  end
  object QrLI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lo.Tipo, li.* '
      'FROM lotesits li'
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Codigo=:P0'
      ''
      '')
    Left = 268
    Top = 65
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLIComp: TIntegerField
      FieldName = 'Comp'
    end
    object QrLIBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLIAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrLIConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrLICheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrLICPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLIEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLIBruto: TFloatField
      FieldName = 'Bruto'
    end
    object QrLIDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrLIValor: TFloatField
      FieldName = 'Valor'
    end
    object QrLIEmissao: TDateField
      FieldName = 'Emissao'
    end
    object QrLIDCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrLIDDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrLIVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrLITxaCompra: TFloatField
      FieldName = 'TxaCompra'
    end
    object QrLITxaJuros: TFloatField
      FieldName = 'TxaJuros'
    end
    object QrLITxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
    end
    object QrLIVlrCompra: TFloatField
      FieldName = 'VlrCompra'
    end
    object QrLIVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
    end
    object QrLIDMais: TIntegerField
      FieldName = 'DMais'
    end
    object QrLIDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrLIDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrLIDevolucao: TIntegerField
      FieldName = 'Devolucao'
    end
    object QrLIQuitado: TIntegerField
      FieldName = 'Quitado'
    end
    object QrLILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLIPraca: TIntegerField
      FieldName = 'Praca'
    end
    object QrLITipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLIBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
      Required = True
    end
    object QrLIAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
      Required = True
    end
    object QrLITotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrLITotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrLITotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrLIData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrLIProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrLIProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrLIRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 300
    OnTimer = Timer1Timer
    Left = 274
    Top = 313
  end
  object frxRepasIts: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.970945266200000000
    ReportOptions.LastChange = 39717.970945266200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxRepasItsGetValue
    Left = 630
    Top = 21
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsRepas
        DataSetName = 'frxDsRepas'
      end
      item
        DataSet = frxDsRepasIts
        DataSetName = 'frxDsRepasIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 538.661410000000000000
          Top = 7.881880000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 2.661410000000000000
          Top = 3.881880000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          Left = 174.661410000000000000
          Top = 31.881880000000000000
          Width = 520.000000000000000000
          Height = 26.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 393.071120000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 565.118120000000000000
          Top = 6.267159999999990000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 653.118120000000000000
          Top = 6.267159999999990000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        Height = 17.000000000000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsRepasIts
        DataSetName = 'frxDsRepasIts'
        RowCount = 0
        object Memo6: TfrxMemoView
          Left = 2.661410000000000000
          Width = 24.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsRepasIts."Banco">)]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 26.661410000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'0000'#39', <frxDsRepasIts."Agencia">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 54.661410000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepasIts."Conta"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 114.661410000000000000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsRepasIts."Cheque">)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 154.661410000000000000
          Width = 152.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsRepasIts."Emitente"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 306.661410000000000000
          Width = 92.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsRepasIts."CPF_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 398.661410000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."Valor"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 458.661410000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepasIts."Vencto"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 506.661410000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepasIts."DDeposito"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 554.661410000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."Dias"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 582.661410000000000000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."JurosV"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 638.661410000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."LIQUIDO"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 95.173160000000000000
        Top = 113.385900000000000000
        Width = 718.110700000000000000
        object frxMemoView1: TfrxMemoView
          Left = 2.661410000000000000
          Top = 6.173160000000000000
          Width = 696.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'REPASSE DE CHEQUES')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 598.661410000000000000
          Top = 53.173160000000000000
          Width = 100.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsRepas."Data"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 2.661410000000000000
          Top = 51.173160000000000000
          Width = 696.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Memo24: TfrxMemoView
          Left = 2.661410000000000000
          Top = 53.173160000000000000
          Width = 436.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Coligado: [frxDsRepas."Coligado"] - [frxDsRepas."NOMECOLIGADO"]')
          ParentFont = False
        end
        object Line2: TfrxLineView
          Left = 2.661410000000000000
          Top = 74.173160000000000000
          Width = 696.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Memo69: TfrxMemoView
          Left = 494.661410000000000000
          Top = 53.173160000000000000
          Width = 100.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Data do repasse:')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 2.661410000000000000
          Top = 78.173160000000000000
          Width = 24.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bco.')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 26.661410000000000000
          Top = 78.173160000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag.')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 54.661410000000000000
          Top = 78.173160000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 114.661410000000000000
          Top = 78.173160000000000000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cheq.')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 154.661410000000000000
          Top = 78.173160000000000000
          Width = 152.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emitente')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 306.661410000000000000
          Top = 78.173160000000000000
          Width = 92.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 398.661410000000000000
          Top = 78.173160000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 458.661410000000000000
          Top = 78.173160000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 506.661410000000000000
          Top = 78.173160000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dep'#243'sito')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 554.661410000000000000
          Top = 78.173160000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Dias')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 582.661410000000000000
          Top = 78.173160000000000000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Juros')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 638.661410000000000000
          Top = 78.173160000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ L'#237'quido')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 634.709030000000000000
          Top = 7.559060000000000000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepas."Codigo"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 570.709030000000000000
          Top = 7.559059999999990000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Controle:')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 24.000000000000000000
        Top = 347.716760000000000000
        Width = 718.110700000000000000
        object Memo34: TfrxMemoView
          Left = 2.661410000000000000
          Top = 4.535250000000020000
          Width = 396.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total dos [VARF_QTD_CHEQUES] cheques deste border'#244':')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 398.661410000000000000
          Top = 4.535250000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 582.661410000000000000
          Top = 4.535250000000020000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 638.661410000000000000
          Top = 4.535250000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 458.661410000000000000
          Top = 4.535250000000020000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
    end
  end
  object frxDsRepasIts: TfrxDBDataset
    UserName = 'frxDsRepasIts'
    CloseDataSource = False
    DataSet = QrRepasIts
    BCDToCurrency = False
    Left = 658
    Top = 21
  end
  object frxDsRepas: TfrxDBDataset
    UserName = 'frxDsRepas'
    CloseDataSource = False
    DataSet = QrRepas
    BCDToCurrency = False
    Left = 686
    Top = 21
  end
  object QrConfigBB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ban.Nome NOMEBANCO, cbb.*'
      'FROM configbb cbb'
      'LEFT JOIN bancos ban ON ban.Codigo=cbb.Banco '
      'WHERE cbb.Codigo=:P0')
    Left = 304
    Top = 65
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConfigBBCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConfigBBNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrConfigBBConvenio: TIntegerField
      FieldName = 'Convenio'
    end
    object QrConfigBBCarteira: TWideStringField
      FieldName = 'Carteira'
      Size = 2
    end
    object QrConfigBBVariacao: TWideStringField
      FieldName = 'Variacao'
      Size = 3
    end
    object QrConfigBBSiglaEspecie: TWideStringField
      FieldName = 'SiglaEspecie'
      Size = 5
    end
    object QrConfigBBMoeda: TWideStringField
      FieldName = 'Moeda'
      Size = 5
    end
    object QrConfigBBAceite: TSmallintField
      FieldName = 'Aceite'
    end
    object QrConfigBBProtestar: TSmallintField
      FieldName = 'Protestar'
    end
    object QrConfigBBMsgLinha1: TWideStringField
      FieldName = 'MsgLinha1'
      Size = 40
    end
    object QrConfigBBPgAntes: TSmallintField
      FieldName = 'PgAntes'
    end
    object QrConfigBBMultaCodi: TSmallintField
      FieldName = 'MultaCodi'
    end
    object QrConfigBBMultaDias: TSmallintField
      FieldName = 'MultaDias'
    end
    object QrConfigBBMultaValr: TFloatField
      FieldName = 'MultaValr'
    end
    object QrConfigBBMultaPerc: TFloatField
      FieldName = 'MultaPerc'
    end
    object QrConfigBBMultaTiVe: TSmallintField
      FieldName = 'MultaTiVe'
    end
    object QrConfigBBImpreLoc: TSmallintField
      FieldName = 'ImpreLoc'
    end
    object QrConfigBBModalidade: TIntegerField
      FieldName = 'Modalidade'
    end
    object QrConfigBBclcAgencNr: TWideStringField
      FieldName = 'clcAgencNr'
      Size = 4
    end
    object QrConfigBBclcAgencDV: TWideStringField
      FieldName = 'clcAgencDV'
      Size = 1
    end
    object QrConfigBBclcContaNr: TWideStringField
      FieldName = 'clcContaNr'
      Size = 8
    end
    object QrConfigBBclcContaDV: TWideStringField
      FieldName = 'clcContaDV'
      Size = 1
    end
    object QrConfigBBcedAgencNr: TWideStringField
      FieldName = 'cedAgencNr'
      Size = 4
    end
    object QrConfigBBcedAgencDV: TWideStringField
      FieldName = 'cedAgencDV'
      Size = 1
    end
    object QrConfigBBcedContaNr: TWideStringField
      FieldName = 'cedContaNr'
      Size = 8
    end
    object QrConfigBBcedContaDV: TWideStringField
      FieldName = 'cedContaDV'
      Size = 1
    end
    object QrConfigBBEspecie: TSmallintField
      FieldName = 'Especie'
    end
    object QrConfigBBCorrido: TSmallintField
      FieldName = 'Corrido'
    end
    object QrConfigBBBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrConfigBBIDEmpresa: TWideStringField
      FieldName = 'IDEmpresa'
    end
    object QrConfigBBProduto: TWideStringField
      FieldName = 'Produto'
      Size = 4
    end
    object QrConfigBBNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrConfigBBInfoCovH: TSmallintField
      FieldName = 'InfoCovH'
      Required = True
    end
    object QrConfigBBCarteira240: TWideStringField
      FieldName = 'Carteira240'
      Size = 1
    end
    object QrConfigBBCadastramento: TWideStringField
      FieldName = 'Cadastramento'
      Size = 1
    end
    object QrConfigBBTradiEscrit: TWideStringField
      FieldName = 'TradiEscrit'
      Size = 1
    end
    object QrConfigBBDistribuicao: TWideStringField
      FieldName = 'Distribuicao'
      Size = 1
    end
    object QrConfigBBAceite240: TWideStringField
      FieldName = 'Aceite240'
      Size = 1
    end
    object QrConfigBBProtesto: TWideStringField
      FieldName = 'Protesto'
      Size = 1
    end
    object QrConfigBBProtestodd: TIntegerField
      FieldName = 'Protestodd'
      Required = True
    end
    object QrConfigBBBaixaDevol: TWideStringField
      FieldName = 'BaixaDevol'
      Size = 1
    end
    object QrConfigBBBaixaDevoldd: TIntegerField
      FieldName = 'BaixaDevoldd'
      Required = True
    end
    object QrConfigBBLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrConfigBBDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrConfigBBDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrConfigBBUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrConfigBBUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrConfigBBEmisBloqueto: TWideStringField
      FieldName = 'EmisBloqueto'
      Size = 1
    end
    object QrConfigBBEspecie240: TWideStringField
      FieldName = 'Especie240'
      Size = 2
    end
    object QrConfigBBJuros240Cod: TWideStringField
      FieldName = 'Juros240Cod'
      Size = 1
    end
    object QrConfigBBJuros240Qtd: TFloatField
      FieldName = 'Juros240Qtd'
      Required = True
    end
    object QrConfigBBContrOperCred: TIntegerField
      FieldName = 'ContrOperCred'
      Required = True
    end
    object QrConfigBBReservBanco: TWideStringField
      FieldName = 'ReservBanco'
    end
    object QrConfigBBReservEmprs: TWideStringField
      FieldName = 'ReservEmprs'
    end
    object QrConfigBBLH_208_33: TWideStringField
      FieldName = 'LH_208_33'
      Size = 33
    end
    object QrConfigBBSQ_233_008: TWideStringField
      FieldName = 'SQ_233_008'
      Size = 8
    end
    object QrConfigBBTL_124_117: TWideStringField
      FieldName = 'TL_124_117'
      Size = 117
    end
    object QrConfigBBSR_208_033: TWideStringField
      FieldName = 'SR_208_033'
      Size = 33
    end
    object QrConfigBBDiretorio: TWideStringField
      FieldName = 'Diretorio'
      Size = 255
    end
  end
  object QrConfigs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM configbb')
    Left = 389
    Top = 121
    object QrConfigsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConfigsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsConfigs: TDataSource
    DataSet = QrConfigs
    Left = 417
    Top = 121
  end
  object PMGera: TPopupMenu
    Left = 628
    Top = 444
    object Somentecheques1: TMenuItem
      Caption = '&Somente cheques (Para importar no aplicativo do banco)'
      OnClick = Somentecheques1Click
    end
    object CabealhochequesArquivodiretoaobanco1: TMenuItem
      Caption = '&Cabe'#231'alho + cheques (Para enviar arquivo direto ao banco)'
      Enabled = False
      OnClick = CabealhochequesArquivodiretoaobanco1Click
    end
  end
end
