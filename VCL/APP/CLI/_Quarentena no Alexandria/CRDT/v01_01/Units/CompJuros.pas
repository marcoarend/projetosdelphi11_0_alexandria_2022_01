unit CompJuros;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit, dmkGeral, UnDmkEnums;

type
  TFmCompJuros = class(TForm)
    PainelConfirma: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    PainelDecomp: TPanel;
    Label11: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    EdJuros: TdmkEdit;
    EdPrazD: TdmkEdit;
    EdCasas: TdmkEdit;
    EdTaxa: TdmkEdit;
    PainelDados: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    EdBaseT: TdmkEdit;
    EdPrazC: TdmkEdit;
    EdJuroV: TdmkEdit;
    EdJuroP: TdmkEdit;
    EdLiqui: TdmkEdit;
    EdTaxaM: TdmkEdit;
    Painel2: TPanel;
    Image2: TImage;
    Painel1: TPanel;
    Image1: TImage;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdBaseTChange(Sender: TObject);
    procedure EdPrazCChange(Sender: TObject);
    procedure EdTaxaMChange(Sender: TObject);
    procedure EdJurosChange(Sender: TObject);
    procedure EdPrazDChange(Sender: TObject);
    procedure EdCasasChange(Sender: TObject);
  private
    { Private declarations }
    procedure CompoeJuros;
    procedure DecompoeJuros;

  public
    { Public declarations }
  end;

  var
  FmCompJuros: TFmCompJuros;

implementation

{$R *.DFM}

uses UnMyObjects, Module;

procedure TFmCompJuros.DecompoeJuros;
var
  Casas: Integer;
  Juros, PrazD, TaxaM: Double;
begin
  Casas := Geral.IMV(EdCasas.Text);
  Juros := Geral.DMV(EdJuros.Text);
  PrazD := Geral.DMV(EdPrazD.Text);
  //
  TaxaM := MLAGeral.DescobreJuroComposto(Juros, PrazD, Casas);
  EdTaxa.Text := Geral.FFT(TaxaM, Casas, siNegativo);
end;

procedure TFmCompJuros.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCompJuros.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCompJuros.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, 'Decomp�em Juros', Image1, Painel1, True, 0);
  MLAGeral.LoadTextBitmapToPanel(0, 0, 'Comp�em Juros', Image2, Painel2, True, 0);
end;

procedure TFmCompJuros.EdBaseTChange(Sender: TObject);
begin
  CompoeJuros;
end;

procedure TFmCompJuros.EdPrazCChange(Sender: TObject);
begin
  CompoeJuros;
end;

procedure TFmCompJuros.EdTaxaMChange(Sender: TObject);
begin
  CompoeJuros;
end;

procedure TFmCompJuros.CompoeJuros;
var
  BaseT, JuroP, JuroV, PrazC, TaxaM, Liqui: Double;
begin
  BaseT := Geral.DMV(EdBaseT.Text);
  PrazC := Geral.DMV(EdPrazC.Text);
  TaxaM := Geral.DMV(EdTaxaM.Text);
  //
  JuroP := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(TaxaM, PrazC);
  JuroV := Trunc(JuroP * BaseT) / 100;
  Liqui := BaseT - JuroV;
  //
  EdJuroP.Text := Geral.FFT(JuroP, 6, siNegativo);
  EdJuroV.Text := Geral.FFT(JuroV, 2, siNegativo);
  EdLiqui.Text := Geral.FFT(Liqui, 2, siNegativo);
end;

procedure TFmCompJuros.EdJurosChange(Sender: TObject);
begin
  DecompoeJuros;
end;

procedure TFmCompJuros.EdPrazDChange(Sender: TObject);
begin
  DecompoeJuros;
end;

procedure TFmCompJuros.EdCasasChange(Sender: TObject);
begin
  DecompoeJuros;
end;

end.
