unit Evolucapi;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, ComCtrls, Grids,
  DBGrids, DBCtrls, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UnDmkEnums;

type
  TFmEvolucapi = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    QrCarteiTudo: TmySQLQuery;
    QrCarteiTudoValor: TFloatField;
    QrEvolucapi: TmySQLQuery;
    DsEvolucapi: TDataSource;
    QrColigado: TmySQLQuery;
    QrColigadoCodigo: TIntegerField;
    QrColigadoNOMECOLIGADO: TWideStringField;
    DsColigado: TDataSource;
    QrEvolucapiDataE: TDateField;
    QrEvolucapiCarteiTudo: TFloatField;
    QrEvolucapiRepassEspe: TFloatField;
    QrEvolucapiRepassOutr: TFloatField;
    QrEvolucapiCARTEIREAL: TFloatField;
    QrRepassOutr: TmySQLQuery;
    QrRepassEspe: TmySQLQuery;
    QrRepassOutrValor: TFloatField;
    QrRepassEspeValor: TFloatField;
    QrCHDevolAbe: TmySQLQuery;
    QrCHDevolAbeValor: TFloatField;
    QrEvolucapiCHDevolAbe: TFloatField;
    QrDUDevolAbe: TmySQLQuery;
    QrEvolucapiDUDevolAbe: TFloatField;
    QrDUDevolAbeValor: TFloatField;
    QrEvolucapiVALOR_TOTAL: TFloatField;
    QrEvolucapiVALOR_SUB_T: TFloatField;
    QrEvolucapiSaldoConta: TFloatField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    PnAtualiza: TPanel;
    PnEditSdoCC: TPanel;
    Label3: TLabel;
    BitBtn1: TBitBtn;
    EdSaldoConta: TdmkEdit;
    BitBtn2: TBitBtn;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    TPIniB: TDateTimePicker;
    TPFimB: TDateTimePicker;
    QrEvolper: TmySQLQuery;
    QrEvolperDataE: TDateField;
    QrEvolperCarteiTudo: TFloatField;
    QrEvolperRepassEspe: TFloatField;
    QrEvolperRepassOutr: TFloatField;
    QrEvolperCARTEIREAL: TFloatField;
    QrEvolperCHDevolAbe: TFloatField;
    QrEvolperDUDevolAbe: TFloatField;
    QrEvolperVALOR_SUB_T: TFloatField;
    QrEvolperSaldoConta: TFloatField;
    QrEvolperVALOR_TOTAL: TFloatField;
    QrMax: TmySQLQuery;
    QrMaxTOTAL: TFloatField;
    BtGrafico: TBitBtn;
    RGIntervalo: TRadioGroup;
    QrEvolucapiSaldoCaixa: TFloatField;
    PnDados: TPanel;
    GroupBox1: TGroupBox;
    Label34: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TPIniA: TDateTimePicker;
    TPFimA: TDateTimePicker;
    EdColigado: TdmkEditCB;
    CBColigado: TdmkDBLookupComboBox;
    BtOK: TBitBtn;
    ProgressBar1: TProgressBar;
    EdSaldoCaixa: TdmkEdit;
    Label6: TLabel;
    QrEvolperSaldoCaixa: TFloatField;
    TabSheet3: TTabSheet;
    DBGrid2: TDBGrid;
    QrCHNull: TmySQLQuery;
    DsCHNull: TDataSource;
    QrCHNullCliente: TIntegerField;
    QrCHNullEmitente: TWideStringField;
    QrCHNullCPF: TWideStringField;
    QrCHNullBanco: TIntegerField;
    QrCHNullAgencia: TIntegerField;
    QrCHNullConta: TWideStringField;
    QrCHNullCheque: TIntegerField;
    QrCHNullValor: TFloatField;
    QrCHNullTaxas: TFloatField;
    QrCHNullMulta: TFloatField;
    QrCHNullJuros: TFloatField;
    QrCHNullDesconto: TFloatField;
    QrCHNullValPago: TFloatField;
    QrCHNullChequeOrigem: TIntegerField;
    QrCHNullLoteOrigem: TIntegerField;
    Panel2: TPanel;
    BtExclui1: TBitBtn;
    QrCHNullCodigo: TIntegerField;
    TabSheet4: TTabSheet;
    DBGrid3: TDBGrid;
    QrCHPrecoce: TmySQLQuery;
    DsCHPrecoce: TDataSource;
    Panel3: TPanel;
    QrCHPrecoceCodigo: TIntegerField;
    QrCHPrecoceAlinea1: TIntegerField;
    QrCHPrecoceAlinea2: TIntegerField;
    QrCHPrecoceData1: TDateField;
    QrCHPrecoceData2: TDateField;
    QrCHPrecoceData3: TDateField;
    QrCHPrecoceCliente: TIntegerField;
    QrCHPrecoceBanco: TIntegerField;
    QrCHPrecoceAgencia: TIntegerField;
    QrCHPrecoceConta: TWideStringField;
    QrCHPrecoceCheque: TIntegerField;
    QrCHPrecoceCPF: TWideStringField;
    QrCHPrecoceValor: TFloatField;
    QrCHPrecoceTaxas: TFloatField;
    QrCHPrecoceMulta: TFloatField;
    QrCHPrecoceJurosP: TFloatField;
    QrCHPrecoceJurosV: TFloatField;
    QrCHPrecoceDesconto: TFloatField;
    QrCHPrecoceEmitente: TWideStringField;
    QrCHPrecoceChequeOrigem: TIntegerField;
    QrCHPrecoceValPago: TFloatField;
    QrCHPrecoceStatus: TSmallintField;
    QrCHPrecoceLk: TIntegerField;
    QrCHPrecoceDataCad: TDateField;
    QrCHPrecoceDataAlt: TDateField;
    QrCHPrecoceUserCad: TIntegerField;
    QrCHPrecoceUserAlt: TIntegerField;
    QrCHPrecocePgDesc: TFloatField;
    QrCHPrecoceDDeposito: TDateField;
    QrCHPrecoceVencto: TDateField;
    QrCHPrecoceLoteOrigem: TIntegerField;
    QrCHPrecoceAlterWeb: TSmallintField;
    QrCHPrecoceCodigo_1: TIntegerField;
    QrCHPrecoceControle: TIntegerField;
    QrCHPrecoceComp: TIntegerField;
    QrCHPrecoceBanco_1: TIntegerField;
    QrCHPrecoceAgencia_1: TIntegerField;
    QrCHPrecoceConta_1: TWideStringField;
    QrCHPrecoceCheque_1: TIntegerField;
    QrCHPrecoceCPF_1: TWideStringField;
    QrCHPrecoceEmitente_1: TWideStringField;
    QrCHPrecoceBruto: TFloatField;
    QrCHPrecoceDesco: TFloatField;
    QrCHPrecoceValor_1: TFloatField;
    QrCHPrecoceEmissao: TDateField;
    QrCHPrecoceDCompra: TDateField;
    QrCHPrecoceDDeposito_1: TDateField;
    QrCHPrecoceVencto_1: TDateField;
    QrCHPrecoceTxaCompra: TFloatField;
    QrCHPrecoceTxaJuros: TFloatField;
    QrCHPrecoceTxaAdValorem: TFloatField;
    QrCHPrecoceVlrCompra: TFloatField;
    QrCHPrecoceVlrAdValorem: TFloatField;
    QrCHPrecoceDMais: TIntegerField;
    QrCHPrecoceDias: TIntegerField;
    QrCHPrecoceDuplicata: TWideStringField;
    QrCHPrecoceDevolucao: TIntegerField;
    QrCHPrecoceQuitado: TIntegerField;
    QrCHPrecoceLk_1: TIntegerField;
    QrCHPrecoceDataCad_1: TDateField;
    QrCHPrecoceDataAlt_1: TDateField;
    QrCHPrecoceUserCad_1: TIntegerField;
    QrCHPrecoceUserAlt_1: TIntegerField;
    QrCHPrecocePraca: TIntegerField;
    QrCHPrecoceBcoCobra: TIntegerField;
    QrCHPrecoceAgeCobra: TIntegerField;
    QrCHPrecoceTotalJr: TFloatField;
    QrCHPrecoceTotalDs: TFloatField;
    QrCHPrecoceTotalPg: TFloatField;
    QrCHPrecoceData3_1: TDateField;
    QrCHPrecoceProrrVz: TIntegerField;
    QrCHPrecoceProrrDd: TIntegerField;
    QrCHPrecoceRepassado: TSmallintField;
    QrCHPrecoceDepositado: TSmallintField;
    QrCHPrecoceValQuit: TFloatField;
    QrCHPrecoceValDeposito: TFloatField;
    QrCHPrecoceTipo: TIntegerField;
    QrCHPrecoceAliIts: TIntegerField;
    QrCHPrecoceAlinPgs: TIntegerField;
    QrCHPrecoceNaoDeposita: TSmallintField;
    QrCHPrecoceReforcoCxa: TSmallintField;
    QrCHPrecoceCartDep: TIntegerField;
    QrCHPrecoceCobranca: TIntegerField;
    QrCHPrecoceRepCli: TIntegerField;
    QrCHPrecoceMotivo: TWideStringField;
    QrCHPrecoceObservacao: TWideStringField;
    QrCHPrecoceObsGerais: TWideStringField;
    QrCHPrecoceCliente_1: TIntegerField;
    QrCHPrecoceAlterWeb_1: TSmallintField;
    QrCHPrecoceTeste: TIntegerField;
    QrEmcart: TmySQLQuery;
    QrduplVencMoroso: TmySQLQuery;
    DBGrid1: TDBGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtOKClick(Sender: TObject);
    procedure QrEvolucapiCalcFields(DataSet: TDataSet);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure QrEvolperCalcFields(DataSet: TDataSet);
    procedure BtGraficoClick(Sender: TObject);
    procedure QrCHNullAfterOpen(DataSet: TDataSet);
    procedure QrCHNullBeforeClose(DataSet: TDataSet);
    procedure BtExclui1Click(Sender: TObject);
  private
    { Private declarations }
    FRegistryPath: String;
    procedure EditaSaldoConta;
    procedure ReopenEvolucapi(Data: TDateTime);
  public
    { Public declarations }
  end;

  var
  FmEvolucapi: TFmEvolucapi;

implementation

{$R *.DFM}

uses Module, Principal, UnMyObjects;

procedure TFmEvolucapi.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEvolucapi.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  QrCHNull.Close;
  QrCHNull.Open;
  if QrCHNull.RecordCount > 0 then
    PageControl1.ActivePageIndex := 2;
  QrCHPrecoce.Close;
  QrCHPrecoce.Open;
  if QrCHPrecoce.RecordCount > 0 then
    PageControl1.ActivePageIndex := 3;
end;

procedure TFmEvolucapi.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmEvolucapi.FormCreate(Sender: TObject);
var
  Especial: Integer;
begin
  // Atualizar LastEditLote
  PageControl1.ActivePageIndex := 0;
  Dmod.QrControle.Close;
  Dmod.QrControle.Open;
  //
  TabSheet2.TabVisible := False;
  //
  FRegistryPath := Application.Title + '\Evolucapi';
  TPIniA.Date   := Geral.ReadAppKeyLM('DataIniA', FRegistryPath, ktDate, Int(Date));
  TPFimA.Date   := Geral.ReadAppKeyLM('DataFimA', FRegistryPath, ktDate, Int(Date));
  TPIniB.Date   := Geral.ReadAppKeyLM('DataIniB', FRegistryPath, ktDate, Int(Date-30));
  TPFimB.Date   := Geral.ReadAppKeyLM('DataFimB', FRegistryPath, ktDate, Int(Date));
  //
  if Dmod.QrControleLastEditLote.Value > 0 then
    if TPIniA.Date > Dmod.QrControleLastEditLote.Value then
      TPIniA.Date := Dmod.QrControleLastEditLote.Value;
  //
  Especial := Geral.ReadAppKeyLM('Coligado Especial', FRegistryPath, ktInteger, 0);
  //
  if Especial <> 0 then
  begin
    EdColigado.Text := IntToStr(Especial);
    CBColigado.KeyValue := Especial;
  end;
  QrColigado.Open;
  ReopenEvolucapi(0);
  //
end;

procedure TFmEvolucapi.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Geral.WriteAppKeyLM2('DataIniA', FRegistryPath, Int(TPIniA.Date), ktDate);
  Geral.WriteAppKeyLM2('DataFimA', FRegistryPath, Int(TPFimA.Date), ktDate);
  Geral.WriteAppKeyLM2('DataIniB', FRegistryPath, Int(TPIniB.Date), ktDate);
  Geral.WriteAppKeyLM2('DataFimB', FRegistryPath, Int(TPFimB.Date), ktDate);
  Geral.WriteAppKeyLM2('Coligado Especial', FRegistryPath, Geral.IMV(EdColigado.Text), ktInteger);
end;

procedure TFmEvolucapi.BtOKClick(Sender: TObject);
var
  i, a, b, e: Integer;
  d: String;
begin
  a := Trunc(TPIniA.Date);
  b := Trunc(TPFimA.Date);
  if b - a > 92 then
  begin
    if Application.MessageBox(PChar('Confirma a atualiza��o dos dados? '+
    'Per�odos grandes podem demorar alguns minutos!'), 'Pergunta', MB_YESNOCANCEL+
    MB_ICONQUESTION) <> ID_YES then Exit;
  end;
  Screen.Cursor := crDefault;
  if b - a < 0 then
  begin
    Application.MessageBox('Per�odo inv�lido', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  e := Geral.IMV(EdColigado.Text);
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM evolucapi WHERE DataE BETWEEN :P0 AND :P1');
  Dmod.QrUpd.Params[0].AsString := Geral.FDT(a, 1);
  Dmod.QrUpd.Params[1].AsString := Geral.FDT(b, 1);
  Dmod.QrUpd.ExecSQL;
  //
  ProgressBar1.Position := 0;
  ProgressBar1.Max := b - a + 1;
  ProgressBar1.Visible := True;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO evolucapi SET DataE=:P0, CarteiTudo=:P1,');
  Dmod.QrUpd.SQL.Add('RepassEspe=:P2, RepassOutr=:P3, CHDevolAbe=:P4, ');
  Dmod.QrUpd.SQL.Add('DUDevolAbe=:P5 ');
  for i := a to b do
  begin
    ProgressBar1.Position := ProgressBar1.Position + 1;
    //
    d := Geral.FDT(i, 1);
    // Valores emitidos antes da data em refer�ncia que ainda n�o venceram.
    // Ou seja, valores (QrCarteiTudoValor.Value) em carteira no dia em refer�cia (i).
    //
    // Novo 2007 08 17 - eliminar cheques devolvidos e DDeposito posterior a
    //data pesquisada com a adi��o das linhas:
    /////////////////////////////////////////////////////
    //LEFT JOIN alinits ai ON ai.ChequeOrigem=li.Controle
    /////////////////////////////////////////////////////
    //AND ai.ChequeOrigem IS NULL
    /////////////////////////////////////////////////////
    QrCarteiTudo.Close;
    QrCarteiTudo.Params[0].AsString := d;
    QrCarteiTudo.Params[1].AsString := d;
    QrCarteiTudo.Open;
    //
    // Valores emitidos antes da data em refer�ncia que ainda n�o venceram,
    // mas foram repassados a um coligado especial.
    QrRepassEspe.Close;
    QrRepassEspe.Params[0].AsString  := d;
    QrRepassEspe.Params[1].AsString  := d;
    QrRepassEspe.Params[2].AsString  := d;
    QrRepassEspe.Params[3].AsInteger := e;
    QrRepassEspe.Open;
    //
    // Valores emitidos antes da data em refer�ncia que ainda n�o venceram,
    // mas foram repassados (menos ao coligado especial).
    QrRepassOutr.Close;
    QrRepassOutr.Params[0].AsString  := d;
    QrRepassOutr.Params[1].AsString  := d;
    QrRepassOutr.Params[2].AsString  := d;
    QrRepassOutr.Params[3].AsInteger := e;
    QrRepassOutr.Open;
    //
    // Valores abertos de cheques devolvidos antes da data em refer�ncia e n�o
    // pagos at� a data de refer�ncia.
    //ver cheques devolvidos que n�o existem mais nos itens de lote!
    QrCHDevolAbe.Close;
    QrCHDevolAbe.Params[0].AsString  := d;
    QrCHDevolAbe.Params[1].AsString  := d;
    QrCHDevolAbe.Open;
    //
    // Valores abertos de duplicatas vencidas e n�o pagas at� a data
    // em refer�ncia.
    QrDUDevolAbe.Close;
    QrDUDevolAbe.Params[0].AsString  := d;
    QrDUDevolAbe.Params[1].AsString  := d;
    QrDUDevolAbe.Open;
    //
    Dmod.QrUpd.Params[0].AsString := d;
    Dmod.QrUpd.Params[1].AsFloat  := QrCarteiTudoValor.Value;
    Dmod.QrUpd.Params[2].AsFloat  := QrRepassEspeValor.Value;
    Dmod.QrUpd.Params[3].AsFloat  := QrRepassOutrValor.Value;
    Dmod.QrUpd.Params[4].AsFloat  := QrCHDevolAbeValor.Value;
    Dmod.QrUpd.Params[5].AsFloat  := QrDUDevolAbeValor.Value;
    Dmod.QrUpd.ExecSQL;
  end;
  ReopenEvolucapi(0);
  FmPrincipal.AtualizaLastEditLote(Geral.FDT(b, 1));
  ProgressBar1.Visible := False;
  ProgressBar1.Position := 0;
  Screen.Cursor := crDefault;
end;

procedure TFmEvolucapi.QrEvolucapiCalcFields(DataSet: TDataSet);
begin
  QrEvolucapiCARTEIREAL.Value :=
  // Parei aqui
    QrEvolucapiCarteiTudo.Value -
    QrEvolucapiRepassEspe.Value -
    QrEvolucapiRepassOutr.Value;
  QrEvolucapiVALOR_TOTAL.Value :=
    QrEvolucapiCarteiTudo.Value +
    QrEvolucapiCHDevolAbe.Value +
    QrEvolucapiDUDevolAbe.Value +
    QrEvolucapiSaldoCaixa.Value +
    QrEvolucapiSaldoConta.Value;
  QrEvolucapiVALOR_SUB_T.Value :=
    QrEvolucapiVALOR_TOTAL.Value -
    QrEvolucapiRepassOutr.Value;
end;

procedure TFmEvolucapi.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then EditaSaldoConta;
end;

procedure TFmEvolucapi.DBGrid1DblClick(Sender: TObject);
begin
  EditaSaldoConta;
end;

procedure TFmEvolucapi.BitBtn1Click(Sender: TObject);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE evolucapi SET SaldoConta=:P0, SaldoCaixa=:P1 ');
  Dmod.QrUpd.SQL.Add('WHERE DataE=:Pa');
  Dmod.QrUpd.Params[0].AsFloat   := Geral.DMV(EdSaldoConta.Text);
  Dmod.QrUpd.Params[1].AsFloat   := Geral.DMV(EdSaldoCaixa.Text);
  //
  Dmod.QrUpd.Params[2].AsString  := Geral.FDT(QrEvolucapiDataE.Value, 1);
  Dmod.QrUpd.ExecSQL;
  //
  ReopenEvolucapi(QrEvolucapiDataE.Value);
  PnAtualiza.Visible  := True;
  PnDados.Enabled     := True;
  PnEditSdoCC.Visible := False;
  DBGrid1.SetFocus;
end;

procedure TFmEvolucapi.ReopenEvolucapi(Data: TDateTime);
begin
  QrEvolucapi.Close;
  QrEvolucapi.Open;
  //
  if Data > 0 then QrEvolucapi.Locate('DataE', Int(Data), [])
  else QrEvolucapi.Last;
  //
end;

procedure TFmEvolucapi.EditaSaldoConta;
begin
  PnEditSdoCC.Visible := True;
  PnAtualiza.Visible  := False;
  PnDados.Enabled     := False;
  //
  EdSaldoConta.Text := Geral.FFT(QrEvolucapiSaldoConta.Value, 2, siNegativo);
  EdSaldoCaixa.Text := Geral.FFT(QrEvolucapiSaldoCaixa.Value, 2, siNegativo);
  EdSaldoConta.SetFocus;
end;

procedure TFmEvolucapi.BitBtn2Click(Sender: TObject);
begin
  PnAtualiza.Visible  := True;
  PnDados.Enabled     := True;
  PnEditSdoCC.Visible := False;
  //
  DBGrid1.SetFocus;
end;

procedure TFmEvolucapi.QrEvolperCalcFields(DataSet: TDataSet);
begin
  QrEvolperCARTEIREAL.Value :=
    QrEvolperCarteiTudo.Value -
    QrEvolperRepassEspe.Value -
    QrEvolperRepassOutr.Value;
  QrEvolperVALOR_TOTAL.Value :=
    QrEvolperCarteiTudo.Value +
    QrEvolperCHDevolAbe.Value +
    QrEvolperDUDevolAbe.Value +
    QrEvolperSaldoCaixa.Value +
    QrEvolperSaldoConta.Value;
  QrEvolperVALOR_SUB_T.Value :=
    QrEvolperVALOR_TOTAL.Value -
    QrEvolperRepassOutr.Value;
end;

procedure TFmEvolucapi.BtGraficoClick(Sender: TObject);
(*
var
  Fator: Integer;
  Maximo: Double;
*)
begin
(*
  O ProEssentials n�o � mais instalado migrar para os gr�ficos nativos do Delphi

  QrEvolper.Close;
  QrEvolper.SQL.Clear;
  case RGIntervalo.ItemIndex of
    0:
    begin
      QrEvolper.SQL.Add('SELECT * FROM evolucapi');
      QrEvolper.SQL.Add('WHERE DataE BETWEEN :P0 AND :P1');
      QrEvolper.SQL.Add('ORDER BY DataE');
    end;
    1:
    begin
      QrEvolper.SQL.Add('SELECT * FROM evolucapi');
      QrEvolper.SQL.Add('WHERE DataE BETWEEN :P0 AND :P1');
      QrEvolper.SQL.Add('AND MONTH(DataE) <> MONTH(DATE_ADD(DataE, INTERVAL 1 DAY))');
    end;
  end;
  QrEvolper.Params[0].AsString := Geral.FDT(TPIniB.Date, 1);
  QrEvolper.Params[1].AsString := Geral.FDT(TPFimB.Date, 1);
  QrEvolper.Open;
  //
  QrMax.Close;
  QrMax.Params[0].AsString := Geral.FDT(TPIniB.Date, 1);
  QrMax.Params[1].AsString := Geral.FDT(TPFimB.Date, 1);
  QrMax.Open;
  Fator := 1;
  Maximo := QrMaxTOTAL.Value;
  while Maximo > 1000 do
  begin
    Fator := Fator * 1000;
    Maximo := Maximo / 1000;
  end;
  //
  Pego1.Subsets := 9; // Carteira, Especial, Outros, CH Dev, DU venc., saldo conta, Total - outros, Total
  Pego1.Points := QrEvolper.RecordCount;
  QrEvolper.First;
  while not QrEvolper.Eof do
  begin
    Pego1.YData[0, QrEvolper.RecNo-1] := QrEvolperCARTEIREAL.Value / Fator;
    Pego1.YData[1, QrEvolper.RecNo-1] := QrEvolperRepassEspe.Value / Fator;
    Pego1.YData[2, QrEvolper.RecNo-1] := QrEvolperRepassOutr.Value / Fator;
    Pego1.YData[3, QrEvolper.RecNo-1] := QrEvolperCHDevolAbe.Value / Fator;
    Pego1.YData[4, QrEvolper.RecNo-1] := QrEvolperDUDevolAbe.Value / Fator;
    Pego1.YData[5, QrEvolper.RecNo-1] := QrEvolperSaldoConta.Value / Fator;
    Pego1.YData[6, QrEvolper.RecNo-1] := QrEvolperSaldoCaixa.Value / Fator;
    Pego1.YData[7, QrEvolper.RecNo-1] := QrEvolperVALOR_SUB_T.Value / Fator;
    Pego1.YData[8, QrEvolper.RecNo-1] := QrEvolperVALOR_TOTAL.Value / Fator;
    //
    Pego1.PointLabels[QrEvolper.RecNo-1] := Geral.FDT(QrEvolperDataE.Value, 2);
    //
    QrEvolper.Next;
  end;
  // Set Various Properties //
  Pego1.DeskColor := RGB(192, 192, 192);
  Pego1.GraphBackColor := 0;
  Pego1.GraphForeColor := RGB(255, 255, 255);

  // Set DataShadows to show shadows//
  Pego1.DataShadows := gWithShadows;
  Pego1.BorderTypes := gInset;
  Pego1.MainTitle := '';
  Pego1.SubTitle := 'Evolu��o do Capital de '+
    Geral.FDT(TPIniB.Date, 2)+' at� ' + Geral.FDT(TPFimB.Date, 2);
  if Fator = 1 then
    Pego1.YAxisLabel := '$ Valor'
  else
    Pego1.YAxisLabel := '$ Valor x '+Geral.FFT(Fator, 0, siNegativo);
  Pego1.XAxisLabel := 'Data';
  Pego1.FocalRect := False;
  Pego1.PlottingMethod := gSpline;
  Pego1.GridLineControl := gNoGrid;
  Pego1.AllowRibbon := True;
  Pego1.AllowZooming := gHorzPlusVertZooming;
  Pego1.ZoomStyle := gRO2NOT;

  // Set SubsetLabels property array for 4 subsets //
  Pego1.SubsetLabels[00] := 'Carteira';
  Pego1.SubsetLabels[01] := 'Especial';
  Pego1.SubsetLabels[02] := 'Outros';
  Pego1.SubsetLabels[03] := 'CH Dev.';
  Pego1.SubsetLabels[04] := 'DU Dev.';
  Pego1.SubsetLabels[05] := 'Sdo caixa';
  Pego1.SubsetLabels[06] := 'Sdo conta';
  Pego1.SubsetLabels[07] := '(-)outros';
  Pego1.SubsetLabels[08] := 'Total';

  // this is how to change subset colors //
  Pego1.SubsetColors[0] := RGB(198, 0, 0);
  Pego1.SubsetColors[1] := RGB(0, 198, 198);
  Pego1.SubsetColors[2] := RGB(198, 198, 0);
  Pego1.SubsetColors[3] := RGB(0, 198, 0);

  // this is how to change line types //
  Pego1.SubsetLineTypes[0] := 0;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[1] := 1;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[2] := 2;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[3] := 3;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[4] := 0;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[5] := 1;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[6] := 2;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[7] := 3;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[8] := 6;//PELT_MEDIUMSOLID;

  // this is how to change point types //
  Pego1.SubsetPointTypes[0] := 0;//PEPT_DOTSOLID;
  Pego1.SubsetPointTypes[1] := 1;//PEPT_UPTRIANGLESOLID;
  Pego1.SubsetPointTypes[2] := 2;//PEPT_SQUARESOLID;
  Pego1.SubsetPointTypes[3] := 3;//PEPT_DOWNTRIANGLESOLID;
  Pego1.SubsetPointTypes[4] := 0;//PEPT_DOTSOLID;
  Pego1.SubsetPointTypes[5] := 1;//PEPT_UPTRIANGLESOLID;
  Pego1.SubsetPointTypes[6] := 2;//PEPT_SQUARESOLID;
  Pego1.SubsetPointTypes[7] := 0;//PEPT_SQUARESOLID;
  Pego1.SubsetPointTypes[8] := 3;//PEPT_DOWNTRIANGLESOLID;

  // Various other features //
  Pego1.FixedFonts := True;
  Pego1.BitmapGradientMode := True;
  Pego1.QuickStyle := gLightLine;
  Pego1.SimpleLineLegend:=True;
  Pego1.SimplePointLegend:=True;
  Pego1.LegendStyle:=gOneLine;

  Pego1.GradientBars := 8;
  Pego1.MainTitleBold := True;
  Pego1.SubTitleBold := True;
  Pego1.LabelBold := True;
  Pego1.LineShadows := True;
  Pego1.TextShadows := gShadowBoldText;
  Pego1.FontSize := gSmall;

  // Always call PEactions := 0 at end //
  Pego1.Visible := True;
  Pego1.PEactions := epeactions(0);
  PageControl1.ActivePageIndex := 1;
*)
end;

procedure TFmEvolucapi.QrCHNullAfterOpen(DataSet: TDataSet);
begin
  if QrCHNull.RecordCount > 0 then
    BtExclui1.Enabled := True
  else
    BtExclui1.Enabled := False;
end;

procedure TFmEvolucapi.QrCHNullBeforeClose(DataSet: TDataSet);
begin
  BtExclui1.Enabled := False;
end;

procedure TFmEvolucapi.BtExclui1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o do cheque devolvido orf�o?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM alinits WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrCHNullCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrCHNull.Close;
    QrCHNull.Open;
  end;
end;

end.

