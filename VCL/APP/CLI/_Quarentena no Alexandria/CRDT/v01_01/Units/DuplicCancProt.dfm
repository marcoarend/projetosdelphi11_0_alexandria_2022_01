object FmDuplicCancProt: TFmDuplicCancProt
  Left = 339
  Top = 185
  Caption = 'Carta de Cancelamento de Protesto'
  ClientHeight = 239
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 191
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Carta de Cancelamento de Protesto'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 143
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 8
      Width = 169
      Height = 13
      Caption = 'Carta de cancelamento de protesto:'
    end
    object Label2: TLabel
      Left = 12
      Top = 48
      Width = 70
      Height = 13
      Caption = #192' Comarca de:'
    end
    object Label3: TLabel
      Left = 12
      Top = 88
      Width = 96
      Height = 13
      Caption = 'N'#250'mero distribui'#231#227'o:'
    end
    object Label4: TLabel
      Left = 268
      Top = 88
      Width = 26
      Height = 13
      Caption = 'Livro:'
    end
    object Label5: TLabel
      Left = 524
      Top = 88
      Width = 34
      Height = 13
      Caption = 'Folhas:'
    end
    object EdCarta: TdmkEditCB
      Left = 12
      Top = 24
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCarta
      IgnoraDBLookupComboBox = False
    end
    object CBCarta: TdmkDBLookupComboBox
      Left = 72
      Top = 24
      Width = 701
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Titulo'
      ListSource = DsCartas
      TabOrder = 1
      dmkEditCB = EdCarta
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdComarca: TdmkEdit
      Left = 12
      Top = 64
      Width = 761
      Height = 21
      MaxLength = 255
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdNumDistrib: TdmkEdit
      Left = 12
      Top = 104
      Width = 250
      Height = 21
      MaxLength = 30
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdLivro: TdmkEdit
      Left = 268
      Top = 104
      Width = 250
      Height = 21
      MaxLength = 30
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdFolhas: TdmkEdit
      Left = 524
      Top = 104
      Width = 250
      Height = 21
      MaxLength = 30
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
  end
  object QrCartas: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCartasAfterOpen
    SQL.Strings = (
      'SELECT Codigo, Titulo, Texto'
      'FROM cartas'
      'WHERE Tipo=5'
      'ORDER BY Titulo')
    Left = 8
    Top = 8
    object QrCartasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartasTitulo: TWideStringField
      FieldName = 'Titulo'
      Size = 100
    end
    object QrCartasTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object DsCartas: TDataSource
    DataSet = QrCartas
    Left = 36
    Top = 8
  end
  object frxCartaCancProtesto: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39685.834195960600000000
    ReportOptions.LastChange = 39685.834195960600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure DadosDeDetalhe3OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <VARF_NOTEOF> then Engine.NewPage;'
      'end;'
      ''
      'begin'
      ''
      'end.')
    OnGetValue = frxCartaCancProtestoGetValue
    Left = 636
    Top = 4
    Datasets = <
      item
        DataSet = frxDsCarta
        DataSetName = 'frxDsCarta'
      end
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsSaCart
        DataSetName = 'frxDsSaCart'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object DadosDeDetalhe1: TfrxDetailData
        Height = 38.000000000000000000
        Top = 249.448980000000000000
        Width = 793.701300000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        RowCount = 1
        Stretched = True
        object Rich1: TfrxRichView
          Left = 42.559060000000000000
          Top = 3.149350000000000000
          Width = 676.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          StretchMode = smMaxHeight
          DataField = 'Texto'
          DataSet = frxDsCarta
          DataSetName = 'frxDsCarta'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6465
            666C616E67313034367B5C666F6E7474626C7B5C66305C666E696C204D532053
            616E732053657269663B7D7D0D0A7B5C636F6C6F7274626C203B5C726564305C
            677265656E305C626C7565303B7D0D0A7B5C2A5C67656E657261746F72204D73
            66746564697420352E34312E32312E323531303B7D5C766965776B696E64345C
            7563315C706172645C6366315C66305C667331365C7061720D0A7D0D0A00}
        end
      end
      object DadosMestre1: TfrxMasterData
        Height = 165.204700000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsSaCart
        DataSetName = 'frxDsSaCart'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 42.795300000000000000
          Top = 105.204700000000000000
          Width = 705.000000000000000000
          Height = 23.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 43.795300000000000000
          Top = 149.204700000000000000
          Width = 703.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CIDADE_E_DATA]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 43.795300000000000000
          Top = 34.204700000000000000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object RodapeDePagina1: TfrxPageFooter
        Height = 76.000000000000000000
        Top = 521.575140000000000000
        Width = 793.701300000000000000
        object Memo4: TfrxMemoView
          Left = 42.708661420000000000
          Top = 6.464130000000000000
          Width = 668.000000000000000000
          Height = 34.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."E_CUC"] :: [frxDsDono."TE1_TXT"]')
          ParentFont = False
        end
      end
      object CabecalhoDetalhe1: TfrxHeader
        Height = 20.000000000000000000
        Top = 207.874150000000000000
        Width = 793.701300000000000000
      end
      object CabecalhoDetalhe2: TfrxHeader
        Height = 4.000000000000000000
        Top = 309.921460000000000000
        Width = 793.701300000000000000
      end
      object DadosDeDetalhe2: TfrxDetailData
        Height = 83.338590000000000000
        Top = 336.378170000000000000
        Width = 793.701300000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        RowCount = 1
        object Memo36: TfrxMemoView
          Left = 339.362400000000000000
          Top = 1.204390000000000000
          Width = 377.952755910000000000
          Height = 19.779530000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Atenciosamente,')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.582870000000000000
          Top = 63.102040000000000000
          Width = 377.952755910000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
      end
      object DadosDeDetalhe3: TfrxDetailData
        Height = 20.000000000000000000
        Top = 442.205010000000000000
        Width = 793.701300000000000000
        OnBeforePrint = 'DadosDeDetalhe3OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        RowCount = 1
      end
    end
  end
  object frxDsSaCart: TfrxDBDataset
    UserName = 'frxDsSaCart'
    CloseDataSource = False
    DataSet = QrSaCart
    BCDToCurrency = False
    Left = 692
    Top = 4
  end
  object QrSaCart: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSaCartCalcFields
    SQL.Strings = (
      'SELECT li.Codigo, sa.Numero+0.000 Numero, sa.* '
      'FROM lotesits li'
      'LEFT JOIN sacados sa ON sa.CNPJ=li.CPF'
      'WHERE li.Controle=:P0'
      'GROUP BY li.CPF')
    Left = 664
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSaCartCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 15
    end
    object QrSaCartIE: TWideStringField
      FieldName = 'IE'
      Size = 25
    end
    object QrSaCartNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrSaCartRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrSaCartCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrSaCartBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrSaCartCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrSaCartUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrSaCartCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrSaCartTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrSaCartRisco: TFloatField
      FieldName = 'Risco'
    end
    object QrSaCartCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSaCartNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Calculated = True
    end
    object QrSaCartLNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LNR'
      Size = 255
      Calculated = True
    end
    object QrSaCartLN2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LN2'
      Size = 255
      Calculated = True
    end
    object QrSaCartCUC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CUC'
      Size = 255
      Calculated = True
    end
    object QrSaCartCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrSaCartNumero: TFloatField
      FieldName = 'Numero'
    end
  end
  object frxDsCarta: TfrxDBDataset
    UserName = 'frxDsCarta'
    CloseDataSource = False
    DataSet = QrCartas
    BCDToCurrency = False
    Left = 64
    Top = 8
  end
  object QrDupSac: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ba.Nome NOMEBANCO, li.Duplicata, li.Emissao, '
      'li.Valor, li.Vencto, li.Banco, li.Agencia, li.Data3'
      'FROM lotesits li'
      'LEFT JOIN bancos ba ON ba.Codigo=li.Banco'
      'WHERE li.Controle=:P0'
      '')
    Left = 720
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDupSacDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrDupSacEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrDupSacValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrDupSacVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrDupSacBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrDupSacAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrDupSacNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrDupSacData3: TDateField
      FieldName = 'Data3'
    end
  end
  object QrLotesIProt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM lotesiprot'
      'WHERE Controle=:P0'
      ''
      '')
    Left = 748
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotesIProtComarca: TWideStringField
      FieldName = 'Comarca'
      Size = 255
    end
    object QrLotesIProtNumDistrib: TWideStringField
      FieldName = 'NumDistrib'
      Size = 30
    end
    object QrLotesIProtLivro: TWideStringField
      FieldName = 'Livro'
      Size = 30
    end
    object QrLotesIProtFolhas: TWideStringField
      FieldName = 'Folhas'
      Size = 30
    end
  end
end
