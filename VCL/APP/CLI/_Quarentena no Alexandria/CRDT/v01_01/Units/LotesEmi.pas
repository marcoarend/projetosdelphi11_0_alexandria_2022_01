unit LotesEmi;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids;

type
  TFmLotesEmi = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    BtCancela: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    DBGrid1: TDBGrid;
    procedure BtOKClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FEmitenteNome: String;
  end;

  var
  FmLotesEmi: TFmLotesEmi;

implementation

uses Module, UnMyObjects;

{$R *.DFM}

procedure TFmLotesEmi.BtOKClick(Sender: TObject);
begin
  FEmitenteNome := Dmod.QrLocCPFNome.Value;
  Close;
end;

procedure TFmLotesEmi.BtCancelaClick(Sender: TObject);
begin
  FEmitenteNome := '';
  Close;
end;

procedure TFmLotesEmi.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmLotesEmi.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLotesEmi.DBGrid1DblClick(Sender: TObject);
begin
  BtOKClick(Self);
end;

procedure TFmLotesEmi.FormCreate(Sender: TObject);
begin
  FEmitenteNome := '';
end;

end.
