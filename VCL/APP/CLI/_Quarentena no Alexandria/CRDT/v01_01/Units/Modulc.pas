unit Modulc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, frxClass, frxDBSet, UnMLAGeral, dmkGeral;

type
  TDmoc = class(TDataModule)
    QrLocEmiBAC: TmySQLQuery;
    QrLocEmiBACBAC: TWideStringField;
    QrLocEmiBACCPF: TWideStringField;
    QrLocEmiCPF: TmySQLQuery;
    QrLCHs: TmySQLQuery;
    QrLCHsTipo: TIntegerField;
    QrLCHsOrdem: TIntegerField;
    QrLCHsDEmiss: TDateField;
    QrLCHsDCompra: TDateField;
    QrLCHsDVence: TDateField;
    QrLCHsDeposito: TDateField;
    QrLCHsComp: TIntegerField;
    QrLCHsBanco: TIntegerField;
    QrLCHsAgencia: TIntegerField;
    QrLCHsConta: TWideStringField;
    QrLCHsCPF: TWideStringField;
    QrLCHsEmitente: TWideStringField;
    QrLCHsCheque: TIntegerField;
    QrLCHsDuplicata: TWideStringField;
    QrLCHsValor: TFloatField;
    QrLCHsRua: TWideStringField;
    QrLCHsNumero: TIntegerField;
    QrLCHsCompl: TWideStringField;
    QrLCHsBirro: TWideStringField;
    QrLCHsCidade: TWideStringField;
    QrLCHsUF: TWideStringField;
    QrLCHsCEP: TIntegerField;
    QrLCHsTel1: TWideStringField;
    QrLCHsBAC: TWideStringField;
    QrLCHsDIAS: TFloatField;
    QrLCHsCPF_TXT: TWideStringField;
    QrLCHsSit: TIntegerField;
    QrLCHsMEU_SIT: TIntegerField;
    QrLCHsAberto: TFloatField;
    QrLCHsOcorrA: TFloatField;
    QrLCHsOcorrT: TFloatField;
    QrLCHsQtdeCH: TIntegerField;
    QrLCHsAberCH: TIntegerField;
    QrLCHsValAcum: TFloatField;
    QrLCHsIE: TWideStringField;
    QrLCHsDesco: TFloatField;
    QrLCHsBruto: TFloatField;
    QrLCHsDuplicado: TIntegerField;
    QrLCHsNOMEDUPLICADO: TWideStringField;
    QrLCHsPraca: TIntegerField;
    QrLCHsDisponiv: TFloatField;
    QrLCHsRISCO: TFloatField;
    DsLCHs: TDataSource;
    QrLDUs: TmySQLQuery;
    QrLDUsTipo: TIntegerField;
    QrLDUsOrdem: TIntegerField;
    QrLDUsDEmiss: TDateField;
    QrLDUsDCompra: TDateField;
    QrLDUsDVence: TDateField;
    QrLDUsDeposito: TDateField;
    QrLDUsComp: TIntegerField;
    QrLDUsBanco: TIntegerField;
    QrLDUsAgencia: TIntegerField;
    QrLDUsConta: TWideStringField;
    QrLDUsCPF: TWideStringField;
    QrLDUsEmitente: TWideStringField;
    QrLDUsCheque: TIntegerField;
    QrLDUsDuplicata: TWideStringField;
    QrLDUsValor: TFloatField;
    QrLDUsRua: TWideStringField;
    QrLDUsNumero: TIntegerField;
    QrLDUsCompl: TWideStringField;
    QrLDUsBirro: TWideStringField;
    QrLDUsCidade: TWideStringField;
    QrLDUsUF: TWideStringField;
    QrLDUsCEP: TIntegerField;
    QrLDUsTel1: TWideStringField;
    QrLDUsCPF_2: TWideStringField;
    QrLDUsNOME_2: TWideStringField;
    QrLDUsDIAS: TFloatField;
    QrLDUsCPF_TXT: TWideStringField;
    QrLDUsSit: TIntegerField;
    QrLDUsMEU_SIT: TIntegerField;
    QrLDUsAberto: TFloatField;
    QrLDUsOcorrA: TFloatField;
    QrLDUsOcorrT: TFloatField;
    QrLDUsAberCH: TIntegerField;
    QrLDUsQtdeCH: TIntegerField;
    QrLDUsRUA_2: TWideStringField;
    QrLDUsCOMPL_2: TWideStringField;
    QrLDUsBAIRRO_2: TWideStringField;
    QrLDUsCIDADE_2: TWideStringField;
    QrLDUsUF_2: TWideStringField;
    QrLDUsCEP_2: TIntegerField;
    QrLDUsTEL1_2: TWideStringField;
    QrLDUsTEL1_TXT: TWideStringField;
    QrLDUsTEL1_2_TXT: TWideStringField;
    QrLDUsCEP_TXT: TWideStringField;
    QrLDUsCEP_2_TXT: TWideStringField;
    QrLDUsNUMERO_2_TXT: TWideStringField;
    QrLDUsNUMERO_TXT: TWideStringField;
    QrLDUsIE: TWideStringField;
    QrLDUsIE_2: TWideStringField;
    QrLDUsDesco: TFloatField;
    QrLDUsBruto: TFloatField;
    QrLDUsValAcum: TFloatField;
    QrLDUsDuplicado: TIntegerField;
    QrLDUsNOMEDUPLICADO: TWideStringField;
    QrLDUsPraca: TIntegerField;
    QrLDUsDisponiv: TFloatField;
    QrLDUsRISCOEM: TFloatField;
    QrLDUsRISCOSA: TFloatField;
    QrLDUsRISCO: TFloatField;
    DsLDUs: TDataSource;
    QrEmitBAC: TmySQLQuery;
    QrEmitBACBAC: TWideStringField;
    QrEmitBACCPF: TWideStringField;
    QrValAcum: TmySQLQuery;
    QrValAcumValor: TFloatField;
    QrValAcumCPF: TWideStringField;
    QrValAcumAberto: TFloatField;
    QrValAcumRISCOEM: TFloatField;
    QrValAcumRISCOSA: TFloatField;
    QrLocEmiCPFCPF: TWideStringField;
    QrLocEmiCPFNome: TWideStringField;
    QrLocEmiCPFLimite: TFloatField;
    QrLocEmiCPFLastAtz: TDateField;
    QrLocEmiCPFAcumCHComV: TFloatField;
    QrLocEmiCPFAcumCHComQ: TIntegerField;
    QrLocEmiCPFAcumCHDevV: TFloatField;
    QrLocEmiCPFAcumCHDevQ: TIntegerField;
    QrQuit: TmySQLQuery;
    QrQuitCodigo: TIntegerField;
    QrQuitRegistros: TFloatField;
    QrQuitItens: TIntegerField;
    QrLDUsBAC: TWideStringField;
    QrBanco: TmySQLQuery;
    QrBancoNome: TWideStringField;
    QrBancoDVCC: TSmallintField;
    DsBanco: TDataSource;
    QrSobraIni: TmySQLQuery;
    QrSobraIniData: TDateField;
    QrSobraIniCodigo: TIntegerField;
    QrOutros: TmySQLQuery;
    QrOutrosData: TDateField;
    QrOutrosCodigo: TIntegerField;
    QrOutrosLote: TIntegerField;
    frxOutros: TfrxReport;
    frxDsOutros: TfrxDBDataset;
    QrSobraIniSobraNow: TFloatField;
    QrOutrosSobraNow: TFloatField;
    Qrwlc: TmySQLQuery;
    QrwlcCodigo: TAutoIncField;
    QrwlcCliente: TIntegerField;
    QrwlcLote: TIntegerField;
    QrwlcTipo: TSmallintField;
    QrwlcData: TDateField;
    QrwlcTotal: TFloatField;
    QrwlcDias: TFloatField;
    QrwlcItens: TIntegerField;
    QrwlcBaixado: TIntegerField;
    QrwlcCPF: TWideStringField;
    Qrwli: TmySQLQuery;
    QrwliCodigo: TIntegerField;
    QrwliControle: TAutoIncField;
    QrwliComp: TIntegerField;
    QrwliPraca: TIntegerField;
    QrwliBanco: TIntegerField;
    QrwliAgencia: TIntegerField;
    QrwliConta: TWideStringField;
    QrwliCheque: TIntegerField;
    QrwliCPF: TWideStringField;
    QrwliEmitente: TWideStringField;
    QrwliBruto: TFloatField;
    QrwliDesco: TFloatField;
    QrwliValor: TFloatField;
    QrwliEmissao: TDateField;
    QrwliDCompra: TDateField;
    QrwliDDeposito: TDateField;
    QrwliVencto: TDateField;
    QrwliDias: TIntegerField;
    QrwliDuplicata: TWideStringField;
    QrwliUser_ID: TIntegerField;
    QrwliPasso: TIntegerField;
    QrwliRua: TWideStringField;
    QrwliNumero: TLargeintField;
    QrwliCompl: TWideStringField;
    QrwliBairro: TWideStringField;
    QrwliCidade: TWideStringField;
    QrwliUF: TWideStringField;
    QrwliCEP: TIntegerField;
    QrwliTel1: TWideStringField;
    QrwliIE: TWideStringField;
    QrSumLLC: TmySQLQuery;
    QrSumLLCTotal: TFloatField;
    QrLLC: TmySQLQuery;
    QrLLCCodigo: TIntegerField;
    QrLLCCliente: TIntegerField;
    QrLLCLote: TIntegerField;
    QrLLCTipo: TSmallintField;
    QrLLCData: TDateField;
    QrLLCTotal: TFloatField;
    QrLLCDias: TFloatField;
    QrLLCItens: TIntegerField;
    QrLLCBaixado: TIntegerField;
    QrLLCCPF: TWideStringField;
    QrLLCNOMECLI: TWideStringField;
    QrLLCNOMETIPO: TWideStringField;
    QrLLI: TmySQLQuery;
    QrLLICodigo: TIntegerField;
    QrLLIControle: TIntegerField;
    QrLLIComp: TIntegerField;
    QrLLIPraca: TIntegerField;
    QrLLIBanco: TIntegerField;
    QrLLIAgencia: TIntegerField;
    QrLLIConta: TWideStringField;
    QrLLICheque: TIntegerField;
    QrLLICPF: TWideStringField;
    QrLLIEmitente: TWideStringField;
    QrLLIBruto: TFloatField;
    QrLLIDesco: TFloatField;
    QrLLIValor: TFloatField;
    QrLLIEmissao: TDateField;
    QrLLIDCompra: TDateField;
    QrLLIDDeposito: TDateField;
    QrLLIVencto: TDateField;
    QrLLIDias: TIntegerField;
    QrLLIDuplicata: TWideStringField;
    QrLLIIE: TWideStringField;
    QrLLIRua: TWideStringField;
    QrLLINumero: TLargeintField;
    QrLLICompl: TWideStringField;
    QrLLIBairro: TWideStringField;
    QrLLICidade: TWideStringField;
    QrLLIUF: TWideStringField;
    QrLLICEP: TIntegerField;
    QrLLITel1: TWideStringField;
    QrLLIUser_ID: TIntegerField;
    QrLLIPasso: TIntegerField;
    DsLLC: TDataSource;
    QrSumLLCItens: TFloatField;
    QrUDLC: TmySQLQuery;
    QrVeri: TmySQLQuery;
    QrVeriControle: TIntegerField;
    QrCtrl: TmySQLQuery;
    QrCtrlCodigo: TIntegerField;
    QrCtrlControle: TAutoIncField;
    QrLocSacados: TmySQLQuery;
    QrLocSacadosCNPJ: TWideStringField;
    QrLocSacadosIE: TWideStringField;
    QrLocSacadosNome: TWideStringField;
    QrLocSacadosRua: TWideStringField;
    QrLocSacadosNumero: TLargeintField;
    QrLocSacadosCompl: TWideStringField;
    QrLocSacadosBairro: TWideStringField;
    QrLocSacadosCidade: TWideStringField;
    QrLocSacadosUF: TWideStringField;
    QrLocSacadosCEP: TIntegerField;
    QrLocSacadosTel1: TWideStringField;
    QrLocSacadosRisco: TFloatField;
    QrLocSacadosAlterWeb: TSmallintField;
    QrLocSacadosAtivo: TSmallintField;
    QrLCHsCPF_2: TWideStringField;
    QrLCHsNome_2: TWideStringField;
    QrLCHsRISCOEM: TFloatField;
    QrLCHsRISCOSA: TFloatField;
    QrLCHsRua_2: TWideStringField;
    QrLCHsNumero_2: TIntegerField;
    QrLCHsCompl_2: TWideStringField;
    QrLCHsIE_2: TWideStringField;
    QrLCHsBairro_2: TWideStringField;
    QrLCHsCidade_2: TWideStringField;
    QrLCHsUF_2: TWideStringField;
    QrLCHsCEP_2: TIntegerField;
    QrLCHsTEL1_2: TWideStringField;
    QrLDUsNumero_2: TIntegerField;
    QrLCHsStatusSPC: TSmallintField;
    QrLDUsStatusSPC: TSmallintField;
    QrLDUsTEXTO_SPC: TWideStringField;
    QrLCHsTEXTO_SPC: TWideStringField;
    QrSPC_Cfg: TmySQLQuery;
    QrSPC_CfgSPC_ValMin: TFloatField;
    QrSPC_CfgSPC_ValMax: TFloatField;
    QrSPC_CfgServidor: TWideStringField;
    QrSPC_CfgSPC_Porta: TSmallintField;
    QrSPC_CfgPedinte: TWideStringField;
    QrSPC_CfgCodigSocio: TIntegerField;
    QrSPC_CfgModalidade: TIntegerField;
    QrSPC_CfgInfoExtra: TIntegerField;
    QrSPC_CfgBAC_CMC7: TSmallintField;
    QrSPC_CfgTipoCred: TSmallintField;
    QrSPC_CfgSenhaSocio: TWideStringField;
    QrSPC_CfgCodigo: TIntegerField;
    QrSPC_CfgValAviso: TFloatField;
    QrSPC_CfgVALCONSULTA: TFloatField;
    QrSCHs: TmySQLQuery;
    QrSCHsValor: TFloatField;
    QrSCHsItens: TLargeintField;
    DsSCHs: TDataSource;
    DsSDUs: TDataSource;
    QrSDUs: TmySQLQuery;
    QrSDUsValor: TFloatField;
    QrSDUsItens: TLargeintField;
    QrSDUsTOTAL_V: TFloatField;
    QrSDUsTOTAL_Q: TFloatField;
    QrILd: TmySQLQuery;
    QrILdCPF: TWideStringField;
    QrUpdLocDB: TmySQLQuery;
    procedure QrLCHsCalcFields(DataSet: TDataSet);
    procedure QrLCHsAfterScroll(DataSet: TDataSet);
    procedure QrLDUsCalcFields(DataSet: TDataSet);
    procedure QrLDUsAfterScroll(DataSet: TDataSet);
    procedure QrSCHsBeforeClose(DataSet: TDataSet);
    procedure QrSCHsAfterOpen(DataSet: TDataSet);
    procedure QrSDUsAfterOpen(DataSet: TDataSet);
    procedure QrSDUsBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    FORDA_LCHs, FORDA_LDUs, FORDB_LCHs,FORDB_LDUs: String;
    procedure ReopenBanco(Banco: String);
    procedure AtualizaEmitente(Banco, Agencia, Conta, CPF, Nome:
              String; Risco: Double);
    function BaixaLotesWeb: Integer;
    function ConectaBDn(Pergunta: Boolean): Boolean;
    procedure ReopenLLoteCab(Status: Integer);
    procedure AlterarNaWeb_Lote(Lote: Integer);
    //procedure VerificaControle(Codigo, Controle: Integer);
    procedure VerificaCPFCNP_XX_Importacao(NomeEnt: String; Entidade, Pagina:
              Integer; SelType: TSelType);
    procedure VerificaCPFCNP_CH_Importacao(SelType: TSelType);
    procedure VerificaCPFCNP_DU_Importacao(SelType: TSelType);
    procedure ReopenImportLote(CH, DU: Integer);
    function DataTPCheque(): TDateTime;
    procedure HabilitaControle(Controle0, Controle1: TControl; Habilita: Boolean);
    procedure HabilitaBotoes(Meu_Sit, TipoDoc: Integer);
  end;

var
  Dmoc: TDmoc;

implementation

{$R *.DFM}

uses Module, Lotes1, UnitSPC, UnInternalConsts, ModuleLotes, Principal, UCreate;

procedure TDmoc.HabilitaBotoes(Meu_Sit, TipoDoc: Integer);
begin
  case FmPrincipal.FFormLotesShow of
    //0: FmLotes0.HabilitaBotoes(Meu_Sit, TipoDoc);
    1: FmLotes1.HabilitaBotoes(Meu_Sit, TipoDoc);
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (08)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
end;

function TDmoc.DataTPCheque(): TDateTime;
begin
  Result := 0;
  case FmPrincipal.FFormLotesShow of
    //0: Result := FmLotes0.TPCheque.Date;
    1: Result := FmLotes1.TPCheque.Date;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (07)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
end;

procedure TDmoc.QrLCHsCalcFields(DataSet: TDataSet);
begin
  QrLCHsBAC.Value :=
    FormatFloat('000', QrLCHsBanco.Value) +
    FormatFloat('0000', QrLCHsAgencia.Value) +
    QrLCHsConta.Value ;
  QrLCHsDIAS.Value := QrLCHsDVence.Value - Int(DataTPCheque);
  QrLCHsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrLCHsCPF.Value);
  //
  if QrLCHsSit.Value = 2 then QrLCHsMEU_SIT.Value := 2 else
  if QrLCHsCPF_2.Value = '' then QrLCHsMEU_SIT.Value := 0
  else if QrLCHsCPF.Value = QrLCHsCPF_2.Value then
  begin
    if AnsiCompareText(QrLCHsEmitente.Value, QrLCHsNOME_2.Value) = 0 then
      QrLCHsMEU_SIT.Value := 2 else QrLCHsMEU_SIT.Value := 1;
  end else
  QrLCHsMEU_SIT.Value := -1;
  //
  if QrLCHsDuplicado.Value = 0 then QrLCHsNOMEDUPLICADO.Value := 'N'
  else QrLCHsNOMEDUPLICADO.Value := 'S';
  //
  QrLCHsRISCO.Value := QrLCHsRISCOEM.Value + QrLCHsRISCOSA.Value;
  //
  QrLCHsTEXTO_SPC.Value := MLAGeral.StatusSPC_TXT(QrLCHsStatusSPC.Value);
end;

procedure TDmoc.QrLCHsAfterScroll(DataSet: TDataSet);
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
  HabilitaBotoes(QrLCHsMEU_SIT.Value, 0);
  //
  DmLotes.QrCHOpen.Close;
  DmLotes.QrCHOpen.Params[0].AsString  := QrLCHsCPF.Value;
  DmLotes.QrCHOpen.Open;
  //
  DmLotes.QrAlinIts.Close;
  DmLotes.QrAlinIts.Params[0].AsString := QrLCHsCPF.Value;
  DmLotes.QrAlinIts.Open;
  //
  DmLotes.ReopenSPC_Result(QrLCHsCPF.Value, 0);
  //
  Screen.Cursor := MyCursor;
  except
    Screen.Cursor := MyCursor;
    raise;
  end;
end;

procedure TDmoc.QrLDUsCalcFields(DataSet: TDataSet);
begin
  QrLDUsDIAS.Value := QrLDUsDVence.Value - Int(DataTPCheque);
  QrLDUsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrLDUsCPF.Value);
  //
  if QrLDUsSit.Value = 2 then QrLDUsMEU_SIT.Value := 2 else
  if QrLDUsCPF_2.Value = '' then QrLDUsMEU_SIT.Value := 0
  else if QrLDUsCPF.Value = QrLDUsCPF_2.Value then
  begin
    if (AnsiCompareText(QrLDUsEmitente.Value, QrLDUsNOME_2.Value) = 0)
    and (AnsiCompareText(QrLDUsIE.Value, QrLDUsIE_2.Value) = 0)
    and (AnsiCompareText(QrLDUsEmitente.Value, QrLDUsNOME_2.Value) = 0)
    and (AnsiCompareText(QrLDUsRua.Value, QrLDUsRUA_2.Value) = 0)
    and (AnsiCompareText(QrLDUsCompl.Value, QrLDUsCOMPL_2.Value) = 0)
    and (AnsiCompareText(QrLDUsBirro.Value, QrLDUsBAIRRO_2.Value) = 0)
    and (AnsiCompareText(QrLDUsCidade.Value, QrLDUsCIDADE_2.Value) = 0)
    and (AnsiCompareText(QrLDUsUF.Value, QrLDUsUF_2.Value) = 0)
    and (AnsiCompareText(Geral.SoNumero_TT(QrLDUsTel1.Value), QrLDUsTEL1_2.Value) = 0)
    and (QrLDUsNumero.Value = QrLDUsNUMERO_2.Value)
    and (Geral.FormataCEP_NT(QrLDUsCEP.Value) = Geral.FormataCEP_NT(QrLDUsCEP_2.Value))
    then QrLDUsMEU_SIT.Value := 2 else QrLDUsMEU_SIT.Value := 1;
  end else QrLDUsMEU_SIT.Value := -1;

  //////////////////////////////////////////////////////////////////////////////

  QrLDUsCEP_TXT.Value := Geral.FormataCEP_NT(QrLDUsCEP.Value);
  QrLDUsCEP_2_TXT.Value := Geral.FormataCEP_NT(QrLDUsCEP_2.Value);
  //
  QrLDUsTel1_TXT.Value := MLAGeral.FormataTelefone_TT_Curto(QrLDUsTel1.Value);
  QrLDUsTel1_2_TXT.Value := MLAGeral.FormataTelefone_TT_Curto(QrLDUsTel1_2.Value);
  //
  QrLDUsNumero_TXT.Value := MLAGeral.FormataNumeroDeRua(QrLDUsRua.Value, QrLDUsNumero.Value, False);
  QrLDUsNumero_2_TXT.Value := MLAGeral.FormataNumeroDeRua(QrLDUsRUA_2.Value, Trunc(QrLDUsNUMERO_2.Value), False);
  //
  if QrLDUsDuplicado.Value = 0 then QrLDUsNOMEDUPLICADO.Value := 'N'
  else QrLDUsNOMEDUPLICADO.Value := 'S';
  //
  QrLDUsRISCO.Value := QrLDUsRISCOEM.Value + QrLDUsRISCOSA.Value;
  //
  QrLDUsTEXTO_SPC.Value := MLAGeral.StatusSPC_TXT(QrLDUsStatusSPC.Value);
end;

procedure TDmoc.HabilitaControle(Controle0, Controle1: TControl; Habilita: Boolean);
begin
  case FmPrincipal.FFormLotesShow of
    0: if Controle0 <> nil then Controle0.Enabled := Habilita;
    1: if Controle1 <> nil then Controle1.Enabled := Habilita;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (09)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;

end;

procedure TDmoc.QrSCHsAfterOpen(DataSet: TDataSet);
begin
  HabilitaControle(nil, FmLotes1.BtSPC_CH, QrSCHs.RecordCount > 0);
end;

procedure TDmoc.QrSCHsBeforeClose(DataSet: TDataSet);
begin
  HabilitaControle(nil, FmLotes1.BtSPC_CH, False);
end;

procedure TDmoc.QrSDUsAfterOpen(DataSet: TDataSet);
begin
  HabilitaControle(nil, FmLotes1.BtSPC_DU, QrSDUs.RecordCount > 0);
end;

procedure TDmoc.QrSDUsBeforeClose(DataSet: TDataSet);
begin
  HabilitaControle(nil, FmLotes1.BtSPC_DU, False);
end;

procedure TDmoc.QrLDUsAfterScroll(DataSet: TDataSet);
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
  HabilitaBotoes(QrLDUsMEU_SIT.Value, 1);
  //
  DmLotes.QrDUOpen.Close;
  DmLotes.QrDUOpen.Params[0].AsString  := QrLDUsCPF.Value;
  DmLotes.QrDUOpen.Open;
  //
  DmLotes.QrDUVenc.Close;
  DmLotes.QrDUVenc.Params[0].AsString  := QrLDUsCPF.Value;
  DmLotes.QrDUVenc.Open;
  //
  DmLotes.ReopenSPC_Result(QrLDUsCPF.Value, 0);
  //
  Screen.Cursor := MyCursor;
  except
    Screen.Cursor := MyCursor;
    raise;
  end;
end;

procedure TDmoc.AtualizaEmitente(Banco, Agencia, Conta, CPF, Nome:
String; Risco: Double);
var
  Registros: Integer;
  MyCursor: TCursor;
begin
  if Trim(CPF) = '' then Exit;
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
  CPF := Geral.SoNumero_TT(CPF);
  //
  QrEmitBAC.Close;
  QrEmitBAC.Params[0].AsString := Banco+Agencia+Conta;
  QrEmitBAC.Params[1].AsString := CPF;
  QrEmitBAC.Open;
  //
  QrLocEmiCPF.Close;
  QrLocEmiCPF.Params[0].AsString := CPF;
  QrLocEmiCPF.Open;
  //
  if Risco = -1 then Risco := QrLocEmiCPFLimite.Value;
  if (Risco < 0.01) then
    Risco := Dmod.QrControleCHRisco.Value;
  if QrEmitBAC.RecordCount = 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO emitbac SET BAC=:P0, CPF=:P1 ');
    Dmod.QrUpd.Params[0].AsString := Banco+Agencia+Conta;
    Dmod.QrUpd.Params[1].AsString := CPF;
    Dmod.QrUpd.ExecSQL;
  end;
  //
  Registros := QrLocEmiCPF.RecordCount;
  Dmod.QrUpd.SQL.Clear;
  if Registros = 0 then
    Dmod.QrUpd.SQL.Add('INSERT INTO emitcpf SET ')
  else
    Dmod.QrUpd.SQL.Add('UPDATE emitcpf SET ');
  Dmod.QrUpd.SQL.Add('Nome=:P0, Limite=:P1, LastAtz=:P2');
  if Registros = 0 then
    Dmod.QrUpd.SQL.Add(', CPF=:Pa')
  else
    Dmod.QrUpd.SQL.Add('WHERE CPF=:Pa');
  Dmod.QrUpd.Params[0].AsString  := Nome;
  Dmod.QrUpd.Params[1].AsFloat   := Risco;
  Dmod.QrUpd.Params[2].AsString  := Geral.FDT(Now, 1);
  //
  Dmod.QrUpd.Params[3].AsString  := CPF;
  Dmod.QrUpd.ExecSQL;
  //

  //  ATUALIZAR ITENS SENDO IMPORTADOS
  //Cria a tabela caso n�o exista!

  QrUpdLocDB.Close;
  QrUpdLocDB.SQL.Clear;
  QrUpdLocDB.SQL.Add('SHOW TABLES ');
  QrUpdLocDB.SQL.Add('LIKE "importlote"');
  QrUpdLocDB.Open;
  if QrUpdLocDB.RecordCount = 0 then
    UCriar.RecriaTabelaLocal('ImportLote', 1);
  //
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE importlote SET ');
  Dmod.QrUpdL.SQL.Add('Nome_2=:P0, RISCOEM=:P1, ');
  Dmod.QrUpdL.SQL.Add('CPF_2=:Pa WHERE CPF=:Pb');
  Dmod.QrUpdL.Params[00].AsString  := Nome;
  Dmod.QrUpdL.Params[01].AsFloat   := Risco;
  Dmod.QrUpdL.Params[02].AsString  := CPF;
  //
  Dmod.QrUpdL.Params[03].AsString  := CPF;
  Dmod.QrUpdL.ExecSQL;
  //

  Screen.Cursor := MyCursor;
  except
    Screen.Cursor := MyCursor;
    raise;
  end;
end;

procedure TDmoc.ReopenBanco(Banco: String);
begin
  QrBanco.Close;
  QrBanco.Params[0].AsString := Banco;
  QrBanco.Open;
end;

function TDmoc.BaixaLotesWeb: Integer;
var
  Baixar: Integer;
begin
  // Parei Aqui Fazer conex�o do BD, se este estiver desconectado
  if not ConectaBDn(True) then
  begin
    Result := 0;
    Exit;
  end;
  Qrwlc.Close;
  Qrwlc.Open;
  Baixar := Qrwlc.RecordCount;
  case Baixar of
    0: Application.MessageBox('N�o h� lote novo na web!', 'Informa��o',
      MB_OK+MB_ICONINFORMATION);
    1: if Application.MessageBox(PChar('Foi localizado um lote novo na web! '+
       'Deseja baix�-lo?'), 'Pergunta', MB_YESNO+MB_ICONQUESTION) <> ID_YES then
       Baixar := 0;
    else if Application.MessageBox(PChar('Foram localizados ' +
       IntToStr(Baixar) + ' lotes novos na web! Deseja baix�-los?'), 'Pergunta',
       MB_YESNO+MB_ICONQUESTION) <> ID_YES then Baixar := 0;
  end;
  if Baixar > 0 then
  begin
    Screen.Cursor := crHourGlass;
    try
      Qrwlc.First;
      while not Qrwlc.Eof do
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM llotecab WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrwlcCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM lloteits WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrwlcCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO llotecab SET ');
        Dmod.QrUpd.SQL.Add('Codigo=:P0, Cliente=:P1, Lote=:P2, Tipo=:P3, ');
        Dmod.QrUpd.SQL.Add('Data=:P4, Total=:P5, Itens=:P6');

        Dmod.QrUpd.Params[00].AsInteger := QrwlcCodigo.Value;
        Dmod.QrUpd.Params[01].AsInteger := QrwlcCliente.Value;
        Dmod.QrUpd.Params[02].AsInteger := QrwlcLote.Value;
        Dmod.QrUpd.Params[03].AsInteger := QrwlcTipo.Value;
        Dmod.QrUpd.Params[04].AsString  := Geral.FDT(QrwlcData.Value, 1);
        Dmod.QrUpd.Params[05].AsFloat   := QrwlcTotal.Value;
        Dmod.QrUpd.Params[06].AsInteger := QrwlcItens.Value;
        Dmod.QrUpd.ExecSQL;
        //
        Qrwli.Close;
        Qrwli.Params[0].AsInteger := QrwlcCodigo.Value;
        Qrwli.Open;
        //
        Qrwli.First;
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO lloteits SET ');
        Dmod.QrUpd.SQL.Add('Codigo=:P0, Controle=:P1, Praca=:P2, Banco=:P3, ');
        Dmod.QrUpd.SQL.Add('Agencia=:P4, Conta=:P5, Cheque=:P6, CPF=:P7, ');
        Dmod.QrUpd.SQL.Add('Emitente=:P8, Bruto=:P9, Valor=:P10, ');
        Dmod.QrUpd.SQL.Add('Vencto=:P11, Duplicata=:P12, Rua=:P13, ');
        Dmod.QrUpd.SQL.Add('Numero=:P14, Compl=:P15, Bairro=:P16, ');
        Dmod.QrUpd.SQL.Add('Cidade=:P17, UF=:P18, CEP=:P19, Tel1=:P20, ');
        Dmod.QrUpd.SQL.Add('IE=:P21, Emissao=:P22');
        Dmod.QrUpd.SQL.Add('');
        //Controle = VerificaControle(QrwliCodigo.Value, QrwliControle.Value);
        while not Qrwli.Eof do
        begin
          Dmod.QrUpd.Params[00].AsInteger := QrwliCodigo.Value;
          Dmod.QrUpd.Params[01].AsInteger := QrwliControle.Value;
          Dmod.QrUpd.Params[02].AsInteger := QrwliPraca.Value;
          Dmod.QrUpd.Params[03].AsInteger := QrwliBanco.Value;
          Dmod.QrUpd.Params[04].AsInteger := QrwliAgencia.Value;
          Dmod.QrUpd.Params[05].AsString  := QrwliConta.Value;
          Dmod.QrUpd.Params[06].AsInteger := QrwliCheque.Value;
          Dmod.QrUpd.Params[07].AsString  := QrwliCPF.Value;
          Dmod.QrUpd.Params[08].AsString  := QrwliEmitente.Value;
          Dmod.QrUpd.Params[09].AsFloat   := QrwliBruto.Value;
          Dmod.QrUpd.Params[10].AsFloat   := QrwliValor.Value;
          Dmod.QrUpd.Params[11].AsString  := Geral.FDT(QrwliVencto.Value, 1);
          Dmod.QrUpd.Params[12].AsString  := QrwliDuplicata.Value;
          Dmod.QrUpd.Params[13].AsString  := QrwliRua.Value;
          Dmod.QrUpd.Params[14].AsInteger := QrwliNumero.Value;
          Dmod.QrUpd.Params[15].AsString  := QrwliCompl.Value;
          Dmod.QrUpd.Params[16].AsString  := QrwliBairro.Value;
          Dmod.QrUpd.Params[17].AsString  := QrwliCidade.Value;
          Dmod.QrUpd.Params[18].AsString  := QrwliUF.Value;
          Dmod.QrUpd.Params[19].AsInteger := QrwliCEP.Value;
          Dmod.QrUpd.Params[20].AsString  := QrwliTel1.Value;
          Dmod.QrUpd.Params[21].AsString  := QrwliIE.Value;
          Dmod.QrUpd.Params[22].AsString  := Geral.FDT(QrwliEmissao.Value, 1);
          Dmod.QrUpd.ExecSQL;
          Qrwli.Next;
        end;
        QrSumLLC.Close;
        QrSumLLC.Params[0].AsInteger := QrwlcCodigo.Value;
        QrSumLLC.Open;
        //
        Dmod.QrWeb.SQL.Clear;
        Dmod.QrWeb.SQL.Add('UPDATE wlotecab SET Baixado=2, Total=:P0, Itens=:P1 ');
        Dmod.QrWeb.SQL.Add('WHERE Codigo=:P2');
        Dmod.QrWeb.Params[00].AsFloat   := QrSumLLCTotal.Value;
        Dmod.QrWeb.Params[01].AsInteger := Trunc(QrSumLLCItens.Value);
        Dmod.QrWeb.Params[02].AsInteger := QrwlcCodigo.Value;
        Dmod.QrWeb.ExecSQL;
        //
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE llotecab SET Baixado=2, Total=:P0, Itens=:P1 ');
        Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2');
        Dmod.QrUpd.Params[00].AsFloat   := QrSumLLCTotal.Value;
        Dmod.QrUpd.Params[01].AsInteger := Trunc(QrSumLLCItens.Value);
        Dmod.QrUpd.Params[02].AsInteger := QrwlcCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        Qrwlc.Next;
      end;
      Application.MessageBox('Download conclu�do com sucesso!', 'Informa��o',
        MB_OK+MB_ICONINFORMATION);
      Screen.Cursor := crDefault;
    finally
      Screen.Cursor := crDefault;
    end;  
  end;
  Result := Baixar;
end;

function TDmoc.ConectaBDn(Pergunta: Boolean): Boolean;
var
  Conectar: Integer;
  BD, IP, PW, ID: String;
begin
  Result := False;
  if not Dmod.MyDBn.Connected then
  begin
    if Pergunta then Conectar := Application.MessageBox(PChar('A base de dados'+
    ' web est� desconectada. Deseja se conectar agora?'), 'Pergunta', MB_YESNO+
    MB_ICONQUESTION) else Conectar := ID_YES;
    if Conectar = ID_YES then
    begin
      try
        IP := Dmod.QrControleWeb_Host.Value;
        ID := Dmod.QrControleWeb_User.Value;
        PW := Dmod.QrControleWeb_Pwd.Value;
        BD := Dmod.QrControleWeb_DB.Value;
        if MLAGeral.FaltaInfo(4,
        [IP,'Servidor',ID,'Usu�rio',PW,'Senha',BD,'Banco de dados'],
        'Conex�o ao MySQL no meu site') then
        begin
          Result := False;
          Exit;
        end;
        //
        Dmod.MyDBn.Connected    := False;
        Dmod.MyDBn.UserPassword := PW;
        Dmod.MyDBn.UserName     := ID;
        Dmod.MyDBn.Host         := IP;
        Dmod.MyDBn.DatabaseName := BD; // J� deve existir (configurado pelo hospedeiro)
        Dmod.MyDBn.Connected := True;
        Result := True;
      except
        Application.MessageBox(PChar('N�o foi poss�vel a conex�o � base de '+
        'dados web!'), 'Aviso', MB_OK+ MB_ICONWARNING);
        raise;
      end;
    end;
  end else Result := True;
end;

procedure TDmoc.ReopenLLoteCab(Status: Integer);
begin
  //Dmoc.ReopenLLoteCab(CgStatus.Value);
  // Parei Aqui
end;

procedure TDmoc.AlterarNaWeb_Lote(Lote: Integer);
begin
  QrUDLC.Params[0].AsInteger := Lote;
  QrUDLC.ExecSQL;
end;

{function TDmoc.VerificaControle(Codigo, Controle: Integer): Integer;
begin
  Result := Controle;
  QrVeri.Close;
  QrVeri.Open;
  if QrVeriControle.Value > Controle then
  begin
    Result := QrVeriControle.Value + 1;
    Dmod.QrWeb.Close;
    Dmod.QrWeb.SQL.Clear;
    Dmod.QrWeb.SQL.Add('UPDATE wloteits SET ');
    Dmod.QrWeb.SQL.Add('Controle=:P0');
    Dmod.QrWeb.SQL.Add('WHERE Controle=:P1 AND Codigo=:P2');
    //
    Dmod.QrWeb.Params[00].AsInteger = Result;
    Dmod.QrWeb.Params[01].AsInteger = Controle;
    Dmod.QrWeb.Params[02].AsInteger = Codigo;
    Dmod.QrWeb.ExecSQL;
  end;
end;}

procedure TDmoc.VerificaCPFCNP_XX_Importacao(NomeEnt: String; Entidade, Pagina: Integer;
SelType: TSelType);
begin
  QrSPC_Cfg.Close;
  QrSPC_Cfg.Params[0].AsInteger := Entidade;
  QrSPC_Cfg.Open;
  //
  if QrSPC_CfgCodigo.Value = 0 then
  begin
    Application.MessageBox(PChar('O cliente ' + IntToStr(Entidade) + ' - ' +
    NomeEnt + ' n�o possui configura��o de pesquisa ao SPC em seu cadastro!'),
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  case Pagina of
    0: VerificaCPFCNP_CH_Importacao(SelType);
    1: VerificaCPFCNP_DU_Importacao(SelType);
    else Application.MessageBox(PChar('Aba n�o definida para consulta de CPF ' +
    '/CNPJ na importa��o de lote!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TDmoc.VerificaCPFCNP_CH_Importacao(SelType: TSelType);
  procedure VerificaCPF_Atual(const MinMax: Boolean; var Contagem: Integer;
  AvisaValor: Boolean);
  var
    TipoDoc, Solicitante, RG, UF, Endereco, Telefone, CEP, Banda: String;
    Nascimento: TDateTime;
    DigitoCC, DigitoCH, Qtde: Integer;
    //
    StatusSPC: Integer;
    ValMax, ValMin: Double;
  begin
    if Length(QrLCHsCPF.Value) = 11 then
      TipoDoc := 'CPF'
    else
      TipoDoc := 'CNPJ';
    FmLotes1.StCPF_CH.Caption := 'Aguarde... consultando ' + TipoDoc +
      Geral.FormataCNPJ_TT(QrLCHsCPF.Value);
    FmLotes1.StCPF_CH.Update;
    //
    Application.ProcessMessages;
    //
    Solicitante := VAR_LOGIN + '#' + FormatFloat('0', VAR_USUARIO);
    RG          := '';
    UF          := '';
    Nascimento  := 0;
    //
    //MLAGeral.GeraCMC7_30(Comp, Banco, Agencia, Conta, Cheque, Tipif, Banda);
    Banda       := '';
    //
    DigitoCC    := 0;
    DigitoCH    := 0;
    Qtde        := 1;
    //
    Endereco    := '';
    Telefone    := '';
    CEP         := '';
    //
    if MinMax then
    begin
      ValMin := QrSPC_CfgSPC_ValMin.Value;
      ValMax := QrSPC_CfgSPC_ValMax.Value;
    end else begin
      ValMin := 0;
      ValMax := 0;
    end;
    StatusSPC :=
    UnSPC.ConsultaSPC_Rapida(QrSPC_CfgCodigo.Value, True, QrLCHsCPF.Value,
      QrLCHsEmitente.Value, QrSPC_CfgServidor.Value,
      QrSPC_CfgSPC_Porta.Value, QrSPC_CfgCodigSocio.Value,
      QrSPC_CfgSenhaSocio.Value, QrSPC_CfgModalidade.Value,
      Solicitante, QrLCHsDCompra.Value, QrSPC_CfgBAC_CMC7.Value,
      Banda, RG, UF, Nascimento, QrLCHsBanco.Value, FormatFloat('0', QrLCHsAgencia.Value),
      QrLCHsConta.Value, DigitoCC, QrLCHsCheque.Value, DigitoCH, Qtde,
      QrSPC_CfgTipoCred.Value, QrLCHsValor.Value,
      QrSPC_CfgValAviso.Value, QrSPC_CfgVALCONSULTA.Value,
      Endereco, Telefone, CEP, ValMax, ValMin, nil, nil, AvisaValor);
    Dmod.QrUpdL.Params[00].AsInteger := StatusSPC;
    Dmod.QrUpdL.Params[01].AsString  := QrLCHsCPF.Value;
    Dmod.QrUpdL.ExecSQL;
    //
    if StatusSPC = 2 then Contagem := Contagem + 1;
  end;
  var
  CH, DU, i, Conta: Integer;
begin
  FmLotes1.StCPF_CH.Visible := True;
  CH := QrLCHsOrdem.Value;
  DU := QrLDUsOrdem.Value;
  //
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE importlote SET StatusSPC=:P0');
  Dmod.QrUpdL.SQL.Add('WHERE CPF=:P1');
  //
  Conta := 0;
  case SelType of
    istAtual: VerificaCPF_Atual(False, Conta, True);
    istExtra1, istExtra2:
    begin
      QrILd.Close;
      QrILd.Params[0].AsInteger := 0; // CH
      QrILd.Open;
      //
      if UnSPC.AceitaValorSPC(QrSPC_CfgCodigo.Value,
      QrSPC_CfgVALCONSULTA.Value, QrILd.RecordCount, QrSPC_CfgValAviso.Value) =
      ID_YES then
      begin
        FmLotes1.PB_SPC.Position := 0;
        FmLotes1.PB_SPC.Max := QrILd.RecordCount;
        FmLotes1.PanelPB_SPC.Visible := True;
        //
        while not QrILd.Eof do
        begin
          FmLotes1.PB_SPC.Position := FmLotes1.PB_SPC.Position + 1;
          FmLotes1.PB_SPC.Update;
          Application.ProcessMessages;
          //
          if QrLCHs.Locate('CPF', QrILdCPF.Value, [])  then
          begin
            if SelType = istExtra1 then
              VerificaCPF_Atual(False, Conta, False)
            else
              VerificaCPF_Atual(True, Conta, False);
          end;
          //
          QrILd.Next;
        end;
      end;
    end;
    istSelecionados:
    begin
      if FmLotes1.GradeC_L.SelectedRows.Count > 0 then
      begin
        //
        //
        if UnSPC.AceitaValorSPC(QrSPC_CfgCodigo.Value,
        QrSPC_CfgVALCONSULTA.Value, FmLotes1.GradeC_L.SelectedRows.Count,
        QrSPC_CfgValAviso.Value) =
        ID_YES then
        begin
          FmLotes1.PB_SPC.Position := 0;
          FmLotes1.PB_SPC.Max := FmLotes1.GradeC_L.SelectedRows.Count;
          FmLotes1.PanelPB_SPC.Visible := True;
          //
          with FmLotes1.GradeC_L.DataSource.DataSet do
          for i:= 0 to FmLotes1.GradeC_L.SelectedRows.Count-1 do
          begin
            FmLotes1.PB_SPC.Position := FmLotes1.PB_SPC.Position + 1;
            FmLotes1.PB_SPC.Update;
            Application.ProcessMessages;
            //
            GotoBookmark(pointer(FmLotes1.GradeC_L.SelectedRows.Items[i]));
            VerificaCPF_Atual(False, Conta, False);
          end;
        end;
      end else VerificaCPF_Atual(False, Conta, True);
    end;
  end;
  //
  ReopenImportLote(CH, DU);
  FmLotes1.StCPF_CH.Visible := False;
  FmLotes1.PanelPB_SPC.Visible := False;
  case Conta of
    0: ; // Nada
    1: Application.MessageBox('Foi localizado um documento com informa��es!',
    'Aviso', MB_OK+MB_ICONWARNING);
    else Application.MessageBox(PChar('Foram localizados ' + IntToStr(Conta) +
    ' documentos com informa��es!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TDmoc.VerificaCPFCNP_DU_Importacao(SelType: TSelType);
  procedure VerificaCPF_Atual(const MinMax: Boolean; var Contagem: Integer;
  AvisaValor: Boolean);
  var
    TipoDoc, Solicitante, RG, UF, Endereco, Telefone, CEP, Banda: String;
    Nascimento: TDateTime;
    DigitoCC, DigitoCH, Qtde: Integer;
    //
    StatusSPC: Integer;
    ValMax, ValMin: Double;
  begin
    if Length(QrLDUsCPF.Value) = 11 then
      TipoDoc := 'CPF'
    else
      TipoDoc := 'CNPJ';
    FmLotes1.StCPF_DU.Caption := 'Aguarde... consultando ' + TipoDoc +
      Geral.FormataCNPJ_TT(QrLDUsCPF.Value);
    FmLotes1.StCPF_DU.Update;
    //
    Solicitante := VAR_LOGIN + '#' + FormatFloat('0', VAR_USUARIO);
    RG          := '';
    UF          := '';
    Nascimento  := 0;
    //
    //MLAGeral.GeraCMC7_30(Comp, Banco, Agencia, Conta, Cheque, Tipif, Banda);
    Banda       := '';
    //
    DigitoCC    := 0;
    DigitoCH    := 0;
    Qtde        := 1;
    //
    Endereco    := '';
    Telefone    := '';
    CEP         := '';
    //
    if MinMax then
    begin
      ValMin := QrSPC_CfgSPC_ValMin.Value;
      ValMax := QrSPC_CfgSPC_ValMax.Value;
    end else begin
      ValMin := 0;
      ValMax := 0;
    end;
    StatusSPC :=
    UnSPC.ConsultaSPC_Rapida(QrSPC_CfgCodigo.Value, True, QrLDUsCPF.Value,
      QrLDUsEmitente.Value, QrSPC_CfgServidor.Value,
      QrSPC_CfgSPC_Porta.Value, QrSPC_CfgCodigSocio.Value,
      QrSPC_CfgSenhaSocio.Value, QrSPC_CfgModalidade.Value,
      Solicitante, QrLDUsDCompra.Value, QrSPC_CfgBAC_CMC7.Value,
      Banda, RG, UF, Nascimento, QrLDUsBanco.Value, FormatFloat('0', QrLDUsAgencia.Value),
      QrLDUsConta.Value, DigitoCC, QrLDUsCheque.Value, DigitoCH, Qtde,
      QrSPC_CfgTipoCred.Value, QrLDUsValor.Value,
      QrSPC_CfgValAviso.Value, QrSPC_CfgVALCONSULTA.Value,
      Endereco, Telefone, CEP, ValMax, ValMin, nil, nil, AvisaValor);
    Dmod.QrUpdL.Params[00].AsInteger := StatusSPC;
    Dmod.QrUpdL.Params[01].AsString  := QrLDUsCPF.Value;
    Dmod.QrUpdL.ExecSQL;
    //
    if StatusSPC = 2 then Contagem := Contagem + 1;
  end;
  var
  CH, DU, i, Conta: Integer;
begin
  FmLotes1.StCPF_DU.Visible := True;
  CH := QrLCHsOrdem.Value;
  DU := QrLDUsOrdem.Value;
  //
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE importlote SET StatusSPC=:P0');
  Dmod.QrUpdL.SQL.Add('WHERE CPF=:P1');
  //
  Conta := 0;
  case SelType of
    istAtual: VerificaCPF_Atual(False, Conta, True);
    istExtra1, istExtra2:
    begin
      QrILd.Close;
      QrILd.Params[0].AsInteger := 1; // DU
      QrILd.Open;
      //
      if UnSPC.AceitaValorSPC(QrSPC_CfgCodigo.Value,
      QrSPC_CfgVALCONSULTA.Value, QrILd.RecordCount, QrSPC_CfgValAviso.Value) =
      ID_YES then
      begin
        FmLotes1.PB_SPC.Position := 0;
        FmLotes1.PB_SPC.Max := QrILd.RecordCount;
        FmLotes1.PanelPB_SPC.Visible := True;
        //
        while not QrILd.Eof do
        begin
          FmLotes1.PB_SPC.Position := FmLotes1.PB_SPC.Position + 1;
          FmLotes1.PB_SPC.Update;
          Application.ProcessMessages;
          //
          if QrLDUs.Locate('CPF', QrILdCPF.Value, [])  then
          begin
            if SelType = istExtra1 then
              VerificaCPF_Atual(False, Conta, False)
            else
              VerificaCPF_Atual(True, Conta, False);
          end;
          //
          QrILd.Next;
        end;
      end;
    end;
    istSelecionados:
    begin
      if FmLotes1.GradeD_L.SelectedRows.Count > 0 then
      begin
        //
        if UnSPC.AceitaValorSPC(QrSPC_CfgCodigo.Value,
        QrSPC_CfgVALCONSULTA.Value, FmLotes1.GradeD_L.SelectedRows.Count,
        QrSPC_CfgValAviso.Value) =
        ID_YES then
        begin
          FmLotes1.PB_SPC.Position := 0;
          FmLotes1.PB_SPC.Max := FmLotes1.GradeD_L.SelectedRows.Count;
          FmLotes1.PanelPB_SPC.Visible := True;
          //
          with FmLotes1.GradeD_L.DataSource.DataSet do
          for i:= 0 to FmLotes1.GradeD_L.SelectedRows.Count-1 do
          begin
            FmLotes1.PB_SPC.Position := FmLotes1.PB_SPC.Position + 1;
            FmLotes1.PB_SPC.Update;
            Application.ProcessMessages;
            //
            GotoBookmark(pointer(FmLotes1.GradeD_L.SelectedRows.Items[i]));
            VerificaCPF_Atual(False, Conta, False);
          end;
        end;
      end else VerificaCPF_Atual(False, Conta, True);
    end;
  end;
  //
  ReopenImportLote(CH, DU);
  FmLotes1.StCPF_DU.Visible := False;
  FmLotes1.PanelPB_SPC.Visible := False;
  case Conta of
    0: ; // Nada
    1: Application.MessageBox('Foi localizado um documento com informa��es!',
    'Aviso', MB_OK+MB_ICONWARNING);
    else Application.MessageBox(PChar('Foram localizados ' + IntToStr(Conta) +
    ' documentos com informa��es!'), 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TDmoc.ReopenImportLote(CH, DU: Integer);
begin
  QrLCHs.Close;
  QrLCHs.SQL.Clear;
  QrLCHs.SQL.Add('SELECT  il.*');
  QrLCHs.SQL.Add('FROM importlote il');
  QrLCHs.SQL.Add('WHERE il.Tipo=0');
  QrLCHs.SQL.Add(MLAGeral.OrdemSQL1(FORDA_LCHs, FORDB_LCHs));
  QrLCHs.Open;
  QrLCHs.Locate('Ordem', CH, []);

  QrLDUs.Close;
  QrLDUs.SQL.Clear;
  QrLDUs.SQL.Add('SELECT  il.*');
  QrLDUs.SQL.Add('FROM importlote il');
  QrLDUs.SQL.Add('WHERE il.Tipo=1');
  QrLDUs.SQL.Add(MLAGeral.OrdemSQL1(FORDA_LDUs, FORDB_LDUs));
  QrLDUs.Open;
  QrLDUs.Locate('Ordem', DU, []);
  //
  QrSCHs.Close;
  QrSCHs.Open;
  //
  QrSDUs.Close;
  QrSDUs.Open;
  //
end;

end.

