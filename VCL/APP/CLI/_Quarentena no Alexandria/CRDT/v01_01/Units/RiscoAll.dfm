object FmRiscoAll: TFmRiscoAll
  Left = 340
  Top = 179
  Caption = 'Risco de Clientes / Sacados e Emitentes'
  ClientHeight = 496
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 400
    Align = alClient
    TabOrder = 0
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 160
      Align = alTop
      TabOrder = 0
      object Label75: TLabel
        Left = 4
        Top = 2
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label1: TLabel
        Left = 4
        Top = 40
        Width = 44
        Height = 13
        Caption = 'Coligado:'
      end
      object Label2: TLabel
        Left = 4
        Top = 80
        Width = 120
        Height = 13
        Caption = 'Nome sacado / emitente:'
      end
      object Label4: TLabel
        Left = 4
        Top = 120
        Width = 138
        Height = 13
        Caption = 'CPF/CNPJ sacado/emitente:'
      end
      object EdCliente: TdmkEditCB
        Left = 4
        Top = 18
        Width = 61
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 68
        Top = 18
        Width = 421
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECLIENTE'
        ListSource = DsClientes
        TabOrder = 1
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object GroupBox1: TGroupBox
        Left = 492
        Top = 1
        Width = 297
        Height = 158
        Align = alRight
        TabOrder = 2
        object Label19: TLabel
          Left = 4
          Top = 26
          Width = 45
          Height = 13
          Caption = 'Cheques:'
        end
        object Label23: TLabel
          Left = 4
          Top = 48
          Width = 53
          Height = 13
          Caption = 'Duplicatas:'
        end
        object Label24: TLabel
          Left = 60
          Top = 8
          Width = 30
          Height = 13
          Caption = 'Risco:'
        end
        object Label96: TLabel
          Left = 137
          Top = 8
          Width = 42
          Height = 13
          Caption = 'Vencido:'
        end
        object Label98: TLabel
          Left = 214
          Top = 8
          Width = 56
          Height = 13
          Caption = 'Sub-total:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label99: TLabel
          Left = 4
          Top = 74
          Width = 56
          Height = 13
          Caption = 'Sub-total:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label91: TLabel
          Left = 4
          Top = 116
          Width = 50
          Height = 13
          Caption = 'Ocorr'#234'nc.:'
        end
        object Label3: TLabel
          Left = 214
          Top = 96
          Width = 45
          Height = 13
          Caption = 'TOTAL:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 138
          Top = 96
          Width = 75
          Height = 13
          Caption = 'Limi.Cred.Clien.:'
        end
        object EdDR: TdmkEdit
          Left = 60
          Top = 44
          Width = 76
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdDT: TdmkEdit
          Left = 214
          Top = 44
          Width = 76
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdRT: TdmkEdit
          Left = 60
          Top = 70
          Width = 76
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdVT: TdmkEdit
          Left = 137
          Top = 70
          Width = 76
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdST: TdmkEdit
          Left = 214
          Top = 70
          Width = 76
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 8
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdCT: TdmkEdit
          Left = 214
          Top = 22
          Width = 76
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdDV: TdmkEdit
          Left = 137
          Top = 44
          Width = 76
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdCR: TdmkEdit
          Left = 60
          Top = 22
          Width = 76
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdCV: TdmkEdit
          Left = 137
          Top = 22
          Width = 76
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdOA: TdmkEdit
          Left = 60
          Top = 112
          Width = 76
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 9
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdTT: TdmkEdit
          Left = 214
          Top = 112
          Width = 76
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 10
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdLimiCredCli: TdmkEdit
          Left = 137
          Top = 112
          Width = 76
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 11
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
      end
      object EdColigado: TdmkEditCB
        Left = 4
        Top = 56
        Width = 61
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdColigadoChange
        DBLookupComboBox = CBColigado
        IgnoraDBLookupComboBox = False
      end
      object CBColigado: TdmkDBLookupComboBox
        Left = 68
        Top = 56
        Width = 421
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECOLIGADO'
        ListSource = DsColigados
        TabOrder = 4
        dmkEditCB = EdColigado
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEmitente: TdmkEdit
        Left = 4
        Top = 96
        Width = 477
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdEmitenteChange
      end
      object EdCPF: TdmkEdit
        Left = 4
        Top = 136
        Width = 141
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdCPFChange
        OnExit = EdCPFExit
      end
      object RGMascara: TRadioGroup
        Left = 180
        Top = 117
        Width = 301
        Height = 41
        Caption = ' M'#225'scara: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Prefixo e sufixo'
          'Prefixo'
          'Sufixo')
        TabOrder = 7
        OnClick = RGMascaraClick
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 161
      Width = 790
      Height = 238
      ActivePage = TabSheet6
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Risco cheques'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid14: TDBGrid
          Left = 0
          Top = 0
          Width = 782
          Height = 210
          Align = alClient
          DataSource = DsRiscoC
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Banco'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Agencia'
              Title.Caption = 'Ag'#234'ncia'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conta'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cheque'
              Width = 49
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DCompra'
              Title.Caption = 'D.Compra'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Alignment = taRightJustify
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DDeposito'
              Title.Caption = 'Vencimento'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Width = 257
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPF_TXT'
              Title.Caption = 'CPF / CNPJ'
              Width = 113
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Cheques devolvidos'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid10: TDBGrid
          Left = 0
          Top = 0
          Width = 782
          Height = 210
          Align = alClient
          DataSource = DsCHDevA
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'DATA1_TXT'
              Title.Caption = '1'#170' Devol.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Alinea1'
              Title.Caption = 'Ali 1'
              Width = 24
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Title.Caption = 'Nome emitente'
              Width = 184
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPF_TXT'
              Title.Caption = 'CPF / CNPJ emitente'
              Width = 113
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Banco'
              Title.Caption = 'Bco.'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Agencia'
              Title.Caption = 'Ag.'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conta'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cheque'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Taxas'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA3_TXT'
              Title.Caption = #218'lt.Pgto'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValPago'
              Title.Caption = 'Pago'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ATUAL'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DATA2_TXT'
              Title.Caption = '2'#170' Devol.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Alinea2'
              Title.Caption = 'Ali 2'
              Width = 24
              Visible = True
            end>
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Duplicatas'
        ImageIndex = 2
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid12: TDBGrid
          Left = 0
          Top = 0
          Width = 782
          Height = 210
          Align = alClient
          DataSource = DsDOpen
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Duplicata'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DCompra'
              Title.Caption = 'D.Compra'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Alignment = taRightJustify
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DDeposito'
              Title.Caption = 'Vencto.'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Width = 310
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO_DESATUALIZ'
              Title.Caption = 'Saldo'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO_ATUALIZADO'
              Title.Caption = 'Atualizado'
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESTATUS'
              Title.Caption = 'Status'
              Width = 90
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STATUS'
              Title.Caption = #218'ltima posi'#231#227'o no hist'#243'rico'
              Width = 196
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Banco'
              Title.Caption = 'Bco.'
              Width = 27
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Agencia'
              Title.Caption = 'Ag.'
              Width = 31
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEBANCO'
              Width = 240
              Visible = True
            end>
        end
      end
      object TabSheet4: TTabSheet
        Caption = 'Ocorr'#234'ncias'
        ImageIndex = 3
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 782
          Height = 210
          Align = alClient
          DataSource = DsOcorA
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'TIPODOC'
              Title.Caption = 'TD'
              Width = 20
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DOCUM_TXT'
              Title.Caption = 'Documento'
              Width = 160
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DCompra'
              Title.Caption = 'Compra'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEOCORRENCIA'
              Title.Caption = 'Ocorr'#234'ncia'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataO'
              Title.Caption = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TaxaV'
              Title.Caption = '$ Taxa'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pago'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data3'
              Title.Caption = 'Ult.Pg'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ATUALIZADO'
              Title.Caption = 'Atualiz.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPF'
              Title.Caption = 'CNPJ / CPF'
              Width = 113
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emissao'
              Title.Caption = 'Emitido'
              Width = 56
              Visible = True
            end>
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Impress'#227'o'
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GroupBox2: TGroupBox
          Left = 0
          Top = 0
          Width = 782
          Height = 73
          Align = alTop
          Caption = ' Impress'#227'o: '
          TabOrder = 0
          object Ck10: TCheckBox
            Left = 20
            Top = 20
            Width = 160
            Height = 17
            Caption = 'Cheques'
            Checked = True
            State = cbChecked
            TabOrder = 0
          end
          object Ck30: TCheckBox
            Left = 200
            Top = 20
            Width = 160
            Height = 17
            Caption = 'Duplicatas'
            Checked = True
            State = cbChecked
            TabOrder = 1
          end
          object Ck20: TCheckBox
            Left = 400
            Top = 20
            Width = 160
            Height = 17
            Caption = 'Cheques devolvidos'
            Checked = True
            State = cbChecked
            TabOrder = 2
          end
          object Ck50: TCheckBox
            Left = 20
            Top = 44
            Width = 160
            Height = 17
            Caption = 'Ocorr'#234'ncias em Clientes'
            Checked = True
            State = cbChecked
            TabOrder = 3
          end
          object Ck40: TCheckBox
            Left = 600
            Top = 20
            Width = 160
            Height = 17
            Caption = 'Duplicatas devolvidas'
            Checked = True
            State = cbChecked
            TabOrder = 4
          end
          object Ck60: TCheckBox
            Left = 200
            Top = 44
            Width = 160
            Height = 17
            Caption = 'Ocorr'#234'ncias de Cheques'
            Checked = True
            State = cbChecked
            TabOrder = 5
          end
          object Ck70: TCheckBox
            Left = 400
            Top = 44
            Width = 160
            Height = 17
            Caption = 'Ocorr'#234'ncias de duplicatas'
            Checked = True
            State = cbChecked
            TabOrder = 6
          end
        end
        object Panel2: TPanel
          Left = 0
          Top = 162
          Width = 782
          Height = 48
          Align = alBottom
          TabOrder = 1
          object BtImprime: TBitBtn
            Tag = 5
            Left = 196
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Imprime'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtImprimeClick
          end
          object BtCancela: TBitBtn
            Tag = 15
            Left = 100
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Cancela'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 1
            OnClick = BtCancelaClick
          end
          object BtGera: TBitBtn
            Tag = 163
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Caption = '&Gera'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 2
            OnClick = BtGeraClick
          end
        end
        object Panel3: TPanel
          Left = 0
          Top = 73
          Width = 782
          Height = 89
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 2
          object RGOrdem3: TRadioGroup
            Left = 264
            Top = 0
            Width = 132
            Height = 89
            Align = alLeft
            Caption = ' Ordem 3: '
            ItemIndex = 2
            Items.Strings = (
              'Cliente'
              'Tipo documento'
              'Vencimento'
              'Status')
            TabOrder = 2
          end
          object RGOrdem2: TRadioGroup
            Left = 132
            Top = 0
            Width = 132
            Height = 89
            Align = alLeft
            Caption = ' Ordem 2: '
            ItemIndex = 1
            Items.Strings = (
              'Cliente'
              'Tipo documento'
              'Vencimento'
              'Status')
            TabOrder = 1
          end
          object RGOrdem1: TRadioGroup
            Left = 0
            Top = 0
            Width = 132
            Height = 89
            Align = alLeft
            Caption = ' Ordem 1: '
            ItemIndex = 0
            Items.Strings = (
              'Cliente'
              'Tipo documento'
              'Vencimento'
              'Status')
            TabOrder = 0
          end
          object Panel4: TPanel
            Left = 396
            Top = 0
            Width = 386
            Height = 89
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 3
            object Panel5: TPanel
              Left = 0
              Top = 0
              Width = 96
              Height = 89
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object RGAgrupa: TRadioGroup
                Left = 0
                Top = 0
                Width = 96
                Height = 89
                Align = alClient
                Caption = ' Agrupamentos: '
                ItemIndex = 0
                Items.Strings = (
                  '0'
                  '1'
                  '2'
                  '3')
                TabOrder = 0
              end
            end
            object Panel6: TPanel
              Left = 96
              Top = 0
              Width = 290
              Height = 89
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 1
              object Progress1: TProgressBar
                Left = 0
                Top = 72
                Width = 290
                Height = 17
                Align = alBottom
                TabOrder = 0
                Visible = False
              end
            end
          end
        end
      end
      object TabSheet6: TTabSheet
        Caption = 'Sacados / Emitentes'
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid2: TDBGrid
          Left = 0
          Top = 37
          Width = 782
          Height = 173
          Align = alClient
          DataSource = DsSacEmi
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid2DblClick
          OnTitleClick = DBGrid2TitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Emitente'
              Title.Caption = 'Emitente / Sacado'
              Width = 448
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPF_TXT'
              Title.Caption = 'CPF / CNPJ'
              Width = 113
              Visible = True
            end>
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 782
          Height = 37
          Align = alTop
          BevelOuter = bvNone
          Caption = 
            'Esta tabela serve para mostrar todos sacados/emitentes que se en' +
            'caixam na pesquisa selecionada. '#13#10'Para ver o risco de um determi' +
            'nado sacado/emitente, d'#234' um duplo clique na linha desejada, e ap' +
            'enas o CPF/CNPJ ser'#225' pesquisado!'
          TabOrder = 1
          object Memo1: TMemo
            Left = 0
            Top = 0
            Width = 782
            Height = 37
            Align = alClient
            Color = clInactiveCaption
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            Lines.Strings = (
              
                'Esta tabela serve para mostrar todos sacados/emitentes que se en' +
                'caixam na pesquisa selecionada (Nome  e CPF/CNPJ acima).'
              
                'Para ver o risco de um determinado sacado/emitente, d'#234' um duplo ' +
                'clique na linha desejada, e apenas o CPF/CNPJ ser'#225' pesquisado!')
            ParentFont = False
            TabOrder = 0
          end
        end
      end
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtPesquisa: TBitBtn
      Tag = 22
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Pesquisa'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtPesquisaClick
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 682
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
    end
    object BtHistorico: TBitBtn
      Tag = 259
      Left = 352
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Hist'#243'rico'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtHistoricoClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Risco de Clientes / Sacados e Emitentes'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object QrOcorA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrOcorAAfterOpen
    BeforeClose = QrOcorABeforeClose
    OnCalcFields = QrOcorACalcFields
    SQL.Strings = (
      'SELECT IF(oc.Cliente > 0,'#39'CL'#39',IF(lo.Tipo=0,"CH","DU"))TIPODOC, '
      'ob.Nome NOMEOCORRENCIA, li.Banco, li.Agencia, li.Cheque, '
      
        'li.Duplicata, li.Conta, lo.Tipo, li.Emissao, li.DCompra, li.Venc' +
        'to, '
      'li.DDeposito, li.Emitente, li.CPF, lo.Cliente CLIENTELOTE, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.Status<2'
      'AND (lo.Cliente=:P0 OR oc.Cliente=:P1)')
    Left = 725
    Top = 361
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOcorATipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrOcorATIPODOC: TWideStringField
      FieldName = 'TIPODOC'
      Size = 2
    end
    object QrOcorANOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcorACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOcorALotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrOcorADataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorAOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrOcorAValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorALoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrOcorALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorATaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrOcorATaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOcorAPago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOcorADataP: TDateField
      FieldName = 'DataP'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorATaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrOcorAData3: TDateField
      FieldName = 'Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorAStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrOcorASALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,###,##0.00'
      Calculated = True
    end
    object QrOcorAATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,###,##0.00'
      Calculated = True
    end
    object QrOcorACliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOcorABanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrOcorAAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrOcorACheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrOcorADuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrOcorAConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrOcorAEmissao: TDateField
      FieldName = 'Emissao'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorADCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrOcorAVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrOcorADDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrOcorAEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrOcorACPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrOcorACLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
    object QrOcorACLIENTEDONO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CLIENTEDONO'
      Calculated = True
    end
    object QrOcorADOCUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOCUM_TXT'
      Size = 255
      Calculated = True
    end
    object QrOcorACPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrOcorADescri: TWideStringField
      FieldName = 'Descri'
      Size = 30
    end
  end
  object DsOcorA: TDataSource
    DataSet = QrOcorA
    Left = 753
    Top = 361
  end
  object QrCHDevA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCHDevAAfterOpen
    BeforeClose = QrCHDevABeforeClose
    OnCalcFields = QrCHDevACalcFields
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLIENTE, ai.*'
      'FROM alinits ai'
      'LEFT JOIN entidades en ON en.Codigo=ai.Cliente'
      'WHERE ai.Status<2'
      'AND ai.Cliente=:P0'
      'ORDER BY ai.Data1, ai.Data2')
    Left = 669
    Top = 361
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCHDevADATA1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA1_TXT'
      Calculated = True
    end
    object QrCHDevADATA2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA2_TXT'
      Calculated = True
    end
    object QrCHDevADATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Calculated = True
    end
    object QrCHDevACPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrCHDevANOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrCHDevACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCHDevAAlinea1: TIntegerField
      FieldName = 'Alinea1'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrCHDevAAlinea2: TIntegerField
      FieldName = 'Alinea2'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrCHDevAData1: TDateField
      FieldName = 'Data1'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCHDevAData2: TDateField
      FieldName = 'Data2'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCHDevAData3: TDateField
      FieldName = 'Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCHDevACliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrCHDevABanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrCHDevAAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrCHDevAConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrCHDevACheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrCHDevACPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrCHDevAValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevATaxas: TFloatField
      FieldName = 'Taxas'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCHDevADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCHDevADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCHDevAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCHDevAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCHDevAEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrCHDevAChequeOrigem: TIntegerField
      FieldName = 'ChequeOrigem'
      Required = True
    end
    object QrCHDevAStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrCHDevAValPago: TFloatField
      FieldName = 'ValPago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevAMulta: TFloatField
      FieldName = 'Multa'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevAJurosP: TFloatField
      FieldName = 'JurosP'
      Required = True
      DisplayFormat = '#,###,##0.000000'
    end
    object QrCHDevAJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevADesconto: TFloatField
      FieldName = 'Desconto'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevASALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCHDevAATUAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsCHDevA: TDataSource
    DataSet = QrCHDevA
    Left = 697
    Top = 361
  end
  object QrDOpen: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrDOpenAfterOpen
    BeforeClose = QrDOpenBeforeClose
    OnCalcFields = QrDOpenCalcFields
    SQL.Strings = (
      
        'SELECT ba.Nome NOMEBANCO, od.Nome STATUS, li.Controle, li.Duplic' +
        'ata,  li.Repassado,'
      'li.DCompra, li.Valor, li.DDeposito, li.Emitente, li.CPF, '
      'lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg,'
      'li.Vencto, li.Data3, li.Banco, li.Agencia, li.Emissao'
      'FROM lotesits li'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'LEFT JOIN bancos    ba ON li.Banco=ba.Codigo'
      'WHERE lo.Tipo=1 '
      'AND li.Quitado <> 2'
      'AND lo.Cliente=:P0'
      'ORDER BY li.Vencto')
    Left = 613
    Top = 361
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDOpenControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDOpenDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrDOpenDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDOpenValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrDOpenDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDOpenEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrDOpenCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrDOpenCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDOpenSTATUS: TWideStringField
      FieldName = 'STATUS'
      Size = 50
    end
    object QrDOpenQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrDOpenTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrDOpenTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrDOpenTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrDOpenSALDO_DESATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_DESATUALIZ'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrDOpenSALDO_ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrDOpenNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 30
      Calculated = True
    end
    object QrDOpenVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrDOpenDDCALCJURO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DDCALCJURO'
      Calculated = True
    end
    object QrDOpenData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrDOpenBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000;-000; '
    end
    object QrDOpenAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000;-0000; '
    end
    object QrDOpenEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrDOpenRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrDOpenNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrDOpenCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsDOpen: TDataSource
    DataSet = QrDOpen
    Left = 641
    Top = 361
  end
  object QrRiscoC: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrRiscoCCalcFields
    SQL.Strings = (
      
        'SELECT lo.Cliente, lo.Tipo, li.Banco, li.Agencia, li.Conta, li.C' +
        'heque, li.Valor, '
      
        'li.DCompra, li.DDeposito, li.Emitente, li.CPF, li.Emissao, li.Ve' +
        'ncto, li.Controle'
      'FROM lotesits li '
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo')
    Left = 557
    Top = 361
    object QrRiscoCBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrRiscoCAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrRiscoCConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrRiscoCCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrRiscoCValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrRiscoCDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRiscoCDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRiscoCEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrRiscoCCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrRiscoCCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrRiscoCTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrRiscoCEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrRiscoCVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrRiscoCCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrRiscoCControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object DsRiscoC: TDataSource
    DataSet = QrRiscoC
    Left = 585
    Top = 361
  end
  object DsRiscoTC: TDataSource
    DataSet = QrRiscoTC
    Left = 585
    Top = 389
  end
  object QrRiscoTC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(li.Valor) Valor'
      'FROM lotesits li '
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Cliente=:P0'
      'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE()))'
      'AND lo.Tipo = 0')
    Left = 557
    Top = 389
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRiscoTCValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, '
      'AdValorem, DMaisC, DMaisD, FatorCompra, LimiCred'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMECLIENTE'
      '')
    Left = 384
    Top = 60
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesAdValorem: TFloatField
      FieldName = 'AdValorem'
    end
    object QrClientesDMaisC: TIntegerField
      FieldName = 'DMaisC'
    end
    object QrClientesFatorCompra: TFloatField
      FieldName = 'FatorCompra'
    end
    object QrClientesMAIOR_T: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MAIOR_T'
      Calculated = True
    end
    object QrClientesADVAL_T: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ADVAL_T'
      Calculated = True
    end
    object QrClientesDMaisD: TIntegerField
      FieldName = 'DMaisD'
    end
    object QrClientesLimiCred: TFloatField
      FieldName = 'LimiCred'
      EditFormat = '#,###,##0.00'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 412
    Top = 60
  end
  object QrColigados: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECOLIGADO'
      'FROM entidades'
      'WHERE Fornece2="V"'
      'ORDER BY NOMECOLIGADO'
      '')
    Left = 384
    Top = 96
    object QrColigadosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrColigadosNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
  end
  object DsColigados: TDataSource
    DataSet = QrColigados
    Left = 412
    Top = 96
  end
  object QrRisco1: TmySQLQuery
    Database = Dmod.MyLocDatabase
    OnCalcFields = QrRisco1CalcFields
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial '
      'ELSE en.Nome END NOMECLIENTE, ri.* '
      'FROM risco ri'
      'LEFT JOIN creditor.entidades en ON ri.Cliente=en.Codigo')
    Left = 512
    Top = 260
    object QrRisco1Tipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrRisco1Banco: TIntegerField
      FieldName = 'Banco'
    end
    object QrRisco1Agencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrRisco1Conta: TWideStringField
      FieldName = 'Conta'
      Size = 30
    end
    object QrRisco1Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 50
    end
    object QrRisco1Cheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrRisco1DEmissao: TDateField
      FieldName = 'DEmissao'
    end
    object QrRisco1DCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrRisco1DVence: TDateField
      FieldName = 'DVence'
    end
    object QrRisco1DDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrRisco1DDevol1: TDateField
      FieldName = 'DDevol1'
    end
    object QrRisco1DDevol2: TDateField
      FieldName = 'DDevol2'
    end
    object QrRisco1UltPagto: TDateField
      FieldName = 'UltPagto'
    end
    object QrRisco1Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrRisco1Taxas: TFloatField
      FieldName = 'Taxas'
    end
    object QrRisco1Saldo: TFloatField
      FieldName = 'Saldo'
    end
    object QrRisco1Juros: TFloatField
      FieldName = 'Juros'
    end
    object QrRisco1Desco: TFloatField
      FieldName = 'Desco'
    end
    object QrRisco1Pago: TFloatField
      FieldName = 'Pago'
    end
    object QrRisco1Atual: TFloatField
      FieldName = 'Atual'
    end
    object QrRisco1Emitente: TWideStringField
      FieldName = 'Emitente'
      Size = 100
    end
    object QrRisco1CPF: TWideStringField
      FieldName = 'CPF'
      Size = 30
    end
    object QrRisco1Alinea1: TIntegerField
      FieldName = 'Alinea1'
    end
    object QrRisco1Alinea2: TIntegerField
      FieldName = 'Alinea2'
    end
    object QrRisco1Status: TWideStringField
      FieldName = 'Status'
      Size = 100
    end
    object QrRisco1Historico: TWideStringField
      FieldName = 'Historico'
      Size = 100
    end
    object QrRisco1CONTA_DUPLICATA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CONTA_DUPLICATA'
      Size = 50
      Calculated = True
    end
    object QrRisco1DEMISSAO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DEMISSAO_TXT'
      Size = 8
      Calculated = True
    end
    object QrRisco1DCOMPRA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DCOMPRA_TXT'
      Size = 8
      Calculated = True
    end
    object QrRisco1DVENCE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DVENCE_TXT'
      Size = 8
      Calculated = True
    end
    object QrRisco1DDEPOSITO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DDEPOSITO_TXT'
      Size = 8
      Calculated = True
    end
    object QrRisco1DDEVOL1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DDEVOL1_TXT'
      Calculated = True
    end
    object QrRisco1DDEVOL2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DDEVOL2_TXT'
      Calculated = True
    end
    object QrRisco1ULTPAGTO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ULTPAGTO_TXT'
      Calculated = True
    end
    object QrRisco1TIPODOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPODOC'
      Size = 10
      Calculated = True
    end
    object QrRisco1CONTAGEM: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CONTAGEM'
      Calculated = True
    end
    object QrRisco1NOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrRisco1Cliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrRisco1Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrRisco1STATUS_HISTORICO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'STATUS_HISTORICO'
      Size = 255
      Calculated = True
    end
    object QrRisco1Devolvido: TFloatField
      FieldName = 'Devolvido'
      Required = True
    end
  end
  object QrSintetico: TmySQLQuery
    Database = Dmod.MyLocDatabase
    OnCalcFields = QrSinteticoCalcFields
    SQL.Strings = (
      'SELECT en.LimiCred, ri.Cliente, ri.Tipo, SUM(ri.Valor) Valor, '
      'SUM(ri.Taxas) Taxas, SUM(ri.Saldo) Saldo, SUM(ri.Juros) Juros, '
      'SUM(ri.Desco) Desco, SUM(ri.Pago) Pago, SUM(ri.Atual) Atual, '
      'SUM(ri.Devolvido) Devolvido, COUNT(ri.Controle) ITENS,'
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMECLI'
      'FROM risco ri'
      'LEFT JOIN creditor.entidades en ON en.Codigo=ri.Cliente'
      'GROUP BY NOMECLI')
    Left = 512
    Top = 316
    object QrSinteticoCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrSinteticoTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrSinteticoValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSinteticoTaxas: TFloatField
      FieldName = 'Taxas'
    end
    object QrSinteticoSaldo: TFloatField
      FieldName = 'Saldo'
    end
    object QrSinteticoJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSinteticoDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrSinteticoPago: TFloatField
      FieldName = 'Pago'
    end
    object QrSinteticoAtual: TFloatField
      FieldName = 'Atual'
    end
    object QrSinteticoITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrSinteticoNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrSinteticoDevolvido: TFloatField
      FieldName = 'Devolvido'
    end
    object QrSinteticoLimiCred: TFloatField
      FieldName = 'LimiCred'
    end
    object QrSinteticoULO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ULO'
      Calculated = True
    end
  end
  object QrAnalitico: TmySQLQuery
    Database = Dmod.MyLocDatabase
    OnCalcFields = QrAnaliticoCalcFields
    SQL.Strings = (
      'SELECT ri.Cliente, ri.Tipo, SUM(ri.Valor) Valor, '
      'SUM(ri.Taxas) Taxas, SUM(ri.Saldo) Saldo, SUM(ri.Juros) Juros, '
      'SUM(ri.Desco) Desco, SUM(ri.Pago) Pago, SUM(ri.Atual) Atual, '
      'SUM(ri.Devolvido) Devolvido, COUNT(ri.Controle) ITENS,'
      'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMECLI'
      'FROM risco ri'
      'LEFT JOIN creditor.entidades en ON en.Codigo=ri.Cliente'
      'GROUP BY NOMECLI, ri.Tipo')
    Left = 512
    Top = 288
    object QrAnaliticoCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrAnaliticoTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrAnaliticoValor: TFloatField
      FieldName = 'Valor'
    end
    object QrAnaliticoTaxas: TFloatField
      FieldName = 'Taxas'
    end
    object QrAnaliticoSaldo: TFloatField
      FieldName = 'Saldo'
    end
    object QrAnaliticoJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrAnaliticoDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrAnaliticoPago: TFloatField
      FieldName = 'Pago'
    end
    object QrAnaliticoAtual: TFloatField
      FieldName = 'Atual'
    end
    object QrAnaliticoITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrAnaliticoNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrAnaliticoDESCRITIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DESCRITIPO'
      Size = 100
      Calculated = True
    end
    object QrAnaliticoDevolvido: TFloatField
      FieldName = 'Devolvido'
    end
  end
  object PMImprime: TPopupMenu
    Left = 309
    Top = 403
    object Descritivo1: TMenuItem
      Caption = '&Descritivo'
      OnClick = Descritivo1Click
    end
    object Analtico1: TMenuItem
      Caption = '&Anal'#237'tico'
      OnClick = Analtico1Click
    end
    object Sinttico1: TMenuItem
      Caption = '&Sint'#233'tico'
      OnClick = Sinttico1Click
    end
  end
  object QrSacEmi: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSacEmiCalcFields
    SQL.Strings = (
      'SELECT li.Emitente, li.CPF'
      'FROM lotesits li')
    Left = 557
    Top = 193
    object QrSacEmiEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrSacEmiCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrSacEmiCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsSacEmi: TDataSource
    DataSet = QrSacEmi
    Left = 585
    Top = 193
  end
  object PMHistorico: TPopupMenu
    OnPopup = PMHistoricoPopup
    Left = 397
    Top = 409
    object Clientepesquisado1: TMenuItem
      Caption = '&Cliente pesquisado'
      OnClick = Clientepesquisado1Click
    end
    object Emitentesacadopesquisado1: TMenuItem
      Caption = 'Emitente/sacado pesquisado'
      OnClick = Emitentesacadopesquisado1Click
    end
    object Ambospesquisados1: TMenuItem
      Caption = '&Ambos pesquisados'
      OnClick = Ambospesquisados1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Emitentesacadoselecionado1: TMenuItem
      Caption = '&Emitente/sacado selecionado'
      OnClick = Emitentesacadoselecionado1Click
    end
    object EmitentesacadoselecionadoClientepesquisado1: TMenuItem
      Caption = '&Emitente/sacado selecionado + Cliente pesquisado'
      OnClick = EmitentesacadoselecionadoClientepesquisado1Click
    end
  end
  object frxRisco1: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.513937164400000000
    ReportOptions.LastChange = 39718.513937164400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  GH1.Visible := <VFR_ORD1>;'
      '  GF1.Visible   := <VFR_ORD1>;'
      
        '  if <VFR_IDX1>= 0 then GH1.Condition := '#39'frxDsRisco1."NOMECLIEN' +
        'TE"'#39';'
      
        '  if <VFR_IDX1>= 1 then GH1.Condition := '#39'frxDsRisco1."TIPODOC"'#39 +
        ';'
      
        '  if <VFR_IDX1>= 2 then GH1.Condition := '#39'frxDsRisco1."DVENCE_TX' +
        'T"'#39';'
      '  if <VFR_IDX1>= 3 then GH1.Condition := '#39'frxDsRisco1."Status"'#39';'
      ''
      ''
      '  GH2.Visible   := <VFR_ORD2>;'
      '  GF2.Visible   := <VFR_ORD2>;'
      
        '  if <VFR_IDX2>= 0 then GH2.Condition := '#39'frxDsRisco1."NOMECLIEN' +
        'TE"'#39';'
      
        '  if <VFR_IDX2>= 1 then GH2.Condition := '#39'frxDsRisco1."TIPODOC"'#39 +
        ';'
      
        '  if <VFR_IDX2>= 2 then GH2.Condition := '#39'frxDsRisco1."DVENCE_TX' +
        'T"'#39';'
      '  if <VFR_IDX2>= 3 then GH2.Condition := '#39'frxDsRisco1."Status"'#39';'
      ''
      ''
      '  GH3.Visible   := <VFR_ORD3>;'
      '  GF3.Visible   := <VFR_ORD3>;'
      
        '  if <VFR_IDX3>= 0 then GH3.Condition := '#39'frxDsRisco1."NOMECLIEN' +
        'TE"'#39';'
      
        '  if <VFR_IDX3>= 1 then GH3.Condition := '#39'frxDsRisco1."TIPODOC"'#39 +
        ';'
      
        '  if <VFR_IDX3>= 2 then GH3.Condition := '#39'frxDsRisco1."DVENCE_TX' +
        'T"'#39';'
      '  if <VFR_IDX3>= 3 then GH3.Condition := '#39'frxDsRisco1."Status"'#39';'
      'end.')
    OnGetValue = frxRisco1GetValue
    Left = 484
    Top = 260
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsRisco1
        DataSetName = 'frxDsRisco1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object ReportTitle1: TfrxReportTitle
        Height = 96.661410000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Memo49: TfrxMemoView
          Left = 6.000000000000000000
          Top = 3.661410000000000000
          Width = 1000.000000000000000000
          Height = 26.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 834.000000000000000000
          Top = 4.661410000000000000
          Width = 168.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Left = 6.000000000000000000
          Top = 32.661410000000000000
          Width = 1000.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RISCO EM [VARF_SITUACOES]')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Left = 6.000000000000000000
          Top = 56.661410000000000000
          Width = 1000.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_TODOS]')
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Left = 6.000000000000000000
          Top = 78.661410000000000000
          Width = 1000.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_FILTROS]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 657.638220000000000000
        Width = 1046.929810000000000000
        object Line2: TfrxLineView
          Left = 2.000000000000000000
          Top = 11.968150000000000000
          Width = 1004.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Memo31: TfrxMemoView
          Left = 875.039580000000000000
          Top = 13.826219999999900000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 963.039580000000000000
          Top = 13.826219999999900000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        Height = 12.000000000000000000
        Top = 381.732530000000000000
        Width = 1046.929810000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsRisco1
        DataSetName = 'frxDsRisco1'
        RowCount = 0
        object Memo6: TfrxMemoView
          Left = 35.779530000000000000
          Width = 20.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000;-000; '#39', <frxDsRisco1."Banco">)]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 55.779530000000000000
          Width = 24.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'0000;-0000; '#39', <frxDsRisco1."Agencia">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 79.779530000000000000
          Width = 44.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRisco1."CONTA_DUPLICATA"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 123.779530000000000000
          Width = 28.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsRisco1."Cheque">)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 179.779530000000000000
          Width = 108.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsRisco1."Emitente"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 287.779530000000000000
          Width = 72.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRisco1."CPF"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 487.779530000000000000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRisco1."Valor"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 423.779530000000000000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRisco1."DVENCE_TXT"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 455.779530000000000000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRisco1."DDEPOSITO_TXT"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 619.779530000000000000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRisco1."Taxas"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 747.779530000000000000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRisco1."Pago"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 359.779530000000000000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRisco1."DEMISSAO_TXT"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 391.779530000000000000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRisco1."DCOMPRA_TXT"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 527.779530000000000000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRisco1."DDEVOL1_TXT"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 573.779530000000000000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRisco1."DDEVOL2_TXT"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 559.779530000000000000
          Width = 14.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'00;-00; '#39', <frxDsRisco1."Alinea1">)]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 605.779530000000000000
          Width = 14.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'00;-00; '#39', <frxDsRisco1."Alinea2">)]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 715.779530000000000000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRisco1."ULTPAGTO_TXT"]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 787.779530000000000000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRisco1."Saldo"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 827.779530000000000000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRisco1."Atual"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 867.779530000000000000
          Width = 112.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsRisco1."STATUS_HISTORICO"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 651.779530000000000000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRisco1."Desco"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 683.779530000000000000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRisco1."Juros"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 3.779530000000000000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRisco1."TIPODOC"]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 979.779530000000000000
          Width = 28.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsRisco1."Controle">)]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 151.779530000000000000
          Width = 28.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsRisco1."Cliente">)]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 55.779530000000000000
        Top = 139.842610000000000000
        Width = 1046.929810000000000000
        object Memo1: TfrxMemoView
          Left = 3.779530000000000000
          Top = 3.779529999999990000
          Width = 1004.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'RISCO DESCRITIVO')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 35.779530000000000000
          Top = 27.779530000000000000
          Width = 20.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bco')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 55.779530000000000000
          Top = 27.779530000000000000
          Width = 24.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 79.779530000000000000
          Top = 27.779530000000000000
          Width = 44.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 123.779530000000000000
          Top = 27.779530000000000000
          Width = 28.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cheq.')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 179.779530000000000000
          Top = 27.779530000000000000
          Width = 108.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sacado duplicata'
            'Emitente cheque')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 287.779530000000000000
          Top = 27.779530000000000000
          Width = 72.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 487.779530000000000000
          Top = 27.779530000000000000
          Width = 40.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 423.779530000000000000
          Top = 27.779530000000000000
          Width = 32.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data'
            'Vencto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 455.779530000000000000
          Top = 27.779530000000000000
          Width = 32.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data'
            'Dep'#243's.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 619.779530000000000000
          Top = 27.779530000000000000
          Width = 32.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Taxas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 747.779530000000000000
          Top = 27.779530000000000000
          Width = 40.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Pago')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 79.779530000000000000
          Top = 41.779530000000000000
          Width = 72.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Duplicata')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 359.779530000000000000
          Top = 27.779530000000000000
          Width = 32.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data'
            'Emiss.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 391.779530000000000000
          Top = 27.779530000000000000
          Width = 32.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data'
            'Comp.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 527.779530000000000000
          Top = 27.779530000000000000
          Width = 32.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '1'#170' Devolu'#231#227'o')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 559.779530000000000000
          Top = 27.779530000000000000
          Width = 14.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'A1')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 573.779530000000000000
          Top = 27.779530000000000000
          Width = 32.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '2'#170' Devol')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 605.779530000000000000
          Top = 27.779530000000000000
          Width = 14.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'A2')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 715.779530000000000000
          Top = 27.779530000000000000
          Width = 32.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data'
            #218'lt.Pg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 787.779530000000000000
          Top = 27.779530000000000000
          Width = 40.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Saldo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 827.779530000000000000
          Top = 27.779530000000000000
          Width = 40.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Atual')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 867.779530000000000000
          Top = 27.779530000000000000
          Width = 112.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Status + Hist'#243'rico')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 651.779530000000000000
          Top = 27.779530000000000000
          Width = 32.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Desco')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 683.779530000000000000
          Top = 27.779530000000000000
          Width = 32.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Juros')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 3.779530000000000000
          Top = 27.779530000000000000
          Width = 32.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TIPO'
            'DOC.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 979.779530000000000000
          Top = 27.779530000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Ctrl')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 151.779530000000000000
          Top = 27.779530000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 527.779530000000000000
          Top = 41.779530000000000000
          Width = 32.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Ocorr'#234'ncia')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 257.008040000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsRisco1."TIPODOC"'
        object Memo97: TfrxMemoView
          Left = 2.000000000000000000
          Width = 1004.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 298.582870000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsRisco1."CONTA_DUPLICATA"'
        object Memo98: TfrxMemoView
          Left = 34.000000000000000000
          Width = 972.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GH3: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 340.157700000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GH3OnBeforePrint'
        Condition = 'frxDsRisco1."DEmissao"'
        object Memo99: TfrxMemoView
          Left = 58.000000000000000000
          Width = 948.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA3NOME]')
          ParentFont = False
        end
      end
      object GF3: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 415.748300000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GF3OnBeforePrint'
        object Memo100: TfrxMemoView
          Left = 3.779530000000000000
          Top = 3.779530000000020000
          Width = 484.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsRisco1."CONTAGEM">)] documento(s):')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 487.779530000000000000
          Top = 3.779530000000020000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Valor">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 619.779530000000000000
          Top = 3.779530000000020000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Taxas">)]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 747.779530000000000000
          Top = 3.779530000000020000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Pago">)]')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 787.779530000000000000
          Top = 3.779530000000020000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Saldo">)]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 827.779530000000000000
          Top = 3.779530000000020000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Atual">)]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 651.779530000000000000
          Top = 3.779530000000020000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Desco">)]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 683.779530000000000000
          Top = 3.779530000000020000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Juros">)]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 464.882190000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo67: TfrxMemoView
          Left = 3.779530000000000000
          Top = 3.779530000000080000
          Width = 484.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsRisco1."CONTAGEM">)] documento(s):')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 487.779530000000000000
          Top = 3.779530000000080000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Valor">)]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 619.779530000000000000
          Top = 3.779530000000080000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Taxas">)]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 747.779530000000000000
          Top = 3.779530000000080000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Pago">)]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 787.779530000000000000
          Top = 3.779530000000080000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Saldo">)]')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 827.779530000000000000
          Top = 3.779530000000080000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Atual">)]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 651.779530000000000000
          Top = 3.779530000000080000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Desco">)]')
          ParentFont = False
        end
        object Memo74: TfrxMemoView
          Left = 683.779530000000000000
          Top = 3.779530000000080000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Juros">)]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 514.016080000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo75: TfrxMemoView
          Left = 3.779530000000000000
          Top = 3.779530000000020000
          Width = 484.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsRisco1."CONTAGEM">)] documento(s):')
          ParentFont = False
        end
        object Memo76: TfrxMemoView
          Left = 487.779530000000000000
          Top = 3.779530000000020000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Valor">)]')
          ParentFont = False
        end
        object Memo77: TfrxMemoView
          Left = 619.779530000000000000
          Top = 3.779530000000020000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Taxas">)]')
          ParentFont = False
        end
        object Memo78: TfrxMemoView
          Left = 747.779530000000000000
          Top = 3.779530000000020000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Pago">)]')
          ParentFont = False
        end
        object Memo79: TfrxMemoView
          Left = 787.779530000000000000
          Top = 3.779530000000020000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Saldo">)]')
          ParentFont = False
        end
        object Memo80: TfrxMemoView
          Left = 827.779530000000000000
          Top = 3.779530000000020000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Atual">)]')
          ParentFont = False
        end
        object Memo81: TfrxMemoView
          Left = 651.779530000000000000
          Top = 3.779530000000020000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Desco">)]')
          ParentFont = False
        end
        object Memo82: TfrxMemoView
          Left = 683.779530000000000000
          Top = 3.779530000000020000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Juros">)]')
          ParentFont = False
        end
      end
      object Band11: TfrxReportSummary
        Height = 32.000000000000000000
        Top = 600.945270000000000000
        Width = 1046.929810000000000000
        object Memo83: TfrxMemoView
          Left = 3.779530000000000000
          Top = 3.779529999999910000
          Width = 484.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsRisco1."CONTAGEM">)] documento(s):')
          ParentFont = False
        end
        object Memo84: TfrxMemoView
          Left = 487.779530000000000000
          Top = 3.779529999999910000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Valor">)]')
          ParentFont = False
        end
        object Memo85: TfrxMemoView
          Left = 619.779530000000000000
          Top = 3.779529999999910000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Taxas">)]')
          ParentFont = False
        end
        object Memo86: TfrxMemoView
          Left = 747.779530000000000000
          Top = 3.779529999999910000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Pago">)]')
          ParentFont = False
        end
        object Memo87: TfrxMemoView
          Left = 787.779530000000000000
          Top = 3.779529999999910000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Saldo">)]')
          ParentFont = False
        end
        object Memo88: TfrxMemoView
          Left = 827.779530000000000000
          Top = 3.779529999999910000
          Width = 40.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Atual">)]')
          ParentFont = False
        end
        object Memo89: TfrxMemoView
          Left = 651.779530000000000000
          Top = 3.779529999999910000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Desco">)]')
          ParentFont = False
        end
        object Memo90: TfrxMemoView
          Left = 683.779530000000000000
          Top = 3.779529999999910000
          Width = 32.000000000000000000
          Height = 12.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRisco1."Juros">)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsRisco1: TfrxDBDataset
    UserName = 'frxDsRisco1'
    CloseDataSource = False
    DataSet = QrRisco1
    BCDToCurrency = False
    Left = 456
    Top = 260
  end
  object frxAnalitico: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.535721388900000000
    ReportOptions.LastChange = 39718.535721388900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxRisco1GetValue
    Left = 484
    Top = 288
    Datasets = <
      item
        DataSet = frxDsAnalitico
        DataSetName = 'frxDsAnalitico'
      end
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 100.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo49: TfrxMemoView
          Left = 2.000000000000000000
          Top = 3.661410000000000000
          Width = 716.000000000000000000
          Height = 26.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 546.000000000000000000
          Top = 8.661410000000000000
          Width = 168.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Left = 2.000000000000000000
          Top = 32.661410000000000000
          Width = 716.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'RISCO EM [VARF_SITUACOES]')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Left = 2.000000000000000000
          Top = 56.661410000000000000
          Width = 716.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_TODOS]')
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Left = 2.000000000000000000
          Top = 78.661410000000000000
          Width = 716.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_FILTROS]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 60.000000000000000000
        Top = 495.118430000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 584.015770000000000000
          Top = 8.723869999999980000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 672.015770000000000000
          Top = 8.723869999999980000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        Height = 17.000000000000000000
        Top = 313.700990000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsAnalitico
        DataSetName = 'frxDsAnalitico'
        RowCount = 0
        object Memo12: TfrxMemoView
          Left = 314.000000000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalitico."Valor"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 378.000000000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalitico."Taxas"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 526.000000000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalitico."Pago"]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 590.000000000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalitico."Saldo"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 654.000000000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalitico."Atual"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 426.000000000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalitico."Desco"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 474.000000000000000000
          Width = 52.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsAnalitico."Juros"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 2.000000000000000000
          Width = 280.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsAnalitico."DESCRITIPO"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 282.000000000000000000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsAnalitico."ITENS">)]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 62.377860000000000000
        Top = 143.622140000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 2.000000000000000000
          Top = 4.377860000000000000
          Width = 716.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'RISCO ANAL'#205'TICO')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 314.000000000000000000
          Top = 28.377860000000000000
          Width = 64.000000000000000000
          Height = 34.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Valor')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 378.000000000000000000
          Top = 28.377860000000000000
          Width = 48.000000000000000000
          Height = 34.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Taxas')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 526.000000000000000000
          Top = 28.377860000000000000
          Width = 64.000000000000000000
          Height = 34.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Pago')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 590.000000000000000000
          Top = 28.377860000000000000
          Width = 64.000000000000000000
          Height = 34.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Saldo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 654.000000000000000000
          Top = 28.377860000000000000
          Width = 64.000000000000000000
          Height = 34.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Atual')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 426.000000000000000000
          Top = 28.377860000000000000
          Width = 48.000000000000000000
          Height = 34.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Desco')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 474.000000000000000000
          Top = 28.377860000000000000
          Width = 51.779530000000000000
          Height = 34.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Juros')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 2.000000000000000000
          Top = 28.377860000000000000
          Width = 280.220470000000000000
          Height = 34.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente / Tipo de documento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 282.000000000000000000
          Top = 28.377860000000000000
          Width = 32.000000000000000000
          Height = 34.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde'
            'Docs')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        Height = 22.000000000000000000
        Top = 268.346630000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsAnalitico."NOMECLI"'
        object Memo97: TfrxMemoView
          Left = 2.000000000000000000
          Top = 2.330550000000020000
          Width = 716.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsAnalitico."NOMECLI"]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        Height = 20.779530000000000000
        Top = 355.275820000000000000
        Width = 718.110700000000000000
        object Memo104: TfrxMemoView
          Left = 2.000000000000000000
          Width = 280.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [frxDsAnalitico."NOMECLI"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 314.000000000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalitico."Valor">)]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 378.000000000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalitico."Taxas">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 526.000000000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalitico."Pago">)]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 590.000000000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalitico."Saldo">)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 654.000000000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalitico."Atual">)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 426.000000000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalitico."Desco">)]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 474.000000000000000000
          Width = 52.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalitico."Juros">)]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 282.000000000000000000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsAnalitico."ITENS">)]')
          ParentFont = False
        end
      end
      object Band11: TfrxReportSummary
        Height = 32.000000000000000000
        Top = 438.425480000000000000
        Width = 718.110700000000000000
        object Memo106: TfrxMemoView
          Left = 2.000000000000000000
          Top = 6.928879999999990000
          Width = 280.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL GERAL')
          ParentFont = False
        end
        object Line2: TfrxLineView
          Left = 2.000000000000000000
          Top = 2.708410000000010000
          Width = 716.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
      end
    end
  end
  object frxDsAnalitico: TfrxDBDataset
    UserName = 'frxDsAnalitico'
    CloseDataSource = False
    DataSet = QrAnalitico
    BCDToCurrency = False
    Left = 456
    Top = 288
  end
  object frxSintetico: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.546518912000000000
    ReportOptions.LastChange = 39718.546518912000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxRisco1GetValue
    Left = 484
    Top = 316
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsSintetico
        DataSetName = 'frxDsSintetico'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 100.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo49: TfrxMemoView
          Left = 2.000000000000000000
          Top = 3.661410000000000000
          Width = 716.000000000000000000
          Height = 26.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 546.000000000000000000
          Top = 8.661410000000000000
          Width = 168.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Left = 2.000000000000000000
          Top = 32.661410000000000000
          Width = 716.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'RISCO EM [VARF_SITUACOES]')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Left = 2.000000000000000000
          Top = 56.661410000000000000
          Width = 716.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_TODOS]')
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Left = 2.000000000000000000
          Top = 78.661410000000000000
          Width = 716.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_FILTROS]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 60.000000000000000000
        Top = 393.071120000000000000
        Width = 718.110700000000000000
        object Line2: TfrxLineView
          Left = 2.000000000000000000
          Top = 2.708410000000010000
          Width = 720.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
        end
        object Memo31: TfrxMemoView
          Left = 587.795300000000000000
          Top = 8.723869999999980000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 675.795300000000000000
          Top = 8.723869999999980000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 43.795300000000000000
          Top = 8.723869999999980000
          Width = 192.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '0.00;-0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '%ULO Utiliza'#231#227'o do limite operacional em %.')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        Height = 14.000000000000000000
        Top = 260.787570000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsSintetico
        DataSetName = 'frxDsSintetico'
        RowCount = 0
        object Memo12: TfrxMemoView
          Left = 518.000000000000000000
          Width = 56.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSintetico."Valor"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 394.000000000000000000
          Width = 64.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSintetico."Pago"]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 574.000000000000000000
          Width = 56.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSintetico."Saldo"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 630.000000000000000000
          Width = 56.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSintetico."Atual"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 306.000000000000000000
          Width = 44.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSintetico."Desco"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 350.000000000000000000
          Width = 44.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSintetico."Juros"]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 2.000000000000000000
          Width = 164.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DataField = 'NOMECLI'
          DataSet = frxDsSintetico
          DataSetName = 'frxDsSintetico'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsSintetico."NOMECLI"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 686.000000000000000000
          Width = 32.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0.00;-0.00; '#39', <frxDsSintetico."ULO">)]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 250.000000000000000000
          Width = 56.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSintetico."Devolvido"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 462.000000000000000000
          Width = 56.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[(<frxDsSintetico."Valor"> - <frxDsSintetico."Devolvido">)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 190.000000000000000000
          Width = 56.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSintetico."LimiCred"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 166.000000000000000000
          Width = 24.000000000000000000
          Height = 14.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsSintetico."ITENS"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 56.377860000000000000
        Top = 143.622140000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 2.000000000000000000
          Top = 4.377860000000000000
          Width = 716.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGray
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            'RISCO SINT'#201'TICO')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 518.000000000000000000
          Top = 28.377860000000000000
          Width = 56.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 394.000000000000000000
          Top = 28.377860000000000000
          Width = 64.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Pago')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 574.000000000000000000
          Top = 28.377860000000000000
          Width = 56.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Saldo')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 630.000000000000000000
          Top = 28.377860000000000000
          Width = 56.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Atual')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 306.000000000000000000
          Top = 28.377860000000000000
          Width = 44.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Tx-desc')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 350.000000000000000000
          Top = 28.377860000000000000
          Width = 44.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Juros')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 2.000000000000000000
          Top = 28.377860000000000000
          Width = 164.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 686.000000000000000000
          Top = 28.377860000000000000
          Width = 32.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '%'
            'ULO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 250.000000000000000000
          Top = 28.377860000000000000
          Width = 56.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Vencido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 462.000000000000000000
          Top = 28.377860000000000000
          Width = 56.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$'
            'Aberto')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 190.000000000000000000
          Top = 28.377860000000000000
          Width = 56.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Limite'
            'Operacional')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 166.000000000000000000
          Top = 28.377860000000000000
          Width = 24.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde'
            'Docs')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Band11: TfrxReportSummary
        Height = 32.000000000000000000
        Top = 336.378170000000000000
        Width = 718.110700000000000000
        object Memo106: TfrxMemoView
          Left = 2.000000000000000000
          Top = 3.149350000000030000
          Width = 164.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL GERAL')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 518.000000000000000000
          Top = 3.149350000000030000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSintetico."Valor">)]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 394.000000000000000000
          Top = 3.149350000000030000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSintetico."Pago">)]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 574.000000000000000000
          Top = 3.149350000000030000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSintetico."Saldo">)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 630.000000000000000000
          Top = 3.149350000000030000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSintetico."Atual">)]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 306.000000000000000000
          Top = 3.149350000000030000
          Width = 44.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSintetico."Desco">)]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 350.000000000000000000
          Top = 3.149350000000030000
          Width = 44.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSintetico."Juros">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 686.000000000000000000
          Top = 3.149350000000030000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsSintetico."Atual">) / SUM(<frxDsSintetico."LimiCred"' +
              '>) * 100)]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 250.000000000000000000
          Top = 3.149350000000030000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSintetico."Devolvido">)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 462.000000000000000000
          Top = 3.149350000000030000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSintetico."Valor">-<frxDsSintetico."Devolvido">)]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 190.000000000000000000
          Top = 3.149350000000030000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSintetico."LimiCred">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 166.000000000000000000
          Top = 3.149350000000030000
          Width = 24.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsSintetico."ITENS">)]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsSintetico: TfrxDBDataset
    UserName = 'frxDsSintetico'
    CloseDataSource = False
    DataSet = QrSintetico
    BCDToCurrency = False
    Left = 456
    Top = 316
  end
end
