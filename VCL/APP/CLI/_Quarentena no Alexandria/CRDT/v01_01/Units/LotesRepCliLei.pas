unit LotesRepCliLei;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Variants, Grids, DBGrids, Db,
  mySQLDbTables, Mask, DBCtrls, dmkEdit, UnDmkProcFunc, dmkGeral, UnDmkEnums;

type
  TFmLotesRepCliLei = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    Panel3: TPanel;
    DBG1: TDBGrid;
    QrCheques: TmySQLQuery;
    DsCheques: TDataSource;
    BitBtn1: TBitBtn;
    QrChequesCodigo: TIntegerField;
    QrChequesControle: TIntegerField;
    QrChequesComp: TIntegerField;
    QrChequesBanco: TIntegerField;
    QrChequesAgencia: TIntegerField;
    QrChequesConta: TWideStringField;
    QrChequesCheque: TIntegerField;
    QrChequesCPF: TWideStringField;
    QrChequesEmitente: TWideStringField;
    QrChequesBruto: TFloatField;
    QrChequesDesco: TFloatField;
    QrChequesValor: TFloatField;
    QrChequesEmissao: TDateField;
    QrChequesDCompra: TDateField;
    QrChequesDDeposito: TDateField;
    QrChequesVencto: TDateField;
    QrChequesTxaCompra: TFloatField;
    QrChequesTxaJuros: TFloatField;
    QrChequesTxaAdValorem: TFloatField;
    QrChequesVlrCompra: TFloatField;
    QrChequesVlrAdValorem: TFloatField;
    QrChequesDMais: TIntegerField;
    QrChequesDias: TIntegerField;
    QrChequesDuplicata: TWideStringField;
    QrChequesDevolucao: TIntegerField;
    QrChequesQuitado: TIntegerField;
    QrChequesLk: TIntegerField;
    QrChequesDataCad: TDateField;
    QrChequesDataAlt: TDateField;
    QrChequesUserCad: TIntegerField;
    QrChequesUserAlt: TIntegerField;
    QrChequesPraca: TIntegerField;
    QrChequesBcoCobra: TIntegerField;
    QrChequesAgeCobra: TIntegerField;
    QrChequesTotalJr: TFloatField;
    QrChequesTotalDs: TFloatField;
    QrChequesTotalPg: TFloatField;
    QrChequesData3: TDateField;
    QrChequesProrrVz: TIntegerField;
    QrChequesProrrDd: TIntegerField;
    QrChequesRepassado: TSmallintField;
    QrChequesDepositado: TSmallintField;
    QrChequesValQuit: TFloatField;
    QrChequesValDeposito: TFloatField;
    QrChequesTipo: TIntegerField;
    QrChequesAliIts: TIntegerField;
    QrChequesAlinPgs: TIntegerField;
    QrChequesNaoDeposita: TSmallintField;
    QrChequesReforcoCxa: TSmallintField;
    QrChequesCartDep: TIntegerField;
    QrChequesCobranca: TIntegerField;
    QrChequesRepCli: TIntegerField;
    QrChequesNOMECLI: TWideStringField;
    QrChequesLote: TIntegerField;
    Timer1: TTimer;
    Panel9: TPanel;
    Panel10: TPanel;
    Label43: TLabel;
    EdCMC_7_1: TEdit;
    Label49: TLabel;
    EdCPF_2: TdmkEdit;
    EdEmitente2: TdmkEdit;
    Label50: TLabel;
    Label44: TLabel;
    EdRegiaoCompe: TdmkEdit;
    EdBanco: TdmkEdit;
    Label45: TLabel;
    Label46: TLabel;
    EdAgencia: TdmkEdit;
    EdConta: TdmkEdit;
    Label47: TLabel;
    EdRealCC: TdmkEdit;
    EdCheque: TdmkEdit;
    Label48: TLabel;
    Label42: TLabel;
    EdValor: TdmkEdit;
    EdVencto: TdmkEdit;
    Label3: TLabel;
    EdBanda: TdmkEdit;
    Panel8: TPanel;
    Label24: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    EdSoma: TdmkEdit;
    EdSaldo: TdmkEdit;
    EdFalta: TDBEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure DBG1CellClick(Column: TColumn);
    procedure DBG1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdSomaChange(Sender: TObject);
    procedure EdFaltaChange(Sender: TObject);
    procedure EdBandaChange(Sender: TObject);
    procedure EdBandaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCMC_7_1Change(Sender: TObject);
  private
    { Private declarations }
    FAscendente: Boolean;
    procedure ReopenCheques(Controle: Integer);
    procedure SomaLinhas(Ascendente: Boolean);
    procedure PreparaSoma(Ascendente: Boolean);
    procedure ExecutaSelecionados(Acao: Integer);
    procedure ExecutaAtual(Acao: Integer);
    procedure Executa;
    procedure CalculaSaldo;
  public
    { Public declarations }
    FAcao, FRepCli: Integer;

  end;

  var
  FmLotesRepCliLei: TFmLotesRepCliLei;

implementation

uses Module, Lotes1, Principal, UnMyObjects;

{$R *.DFM}

procedure TFmLotesRepCliLei.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLotesRepCliLei.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmLotesRepCliLei.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLotesRepCliLei.RGOrdem1Click(Sender: TObject);
begin
  ReopenCheques(QrChequesControle.Value);
end;

procedure TFmLotesRepCliLei.ReopenCheques(Controle: Integer);
const
  NomeA: array[0..5] of string = ('Vencto','Valor','Cheque','Banco , Agencia , Conta ','Emitente','NOMECLI');
  NomeD: array[0..5] of string = ('Vencto DESC','Cheque DESC','Valor DESC','Banco DESC, Agencia DESC, Conta DESC','Emitente','NOMECLI');
var
  Ordem: String;
begin
  Ordem := 'ORDER BY Vencto';
  //
  QrCheques.Close;
  QrCheques.SQL.Clear;
  QrCheques.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrCheques.SQL.Add('ELSE ent.Nome END NOMECLI, lot.Lote, loi.*');
  QrCheques.SQL.Add('FROM lotesits loi');
  QrCheques.SQL.Add('LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo');
  QrCheques.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente');
  QrCheques.SQL.Add('WHERE lot.Tipo = 0');
  QrCheques.SQL.Add('AND loi.RepCli = 0');
  QrCheques.SQL.Add('AND loi.Repassado = 0');
  QrCheques.SQL.Add('AND loi.Devolucao = 0');
  QrCheques.SQL.Add('AND loi.Depositado = 0');
  QrCheques.SQL.Add('');
  QrCheques.SQL.Add(dmkPF.SQL_Periodo('AND loi.Vencto ', 0, Date, False, True));
  //
  QrCheques.SQL.Add(Ordem);
  QrCheques.Open;
end;

procedure TFmLotesRepCliLei.PreparaSoma(Ascendente: Boolean);
begin
  FAscendente := Ascendente;
  if Timer1.Enabled then Timer1.Enabled := False;
  Timer1.Enabled := True;
end;

procedure TFmLotesRepCliLei.SomaLinhas(Ascendente: Boolean);
var
  Valor: Double;
  i, k: Integer;
begin
  k := QrChequesControle.Value;
  if DBG1.SelectedRows.Count = 0 then Valor := QrChequesValor.Value else
  begin
    Valor := 0;
    with DBG1.DataSource.DataSet do
    begin
      if Ascendente then
      begin
        for i:= 0 to DBG1.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          Valor := Valor + QrChequesValor.Value;
        end;
      end else begin
        for i:= DBG1.SelectedRows.Count-1 downto 0 do
        begin
          GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          Valor := Valor + QrChequesValor.Value;
        end;
      end;
    end;
    QrCheques.Locate('Controle', k, []);
  end;
  EdSoma.Text := Geral.FFT(Valor, 2, siNegativo);
end;

procedure TFmLotesRepCliLei.Timer1Timer(Sender: TObject);
begin
  SomaLinhas(FAscendente);
end;

procedure TFmLotesRepCliLei.DBG1CellClick(Column: TColumn);
begin
  SomaLinhas(True);
end;

procedure TFmLotesRepCliLei.DBG1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_DOWN) and (Shift=[ssShift]) then PreparaSoma(True)  else
  if (Key=VK_UP)   and (Shift=[ssShift]) then PreparaSoma(False) else
  if (Key=VK_DOWN)                       then PreparaSoma(True)  else
  if (Key=VK_UP)                         then PreparaSoma(False);
end;

procedure TFmLotesRepCliLei.FormCreate(Sender: TObject);
begin
  case FmPrincipal.FFormLotesShow of
    //0: EdFalta.DataSource := FmLotes0.DsLotes;
    1: EdFalta.DataSource := FmLotes1.DsLotes;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (19)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  ReopenCheques(0);
end;

procedure TFmLotesRepCliLei.ExecutaSelecionados(Acao: Integer);
var
  i : Integer;
begin
  with DBG1.DataSource.DataSet do
  for i:= 0 to DBG1.SelectedRows.Count-1 do
  begin
    GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
    ExecutaAtual(Acao);
  end;
end;

procedure TFmLotesRepCliLei.ExecutaAtual(Acao: Integer);
begin
  Dmod.QrUpdM.SQL.Clear;
  case acao of
    0:
    begin
      Dmod.QrUpdM.SQL.Add('UPDATE lotesits SET Depositado=1 WHERE Controle=:P0 ');
      Dmod.QrUpdM.Params[0].AsInteger := QrChequesControle.Value;
    end;
    1:
    begin
      Dmod.QrUpdM.SQL.Add('UPDATE lotesits SET RepCli=:P0 WHERE Controle=:P1 ');
      Dmod.QrUpdM.Params[0].AsInteger := FRepCli;
      Dmod.QrUpdM.Params[1].AsInteger := QrChequesControle.Value;
    end;
  end;
  //
  Dmod.QrUpdM.ExecSQL;
end;

procedure TFmLotesRepCliLei.Executa;
begin
  Screen.Cursor := crHourGlass;
  if DBG1.SelectedRows.Count = 0 then
    ExecutaAtual(FAcao)
  else
    ExecutaSelecionados(FAcao);
  ReopenCheques(0);
  if FAcao = 1 then
  begin
    case FmPrincipal.FFormLotesShow of
      {
      0:
      begin
        FmLotes0.CalculaLote(FRepCli, True);
        FmLotes0.LocCod(FRepCli, FRepCli);
      end;
      }
      1:
      begin
        FmLotes1.CalculaLote(FRepCli, True);
        FmLotes1.LocCod(FRepCli, FRepCli);
      end;
      else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
      'n�o definida! (18)'), 'ERRO', MB_OK+MB_ICONERROR);
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmLotesRepCliLei.BitBtn1Click(Sender: TObject);
begin
  Executa;
end;

procedure TFmLotesRepCliLei.BtOKClick(Sender: TObject);
begin
  Executa;
end;

procedure TFmLotesRepCliLei.EdSomaChange(Sender: TObject);
begin
  CalculaSaldo;
end;

procedure TFmLotesRepCliLei.EdFaltaChange(Sender: TObject);
begin
  CalculaSaldo;
end;

procedure TFmLotesRepCliLei.CalculaSaldo;
begin
  EdSaldo.Text := Geral.FFT(Geral.DMV(EdSoma.Text) - Geral.DMV(
    EdFalta.Text), 2, siNegativo); 
end;

procedure TFmLotesRepCliLei.EdBandaChange(Sender: TObject);
begin
  EdCMC_7_1.Text := Geral.SoNumero_TT(EdBanda.Text);
end;

procedure TFmLotesRepCliLei.EdBandaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then BtSaidaClick(Self);
end;

procedure TFmLotesRepCliLei.EdCMC_7_1Change(Sender: TObject);
var
  Banda: TBandaMagnetica;
  Tam: Integer;
begin
  Tam := Length(EdCMC_7_1.Text);
  if Tam = 30 then
  begin
    if MLAGeral.CalculaCMC7(EdCMC_7_1.Text) = 0 then
    begin
      Banda := TBandaMagnetica.Create;
      Banda.BandaMagnetica := EdCMC_7_1.Text;
      //EdComp.Text    := Banda.Compe;
      EdBanco.Text   := Banda.Banco;
      EdAgencia.Text := Banda.Agencia;
      EdConta.Text   := Banda.Conta;
      EdCheque.Text  := Banda.Numero;
      //
      if QrCheques.Locate('Banco;Agencia;Conta;Cheque', VarArrayOf([
      EdBanco.Text, EdAgencia.Text, EdConta.Text, EdCheque.Text]), []) then
      begin
        EdValor.Text := Geral.FFT(QrChequesValor.Value, 2, siPositivo);
        EdVencto.Text := Geral.FDT(QrChequesVencto.Value, 2);
        EdEmitente2.text := QrChequesEmitente.Value;
        EdCPF_2.Text := MLAGeral.FormataCNPJ_TFT(QrChequesCPF.Value);
      end else Application.MessageBox('Cheque n�o localizado!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      //Pesquisa(False, False);
    end;
  end;
end;

end.

