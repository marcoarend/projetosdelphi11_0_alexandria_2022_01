object FmLotesDep: TFmLotesDep
  Left = 339
  Top = 185
  Caption = 'Dep'#243'sitos'
  ClientHeight = 496
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtDeposito: TBitBtn
      Tag = 301
      Left = 104
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Dep'#243'sito'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtDepositoClick
    end
    object Panel2: TPanel
      Left = 680
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtRefresh: TBitBtn
      Tag = 18
      Left = 10
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Reabre'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtRefreshClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Dep'#243'sitos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 400
    Align = alClient
    TabOrder = 0
    object DBG1: TDBGrid
      Left = 1
      Top = 102
      Width = 790
      Height = 297
      Align = alClient
      DataSource = DsCheques
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnCellClick = DBG1CellClick
      OnKeyDown = DBG1KeyDown
      Columns = <
        item
          Expanded = False
          FieldName = 'DDeposito'
          Title.Caption = 'Vencto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cheque'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Banco'
          Title.Caption = 'Bco'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Agencia'
          Title.Caption = 'Agen.'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Conta'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Width = 208
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 208
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Title.Caption = 'Border'#244
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Lote'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Controle'
          Visible = True
        end>
    end
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 101
      Align = alTop
      TabOrder = 1
      object Panel5: TPanel
        Left = 480
        Top = 1
        Width = 309
        Height = 99
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 101
          Height = 99
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object CkDescende1: TCheckBox
            Left = 4
            Top = 12
            Width = 89
            Height = 17
            Caption = 'Descendente.'
            TabOrder = 0
          end
          object CkDescende2: TCheckBox
            Left = 4
            Top = 44
            Width = 89
            Height = 17
            Caption = 'Descendente.'
            Checked = True
            State = cbChecked
            TabOrder = 1
          end
          object CkDescende3: TCheckBox
            Left = 4
            Top = 76
            Width = 89
            Height = 17
            Caption = 'Descendente.'
            TabOrder = 2
          end
        end
        object Panel8: TPanel
          Left = 101
          Top = 0
          Width = 208
          Height = 99
          Align = alClient
          BevelOuter = bvLowered
          TabOrder = 1
          object Label24: TLabel
            Left = 106
            Top = 56
            Width = 95
            Height = 13
            Caption = 'Soma selecionados:'
          end
          object Label2: TLabel
            Left = 4
            Top = 56
            Width = 84
            Height = 13
            Caption = 'Total pesquisado:'
            FocusControl = DBEdit1
          end
          object EdSoma: TdmkEdit
            Left = 106
            Top = 72
            Width = 97
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object Panel3: TPanel
            Left = 1
            Top = 1
            Width = 206
            Height = 52
            Align = alTop
            TabOrder = 1
          end
          object DBEdit1: TDBEdit
            Left = 4
            Top = 72
            Width = 97
            Height = 21
            DataField = 'VALOR'
            DataSource = DsSoma
            TabOrder = 2
          end
        end
      end
      object Panel6: TPanel
        Left = 1
        Top = 1
        Width = 479
        Height = 99
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object RGOrdem3: TRadioGroup
          Left = 0
          Top = 32
          Width = 479
          Height = 32
          Align = alTop
          Caption = ' Ordem 3: '
          Columns = 6
          ItemIndex = 1
          Items.Strings = (
            'Vencimento'
            'Valor'
            'Cheque'
            'BAC'
            'Emitente'
            'Cliente')
          TabOrder = 0
          OnClick = RGOrdem1Click
        end
        object RGOrdem2: TRadioGroup
          Left = 0
          Top = 64
          Width = 479
          Height = 32
          Align = alTop
          Caption = ' Ordem 2: '
          Columns = 6
          ItemIndex = 2
          Items.Strings = (
            'Vencimento'
            'Valor'
            'Cheque'
            'BAC'
            'Emitente'
            'Cliente')
          TabOrder = 1
          OnClick = RGOrdem1Click
        end
        object RGOrdem1: TRadioGroup
          Left = 0
          Top = 0
          Width = 479
          Height = 32
          Align = alTop
          Caption = ' Ordem 1: '
          Columns = 6
          ItemIndex = 0
          Items.Strings = (
            'Vencimento'
            'Valor'
            'Cheque'
            'BAC'
            'Emitente'
            'Cliente')
          TabOrder = 2
          OnClick = RGOrdem1Click
        end
      end
    end
  end
  object QrCheques: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrChequesAfterOpen
    AfterClose = QrChequesAfterClose
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMECLI, lot.Lote, loi.*'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE lot.Tipo = 0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Depositado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.Vencto <= :P0')
    Left = 212
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrChequesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrChequesControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrChequesComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrChequesBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrChequesAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrChequesConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrChequesCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrChequesCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrChequesEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrChequesBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
    end
    object QrChequesDesco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrChequesValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrChequesEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrChequesDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
    end
    object QrChequesDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrChequesTxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrChequesTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrChequesVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrChequesVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrChequesDMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrChequesDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrChequesDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrChequesDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrChequesQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrChequesLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrChequesDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChequesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChequesUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrChequesUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrChequesPraca: TIntegerField
      FieldName = 'Praca'
      Required = True
    end
    object QrChequesBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
      Required = True
    end
    object QrChequesAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
      Required = True
    end
    object QrChequesTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrChequesTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrChequesTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrChequesData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrChequesProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrChequesProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrChequesRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrChequesDepositado: TSmallintField
      FieldName = 'Depositado'
      Required = True
    end
    object QrChequesValQuit: TFloatField
      FieldName = 'ValQuit'
      Required = True
    end
    object QrChequesValDeposito: TFloatField
      FieldName = 'ValDeposito'
      Required = True
    end
    object QrChequesTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrChequesAliIts: TIntegerField
      FieldName = 'AliIts'
      Required = True
    end
    object QrChequesAlinPgs: TIntegerField
      FieldName = 'AlinPgs'
      Required = True
    end
    object QrChequesNaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
      Required = True
    end
    object QrChequesReforcoCxa: TSmallintField
      FieldName = 'ReforcoCxa'
      Required = True
    end
    object QrChequesCartDep: TIntegerField
      FieldName = 'CartDep'
      Required = True
    end
    object QrChequesCobranca: TIntegerField
      FieldName = 'Cobranca'
      Required = True
    end
    object QrChequesRepCli: TIntegerField
      FieldName = 'RepCli'
      Required = True
    end
    object QrChequesNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrChequesLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrChequesObsGerais: TWideStringField
      FieldName = 'ObsGerais'
      Size = 60
    end
  end
  object DsCheques: TDataSource
    DataSet = QrCheques
    Left = 240
    Top = 68
  end
  object Timer1: TTimer
    Interval = 300
    OnTimer = Timer1Timer
    Left = 332
    Top = 205
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) VALOR'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'WHERE lot.Tipo = 0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Depositado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.Vencto <= :P0')
    Left = 212
    Top = 97
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSoma: TDataSource
    DataSet = QrSoma
    Left = 241
    Top = 97
  end
  object QrMin: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MIN(loi.Vencto) Vencto'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'WHERE lot.Tipo=0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.Depositado = 0')
    Left = 112
    Top = 232
    object QrMinVencto: TDateField
      FieldName = 'Vencto'
    end
  end
end
