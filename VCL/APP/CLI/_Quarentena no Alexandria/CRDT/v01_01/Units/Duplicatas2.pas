unit Duplicatas2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, DBCtrls, Db, mySQLDbTables,
  Grids, DBGrids, Menus, Mask, frxClass, frxDBSet, Variants, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkCheckGroup, UnDmkProcFunc,
  UnDmkEnums, dmkCheckBox;

type
  TFmDuplicatas2 = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLIENTE: TWideStringField;
    DsClientes: TDataSource;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqControle: TIntegerField;
    QrPesqDuplicata: TWideStringField;
    QrPesqDCompra: TDateField;
    QrPesqValor: TFloatField;
    QrPesqDDeposito: TDateField;
    QrPesqEmitente: TWideStringField;
    QrPesqCPF: TWideStringField;
    QrPesqCliente: TIntegerField;
    QrPesqNOMECLIENTE: TWideStringField;
    QrPesqSTATUS: TWideStringField;
    QrPesqQuitado: TIntegerField;
    QrSumPg: TmySQLQuery;
    QrSumPgJuros: TFloatField;
    QrSumPgPago: TFloatField;
    QrSumPgDesco: TFloatField;
    QrSumPgValor: TFloatField;
    QrSumPgMaxData: TDateField;
    QrLocPg: TmySQLQuery;
    QrLocPgData: TDateField;
    QrLocPgJuros: TFloatField;
    QrLocPgPago: TFloatField;
    QrLocPgLotePg: TIntegerField;
    QrLocPgLk: TIntegerField;
    QrLocPgDataCad: TDateField;
    QrLocPgDataAlt: TDateField;
    QrLocPgUserCad: TIntegerField;
    QrLocPgUserAlt: TIntegerField;
    QrLocPgLotesIts: TIntegerField;
    QrLocPgControle: TIntegerField;
    QrLocPgDesco: TFloatField;
    QrPesqTotalJr: TFloatField;
    QrPesqTotalDs: TFloatField;
    QrPesqTotalPg: TFloatField;
    QrADupIts: TmySQLQuery;
    QrADupItsAlinea: TIntegerField;
    QrADupItsLk: TIntegerField;
    QrADupItsDataCad: TDateField;
    QrADupItsDataAlt: TDateField;
    QrADupItsUserCad: TIntegerField;
    QrADupItsUserAlt: TIntegerField;
    QrADupItsLotesIts: TIntegerField;
    QrADupItsControle: TIntegerField;
    QrADupItsDataA: TDateField;
    DsADupIts: TDataSource;
    DsADupPgs: TDataSource;
    QrADupPgs: TmySQLQuery;
    QrADupPgsData: TDateField;
    QrADupPgsJuros: TFloatField;
    QrADupPgsPago: TFloatField;
    QrADupPgsLotePg: TIntegerField;
    QrADupPgsLk: TIntegerField;
    QrADupPgsDataCad: TDateField;
    QrADupPgsDataAlt: TDateField;
    QrADupPgsUserCad: TIntegerField;
    QrADupPgsUserAlt: TIntegerField;
    QrADupPgsLotesIts: TIntegerField;
    QrADupPgsControle: TIntegerField;
    QrADupPgsDesco: TFloatField;
    Panel3: TPanel;
    QrADupItsNOMESTATUS: TWideStringField;
    QrPos: TmySQLQuery;
    QrPosAlinea: TIntegerField;
    PMStatus: TPopupMenu;
    Inclui1: TMenuItem;
    Exclui1: TMenuItem;
    QrPesqSALDO_ATUALIZADO: TFloatField;
    QrPesqNOMESTATUS: TWideStringField;
    PMPagto: TPopupMenu;
    Inclui2: TMenuItem;
    Exclui2: TMenuItem;
    QrPesqVencto: TDateField;
    QrPesqDDCALCJURO: TIntegerField;
    QrPesqData3: TDateField;
    Panel4: TPanel;
    PainelCtrl: TPanel;
    PainelConf: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Panel8: TPanel;
    GradeItens: TDBGrid;
    PainelMuda: TPanel;
    Panel7: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    DBGrid4: TDBGrid;
    DBGrid3: TDBGrid;
    TabSheet2: TTabSheet;
    BtZAZ: TBitBtn;
    BtPagtoDuvida: TBitBtn;
    BtCuidado: TBitBtn;
    PanelFill1: TPanel;
    LaTipo: TLabel;
    BtDuplicataOcorr: TBitBtn;
    BtStatusManual: TBitBtn;
    PMStatusManual: TPopupMenu;
    Incluiprorrogao1: TMenuItem;
    ExcluiProrrogao1: TMenuItem;
    PMOcorreu: TPopupMenu;
    Incluiocorrncia1: TMenuItem;
    Alteraocorrncia1: TMenuItem;
    Excluiocorrncia1: TMenuItem;
    QrOcorreu: TmySQLQuery;
    QrOcorreuNOMEOCORRENCIA: TWideStringField;
    QrOcorreuCodigo: TIntegerField;
    QrOcorreuLotesIts: TIntegerField;
    QrOcorreuDataO: TDateField;
    QrOcorreuOcorrencia: TIntegerField;
    QrOcorreuValor: TFloatField;
    QrOcorreuLoteQuit: TIntegerField;
    QrOcorreuLk: TIntegerField;
    QrOcorreuDataCad: TDateField;
    QrOcorreuDataAlt: TDateField;
    QrOcorreuUserCad: TIntegerField;
    QrOcorreuUserAlt: TIntegerField;
    QrOcorreuTaxaP: TFloatField;
    QrOcorreuTaxaV: TFloatField;
    QrOcorreuPago: TFloatField;
    QrOcorreuDataP: TDateField;
    QrOcorreuTaxaB: TFloatField;
    DsOcorreu: TDataSource;
    QrLotesPrr: TmySQLQuery;
    QrLotesPrrCodigo: TIntegerField;
    QrLotesPrrControle: TIntegerField;
    QrLotesPrrData1: TDateField;
    QrLotesPrrData2: TDateField;
    QrLotesPrrData3: TDateField;
    QrLotesPrrOcorrencia: TIntegerField;
    QrLotesPrrLk: TIntegerField;
    QrLotesPrrDataCad: TDateField;
    QrLotesPrrDataAlt: TDateField;
    QrLotesPrrUserCad: TIntegerField;
    QrLotesPrrUserAlt: TIntegerField;
    QrLotesPrrDIAS_1_3: TIntegerField;
    QrLotesPrrDIAS_2_3: TIntegerField;
    DsLotesPrr: TDataSource;
    QrPesqCNPJ_TXT: TWideStringField;
    QrPesqTAXA_COMPRA: TFloatField;
    QrPesqTxaCompra: TFloatField;
    QrPesqDevolucao: TIntegerField;
    QrPesqProrrVz: TIntegerField;
    QrPesqProrrDd: TIntegerField;
    PainelOcor: TPanel;
    Panel5: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    EdOcorrencia: TdmkEditCB;
    CBOcorrencia: TdmkDBLookupComboBox;
    TPDataO: TDateTimePicker;
    EdValor: TdmkEdit;
    PainelPror: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label29: TLabel;
    Label27: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    TPDataN: TDateTimePicker;
    EdTaxaPror: TdmkEdit;
    EdJurosPeriodoN: TdmkEdit;
    EdJurosPr: TdmkEdit;
    EdValorBase: TdmkEdit;
    TPDataI: TDateTimePicker;
    QrOcorBank: TmySQLQuery;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    QrOcorBankLk: TIntegerField;
    QrOcorBankDataCad: TDateField;
    QrOcorBankDataAlt: TDateField;
    QrOcorBankUserCad: TIntegerField;
    QrOcorBankUserAlt: TIntegerField;
    QrOcorBankBase: TFloatField;
    DsOcorBank: TDataSource;
    QrLocPr: TmySQLQuery;
    QrLocPrCodigo: TIntegerField;
    QrLocPrControle: TIntegerField;
    QrLocPrData1: TDateField;
    QrLocPrData2: TDateField;
    QrLocPrData3: TDateField;
    QrLocPrOcorrencia: TIntegerField;
    QrLocPrLk: TIntegerField;
    QrLocPrDataCad: TDateField;
    QrLocPrDataAlt: TDateField;
    QrLocPrUserCad: TIntegerField;
    QrLocPrUserAlt: TIntegerField;
    N1: TMenuItem;
    Recibo1: TMenuItem;
    QrPesqEmissao: TDateField;
    QrSoma: TmySQLQuery;
    DsSoma: TDataSource;
    QrVcto: TmySQLQuery;
    QrSomaValor: TFloatField;
    QrSomaSALDO_DESATUALIZADO: TFloatField;
    QrVctoSOMA_VALOR: TFloatField;
    QrVctoSALDO_DESATUALIZADO: TFloatField;
    QrVctoSOMA_DIAS: TFloatField;
    QrVctoFATOR_VD: TFloatField;
    QrVctoPRAZO_MEDIO: TFloatField;
    DsVcto: TDataSource;
    QrVctoJURO_CLI: TFloatField;
    QrSP: TmySQLQuery;
    QrSPSTATUS: TWideStringField;
    QrSPControle: TIntegerField;
    QrSPDuplicata: TWideStringField;
    QrSPEmissao: TDateField;
    QrSPDCompra: TDateField;
    QrSPValor: TFloatField;
    QrSPDDeposito: TDateField;
    QrSPEmitente: TWideStringField;
    QrSPCPF: TWideStringField;
    QrSPCliente: TIntegerField;
    QrSPQuitado: TIntegerField;
    QrSPTotalJr: TFloatField;
    QrSPTotalDs: TFloatField;
    QrSPTotalPg: TFloatField;
    QrSPNOMECLIENTE: TWideStringField;
    QrSPVencto: TDateField;
    QrSPData3: TDateField;
    QrSPTxaCompra: TFloatField;
    QrSPDevolucao: TIntegerField;
    QrSPProrrVz: TIntegerField;
    QrSPProrrDd: TIntegerField;
    QrSPSALDO_DESATUALIZ: TFloatField;
    QrSPSALDO_ATUALIZADO: TFloatField;
    BtReabre: TBitBtn;
    QrPesqRepassado: TSmallintField;
    N2: TMenuItem;
    IncluiPagamento1: TMenuItem;
    Excluipagamento1: TMenuItem;
    N3: TMenuItem;
    Recibodepagamento1: TMenuItem;
    PainelOcorPg: TPanel;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    Label82: TLabel;
    Label83: TLabel;
    Label85: TLabel;
    Label84: TLabel;
    TPDataBase6: TDateTimePicker;
    EdValorBase6: TdmkEdit;
    EdJurosBase6: TdmkEdit;
    EdJurosPeriodo6: TdmkEdit;
    TPPagto6: TDateTimePicker;
    EdJuros6: TdmkEdit;
    EdAPagar6: TdmkEdit;
    EdPago6: TdmkEdit;
    QrLocOc: TmySQLQuery;
    QrLocOcCodigo: TIntegerField;
    QrLocOcOcorreu: TIntegerField;
    QrLocOcData: TDateField;
    QrLocOcJuros: TFloatField;
    QrLocOcPago: TFloatField;
    QrLocOcLotePg: TIntegerField;
    QrLocOcLk: TIntegerField;
    QrLocOcDataCad: TDateField;
    QrLocOcDataAlt: TDateField;
    QrLocOcUserCad: TIntegerField;
    QrLocOcUserAlt: TIntegerField;
    QrSumOc: TmySQLQuery;
    QrSumOcJuros: TFloatField;
    QrSumOcPago: TFloatField;
    QrLastOcor: TmySQLQuery;
    QrLastOcorData: TDateField;
    QrLastOcorCodigo: TIntegerField;
    QrOcorrPg: TmySQLQuery;
    QrOcorrPgCodigo: TIntegerField;
    QrOcorrPgOcorreu: TIntegerField;
    QrOcorrPgData: TDateField;
    QrOcorrPgJuros: TFloatField;
    QrOcorrPgPago: TFloatField;
    QrOcorrPgLotePg: TIntegerField;
    QrOcorrPgLk: TIntegerField;
    QrOcorrPgDataCad: TDateField;
    QrOcorrPgDataAlt: TDateField;
    QrOcorrPgUserCad: TIntegerField;
    QrOcorrPgUserAlt: TIntegerField;
    DsOcorrPg: TDataSource;
    TabSheet3: TTabSheet;
    DBGrid2: TDBGrid;
    DBGrid5: TDBGrid;
    DBGrid1: TDBGrid;
    QrOcorreuATUALIZADO: TFloatField;
    QrOcorreuCLIENTELOTE: TIntegerField;
    QrOcorreuData3: TDateField;
    QrOcorreuStatus: TSmallintField;
    QrOcorreuCliente: TIntegerField;
    QrOcorreuSALDO: TFloatField;
    BtQuitacao: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    PMQuitacao: TPopupMenu;
    Quitadocumento1: TMenuItem;
    Imprimerecibodequitao1: TMenuItem;
    QrPesqValQuit: TFloatField;
    QrPesqNF: TIntegerField;
    QrPesqBanco: TIntegerField;
    QrPesqAgencia: TIntegerField;
    PageControl2: TPageControl;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    PainelPesq: TPanel;
    Label75: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label9: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdDuplicata: TdmkEdit;
    EdEmitente: TdmkEdit;
    RGMascara: TRadioGroup;
    EdCPF: TdmkEdit;
    TPEIni: TDateTimePicker;
    TPEFim: TDateTimePicker;
    CkEIni: TCheckBox;
    CkEFim: TCheckBox;
    CkVIni: TCheckBox;
    TPVIni: TDateTimePicker;
    CkVFim: TCheckBox;
    TPVFim: TDateTimePicker;
    EdColigado: TdmkEditCB;
    CBColigado: TdmkDBLookupComboBox;
    GroupBox1: TGroupBox;
    Label3: TLabel;
    Label5: TLabel;
    Label7: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit3: TDBEdit;
    EdAtualizado: TdmkEdit;
    EdITENS: TdmkEdit;
    CkRepassado: TdmkCheckGroup;
    CkStatus: TdmkCheckGroup;
    Panel6: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem4: TRadioGroup;
    QrPesqPERIODO: TFloatField;
    RGAgrupa: TRadioGroup;
    BtImprime: TBitBtn;
    QrPesq2: TmySQLQuery;
    QrPesq2Controle: TIntegerField;
    QrPesq2Duplicata: TWideStringField;
    QrPesq2DCompra: TDateField;
    QrPesq2Valor: TFloatField;
    QrPesq2DDeposito: TDateField;
    QrPesq2Emitente: TWideStringField;
    QrPesq2CPF: TWideStringField;
    QrPesq2Cliente: TIntegerField;
    QrPesq2NOMECLIENTE: TWideStringField;
    QrPesq2STATUS: TWideStringField;
    QrPesq2Quitado: TIntegerField;
    QrPesq2TotalJr: TFloatField;
    QrPesq2TotalDs: TFloatField;
    QrPesq2TotalPg: TFloatField;
    QrPesq2SALDO_DESATUALIZ: TFloatField;
    QrPesq2SALDO_ATUALIZADO: TFloatField;
    QrPesq2NOMESTATUS: TWideStringField;
    QrPesq2Vencto: TDateField;
    QrPesq2DDCALCJURO: TIntegerField;
    QrPesq2Data3: TDateField;
    QrPesq2CNPJ_TXT: TWideStringField;
    QrPesq2TAXA_COMPRA: TFloatField;
    QrPesq2TxaCompra: TFloatField;
    QrPesq2Devolucao: TIntegerField;
    QrPesq2ProrrVz: TIntegerField;
    QrPesq2ProrrDd: TIntegerField;
    QrPesq2Emissao: TDateField;
    QrPesq2Repassado: TSmallintField;
    QrPesq2ValQuit: TFloatField;
    QrPesq2NF: TIntegerField;
    QrPesq2Banco: TIntegerField;
    QrPesq2Agencia: TIntegerField;
    QrPesq2CONTA: TIntegerField;
    QrPesq2PERIODO: TFloatField;
    QrPesq2Valor_TXT: TWideStringField;
    QrPesq2DDeposito_TXT: TWideStringField;
    QrPesq2MES_TXT: TWideStringField;
    QrPesqSALDO_DESATUALIZ: TFloatField;
    BtRefresh: TBitBtn;
    QrOcu1: TmySQLQuery;
    QrOcu1NOMEOCORRENCIA: TWideStringField;
    QrOcu1Codigo: TIntegerField;
    QrOcu1LotesIts: TIntegerField;
    QrOcu1DataO: TDateField;
    QrOcu1Ocorrencia: TIntegerField;
    QrOcu1Valor: TFloatField;
    QrOcu1LoteQuit: TIntegerField;
    QrOcu1Lk: TIntegerField;
    QrOcu1DataCad: TDateField;
    QrOcu1DataAlt: TDateField;
    QrOcu1UserCad: TIntegerField;
    QrOcu1UserAlt: TIntegerField;
    QrOcu1TaxaP: TFloatField;
    QrOcu1TaxaV: TFloatField;
    QrOcu1Pago: TFloatField;
    QrOcu1DataP: TDateField;
    QrOcu1TaxaB: TFloatField;
    QrOcu1ATUALIZADO: TFloatField;
    QrOcu1CLIENTELOTE: TIntegerField;
    QrOcu1Data3: TDateField;
    QrOcu1Status: TSmallintField;
    QrOcu1Cliente: TIntegerField;
    QrOcu1SALDO: TFloatField;
    ProgressBar1: TProgressBar;
    Label11: TLabel;
    EdOcorrencias: TdmkEdit;
    EdOcorAtualiz: TdmkEdit;
    Label12: TLabel;
    Label17: TLabel;
    EdTotalAtualiz: TdmkEdit;
    QrPesqOCORRENCIAS: TFloatField;
    QrPesqOCORATUALIZ: TFloatField;
    QrPesqTOTALATUALIZ: TFloatField;
    QrOcu2: TmySQLQuery;
    QrOcu2NOMEOCORRENCIA: TWideStringField;
    QrOcu2Codigo: TIntegerField;
    QrOcu2LotesIts: TIntegerField;
    QrOcu2DataO: TDateField;
    QrOcu2Ocorrencia: TIntegerField;
    QrOcu2Valor: TFloatField;
    QrOcu2LoteQuit: TIntegerField;
    QrOcu2Lk: TIntegerField;
    QrOcu2DataCad: TDateField;
    QrOcu2DataAlt: TDateField;
    QrOcu2UserCad: TIntegerField;
    QrOcu2UserAlt: TIntegerField;
    QrOcu2TaxaP: TFloatField;
    QrOcu2TaxaV: TFloatField;
    QrOcu2Pago: TFloatField;
    QrOcu2DataP: TDateField;
    QrOcu2TaxaB: TFloatField;
    QrOcu2ATUALIZADO: TFloatField;
    QrOcu2CLIENTELOTE: TIntegerField;
    QrOcu2Data3: TDateField;
    QrOcu2Status: TSmallintField;
    QrOcu2Cliente: TIntegerField;
    QrOcu2SALDO: TFloatField;
    QrPesq2OCORRENCIAS: TFloatField;
    QrPesq2OCORATUALIZ: TFloatField;
    QrPesq2TOTALATUALIZ: TFloatField;
    QrColigados: TmySQLQuery;
    DsColigados: TDataSource;
    QrColigadosNOMECLIENTE: TWideStringField;
    QrColigadosCodigo: TIntegerField;
    QrADupPgsLote: TIntegerField;
    TabSheet6: TTabSheet;
    QrPesqSALDO_TEXTO: TWideStringField;
    QrOcorreuATZ_TEXTO: TWideStringField;
    QrPesq2ATZ_TEXTO: TWideStringField;
    CkAtualiza: TCheckBox;
    CkMorto: TCheckBox;
    QrPesqLOCALT: TLargeintField;
    QrPesqNOMELOCAL: TWideStringField;
    QrPesq2DATA3TXT: TWideStringField;
    QrPesqPERIODO3: TFloatField;
    QrPesq2PERIODO3: TFloatField;
    QrPesq2MEQ_TXT: TWideStringField;
    Label55: TLabel;
    EdControle: TdmkEdit;
    TabSheet7: TTabSheet;
    Memo1: TMemo;
    QrCNAB240: TmySQLQuery;
    DsCNAB240: TDataSource;
    DBGrid6: TDBGrid;
    QrCNAB240CNAB240L: TIntegerField;
    QrCNAB240CNAB240I: TIntegerField;
    QrCNAB240Controle: TIntegerField;
    QrCNAB240Banco: TIntegerField;
    QrCNAB240Sequencia: TIntegerField;
    QrCNAB240Convenio: TWideStringField;
    QrCNAB240ConfigBB: TIntegerField;
    QrCNAB240Lote: TIntegerField;
    QrCNAB240Item: TIntegerField;
    QrCNAB240DataOcor: TDateField;
    QrCNAB240Envio: TSmallintField;
    QrCNAB240Movimento: TSmallintField;
    QrCNAB240Custas: TFloatField;
    QrCNAB240ValPago: TFloatField;
    QrCNAB240ValCred: TFloatField;
    QrCNAB240Acao: TIntegerField;
    QrCNAB240Lk: TIntegerField;
    QrCNAB240DataCad: TDateField;
    QrCNAB240DataAlt: TDateField;
    QrCNAB240UserCad: TIntegerField;
    QrCNAB240UserAlt: TIntegerField;
    QrCNAB240NOMEENVIO: TWideStringField;
    QrCNAB240NOMEMOVIMENTO: TWideStringField;
    QrCNAB240IRTCLB: TWideStringField;
    Splitter1: TSplitter;
    CkQIni: TCheckBox;
    TPQIni: TDateTimePicker;
    CkQFim: TCheckBox;
    TPQFim: TDateTimePicker;
    QrClientesTe1: TWideStringField;
    Label18: TLabel;
    DBEdit5: TDBEdit;
    QrClientesTe1_TXT: TWideStringField;
    QrSacados: TmySQLQuery;
    QrSacadosNumero: TFloatField;
    QrSacadosCNPJ: TWideStringField;
    QrSacadosIE: TWideStringField;
    QrSacadosNome: TWideStringField;
    QrSacadosRua: TWideStringField;
    QrSacadosCompl: TWideStringField;
    QrSacadosBairro: TWideStringField;
    QrSacadosCidade: TWideStringField;
    QrSacadosUF: TWideStringField;
    QrSacadosCEP: TIntegerField;
    QrSacadosTel1: TWideStringField;
    QrSacadosRisco: TFloatField;
    QrSacadosAlterWeb: TSmallintField;
    Label19: TLabel;
    N4: TMenuItem;
    N1Forastatusprorrogado1: TMenuItem;
    N0Statusautomtico1: TMenuItem;
    N2ForastatusBaixado1: TMenuItem;
    N3ForastatusMoroso1: TMenuItem;
    DBEdit6: TDBEdit;
    QrSomaPrincipal: TFloatField;
    CkOcorrencias: TCheckBox;
    BtHistCNAB: TBitBtn;
    PMHistCNAB: TPopupMenu;
    Excluihistriciatual1: TMenuItem;
    Excluitodositensdehistricosdestaduplicata1: TMenuItem;
    frxPesq1: TfrxReport;
    frxDsPesq2: TfrxDBDataset;
    frxPesq2: TfrxReport;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbBordero: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PMImprime: TPopupMenu;
    Extratodeduplicata1: TMenuItem;
    frxExtratoDuplicata: TfrxReport;
    frxDsOcorreu: TfrxDBDataset;
    QrOcorreuSEQ: TIntegerField;
    QrPesqDATA3_TXT: TWideStringField;
    frxDsADupIts: TfrxDBDataset;
    frxDsADupPgs: TfrxDBDataset;
    frxDsLotesPrr: TfrxDBDataset;
    ImprimecartadeCancelamentodeProtesto1: TMenuItem;
    ImprimecartadeCancelamentodeProtesto2: TMenuItem;
    QrPesqCodigo: TIntegerField;
    frxDsPesq: TfrxDBDataset;
    PMDiario: TPopupMenu;
    Adicionareventoaodiario1: TMenuItem;
    Gerenciardirio1: TMenuItem;
    QrPesq2Tel1: TWideStringField;
    QrPesq2TEL1_TXT: TWideStringField;
    QrPesqTel1: TWideStringField;
    QrSacadosEmail: TWideStringField;
    EdHistorico: TdmkEditCB;
    CBHistorico: TdmkDBLookupComboBox;
    CkHistorico: TdmkCheckBox;
    DsOcorDupl: TDataSource;
    QrOcorDupl: TmySQLQuery;
    QrOcorDuplCodigo: TIntegerField;
    QrOcorDuplNome: TWideStringField;
    frxDsOcorrPg: TfrxDBDataset;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdCPFExit(Sender: TObject);
    procedure RGMascaraClick(Sender: TObject);
    procedure QrPesqCalcFields(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure BtZAZClick(Sender: TObject);
    procedure QrPesqAfterScroll(DataSet: TDataSet);
    procedure BtCuidadoClick(Sender: TObject);
    procedure BtPagtoDuvidaClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure Exclui1Click(Sender: TObject);
    procedure Inclui2Click(Sender: TObject);
    procedure Exclui2Click(Sender: TObject);
    procedure GradeItensDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure QrPesqAfterClose(DataSet: TDataSet);
    procedure PMPagtoPopup(Sender: TObject);
    procedure BtStatusManualClick(Sender: TObject);
    procedure BtDuplicataOcorrClick(Sender: TObject);
    procedure Incluiocorrncia1Click(Sender: TObject);
    procedure Alteraocorrncia1Click(Sender: TObject);
    procedure Excluiocorrncia1Click(Sender: TObject);
    procedure QrLotesPrrCalcFields(DataSet: TDataSet);
    procedure BtDesisteClick(Sender: TObject);
    procedure TPDataNChange(Sender: TObject);
    procedure EdTaxaProrChange(Sender: TObject);
    procedure EdValorBaseChange(Sender: TObject);
    procedure EdJurosPrChange(Sender: TObject);
    procedure TPDataIChange(Sender: TObject);
    procedure EdOcorrenciaExit(Sender: TObject);
    procedure EdValorExit(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure Incluiprorrogao1Click(Sender: TObject);
    procedure ExcluiProrrogao1Click(Sender: TObject);
    procedure CkEIniClick(Sender: TObject);
    procedure TPEIniChange(Sender: TObject);
    procedure CkEFimClick(Sender: TObject);
    procedure TPEFimChange(Sender: TObject);
    procedure CkVIniClick(Sender: TObject);
    procedure TPVIniChange(Sender: TObject);
    procedure CkVFimClick(Sender: TObject);
    procedure TPVFimChange(Sender: TObject);
    procedure Recibo1Click(Sender: TObject);
    procedure QrVctoCalcFields(DataSet: TDataSet);
    procedure BtReabreClick(Sender: TObject);
    procedure EdColigadoChange(Sender: TObject);
    procedure IncluiPagamento1Click(Sender: TObject);
    procedure Excluipagamento1Click(Sender: TObject);
    procedure EdValorBase6Change(Sender: TObject);
    procedure EdJurosBase6Change(Sender: TObject);
    procedure TPPagto6Change(Sender: TObject);
    procedure EdJuros6Change(Sender: TObject);
    procedure EdAPagar6Change(Sender: TObject);
    procedure Recibodepagamento1Click(Sender: TObject);
    procedure QrOcorreuCalcFields(DataSet: TDataSet);
    procedure QrOcorreuAfterScroll(DataSet: TDataSet);
    procedure BtQuitacaoClick(Sender: TObject);
    procedure Quitadocumento1Click(Sender: TObject);
    procedure Imprimerecibodequitao1Click(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure RGOrdem3Click(Sender: TObject);
    procedure RGOrdem4Click(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrPesq2CalcFields(DataSet: TDataSet);
    procedure GradeItensTitleClick(Column: TColumn);
    procedure BtRefreshClick(Sender: TObject);
    procedure QrOcu1CalcFields(DataSet: TDataSet);
    procedure QrOcu2CalcFields(DataSet: TDataSet);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
    procedure EdDuplicataChange(Sender: TObject);
    procedure EdEmitenteChange(Sender: TObject);
    procedure EdControleExit(Sender: TObject);
    procedure QrCNAB240CalcFields(DataSet: TDataSet);
    procedure QrClientesCalcFields(DataSet: TDataSet);
    procedure GradeItensDblClick(Sender: TObject);
    procedure N0Statusautomtico1Click(Sender: TObject);
    procedure N1Forastatusprorrogado1Click(Sender: TObject);
    procedure N2ForastatusBaixado1Click(Sender: TObject);
    procedure N3ForastatusMoroso1Click(Sender: TObject);
    procedure BtHistCNABClick(Sender: TObject);
    procedure Excluihistriciatual1Click(Sender: TObject);
    procedure Excluitodositensdehistricosdestaduplicata1Click(
      Sender: TObject);
    procedure QrCNAB240AfterOpen(DataSet: TDataSet);
    procedure frxPesq1GetValue(const VarName: String; var Value: Variant);
    procedure Extratodeduplicata1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxExtratoDuplicataGetValue(const VarName: string;
      var Value: Variant);
    procedure ImprimecartadeCancelamentodeProtesto1Click(Sender: TObject);
    procedure ImprimecartadeCancelamentodeProtesto2Click(Sender: TObject);
    procedure SbBorderoClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GradeItensMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Adicionareventoaodiario1Click(Sender: TObject);
    procedure Gerenciardirio1Click(Sender: TObject);
    procedure PMDiarioPopup(Sender: TObject);
    procedure PMStatusPopup(Sender: TObject);
    procedure PMStatusManualPopup(Sender: TObject);
    procedure PMOcorreuPopup(Sender: TObject);
    procedure PMQuitacaoPopup(Sender: TObject);
    procedure PMHistCNABPopup(Sender: TObject);
    procedure EdHistoricoChange(Sender: TObject);
    procedure CkHistoricoClick(Sender: TObject);
    procedure QrOcorreuBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    FOrdC1, FOrdC2, FOrdC3, FOrdS1, FOrdS2, FOrdS3: String;
    FEmitente, FCPF, FDuplicata: String;
    FCliente, FOcorrPg, FOcorP, FOcorreu, FLotesPrr, FControle, FCNAB240I: Integer;
    FSomando: Boolean;
    FTempo, FUltim: TDateTime;

    function TextoSQLPesq(TabLote, TabLoteIts, Local: String): Integer;

    procedure ForcaStatus(NovoStatus: Integer);
    procedure InfoTempo(Tempo: TDateTime; Texto: String; Inicial: Boolean);
    procedure ConfiguraPg(Data: TDateTime);
    procedure AtualizaDevolucao(LotesIts: Integer);
    procedure ReopenSubItens;
    procedure MostraEdicao(Acao: Integer; Titulo: String);
    procedure ReopenOcorreu(Codigo: Integer);
    procedure ReopenLotesPrr(Controle: Integer);
    procedure ReopenCNAB240(CNAB240I: Integer);
    procedure CalculaAPagarPrr;
    procedure CalculaJurosPrr;
    procedure ConfirmaOcorrencia;
    procedure Pesquisa(Forca: Boolean);
    procedure ConfiguraPr(Data: TDateTime);
    procedure CondicoesExtras(Query: TmySQLQuery; Coligado: Integer);
    procedure ConfiguraPgOC;
    procedure CalculaJurosOcor;
    procedure CalculaAPagarOcor;
    procedure ConfirmaOcorPG;
    procedure CalculaPagtoOcorrPg(Quitacao: TDateTime; OcorrPg: Integer);
    procedure ReopenOcorrPg;
    procedure FecharTabelas;

  public
    { Public declarations }
    FCallFromLotes: Boolean;
    ForcaOBData: TDateTime;
    ForcaOcorBank, FLotesIts, FAdupIts, FAdupPgs: Integer;
    FLotePgOrigem: Integer;
    procedure PreparaDuplicataPg(Acao, LotesIts: Integer);
    procedure ReabrirTabelas;
    procedure CalculaPagamento(LotesIts: Integer);
  end;
var
  FmDuplicatas2: TFmDuplicatas2;

implementation

{$R *.DFM}

uses ModuleGeral, Module, Duplicatas2Pg, UnInternalConsts, UMySQLModule,
  UnGOTOy, Modulc, SacadosEdit, DuplicCancProt, Principal, UnMyObjects, UnBancos;

procedure TFmDuplicatas2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDuplicatas2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if FCallFromLotes then
    if PainelOcor.Visible then
      if EdValor.Visible then EdValor.SetFocus;
end;

procedure TFmDuplicatas2.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmDuplicatas2.FormShow(Sender: TObject);
begin
  if Length(EdControle.Text) > 0 then
  begin
    CkStatus.SetMaxValue;
    ReabrirTabelas();
  end;
end;

procedure TFmDuplicatas2.FormCreate(Sender: TObject);
begin
  GradeItens.PopupMenu := PMDiario;
  FOrdC1 := 'SALDO_DESATUALIZ';
  FOrdS1 := '';
  FOrdC2 := '';
  FOrdS2 := '';
  FOrdC3 := '';
  FOrdS3 := '';
  if Screen.Width > 800 then
  begin
    Width := Width + (Screen.Width - 800) div 2;
    Left  := (Screen.Width - Width) div 2;
  end;
  UMyMod.AbreQuery(QrClientes, Dmod.MyDB);
  UMyMod.AbreQuery(QrColigados, Dmod.MyDB);
  UMyMod.AbreQuery(QrOcorBank, Dmod.MyDB);
  UMyMod.AbreQuery(QrOcorDupl, Dmod.MyDB);
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  Panel5.Align     := alClient;
  TPEIni.Date := Date - 180;
  TPEFim.Date := Date;
  TPVFim.Date := Date + 180;
  TPVIni.Date := Date;
  TPQFim.Date := Date;
  TPQIni.Date := Date;
  //
  CkRepassado.Value := 3;
  CkStatus.Value    := 3;
end;

procedure TFmDuplicatas2.EdClienteChange(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.EdCPFExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCPF.Text);
  if CPF <> '' then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if MLAGeral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Geral.MB_Erro('N�mero inv�lido para CPF/CNPJ');
      EdCPF.SetFocus;
    end else EdCPF.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCPF.Text := '';
  FecharTabelas;
end;

procedure TFmDuplicatas2.RGMascaraClick(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.Pesquisa;
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.ReabrirTabelas;
const
  ItensOrdem: array[0..6] of String = ('NOMECLIENTE', 'li.DDeposito', 'PERIODO',
  'Emitente', 'CNPJ_TXT', 'li.Data3', 'PERIODO3');
var
  Atualizado, Ocorrencias, OcorAtualiz, TotalAtualiz, CalcAtualiz: Double;
  Orelha: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
  Orelha := PageControl1.ActivePageIndex;
  PageControl1.ActivePageIndex := 4;
  Application.ProcessMessages;
  InfoTempo(Now, 'Reabertura de tabelas', True);
  if Trim(EdEmitente.Text) <> '' then
  begin
    FEmitente := EdEmitente.Text;
    if RGMascara.ItemIndex in ([0,1]) then FEmitente := '%'+FEmitente;
    if RGMascara.ItemIndex in ([0,2]) then FEmitente := FEmitente+'%';
  end else FEmitente := '';
  if Trim(EdCPF.Text) <> '' then FCPF := Geral.SoNumero_TT(EdCPF.Text)
  else FCPF := '';
  if Trim(EdDuplicata.Text) <> '' then FDuplicata := EdDuplicata.Text
  else FDuplicata := '';
  FControle := Geral.IMV(EdControle.Text);
  FCliente := Geral.IMV(EdCliente.Text);
  //////////////////////////////////////////////////////////////////////////////
  QrPesq.Close;
  QrPesq.SQL.Clear;

  QrSoma.Close;
  QrSoma.SQL.Clear;

  QrVcto.Close;
  QrVcto.SQL.Clear;

  //Ok := Ok +
  TextoSQLPesq('lotes', 'lotesits', '10');
  if CkMorto.Checked then
  begin
    QrPesq.SQL.Add('');
    QrPesq.SQL.Add('UNION');
    QrPesq.SQL.Add('');
    //
    QrSoma.SQL.Add('');
    QrSoma.SQL.Add('UNION');
    QrSoma.SQL.Add('');
    //
    QrVcto.SQL.Add('');
    QrVcto.SQL.Add('UNION');
    QrVcto.SQL.Add('');
    //
    //Ok := Ok +
    TextoSQLPesq('lotez', 'lotezits', '20');
  end;
  //

  case PageControl2.ActivePageIndex of
    0: QrPesq.SQL.Add(MLAGeral.OrdemSQL3(FOrdC1, FOrdS1, FOrdC2, FOrdS2, FOrdC3, FOrdS3));
    1: QrPesq.SQL.Add('ORDER BY '+
    ItensOrdem[RGOrdem1.ItemIndex]+', '+
    ItensOrdem[RGOrdem2.ItemIndex]+', '+
    ItensOrdem[RGOrdem3.ItemIndex]+', '+
    ItensOrdem[RGOrdem4.ItemIndex]);
    else QrPesq.SQL.Add('ORDER BY ?');
  end;
  QrPesq.Open;
  //
  InfoTempo(Now, 'Duplicatas', False);
  QrSoma.Open;
  InfoTempo(Now, 'Saldo desatualizado', False);
  QrVcto.Open;
  InfoTempo(Now, 'Saldo Vencido', False);
  //////////////////////////////////////////////////////////////////////////////
  if FLotesIts > 0 then QrPesq.Locate('Controle', FLotesIts, []);
  //
  if CkAtualiza.Checked then
  begin
    QrSP.Close;
    QrSP.SQL := QrPesq.SQL;
    QrSP.Open;
    Atualizado   := 0;
    Ocorrencias  := 0;
    OcorAtualiz  := 0;
    TotalAtualiz := 0;
    EdAtualizado.Visible := False;
    ProgressBar1.Max := QrSP.RecordCount;
    ProgressBar1.Position := 0;
    ProgressBar1.Visible := True;
    while not QrSP.Eof do
    begin
      ProgressBar1.Position := ProgressBar1.Position + 1;
      CalcAtualiz := DMod.ObtemValorAtualizado(
        QrSPCliente.Value, QrSPQuitado.Value, QrSPVencto.Value, Date,
        QrSPData3.Value, QrSPValor.Value, QrSPTotalJr.Value,
        QrSPTotalDs.Value, QrSPTotalPg.Value, 0, True);
      Atualizado := Atualizado + CalcAtualiz;
      if CkOcorrencias.Checked then
      begin
        QrOcu2.Close;
        QrOcu2.Params[00].AsInteger := QrSPControle.Value;
        QrOcu2.Open;
        while not QrOcu2.Eof do
        begin
          Ocorrencias  := Ocorrencias  + QrOcu2SALDO.Value;
          OcorAtualiz  := OcorAtualiz  + QrOcu2ATUALIZADO.Value;
          TotalAtualiz := TotalAtualiz + QrOcu2ATUALIZADO.Value;
          QrOcu2.Next;
        end;
        TotalAtualiz := TotalAtualiz + CalcAtualiz;
      end;
      QrSP.Next;
    end;
    EdAtualizado.Text   := Geral.FFT(Atualizado,   2, siNegativo);
    EdOcorrencias.Text  := Geral.FFT(Ocorrencias,  2, siNegativo);
    EdOcorAtualiz.Text  := Geral.FFT(OcorAtualiz,  2, siNegativo);
    EdTotalAtualiz.Text := Geral.FFT(TotalAtualiz, 2, siNegativo);
    InfoTempo(Now, 'Atualiza��es', False);
  end else begin
    EdAtualizado.Text   := 'n�o calculado';
    EdOcorrencias.Text  := 'n�o calculado';
    EdOcorAtualiz.Text  := 'n�o calculado';
    EdTotalAtualiz.Text := 'n�o calculado';
  end;
  //
  ProgressBar1.Visible := False;
  EdAtualizado.Visible := True;
  Memo1.Lines.Add('========================================== Fim reaberturas');
  PageControl1.ActivePageIndex := Orelha;
  Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmDuplicatas2.QrPesqCalcFields(DataSet: TDataSet);
var
  Taxas: MyArrayR07;
  Ocorrencias, OcorAtualiz, TotalAtualiz: Double;
  DtUltimoPg: TDateTime;
begin
  if QrPesqLOCALT.Value < 20 then QrPesqNOMELOCAL.Value := 'Ativo'
  else QrPesqNOMELOCAL.Value := 'Morto';
  //////////////////////////////////////////////////////////////////////////////
  QrPesqNOMESTATUS.Value := MLAGeral.NomeStatusPgto2(QrPesqQuitado.Value,
    QrPesqDDeposito.Value, Date, QrPesqData3.Value, QrPesqRepassado.Value);
  //////////////////////////////////////////////////////////////////////////////
  if CkAtualiza.Checked then
  begin
    { Com problema na data 3 modificado em 17/12/2012
    QrPesqSALDO_ATUALIZADO.Value := DMod.ObtemValorAtualizado(
      QrPesqCliente.Value, QrPesqQuitado.Value, QrPesqVencto.Value, Date,
      QrPesqData3.Value, QrPesqValor.Value, QrPesqTotalJr.Value, QrPesqTotalDs.Value,
      QrPesqTotalPg.Value, 0, True);
    }
    QrLocPg.Close;
    QrLocPg.Params[0].AsInteger := QrPesqControle.Value;
    QrLocPg.Open;
    if QrLocPg.RecordCount > 0 then
      DtUltimoPg := QrLocPgData.Value
    else
      DtUltimoPg := QrPesqDDeposito.Value;
    //
    QrPesqSALDO_ATUALIZADO.Value := DMod.ObtemValorAtualizado(
      QrPesqCliente.Value, QrPesqQuitado.Value, QrPesqVencto.Value, Date,
      DtUltimoPg, QrPesqValor.Value, QrPesqTotalJr.Value, QrPesqTotalDs.Value,
      QrPesqTotalPg.Value, 0, True);
    //
    QrPesqSALDO_TEXTO.Value := Geral.FFT(QrPesqSALDO_ATUALIZADO.Value, 2, siNegativo);
    if CkOcorrencias.Checked then
    begin
      Ocorrencias  := 0;
      OcorAtualiz  := 0;
      TotalAtualiz := 0;
      QrOcu1.Close;
      QrOcu1.Params[00].AsInteger := QrPesqControle.Value;
      QrOcu1.Open;
      while not QrOcu1.Eof do
      begin
        Ocorrencias  := Ocorrencias  + QrOcu1SALDO.Value;
        OcorAtualiz  := OcorAtualiz  + QrOcu1ATUALIZADO.Value;
        TotalAtualiz := TotalAtualiz + QrOcu1ATUALIZADO.Value;
        QrOcu1.Next;
      end;
      QrPesqOCORRENCIAS.Value  := Ocorrencias;
      QrPesqOCORATUALIZ.Value  := OcorAtualiz;
      QrPesqTOTALATUALIZ.Value := TotalAtualiz + QrPesqSALDO_ATUALIZADO.Value;
    end else begin
      QrPesqOCORRENCIAS.Value  := 0;
      QrPesqOCORATUALIZ.Value  := 0;
      QrPesqTOTALATUALIZ.Value := 0;
    end;
  end else begin
    QrPesqSALDO_ATUALIZADO.Value := QrPesqSALDO_DESATUALIZ.Value;
    QrPesqSALDO_TEXTO.Value := 'n�o calculado';
    //
    QrPesqOCORRENCIAS.Value  := 0;
    QrPesqOCORATUALIZ.Value  := 0;
    QrPesqTOTALATUALIZ.Value := 0;
  end;
  //////////////////////////////////////////////////////////////////////////////
  QrPesqCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrPesqCPF.Value);
  //
  Taxas := DMod.ObtemTaxaDeCompraRealizada(QrPesqControle.Value);
  QrPesqTAXA_COMPRA.Value := QrPesqTxaCompra.Value + Taxas[0];
  QrPesqDATA3_TXT.Value := dmkPF.FDT_NULO(QrPesqDATA3.Value, 2);
  //
end;

procedure TFmDuplicatas2.CalculaPagamento(LotesIts: Integer);
var
  Sit: Integer;
  Cred, Debi: Double;
  Data3, Data4: String;
begin
  QrSumPg.Close;
  QrSumPg.Params[0].AsInteger := LotesIts;
  QrSumPg.Open;
  Cred := QrSumPgPago.Value + QrSumPgDesco.Value;
  Debi := QrSumPgValor.Value + QrSumPgJuros.Value;
  if QrSumPgPago.Value < 0.01 then Sit := 0 else
  begin
    if Cred < (Debi - 0.009) then Sit := 1 else
    begin
      if Cred > (Debi + 0.009) then Sit := 3 else Sit := 2;
    end;
  end;
  // novo 2007 08 21
  Data4 := FormatDateTime(VAR_FORMATDATE, QrSumPgMaxData.Value);
  if Sit > 1 then Data3 := Data4 else Data3 := '0000-00-00';
  // fim novo
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   ');
  Dmod.QrUpd.SQL.Add('TotalJr=:P0, TotalDs=:P1, TotalPg=:P2, ');
  Dmod.QrUpd.SQL.Add('Data3=:P3, Data4=:P4, Quitado=:P5 WHERE Controle=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := QrSumPgJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumPgDesco.Value;
  Dmod.QrUpd.Params[02].AsFloat   := QrSumPgPago.Value;
  Dmod.QrUpd.Params[03].AsString  := Data3;
  Dmod.QrUpd.Params[04].AsString  := Data4;
  Dmod.QrUpd.Params[05].AsInteger := Sit;
  //
  Dmod.QrUpd.Params[06].AsInteger := LotesIts;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TFmDuplicatas2.QrPesqAfterOpen(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  EdITENS.Text := Geral.FF0(QrPesq.RecordCount);
  //
  Enab := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  //
  SbImprime.Enabled        := Enab;
  BtImprime.Enabled        := Enab;
  SbBordero.Enabled        := Enab;
  BtCuidado.Enabled        := Enab;
  BtPagtoDuvida.Enabled    := Enab;
  BtZAZ.Enabled            := Enab;
  BtStatusManual.Enabled   := Enab;
  BtDuplicataOcorr.Enabled := Enab;
  BtQuitacao.Enabled       := Enab;
  BtHistCNAB.Enabled       := Enab;
end;

procedure TFmDuplicatas2.ConfiguraPg(Data: TDateTime);
var
  ValorBase: Double;
begin
  QrLocPg.Close;
  QrLocPg.Params[0].AsInteger := QrPesqControle.Value;
  QrLocPg.Open;
  if QrLocPg.RecordCount > 0 then
  begin
    FmDuplicatas2Pg.TPDataBase2.Date := Int(QrLocPgData.Value)
  end else
    FmDuplicatas2Pg.TPDataBase2.Date := Int(QrPesqDDeposito.Value);
  FmDuplicatas2Pg.EdJurosBase2.Text := Geral.FFT(
  Dmod.ObtemTaxaDeCompraCliente(QrPesqCliente.Value), 6, siPositivo);
  ValorBase := QrPesqValor.Value +QrPesqTotalJr.Value - QrPesqTotalDs.Value - QrPesqTotalPg.Value;
  FmDuplicatas2Pg.TPPagto2.Date := Int(Date + 360);
  FmDuplicatas2Pg.EdValorBase2.Text := Geral.FFT(ValorBase, 2, siPositivo);
  if Data >= FmDuplicatas2Pg.TPDataBase2.Date then
    FmDuplicatas2Pg.TPPagto2.Date := Int(Data)
  else FmDuplicatas2Pg.TPPagto2.Date := Int(FmDuplicatas2Pg.TPDataBase2.Date);
  //FmDuplicatas2Pg.TPPagto2.MinDate := Int(FmDuplicatas2Pg.TPDataBase2.Date);
  FmDuplicatas2Pg.TPVenc.Date := QrPesqVencto.Value;
  // Erro
  //FmDuplicatas2Pg.TPPagto2.SetFocus;
end;

procedure TFmDuplicatas2.PreparaDuplicataPg(Acao, LotesIts: Integer);
begin
  FLotesIts := LotesIts;
  FAdupIts  := QrADupItsControle.Value;
  FAdupPgs  := QrADupPgsControle.Value;
  //
  Application.CreateForm(TFmDuplicatas2Pg, FmDuplicatas2Pg);
  FmDuplicatas2Pg.FLotePagto       := 0;
  FmDuplicatas2Pg.FDuplicataOrigem := QrPesqControle.Value;
  FmDuplicatas2Pg.EdCliCod.Text    := Geral.FF0(QrPesqCliente.Value);
  FmDuplicatas2Pg.EdCliNome.Text   := QrPesqNOMECLIENTE.Value;
  FmDuplicatas2Pg.EdDuplicata.Text := QrPesqDuplicata.Value;
  FmDuplicatas2Pg.EdCPF.Text       := Geral.FormataCNPJ_TT(QrPesqCPF.Value);
  FmDuplicatas2Pg.EdEmitente.Text  := QrPesqEmitente.Value;
  FmDuplicatas2Pg.EdValor.Text     := Geral.FFT(QrPesqValor.Value, 2, siPositivo);
  ConfiguraPg(Date);
  //
  case Acao of
    1:
    begin
      FmDuplicatas2Pg.PainelPagto.Visible  := True;
      FmDuplicatas2Pg.PainelStatus.Visible := False;
    end;
    2:
    begin
      FmDuplicatas2Pg.PainelPagto.Visible  := False;
      FmDuplicatas2Pg.PainelStatus.Align   := alClient;
      FmDuplicatas2Pg.PainelStatus.Visible := True;
      //
      FmDuplicatas2Pg.CalculaJuros;
      FmDuplicatas2Pg.CalculaAPagar;
    end;
    3:
    begin
      FmDuplicatas2Pg.PainelPagto.Visible  := True;
      FmDuplicatas2Pg.PainelStatus.Visible := True;
      //
      FmDuplicatas2Pg.CalculaJuros;
      FmDuplicatas2Pg.CalculaAPagar;
    end;
  end;
  //
  FmDuplicatas2Pg.ShowModal;
  FmDuplicatas2Pg.Destroy;
  //
  AtualizaDevolucao(LotesIts);
  ReabrirTabelas;
  //
  if ForcaOcorBank > 0 then MostraEdicao(1, CO_INCLUSAO);
end;

procedure TFmDuplicatas2.QrPesqAfterScroll(DataSet: TDataSet);
begin
  if FSomando then
    Exit;
  //
  ReopenSubItens;
  ReopenOcorreu(0);
  ReopenLotesPrr(0);
  ReopenCNAB240(0);
end;

procedure TFmDuplicatas2.ReopenSubItens;
begin
  QrADupIts.Close;
  QrADupIts.Params[0].AsInteger := QrPesqControle.Value;
  QrADupIts.Open;
  if FAdupIts <> 0 then QrADupIts.Locate('Controle', FADupIts, []);
  //
  QrADupPgs.Close;
  QrADupPgs.Params[0].AsInteger := QrPesqControle.Value;
  QrADupPgs.Open;
  if FAdupPgs <> 0 then QrADupPgs.Locate('Controle', FADupPgs, []);
end;

procedure TFmDuplicatas2.AtualizaDevolucao(LotesIts: Integer);
begin
  QrPos.Close;
  QrPos.Params[0].AsInteger := LotesIts;
  QrPos.Open;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1, Devolucao=:P0 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  Dmod.QrUpd.Params[0].AsInteger := QrPosAlinea.Value;
  Dmod.QrUpd.Params[1].AsInteger := LotesIts;
  Dmod.QrUpd.ExecSQL;
  //
  ReabrirTabelas;
end;

procedure TFmDuplicatas2.BtCuidadoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMStatus, BtCuidado);
end;

procedure TFmDuplicatas2.BtPagtoDuvidaClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMPagto, BtPagtoDuvida);
end;

procedure TFmDuplicatas2.BtZAZClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  PreparaDuplicataPg(3, QrPesqControle.Value);
end;

procedure TFmDuplicatas2.Inclui1Click(Sender: TObject);
begin
  PreparaDuplicataPg(2, QrPesqControle.Value);
end;

procedure TFmDuplicatas2.Exclui1Click(Sender: TObject);
begin
  if QrADupIts.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o do item do hist�rico?') = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM adupits WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrADupItsControle.Value;
      Dmod.QrUpd.ExecSQL;
      //
      FLotesIts     := QrPesqControle.Value;
      FADupIts := UmyMod.ProximoRegistro(QrADupIts, 'Controle',
        QrADupItsControle.Value);
      AtualizaDevolucao(QrPesqControle.Value);
    end;
  end;
end;

procedure TFmDuplicatas2.Inclui2Click(Sender: TObject);
begin
  PreparaDuplicataPg(1, QrPesqControle.Value);
end;

procedure TFmDuplicatas2.Exclui2Click(Sender: TObject);
begin
  if QrADupPgs.RecNo = QrADupPgs.RecordCount then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o do item de pagamento?') = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM aduppgs WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrADupPgsControle.Value;
      Dmod.QrUpd.ExecSQL;
      //
      FLotesIts     := QrPesqControle.Value;
      FADupPgs := UmyMod.ProximoRegistro(QrADupPgs, 'Controle',
        QrADupPgsControle.Value);
      // N�o precisa
      //AtualizaDevolucao(QrPesqControle.Value);
      CalculaPagamento(QrPesqControle.Value);
      ReabrirTabelas;
    end;
  end else Geral.MB_Erro('Somente o �ltimo pagamento pode ser exclu�do!');
end;

procedure TFmDuplicatas2.GradeItensDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  OldAlign: Integer;
begin
  with GradeItens.Canvas do
  begin
    if (Column.FieldName = 'NOMESTATUS') then
    begin
      Font.Color := MLAGeral.CorStatusPgto2(QrPesqQuitado.Value,
        QrPesqDDeposito.Value, Date);
      Font.Style := [fsBold];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_LEFT);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end;
end;

procedure TFmDuplicatas2.GradeItensMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  ACol, ARow: Integer;
begin
end;

procedure TFmDuplicatas2.QrPesqAfterClose(DataSet: TDataSet);
begin
  {
  BtDuplicataOcorr.Enabled := False;
  BtStatusManual.Enabled   := False;
  BtQuitacao.Enabled       := False;
  SbImprime.Enabled        := False;
  //
  }
  EdITENS.Text := '0';
  //
  //QrADupIts.Close;
  QrADupPgs.Close;
end;

procedure TFmDuplicatas2.PMDiarioPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  //
  Adicionareventoaodiario1.Enabled := Enab;
  Gerenciardirio1.Enabled          := Enab;
end;

procedure TFmDuplicatas2.PMHistCNABPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  Enab2 := (QrCNAB240.State <> dsInactive) and (QrCNAB240.RecordCount > 0);
  //
  Excluihistriciatual1.Enabled                       := Enab and Enab2;
  Excluitodositensdehistricosdestaduplicata1.Enabled := Enab and Enab2;
end;

procedure TFmDuplicatas2.PMOcorreuPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  Enab2 := (QrOcorreu.State <> dsInactive) and (QrOcorreu.RecordCount > 0);
  Enab3 := (QrOcorrPg.State <> dsInactive) and (QrOcorrPg.RecordCount > 0);
  //
  Incluiocorrncia1.Enabled := Enab;
  Alteraocorrncia1.Enabled := Enab and Enab2;
  Excluiocorrncia1.Enabled := Enab and Enab2;
  //
  IncluiPagamento1.Enabled := Enab and Enab2;
  Excluipagamento1.Enabled := Enab and Enab2 and Enab3;
  //
  Recibodepagamento1.Enabled := Enab and Enab2 and Enab3;
end;

procedure TFmDuplicatas2.PMPagtoPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  Enab2 := (QrADupPgs.State <> dsInactive) and (QrADupPgs.RecordCount > 0);
  //
  Inclui2.Enabled := Enab;
  Exclui2.Enabled := Enab and Enab2;
  Recibo1.Enabled := Enab and Enab2;
end;

procedure TFmDuplicatas2.PMQuitacaoPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  //
  Quitadocumento1.Enabled                       := Enab;
  Imprimerecibodequitao1.Enabled                := Enab;
  ImprimecartadeCancelamentodeProtesto1.Enabled := Enab;
end;

procedure TFmDuplicatas2.PMStatusManualPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  Enab2 := (QrLotesPrr.State <> dsInactive) and (QrLotesPrr.RecordCount > 0);
  //
  Incluiprorrogao1.Enabled := Enab;
  ExcluiProrrogao1.Enabled := Enab and Enab2;
  //
  N0Statusautomtico1.Enabled      := Enab;
  N1Forastatusprorrogado1.Enabled := Enab;
  N2ForastatusBaixado1.Enabled    := Enab;
  N3ForastatusMoroso1.Enabled     := Enab;
end;

procedure TFmDuplicatas2.PMStatusPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  Enab2 := (QrADupIts.State <> dsInactive) and (QrADupIts.RecordCount > 0);
  //
  Inclui1.Enabled := Enab;
  Exclui1.Enabled := Enab and Enab2;
end;

procedure TFmDuplicatas2.MostraEdicao(Acao: Integer; Titulo: String);
begin
  LaTipo.Caption := Titulo;
  if Acao <> 0 then
  begin
    GradeItens.Enabled := False;
    PainelMuda.Visible := True;
    PainelConf.Visible := True;
    PainelPesq.Enabled := False;
    PainelCtrl.Visible := False;
  end;
  case Acao of
    0:
    begin
      GradeItens.Enabled := True;
      PainelCtrl.Visible := True;
      PainelPesq.Enabled := True;
      PainelMuda.Visible := False;
      PainelConf.Visible := False;
      PainelPror.Visible := False;
    end;
    1:
    begin
      PainelOcor.Visible := True;
      if Titulo = CO_INCLUSAO then
      begin
        if ForcaOcorBank > 0 then
        begin
          EdOcorrencia.Text := Geral.FF0(ForcaOcorBank);
          CBOcorrencia.KeyValue := ForcaOcorBank;
          TPDataO.Date := ForcaOBData;
          if not FCallFromLotes then EdValor.SetFocus;
        end else begin
          EdOcorrencia.Text := '';
          CBOcorrencia.KeyValue := NULL;
          EdOcorrencia.SetFocus;
        end;
        TPDataO.Date := Date;
        EdValor.Text := '';
      end else begin
        EdOcorrencia.Text := Geral.FF0(QrOcorreuOcorrencia.Value);
        CBOcorrencia.KeyValue := QrOcorreuOcorrencia.Value;
        TPDataO.Date := QrOcorreuDataO.Value;
        EdValor.Text := Geral.FFT(QrOcorreuValor.Value, 2, siPositivo);
        EdOcorrencia.SetFocus;
      end;
    end;
    2:
    begin
      //ERRO: Controle Duplicatas: Liberar data da ocorr�ncia na prorroga��o.
      PainelOcor.Visible := True;
      PainelPror.Visible := True;
      //
      // Permitir Retroceder data
      //TPDataO.Enabled    := False;
      if Titulo = CO_INCLUSAO then
      begin
        EdOcorrencia.Text := '';
        CBOcorrencia.KeyValue := NULL;
        TPDataO.Date := Date;
        EdValor.Text := '';
        ConfiguraPr(Date);
      end else begin
        EdOcorrencia.Text := Geral.FF0(QrOcorreuOcorrencia.Value);
        CBOcorrencia.KeyValue := QrOcorreuOcorrencia.Value;
        TPDataO.Date := QrOcorreuDataO.Value;
        EdValor.Text := Geral.FFT(QrOcorreuValor.Value, 2, siPositivo);
      end;
      TPDataN.SetFocus;
    end;
    5:
    begin
      PainelOcorPg.Visible   := True;
      PainelConf.Visible     := True;
      PainelOcor.Visible     := False;
      PainelCtrl.Visible     := False;
      //
      if Titulo = CO_INCLUSAO then
      begin
        ConfiguraPgOC;
        CalculaJurosOcor;
        CalculaAPagarOcor;
      end else begin
      //
      end;
      EdJurosBase6.SetFocus;
    end;
  end;
end;

procedure TFmDuplicatas2.BtStatusManualClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  //
  MyObjects.MostraPopUpDeBotao(PMStatusManual, BtStatusManual);
end;

procedure TFmDuplicatas2.BtDuplicataOcorrClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  //
  MyObjects.MostraPopUpDeBotao(PMOcorreu, BtDuplicataOcorr);
end;

procedure TFmDuplicatas2.Incluiocorrncia1Click(Sender: TObject);
begin
  MostraEdicao(1, CO_INCLUSAO);
end;

procedure TFmDuplicatas2.Adicionareventoaodiario1Click(Sender: TObject);
var
  Texto: String;
begin
  Texto := QrPesqCPF.Value + ' - ' + QrPesqEmitente.Value + sLineBreak;
  //
  FmPrincipal.MostraDiarioAdd(0, QrPesqCliente.Value, QrPesqControle.Value, Texto);
end;

procedure TFmDuplicatas2.Alteraocorrncia1Click(Sender: TObject);
begin
  MostraEdicao(1, CO_ALTERACAO);
end;

procedure TFmDuplicatas2.Excluiocorrncia1Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da ocorr�ncia selecionada?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM ocorreu WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrOcorreuCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    QrOcorreu.Next;
    //
    ReopenOcorreu(QrOcorreuCodigo.Value);
  end;
end;

procedure TFmDuplicatas2.ReopenOcorreu(Codigo: Integer);
begin
  QrOcorreu.Close;
  QrOcorreu.Params[00].AsInteger := QrPesqControle.Value;
  QrOcorreu.Open;
  //
  if Codigo <> 0 then QrOcorreu.Locate('Codigo', Codigo, [])
  else QrOcorreu.Locate('Codigo', FOcorreu, []);
end;

procedure TFmDuplicatas2.ReopenLotesPrr(Controle: Integer);
begin
  QrLotesPrr.Close;
  QrLotesPrr.Params[00].AsInteger := QrPesqControle.Value;
  QrLotesPrr.Open;
  //
  if Controle <> 0 then QrLotesPrr.Locate('Controle', Controle, [])
  else QrLotesPrr.Locate('Controle', FLotesPrr, []);
end;

procedure TFmDuplicatas2.ReopenCNAB240(CNAB240I: Integer);
begin
  QrCNAB240.Close;
  QrCNAB240.Params[00].AsInteger := QrPesqControle.Value;
  QrCNAB240.Open;
  //
  if CNAB240I <> 0 then QrCNAB240.Locate('CNAB240I', CNAB240I, [])
  else QrCNAB240.Locate('CNAB240I', FCNAB240I, []);
end;

procedure TFmDuplicatas2.QrLotesPrrCalcFields(DataSet: TDataSet);
begin
  QrLotesPrrDIAS_1_3.Value := Trunc(Int(QrLotesPrrData3.Value)-Int(QrLotesPrrData1.Value));
  QrLotesPrrDIAS_2_3.Value := Trunc(Int(QrLotesPrrData3.Value)-Int(QrLotesPrrData2.Value));
end;

procedure TFmDuplicatas2.BtDesisteClick(Sender: TObject);
begin
  ForcaOcorBank := 0;
  MostraEdicao(0, CO_TRAVADO);
end;

procedure TFmDuplicatas2.TPDataNChange(Sender: TObject);
begin
  CalculaJurosPrr;
end;

procedure TFmDuplicatas2.EdTaxaProrChange(Sender: TObject);
begin
  CalculaJurosPrr;
end;

procedure TFmDuplicatas2.EdValorBaseChange(Sender: TObject);
begin
  CalculaAPagarPrr;
end;

procedure TFmDuplicatas2.EdJurosPrChange(Sender: TObject);
begin
  CalculaAPagarPrr;
end;

procedure TFmDuplicatas2.TPDataIChange(Sender: TObject);
begin
  CalculaJurosPrr;
end;

procedure TFmDuplicatas2.EdOcorrenciaExit(Sender: TObject);
begin
  if (LaTipo.Caption = CO_INCLUSAO) and (PainelPror.Visible = False) then
  EdValor.Text := Geral.FFT(QrOcorBankBase.Value, 2, siNegativo);
end;

procedure TFmDuplicatas2.EdValorExit(Sender: TObject);
begin
  BtConfirma.SetFocus;
end;

procedure TFmDuplicatas2.CalculaJurosPrr;
var
  Prazo, Praz2: Integer;
  Taxa, Juros, Valor: Double;
begin
  Prazo := Trunc(Int(TPDataN.Date) - Int(TPDataO.Date));
  if Prazo < 0 then Praz2 := -Prazo else Praz2 := Prazo;
  begin
    Taxa  := Geral.DMV(EdTaxaPror.Text);
    Juros := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(Taxa, Praz2);
  end;
  Valor := Geral.DMV(EdValorBase.Text);
  EdJurosPeriodoN.Text := Geral.FFT(Juros, 6, siPositivo);
  Juros := Juros * Valor / 100;
  if Prazo < 0 then Juros := Juros * -1;
  EdJurosPr.Text := Geral.FFT(Juros, 2, siNegativo);
end;

procedure TFmDuplicatas2.CalculaAPagarPrr;
begin
  EdValor.Text := EdJurosPr.Text;
end;

procedure TFmDuplicatas2.BtConfirmaClick(Sender: TObject);
var
  Data: Integer;
begin
  if PainelOcor.Visible and PainelPror.Visible then
  begin
    Data := Trunc(TPDataN.Date);
    if UMyMod.DiaInutil(Data) <> 0 then
    begin
      Geral.MB_Aviso('A data '+Geral.FDT(Data, 3) + ' n�o � um dia �til!');
      Exit;
    end;
  end;
  ForcaOcorBank := 0;
  if QrPesq.State     = dsBrowse then FLotesIts := QrPesqControle.Value;
  if QrOcorreu.State  = dsBrowse then FOcorreu  := QrOcorreuCodigo.Value;
  if QrLotesPrr.State = dsBrowse then FLotesPrr := QrLotesPrrControle.Value;
  if QrADupIts.State  = dsBrowse then FADupIts  := QrADupItsControle.Value;
  if QrADupPgs.State  = dsBrowse then FADupPgs  := QrADupPgsControle.Value;
  //
  if PainelOcor.Visible then ConfirmaOcorrencia else
  if PainelOcorPg.Visible then ConfirmaOcorPg;

  MostraEdicao(0, CO_TRAVADO);
  //
  if FCallFromLotes then Close else Pesquisa(True);
end;

procedure TFmDuplicatas2.ConfirmaOcorrencia;
var
  Ocorr, Prorr, Data2: Integer;
  DataN: String;
begin
  Prorr := QrLotesPrrControle.Value;
  Dmod.QrUpd.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpd.SQL.Add('INSERT INTO ocorreu SET AlterWeb=1, ');
    Ocorr := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'Ocorreu', 'Ocorreu', 'Codigo');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET AlterWeb=1, ');
    Ocorr := QrOcorreuCodigo.Value;
  end;
  Dmod.QrUpd.SQL.Add('LotesIts=:P0, DataO=:P1, Ocorrencia=:P2, Valor=:P3');
  Dmod.QrUpd.SQL.Add('');
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa');
  end else begin
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
  end;
  Dmod.QrUpd.Params[00].AsInteger := QrPesqControle.Value;
  Dmod.QrUpd.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataO.Date);
  Dmod.QrUpd.Params[02].AsInteger := Geral.IMV(EdOcorrencia.Text);
  Dmod.QrUpd.Params[03].AsFloat   := Geral.DMV(EdValor.Text);
  Dmod.QrUpd.Params[04].AsInteger := Ocorr;
  Dmod.QrUpd.ExecSQL;
  //
  if PainelPror.Visible then
  begin
    DataN := FormatDateTime(VAR_FORMATDATE, TPDataN.Date);
    Dmod.QrUpd.SQL.Clear;
    if LaTipo.Caption = CO_INCLUSAO then
    begin
      Dmod.QrUpd.SQL.Add('INSERT INTO lotesprr SET ');
      Prorr := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'LotesPrr', 'LotesPrr', 'Controle');
      if QrLotesPrr.RecordCount = 0 then
        Data2 := Trunc(TPDataI.Date)
      else
        Data2 := Trunc(QrLotesPrrData3.Value);
    end else begin
      Dmod.QrUpd.SQL.Add('UPDATE lotesprr SET ');
      Prorr := QrLotesPrrControle.Value;
      Data2 := Trunc(QrLotesPrrData2.Value);
    end;
    Dmod.QrUpd.SQL.Add('Codigo=:P0, Data1=:P1, Data2=:P2, Data3=:P3, ');
    Dmod.QrUpd.SQL.Add('Ocorrencia=:P4');
    if LaTipo.Caption = CO_INCLUSAO then
    begin
      Dmod.QrUpd.SQL.Add(', Controle=:Pa');
    end else begin
      Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa');
    end;
    Dmod.QrUpd.Params[00].AsInteger := QrPesqControle.Value;
    Dmod.QrUpd.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataI.Date);
    Dmod.QrUpd.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Data2);//TPDataO.Date);
    Dmod.QrUpd.Params[03].AsString  := DataN;
    Dmod.QrUpd.Params[04].AsInteger := Ocorr;
    //
    Dmod.QrUpd.Params[05].AsInteger := Prorr;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   DDeposito=:P0, ProrrVz=:P1, ');
    Dmod.QrUpd.SQL.Add('ProrrDd=:P2, Quitado=:P3 WHERE Controle=:P4');
    Dmod.QrUpd.Params[0].AsString  := DataN;
    Dmod.QrUpd.Params[1].AsInteger := QrLotesPrr.RecordCount+1;
    Dmod.QrUpd.Params[2].AsInteger := Trunc(Int(TPDataN.Date)-Int(TPDataI.Date));
    Dmod.QrUpd.Params[3].AsInteger := -1; //Prorrogado
    //
    Dmod.QrUpd.Params[4].AsInteger := QrPesqControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
  end;
  FOcorreu  := Ocorr;
  FLotesPrr := Prorr;
end;

procedure TFmDuplicatas2.Incluiprorrogao1Click(Sender: TObject);
begin
  MostraEdicao(2, CO_INCLUSAO);
end;

procedure TFmDuplicatas2.ExcluiProrrogao1Click(Sender: TObject);
var
  Dias: Integer;
  DDep: String;
  Ocor: Integer;
  Orig: Integer;
begin
  if QrLotesPrr.RecNo <> QrLotesPrr.RecordCount then
    Geral.MB_Aviso('Somente a �ltima prorroga��o pode ser exclu�da!')
  else begin
    if Geral.MB_Pergunta('Confirma a exclus�o da prorroga��o?') = ID_YES then
    begin
      Ocor := QrLotesPrrOcorrencia.Value;
      Orig := Trunc(QrLotesPrrData1.Value);
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM lotesprr WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrLotesPrrControle.Value;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenLotesPrr(0);
      QrLotesPrr.Last;
      Dias := Trunc(Int(QrLotesPrrData3.Value)-Int(QrLotesPrrData1.Value));
      if QrLotesPrrData3.Value < 2 then
        DDep := FormatDateTime(VAR_FORMATDATE, Orig)
      else
        DDep := FormatDateTime(VAR_FORMATDATE, QrLotesPrrData3.Value);
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   DDeposito=:P0, ProrrVz=:P1, ');
      Dmod.QrUpd.SQL.Add('ProrrDd=:P2 WHERE Controle=:P3');
      Dmod.QrUpd.Params[0].AsString  := DDep;
      Dmod.QrUpd.Params[1].AsInteger := QrLotesPrr.RecordCount;
      Dmod.QrUpd.Params[2].AsInteger := Dias;
      //
      Dmod.QrUpd.Params[3].AsInteger := QrPesqControle.Value;
      Dmod.QrUpd.ExecSQL;
      //
      FLotesIts := QrPesqControle.Value;
      FOcorreu  := QrOcorreuCodigo.Value;
      FLotesPrr := QrLotesPrrControle.Value;
      //
      if Geral.MB_Pergunta('Deseja excluir tamb�m a ocorr�ncia da prorroga��o exclu�da?') = ID_YES then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM ocorreu WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := Ocor;
        Dmod.QrUpd.ExecSQL;
        QrOcorreu.Next;
        //
        ReopenOcorreu(QrOcorreuCodigo.Value);
      end;
      Pesquisa(True);
      ReabrirTabelas();
    end;
  end;
end;

procedure TFmDuplicatas2.ConfiguraPr(Data: TDateTime);
begin
  QrLocPr.Close;
  QrLocPr.Params[0].AsInteger := QrPesqControle.Value;
  QrLocPr.Open;
  TPDataN.MinDate := 0;
  if QrLocPr.RecordCount > 0 then
  begin
    TPDataN.Date := Int(QrLocPrData3.Value);
    TPDataO.Date := Int(QrLocPrData3.Value);
    TPDataI.Date := Int(QrLocPrData1.Value);
  end else begin
    TPDataN.Date := Int(QrPesqVencto.Value);
    TPDataO.Date := Int(QrPesqVencto.Value);
    TPDataI.Date := Int(QrPesqVencto.Value);
  end;
  EdTaxaPror.Text := Geral.FFT(
    Dmod.ObtemTaxaDeCompraCliente(QrPesqCliente.Value), 6, siPositivo);
  EdValorBase.Text := Geral.FFT(QrPesqValor.Value, 2, siPositivo);
  // Permitir adiantamento
  //TPDataN.MinDate := TPDataO.Date;
  if Int(Data) > TPDataN.Date then TPDataN.Date := Int(Data);
  TPdataN.SetFocus;
  CalculaJurosPrr;
end;

procedure TFmDuplicatas2.CkEIniClick(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.CkHistoricoClick(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.TPEIniChange(Sender: TObject);
begin
  if CkEIni.Checked then FecharTabelas;
end;

procedure TFmDuplicatas2.CkEFimClick(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.TPEFimChange(Sender: TObject);
begin
  if CkEFim.Checked then FecharTabelas;
end;

procedure TFmDuplicatas2.CkVIniClick(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.TPVIniChange(Sender: TObject);
begin
  if CkVIni.Checked then FecharTabelas;
end;

procedure TFmDuplicatas2.CkVFimClick(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.TPVFimChange(Sender: TObject);
begin
  if CkVFim.Checked then FecharTabelas;
end;

procedure TFmDuplicatas2.Recibo1Click(Sender: TObject);
var
  Pagamento, Texto: String;
begin
  Pagamento := MLAGeral.NomeStatusPgto5(QrPesqQuitado.Value,
    QrPesqDDeposito.Value, Date, '', False, QrPesqDevolucao.Value,
    QrPesqQuitado.Value, QrPesqProrrVz.Value, QrPesqProrrDd.Value, True, False);
  Texto := Pagamento + ' da duplicata ' + QrPesqDuplicata.Value +
    ' emitida por '+ QrPesqEmitente.Value + ' em '+FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqEmissao.Value);
  //
  //if QrLctDebito.Value > 0 then
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value,
    QrADupPgsPago.Value, 0, 0, 'DUP-'+FormatFloat('000',
    QrADupPgsControle.Value), Texto, '', '', QrADupPgsData.Value, 0);
end;

procedure TFmDuplicatas2.QrVctoCalcFields(DataSet: TDataSet);
(*var
  Cliente: Integer;
  Juro: Double;*)
begin
  if QrVctoSALDO_DESATUALIZADO.Value <> 0 then
    QrVctoPRAZO_MEDIO.Value := QrVctoFATOR_VD.Value /
    QrVctoSALDO_DESATUALIZADO.Value
  else QrVctoPRAZO_MEDIO.Value := 0;
  //
  (*Cliente := Geral.IMV(EdCliente.Text);
  if Cliente = 0 then
  begin
    QrVctoJURO_CLI.Value   := 0;
    DBEdTaxa.Visible := False;
  end else begin
    QrVctoJURO_CLI.Value   := Dmod.ObtemTaxaDeCompraCliente(Cliente);
    DBEdTaxa.Visible       := True;
  end;*)
end;

procedure TFmDuplicatas2.BtReabreClick(Sender: TObject);
begin
  ReabrirTabelas;
end;

procedure TFmDuplicatas2.EdColigadoChange(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.CondicoesExtras(Query: TmySQLQuery; Coligado: Integer);
begin
  case CkRepassado.Value of
    0:
    begin
      Geral.MB_Erro('Defina o status de repassado!');
      Exit;
    end;
    1:
    begin
      if Coligado = 0 then Query.SQL.Add('AND li.Repassado = 1')
      else begin
        Query.SQL.Add('AND (re.Coligado = ' + Geral.FF0(Coligado)+
        ' AND li.Repassado = 1)')
      end;
    end;
    2: Query.SQL.Add('AND li.Repassado = 0');
    3:
    begin
      if Coligado <> 0 then
      begin
        Query.SQL.Add('AND (li.Repassado = 0 OR (');
        Query.SQL.Add('li.Repassado=1 AND re.Coligado = '+
        Geral.FF0(Coligado)+'))');
      end;
    end;
  end;
  case CkStatus.Value of
    // Nada
    0: Query.SQL.Add('AND li.Quitado=-1000');
    // Vencido
    1: Query.SQL.Add('AND li.Quitado in (-1,0,1) AND li.DDeposito < CURRENT_DATE');
    // Aberto
    2: Query.SQL.Add('AND li.Quitado in (-1,0,1) AND li.DDeposito >= CURRENT_DATE');
    // Vencido + Aberto
    3: Query.SQL.Add('AND li.Quitado in (-1,0,1)');
    // Quitado
    4: Query.SQL.Add('AND li.Quitado in (2,3)');
    5: // Vencido + Quitado
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito < CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (2,3)))');
    end;
    6: // Aberto + Quitado
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito >= CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (2,3)))');
    end;
    // Vencido + Aberto + Quitado
    7: Query.SQL.Add('AND li.Quitado in (-1,0,1,2,3)');
    // Baixado
    8: Query.SQL.Add('AND li.Quitado =-2');
    9: // Vencido + Baixado
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito < CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado =-2))');
    end;
    10: // Aberto + Baixado
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito >= CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado =-2))');
    end;
    // Vencido + Aberto + Baixado
    11: Query.SQL.Add('AND li.Quitado in (-2,-1,0,1)');
    // Quitado
    12: Query.SQL.Add('AND li.Quitado in (-2,2,3)');
    13: // Vencido + Quitado + Baixado
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito < CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-2,2,3)))');
    end;
    14: // Aberto + Quitado + Baixado
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito >= CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-2,2,3)))');
    end;
    // Vencido + Aberto + Quitado
    15: Query.SQL.Add('AND li.Quitado in (-2,-1,0,1,2,3)');
    16: Query.SQL.Add('AND li.Quitado =-3');
    17: // Vencido + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito < CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado =-3))');
    end;
    18: // Aberto + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito >= CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado =-3))');
    end;
    // Vencido + Aberto + Moroso
    19: Query.SQL.Add('AND li.Quitado in (-3,-1,0,1)');
    // Quitado
    20: Query.SQL.Add('AND li.Quitado in (-3,2,3)');
    21: // Vencido + Quitado + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito < CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-3,2,3)))');
    end;
    22: // Aberto + Quitado + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito >= CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-3,2,3)))');
    end;
    // Vencido + Aberto + Quitado
    23: Query.SQL.Add('AND li.Quitado in (-3,-1,0,1,2,3)');
    24: Query.SQL.Add('AND li.Quitado in (-3,-2)');
    25: // Vencido + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito < CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-3,-2)))');
    end;
    26: // Aberto + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito >= CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-3,-2)))');
    end;
    // Vencido + Aberto + Baixado + Moroso
    27: Query.SQL.Add('AND li.Quitado in (-3,-2,-1,0,1)');
    // Quitado
    28: Query.SQL.Add('AND li.Quitado in (-3,-2,2,3)');
    29: // Vencido + Quitado + Baixado + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito < CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-3,-2,2,3)))');
    end;
    30: // Aberto + Quitado + Baixado + Moroso
    begin
      Query.SQL.Add('AND ((li.Quitado in (-1,0,1)) ');
      Query.SQL.Add('  AND (li.DDeposito >= CURRENT_DATE) ');
      Query.SQL.Add('OR (li.Quitado in (-3,-2,2,3)))');
    end;
    // Vencido + Aberto + Quitado + Baixado + Moroso
    31: Query.SQL.Add('AND li.Quitado in (-3,-2,-1,0,1,2,3)');
  end;
  // Parei aqui
end;

procedure TFmDuplicatas2.ConfirmaOcorPg;
var
  OcorrPg: Integer;
begin
  OcorrPg := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'OcorrPG', 'OcorrPG', 'Codigo');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO ocorrpg SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2, LotePg=:P3');
  Dmod.QrUpd.SQL.Add(', Ocorreu=:Pa, Codigo=:Pb');
  Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPPagto6.Date);
  Dmod.QrUpd.Params[01].AsFloat   := Geral.DMV(EdJuros6.Text);
  Dmod.QrUpd.Params[02].AsFloat   := Geral.DMV(EdPago6.Text);
  Dmod.QrUpd.Params[03].AsInteger := 0;
  //
  Dmod.QrUpd.Params[04].AsInteger := QrOcorreuCodigo.Value;
  Dmod.QrUpd.Params[05].AsInteger := OcorrPg;
  Dmod.QrUpd.ExecSQL;
  //
  CalculaPagtoOcorrPg(TPPagto6.Date, QrOcorreuCodigo.Value);
  ///
  FOcorP := QrOcorreuCodigo.Value;
  MostraEdicao(0, CO_TRAVADO);
  FOcorP := OcorrPg;
  ReopenOcorreu(QrOcorreuCodigo.Value);
end;

procedure TFmDuplicatas2.CalculaPagtoOcorrPg(Quitacao: TDateTime; OcorrPg: Integer);
var
  Sit: Integer;
begin
  QrSumOc.Close;
  QrSumOc.Params[0].AsInteger := OcorrPg;
  QrSumOc.Open;
   //Errado corrigido em 09/08/2012 
  {if QrSumOcPago.Value < 0.01 then Sit := 0 else if QrSumOcPago.Value <
    (QrOcorreuValor.Value + ==>QrOcorreuTaxaV.Value<==)-0.009 then}
  //if QrSumOcPago.Value < 0.01 then
  Sit := 0;
  if QrSumOc.RecordCount > 0 then
  begin
    if QrSumOcPago.Value < (QrOcorreuValor.Value + QrSumOcJuros.Value)-0.009 then
      Sit := 1
    else
      Sit := 2;
  end;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('TaxaV=:P0, Pago=:P1, Status=:P2, ');
  Dmod.QrUpd.SQL.Add('Data3=:P3 WHERE Codigo=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := QrSumOcJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumOcPago.Value;
  Dmod.QrUpd.Params[02].AsInteger := Sit;
  Dmod.QrUpd.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Quitacao);
  Dmod.QrUpd.Params[04].AsInteger := OcorrPg;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TFmDuplicatas2.ConfiguraPgOc;
var
  ValorBase: Double;
begin
  QrLocOc.Close;
  QrLocOc.Params[0].AsInteger := QrOcorreuCodigo.Value;
  QrLocOc.Open;
  TPPagto6.MinDate := 0;
  if QrLocOc.RecordCount > 0 then
  begin
    TPPagto6.Date     := Int(QrLocOcData.Value);
    TPDataBase6.Date  := Int(QrLocOcData.Value);
  end else begin
    TPPagto6.Date    := Int(QrOcorreuDataO.Value);
    TPDataBase6.Date := Int(QrOcorreuDataO.Value);
  end;
  EdJurosBase6.Text := Geral.FFT(
    Dmod.ObtemTaxaDeCompraCliente(QrPesqCliente.Value), 6, siPositivo);
  ValorBase := QrOcorreuValor.Value + QrOcorreuTaxaV.Value - QrOcorreuPago.Value;
  EdValorBase6.Text := Geral.FFT(ValorBase, 2, siNegativo);
  TPPagto6.MinDate := TPPagto6.Date;
  if Date > TPPagto6.MinDate then TPPagto6.Date := Int(Date);
  TPPagto6.SetFocus;
end;

procedure TFmDuplicatas2.CalculaJurosOcor;
var
  Prazo: Integer;
  Taxa, Juros, Valor: Double;
begin
  Juros := 0;
  Prazo := Trunc(Int(TPPagto6.Date) - Int(TPDataBase6.Date));
  if Prazo > 0 then
  begin
    Taxa  := Geral.DMV(EdJurosBase6.Text);
    Juros := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(Taxa, Prazo);
  end;
  Valor := Geral.DMV(EdValorBase6.Text);
  EdJurosPeriodo6.Text := Geral.FFT(Juros, 6, siPositivo);
  EdJuros6.Text := Geral.FFT(Juros * Valor / 100, 2, siPositivo);
end;

procedure TFmDuplicatas2.CalculaAPagarOcor;
var
  Base, Juro: Double;
begin
  Base := Geral.DMV(EdValorBase6.Text);
  Juro := Geral.DMV(EdJuros6.Text);
  //
  EdAPagar6.Text := Geral.FFT(Base+Juro, 2, siNegativo);
end;

procedure TFmDuplicatas2.IncluiPagamento1Click(Sender: TObject);
begin
  MostraEdicao(5, CO_INCLUSAO);
end;

procedure TFmDuplicatas2.Excluipagamento1Click(Sender: TObject);
var
  Ocorreu: Integer;
begin
  QrLastOcor.Close;
  QrLastOcor.Params[0].AsInteger := QrOcorrPgOcorreu.Value;
  QrLastOcor.Open;
  if (QrLastOcorData.Value > QrOcorrPgData.Value)
  or ((QrLastOcorData.Value = QrOcorrPgData.Value)
  and (QrLastOcorCodigo.Value > QrOcorrPgCodigo.Value))
  then
     Geral.MB_Erro('Somente o �ltimo pagamento pode ser exclu�do!')
  else begin
    if Geral.MB_Pergunta('Confirma a exclus�o deste pagamento?') = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        Ocorreu := QrOcorrPgOcorreu.Value;
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM ocorrpg WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrOcorrPgCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        QrLastOcor.Close;
        QrLastOcor.Params[0].AsInteger := Ocorreu;
        QrLastOcor.Open;
        CalculaPagtoOcorrPg(QrLastOcorData.Value, Ocorreu);
        //
        QrOcorrPg.Next;
        FOcorP := QrOcorrPgCodigo.Value;
        ReopenOcorreu(QrOcorreuCodigo.Value);
        Screen.Cursor := crDefault;
      except
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmDuplicatas2.ReopenOcorrPg;
begin
  QrOcorrPg.Close;
  QrOcorrPg.Params[0].AsInteger := QrOcorreuCodigo.Value;
  QrOcorrPg.Open;
  //
  QrOcorrPg.Locate('Codigo', FOcorrPg, []);
end;

procedure TFmDuplicatas2.EdValorBase6Change(Sender: TObject);
begin
  CalculaAPagarOcor;
end;

procedure TFmDuplicatas2.EdJurosBase6Change(Sender: TObject);
begin
  CalculaJurosOcor;
  CalculaAPagarOcor;
end;

procedure TFmDuplicatas2.TPPagto6Change(Sender: TObject);
begin
  CalculaJurosOcor;
end;

procedure TFmDuplicatas2.EdJuros6Change(Sender: TObject);
var
  VBase, Juros: Double;
begin
  VBase := Geral.DMV(EdValorBase6.Text);
  Juros := Geral.DMV(EdJuros6.Text);
  EdAPagar6.Text := Geral.FFT(VBase+Juros, 2, siNegativo);
end;

procedure TFmDuplicatas2.EdAPagar6Change(Sender: TObject);
begin
  EdPago6.Text := EdAPagar6.Text;
end;

procedure TFmDuplicatas2.Recibodepagamento1Click(Sender: TObject);
var
  Pagamento, Texto: String;
begin
  Pagamento := MLAGeral.NomeStatusPgto6(QrOcorreuATUALIZADO.Value,
    QrOcorrPg.RecNo)+' da ocorr�ncia "'+ QrOcorreuNOMEOCORRENCIA.Value+
    '" cobrada sobre';
  Texto := Pagamento + ' a duplicata n� ' + QrPesqDuplicata.Value +
    ' emitida por '+ QrPesqEmitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqVencto.Value)+'. A quita��o desta '+
    'ocorr�ncia n�o quita a duplicata';
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value,
    QrOcorrPgPago.Value, 0, 0, 'DUO-'+FormatFloat('000',
    QrOcorrPgCodigo.Value), Texto, '', '', QrOcorrPgData.Value, 0);
end;

procedure TFmDuplicatas2.QrOcorreuCalcFields(DataSet: TDataSet);
var
  Cliente: Integer;
begin
  if QrOcorreuCLIENTELOTE.Value > 0 then
    Cliente := QrOcorreuCLIENTELOTE.Value else
    Cliente := QrOcorreuCliente.Value;
  //
  QrOcorreuSALDO.Value := QrOcorreuValor.Value + QrOcorreuTaxaV.Value - QrOcorreuPago.Value;
  //
  if CkAtualiza.Checked then
  begin
    QrOcorreuATUALIZADO.Value := DMod.ObtemValorAtualizado(
      Cliente, 1, QrOcorreuDataO.Value, Date, QrOcorreuData3.Value,
      QrOcorreuValor.Value, QrOcorreuTaxaV.Value, 0 (*Desco*),
      QrOcorreuPago.Value, (*QrOcorreuTaxaP.Value*)0, (*False*)True);
    QrOcorreuATZ_TEXTO.Value := Geral.FFT(QrOcorreuATUALIZADO.Value, 2, siNegativo);
  end else begin
    QrOcorreuATUALIZADO.Value := QrOcorreuSALDO.Value;
    QrOcorreuATZ_TEXTO.Value := 'n�o calculado';
  end;
  //
  QrOcorreuSEQ.Value := QrOcorreu.RecNo;
  //
end;

procedure TFmDuplicatas2.QrOcorreuAfterScroll(DataSet: TDataSet);
begin
  ReopenOcorrPg;
end;

procedure TFmDuplicatas2.QrOcorreuBeforeClose(DataSet: TDataSet);
begin
  QrOcorrPg.Close;
end;

procedure TFmDuplicatas2.BtQuitacaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMQuitacao, BtQuitacao);
end;

procedure TFmDuplicatas2.Quitadocumento1Click(Sender: TObject);
var
  ValQuit: Double;
  Localiz, ADupPgs, OcorrPg, Ocorreu: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Ocorreu := QrOcorreuCodigo.Value;
    //PesqCtrl := QrPesqControle.Value;
    ValQuit := 0;
    if QrPesqSALDO_ATUALIZADO.Value > 0 then
    begin
      ValQuit := QrPesqSALDO_ATUALIZADO.Value;
      ADupPgs := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
            'ADupPgs', 'ADupPgs', 'Controle');
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO aduppgs SET AlterWeb=1, ');
      Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Desco=:P2, Pago=:P3, LotePg=:P4');
      Dmod.QrUpd.SQL.Add(', LotesIts=:Pa, Controle=:Pb');
      Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpd.Params[01].AsFloat   := QrPesqSALDO_ATUALIZADO.Value-QrPesqSALDO_DESATUALIZ.Value;
      Dmod.QrUpd.Params[02].AsFloat   := 0;
      Dmod.QrUpd.Params[03].AsFloat   := QrPesqSALDO_ATUALIZADO.Value;
      Dmod.QrUpd.Params[04].AsInteger := 0;
      //
      Dmod.QrUpd.Params[05].AsInteger := QrPesqControle.Value;
      Dmod.QrUpd.Params[06].AsInteger := ADupPgs;
      Dmod.QrUpd.ExecSQL;
      //
      FAdupPgs := ADupPgs;
      CalculaPagamento(QrPesqControle.Value);
      //
      ReabrirTabelas;
    end;
    QrOcorreu.First;
    while not QrOcorreu.Eof do
    begin
      if QrOcorreuATUALIZADO.Value > 0 then
      begin
        ValQuit := ValQuit + QrOcorreuATUALIZADO.Value;
        OcorrPg := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
              'OcorrPG', 'OcorrPG', 'Codigo');
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO ocorrpg SET AlterWeb=1, ');
        Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2, LotePg=:P3');
        Dmod.QrUpd.SQL.Add(', Ocorreu=:Pa, Codigo=:Pb');
        Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpd.Params[01].AsFloat   := QrOcorreuATUALIZADO.Value-QrOcorreuSALDO.Value;
        Dmod.QrUpd.Params[02].AsFloat   := QrOcorreuATUALIZADO.Value;
        Dmod.QrUpd.Params[03].AsInteger := 0;
        //
        Dmod.QrUpd.Params[04].AsInteger := QrOcorreuCodigo.Value;
        Dmod.QrUpd.Params[05].AsInteger := OcorrPg;
        Dmod.QrUpd.ExecSQL;
        //
        CalculaPagtoOcorrPg(Date, QrOcorreuCodigo.Value);
      end;
      QrOcorreu.Next;
    end;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   ValQuit=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.Params[0].AsFloat   := ValQuit;
    Dmod.QrUpd.Params[1].AsInteger := QrPesqControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Localiz := QrPesqControle.Value;
    QrPesq.Close;
    QrPesq.Open;
    QrPesq.Locate('Controle', Localiz, []);
    ReopenOcorreu(Ocorreu);
    if Geral.MB_Pergunta('Deseja imprimir um recibo com a soma dos recebimentos efetuados?') = ID_YES then
      Imprimerecibodequitao1Click(Self);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmDuplicatas2.ImprimecartadeCancelamentodeProtesto1Click(
  Sender: TObject);
begin
  Application.CreateForm(TFmDuplicCancProt, FmDuplicCancProt);
  FmDuplicCancProt.ReopenLotesIProt(QrPesqControle.Value);
  FmDuplicCancProt.EdComarca.Text := FmDuplicCancProt.QrLotesIProtComarca.Value;
  FmDuplicCancProt.EdNumDistrib.Text := FmDuplicCancProt.QrLotesIProtNumDistrib.Value;
  FmDuplicCancProt.EdLivro.Text := FmDuplicCancProt.QrLotesIProtLivro.Value;
  FmDuplicCancProt.EdFolhas.Text := FmDuplicCancProt.QrLotesIProtFolhas.Value;
  FmDuplicCancProt.ShowModal;
  FmDuplicCancProt.Destroy;
end;

procedure TFmDuplicatas2.ImprimecartadeCancelamentodeProtesto2Click(
  Sender: TObject);
begin
  ImprimecartadeCancelamentodeProtesto1Click(Self);
end;

procedure TFmDuplicatas2.Imprimerecibodequitao1Click(Sender: TObject);
var
  Pagamento, Texto, Liga: String;
begin
  Pagamento := 'Quita��o de d�bitos gerados at� esta data relativos a';
  Texto := Pagamento + ' duplicata n� ' + QrPesqDuplicata.Value+
    ' emitida por '+ QrPesqEmitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqVencto.Value);
  if QrOcorreu.RecordCount > 0 then
  begin
    if QrOcorreu.RecordCount = 1 then Texto := Texto + ' e a ocorr�ncia'
    else Texto := Texto + ' e as ocorr�ncias';
    QrOcorreu.First;
    Liga := ' ';
    while not QrOcorreu.Eof do
    begin
      Texto := Texto + Liga + '"'+QrOcorreuNOMEOCORRENCIA.Value + '"';
      if QrOcorreu.RecNo+1= QrOcorreu.RecordCount then Liga := ' e '
      else liga := ', ';
      QrOcorreu.Next;
    end;
  end;
  //
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value,
    QrPesqValQuit.Value, 0, 0, 'DUX-'+FormatFloat('000000',
    QrPesqControle.Value), Texto, '', '', Date, 0);
end;

procedure TFmDuplicatas2.RGOrdem1Click(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.RGOrdem2Click(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.RGOrdem3Click(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.RGOrdem4Click(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.SbBorderoClick(Sender: TObject);
begin
  FmPrincipal.CriaFormLotes(1, 0, QrPesqCodigo.Value, QrPesqControle.Value);
end;

procedure TFmDuplicatas2.SbImprimeClick(Sender: TObject);
var
  Valor: Double;  
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmDuplicatas2.BtImprimeClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  ReabrirTabelas;
  QrPesq2.Close;
  QrPesq2.SQL := QrPesq.SQL;
  QrPesq2.Open;
  if QrPesq.State = dsBrowse  then
  begin
    if CkOcorrencias.Checked = True then
      MyObjects.frxMostra(frxPesq2, 'Relat�rio de duplicatas com ocorr�ncias')
    else
      MyObjects.frxMostra(frxPesq1, 'Relat�rio de duplicatas');
  end;
end;

procedure TFmDuplicatas2.QrPesq2CalcFields(DataSet: TDataSet);
var
  Taxas: MyArrayR07;
  Ano, Mes: Integer;
  Ocorrencias, OcorAtualiz, TotalAtualiz: Double;
  DtUltimoPg: TDateTime;
begin
  QrPesq2CONTA.Value := 1;
  QrPesq2DDeposito_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE3, QrPesq2DDeposito.Value);
  QrPesq2Valor_TXT.Value :=
    FormatFloat('#,###,##0.00', QrPesq2Valor.Value);
  //
  QrPesq2SALDO_DESATUALIZ.Value := QrPesq2Valor.Value + QrPesq2TotalJr.Value -
  QrPesq2TotalDs.Value - QrPesq2TotalPg.Value;
  //
  QrPesq2NOMESTATUS.Value := MLAGeral.NomeStatusPgto2(QrPesq2Quitado.Value,
    QrPesq2DDeposito.Value, Date, QrPesq2Data3.Value, QrPesq2Repassado.Value);
  //
  {
  QrPesq2SALDO_ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrPesq2Cliente.Value, QrPesq2Quitado.Value, QrPesq2Vencto.Value, Date,
    QrPesq2Data3.Value, QrPesq2Valor.Value, QrPesq2TotalJr.Value, QrPesq2TotalDs.Value,
    QrPesq2TotalPg.Value, 0, True);
  }
  QrLocPg.Close;
  QrLocPg.Params[0].AsInteger := QrPesq2Controle.Value;
  QrLocPg.Open;
  if QrLocPg.RecordCount > 0 then
    DtUltimoPg := QrLocPgData.Value
  else
    DtUltimoPg := QrPesq2DDeposito.Value;
  //
  QrPesq2SALDO_ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrPesq2Cliente.Value, QrPesq2Quitado.Value, QrPesq2Vencto.Value, Date,
    DtUltimoPg, QrPesq2Valor.Value, QrPesq2TotalJr.Value, QrPesq2TotalDs.Value,
    QrPesq2TotalPg.Value, 0, True);
  //
  //////////////////////////////////////////////////////////////////////////////
  QrPesq2CNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrPesq2CPF.Value);
  //
  Taxas := DMod.ObtemTaxaDeCompraRealizada(QrPesq2Controle.Value);
  QrPesq2TAXA_COMPRA.Value := QrPesq2TxaCompra.Value + Taxas[0];
  //
  //
  Ano := Trunc(QrPesq2PERIODO.Value) div 100;
  Mes := Trunc(QrPesq2PERIODO.Value) mod 100;
  QrPesq2MES_TXT.Value := FormatDateTime(VAR_FORMATDATE7,
    EncodeDate(Ano, Mes, 1));
    //
  Ano := Trunc(QrPesq2PERIODO3.Value) div 100;
  Mes := Trunc(QrPesq2PERIODO3.Value) mod 100;
  if (Ano = 0) and (Mes = 0) then QrPesq2MEQ_TXT.Value := 'N�O QUITADO' else
  QrPesq2MEQ_TXT.Value := FormatDateTime(VAR_FORMATDATE7,
    EncodeDate(Ano, Mes, 1));
////////////////////////////////////////////////////////////////////////////////
  if CkOcorrencias.Checked = True then
  begin
    Ocorrencias  := 0;
    OcorAtualiz  := 0;
    TotalAtualiz := 0;
    QrOcu1.Close;
    QrOcu1.Params[00].AsInteger := QrPesq2Controle.Value;
    QrOcu1.Open;
    while not QrOcu1.Eof do
    begin
      Ocorrencias  := Ocorrencias  + QrOcu1SALDO.Value;
      OcorAtualiz  := OcorAtualiz  + QrOcu1ATUALIZADO.Value;
      TotalAtualiz := TotalAtualiz + QrOcu1ATUALIZADO.Value;
      QrOcu1.Next;
    end;
    QrPesq2OCORRENCIAS.Value  := Ocorrencias;
    QrPesq2OCORATUALIZ.Value  := OcorAtualiz;
    QrPesq2TOTALATUALIZ.Value := TotalAtualiz + QrPesq2SALDO_ATUALIZADO.Value;
  end else begin
    QrPesq2OCORRENCIAS.Value  := 0;
    QrPesq2OCORATUALIZ.Value  := 0;
    QrPesq2TOTALATUALIZ.Value := 0;
  end;
  //
  QrPesq2DATA3TXT.Value := Geral.FDT(QrPesq2Data3.Value, 3);
  //
  QrPesq2TEL1_TXT.Value := Geral.FormataTelefone_TT_Curto(QrPesq2Tel1.Value);
end;

procedure TFmDuplicatas2.GradeItensTitleClick(Column: TColumn);
var
  Antigo: String;
begin
  Antigo := FOrdC1;
  FOrdC1 := Column.FieldName;
  FOrdC2 := '';
  FOrdS2 := '';
  FOrdC3 := '';
  FOrdS3 := '';
  if FOrdC1 = 'SALDO_ATUALIZADO' then
  begin
    Geral.MB_Aviso('N�o � poss�vel ordenar pelo saldo atualizado'+
      ' pois a atualiza��o � feita ap�s a ordena��o!');
    FOrdC1 := 'SALDO_DESATUALIZ';
  end else if FOrdC1 = 'NOMESTATUS' then
  begin
    FOrdC1 := 'Quitado';
    FOrdC2 := 'Vencto';
    FOrdC3 := 'Repassado';
  end;
  if Antigo = FOrdC1 then
  FOrdS1 := MLAGeral.InverteOrdemAsc(FOrdS1);
  ReabrirTabelas;
end;

procedure TFmDuplicatas2.BtRefreshClick(Sender: TObject);
begin
  QrPesq.First;
  while not QrPesq.Eof do
  begin
    CalculaPagamento(QrPesqControle.Value);
    QrPesq.Next;
  end;
  ReabrirTabelas;
end;

procedure TFmDuplicatas2.QrOcu1CalcFields(DataSet: TDataSet);
var
  Cliente: Integer;
begin
  if QrOcu1CLIENTELOTE.Value > 0 then
    Cliente := QrOcu1CLIENTELOTE.Value else
    Cliente := QrOcu1Cliente.Value;
  //
  QrOcu1SALDO.Value := QrOcu1Valor.Value + QrOcu1TaxaV.Value - QrOcu1Pago.Value;
  //
  QrOcu1ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    Cliente, 1, QrOcu1DataO.Value, Date, QrOcu1Data3.Value,
    QrOcu1Valor.Value, QrOcu1TaxaV.Value, 0 (*Desco*),
    QrOcu1Pago.Value, (*QrOcu1TaxaP.Value*)0, (*False*)True);
end;

procedure TFmDuplicatas2.QrOcu2CalcFields(DataSet: TDataSet);
var
  Cliente: Integer;
begin
  if QrOcu2CLIENTELOTE.Value > 0 then
    Cliente := QrOcu2CLIENTELOTE.Value else
    Cliente := QrOcu2Cliente.Value;
  //
  QrOcu2SALDO.Value := QrOcu2Valor.Value + QrOcu2TaxaV.Value - QrOcu2Pago.Value;
  //
  QrOcu2ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    Cliente, 1, QrOcu2DataO.Value, Date, QrOcu2Data3.Value,
    QrOcu2Valor.Value, QrOcu2TaxaV.Value, 0 (*Desco*),
    QrOcu2Pago.Value, (*QrOcu2TaxaP.Value*)0, (*False*)True);
end;

procedure TFmDuplicatas2.QrPesqBeforeClose(DataSet: TDataSet);
begin
  QrOcorreu.Close;
  QrADupIts.Close;
  QrLotesPrr.Close;
  QrADupPgs.Close;
  QrCNAB240.Close;
  //
  SbImprime.Enabled        := False;
  BtImprime.Enabled        := False;
  SbBordero.Enabled        := False;
  BtCuidado.Enabled        := False;
  BtPagtoDuvida.Enabled    := False;
  BtZAZ.Enabled            := False;
  BtStatusManual.Enabled   := False;
  BtDuplicataOcorr.Enabled := False;
  BtQuitacao.Enabled       := False;
  BtHistCNAB.Enabled       := False;
end;

procedure TFmDuplicatas2.PageControl1Change(Sender: TObject);
begin
  BtDuplicataOcorr.Visible := False;
  BtCuidado.Visible        := False;
  BtPagtoDuvida.Visible    := False;
  BtZAZ.Visible            := False;
  BtHistCNAB.Visible       := False;
  case PageControl1.ActivePageIndex of
    0:
    begin
      BtCuidado.Visible := True;
      BtPagtoDuvida.Visible := True;
      BtZAZ.Visible := True;
    end;
    1:
    begin
      BtDuplicataOcorr.Visible := True;
    end;
    3:
    begin
      BtHistCNAB.Visible := true;
    end;
  end;
end;

procedure TFmDuplicatas2.InfoTempo(Tempo: TDateTime; Texto: String; Inicial: Boolean);
begin
  if Inicial then
  begin
    FTempo := Tempo;
    FUltim := Tempo;
    Memo1.Lines.Add('');
    Memo1.Lines.Add('==============================================================================');
    Memo1.Lines.Add('');
    Memo1.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss ', Tempo) +
      '- [ Total ] [Unit�ri] '+ Texto);
  end else begin
    Memo1.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss - ', Tempo)+
    FormatDateTime('nn:ss:zzz ', Tempo-FTempo) +
    FormatDateTime('nn:ss:zzz ', Tempo-FUltim) + Texto);
    FUltim := Tempo;
  end;
end;

procedure TFmDuplicatas2.EdDuplicataChange(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.EdEmitenteChange(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.EdHistoricoChange(Sender: TObject);
begin
  FecharTabelas;
end;

procedure TFmDuplicatas2.FecharTabelas;
begin
  QrPesq.Close;
  QrSoma.Close;
  QrVcto.Close;
end;

function TFmDuplicatas2.TextoSQLPesq(TabLote, TabLoteIts, Local: String): Integer;
var
  Coligado: Integer;
begin
  Result := 0;
  QrPesq.SQL.Add('SELECT MONTH(li.DDeposito)+YEAR(li.DDeposito)*100+0.00 PERIODO,');
  QrPesq.SQL.Add('MONTH(li.Data3)+YEAR(li.Data3)*100+0.00 PERIODO3, ');
  QrPesq.SQL.Add('lo.NF, od.Nome STATUS, li.Controle, li.Duplicata, li.Emissao, ');
  QrPesq.SQL.Add('li.DCompra, li.Valor, li.DDeposito, li.Emitente, li.CPF,');
  QrPesq.SQL.Add('lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg,');
  QrPesq.SQL.Add('li.Repassado, CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial');
  QrPesq.SQL.Add('ELSE cl.Nome END NOMECLIENTE, li.Vencto, li.Data3,');
  QrPesq.SQL.Add('li.TxaCompra, li.Devolucao, li.ProrrVz, li.ProrrDd, li.ValQuit, ');
  QrPesq.SQL.Add('li.Banco, li.Agencia, lo.Tipo + '+Local+' LOCALT, lo.Codigo, ');
  QrPesq.SQL.Add('(li.Valor + li.TotalJr - li.TotalDs - li.TotalPg) SALDO_DESATUALIZ, ');
  QrPesq.SQL.Add('sa.Tel1');
  //
  QrPesq.SQL.Add('FROM '+tabloteits+' li');
  QrPesq.SQL.Add('LEFT JOIN '+tablote+'     lo ON lo.Codigo=li.Codigo');
  QrPesq.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  QrPesq.SQL.Add('LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao');
  QrPesq.SQL.Add('LEFT JOIN repasits  ri ON ri.Origem=li.Controle');
  QrPesq.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  QrPesq.SQL.Add('LEFT JOIN sacados sa ON sa.CNPJ=li.CPF');
  QrPesq.SQL.Add('WHERE lo.Tipo=1');
  //////////////////////////////////////////////////////////////////////////////
  QrSoma.SQL.Add('SELECT SUM(li.Valor) Valor, ');
    //CASE WHEN li.Valor-li.TotalPg > 0 THEN ' +
    //'SUM(li.Valor-li.TotalPg) ELSE 0 END Principal, ');
  QrSoma.SQL.Add('SUM(IF(li.Valor-li.TotalPg > 0, li.Valor-li.TotalPg, 0)) Principal,');
  QrSoma.SQL.Add('SUM(li.Valor + li.TotalJr - li.TotalDs - li.TotalPg) SALDO_DESATUALIZADO');
  QrSoma.SQL.Add('FROM '+tabloteits+' li');
  QrSoma.SQL.Add('LEFT JOIN '+tablote+'     lo ON lo.Codigo=li.Codigo');
  QrSoma.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  QrSoma.SQL.Add('LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao');
  QrSoma.SQL.Add('LEFT JOIN repasits  ri ON ri.Origem=li.Controle');
  QrSoma.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  QrSoma.SQL.Add('WHERE lo.Tipo=1');
  //////////////////////////////////////////////////////////////////////////////
  QrVcto.SQL.Add('SELECT SUM(li.Valor) SOMA_VALOR,');
  QrVcto.SQL.Add('SUM(li.Valor + li.TotalJr - li.TotalDs - li.TotalPg) SALDO_DESATUALIZADO,');
  QrVcto.SQL.Add('SUM(CURDATE()-li.Vencto) SOMA_DIAS,');
  QrVcto.SQL.Add('SUM((CURDATE()-li.Vencto)*li.Valor) FATOR_VD');
  QrVcto.SQL.Add('FROM '+tabloteits+' li');
  QrVcto.SQL.Add('LEFT JOIN '+tablote+'     lo ON lo.Codigo=li.Codigo');
  QrVcto.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  QrVcto.SQL.Add('LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao');
  QrVcto.SQL.Add('LEFT JOIN repasits  ri ON ri.Origem=li.Controle');
  QrVcto.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  QrVcto.SQL.Add('WHERE lo.Tipo=1');
  QrVcto.SQL.Add('AND li.Vencto<CURDATE()');
  //////////////////////////////////////////////////////////////////////////////
  if FCliente <> 0 then
  begin
    QrPesq.SQL.Add('AND lo.Cliente=' + Geral.FF0(FCliente));
    QrSoma.SQL.Add('AND lo.Cliente=' + Geral.FF0(FCliente));
    QrVcto.SQL.Add('AND lo.Cliente=' + Geral.FF0(FCliente));
  end;
  if FEmitente <> '' then
  begin
    QrPesq.SQL.Add('AND li.Emitente LIKE "'+FEmitente+'"');
    QrSoma.SQL.Add('AND li.Emitente LIKE "'+FEmitente+'"');
    QrVcto.SQL.Add('AND li.Emitente LIKE "'+FEmitente+'"');
  end;
  if FCPF <> '' then
  begin
    QrPesq.SQL.Add('AND li.CPF = "'+FCPF+'"');
    QrSoma.SQL.Add('AND li.CPF = "'+FCPF+'"');
    QrVcto.SQL.Add('AND li.CPF = "'+FCPF+'"');
  end;
  if FDuplicata <> '' then
  begin
    QrPesq.SQL.Add('AND li.Duplicata = "'+FDuplicata+'"');
    QrSoma.SQL.Add('AND li.Duplicata = "'+FDuplicata+'"');
    QrVcto.SQL.Add('AND li.Duplicata = "'+FDuplicata+'"');
  end;
  if FControle <> 0 then
  begin
    QrPesq.SQL.Add('AND li.Controle = ' + Geral.FF0(FControle));
    QrSoma.SQL.Add('AND li.Controle = ' + Geral.FF0(FControle));
    QrVcto.SQL.Add('AND li.Controle = ' + Geral.FF0(FControle));
  end;
  if CkHistorico.Checked then
  begin
    QrPesq.SQL.Add('AND li.Devolucao = ' + Geral.FF0(EdHistorico.ValueVariant));
    QrSoma.SQL.Add('AND li.Devolucao = ' + Geral.FF0(EdHistorico.ValueVariant));
    QrVcto.SQL.Add('AND li.Devolucao = ' + Geral.FF0(EdHistorico.ValueVariant));
  end;
  //////////////////////////////////////////////////////////////////////////////
  QrPesq.SQL.Add(dmkPF.SQL_Periodo('AND li.Emissao ',
    TPEIni.Date, TPEFim.Date, CkEIni.Checked, CkEFim.Checked));
  QrPesq.SQL.Add(dmkPF.SQL_Periodo('AND li.DDeposito ',
    TPVIni.Date, TPVFim.Date, CkVIni.Checked, CkVFim.Checked));
  QrPesq.SQL.Add(dmkPF.SQL_Periodo('AND li.Data3 ',
    TPQIni.Date, TPQFim.Date, CkQIni.Checked, CkQFim.Checked));
  Coligado := Geral.IMV(EdColigado.Text);
  CondicoesExtras(QrPesq, Coligado);
  //////////////////////////////////////////////////////////////////////////////
  QrSoma.SQL.Add(dmkPF.SQL_Periodo('AND li.Emissao ',
    TPEIni.Date, TPEFim.Date, CkEIni.Checked, CkEFim.Checked));
  QrSoma.SQL.Add(dmkPF.SQL_Periodo('AND li.DDeposito ',
    TPVIni.Date, TPVFim.Date, CkVIni.Checked, CkVFim.Checked));
  QrSoma.SQL.Add(dmkPF.SQL_Periodo('AND li.Data3 ',
    TPQIni.Date, TPQFim.Date, CkQIni.Checked, CkQFim.Checked));
  CondicoesExtras(QrSoma, Coligado);
  //////////////////////////////////////////////////////////////////////////////
  QrVcto.SQL.Add(dmkPF.SQL_Periodo('AND li.Emissao ',
    TPEIni.Date, TPEFim.Date, CkEIni.Checked, CkEFim.Checked));
  QrVcto.SQL.Add(dmkPF.SQL_Periodo('AND li.DDeposito ',
    TPVIni.Date, TPVFim.Date, CkVIni.Checked, CkVFim.Checked));
  QrVcto.SQL.Add(dmkPF.SQL_Periodo('AND li.Data3 ',
    TPQIni.Date, TPQFim.Date, CkQIni.Checked, CkQFim.Checked));
  CondicoesExtras(QrVcto, Coligado);
end;

procedure TFmDuplicatas2.EdControleExit(Sender: TObject);
begin
  EdControle.Text := Geral.TFT_Null(EdControle.Text, 0, siPositivo);
end;

procedure TFmDuplicatas2.QrCNAB240CalcFields(DataSet: TDataSet);
var
  Envio: TEnvioCNAB;
begin
  case QrCNAB240Envio.Value of
    1: Envio := ecnabRemessa;
    2: Envio := ecnabRetorno;
    else Envio := ecnabIndefinido;
  end;
  QrCNAB240NOMEENVIO.Value := UBancos.CNAB240Envio(QrCNAB240Envio.Value);
  QrCNAB240NOMEMOVIMENTO.Value := UBancos.CNABTipoDeMovimento(1,
    Envio, QrCNAB240Movimento.Value, 0, False, '');
end;

procedure TFmDuplicatas2.QrClientesCalcFields(DataSet: TDataSet);
begin
  QrClientesTe1_TXT.Value := MLAGeral.FormataTelefone_TT_Curto(QrClientesTe1.Value);
end;

procedure TFmDuplicatas2.Gerenciardirio1Click(Sender: TObject);
var
  Texto: String;
begin
  Texto := QrPesqCPF.Value + ' - ' + QrPesqEmitente.Value;
  //
  FmPrincipal.MostraDiarioGer(0, QrPesqCliente.Value, QrPesqControle.Value, Texto);
end;

procedure TFmDuplicatas2.GradeItensDblClick(Sender: TObject);
var
  i: Integer;
begin
  if QrPesq.State = dsInactive then Exit;
  if QrPesq.RecordCount = 0 then Exit;
  QrSacados.Close;
  QrSacados.Params[0].AsString := Geral.SoNumero_TT(QrPesqCPF.Value);
  QrSacados.Open;
  //
  Application.CreateForm(TFmSacadosEdit, FmSacadosEdit);
  with FmSacadosEdit do
  begin
    LaTipo.Caption   := CO_ALTERACAO;
    FOrigem          := 'FmDuplicatas2';
    EdCNPJ.Text      := Geral.FormataCNPJ_TT(QrSacadosCNPJ.Value);
    EdSacado.Text    := QrSacadosNome.Value;
    EdRua.Text       := QrSacadosRua.Value;
    EdNumero.Text    := Geral.FF0(Trunc(QrSacadosNumero.Value));
    EdCompl.Text     := QrSacadosCompl.Value;
    EdBairro.Text    := QrSacadosBairro.Value;
    EdCidade.Text    := QrSacadosCidade.Value;
    EdCEP.Text       := Geral.FormataCEP_NT(QrSacadosCEP.Value);
    EdTel1.Text      := Geral.FormataTelefone_TT(QrSacadosTel1.Value);
    EdEmail.Text     := QrSacadosEmail.Value;
    EdRisco.Text     := Geral.FFT(QrSacadosRisco.Value, 2, siPositivo);
    //
    for i := 0 to CBUF.Items.Count-1 do
      if CBUF.Items[i] = QrSacadosUF.Value then CBUF.ItemIndex := i;
  end;
  FmSacadosEdit.ShowModal;
  FmSacadosEdit.Destroy;
end;

procedure TFmDuplicatas2.N0Statusautomtico1Click(Sender: TObject);
begin
  ForcaStatus(0);
end;

procedure TFmDuplicatas2.N1Forastatusprorrogado1Click(Sender: TObject);
begin
  ForcaStatus(-1);
end;

procedure TFmDuplicatas2.N2ForastatusBaixado1Click(Sender: TObject);
begin
  ForcaStatus(-2);
end;

procedure TFmDuplicatas2.N3ForastatusMoroso1Click(Sender: TObject);
begin
  ForcaStatus(-3);
end;

procedure TFmDuplicatas2.ForcaStatus(NovoStatus: Integer);
var
  Controle: Integer;
begin
  if QrPesq.State = dsBrowse then
  begin
    if QrPesq.RecordCount > 0 then
    begin
      Controle := QrPesqControle.Value;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lotesits SET Quitado=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
      Dmod.QrUpd.Params[0].AsInteger := NovoStatus;
      Dmod.QrUpd.Params[1].AsInteger := Controle;
      Dmod.QrUpd.ExecSQL;
      //
      QrPesq.Close;
      QrPesq.Open;
      QrPesq.Locate('Controle', Controle, []);
    end;
  end;
end;

procedure TFmDuplicatas2.BtHistCNABClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 3;
  //
  MyObjects.MostraPopUpDeBotao(PMHistCNAB, BtHistCNAB);
end;

procedure TFmDuplicatas2.Excluihistriciatual1Click(Sender: TObject);
begin
  UMyMod.SQLDel1(Dmod.QrUpd, QrCNAB240, 'cnab240', 'CNAB240I',
  QrCNAB240CNAB240I.Value, True, 'Confirma a exclus�o do item de hist�rico ' +
  ' selecionado?', False);
end;

procedure TFmDuplicatas2.Excluitodositensdehistricosdestaduplicata1Click(
  Sender: TObject);
begin
  UMyMod.SQLDel1(Dmod.QrUpd, QrCNAB240, 'cnab240', 'Controle',
  QrCNAB240Controle.Value, True, 'Confirma a exclus�o de todo hist�rico ' +
  ' da duplicata selecionada?', False);
end;

procedure TFmDuplicatas2.Extratodeduplicata1Click(Sender: TObject);
begin
  MyObjects.frxDefineDataSets(frxExtratoDuplicata, [
    frxDsADupIts,
    frxDsADupPgs,
    DmodG.frxDsDono,
    frxDsLotesPrr,
    frxDsOcorreu,
    frxDsPesq,
    frxDsOcorrPg
    ]);
  MyObjects.frxMostra(frxExtratoDuplicata, 'Extrato de Duplicata');
end;

procedure TFmDuplicatas2.QrCNAB240AfterOpen(DataSet: TDataSet);
begin
  BtHistCNAB.Enabled := Geral.IntToBool_0(QrCNAB240.RecordCount);
end;

procedure TFmDuplicatas2.frxExtratoDuplicataGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_CNPJ_CPF' then
  begin
    if Length(QrPesqCPF.Value) = 11 then
      Value := 'CPF:'
    else
      Value := 'CNPJ:';
  end
end;

procedure TFmDuplicatas2.frxPesq1GetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_CLIENTE' then
  begin
    if CBCliente.KeyValue = NULL then Value := ' ' else
    Value := CBCliente.Text;
  end else if VarName = 'VARF_TODOS' then
  begin
    if CBCliente.KeyValue = NULL then Value := 'TODOS CLIENTES' else
    Value := ' ';
  end else if VarName = 'VARF_PERIODO' then Value :=
    dmkPF.PeriodoImp(TPEIni.Date, TPEFim.Date, TPVIni.Date, TPVFim.Date,
      CkEIni.Checked, CkEFim.Checked, CkVIni.Checked, CkVFim.Checked,
      'Emiss�o:', 'Vencimento:')
  else if VarName = 'VARF_QTD_CHEQUES' then Value := QrPesq2.RecordCount
  else if VarName = 'VFR_LA1NOME' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Cliente: '+QrPesq2NOMECLIENTE.Value;
      1: Value := 'Dep�sito no dia  '+QrPesq2DDeposito_TXT.Value;
      2: Value := 'Dep�sito no m�s de '+QrPesq2MES_TXT.Value;
      3: Value := 'Emitido por: '+QrPesq2Emitente.Value;
      4: Value := 'CPF/CNPJ: '+QrPesq2CNPJ_TXT.Value;
      5: Value := 'Dia da quita��o: '+QrPesq2DATA3TXT.Value;
      6: Value := 'M�s da quita��o: '+QrPesq2MEQ_TXT.Value;
    end;
  end
  else if VarName = 'VFR_LA2NOME' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Cliente: '+QrPesq2NOMECLIENTE.Value;
      1: Value := 'Dep�sito no dia  '+QrPesq2DDeposito_TXT.Value;
      2: Value := 'Dep�sito no m�s de '+QrPesq2MES_TXT.Value;
      3: Value := 'Emitido por: '+QrPesq2Emitente.Value;
      4: Value := 'CPF/CNPJ: '+QrPesq2CNPJ_TXT.Value;
      5: Value := 'Dia da quita��o: '+QrPesq2DATA3TXT.Value;
      6: Value := 'M�s da quita��o: '+QrPesq2MEQ_TXT.Value;
    end;
  end
  else if VarName = 'VFR_LA3NOME' then
  begin
    case RGOrdem3.ItemIndex of
      0: Value := 'Cliente: '+QrPesq2NOMECLIENTE.Value;
      1: Value := 'Dep�sito no dia  '+QrPesq2DDeposito_TXT.Value;
      2: Value := 'Dep�sito no m�s de '+QrPesq2MES_TXT.Value;
      3: Value := 'Emitido por: '+QrPesq2Emitente.Value;
      4: Value := 'CPF/CNPJ: '+QrPesq2CNPJ_TXT.Value;
      5: Value := 'Dia da quita��o: '+QrPesq2DATA3TXT.Value;
      6: Value := 'M�s da quita��o: '+QrPesq2MEQ_TXT.Value;
    end;
  end
  else if VarName = 'VARF_SITUACOES' then
  begin
    Value := ' ';
    //if CkQuitado.Checked then Value := Value + CkQuitado.Caption;
    //if CkRepassado.Checked then Value := Value + '(incluindo repassados)';
    //if Trim(Value) = '' then Value := '(??????)';
  end
  else if VarName = 'VARF_FILTROS' then
  begin
    Value := '';
    if Geral.IMV(EdColigado.Text) <> 0 then Value := Value +
      'Coligado: '+CBColigado.Text+ '  ';
    if FEmitente <> '' then Value := Value + '  {Emitente: '+ FEmitente+'}';
    if FCPF <> '' then Value := Value + '  {CPF/CNPJ: '+
    Geral.FormataCNPJ_TT(FCPF)+'}';
    if Value <> '' then Value := 'FILTROS : '+Value;
  end

  // User function

  else if VarName = 'VFR_ORD1' then
  begin
    if RGAgrupa.ItemIndex < 1 then Value := 0 else
    Value := RGOrdem1.ItemIndex + 1;
  end else
  if VarName = 'VFR_ORD2' then
  begin
    if RGAgrupa.ItemIndex < 2 then Value := 0 else
    Value := RGOrdem2.ItemIndex + 1;
  end else
  if VarName = 'VFR_ORD3' then
  begin
    if RGAgrupa.ItemIndex < 3 then Value := 0 else
    Value := RGOrdem3.ItemIndex + 1;
  end

end;

end.

