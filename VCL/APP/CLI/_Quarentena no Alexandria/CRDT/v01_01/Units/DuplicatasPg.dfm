object FmDuplicatasPg: TFmDuplicatasPg
  Left = 317
  Top = 252
  Caption = 'Baixa de Duplicata'
  ClientHeight = 313
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelStatus: TPanel
    Left = 0
    Top = 108
    Width = 792
    Height = 57
    Align = alTop
    TabOrder = 1
    Visible = False
    object Label15: TLabel
      Left = 8
      Top = 4
      Width = 121
      Height = 13
      Caption = 'Novo status da duplicata:'
    end
    object Label17: TLabel
      Left = 500
      Top = 4
      Width = 26
      Height = 13
      Caption = 'Data:'
    end
    object EdAlinea: TdmkEditCB
      Left = 8
      Top = 20
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdAlineaChange
      DBLookupComboBox = CBAlinea
      IgnoraDBLookupComboBox = False
    end
    object CBAlinea: TdmkDBLookupComboBox
      Left = 56
      Top = 20
      Width = 441
      Height = 21
      Color = clWhite
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsOcorDupl
      TabOrder = 1
      dmkEditCB = EdAlinea
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object TPData: TDateTimePicker
      Left = 500
      Top = 20
      Width = 89
      Height = 21
      Date = 38698.785142685200000000
      Time = 38698.785142685200000000
      TabOrder = 2
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 265
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 3
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 682
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Desiste'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Baixa de Duplicata'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 4
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 60
    Align = alTop
    Enabled = False
    TabOrder = 0
    object Label2: TLabel
      Left = 8
      Top = 4
      Width = 59
      Height = 13
      Caption = 'Vencimento:'
    end
    object Label3: TLabel
      Left = 100
      Top = 4
      Width = 35
      Height = 13
      Caption = 'Cliente:'
    end
    object Label4: TLabel
      Left = 340
      Top = 4
      Width = 48
      Height = 13
      Caption = 'Duplicata:'
    end
    object Label5: TLabel
      Left = 668
      Top = 4
      Width = 20
      Height = 13
      Caption = 'CPF'
    end
    object Label6: TLabel
      Left = 500
      Top = 4
      Width = 44
      Height = 13
      Caption = 'Emitente:'
    end
    object Label7: TLabel
      Left = 424
      Top = 4
      Width = 27
      Height = 13
      Caption = 'Valor:'
    end
    object TPVenc: TDateTimePicker
      Left = 8
      Top = 20
      Width = 89
      Height = 21
      Date = 38698.785142685200000000
      Time = 38698.785142685200000000
      TabOrder = 0
    end
    object EdCliCod: TdmkEdit
      Left = 100
      Top = 20
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdCliNome: TdmkEdit
      Left = 144
      Top = 20
      Width = 193
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdDuplicata: TdmkEdit
      Left = 340
      Top = 20
      Width = 81
      Height = 21
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdCPF: TdmkEdit
      Left = 668
      Top = 20
      Width = 113
      Height = 21
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdEmitente: TdmkEdit
      Left = 500
      Top = 20
      Width = 165
      Height = 21
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdValor: TdmkEdit
      Left = 424
      Top = 20
      Width = 73
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
  end
  object PainelPagto: TPanel
    Left = 0
    Top = 165
    Width = 792
    Height = 100
    Align = alClient
    TabOrder = 2
    Visible = False
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 48
      Align = alTop
      TabOrder = 0
      object Label23: TLabel
        Left = 8
        Top = 4
        Width = 52
        Height = 13
        Caption = 'Data base:'
      end
      object Label24: TLabel
        Left = 100
        Top = 4
        Width = 53
        Height = 13
        Caption = 'Valor base:'
      end
      object Label25: TLabel
        Left = 196
        Top = 4
        Width = 89
        Height = 13
        Caption = '% Taxa juros base:'
      end
      object Label29: TLabel
        Left = 292
        Top = 4
        Width = 79
        Height = 13
        Caption = '% Juros per'#237'odo:'
      end
      object TPDataBase2: TDateTimePicker
        Left = 8
        Top = 20
        Width = 89
        Height = 21
        Date = 38698.785142685200000000
        Time = 38698.785142685200000000
        Color = clBtnFace
        Enabled = False
        TabOrder = 0
        TabStop = False
      end
      object EdValorBase2: TdmkEdit
        Left = 100
        Top = 20
        Width = 93
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdValorBase2Change
      end
      object EdJurosBase2: TdmkEdit
        Left = 196
        Top = 20
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Color = clWhite
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdJurosBase2Change
      end
      object EdJurosPeriodo2: TdmkEdit
        Left = 292
        Top = 20
        Width = 93
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 49
      Width = 790
      Height = 50
      Align = alClient
      TabOrder = 1
      object Label26: TLabel
        Left = 8
        Top = 4
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object Label27: TLabel
        Left = 100
        Top = 4
        Width = 37
        Height = 13
        Caption = '$ Juros:'
      end
      object Label28: TLabel
        Left = 388
        Top = 4
        Width = 75
        Height = 13
        Caption = '$ Valor a pagar:'
      end
      object Label30: TLabel
        Left = 292
        Top = 4
        Width = 66
        Height = 13
        Caption = 'Total a pagar:'
      end
      object Label1: TLabel
        Left = 196
        Top = 4
        Width = 58
        Height = 13
        Caption = '$ Desconto:'
      end
      object TPPagto2: TDateTimePicker
        Left = 8
        Top = 20
        Width = 89
        Height = 21
        Date = 38698.785142685200000000
        Time = 38698.785142685200000000
        TabOrder = 0
        OnChange = TPPagto2Change
      end
      object EdJuros2: TdmkEdit
        Left = 100
        Top = 20
        Width = 93
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdJuros2Change
      end
      object EdPago2: TdmkEdit
        Left = 388
        Top = 20
        Width = 93
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdAPagar: TdmkEdit
        Left = 292
        Top = 20
        Width = 93
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdDesco2: TdmkEdit
        Left = 196
        Top = 20
        Width = 93
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdDesco2Change
      end
    end
  end
  object QrOcorDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM ocordupl'
      'ORDER BY Nome')
    Left = 164
    Top = 12
    object QrOcorDuplCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorDuplNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrOcorDuplBase: TFloatField
      FieldName = 'Base'
    end
    object QrOcorDuplDatapbase: TIntegerField
      FieldName = 'Datapbase'
    end
    object QrOcorDuplOcorrbase: TIntegerField
      FieldName = 'Ocorrbase'
    end
    object QrOcorDuplStatusbase: TIntegerField
      FieldName = 'Statusbase'
    end
  end
  object DsOcorDupl: TDataSource
    DataSet = QrOcorDupl
    Left = 192
    Top = 12
  end
end
