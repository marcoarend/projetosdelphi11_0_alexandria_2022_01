unit OperaImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, ComCtrls,
  frxClass, frxDBSet, Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB;

type
  TFmOperaImp = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtImprime: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    QrOpera: TmySQLQuery;
    QrOperaNOMECLIENTE: TWideStringField;
    QrOperaCodigo: TIntegerField;
    QrOperaTipo: TSmallintField;
    QrOperaSpread: TSmallintField;
    QrOperaCliente: TIntegerField;
    QrOperaLote: TSmallintField;
    QrOperaData: TDateField;
    QrOperaTotal: TFloatField;
    QrOperaDias: TFloatField;
    QrOperaPeCompra: TFloatField;
    QrOperaTxCompra: TFloatField;
    QrOperaValValorem: TFloatField;
    QrOperaAdValorem: TFloatField;
    QrOperaIOC: TFloatField;
    QrOperaCPMF: TFloatField;
    QrOperaTipoAdV: TIntegerField;
    QrOperaIRRF: TFloatField;
    QrOperaIRRF_Val: TFloatField;
    QrOperaISS: TFloatField;
    QrOperaISS_Val: TFloatField;
    QrOperaPIS_R: TFloatField;
    QrOperaPIS_R_Val: TFloatField;
    QrOperaLk: TIntegerField;
    QrOperaDataCad: TDateField;
    QrOperaDataAlt: TDateField;
    QrOperaUserCad: TIntegerField;
    QrOperaUserAlt: TIntegerField;
    QrOperaTarifas: TFloatField;
    QrOperaOcorP: TFloatField;
    QrOperaCOFINS_R: TFloatField;
    QrOperaCOFINS_R_Val: TFloatField;
    QrOperaPIS: TFloatField;
    QrOperaPIS_Val: TFloatField;
    QrOperaCOFINS: TFloatField;
    QrOperaCOFINS_Val: TFloatField;
    QrOperaPIS_TOTAL: TFloatField;
    QrOperaCOFINS_TOTAL: TFloatField;
    QrOperaIOC_VAL: TFloatField;
    QrOperaCPMF_VAL: TFloatField;
    QrOperaMaxVencto: TDateField;
    QrOperaCHDevPg: TFloatField;
    QrOperaMINTC: TFloatField;
    QrOperaMINAV: TFloatField;
    QrOperaMINTC_AM: TFloatField;
    QrOperaPIS_T_Val: TFloatField;
    QrOperaCOFINS_T_Val: TFloatField;
    QrOperaPgLiq: TFloatField;
    QrOperaDUDevPg: TFloatField;
    QrOperaNF: TIntegerField;
    QrOperaItens: TIntegerField;
    frxOperacoes: TfrxReport;
    Panel1: TPanel;
    Panel2: TPanel;
    RGPeriodo: TRadioGroup;
    CBDiaSemanaI: TComboBox;
    StaticText1: TStaticText;
    CBDiaSemanaF: TComboBox;
    StaticText2: TStaticText;
    Panel3: TPanel;
    Panel4: TPanel;
    Label3: TLabel;
    EdCliente: TdmkEditCB;
    CbCliente: TdmkDBLookupComboBox;
    TPIni: TDateTimePicker;
    Label34: TLabel;
    Label1: TLabel;
    TPFim: TDateTimePicker;
    Panel5: TPanel;
    QrOperaCNPJCPF: TWideStringField;
    QrOperaIOFd_VAL: TFloatField;
    QrOperaIOFv_VAL: TFloatField;
    QrOperaTipoIOF: TSmallintField;
    QrOperaCNPJCPF_TXT: TWideStringField;
    frxDsOperacoes: TfrxDBDataset;
    QrOperaIOF_TOTAL: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure frxOperacoesGetValue(const VarName: String;
      var Value: Variant);
    procedure QrOperaCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmOperaImp: TFmOperaImp;

implementation

{$R *.DFM}

uses Module, UnInternalConsts, UnMyObjects;

procedure TFmOperaImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOperaImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmOperaImp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmOperaImp.FormCreate(Sender: TObject);
var
  Cam: String;
  i, f, k, t: Integer;
  Data: TDateTime;
  Ano, Mes, Dia, DiaI, DiaF: Word;
begin
  QrClientes.Open;
  //
  with CBDiaSemanaI.Items do
  begin
    Add(CO_DOMINGO      );
    Add(CO_SEGUNDAFEIRA );
    Add(CO_TERCAFEIRA   );
    Add(CO_QUARTAFEIRA  );
    Add(CO_QUINTAFEIRA  );
    Add(CO_SEXTAFEIRA   );
    Add(CO_SABADO       );
  end;
  with CBDiaSemanaF.Items do
  begin
    Add(CO_DOMINGO      );
    Add(CO_SEGUNDAFEIRA );
    Add(CO_TERCAFEIRA   );
    Add(CO_QUARTAFEIRA  );
    Add(CO_QUINTAFEIRA  );
    Add(CO_SEXTAFEIRA   );
    Add(CO_SABADO       );
  end;
  //
  Cam := Application.Title+'\InitialConfig\'+'OperaImp';
  t   := Geral.ReadAppKeyLM('TipPeriod', Cam, ktInteger, 0);
  i   := Geral.ReadAppKeyLM('DiaSemIni', Cam, ktInteger, 0);
  f   := Geral.ReadAppKeyLM('DiaSemFim', Cam, ktInteger, 6);
  //
  RGPeriodo.ItemIndex := t;
  //
  CBDiaSemanaI.ItemIndex := i-1;
  CBDiaSemanaF.ItemIndex := f-1;
  //if t = 0 then
  //begin
    TPIni.Date := Date-7;
    TPFim.Date := Date;
  //end else
  case t of
    1:
    begin
      k := -1;
      Data := Date+1;
      while k <> f-1 do
      begin
        Data := Data -1;
        k := DayOfWeek(Data);
      end;
      TPFim.Date := Data+1;
      TPIni.Date := Data-f+i+1;
    end;
    2:
    begin
      DecodeDate(Date, Ano, Mes, Dia);
      if Dia > 21 then DiaI := 11 else
      if Dia > 11 then DiaI := 01 else
                       DiaI := 21;
      if DiaI = 01 then DiaF := 10 else
      if DiaI = 11 then DiaF := 20 else
         DecodeDate(Geral.UltimoDiaDoMes(IncMonth(Date, -1)), Ano, Mes, DiaF);
      //
      TPIni.Date := EncodeDate(Ano, Mes, DiaI);
      TPFim.Date := EncodeDate(Ano, Mes, DiaF);
    end;
  end;
end;

procedure TFmOperaImp.BtImprimeClick(Sender: TObject);
var
  Cliente: Integer;
begin
  Cliente := Geral.IMV(EdCliente.Text);
  QrOpera.Close;
  QrOpera.SQL.Clear;
  QrOpera.SQL.Add('SELECT lo.*, IF(en.Tipo=0, en.RazaoSocial, en.Nome)NOMECLIENTE,');
  QrOpera.SQL.Add('IF(en.Tipo=0, en.CNPJ, en.CPF) CNPJCPF');
  QrOpera.SQL.Add('FROM lotes lo');
  QrOpera.SQL.Add('LEFT JOIN entidades en ON en.Codigo=lo.Cliente');
  QrOpera.SQL.Add('');
  QrOpera.SQL.Add('');
  QrOpera.SQL.Add('WHERE lo.data BETWEEN :P0 AND :P1');
  QrOpera.SQL.Add('AND (TxCompra+ValValorem)>=0.01');
  if Cliente <> 0 then
    QrOpera.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
  QrOpera.SQL.Add('ORDER BY lo.NF, lo.Data, NOMECLIENTE, lo.Lote');
  //
  QrOpera.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrOpera.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  QrOpera.Open;
  //
  MyObjects.frxMostra(frxOperacoes, 'Relat�rio de opera��es');
end;

procedure TFmOperaImp.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Cam: String;
begin
  Cam := Application.Title+'\InitialConfig\'+'OperaImp';
  Geral.WriteAppKeyLM2('DiaSemIni', Cam, CBDiaSemanaI.ItemIndex+1, ktInteger);
  Geral.WriteAppKeyLM2('DiaSemFim', Cam, CBDiaSemanaF.ItemIndex+1, ktInteger);
  Geral.WriteAppKeyLM2('TipPeriod', Cam, RGPeriodo.ItemIndex, ktInteger);
end;

procedure TFmOperaImp.frxOperacoesGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_CLIENTE') = 0 then
  begin
    if CbCliente.KeyValue = NULL then Value := 'TODOS' else
    Value := CBCliente.Text;
  end else if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then Value :=
    FormatDateTime(VAR_FORMATDATE3, TPIni.Date) + CO_ATE +
    FormatDateTime(VAR_FORMATDATE3, TPFim.Date);
end;

procedure TFmOperaImp.QrOperaCalcFields(DataSet: TDataSet);
begin
  QrOperaCNPJCPF_TXT.Value := Geral.FormataCNPJ_TT(QrOperaCNPJCPF.Value);
  QrOperaIOF_TOTAL.Value := QrOperaIOC_VAL.Value +
    QrOperaIOFd_VAL.Value + QrOperaIOFv_VAL.Value;
end;

end.

