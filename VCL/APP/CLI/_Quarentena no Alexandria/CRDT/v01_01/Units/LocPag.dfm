object FmLocPag: TFmLocPag
  Left = 419
  Top = 217
  Caption = 'Localiza pagamento'
  ClientHeight = 411
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 363
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 2
    object BtOK: TBitBtn
      Tag = 22
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Pesquisa'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 682
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
    end
    object BtCancela: TBitBtn
      Tag = 28
      Left = 354
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Localiza'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtCancelaClick
    end
    object CkAutomatico: TCheckBox
      Left = 124
      Top = 16
      Width = 121
      Height = 17
      Caption = 'Pesquisa autom'#225'tico.'
      Checked = True
      State = cbChecked
      TabOrder = 3
    end
    object BitBtn1: TBitBtn
      Tag = 5
      Left = 446
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Imprime'
      NumGlyphs = 2
      TabOrder = 4
      OnClick = BitBtn1Click
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Localiza pagamento'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 315
    Align = alClient
    TabOrder = 0
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 68
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 1
        Top = 1
        Width = 228
        Height = 66
        Align = alLeft
        Caption = ' Intervalo de valor: '
        TabOrder = 0
        object EdMinimo: TdmkEdit
          Left = 12
          Top = 36
          Width = 101
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdMinimoExit
        end
        object CkMinimo: TCheckBox
          Left = 12
          Top = 16
          Width = 105
          Height = 17
          Caption = 'M'#237'nimo:'
          TabOrder = 0
          OnClick = CkMinimoClick
        end
        object CkMaximo: TCheckBox
          Left = 120
          Top = 16
          Width = 105
          Height = 17
          Caption = 'M'#225'ximo:'
          TabOrder = 2
          OnClick = CkMaximoClick
        end
        object EdMaximo: TdmkEdit
          Left = 120
          Top = 36
          Width = 101
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdMaximoExit
        end
      end
      object GroupBox2: TGroupBox
        Left = 229
        Top = 1
        Width = 240
        Height = 66
        Align = alLeft
        Caption = ' Intervalo de datas: '
        TabOrder = 1
        object TPIniA: TDateTimePicker
          Left = 8
          Top = 36
          Width = 101
          Height = 21
          Date = 38675.714976851900000000
          Time = 38675.714976851900000000
          TabOrder = 1
          OnChange = TPIniAChange
        end
        object TPFimA: TDateTimePicker
          Left = 120
          Top = 36
          Width = 101
          Height = 21
          Date = 38675.714976851900000000
          Time = 38675.714976851900000000
          TabOrder = 3
          OnChange = TPFimAChange
        end
        object CkIniA: TCheckBox
          Left = 12
          Top = 16
          Width = 105
          Height = 17
          Caption = 'Emiss'#227'o inicial:'
          TabOrder = 0
          OnClick = CkIniAClick
        end
        object CkFimA: TCheckBox
          Left = 120
          Top = 16
          Width = 105
          Height = 17
          Caption = 'Emiss'#227'o final:'
          TabOrder = 2
          OnClick = CkFimAClick
        end
      end
      object GroupBox3: TGroupBox
        Left = 469
        Top = 1
        Width = 312
        Height = 66
        Align = alClient
        TabOrder = 2
        object CkCheque: TCheckBox
          Left = 8
          Top = 17
          Width = 105
          Height = 17
          Caption = 'N'#186' cheque:'
          TabOrder = 0
          OnClick = CkChequeClick
        end
        object EdCheque: TdmkEdit
          Left = 8
          Top = 41
          Width = 101
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnExit = EdChequeExit
        end
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 117
      Width = 782
      Height = 197
      TabStop = False
      Align = alClient
      DataSource = DsPesq
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = DBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Lote'
          Title.Caption = 'Border'#244
          Width = 52
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Data'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Title.Caption = 'Valor'
          Width = 68
          Visible = True
        end
        item
          Alignment = taCenter
          Expanded = False
          FieldName = 'Documento'
          Title.Caption = 'Cheque'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vencto'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'Lote'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Benefici'#225'rio'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECARTEIRA'
          Title.Caption = 'Carteira'
          Width = 153
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Banco'
          Title.Caption = 'Bco'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Agencia'
          Title.Caption = 'Ag'#234'n.'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ContaCorrente'
          Title.Caption = 'Conta corrente'
          Width = 76
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 1
      Top = 69
      Width = 782
      Height = 48
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 1
      object LaCliente: TLabel
        Left = 12
        Top = 3
        Width = 58
        Height = 13
        Caption = 'Benefici'#225'rio:'
      end
      object Label1: TLabel
        Left = 400
        Top = 3
        Width = 39
        Height = 13
        Caption = 'Carteira:'
      end
      object EdCliente: TdmkEditCB
        Left = 12
        Top = 19
        Width = 61
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 76
        Top = 19
        Width = 320
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECLI'
        ListSource = DsClientes
        TabOrder = 1
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCarteira: TdmkEditCB
        Left = 400
        Top = 19
        Width = 61
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCarteiraChange
        DBLookupComboBox = CBCarteira
        IgnoraDBLookupComboBox = False
      end
      object CBCarteira: TdmkDBLookupComboBox
        Left = 464
        Top = 19
        Width = 320
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCarteiras
        TabOrder = 3
        dmkEditCB = EdCarteira
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.Data, la.Debito, la.Documento, la.Vencimento, '
      'la.Carteira, la.FatNum, la.Banco, la.Agencia, '
      'la.ContaCorrente, la.Cliente, CASE WHEN cl.Tipo=0'
      'THEN cl.RazaoSocial ELSE cl.Nome END NOMECLI,'
      'ca.Nome NOMECARTEIRA, lo.Lote, lo.Tipo, lo.NF '
      'FROM lanctos la'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'LEFT JOIN lotes     lo ON lo.Codigo=la.FatNum'
      'WHERE la.FatID=300')
    Left = 236
    Top = 268
    object QrPesqData: TDateField
      DisplayWidth = 10
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqDebito: TFloatField
      DisplayWidth = 12
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesqDocumento: TFloatField
      DisplayWidth = 8
      FieldName = 'Documento'
      DisplayFormat = '000000;-000000; '
    end
    object QrPesqVencimento: TDateField
      DisplayWidth = 10
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqCarteira: TIntegerField
      DisplayWidth = 12
      FieldName = 'Carteira'
      Required = True
    end
    object QrPesqBanco: TIntegerField
      DisplayWidth = 5
      FieldName = 'Banco'
      DisplayFormat = '000;-000; '
    end
    object QrPesqContaCorrente: TWideStringField
      DisplayWidth = 18
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrPesqCliente: TIntegerField
      DisplayWidth = 12
      FieldName = 'Cliente'
    end
    object QrPesqNOMECLI: TWideStringField
      DisplayWidth = 120
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrPesqNOMECARTEIRA: TWideStringField
      DisplayWidth = 120
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrPesqLote: TIntegerField
      FieldName = 'Lote'
      DisplayFormat = '0000;-0000; '
    end
    object QrPesqTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPesqNF: TIntegerField
      FieldName = 'NF'
    end
    object QrPesqFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPesqAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 264
    Top = 268
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0'
      'THEN RazaoSocial ELSE Nome END NOMECLI'
      'FROM entidades '
      'ORDER BY NOMECLI')
    Left = 260
    Top = 128
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 288
    Top = 128
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 524
    Top = 132
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, Codigo'
      'FROM carteiras'
      'ORDER BY Nome')
    Left = 496
    Top = 132
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object frxReport1: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39016.835672013900000000
    ReportOptions.LastChange = 39016.835672013900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxReport1GetValue
    Left = 448
    Top = 220
    Datasets = <
      item
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
      end>
    Variables = <>
    Style = <>
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 120.944960000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo13: TfrxMemoView
          Top = 102.047310000000000000
          Width = 45.354330708661400000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 45.354360000000000000
          Top = 102.047310000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 98.267780000000000000
          Top = 102.047310000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 154.960730000000000000
          Top = 102.047310000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cheque')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 200.315090000000000000
          Top = 102.047310000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 253.228510000000000000
          Top = 102.047310000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 343.937230000000000000
          Top = 102.047310000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 445.984540000000000000
          Top = 102.047310000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Carteira')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 570.709030000000000000
          Top = 102.047310000000000000
          Width = 30.236220470000000000
          Height = 18.897650000000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Banco')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 600.945270000000000000
          Top = 102.047310000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag'#234'ncia')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 646.299630000000000000
          Top = 102.047310000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Width = 718.110700000000000000
          Height = 30.236240000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -24
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Pesquisa de Pagamentos')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Top = 34.015770000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Intrervalos de valor:')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 362.834880000000000000
          Top = 34.015770000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Intrervalos de datas:')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 86.929190000000000000
          Top = 34.015770000000000000
          Width = 272.126160000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[INTERVALO_VALORES]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 457.323130000000000000
          Top = 34.015770000000000000
          Width = 257.008040000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[INTERVALO_DATAS]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Top = 56.692950000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Benefici'#225'rio:')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 362.834880000000000000
          Top = 56.692950000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Carteira:')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 86.929190000000000000
          Top = 56.692950000000000000
          Width = 272.126160000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[BENEFICIARIO]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 457.323130000000000000
          Top = 56.692950000000000000
          Width = 257.008040000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[CARTEIRA]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Top = 79.370130000000000000
          Width = 86.929190000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'N'#186' Cheque:')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 86.929190000000000000
          Top = 79.370130000000000000
          Width = 272.126160000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[CHEQUE]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 298.582870000000000000
          Top = 102.047310000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 200.315090000000000000
        Width = 718.110700000000000000
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
        RowCount = 0
        object Memo2: TfrxMemoView
          Width = 45.354330708661400000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq."Lote"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 45.354360000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          DataField = 'Data'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq."Data"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 98.267780000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataField = 'Debito'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq."Debito"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 154.960730000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataField = 'Documento'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq."Documento"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 200.315090000000000000
          Width = 52.913385830000000000
          Height = 18.897650000000000000
          DataField = 'Vencimento'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq."Vencimento"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 253.228510000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataField = 'FatNum'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq."FatNum"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 343.937230000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          DataField = 'NOMECLI'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NOMECLI"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 445.984540000000000000
          Width = 124.724490000000000000
          Height = 18.897650000000000000
          DataField = 'NOMECARTEIRA'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."NOMECARTEIRA"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 570.709030000000000000
          Width = 30.236220470000000000
          Height = 18.897650000000000000
          DataField = 'Banco'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq."Banco"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 600.945270000000000000
          Width = 45.354330710000000000
          Height = 18.897650000000000000
          DataField = 'Agencia'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq."Agencia"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 646.299630000000000000
          Width = 68.031496060000000000
          Height = 18.897650000000000000
          DataField = 'ContaCorrente'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq."ContaCorrente"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 298.582870000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DataField = 'NF'
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq."NF"]')
          ParentFont = False
        end
      end
      object Memo24: TfrxMemoView
        Left = 593.386210000000000000
        Width = 117.165430000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Arial'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[Date], [Time]')
        ParentFont = False
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 279.685220000000000000
        Width = 718.110700000000000000
        object Memo26: TfrxMemoView
          Left = 49.133890000000000000
          Top = 3.779529999999970000
          Width = 120.944960000000000000
          Height = 18.897650000000000000
          DataSet = frxDsPesq
          DataSetName = 'frxDsPesq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq."Debito">,MasterData1)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Top = 3.779529999999970000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'TOTAL:')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 325.039580000000000000
        Width = 718.110700000000000000
        object Memo86: TfrxMemoView
          Left = 597.165740000000000000
          Width = 117.165430000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsPesq: TfrxDBDataset
    UserName = 'frxDsPesq'
    CloseDataSource = False
    DataSet = QrPesq
    BCDToCurrency = False
    Left = 476
    Top = 220
  end
end
