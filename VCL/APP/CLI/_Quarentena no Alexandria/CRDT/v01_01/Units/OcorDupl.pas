unit OcorDupl;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkDBLookupComboBox,
  dmkEditCB, dmkRadioGroup, Variants, ComCtrls, UnDmkProcFunc, UnDmkEnums;

type
  TFmOcorDupl = class(TForm)
    PainelDados: TPanel;
    DsOcorDupl: TDataSource;
    QrOcorDupl: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PnPesq: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TStaticText;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label9: TLabel;
    QrOcorDuplCodigo: TIntegerField;
    QrOcorDuplNome: TWideStringField;
    QrOcorDuplBase: TFloatField;
    QrOcorDuplLk: TIntegerField;
    QrOcorDuplDataCad: TDateField;
    QrOcorDuplDataAlt: TDateField;
    QrOcorDuplUserCad: TIntegerField;
    QrOcorDuplUserAlt: TIntegerField;
    QrOcorDuplDatapbase: TIntegerField;
    QrOcorDuplOcorrbase: TIntegerField;
    QrOcorDuplStatusbase: TIntegerField;
    QrOcorDuplEnvio: TIntegerField;
    QrOcorDuplMovimento: TIntegerField;
    QrOcorDuplAlterWeb: TSmallintField;
    QrOcorDuplAtivo: TSmallintField;
    EdBase: TdmkEdit;
    Label3: TLabel;
    RGDataPBase: TdmkRadioGroup;
    EdOcorrencia: TdmkEditCB;
    CBOcorrencia: TdmkDBLookupComboBox;
    Label4: TLabel;
    RGStatusBase: TdmkRadioGroup;
    QrOcorBank: TmySQLQuery;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    DsOcorBank: TDataSource;
    SpeedButton5: TSpeedButton;
    dmkDBEdit1: TdmkDBEdit;
    Label5: TLabel;
    dmkDBEdit2: TdmkDBEdit;
    Label6: TLabel;
    QrOcorDuplNOMEOCORBANK: TWideStringField;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    BtImportar: TBitBtn;
    Progress: TProgressBar;
    OpenDialog1: TOpenDialog;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrOcorDuplAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrOcorDuplBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure EdCodigoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BtImportarClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmOcorDupl: TFmOcorDupl;
const
  FFormatFloat = '00000';

implementation

uses Module, Principal, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmOcorDupl.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmOcorDupl.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrOcorDuplCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmOcorDupl.DefParams;
begin
  VAR_GOTOTABELA := 'ocordupl';
  VAR_GOTOMYSQLTABLE := QrOcorDupl;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ok.Nome NOMEOCORBANK, od.*');
  VAR_SQLx.Add('FROM ocordupl od');
  VAR_SQLx.Add('LEFT JOIN ocorbank ok ON ok.Codigo=od.Ocorrbase');
  VAR_SQLx.Add('WHERE od.Codigo <> 0');
  //
  VAR_SQL1.Add('AND od.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND od.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND od.Nome Like :P0');
  //
end;

procedure TFmOcorDupl.EdCodigoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (LaTipo.SQLType = stIns) then
    UMyMod.BuscaNovoCodigo_Int(Dmod.QrAux, 'ocordupl', 'Codigo', [], [],
    stIns, 0, siPositivo, EdCodigo);
end;

procedure TFmOcorDupl.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible := True;
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
    end;
    1:
    begin
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      PainelControle.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant     := FormatFloat(FFormatFloat, Codigo);
        EdNome.ValueVariant       := '';
        EdBase.ValueVariant       := 0;
        RGDataPBase.ItemIndex     := 0;
        EdOcorrencia.ValueVariant := 0;
        CBOcorrencia.KeyValue     := Null;
        RGStatusBase.ItemIndex    := 0;
        //
      end else begin
        EdCodigo.ValueVariant     := QrOcorDuplCodigo.Value;
        EdNome.ValueVariant       := QrOcorDuplNome.Value;
        EdBase.ValueVariant       := QrOcorDuplBase.Value;
        RGDataPBase.ItemIndex     := QrOcorDuplDatapbase.Value;
        EdOcorrencia.ValueVariant := QrOcorDuplOcorrbase.Value;
        CBOcorrencia.KeyValue     := QrOcorDuplOcorrbase.Value;
        RGStatusBase.ItemIndex    := QrOcorDuplStatusbase.Value;
        //
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.SQLType);
end;

procedure TFmOcorDupl.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmOcorDupl.QueryPrincipalAfterOpen;
begin
end;

procedure TFmOcorDupl.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmOcorDupl.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmOcorDupl.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmOcorDupl.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmOcorDupl.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmOcorDupl.SpeedButton5Click(Sender: TObject);
var
  Ocorr: Integer;
begin
  VAR_CADASTRO := 0;
  Ocorr        := EdOcorrencia.ValueVariant;
  //
  FmPrincipal.CadastroOcorBank(Ocorr);
  //
  if VAR_CADASTRO <> 0 then
  begin
    QrOcorBank.Close;
    QrOcorBank.Open;
    //
    EdOcorrencia.ValueVariant := VAR_CADASTRO;
    CBOcorrencia.KeyValue     := VAR_CADASTRO;
    EdOcorrencia.SetFocus;
  end;
end;

procedure TFmOcorDupl.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrOcorDupl, [PainelDados],
  [PainelEdita], EdNome, LaTipo, 'ocordupl');
end;

procedure TFmOcorDupl.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrOcorDuplCodigo.Value;
  Close;
end;

procedure TFmOcorDupl.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Codigo := EdCodigo.ValueVariant;
  Nome   := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Codigo = 0, EdCodigo, 'Defina o ID!') then Exit;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmOcorDupl, PainelEdit,
    'ocordupl', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmOcorDupl.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.ValueVariant);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ocordupl', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ocordupl', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ocordupl', 'Codigo');
end;

procedure TFmOcorDupl.BtImportarClick(Sender: TObject);
var
  Lista: TStringList;
  i, PosNum, Tam: Integer;
  Codigo, Nome: String;
begin
  if OpenDialog1.Execute then
  begin
    Screen.Cursor := crHourGlass;
    //
    PosNum := 0;
    Lista := TStringList.Create;
    Lista.LoadFromFile(OpenDialog1.FileName);
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('DELETE FROM ocordupl WHERE Codigo=:P0');
    //
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('INSERT INTO ocordupl SET');
    Dmod.QrUpdU.SQL.Add('Nome=:P0, Codigo=:P1');
    //
    Progress.Position := 0;
    Progress.Visible := True;
    Progress.Max := Lista.Count;

    for i := 0 to Lista.Count-1 do
    begin
      Progress.Position := Progress.Position + 1;
      Tam := Length(Lista[i]);
      if Tam > 4 then
      begin
        if (Lista[i][1] in (['0'..'9'])) then
        begin
          Codigo := Copy(Lista[i], 1, 3);
          if PosNum <> 0 then
            Nome := TrimRight(Copy(Lista[i], 5, Length(Lista[i])))
          else
            Nome := Copy(Lista[i], 5, Length(Lista[i]));
          //ShowMessage(PChar(Codigo+' - '+Nome+' @ '+Motivo));
          Dmod.QrUpdM.Params[00].AsString := Codigo;
          Dmod.QrUpdM.ExecSQL;
          //
          Dmod.QrUpdU.Params[00].AsString := Nome;
          Dmod.QrUpdU.Params[01].AsString := Codigo;
          Dmod.QrUpdU.ExecSQL;
        end;
      end;
    end;
    Progress.Visible := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOcorDupl.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrOcorDupl, [PainelDados],
  [PainelEdita], EdCodigo, LaTipo, 'ocordupl');
end;

procedure TFmOcorDupl.FormCreate(Sender: TObject);
begin
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
  //
  QrOcorBank.Open;
end;

procedure TFmOcorDupl.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrOcorDuplCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOcorDupl.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmOcorDupl.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrOcorDuplCodigo.Value, LaRegistro.Caption);
end;

procedure TFmOcorDupl.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.SQLType);
end;

procedure TFmOcorDupl.QrOcorDuplAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmOcorDupl.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmOcorDupl.SbQueryClick(Sender: TObject);
begin
  LocCod(QrOcorDuplCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ocordupl', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmOcorDupl.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmOcorDupl.QrOcorDuplBeforeOpen(DataSet: TDataSet);
begin
  QrOcorDuplCodigo.DisplayFormat := FFormatFloat;
end;

end.

