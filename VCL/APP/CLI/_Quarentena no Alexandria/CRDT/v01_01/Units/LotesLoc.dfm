object FmLotesLoc: TFmLotesLoc
  Left = 333
  Top = 182
  Caption = 'Localiza'#231#227'o de Border'#244
  ClientHeight = 334
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 105
    Width = 784
    Height = 181
    Align = alClient
    DataSource = DsLoc
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'Lote'
        Title.Caption = 'Border'#244
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Data'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMETIPO'
        Title.Caption = 'Tipo de documento'
        Width = 98
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Codigo'
        Title.Caption = 'Lote'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Total'
        Title.Caption = 'Valor'
        Width = 77
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMECLIENTE'
        Title.Caption = 'Cliente'
        Width = 311
        Visible = True
      end
      item
        Alignment = taCenter
        Expanded = False
        FieldName = 'NF'
        Width = 54
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Itens'
        Width = 49
        Visible = True
      end>
  end
  object PainelDados: TPanel
    Left = 0
    Top = 40
    Width = 784
    Height = 65
    Align = alTop
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label2: TLabel
      Left = 80
      Top = 4
      Width = 101
      Height = 13
      Caption = 'Descri'#231#227'o do Cliente:'
    end
    object Label3: TLabel
      Left = 448
      Top = 4
      Width = 70
      Height = 13
      Caption = 'N'#186' do Border'#244':'
    end
    object Label34: TLabel
      Left = 528
      Top = 4
      Width = 55
      Height = 13
      Caption = 'Data inicial:'
    end
    object Label4: TLabel
      Left = 632
      Top = 4
      Width = 48
      Height = 13
      Caption = 'Data final:'
    end
    object Label5: TLabel
      Left = 736
      Top = 4
      Width = 46
      Height = 13
      Caption = 'Controle*:'
    end
    object Label6: TLabel
      Left = 392
      Top = 48
      Width = 393
      Height = 13
      Caption = 
        '* N'#250'mero do controle do cheque ou duplicata ao qual se deseja lo' +
        'calizar o border'#244'.'
    end
    object CBCliente: TdmkDBLookupComboBox
      Left = 80
      Top = 20
      Width = 365
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsClientes
      TabOrder = 1
      dmkEditCB = EdCliente
      UpdType = utYes
    end
    object EdCliente: TdmkEditCB
      Left = 12
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdClienteChange
      DBLookupComboBox = CBCliente
    end
    object EdLote: TdmkEdit
      Left = 448
      Top = 20
      Width = 77
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdLoteChange
      OnExit = EdLoteExit
    end
    object TPIni: TDateTimePicker
      Left = 528
      Top = 20
      Width = 101
      Height = 21
      Date = 38675.714976851900000000
      Time = 38675.714976851900000000
      TabOrder = 3
      OnClick = TPIniClick
    end
    object TPFim: TDateTimePicker
      Left = 632
      Top = 20
      Width = 101
      Height = 21
      Date = 38675.714976851900000000
      Time = 38675.714976851900000000
      TabOrder = 4
      OnClick = TPFimClick
    end
    object EdControle: TdmkEdit
      Left = 736
      Top = 20
      Width = 48
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdControleChange
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 286
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 14
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Confirma'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 684
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 40
    Align = alTop
    Caption = 'Border'#244's'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 701
      Top = 1
      Width = 82
      Height = 38
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 36
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 700
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 706
      ExplicitHeight = 36
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 416
    Top = 46
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE 1 WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE, Codigo'
      'FROM entidades'
      'WHERE Cliente1="V" '
      'ORDER BY NOMEENTIDADE')
    Left = 388
    Top = 46
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLocAfterOpen
    OnCalcFields = QrLocCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLIENTE, '
      'CASE WHEN en.Tipo=0 THEN en.CNPJ'
      'ELSE en.CPF END CNPJCPF, lo.* '
      'FROM lotes lo'
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente'
      'WHERE Cliente=:P0'
      'AND TxCompra+ValValorem+:P0>=0.01'
      'ORDER BY Lote DESC')
    Left = 40
    Top = 156
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P0>=0.01'
        ParamType = ptUnknown
      end>
    object QrLocCodigo: TIntegerField
      FieldName = 'Codigo'
      DisplayFormat = '000000'
    end
    object QrLocLote: TSmallintField
      FieldName = 'Lote'
      DisplayFormat = '0000'
    end
    object QrLocData: TDateField
      FieldName = 'Data'
    end
    object QrLocTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLocNOMETIPO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPO'
      Size = 50
      Calculated = True
    end
    object QrLocTotal: TFloatField
      FieldName = 'Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLocNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrLocCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrLocSpread: TSmallintField
      FieldName = 'Spread'
      Required = True
    end
    object QrLocCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrLocDias: TFloatField
      FieldName = 'Dias'
      Required = True
    end
    object QrLocPeCompra: TFloatField
      FieldName = 'PeCompra'
      Required = True
    end
    object QrLocTxCompra: TFloatField
      FieldName = 'TxCompra'
      Required = True
    end
    object QrLocValValorem: TFloatField
      FieldName = 'ValValorem'
      Required = True
    end
    object QrLocAdValorem: TFloatField
      FieldName = 'AdValorem'
      Required = True
    end
    object QrLocIOC: TFloatField
      FieldName = 'IOC'
      Required = True
    end
    object QrLocIOC_VAL: TFloatField
      FieldName = 'IOC_VAL'
      Required = True
    end
    object QrLocTarifas: TFloatField
      FieldName = 'Tarifas'
      Required = True
    end
    object QrLocCPMF: TFloatField
      FieldName = 'CPMF'
      Required = True
    end
    object QrLocCPMF_VAL: TFloatField
      FieldName = 'CPMF_VAL'
      Required = True
    end
    object QrLocTipoAdV: TIntegerField
      FieldName = 'TipoAdV'
      Required = True
    end
    object QrLocIRRF: TFloatField
      FieldName = 'IRRF'
      Required = True
    end
    object QrLocIRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
      Required = True
    end
    object QrLocISS: TFloatField
      FieldName = 'ISS'
      Required = True
    end
    object QrLocISS_Val: TFloatField
      FieldName = 'ISS_Val'
      Required = True
    end
    object QrLocPIS: TFloatField
      FieldName = 'PIS'
      Required = True
    end
    object QrLocPIS_Val: TFloatField
      FieldName = 'PIS_Val'
      Required = True
    end
    object QrLocPIS_R: TFloatField
      FieldName = 'PIS_R'
      Required = True
    end
    object QrLocPIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
      Required = True
    end
    object QrLocCOFINS: TFloatField
      FieldName = 'COFINS'
      Required = True
    end
    object QrLocCOFINS_Val: TFloatField
      FieldName = 'COFINS_Val'
      Required = True
    end
    object QrLocCOFINS_R: TFloatField
      FieldName = 'COFINS_R'
      Required = True
    end
    object QrLocCOFINS_R_Val: TFloatField
      FieldName = 'COFINS_R_Val'
      Required = True
    end
    object QrLocOcorP: TFloatField
      FieldName = 'OcorP'
      Required = True
    end
    object QrLocMaxVencto: TDateField
      FieldName = 'MaxVencto'
      Required = True
    end
    object QrLocCHDevPg: TFloatField
      FieldName = 'CHDevPg'
      Required = True
    end
    object QrLocLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLocMINTC: TFloatField
      FieldName = 'MINTC'
      Required = True
    end
    object QrLocMINAV: TFloatField
      FieldName = 'MINAV'
      Required = True
    end
    object QrLocMINTC_AM: TFloatField
      FieldName = 'MINTC_AM'
      Required = True
    end
    object QrLocPIS_T_Val: TFloatField
      FieldName = 'PIS_T_Val'
      Required = True
    end
    object QrLocCOFINS_T_Val: TFloatField
      FieldName = 'COFINS_T_Val'
      Required = True
    end
    object QrLocPgLiq: TFloatField
      FieldName = 'PgLiq'
      Required = True
    end
    object QrLocDUDevPg: TFloatField
      FieldName = 'DUDevPg'
      Required = True
    end
    object QrLocNF: TIntegerField
      FieldName = 'NF'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrLocItens: TIntegerField
      FieldName = 'Itens'
      Required = True
      DisplayFormat = '00;-00; '
    end
    object QrLocConferido: TIntegerField
      FieldName = 'Conferido'
      Required = True
    end
    object QrLocECartaSac: TSmallintField
      FieldName = 'ECartaSac'
      Required = True
    end
  end
  object DsLoc: TDataSource
    DataSet = QrLoc
    Left = 68
    Top = 156
  end
  object QrCon: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM lotesits'
      'WHERE Controle=:P0')
    Left = 344
    Top = 180
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
end
