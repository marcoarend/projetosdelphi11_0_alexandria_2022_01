unit LocPag;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, Grids, DBGrids, Db,
  mySQLDbTables, DBCtrls, frxClass, frxDBSet, dmkGeral, dmkDBLookupComboBox,
  dmkEditCB, dmkEdit, UnDmkProcFunc, UnDmkEnums;

type
  TFmLocPag = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    BtCancela: TBitBtn;
    Panel2: TPanel;
    GroupBox1: TGroupBox;
    EdMinimo: TdmkEdit;
    CkMinimo: TCheckBox;
    CkMaximo: TCheckBox;
    EdMaximo: TdmkEdit;
    GroupBox2: TGroupBox;
    TPIniA: TDateTimePicker;
    TPFimA: TDateTimePicker;
    CkIniA: TCheckBox;
    CkFimA: TCheckBox;
    GroupBox3: TGroupBox;
    CkCheque: TCheckBox;
    EdCheque: TdmkEdit;
    DBGrid1: TDBGrid;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqData: TDateField;
    QrPesqDebito: TFloatField;
    QrPesqDocumento: TFloatField;
    QrPesqVencimento: TDateField;
    QrPesqCarteira: TIntegerField;
    QrPesqBanco: TIntegerField;
    QrPesqContaCorrente: TWideStringField;
    QrPesqCliente: TIntegerField;
    QrPesqNOMECLI: TWideStringField;
    QrPesqNOMECARTEIRA: TWideStringField;
    Panel3: TPanel;
    LaCliente: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    DsCarteiras: TDataSource;
    QrCarteiras: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLI: TWideStringField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasCodigo: TIntegerField;
    QrPesqLote: TIntegerField;
    CkAutomatico: TCheckBox;
    QrPesqTipo: TSmallintField;
    frxReport1: TfrxReport;
    frxDsPesq: TfrxDBDataset;
    BitBtn1: TBitBtn;
    QrPesqNF: TIntegerField;
    QrPesqFatNum: TFloatField;
    QrPesqAgencia: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdMinimoExit(Sender: TObject);
    procedure EdMaximoExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdChequeExit(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure EdCarteiraChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CkMinimoClick(Sender: TObject);
    procedure CkMaximoClick(Sender: TObject);
    procedure TPIniAChange(Sender: TObject);
    procedure CkFimAClick(Sender: TObject);
    procedure TPFimAChange(Sender: TObject);
    procedure CkChequeClick(Sender: TObject);
    procedure CkIniAClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure frxReport1GetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
    FRegistryPath: String;
    procedure Pesquisa(Forca: Boolean);
  public
    { Public declarations }
  end;

  var
  FmLocPag: TFmLocPag;

implementation

uses Module, Principal, UnInternalConsts, UnMyObjects;

{$R *.DFM}

procedure TFmLocPag.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLocPag.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmLocPag.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLocPag.EdMinimoExit(Sender: TObject);
begin
  if Geral.DMV(EdMaximo.Text) < Geral.DMV(EdMinimo.Text) then
  EdMaximo.Text := EdMinimo.Text;
  Pesquisa(False);
end;

procedure TFmLocPag.EdMaximoExit(Sender: TObject);
begin
  Pesquisa(False);  
end;

procedure TFmLocPag.FormCreate(Sender: TObject);
begin
  FRegistryPath        := Application.Title + '\LocalizaPagto';
  CkAutomatico.Checked := Geral.ReadAppKeyLM('Automatico', FRegistryPath, ktBoolean, True);
  //
  TPIniA.Date := Date - 30;
  TPFimA.Date := Date;
  //
  QrClientes.Open;
  QrCarteiras.Open;
end;

procedure TFmLocPag.EdChequeExit(Sender: TObject);
begin
  EdCheque.Text := Geral.TFT_Null(EdCheque.Text, 0, siPositivo);
  Pesquisa(False);
end;

procedure TFmLocPag.EdClienteChange(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.EdCarteiraChange(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.BtOKClick(Sender: TObject);
begin
  Pesquisa(True);
end;

procedure TFmLocPag.Pesquisa(Forca: Boolean);
var
  Minimo, Maximo: Double;
  //Carteira, Cliente: Integer;
begin
  if (not Forca) and (not CkAutomatico.Checked) then Exit;
  Minimo := Geral.DMV(EdMinimo.Text);
  Maximo := Geral.DMV(EdMaximo.Text);
  //Carteira := Geral.IMV(EdCarteira.Text);
  //Cliente := Geral.IMV(EdCliente.Text);
  if CkMaximo.Checked and CkMinimo.Checked then
  begin
    if (Maximo < Minimo) then
    begin
      Application.MessageBox('Valor m�ximo n�o pode ser menor que m�nimo!',
        'Erro', MB_OK+MB_ICONERROR);
      Exit;
    end;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    QrPesq.Close;
    QrPesq.SQL.Clear;
    QrPesq.SQL.Add('SELECT la.Data, la.Debito, la.Documento, la.Vencimento,');
    QrPesq.SQL.Add('la.Carteira, la.FatNum, la.Banco, la.Agencia,');
    QrPesq.SQL.Add('la.ContaCorrente, la.Cliente, CASE WHEN cl.Tipo=0');
    QrPesq.SQL.Add('THEN cl.RazaoSocial ELSE cl.Nome END NOMECLI,');
    QrPesq.SQL.Add('ca.Nome NOMECARTEIRA, lo.Lote, lo.Tipo, lo.NF');
    QrPesq.SQL.Add('FROM ' + VAR_LCT  + ' la');
    QrPesq.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=la.Cliente');
    QrPesq.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira');
    QrPesq.SQL.Add('LEFT JOIN lotes     lo ON lo.Codigo=la.FatNum');
    QrPesq.SQL.Add('WHERE la.FatID=300');
    QrPesq.SQL.Add('');
    QrPesq.SQL.Add(dmkPF.SQL_Periodo('AND la.Data ', TPIniA.Date, TPFimA.Date,
      CkIniA.Checked, CkFimA.Checked));
    QrPesq.SQL.Add(MLAGeral.SQL_CodTxt('AND la.Documento ', EdCheque.Text,
      True, True, not CkCheque.Checked));
    QrPesq.SQL.Add(MLAGeral.SQL_CodTxt('AND la.Cliente ', EdCliente.Text,
      False, True, False));
    QrPesq.SQL.Add(MLAGeral.SQL_CodTxt('AND la.Carteira ', EdCarteira.Text,
      False, True, False));
    QrPesq.SQL.Add(MLAGeral.SQL_Valores('AND la.Debito ', Minimo, Maximo,
      CkMinimo.Checked, CkMaximo.Checked));
    QrPesq.SQL.Add('');
    QrPesq.SQL.Add('ORDER BY la.Data DESC, la.FatNum');
    QrPesq.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLocPag.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Geral.WriteAppKeyLM2('Automatico', FRegistryPath, CkAutomatico.Checked, ktBoolean);
end;

procedure TFmLocPag.CkMinimoClick(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.CkMaximoClick(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.TPIniAChange(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.CkFimAClick(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.TPFimAChange(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.CkChequeClick(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.CkIniAClick(Sender: TObject);
begin
  Pesquisa(False);
end;

procedure TFmLocPag.BtCancelaClick(Sender: TObject);
begin
  FmPrincipal.CriaFormLotes(1, QrPesqTipo.Value, Trunc(QrPesqFatNum.Value), 0);
end;

procedure TFmLocPag.DBGrid1DblClick(Sender: TObject);
begin
  if QrPesq.State = dsBrowse then
    FmPrincipal.CriaFormLotes(1, QrPesqTipo.Value, Trunc(QrPesqFatNum.Value), 0);
end;

procedure TFmLocPag.BitBtn1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxReport1, 'Pesquisa de Pagamentos');
end;

procedure TFmLocPag.frxReport1GetValue(const VarName: String;
  var Value: Variant);
var
  IniA, FimA: String;
begin
  if VarName = 'INTERVALO_VALORES' then
  begin
    if CkMinimo.Checked and CkMaximo.Checked then Value := 'M�nimo = '+
      EdMinimo.Text + ' e M�ximo = ' + EdMaximo.Text
    else if CkMinimo.Checked then Value := 'M�nimo = '+ EdMinimo.Text
    else if CkMaximo.Checked then Value := 'M�ximo = '+ EdMaximo.Text
    else Value := 'Qualquer'
  end else if VarName = 'INTERVALO_DATAS' then
  begin
    IniA := Geral.FDT(TPIniA.Date, 2);
    FimA := Geral.FDT(TPFimA.Date, 2);
    if CkIniA.Checked and CkFimA.Checked then Value := 'In�cio = '+
      IniA + ' e Final = ' + FimA
    else if CkIniA.Checked then Value := 'In�cio = '+ IniA
    else if CkFimA.Checked then Value := 'Final = '+ FimA
    else Value := 'Qualquer'
  end else if VarName = 'BENEFICIARIO' then
    Value := MLAGeral.CampoReportTxt(CBCliente.Text, 'Todos')
  else if VarName = 'CARTEIRA' then
    Value := MLAGeral.CampoReportTxt(CBCarteira.Text, 'Todas')
  else if VarName = 'CHEQUE' then
    Value := MLAGeral.CampoReportTxt(EdCheque.Text, 'Todas')
end;

end.

