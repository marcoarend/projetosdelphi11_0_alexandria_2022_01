object FmInadimps: TFmInadimps
  Left = 333
  Top = 174
  Caption = 'Inadimpl'#234'ncia'
  ClientHeight = 334
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Inadimpl'#234'ncia'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 792
    Height = 69
    ActivePage = TabSheet3
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Cheques'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 64
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 41
        Align = alClient
        TabOrder = 0
        ExplicitHeight = 64
        object Label2: TLabel
          Left = 8
          Top = 8
          Width = 164
          Height = 13
          Caption = 'CPF / CNPJ Emitente [F8 localiza]:'
        end
        object EdCPF1: TdmkEdit
          Left = 8
          Top = 24
          Width = 177
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnExit = EdCPF1Exit
        end
        object DBEdEmitente: TDBEdit
          Left = 188
          Top = 24
          Width = 593
          Height = 21
          TabStop = False
          DataField = 'Nome'
          DataSource = DsEmitCPF
          TabOrder = 1
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Duplicatas'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 64
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 41
        Align = alClient
        TabOrder = 0
        ExplicitHeight = 64
        object Label14: TLabel
          Left = 8
          Top = 8
          Width = 129
          Height = 13
          Caption = 'CNPJ Sacado [F8 localiza]:'
        end
        object EdCPF2: TdmkEdit
          Left = 8
          Top = 24
          Width = 177
          Height = 21
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnExit = EdCPF2Exit
        end
        object DBEdSacado: TDBEdit
          Left = 188
          Top = 24
          Width = 593
          Height = 21
          TabStop = False
          DataField = 'Nome'
          DataSource = DsSacado
          TabOrder = 1
        end
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Cheques + Duplicatas'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label15: TLabel
        Left = 0
        Top = 0
        Width = 451
        Height = 19
        Align = alClient
        Alignment = taCenter
        Caption = 'Utilize as abas de Cheques e Duplicatas para configura'#231#227'o.'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 117
    Width = 792
    Height = 169
    Align = alBottom
    TabOrder = 2
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 112
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label10: TLabel
        Left = 530
        Top = 16
        Width = 254
        Height = 13
        Caption = '[F5]: Define data inicial como a do menor vencimento.'
      end
      object Label11: TLabel
        Left = 530
        Top = 32
        Width = 243
        Height = 13
        Caption = '[F6]: Define data final como a do maior vencimento.'
      end
      object Label12: TLabel
        Left = 530
        Top = 48
        Width = 105
        Height = 13
        Caption = '[F7]: Executa F5 e F6.'
      end
      object Label13: TLabel
        Left = 530
        Top = 64
        Width = 113
        Height = 13
        Caption = '[F8]: Pesquisa emitente.'
      end
      object RGOrdem1: TRadioGroup
        Left = 0
        Top = 0
        Width = 80
        Height = 112
        Align = alLeft
        Caption = ' Ordem 1: '
        ItemIndex = 0
        Items.Strings = (
          'Cliente'
          'Emitente'
          'Banco'
          'M'#234's'
          'Dia')
        TabOrder = 0
      end
      object RGOrdem2: TRadioGroup
        Left = 80
        Top = 0
        Width = 80
        Height = 112
        Align = alLeft
        Caption = ' Ordem 2: '
        ItemIndex = 1
        Items.Strings = (
          'Cliente'
          'Emitente'
          'Banco'
          'M'#234's'
          'Dia')
        TabOrder = 1
      end
      object RGOrdem3: TRadioGroup
        Left = 160
        Top = 0
        Width = 80
        Height = 112
        Align = alLeft
        Caption = ' Ordem 3: '
        ItemIndex = 3
        Items.Strings = (
          'Cliente'
          'Emitente'
          'Banco'
          'M'#234's'
          'Dia')
        TabOrder = 2
      end
      object RGOrdem4: TRadioGroup
        Left = 240
        Top = 0
        Width = 80
        Height = 112
        Align = alLeft
        Caption = ' Ordem 4: '
        ItemIndex = 4
        Items.Strings = (
          'Cliente'
          'Emitente'
          'Banco'
          'M'#234's'
          'Dia')
        TabOrder = 3
      end
      object RGGrupos: TRadioGroup
        Left = 320
        Top = 0
        Width = 58
        Height = 112
        Align = alLeft
        Caption = ' Grupos: '
        ItemIndex = 0
        Items.Strings = (
          '0'
          '1'
          '2')
        TabOrder = 4
      end
      object RGSintetico: TRadioGroup
        Left = 378
        Top = 0
        Width = 79
        Height = 112
        Align = alLeft
        Caption = ' Tipo: '
        ItemIndex = 0
        Items.Strings = (
          'Anal'#237'tico'
          'Sint'#233'tico')
        TabOrder = 5
      end
      object RGImpressao: TRadioGroup
        Left = 457
        Top = 0
        Width = 72
        Height = 112
        Align = alLeft
        Caption = ' Impress'#227'o: '
        ItemIndex = 0
        Items.Strings = (
          'Valores'
          'Tudo')
        TabOrder = 6
      end
    end
    object Panel6: TPanel
      Left = 1
      Top = 113
      Width = 790
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label75: TLabel
        Left = 8
        Top = 4
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label4: TLabel
        Left = 336
        Top = 4
        Width = 88
        Height = 13
        Caption = 'Vencimento inicial:'
      end
      object Label5: TLabel
        Left = 428
        Top = 4
        Width = 81
        Height = 13
        Caption = 'Vencimento final:'
      end
      object Label3: TLabel
        Left = 520
        Top = 4
        Width = 34
        Height = 13
        Caption = 'Banco:'
      end
      object EdCliente: TdmkEditCB
        Left = 8
        Top = 20
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 56
        Top = 20
        Width = 280
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECLIENTE'
        ListSource = DsClientes
        TabOrder = 1
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPIni: TDateTimePicker
        Left = 336
        Top = 20
        Width = 90
        Height = 21
        Date = 38855.863521944400000000
        Time = 38855.863521944400000000
        TabOrder = 2
      end
      object TPFim: TDateTimePicker
        Left = 428
        Top = 20
        Width = 90
        Height = 21
        Date = 38855.863521944400000000
        Time = 38855.863521944400000000
        TabOrder = 3
      end
      object EdBanco: TdmkEditCB
        Left = 520
        Top = 20
        Width = 45
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBBanco
        IgnoraDBLookupComboBox = False
      end
      object CBBanco: TdmkDBLookupComboBox
        Left = 568
        Top = 20
        Width = 217
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsBancos
        TabOrder = 5
        dmkEditCB = EdBanco
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 286
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 3
    object Label6: TLabel
      Left = 116
      Top = 4
      Width = 47
      Height = 13
      Caption = 'Registros:'
    end
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 682
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
    end
    object Edit1: TEdit
      Left = 116
      Top = 20
      Width = 69
      Height = 21
      Enabled = False
      TabOrder = 2
      Text = '0'
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMECLIENTE')
    Left = 261
    Top = 269
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 289
    Top = 269
  end
  object QrBancos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM bancos'
      'ORDER BY Nome')
    Left = 701
    Top = 269
    object QrBancosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrBancosSite: TWideStringField
      FieldName = 'Site'
      Size = 255
    end
    object QrBancosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBancosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBancosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBancosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBancosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBancosDVCC: TSmallintField
      FieldName = 'DVCC'
    end
  end
  object DsBancos: TDataSource
    DataSet = QrBancos
    Left = 729
    Top = 269
  end
  object QrPesq1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(loi.Valor) Valor, COUNT(loi.Controle) ITENS,'
      'SUM(IF(loi.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer,'
      'SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos,'
      
        'SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 1, 0)) IT' +
        'ENS_PgVencto,'
      'SUM(IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0)) +'
      
        'SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0)) IT' +
        'ENS_Devolvid,'
      
        'SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0)) IT' +
        'ENS_DevPgTot,'
      
        'SUM(IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0)) IT' +
        'ENS_DevNaoQt,'
      'SUM((loi.TotalPg - loi.TotalJr + loi.TotalDs)) Pago,'
      'SUM(IF(loi.DDeposito < SYSDATE(), loi.Valor, 0)) EXPIRADO,'
      'SUM(IF(loi.DDeposito < SYSDATE(), 0, loi.Valor)) AEXPIRAR,'
      'SUM(IF((loi.DDeposito<SYSDATE()) AND ((loi.DDeposito= 0) '
      '  OR (loi.DDeposito<loi.Data3)), loi.Valor, 0)) VENCIDO,'
      'SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), '
      '  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGDDEPOSITO,'
      'SUM(IF(/*(loi.Quitado> 1)AND*/ (loi.DDeposito< loi.Data3), '
      '  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGATRAZO,'
      'SUM(IF((loi.Quitado< 2)AND (loi.DDeposito< SYSDATE()),'
      
        '  loi.Valor - (loi.TotalPg - loi.TotalJr) + loi.TotalDs, 0)) PGA' +
        'BERTO,'
      'MONTH(loi.DDeposito)+YEAR(loi.DDeposito)*100+0.00 PERIODO,'
      
        'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOME' +
        'CLI,'
      'ban.Nome NOMEBANCO, loi.DDeposito, loi.Emitente NOMEEMITENTE'
      'FROM lotesits loi'
      'LEFT JOIN lotes     lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente'
      'LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco'
      'WHERE lot.Tipo=1'
      'AND loi.DDeposito  BETWEEN "2000/06/05" AND "2010/07/05"'
      'GROUP BY Periodo')
    Left = 496
    Top = 4
    object QrPesq1Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrPesq1Pago: TFloatField
      FieldName = 'Pago'
    end
    object QrPesq1PGDDeposito: TFloatField
      FieldName = 'PGDDeposito'
    end
    object QrPesq1PGATRAZO: TFloatField
      FieldName = 'PGATRAZO'
    end
    object QrPesq1PGABERTO: TFloatField
      FieldName = 'PGABERTO'
    end
    object QrPesq1PERIODO: TFloatField
      FieldName = 'PERIODO'
    end
    object QrPesq1NOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrPesq1NOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrPesq1DDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
    end
    object QrPesq1NOMEEMITENTE: TWideStringField
      FieldName = 'NOMEEMITENTE'
      Size = 50
    end
    object QrPesq1ITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrPesq1ITENS_AVencer: TFloatField
      FieldName = 'ITENS_AVencer'
    end
    object QrPesq1ITENS_Vencidos: TFloatField
      FieldName = 'ITENS_Vencidos'
    end
    object QrPesq1ITENS_PgVencto: TFloatField
      FieldName = 'ITENS_PgVencto'
    end
    object QrPesq1ITENS_Devolvid: TFloatField
      FieldName = 'ITENS_Devolvid'
    end
    object QrPesq1ITENS_DevPgTot: TFloatField
      FieldName = 'ITENS_DevPgTot'
    end
    object QrPesq1ITENS_DevNaoQt: TFloatField
      FieldName = 'ITENS_DevNaoQt'
    end
    object QrPesq1EXPIRADO: TFloatField
      FieldName = 'EXPIRADO'
    end
    object QrPesq1AEXPIRAR: TFloatField
      FieldName = 'AEXPIRAR'
    end
    object QrPesq1VENCIDO: TFloatField
      FieldName = 'VENCIDO'
    end
  end
  object QrDef: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MIN(DDeposito) Minimo, '
      'MAX(DDeposito) Maximo '
      'FROM lotesits')
    Left = 208
    Top = 433
    object QrDefMinimo: TDateField
      FieldName = 'Minimo'
    end
    object QrDefMaximo: TDateField
      FieldName = 'Maximo'
    end
  end
  object QrEmitCPF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome'
      'FROM emitcpf'
      'WHERE CPF=:P0')
    Left = 516
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmitCPFNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsEmitCPF: TDataSource
    DataSet = QrEmitCPF
    Left = 544
    Top = 280
  end
  object QrSacado: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome'
      'FROM sacados'
      'WHERE CNPJ=:P0')
    Left = 440
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacadoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsSacado: TDataSource
    DataSet = QrSacado
    Left = 468
    Top = 280
  end
  object frxInad1: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39719.659662361090000000
    ReportOptions.LastChange = 39719.659662361090000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '// Page header'
      '  if <VFR_SEQ1> = 0 then Memo12.Text := '#39'Cliente contratante'#39';'
      '  if <VFR_SEQ1> = 1 then Memo12.Text := '#39'Emitente'#39';'
      '  if <VFR_SEQ1> = 2 then Memo12.Text := '#39'Banco'#39';'
      '  if <VFR_SEQ1> = 3 then Memo12.Text := '#39'Per'#237'odo (M'#234's / Ano)'#39';'
      '  if <VFR_SEQ1> = 4 then Memo12.Text := '#39'Dia do dep'#243'sito'#39';'
      ''
      '// GH1'
      '  if <VFR_ORD1> > 0 then'
      '  GH1.Visible := True else GH1.Visible := False;'
      '  //'
      '  if <VFR_ORD1> > 0 then'
      '  GF1.Visible := True else GF1.Visible := False;'
      '  //'
      
        '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsPesq1."NOMECLI"'#39 +
        ';'
      
        '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsPesq1."NOMEEMITE' +
        'NTE"'#39';'
      
        '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsPesq1."NOMEBANCO' +
        '"'#39';'
      
        '  if <VFR_ORD1> = 4 then GH1.Condition := '#39'frxDsPesq1."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD1> = 5 then GH1.Condition := '#39'frxDsPesq1."loi.DDepo' +
        'sito"'#39';'
      '  //'
      ''
      '// GH2'
      '  if <VFR_ORD2> > 0 then'
      '  GH2.Visible := True else GH2.Visible := False;'
      '  //'
      '  if <VFR_ORD2> > 0 then'
      '  GF2.Visible := True else GF2.Visible := False;'
      '  //'
      
        '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsPesq1."NOMECLI"'#39 +
        ';'
      
        '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsPesq1."NOMEEMITE' +
        'NTE"'#39';'
      
        '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsPesq1."NOMEBANCO' +
        '"'#39';'
      
        '  if <VFR_ORD2> = 4 then GH2.Condition := '#39'frxDsPesq1."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD2> = 5 then GH2.Condition := '#39'frxDsPesq1."loi.DDepo' +
        'sito"'#39';'
      '  //'
      ''
      '// Master data'
      
        '  if <VFR_SEQ1> = 0 then Memo10.Text := '#39'[frxDsPesq1."NOMECLI"]'#39 +
        ';'
      
        '  if <VFR_SEQ1> = 1 then Memo10.Text := '#39'[frxDsPesq1."NOMEEMITEN' +
        'TE"]'#39';'
      
        '  if <VFR_SEQ1> = 2 then Memo10.Text := '#39'[frxDsPesq1."NOMEBANCO"' +
        ']'#39';'
      '  if <VFR_SEQ1> = 3 then Memo10.Text := '#39'[VARF_NOMEMES]'#39';'
      
        '  if <VFR_SEQ1> = 4 then Memo10.Text := '#39'[frxDsPesq1."DDeposito"' +
        ']'#39';'
      'end.')
    OnGetValue = frxInad1GetValue
    Left = 524
    Top = 32
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPesq1
        DataSetName = 'frxDsPesq1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 36.000000000000000000
        Top = 18.897650000000000000
        Width = 737.008350000000000000
        object Memo49: TfrxMemoView
          Left = 5.779530000000000000
          Top = 3.779530000000000000
          Width = 720.000000000000000000
          Height = 26.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 563.149970000000000000
        Width = 737.008350000000000000
        object Memo31: TfrxMemoView
          Left = 9.779530000000000000
          Top = 0.094000000000050910
          Width = 712.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME] - P'#225'g. [PAGE#] de [TOTALPAGES]'
            ' ')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 107.000000000000000000
        Top = 79.370130000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'PageHeader1OnBeforePrint'
        object Memo19: TfrxMemoView
          Left = 6.000000000000000000
          Top = 1.732220000000000000
          Width = 700.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[VFR_TITULO]')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 58.000000000000000000
          Top = 29.732220000000000000
          Width = 408.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 6.000000000000000000
          Top = 29.732220000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 574.000000000000000000
          Top = 29.732220000000000000
          Width = 132.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 470.000000000000000000
          Top = 29.732220000000000000
          Width = 104.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo vencimento:')
          ParentFont = False
          WordWrap = False
        end
        object Memo87: TfrxMemoView
          Left = 58.000000000000000000
          Top = 50.732220000000000000
          Width = 316.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_EMITENTE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          Left = 6.000000000000000000
          Top = 50.732220000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Emitente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo89: TfrxMemoView
          Left = 410.000000000000000000
          Top = 50.732220000000000000
          Width = 296.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_BANCO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          Left = 374.000000000000000000
          Top = 50.732220000000000000
          Width = 36.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Banco:')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Top = 75.590600000000000000
          Width = 136.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente contratante')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 284.000000000000000000
          Top = 75.590600000000000000
          Width = 60.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'A'
            'Expirar')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 372.000000000000000000
          Top = 75.590600000000000000
          Width = 60.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pago'
            'Vencimento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 544.000000000000000000
          Top = 75.590600000000000000
          Width = 60.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Revendido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 632.000000000000000000
          Top = 75.590600000000000000
          Width = 60.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Pendente')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 136.000000000000000000
          Top = 75.590600000000000000
          Width = 60.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Documento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 196.000000000000000000
          Top = 75.590600000000000000
          Width = 60.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Expirado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 256.000000000000000000
          Top = 75.590600000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Expir')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 344.000000000000000000
          Top = 75.590600000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'A E.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 432.000000000000000000
          Top = 75.590600000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Exp')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 604.000000000000000000
          Top = 75.590600000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Venc.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 692.000000000000000000
          Top = 75.590600000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Venc.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 460.000000000000000000
          Top = 75.590600000000000000
          Width = 56.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Vencido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 516.000000000000000000
          Top = 75.590600000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Exp')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DadosMestre1: TfrxMasterData
        Height = 17.000000000000000000
        Top = 328.819110000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'DadosMestre1OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPesq1
        DataSetName = 'frxDsPesq1'
        RowCount = 0
        object Memo10: TfrxMemoView
          Width = 136.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 284.000000000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."AEXPIRAR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 544.000000000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."PGATRAZO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 632.000000000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."PGABERTO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 372.000000000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."PGDDeposito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 196.000000000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."EXPIRADO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 136.000000000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 256.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."Valor"> = 0, 0, '
            '<frxDsPesq1."EXPIRADO">/<frxDsPesq1."Valor">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 344.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."Valor"> = 0, 0, '
            '<frxDsPesq1."AEXPIRAR">/<frxDsPesq1."Valor">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 432.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."EXPIRADO"> = 0, 0,'
            '<frxDsPesq1."PGDDeposito">/<frxDsPesq1."EXPIRADO">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 604.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."VENCIDO"> = 0, 0, '
            '<frxDsPesq1."PGATRAZO">/<frxDsPesq1."VENCIDO">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 692.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."VENCIDO"> = 0, 0,'
            '<frxDsPesq1."PGABERTO">/<frxDsPesq1."VENCIDO">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 460.000000000000000000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."VENCIDO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 516.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."EXPIRADO"> = 0, 0,'
            '<frxDsPesq1."VENCIDO">/<frxDsPesq1."EXPIRADO">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 32.000000000000000000
        Top = 506.457020000000000000
        Width = 737.008350000000000000
        object Memo36: TfrxMemoView
          Top = 3.779530000000020000
          Width = 136.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Left = 284.000000000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."AEXPIRAR">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 544.000000000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGATRAZO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 632.000000000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGABERTO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 372.000000000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGDDeposito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 196.000000000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."EXPIRADO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 136.000000000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 256.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."Valor">) = 0, 0, '
            '(SUM(<frxDsPesq1."EXPIRADO">) / SUM(<frxDsPesq1."Valor">) * 100)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 344.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."Valor">) = 0, 0, '
            '(SUM(<frxDsPesq1."AEXPIRAR">) / SUM(<frxDsPesq1."Valor">) * 100)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Left = 432.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."EXPIRADO">) = 0, 0, '
            
              '(SUM(<frxDsPesq1."PGDDeposito">) / SUM(<frxDsPesq1."EXPIRADO">) ' +
              '* 100)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 604.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."VENCIDO">) = 0, 0,'
            
              '(SUM(<frxDsPesq1."PGATRAZO">) / SUM(<frxDsPesq1."VENCIDO">) * 10' +
              '0)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 692.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."VENCIDO">) = 0, 0,'
            
              '(SUM(<frxDsPesq1."PGABERTO">) / SUM(<frxDsPesq1."VENCIDO">) * 10' +
              '0)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Left = 460.000000000000000000
          Top = 3.779530000000020000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."VENCIDO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Left = 516.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."EXPIRADO">) = 0, 0,'
            
              '(SUM(<frxDsPesq1."VENCIDO">) / SUM(<frxDsPesq1."EXPIRADO">) * 10' +
              '0)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 245.669450000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsPesq1."NOMECLI"'
        object Memo24: TfrxMemoView
          Left = 1.779530000000000000
          Width = 712.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 287.244280000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsPesq1."NOMEBANCO"'
        object Memo30: TfrxMemoView
          Left = 11.338582677165400000
          Width = 680.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 370.393940000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo25: TfrxMemoView
          Left = 11.338582677165400000
          Top = 4.472170000000010000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA2NOME]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 284.063080000000000000
          Top = 7.559059999999990000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."AEXPIRAR">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 544.063080000000000000
          Top = 7.559059999999990000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGATRAZO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 632.063080000000000000
          Top = 7.559059999999990000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGABERTO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 372.063080000000000000
          Top = 7.559059999999990000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGDDeposito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 196.063080000000000000
          Top = 7.559059999999990000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."EXPIRADO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 136.063080000000000000
          Top = 7.559059999999990000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 256.063080000000000000
          Top = 7.559059999999990000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."Valor">) = 0, 0, '
            '(SUM(<frxDsPesq1."EXPIRADO">) / SUM(<frxDsPesq1."Valor">) * 100)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 344.063080000000000000
          Top = 7.559059999999990000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."Valor">) = 0, 0, '
            '(SUM(<frxDsPesq1."AEXPIRAR">) / SUM(<frxDsPesq1."Valor">) * 100)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Left = 432.063080000000000000
          Top = 7.559059999999990000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."EXPIRADO">) = 0, 0, '
            
              '(SUM(<frxDsPesq1."PGDDeposito">) / SUM(<frxDsPesq1."EXPIRADO">) ' +
              '* 100)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 604.063080000000000000
          Top = 7.559059999999990000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."VENCIDO">) = 0, 0,'
            
              '(SUM(<frxDsPesq1."PGATRAZO">) / SUM(<frxDsPesq1."VENCIDO">) * 10' +
              '0)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 692.063080000000000000
          Top = 7.559059999999990000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."VENCIDO">) = 0, 0,'
            
              '(SUM(<frxDsPesq1."PGABERTO">) / SUM(<frxDsPesq1."VENCIDO">) * 10' +
              '0)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 460.063080000000000000
          Top = 7.559059999999990000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."VENCIDO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 516.063080000000000000
          Top = 7.559059999999990000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."EXPIRADO">) = 0, 0,'
            
              '(SUM(<frxDsPesq1."VENCIDO">) / SUM(<frxDsPesq1."EXPIRADO">) * 10' +
              '0)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 419.527830000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo27: TfrxMemoView
          Top = 3.558749999999970000
          Width = 136.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA1NOME]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 284.063080000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."AEXPIRAR">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 544.063080000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGATRAZO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 632.063080000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGABERTO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 372.063080000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGDDeposito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 196.063080000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."EXPIRADO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 136.063080000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 256.063080000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."Valor">) = 0, 0, '
            '(SUM(<frxDsPesq1."EXPIRADO">) / SUM(<frxDsPesq1."Valor">) * 100)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 344.063080000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."Valor">) = 0, 0, '
            '(SUM(<frxDsPesq1."AEXPIRAR">) / SUM(<frxDsPesq1."Valor">) * 100)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 432.063080000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."EXPIRADO">) = 0, 0, '
            
              '(SUM(<frxDsPesq1."PGDDeposito">) / SUM(<frxDsPesq1."EXPIRADO">) ' +
              '* 100)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 604.063080000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."VENCIDO">) = 0, 0,'
            
              '(SUM(<frxDsPesq1."PGATRAZO">) / SUM(<frxDsPesq1."VENCIDO">) * 10' +
              '0)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 692.063080000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."VENCIDO">) = 0, 0,'
            
              '(SUM(<frxDsPesq1."PGABERTO">) / SUM(<frxDsPesq1."VENCIDO">) * 10' +
              '0)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 460.063080000000000000
          Top = 3.779530000000020000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."VENCIDO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 516.063080000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."EXPIRADO">) = 0, 0,'
            
              '(SUM(<frxDsPesq1."VENCIDO">) / SUM(<frxDsPesq1."EXPIRADO">) * 10' +
              '0)'
            ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsPesq1: TfrxDBDataset
    UserName = 'frxDsPesq1'
    CloseDataSource = False
    DataSet = QrPesq1
    BCDToCurrency = False
    Left = 496
    Top = 32
  end
  object frxInad2: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39719.692945601900000000
    ReportOptions.LastChange = 39719.692945601900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '// Page header'
      '  if <VFR_SEQ1> = 0 then Memo12.Text := '#39'Cliente contratante'#39';'
      '  if <VFR_SEQ1> = 1 then Memo12.Text := '#39'Emitente'#39';'
      '  if <VFR_SEQ1> = 2 then Memo12.Text := '#39'Banco'#39';'
      '  if <VFR_SEQ1> = 3 then Memo12.Text := '#39'Per'#237'odo (M'#234's / Ano)'#39';'
      '  if <VFR_SEQ1> = 4 then Memo12.Text := '#39'Dia do dep'#243'sito'#39';'
      ''
      '// GH1'
      '  if <VFR_ORD1> > 0 then'
      '  GH1.Visible := True else GH1.Visible := False;'
      '  //'
      '  if <VFR_ORD1> > 0 then'
      '  GF1.Visible := True else GF1.Visible := False;'
      '  //'
      
        '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsPesq1."NOMECLI"'#39 +
        ';'
      
        '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsPesq1."NOMEEMITE' +
        'NTE"'#39';'
      
        '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsPesq1."NOMEBANCO' +
        '"'#39';'
      
        '  if <VFR_ORD1> = 4 then GH1.Condition := '#39'frxDsPesq1."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD1> = 5 then GH1.Condition := '#39'frxDsPesq1."loi.DDepo' +
        'sito"'#39';'
      '  //'
      ''
      '// GH2'
      '  if <VFR_ORD2> > 0 then'
      '  GH2.Visible := True else GH2.Visible := False;'
      '  //'
      '  if <VFR_ORD2> > 0 then'
      '  GF2.Visible := True else GF2.Visible := False;'
      '  //'
      
        '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsPesq1."NOMECLI"'#39 +
        ';'
      
        '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsPesq1."NOMEEMITE' +
        'NTE"'#39';'
      
        '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsPesq1."NOMEBANCO' +
        '"'#39';'
      
        '  if <VFR_ORD2> = 4 then GH2.Condition := '#39'frxDsPesq1."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD2> = 5 then GH2.Condition := '#39'frxDsPesq1."loi.DDepo' +
        'sito"'#39';'
      '  //'
      ''
      '// Master data'
      
        '  if <VFR_SEQ1> = 0 then Memo10.Text := '#39'[frxDsPesq1."NOMECLI"]'#39 +
        ';'
      
        '  if <VFR_SEQ1> = 1 then Memo10.Text := '#39'[frxDsPesq1."NOMEEMITEN' +
        'TE"]'#39';'
      
        '  if <VFR_SEQ1> = 2 then Memo10.Text := '#39'[frxDsPesq1."NOMEBANCO"' +
        ']'#39';'
      '  if <VFR_SEQ1> = 3 then Memo10.Text := '#39'[VARF_NOMEMES]'#39';'
      
        '  if <VFR_SEQ1> = 4 then Memo10.Text := '#39'[frxDsPesq1."DDeposito"' +
        ']'#39';'
      'end.')
    OnGetValue = frxInad1GetValue
    Left = 524
    Top = 4
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPesq1
        DataSetName = 'frxDsPesq1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 36.000000000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Memo49: TfrxMemoView
          Left = 9.559060000000000000
          Top = 1.984230000000000000
          Width = 1003.464750000000000000
          Height = 26.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 563.149970000000000000
        Width = 1046.929810000000000000
        object Memo31: TfrxMemoView
          Left = 308.362400000000000000
          Top = 7.653059999999980000
          Width = 712.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME] - P'#225'g. [PAGE#] de [TOTALPAGES]'
            ' ')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 105.511750000000000000
        Top = 79.370130000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'PageHeader1OnBeforePrint'
        object Memo19: TfrxMemoView
          Left = 6.000000000000000000
          Top = 5.511750000000010000
          Width = 700.000000000000000000
          Height = 22.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE INADIMPL'#202'NCIAS DE DUPLICATAS')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = -2.000000000000000000
          Top = 77.511750000000000000
          Width = 124.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente contratante')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 302.000000000000000000
          Top = 77.511750000000000000
          Width = 60.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'A'
            'Expirar')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 58.000000000000000000
          Top = 33.511750000000000000
          Width = 408.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 6.000000000000000000
          Top = 33.511750000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 574.000000000000000000
          Top = 33.511750000000000000
          Width = 132.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 470.000000000000000000
          Top = 33.511750000000000000
          Width = 104.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo vencimento:')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 390.000000000000000000
          Top = 77.511750000000000000
          Width = 60.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pago'
            'Vencimento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 566.000000000000000000
          Top = 77.511750000000000000
          Width = 60.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Devolvido'
            'Pago')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 654.000000000000000000
          Top = 77.511750000000000000
          Width = 60.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Pendente')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 154.000000000000000000
          Top = 77.511750000000000000
          Width = 60.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Documento')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 214.000000000000000000
          Top = 77.511750000000000000
          Width = 60.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Expirado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 274.000000000000000000
          Top = 77.511750000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Val')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 362.000000000000000000
          Top = 77.511750000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Val')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 450.000000000000000000
          Top = 77.511750000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Expir')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 626.000000000000000000
          Top = 77.511750000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Devol')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 714.000000000000000000
          Top = 77.511750000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Devol')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo87: TfrxMemoView
          Left = 58.000000000000000000
          Top = 54.511750000000000000
          Width = 408.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_EMITENTE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo88: TfrxMemoView
          Left = 6.000000000000000000
          Top = 54.511750000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Emitente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo89: TfrxMemoView
          Left = 506.000000000000000000
          Top = 54.511750000000000000
          Width = 524.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_BANCO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo90: TfrxMemoView
          Left = 470.000000000000000000
          Top = 54.511750000000000000
          Width = 36.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Banco:')
          ParentFont = False
          WordWrap = False
        end
        object Memo32: TfrxMemoView
          Left = 750.000000000000000000
          Top = 77.511750000000000000
          Width = 32.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde'
            'Dev')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo91: TfrxMemoView
          Left = 838.000000000000000000
          Top = 77.511750000000000000
          Width = 32.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde'
            'DevPg')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo93: TfrxMemoView
          Left = 870.000000000000000000
          Top = 77.511750000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo94: TfrxMemoView
          Left = 898.000000000000000000
          Top = 77.511750000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Expir')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo95: TfrxMemoView
          Left = 926.000000000000000000
          Top = 77.511750000000000000
          Width = 32.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde'
            'Pend.')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo96: TfrxMemoView
          Left = 958.000000000000000000
          Top = 77.511750000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo97: TfrxMemoView
          Left = 986.000000000000000000
          Top = 77.511750000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Expir')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo98: TfrxMemoView
          Left = 122.000000000000000000
          Top = 77.511750000000000000
          Width = 32.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Qtde'
            'Docs')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo53: TfrxMemoView
          Left = 782.000000000000000000
          Top = 77.511750000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Total')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo92: TfrxMemoView
          Left = 810.000000000000000000
          Top = 77.511750000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Expir')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 478.000000000000000000
          Top = 77.511750000000000000
          Width = 60.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Devolvido')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 538.000000000000000000
          Top = 77.511750000000000000
          Width = 28.000000000000000000
          Height = 28.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '%'
            'Expir')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DadosMestre1: TfrxMasterData
        Height = 17.000000000000000000
        Top = 328.819110000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'DadosMestre1OnBeforePrint'
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPesq1
        DataSetName = 'frxDsPesq1'
        RowCount = 0
        object Memo10: TfrxMemoView
          Width = 122.078740160000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 302.000000000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."AEXPIRAR"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 566.000000000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."PGATRAZO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 654.000000000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."PGABERTO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 390.000000000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."PGDDeposito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 214.000000000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."EXPIRADO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 154.000000000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo3: TfrxMemoView
          Left = 274.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."Valor"> = 0, 0, '
            '<frxDsPesq1."EXPIRADO">/<frxDsPesq1."Valor">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 362.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."Valor"> = 0, 0, '
            '<frxDsPesq1."AEXPIRAR">/<frxDsPesq1."Valor">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 450.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."EXPIRADO"> = 0, 0, '
            '<frxDsPesq1."PGDDeposito">/<frxDsPesq1."EXPIRADO">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 626.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."VENCIDO"> = 0, 0, '
            '<frxDsPesq1."PGATRAZO">/<frxDsPesq1."VENCIDO">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 714.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."VENCIDO"> = 0, 0, '
            '<frxDsPesq1."PGABERTO">/<frxDsPesq1."VENCIDO">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo100: TfrxMemoView
          Left = 750.000000000000000000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsPesq1."ITENS_Devolvid">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo99: TfrxMemoView
          Left = 782.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."ITENS"> = 0, 0, '
            '<frxDsPesq1."ITENS_Devolvid">/<frxDsPesq1."ITENS">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo101: TfrxMemoView
          Left = 810.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."ITENS_Vencidos"> = 0, 0, '
            
              '<frxDsPesq1."ITENS_Devolvid"> / <frxDsPesq1."ITENS_Vencidos"> * ' +
              '100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo102: TfrxMemoView
          Left = 122.000000000000000000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsPesq1."ITENS">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo103: TfrxMemoView
          Left = 838.000000000000000000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsPesq1."ITENS_DevPgTot">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo104: TfrxMemoView
          Left = 870.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."ITENS"> = 0, 0, '
            '<frxDsPesq1."ITENS_DevPgTot">/<frxDsPesq1."ITENS">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo105: TfrxMemoView
          Left = 898.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."ITENS_Vencidos"> = 0, 0, '
            
              '<frxDsPesq1."ITENS_DevPgTot">/<frxDsPesq1."ITENS_Vencidos">*100)' +
              ']')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo106: TfrxMemoView
          Left = 926.000000000000000000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', <frxDsPesq1."ITENS_DevNaoQt">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo107: TfrxMemoView
          Left = 958.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."ITENS"> = 0, 0, '
            '<frxDsPesq1."ITENS_DevNaoQt">/<frxDsPesq1."ITENS">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo108: TfrxMemoView
          Left = 986.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(<frxDsPesq1."ITENS_Vencidos"> = 0, 0, <frxDsPesq1."ITENS_De' +
              'vNaoQt">/<frxDsPesq1."ITENS_Vencidos">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 478.000000000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."VENCIDO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 538.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(<frxDsPesq1."EXPIRADO"> = 0, 0, '
            '<frxDsPesq1."VENCIDO">/<frxDsPesq1."EXPIRADO">*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 32.000000000000000000
        Top = 506.457020000000000000
        Width = 1046.929810000000000000
        object Memo25: TfrxMemoView
          Top = 3.779530000000020000
          Width = 121.732282000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 302.000000000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."AEXPIRAR">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 566.000000000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGATRAZO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 654.000000000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGABERTO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 390.000000000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGDDeposito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 214.000000000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."EXPIRADO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 154.000000000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 274.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."Valor">) = 0, 0, '
            'SUM(<frxDsPesq1."EXPIRADO">)/SUM(<frxDsPesq1."Valor">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 362.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."Valor">) = 0, 0, '
            'SUM(<frxDsPesq1."AEXPIRAR">)/SUM(<frxDsPesq1."Valor">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 450.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."EXPIRADO">) = 0, 0, '
            
              'SUM(<frxDsPesq1."PGDDeposito">)/SUM(<frxDsPesq1."EXPIRADO">)*100' +
              ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 626.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."VENCIDO">) = 0, 0, '
            'SUM(<frxDsPesq1."PGATRAZO">)/SUM(<frxDsPesq1."VENCIDO">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 714.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."VENCIDO">) = 0, 0, '
            'SUM(<frxDsPesq1."PGABERTO">)/SUM(<frxDsPesq1."VENCIDO">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 750.000000000000000000
          Top = 3.779530000000020000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', SUM(<frxDsPesq1."ITENS_Devolvid">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 782.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."ITENS">) = 0, 0, '
            
              'SUM(<frxDsPesq1."ITENS_Devolvid">)/SUM(<frxDsPesq1."ITENS">)*100' +
              ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Left = 810.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."ITENS_Vencidos">) = 0, 0, '
            
              'SUM(<frxDsPesq1."ITENS_Devolvid">) / SUM(<frxDsPesq1."ITENS_Venc' +
              'idos">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 122.000000000000000000
          Top = 3.779530000000020000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', SUM(<frxDsPesq1."ITENS">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 838.000000000000000000
          Top = 3.779530000000020000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', SUM(<frxDsPesq1."ITENS_DevPgTot">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 870.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."ITENS">) = 0, 0, '
            
              'SUM(<frxDsPesq1."ITENS_DevPgTot">)/SUM(<frxDsPesq1."ITENS">)*100' +
              ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 898.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."ITENS_Vencidos">) = 0, 0, '
            
              'SUM(<frxDsPesq1."ITENS_DevPgTot">)/SUM(<frxDsPesq1."ITENS_Vencid' +
              'os">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 926.000000000000000000
          Top = 3.779530000000020000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', SUM(<frxDsPesq1."ITENS_DevNaoQt">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 958.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."ITENS">) = 0, 0, '
            
              'SUM(<frxDsPesq1."ITENS_DevNaoQt">)/SUM(<frxDsPesq1."ITENS">)*100' +
              ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo61: TfrxMemoView
          Left = 986.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPesq1."ITENS_Vencidos">) = 0, 0, SUM(<frxDsPesq1.' +
              '"ITENS_DevNaoQt">)/SUM(<frxDsPesq1."ITENS_Vencidos">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 478.000000000000000000
          Top = 3.779530000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."VENCIDO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 538.000000000000000000
          Top = 3.779530000000020000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."EXPIRADO">) = 0, 0, '
            'SUM(<frxDsPesq1."VENCIDO">)/SUM(<frxDsPesq1."EXPIRADO">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 245.669450000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsPesq1."NOMECLI"'
        object Memo24: TfrxMemoView
          Left = 1.779530000000000000
          Width = 712.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        Height = 18.000000000000000000
        Top = 287.244280000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsPesq1."NOMEBANCO"'
        object Memo30: TfrxMemoView
          Left = 10.000000000000000000
          Width = 680.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 370.393940000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo109: TfrxMemoView
          Left = 302.078819000000000000
          Top = 3.779527559055110000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."AEXPIRAR">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo110: TfrxMemoView
          Left = 566.078819000000000000
          Top = 3.779527559055110000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGATRAZO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo111: TfrxMemoView
          Left = 654.078819000000000000
          Top = 3.779527559055110000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGABERTO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo112: TfrxMemoView
          Left = 390.078819000000000000
          Top = 3.779527559055110000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGDDeposito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo113: TfrxMemoView
          Left = 214.078819000000000000
          Top = 3.779527559055110000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."EXPIRADO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo114: TfrxMemoView
          Left = 154.078819000000000000
          Top = 3.779527559055110000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo115: TfrxMemoView
          Left = 274.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."Valor">) = 0, 0, '
            'SUM(<frxDsPesq1."EXPIRADO">)/SUM(<frxDsPesq1."Valor">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo116: TfrxMemoView
          Left = 362.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."Valor">) = 0, 0, '
            'SUM(<frxDsPesq1."AEXPIRAR">)/SUM(<frxDsPesq1."Valor">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo117: TfrxMemoView
          Left = 450.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."EXPIRADO">) = 0, 0, '
            
              'SUM(<frxDsPesq1."PGDDeposito">)/SUM(<frxDsPesq1."EXPIRADO">)*100' +
              ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo118: TfrxMemoView
          Left = 626.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."VENCIDO">) = 0, 0, '
            'SUM(<frxDsPesq1."PGATRAZO">)/SUM(<frxDsPesq1."VENCIDO">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo119: TfrxMemoView
          Left = 714.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."VENCIDO">) = 0, 0, '
            'SUM(<frxDsPesq1."PGABERTO">)/SUM(<frxDsPesq1."VENCIDO">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo120: TfrxMemoView
          Left = 750.078819000000000000
          Top = 3.779527559055110000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', SUM(<frxDsPesq1."ITENS_Devolvid">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo121: TfrxMemoView
          Left = 782.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."ITENS">) = 0, 0, '
            
              'SUM(<frxDsPesq1."ITENS_Devolvid">)/SUM(<frxDsPesq1."ITENS">)*100' +
              ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo122: TfrxMemoView
          Left = 810.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."ITENS_Vencidos">) = 0, 0, '
            
              'SUM(<frxDsPesq1."ITENS_Devolvid">) / SUM(<frxDsPesq1."ITENS_Venc' +
              'idos">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo123: TfrxMemoView
          Left = 122.078819000000000000
          Top = 3.779527559055110000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', SUM(<frxDsPesq1."ITENS">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo124: TfrxMemoView
          Left = 838.078819000000000000
          Top = 3.779527559055110000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', SUM(<frxDsPesq1."ITENS_DevPgTot">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo125: TfrxMemoView
          Left = 870.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."ITENS">) = 0, 0, '
            
              'SUM(<frxDsPesq1."ITENS_DevPgTot">)/SUM(<frxDsPesq1."ITENS">)*100' +
              ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo126: TfrxMemoView
          Left = 898.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."ITENS_Vencidos">) = 0, 0, '
            
              'SUM(<frxDsPesq1."ITENS_DevPgTot">)/SUM(<frxDsPesq1."ITENS_Vencid' +
              'os">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo127: TfrxMemoView
          Left = 926.078819000000000000
          Top = 3.779527559055110000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', SUM(<frxDsPesq1."ITENS_DevNaoQt">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo128: TfrxMemoView
          Left = 958.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."ITENS">) = 0, 0, '
            
              'SUM(<frxDsPesq1."ITENS_DevNaoQt">)/SUM(<frxDsPesq1."ITENS">)*100' +
              ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo129: TfrxMemoView
          Left = 986.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPesq1."ITENS_Vencidos">) = 0, 0, SUM(<frxDsPesq1.' +
              '"ITENS_DevNaoQt">)/SUM(<frxDsPesq1."ITENS_Vencidos">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo130: TfrxMemoView
          Left = 478.078819000000000000
          Top = 3.779527559055110000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."VENCIDO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo131: TfrxMemoView
          Left = 538.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."EXPIRADO">) = 0, 0, '
            'SUM(<frxDsPesq1."VENCIDO">)/SUM(<frxDsPesq1."EXPIRADO">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo132: TfrxMemoView
          Top = 3.779527559055110000
          Width = 122.078740160000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        Height = 26.000000000000000000
        Top = 419.527830000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo64: TfrxMemoView
          Left = 302.078819000000000000
          Top = 3.779527559055110000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."AEXPIRAR">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 566.078819000000000000
          Top = 3.779527559055110000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGATRAZO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 654.078819000000000000
          Top = 3.779527559055110000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGABERTO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 390.078819000000000000
          Top = 3.779527559055110000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."PGDDeposito">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 214.078819000000000000
          Top = 3.779527559055110000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."EXPIRADO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 154.078819000000000000
          Top = 3.779527559055110000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."Valor">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 274.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."Valor">) = 0, 0, '
            'SUM(<frxDsPesq1."EXPIRADO">)/SUM(<frxDsPesq1."Valor">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 362.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."Valor">) = 0, 0, '
            'SUM(<frxDsPesq1."AEXPIRAR">)/SUM(<frxDsPesq1."Valor">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 450.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."EXPIRADO">) = 0, 0, '
            
              'SUM(<frxDsPesq1."PGDDeposito">)/SUM(<frxDsPesq1."EXPIRADO">)*100' +
              ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 626.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."VENCIDO">) = 0, 0, '
            'SUM(<frxDsPesq1."PGATRAZO">)/SUM(<frxDsPesq1."VENCIDO">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 714.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."VENCIDO">) = 0, 0, '
            'SUM(<frxDsPesq1."PGABERTO">)/SUM(<frxDsPesq1."VENCIDO">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Left = 750.078819000000000000
          Top = 3.779527559055110000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', SUM(<frxDsPesq1."ITENS_Devolvid">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 782.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."ITENS">) = 0, 0, '
            
              'SUM(<frxDsPesq1."ITENS_Devolvid">)/SUM(<frxDsPesq1."ITENS">)*100' +
              ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 810.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."ITENS_Vencidos">) = 0, 0, '
            
              'SUM(<frxDsPesq1."ITENS_Devolvid">) / SUM(<frxDsPesq1."ITENS_Venc' +
              'idos">) * 100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Left = 122.078819000000000000
          Top = 3.779527559055110000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', SUM(<frxDsPesq1."ITENS">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Left = 838.078819000000000000
          Top = 3.779527559055110000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', SUM(<frxDsPesq1."ITENS_DevPgTot">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          Left = 870.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."ITENS">) = 0, 0, '
            
              'SUM(<frxDsPesq1."ITENS_DevPgTot">)/SUM(<frxDsPesq1."ITENS">)*100' +
              ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo81: TfrxMemoView
          Left = 898.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."ITENS_Vencidos">) = 0, 0, '
            
              'SUM(<frxDsPesq1."ITENS_DevPgTot">)/SUM(<frxDsPesq1."ITENS_Vencid' +
              'os">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo82: TfrxMemoView
          Left = 926.078819000000000000
          Top = 3.779527559055110000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[FormatFloat('#39'0;-0; '#39', SUM(<frxDsPesq1."ITENS_DevNaoQt">))]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo83: TfrxMemoView
          Left = 958.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."ITENS">) = 0, 0, '
            
              'SUM(<frxDsPesq1."ITENS_DevNaoQt">)/SUM(<frxDsPesq1."ITENS">)*100' +
              ')]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          Left = 986.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[IIF(SUM(<frxDsPesq1."ITENS_Vencidos">) = 0, 0, SUM(<frxDsPesq1.' +
              '"ITENS_DevNaoQt">)/SUM(<frxDsPesq1."ITENS_Vencidos">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          Left = 478.078819000000000000
          Top = 3.779527559055110000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."VENCIDO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Left = 538.078819000000000000
          Top = 3.779527559055110000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[IIF(SUM(<frxDsPesq1."EXPIRADO">) = 0, 0, '
            'SUM(<frxDsPesq1."VENCIDO">)/SUM(<frxDsPesq1."EXPIRADO">)*100)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo133: TfrxMemoView
          Top = 3.779527559055110000
          Width = 122.078740160000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
end
