object FmRepasDU: TFmRepasDU
  Left = 368
  Top = 194
  Caption = 'Repasses de Duplicatas'
  ClientHeight = 496
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 448
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Panel1: TPanel
      Left = 1
      Top = 53
      Width = 790
      Height = 296
      Align = alTop
      Color = clMaroon
      TabOrder = 1
      object PainelItens: TPanel
        Left = 1
        Top = 84
        Width = 788
        Height = 211
        Align = alBottom
        Color = clAppWorkSpace
        TabOrder = 0
        Visible = False
        object GradeCHs: TDBGrid
          Left = 1
          Top = 89
          Width = 786
          Height = 121
          Align = alClient
          DataSource = DsCheques
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Lote'
              Title.Caption = 'Border'#244
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Duplicata'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Banco'
              Title.Caption = 'Bco.'
              Width = 27
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Agencia'
              Title.Caption = 'Ag.'
              Width = 31
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DCompra'
              Title.Caption = 'D. Compra'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DDeposito'
              Title.Caption = 'D. Dep'#243'sito'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CPF_TXT'
              Title.Caption = 'CPF / CNPJ'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Width = 255
              Visible = True
            end>
        end
        object Panel4: TPanel
          Left = 1
          Top = 1
          Width = 786
          Height = 88
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object Label11: TLabel
            Left = 8
            Top = 44
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object Label13: TLabel
            Left = 8
            Top = 4
            Width = 48
            Height = 13
            Caption = 'Duplicata:'
          end
          object Label8: TLabel
            Left = 124
            Top = 4
            Width = 67
            Height = 13
            Caption = 'CPF Emitente:'
          end
          object Label14: TLabel
            Left = 368
            Top = 44
            Width = 54
            Height = 13
            Caption = 'Taxa [F12]:'
          end
          object Label15: TLabel
            Left = 320
            Top = 44
            Width = 40
            Height = 13
            Caption = 'Border'#244':'
          end
          object EdCliente: TdmkEditCB
            Left = 8
            Top = 60
            Width = 45
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnChange = EdClienteChange
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 56
            Top = 60
            Width = 261
            Height = 21
            Color = clWhite
            KeyField = 'Codigo'
            ListField = 'NOMECLIENTE'
            ListSource = DsClientes
            TabOrder = 3
            dmkEditCB = EdCliente
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdDuplicata: TdmkEdit
            Left = 8
            Top = 20
            Width = 113
            Height = 21
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnExit = EdDuplicataExit
          end
          object EdCNPJ: TdmkEdit
            Left = 124
            Top = 20
            Width = 113
            Height = 21
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnExit = EdCNPJExit
          end
          object EdTaxa: TdmkEdit
            Left = 368
            Top = 60
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnKeyDown = EdTaxaKeyDown
          end
          object CkInap: TCheckBox
            Left = 436
            Top = 60
            Width = 149
            Height = 17
            Caption = 'Mostrar cheques vencidos.'
            TabOrder = 6
            Visible = False
            OnClick = CkInapClick
          end
          object EdBordero: TdmkEdit
            Left = 320
            Top = 60
            Width = 45
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnExit = EdBorderoExit
          end
          object Memo1: TMemo
            Left = 588
            Top = 1
            Width = 197
            Height = 86
            Align = alRight
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Lines.Strings = (
              'Ctrl + Mouse '
              'seleciona mais'
              'de uma duplicata.')
            ParentFont = False
            ReadOnly = True
            TabOrder = 7
          end
        end
      end
      object GradeRepas: TDBGrid
        Left = 1
        Top = 1
        Width = 788
        Height = 59
        Align = alTop
        DataSource = DsRepasIts
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Duplicata'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Banco'
            Title.Caption = 'Bco.'
            Width = 27
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Agencia'
            Title.Caption = 'Ag.'
            Width = 31
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Taxa'
            Title.Caption = '% Taxa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'JurosP'
            Title.Caption = '% Juros'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'JurosV'
            Title.Caption = '$ Juros'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'LIQUIDO'
            Title.Caption = 'L'#205'QUIDO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CPF_TXT'
            Title.Caption = 'CPF / CNPJ'
            Width = 120
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Emitente'
            Width = 172
            Visible = True
          end>
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 52
      Align = alTop
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 76
        Top = 8
        Width = 44
        Height = 13
        Caption = 'Coligado:'
        FocusControl = DBEdNome
      end
      object Label4: TLabel
        Left = 388
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Data:'
        FocusControl = DBEdit1
      end
      object Label5: TLabel
        Left = 456
        Top = 8
        Width = 53
        Height = 13
        Caption = 'Valor base:'
        FocusControl = DBEdit2
      end
      object Label6: TLabel
        Left = 532
        Top = 8
        Width = 37
        Height = 13
        Caption = '$ Juros:'
        FocusControl = DBEdit3
      end
      object Label7: TLabel
        Left = 608
        Top = 8
        Width = 39
        Height = 13
        Caption = 'L'#237'quido:'
        FocusControl = DBEdit4
      end
      object DBEdCodigo: TDBEdit
        Left = 8
        Top = 24
        Width = 65
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsRepas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 76
        Top = 24
        Width = 309
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NOMECOLIGADO'
        DataSource = DsRepas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 388
        Top = 24
        Width = 64
        Height = 21
        DataField = 'Data'
        DataSource = DsRepas
        TabOrder = 2
      end
      object DBEdit2: TDBEdit
        Left = 456
        Top = 24
        Width = 72
        Height = 21
        DataField = 'Total'
        DataSource = DsRepas
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 532
        Top = 24
        Width = 72
        Height = 21
        DataField = 'JurosV'
        DataSource = DsRepas
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 608
        Top = 24
        Width = 72
        Height = 21
        DataField = 'SALDO'
        DataSource = DsRepas
        TabOrder = 5
      end
      object Progress: TProgressBar
        Left = 684
        Top = 28
        Width = 101
        Height = 17
        TabOrder = 6
        Visible = False
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 399
      Width = 790
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object PainelConfI: TPanel
        Left = 0
        Top = 0
        Width = 790
        Height = 48
        Align = alBottom
        TabOrder = 1
        Visible = False
        object BtConfirma2: TBitBtn
          Tag = 14
          Left = 12
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirma2Click
        end
        object BtDesiste2: TBitBtn
          Tag = 13
          Left = 596
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Sair'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtDesiste2Click
        end
        object BtExclui2: TBitBtn
          Tag = 12
          Left = 276
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Exclui'
          Enabled = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtExclui2Click
        end
      end
      object PainelControle: TPanel
        Left = 108
        Top = 16
        Width = 702
        Height = 48
        Color = clAppWorkSpace
        TabOrder = 0
        object LaRegistro: TLabel
          Left = 173
          Top = 1
          Width = 26
          Height = 13
          Caption = '[N]: 0'
        end
        object Panel5: TPanel
          Left = 1
          Top = 1
          Width = 172
          Height = 46
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object SpeedButton4: TBitBtn
            Tag = 4
            Left = 128
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Hint = 'ltimo registro'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = SpeedButton4Click
          end
          object SpeedButton3: TBitBtn
            Tag = 3
            Left = 88
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Hint = 'Pr'#243'ximo registro'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = SpeedButton3Click
          end
          object SpeedButton2: TBitBtn
            Tag = 2
            Left = 48
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Hint = 'Registro anterior'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = SpeedButton2Click
          end
          object SpeedButton1: TBitBtn
            Tag = 1
            Left = 8
            Top = 4
            Width = 40
            Height = 40
            Cursor = crHandPoint
            Hint = 'Primeiro registro'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = SpeedButton1Click
          end
        end
        object Panel6: TPanel
          Left = 232
          Top = 1
          Width = 469
          Height = 46
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 372
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
          object BtCheque: TBitBtn
            Tag = 146
            Left = 99
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Altera banco atual'
            Caption = '&Duplicata'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtChequeClick
          end
          object BtReceitas: TBitBtn
            Tag = 143
            Left = 8
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Inclui novo banco'
            Caption = '&Lote'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtReceitasClick
          end
          object BtImportar: TBitBtn
            Tag = 147
            Left = 190
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = 'Im&porta'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = BtImportarClick
          end
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 448
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 399
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Default = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 584
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtDesisteClick
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 820
      Height = 196
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label75: TLabel
        Left = 120
        Top = 8
        Width = 44
        Height = 13
        Caption = 'Coligado:'
      end
      object Label3: TLabel
        Left = 460
        Top = 8
        Width = 66
        Height = 13
        Caption = 'Data repasse:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdColigado: TdmkEditCB
        Left = 120
        Top = 24
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBColigado
        IgnoraDBLookupComboBox = False
      end
      object CBColigado: TdmkDBLookupComboBox
        Left = 188
        Top = 24
        Width = 269
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECOLIGADO'
        ListSource = DsColigado
        TabOrder = 2
        dmkEditCB = EdColigado
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPData: TDateTimePicker
        Left = 460
        Top = 24
        Width = 112
        Height = 21
        Date = 38724.439835069400000000
        Time = 38724.439835069400000000
        TabOrder = 3
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Repasses de Duplicatas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TLabel
      Left = 709
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 483
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 481
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object BtImpCalculado: TBitBtn
        Tag = 144
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtImpCalculadoClick
      end
      object BtImpPesquisa: TBitBtn
        Tag = 145
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtImpPesquisaClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsRepas: TDataSource
    DataSet = QrRepas
    Left = 544
    Top = 21
  end
  object QrRepas: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrRepasBeforeOpen
    AfterOpen = QrRepasAfterOpen
    AfterScroll = QrRepasAfterScroll
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECOLIGADO, en.FatorCompra,'
      're.*, (re.Total-re.JurosV) SALDO'
      'FROM repas re'
      'LEFT JOIN entidades en ON en.Codigo=re.Coligado'
      'WHERE re.Codigo > 0')
    Left = 516
    Top = 21
    object QrRepasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRepasColigado: TIntegerField
      FieldName = 'Coligado'
    end
    object QrRepasData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRepasTotal: TFloatField
      FieldName = 'Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepasJurosV: TFloatField
      FieldName = 'JurosV'
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepasNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
    object QrRepasSALDO: TFloatField
      FieldName = 'SALDO'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepasFatorCompra: TFloatField
      FieldName = 'FatorCompra'
      DisplayFormat = '#,###,##0.000000'
    end
  end
  object QrColigado: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrRepasBeforeOpen
    AfterOpen = QrRepasAfterOpen
    AfterScroll = QrRepasAfterScroll
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECOLIGADO'
      'FROM entidades'
      'WHERE Fornece2="V"')
    Left = 140
    Top = 69
    object QrColigadoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrColigadoNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
  end
  object DsColigado: TDataSource
    DataSet = QrColigado
    Left = 168
    Top = 65
  end
  object QrRepasIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrRepasItsAfterOpen
    AfterClose = QrRepasItsAfterClose
    OnCalcFields = QrRepasItsCalcFields
    SQL.Strings = (
      'SELECT li.Duplicata, li.Banco, li.Agencia,'
      'li.Emitente, li.CPF, li.DDeposito, li.Vencto, '
      '(ri.Valor - ri.JurosV) LIQUIDO, ri.* '
      'FROM repasits ri'
      'LEFT JOIN lotesits li ON li.Controle=ri.Origem'
      'WHERE ri.Codigo=:P0')
    Left = 572
    Top = 21
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRepasItsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrRepasItsBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrRepasItsAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrRepasItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrRepasItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRepasItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrRepasItsOrigem: TIntegerField
      FieldName = 'Origem'
      Required = True
    end
    object QrRepasItsDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrRepasItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrRepasItsTaxa: TFloatField
      FieldName = 'Taxa'
      Required = True
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrRepasItsJurosP: TFloatField
      FieldName = 'JurosP'
      Required = True
      DisplayFormat = '#,###,###,##0.0000'
    end
    object QrRepasItsJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrRepasItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrRepasItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrRepasItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrRepasItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrRepasItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrRepasItsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrRepasItsCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrRepasItsDDeposito: TDateField
      FieldName = 'DDeposito'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRepasItsVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRepasItsLIQUIDO: TFloatField
      FieldName = 'LIQUIDO'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
  end
  object DsRepasIts: TDataSource
    DataSet = QrRepasIts
    Left = 600
    Top = 21
  end
  object QrBanco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM bancos'
      'WHERE Codigo=:P0')
    Left = 633
    Top = 105
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBancoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrBancoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBancoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBancoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBancoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBancoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBancoSite: TWideStringField
      FieldName = 'Site'
      Size = 255
    end
  end
  object DsBanco: TDataSource
    DataSet = QrBanco
    Left = 661
    Top = 105
  end
  object QrCheques: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrChequesAfterOpen
    AfterClose = QrChequesAfterClose
    OnCalcFields = QrChequesCalcFields
    SQL.Strings = (
      'SELECT lo.Lote, li.* '
      'FROM lotesits li'
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Tipo=1'
      'AND li.Repassado=0')
    Left = 640
    Top = 20
    object QrChequesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrChequesControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrChequesComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrChequesBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrChequesAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrChequesConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrChequesCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrChequesCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrChequesEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrChequesValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrChequesEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrChequesTxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrChequesTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrChequesVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrChequesVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrChequesDMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrChequesDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrChequesDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrChequesLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrChequesDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChequesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChequesUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrChequesUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrChequesDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrChequesDesco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrChequesQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrChequesBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
    end
    object QrChequesCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrChequesLote: TSmallintField
      FieldName = 'Lote'
      DisplayFormat = '0000'
    end
  end
  object DsCheques: TDataSource
    DataSet = QrCheques
    Left = 664
    Top = 20
  end
  object PMLote: TPopupMenu
    Left = 268
    Top = 408
    object Incluinovolote1: TMenuItem
      Caption = '&Inclui novo lote'
      OnClick = Incluinovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      Enabled = False
    end
  end
  object PMCheque: TPopupMenu
    Left = 372
    Top = 416
    object Adicionachequeaoloteatual1: TMenuItem
      Caption = '&Adiciona duplicata ao lote atual'
      OnClick = Adicionachequeaoloteatual1Click
    end
    object Alterarepassedochequeatual1: TMenuItem
      Caption = '&Edita repasse da duplicata atual'
      Enabled = False
    end
    object Retirachequedoloteatual1: TMenuItem
      Caption = '&Retira duplicata do lote atual'
      OnClick = Retirachequedoloteatual1Click
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMECLIENTE')
    Left = 576
    Top = 105
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 604
    Top = 105
  end
  object QrSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor, SUM(JurosV) JurosV'
      'FROM repasits'
      'WHERE Codigo=:P0'
      '')
    Left = 201
    Top = 69
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSumJurosV: TFloatField
      FieldName = 'JurosV'
    end
  end
  object QrLI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lo.Tipo, li.* '
      'FROM lotesits li'
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Codigo=:P0'
      ''
      '')
    Left = 108
    Top = 233
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLIComp: TIntegerField
      FieldName = 'Comp'
    end
    object QrLIBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLIAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrLIConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrLICheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrLICPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLIEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLIBruto: TFloatField
      FieldName = 'Bruto'
    end
    object QrLIDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrLIValor: TFloatField
      FieldName = 'Valor'
    end
    object QrLIEmissao: TDateField
      FieldName = 'Emissao'
    end
    object QrLIDCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrLIDDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrLIVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrLITxaCompra: TFloatField
      FieldName = 'TxaCompra'
    end
    object QrLITxaJuros: TFloatField
      FieldName = 'TxaJuros'
    end
    object QrLITxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
    end
    object QrLIVlrCompra: TFloatField
      FieldName = 'VlrCompra'
    end
    object QrLIVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
    end
    object QrLIDMais: TIntegerField
      FieldName = 'DMais'
    end
    object QrLIDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrLIDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrLIDevolucao: TIntegerField
      FieldName = 'Devolucao'
    end
    object QrLIQuitado: TIntegerField
      FieldName = 'Quitado'
    end
    object QrLILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLIPraca: TIntegerField
      FieldName = 'Praca'
    end
    object QrLITipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLIBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
      Required = True
    end
    object QrLIAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
      Required = True
    end
    object QrLITotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrLITotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrLITotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrLIData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrLIProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrLIProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrLIRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
  end
  object frxRepasIts: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.990536921300000000
    ReportOptions.LastChange = 39717.990536921300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxRepasItsGetValue
    Left = 246
    Top = 345
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsRepas
        DataSetName = 'frxDsRepas'
      end
      item
        DataSet = frxDsRepasIts
        DataSetName = 'frxDsRepasIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 72.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 538.661410000000000000
          Top = 4.102350000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 2.661410000000000000
          Top = 0.102350000000001000
          Width = 168.000000000000000000
          Height = 64.000000000000000000
          ShowHint = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo49: TfrxMemoView
          Left = 174.661410000000000000
          Top = 28.102350000000000000
          Width = 520.000000000000000000
          Height = 26.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 385.512060000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 553.779530000000000000
          Top = 2.487630000000020000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 645.559060000000000000
          Top = 2.487630000000020000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object Band4: TfrxMasterData
        Height = 17.000000000000000000
        Top = 260.787570000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsRepasIts
        DataSetName = 'frxDsRepasIts'
        RowCount = 0
        object Memo6: TfrxMemoView
          Left = 70.661410000000000000
          Width = 24.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsRepasIts."Banco">)]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 94.661410000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'0000'#39', <frxDsRepasIts."Agencia">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 2.661410000000000000
          Width = 68.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsRepasIts."Duplicata"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 122.661410000000000000
          Width = 184.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsRepasIts."Emitente"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 306.661410000000000000
          Width = 92.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsRepasIts."CPF_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 398.661410000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."Valor"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 458.661410000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepasIts."Vencto"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 506.661410000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepasIts."DDeposito"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 554.661410000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."Dias"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 582.661410000000000000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."JurosV"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 638.661410000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."LIQUIDO"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 87.614100000000000000
        Top = 113.385900000000000000
        Width = 718.110700000000000000
        object Memo3: TfrxMemoView
          Left = 598.661410000000000000
          Top = 45.614100000000000000
          Width = 100.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsRepas."Data"]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 2.661410000000000000
          Top = 43.614100000000000000
          Width = 696.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Memo24: TfrxMemoView
          Left = 2.661410000000000000
          Top = 45.614100000000000000
          Width = 436.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Coligado: [frxDsRepas."Coligado"] - [frxDsRepas."NOMECOLIGADO"]')
          ParentFont = False
        end
        object Line2: TfrxLineView
          Left = 2.661410000000000000
          Top = 66.614100000000000000
          Width = 696.000000000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
        end
        object Memo69: TfrxMemoView
          Left = 494.661410000000000000
          Top = 45.614100000000000000
          Width = 100.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Data do repasse:')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 70.661410000000000000
          Top = 70.614100000000000000
          Width = 24.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bco.')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 94.661410000000000000
          Top = 70.614100000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag.')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 2.661410000000000000
          Top = 70.614100000000000000
          Width = 68.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Duplicata')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 122.661410000000000000
          Top = 70.614100000000000000
          Width = 184.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sacado')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 306.661410000000000000
          Top = 70.614100000000000000
          Width = 92.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 398.661410000000000000
          Top = 70.614100000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 458.661410000000000000
          Top = 70.614100000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 506.661410000000000000
          Top = 70.614100000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Resgate')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 554.661410000000000000
          Top = 70.614100000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Dias')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 582.661410000000000000
          Top = 70.614100000000000000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Juros')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 638.661410000000000000
          Top = 70.614100000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ L'#237'quido')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 2.661410000000000000
          Top = 6.173160000000000000
          Width = 696.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'REPASSE DE DUPLICATAS')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 634.661410000000000000
          Top = 6.173160000000000000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepas."Codigo"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 570.661410000000000000
          Top = 6.173160000000000000
          Width = 64.000000000000000000
          Height = 20.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Controle:')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        Height = 24.000000000000000000
        Top = 340.157700000000000000
        Width = 718.110700000000000000
        object Memo34: TfrxMemoView
          Left = 2.661410000000000000
          Top = 4.535250000000020000
          Width = 396.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total dos [VARF_QTD_CHEQUES] cheques deste border'#244':')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 398.661410000000000000
          Top = 4.535250000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 582.661410000000000000
          Top = 4.535250000000020000
          Width = 56.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 638.661410000000000000
          Top = 4.535250000000020000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 458.661410000000000000
          Top = 4.535250000000020000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
      end
    end
  end
  object frxDsRepasIts: TfrxDBDataset
    UserName = 'frxDsRepasIts'
    CloseDataSource = False
    DataSet = QrRepasIts
    BCDToCurrency = False
    Left = 274
    Top = 345
  end
  object frxDsRepas: TfrxDBDataset
    UserName = 'frxDsRepas'
    CloseDataSource = False
    DataSet = QrRepas
    BCDToCurrency = False
    Left = 302
    Top = 345
  end
end
