unit PesqCNPJ;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, Db, mySQLDbTables,
  dmkEdit, dmkGeral;

type
  TFmPesqCNPJ = class(TForm)
    PainelConfirma: TPanel;
    BtPesquisa: TBitBtn;
    BtConfirma: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    RGMascara: TRadioGroup;
    Label1: TLabel;
    EdEmitente: TdmkEdit;
    DBGrid1: TDBGrid;
    QrSacados: TmySQLQuery;
    DsSacados: TDataSource;
    QrSacadosCNPJ: TWideStringField;
    QrSacadosIE: TWideStringField;
    QrSacadosNome: TWideStringField;
    QrSacadosRua: TWideStringField;
    QrSacadosCompl: TWideStringField;
    QrSacadosBairro: TWideStringField;
    QrSacadosCidade: TWideStringField;
    QrSacadosUF: TWideStringField;
    QrSacadosCEP: TIntegerField;
    QrSacadosTel1: TWideStringField;
    QrSacadosRisco: TFloatField;
    QrSacadosNumero: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure RGMascaraClick(Sender: TObject);
    procedure EdEmitenteChange(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmPesqCNPJ: TFmPesqCNPJ;

implementation

uses Module, UnInternalConsts, UnMyObjects;

{$R *.DFM}

procedure TFmPesqCNPJ.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqCNPJ.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPesqCNPJ.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmPesqCNPJ.BtPesquisaClick(Sender: TObject);
var
  Emitente: String;
begin
  //if Trim(EdEmitente.Text) <> '' then
  //begin
    Emitente := EdEmitente.Text;
    if RGMascara.ItemIndex in ([0,1]) then Emitente := '%'+Emitente;
    if RGMascara.ItemIndex in ([0,2]) then Emitente := Emitente+'%';
 // end else Emitente := '';
  //////////////////////////////////////////////////////////////////////////////
  QrSacados.Close;
  QrSacados.Params[0].AsString := Emitente;
  QrSacados.Open;
  //
  BtPesquisa.Enabled := False;
  BtConfirma.Enabled := True;
  //configurar grade
end;

procedure TFmPesqCNPJ.RGMascaraClick(Sender: TObject);
begin
  BtPesquisa.Enabled := True;
  BtConfirma.Enabled := False;
end;

procedure TFmPesqCNPJ.EdEmitenteChange(Sender: TObject);
begin
  BtPesquisa.Enabled := True;
  BtConfirma.Enabled := False;
end;

procedure TFmPesqCNPJ.DBGrid1DblClick(Sender: TObject);
begin
  BtConfirmaClick(Self);
end;

procedure TFmPesqCNPJ.BtConfirmaClick(Sender: TObject);
begin
  VAR_CPF_PESQ := Geral.FormataCNPJ_TT(QrSacadosCNPJ.Value);
  Close;
end;

end.
