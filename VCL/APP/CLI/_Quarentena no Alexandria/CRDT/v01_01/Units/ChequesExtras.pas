unit ChequesExtras;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DBCtrls, Db,
  mySQLDbTables, ComCtrls, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UnDmkEnums;

type
  TFmChequesExtras = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel8: TPanel;
    DBGrid1: TDBGrid;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNOMEENTI: TWideStringField;
    QrSCB: TmySQLQuery;
    QrSCBSCB: TIntegerField;
    QrBanco2: TmySQLQuery;
    QrBanco2Nome: TWideStringField;
    QrBanco2DVCC: TSmallintField;
    Panel1: TPanel;
    Label42: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label43: TLabel;
    EdBanda2: TdmkEdit;
    EdCMC_7_2: TEdit;
    Label44: TLabel;
    EdRegiaoCompe2: TdmkEdit;
    EdBanco2: TdmkEdit;
    Label45: TLabel;
    Label46: TLabel;
    EdAgencia2: TdmkEdit;
    EdConta2: TdmkEdit;
    Label47: TLabel;
    EdRealCC: TdmkEdit;
    EdCheque2: TdmkEdit;
    Label48: TLabel;
    Label49: TLabel;
    EdCPF_2: TdmkEdit;
    EdEmitente2: TdmkEdit;
    Label50: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label35: TLabel;
    EdValor: TdmkEdit;
    GroupBox1: TGroupBox;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label96: TLabel;
    Label98: TLabel;
    Label99: TLabel;
    Label91: TLabel;
    Label54: TLabel;
    EdDR: TdmkEdit;
    EdDT: TdmkEdit;
    EdRT: TdmkEdit;
    EdVT: TdmkEdit;
    EdST: TdmkEdit;
    EdCT: TdmkEdit;
    EdDV: TdmkEdit;
    EdCR: TdmkEdit;
    EdCV: TdmkEdit;
    EdOA: TdmkEdit;
    EdTT: TdmkEdit;
    QrSacRiscoC: TmySQLQuery;
    QrSacRiscoCBanco: TIntegerField;
    QrSacRiscoCAgencia: TIntegerField;
    QrSacRiscoCConta: TWideStringField;
    QrSacRiscoCCheque: TIntegerField;
    QrSacRiscoCValor: TFloatField;
    QrSacRiscoCDCompra: TDateField;
    QrSacRiscoCDDeposito: TDateField;
    QrSacRiscoCEmitente: TWideStringField;
    QrSacRiscoCCPF: TWideStringField;
    QrSacRiscoCCPF_TXT: TWideStringField;
    DsSacRiscoC: TDataSource;
    QrSacDOpen: TmySQLQuery;
    QrSacDOpenControle: TIntegerField;
    QrSacDOpenDuplicata: TWideStringField;
    QrSacDOpenDCompra: TDateField;
    QrSacDOpenValor: TFloatField;
    QrSacDOpenDDeposito: TDateField;
    QrSacDOpenEmitente: TWideStringField;
    QrSacDOpenCPF: TWideStringField;
    QrSacDOpenCliente: TIntegerField;
    QrSacDOpenSTATUS: TWideStringField;
    QrSacDOpenQuitado: TIntegerField;
    QrSacDOpenTotalJr: TFloatField;
    QrSacDOpenTotalDs: TFloatField;
    QrSacDOpenTotalPg: TFloatField;
    QrSacDOpenSALDO_DESATUALIZ: TFloatField;
    QrSacDOpenSALDO_ATUALIZADO: TFloatField;
    QrSacDOpenNOMESTATUS: TWideStringField;
    QrSacDOpenVencto: TDateField;
    QrSacDOpenDDCALCJURO: TIntegerField;
    QrSacDOpenData3: TDateField;
    QrSacDOpenRepassado: TSmallintField;
    DsSacDOpen: TDataSource;
    QrSacCHDevA: TmySQLQuery;
    QrSacCHDevADATA1_TXT: TWideStringField;
    QrSacCHDevADATA2_TXT: TWideStringField;
    QrSacCHDevADATA3_TXT: TWideStringField;
    QrSacCHDevACPF_TXT: TWideStringField;
    QrSacCHDevANOMECLIENTE: TWideStringField;
    QrSacCHDevACodigo: TIntegerField;
    QrSacCHDevAAlinea1: TIntegerField;
    QrSacCHDevAAlinea2: TIntegerField;
    QrSacCHDevAData1: TDateField;
    QrSacCHDevAData2: TDateField;
    QrSacCHDevAData3: TDateField;
    QrSacCHDevACliente: TIntegerField;
    QrSacCHDevABanco: TIntegerField;
    QrSacCHDevAAgencia: TIntegerField;
    QrSacCHDevAConta: TWideStringField;
    QrSacCHDevACheque: TIntegerField;
    QrSacCHDevACPF: TWideStringField;
    QrSacCHDevAValor: TFloatField;
    QrSacCHDevATaxas: TFloatField;
    QrSacCHDevALk: TIntegerField;
    QrSacCHDevADataCad: TDateField;
    QrSacCHDevADataAlt: TDateField;
    QrSacCHDevAUserCad: TIntegerField;
    QrSacCHDevAUserAlt: TIntegerField;
    QrSacCHDevAEmitente: TWideStringField;
    QrSacCHDevAChequeOrigem: TIntegerField;
    QrSacCHDevAStatus: TSmallintField;
    QrSacCHDevAValPago: TFloatField;
    QrSacCHDevAMulta: TFloatField;
    QrSacCHDevAJurosP: TFloatField;
    QrSacCHDevAJurosV: TFloatField;
    QrSacCHDevADesconto: TFloatField;
    QrSacCHDevASALDO: TFloatField;
    QrSacCHDevAATUAL: TFloatField;
    DsSacCHDevA: TDataSource;
    QrSacOcorA: TmySQLQuery;
    QrSacOcorATipo: TSmallintField;
    QrSacOcorATIPODOC: TWideStringField;
    QrSacOcorANOMEOCORRENCIA: TWideStringField;
    QrSacOcorACodigo: TIntegerField;
    QrSacOcorALotesIts: TIntegerField;
    QrSacOcorADataO: TDateField;
    QrSacOcorAOcorrencia: TIntegerField;
    QrSacOcorAValor: TFloatField;
    QrSacOcorALoteQuit: TIntegerField;
    QrSacOcorALk: TIntegerField;
    QrSacOcorADataCad: TDateField;
    QrSacOcorADataAlt: TDateField;
    QrSacOcorAUserCad: TIntegerField;
    QrSacOcorAUserAlt: TIntegerField;
    QrSacOcorATaxaP: TFloatField;
    QrSacOcorATaxaV: TFloatField;
    QrSacOcorAPago: TFloatField;
    QrSacOcorADataP: TDateField;
    QrSacOcorATaxaB: TFloatField;
    QrSacOcorAData3: TDateField;
    QrSacOcorAStatus: TSmallintField;
    QrSacOcorASALDO: TFloatField;
    QrSacOcorAATUALIZADO: TFloatField;
    QrSacOcorACliente: TIntegerField;
    QrSacOcorACLIENTELOTE: TIntegerField;
    DsSacOcorA: TDataSource;
    QrSacRiscoTC: TmySQLQuery;
    QrSacRiscoTCValor: TFloatField;
    DsSacRiscoTC: TDataSource;
    QrSacCHDevAPgDesc: TFloatField;
    QrSacCHDevADDeposito: TDateField;
    QrSacCHDevAVencto: TDateField;
    QrSacCHDevALoteOrigem: TIntegerField;
    QrAvulsos: TmySQLQuery;
    DsAvulsos: TDataSource;
    QrAvulsosControle: TIntegerField;
    QrAvulsosBanco: TIntegerField;
    QrAvulsosAgencia: TIntegerField;
    QrAvulsosConta: TWideStringField;
    QrAvulsosCheque: TIntegerField;
    QrAvulsosCPF: TWideStringField;
    QrAvulsosEmitente: TWideStringField;
    QrAvulsosValor: TFloatField;
    QrAvulsosDCompra: TDateField;
    QrAvulsosVencto: TDateField;
    QrAvulsosCPF_TXT: TWideStringField;
    QrAvulsosCliente: TIntegerField;
    QrAvulsosNOMECLI: TWideStringField;
    EdData4: TdmkEdit;
    EdVence4: TdmkEdit;
    BtExclui: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdBanda2Change(Sender: TObject);
    procedure EdCMC_7_2Change(Sender: TObject);
    procedure EdBanco2Change(Sender: TObject);
    procedure EdBanco2Exit(Sender: TObject);
    procedure EdAgencia2Change(Sender: TObject);
    procedure EdConta2Change(Sender: TObject);
    procedure EdCPF_2Exit(Sender: TObject);
    procedure EdCPF_2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure QrSacDOpenAfterOpen(DataSet: TDataSet);
    procedure QrSacCHDevAAfterOpen(DataSet: TDataSet);
    procedure QrSacOcorAAfterOpen(DataSet: TDataSet);
    procedure QrSacOcorACalcFields(DataSet: TDataSet);
    procedure QrSacOcorABeforeClose(DataSet: TDataSet);
    procedure QrSacDOpenBeforeClose(DataSet: TDataSet);
    procedure QrSacDOpenCalcFields(DataSet: TDataSet);
    procedure QrSacRiscoCCalcFields(DataSet: TDataSet);
    procedure QrSacCHDevACalcFields(DataSet: TDataSet);
    procedure QrAvulsosCalcFields(DataSet: TDataSet);
    procedure EdData4Exit(Sender: TObject);
    procedure EdVence4Exit(Sender: TObject);
    procedure EdCPF_2Change(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    { Private declarations }
    FCPFSacado: String;
    FCHDevOpen_Total, FOcorA_Total: Double;
    function CadastraCheque: Boolean;
    function LocalizaEmitente2(MudaNome: Boolean): Boolean;
    procedure ReopenBanco2;
    function CriaBAC_Pesq(Banco, Agencia, Conta: String; DVCC: Integer): String;
    procedure PesquisaRiscoSacado(CPF: String);
    procedure ReopenSacCHDevA;
    procedure ReopenSacOcorA;
    procedure ReopenSacRiscoC;
    procedure ReopenSacDOpen;
    procedure ReopenAvulsos(Controle: Integer);
    procedure LimpaRisco;
  public
    { Public declarations }
  end;

  var
  FmChequesExtras: TFmChequesExtras;

implementation

uses UnInternalConsts, Module, UMySQLModule, Principal, LotesEmi, Modulc,
UnMyObjects;

{$R *.DFM}

procedure TFmChequesExtras.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmChequesExtras.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  EdCliente.SetFocus;
end;

procedure TFmChequesExtras.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmChequesExtras.BtOKClick(Sender: TObject);
begin
  CadastraCheque;
end;

function TFmChequesExtras.CadastraCheque: Boolean;
var
  Controle, APCD, Cliente: Integer;
  Dias, Cheque, Comp, Banco, Agencia, Praca: Integer;
  Valor: Double;
  DDeposito, DCompra, Vencto, Conta, CPF, Emitente: String;
  Vence, Data: TDateTime;
begin
  Vence := Geral.ValidaDataSimples(EdVence4.Text, False);
  Data  := Geral.ValidaDataSimples(EdData4.Text , False);
  Cliente := Geral.IMV(EdCliente.Text);
  Result := True;
  APCD := -9; // Cheques avulsos
  Dias    := Trunc(Vence - Data);
  Valor   := Geral.DMV(EdValor.Text);
  //Desco   := 0;//Geral.DMV(EdDesco2.Text);
  Vencto  := FormatDateTime(VAR_FORMATDATE, Vence);
  DCompra := FormatDateTime(VAR_FORMATDATE, Data);
  Banco   := Geral.IMV(EdBanco2.Text);
  Agencia := Geral.IMV(EdAgencia2.Text);
  Conta   := EdConta2.Text;
  Cheque  := Geral.IMV(EdCheque2.Text);
  CPF     := Geral.SoNumero_TT(EdCPF_2.Text);
  Emitente:= EdEmitente2.Text;
  Praca   := Geral.IMV(EdRegiaoCompe2.Text);
  QrSCB.Close;
  QrSCB.Params[0].AsInteger := Cliente;
  QrSCB.Open;
  //
  (*Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'AlinPgs', 'AlinPgs', 'Codigo');*)

  //////////////////////////////////////
  //  Depositos, riscos, etc        ////
  //////////////////////////////////////

  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO lotesits SET Quitado=0, ');
  Dmod.QrUpd.SQL.Add('Comp=:P0, Banco=:P1, Agencia=:P2, Conta=:P3, ');
  Dmod.QrUpd.SQL.Add('Cheque=:P4, CPF=:P5, Emitente=:P6, Valor=:P7, ');
  Dmod.QrUpd.SQL.Add('Vencto=:P8, TxaCompra=:P9, TxaAdValorem=:P10, ');
  Dmod.QrUpd.SQL.Add('VlrCompra=:P11, VlrAdValorem=:P12, DMais=:P13, ');
  Dmod.QrUpd.SQL.Add('Dias=:P14, TxaJuros=:P15, DCompra=:P16, ');
  Dmod.QrUpd.SQL.Add('DDeposito=:P17, Praca=:P18, Tipo=:P19, ');
  Dmod.QrUpd.SQL.Add('AliIts=:P20, AlinPgs=:P21, Cliente=:P22, Data3=:P23 ');
  Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb');
  //
  Comp    := FmPrincipal.VerificaDiasCompensacao(Praca, Cliente,
             Valor, Data, Vence, QrSCBSCB.Value);
  DDeposito := FormatDateTime(VAR_FORMATDATE,
    UMyMod.CalculaDataDeposito(Vence));
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'LotesIts', 'LotesIts', 'Controle');
  Dmod.QrUpd.Params[00].AsInteger := Comp;
  Dmod.QrUpd.Params[01].AsInteger := Banco;
  Dmod.QrUpd.Params[02].AsInteger := Agencia;
  Dmod.QrUpd.Params[03].AsString  := Conta;
  Dmod.QrUpd.Params[04].AsInteger := Cheque;
  Dmod.QrUpd.Params[05].AsString  := CPF;
  Dmod.QrUpd.Params[06].AsString  := Emitente;
  Dmod.QrUpd.Params[07].AsFloat   := Valor;
  Dmod.QrUpd.Params[08].AsString  := Vencto;
  //
  Dmod.QrUpd.Params[09].AsFloat   := 0;//FTaxa[0];
  Dmod.QrUpd.Params[10].AsFloat   := 0;
  Dmod.QrUpd.Params[11].AsFloat   := 0;//FValr[0];
  Dmod.QrUpd.Params[12].AsFloat   := 0;
  Dmod.QrUpd.Params[13].AsInteger := 0;//DMais;
  Dmod.QrUpd.Params[14].AsInteger := Dias;
  Dmod.QrUpd.Params[15].AsFloat   := 0;//FJuro[0];
  Dmod.QrUpd.Params[16].AsString  := DCompra;
  Dmod.QrUpd.Params[17].AsString  := DDeposito;
  Dmod.QrUpd.Params[18].AsInteger := Praca;
  Dmod.QrUpd.Params[19].AsInteger := APCD;
  Dmod.QrUpd.Params[20].AsInteger := 0;//QrAlinItsCodigo.Value;
  Dmod.QrUpd.Params[21].AsInteger := 0;//Codigo;
  Dmod.QrUpd.Params[22].AsInteger := Cliente;
  Dmod.QrUpd.Params[23].AsString  := DDeposito; // Quita automatico at� voltar
  //
  Dmod.QrUpd.Params[24].AsInteger := 0;//-Cliente;
  Dmod.QrUpd.Params[25].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  //
  Dmoc.AtualizaEmitente(FormatFloat('000', Banco),
    FormatFloat('0000', Agencia), Conta, CPF, Emitente, -1);
  QrSCB.Close;
  ReopenAvulsos(Controle);
  EdCliente.SetFocus;
  EdBanda2.Text := '';
  EdCPF_2.Text := '';
  EdEmitente2.Text := '';
  EdValor.Text := '';
end;

procedure TFmChequesExtras.EdBanda2Change(Sender: TObject);
begin
  if MLAGeral.LeuTodoCMC7(EdBanda2.Text) then
    EdCMC_7_2.Text := Geral.SoNumero_TT(EdBanda2.Text);
end;

procedure TFmChequesExtras.EdCMC_7_2Change(Sender: TObject);
var
  Banda: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC_7_2.Text) = 0 then
  begin
    Banda := TBandaMagnetica.Create;
    Banda.BandaMagnetica := EdCMC_7_2.Text;
    EdRegiaoCompe2.Text  := Banda.Compe;
    EdBanco2.Text        := Banda.Banco;
    EdAgencia2.Text      := Banda.Agencia;
    EdConta2.Text        := Banda.Conta;
    EdCheque2.Text       := Banda.Numero;
    //
    EdCPF_2.SetFocus;
  end else begin
    EdBanco2.Text   := '000';
    EdAgencia2.Text := '0000';
    EdConta2.Text   := '0000000000';
    EdRealCC.Text   := '0000000000';
    EdCheque2.Text  := '000000';
  end;
end;

procedure TFmChequesExtras.EdBanco2Change(Sender: TObject);
begin
  LocalizaEmitente2(True);
  if not EdBanco2.Focused then ReopenBanco2;
end;

procedure TFmChequesExtras.EdBanco2Exit(Sender: TObject);
begin
  LocalizaEmitente2(True);
end;

procedure TFmChequesExtras.EdAgencia2Change(Sender: TObject);
begin
  LocalizaEmitente2(True);
end;

procedure TFmChequesExtras.EdConta2Change(Sender: TObject);
begin
  LocalizaEmitente2(True);
end;

procedure TFmChequesExtras.EdCPF_2Exit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCPF_2.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if MLAGeral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Application.MessageBox(PChar('N�mero inv�lido'), 'Erro', MB_OK+MB_ICONERROR);
      EdCPF_2.SetFocus;
    end else EdCPF_2.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCPF_2.Text := CO_VAZIO;
  PesquisaRiscoSacado(EdCPF_2.Text);
end;

procedure TFmChequesExtras.EdCPF_2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    Dmod.QrLocCPF.Close;
    Dmod.QrLocCPF.Params[0].AsString := Geral.SoNumero_TT(EdCPF_2.Text);
    Dmod.QrLocCPF.Open;
    case Dmod.QrLocCPF.RecordCount of
      0: Application.MessageBox('N�o foi localizado emitente para este CPF!',
        'Aviso', MB_OK+MB_ICONWARNING);
      1: EdEmitente2.Text := Dmod.QrLocCPFNome.Value;
      else begin
        Application.CreateForm(TFmLotesEmi, FmLotesEmi);
        FmLotesEmi.ShowModal;
        if FmLotesEmi.FEmitenteNome <> '' then
          EdEmitente2.Text := FmLotesEmi.FEmitenteNome;
        FmLotesEmi.Destroy;
      end;
    end;
  end;
end;

procedure TFmChequesExtras.FormCreate(Sender: TObject);
begin
  QrEntidades.Open;
  ReopenAvulsos(0);
end;

function TFmChequesExtras.LocalizaEmitente2(MudaNome: Boolean): Boolean;
var
  B,A,C: String;
begin
  Result := False;
  if  (Geral.IMV(EdBanco2.Text) > 0)
  and (Geral.IMV(EdAgencia2.Text) > 0)
  and (Geral.DMV(EdConta2.Text) > 0.1) then
  begin
    Screen.Cursor := crHourGlass;
    try
      if MudaNome then
      begin
        B := EdBanco2.Text;
        A := EdAgencia2.Text;
        C := EdConta2.Text;
        //
        Dmoc.QrLocEmiBAC.Close;
        Dmoc.QrLocEmiBAC.Params[0].AsInteger := QrBanco2DVCC.Value;
        Dmoc.QrLocEmiBAC.Params[1].AsString  := CriaBAC_Pesq(B,A,C,QrBanco2DVCC.Value);
        Dmoc.QrLocEmiBAC.Open;
        //
        if Dmoc.QrLocEmiBAC.RecordCount = 0 then
        begin
          EdEmitente2.Text := '';
          EdCPF_2.Text := '';
        end else begin
          Dmoc.QrLocEmiCPF.Close;
          Dmoc.QrLocEmiCPF.Params[0].AsString := Dmoc.QrLocEmiBACCPF.Value;
          Dmoc.QrLocEmiCPF.Open;
          //
          if Dmoc.QrLocEmiCPF.RecordCount = 0 then
          begin
            EdEmitente2.Text := '';
            EdCPF_2.Text := '';
          end else begin
            EdEmitente2.Text := Dmoc.QrLocEmiCPFNome.Value;
            EdCPF_2.Text      := Geral.FormataCNPJ_TT(Dmoc.QrLocEmiCPFCPF.Value);
            PesquisaRiscoSacado(EdCPF_2.Text);
          end;
        end;
      end;
      Screen.Cursor := crDefault;
      Result := True;
    except
      Screen.Cursor := crDefault;
      raise;
    end;
  end;
end;

procedure TFmChequesExtras.ReopenBanco2;
begin
  QrBanco2.Close;
  QrBanco2.Params[0].AsString := EdBanco2.Text;
  QrBanco2.Open;
end;

function TFmChequesExtras.CriaBAC_Pesq(Banco, Agencia, Conta: String;
  DVCC: Integer): String;
begin
  EdRealCC.Text := Copy(Conta, 10-DVCC+1, DVCC);
  Result := Banco+Agencia+EdRealCC.Text;
end;

procedure TFmChequesExtras.PesquisaRiscoSacado(CPF: String);
begin
  FCPFSacado := Geral.SoNumero_TT(CPF);
  //FNomeSacado := '';
  // soma antes do �ltimo !!!!
  ReopenSacCHDevA;
  ReopenSacOcorA;
  // fim somas.
  //
  ReopenSacRiscoC;
  //
  // precisa ser o �ltimo !!!! para Ed?.Text
  ReopenSacDOpen;
  //
end;

procedure TFmChequesExtras.ReopenSacCHDevA;
begin
  QrSacCHDevA.Close;
  QrSacCHDevA.SQL.Clear;
  QrSacCHDevA.SQL.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrSacCHDevA.SQL.Add('ELSE en.Nome END NOMECLIENTE, ai.*');
  QrSacCHDevA.SQL.Add('FROM alinits ai');
  QrSacCHDevA.SQL.Add('LEFT JOIN entidades en ON en.Codigo=ai.Cliente');
  if FmPrincipal.FConnections = 0 then
  begin
    //QrSacCHDevA.SQL.Add('LEFT JOIN lotesits li ON li.Controle=ai.ChequeOrigem');
    QrSacCHDevA.SQL.Add('LEFT JOIN lotes lo ON ai.LoteOrigem=lo.Codigo');
  end;
  QrSacCHDevA.SQL.Add('WHERE ai.Status<2');
  if FmPrincipal.FConnections = 0 then
  begin
    QrSacCHDevA.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  end;
  if FCPFSacado <> '' then
    QrSacCHDevA.SQL.Add('AND ai.CPF = "'+FCPFSacado+'"');
  QrSacCHDevA.SQL.Add('ORDER BY ai.Data1, ai.Data2');
  QrSacCHDevA.SQL.Add('');
  QrSacCHDevA.Open;
end;

procedure TFmChequesExtras.ReopenSacOcorA;
begin
  QrSacOcorA.Close;
  QrSacOcorA.SQL.Clear;
  QrSacOcorA.SQL.Add('SELECT li.Controle, li.Emitente, li.CPF, ');
  QrSacOcorA.SQL.Add('lo.Tipo, IF(oc.Cliente > 0,"CL",IF(lo.Tipo=0,"CH","DU"))TIPODOC,');
  QrSacOcorA.SQL.Add('ob.Nome NOMEOCORRENCIA, lo.Cliente CLIENTELOTE, oc.*');
  QrSacOcorA.SQL.Add('FROM ocorreu oc');
  QrSacOcorA.SQL.Add('LEFT JOIN lotesits li ON oc.LotesIts = li.Controle');
  QrSacOcorA.SQL.Add('LEFT JOIN lotes    lo ON lo.Codigo   = li.Codigo');
  QrSacOcorA.SQL.Add('LEFT JOIN ocorbank ob ON ob.Codigo   = oc.Ocorrencia');
  QrSacOcorA.SQL.Add('WHERE oc.Status<2');
  //if FNomeSacado <> '' then
    //QrSacOcorA.SQL.Add('AND li.Emitente LIKE "'+FNomeSacado+'"');
  if FCPFSacado <> '' then
    QrSacOcorA.SQL.Add('AND li.CPF = "'+FCPFSacado+'"');
  QrSacOcorA.SQL.Add('');
  QrSacOcorA.Open;
  //
  //if FOcorA > 0 then QrSacOcorA.Locate('Codigo', FOcorA, []);
end;

procedure TFmChequesExtras.ReopenSacDOpen;
begin
  QrSacDOpen.Close;
  QrSacDOpen.SQL.Clear;
  QrSacDOpen.SQL.Add('SELECT od.Nome STATUS, li.Controle, li.Duplicata, li.Repassado, ');
  QrSacDOpen.SQL.Add('li.DCompra, li.Valor, li.DDeposito, li.Emitente, li.CPF,');
  QrSacDOpen.SQL.Add('lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg,');
  QrSacDOpen.SQL.Add('li.Vencto, li.Data3');
  QrSacDOpen.SQL.Add('FROM lotesits li');
  QrSacDOpen.SQL.Add('LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo');
  QrSacDOpen.SQL.Add('LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao');
  QrSacDOpen.SQL.Add('WHERE lo.Tipo=1');
  QrSacDOpen.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  QrSacDOpen.SQL.Add('AND li.Quitado <> 2');
  if FCPFSacado <> '' then
    QrSacDOpen.SQL.Add('AND li.CPF = "'+FCPFSacado+'"');
  QrSacDOpen.SQL.Add('ORDER BY li.Vencto');
  QrSacDOpen.SQL.Add('');
  QrSacDOpen.Open;
end;

procedure TFmChequesExtras.ReopenSacRiscoC;
begin
  QrSacRiscoC.Close;
  QrSacRiscoC.SQL.Clear;
  QrSacRiscoC.SQL.Add('SELECT lo.Tipo, li.Banco, li.Agencia, li.Conta, li.Cheque, li.Valor,');
  QrSacRiscoC.SQL.Add('li.DCompra, li.DDeposito, li.Emitente, li.CPF');
  QrSacRiscoC.SQL.Add('FROM lotesits li');
  QrSacRiscoC.SQL.Add('LEFT JOIN lotes lo ON lo.Codigo=li.Codigo');
  QrSacRiscoC.SQL.Add('WHERE lo.Tipo = 0');
  QrSacRiscoC.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
    IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  QrSacRiscoC.SQL.Add('AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())');
  if FCPFSacado <> '' then
    QrSacRiscoC.SQL.Add('AND li.CPF = "'+FCPFSacado+'"');
  QrSacRiscoC.SQL.Add('ORDER BY DDeposito');
  QrSacRiscoC.Open;
  //
  QrSacRiscoTC.Close;
  QrSacRiscoTC.SQL.Clear;
  QrSacRiscoTC.SQL.Add('SELECT SUM(li.Valor) Valor');
  QrSacRiscoTC.SQL.Add('FROM lotesits li');
  QrSacRiscoTC.SQL.Add('LEFT JOIN lotes lo ON lo.Codigo=li.Codigo');
  QrSacRiscoTC.SQL.Add('WHERE lo.Tipo = 0');
  QrSacRiscoTC.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  QrSacRiscoTC.SQL.Add('AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())');
  if FCPFSacado <> '' then
    QrSacRiscoTC.SQL.Add('AND li.CPF = "'+FCPFSacado+'"');
  QrSacRiscoTC.Open;
  //
end;

procedure TFmChequesExtras.QrSacDOpenAfterOpen(DataSet: TDataSet);
var
  DR, DV, DT, CR, CV, CT, RT, VT, ST, OA, TT: Double;
begin
  DR := 0; DV := 0;
  while not QrSacDOpen.Eof do
  begin
    if Int(Date) > QrSacDOpenVencto.Value then
      DV := DV + QrSacDOpenSALDO_ATUALIZADO.Value
    else
      DR := DR + QrSacDOpenSALDO_ATUALIZADO.Value;
    QrSacDOpen.Next;
  end;
  DT := DR+DV;
  QrSacDOpen.First;
  CR := QrSacRiscoTCValor.Value;
  CV := FCHDevOpen_Total;
  CT := CR+CV;
  RT := DR+CR;
  VT := DV+CV;
  ST := RT+VT;
  OA := FOcorA_Total;
  TT := ST + OA;
  //
  EdDV.Text := Geral.FFT(DV, 2, siPositivo);
  EdDR.Text := Geral.FFT(DR, 2, siPositivo);
  EdDT.Text := Geral.FFT(DT, 2, siPositivo);
  EdCV.Text := Geral.FFT(CV, 2, siPositivo);
  EdCR.Text := Geral.FFT(CR, 2, siPositivo);
  EdCT.Text := Geral.FFT(CT, 2, siPositivo);
  EdRT.Text := Geral.FFT(RT, 2, siPositivo);
  EdVT.Text := Geral.FFT(VT, 2, siPositivo);
  EdST.Text := Geral.FFT(ST, 2, siPositivo);
  EdOA.Text := Geral.FFT(OA, 2, siPositivo);
  EdTT.Text := Geral.FFT(TT, 2, siPositivo);
end;

procedure TFmChequesExtras.QrSacCHDevAAfterOpen(DataSet: TDataSet);
begin
  FCHDevOpen_Total := 0;
  while not QrSacCHDevA.Eof do
  begin
    //if Int(Date) > QrCHDevAData1.Value then
      FCHDevOpen_Total := FCHDevOpen_Total + QrSacCHDevAATUAL.Value;
    QrSacCHDevA.Next;
  end;
end;

procedure TFmChequesExtras.QrSacOcorAAfterOpen(DataSet: TDataSet);
begin
  (*if QrSacOcorA.RecordCount > 0 then BtPagamento6.Enabled := True
  else BtPagamento6.Enabled := False;*)
  //
  FOcorA_Total := 0;
  while not QrSacOcorA.Eof do
  begin
    //if Int(Date) > QrOcorADataO.Value then
      FOcorA_Total := FOcorA_Total + QrSacOcorAATUALIZADO.Value;
    QrSacOcorA.Next;
  end;
end;

procedure TFmChequesExtras.QrSacOcorACalcFields(DataSet: TDataSet);
var
  Cliente: Integer;
begin
  if QrSacOcorACLIENTELOTE.Value > 0 then
    Cliente := QrSacOcorACLIENTELOTE.Value else
    Cliente := QrSacOcorACliente.Value;
  //
  QrSacOcorASALDO.Value := QrSacOcorAValor.Value + QrSacOcorATaxaV.Value - QrSacOcorAPago.Value;
  //
  QrSacOcorAATUALIZADO.Value := DMod.ObtemValorAtualizado(
    Cliente, 1, QrSacOcorADataO.Value, Date, QrSacOcorAData3.Value,
    QrSacOcorAValor.Value, QrSacOcorATaxaV.Value, 0 (*Desco*),
    QrSacOcorAPago.Value, QrSacOcorATaxaP.Value, False);
end;

procedure TFmChequesExtras.QrSacOcorABeforeClose(DataSet: TDataSet);
begin
  FCHDevOpen_Total := 0;
end;

procedure TFmChequesExtras.QrSacDOpenBeforeClose(DataSet: TDataSet);
begin
  LimpaRisco;
end;

procedure TFmChequesExtras.LimpaRisco;
begin
  EdDV.Text := '';
  EdDR.Text := '';
  EdDT.Text := '';
  EdCV.Text := '';
  EdCR.Text := '';
  EdCT.Text := '';
  EdRT.Text := '';
  EdVT.Text := '';
  EdST.Text := '';
  EdOA.Text := '';
  EdTT.Text := '';
end;

procedure TFmChequesExtras.QrSacDOpenCalcFields(DataSet: TDataSet);
begin
  QrSacDOpenSALDO_DESATUALIZ.Value := QrSacDOpenValor.Value + QrSacDOpenTotalJr.Value -
  QrSacDOpenTotalDs.Value - QrSacDOpenTotalPg.Value;
  //
  QrSacDOpenNomeStatus.Value := MLAGeral.NomeStatusPgto2(QrSacDOpenQuitado.Value,
    QrSacDOpenDDeposito.Value, Date, QrSacDOpenData3.Value, QrSacDOpenRepassado.Value);
  //
  QrSacDOpenSALDO_ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrSacDOpenCliente.Value, QrSacDOpenQuitado.Value, QrSacDOpenVencto.Value, Date,
    QrSacDOpenData3.Value, QrSacDOpenValor.Value, QrSacDOpenTotalJr.Value, QrSacDOpenTotalDs.Value,
    QrSacDOpenTotalPg.Value, 0, True);
end;

procedure TFmChequesExtras.QrSacRiscoCCalcFields(DataSet: TDataSet);
begin
  QrSacRiscoCCPF_TXT.Value := Geral.FormataCNPJ_TT(QrSacRiscoCCPF.Value);
end;

procedure TFmChequesExtras.ReopenAvulsos(Controle: Integer);
begin
  QrAvulsos.Close;
  QrAvulsos.Open;
  //
  if Controle > 0 then QrAvulsos.Locate('Controle', Controle, []);
end;

procedure TFmChequesExtras.QrSacCHDevACalcFields(DataSet: TDataSet);
begin
  QrSacCHDevADATA1_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrSacCHDevAData1.Value);
  QrSacCHDevADATA2_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrSacCHDevAData2.Value);
  QrSacCHDevADATA3_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrSacCHDevAData3.Value);
  QrSacCHDevACPF_TXT.Value := Geral.FormataCNPJ_TT(QrSacCHDevACPF.Value);
  QrSacCHDevASALDO.Value := QrSacCHDevAValor.Value +QrSacCHDevATaxas.Value +
    QrSacCHDevAMulta.Value + QrSacCHDevAJurosV.Value - QrSacCHDevAValPago.Value -
    QrSacCHDevADesconto.Value - QrSacCHDevAPgDesc.Value;
  //
  QrSacCHDevAATUAL.Value := DMod.ObtemValorAtualizado(
    QrSacCHDevACliente.Value, QrSacCHDevAStatus.Value, QrSacCHDevAData1.Value, Date,
    QrSacCHDevAData3.Value, QrSacCHDevAValor.Value, QrSacCHDevAJurosV.Value,
    QrSacCHDevADesconto.Value, QrSacCHDevAValPago.Value +
    QrSacCHDevAPgDesc.Value, QrSacCHDevAJurosP.Value, False);
end;

procedure TFmChequesExtras.QrAvulsosCalcFields(DataSet: TDataSet);
begin
  QrAvulsosCPF_TXT.Value := Geral.FormataCNPJ_TT(QrAvulsosCPF.Value);
end;

procedure TFmChequesExtras.EdData4Exit(Sender: TObject);
var
  Data: TDateTime;
begin
  Data := Geral.ValidaDataSimples(EdData4.Text, False);
  if Data > 2 then
  begin
    EdData4.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdData4.SetFocus;
end;

procedure TFmChequesExtras.EdVence4Exit(Sender: TObject);
var
  Data, Vence: TDateTime;
  Casas: Integer;
begin
  Casas := Length(EdVence4.Text);
  Data  := Geral.ValidaDataSimples(EdData4.Text , False);
  Vence := Geral.ValidaDataSimples(EdVence4.Text, False);
  if Vence > 2 then
  begin
    if Vence < Data then
      if Casas < 6 then Data := IncMonth(Vence, 12);
    if Vence < Data then
    begin
      if Application.MessageBox('Data j� passada! Deseja incrementar um ano?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Vence := IncMonth(Vence, 12);
        EdVence4.SetFocus;
      end else begin
        EdVence4.SetFocus;
        Exit;
      end;
    end;
    EdVence4.Text := FormatDateTime('dd/mm/yyyy', Vence);
  end else EdVence4.SetFocus;
end;

procedure TFmChequesExtras.EdCPF_2Change(Sender: TObject);
begin
  LimpaRisco;
end;

procedure TFmChequesExtras.BtExcluiClick(Sender: TObject);
var
  Controle : Integer;
begin
  if Application.MessageBox(PChar('Confirma a exclus�o deste cheque?'),
  'Pergunta de exclus�o',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Controle := QrAvulsosControle.Value;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM lotesits WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    ReopenAvulsos(Controle);
    //
    Screen.Cursor := crDefault;
  end;
end;

end.

