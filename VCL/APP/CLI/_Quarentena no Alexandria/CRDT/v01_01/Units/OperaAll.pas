unit OperaAll;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, ComCtrls,
  Grids, DBGrids, frxClass, frxDBSet, Variants, dmkGeral, dmkEdit, dmkEditCB,
  dmkDBLookupComboBox, UnDmkEnums;

type
  TFmOperaAll = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtCalcula: TBitBtn;
    ProgressBar1: TProgressBar;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    TPFim: TDateTimePicker;
    Label1: TLabel;
    Label34: TLabel;
    TPIni: TDateTimePicker;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DBGrid1: TDBGrid;
    QrOper1: TmySQLQuery;
    QrOper1Codigo: TIntegerField;
    QrOper1Tipo: TSmallintField;
    QrOper1Spread: TSmallintField;
    QrOper1Cliente: TIntegerField;
    QrOper1Lote: TIntegerField;
    QrOper1Data: TDateField;
    QrOper1Total: TFloatField;
    QrOper1Dias: TFloatField;
    QrOper1PeCompra: TFloatField;
    QrOper1TxCompra: TFloatField;
    QrOper1ValValorem: TFloatField;
    QrOper1AdValorem: TFloatField;
    QrOper1IOC: TFloatField;
    QrOper1IOC_VAL: TFloatField;
    QrOper1Tarifas: TFloatField;
    QrOper1CPMF: TFloatField;
    QrOper1CPMF_VAL: TFloatField;
    QrOper1TipoAdV: TIntegerField;
    QrOper1IRRF: TFloatField;
    QrOper1IRRF_Val: TFloatField;
    QrOper1ISS: TFloatField;
    QrOper1ISS_Val: TFloatField;
    QrOper1PIS: TFloatField;
    QrOper1PIS_Val: TFloatField;
    QrOper1PIS_R: TFloatField;
    QrOper1PIS_R_Val: TFloatField;
    QrOper1COFINS: TFloatField;
    QrOper1COFINS_Val: TFloatField;
    QrOper1COFINS_R: TFloatField;
    QrOper1COFINS_R_Val: TFloatField;
    QrOper1OcorP: TFloatField;
    QrOper1MaxVencto: TDateField;
    QrOper1CHDevPg: TFloatField;
    QrOper1Lk: TIntegerField;
    QrOper1DataCad: TDateField;
    QrOper1DataAlt: TDateField;
    QrOper1UserCad: TIntegerField;
    QrOper1UserAlt: TIntegerField;
    QrOper1MINTC: TFloatField;
    QrOper1MINAV: TFloatField;
    QrOper1MINTC_AM: TFloatField;
    QrOper1PIS_T_Val: TFloatField;
    QrOper1COFINS_T_Val: TFloatField;
    QrOper1PgLiq: TFloatField;
    QrOper1DUDevPg: TFloatField;
    QrOper1NF: TIntegerField;
    QrOper1Itens: TIntegerField;
    QrOper1Conferido: TIntegerField;
    QrOper1ECartaSac: TSmallintField;
    QrOper1CBE: TIntegerField;
    QrOper1NOMECLIENTE: TWideStringField;
    QrOper1IOFd: TFloatField;
    QrOper1IOFd_VAL: TFloatField;
    QrOper1IOFv: TFloatField;
    QrOper1IOFv_VAL: TFloatField;
    QrLoteCHDevP: TmySQLQuery;
    QrLoteCHDevPJuros: TFloatField;
    QrLoteDUDevP: TmySQLQuery;
    QrLoteDUDevPJuros: TFloatField;
    QrEmLo1: TmySQLQuery;
    QrEmLo1Codigo: TIntegerField;
    QrEmLo1ValValorem: TFloatField;
    QrEmLo1TaxaVal: TFloatField;
    QrLReA: TmySQLQuery;
    QrLReACodigo: TIntegerField;
    QrLReAData: TDateField;
    QrLReANomeCli: TWideStringField;
    QrLReALote: TIntegerField;
    QrLReANF: TIntegerField;
    QrLReATotal: TFloatField;
    QrLReATC0: TFloatField;
    QrLReAVV0: TFloatField;
    QrLReAETC: TFloatField;
    QrLReAEVV: TFloatField;
    QrLReATTC: TFloatField;
    QrLReATVV: TFloatField;
    QrLReADias: TFloatField;
    QrLReATTC_T: TFloatField;
    QrLReATTC_J: TFloatField;
    QrLReAALL_T: TFloatField;
    QrLReAALL_J: TFloatField;
    QrLReAddVal: TFloatField;
    QrLReANomeMes: TWideStringField;
    QrLReAMes: TIntegerField;
    QrLReAQtdeDocs: TIntegerField;
    QrLReAMediaDoc: TFloatField;
    QrLReAITEM: TIntegerField;
    QrLReATC1: TFloatField;
    QrLReATC2: TFloatField;
    QrLReATC3: TFloatField;
    QrLReATC4: TFloatField;
    QrLReATC5: TFloatField;
    QrLReATC6: TFloatField;
    QrLReAVV1: TFloatField;
    QrLReAVV2: TFloatField;
    QrLReAVV3: TFloatField;
    QrLReAVV4: TFloatField;
    QrLReAVV5: TFloatField;
    QrLReAVV6: TFloatField;
    QrLReAISS_Val: TFloatField;
    QrLReAPIS_Val: TFloatField;
    QrLReACOFINS_Val: TFloatField;
    QrLReAImpostos: TFloatField;
    QrLReATRC_T: TFloatField;
    QrLReATRC_J: TFloatField;
    QrLReAddLiq: TFloatField;
    QrLReAIOF_TOTAL: TFloatField;
    QrLReATarifas: TFloatField;
    QrLReAPgOcorren: TFloatField;
    QrLReACHDevPg: TFloatField;
    QrLReADUDevPg: TFloatField;
    DsLReA: TDataSource;
    QrOcorrPg: TmySQLQuery;
    QrOcorrPgData: TDateField;
    QrOcorrPgJuros: TFloatField;
    QrAlinPGs: TmySQLQuery;
    QrAlinPGsData: TDateField;
    QrAlinPGsJuros: TFloatField;
    QrAdupPgs: TmySQLQuery;
    QrAdupPgsData: TDateField;
    QrAdupPgsJuros: TFloatField;
    QrLoc: TmySQLQuery;
    QrLReAColCod01: TIntegerField;
    QrLReAColCod02: TIntegerField;
    QrLReAColCod03: TIntegerField;
    QrLReAColCod04: TIntegerField;
    QrLReAColCod05: TIntegerField;
    QrLReAColCod06: TIntegerField;
    QrLReAColCod07: TIntegerField;
    QrLReAColCod08: TIntegerField;
    QrLReAColCod09: TIntegerField;
    QrLReAColCod10: TIntegerField;
    QrLReAColNom01: TWideStringField;
    QrLReAColNom02: TWideStringField;
    QrLReAColNom03: TWideStringField;
    QrLReAColNom04: TWideStringField;
    QrLReAColNom05: TWideStringField;
    QrLReAColNom06: TWideStringField;
    QrLReAColNom07: TWideStringField;
    QrLReAColNom08: TWideStringField;
    QrLReAColNom09: TWideStringField;
    QrLReAColNom10: TWideStringField;
    QrLReAColVal01: TFloatField;
    QrLReAColVal02: TFloatField;
    QrLReAColVal03: TFloatField;
    QrLReAColVal04: TFloatField;
    QrLReAColVal05: TFloatField;
    QrLReAColVal06: TFloatField;
    QrLReAColVal07: TFloatField;
    QrLReAColVal08: TFloatField;
    QrLReAColVal09: TFloatField;
    QrLReAColVal10: TFloatField;
    QrLoteOcorrPg: TmySQLQuery;
    QrLoteOcorrPgJuros: TFloatField;
    QrLocCol: TmySQLQuery;
    QrLocData: TmySQLQuery;
    QrColSBor: TmySQLQuery;
    QrLReAColCod11: TIntegerField;
    QrLReAColCod12: TIntegerField;
    QrLReAColNom11: TWideStringField;
    QrLReAColNom12: TWideStringField;
    QrLReAColVal11: TFloatField;
    QrLReAColVal12: TFloatField;
    QrLReAColTip01: TSmallintField;
    QrLReAColTip02: TSmallintField;
    QrLReAColTip03: TSmallintField;
    QrLReAColTip04: TSmallintField;
    QrLReAColTip05: TSmallintField;
    QrLReAColTip06: TSmallintField;
    QrLReAColTip07: TSmallintField;
    QrLReAColTip08: TSmallintField;
    QrLReAColTip09: TSmallintField;
    QrLReAColTip10: TSmallintField;
    QrLReAColTip11: TSmallintField;
    QrLReAColTip12: TSmallintField;
    BtResultado: TBitBtn;
    frxOper3: TfrxReport;
    RGModelo: TRadioGroup;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGSintetico: TRadioGroup;
    frxDsLReA: TfrxDBDataset;
    frxOperaAll: TfrxReport;
    frxOper1: TfrxReport;
    frxOper2: TfrxReport;
    frxDsLotesTxs: TfrxDBDataset;
    QrLotesTxs: TmySQLQuery;
    QrLotesTxsNome: TWideStringField;
    QrLotesTxsCodigo: TIntegerField;
    QrLotesTxsControle: TIntegerField;
    QrLotesTxsTaxaCod: TIntegerField;
    QrLotesTxsTaxaTxa: TFloatField;
    QrLotesTxsTaxaVal: TFloatField;
    QrLotesTxsTaxaQtd: TFloatField;
    QrLotesTxsLk: TIntegerField;
    QrLotesTxsDataCad: TDateField;
    QrLotesTxsDataAlt: TDateField;
    QrLotesTxsUserCad: TIntegerField;
    QrLotesTxsUserAlt: TIntegerField;
    QrLotesTxsTAXA_STR: TWideStringField;
    QrLotesTxsForma: TSmallintField;
    QrLotesTxsSysAQtd: TFloatField;
    QrLotesTxsCALCQtd: TFloatField;
    QrLotesTxsTaxaValNEG: TFloatField;
    frxDsOcorP: TfrxDBDataset;
    QrOcorP: TmySQLQuery;
    QrOcorPDOCUM_TXT: TWideStringField;
    QrOcorPBanco: TIntegerField;
    QrOcorPAgencia: TIntegerField;
    QrOcorPConta: TWideStringField;
    QrOcorPCheque: TIntegerField;
    QrOcorPDuplicata: TWideStringField;
    QrOcorPTipo: TSmallintField;
    QrOcorPTIPODOC: TWideStringField;
    QrOcorPNOMEOCORRENCIA: TWideStringField;
    QrOcorPCodigo: TIntegerField;
    QrOcorPOcorreu: TIntegerField;
    QrOcorPData: TDateField;
    QrOcorPJuros: TFloatField;
    QrOcorPPago: TFloatField;
    QrOcorPLotePg: TIntegerField;
    QrOcorPLk: TIntegerField;
    QrOcorPDataCad: TDateField;
    QrOcorPDataAlt: TDateField;
    QrOcorPUserCad: TIntegerField;
    QrOcorPUserAlt: TIntegerField;
    QrOcorPOcorrencia: TIntegerField;
    QrOcorPValor: TFloatField;
    QrOcorPDataO: TDateField;
    QrOcorPSALDO: TFloatField;
    QrOcorPEmitente: TWideStringField;
    QrOcorPCPF: TWideStringField;
    QrOcorPCPF_TXT: TWideStringField;
    QrOcorPDescri: TWideStringField;
    QrOcorPPagoNeg: TFloatField;
    QrOcorPSALDONEG: TFloatField;
    QrCHDevP: TmySQLQuery;
    QrCHDevPData: TDateField;
    QrCHDevPJuros: TFloatField;
    QrCHDevPPago: TFloatField;
    QrCHDevPBanco: TIntegerField;
    QrCHDevPAgencia: TIntegerField;
    QrCHDevPConta: TWideStringField;
    QrCHDevPCheque: TIntegerField;
    QrCHDevPCPF: TWideStringField;
    QrCHDevPValor: TFloatField;
    QrCHDevPTaxas: TFloatField;
    QrCHDevPMulta: TFloatField;
    QrCHDevPEmitente: TWideStringField;
    QrCHDevPStatus: TSmallintField;
    QrCHDevPJurosP: TFloatField;
    QrCHDevPJurosV: TFloatField;
    QrCHDevPDesconto: TFloatField;
    QrCHDevPNOMESTATUS: TWideStringField;
    QrCHDevPAlinIts: TIntegerField;
    QrCHDevPCodigo: TIntegerField;
    QrCHDevPCPF_TXT: TWideStringField;
    QrCHDevPValPago: TFloatField;
    QrCHDevPSALDO: TFloatField;
    QrCHDevPLotePG: TIntegerField;
    frxDsCHDevP: TfrxDBDataset;
    QrDPago: TmySQLQuery;
    QrDPagoData: TDateField;
    QrDPagoJuros: TFloatField;
    QrDPagoPago: TFloatField;
    QrDPagoLotePg: TIntegerField;
    QrDPagoLk: TIntegerField;
    QrDPagoDataCad: TDateField;
    QrDPagoDataAlt: TDateField;
    QrDPagoUserCad: TIntegerField;
    QrDPagoUserAlt: TIntegerField;
    QrDPagoLotesIts: TIntegerField;
    QrDPagoControle: TIntegerField;
    QrDPagoDesco: TFloatField;
    QrDPagoEmitente: TWideStringField;
    QrDPagoCPF: TWideStringField;
    QrDPagoValor: TFloatField;
    QrDPagoDuplicata: TWideStringField;
    QrDPagoNOMESTATUS: TWideStringField;
    QrDPagoQuitado: TIntegerField;
    QrDPagoCPF_TXT: TWideStringField;
    QrDPagoSALDO: TFloatField;
    frxDsDPago: TfrxDBDataset;
    QrOcorPSL: TmySQLQuery;
    QrOcorPSLDOCUM_TXT: TWideStringField;
    QrOcorPSLBanco: TIntegerField;
    QrOcorPSLAgencia: TIntegerField;
    QrOcorPSLConta: TWideStringField;
    QrOcorPSLCheque: TIntegerField;
    QrOcorPSLDuplicata: TWideStringField;
    QrOcorPSLTipo: TSmallintField;
    QrOcorPSLTIPODOC: TWideStringField;
    QrOcorPSLNOMEOCORRENCIA: TWideStringField;
    QrOcorPSLCodigo: TIntegerField;
    QrOcorPSLOcorreu: TIntegerField;
    QrOcorPSLData: TDateField;
    QrOcorPSLJuros: TFloatField;
    QrOcorPSLPago: TFloatField;
    QrOcorPSLLotePg: TIntegerField;
    QrOcorPSLLk: TIntegerField;
    QrOcorPSLDataCad: TDateField;
    QrOcorPSLDataAlt: TDateField;
    QrOcorPSLUserCad: TIntegerField;
    QrOcorPSLUserAlt: TIntegerField;
    QrOcorPSLOcorrencia: TIntegerField;
    QrOcorPSLValor: TFloatField;
    QrOcorPSLDataO: TDateField;
    QrOcorPSLSALDO: TFloatField;
    QrOcorPSLEmitente: TWideStringField;
    QrOcorPSLCPF: TWideStringField;
    QrOcorPSLCPF_TXT: TWideStringField;
    QrOcorPSLDescri: TWideStringField;
    QrOcorPSLPagoNeg: TFloatField;
    QrOcorPSLSALDONEG: TFloatField;
    frxDsOcorPSL: TfrxDBDataset;
    QrCHDevPSL: TmySQLQuery;
    QrCHDevPSLData: TDateField;
    QrCHDevPSLJuros: TFloatField;
    QrCHDevPSLPago: TFloatField;
    QrCHDevPSLBanco: TIntegerField;
    QrCHDevPSLAgencia: TIntegerField;
    QrCHDevPSLConta: TWideStringField;
    QrCHDevPSLCheque: TIntegerField;
    QrCHDevPSLCPF: TWideStringField;
    QrCHDevPSLValor: TFloatField;
    QrCHDevPSLTaxas: TFloatField;
    QrCHDevPSLMulta: TFloatField;
    QrCHDevPSLEmitente: TWideStringField;
    QrCHDevPSLStatus: TSmallintField;
    QrCHDevPSLJurosP: TFloatField;
    QrCHDevPSLJurosV: TFloatField;
    QrCHDevPSLDesconto: TFloatField;
    QrCHDevPSLNOMESTATUS: TWideStringField;
    QrCHDevPSLAlinIts: TIntegerField;
    QrCHDevPSLCodigo: TIntegerField;
    QrCHDevPSLCPF_TXT: TWideStringField;
    QrCHDevPSLValPago: TFloatField;
    QrCHDevPSLSALDO: TFloatField;
    QrCHDevPSLLotePG: TIntegerField;
    QrDPagoSL: TmySQLQuery;
    QrDPagoSLData: TDateField;
    QrDPagoSLJuros: TFloatField;
    QrDPagoSLPago: TFloatField;
    QrDPagoSLLotePg: TIntegerField;
    QrDPagoSLLk: TIntegerField;
    QrDPagoSLDataCad: TDateField;
    QrDPagoSLDataAlt: TDateField;
    QrDPagoSLUserCad: TIntegerField;
    QrDPagoSLUserAlt: TIntegerField;
    QrDPagoSLLotesIts: TIntegerField;
    QrDPagoSLControle: TIntegerField;
    QrDPagoSLDesco: TFloatField;
    QrDPagoSLEmitente: TWideStringField;
    QrDPagoSLCPF: TWideStringField;
    QrDPagoSLValor: TFloatField;
    QrDPagoSLDuplicata: TWideStringField;
    QrDPagoSLNOMESTATUS: TWideStringField;
    QrDPagoSLQuitado: TIntegerField;
    QrDPagoSLCPF_TXT: TWideStringField;
    QrDPagoSLSALDO: TFloatField;
    frxDsDPagoSL: TfrxDBDataset;
    frxDsCHDevPSL: TfrxDBDataset;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrLReACalcFields(DataSet: TDataSet);
    procedure BtResultadoClick(Sender: TObject);
    procedure frxOper3GetValue(const VarName: string; var Value: Variant);
    function frxOper3UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure BtCalculaClick(Sender: TObject);
    procedure RGModeloClick(Sender: TObject);
    procedure frxOper1GetValue(const VarName: string; var Value: Variant);
    function frxOper1UserFunction(const MethodName: string;
      var Params: Variant): Variant;
    procedure QrLotesTxsCalcFields(DataSet: TDataSet);
    procedure QrOcorPCalcFields(DataSet: TDataSet);
    procedure QrCHDevPCalcFields(DataSet: TDataSet);
    procedure QrDPagoCalcFields(DataSet: TDataSet);
    procedure QrOcorPSLCalcFields(DataSet: TDataSet);
    procedure QrCHDevPSLCalcFields(DataSet: TDataSet);
    procedure QrDPagoSLCalcFields(DataSet: TDataSet);
    procedure QrLReAAfterScroll(DataSet: TDataSet);
    procedure QrLReABeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    const
      MaxCols = 12; //M�ximo de colunas do relat�rio
    var
      FContaGF1: Integer;
      FLReA: String;
      FMostraCols: Boolean;
    procedure ReopenTxs;
    procedure ReopenOcorP;
    procedure ReopenCHDevP;
    procedure ReopenDPago;
    procedure ReopenOcorPSL;
    procedure ReopenCHDevPSL;
    procedure ReopenDPagoSL;
    procedure ReopenQrLReA(VerificaBot, Redefine: Boolean);
    procedure ConfiguraGrade(ColunasVisi: Boolean);
    procedure CalculaModeloA();
    procedure CalculaModeloB();
    function LocalizaData(Codigo: Integer; Data: TDate): TDate;
    function LocalizaColuna(Tipo, CodCol: Integer): Integer;
    function LocalizaProximaColunaVazia(): Integer;
    function CalculaJuroMes_frxReport(d, v, t: Extended): String;
  public
    { Public declarations }
  end;

  var
  FmOperaAll: TFmOperaAll;

implementation

{$R *.DFM}

uses Module, UnInternalConsts, Principal, UCreate, UMySQLModule, ModuleGeral,
Periodo, MeuFrx, UnMyObjects;

procedure TFmOperaAll.BtCalculaClick(Sender: TObject);
var
  DataI, DataF: TDate;
begin
  DataI := TPIni.Date;
  DataF := TPFim.Date;
  if DataI > DataF then
  begin
    Geral.MensagemBox('A data inicial deve ser superior a data final!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  case RGModelo.ItemIndex of
    0: CalculaModeloA;
    1: CalculaModeloB;
  end;
end;

procedure TFmOperaAll.BtResultadoClick(Sender: TObject);
const
  ColSize = 16;
var
  Modelo: Integer;
  s: TMemoryStream;
  Page: TfrxReportPage;
begin
  Modelo := RGModelo.ItemIndex;
  //
  case Modelo of
    0:
    begin
      case RGSintetico.ItemIndex of
        0:
        begin
          frxOper2.AddFunction(
            'function CalculaJuroMes_frxReport(d, v, t: Extended): String');
          MyObjects.frxMostra(frxOper2,
            'Relat�rio de Opera��es');
        end;
        1:
        begin
          frxOper1.AddFunction(
            'function CalculaJuroMes_frxReport(d, v, t: Extended): String');
          MyObjects.frxMostra(frxOper1,
            'Relat�rio de Opera��es');
        end;
      end;
    end;
    1:
    begin
      Page            := TfrxReportPage(frxOper3.Pages[1]);
      Page.PaperWidth := 265;
      //
      if QrLReAColNom01.Value <> '' then
        Page.PaperWidth := Page.PaperWidth + ColSize;
      if QrLReAColNom02.Value <> '' then
        Page.PaperWidth := Page.PaperWidth + ColSize;
      if QrLReAColNom03.Value <> '' then
        Page.PaperWidth := Page.PaperWidth + ColSize;
      if QrLReAColNom04.Value <> '' then
        Page.PaperWidth := Page.PaperWidth + ColSize;
      if QrLReAColNom05.Value <> '' then
        Page.PaperWidth := Page.PaperWidth + ColSize;
      if QrLReAColNom06.Value <> '' then
        Page.PaperWidth := Page.PaperWidth + ColSize;
      if QrLReAColNom07.Value <> '' then
        Page.PaperWidth := Page.PaperWidth + ColSize;
      if QrLReAColNom08.Value <> '' then
        Page.PaperWidth := Page.PaperWidth + ColSize;
      if QrLReAColNom09.Value <> '' then
        Page.PaperWidth := Page.PaperWidth + ColSize;
      if QrLReAColNom10.Value <> '' then
        Page.PaperWidth := Page.PaperWidth + ColSize;
      if QrLReAColNom11.Value <> '' then
        Page.PaperWidth := Page.PaperWidth + ColSize;
      if QrLReAColNom12.Value <> '' then
        Page.PaperWidth := Page.PaperWidth + ColSize;
      //
      frxOperaAll.AddFunction(
        'function CalculaJuroMes_frxReport(d, v, t: Extended): String');
      //
      frxOperaAll.OnGetValue         := frxOper3.OnGetValue;
      frxOperaAll.OnUserFunction     := frxOper3.OnUserFunction;
      frxOperaAll.ReportOptions.Name := 'Relat�rio de Opera��es';
      //
      s := TMemoryStream.Create;
      frxOper3.SaveToStream(s);
      s.Position := 0;
      frxOperaAll.LoadFromStream(s);
      frxOperaAll.PrepareReport(True);
      //
      Application.CreateForm(TFmMeuFrx, FmMeuFrx);
      frxOperaAll.Preview := FmMeufrx.PvVer;
      //
      FmMeufrx.PvVer.OutlineWidth := frxOperaAll.PreviewOptions.OutlineWidth;
      FmMeufrx.PvVer.Zoom         := frxOperaAll.PreviewOptions.Zoom;
      FmMeufrx.PvVer.ZoomMode     := frxOperaAll.PreviewOptions.ZoomMode;
      FmMeufrx.UpdateZoom;
      //
      FmMeufrx.ShowModal;
      FmMeufrx.Destroy;
    end;
  end;
end;

procedure TFmOperaAll.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmOperaAll.CalculaJuroMes_frxReport(d, v, t: Extended): String;
var
  Taxa, Dias: Double;
  Res: String;
begin
  if v > 0 then
  begin
    Taxa := t / v * 100;
    Dias := d / v;
    Res := Geral.FFT(MLAGeral.DescobreJuroComposto(Taxa, Dias, 2), 2, siNegativo);
    Result := Res;
  end else Result := '0.00';
end;

procedure TFmOperaAll.CalculaModeloA;
var
  Periodo: Integer;
  i, j: Integer;
  TTC_T, TRC_T, TTC_J, TRC_J, MediaDoc, JurOcorr, JurCHDev, JurDUDev: Double;
  Data: TDate;
begin
  Screen.Cursor       := crHourGlass;
  BtResultado.Enabled := False;
  //
  FLReA := UCriar.RecriaTempTableNovo(ntrttLReA, DmodG.QrUpdPID1, True, 1, '');
  //
  QrOper1.Close;
  QrOper1.SQL.Clear;
  QrOper1.SQL.Add('SELECT lo.*, CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrOper1.SQL.Add('ELSE en.Nome END NOMECLIENTE');
  QrOper1.SQL.Add('FROM lotes lo');
  QrOper1.SQL.Add('LEFT JOIN entidades en ON en.Codigo=lo.Cliente');
  QrOper1.SQL.Add('WHERE lo.data BETWEEN :P0 AND :P1');
  QrOper1.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrOper1.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  QrOper1.Open;
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FLReA + ' SET Codigo=:P0, Data=:P1, ');
  DmodG.QrUpdPID1.SQL.Add('NomeCli=:P2, Lote=:P3, NF=:P4, Total=:P5, ');
  DmodG.QrUpdPID1.SQL.Add('TC0=:P6, VV0=:P7, TTC=:P8, TVV=:P9, Dias=:P10, ');
  DmodG.QrUpdPID1.SQL.Add('Mes=:P11, NomeMes=:P12, QtdeDocs=:P13, MediaDoc=:P14, ');
  DmodG.QrUpdPID1.SQL.Add('ISS_Val=:P15, PIS_Val=:P16, COFINS_Val=:P17, ');
  DmodG.QrUpdPID1.SQL.Add('Impostos=:P18, IOF_TOTAL=:P19, Tarifas=:P20, ');
  DModG.QrUpdPID1.SQL.Add('PgOcorren=:P21, CHDevPg=:P22, DUDevPg=:P23');
  //
  ProgressBar1.Position := 0;
  ProgressBar1.Max      := QrOper1.RecordCount;
  while not QrOper1.Eof do
  begin
    if QrOper1Itens.Value = 0 then
      MediaDoc := 0
    else
      MediaDoc := QrOper1Total.Value / QrOper1Itens.Value;
    //
    Periodo               := Geral.Periodo2000(QrOper1Data.Value);
    ProgressBar1.Position := ProgressBar1.Position +1;
    //
    //Juros das ocorr�ncias dos lotes
    QrLoteOcorrPg.Close;
    QrLoteOcorrPg.Params[0].AsInteger := QrOper1Codigo.Value;
    QrLoteOcorrPg.Open;
    //
    JurOcorr := QrLoteOcorrPgJuros.Value;
    //
    //Juros dos cheques devolvidos
    QrLoteCHDevP.Close;
    QrLoteCHDevP.Params[0].AsInteger := QrOper1Codigo.Value;
    QrLoteCHDevP.Open;
    //
    JurCHDev := QrLoteCHDevPJuros.Value;
    //
    //Juros das duplicatasa devolvidas
    QrLoteDUDevP.Close;
    QrLoteDUDevP.Params[0].AsInteger := QrOper1Codigo.Value;
    QrLoteDUDevP.Open;
    //
    JurDUDev := QrLoteDUDevPJuros.Value;
    //
    DmodG.QrUpdPID1.Params[00].AsInteger := QrOper1Codigo.Value;
    DmodG.QrUpdPID1.Params[01].AsString  := Geral.FDT(QrOper1Data.Value, 1);
    DmodG.QrUpdPID1.Params[02].AsString  := QrOper1NOMECLIENTE.Value;
    DmodG.QrUpdPID1.Params[03].AsInteger := QrOper1Lote.Value;
    DmodG.QrUpdPID1.Params[04].AsInteger := QrOper1NF.Value;
    DmodG.QrUpdPID1.Params[05].AsFloat   := QrOper1Total.Value;
    DmodG.QrUpdPID1.Params[06].AsFloat   := QrOper1TxCompra.Value;
    DmodG.QrUpdPID1.Params[07].AsFloat   := QrOper1ValValorem.Value;
    DmodG.QrUpdPID1.Params[08].AsFloat   := QrOper1TxCompra.Value;
    DmodG.QrUpdPID1.Params[09].AsFloat   := QrOper1ValValorem.Value;
    DmodG.QrUpdPID1.Params[10].AsFloat   := QrOper1Dias.Value;
    DmodG.QrUpdPID1.Params[11].AsInteger := Periodo;
    DmodG.QrUpdPID1.Params[12].AsString  := MLAGeral.MesEAnoDoPeriodo(Periodo);
    DmodG.QrUpdPID1.Params[13].AsFloat   := QrOper1Itens.Value;
    DmodG.QrUpdPID1.Params[14].AsFloat   := MediaDoc;
    DmodG.QrUpdPID1.Params[15].AsFloat   := QrOper1ISS_Val.Value;
    DmodG.QrUpdPID1.Params[16].AsFloat   := QrOper1PIS_T_Val.Value;
    DmodG.QrUpdPID1.Params[17].AsFloat   := QrOper1COFINS_T_Val.Value;
    DmodG.QrUpdPID1.Params[18].AsFloat   := QrOper1ISS_Val.Value +
                                            QrOper1PIS_T_Val.Value +
                                            QrOper1COFINS_T_Val.Value;
    DModG.QrUpdPID1.Params[19].AsFloat   := QrOper1IOC_VAL.Value +
                                            QrOper1IOFd_VAL.Value +
                                            QrOper1IOFv_VAL.Value;
    DModG.QrUpdPID1.Params[20].AsFloat   := QrOper1Tarifas.Value;
    DModG.QrUpdPID1.Params[21].AsFloat   := JurOcorr;
    DModG.QrUpdPID1.Params[22].AsFloat   := JurCHDev;
    DModG.QrUpdPID1.Params[23].AsFloat   := JurDUDev;
    DmodG.QrUpdPID1.ExecSQL;
    //
    QrOper1.Next;
  end;
  //
  ProgressBar1.Position := 0;
  QrEmLo1.Close;
  QrEmLo1.SQL.Clear;
  QrEmLo1.SQL.Add('SELECT el.Codigo, el.ValValorem, el.TaxaVal');
  QrEmLo1.SQL.Add('FROM emlot el');
  QrEmLo1.SQL.Add('LEFT JOIN creditor.lotes lo');
  QrEmLo1.SQL.Add('ON lo.Codigo=el.Codigo');
  QrEmLo1.SQL.Add('WHERE lo.data BETWEEN :P0 AND :P1');
  //
  QrEmLo1.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrEmLo1.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  //
  j := FmPrincipal.FMyDBs.MaxDBs -1;
  for i := 0 to j do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      QrEmLo1.Close;
      QrEmLo1.Database := Dmod.FArrayMySQLBD[i];
      QrEmLo1.Open;
      //
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FLReA + ' SET TC'+IntToStr(i+1)+'=:P0, ');
      DmodG.QrUpdPID1.SQL.Add('VV'+IntToStr(i+1)+'=:P1, ETC=ETC+ :P2, ');
      DmodG.QrUpdPID1.SQL.Add('EVV=EVV+ :P3, TTC=TTC+ :P4, TVV=TVV+ :P5');
      DmodG.QrUpdPID1.SQL.Add('WHERE Codigo=:Pa');
      //
      ProgressBar1.Max := QrEmLo1.RecordCount;
      //
      while not QrEmLo1.Eof do
      begin
        ProgressBar1.Position := ProgressBar1.Position +1;
        //
        DmodG.QrUpdPID1.Params[00].AsFloat   := QrEmLo1TaxaVal.Value;
        DmodG.QrUpdPID1.Params[01].AsFloat   := QrEmLo1ValValorem.Value;
        DmodG.QrUpdPID1.Params[02].AsFloat   := QrEmLo1TaxaVal.Value;
        DmodG.QrUpdPID1.Params[03].AsFloat   := QrEmLo1ValValorem.Value;
        DmodG.QrUpdPID1.Params[04].AsFloat   := QrEmLo1TaxaVal.Value;
        DmodG.QrUpdPID1.Params[05].AsFloat   := QrEmLo1ValValorem.Value;
        //
        DmodG.QrUpdPID1.Params[06].AsInteger := QrEmLo1Codigo.Value;
        DmodG.QrUpdPID1.ExecSQL;
        //
        QrEmLo1.Next;
      end;
      ProgressBar1.Position := 0;
    end;
  end;
  ReopenQrLReA(False, True);
  //
  ProgressBar1.Max := QrLReA.RecordCount;
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FLReA + ' SET TTC_T=:P0, TRC_T=:P1, TTC_J=:P2, ');
  DmodG.QrUpdPID1.SQL.Add('TRC_J=:P3, ddVal=:P4, ddLiq=:P5 WHERE Codigo=:Pa');
  //
  QrLReA.DisableControls;
  try
    QrLReA.First;
    while not QrLReA.Eof do
    begin
      ProgressBar1.Position := ProgressBar1.Position +1;
      if QrLReADias.Value = 0 then
      begin
        TTC_J := 0;
        TRC_J := 0;
      end else begin
        TTC_J := QrLReATTC.Value / QrLReATotal.Value * 100;
        TRC_J := (QrLReATTC.Value - QrLReAImpostos.Value) / QrLReATotal.Value * 100;
      end;
       //
      TTC_T := MLAGeral.DescobreJuroComposto(TTC_J, QrLReADias.Value, 2);
      TRC_T := MLAGeral.DescobreJuroComposto(TRC_J, QrLReADias.Value, 2);
      //
      DmodG.QrUpdPID1.Params[00].AsFloat   := TTC_T;
      DmodG.QrUpdPID1.Params[01].AsFloat   := TRC_T;
      DmodG.QrUpdPID1.Params[02].AsFloat   := TTC_J;
      DmodG.QrUpdPID1.Params[03].AsFloat   := TRC_J;
      DmodG.QrUpdPID1.Params[04].AsFloat   := QrLReATotal.Value * QrLReADias.Value;
      DmodG.QrUpdPID1.Params[05].AsFloat   := (QrLReATotal.Value - QrLReAImpostos.Value) * QrLReADias.Value;
      //
      DmodG.QrUpdPID1.Params[06].AsInteger := QrLReACodigo.Value;
      DmodG.QrUpdPID1.ExecSQL;
      //
      QrLReA.Next;
    end;
  finally
    QrLReA.EnableControls;
  end;
  //Ocorr�ncias sem lotes / Cheques - Duplicatas - Cliente
  QrOcorrPg.Close;
  QrOcorrPg.SQL.Clear;
  QrOcorrPg.SQL.Add('SELECT pg.Data, SUM(pg.Juros) Juros');
  QrOcorrPg.SQL.Add('FROM ocorrpg pg');
  QrOcorrPg.SQL.Add('WHERE pg.LotePG = 0');
  QrOcorrPg.SQL.Add('AND pg.Data BETWEEN :P0 AND :P1');
  QrOcorrPg.SQL.Add('GROUP BY pg.Data');
  QrOcorrPg.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrOcorrPg.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  QrOcorrPg.Open;
  //
  ProgressBar1.Position := 0;
  ProgressBar1.Max      := QrOcorrPg.RecordCount;
  //
  ReopenQrLReA(False, True);
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FLReA + ' SET Codigo=:P00, ');
  DmodG.QrUpdPID1.SQL.Add('Data=:P01, PgOcorren=:P02, NomeMes=:P03, NomeCli=:P04');
  //
  QrOcorrPg.First;
  while not QrOcorrPg.Eof do
  begin
    Periodo := Geral.Periodo2000(QrOcorrPgData.Value);
    //
    DModG.QrUpdPID1.Params[00].AsInteger := 0;
    DModG.QrUpdPID1.Params[01].AsString  := Geral.FDT(QrOcorrPgData.Value, 1);
    DModG.QrUpdPID1.Params[02].AsFloat   := QrOcorrPgJuros.Value;
    DmodG.QrUpdPID1.Params[03].AsString  := MLAGeral.MesEAnoDoPeriodo(Periodo);
    DModG.QrUpdPID1.Params[04].AsString  := '';
    DModG.QrUpdPID1.ExecSQL;
    //
    ProgressBar1.Position := ProgressBar1.Position + 1;
    //
    QrOcorrPg.Next;
  end;
  //Cheques devolvidos sem border�
  QrAlinPGs.Close;
  QrAlinPGs.SQL.Clear;
  QrAlinPGs.SQL.Add('SELECT pg.Data, (SUM(pg.Juros) - SUM(pg.Desco)) Juros');
  QrAlinPGs.SQL.Add('FROM alinpgs pg');
  QrAlinPGs.SQL.Add('WHERE pg.LotePG = 0');
  QrAlinPGs.SQL.Add('AND pg.Data BETWEEN :P0 AND :P1');
  QrAlinPGs.SQL.Add('GROUP BY pg.Data');
  QrAlinPGs.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrAlinPGs.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  QrAlinPGs.Open;
  //
  ProgressBar1.Position := 0;
  ProgressBar1.Max      := QrAlinPGs.RecordCount;
  //
  ReopenQrLReA(False, True);
  //
  QrAlinPGs.First;
  while not QrAlinPGs.Eof do
  begin
    Periodo := Geral.Periodo2000(QrAlinPGsData.Value);
    Data    := LocalizaData(0, QrAlinPGsData.Value);
    //
    if Data > 0 then
    begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FLReA + ' SET CHDevPg=:P00, ');
      DmodG.QrUpdPID1.SQL.Add('NomeMes=:P01, NomeCli=:P02 ');
      DModG.QrUpdPID1.SQL.Add('WHERE Codigo=:P03 AND Data=:P04');
      DModG.QrUpdPID1.Params[00].AsFloat   := QrAlinPGsJuros.Value;
      DModG.QrUpdPID1.Params[01].AsString  := MLAGeral.MesEAnoDoPeriodo(Periodo);
      DModG.QrUpdPID1.Params[02].AsString  := '';
      DmodG.QrUpdPID1.Params[03].AsInteger := 0;
      DModG.QrUpdPID1.Params[04].AsString  := Geral.FDT(QrAlinPGsData.Value, 1);
      DModG.QrUpdPID1.ExecSQL;
    end else
    begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FLReA + ' SET Codigo=:P00, ');
      DmodG.QrUpdPID1.SQL.Add('Data=:P01, CHDevPg=:P02, NomeMes=:P03, NomeCli=:P04');
      DModG.QrUpdPID1.Params[00].AsInteger := 0;
      DModG.QrUpdPID1.Params[01].AsString  := Geral.FDT(QrAlinPGsData.Value, 1);
      DModG.QrUpdPID1.Params[02].AsFloat   := QrAlinPGsJuros.Value;
      DmodG.QrUpdPID1.Params[03].AsString  := MLAGeral.MesEAnoDoPeriodo(Periodo);
      DModG.QrUpdPID1.Params[04].AsString  := '';
      DModG.QrUpdPID1.ExecSQL;
    end;
    //
    ProgressBar1.Position := ProgressBar1.Position + 1;
    //
    QrAlinPGs.Next;
  end;
  //
  //Duplicatas devolvidas sem border�
  QrAdupPgs.Close;
  QrAdupPgs.SQL.Clear;
  QrAdupPgs.SQL.Add('SELECT pg.Data, (SUM(pg.Juros) - SUM(pg.Desco)) Juros');
  QrAdupPgs.SQL.Add('FROM aduppgs pg');
  QrAdupPgs.SQL.Add('WHERE pg.LotePG = 0');
  QrAdupPgs.SQL.Add('AND pg.Data BETWEEN :P0 AND :P1');
  QrAdupPgs.SQL.Add('GROUP BY pg.Data');
  QrAdupPgs.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrAdupPgs.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  QrAdupPgs.Open;
  //
  ProgressBar1.Position := 0;
  ProgressBar1.Max      := QrAdupPgs.RecordCount;
  //
  ReopenQrLReA(False, True);
  //
  QrAdupPgs.First;
  while not QrAdupPgs.Eof do
  begin
    Periodo := Geral.Periodo2000(QrAdupPgsData.Value);
    Data    := LocalizaData(0, QrAdupPgsData.Value);
    //
    if Data > 0 then
    begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FLReA + ' SET DUDevPg=:P00, ');
      DmodG.QrUpdPID1.SQL.Add('NomeMes=:P01, NomeCli=:P02 ');
      DModG.QrUpdPID1.SQL.Add('WHERE Codigo=:P03 AND Data=:P04');
      DModG.QrUpdPID1.Params[00].AsFloat   := QrAdupPgsJuros.Value;
      DModG.QrUpdPID1.Params[01].AsString  := MLAGeral.MesEAnoDoPeriodo(Periodo);
      DModG.QrUpdPID1.Params[02].AsString  := '';
      DmodG.QrUpdPID1.Params[03].AsInteger := 0;
      DModG.QrUpdPID1.Params[04].AsString  := Geral.FDT(QrAdupPgsData.Value, 1);
      DModG.QrUpdPID1.ExecSQL;
    end else
    begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FLReA + ' SET Codigo=:P00, ');
      DmodG.QrUpdPID1.SQL.Add('Data=:P01, DUDevPg=:P02, NomeMes=:P03, NomeCli=:P04');
      DModG.QrUpdPID1.Params[00].AsInteger := 0;
      DModG.QrUpdPID1.Params[01].AsString  := Geral.FDT(QrAdupPgsData.Value, 1);
      DModG.QrUpdPID1.Params[02].AsFloat   := QrAdupPgsJuros.Value;
      DmodG.QrUpdPID1.Params[03].AsString  := MLAGeral.MesEAnoDoPeriodo(Periodo);
      DModG.QrUpdPID1.Params[04].AsString  := '';
      DModG.QrUpdPID1.ExecSQL;
    end;
    //
    ProgressBar1.Position := ProgressBar1.Position + 1;
    //
    QrAdupPgs.Next;
  end;
  BtResultado.Enabled   := True;
  ProgressBar1.Position := 0;
  ReopenQrLReA(True, False);
  Screen.Cursor := crDefault;
end;

procedure TFmOperaAll.CalculaModeloB;
  procedure MensagemDeLimiteDeColunasExcedido();
  begin
    Geral.MensagemBox('Limite de colunas excedido para este modelo de relat�rio!'
      + #13#10 + 'O n� m�ximo de colunas destinadas a taxas e ocorr�ncias � de ' +
      FormatFloat('0', MaxCols) + '.' + #13#10 + 'Utilize outro modelo!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    //
    FMostraCols           := True;
    ProgressBar1.Position := 0;
    ProgressBar1.Visible  := False;
    //
    ReopenQrLReA(True, False);
    //
    Screen.Cursor := crDefault;
  end;
  function ObtemValorCampo(Coluna: Double; Data: String; Codigo: Integer): Double;
  begin
    QrLocCol.Close;
    QrLocCol.SQL.Clear;
    QrLocCol.SQL.Add('SELECT ColVal' + FormatFloat('00', Coluna));
    QrLocCol.SQL.Add('FROM ' + FLReA);
    QrLocCol.SQL.Add('WHERE Codigo=:P0');
    QrLocCol.SQL.Add('AND Data=:P1');
    QrLocCol.Params[00].AsInteger := Codigo;
    QrLocCol.Params[01].AsString  := Data;
    QrLocCol.Open;
    if QrLocCol.RecordCount > 0 then
      Result := QrLocCol.FieldByName('ColVal' + FormatFloat('00', Coluna)).Value
    else
      Result := 0;
  end;
  procedure PreencheNomeCol();
  var
    ColCod, ColTip, i: Integer;
    ColNome: String;
  begin
    //Preenche o valor nas colunas sem border�s
    for i := 1 to MaxCols do
    begin
      QrColSBor.Close;
      QrColSBor.Database := DModG.MyPID_DB;
      QrColSBor.SQL.Clear;
      QrColSBor.SQL.Add('SELECT ColCod'+ FormatFloat('00', i));
      QrColSBor.SQL.Add(', ColNom'+ FormatFloat('00', i));
      QrColSBor.SQL.Add(', ColTip'+ FormatFloat('00', i));
      QrColSBor.SQL.Add('FROM ' + FLReA);
      QrColSBor.SQL.Add('WHERE ColCod'+ FormatFloat('00', i) +' <> 0');
      QrColSBor.SQL.Add('GROUP BY ColCod'+ FormatFloat('00', i));
      QrColSBor.Open;
      //
      ColCod  := QrColSBor.FieldByName('ColCod'+ FormatFloat('00', i)).AsInteger;
      ColNome := QrColSBor.FieldByName('ColNom'+ FormatFloat('00', i)).AsString;
      ColTip  := QrColSBor.FieldByName('ColTip'+ FormatFloat('00', i)).AsInteger;
      //
      DmodG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add('UPDATE ' + FLReA + ' SET ');
      DModG.QrUpdPID1.SQL.Add('ColCod'+ FormatFloat('00', i) +'=:P0,');
      DModG.QrUpdPID1.SQL.Add('ColNom'+ FormatFloat('00', i) +'=:P1,');
      DModG.QrUpdPID1.SQL.Add('ColTip'+ FormatFloat('00', i) +'=:P2');
      DModG.QrUpdPID1.Params[0].AsInteger := ColCod;
      DModG.QrUpdPID1.Params[1].AsString  := ColNome;
      DModG.QrUpdPID1.Params[2].AsInteger := ColTip;
      DModG.QrUpdPID1.ExecSQL;
    end;
  end;
var
  Periodo, i, j, ColCod: Integer;
  TTC_T, TRC_T, TTC_J, TRC_J, MediaDoc, JurOcorr, JurCHDev, JurDUDev, ColVal,
  ColAtual, ColValAnt: Double;
  Data: TDate;
  ColNome: string;
begin
  Screen.Cursor        := crHourGlass;
  ProgressBar1.Visible := True;
  FMostraCols          := True;
  BtResultado.Enabled  := False;
  //
  FLReA := UCriar.RecriaTempTableNovo(ntrttLReA, DmodG.QrUpdPID1, True, 1, '');
  //
  //Border�s no per�odo
  QrOper1.Close;
  QrOper1.SQL.Clear;
  QrOper1.SQL.Add('SELECT lo.*, CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrOper1.SQL.Add('ELSE en.Nome END NOMECLIENTE');
  QrOper1.SQL.Add('FROM lotes lo');
  QrOper1.SQL.Add('LEFT JOIN entidades en ON en.Codigo=lo.Cliente');
  QrOper1.SQL.Add('WHERE lo.data BETWEEN :P0 AND :P1');
  QrOper1.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrOper1.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  QrOper1.Open;
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FLReA + ' SET Codigo=:P0, Data=:P1, ');
  DmodG.QrUpdPID1.SQL.Add('NomeCli=:P2, Lote=:P3, NF=:P4, Total=:P5, ');
  DmodG.QrUpdPID1.SQL.Add('TC0=:P6, VV0=:P7, TTC=:P8, TVV=:P9, Dias=:P10, ');
  DmodG.QrUpdPID1.SQL.Add('Mes=:P11, NomeMes=:P12, QtdeDocs=:P13, MediaDoc=:P14, ');
  DmodG.QrUpdPID1.SQL.Add('ISS_Val=:P15, PIS_Val=:P16, COFINS_Val=:P17, ');
  DmodG.QrUpdPID1.SQL.Add('Impostos=:P18, IOF_TOTAL=:P19, CHDevPg=:P20, DUDevPg=:P21 ');
  //
  ProgressBar1.Position := 0;
  ProgressBar1.Max      := QrOper1.RecordCount;
  while not QrOper1.Eof do
  begin
    if QrOper1Itens.Value = 0 then
      MediaDoc := 0
    else
      MediaDoc := QrOper1Total.Value / QrOper1Itens.Value;
    //
    Periodo               := Geral.Periodo2000(QrOper1Data.Value);
    ProgressBar1.Position := ProgressBar1.Position + 1;
    ProgressBar1.Update;
    Application.ProcessMessages;
    //
    //Juros dos cheques devolvidos
    QrLoteCHDevP.Close;
    QrLoteCHDevP.Params[0].AsInteger := QrOper1Codigo.Value;
    QrLoteCHDevP.Open;
    //
    JurCHDev := QrLoteCHDevPJuros.Value;
    //
    //Juros das duplicatas devolvidas
    QrLoteDUDevP.Close;
    QrLoteDUDevP.Params[0].AsInteger := QrOper1Codigo.Value;
    QrLoteDUDevP.Open;
    //
    JurDUDev := QrLoteDUDevPJuros.Value;
    //
    DmodG.QrUpdPID1.Params[00].AsInteger := QrOper1Codigo.Value;
    DmodG.QrUpdPID1.Params[01].AsString  := Geral.FDT(QrOper1Data.Value, 1);
    DmodG.QrUpdPID1.Params[02].AsString  := QrOper1NOMECLIENTE.Value;
    DmodG.QrUpdPID1.Params[03].AsInteger := QrOper1Lote.Value;
    DmodG.QrUpdPID1.Params[04].AsInteger := QrOper1NF.Value;
    DmodG.QrUpdPID1.Params[05].AsFloat   := QrOper1Total.Value;
    DmodG.QrUpdPID1.Params[06].AsFloat   := QrOper1TxCompra.Value;
    DmodG.QrUpdPID1.Params[07].AsFloat   := QrOper1ValValorem.Value;
    DmodG.QrUpdPID1.Params[08].AsFloat   := QrOper1TxCompra.Value;
    DmodG.QrUpdPID1.Params[09].AsFloat   := QrOper1ValValorem.Value;
    DmodG.QrUpdPID1.Params[10].AsFloat   := QrOper1Dias.Value;
    DmodG.QrUpdPID1.Params[11].AsInteger := Periodo;
    DmodG.QrUpdPID1.Params[12].AsString  := MLAGeral.MesEAnoDoPeriodo(Periodo);
    DmodG.QrUpdPID1.Params[13].AsFloat   := QrOper1Itens.Value;
    DmodG.QrUpdPID1.Params[14].AsFloat   := MediaDoc;
    DmodG.QrUpdPID1.Params[15].AsFloat   := QrOper1ISS_Val.Value;
    DmodG.QrUpdPID1.Params[16].AsFloat   := QrOper1PIS_T_Val.Value;
    DmodG.QrUpdPID1.Params[17].AsFloat   := QrOper1COFINS_T_Val.Value;
    DmodG.QrUpdPID1.Params[18].AsFloat   := QrOper1ISS_Val.Value +
                                            QrOper1PIS_T_Val.Value +
                                            QrOper1COFINS_T_Val.Value;
    DModG.QrUpdPID1.Params[19].AsFloat   := QrOper1IOC_VAL.Value +
                                            QrOper1IOFd_VAL.Value +
                                            QrOper1IOFv_VAL.Value;
    DModG.QrUpdPID1.Params[20].AsFloat   := JurCHDev;
    DModG.QrUpdPID1.Params[21].AsFloat   := JurDUDev;
    DmodG.QrUpdPID1.ExecSQL;
    //
    QrOper1.Next;
  end;
  //
  //Taxas cobradas nos border�s
  QrOper1.Close;
  QrOper1.Open;
  QrOper1.First;
  //
  ProgressBar1.Position := 0;
  ProgressBar1.Max      := QrOper1.RecordCount;
  //
  while not QrOper1.Eof do
  begin
    QrLoc.Close;
    QrLoc.Database := Dmod.MyDB;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT lt.TaxaCod, SUM(lt.TaxaVal) TaxaVal, ');
    QrLoc.SQL.Add('tx.Nome NOMETAXA ');
    QrLoc.SQL.Add('FROM lotestxs lt');
    QrLoc.SQL.Add('LEFT JOIN taxas tx ON tx.Codigo=lt.TaxaCod');
    QrLoc.SQL.Add('WHERE lt.Codigo=:P0');
    QrLoc.SQL.Add('GROUP BY lt.TaxaCod');
    QrLoc.Params[0].AsInteger := QrOper1Codigo.Value;
    QrLoc.Open;
    //
    if QrLoc.RecordCount > 0 then
    begin
      while not QrLoc.Eof do
      begin
        ColCod  := QrLoc.FieldByName('TaxaCod').AsInteger;
        ColNome := QrLoc.FieldByName('NOMETAXA').AsString;
        ColVal  := QrLoc.FieldByName('TaxaVal').AsFloat;
        //
        ColAtual := LocalizaColuna(0, ColCod);
        //
        if ColAtual = 0 then
          ColAtual := LocalizaProximaColunaVazia();
        //
        if ColAtual > MaxCols then
        begin
          MensagemDeLimiteDeColunasExcedido();
          exit;
        end;                                   
        //
        ColValAnt := ObtemValorCampo(ColAtual, Geral.FDT(QrOper1Data.Value, 1),
          QrOper1Codigo.Value);
        //
        DModG.QrUpdPID2.SQL.Clear;
        DModG.QrUpdPID2.SQL.Add('UPDATE ' + FLReA + ' SET ');
        DModG.QrUpdPID2.SQL.Add('ColCod'+ FormatFloat('00', ColAtual) +'=:P0, ');
        DModG.QrUpdPID2.SQL.Add('ColNom'+ FormatFloat('00', ColAtual) +'=:P1, ');
        DModG.QrUpdPID2.SQL.Add('ColTip'+ FormatFloat('00', ColAtual) +'=:P2  ');
        DModG.QrUpdPID2.Params[0].AsInteger := ColCod;
        DModG.QrUpdPID2.Params[1].AsString  := ColNome;
        DModG.QrUpdPID2.Params[2].AsInteger := 0; //Taxas
        DModG.QrUpdPID2.ExecSQL;
        //
        DModG.QrUpdPID2.SQL.Clear;
        DModG.QrUpdPID2.SQL.Add('UPDATE ' + FLReA + ' SET ');
        DModG.QrUpdPID2.SQL.Add('ColVal'+ FormatFloat('00', ColAtual) +'=:P0 ');
        DModG.QrUpdPID2.SQL.Add('WHERE Codigo=:P1');
        DModG.QrUpdPID2.Params[0].AsFloat   := ColVal + ColValAnt;
        DModG.QrUpdPID2.Params[1].AsInteger := QrOper1Codigo.Value;
        DModG.QrUpdPID2.ExecSQL;
        //
        QrLoc.Next;
      end;
    end;
    ProgressBar1.Position := ProgressBar1.Position + 1;
    ProgressBar1.Update;
    Application.ProcessMessages;
    //
    QrOper1.Next;
  end;
  //
  //Ocorr�ncias cobradas nos border�s
  QrOper1.Close;
  QrOper1.Open;
  QrOper1.First;
  //
  ProgressBar1.Position := 0;
  ProgressBar1.Max      := QrOper1.RecordCount;
  //
  while not QrOper1.Eof do
  begin
    QrLoc.Close;
    QrLoc.Database := DMod.MyDB;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT op.Juros, ob.Nome NOMEOCORRENCIA, oc.Ocorrencia');
    QrLoc.SQL.Add('FROM ocorrpg op');
    QrLoc.SQL.Add('LEFT JOIN ocorreu  oc ON oc.Codigo=op.Ocorreu');
    QrLoc.SQL.Add('LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia');
    QrLoc.SQL.Add('LEFT JOIN lotes lo ON lo.Codigo = op.LotePg');
    QrLoc.SQL.Add('WHERE lo.Codigo=:P0');
    QrLoc.Params[0].AsInteger := QrOper1Codigo.Value;
    QrLoc.Open;
    //
    if QrLoc.RecordCount > 0 then
    begin
      while not QrLoc.Eof do
      begin
        ColCod  := QrLoc.FieldByName('Ocorrencia').AsInteger;
        ColNome := QrLoc.FieldByName('NOMEOCORRENCIA').AsString;
        ColVal  := QrLoc.FieldByName('Juros').AsFloat;
        //
        ColAtual := LocalizaColuna(1, ColCod);
        //
        if ColAtual = 0 then
          ColAtual := LocalizaProximaColunaVazia();
        //
        if ColAtual > MaxCols then
        begin
          MensagemDeLimiteDeColunasExcedido();
          exit;
        end;
        //
        ColValAnt := ObtemValorCampo(ColAtual, Geral.FDT(QrOper1Data.Value, 1),
          QrOper1Codigo.Value);
        //
        DModG.QrUpdPID2.SQL.Clear;
        DModG.QrUpdPID2.SQL.Add('UPDATE ' + FLReA + ' SET ');
        DModG.QrUpdPID2.SQL.Add('ColCod'+ FormatFloat('00', ColAtual) +'=:P0, ');
        DModG.QrUpdPID2.SQL.Add('ColNom'+ FormatFloat('00', ColAtual) +'=:P1, ');
        DModG.QrUpdPID2.SQL.Add('ColTip'+ FormatFloat('00', ColAtual) +'=:P2  ');
        DModG.QrUpdPID2.Params[0].AsInteger := ColCod;
        DModG.QrUpdPID2.Params[1].AsString  := ColNome;
        DModG.QrUpdPID2.Params[2].AsInteger := 1; //Ocorr�ncias
        DModG.QrUpdPID2.ExecSQL;
        //
        DModG.QrUpdPID2.SQL.Clear;
        DModG.QrUpdPID2.SQL.Add('UPDATE ' + FLReA + ' SET ');
        DModG.QrUpdPID2.SQL.Add('ColVal'+ FormatFloat('00', ColAtual) +'=:P0 ');
        DModG.QrUpdPID2.SQL.Add('WHERE Codigo=:P1');
        DModG.QrUpdPID2.Params[0].AsFloat   := ColVal + ColValAnt;
        DModG.QrUpdPID2.Params[1].AsInteger := QrOper1Codigo.Value;
        DModG.QrUpdPID2.ExecSQL;
        //
        QrLoc.Next;
      end;
    end;
    ProgressBar1.Position := ProgressBar1.Position + 1;
    ProgressBar1.Update;
    Application.ProcessMessages;
    //
    QrOper1.Next;
  end;
  //
  ProgressBar1.Position := 0;
  QrEmLo1.Close;
  QrEmLo1.SQL.Clear;
  QrEmLo1.SQL.Add('SELECT el.Codigo, el.ValValorem, el.TaxaVal');
  QrEmLo1.SQL.Add('FROM emlot el');
  QrEmLo1.SQL.Add('LEFT JOIN creditor.lotes lo');
  QrEmLo1.SQL.Add('ON lo.Codigo=el.Codigo');
  QrEmLo1.SQL.Add('WHERE lo.data BETWEEN :P0 AND :P1');
  //
  QrEmLo1.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrEmLo1.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  //
  j := FmPrincipal.FMyDBs.MaxDBs -1;
  for i := 0 to j do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      QrEmLo1.Close;
      QrEmLo1.Database := Dmod.FArrayMySQLBD[i];
      QrEmLo1.Open;
      //
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FLReA + ' SET TC'+IntToStr(i+1)+'=:P0, ');
      DmodG.QrUpdPID1.SQL.Add('VV'+IntToStr(i+1)+'=:P1, ETC=ETC+ :P2, ');
      DmodG.QrUpdPID1.SQL.Add('EVV=EVV+ :P3, TTC=TTC+ :P4, TVV=TVV+ :P5');
      DmodG.QrUpdPID1.SQL.Add('WHERE Codigo=:Pa');
      //
      ProgressBar1.Max := QrEmLo1.RecordCount;
      //
      while not QrEmLo1.Eof do
      begin
        ProgressBar1.Position := ProgressBar1.Position + 1;
        //
        DmodG.QrUpdPID1.Params[00].AsFloat   := QrEmLo1TaxaVal.Value;
        DmodG.QrUpdPID1.Params[01].AsFloat   := QrEmLo1ValValorem.Value;
        DmodG.QrUpdPID1.Params[02].AsFloat   := QrEmLo1TaxaVal.Value;
        DmodG.QrUpdPID1.Params[03].AsFloat   := QrEmLo1ValValorem.Value;
        DmodG.QrUpdPID1.Params[04].AsFloat   := QrEmLo1TaxaVal.Value;
        DmodG.QrUpdPID1.Params[05].AsFloat   := QrEmLo1ValValorem.Value;
        //
        DmodG.QrUpdPID1.Params[06].AsInteger := QrEmLo1Codigo.Value;
        DmodG.QrUpdPID1.ExecSQL;
        //
        QrEmLo1.Next;
      end;
      ProgressBar1.Position := 0;
    end;
  end;
  ReopenQrLReA(False, True);
  //
  ProgressBar1.Max := QrLReA.RecordCount;
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FLReA + ' SET TTC_T=:P0, TRC_T=:P1, TTC_J=:P2, ');
  DmodG.QrUpdPID1.SQL.Add('TRC_J=:P3, ddVal=:P4, ddLiq=:P5 WHERE Codigo=:Pa');
  //
  QrLReA.DisableControls;
  try
    QrLReA.First;
    while not QrLReA.Eof do
    begin
      ProgressBar1.Position := ProgressBar1.Position +1;
      if QrLReADias.Value = 0 then
      begin
        TTC_J := 0;
        TRC_J := 0;
      end else begin
        TTC_J := QrLReATTC.Value / QrLReATotal.Value * 100;
        TRC_J := (QrLReATTC.Value - QrLReAImpostos.Value) / QrLReATotal.Value * 100;
      end;
       //
      TTC_T := MLAGeral.DescobreJuroComposto(TTC_J, QrLReADias.Value, 2);
      TRC_T := MLAGeral.DescobreJuroComposto(TRC_J, QrLReADias.Value, 2);
      //
      DmodG.QrUpdPID1.Params[00].AsFloat   := TTC_T;
      DmodG.QrUpdPID1.Params[01].AsFloat   := TRC_T;
      DmodG.QrUpdPID1.Params[02].AsFloat   := TTC_J;
      DmodG.QrUpdPID1.Params[03].AsFloat   := TRC_J;
      DmodG.QrUpdPID1.Params[04].AsFloat   := QrLReATotal.Value * QrLReADias.Value;
      DmodG.QrUpdPID1.Params[05].AsFloat   := (QrLReATotal.Value - QrLReAImpostos.Value) * QrLReADias.Value;
      //
      DmodG.QrUpdPID1.Params[06].AsInteger := QrLReACodigo.Value;
      DmodG.QrUpdPID1.ExecSQL;
      //
      QrLReA.Next;
    end;
  finally
    QrLReA.EnableControls;
  end;
  //Cheques devolvidos sem border�
  QrAlinPGs.Close;
  QrAlinPGs.SQL.Clear;
  QrAlinPGs.SQL.Add('SELECT pg.Data, (SUM(pg.Juros) - SUM(pg.Desco)) Juros');
  QrAlinPGs.SQL.Add('FROM alinpgs pg');
  QrAlinPGs.SQL.Add('WHERE pg.LotePG = 0');
  QrAlinPGs.SQL.Add('AND pg.Data BETWEEN :P0 AND :P1');
  QrAlinPGs.SQL.Add('GROUP BY pg.Data');
  QrAlinPGs.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrAlinPGs.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  QrAlinPGs.Open;
  //
  ProgressBar1.Position := 0;
  ProgressBar1.Max      := QrAlinPGs.RecordCount;
  //
  ReopenQrLReA(False, True);
  //
  QrAlinPGs.First;
  while not QrAlinPGs.Eof do
  begin
    Periodo := Geral.Periodo2000(QrAlinPGsData.Value);
    Data    := LocalizaData(0, QrAlinPGsData.Value);
    //
    if Data > 0 then
    begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FLReA + ' SET CHDevPg=:P00, ');
      DmodG.QrUpdPID1.SQL.Add('NomeMes=:P01, NomeCli=:P02 ');
      DModG.QrUpdPID1.SQL.Add('WHERE Codigo=:P03 AND Data=:P04');
      DModG.QrUpdPID1.Params[00].AsFloat   := QrAlinPGsJuros.Value;
      DModG.QrUpdPID1.Params[01].AsString  := MLAGeral.MesEAnoDoPeriodo(Periodo);
      DModG.QrUpdPID1.Params[02].AsString  := '';
      DmodG.QrUpdPID1.Params[03].AsInteger := 0;
      DModG.QrUpdPID1.Params[04].AsString  := Geral.FDT(QrAlinPGsData.Value, 1);
      DModG.QrUpdPID1.ExecSQL;
    end else
    begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FLReA + ' SET Codigo=:P00, ');
      DmodG.QrUpdPID1.SQL.Add('Data=:P01, CHDevPg=:P02, NomeMes=:P03, NomeCli=:P04');
      DModG.QrUpdPID1.Params[00].AsInteger := 0;
      DModG.QrUpdPID1.Params[01].AsString  := Geral.FDT(QrAlinPGsData.Value, 1);
      DModG.QrUpdPID1.Params[02].AsFloat   := QrAlinPGsJuros.Value;
      DmodG.QrUpdPID1.Params[03].AsString  := MLAGeral.MesEAnoDoPeriodo(Periodo);
      DModG.QrUpdPID1.Params[04].AsString  := '';
      DModG.QrUpdPID1.ExecSQL;
    end;
    //
    ProgressBar1.Position := ProgressBar1.Position + 1;
    //
    QrAlinPGs.Next;
  end;
  //
  //Duplicatas devolvidas sem border�
  QrAdupPgs.Close;
  QrAdupPgs.SQL.Clear;
  QrAdupPgs.SQL.Add('SELECT pg.Data, (SUM(pg.Juros) - SUM(pg.Desco)) Juros');
  QrAdupPgs.SQL.Add('FROM aduppgs pg');
  QrAdupPgs.SQL.Add('WHERE pg.LotePG = 0');
  QrAdupPgs.SQL.Add('AND pg.Data BETWEEN :P0 AND :P1');
  QrAdupPgs.SQL.Add('GROUP BY pg.Data');
  QrAdupPgs.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrAdupPgs.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  QrAdupPgs.Open;
  //
  ProgressBar1.Position := 0;
  ProgressBar1.Max      := QrAdupPgs.RecordCount;
  //
  ReopenQrLReA(False, True);
  //
  QrAdupPgs.First;
  while not QrAdupPgs.Eof do
  begin
    Periodo := Geral.Periodo2000(QrAdupPgsData.Value);
    Data    := LocalizaData(0, QrAdupPgsData.Value);
    //
    if Data > 0 then
    begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('UPDATE ' + FLReA + ' SET DUDevPg=:P00, ');
      DmodG.QrUpdPID1.SQL.Add('NomeMes=:P01, NomeCli=:P02 ');
      DModG.QrUpdPID1.SQL.Add('WHERE Codigo=:P03 AND Data=:P04');
      DModG.QrUpdPID1.Params[00].AsFloat   := QrAdupPgsJuros.Value;
      DModG.QrUpdPID1.Params[01].AsString  := MLAGeral.MesEAnoDoPeriodo(Periodo);
      DModG.QrUpdPID1.Params[02].AsString  := '';
      DmodG.QrUpdPID1.Params[03].AsInteger := 0;
      DModG.QrUpdPID1.Params[04].AsString  := Geral.FDT(QrAdupPgsData.Value, 1);
      DModG.QrUpdPID1.ExecSQL;
    end else
    begin
      DmodG.QrUpdPID1.SQL.Clear;
      DmodG.QrUpdPID1.SQL.Add('INSERT INTO ' + FLReA + ' SET Codigo=:P00, ');
      DmodG.QrUpdPID1.SQL.Add('Data=:P01, DUDevPg=:P02, NomeMes=:P03, NomeCli=:P04');
      DModG.QrUpdPID1.Params[00].AsInteger := 0;
      DModG.QrUpdPID1.Params[01].AsString  := Geral.FDT(QrAdupPgsData.Value, 1);
      DModG.QrUpdPID1.Params[02].AsFloat   := QrAdupPgsJuros.Value;
      DmodG.QrUpdPID1.Params[03].AsString  := MLAGeral.MesEAnoDoPeriodo(Periodo);
      DModG.QrUpdPID1.Params[04].AsString  := '';
      DModG.QrUpdPID1.ExecSQL;
    end;
    //
    ProgressBar1.Position := ProgressBar1.Position + 1;
    //
    QrAdupPgs.Next;
  end;
  //
  PreencheNomeCol;
  //
  //Adiciona valores referentes as ocorr�ncias sem border�s
  QrLoc.Close;
  QrLoc.Database := Dmod.MyDB;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT SUM(pg.Juros) Juros, ob.Nome NOMEOCORRENCIA, ');
  QrLoc.SQL.Add('ob.Codigo, pg.Data');
  QrLoc.SQL.Add('FROM ocorrpg pg');
  QrLoc.SQL.Add('LEFT JOIN ocorreu  oc ON oc.Codigo=pg.Ocorreu');
  QrLoc.SQL.Add('LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia');
  QrLoc.SQL.Add('WHERE pg.LotePG = 0');
  QrLoc.SQL.Add('AND oc.Ocorrencia <> 0');
  QrLoc.SQL.Add('AND pg.Data BETWEEN :P0 AND :P1');
  QrLoc.SQL.Add('GROUP BY pg.Data, oc.Ocorrencia');
  QrLoc.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrLoc.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  QrLoc.Open;
  //
  ProgressBar1.Position := 0;
  ProgressBar1.Max      := QrLoc.RecordCount;
  //
  if QrLoc.RecordCount > 0 then
  begin
    QrLoc.First;
    while not QrLoc.Eof do
    begin
      ColCod  := QrLoc.FieldByName('Codigo').AsInteger;
      ColNome := QrLoc.FieldByName('NOMEOCORRENCIA').AsString;
      ColVal  := QrLoc.FieldByName('Juros').AsFloat;
      Periodo := Geral.Periodo2000(QrLoc.FieldByName('Data').AsDateTime);
      Data    := LocalizaData(0, QrLoc.FieldByName('Data').AsDateTime);
      //
      if Data > 0 then
      begin
        PreencheNomeCol;
        //
        ColAtual := LocalizaColuna(1, ColCod);
        //
        if ColAtual = 0 then
          ColAtual := LocalizaProximaColunaVazia();
        //
        if ColAtual > MaxCols then
        begin
          MensagemDeLimiteDeColunasExcedido();
          exit;
        end;
        ColValAnt := ObtemValorCampo(ColAtual, Geral.FDT(QrLoc.FieldByName('Data').Value, 1), 0);
        //
        DModG.QrUpdPID1.SQL.Clear;
        DModG.QrUpdPID1.SQL.Add('UPDATE ' + FLReA + ' SET ');
        DModG.QrUpdPID1.SQL.Add('ColCod'+ FormatFloat('00', ColAtual) +'=:P00, ');
        DModG.QrUpdPID1.SQL.Add('ColNom'+ FormatFloat('00', ColAtual) +'=:P01, ');
        DModG.QrUpdPID1.SQL.Add('ColTip'+ FormatFloat('00', ColAtual) +'=:P02, ');
        DModG.QrUpdPID1.SQL.Add('ColVal'+ FormatFloat('00', ColAtual) +'=:P03, ');
        DModG.QrUpdPID1.SQL.Add('NomeMes=:P04 WHERE Codigo=:P05 AND Data=:P06');
        DModG.QrUpdPID1.Params[00].AsInteger := ColCod;
        DModG.QrUpdPID1.Params[01].AsString  := ColNome;
        DModG.QrUpdPID1.Params[02].AsInteger := 1; //Ocorr�ncias
        DModG.QrUpdPID1.Params[03].AsFloat   := ColVal + ColValAnt;
        DModG.QrUpdPID1.Params[04].AsString  := MLAGeral.MesEAnoDoPeriodo(Periodo);
        DModG.QrUpdPID1.Params[05].AsInteger := 0;
        DModG.QrUpdPID1.Params[06].AsString  := Geral.FDT(QrLoc.FieldByName('Data').Value, 1);
        DModG.QrUpdPID1.ExecSQL;
      end else
      begin
        PreencheNomeCol;
        //
        DModG.QrUpdPID1.SQL.Clear;
        DModG.QrUpdPID1.SQL.Add('INSERT INTO '+ FLReA +' SET Data=:P0');
        DModG.QrUpdPID1.Params[0].AsString := Geral.FDT(QrLoc.FieldByName('Data').Value, 1);
        DModG.QrUpdPID1.ExecSQL; 
        //
        ColAtual := LocalizaColuna(1, ColCod);
        //
        if ColAtual = 0 then
          ColAtual := LocalizaProximaColunaVazia();
        //
        if ColAtual > MaxCols then
        begin
          MensagemDeLimiteDeColunasExcedido();
          exit;
        end;
        //
        ColValAnt := ObtemValorCampo(ColAtual, Geral.FDT(QrLoc.FieldByName('Data').Value, 1), 0);
        //
        DModG.QrUpdPID1.SQL.Clear;
        DModG.QrUpdPID1.SQL.Add('UPDATE ' + FLReA + ' SET ');
        DModG.QrUpdPID1.SQL.Add('ColCod'+ FormatFloat('00', ColAtual) +'=:P00, ');
        DModG.QrUpdPID1.SQL.Add('ColNom'+ FormatFloat('00', ColAtual) +'=:P01, ');
        DModG.QrUpdPID1.SQL.Add('ColTip'+ FormatFloat('00', ColAtual) +'=:P02, ');
        DModG.QrUpdPID1.SQL.Add('ColVal'+ FormatFloat('00', ColAtual) +'=:P03, ');
        DModG.QrUpdPID1.SQL.Add('NomeMes=:P04 WHERE Codigo=:P05 AND Data=:P06');
        DModG.QrUpdPID1.Params[00].AsInteger := ColCod;
        DModG.QrUpdPID1.Params[01].AsString  := ColNome;
        DModG.QrUpdPID1.Params[02].AsInteger := 1; //Ocorr�ncias
        DModG.QrUpdPID1.Params[03].AsFloat   := ColVal + ColValAnt;
        DModG.QrUpdPID1.Params[04].AsString  := MLAGeral.MesEAnoDoPeriodo(Periodo);
        DModG.QrUpdPID1.Params[05].AsInteger := 0;
        DModG.QrUpdPID1.Params[06].AsString  := Geral.FDT(QrLoc.FieldByName('Data').Value, 1);
        DModG.QrUpdPID1.ExecSQL;
      end;
      ProgressBar1.Position := ProgressBar1.Position + 1;
      ProgressBar1.Update;
      Application.ProcessMessages;
      //
      QrLoc.Next;
    end;
  end;
  //
  ProgressBar1.Position := 0;
  ProgressBar1.Max      := QrLoc.RecordCount;
  //
  ConfiguraGrade(True);
  //
  BtResultado.Enabled   := True;
  ProgressBar1.Position := 0;
  ProgressBar1.Visible  := False;
  //
  ReopenQrLReA(True, False);
  Screen.Cursor := crDefault;
end;

procedure TFmOperaAll.ConfiguraGrade(ColunasVisi: Boolean);
const
 TotFixCol = 17;  
var
  i: Integer;
  NomeCol: String;
begin
  DBGrid1.Columns[18].Visible := ColunasVisi;
  DBGrid1.Columns[19].Visible := ColunasVisi;
  DBGrid1.Columns[20].Visible := ColunasVisi;
  DBGrid1.Columns[21].Visible := ColunasVisi;
  DBGrid1.Columns[22].Visible := ColunasVisi;
  DBGrid1.Columns[23].Visible := ColunasVisi;
  DBGrid1.Columns[24].Visible := ColunasVisi;
  DBGrid1.Columns[25].Visible := ColunasVisi;
  DBGrid1.Columns[26].Visible := ColunasVisi;
  DBGrid1.Columns[27].Visible := ColunasVisi;
  DBGrid1.Columns[28].Visible := ColunasVisi;
  DBGrid1.Columns[29].Visible := ColunasVisi;
  //
  if ColunasVisi then
  begin
    for i := 1 to MaxCols do
    begin
      QrLoc.Close;
      QrLoc.Database := DModG.MyPID_DB;
      QrLoc.SQL.Clear;
      QrLoc.SQL.Add('SELECT ColNom'+ FormatFloat('00', i));
      QrLoc.SQL.Add('FROM '+ FLReA);
      QrLoc.SQL.Add('GROUP BY ColNom'+ FormatFloat('00', i));
      QrLoc.Open;
      //
      NomeCol := QrLoc.FieldByName('ColNom'+ FormatFloat('00', i)).AsString;
      //
      if NomeCol <> '' then
      begin
        DBGrid1.Columns[i + TotFixCol].Visible := True;
        DBGrid1.Columns[i + TotFixCol].Title.Caption := NomeCol;
      end else
        DBGrid1.Columns[i + TotFixCol].Visible := False; 
    end;
  end;
end;

procedure TFmOperaAll.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmOperaAll.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmOperaAll.frxOper1GetValue(const VarName: string;
  var Value: Variant);
const
  ITEM: array[0..1] of string = ('Dia', 'M�s');
  ITEMs: array[0..1] of string = ('Dias com transa��o', 'Meses');
var
  MeuItem: String;
begin
  if VarName = 'VARF_TITULO1' then Value := ITEM[RGOrdem1.ItemIndex]
  else if VarName = 'VARF_CONTA_GF1' then
  begin
    if FContaGF1 < 2 then MeuItem := ITEM[RGOrdem1.ItemIndex]
    else MeuItem := ITEMs[RGOrdem1.ItemIndex];
    Value := 'Total de '+Geral.FFT(FContaGF1, 0, siPositivo)+' '+MeuItem;
  end
  else if VarName = 'VARF_PERIODO' then Value :=
    FormatDateTime(VAR_FORMATDATE3, TPIni.Date) + CO_ATE +
    FormatDateTime(VAR_FORMATDATE3, TPFim.Date)
  else if VarName = 'VFR_LA1NOME' then
  begin
    if RGSintetico.ItemIndex = 0 then
    begin
      case RGOrdem1.ItemIndex of
        0: Value := 'Emitido no dia  '+Geral.FDT(QrLReAData.Value, 2);
        1: Value := 'Emitido no m�s de '+QrLReANomeMes.Value;
      end;
    end else begin
      case RGOrdem1.ItemIndex of
        0: Value := Geral.FDT(QrLReAData.Value, 2);
        1: Value := QrLReANomeMes.Value;
      end;
    end;
  end
  else if VarName = 'VFR_LA2NOME' then
  begin
    if RGSintetico.ItemIndex = 0 then
    begin
      case RGOrdem2.ItemIndex of
        0: Value := 'Emitido no dia  '+Geral.FDT(QrLReAData.Value, 2);
        1: Value := 'Emitido no m�s de '+QrLReANomeMes.Value;
      end;
    end else begin
      case RGOrdem2.ItemIndex of
        0: Value := Geral.FDT(QrLReAData.Value, 2);
        1: Value := QrLReANomeMes.Value;
      end;
    end;
  end
  else if VarName = 'VARF_CONTA_GF1' then
  begin
    {if Geral.IMV(frParser.Calc(p1)) = 0 then FContaGF1 := 0
    else FContaGF1 := FContaGF1 + 1;}
    Value := ' ';
  end;
  if VarName = 'VFR_ORD1' then
    Value := 0
  else if VarName = 'VFR_ORD2' then
    Value := 0
  else if VarName = 'VARF_VISIBLE' then
    Value := not Geral.IntToBool_0(RGSintetico.ItemIndex);
end;

function TFmOperaAll.frxOper1UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  if MethodName = Uppercase('CalculaJuroMes_frxReport') then
    Result := CalculaJuroMes_frxReport(Params[0], Params[1], Params[2])
end;

procedure TFmOperaAll.frxOper3GetValue(const VarName: string;
  var Value: Variant);
const
  ITEM: array[0..1] of string = ('Dia', 'M�s');
  ITEMs: array[0..1] of string = ('Dias com transa��o', 'Meses');
var
  MeuItem: String;
begin
  if VarName = 'VARF_TITULO1' then Value := ITEM[RGOrdem1.ItemIndex]
  else if VarName = 'VARF_CONTA_GF1' then
  begin
    if FContaGF1 < 2 then MeuItem := ITEM[RGOrdem1.ItemIndex]
    else MeuItem := ITEMs[RGOrdem1.ItemIndex];
    Value := 'Total de '+Geral.FFT(FContaGF1, 0, siPositivo)+' '+MeuItem;
  end
  else if VarName = 'VARF_PERIODO' then Value :=
    FormatDateTime(VAR_FORMATDATE3, TPIni.Date) + CO_ATE +
    FormatDateTime(VAR_FORMATDATE3, TPFim.Date)
  else if VarName = 'VFR_LA1NOME' then
  begin
    if RGSintetico.ItemIndex = 0 then
    begin
      case RGOrdem1.ItemIndex of
        0: Value := 'Emitido no dia  '+Geral.FDT(QrLReAData.Value, 2);
        1: Value := 'Emitido no m�s de '+QrLReANomeMes.Value;
      end;
    end else begin
      case RGOrdem1.ItemIndex of
        0: Value := Geral.FDT(QrLReAData.Value, 2);
        1: Value := QrLReANomeMes.Value;
      end;
    end;
  end
  else if VarName = 'VFR_LA2NOME' then
  begin
    if RGSintetico.ItemIndex = 0 then
    begin
      case RGOrdem2.ItemIndex of
        0: Value := 'Emitido no dia  '+Geral.FDT(QrLReAData.Value, 2);
        1: Value := 'Emitido no m�s de '+QrLReANomeMes.Value;
      end;
    end else begin
      case RGOrdem2.ItemIndex of
        0: Value := Geral.FDT(QrLReAData.Value, 2);
        1: Value := QrLReANomeMes.Value;
      end;
    end;
  end
  else if VarName = 'VARF_CONTA_GF1' then
  begin
    {if Geral.IMV(frParser.Calc(p1)) = 0 then FContaGF1 := 0
    else FContaGF1 := FContaGF1 + 1;}
    Value := ' ';
  end;
  if VarName = 'VFR_ORD1' then
    Value := 0
  else if VarName = 'VFR_ORD2' then
    Value := 0
  else if VarName = 'VARF_VISIBLE' then
    Value := not Geral.IntToBool_0(RGSintetico.ItemIndex)
  //Colunas vari�veis
  else if VarName = 'VAR_COLUNA01' then
    Value := QrLReAColNom01.Value <> ''
  else if VarName = 'VAR_COLUNA02' then
    Value := QrLReAColNom02.Value <> ''
  else if VarName = 'VAR_COLUNA03' then
    Value := QrLReAColNom03.Value <> ''
  else if VarName = 'VAR_COLUNA04' then
    Value := QrLReAColNom04.Value <> ''
  else if VarName = 'VAR_COLUNA05' then
    Value := QrLReAColNom05.Value <> ''
  else if VarName = 'VAR_COLUNA06' then
    Value := QrLReAColNom06.Value <> ''
  else if VarName = 'VAR_COLUNA07' then
    Value := QrLReAColNom07.Value <> ''
  else if VarName = 'VAR_COLUNA08' then
    Value := QrLReAColNom08.Value <> ''
  else if VarName = 'VAR_COLUNA09' then
    Value := QrLReAColNom09.Value <> ''
  else if VarName = 'VAR_COLUNA10' then
    Value := QrLReAColNom10.Value <> ''
  else if VarName = 'VAR_COLUNA11' then
    Value := QrLReAColNom11.Value <> ''
  else if VarName = 'VAR_COLUNA12' then
    Value := QrLReAColNom12.Value <> '';
end;

function TFmOperaAll.frxOper3UserFunction(const MethodName: string;
  var Params: Variant): Variant;
begin
  if MethodName = Uppercase('CalculaJuroMes_frxReport') then
    Result := CalculaJuroMes_frxReport(Params[0], Params[1], Params[2])
end;

function TFmOperaAll.LocalizaColuna(Tipo, CodCol: Integer): Integer;
var
  i: Integer;
begin
  Result := 0;
  if Tipo in [0, 1] then
  begin
    //Tipo = 0 - Taxas
    //Tipo = 1 - Ocorr�ncias
    //
    for i := 1 to MaxCols do
    begin
      QrLocCol.Close;
      QrLocCol.SQL.Clear;
      QrLocCol.SQL.Add('SELECT ColCod'+ FormatFloat('00', i));
      QrLocCol.SQL.Add('FROM ' + FLReA);
      QrLocCol.SQL.Add('WHERE ColTip'+ FormatFloat('00', i) +'='+ FormatFloat('0', Tipo));
      QrLocCol.SQL.Add('AND ColCod'+ FormatFloat('00', i) +'='+ FormatFloat('0', CodCol));
      QrLocCol.Open;
      if QrLocCol.FieldByName('ColCod' + FormatFloat('00', i)).AsInteger > 0 then
      begin
        Result := i;
        Exit;
      end;
    end;
  end;
end;

function TFmOperaAll.LocalizaData(Codigo: Integer; Data: TDate): TDate;
begin
   QrLocData.Close;
   QrLocData.Database := DModG.MyPID_DB;
   QrLocData.SQL.Clear;
   QrLocData.SQL.Add('SELECT Data FROM ' + FLReA);
   QrLocData.SQL.Add('WHERE Codigo=:P0');
   QrLocData.SQL.Add('AND Data=:P1');
   QrLocData.Params[0].AsInteger := Codigo;
   QrLocData.Params[1].AsString  := Geral.FDT(Data, 1);
   QrLocData.Open;
   if QrLocData.RecordCount > 0 then
     Result := QrLocData.FieldByName('Data').AsDateTime
   else
     Result := 0;
end;

function TFmOperaAll.LocalizaProximaColunaVazia: Integer;
var
  i: Integer;
begin
  for i := 1 to MaxCols do
  begin
    QrLocCol.Close;
    QrLocCol.SQL.Clear;
    QrLocCol.SQL.Add('SELECT ColCod' + FormatFloat('00', i));
    QrLocCol.SQL.Add('FROM ' + FLReA);
    QrLocCol.Open;
    if QrLocCol.FieldByName('ColCod' + FormatFloat('00', i)).AsInteger = 0 then
    begin
      Result := i;
      Exit;
    end;
  end;
  Result := MaxCols + 1;
end;

procedure TFmOperaAll.QrCHDevPCalcFields(DataSet: TDataSet);
begin
  QrCHDevPNOMESTATUS.Value := MLAGeral.NomeStatusPgto(QrCHDevPStatus.Value);
  QrCHDevPCPF_TXT.Value := Geral.FormataCNPJ_TT(QrCHDevPCPF.Value);
end;

procedure TFmOperaAll.QrCHDevPSLCalcFields(DataSet: TDataSet);
begin
  QrCHDevPSLNOMESTATUS.Value := MLAGeral.NomeStatusPgto(QrCHDevPSLStatus.Value);
  QrCHDevPSLCPF_TXT.Value := Geral.FormataCNPJ_TT(QrCHDevPSLCPF.Value);
end;

procedure TFmOperaAll.QrDPagoCalcFields(DataSet: TDataSet);
begin
  QrDPagoNOMESTATUS.Value := MLAGeral.NomeStatusPgto(QrDPagoQuitado.Value);
  QrDPagoCPF_TXT.Value := Geral.FormataCNPJ_TT(QrDPagoCPF.Value);
end;

procedure TFmOperaAll.QrDPagoSLCalcFields(DataSet: TDataSet);
begin
  QrDPagoSLNOMESTATUS.Value := MLAGeral.NomeStatusPgto(QrDPagoSLQuitado.Value);
  QrDPagoSLCPF_TXT.Value := Geral.FormataCNPJ_TT(QrDPagoSLCPF.Value);
end;

procedure TFmOperaAll.QrLotesTxsCalcFields(DataSet: TDataSet);
begin
  QrLotesTxsTAXA_STR.Value := MLAGeral.FormataTaxa(
    QrLotesTxsForma.Value, Dmod.QrControleMoeda.Value);
  //
  if QrLotesTxsTaxaQtd.Value > 0 then
    QrLotesTxsCALCQtd.Value := QrLotesTxsTaxaQtd.Value else
    if QrLotesTxsSysAQtd.Value > 0 then
      QrLotesTxsCALCQtd.Value := QrLotesTxsSysAQtd.Value else
      if QrLotesTxsTaxaTxa.Value > 0 then
        QrLotesTxsCALCQtd.Value := 1 else QrLotesTxsCALCQtd.Value := 0;
  //
  QrLotesTxsTaxaValNEG.Value := - QrLotesTxsTaxaVal.Value;
end;

procedure TFmOperaAll.QrLReAAfterScroll(DataSet: TDataSet);
begin
  if (RGModelo.ItemIndex  = 0) and (RGSintetico.ItemIndex = 0) then
  begin
    ReopenTxs;
    ReopenOcorP;
    ReopenOcorPSL;
    ReopenCHDevP;
    ReopenCHDevPSL;
    ReopenDPago;
    ReopenDPagoSL;
  end;
end;

procedure TFmOperaAll.QrLReABeforeClose(DataSet: TDataSet);
begin
  QrLotesTxs.Close;
  QrOcorP.Close;
  QrOcorPSL.Close;
  QrCHDevP.Close;
  QrCHDevPSL.Close;
  QrDPago.Close;
  QrDPagoSL.Close;
end;

procedure TFmOperaAll.QrLReACalcFields(DataSet: TDataSet);
begin
  QrLReAITEM.Value := 1;
end;

procedure TFmOperaAll.QrOcorPCalcFields(DataSet: TDataSet);
begin
  if QrOcorPDescri.Value <> '' then
    QrOcorPDOCUM_TXT.Value := QrOcorPDescri.Value
  else if QrOcorPTIPODOC.Value = 'CH' then
    QrOcorPDOCUM_TXT.Value := FormatFloat('000', QrOcorPBanco.Value) + '/' +
    FormatFloat('0000', QrOcorPAgencia.Value) + '/' +
    QrOcorPConta.Value + '-' + FormatFloat('000000', QrOcorPCheque.Value)
  else if QrOcorPTIPODOC.Value = 'DU' then
    QrOcorPDOCUM_TXT.Value := QrOcorPDuplicata.Value
  else QrOcorPDOCUM_TXT.Value := '';
  //
  QrOcorPCPF_TXT.Value := Geral.FormataCNPJ_TT(QrOcorPCPF.Value);
  //
  QrOcorPPagoNeg.Value := - QrOcorPPago.Value;
  QrOcorPSALDONEG.Value := - QrOcorPSALDO.Value;
end;

procedure TFmOperaAll.QrOcorPSLCalcFields(DataSet: TDataSet);
begin
  if QrOcorPSLDescri.Value <> '' then
    QrOcorPSLDOCUM_TXT.Value := QrOcorPSLDescri.Value
  else if QrOcorPSLTIPODOC.Value = 'CH' then
    QrOcorPSLDOCUM_TXT.Value := FormatFloat('000', QrOcorPSLBanco.Value) + '/' +
    FormatFloat('0000', QrOcorPSLAgencia.Value) + '/' +
    QrOcorPSLConta.Value + '-' + FormatFloat('000000', QrOcorPSLCheque.Value)
  else if QrOcorPSLTIPODOC.Value = 'DU' then
    QrOcorPSLDOCUM_TXT.Value := QrOcorPSLDuplicata.Value
  else QrOcorPSLDOCUM_TXT.Value := '';
  //
  QrOcorPSLCPF_TXT.Value := Geral.FormataCNPJ_TT(QrOcorPSLCPF.Value);
  //
  QrOcorPSLPagoNeg.Value := - QrOcorPSLPago.Value;
  QrOcorPSLSALDONEG.Value := - QrOcorPSLSALDO.Value;
end;

procedure TFmOperaAll.ReopenCHDevP;
begin
  QrCHDevP.Close;
  QrCHDevP.Params[0].AsInteger := QrLReACodigo.Value;
  QrCHDevP.Open;
end;

procedure TFmOperaAll.ReopenCHDevPSL;
begin
  QrCHDevPSL.Close;
  QrCHDevPSL.Params[0].AsDate    := QrLReAData.Value;
  QrCHDevPSL.Params[1].AsInteger := QrLReACodigo.Value;
  QrCHDevPSL.Open;
end;

procedure TFmOperaAll.ReopenDPago;
begin
  QrDPago.Close;
  QrDPago.Params[0].AsInteger := QrLReACodigo.Value;
  QrDPago.Open;
end;

procedure TFmOperaAll.ReopenDPagoSL;
begin
  QrDPagoSL.Close;
  QrDPagoSL.Params[0].AsDate    := QrLReAData.Value;
  QrDPagoSL.Params[1].AsInteger := QrLReACodigo.Value;
  QrDPagoSL.Open;
end;

procedure TFmOperaAll.ReopenOcorP;
begin
  QrOcorP.Close;
  QrOcorP.Params[0].AsInteger := QrLReACodigo.Value;
  QrOcorP.Open;
end;

procedure TFmOperaAll.ReopenOcorPSL;
begin
  QrOcorPSL.Close;
  QrOcorPSL.Params[0].AsDate    := QrLReAData.Value;
  QrOcorPSL.Params[1].AsInteger := QrLReACodigo.Value;
  QrOcorPSL.Open;
end;

procedure TFmOperaAll.ReopenQrLReA(VerificaBot, Redefine: Boolean);
begin
  QrLReA.Close;
  QrLReA.SQL.Clear;
  QrLReA.SQL.Add('SELECT * FROM ' + FLReA + ' ORDER BY Data');
  QrLReA.Open;
end;

procedure TFmOperaAll.ReopenTxs;
begin
  QrLotesTxs.Close;
  QrLotesTxs.Params[0].AsInteger := QrLReACodigo.Value;
  QrLotesTxs.Open;
end;

procedure TFmOperaAll.RGModeloClick(Sender: TObject);
var
  Modelo: Integer;
begin
  BtCalcula.Enabled := True;
  Modelo            := RGModelo.ItemIndex;
  //
  case Modelo of
    0:
    begin
      RGOrdem1.Visible    := True;
      RGOrdem2.Visible    := True;
      RGSintetico.Visible := True;
    end;
    1:
    begin
      RGOrdem1.Visible    := False;
      RGOrdem2.Visible    := False;
      RGSintetico.Visible := False;
    end;
  end;
end;

procedure TFmOperaAll.FormCreate(Sender: TObject);
begin
  QrLReA.Database   := DModG.MyPID_DB;
  QrLocCol.Database := DModG.MyPID_DB;
  //
  ConfiguraGrade(False);
  //
  TPIni.Date := Date - 30;
  TPFim.Date := Date;
  //
  RGModelo.ItemIndex  := -1;
  BtCalcula.Enabled   := False;
  BtResultado.Enabled := False;
end;

end.

