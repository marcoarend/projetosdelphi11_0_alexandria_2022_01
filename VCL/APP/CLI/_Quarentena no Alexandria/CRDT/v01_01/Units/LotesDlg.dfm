object FmLotesDlg: TFmLotesDlg
  Left = 419
  Top = 217
  Caption = 'Impress'#245'es'
  ClientHeight = 254
  ClientWidth = 306
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 206
    Width = 306
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtImprime: TBitBtn
      Tag = 5
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Imprime'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtImprimeClick
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 204
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtSaidaClick
    end
    object BtPreview: TBitBtn
      Tag = 27
      Left = 112
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Preview'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtPreviewClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 306
    Height = 48
    Align = alTop
    Caption = 'Impress'#245'es'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 304
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 310
      ExplicitHeight = 44
    end
  end
  object CLImpressoes: TCheckListBox
    Left = 0
    Top = 48
    Width = 194
    Height = 158
    Align = alClient
    ItemHeight = 13
    Items.Strings = (
      'Border'#244' Cliente'
      'Border'#244' Cont'#225'bil'
      'Aditivo + N.P. + A.P.'
      'Aditivo + N.P.'
      'Aditivo'
      'Nota Promiss'#243'ria'
      'Autoriza'#231#227'o de Protesto'
      'Carta ao Sacado'
      'Margem Coligadas')
    TabOrder = 2
    OnClick = CLImpressoesClick
  end
  object Panel1: TPanel
    Left = 194
    Top = 48
    Width = 112
    Height = 158
    Align = alRight
    BevelOuter = bvLowered
    TabOrder = 3
    object BtTodos: TBitBtn
      Tag = 127
      Left = 10
      Top = 8
      Width = 90
      Height = 40
      Caption = '&Todos'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtTodosClick
    end
    object BtNenhum: TBitBtn
      Tag = 128
      Left = 10
      Top = 52
      Width = 90
      Height = 40
      Caption = '&Nenhum'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtNenhumClick
    end
    object BtSalvar: TBitBtn
      Tag = 24
      Left = 10
      Top = 96
      Width = 90
      Height = 40
      Caption = 'Sal&va'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtSalvarClick
    end
  end
end
