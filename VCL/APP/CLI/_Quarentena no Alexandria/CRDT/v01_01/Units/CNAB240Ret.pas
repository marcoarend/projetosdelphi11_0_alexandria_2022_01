unit CNAB240Ret;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls, Grids,
  DBGrids, math, ComCtrls, dmkGeral, UnDmkEnums, UnDmkProcFunc;

type
  TFmCNAB240Ret = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    QrCNAB240Dir: TmySQLQuery;
    DsCNAB240Dir: TDataSource;
    QrCNAB240DirNOMEBANCO: TWideStringField;
    QrCNAB240DirCodigo: TIntegerField;
    QrCNAB240DirLk: TIntegerField;
    QrCNAB240DirDataCad: TDateField;
    QrCNAB240DirDataAlt: TDateField;
    QrCNAB240DirUserCad: TIntegerField;
    QrCNAB240DirUserAlt: TIntegerField;
    QrCNAB240DirNome: TWideStringField;
    QrCNAB240DirEnvio: TSmallintField;
    QrCNAB240DirBanco: TIntegerField;
    Timer1: TTimer;
    PnCarrega: TPanel;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Button1: TButton;
    PnArquivos: TPanel;
    ListBox1: TListBox;
    MemoTam: TMemo;
    QrDupl: TmySQLQuery;
    QrLotesIts: TmySQLQuery;
    Panel1: TPanel;
    DBGrid1: TDBGrid;
    GradeA: TStringGrid;
    QrLotesItsNOMECLI: TWideStringField;
    QrLotesItsCodigo: TIntegerField;
    QrLotesItsDuplicata: TWideStringField;
    QrLotesItsCPF: TWideStringField;
    QrLotesItsEmitente: TWideStringField;
    QrLotesItsBruto: TFloatField;
    QrLotesItsDesco: TFloatField;
    QrLotesItsQuitado: TIntegerField;
    QrLotesItsValor: TFloatField;
    QrLotesItsEmissao: TDateField;
    QrLotesItsDCompra: TDateField;
    QrLotesItsVencto: TDateField;
    QrLotesItsDDeposito: TDateField;
    QrLotesItsDias: TIntegerField;
    QrLotesItsData3: TDateField;
    QrLotesItsCobranca: TIntegerField;
    QrLotesItsRepassado: TSmallintField;
    QrLotesItsLote: TIntegerField;
    QrLotesItsTIPOLOTE: TSmallintField;
    QrLotesItsCliente: TIntegerField;
    QrCNAB240: TmySQLQuery;
    QrCNAB240CNAB240L: TIntegerField;
    QrCNAB240CNAB240I: TIntegerField;
    QrCNAB240Controle: TIntegerField;
    QrCNAB240Banco: TIntegerField;
    QrCNAB240ConfigBB: TIntegerField;
    QrCNAB240Lote: TIntegerField;
    QrCNAB240Item: TIntegerField;
    QrCNAB240DataOcor: TDateField;
    QrCNAB240Envio: TSmallintField;
    QrCNAB240Movimento: TSmallintField;
    QrCNAB240Lk: TIntegerField;
    QrCNAB240DataCad: TDateField;
    QrCNAB240DataAlt: TDateField;
    QrCNAB240UserCad: TIntegerField;
    QrCNAB240UserAlt: TIntegerField;
    QrCNAB240NOMEENVIO: TWideStringField;
    QrCNAB240NOMEBANCO: TWideStringField;
    QrCNAB240NOMEMOVIMENTO: TWideStringField;
    QrCNAB240NOMECLI: TWideStringField;
    QrCNAB240MeuLote: TIntegerField;
    QrCNAB240Duplicata: TWideStringField;
    QrCNAB240CPF: TWideStringField;
    QrCNAB240Emitente: TWideStringField;
    QrCNAB240Bruto: TFloatField;
    QrCNAB240Desco: TFloatField;
    QrCNAB240Quitado: TIntegerField;
    QrCNAB240Valor: TFloatField;
    QrCNAB240Emissao: TDateField;
    QrCNAB240DCompra: TDateField;
    QrCNAB240Vencto: TDateField;
    QrCNAB240DDeposito: TDateField;
    QrCNAB240Dias: TIntegerField;
    QrCNAB240Data3: TDateField;
    QrCNAB240Cobranca: TIntegerField;
    QrCNAB240Repassado: TSmallintField;
    QrCNAB240Bordero: TIntegerField;
    QrCNAB240TIPOLOTE: TSmallintField;
    QrCNAB240DATA3_TXT: TWideStringField;
    QrCNAB240NOMESTATUS: TWideStringField;
    QrCNAB240NOMETIPOLOTE: TWideStringField;
    QrCNAB240CPF_TXT: TWideStringField;
    QrCNAB240Custas: TFloatField;
    QrCNAB240IRTCLB_TXT: TWideStringField;
    QrCNAB240ValPago: TFloatField;
    QrCNAB240ValCred: TFloatField;
    QrCNAB240Cliente: TIntegerField;
    QrCNAB240Acao: TIntegerField;
    QrCNAB240NOMEACAO: TWideStringField;
    QrCNAB240Sequencia: TIntegerField;
    QrCNAB240Convenio: TWideStringField;
    QrCNAB240IRTCLB: TWideStringField;
    DsCNAB240: TDataSource;
    QrSumPg: TmySQLQuery;
    QrSumPgJuros: TFloatField;
    QrSumPgPago: TFloatField;
    QrSumPgDesco: TFloatField;
    QrSumPgValor: TFloatField;
    QrSumPgMaxData: TDateField;
    PnMovimento: TPanel;
    Panel3: TPanel;
    Panel4: TPanel;
    BitBtn2: TBitBtn;
    Button2: TButton;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    QrOcorreu: TmySQLQuery;
    QrOcorreuNOMEOCORRENCIA: TWideStringField;
    QrOcorreuCodigo: TIntegerField;
    QrOcorreuLotesIts: TIntegerField;
    QrOcorreuDataO: TDateField;
    QrOcorreuOcorrencia: TIntegerField;
    QrOcorreuValor: TFloatField;
    QrOcorreuLoteQuit: TIntegerField;
    QrOcorreuLk: TIntegerField;
    QrOcorreuDataCad: TDateField;
    QrOcorreuDataAlt: TDateField;
    QrOcorreuUserCad: TIntegerField;
    QrOcorreuUserAlt: TIntegerField;
    QrOcorreuTaxaP: TFloatField;
    QrOcorreuTaxaV: TFloatField;
    QrOcorreuPago: TFloatField;
    QrOcorreuDataP: TDateField;
    QrOcorreuTaxaB: TFloatField;
    QrOcorreuATUALIZADO: TFloatField;
    QrOcorreuCLIENTELOTE: TIntegerField;
    QrOcorreuData3: TDateField;
    QrOcorreuStatus: TSmallintField;
    QrOcorreuCliente: TIntegerField;
    QrOcorreuSALDO: TFloatField;
    QrOcorreuATZ_TEXTO: TWideStringField;
    DsOcorreu: TDataSource;
    QrOcorDupl: TmySQLQuery;
    QrOcorDuplNOMEOCORBANK: TWideStringField;
    QrOcorDuplValOcorBank: TFloatField;
    QrOcorDuplOcorrbase: TIntegerField;
    QrOcorBank: TmySQLQuery;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    QrOcorBankLk: TIntegerField;
    QrOcorBankDataCad: TDateField;
    QrOcorBankDataAlt: TDateField;
    QrOcorBankUserCad: TIntegerField;
    QrOcorBankUserAlt: TIntegerField;
    QrOcorBankBase: TFloatField;
    QrOcorBankEnvio: TIntegerField;
    QrOcorBankMovimento: TIntegerField;
    QrOcorBankFormaCNAB: TSmallintField;
    Timer2: TTimer;
    DBGrid6: TDBGrid;
    QrOB2: TmySQLQuery;
    QrOB2Nome: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    GradeB: TStringGrid;
    GradeC: TStringGrid;
    QrOcorCli: TmySQLQuery;
    QrOcorCliBase: TFloatField;
    QrOcorCliFormaCNAB: TSmallintField;
    MeRejeicoes: TMemo;
    Pn_L: TPanel;
    BtPasta: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure GradeASelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure GradeBDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure QrCNAB240DirAfterOpen(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure QrCNAB240CalcFields(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure QrOcorreuCalcFields(DataSet: TDataSet);
    procedure QrCNAB240AfterScroll(DataSet: TDataSet);
    procedure Timer2Timer(Sender: TObject);
    procedure GradeBSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure GradeCSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure GradeBClick(Sender: TObject);
    procedure GradeBKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GradeCClick(Sender: TObject);
    procedure GradeCKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtPastaClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
  private
    { Private declarations }
    FLinB, FLinA, FActiveRowA, FActiveRowB, FActiveRowC, FCNAB240L: Integer;
    procedure LeArquivos(Diretorio: String; CodDir, LinA: Integer);
    procedure Liquidacao;
    procedure CalculaPagamento(LotesIts: Integer);
    procedure ImprimeRecibodeQuitacao;
    procedure MostraFmDuplicatas(Controle: String);
    procedure ReopenOcorreu(Codigo: Integer);
    procedure ExcluiArquivos;
    //
    function ExecutaItens: Boolean;
    function GravaItens: Integer;
    function CarregaArquivo(Arquivo: String; LinA: Integer): Boolean;
    //
    procedure MensagensRejeicao(IRTCLB: String);
    procedure AbreDiretorioCNAB();
  public
    { Public declarations }
    FLista: TStrings;
    FLengt: Integer;
  end;

  var
  FmCNAB240Ret: TFmCNAB240Ret;

implementation

{$R *.DFM}

uses Module, UMySQLModule, UnGOTOy, Duplicatas2, UnInternalConsts, UnMyObjects,
  UnBancos;

procedure TFmCNAB240Ret.AbreDiretorioCNAB;
begin
  if (QrCNAB240Dir.State <> dsInactive) and (QrCNAB240Dir.RecordCount > 0) and
    (DirectoryExists(QrCNAB240DirNome.Value)) then
  begin
    //Abre diret�rio
    Geral.AbreArquivo(QrCNAB240DirNome.Value);
  end;
end;

procedure TFmCNAB240Ret.QrCNAB240CalcFields(DataSet: TDataSet);
var
  Envio: TEnvioCNAB;
begin
  case QrCNAB240Envio.Value of
    1: Envio := ecnabRemessa;
    2: Envio := ecnabRetorno;
    else Envio := ecnabIndefinido;
  end;
  case QrCNAB240TIPOLOTE.Value of
    0: QrCNAB240NOMETIPOLOTE.Value := 'Cheque';
    1: QrCNAB240NOMETIPOLOTE.Value := 'Duplicata';
    else QrCNAB240NOMETIPOLOTE.Value := 'Desconhecido';
  end;
  QrCNAB240DATA3_TXT.Value := Geral.FDT(QrCNAB240Data3.Value, 2);
  QrCNAB240CPF_TXT.Value := Geral.FormataCNPJ_TT(QrCNAB240CPF.Value);
  //////////////////////////////////////////////////////////////////////////////
  QrCNAB240NOMESTATUS.Value := MLAGeral.NomeStatusPgto2(QrCNAB240Quitado.Value,
    QrCNAB240DDeposito.Value, Date, QrCNAB240Data3.Value, QrCNAB240Repassado.Value);
  //////////////////////////////////////////////////////////////////////////////
  QrCNAB240NOMEMOVIMENTO.Value := UBancos.CNABTipoDeMovimento(1,
    Envio,  QrCNAB240Movimento.Value, 0, False, '');
  QrCNAB240NOMEENVIO.Value := UBancos.CNAB240Envio(QrCNAB240Envio.Value);
  // Parei Aqui Fazer certo!!!
  (*QrCNAB240IRTCLB_TXT.Value := InformacaoComplementar(QrCNAB240IRTCLB.Value,
    QrCNAB240Movimento.Value, False);*)
  if QrCNAB240Acao.Value = 1 then QrCNAB240NOMEACAO.Value := 'Sim'
  else QrCNAB240NOMEACAO.Value := '';
end;

procedure TFmCNAB240Ret.FormDestroy(Sender: TObject);
begin
  FLista.Free;
end;

procedure TFmCNAB240Ret.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCNAB240Ret.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if QrCNAB240Dir.State = dsInactive then Timer1.Enabled := True;
end;

procedure TFmCNAB240Ret.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCNAB240Ret.FormCreate(Sender: TObject);
begin
  MeRejeicoes.Lines.Clear;
  PnMovimento.Align := alClient;
  FLengt := 240;
  FLista := TStringList.Create;
  FActiveRowA := 0;
  FActiveRowB := 0;
  FActiveRowC := 0;
  //
  GradeA.ColWidths[00] := 032;
  GradeA.ColWidths[01] := 120;
  GradeA.ColWidths[02] := 044;
  GradeA.ColWidths[03] := 036;
  GradeA.ColWidths[04] := 028;
  GradeA.ColWidths[05] := 064;
  //
  GradeA.Cells[00,00] := 'Seq';
  GradeA.Cells[01,00] := 'Arquivo';
  GradeA.Cells[02,00] := 'Lotes';
  GradeA.Cells[03,00] := 'Itens';
  GradeA.Cells[04,00] := 'Dir';
  GradeA.Cells[05,00] := 'Cr�ditos';
  //
  GradeB.ColWidths[00] := 024;
  GradeB.ColWidths[01] := 044;
  GradeB.ColWidths[02] := 024;
  GradeB.ColWidths[03] := 024;
  GradeB.ColWidths[04] := 052;
  GradeB.ColWidths[05] := 056;
  GradeB.ColWidths[06] := 026;
  GradeB.ColWidths[07] := 160;
  GradeB.ColWidths[08] := 064;
  GradeB.ColWidths[09] := 064;
  GradeB.ColWidths[10] := 064;
  GradeB.ColWidths[11] := 064;
  GradeB.ColWidths[12] := 100;
  GradeB.ColWidths[13] := 028;
  GradeB.ColWidths[14] := 072;
  GradeB.ColWidths[15] := 040;
  GradeB.ColWidths[16] := 120;
  GradeB.ColWidths[17] := 064;
  GradeB.ColWidths[18] := 056;
  GradeB.ColWidths[19] := 120;
  GradeB.ColWidths[20] := 113;
  //
  GradeB.Cells[00,00] := 'Seq';
  GradeB.Cells[01,00] := 'Seq.Arq.';
  GradeB.Cells[02,00] := 'Lote';
  GradeB.Cells[03,00] := 'Item';
  GradeB.Cells[04,00] := 'Meu ID';
  GradeB.Cells[05,00] := 'Data Ocor.';
  GradeB.Cells[06,00] := 'Mov';
  GradeB.Cells[07,00] := 'Descri��o do movimento';
  GradeB.Cells[08,00] := 'Custas';
  GradeB.Cells[09,00] := 'Val.pago';
  GradeB.Cells[10,00] := 'Cr�dito';
  GradeB.Cells[11,00] := 'IRTCLB';
  GradeB.Cells[12,00] := 'Conv�nio';
  GradeB.Cells[13,00] := 'Bco';
  GradeB.Cells[14,00] := 'Duplicata';
  GradeB.Cells[15,00] := 'Cliente';
  GradeB.Cells[16,00] := 'Nome Cliente';
  GradeB.Cells[17,00] := 'Val.bruto';
  GradeB.Cells[18,00] := 'Resgate';
  GradeB.Cells[19,00] := 'Sacado';
  GradeB.Cells[20,00] := 'CPF/CNPJ Sacado';
  //
  GradeC.ColWidths[00] := 024;
  GradeC.ColWidths[01] := 026;
  GradeC.ColWidths[02] := 160;
  GradeC.ColWidths[03] := 064;
  GradeC.ColWidths[04] := 064;
  GradeC.ColWidths[05] := 064;
  GradeC.ColWidths[06] := 100;
  GradeC.ColWidths[07] := 028;
  GradeC.ColWidths[08] := 072;
  GradeC.ColWidths[09] := 040;
  GradeC.ColWidths[10] := 120;
  GradeC.ColWidths[11] := 064;
  GradeC.ColWidths[12] := 056;
  GradeC.ColWidths[13] := 120;
  GradeC.ColWidths[14] := 113;
  GradeC.ColWidths[15] := 056;
  GradeC.ColWidths[16] := 044;
  GradeC.ColWidths[17] := 024;
  GradeC.ColWidths[18] := 024;
  GradeC.ColWidths[19] := 052;
  GradeC.ColWidths[20] := 064;
  //
  GradeC.Cells[00,00] := 'Seq';
  GradeC.Cells[01,00] := 'Mov';
  GradeC.Cells[02,00] := 'Descri��o do movimento';
  GradeC.Cells[03,00] := 'Custas';
  GradeC.Cells[04,00] := 'Val.pago';
  GradeC.Cells[05,00] := 'Cr�dito';
  GradeC.Cells[06,00] := 'Conv�nio';
  GradeC.Cells[07,00] := 'Bco';
  GradeC.Cells[08,00] := 'Duplicata';
  GradeC.Cells[09,00] := 'Cliente';
  GradeC.Cells[10,00] := 'Nome Cliente';
  GradeC.Cells[11,00] := 'Val.bruto';
  GradeC.Cells[12,00] := 'Resgate';
  GradeC.Cells[13,00] := 'Sacado';
  GradeC.Cells[14,00] := 'CPF/CNPJ Sacado';
  GradeC.Cells[15,00] := 'Data Ocor.';
  GradeC.Cells[16,00] := 'Seq.Arq.';
  GradeC.Cells[17,00] := 'Lote';
  GradeC.Cells[18,00] := 'Item';
  GradeC.Cells[19,00] := 'Meu ID';
  GradeC.Cells[20,00] := 'IRTCLB';
  //
end;

procedure TFmCNAB240Ret.LeArquivos(Diretorio: String; CodDir, LinA: Integer);
var
  i, n: Integer;
begin
  if Diretorio[Length(Diretorio)] <> '\' then Diretorio := Diretorio + '\';
  //dmkPF.GetAllFiles(False, Diretorio + '*.*', ListBox1, True);
  dmkPF.GetAllFiles(False, Diretorio + '*.ret', ListBox1, True);
  n := LinA;
  for i := 0 to ListBox1.Items.Count -1 do
  begin
    if FileExists(ListBox1.Items[i]) then
    begin
      if Trim(GradeA.Cells[01, n]) <> '' then
        n := n + 1;
      // Parei Aqui
      GradeA.RowCount := n + 1;
      GradeA.Cells[00, n] := Geral.FF0(n);
      GradeA.Cells[01, n] := ExtractFileName(ListBox1.Items[i]);
      GradeA.Cells[04, n] := FormatFloat('000', CodDir);
      //
      CarregaArquivo(ListBox1.Items[i], n);
    end;
  end;
  if GradeB.RowCount > 2 then
    if GradeB.Cells[00, GradeB.RowCount -1] = '' then
      GradeB.RowCount := GradeB.RowCount - 1;
  //    
  if GradeC.RowCount > 2 then
    if GradeC.Cells[00, GradeC.RowCount -1] = '' then
      GradeC.RowCount := GradeC.RowCount - 1;
end;

procedure TFmCNAB240Ret.GradeASelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if ARow <> FActiveRowA then
    FActiveRowA := ARow;
end;

function TFmCNAB240Ret.CarregaArquivo(Arquivo: String; LinA: Integer): Boolean;
var
  i, k, lote, Item: Integer;
  Controle: Int64;
  x: Char;
  Bufer, DtOcor, Movim2, IRTCLB, Custas, ValPag, ValCre, SeqArq, Convenio,
  Banco: String;
  SumVal: Double;
begin
  Result := False;
  Controle := 0;
  if not FileExists(Arquivo) then
  begin
    Geral.MB_Aviso('O arquivo "' + Arquivo + '" n�o foi localizado.' +
      ' Ele pode ter sido exclu�do!');
    Exit;
  end;
  FLista.Clear;
  FLista.LoadFromFile(Arquivo);
  k := 0;
  for i := 0 to FLista.Count -1 do
    if Length(FLista[i]) <> FLengt then k := k + 1;
  if k > 0 then
  begin
    Geral.MB_Erro('O arquivo "' + Arquivo + '" possui ' + Geral.FF0(k) +
      ' linhas que possuem quantidade de caracteres diferente de ' + Geral.FF0(FLengt) +
      '.');
    Exit;
  end;
  k := Geral.IMV(Copy(FLista[FLista.Count -1], 24, 6));
  if FLista.Count <> k then
  begin
    Geral.MB_Erro('O arquivo "'+Arquivo+'" possui ' +
      Geral.FF0(FLista.Count) + ' registros mas seu trailer informa ' +
      Geral.FF0(k)+'!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if Copy(FLista[0], 4, 4) <> '0000' then
  begin
    Geral.MB_Erro('O arquivo "' + Arquivo + '" n�o � lote de servi�os!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if FLista[0][8] <> '0' then
  begin
    Geral.MB_Erro('A linha 1 do arquivo "'+Arquivo+'" n�o � '+
      'cabe�alho de arquivo!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  if FLista[0][143] <> '2' then
  begin
    Geral.MB_Erro('O arquivo "' + Arquivo + '" n�o � lote de retorno!');
    Screen.Cursor := crDefault;
    Exit;
  end;

  //////////////////////////////////////////////////////////////////////////////
  SumVal := 0;
  GradeA.Cells[02, linA] := Copy(FLista[FLista.Count - 1], 018, 006);
  lote   := 0;
  Item   := 0;
  for i := 0 to FLista.Count -1 do
  begin
    k := Geral.IMV(FLista[i][008]);
    x := FLista[i][014];
    case k of
      0:
      begin
        SeqArq := Copy(FLista[0], 158, 06);
      end;
      1:
      begin
        Lote     := Lote + 1;
        Item     := 0;
        Convenio := Copy(FLista[i], 034, 020);
      end;
      3:
      begin
        if x = 'T' then
        begin
          Bufer := Copy(FLista[i], 106, 025);

          // Parei Aqui
          // tempor�rio para arrumar erro
          case Geral.IMV(Bufer[1]) of
            0: ;
            1: Bufer := Copy(Bufer, 1, 5);
            else Bufer := Copy(Bufer, 1, 4);
          end;
          
          //
          Controle := MLAGeral.I64MV(Bufer);
          Custas := MLAGeral.XFT(Copy(FLista[i], 199, 015), 2, siPositivo);
          //
          if FLinB >= GradeB.RowCount then
            GradeB.RowCount := FLinB + 1;
          GradeB.Cells[00, FLinB] := Geral.FF0(LinA);
          GradeB.Cells[02, FLinB] := Geral.FF0(Lote);
          GradeB.Cells[04, FLinB] := Geral.FF0(Controle);
          GradeB.Cells[08, FLinB] := Geral.TFT_Null(Custas, 2, siPositivo);
          //
          if FLinB >= GradeC.RowCount then
            GradeC.RowCount := FLinB + 1;
          GradeC.Cells[00, FLinB] := Geral.FF0(LinA);
          GradeC.Cells[17, FLinB] := Geral.FF0(Lote);
          GradeC.Cells[19, FLinB] := Geral.FF0(Controle);
          GradeC.Cells[03, FLinB] := Geral.TFT_Null(Custas, 2, siPositivo);
          IRTCLB := Copy(FLista[i], 214, 010);
          GradeB.Cells[11, FLinB] := IRTCLB;
          GradeC.Cells[20, FLinB] := IRTCLB;
          //
        end else
        if x =  'U' then
        begin
          Item   := Item + 1;
          DtOcor := MLAGeral.DataCBRTxt(Copy(FLista[i], 138, 008));
          DtOcor := Geral.FDT(Geral.ValidaDataSimples(DtOcor, True), 3);
          Movim2 := Copy(FLista[i], 016, 002);
          ValPag := MLAGeral.XFT(Copy(FLista[i], 078, 015), 2, siPositivo);
          ValCre := MLAGeral.XFT(Copy(FLista[i], 093, 015), 2, siPositivo);
          SumVal := SumVal + Geral.DMV(ValCre);
          Banco  := Copy(FLista[i], 001, 003);
          //
          GradeA.Cells[03, linA] := FormatFloat('00000', Item);
          //
          GradeB.Cells[01, FLinB] := SeqArq;
          GradeB.Cells[03, FLinB] := Geral.FF0(Item);
          GradeB.Cells[05, FLinB] := DtOcor;
          GradeB.Cells[06, FLinB] := Movim2;
          GradeB.Cells[07, FLinB] := UBancos.CNABTipoDeMovimento(1,
            ecnabRetorno, Geral.IMV(Movim2), 0, False, '');
          GradeB.Cells[09, FLinB] := Geral.TFT_Null(ValPag, 2, siPositivo);
          GradeB.Cells[10, FLinB] := Geral.TFT_Null(ValCre, 2, siPositivo);
          GradeB.Cells[12, FLinB] := Convenio;
          GradeB.Cells[13, FLinB] := Banco;
          //
          GradeC.Cells[16, FLinB] := SeqArq;
          GradeC.Cells[18, FLinB] := Geral.FF0(Item);
          GradeC.Cells[15, FLinB] := DtOcor;
          GradeC.Cells[01, FLinB] := Movim2;
          GradeC.Cells[02, FLinB] := UBancos.CNABTipoDeMovimento(1,
            ecnabRetorno, Geral.IMV(Movim2), 0, False, '');
          GradeC.Cells[04, FLinB] := Geral.TFT_Null(ValPag, 2, siPositivo);
          GradeC.Cells[05, FLinB] := Geral.TFT_Null(ValCre, 2, siPositivo);
          GradeC.Cells[06, FLinB] := Convenio;
          GradeC.Cells[07, FLinB] := Banco;
          //
          QrLotesIts.Close;
          QrLotesIts.Params[0].AsInteger := Controle;
          QrLotesIts.Open;
          GradeB.Cells[14, FLinB] := QrLotesItsDuplicata.Value;
          GradeB.Cells[15, FLinB] := Geral.FF0(QrLotesItsCliente.Value);
          GradeB.Cells[16, FLinB] := QrLotesItsNOMECLI.Value;
          GradeB.Cells[17, FLinB] := Geral.FFT(QrLotesItsBruto.Value, 2, siPositivo);
          GradeB.Cells[18, FLinB] := Geral.FDT(QrLotesItsDDeposito.Value, 3);
          GradeB.Cells[19, FLinB] := QrLotesItsEmitente.Value;
          GradeB.Cells[20, FLinB] := Geral.FormataCNPJ_TT(QrLotesItsCPF.Value);
          //
          GradeC.Cells[08, FLinB] := QrLotesItsDuplicata.Value;
          GradeC.Cells[09, FLinB] := Geral.FF0(QrLotesItsCliente.Value);
          GradeC.Cells[10, FLinB] := QrLotesItsNOMECLI.Value;
          GradeC.Cells[11, FLinB] := Geral.FFT(QrLotesItsBruto.Value, 2, siPositivo);
          GradeC.Cells[12, FLinB] := Geral.FDT(QrLotesItsDDeposito.Value, 3);
          GradeC.Cells[13, FLinB] := QrLotesItsEmitente.Value;
          GradeC.Cells[14, FLinB] := Geral.FormataCNPJ_TT(QrLotesItsCPF.Value);
          //
          GradeB.RowCount := GradeB.RowCount + 1;
          //
          GradeC.RowCount := GradeC.RowCount + 1;
          //
          FLinB := FLinB + 1;
        end else
          Geral.MB_Erro('C�digo de segmento do registro de ' +
            'detalhe desconhecido: ' + x);
      end;
      5:
      begin
        GradeA.Cells[05, linA] := Geral.FFT(SumVal, 2, siPositivo);
        SumVal := 0;
      end;
      9:
      begin
        //
      end;
      else
        Geral.MB_Erro('Tipo de registro desconhecido: '+
          Geral.FF0(k)+'!');
    end;
  end;
  for i := 1 to GradeC.RowCount - 1 do
  begin
  end;
  Result := True;
end;

procedure TFmCNAB240Ret.DBGrid1DblClick(Sender: TObject);
begin
  AbreDiretorioCNAB();
end;

procedure TFmCNAB240Ret.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  QrCNAB240Dir.Close;
  QrCNAB240Dir.Open;
  //
  if QrCNAB240Dir.RecordCount = 0 then
    Geral.MB_Aviso('Nenhum diret�rio foi definido em Cadastros -> B�sicos -> Diret�rios ' +
      'CNAB240. Defina ao menos um diret�rio!');
end;

procedure TFmCNAB240Ret.Button1Click(Sender: TObject);
var
  i: Integer;
begin
  MemoTam.Lines.Clear;
  for i := 0 to GradeA.ColCount -1 do
    MemoTam.Lines.Add('  GradeA.ColWidths['+FormatFloat('000', i) + '] := ' +
      FormatFloat('0000', GradeA.ColWidths[i])+';');
  for i := 0 to GradeB.ColCount -1 do
    MemoTam.Lines.Add('  GradeB.ColWidths['+FormatFloat('000', i) + '] := ' +
      FormatFloat('0000', GradeB.ColWidths[i])+';');
  for i := 0 to GradeC.ColCount -1 do
    MemoTam.Lines.Add('  GradeC.ColWidths['+FormatFloat('000', i) + '] := ' +
      FormatFloat('0000', GradeC.ColWidths[i])+';');
end;

procedure TFmCNAB240Ret.GradeADrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  OldAlign: Integer;
begin
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Geral.TFD(GradeA.Cells[Acol, ARow], 3, siPositivo));
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_LEFT);
      GradeA.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 02 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 03 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 04 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_CENTER);
      GradeA.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end else if ACol = 05 then begin
      OldAlign := SetTextAlign(GradeA.Canvas.Handle, TA_RIGHT);
      GradeA.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeA.Cells[Acol, ARow]);
      SetTextAlign(GradeA.Canvas.Handle, OldAlign);
  end
end;

procedure TFmCNAB240Ret.GradeBClick(Sender: TObject);
begin
  MensagensRejeicao(GradeB.Cells[11,GradeB.Row]);
end;

procedure TFmCNAB240Ret.GradeBDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  OldAlign: Integer;
begin
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(GradeB.Canvas.Handle, TA_CENTER);
      GradeB.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Geral.TFD(GradeB.Cells[Acol, ARow], 3, siPositivo));
      SetTextAlign(GradeB.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(GradeB.Canvas.Handle, TA_CENTER);
      GradeB.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeB.Cells[Acol, ARow]);
      SetTextAlign(GradeB.Canvas.Handle, OldAlign);
  end else if ACol = 02 then begin
      OldAlign := SetTextAlign(GradeB.Canvas.Handle, TA_RIGHT);
      GradeB.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeB.Cells[Acol, ARow]);
      SetTextAlign(GradeB.Canvas.Handle, OldAlign);
  end else if ACol = 03 then begin
      OldAlign := SetTextAlign(GradeB.Canvas.Handle, TA_RIGHT);
      GradeB.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeB.Cells[Acol, ARow]);
      SetTextAlign(GradeB.Canvas.Handle, OldAlign);
  end else if ACol = 04 then begin
      OldAlign := SetTextAlign(GradeB.Canvas.Handle, TA_RIGHT);
      GradeB.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeB.Cells[Acol, ARow]);
      SetTextAlign(GradeB.Canvas.Handle, OldAlign);
  end else if ACol = 05 then begin
      OldAlign := SetTextAlign(GradeB.Canvas.Handle, TA_CENTER);
      GradeB.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeB.Cells[Acol, ARow]);
      SetTextAlign(GradeB.Canvas.Handle, OldAlign);
  end else if ACol = 06 then begin
      OldAlign := SetTextAlign(GradeB.Canvas.Handle, TA_CENTER);
      GradeB.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeB.Cells[Acol, ARow]);
      SetTextAlign(GradeB.Canvas.Handle, OldAlign);
  end else if ACol = 08 then begin
      OldAlign := SetTextAlign(GradeB.Canvas.Handle, TA_RIGHT);
      GradeB.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeB.Cells[Acol, ARow]);
      SetTextAlign(GradeB.Canvas.Handle, OldAlign);
  end else if ACol = 09 then begin
      OldAlign := SetTextAlign(GradeB.Canvas.Handle, TA_RIGHT);
      GradeB.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeB.Cells[Acol, ARow]);
      SetTextAlign(GradeB.Canvas.Handle, OldAlign);
  end else if ACol = 10 then begin
      OldAlign := SetTextAlign(GradeB.Canvas.Handle, TA_RIGHT);
      GradeB.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeB.Cells[Acol, ARow]);
      SetTextAlign(GradeB.Canvas.Handle, OldAlign);
  end else if ACol = 15 then begin
      OldAlign := SetTextAlign(GradeB.Canvas.Handle, TA_RIGHT);
      GradeB.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeB.Cells[Acol, ARow]);
      SetTextAlign(GradeB.Canvas.Handle, OldAlign);
  end else if ACol = 17 then begin
      OldAlign := SetTextAlign(GradeB.Canvas.Handle, TA_RIGHT);
      GradeB.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeB.Cells[Acol, ARow]);
      SetTextAlign(GradeB.Canvas.Handle, OldAlign);
  end else if ACol = 18 then begin
      OldAlign := SetTextAlign(GradeB.Canvas.Handle, TA_CENTER);
      GradeB.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeB.Cells[Acol, ARow]);
      SetTextAlign(GradeB.Canvas.Handle, OldAlign);
  end else if ACol = 20 then begin
      OldAlign := SetTextAlign(GradeB.Canvas.Handle, TA_CENTER);
      GradeB.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeB.Cells[Acol, ARow]);
      SetTextAlign(GradeB.Canvas.Handle, OldAlign);
  end
end;

procedure TFmCNAB240Ret.GradeBKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MensagensRejeicao(GradeB.Cells[11,GradeB.Row]);
end;

procedure TFmCNAB240Ret.QrCNAB240DirAfterOpen(DataSet: TDataSet);
begin
  MyObjects.LimpaGrade(GradeA, 1, 1, True);
  MyObjects.LimpaGrade(GradeB, 1, 1, True);
  MyObjects.LimpaGrade(GradeC, 1, 1, True);
  FLinA := 0;
  FLinB := 1;
  while not QrCNAB240Dir.Eof do
  begin
    LeArquivos(QrCNAB240DirNome.Value, QrCNAB240DirCodigo.Value, FLinA);
    FLinA := GradeA.RowCount - 1;
    QrCNAB240Dir.Next;
  end;
end;

function TFmCNAB240Ret.GravaItens: Integer;
var
  i, n, a, Lote, Controle, Item, Movimento, ArqSeq, Continua, CNAB240I, Banco,
  CNAB240L: Integer;
  DataOcor, Convenio, IRTCLB: String;
  Custas, ValPago, ValCred: Double;
begin
(**)
  Result := 0;
  n := 0;
  for i := 1 to GradeB.RowCount - 1 do
  begin
    if Geral.IMV(GradeB.Cells[3, i]) > 0 then
      n := n + 1;
  end;
  {
  for i := 1 to GradeC.RowCount - 1 do
  begin
    if Geral.IMV(GradeC.Cells[18, i]) > 0 then
      n := n + 1;
  end;
  }
  if n = 0 then
  begin
    Geral.MB_Aviso('N�o h� itens a serem gravados!');
    Exit;  
  end;
  Screen.Cursor := crHourGlass;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO cnab240 SET ');
  Dmod.QrUpd.SQL.Add('Controle=:P0, Banco=:P1, ConfigBB=:P2, Lote=:P3, ');
  Dmod.QrUpd.SQL.Add('Item=:P4, DataOcor=:P5, Envio=:P6, Movimento=:P7, ');
  Dmod.QrUpd.SQL.Add('IRTCLB=:P8, Custas=:P9, ValPago=:P10, ');
  Dmod.QrUpd.SQL.Add('ValCred=:P11, Sequencia=:P12, Convenio=:P13,');
  Dmod.QrUpd.SQL.Add('CNAB240L=:Pa, CNAB240I=:Pb');
  CNAB240L := 0;
  for a := 1 to GradeB.RowCount - 1 do
  begin
    Controle  := Geral.IMV(GradeB.Cells[04, a]);
    Lote      := Geral.IMV(GradeB.Cells[02, a]);
    Item      := Geral.IMV(GradeB.Cells[03, a]);
    DataOcor  := MLAGeral.CDS(GradeB.Cells[05, a], 2, 1);
    Movimento := Geral.IMV(GradeB.Cells[06, a]);
    ArqSeq    := Geral.IMV(GradeB.Cells[01, a]);
    Convenio  := Trim(GradeB.Cells[12, a]);
    //
    Banco     := Geral.IMV(GradeB.Cells[13, a]);
    IRTCLB    := GradeB.Cells[11, a];
    Custas    := Geral.DMV(GradeB.Cells[08, a]);
    ValPago   := Geral.DMV(GradeB.Cells[09, a]);
    ValCred   := Geral.DMV(GradeB.Cells[10, a]);
    //
    QrDupl.Close;
    QrDupl.Params[00].AsInteger := Controle;
    QrDupl.Params[01].AsInteger := Lote;
    QrDupl.Params[02].AsInteger := Item;
    QrDupl.Params[03].AsString  := DataOcor;
    QrDupl.Params[04].AsInteger := 2;//Envio; = Retorno
    QrDupl.Params[05].AsInteger := Movimento;
    QrDupl.Params[06].AsInteger := ArqSeq;
    QrDupl.Params[07].AsString  := Convenio;
    QrDupl.Open;
    if QrDupl.RecordCount = 0 then Continua := ID_YES else
    begin
      Continua :=  Geral.MB_Pergunta('O item ' + Geral.FF0(Item) +
        ' do lote ' + Geral.FF0(Lote) + ' deste arquivo retorno (Controle ' +
        Geral.FF0(Controle) + ' do Border� ' + Geral.FF0(QrLotesItsLote.Value) +
        ' do lote creditor ' + Geral.FF0(QrLotesItsCodigo.Value) +
        ') j� foi registrado. Deseja registr�-lo assim mesmo?');
    end;
    case Continua of
      ID_YES:
      begin
        if CNAB240L = 0 then CNAB240L := UMyMod.BuscaEmLivreY(Dmod.MyDB,
          'Livres', 'Controle', 'CNAB240', 'CNAB240L', 'CNAB240L');
        CNAB240I := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'CNAB240', 'CNAB240I', 'CNAB240I');
        Dmod.QrUpd.Params[00].AsInteger := Controle;
        Dmod.QrUpd.Params[01].AsInteger := Banco;
        Dmod.QrUpd.Params[02].AsInteger := 0; // Parei Aqui nao usado ainda
        Dmod.QrUpd.Params[03].AsInteger := Lote;
        Dmod.QrUpd.Params[04].AsInteger := Item;
        Dmod.QrUpd.Params[05].AsString  := DataOcor;
        Dmod.QrUpd.Params[06].AsInteger := 2; //Envio - Retorno
        Dmod.QrUpd.Params[07].AsInteger := Movimento;
        Dmod.QrUpd.Params[08].AsString  := IRTCLB;
        Dmod.QrUpd.Params[09].AsFloat   := Custas;
        Dmod.QrUpd.Params[10].AsFloat   := ValPago;
        Dmod.QrUpd.Params[11].AsFloat   := ValCred;
        Dmod.QrUpd.Params[12].AsInteger := ArqSeq;
        Dmod.QrUpd.Params[13].AsString  := Convenio;
        //
        Dmod.QrUpd.Params[14].AsInteger := CNAB240L;
        Dmod.QrUpd.Params[15].AsInteger := CNAB240I;
        Dmod.QrUpd.ExecSQL;
      end;
      //ID_CANCEL: Exit;
    end;
  end;

  {
  for a := 1 to GradeC.RowCount - 1 do
  begin
    Controle  := Geral.IMV(GradeC.Cells[19, a]);
    Lote      := Geral.IMV(GradeC.Cells[17, a]);
    Item      := Geral.IMV(GradeC.Cells[18, a]);
    DataOcor  := MLAGeral.CDS(GradeC.Cells[15, a], 2, 1);
    Movimento := Geral.IMV(GradeC.Cells[01, a]);
    ArqSeq    := Geral.IMV(GradeC.Cells[16, a]);
    Convenio  := Trim(GradeC.Cells[12, a]);
    //
    Banco     := Geral.IMV(GradeC.Cells[07, a]);
    IRTCLB    := GradeC.Cells[20, a];
    Custas    := Geral.DMV(GradeC.Cells[03, a]);
    ValPago   := Geral.DMV(GradeC.Cells[04, a]);
    ValCred   := Geral.DMV(GradeC.Cells[05, a]);
    //
    QrDupl.Close;
    QrDupl.Params[00].AsInteger := Controle;
    QrDupl.Params[01].AsInteger := Lote;
    QrDupl.Params[02].AsInteger := Item;
    QrDupl.Params[03].AsString  := DataOcor;
    QrDupl.Params[04].AsInteger := 2;//Envio; = Retorno
    QrDupl.Params[05].AsInteger := Movimento;
    QrDupl.Params[06].AsInteger := ArqSeq;
    QrDupl.Params[07].AsString  := Convenio;
    QrDupl.Open;
    if QrDupl.RecordCount = 0 then Continua := ID_YES else
    begin
      Continua :=  Geral.MB_Pergunta('O item ' + Geral.FF0(Item) +
        ' do lote ' + Geral.FF0(Lote) + ' deste arquivo retorno (Controle ' +
        Geral.FF0(Controle)+' do Border� ' + Geral.FF0(QrLotesItsLote.Value) +
        ' do lote creditor ' + Geral.FF0(QrLotesItsCodigo.Value) +
        ') j� foi registrado. Deseja registr�-lo assim mesmo?');
    end;
    case Continua of
      ID_YES:
      begin
        if CNAB240L = 0 then CNAB240L := UMyMod.BuscaEmLivreY(Dmod.MyDB,
          'Livres', 'Controle', 'CNAB240', 'CNAB240L', 'CNAB240L');
        CNAB240I := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'CNAB240', 'CNAB240I', 'CNAB240I');
        Dmod.QrUpd.Params[00].AsInteger := Controle;
        Dmod.QrUpd.Params[01].AsInteger := Banco;
        Dmod.QrUpd.Params[02].AsInteger := 0; // Parei Aqui nao usado ainda
        Dmod.QrUpd.Params[03].AsInteger := Lote;
        Dmod.QrUpd.Params[04].AsInteger := Item;
        Dmod.QrUpd.Params[05].AsString  := DataOcor;
        Dmod.QrUpd.Params[06].AsInteger := 2; //Envio - Retorno
        Dmod.QrUpd.Params[07].AsInteger := Movimento;
        Dmod.QrUpd.Params[08].AsString  := IRTCLB;
        Dmod.QrUpd.Params[09].AsFloat   := Custas;
        Dmod.QrUpd.Params[10].AsFloat   := ValPago;
        Dmod.QrUpd.Params[11].AsFloat   := ValCred;
        Dmod.QrUpd.Params[12].AsInteger := ArqSeq;
        Dmod.QrUpd.Params[13].AsString  := Convenio;
        //
        Dmod.QrUpd.Params[14].AsInteger := CNAB240L;
        Dmod.QrUpd.Params[15].AsInteger := CNAB240I;
        Dmod.QrUpd.ExecSQL;
      end;
      //ID_CANCEL: Exit;
    end;
  end;
  }

  Screen.Cursor := crDefault;
  Result := CNAB240L;
end;

procedure TFmCNAB240Ret.BtOKClick(Sender: TObject);
begin

  // Carrega arquivos na Stringgrid e cruza com lotes
  FCNAB240L := GravaItens;

  // Grava dados no banco de dados
  if FCNAB240L > 0 then
  begin
    Application.ProcessMessages;
    //
    PnMovimento.Visible := True;
    PnCarrega.Visible := False;
    Application.ProcessMessages;
    //
    Timer2.Enabled := True;
  end;
end;

procedure TFmCNAB240Ret.BtPastaClick(Sender: TObject);
begin
  AbreDiretorioCNAB();
end;

function TFmCNAB240Ret.ExecutaItens: Boolean;
var
  Movimento, Envio, IDAliDup, MeuID, Controle, Status: Integer;
  Data: String;
  Valor: Double;
begin
  Screen.Cursor := crHourGlass;
  QrCNAB240.Close;
  QrCNAB240.Params[0].AsInteger := FCNAB240L;
  QrCNAB240.Open;
  //
  QrCNAB240.First;
  while not QrCNAB240.Eof do
  begin
    MeuID     := QrCNAB240Controle.Value;
    Envio     := QrCNAB240Envio.Value;
    Movimento := QrCNAB240Movimento.Value;
    //
    (*
    if QrCNAB240Acao.Value > 0 then
      Geral.MB_Aviso('Este movimento j� foi executado anteriormente!');
    *)
    
    // Localizar a al�nea que ser� utilizada pelo Status e pela ocorr�ncia
    QrOcorBank.Close;
    QrOcorBank.Params[00].AsInteger := Envio;
    QrOcorBank.Params[01].AsInteger := Movimento;
    QrOcorBank.Open;
    if QrOcorBankCodigo.Value <> 0 then
    begin
      // Atualiza status da duplicata
      Data := FormatDateTime(VAR_FORMATDATE, QrCNAB240DataOcor.Value);

      // Define status da duplicata
      Status := 0;
      case QrCNAB240Movimento.Value of
        06: // Liquida��o.
        begin
          if (int(QrCNAB240DDeposito.Value) > int(QrCNAB240DataOcor.Value)) then
            Status := Dmod.QrControleLiqstaAnt.Value;
          if (int(QrCNAB240DDeposito.Value) = int(QrCNAB240DataOcor.Value)) then
            Status := Dmod.QrControleLiqstaVct.Value;
          if (int(QrCNAB240DDeposito.Value) < int(QrCNAB240DataOcor.Value)) then
            Status := Dmod.QrControleLiqstaVcd.Value;
        end;
      end;
      if Status = 0 then Status := QrOcorBankCodigo.Value;
      //

      IDAliDup := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'ADupIts',
        'ADupIts', 'Controle');
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO adupits SET Alinea=:P0, DataA=:P1, ');
      Dmod.QrUpd.SQL.Add('Controle=:Pa, LotesIts=:Pb');
      //
      Dmod.QrUpd.Params[00].AsInteger := Status;
      Dmod.QrUpd.Params[01].AsString  := Data;
      //
      Dmod.QrUpd.Params[02].AsInteger := IDAliDup;
      Dmod.QrUpd.Params[03].AsInteger := MeuID;
      Dmod.QrUpd.ExecSQL;
      //

      // Ocorrencia associada ao movimento
      case QrOcorBankFormaCNAB.Value of
        0: Valor := 0;
        1: Valor := QrCNAB240Custas.Value;
        2: Valor := QrOcorBankBase.Value;
        else Valor := 0;
      end;

      // Se o cliente tiver em seu cadastro (FmEntiJur1) uma configura��o
      // para a ocorerncia, ent�o, seguir orienta��o dela
      // mudadndo o valor caso configurado
      QrOcorCli.Close;
      QrOcorCli.Params[0].AsInteger := QrCNAB240Cliente.Value;
      QrOcorCli.Params[1].AsInteger := QrOcorBankCodigo.Value;
      QrOcorCli.Open;
      //
      if QrOcorCli.RecordCount > 0 then
      begin
        case QrOcorCliFormaCNAB.Value of
          1: Valor := QrCNAB240Custas.Value;
          2: Valor := QrOcorCliBase.Value;
          3: ;//Valor := Valor; o valor j� est� definido
          else Valor := 0;
        end;
      end;
      //

      if Valor > 0 then
      begin
        Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'Ocorreu', 'Ocorreu', 'Codigo');
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO ocorreu SET AlterWeb=1, ');
        Dmod.QrUpd.SQL.Add('LotesIts=:P0, DataO=:P1, Ocorrencia=:P2, Valor=:P3, ');
        Dmod.QrUpd.SQL.Add('Codigo=:Pa');
        Dmod.QrUpd.Params[00].AsInteger := MeuID;
        Dmod.QrUpd.Params[01].AsString  := Data;
        Dmod.QrUpd.Params[02].AsInteger := QrOcorBankCodigo.Value; //
        Dmod.QrUpd.Params[03].AsFloat   := Valor;
        //
        Dmod.QrUpd.Params[04].AsInteger := Controle;
        Dmod.QrUpd.ExecSQL;
      end;
    end else
      Geral.MB_Aviso('N�o foi localizado c�digo de '+
        'status e/ou ocorr�ncia para o movimento ' + Geral.FF0(Movimento) + '!');

    // A��es espec�ficas do movimento
    case QrCNAB240Movimento.Value of
      02: ;// Nada - s� recebimento;
      06: Liquidacao;
      else begin
        {if QrCNAB240Custas.Value > 0 then
        begin
          Data  := FormatDateTime(VAR_FORMATDATE, QrCNAB240DataOcor.Value);
          Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
            'Ocorreu', 'Ocorreu', 'Codigo');
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('INSERT INTO ocorreu SET AlterWeb=1, ');
          Dmod.QrUpd.SQL.Add('LotesIts=:P0, DataO=:P1, Ocorrencia=:P2, ');
          Dmod.QrUpd.SQL.Add('Valor=:P3, MoviBank=:P4, Codigo=:Pa');
          Dmod.QrUpd.Params[00].AsInteger := MeuID;
          Dmod.QrUpd.Params[01].AsString  := Data;
          Dmod.QrUpd.Params[02].AsInteger := -1; // Ocorrencia banc�ria
          Dmod.QrUpd.Params[03].AsFloat   := QrCNAB240Custas.Value;
          Dmod.QrUpd.Params[04].AsInteger := QrCNAB240Movimento.Value;
          //
          Dmod.QrUpd.Params[05].AsInteger := Controle;
          Dmod.QrUpd.ExecSQL;
        end {else
          Geral.MB_Aviso('N�o h� a��o espec�fica implementada '+
            'para o movimento "'+FormatFloat('00', QrCNAB240Movimento.Value)+' - '+
            UBancos.CNABTipoDeMovimento(1, ecnabRetorno, QrCNAB240Movimento.Value,
            0, False)+'"!');}
      end;
    end;
    QrCNAB240.Next;
  end;
  QrCNAB240.Close;
  QrCNAB240.Open;
  Result := True;
  Screen.Cursor := crDefault;
end;

procedure TFmCNAB240Ret.Liquidacao;
var
  ValQuit, ValJr, Difer: Double;
  ADupPgs, MeuID, Ocorr: Integer;
  Data: String;
begin
  if QrCNAB240Quitado.Value > 1 then
  begin
    Geral.MB_Aviso('Esta duplicata j� foi quitada anteriormente!');
    Exit;
  end;
  if (QrCNAB240MeuLote.Value = 0) and (QrCNAB240Bruto.Value = 0) then
  begin
    Geral.MB_Aviso('A duplicata com o "Meu ID" n� ' +
      Geral.FF0(QrCNAB240Controle.Value) +
      ' n�o pode ser liquidada. Ela n�o foi localizada pelo sistema!');
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    MeuID   := QrCNAB240Controle.Value;
    ValQuit := QrCNAB240ValPago.Value;
    //
    // Cria ocorr�ncia se cliente pagou a menor,
    // porque n�o h� desconto p/ pagto antecipado ou no vencto.
    Difer := QrCNAB240Bruto.Value - QrCNAB240ValPago.Value;
    if Difer > 0 then
    begin
      if (Dmod.QrControleLiqOcoCDi.Value = 1) and (Dmod.QrControleLiqOcoCOc.Value <> 0) then
      begin
        Ocorr := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'Ocorreu', 'Ocorreu', 'Codigo');
        Data  := FormatDateTime(VAR_FORMATDATE, QrCNAB240DataOcor.Value);
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO ocorreu SET AlterWeb=1, ');
        Dmod.QrUpd.SQL.Add('LotesIts=:P0, DataO=:P1, Ocorrencia=:P2, Valor=:P3, ');
        Dmod.QrUpd.SQL.Add('Codigo=:Pa');
        Dmod.QrUpd.Params[00].AsInteger := MeuID;
        Dmod.QrUpd.Params[01].AsString  := Data;
        Dmod.QrUpd.Params[02].AsInteger := Dmod.QrControleLiqOcoCOc.Value;
        Dmod.QrUpd.Params[03].AsFloat   := Difer;
        //
        Dmod.QrUpd.Params[04].AsInteger := Ocorr;
        Dmod.QrUpd.ExecSQL;

        // informar diferen�a no pgto se setado que quer info
        if Dmod.QrControleLiqOcoShw.Value = 1 then
        begin
          QrOB2.Close;
          QrOB2.Params[0].AsInteger := Dmod.QrControleLiqOcoCOc.Value;
          QrOB2.Open;
          //
          Geral.MB_Aviso('Foi criada a ocorr�ncia "' + QrOB2Nome.Value +
            '" no valor de ' + Dmod.QrControleMoeda.Value + ' ' +
            Geral.FFT(Difer, 2, siPositivo) + ' para a duplicata com o "Meu ID" n� ' +
            Geral.FF0(QrCNAB240Controle.Value) + '. ');
        end;
      end;
    end;

    // Calcular taxa de juros caso pago em atraso.
    if Int(QrCNAB240DataOcor.Value) > Int(QrCNAB240DDeposito.Value) then
      ValJr := QrCNAB240ValPago.Value - QrCNAB240Bruto.Value
    else ValJr := 0;
    //
    ADupPgs := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'ADupPgs', 'ADupPgs', 'Controle');
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO aduppgs SET AlterWeb=1, ');
    Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Desco=:P2, Pago=:P3, LotePg=:P4');
    Dmod.QrUpd.SQL.Add(', LotesIts=:Pa, Controle=:Pb');
    Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, QrCNAB240DataOcor.Value);
    Dmod.QrUpd.Params[01].AsFloat   := ValJr;
    Dmod.QrUpd.Params[02].AsFloat   := 0;
    Dmod.QrUpd.Params[03].AsFloat   := ValQuit;
    Dmod.QrUpd.Params[04].AsInteger := 0; // N�o tem lote de origem - n�o pago em border�
    //
    Dmod.QrUpd.Params[05].AsInteger := MeuID;
    Dmod.QrUpd.Params[06].AsInteger := ADupPgs;
    Dmod.QrUpd.ExecSQL;
    //
    CalculaPagamento(MeuID);
    if Geral.MB_Pergunta('Deseja imprimir um recibo com a soma ' +
      'dos recebimentos efetuados?') = ID_YES
    then
      ImprimeRecibodeQuitacao;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCNAB240Ret.CalculaPagamento(LotesIts: Integer);
var
  Sit: Integer;
  Cred, Debi: Double;
  data3, Data4: String;
begin
  QrSumPg.Close;
  QrSumPg.Params[0].AsInteger := LotesIts;
  QrSumPg.Open;
  Cred := QrSumPgPago.Value + QrSumPgDesco.Value;
  Debi := QrSumPgValor.Value + QrSumPgJuros.Value;
  if QrSumPgPago.Value < 0.01 then Sit := 0 else
  begin
    if Cred < (Debi - 0.009) then Sit := 1 else
    begin
      if Cred > (Debi + 0.009) then Sit := 3 else Sit := 2;
    end;
  end;
  // Novo 2007 08 21
  Data4 := Geral.FDT(QrSumPgMaxData.Value, 1);
  if Sit > 1 then Data3 := Data4 else Data3 := '0000-00-00';
  // Fim Novo
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,  ');
  Dmod.QrUpd.SQL.Add('TotalJr=:P0, TotalDs=:P1, TotalPg=:P2, ');
  Dmod.QrUpd.SQL.Add('Data3=:P3, Data4=:P4, Quitado=:P5 WHERE Controle=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := QrSumPgJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumPgDesco.Value;
  Dmod.QrUpd.Params[02].AsFloat   := QrSumPgPago.Value;
  Dmod.QrUpd.Params[03].AsString  := Data3;
  Dmod.QrUpd.Params[04].AsString  := Data4;
  Dmod.QrUpd.Params[05].AsInteger := Sit;
  //
  Dmod.QrUpd.Params[06].AsInteger := LotesIts;
  Dmod.QrUpd.ExecSQL;
end;

procedure TFmCNAB240Ret.ImprimeRecibodeQuitacao;
var
  Pagamento, Texto(*, Liga*): String;
begin
  Pagamento := 'Quita��o de d�bitos gerados at� esta data relativos a';
  Texto := Pagamento + ' duplicata n� ' + QrCNAB240Duplicata.Value+
    ' emitida por '+ QrCNAB240Emitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrCNAB240Vencto.Value);
   //Ocorr�ncia foram rec�m geradoas e n�o pagas
   // Colocar que falta d�bito ?  Parei Aqui
  (*if QrOcorreu.RecordCount > 0 then
  begin
    if QrOcorreu.RecordCount = 1 then Texto := Texto + ' e a ocorr�ncia'
    else Texto := Texto + ' e as ocorr�ncias';
    QrOcorreu.First;
    Liga := ' ';
    while not QrOcorreu.Eof do
    begin
      Texto := Texto + Liga + '"'+QrOcorreuNOMEOCORRENCIA.Value + '"';
      if QrOcorreu.RecNo+1= QrOcorreu.RecordCount then Liga := ' e '
      else liga := ', ';
      QrOcorreu.Next;
    end;
  end;*)
  //
  GOTOy.EmiteRecibo(QrCNAB240Cliente.Value, Dmod.QrMasterDono.Value,
    QrCNAB240ValPago.Value, 0, 0, 'DUX-'+FormatFloat('000000',
    QrCNAB240Controle.Value), Texto, '', '', Date, 0);
end;

procedure TFmCNAB240Ret.BitBtn1Click(Sender: TObject);
begin
  MostraFmDuplicatas(Geral.FF0(QrCNAB240Controle.Value));
end;

procedure TFmCNAB240Ret.BitBtn3Click(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0: MostraFmDuplicatas(GradeC.Cells[09, FActiveRowC]);
    1: MostraFmDuplicatas(GradeB.Cells[04, FActiveRowB]);
  end;
end;

procedure TFmCNAB240Ret.MostraFmDuplicatas(Controle: String);
begin
  Application.CreateForm(TFmDuplicatas2, FmDuplicatas2);
  FmDuplicatas2.CkStatus.Value :=
    Trunc(Power(2, FmDuplicatas2.CkStatus.Items.Count)-1);
  if Geral.IMV(Controle) > 0 then
  begin
    FmDuplicatas2.EdControle.Text := Controle;
    FmDuplicatas2.ReabrirTabelas;
  end;
  FmDuplicatas2.ShowModal;
  FmDuplicatas2.Destroy;
  // Parei aqui
  //informar pagto menor ao marcelo
end;

procedure TFmCNAB240Ret.ReopenOcorreu(Codigo: Integer);
begin
  QrOcorreu.Close;
  QrOcorreu.Params[00].AsInteger := QrCNAB240Controle.Value;
  QrOcorreu.Open;
  //
  (*if Codigo <> 0 then QrOcorreu.Locate('Codigo', Codigo, [])
  else QrOcorreu.Locate('Codigo', FOcorreu, []);*)
end;

procedure TFmCNAB240Ret.QrOcorreuCalcFields(DataSet: TDataSet);
var
  Cliente: Integer;
begin
  if QrOcorreuCLIENTELOTE.Value > 0 then
    Cliente := QrOcorreuCLIENTELOTE.Value else
    Cliente := QrOcorreuCliente.Value;
  //
  QrOcorreuSALDO.Value := QrOcorreuValor.Value + QrOcorreuTaxaV.Value - QrOcorreuPago.Value;
  //
  //if CkAtualiza.Checked then
  //begin
    QrOcorreuATUALIZADO.Value := DMod.ObtemValorAtualizado(
      Cliente, 1, QrOcorreuDataO.Value, (*Date*) QrCNAB240DataOcor.Value, QrOcorreuData3.Value,
      QrOcorreuValor.Value, QrOcorreuTaxaV.Value, 0 (*Desco*),
      QrOcorreuPago.Value, (*QrOcorreuTaxaP.Value*)0, (*False*)True);
    QrOcorreuATZ_TEXTO.Value := Geral.FFT(QrOcorreuATUALIZADO.Value, 2, siNegativo);
  (*end else begin
    QrOcorreuATUALIZADO.Value := QrOcorreuSALDO.Value;
    QrOcorreuATZ_TEXTO.Value := 'n�o calculado';
  end;*)
end;

procedure TFmCNAB240Ret.QrCNAB240AfterScroll(DataSet: TDataSet);
begin
  ReopenOcorreu(0);
end;

procedure TFmCNAB240Ret.Timer2Timer(Sender: TObject);
begin
  Timer2.Enabled := False;

  // Grava dados no banco de dado
  if ExecutaItens then

  // Exclui arquivos carregados
    ExcluiArquivos;
end;

procedure TFmCNAB240Ret.ExcluiArquivos;
var
  Dir, Arq: String;
  i: Integer;
begin
  QrCNAB240Dir.First;
  while not QrCNAB240Dir.Eof do
  begin
    Dir := QrCNAB240DirNome.Value;
    if Dir[Length(Dir)] <> '\' then Dir := Dir + '\';
    for i := 1 to GradeA.RowCount -1 do
    begin
      if Geral.IMV(GradeA.Cells[04, i]) = QrCNAB240DirCodigo.Value then
      begin
        Arq := Dir + GradeA.Cells[01, i];
        if FileExists(Arq) then
          DeleteFile(Arq);
      end;
    end;
    QrCNAB240Dir.Next;
  end;
end;

procedure TFmCNAB240Ret.GradeBSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if ARow <> FActiveRowB then
    FActiveRowB := ARow;
  MensagensRejeicao(GradeB.Cells[11,GradeB.Row]);
end;

procedure TFmCNAB240Ret.GradeCClick(Sender: TObject);
begin
  MensagensRejeicao(GradeC.Cells[20,GradeC.Row]);
end;

procedure TFmCNAB240Ret.GradeCDrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
var
  OldAlign: Integer;
begin
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(GradeC.Canvas.Handle, TA_CENTER);
      GradeC.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        Geral.TFD(GradeC.Cells[Acol, ARow], 3, siPositivo));
      SetTextAlign(GradeC.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(GradeC.Canvas.Handle, TA_CENTER);
      GradeC.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeC.Cells[Acol, ARow]);
      SetTextAlign(GradeC.Canvas.Handle, OldAlign);
  end else if ACol = 03 then begin
      OldAlign := SetTextAlign(GradeC.Canvas.Handle, TA_RIGHT);
      GradeC.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeC.Cells[Acol, ARow]);
      SetTextAlign(GradeC.Canvas.Handle, OldAlign);
  end else if ACol = 04 then begin
      OldAlign := SetTextAlign(GradeC.Canvas.Handle, TA_RIGHT);
      GradeC.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeC.Cells[Acol, ARow]);
      SetTextAlign(GradeC.Canvas.Handle, OldAlign);
  end else if ACol = 05 then begin
      OldAlign := SetTextAlign(GradeC.Canvas.Handle, TA_RIGHT);
      GradeC.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeC.Cells[Acol, ARow]);
      SetTextAlign(GradeC.Canvas.Handle, OldAlign);
  end else if ACol = 09 then begin
      OldAlign := SetTextAlign(GradeC.Canvas.Handle, TA_RIGHT);
      GradeC.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeC.Cells[Acol, ARow]);
      SetTextAlign(GradeC.Canvas.Handle, OldAlign);
  end else if ACol = 11 then begin
      OldAlign := SetTextAlign(GradeC.Canvas.Handle, TA_RIGHT);
      GradeC.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeC.Cells[Acol, ARow]);
      SetTextAlign(GradeC.Canvas.Handle, OldAlign);
  end else if ACol = 12 then begin
      OldAlign := SetTextAlign(GradeC.Canvas.Handle, TA_CENTER);
      GradeC.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeC.Cells[Acol, ARow]);
      SetTextAlign(GradeC.Canvas.Handle, OldAlign);
  end else if ACol = 14 then begin
      OldAlign := SetTextAlign(GradeC.Canvas.Handle, TA_CENTER);
      GradeC.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeC.Cells[Acol, ARow]);
      SetTextAlign(GradeC.Canvas.Handle, OldAlign);
  end else if ACol = 15 then begin
      OldAlign := SetTextAlign(GradeC.Canvas.Handle, TA_CENTER);
      GradeC.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeC.Cells[Acol, ARow]);
      SetTextAlign(GradeC.Canvas.Handle, OldAlign);
  end else if ACol = 16 then begin
      OldAlign := SetTextAlign(GradeC.Canvas.Handle, TA_CENTER);
      GradeC.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        GradeC.Cells[Acol, ARow]);
      SetTextAlign(GradeC.Canvas.Handle, OldAlign);
  end else if ACol = 17 then begin
      OldAlign := SetTextAlign(GradeC.Canvas.Handle, TA_RIGHT);
      GradeC.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeC.Cells[Acol, ARow]);
      SetTextAlign(GradeC.Canvas.Handle, OldAlign);
  end else if ACol = 18 then begin
      OldAlign := SetTextAlign(GradeC.Canvas.Handle, TA_RIGHT);
      GradeC.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeC.Cells[Acol, ARow]);
      SetTextAlign(GradeC.Canvas.Handle, OldAlign);
  end else if ACol = 19 then begin
      OldAlign := SetTextAlign(GradeC.Canvas.Handle, TA_RIGHT);
      GradeC.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        GradeC.Cells[Acol, ARow]);
      SetTextAlign(GradeC.Canvas.Handle, OldAlign);
  end
end;

procedure TFmCNAB240Ret.GradeCKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MensagensRejeicao(GradeC.Cells[20,GradeC.Row]);
end;

procedure TFmCNAB240Ret.GradeCSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  if ARow <> FActiveRowC then
    FActiveRowC := ARow;
  MensagensRejeicao(GradeC.Cells[20,GradeC.Row]);
end;

procedure TFmCNAB240Ret.MensagensRejeicao(IRTCLB: String);
  function MensagemX(Codigo: Integer): String;
  begin
    case Codigo of
      01: Result := 'Codigo do banco invalido.';
      02: Result := 'Codigo do registro detalhe invalido.';
      03: Result := 'Codigo do segmento invalido.';
      04: Result := 'Codigo do movimento nao permitido para carteira.';
      05: Result := 'Codigo de movimento invalido.';
      06: Result := 'Tipo/nUmero de inscricao do cedente Invalidos.';
      07: Result := 'Agencia/Conta/DV invalido.';
      08: Result := 'Nosso nUmero invalido.';
      09: Result := 'Nosso nUmero duplicado.';
      10: Result := 'Carteira invalida.';
      11: Result := 'Forma de cadastramento do titulo invalido';
      12: Result := 'Tipo de documento invalido.';
      13: Result := 'Identificacao da emissao do bloqueto invalida.';
      14: Result := 'Identificacao da distribuicao do bloqueto invalida.';
      15: Result := 'Caracteristicas da cobranca incompativeis';
      16: Result := 'Data de vencimento invalida.';
      17: Result := 'Data de vencimento anterior a data de emissao.';
      18: Result := 'Vencimento fora do prazo de operacao.';
      19: Result := 'Titulo a cargo de Bancos Correspondentes com vencimento inferior XX dias';
      20: Result := 'Valor do titulo invalido.';
      21: Result := 'Especie do titulo invalida.';
      22: Result := 'Especie nao permitida para a carteira.';
      23: Result := 'Aceite invalido.';
      24: Result := 'Data da emissao invalida.';
      25: Result := 'Data da emissao posterior a data';
      26: Result := 'Codigo de juros de mora invalido.';
      27: Result := 'Valor/Taxa de juros de mora invalido.';
      28: Result := 'Codigo do desconto invalido.';
      29: Result := 'Valor do desconto maior ou igual ao valor do titulo.';
      30: Result := 'Desconto a conceder nao confere.';
      31: Result := 'Concessao de desconto - ja existe desconto anterior.';
      32: Result := 'Valor do IOF invalido.';
      33: Result := 'Valor do abatimento invalido.';
      34: Result := 'Valor do abatimento maior ou igual ao valor do titulo.';
      35: Result := 'Abatimento a conceder nao confere.';
      36: Result := 'Concessao de abatimento - ja existe abatimento anterior.';
      37: Result := 'Codigo para protesto invalido.';
      38: Result := 'Prazo para protesto invalido.';
      39: Result := 'Pedido de protesto nao permitido para o titulo.';
      40: Result := 'Titulo com ordem de protesto emitida.';
      41: Result := 'Pedido de cancelamento/sustacao para titulos sem instrucao de protesto.';
      42: Result := 'Codigo para baixa/devolucao invalido.';
      43: Result := 'Prazo para baixa/devolucao invalido.';
      44: Result := 'Codigo da moeda invalido.';
      45: Result := 'Nome do sacado nao informado.';
      46: Result := 'Tipo/nUmero de inscricao do sacado invalidos.';
      47: Result := 'Endereco do sacado nao informado.';
      48: Result := 'CEP invalido.';
      49: Result := 'CEP sem praca de cobranca /nao localizado';
      50: Result := 'CEP referente a um Banco Correspondente.';
      51: Result := 'CEP incompativel com a unidade da federaCcao';
      52: Result := 'Unidade da federacao invalida.';
      53: Result := 'Tipo/nUmero de inscricao do sacador/avalista invalidos.';
      54: Result := 'Sacador/Avalista nao informado.';
      55: Result := 'Nosso nUmero no Banco Correspondente nao informado.';
      56: Result := 'Codigo do Banco Correspondente nao informado.';
      57: Result := 'Codigo da multa invalido.';
      58: Result := 'Data da multa invalida.';
      59: Result := 'Valor/Percentual da multa invalido.';
      60: Result := 'Movimento para titulo nao cadastrado.';
      61: Result := 'Alteracao da agencia cobradora/dv invalida.';
      62: Result := 'Tipo de impressao invalido.';
      63: Result := 'Entrada para titulo ja cadastrado.';
      64: Result := 'Numero da linha invalido.';
      65: Result := 'Codigo do banco para debito invalido.';
      66: Result := 'Agencia/conta/DV para debito invalido.';
      67: Result := 'Dados para debito incompativel com a identificacao da emissao do bloqueto.';
      88: Result := 'Arquivo em duplicidade';
      99: Result := 'Contrato inexistente.';
      else Result := '';
    end;
  end;
  var
    I: Integer;
begin
  MeRejeicoes.Lines.Clear;
  //
  for I := 1 to 5 do
    MeRejeicoes.Lines.Add(MensagemX(Geral.IMV(Copy(IRTCLB, ((I-1)*2)-1, 2))));

{
!                 !Os c�digos abaixo s�o exclusivos do bloqueto   !
!                 !por e-mail                                     !
!                 !46 - Documento de identifica��o do sacado inv�-!
!                 !     lido - bloqueto ser� impresso.            !
!                 !47 - Endere�o e-mail do sacado inv�lido - Blo- !
!                 !     queto ser� impresso.                      !
!                 !14 - Sacado optante por Cobran�a Eletr�nica -  !
!                 !     Bloqueto ser� eletr�nico                  !
!                 !15 - Tipo de conv�nio n�o permite envio de     !
!                 !     e-mail - Bloqueto ser� impresso           !
!                 !15 - Modalidade de cobran�a inv�lida para en-  !
!                 !     vio de e-mail - bloqueto ser� impresso.   !
!                 !13 - Cedente n�o autorizado a enviar e-mail -  !
!                 !     Bloqueto ser� impresso                    !
*-----------------------------------------------------------------*
}
end;

end.

