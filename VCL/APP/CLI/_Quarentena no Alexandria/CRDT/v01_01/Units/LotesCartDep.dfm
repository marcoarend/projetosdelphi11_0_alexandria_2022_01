object FmLotesCartDep: TFmLotesCartDep
  Left = 339
  Top = 185
  Caption = 'Carteira de dep'#243'sito'
  ClientHeight = 157
  ClientWidth = 513
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 109
    Width = 513
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 401
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 513
    Height = 48
    Align = alTop
    Caption = 'Carteira de dep'#243'sito'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 511
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 517
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 513
    Height = 61
    Align = alClient
    TabOrder = 0
    object LaCartDep1: TLabel
      Left = 12
      Top = 8
      Width = 115
      Height = 13
      Caption = 'Carteira de recebimento:'
    end
    object EdCartDep: TdmkEditCB
      Left = 12
      Top = 23
      Width = 45
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCartDep
    end
    object CBCartDep: TdmkDBLookupComboBox
      Left = 60
      Top = 23
      Width = 449
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCarteiras
      TabOrder = 1
      dmkEditCB = EdCartDep
      UpdType = utYes
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Banco1, Agencia1, Conta1'
      'FROM carteiras'
      'ORDER BY Nome')
    Left = 14
    Top = 10
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
      DisplayFormat = '000'
    end
    object QrCarteirasAgencia1: TIntegerField
      FieldName = 'Agencia1'
      DisplayFormat = '0000'
    end
    object QrCarteirasConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 42
    Top = 10
  end
end
