unit ChequesGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Math, DBCtrls, Db, mySQLDbTables,
  Grids, DBGrids, Mask, ComCtrls, Menus, Variants, dmkGeral, UnDmkProcFunc,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkCheckGroup, UnDmkEnums;

type
  TFmChequesGer = class(TForm)
    PainelDados: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    PainelData: TPanel;
    QrPesq: TmySQLQuery;
    QrOcorreu: TmySQLQuery;
    DsOcorreu: TDataSource;
    QrOcorreuCodigo: TIntegerField;
    QrOcorreuLotesIts: TIntegerField;
    QrOcorreuDataO: TDateField;
    QrOcorreuOcorrencia: TIntegerField;
    QrOcorreuValor: TFloatField;
    QrOcorreuLoteQuit: TIntegerField;
    QrOcorreuLk: TIntegerField;
    QrOcorreuDataCad: TDateField;
    QrOcorreuDataAlt: TDateField;
    QrOcorreuUserCad: TIntegerField;
    QrOcorreuUserAlt: TIntegerField;
    QrOcorreuNOMEOCORRENCIA: TWideStringField;
    PainelPesq: TPanel;
    Panel3: TPanel;
    DsPesq: TDataSource;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label7: TLabel;
    DBEdit6: TDBEdit;
    Panel4: TPanel;
    Label8: TLabel;
    DBEdit7: TDBEdit;
    QrPesqCNPJ_TXT: TWideStringField;
    QrPesqTAXA_COMPRA: TFloatField;
    PainelBtns: TPanel;
    QrOcorBank: TmySQLQuery;
    DsOcorBank: TDataSource;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    QrOcorBankLk: TIntegerField;
    QrOcorBankDataCad: TDateField;
    QrOcorBankDataAlt: TDateField;
    QrOcorBankUserCad: TIntegerField;
    QrOcorBankUserAlt: TIntegerField;
    Label38: TLabel;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    Label39: TLabel;
    Label40: TLabel;
    EdConta: TdmkEdit;
    EdCheque: TdmkEdit;
    Label41: TLabel;
    Label36: TLabel;
    EdBanda: TdmkEdit;
    Label75: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrBanco: TmySQLQuery;
    QrBancoCodigo: TIntegerField;
    QrBancoNome: TWideStringField;
    QrBancoLk: TIntegerField;
    QrBancoDataCad: TDateField;
    QrBancoDataAlt: TDateField;
    QrBancoUserCad: TIntegerField;
    QrBancoUserAlt: TIntegerField;
    QrBancoSite: TWideStringField;
    DsBanco: TDataSource;
    DBEdit19: TDBEdit;
    QrPesqControle: TIntegerField;
    QrPesqEmissao: TDateField;
    QrPesqTxaCompra: TFloatField;
    QrPesqDCompra: TDateField;
    QrPesqValor: TFloatField;
    QrPesqDesco: TFloatField;
    QrPesqVencto: TDateField;
    QrOcorreuTaxaP: TFloatField;
    QrOcorreuTaxaV: TFloatField;
    QrOcorreuPago: TFloatField;
    QrOcorreuDataP: TDateField;
    QrOcorreuTaxaB: TFloatField;
    Panel000: TPanel;
    PainelCtrl: TPanel;
    PainelBtsE: TPanel;
    BtChequeOcorr: TBitBtn;
    BtChequeDev: TBitBtn;
    BtStatus: TBitBtn;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    PainelConf: TPanel;
    BtConfirma5: TBitBtn;
    BtDesiste5: TBitBtn;
    QrPesqCheque: TIntegerField;
    QrOcorBankBase: TFloatField;
    Label2: TLabel;
    Label9: TLabel;
    EdCPF_1: TdmkEdit;
    QrPesqNOMESTATUS: TWideStringField;
    QrPesqQuitado: TIntegerField;
    Label10: TLabel;
    DBEdit1: TDBEdit;
    QrPesqNOMECOLIGADO: TWideStringField;
    Label11: TLabel;
    DBEdit8: TDBEdit;
    QrPesqSTATUSDEV: TSmallintField;
    QrPesqDevolucao: TIntegerField;
    BtRefresh: TBitBtn;
    PMOcorreu: TPopupMenu;
    Incluiocorrncia1: TMenuItem;
    Alteraocorrncia1: TMenuItem;
    Excluiocorrncia1: TMenuItem;
    QrLocPr: TmySQLQuery;
    QrLocPrCodigo: TIntegerField;
    QrLocPrControle: TIntegerField;
    QrLocPrData1: TDateField;
    QrLocPrData2: TDateField;
    QrLocPrData3: TDateField;
    QrLocPrOcorrencia: TIntegerField;
    QrLocPrLk: TIntegerField;
    QrLocPrDataCad: TDateField;
    QrLocPrDataAlt: TDateField;
    QrLocPrUserCad: TIntegerField;
    QrLocPrUserAlt: TIntegerField;
    QrPesqCliente: TIntegerField;
    QrLotesPrr: TmySQLQuery;
    DsLotesPrr: TDataSource;
    QrLotesPrrCodigo: TIntegerField;
    QrLotesPrrControle: TIntegerField;
    QrLotesPrrData1: TDateField;
    QrLotesPrrData2: TDateField;
    QrLotesPrrData3: TDateField;
    QrLotesPrrOcorrencia: TIntegerField;
    QrLotesPrrLk: TIntegerField;
    QrLotesPrrDataCad: TDateField;
    QrLotesPrrDataAlt: TDateField;
    QrLotesPrrUserCad: TIntegerField;
    QrLotesPrrUserAlt: TIntegerField;
    QrPesqNOMECLIENTE: TWideStringField;
    QrPesqProrrVz: TIntegerField;
    QrPesqProrrDd: TIntegerField;
    QrLotesPrrDIAS_1_3: TIntegerField;
    QrLotesPrrDIAS_2_3: TIntegerField;
    PMStatus: TPopupMenu;
    Incluiprorrogao1: TMenuItem;
    ExcluiProrrogao1: TMenuItem;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrAlinIts: TmySQLQuery;
    QrAlinItsCodigo: TIntegerField;
    QrAlinItsAlinea1: TIntegerField;
    QrAlinItsAlinea2: TIntegerField;
    QrAlinItsData1: TDateField;
    QrAlinItsData2: TDateField;
    QrAlinItsData3: TDateField;
    QrAlinItsCliente: TIntegerField;
    QrAlinItsBanco: TIntegerField;
    QrAlinItsAgencia: TIntegerField;
    QrAlinItsConta: TWideStringField;
    QrAlinItsCheque: TIntegerField;
    QrAlinItsCPF: TWideStringField;
    QrAlinItsValor: TFloatField;
    QrAlinItsTaxas: TFloatField;
    QrAlinItsLk: TIntegerField;
    QrAlinItsDataCad: TDateField;
    QrAlinItsDataAlt: TDateField;
    QrAlinItsUserCad: TIntegerField;
    QrAlinItsUserAlt: TIntegerField;
    QrAlinItsEmitente: TWideStringField;
    QrAlinItsCPF_TXT: TWideStringField;
    QrAlinItsDATA1_TXT: TWideStringField;
    QrAlinItsDATA2_TXT: TWideStringField;
    QrAlinItsDATA3_TXT: TWideStringField;
    QrAlinItsChequeOrigem: TIntegerField;
    QrAlinItsStatus: TSmallintField;
    QrAlinItsValPago: TFloatField;
    QrAlinItsMulta: TFloatField;
    QrAlinItsJurosP: TFloatField;
    QrAlinItsJurosV: TFloatField;
    QrAlinItsDesconto: TFloatField;
    DsAlinIts: TDataSource;
    QrAlinPgs: TmySQLQuery;
    QrAlinPgsCodigo: TIntegerField;
    QrAlinPgsAlinIts: TIntegerField;
    QrAlinPgsData: TDateField;
    QrAlinPgsLk: TIntegerField;
    QrAlinPgsDataCad: TDateField;
    QrAlinPgsDataAlt: TDateField;
    QrAlinPgsUserCad: TIntegerField;
    QrAlinPgsUserAlt: TIntegerField;
    QrAlinPgsJuros: TFloatField;
    QrAlinPgsPago: TFloatField;
    QrAlinPgsLotePg: TIntegerField;
    DsAlinPgs: TDataSource;
    PMDevolucao: TPopupMenu;
    QrPesqEmitente: TWideStringField;
    GradeItens: TDBGrid;
    PainelMuda: TPanel;
    PainelOcor: TPanel;
    Panel5: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    EdOcorrencia: TdmkEditCB;
    CBOcorrencia: TdmkDBLookupComboBox;
    TPDataO: TDateTimePicker;
    EdValor: TdmkEdit;
    PainelPror: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label29: TLabel;
    Label27: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    TPDataN: TDateTimePicker;
    EdTaxaPror: TdmkEdit;
    EdJurosPeriodoN: TdmkEdit;
    EdJurosPr: TdmkEdit;
    EdValorBase: TdmkEdit;
    TPDataI: TDateTimePicker;
    PainelDevo: TPanel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    EdAlinea1: TdmkEditCB;
    EdAlinea2: TdmkEditCB;
    CBAlinea2: TdmkDBLookupComboBox;
    CBAlinea1: TdmkDBLookupComboBox;
    TPData1_1: TDateTimePicker;
    TPData1_2: TDateTimePicker;
    EdMulta1: TdmkEdit;
    EdJuros1: TdmkEdit;
    EdTaxas1: TdmkEdit;
    QrAlineas: TmySQLQuery;
    QrAlineasCodigo: TIntegerField;
    QrAlineasNome: TWideStringField;
    QrAlineasLk: TIntegerField;
    QrAlineasDataCad: TDateField;
    QrAlineasDataAlt: TDateField;
    QrAlineasUserCad: TIntegerField;
    QrAlineasUserAlt: TIntegerField;
    DsAlinea1: TDataSource;
    DsAlinea2: TDataSource;
    Alteradevoluo1: TMenuItem;
    N1: TMenuItem;
    PainelPgDv: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Label33: TLabel;
    Label34: TLabel;
    Label35: TLabel;
    Label37: TLabel;
    TPDataBase2: TDateTimePicker;
    EdValorBase2: TdmkEdit;
    EdJurosBase2: TdmkEdit;
    EdJurosPeriodo2: TdmkEdit;
    QrSumPg: TmySQLQuery;
    QrSumPgJuros: TFloatField;
    QrSumPgPago: TFloatField;
    IncluiPagamento1: TMenuItem;
    QrLocPg: TmySQLQuery;
    QrLocPgCodigo: TIntegerField;
    QrLocPgAlinIts: TIntegerField;
    QrLocPgData: TDateField;
    QrLocPgLk: TIntegerField;
    QrLocPgDataCad: TDateField;
    QrLocPgDataAlt: TDateField;
    QrLocPgUserCad: TIntegerField;
    QrLocPgUserAlt: TIntegerField;
    QrLocPgJuros: TFloatField;
    QrLocPgPago: TFloatField;
    ExcluiPagamento1: TMenuItem;
    QrLast: TmySQLQuery;
    QrLastData: TDateField;
    Desfazdevoluo1: TMenuItem;
    EdCMC_7_1: TEdit;
    N2: TMenuItem;
    RecibodoPagamento1: TMenuItem;
    QrBco1: TmySQLQuery;
    QrBco1Nome: TWideStringField;
    QrAlinItsNOMEALINEA1: TWideStringField;
    QrAlinItsNOMEALINEA2: TWideStringField;
    LaColigado: TLabel;
    EdColigado: TdmkEditCB;
    CBColigado: TdmkDBLookupComboBox;
    CkRepassado: TdmkCheckGroup;
    DsColigado: TDataSource;
    QrColigado: TmySQLQuery;
    QrColigadoCodigo: TIntegerField;
    QrColigadoNOMECOLIGADO: TWideStringField;
    Splitter1: TSplitter;
    TabSheet3: TTabSheet;
    GradeDev: TDBGrid;
    DBGrid4: TDBGrid;
    DBGrid2: TDBGrid;
    QrOcorrPg: TmySQLQuery;
    QrOcorrPgCodigo: TIntegerField;
    QrOcorrPgOcorreu: TIntegerField;
    QrOcorrPgData: TDateField;
    QrOcorrPgJuros: TFloatField;
    QrOcorrPgPago: TFloatField;
    QrOcorrPgLotePg: TIntegerField;
    QrOcorrPgLk: TIntegerField;
    QrOcorrPgDataCad: TDateField;
    QrOcorrPgDataAlt: TDateField;
    QrOcorrPgUserCad: TIntegerField;
    QrOcorrPgUserAlt: TIntegerField;
    DsOcorrPg: TDataSource;
    DBGrid3: TDBGrid;
    Panel2: TPanel;
    DBGrid1: TDBGrid;
    PainelOcorPg: TPanel;
    Label86: TLabel;
    TPDataBase6: TDateTimePicker;
    EdValorBase6: TdmkEdit;
    Label87: TLabel;
    Label88: TLabel;
    EdJurosBase6: TdmkEdit;
    EdJurosPeriodo6: TdmkEdit;
    Label89: TLabel;
    TPPagto6: TDateTimePicker;
    Label82: TLabel;
    EdJuros6: TdmkEdit;
    Label83: TLabel;
    Label85: TLabel;
    EdAPagar6: TdmkEdit;
    EdPago6: TdmkEdit;
    Label84: TLabel;
    N3: TMenuItem;
    IncluiPagamento2: TMenuItem;
    Excluipagamento2: TMenuItem;
    PainelOcorConf: TPanel;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    QrLastOcor: TmySQLQuery;
    QrLastOcorData: TDateField;
    QrLastOcorCodigo: TIntegerField;
    QrSumOc: TmySQLQuery;
    QrSumOcJuros: TFloatField;
    QrSumOcPago: TFloatField;
    QrLocOc: TmySQLQuery;
    QrLocOcCodigo: TIntegerField;
    QrLocOcOcorreu: TIntegerField;
    QrLocOcData: TDateField;
    QrLocOcJuros: TFloatField;
    QrLocOcPago: TFloatField;
    QrLocOcLotePg: TIntegerField;
    QrLocOcLk: TIntegerField;
    QrLocOcDataCad: TDateField;
    QrLocOcDataAlt: TDateField;
    QrLocOcUserCad: TIntegerField;
    QrLocOcUserAlt: TIntegerField;
    QrOcorreuCLIENTELOTE: TIntegerField;
    QrOcorreuData3: TDateField;
    QrOcorreuStatus: TSmallintField;
    QrOcorreuCliente: TIntegerField;
    QrOcorreuSALDO: TFloatField;
    QrOcorreuATUALIZADO: TFloatField;
    N4: TMenuItem;
    Recibodopagamento2: TMenuItem;
    QrAlinItsSALDO: TFloatField;
    QrAlinItsATUAL: TFloatField;
    BtQuitacao: TBitBtn;
    PMQuitacao: TPopupMenu;
    Quitadocumento1: TMenuItem;
    Imprimerecibodequitao1: TMenuItem;
    QrPesqValQuit: TFloatField;
    BitBtn1: TBitBtn;
    QrPesqCodigo: TIntegerField;
    Label6: TLabel;
    Label42: TLabel;
    EdAPCD: TdmkEditCB;
    CBAPCD: TdmkDBLookupComboBox;
    QrAPCD: TmySQLQuery;
    DsAPCD: TDataSource;
    QrAPCDCodigo: TIntegerField;
    QrAPCDNome: TWideStringField;
    QrAPCDDeposita: TSmallintField;
    EdBanda2: TdmkEdit;
    Label43: TLabel;
    EdRegiaoCompe2: TdmkEdit;
    Label44: TLabel;
    Label45: TLabel;
    EdBanco2: TdmkEdit;
    EdAgencia2: TdmkEdit;
    Label46: TLabel;
    Label47: TLabel;
    EdConta2: TdmkEdit;
    EdCheque2: TdmkEdit;
    Label48: TLabel;
    EdCPF_2: TdmkEdit;
    Label49: TLabel;
    EdEmitente2: TdmkEdit;
    Label50: TLabel;
    QrSCB: TmySQLQuery;
    QrSCBSCB: TIntegerField;
    TPData2: TDateTimePicker;
    Label12: TLabel;
    Label28: TLabel;
    TPPagto2: TDateTimePicker;
    EdJuros2: TdmkEdit;
    Label30: TLabel;
    Label32: TLabel;
    EdAPagar: TdmkEdit;
    EdPago2: TdmkEdit;
    Label31: TLabel;
    GroupBox1: TGroupBox;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label96: TLabel;
    Label98: TLabel;
    Label99: TLabel;
    Label91: TLabel;
    Label54: TLabel;
    EdDR: TdmkEdit;
    EdDT: TdmkEdit;
    EdRT: TdmkEdit;
    EdVT: TdmkEdit;
    EdST: TdmkEdit;
    EdCT: TdmkEdit;
    EdDV: TdmkEdit;
    EdCR: TdmkEdit;
    EdCV: TdmkEdit;
    EdOA: TdmkEdit;
    EdTT: TdmkEdit;
    LaTipo: TLabel;
    EdCMC_7_2: TEdit;
    QrAPCDExigeCMC7: TSmallintField;
    QrBanco2: TmySQLQuery;
    DsBanco2: TDataSource;
    QrBanco2Nome: TWideStringField;
    QrBanco2DVCC: TSmallintField;
    QrSacRiscoTC: TmySQLQuery;
    QrSacRiscoTCValor: TFloatField;
    DsSacRiscoTC: TDataSource;
    QrSacRiscoC: TmySQLQuery;
    QrSacRiscoCBanco: TIntegerField;
    QrSacRiscoCAgencia: TIntegerField;
    QrSacRiscoCConta: TWideStringField;
    QrSacRiscoCCheque: TIntegerField;
    QrSacRiscoCValor: TFloatField;
    QrSacRiscoCDCompra: TDateField;
    QrSacRiscoCDDeposito: TDateField;
    QrSacRiscoCEmitente: TWideStringField;
    QrSacRiscoCCPF: TWideStringField;
    QrSacRiscoCCPF_TXT: TWideStringField;
    DsSacRiscoC: TDataSource;
    QrSacDOpen: TmySQLQuery;
    QrSacDOpenControle: TIntegerField;
    QrSacDOpenDuplicata: TWideStringField;
    QrSacDOpenDCompra: TDateField;
    QrSacDOpenValor: TFloatField;
    QrSacDOpenDDeposito: TDateField;
    QrSacDOpenEmitente: TWideStringField;
    QrSacDOpenCPF: TWideStringField;
    QrSacDOpenCliente: TIntegerField;
    QrSacDOpenSTATUS: TWideStringField;
    QrSacDOpenQuitado: TIntegerField;
    QrSacDOpenTotalJr: TFloatField;
    QrSacDOpenTotalDs: TFloatField;
    QrSacDOpenTotalPg: TFloatField;
    QrSacDOpenSALDO_DESATUALIZ: TFloatField;
    QrSacDOpenSALDO_ATUALIZADO: TFloatField;
    QrSacDOpenNOMESTATUS: TWideStringField;
    QrSacDOpenVencto: TDateField;
    QrSacDOpenDDCALCJURO: TIntegerField;
    QrSacDOpenData3: TDateField;
    QrSacDOpenRepassado: TSmallintField;
    DsSacDOpen: TDataSource;
    QrSacCHDevA: TmySQLQuery;
    QrSacCHDevADATA1_TXT: TWideStringField;
    QrSacCHDevADATA2_TXT: TWideStringField;
    QrSacCHDevADATA3_TXT: TWideStringField;
    QrSacCHDevACPF_TXT: TWideStringField;
    QrSacCHDevANOMECLIENTE: TWideStringField;
    QrSacCHDevACodigo: TIntegerField;
    QrSacCHDevAAlinea1: TIntegerField;
    QrSacCHDevAAlinea2: TIntegerField;
    QrSacCHDevAData1: TDateField;
    QrSacCHDevAData2: TDateField;
    QrSacCHDevAData3: TDateField;
    QrSacCHDevACliente: TIntegerField;
    QrSacCHDevABanco: TIntegerField;
    QrSacCHDevAAgencia: TIntegerField;
    QrSacCHDevAConta: TWideStringField;
    QrSacCHDevACheque: TIntegerField;
    QrSacCHDevACPF: TWideStringField;
    QrSacCHDevAValor: TFloatField;
    QrSacCHDevATaxas: TFloatField;
    QrSacCHDevALk: TIntegerField;
    QrSacCHDevADataCad: TDateField;
    QrSacCHDevADataAlt: TDateField;
    QrSacCHDevAUserCad: TIntegerField;
    QrSacCHDevAUserAlt: TIntegerField;
    QrSacCHDevAEmitente: TWideStringField;
    QrSacCHDevAChequeOrigem: TIntegerField;
    QrSacCHDevAStatus: TSmallintField;
    QrSacCHDevAValPago: TFloatField;
    QrSacCHDevAMulta: TFloatField;
    QrSacCHDevAJurosP: TFloatField;
    QrSacCHDevAJurosV: TFloatField;
    QrSacCHDevADesconto: TFloatField;
    QrSacCHDevASALDO: TFloatField;
    QrSacCHDevAATUAL: TFloatField;
    DsSacCHDevA: TDataSource;
    QrSacOcorA: TmySQLQuery;
    QrSacOcorATipo: TSmallintField;
    QrSacOcorATIPODOC: TWideStringField;
    QrSacOcorANOMEOCORRENCIA: TWideStringField;
    QrSacOcorACodigo: TIntegerField;
    QrSacOcorALotesIts: TIntegerField;
    QrSacOcorADataO: TDateField;
    QrSacOcorAOcorrencia: TIntegerField;
    QrSacOcorAValor: TFloatField;
    QrSacOcorALoteQuit: TIntegerField;
    QrSacOcorALk: TIntegerField;
    QrSacOcorADataCad: TDateField;
    QrSacOcorADataAlt: TDateField;
    QrSacOcorAUserCad: TIntegerField;
    QrSacOcorAUserAlt: TIntegerField;
    QrSacOcorATaxaP: TFloatField;
    QrSacOcorATaxaV: TFloatField;
    QrSacOcorAPago: TFloatField;
    QrSacOcorADataP: TDateField;
    QrSacOcorATaxaB: TFloatField;
    QrSacOcorAData3: TDateField;
    QrSacOcorAStatus: TSmallintField;
    QrSacOcorASALDO: TFloatField;
    QrSacOcorAATUALIZADO: TFloatField;
    QrSacOcorACliente: TIntegerField;
    QrSacOcorACLIENTELOTE: TIntegerField;
    DsSacOcorA: TDataSource;
    EdRealCC: TdmkEdit;
    EdControle: TdmkEdit;
    Label55: TLabel;
    QrAlinPgsVencto: TDateField;
    QrAlinPgsDDeposito: TDateField;
    QrAlinPgsBanco: TIntegerField;
    QrAlinPgsAgencia: TIntegerField;
    QrAlinPgsConta: TWideStringField;
    QrAlinPgsCheque: TIntegerField;
    QrAlinPgsCPF: TWideStringField;
    QrAlinPgsEmitente: TWideStringField;
    QrAlinPgsAPCD: TIntegerField;
    QrAlinPgsDataCh: TDateField;
    QrCli: TmySQLQuery;
    Panel7: TPanel;
    BtReciboAntecip: TBitBtn;
    QrAntePror: TmySQLQuery;
    QrAnteProrData1: TDateField;
    QrAnteProrData2: TDateField;
    QrAnteProrData3: TDateField;
    QrAnteProrCheque: TIntegerField;
    QrAnteProrBanco: TIntegerField;
    QrAnteProrAgencia: TIntegerField;
    QrAnteProrConta: TWideStringField;
    QrAnteProrEmitente: TWideStringField;
    QrAnteProrVALORCHEQUE: TFloatField;
    QrAnteProrVALORRESSARCIDO: TFloatField;
    QrAnteProrControle: TIntegerField;
    PMReciboAntecip: TPopupMenu;
    Meuparaocliente1: TMenuItem;
    Clienteparamim1: TMenuItem;
    QrAnteProrITEMLOTE: TIntegerField;
    QrAnteProrVlrCompra: TFloatField;
    IncluiQuitao1: TMenuItem;
    QrPesqDDeposito: TDateField;
    EdEmitente1: TdmkEdit;
    QrPesqCPF: TWideStringField;
    QrPesqRisco: TFloatField;
    QrPesqNome: TWideStringField;
    QrPesqNaoDeposita: TSmallintField;
    Panel10: TPanel;
    BtHistorico: TBitBtn;
    PMHistorico: TPopupMenu;
    Clienteselecionado1: TMenuItem;
    Emitentesacadoselecionado1: TMenuItem;
    Ambos1: TMenuItem;
    EdDesco2: TdmkEdit;
    Label1: TLabel;
    QrSumPgDesco: TFloatField;
    QrAlinItsPgDesc: TFloatField;
    QrAlinPgsDesco: TFloatField;
    Label56: TLabel;
    EdTaxaP: TdmkEdit;
    N5: TMenuItem;
    Clientepesquisado1: TMenuItem;
    Emitentesacadopesquisado1: TMenuItem;
    Ambos2: TMenuItem;
    QrPesqRepCli: TIntegerField;
    QrEmitCPF: TmySQLQuery;
    QrEmitBAC: TmySQLQuery;
    QrEmitBACBAC: TWideStringField;
    QrEmitBACCPF: TWideStringField;
    QrPesqBanco: TIntegerField;
    QrPesqAgencia: TIntegerField;
    QrPesqConta: TWideStringField;
    QrEmitCPFCPF: TWideStringField;
    QrEmitCPFNome: TWideStringField;
    QrEmitCPFLimite: TFloatField;
    QrEmitCPFLastAtz: TDateField;
    QrEmitCPFAcumCHComV: TFloatField;
    QrEmitCPFAcumCHComQ: TIntegerField;
    QrEmitCPFAcumCHDevV: TFloatField;
    QrEmitCPFAcumCHDevQ: TIntegerField;
    TabSheet4: TTabSheet;
    Memo1: TMemo;
    CkAtualiza: TCheckBox;
    CkMorto: TCheckBox;
    QrSacCHDevAPgDesc: TFloatField;
    QrSacCHDevADDeposito: TDateField;
    QrSacCHDevAVencto: TDateField;
    QrSacCHDevALoteOrigem: TIntegerField;
    BtDesDep: TBitBtn;
    QrPesqDepositado: TSmallintField;
    QrPesqObsGerais: TWideStringField;
    DBEdit5: TDBEdit;
    QrPesqTEL1CLI: TWideStringField;
    QrPesqTEL1CLI_TXT: TWideStringField;
    Label57: TLabel;
    QrSoma: TmySQLQuery;
    QrSomaItens: TLargeintField;
    QrSomaValor: TFloatField;
    DsSoma: TDataSource;
    EdSomaI: TdmkEdit;
    EdSomaV: TdmkEdit;
    N6: TMenuItem;
    N0ForastatusAutomtico1: TMenuItem;
    N1Forastatus1: TMenuItem;
    N2ForastatusBaixado1: TMenuItem;
    N3ForastatusMoroso1: TMenuItem;
    SpeedButton1: TSpeedButton;
    Query1: TmySQLQuery;
    Query1Controle: TIntegerField;
    CkStatus: TdmkCheckGroup;
    CkProrrogado: TdmkCheckGroup;
    CkCalcFields: TCheckBox;
    PMDiario: TPopupMenu;
    Adicionareventoaodiario1: TMenuItem;
    Gerenciardirio1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrPesqCalcFields(DataSet: TDataSet);
    procedure BtDesiste5Click(Sender: TObject);
    procedure BtConfirma5Click(Sender: TObject);
    procedure BtChequeOcorrClick(Sender: TObject);
    procedure EdOcorrenciaExit(Sender: TObject);
    procedure EdBancoChange(Sender: TObject);
    procedure EdBancoExit(Sender: TObject);
    procedure EdAgenciaExit(Sender: TObject);
    procedure EdContaExit(Sender: TObject);
    procedure EdChequeExit(Sender: TObject);
    procedure EdBandaChange(Sender: TObject);
    procedure EdBandaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdBancoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdClienteChange(Sender: TObject);
    procedure QrPesqAfterScroll(DataSet: TDataSet);
    procedure QrPesqAfterClose(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure EdCPF_1Exit(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure Incluiocorrncia1Click(Sender: TObject);
    procedure Alteraocorrncia1Click(Sender: TObject);
    procedure Excluiocorrncia1Click(Sender: TObject);
    procedure BtChequeDevClick(Sender: TObject);
    procedure BtStatusClick(Sender: TObject);
    procedure EdValorBaseChange(Sender: TObject);
    procedure EdTaxaProrChange(Sender: TObject);
    procedure TPDataNChange(Sender: TObject);
    procedure EdJurosPrChange(Sender: TObject);
    procedure QrLotesPrrCalcFields(DataSet: TDataSet);
    procedure Incluiprorrogao1Click(Sender: TObject);
    procedure ExcluiProrrogao1Click(Sender: TObject);
    procedure QrAlinItsAfterScroll(DataSet: TDataSet);
    procedure QrAlinItsCalcFields(DataSet: TDataSet);
    procedure QrAlinItsBeforeClose(DataSet: TDataSet);
    procedure PMStatusPopup(Sender: TObject);
    procedure EdJuros1Exit(Sender: TObject);
    procedure Alteradevoluo1Click(Sender: TObject);
    procedure IncluiPagamento1Click(Sender: TObject);
    procedure EdValorBase2Change(Sender: TObject);
    procedure EdJurosBase2Change(Sender: TObject);
    procedure TPPagto2Change(Sender: TObject);
    procedure EdJuros2Change(Sender: TObject);
    procedure ExcluiPagamento1Click(Sender: TObject);
    procedure PMDevolucaoPopup(Sender: TObject);
    procedure Desfazdevoluo1Click(Sender: TObject);
    procedure EdEmitenteExit(Sender: TObject);
    procedure EdCMC_7_1Change(Sender: TObject);
    procedure RecibodoPagamento1Click(Sender: TObject);
    procedure EdColigadoChange(Sender: TObject);
    procedure CGStatusAutomaClick(Sender: TObject);
    procedure CkRepassadoChange(Sender: TObject; ButtonIndex: Integer);
    procedure QrOcorreuAfterScroll(DataSet: TDataSet);
    procedure IncluiPagamento2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure EdValorBase6Change(Sender: TObject);
    procedure EdJurosBase6Change(Sender: TObject);
    procedure TPPagto6Change(Sender: TObject);
    procedure EdJuros6Change(Sender: TObject);
    procedure EdAPagar6Change(Sender: TObject);
    procedure Excluipagamento2Click(Sender: TObject);
    procedure QrOcorreuCalcFields(DataSet: TDataSet);
    procedure PMOcorreuPopup(Sender: TObject);
    procedure Recibodopagamento2Click(Sender: TObject);
    procedure BtQuitacaoClick(Sender: TObject);
    procedure Quitadocumento1Click(Sender: TObject);
    procedure Imprimerecibodequitao1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure EdBanda2Change(Sender: TObject);
    procedure EdCMC_7_2Change(Sender: TObject);
    procedure EdCPF_2Exit(Sender: TObject);
    procedure EdEmitente2Exit(Sender: TObject);
    procedure EdBanco2Exit(Sender: TObject);
    procedure EdCPF_2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure ReopenSacCHDevA;
    procedure ReopenSacOcorA;
    procedure ReopenSacRiscoC;
    procedure ReopenSacDOpen;
    procedure ReopenBanco;
    function CriaBAC_Pesq(Banco, Agencia, Conta: String; DVCC: Integer): String;
    procedure EdBanco2Change(Sender: TObject);
    procedure EdAgencia2Change(Sender: TObject);
    procedure EdConta2Change(Sender: TObject);
    procedure QrSacDOpenAfterOpen(DataSet: TDataSet);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure GradeItensDblClick(Sender: TObject);
    procedure BtReciboAntecipClick(Sender: TObject);
    procedure Meuparaocliente1Click(Sender: TObject);
    procedure Clienteparamim1Click(Sender: TObject);
    procedure IncluiQuitao1Click(Sender: TObject);
    procedure EdCPF_1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtHistoricoClick(Sender: TObject);
    procedure Clienteselecionado1Click(Sender: TObject);
    procedure Emitentesacadoselecionado1Click(Sender: TObject);
    procedure Ambos1Click(Sender: TObject);
    procedure EdDesco2Exit(Sender: TObject);
    procedure EdTaxaPExit(Sender: TObject);
    procedure Clientepesquisado1Click(Sender: TObject);
    procedure Emitentesacadopesquisado1Click(Sender: TObject);
    procedure Ambos2Click(Sender: TObject);
    procedure QrSacCHDevAAfterOpen(DataSet: TDataSet);
    procedure QrSacOcorAAfterOpen(DataSet: TDataSet);
    procedure QrSacCHDevACalcFields(DataSet: TDataSet);
    procedure QrSacCHDevABeforeClose(DataSet: TDataSet);
    procedure QrSacDOpenBeforeClose(DataSet: TDataSet);
    procedure QrSacDOpenCalcFields(DataSet: TDataSet);
    procedure QrSacRiscoCCalcFields(DataSet: TDataSet);
    procedure QrSacOcorABeforeClose(DataSet: TDataSet);
    procedure QrSacOcorACalcFields(DataSet: TDataSet);
    procedure BtDesDepClick(Sender: TObject);
    procedure QrSomaAfterOpen(DataSet: TDataSet);
    procedure N0ForastatusAutomtico1Click(Sender: TObject);
    procedure N1Forastatus1Click(Sender: TObject);
    procedure N2ForastatusBaixado1Click(Sender: TObject);
    procedure N3ForastatusMoroso1Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure CkStatusClick(Sender: TObject);
    procedure Adicionareventoaodiario1Click(Sender: TObject);
    procedure Gerenciardirio1Click(Sender: TObject);
    procedure PMDiarioPopup(Sender: TObject);
    procedure PMQuitacaoPopup(Sender: TObject);
  private
    { Private declarations }
    FOcorP, FLotesIts, FOcorreu, FLotesPrr, FAlinIts, FAlinPgs, FOcorrPg: Integer;
    FCPFSacado: String;
    FCHDevOpen_Total, FOcorA_Total: Double;
    FNaoDeposita: Boolean;
    FTempo, FUltim: TDateTime;
    FSoma_i: Integer;
    FSoma_v: Double;

    function TextoSQLPesq(TabLote, TabLoteIts: String; Soma: Boolean;
             Query: TmySQLQuery): Integer;
    function PagaChequeDev: Boolean;
    //function LocalizaEmitenteZ(MudaNome: Boolean): Boolean;
    function LocalizaEmitente2(MudaNome: Boolean): Boolean;

    procedure ReabreSomaPesq;
    procedure InfoTempo(Tempo: TDateTime; Texto: String; Inicial: Boolean);
    procedure MostraEdicao(Acao: Integer; Titulo: String);
    procedure ReopenOcorreu(Codigo: Integer);
    procedure ReopenLotesPrr(Controle: Integer);
    procedure ReopenBanco2;
    procedure ReopenAlinIts(LocCod: Integer);
    procedure ReopenAlinPgs(LocCod: Integer);
    procedure Pesquisa(Forca, Reabre: Boolean);
    procedure ConfiguraPr(Data: TDateTime);
    procedure CalculaAPagarPrr;
    procedure CalculaAPagarDev;
    procedure CalculaJurosPrr;
    procedure CalculaJurosDev;
    procedure ConfirmaInclusaoDev;
    procedure ConfirmaAlteracaoDev;
    procedure CalculaPagamentoCHDev(Quitacao: TDateTime);
    procedure ConfirmaOcorrencia;
    procedure ConfiguraPgCHDev(Data: TDateTime);
    procedure ReopenOcorrPg;
    procedure ConfiguraPgOC;
    procedure CalculaJurosOcor;
    procedure CalculaAPagarOcor;
    procedure CalculaPagtoOcorrPg(Quitacao: TDateTime; OcorrPg: Integer);
    procedure PesquisaRiscoSacado(CPF: String);
    procedure EmiteReciboAntecip(Tipo: Integer);
    procedure MostraHistorico(Quem: Integer);
    procedure ForcaStatus(NovoStatus: Integer);

  public
    { Public declarations }
    //FCk0001, FCk0002, FCk0004, FCk0008, FCk0016, FCk0032,
    Fq1, Fq2, Fq3, Fq0
    //, FCk1a32
    : Integer;
    FCliente: Integer;
  end;

  var
  FmChequesGer: TFmChequesGer;

implementation

{$R *.DFM}

uses Module, UnInternalConsts, UMySQLModule, Principal, UnGOTOy,
LotesEmi, PesqCPFCNPJ, HistCliEmi, Modulc, UnMyObjects;

procedure TFmChequesGer.MostraEdicao(Acao: Integer; Titulo: String);
begin
  LaTipo.Caption := Titulo;
  if Acao <> 0 then
  begin
    GradeItens.Align   := alTop;
    GradeItens.Height  := 60;
    GradeItens.Enabled := False;
    PainelMuda.Visible := True;
  end;
  case Acao of
    0:
    begin
      GradeItens.Align       := alClient;
      GradeItens.Enabled     := True;
      PainelCtrl.Visible     := True;
      PainelPesq.Visible     := True;
      PainelMuda.Visible     := False;
      PainelConf.Visible     := False;
      PainelOcor.Visible     := False;
      PainelOcorConf.Visible := False;
      PainelOcorPg.Visible   := False;
      PainelPror.Visible     := False;
      PainelDevo.Visible     := False;
      PainelPgDv.Visible     := False;
      DBGrid1.Enabled        := True;
      Panel1.Enabled         := True;
    end;
    1:
    begin
      PainelOcor.Visible := True;
      PainelConf.Visible := True;
      PainelCtrl.Visible := False;
      PainelPesq.Visible := False;
      PainelPror.Visible := False;
      PainelDevo.Visible := False;
      PainelPgDv.Visible := False;
      TPDataO.Enabled    := True;
      if Titulo = CO_INCLUSAO then
      begin
        EdOcorrencia.Text := '';
        CBOcorrencia.KeyValue := NULL;
        TPDataO.Date := Date;
        EdValor.Text := '';
        //
        EdTaxaP.Text := Geral.FFT(Dmod.ObtemTaxaDeCompraCliente(
          QrPesqCliente.Value), 4, siPositivo);
        QrSCB.Close;
      end else begin
        EdOcorrencia.Text := IntToStr(QrOcorreuOcorrencia.Value);
        CBOcorrencia.KeyValue := QrOcorreuOcorrencia.Value;
        TPDataO.Date := QrOcorreuDataO.Value;
        EdValor.Text := Geral.FFT(QrOcorreuValor.Value, 2, siPositivo);
        EdTaxaP.Text := Geral.FFT(QrOcorreuTaxaP.Value, 4, siPositivo);
      end;
      EdOcorrencia.SetFocus;
    end;
    2:
    begin
      PainelOcor.Visible := True;
      PainelConf.Visible := True;
      PainelCtrl.Visible := False;
      PainelPesq.Visible := False;
      PainelPror.Visible := True;
      PainelDevo.Visible := False;
      PainelPgDv.Visible := False;
      TPDataO.Enabled    := True;
      if Titulo = CO_INCLUSAO then
      begin
        EdOcorrencia.Text := '';
        CBOcorrencia.KeyValue := NULL;
        TPDataO.Date := Date;
        EdValor.Text := '';
        EdTaxaP.Text := '';
        ConfiguraPr(Date);
      end else begin
        EdOcorrencia.Text := IntToStr(QrOcorreuOcorrencia.Value);
        CBOcorrencia.KeyValue := QrOcorreuOcorrencia.Value;
        TPDataO.Date := QrOcorreuDataO.Value;
        EdValor.Text := Geral.FFT(QrOcorreuValor.Value, 2, siPositivo);
        EdTaxaP.Text := Geral.FFT(QrOcorreuTaxaP.Value, 4, siPositivo);
      end;
      TPDataN.SetFocus;
    end;
    3:
    begin
      PainelDevo.Visible := True;
      PainelConf.Visible := True;
      PainelOcor.Visible := False;
      PainelCtrl.Visible := False;
      PainelPesq.Visible := False;
      PainelPror.Visible := False;
      PainelPgDv.Visible := False;
      //
      if Titulo = CO_INCLUSAO then
      begin
        EdAlinea1.Text := '';
        CBAlinea1.KeyValue := NULL;
        EdAlinea2.Text := '';
        CBAlinea2.KeyValue := NULL;
        EdTaxas1.Text := '';
        EdMulta1.Text := '';
        EdJuros1.Text := Geral.FFT(QrPesqTAXA_COMPRA.Value, 4, siPositivo);
        //Alterado 06/12/12 Ini
        //TPData1_1.Date := QrPesqVencto.Value;
        //TPData1_2.Date := QrPesqVencto.Value;
        TPData1_1.Date := QrPesqDDeposito.Value;
        TPData1_2.Date := QrPesqDDeposito.Value;
        //Alterado 06/12/12 Fim
      end else begin
        EdAlinea1.Text := IntToStr(QrAlinItsAlinea1.Value);
        CBAlinea1.KeyValue := QrAlinItsAlinea1.Value;
        EdAlinea2.Text := IntToStr(QrAlinItsAlinea2.Value);
        CBAlinea2.KeyValue := QrAlinItsAlinea2.Value;
        EdTaxas1.Text := Geral.FFT(QrAlinItsTaxas.Value, 2, siPositivo);
        EdMulta1.Text := Geral.FFT(QrAlinItsMulta.Value, 2, siPositivo);
        EdJuros1.Text := Geral.FFT(QrAlinItsJurosP.Value, 6, siPositivo);
        TPData1_1.Date := QrAlinItsData1.Value;
        TPData1_2.Date := QrAlinItsData2.Value;
        //
      end;
      EdAlinea1.SetFocus;
    end;
    4:
    begin
      PainelPgDv.Visible := True;
      PainelConf.Visible := True;
      PainelDevo.Visible := False;
      PainelOcor.Visible := False;
      PainelCtrl.Visible := False;
      PainelPesq.Visible := False;
      PainelPror.Visible := False;
      //
      EdBanco2.Text       := '';
      EdAgencia2.Text     := '';
      EdConta2.Text       := '';
      EdCheque2.Text      := '';
      EdEmitente2.Text    := '';
      EdCPF_2.Text        := '';
      EdAPCD.Text         := '';
      CBAPCD.KeyValue     := NULL;
      EdBanda2.Text       := '';
      EdRegiaoCompe2.Text := ''; 
      //
      ConfiguraPgCHDev(Date);
      CalculaJurosDev;
      if Panel9.Visible then EdJurosBase2.SetFocus;
    end;
    5:
    begin
      PainelOcorPg.Visible   := True;
      PainelOcorConf.Visible := True;
      DBGrid1.Enabled        := False;
      Panel1.Enabled         := False;
      PainelCtrl.Visible     := False;
      PainelConf.Visible     := False;
      //
      if Titulo = CO_INCLUSAO then
      begin
        ConfiguraPgOC;
        CalculaJurosOcor;
        CalculaAPagarOcor;
      end else begin
      //
      end;
      EdJurosBase6.SetFocus;
    end;
  end;
end;

procedure TFmChequesGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmChequesGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if FCliente > 0 then
  begin
    EdCliente.Text := IntToStr(FCliente);
    CBCliente.KeyValue := FCliente;
  end;
end;

procedure TFmChequesGer.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmChequesGer.FormCreate(Sender: TObject);
begin
  GradeItens.PopupMenu := PMDiario;
  //
  QrEmitCPF.Open;
  QrEmitBAC.Open;
  QrClientes.Open;
  QrColigado.Open;
  QrOcorBank.Open;
  QrAlineas.Open;
  QrAPCD.Open;
  PainelData.Align := alClient;
  PainelBtsE.Align := alClient;
  GradeItens.Align := alClient;
  DBGrid4.Align    := alClient;
  Panel5.Align     := alClient;
  Panel9.Align     := alTop;
  Panel8.Align     := alClient;
  PageControl1.ActivePageIndex := 0;
  //
  CkRepassado.Value := 3;
end;

procedure TFmChequesGer.Pesquisa(Forca, Reabre: Boolean);
var
  Ok: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    QrPesq.Close;
    if not Reabre then Exit;
    if CkStatus.Value = 0 then Exit;
    //
    PainelData.Visible := False;
    PainelOcor.Visible := False;
    (*
    Fq0 := MLAGeral.IntInConjunto2Def(001, CGStatusManual.Value,  0, -1000);
    Fq1 := MLAGeral.IntInConjunto2Def(002, CGStatusManual.Value, -1, -1000);
    Fq2 := MLAGeral.IntInConjunto2Def(004, CGStatusManual.Value, -2, -1000);
    Fq3 := MLAGeral.IntInConjunto2Def(008, CGStatusManual.Value, -3, -1000);
    *)
    //
    if Forca then Ok := 1 else Ok := 0;
    QrPesq.Close;
    QrPesq.SQL.Clear;
    Ok := Ok + TextoSQLPesq('Lotes', 'LotesIts', False, QrPesq);
    if CkMorto.Checked then
    begin
      QrPesq.SQL.Add('');
      QrPesq.SQL.Add('UNION');
      QrPesq.SQL.Add('');
      Ok := Ok + TextoSQLPesq('Lotez', 'LotezIts', False, QrPesq);
    end;
    if Ok > 0 then
    begin
      QrPesq.SQL.Add('');
      QrPesq.SQL.Add('ORDER BY Vencto, DDeposito');
      //
      QrPesq.Open;
      if QrPesq.RecordCount > 0 then PainelData.Visible := True;
      if FLotesIts <> 0 then
      begin
        QrPesq.Locate('Controle', FLotesIts, []);
        InfoTempo(Now, 'Localiza��o de cheque na pesquisa', False);
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmChequesGer.ReabreSomaPesq;
begin
  Screen.Cursor := crHourGlass;
  try
    QrSoma.Close;
    QrSoma.SQL.Clear;
    TextoSQLPesq('Lotes', 'LotesIts', True, QrSoma);
    if CkMorto.Checked then
    begin
      QrSoma.SQL.Add('');
      QrSoma.SQL.Add('UNION');
      QrSoma.SQL.Add('');
      TextoSQLPesq('Lotez', 'LotezIts', True, QrSoma);
    end;
    QrSoma.Open;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmChequesGer.QrPesqCalcFields(DataSet: TDataSet);
var
  Taxas: MyArrayR07;
begin
  if CkCalcFields.Checked then
  begin
    InfoTempo(Now, 'In�cio c�lculos autom�ticos', False);
    Screen.Cursor := crHourGlass;
    QrPesqCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrPesqCPF.Value);
    //
    Taxas := DMod.ObtemTaxaDeCompraRealizada(QrPesqControle.Value);
    QrPesqTAXA_COMPRA.Value := QrPesqTxaCompra.Value + Taxas[0];
    //
    QrPesqNOMESTATUS.Value := MLAGeral.NomeStatusPgto3(QrPesqQuitado.Value,
      QrPesqDDeposito.Value, Date, (*QrPesqNOMECOLIGADO.Value*)'', True,
      QrPesqDevolucao.Value, QrPesqStatusDev.Value, QrPesqProrrVz.Value,
      QrPesqProrrDd.Value, QrPesqNaoDeposita.Value, QrPesqRepCli.Value);
    QrPesqTEL1CLI_TXT.Value := MLAGeral.FormataTelefone_TT_Curto(QrPesqTEL1CLI.Value);
    InfoTempo(Now, 'Fim c�lculos autom�ticos', False);
    Screen.Cursor := crDefault;
  end;  
end;

procedure TFmChequesGer.BtDesiste5Click(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO);
end;

procedure TFmChequesGer.EdOcorrenciaExit(Sender: TObject);
begin
  if (LaTipo.Caption = CO_INCLUSAO) and (PainelPror.Visible = False) then
  EdValor.Text := Geral.FFT(QrOcorBankBase.Value, 2, siNegativo);
end;

procedure TFmChequesGer.BtConfirma5Click(Sender: TObject);
begin
  if QrPesq.State     = dsBrowse then FLotesIts := QrPesqControle.Value;
  if QrOcorreu.State  = dsBrowse then FOcorreu  := QrOcorreuCodigo.Value;
  if QrLotesPrr.State = dsBrowse then FLotesPrr := QrLotesPrrControle.Value;
  if QrAlinIts.State  = dsBrowse then FAlinIts  := QrAlinItsCodigo.Value;
  if QrAlinPgs.State  = dsBrowse then FAlinPgs  := QrAlinPgsCodigo.Value;
  //
  if PainelPgDv.Visible then
  begin
    if not PagaChequeDev then Exit;
  end else
  if PainelDevo.Visible then
  begin
    if LaTipo.Caption = CO_INCLUSAO then
      ConfirmaInclusaoDev
    else
      ConfirmaAlteracaoDev;
  end else ConfirmaOcorrencia;
  MostraEdicao(0, CO_TRAVADO);
  //
  Pesquisa(True, False);
  //
end;

procedure TFmChequesGer.ReopenOcorreu(Codigo: Integer);
begin
  QrOcorreu.Close;
  QrOcorreu.Params[00].AsInteger := QrPesqControle.Value;
  QrOcorreu.Open;
  //
  if Codigo <> 0 then QrOcorreu.Locate('Codigo', Codigo, [])
  else QrOcorreu.Locate('Codigo', FOcorreu, []);
end;

procedure TFmChequesGer.ReopenLotesPrr(Controle: Integer);
begin
  QrLotesPrr.Close;
  QrLotesPrr.Params[00].AsInteger := QrPesqControle.Value;
  QrLotesPrr.Open;
  //
  if Controle <> 0 then QrLotesPrr.Locate('Controle', Controle, [])
  else QrLotesPrr.Locate('Controle', FLotesPrr, []);
end;

procedure TFmChequesGer.BtChequeOcorrClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMOcorreu, BtChequeOcorr);
end;

procedure TFmChequesGer.EdBancoChange(Sender: TObject);
begin
  if not EdBanco.Focused then ReopenBanco;
end;

procedure TFmChequesGer.EdBancoExit(Sender: TObject);
begin
  ReopenBanco;
  Pesquisa(False, False);
end;

procedure TFmChequesGer.EdAgenciaExit(Sender: TObject);
begin
  Pesquisa(False, False);
end;

procedure TFmChequesGer.EdContaExit(Sender: TObject);
begin
  Pesquisa(False, False);
end;

procedure TFmChequesGer.EdChequeExit(Sender: TObject);
begin
  Pesquisa(False, False);
end;

procedure TFmChequesGer.EdBandaChange(Sender: TObject);
begin
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
    EdCMC_7_1.Text := Geral.SoNumero_TT(EdBanda.Text);
end;

procedure TFmChequesGer.EdBandaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then BtSaidaClick(Self);
end;

procedure TFmChequesGer.EdBancoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then BtSaidaClick(Self);
end;

procedure TFmChequesGer.EdClienteChange(Sender: TObject);
begin
  Pesquisa(False, False);
end;

procedure TFmChequesGer.ReopenBanco;
begin
  QrBanco.Close;
  QrBanco.Params[0].AsString := EdBanco.Text;
  QrBanco.Open;
end;

procedure TFmChequesGer.QrPesqAfterScroll(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  InfoTempo(Now, 'Rolagem da tabela', False);
  InfoTempo(Now, 'Reabertura de tabelas', FTempo=0);
  ReopenOcorreu(0);
  InfoTempo(Now, 'Ocorr�ncias', False);
  ReopenLotesPrr(0);
  InfoTempo(Now, 'Prorroga��es', False);
  ReopenAlinIts(0);
  InfoTempo(Now, 'Cheques sem fundo', False);
  Screen.Cursor := crDefault;
  Memo1.Lines.Add('========================================== Fim reaberturas');
  FTempo := 0;
  If QrPesqDepositado.Value = 1 then BtDesDep.Enabled := True
    else BtDesDep.Enabled := False;
end;

procedure TFmChequesGer.QrPesqAfterClose(DataSet: TDataSet);
begin
  BtChequeOcorr.Enabled   := False;
  BtChequeDev.Enabled     := False;
  BtStatus.Enabled        := False;
  BtQuitacao.Enabled      := False;
end;

procedure TFmChequesGer.QrPesqAfterOpen(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  InfoTempo(Now, 'Reabertura de pesquisa', True);
  //
  Enab := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  //
  BtHistorico.Enabled   := Enab;
  BtChequeOcorr.Enabled := Enab;
  BtStatus.Enabled      := Enab;
  BtQuitacao.Enabled    := Enab;
  BtChequeDev.Enabled   := Enab;
  BtDesDep.Enabled      := Enab;
  BitBtn1.Enabled       := Enab;
  //
  ReabreSomaPesq;
end;

procedure TFmChequesGer.EdCPF_1Exit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCPF_1.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if MLAGeral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Application.MessageBox(PChar('N�mero inv�lido'), 'Erro', MB_OK+MB_ICONERROR);
      EdCPF_1.SetFocus;
    end else EdCPF_1.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCPF_1.Text := CO_VAZIO;
  //
  Dmod.QrLocCPF.Close;
  Dmod.QrLocCPF.Params[0].AsString := Geral.SoNumero_TT(EdCPF_1.Text);
  Dmod.QrLocCPF.Open;
  if Dmod.QrLocCPF.RecordCount = 0 then EdEmitente1.Text := ''
  else EdEmitente1.Text := Dmod.QrLocCPFNome.Value;
  //
  Pesquisa(False, False);
end;

procedure TFmChequesGer.BtRefreshClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  Pesquisa(True, True);
end;

procedure TFmChequesGer.Incluiocorrncia1Click(Sender: TObject);
begin
  MostraEdicao(1, CO_INCLUSAO);
end;

procedure TFmChequesGer.Alteraocorrncia1Click(Sender: TObject);
begin
  MostraEdicao(1, CO_ALTERACAO);
end;

procedure TFmChequesGer.Excluiocorrncia1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o da ocorr�ncia selecionada?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM ocorreu WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrOcorreuCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    QrOcorreu.Next;
    //
    ReopenOcorreu(QrOcorreuCodigo.Value);
  end;
end;

procedure TFmChequesGer.BtChequeDevClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  //
  if QrAlinits.RecordCount = 0 then
    MostraEdicao(3, CO_INCLUSAO)
  else
    MyObjects.MostraPopUpDeBotao(PMDevolucao, BtChequeDev);
end;

procedure TFmChequesGer.ConfiguraPr(Data: TDateTime);
begin
  QrLocPr.Close;
  QrLocPr.Params[0].AsInteger := QrPesqControle.Value;
  QrLocPr.Open;
  TPDataN.MinDate := 0;
  if QrLocPr.RecordCount > 0 then
  begin
    TPDataN.Date := Int(QrLocPrData3.Value);
    TPDataO.Date := Int(QrLocPrData3.Value);
    TPDataI.Date := Int(QrLocPrData1.Value);
  end else begin
    TPDataN.Date := Int(QrPesqVencto.Value);
    TPDataO.Date := Date;//Int(QrPesqVencto.Value);
    TPDataI.Date := Int(QrPesqVencto.Value);
  end;
  EdTaxaPror.Text := Geral.FFT(
    Dmod.ObtemTaxaDeCompraCliente(QrPesqCliente.Value), 6, siPositivo);
  EdValorBase.Text := Geral.FFT(QrPesqValor.Value, 2, siPositivo);
  // Permitir adiantamento
  //TPDataN.MinDate := TPDataO.Date;
  if Int(Data) > TPDataN.Date then TPDataN.Date := Int(Data);
  TPdataN.SetFocus;
  CalculaJurosPrr;
end;

procedure TFmChequesGer.BtStatusClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 1;
  //
  MyObjects.MostraPopUpDeBotao(PMStatus, BtStatus);
end;

procedure TFmChequesGer.EdValorBaseChange(Sender: TObject);
begin
  CalculaAPagarPrr;
end;

procedure TFmChequesGer.EdTaxaProrChange(Sender: TObject);
begin
  CalculaJurosPrr;
end;

procedure TFmChequesGer.TPDataNChange(Sender: TObject);
begin
  CalculaJurosPrr;
end;

procedure TFmChequesGer.EdJurosPrChange(Sender: TObject);
begin
  CalculaAPagarPrr;
end;

procedure TFmChequesGer.CalculaAPagarPrr;
begin
  EdValor.Text := EdJurosPr.Text;
end;

procedure TFmChequesGer.CalculaJurosPrr;
var
  Prazo, Praz2: Integer;
  Taxa, Juros, Valor: Double;
begin
  Prazo := Trunc(Int(TPDataN.Date) - Int(QrPesqDDeposito.Value));
  if Prazo < 0 then Praz2 := -Prazo else Praz2 := Prazo;
  begin
    Taxa  := Geral.DMV(EdTaxaPror.Text);
    Juros := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(Taxa, Praz2);
  end;
  Valor := Geral.DMV(EdValorBase.Text);
  EdJurosPeriodoN.Text := Geral.FFT(Juros, 6, siPositivo);
  Juros := Juros * Valor / 100;
  if Prazo < 0 then Juros := Juros * -1;
  EdJurosPr.Text := Geral.FFT(Juros, 2, siNegativo);
end;

procedure TFmChequesGer.QrLotesPrrCalcFields(DataSet: TDataSet);
begin
  QrLotesPrrDIAS_1_3.Value := Trunc(Int(QrLotesPrrData3.Value)-Int(QrLotesPrrData1.Value));
  QrLotesPrrDIAS_2_3.Value := Trunc(Int(QrLotesPrrData3.Value)-Int(QrLotesPrrData2.Value));
end;

procedure TFmChequesGer.Incluiprorrogao1Click(Sender: TObject);
begin
  FNaoDeposita := False;
  MostraEdicao(2, CO_INCLUSAO);
end;

procedure TFmChequesGer.ExcluiProrrogao1Click(Sender: TObject);
var
  Dias: Integer;
  DDep: String;
  Ocor: Integer;
begin
  if QrLotesPrr.RecNo <> QrLotesPrr.RecordCount then
  Application.MessageBox('Somente a �ltima prorroga��o/antecipa��o pode ser exclu�da!',
  'Aviso', MB_OK+MB_ICONWARNING) else
  begin
    if Application.MessageBox('Confirma a exclus�o da prorroga��o/antecipa��o?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Ocor := QrLotesPrrOcorrencia.Value;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM lotesprr WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrLotesPrrControle.Value;
      Dmod.QrUpd.ExecSQL;
      //
      ReopenLotesPrr(0);
      QrLotesPrr.Last;
      Dias := Trunc(Int(QrLotesPrrData3.Value)-Int(QrLotesPrrData1.Value));
      if QrLotesPrrData3.Value = 0 then
        DDep := FormatDateTime(VAR_FORMATDATE,
          UMyMod.CalculaDataDeposito(QrPesqVencto.Value))
      else
        DDep := FormatDateTime(VAR_FORMATDATE, QrLotesPrrData3.Value);
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   DDeposito=:P0, ProrrVz=:P1, ');
      Dmod.QrUpd.SQL.Add('ProrrDd=:P2, Data4=:P3 WHERE Controle=:Pa');
      Dmod.QrUpd.Params[0].AsString  := DDep;
      Dmod.QrUpd.Params[1].AsInteger := QrLotesPrr.RecordCount;
      Dmod.QrUpd.Params[2].AsInteger := Dias;
      Dmod.QrUpd.Params[3].AsString  := DDep;
      //
      Dmod.QrUpd.Params[4].AsInteger := QrPesqControle.Value;
      Dmod.QrUpd.ExecSQL;
      //
      FLotesIts := QrPesqControle.Value;
      FOcorreu  := QrOcorreuCodigo.Value;
      FLotesPrr := QrLotesPrrControle.Value;
      //
      Pesquisa(True, False);
      if Application.MessageBox(PChar('Deseja excluir tamb�m a ocorr�ncia da '+
      'prorroga��o/antecipa��o exclu�da?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
      ID_YES then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM ocorreu WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := Ocor;
        Dmod.QrUpd.ExecSQL;
        //
        ReopenOcorreu(0);
      end;
    end;
  end;
end;

procedure TFmChequesGer.QrAlinItsAfterScroll(DataSet: TDataSet);
begin
  ReopenAlinPgs(QrAlinPgsCodigo.Value);
end;

procedure TFmChequesGer.QrAlinItsCalcFields(DataSet: TDataSet);
begin
  Screen.Cursor := crHourGlass;
  QrAlinItsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrAlinItsCPF.Value);
  QrAlinItsDATA1_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrAlinItsData1.Value);
  QrAlinItsDATA2_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrAlinItsData2.Value);
  QrAlinItsDATA3_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrAlinItsData3.Value);
  QrAlinItsSALDO.Value := QrAlinItsValor.Value +QrAlinItsTaxas.Value +
    QrAlinItsMulta.Value + QrAlinItsJurosV.Value - QrAlinItsValPago.Value -
    QrAlinItsDesconto.Value - QrAlinItsPgDesc.Value;
  //
  QrAlinItsATUAL.Value := DMod.ObtemValorAtualizado(
    QrAlinItsCliente.Value, QrAlinItsStatus.Value, QrAlinItsData1.Value, Date,
    QrAlinItsData3.Value, QrAlinItsValor.Value, QrAlinItsJurosV.Value,
    QrAlinItsDesconto.Value, QrAlinItsValPago.Value + QrAlinItsPgDesc.Value,
    QrAlinItsJurosP.Value, False);
  Screen.Cursor := crDefault;
end;

procedure TFmChequesGer.ReopenAlinPgs(LocCod: Integer);
begin
  QrAlinPgs.Close;
  QrAlinPgs.Params[0].AsInteger := QrAlinItsCodigo.Value;
  QrAlinPgs.Open;
  //
  if LocCod > 0 then QrAlinPgs.Locate('Codigo', LocCod, [])
  else QrAlinPgs.Locate('Codigo', FAlinPgs, []);
end;

procedure TFmChequesGer.ReopenAlinits(LocCod: Integer);
begin
  QrAlinIts.Close;
  QrAlinIts.SQL.Clear;
  QrAlinIts.SQL.Add('SELECT al1.Nome NOMEALINEA1, al2.Nome NOMEALINEA2, ai.*');
  QrAlinIts.SQL.Add('FROM alinits ai');
  QrAlinIts.SQL.Add('LEFT JOIN alineas al1 ON al1.Codigo=ai.Alinea1');
  QrAlinIts.SQL.Add('LEFT JOIN alineas al2 ON al2.Codigo=ai.Alinea2');
  QrAlinIts.SQL.Add('WHERE ai.ChequeOrigem=:P0');
  QrAlinIts.Params[0].AsInteger := QrPesqControle.Value;
  QrAlinIts.Open;
  if LocCod > 0 then QrAlinIts.Locate('Codigo', LocCod, []);
end;

procedure TFmChequesGer.ConfirmaInclusaoDev;
var
  Cliente, Alinea1, Alinea2, Codigo, Banco, Agencia, Cheque, Origem: Integer;
  Data1, Data2, Emitente, Conta, CPF, Vencto, DDeposi: String;
  Valor, Multa, Juros, Taxas: Double;
begin
  Cheque  := QrPesqCheque.Value;//Geral.IMV(EdCheque1.Text);
  Origem  := QrPesqControle.Value;
  Valor   := QrPesqValor.Value;//Geral.DMV(EdValor1.Text);
  Multa   := Geral.DMV(EdMulta1.Text);
  Juros   := Geral.DMV(EdJuros1.Text);
  Taxas   := Geral.DMV(EdTaxas1.Text);
  Banco   := QrPesqBanco.Value;//Geral.IMV(EdBanco1.Text);
  Agencia := QrPesqAgencia.Value;//Geral.IMV(EdAgencia1.Text);
  Cliente := QrPesqCliente.Value;//Geral.IMV(EdCliente1.Text);
  Emitente:= QrPesqEmitente.Value;
  Conta   := QrPesqConta.Value;
  CPF     := QrPesqCPF.Value;
  Alinea1 := Geral.IMV(EdAlinea1.Text);
  Alinea2 := Geral.IMV(EdAlinea2.Text);
  Vencto  := Geral.FDT(QrPesqVencto.Value, 1);
  DDeposi := Geral.FDT(QrPesqDDeposito.Value, 1);
  //
  if Alinea1 > 0 then
    Data1 := FormatDateTime(VAR_FORMATDATE, TPData1_1.Date) else Data1 := '0000-00-00';
  if Alinea2 > 0 then
    Data2 := FormatDateTime(VAR_FORMATDATE, TPData1_2.Date) else Data2 := '0000-00-00';
  //
  Dmod.QrUpd.SQL.Clear;
  Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
  'AlinIts', 'AlinIts', 'Codigo');
  Dmod.QrUpd.SQL.Add('INSERT INTO alinits SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('Alinea1=:P0, Alinea2=:P1, Data1=:P2, Data2=:P3, ');
  Dmod.QrUpd.SQL.Add('Cliente=:P4, Banco=:P5, Agencia=:P6, Conta=:P7, ');
  Dmod.QrUpd.SQL.Add('Cheque=:P8, CPF=:P9, Valor=:P10, Multa=:P11, ');
  Dmod.QrUpd.SQL.Add('Emitente=:P12, ChequeOrigem=:P13, JurosP=:P14, ');
  Dmod.QrUpd.SQL.Add('Taxas=:P15, Vencto=:P16, DDeposito=:P17, ');
  Dmod.QrUpd.SQL.Add('LoteOrigem=:P18');  // , ObsGerais=:P19
  Dmod.QrUpd.SQL.Add(', Codigo=:Pa');
  //
  Dmod.QrUpd.Params[00].AsInteger := Alinea1;
  Dmod.QrUpd.Params[01].AsInteger := Alinea2;
  Dmod.QrUpd.Params[02].AsString  := Data1;
  Dmod.QrUpd.Params[03].AsString  := Data2;
  Dmod.QrUpd.Params[04].AsInteger := Cliente;
  Dmod.QrUpd.Params[05].AsInteger := Banco;
  Dmod.QrUpd.Params[06].AsInteger := Agencia;
  Dmod.QrUpd.Params[07].AsString  := Conta;
  Dmod.QrUpd.Params[08].AsInteger := Cheque;
  Dmod.QrUpd.Params[09].AsString  := CPF;
  Dmod.QrUpd.Params[10].AsFloat   := Valor;
  Dmod.QrUpd.Params[11].AsFloat   := Multa;
  Dmod.QrUpd.Params[12].AsString  := Emitente;
  Dmod.QrUpd.Params[13].AsInteger := Origem;
  Dmod.QrUpd.Params[14].AsFloat   := Juros;
  Dmod.QrUpd.Params[15].AsFloat   := Taxas;
  Dmod.QrUpd.Params[16].AsString  := Vencto;
  Dmod.QrUpd.Params[17].AsString  := DDeposi;
  Dmod.QrUpd.Params[18].AsInteger := QrPesqCodigo.Value;
  //Dmod.QrUpd.Params[19].AsString  := QrPesqObsGerais.Value;
  //
  Dmod.QrUpd.Params[19].AsInteger  := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   Devolucao=:P0, ');
  Dmod.QrUpd.SQL.Add('Data3=0, Data4=0 WHERE Controle=:P1');
  Dmod.QrUpd.Params[00].AsInteger := Codigo;
  Dmod.QrUpd.Params[01].AsInteger := Origem;
  Dmod.QrUpd.ExecSQL;
  //
  FAlinIts := Codigo;
end;

procedure TFmChequesGer.ConfirmaAlteracaoDev;
var
  Alinea1, Alinea2, Codigo: Integer;
  Data1, Data2: String;
  Taxas, Multa, Juros: Double;
begin
  Taxas   := Geral.DMV(EdTaxas1.Text);
  Multa   := Geral.DMV(EdMulta1.Text);
  Juros   := Geral.DMV(EdJuros1.Text);
  Alinea1 := Geral.IMV(EdAlinea1.Text);
  Alinea2 := Geral.IMV(EdAlinea2.Text);
  //
  if Alinea1 > 0 then
    Data1 := FormatDateTime(VAR_FORMATDATE, TPData1_1.Date) else Data1 := '0000-00-00';
  if Alinea2 > 0 then
    Data2 := FormatDateTime(VAR_FORMATDATE, TPData1_2.Date) else Data2 := '0000-00-00';
  //
  Dmod.QrUpd.SQL.Clear;
  Codigo := QrAlinItsCodigo.Value;
  Dmod.QrUpd.SQL.Add('UPDATE alinits SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('Alinea1=:P0, Alinea2=:P1, Data1=:P2, Data2=:P3, ');
  Dmod.QrUpd.SQL.Add('Taxas=:P4, Multa=:P5, JurosP=:P6 WHERE Codigo=:Pa');
  //
  Dmod.QrUpd.Params[00].AsInteger := Alinea1;
  Dmod.QrUpd.Params[01].AsInteger := Alinea2;
  Dmod.QrUpd.Params[02].AsString  := Data1;
  Dmod.QrUpd.Params[03].AsString  := Data2;
  Dmod.QrUpd.Params[04].AsFloat   := Taxas;
  Dmod.QrUpd.Params[05].AsFloat   := Multa;
  Dmod.QrUpd.Params[06].AsFloat   := Juros;
  //
  Dmod.QrUpd.Params[07].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  FAlinIts := Codigo;
  CalculaPagamentoCHDev(QrAlinItsData3.Value);
end;

procedure TFmChequesGer.QrAlinItsBeforeClose(DataSet: TDataSet);
begin
  QrAlinPgs.Close;
end;

procedure TFmChequesGer.PMStatusPopup(Sender: TObject);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  Enab2 := (QrLotesPrr.State <> dsInactive) and (QrLotesPrr.RecordCount > 0);
  //
  Incluiprorrogao1.Enabled := Enab;
  IncluiQuitao1.Enabled    := Enab;
  ExcluiProrrogao1.Enabled := Enab and Enab2;
  //
  N0ForastatusAutomtico1.Enabled := Enab;
  N1Forastatus1.Enabled          := Enab;
  N2ForastatusBaixado1.Enabled   := Enab;
  N3ForastatusMoroso1.Enabled    := Enab;
end;

procedure TFmChequesGer.EdJuros1Exit(Sender: TObject);
begin
  if PainelConf.Visible then BtConfirma5.SetFocus;
end;

procedure TFmChequesGer.Adicionareventoaodiario1Click(Sender: TObject);
var
  Texto: String;
begin
  Texto := QrPesqCPF.Value + ' - ' + QrPesqEmitente.Value + #13#10;
  //
  FmPrincipal.MostraDiarioAdd(0, QrPesqCliente.Value, QrPesqControle.Value, Texto);
end;

procedure TFmChequesGer.Alteradevoluo1Click(Sender: TObject);
begin
  MostraEdicao(3, CO_ALTERACAO);
end;

function TFmChequesGer.PagaChequeDev: Boolean;
var
  Codigo, Controle, APCD, Cliente: Integer;
  Dias, Cheque, Comp, Banco, Agencia, Praca: Integer;
  Valor, Desco: Double;
  DDeposito, DCompra, Vencto, Conta, CPF, Emitente: String;
begin
  Cliente := QrPesqCliente.Value;
  Result := True;
  APCD := Geral.IMV(EdAPCD.Text);
  if APCD = 0 then
  begin
    Application.MessageBox('Defina a al�nea de pagamento!', 'Erro',
      MB_OK+MB_ICONERROR);
    EdAPCD.SetFocus;
    Result := False;
    Exit;
  end;
  Dias    := Trunc(TPPagto2.Date - TPDataBase2.Date);
  Valor   := Geral.DMV(EdPago2.Text);
  Desco   := Geral.DMV(EdDesco2.Text);
  Vencto  := FormatDateTime(VAR_FORMATDATE, TPPagto2.Date);
  DCompra := FormatDateTime(VAR_FORMATDATE, TPData2.Date);
  Banco   := Geral.IMV(EdBanco2.Text);
  Agencia := Geral.IMV(EdAgencia2.Text);
  Conta   := EdConta2.Text;
  Cheque  := Geral.IMV(EdCheque2.Text);
  CPF     := Geral.SoNumero_TT(EdCPF_2.Text);
  Emitente:= EdEmitente2.Text;
  Praca   := Geral.IMV(EdRegiaoCompe2.Text);
  if QrAPCDExigeCMC7.Value = 1 then
  begin
    if (Conta = '') or (Cheque=0) then
    begin
      Application.MessageBox('A al�nea selecionada exige CMC7 (dados do cheque)!',
        'Erro', MB_OK+MB_ICONERROR);
      EdBanda2.SetFocus;
      Result := False;
      Exit;
    end;
  end;
  QrSCB.Close;
  QrSCB.Params[0].AsInteger := QrPesqCliente.Value;
  QrSCB.Open;
  //
  Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'AlinPgs', 'AlinPgs', 'Codigo');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO alinpgs SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2, DataCh=:P3, APCD=:P4, ');
  Dmod.QrUpd.SQL.Add('Desco=:P5, AlinIts=:Pa, Codigo=:Pb');
  Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPPagto2.Date);
  Dmod.QrUpd.Params[01].AsFloat   := Geral.DMV(EdJuros2.Text);
  Dmod.QrUpd.Params[02].AsFloat   := Geral.DMV(EdPago2.Text);
  Dmod.QrUpd.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, TPData2.Date);
  Dmod.QrUpd.Params[04].AsInteger := QrAPCDCodigo.Value;
  Dmod.QrUpd.Params[05].AsFloat   := Desco;
  //
  Dmod.QrUpd.Params[06].AsInteger := QrAlinItsCodigo.Value;
  Dmod.QrUpd.Params[07].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  if QrAPCDExigeCMC7.Value = 1 then
  begin
    //  Depositos, riscos, etc
    QrCli.Close;
    QrCli.Params[0].AsInteger := -Cliente;
    QrCli.Open;
    if QrCli.RecordCount = 0 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO lotes SET Codigo=:P0, Cliente=:P1');
      Dmod.QrUpd.Params[00].AsInteger := -Cliente;
      Dmod.QrUpd.Params[01].AsInteger := Cliente;
      Dmod.QrUpd.ExecSQL;
    end;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO lotesits SET Quitado=0, ');
    Dmod.QrUpd.SQL.Add('Comp=:P0, Banco=:P1, Agencia=:P2, Conta=:P3, ');
    Dmod.QrUpd.SQL.Add('Cheque=:P4, CPF=:P5, Emitente=:P6, Valor=:P7, ');
    Dmod.QrUpd.SQL.Add('Vencto=:P8, TxaCompra=:P9, TxaAdValorem=:P10, ');
    Dmod.QrUpd.SQL.Add('VlrCompra=:P11, VlrAdValorem=:P12, DMais=:P13, ');
    Dmod.QrUpd.SQL.Add('Dias=:P14, TxaJuros=:P15, DCompra=:P16, ');
    Dmod.QrUpd.SQL.Add('DDeposito=:P17, Praca=:P18, Tipo=:P19, ');
    Dmod.QrUpd.SQL.Add('AliIts=:P20, AlinPgs=:P21, Cliente=:P22, Data3=:P23 ');
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb');
    //
    Comp    := FmPrincipal.VerificaDiasCompensacao(Praca, QrPesqCliente.Value,
               Valor, TPData2.Date, TPPagto2.Date, QrSCBSCB.Value);
    DDeposito := FormatDateTime(VAR_FORMATDATE,
      UMyMod.CalculaDataDeposito(TPPagto2.Date));
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'LotesIts', 'LotesIts', 'Controle');
    Dmod.QrUpd.Params[00].AsInteger := Comp;
    Dmod.QrUpd.Params[01].AsInteger := Banco;
    Dmod.QrUpd.Params[02].AsInteger := Agencia;
    Dmod.QrUpd.Params[03].AsString  := Conta;
    Dmod.QrUpd.Params[04].AsInteger := Cheque;
    Dmod.QrUpd.Params[05].AsString  := CPF;
    Dmod.QrUpd.Params[06].AsString  := Emitente;
    Dmod.QrUpd.Params[07].AsFloat   := Valor;
    Dmod.QrUpd.Params[08].AsString  := Vencto;
    //
    Dmod.QrUpd.Params[09].AsFloat   := 0;//FTaxa[0];
    Dmod.QrUpd.Params[10].AsFloat   := 0;
    Dmod.QrUpd.Params[11].AsFloat   := 0;//FValr[0];
    Dmod.QrUpd.Params[12].AsFloat   := 0;
    Dmod.QrUpd.Params[13].AsInteger := 0;//DMais;
    Dmod.QrUpd.Params[14].AsInteger := Dias;
    Dmod.QrUpd.Params[15].AsFloat   := 0;//FJuro[0];
    Dmod.QrUpd.Params[16].AsString  := DCompra;
    Dmod.QrUpd.Params[17].AsString  := DDeposito;
    Dmod.QrUpd.Params[18].AsInteger := Praca;
    Dmod.QrUpd.Params[19].AsInteger := QrAPCDDeposita.Value + 1;
    Dmod.QrUpd.Params[20].AsInteger := QrAlinItsCodigo.Value;
    Dmod.QrUpd.Params[21].AsInteger := Codigo;
    Dmod.QrUpd.Params[22].AsInteger := Cliente;
    Dmod.QrUpd.Params[23].AsString  := DDeposito; // Quita autom�tico at� voltar
    //
    Dmod.QrUpd.Params[24].AsInteger := -Cliente;
    Dmod.QrUpd.Params[25].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
  end else
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1, ValQuit=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.Params[0].AsFloat   := QrAlinItsATUAL.Value;
    Dmod.QrUpd.Params[1].AsInteger := QrPesqControle.Value;
    Dmod.QrUpd.ExecSQL;
  end;
  //
  CalculaPagamentoCHDev(TPPagto2.Date);
  FAlinPgs := Codigo;
  ReopenAlinPgs(Codigo);
  MostraEdicao(0, CO_TRAVADO);
  //
  Dmoc.AtualizaEmitente(FormatFloat('000', Banco),
    FormatFloat('0000', Agencia), Conta, CPF, Emitente, -1);
  QrSCB.Close;
end;

procedure TFmChequesGer.CalculaPagamentoCHDev(Quitacao: TDateTime);
var
  Sit: Integer;
  Data3, Data4: String;
begin
  ReopenAlinits(QrAlinItsCodigo.Value);
  QrSumPg.Close;
  QrSumPg.Params[0].AsInteger := QrAlinItsCodigo.Value;
  QrSumPg.Open;
  if (QrSumPgPago.Value + QrSumPgDesco.Value) < 0.01 then Sit := 0 else if
    (QrSumPgPago.Value + QrSumPgDesco.Value) <
    (QrSumPgJuros.Value + QrAlinItsValor.Value + QrAlinItsTaxas.Value +
    QrAlinItsMulta.Value - QrAlinItsDesconto.Value)-0.009 then
    Sit := 1 else Sit := 2;
  Data4 := FormatDateTime(VAR_FORMATDATE, Quitacao);
  if Sit > 1 then Data3 := Data4 else Data3 := '0000-00-00';
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE alinits SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('JurosV=:P0, ValPago=:P1, PgDesc=:P2, Status=:P3, ');
  Dmod.QrUpd.SQL.Add('Data3=:P4, Data4=:P5 WHERE Codigo=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := QrSumPgJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumPgPago.Value;
  Dmod.QrUpd.Params[02].AsFloat   := QrSumPgDesco.Value;
  Dmod.QrUpd.Params[03].AsInteger := Sit;
  Dmod.QrUpd.Params[04].AsString  := Data3;
  Dmod.QrUpd.Params[05].AsString  := Data4;
  Dmod.QrUpd.Params[06].AsInteger := QrAlinItsCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   ');
  Dmod.QrUpd.SQL.Add('Data3=:P0 WHERE Controle=:Pa');
  Dmod.QrUpd.Params[00].AsString  := Data3;
  Dmod.QrUpd.Params[01].AsInteger := QrAlinItsChequeOrigem.Value;
  Dmod.QrUpd.ExecSQL;
  //
  ReopenAlinits(QrAlinItsCodigo.Value);
end;

procedure TFmChequesGer.ConfirmaOcorrencia;
var
  Ocorr, Prorr: Integer;
  Data: String;
begin
  Prorr := QrLotesPrrControle.Value;
  Dmod.QrUpd.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpd.SQL.Add('INSERT INTO ocorreu SET AlterWeb=1, ');
    Ocorr := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'Ocorreu', 'Ocorreu', 'Codigo');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET AlterWeb=1, ');
    Ocorr := QrOcorreuCodigo.Value;
  end;
  Dmod.QrUpd.SQL.Add('LotesIts=:P0, DataO=:P1, Ocorrencia=:P2, Valor=:P3, ');
  Dmod.QrUpd.SQL.Add('TaxaP=:P4 ');
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa');
  end else begin
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
  end;
  Dmod.QrUpd.Params[00].AsInteger := QrPesqControle.Value;
  Dmod.QrUpd.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataO.Date);
  Dmod.QrUpd.Params[02].AsInteger := Geral.IMV(EdOcorrencia.Text);
  Dmod.QrUpd.Params[03].AsFloat   := Geral.DMV(EdValor.Text);
  Dmod.QrUpd.Params[04].AsFloat   := Geral.DMV(EdTaxaP.Text);
  Dmod.QrUpd.Params[05].AsInteger := Ocorr;
  Dmod.QrUpd.ExecSQL;
  //
  if PainelPror.Visible then
  begin
    Data := FormatDateTime(VAR_FORMATDATE, TPDataN.Date);
    Dmod.QrUpd.SQL.Clear;
    if LaTipo.Caption = CO_INCLUSAO then
    begin
      Dmod.QrUpd.SQL.Add('INSERT INTO lotesprr SET ');
      Prorr := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'LotesPrr', 'LotesPrr', 'Controle');
    end else begin
      Dmod.QrUpd.SQL.Add('UPDATE lotesprr SET ');
      Prorr := QrLotesPrrControle.Value;
    end;
    Dmod.QrUpd.SQL.Add('Codigo=:P0, Data1=:P1, Data2=:P2, Data3=:P3, ');
    Dmod.QrUpd.SQL.Add('Ocorrencia=:P4');
    if LaTipo.Caption = CO_INCLUSAO then
    begin
      Dmod.QrUpd.SQL.Add(', Controle=:Pa');
    end else begin
      Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa');
    end;
    Dmod.QrUpd.Params[00].AsInteger := QrPesqControle.Value;
    Dmod.QrUpd.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataI.Date);
    Dmod.QrUpd.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataO.Date);
    Dmod.QrUpd.Params[03].AsString  := Data;
    Dmod.QrUpd.Params[04].AsInteger := Ocorr;
    //
    Dmod.QrUpd.Params[05].AsInteger := Prorr;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   DDeposito=:P0, ProrrVz=:P1, ');
    Dmod.QrUpd.SQL.Add('ProrrDd=:P2, NaoDeposita=:P3, Data3=:P4, Quitado=:P5  ');  //Parei aqui
    Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa');
    Dmod.QrUpd.Params[0].AsString  := Data;
    Dmod.QrUpd.Params[1].AsInteger := QrLotesPrr.RecordCount+1;
    Dmod.QrUpd.Params[2].AsInteger := Trunc(Int(TPDataN.Date)-Int(TPDataI.Date));
    Dmod.QrUpd.Params[3].AsInteger := MLAGeral.BoolToInt(FNaoDeposita);
    Dmod.QrUpd.Params[4].AsString  := Data;
    Dmod.QrUpd.Params[5].AsInteger := -1; // prorroga��o
    //
    Dmod.QrUpd.Params[6].AsInteger := QrPesqControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
  end;
  FOcorreu  := Ocorr;
  FLotesPrr := Prorr;
end;

procedure TFmChequesGer.IncluiPagamento1Click(Sender: TObject);
begin
  MostraEdicao(4, CO_INCLUSAO);
end;

procedure TFmChequesGer.ConfiguraPgCHDev(Data: TDateTime);
var
  ValorBase: Double;
begin
  TPPagto2.MinDate := 0;
  //ver se funciona !!!
  QrLocPg.Close;
  QrLocPg.Params[0].AsInteger := QrAlinItsCodigo.Value;
  QrLocPg.Open;
  if QrLocPg.RecordCount > 0 then
  begin
    //if Int(QrLocPgData.Value) < int(Data) then
      TPDataBase2.Date := Int(QrLocPgData.Value)
    // Errado !!!!
    //else TPDataBase2.Date := date;
  end else TPDataBase2.Date := QrAlinItsData1.Value;
  EdJurosBase2.Text := Geral.FFT(QrAlinItsJurosP.Value, 6, siPositivo);
  ValorBase := QrAlinItsValor.Value +QrAlinItsTaxas.Value +
    QrAlinItsMulta.Value + QrAlinItsJurosV.Value - QrAlinItsValPago.Value -
    QrAlinItsDesconto.Value - QrAlinItsPgDesc.Value;
  TPPagto2.Date := Date + 360;
  EdValorBase2.Text := Geral.FFT(ValorBase, 2, siPositivo);
  if Data >= TPDataBase2.Date then TPPagto2.Date := Data
  else TPPagto2.Date := TPDataBase2.Date;
  TPPagto2.MinDate := TPDataBase2.Date;
  TPData2.Date := TPDataBase2.Date;
  TPPagto2.SetFocus;
end;

procedure TFmChequesGer.EdValorBase2Change(Sender: TObject);
begin
  CalculaAPagarDev;
end;

procedure TFmChequesGer.CalculaAPagarDev;
var
  Base, Juro: Double;
begin
  Base := Geral.DMV(EdValorBase2.Text);
  Juro := Geral.DMV(EdJuros2.Text);
  //
  EdAPagar.Text := Geral.FFT(Base+Juro, 2, siPositivo);
  EdPago2.Text  := EdAPagar.Text;
end;

procedure TFmChequesGer.EdJurosBase2Change(Sender: TObject);
begin
  CalculaJurosDev;
end;

procedure TFmChequesGer.CalculaJurosDev;
var
  Prazo: Integer;
  Taxa, Juros, Valor: Double;
begin
  Juros := 0;
  Prazo := Trunc(Int(TPPagto2.Date) - Int(TPDataBase2.Date));
  if Prazo > 0 then
  begin
    Taxa  := Geral.DMV(EdJurosBase2.Text);
    Juros := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(Taxa, Prazo);
  end;
  Valor := Geral.DMV(EdValorBase2.Text);
  EdJurosPeriodo2.Text := Geral.FFT(Juros, 6, siPositivo);
  EdJuros2.Text := Geral.FFT(Juros * Valor / 100, 2, siPositivo);
end;

procedure TFmChequesGer.TPPagto2Change(Sender: TObject);
begin
  CalculaJurosDev;
end;

procedure TFmChequesGer.EdJuros2Change(Sender: TObject);
begin
  CalculaAPagarDev;
end;

procedure TFmChequesGer.ExcluiPagamento1Click(Sender: TObject);
begin
  if QrAlinPgs.RecordCount = 0 then
    Application.MessageBox('Nenhum pagamento foi selecionado para exclus�o!',
    'Erro', MB_OK+MB_ICONERROR)
  else if (QrAlinPgs.RecNo < QrAlinPgs.RecordCount) then
    Application.MessageBox('Somente o �ltimo pagamento pode ser exclu�do!',
    'Erro', MB_OK+MB_ICONERROR)
  else begin
    if Application.MessageBox('Confirma a exclus�o do pagamento selecionado?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM lotesits WHERE AlinPgs=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrAlinPgsCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM alinpgs WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrAlinPgsCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      QrLast.Close;
      QrLast.Params[0].AsInteger := QrAlinItsCodigo.Value;
      QrLast.Open;
      //
      QrAlinPgs.Next;
      FAlinPgs := UmyMod.ProximoRegistro(QrAlinPgs, 'Codigo', QrAlinPgsCodigo.Value);
      CalculaPagamentoCHDev(QrLastData.Value);
      Pesquisa(True, False);
    end;
  end;
end;

procedure TFmChequesGer.PMDevolucaoPopup(Sender: TObject);
begin
  if QrAlinPgs.RecordCount > 0 then
  begin
    ExcluiPagamento1.Enabled := True;
    Desfazdevoluo1.Enabled   := False;
  end else begin
    ExcluiPagamento1.Enabled := False;
    if QrAlinIts.RecordCount > 0 then
      Desfazdevoluo1.Enabled := True
    else
      Desfazdevoluo1.Enabled := False;
  end;
end;

procedure TFmChequesGer.PMDiarioPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  //
  Adicionareventoaodiario1.Enabled := Enab;
  Gerenciardirio1.Enabled          := Enab;
end;

procedure TFmChequesGer.Desfazdevoluo1Click(Sender: TObject);
begin
  if QrAlinIts.RecordCount = 0 then
    Application.MessageBox('Nenhuma devolu��o foi selecionada para exclus�o!',
    'Erro', MB_OK+MB_ICONERROR)
  else if QrAlinPgs.RecordCount > 0 then
    Application.MessageBox('Exclus�o cancelada! Este cheque devolvido j� '+
    'possui pagamentos!', 'Erro', MB_OK+MB_ICONERROR) else
  begin
    if Application.MessageBox('Confirma a exclus�o do cheque selecionado?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1, Devolucao=0, Data3=DDeposito ');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrAlinItsChequeOrigem.Value;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM alinits WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrAlinItsCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      CalculaPagamentoCHDev(QrAlinItsData3.Value);
      QrAlinIts.Next;
      FAlinIts := QrAlinItsCodigo.Value;
      Pesquisa(True, False);
    end;
  end;
end;

procedure TFmChequesGer.EdEmitenteExit(Sender: TObject);
begin
  Pesquisa(False, False);
end;

procedure TFmChequesGer.EdCMC_7_1Change(Sender: TObject);
var
  Banda: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC_7_1.Text) = 0 then
  begin
    Banda := TBandaMagnetica.Create;
    Banda.BandaMagnetica := EdCMC_7_1.Text;
    //EdComp.Text    := Banda.Compe;
    EdBanco.Text   := Banda.Banco;
    EdAgencia.Text := Banda.Agencia;
    EdConta.Text   := Banda.Conta;
    EdCheque.Text  := Banda.Numero;
    //
    Pesquisa(False, False);
  end;
end;

procedure TFmChequesGer.RecibodoPagamento1Click(Sender: TObject);
var
  Pagamento, Texto: String;
begin
  QrBco1.Close;
  QrBco1.Params[0].AsInteger := QrPesqBanco.Value;
  QrBco1.Open;
  Pagamento := MLAGeral.NomeStatusPgto5(QrPesqQuitado.Value,
    QrPesqDDeposito.Value, Date, '', True, QrPesqDevolucao.Value,
    QrPesqStatusDev.Value, QrPesqProrrVz.Value, QrPesqProrrDd.Value, True, False);
  Texto := Pagamento + ' do cheque n� ' + FormatFloat('000000', QrPesqCheque.Value)
    +' do banco n� '+FormatFloat('000', QrPesqBanco.Value) + ' ' +
    QrBco1Nome.Value+ ' ag�ncia n� '+ FormatFloat('0000', QrPesqAgencia.Value) +
    ' emitida por '+ QrPesqEmitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqVencto.Value);
  if QrAlinItsAlinea1.Value > 0 then
  begin
    Texto := Texto + ' e devolvido sob a al�nea n�' +
    IntToStr(QrAlinItsAlinea1.Value) + ' "' + QrAlinItsNOMEALINEA1.Value + '"';
  end;
  if QrAlinItsAlinea2.Value > 0 then
  begin
    Texto := Texto + ' e sob a al�nea n� ' +
    IntToStr(QrAlinItsAlinea2.Value) + ' "' + QrAlinItsNOMEALINEA2.Value + '"';
  end;
  //
  //if QrLctDebito.Value > 0 then
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value,
    QrAlinPgsPago.Value, 0, 0, 'CHD-'+FormatFloat('000',
    QrAlinPgsCodigo.Value), Texto, '', '', QrAlinPgsData.Value, 0);
end;

procedure TFmChequesGer.EdColigadoChange(Sender: TObject);
begin
  Pesquisa(False, False);
end;

procedure TFmChequesGer.CGStatusAutomaClick(Sender: TObject);
begin
  Pesquisa(False, False);
end;

procedure TFmChequesGer.CkRepassadoChange(Sender: TObject;
  ButtonIndex: Integer);
begin
  if CkRepassado.Value <> 2 then
  begin
    LaColigado.Enabled := True;
    EdColigado.Enabled := True;
    CBColigado.Enabled := True;
  end else begin
    LaColigado.Enabled := False;
    EdColigado.Enabled := False;
    CBColigado.Enabled := False;
  end;
end;

procedure TFmChequesGer.QrOcorreuAfterScroll(DataSet: TDataSet);
begin
  ReopenOcorrPg;
end;

procedure TFmChequesGer.ReopenOcorrPg;
begin
  QrOcorrPg.Close;
  QrOcorrPg.Params[0].AsInteger := QrOcorreuCodigo.Value;
  QrOcorrPg.Open;
  //
  QrOcorrPg.Locate('Codigo', FOcorrPg, []);
end;

procedure TFmChequesGer.IncluiPagamento2Click(Sender: TObject);
begin
  MostraEdicao(5, CO_INCLUSAO);
end;

procedure TFmChequesGer.ConfiguraPgOc;
var
  ValorBase: Double;
begin
  QrLocOc.Close;
  QrLocOc.Params[0].AsInteger := QrOcorreuCodigo.Value;
  QrLocOc.Open;
  TPPagto6.MinDate := 0;
  if QrLocOc.RecordCount > 0 then
  begin
    TPPagto6.Date     := Int(QrLocOcData.Value);
    TPDataBase6.Date  := Int(QrLocOcData.Value);
  end else begin
    TPPagto6.Date    := Int(QrOcorreuDataO.Value);
    TPDataBase6.Date := Int(QrOcorreuDataO.Value);
  end;
  EdJurosBase6.Text := Geral.FFT(
    Dmod.ObtemTaxaDeCompraCliente(QrPesqCliente.Value), 6, siPositivo);
  ValorBase := QrOcorreuValor.Value + QrOcorreuTaxaV.Value - QrOcorreuPago.Value;
  EdValorBase6.Text := Geral.FFT(ValorBase, 2, siNegativo);
  TPPagto6.MinDate := TPPagto6.Date;
  if Date > TPPagto6.MinDate then TPPagto6.Date := Int(Date);
  TPPagto6.SetFocus;
end;

procedure TFmChequesGer.BitBtn3Click(Sender: TObject);
var
  OcorrPg: Integer;
begin
  OcorrPg := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'OcorrPG', 'OcorrPG', 'Codigo');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO ocorrpg SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2, LotePg=:P3');
  Dmod.QrUpd.SQL.Add(', Ocorreu=:Pa, Codigo=:Pb');
  Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPPagto6.Date);
  Dmod.QrUpd.Params[01].AsFloat   := Geral.DMV(EdJuros6.Text);
  Dmod.QrUpd.Params[02].AsFloat   := Geral.DMV(EdPago6.Text);
  Dmod.QrUpd.Params[03].AsInteger := 0;
  //
  Dmod.QrUpd.Params[04].AsInteger := QrOcorreuCodigo.Value;
  Dmod.QrUpd.Params[05].AsInteger := OcorrPg;
  Dmod.QrUpd.ExecSQL;
  //
  CalculaPagtoOcorrPg(TPPagto6.Date, QrOcorreuCodigo.Value);
  ///
  FOcorP := QrOcorreuCodigo.Value;
  MostraEdicao(0, CO_TRAVADO);
  FOcorP := OcorrPg;
  ReopenOcorreu(QrOcorreuCodigo.Value);
end;

procedure TFmChequesGer.BitBtn4Click(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO);
end;

procedure TFmChequesGer.CalculaJurosOcor;
var
  Prazo: Integer;
  Taxa, Juros, Valor: Double;
begin
  Juros := 0;
  Prazo := Trunc(Int(TPPagto6.Date) - Int(TPDataBase6.Date));
  if Prazo > 0 then
  begin
    Taxa  := Geral.DMV(EdJurosBase6.Text);
    Juros := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(Taxa, Prazo);
  end;
  Valor := Geral.DMV(EdValorBase6.Text);
  EdJurosPeriodo6.Text := Geral.FFT(Juros, 6, siPositivo);
  EdJuros6.Text := Geral.FFT(Juros * Valor / 100, 2, siPositivo);
end;

procedure TFmChequesGer.CalculaAPagarOcor;
var
  Base, Juro: Double;
begin
  Base := Geral.DMV(EdValorBase6.Text);
  Juro := Geral.DMV(EdJuros6.Text);
  //
  EdAPagar6.Text := Geral.FFT(Base+Juro, 2, siNegativo);
end;

procedure TFmChequesGer.CalculaPagtoOcorrPg(Quitacao: TDateTime; OcorrPg: Integer);
var
  Sit: Integer;
begin
  QrSumOc.Close;
  QrSumOc.Params[0].AsInteger := OcorrPg;
  QrSumOc.Open;
   //Errado corrigido em 09/08/2012 
  {if QrSumOcPago.Value < 0.01 then Sit := 0 else if QrSumOcPago.Value <
    (QrOcorreuValor.Value + ==>QrOcorreuTaxaV.Value<==)-0.009 then}
  //if QrSumOcPago.Value < 0.01 then
  Sit := 0;
  if QrSumOc.RecordCount > 0 then
  begin
    if QrSumOcPago.Value < (QrOcorreuValor.Value + QrSumOcJuros.Value)-0.009 then
      Sit := 1
    else
      Sit := 2;
  end;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('TaxaV=:P0, Pago=:P1, Status=:P2, ');
  Dmod.QrUpd.SQL.Add('Data3=:P3 WHERE Codigo=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := QrSumOcJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumOcPago.Value;
  Dmod.QrUpd.Params[02].AsInteger := Sit;
  Dmod.QrUpd.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Quitacao);
  Dmod.QrUpd.Params[04].AsInteger := OcorrPg;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TFmChequesGer.EdValorBase6Change(Sender: TObject);
begin
  CalculaAPagarOcor;
end;

procedure TFmChequesGer.EdJurosBase6Change(Sender: TObject);
begin
  CalculaJurosOcor;
  CalculaAPagarOcor;
end;

procedure TFmChequesGer.TPPagto6Change(Sender: TObject);
begin
  CalculaJurosOcor;
end;

procedure TFmChequesGer.EdJuros6Change(Sender: TObject);
var
  VBase, Juros: Double;
begin
  VBase := Geral.DMV(EdValorBase6.Text);
  Juros := Geral.DMV(EdJuros6.Text);
  EdAPagar6.Text := Geral.FFT(VBase+Juros, 2, siNegativo);
end;

procedure TFmChequesGer.EdAPagar6Change(Sender: TObject);
begin
  EdPago6.Text := EdAPagar6.Text;
end;

procedure TFmChequesGer.Excluipagamento2Click(Sender: TObject);
var
  Ocorreu: Integer;
begin
  QrLastOcor.Close;
  QrLastOcor.Params[0].AsInteger := QrOcorrPgOcorreu.Value;
  QrLastOcor.Open;
  if (QrLastOcorData.Value > QrOcorrPgData.Value)
  or ((QrLastOcorData.Value = QrOcorrPgData.Value)
  and (QrLastOcorCodigo.Value > QrOcorrPgCodigo.Value))
  then
     Application.MessageBox('Somente o �ltimo pagamento pode ser exclu�do!',
    'Erro', MB_OK+MB_ICONERROR)
  else begin
    if Application.MessageBox('Confirma a exclus�o deste pagamento?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        Ocorreu := QrOcorrPgOcorreu.Value;
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM ocorrpg WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrOcorrPgCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        QrLastOcor.Close;
        QrLastOcor.Params[0].AsInteger := Ocorreu;
        QrLastOcor.Open;
        CalculaPagtoOcorrPg(QrLastOcorData.Value, Ocorreu);
        //
        QrOcorrPg.Next;
        FOcorP := QrOcorrPgCodigo.Value;
        ReopenOcorreu(QrOcorreuCodigo.Value);
        Screen.Cursor := crDefault;
      except
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmChequesGer.QrOcorreuCalcFields(DataSet: TDataSet);
var
  Cliente: Integer;
begin
  Screen.Cursor := crHourGlass;
  if QrOcorreuCLIENTELOTE.Value > 0 then
    Cliente := QrOcorreuCLIENTELOTE.Value else
    Cliente := QrOcorreuCliente.Value;
  //
  QrOcorreuSALDO.Value := QrOcorreuValor.Value + QrOcorreuTaxaV.Value - QrOcorreuPago.Value;
  //
  QrOcorreuATUALIZADO.Value := DMod.ObtemValorAtualizado(
    Cliente, 1, QrOcorreuDataO.Value, Date, QrOcorreuData3.Value,
    QrOcorreuValor.Value, QrOcorreuTaxaV.Value, 0 (*Desco*),
    QrOcorreuPago.Value, QrOcorreuTaxaP.Value, False);
  Screen.Cursor := crDefault;
end;

procedure TFmChequesGer.PMOcorreuPopup(Sender: TObject);
var
  Enab, Enab2, Enab3: Boolean;
begin
  Enab  := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  Enab2 := (QrOcorreu.State <> dsInactive) and (QrOcorreu.RecordCount > 0);
  Enab3 := (QrOcorrPg.State <> dsInactive) and (QrOcorrPg.RecordCount > 0);
  //
  Incluiocorrncia1.Enabled := Enab;
  Alteraocorrncia1.Enabled := Enab and Enab2;
  Excluiocorrncia1.Enabled := Enab and Enab2;
  //
  IncluiPagamento2.Enabled := Enab and Enab2;
  Excluipagamento2.Enabled := Enab and Enab2 and Enab3;
  //
  Recibodopagamento2.Enabled := Enab and Enab2 and Enab3; 
end;

procedure TFmChequesGer.PMQuitacaoPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := (QrPesq.State <> dsInactive) and (QrPesq.RecordCount > 0);
  //
  Quitadocumento1.Enabled        := Enab;
  Imprimerecibodequitao1.Enabled := Enab;
end;

procedure TFmChequesGer.Recibodopagamento2Click(Sender: TObject);
var
  Pagamento, Texto: String;
begin
  QrBco1.Close;
  QrBco1.Params[0].AsInteger := QrPesqBanco.Value;
  QrBco1.Open;
  Pagamento := MLAGeral.NomeStatusPgto6(QrOcorreuATUALIZADO.Value,
    QrOcorrPg.RecNo)+' da ocorr�ncia "'+ QrOcorreuNOMEOCORRENCIA.Value+
    '" cobrada sobre';
  Texto := Pagamento + ' o cheque n� ' + FormatFloat('000000', QrPesqCheque.Value)
    +' do banco n� '+FormatFloat('000', QrPesqBanco.Value) + ' ' +
    QrBco1Nome.Value+ ' ag�ncia n� '+ FormatFloat('0000', QrPesqAgencia.Value) +
    ' emitida por '+ QrPesqEmitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqVencto.Value)+'. A quita��o desta '+
    'ocorr�ncia n�o quita o cheque';
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value,
    QrOcorrPgPago.Value, 0, 0, 'CHO-'+FormatFloat('000',
    QrOcorrPgCodigo.Value), Texto, '', '', QrOcorrPgData.Value, 0);
end;

procedure TFmChequesGer.BtQuitacaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMQuitacao, BtQuitacao);
end;

procedure TFmChequesGer.Quitadocumento1Click(Sender: TObject);
var
  ValQuit: Double;
  Localiz, DevolPg, OcorrPg, AlinIts, Ocorreu: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    if ((QrOcorreu.State = dsInactive) or (QrOcorreu.RecordCount = 0))
      and ((QrAlinIts.State = dsInactive) or (QrAlinIts.RecordCount = 0)) then
    begin
      Geral.MensagemBox('N�o h� nenhuma ocorr�ncia ou devolu��o para ser quitado!',
        'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    Ocorreu := QrOcorreuCodigo.Value;
    AlinIts := QrAlinItsCodigo.Value;
    ValQuit := 0;
    if QrAlinItsATUAL.Value > 0 then
    begin
      ValQuit := QrAlinItsATUAL.Value;
      DevolPg := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
            'AlinPgs', 'AlinPgs', 'Codigo');
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO alinpgs SET AlterWeb=1, ');
      Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2');
      Dmod.QrUpd.SQL.Add(', AlinIts=:Pa, Codigo=:Pb');
      Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
      Dmod.QrUpd.Params[01].AsFloat   := QrAlinItsATUAL.Value;
      Dmod.QrUpd.Params[02].AsFloat   := QrAlinItsATUAL.Value-QrAlinItsSALDO.Value;
      Dmod.QrUpd.Params[03].AsInteger := QrAlinItsCodigo.Value;
      Dmod.QrUpd.Params[04].AsInteger := DevolPg;
      Dmod.QrUpd.ExecSQL;
      //
      CalculaPagamentoCHDev(Date);
    end;
    QrOcorreu.First;
    while not QrOcorreu.Eof do
    begin
      if QrOcorreuATUALIZADO.Value > 0 then
      begin
        ValQuit := ValQuit + QrOcorreuATUALIZADO.Value;
        OcorrPg := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
              'OcorrPG', 'OcorrPG', 'Codigo');
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('INSERT INTO ocorrpg SET AlterWeb=1, ');
        Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2, LotePg=:P3');
        Dmod.QrUpd.SQL.Add(', Ocorreu=:Pa, Codigo=:Pb');
        Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpd.Params[01].AsFloat   := QrOcorreuATUALIZADO.Value-QrOcorreuSALDO.Value;
        Dmod.QrUpd.Params[02].AsFloat   := QrOcorreuATUALIZADO.Value;
        Dmod.QrUpd.Params[03].AsInteger := 0;
        //
        Dmod.QrUpd.Params[04].AsInteger := QrOcorreuCodigo.Value;
        Dmod.QrUpd.Params[05].AsInteger := OcorrPg;
        Dmod.QrUpd.ExecSQL;
        //
        CalculaPagtoOcorrPg(Date, QrOcorreuCodigo.Value);
        QrOcorreu.Next;
      end;
    end;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1, ValQuit=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.Params[0].AsFloat   := ValQuit;
    Dmod.QrUpd.Params[1].AsInteger := QrPesqControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Localiz := QrPesqControle.Value;
    QrPesq.Close;
    QrPesq.Open;
    QrPesq.Locate('Controle', Localiz, []);
    ReopenAlinIts(AlinIts);
    ReopenOcorreu(Ocorreu);
    if Application.MessageBox(PChar('Deseja imprimir um recibo com a soma '+
    'dos recebimentos efetuados?'), 'Emiss�o de Recibo', MB_YESNO+MB_ICONQUESTION)
    = ID_YES then Imprimerecibodequitao1Click(Self);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmChequesGer.Imprimerecibodequitao1Click(Sender: TObject);
var
  Pagamento, Texto, Liga: String;
begin
  QrBco1.Close;
  QrBco1.Params[0].AsInteger := QrPesqBanco.Value;
  QrBco1.Open;
  Pagamento := 'Quita��o de d�bitos gerados at� esta data relativos ao';
  Texto := Pagamento + ' cheque n� ' + FormatFloat('000000', QrPesqCheque.Value)
    +' do banco n� '+FormatFloat('000', QrPesqBanco.Value) + ' ' +
    QrBco1Nome.Value+ ' ag�ncia n� '+ FormatFloat('0000', QrPesqAgencia.Value) +
    ' emitida por '+ QrPesqEmitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqVencto.Value);
  if QrAlinItsAlinea1.Value > 0 then
  begin
    Texto := Texto + ' e devolvido sob a al�nea n�' +
    IntToStr(QrAlinItsAlinea1.Value) + ' "' + QrAlinItsNOMEALINEA1.Value + '"';
  end;
  if QrAlinItsAlinea2.Value > 0 then
  begin
    Texto := Texto + ' e sob a al�nea n� ' +
    IntToStr(QrAlinItsAlinea2.Value) + ' "' + QrAlinItsNOMEALINEA2.Value + '"';
  end;
  if QrOcorreu.RecordCount > 0 then
  begin
    if QrOcorreu.RecordCount = 1 then Texto := Texto + ' e a ocorr�ncia'
    else Texto := Texto + ' e as ocorr�ncias';
    QrOcorreu.First;
    Liga := ' ';
    while not QrOcorreu.Eof do
    begin
      Texto := Texto + Liga + '"'+QrOcorreuNOMEOCORRENCIA.Value + '"';
      if QrOcorreu.RecNo+1= QrOcorreu.RecordCount then Liga := ' e '
      else liga := ', ';
      QrOcorreu.Next;
    end;
  end;
  //
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value,
    QrPesqValQuit.Value, 0, 0, 'CHX-'+FormatFloat('000000',
    QrPesqControle.Value), Texto, '', '', Date, 0);
end;

procedure TFmChequesGer.BitBtn1Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  FmPrincipal.CriaFormLotes(1, 0, QrPesqCodigo.Value, QrPesqControle.Value);
end;

procedure TFmChequesGer.EdBanda2Change(Sender: TObject);
begin
  if MLAGeral.LeuTodoCMC7(EdBanda2.Text) then
    EdCMC_7_2.Text := Geral.SoNumero_TT(EdBanda2.Text);
end;

procedure TFmChequesGer.EdCMC_7_2Change(Sender: TObject);
var
  Banda: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC_7_2.Text) = 0 then
  begin
    Banda := TBandaMagnetica.Create;
    Banda.BandaMagnetica := EdCMC_7_2.Text;
    EdRegiaoCompe2.Text  := Banda.Compe;
    EdBanco2.Text        := Banda.Banco;
    EdAgencia2.Text      := Banda.Agencia;
    EdConta2.Text        := Banda.Conta;
    EdCheque2.Text       := Banda.Numero;
    //
  end;
end;

procedure TFmChequesGer.EdCPF_2Exit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCPF_2.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if MLAGeral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Application.MessageBox(PChar('N�mero inv�lido'), 'Erro', MB_OK+MB_ICONERROR);
      EdCPF_2.SetFocus;
    end else EdCPF_2.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCPF_2.Text := CO_VAZIO;
  PesquisaRiscoSacado(EdCPF_2.Text);
end;

procedure TFmChequesGer.EdEmitente2Exit(Sender: TObject);
begin
  if PainelConf.Visible then BtConfirma5.SetFocus;
end;

procedure TFmChequesGer.EdBanco2Exit(Sender: TObject);
begin
  LocalizaEmitente2(True);
end;

procedure TFmChequesGer.EdCPF_2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    Dmod.QrLocCPF.Close;
    Dmod.QrLocCPF.Params[0].AsString := Geral.SoNumero_TT(EdCPF_2.Text);
    Dmod.QrLocCPF.Open;
    case Dmod.QrLocCPF.RecordCount of
      0: Application.MessageBox('N�o foi localizado emitente para este CPF!',
        'Aviso', MB_OK+MB_ICONWARNING);
      1: EdEmitente2.Text := Dmod.QrLocCPFNome.Value;
      else begin
        Application.CreateForm(TFmLotesEmi, FmLotesEmi);
        FmLotesEmi.ShowModal;
        if FmLotesEmi.FEmitenteNome <> '' then
          EdEmitente2.Text := FmLotesEmi.FEmitenteNome;
        FmLotesEmi.Destroy;
      end;
    end;
  end;
end;

procedure TFmChequesGer.PesquisaRiscoSacado(CPF: String);
begin
  FCPFSacado := Geral.SoNumero_TT(CPF);
  //FNomeSacado := '';
  // soma antes do �ltimo !!!!
  ReopenSacCHDevA;
  ReopenSacOcorA;
  // fim somas.
  //
  ReopenSacRiscoC;
  //
  // precisa ser o �ltimo !!!! para Ed?.Text
  ReopenSacDOpen;
  //
end;

function TFmChequesGer.LocalizaEmitente2(MudaNome: Boolean): Boolean;
var
  B,A,C: String;
begin
  Result := False;
  if  (Geral.IMV(EdBanco2.Text) > 0)
  and (Geral.IMV(EdAgencia2.Text) > 0)
  and (Geral.DMV(EdConta2.Text) > 0.1) then
  begin
    Screen.Cursor := crHourGlass;
    try
      (*B := EdBanco.Text;
      A := EdAgencia.Text;
      C := EdConta.Text;
      //
      QrLocEmit.Close;
      QrLocEmit.Params[0].AsInteger := QrBancoDVCC.Value;
      QrLocEmit.Params[1].AsString  := CriaBAC_Pesq(B,A,C,QrBancoDVCC.Value);
      QrLocEmit.Open;*)
      //
      if MudaNome then
      begin
        B := EdBanco2.Text;
        A := EdAgencia2.Text;
        C := EdConta2.Text;
        //
        (*QrLocEmit.Close;
        QrLocEmit.Params[0].AsInteger := QrBancoDVCC.Value;
        QrLocEmit.Params[1].AsString  := CriaBAC_Pesq(B,A,C,QrBancoDVCC.Value);
        QrLocEmit.Open;*)
        Dmoc.QrLocEmiBAC.Close;
        Dmoc.QrLocEmiBAC.Params[0].AsInteger := QrBanco2DVCC.Value;
        Dmoc.QrLocEmiBAC.Params[1].AsString  := CriaBAC_Pesq(B,A,C,QrBanco2DVCC.Value);
        Dmoc.QrLocEmiBAC.Open;
        //
        if Dmoc.QrLocEmiBAC.RecordCount = 0 then
        begin
          EdEmitente2.Text := '';
          EdCPF_2.Text := '';
        end else begin
          Dmoc.QrLocEmiCPF.Close;
          Dmoc.QrLocEmiCPF.Params[0].AsString := Dmoc.QrLocEmiBACCPF.Value;
          Dmoc.QrLocEmiCPF.Open;
          //
          if Dmoc.QrLocEmiCPF.RecordCount = 0 then
          begin
            EdEmitente2.Text := '';
            EdCPF_2.Text := '';
          end else begin
            EdEmitente2.Text := Dmoc.QrLocEmiCPFNome.Value;
            EdCPF_2.Text      := Geral.FormataCNPJ_TT(Dmoc.QrLocEmiCPFCPF.Value);
            PesquisaRiscoSacado(EdCPF_2.Text);
          end;
        end;
      end;
      Screen.Cursor := crDefault;
      Result := True;
    except
      Screen.Cursor := crDefault;
      raise;
    end;
  end;
end;

(*function TFmChequesGer.LocalizaEmitente2(MudaNome: Boolean): Boolean;
var
  B,A,C: String;
begin
  Result := False;
  if  (Geral.IMV(EdBanco2.Text) > 0)
  and (Geral.IMV(EdAgencia2.Text) > 0)
  and (Geral.DMV(EdConta2.Text) > 0.1) then
  begin
    Screen.Cursor := crHourGlass;
    try
      B := EdBanco2.Text;
      A := EdAgencia2.Text;
      C := EdConta2.Text;
      //
      QrLocEmit.Close;
      QrLocEmit.Params[0].AsInteger := QrBanco2DVCC.Value;
      QrLocEmit.Params[1].AsString  := CriaBAC_Pesq(B,A,C,QrBanco2DVCC.Value);
      QrLocEmit.Open;
      //
      if MudaNome then
      begin
        if QrLocEmit.RecordCount = 0 then
        begin
          EdEmitente2.Text := '';
          EdCPF_2.Text := '';
        end else begin
          EdEmitente2.Text := QrLocEmitNome.Value;
          EdCPF_2.Text      := Geral.FormataCNPJ_TT(QrLocEmitCPF.Value);
          PesquisaRiscoSacado(EdCPF_2.Text);
        end;
      end;
      Screen.Cursor := crDefault;
      Result := True;
    except
      Screen.Cursor := crDefault;
      raise;
    end;
  end;
end;*)

procedure TFmChequesGer.ReopenSacCHDevA;
begin
  QrSacCHDevA.Close;
  QrSacCHDevA.SQL.Clear;
  QrSacCHDevA.SQL.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrSacCHDevA.SQL.Add('ELSE en.Nome END NOMECLIENTE, ai.*');
  QrSacCHDevA.SQL.Add('FROM alinits ai');
  QrSacCHDevA.SQL.Add('LEFT JOIN entidades en ON en.Codigo=ai.Cliente');
  if FmPrincipal.FConnections = 0 then
  begin
    //QrSacCHDevA.SQL.Add('LEFT JOIN lotesits li ON li.Controle=ai.ChequeOrigem');
    QrSacCHDevA.SQL.Add('LEFT JOIN lotes lo ON ai.LoteOrigem=lo.Codigo');
  end;
  QrSacCHDevA.SQL.Add('WHERE ai.Status<2');
  if FmPrincipal.FConnections = 0 then
  begin
    QrSacCHDevA.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  end;
  if FCPFSacado <> '' then
    QrSacCHDevA.SQL.Add('AND ai.CPF = "'+FCPFSacado+'"');
  QrSacCHDevA.SQL.Add('ORDER BY ai.Data1, ai.Data2');
  QrSacCHDevA.SQL.Add('');
  QrSacCHDevA.Open;
end;

procedure TFmChequesGer.ReopenSacOcorA;
begin
  QrSacOcorA.Close;
  QrSacOcorA.SQL.Clear;
  QrSacOcorA.SQL.Add('SELECT li.Controle, li.Emitente, li.CPF, ');
  QrSacOcorA.SQL.Add('lo.Tipo, IF(oc.Cliente > 0,"CL",IF(lo.Tipo=0,"CH","DU"))TIPODOC,');
  QrSacOcorA.SQL.Add('ob.Nome NOMEOCORRENCIA, lo.Cliente CLIENTELOTE, oc.*');
  QrSacOcorA.SQL.Add('FROM ocorreu oc');
  QrSacOcorA.SQL.Add('LEFT JOIN lotesits li ON oc.LotesIts = li.Controle');
  QrSacOcorA.SQL.Add('LEFT JOIN lotes    lo ON lo.Codigo   = li.Codigo');
  QrSacOcorA.SQL.Add('LEFT JOIN ocorbank ob ON ob.Codigo   = oc.Ocorrencia');
  QrSacOcorA.SQL.Add('WHERE oc.Status<2');
  //if FNomeSacado <> '' then
    //QrSacOcorA.SQL.Add('AND li.Emitente LIKE "'+FNomeSacado+'"');
  if FCPFSacado <> '' then
    QrSacOcorA.SQL.Add('AND li.CPF = "'+FCPFSacado+'"');
  QrSacOcorA.SQL.Add('');
  QrSacOcorA.Open;
  //
  //if FOcorA > 0 then QrSacOcorA.Locate('Codigo', FOcorA, []);
end;

procedure TFmChequesGer.ReopenSacDOpen;
begin
  QrSacDOpen.Close;
  QrSacDOpen.SQL.Clear;
  QrSacDOpen.SQL.Add('SELECT od.Nome STATUS, li.Controle, li.Duplicata, li.Repassado, ');
  QrSacDOpen.SQL.Add('li.DCompra, li.Valor, li.DDeposito, li.Emitente, li.CPF,');
  QrSacDOpen.SQL.Add('lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg,');
  QrSacDOpen.SQL.Add('li.Vencto, li.Data3');
  QrSacDOpen.SQL.Add('FROM lotesits li');
  QrSacDOpen.SQL.Add('LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo');
  QrSacDOpen.SQL.Add('LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao');
  QrSacDOpen.SQL.Add('WHERE lo.Tipo=1');
  QrSacDOpen.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  QrSacDOpen.SQL.Add('AND li.Quitado <> 2');
  if FCPFSacado <> '' then
    QrSacDOpen.SQL.Add('AND li.CPF = "'+FCPFSacado+'"');
  QrSacDOpen.SQL.Add('ORDER BY li.Vencto');
  QrSacDOpen.SQL.Add('');
  QrSacDOpen.Open;
end;

procedure TFmChequesGer.ReopenSacRiscoC;
begin
  QrSacRiscoC.Close;
  QrSacRiscoC.SQL.Clear;
  QrSacRiscoC.SQL.Add('SELECT lo.Tipo, li.Banco, li.Agencia, li.Conta, li.Cheque, li.Valor,');
  QrSacRiscoC.SQL.Add('li.DCompra, li.DDeposito, li.Emitente, li.CPF');
  QrSacRiscoC.SQL.Add('FROM lotesits li');
  QrSacRiscoC.SQL.Add('LEFT JOIN lotes lo ON lo.Codigo=li.Codigo');
  QrSacRiscoC.SQL.Add('WHERE lo.Tipo = 0');
  QrSacRiscoC.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
    IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  QrSacRiscoC.SQL.Add('AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())');
  if FCPFSacado <> '' then
    QrSacRiscoC.SQL.Add('AND li.CPF = "'+FCPFSacado+'"');
  QrSacRiscoC.SQL.Add('ORDER BY DDeposito');
  QrSacRiscoC.Open;
  //
  QrSacRiscoTC.Close;
  QrSacRiscoTC.SQL.Clear;
  QrSacRiscoTC.SQL.Add('SELECT SUM(li.Valor) Valor');
  QrSacRiscoTC.SQL.Add('FROM lotesits li');
  QrSacRiscoTC.SQL.Add('LEFT JOIN lotes lo ON lo.Codigo=li.Codigo');
  QrSacRiscoTC.SQL.Add('WHERE lo.Tipo = 0');
  QrSacRiscoTC.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  QrSacRiscoTC.SQL.Add('AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())');
  if FCPFSacado <> '' then
    QrSacRiscoTC.SQL.Add('AND li.CPF = "'+FCPFSacado+'"');
  QrSacRiscoTC.Open;
  //
end;

procedure TFmChequesGer.ReopenBanco2;
begin
  QrBanco2.Close;
  QrBanco2.Params[0].AsString := EdBanco.Text;
  QrBanco2.Open;
end;

function TFmChequesGer.CriaBAC_Pesq(Banco, Agencia, Conta: String; DVCC: Integer): String;
begin
  EdRealCC.Text := Copy(Conta, 10-DVCC+1, DVCC);
  Result := Banco+Agencia+EdRealCC.Text;
end;

procedure TFmChequesGer.EdBanco2Change(Sender: TObject);
begin
  LocalizaEmitente2(True);
  if not EdBanco2.Focused then ReopenBanco2;
end;

procedure TFmChequesGer.EdAgencia2Change(Sender: TObject);
begin
  LocalizaEmitente2(True);
end;

procedure TFmChequesGer.EdConta2Change(Sender: TObject);
begin
  LocalizaEmitente2(True);
end;

procedure TFmChequesGer.QrSacDOpenAfterOpen(DataSet: TDataSet);
var
  DR, DV, DT, CR, CV, CT, RT, VT, ST, OA, TT: Double;
begin
  DR := 0; DV := 0;
  while not QrSacDOpen.Eof do
  begin
    if Int(Date) > QrSacDOpenVencto.Value then
      DV := DV + QrSacDOpenSALDO_ATUALIZADO.Value
    else
      DR := DR + QrSacDOpenSALDO_ATUALIZADO.Value;
    QrSacDOpen.Next;
  end;
  DT := DR+DV;
  QrSacDOpen.First;
  CR := QrSacRiscoTCValor.Value;
  CV := FCHDevOpen_Total;
  CT := CR+CV;
  RT := DR+CR;
  VT := DV+CV;
  ST := RT+VT;
  OA := FOcorA_Total;
  TT := ST + OA;
  //
  EdDV.Text := Geral.FFT(DV, 2, siPositivo);
  EdDR.Text := Geral.FFT(DR, 2, siPositivo);
  EdDT.Text := Geral.FFT(DT, 2, siPositivo);
  EdCV.Text := Geral.FFT(CV, 2, siPositivo);
  EdCR.Text := Geral.FFT(CR, 2, siPositivo);
  EdCT.Text := Geral.FFT(CT, 2, siPositivo);
  EdRT.Text := Geral.FFT(RT, 2, siPositivo);
  EdVT.Text := Geral.FFT(VT, 2, siPositivo);
  EdST.Text := Geral.FFT(ST, 2, siPositivo);
  EdOA.Text := Geral.FFT(OA, 2, siPositivo);
  EdTT.Text := Geral.FFT(TT, 2, siPositivo);
end;

procedure TFmChequesGer.QrPesqBeforeClose(DataSet: TDataSet);
begin
  QrSoma.Close;
  //
  EdSomaI.Text := '0';
  EdSomaV.Text := '0,00';
  //
  QrAlinIts.Close;
  QrAlinPgs.Close;
  QrLotesPrr.Close;
  QrOcorreu.Close;
  //
  BtHistorico.Enabled   := False;
  BtChequeOcorr.Enabled := False;
  BtStatus.Enabled      := False;
  BtQuitacao.Enabled    := False;
  BtChequeDev.Enabled   := False;
  BtDesDep.Enabled      := False;
  BitBtn1.Enabled       := False;
end;

procedure TFmChequesGer.Gerenciardirio1Click(Sender: TObject);
var
  Texto: String;
begin
  Texto := QrPesqCPF.Value + ' - ' + QrPesqEmitente.Value;
  //
  FmPrincipal.MostraDiarioGer(0, QrPesqCliente.Value, QrPesqControle.Value, Texto);
end;

procedure TFmChequesGer.GradeItensDblClick(Sender: TObject);
begin
  EdControle.Text := IntToStr(QrPesqControle.Value);
  //CGStatusAutoma.Value := Trunc(Power(2, CGStatusAutoma.Items.Count)-1);
  Pesquisa(True, True);
end;

procedure TFmChequesGer.BtReciboAntecipClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMReciboAntecip, BtReciboAntecip);
end;

procedure TFmChequesGer.EmiteReciboAntecip(Tipo: Integer);
var
  Texto: String;
  Valor: Double;
begin
  QrAntePror.Close;
  QrAntePror.Params[0].AsInteger := QrLotesPrrControle.Value;
  QrAntePror.Open;
  Texto := 'Antecipa��o da quita��o do cheque n� ' +
    FormatFloat('000000', QrAnteProrCheque.Value)
    +' do banco n� '+FormatFloat('000', QrAnteProrBanco.Value) + ' ' +
    QrBco1Nome.Value+ ' ag�ncia n� '+ FormatFloat('0000', QrAnteProrAgencia.Value) +
    ' emitida por '+ QrAnteProrEmitente.Value + ' e comprada no dia '+
    FormatDateTime('dd" de "mmmm" de "yyyy', QrAnteProrData1.Value)+
    ', sendo que o vencimento acordado na compra era para '+
    FormatDateTime('dd" de "mmmm" de "yyyy', QrAnteProrData2.Value)+
    ' e foi alterada para '+
    FormatDateTime('dd" de "mmmm" de "yyyy', QrAnteProrData3.Value);
  //
  if QrAnteProrVALORRESSARCIDO.Value < 0 then
  begin
    if Tipo = 1 then
    GOTOy.EmiteRecibo(Dmod.QrMasterDono.Value, QrPesqCliente.Value,
      -QrAnteProrVALORRESSARCIDO.Value, 0, 0, 'ACH.R-'+FormatFloat('000',
      QrAnteProrControle.Value), Texto, '', '', QrAnteProrData3.Value, 0)
    else
    begin
      Valor := Dmod.ObtemValorDeCompraRealizado(QrAnteProrITEMLOTE.Value)[0]
      + QrAnteProrVlrCompra.Value;
      GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value,
        QrAnteProrVALORCHEQUE.Value+Valor+QrAnteProrVALORRESSARCIDO.Value,
        0, 0, 'ACH.P-'+FormatFloat('000',
        QrAnteProrControle.Value), Texto, '', '', QrAnteProrData3.Value, 0);
    end;
  end else Application.MessageBox('Lan�amento deve ser de antecipa��o!',
    'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmChequesGer.Meuparaocliente1Click(Sender: TObject);
begin
  EmiteReciboAntecip(0);
end;

procedure TFmChequesGer.Clienteparamim1Click(Sender: TObject);
begin
  EmiteReciboAntecip(1);
end;

procedure TFmChequesGer.IncluiQuitao1Click(Sender: TObject);
begin
  FNaoDeposita := True;
  MostraEdicao(2, CO_INCLUSAO);
end;

procedure TFmChequesGer.EdCPF_1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    Dmod.QrLocCPF.Close;
    Dmod.QrLocCPF.Params[0].AsString := Geral.SoNumero_TT(EdCPF_1.Text);
    Dmod.QrLocCPF.Open;
    case Dmod.QrLocCPF.RecordCount of
      0: Application.MessageBox('N�o foi localizado emitente para este CPF!',
        'Aviso', MB_OK+MB_ICONWARNING);
      1: EdEmitente1.Text := Dmod.QrLocCPFNome.Value;
      else begin
        Application.CreateForm(TFmLotesEmi, FmLotesEmi);
        FmLotesEmi.ShowModal;
        if FmLotesEmi.FEmitenteNome <> '' then
          EdEmitente1.Text := FmLotesEmi.FEmitenteNome;
        FmLotesEmi.Destroy;
      end;
    end;
  end;
  if Key=VK_F8 then
  begin
    Application.CreateForm(TFmPesqCPFCNPJ, FmPesqCPFCNPJ);
    FmPesqCPFCNPJ.ShowModal;
    FmPesqCPFCNPJ.Destroy;
    if VAR_CPF_PESQ <> '' then EdCPF_1.Text := VAR_CPF_PESQ;
    Dmod.QrLocCPF.Close;
    Dmod.QrLocCPF.Params[0].AsString := Geral.SoNumero_TT(EdCPF_1.Text);
    Dmod.QrLocCPF.Open;
    if Dmod.QrLocCPF.RecordCount = 0 then EdEmitente1.Text := ''
    else EdEmitente1.Text := Dmod.QrLocCPFNome.Value;
  end;
end;

procedure TFmChequesGer.BtHistoricoClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  MyObjects.MostraPopUpDeBotao(PMHistorico, BtHistorico);
end;

procedure TFmChequesGer.EdDesco2Exit(Sender: TObject);
begin
  EdAPCD.SetFocus;
end;

procedure TFmChequesGer.EdTaxaPExit(Sender: TObject);
begin
  BtConfirma5.SetFocus;
end;

procedure TFmChequesGer.MostraHistorico(Quem: Integer);
begin
  Application.CreateForm(TFmHistCliEmi, FmHistCliEmi);
  case Quem of
    1:
    begin
      FmHistCliEmi.EdCliente.Text     := IntToStr(QrPesqCliente.Value);
      FmHistCliEmi.CBCliente.KeyValue := QrPesqCliente.Value;
    end;
    2: FmHistCliEmi.EdCPF1.Text       := QrPesqCNPJ_TXT.Value;
    3:
    begin
      FmHistCliEmi.EdCliente.Text     := IntToStr(QrPesqCliente.Value);
      FmHistCliEmi.CBCliente.KeyValue := QrPesqCliente.Value;
      FmHistCliEmi.EdCPF1.Text        := QrPesqCNPJ_TXT.Value;
    end;
    4:
    begin
      FmHistCliEmi.EdCliente.Text     := EdCliente.Text;
      FmHistCliEmi.CBCliente.KeyValue := Geral.IMV(EdCliente.Text);
    end;
    5: FmHistCliEmi.EdCPF1.Text       := EdCPF_1.Text;
    6:
    begin
      FmHistCliEmi.EdCliente.Text     := EdCliente.Text;
      FmHistCliEmi.CBCliente.KeyValue := Geral.IMV(EdCliente.Text);
      FmHistCliEmi.EdCPF1.Text        := EdCPF_1.Text;
    end;
  end;
  FmHistCliEmi.Pesquisa;
  FmHistCliEmi.ShowModal;
  FmHistCliEmi.Destroy;
end;

procedure TFmChequesGer.Clienteselecionado1Click(Sender: TObject);
begin
  MostraHistorico(1);
end;

procedure TFmChequesGer.Emitentesacadoselecionado1Click(Sender: TObject);
begin
  MostraHistorico(2);
end;

procedure TFmChequesGer.Ambos1Click(Sender: TObject);
begin
  MostraHistorico(3);
end;

procedure TFmChequesGer.Clientepesquisado1Click(Sender: TObject);
begin
  MostraHistorico(4);
end;

procedure TFmChequesGer.Emitentesacadopesquisado1Click(Sender: TObject);
begin
  MostraHistorico(5);
end;

procedure TFmChequesGer.Ambos2Click(Sender: TObject);
begin
  MostraHistorico(6);
end;

procedure TFmChequesGer.InfoTempo(Tempo: TDateTime; Texto: String; Inicial: Boolean);
begin
  if Inicial then
  begin
    FTempo := Tempo;
    FUltim := Tempo;
    Memo1.Lines.Add('');
    Memo1.Lines.Add('==============================================================================');
    Memo1.Lines.Add('');
    Memo1.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss ', Tempo) +
      '- [ Total ] [Unit�ri] '+ Texto);
  end else begin
    Memo1.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss - ', Tempo)+
    FormatDateTime('nn:ss:zzz ', Tempo-FTempo) +
    FormatDateTime('nn:ss:zzz ', Tempo-FUltim) + Texto);
    FUltim := Tempo;
  end;
end;

function TFmChequesGer.TextoSQLPesq(TabLote, TabLoteIts: String; Soma: Boolean;
 Query: TmySQLQuery): Integer;
const
  ListaSQL: array [1..6] of String = (
  (*Devolvido*) ' (li.Devolucao<>0 AND ai.Status in (0,1)) AND li.Quitado>-2 ',
  (*Aberto   *) ' (li.Devolucao=0 AND li.DDeposito>=SYSDATE()) AND li.Quitado>-2 ',
  (*Liquidado*) ' (li.Devolucao=0 AND li.DDeposito<SYSDATE()) AND li.Quitado>-2 ',
  (*Quitado  *) ' (li.Devolucao<>0 AND ai.Status in (2,3)) AND li.Quitado>-2 ',
  (*Baixado  *) ' (li.Quitado=-2) ',
  (*Moroso   *) ' (li.Quitado=-3) ');
var
  i, m, j, Ok, Coligado, Controle: Integer;
  LIGA_OR: Boolean;
begin
  Liga_OR := False;
  Ok := 0;
  if Soma then
  begin
    Query.SQL.Add('SELECT COUNT(li.Controle) Itens, SUM(li.Valor) Valor ');
  end else begin
    Query.SQL.Add('SELECT lo.Codigo, lo.Cliente, li.Cheque, li.Controle, li.Emissao, ');
    Query.SQL.Add('li.TxaCompra, li.DCompra, li.Valor, li.Desco, li.Vencto, ');
    Query.SQL.Add('li.Quitado,li.Devolucao, ai.Status STATUSDEV, li.ProrrVz, ');
    Query.SQL.Add('li.ProrrDd, li.Emitente, li.ValQuit, CASE WHEN co.Tipo=0 THEN ');
    Query.SQL.Add('co.RazaoSocial ELSE co.Nome END NOMECOLIGADO,');
    Query.SQL.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome');
    Query.SQL.Add('END NOMECLIENTE, li.DDeposito, li.CPF, li.NaoDeposita, '); // em.*
    Query.SQL.Add('li.RepCli, li.Banco, li.Agencia, li.Conta, li.Depositado, '); // em.*
    Query.SQL.Add('CASE WHEN cl.Tipo=0 THEN cl.ETe1 ELSE cl.PTe1 END TEL1CLI, ');
    Query.SQL.Add('li.ObsGerais');
  end;
  Query.SQL.Add('FROM '+tabloteits+' li');
  Query.SQL.Add('LEFT JOIN '+tablote+' lo ON lo.Codigo=li.Codigo');
  //Query.SQL.Add('LEFT JOIN emitentes em ON em.CPF=li.CPF');
  Query.SQL.Add('LEFT JOIN repasits  ri ON ri.Origem=li.Controle');
  Query.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  Query.SQL.Add('LEFT JOIN entidades co ON co.Codigo=re.Coligado');
  Query.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  Query.SQL.Add('LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle');
  Query.SQL.Add('WHERE lo.Tipo=0');
  Query.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
    IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  Coligado := Geral.IMV(EdColigado.Text);
  Controle := Geral.IMV(EdControle.Text);
  case CkRepassado.Value of
    0:
    begin
      Application.MessageBox('Defina o status de repassado!', 'Erro',
        MB_OK+MB_ICONERROR);
      Query.SQL.Add('AND li.Repassado = -1000');
    end;
    1:
    begin
      if Coligado = 0 then Query.SQL.Add('AND li.Repassado = 1')
      else begin
        Query.SQL.Add('AND (re.Coligado = '+IntToStr(Coligado)+
        ' AND li.Repassado = 1)')
      end;
    end;
    2: Query.SQL.Add('AND li.Repassado = 0');
    3:
    begin
      if Coligado <> 0 then
      begin
        Query.SQL.Add('AND (li.Repassado = 0 OR (');
        Query.SQL.Add('li.Repassado=1 AND re.Coligado = '+
        IntToStr(Coligado)+'))');
      end else begin
        // Sem SQL - vale qualquer situa��o.
      end;
    end;
  end;
{ 2012-01-22
  if Trim(EdCliente.Text) <> '' then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND lo.Cliente='+EdCliente.Text);
  end;
}
  if EdCliente.ValueVariant <> 0 then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND lo.Cliente=' + Geral.FF0(EdCliente.ValueVariant));
  end;
  //  Fim 2012-01-22
  //
  if Geral.IMV(EdBanco.Text) <> 0 then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND li.Banco='+EdBanco.Text);
  end;
  if Geral.IMV(EdAgencia.Text) <> 0 then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND li.Agencia='+EdAgencia.Text);
  end;
  if Geral.IMV(EdConta.Text) <> 0 then
  begin
    Ok := Ok + 1;
    //Query.SQL.Add('AND li.Conta='+EdConta.Text);
    Query.SQL.Add('AND RIGHT(li.Conta, 6)=RIGHT('+EdConta.Text+', 6)');
  end;
  if Geral.IMV(EdCheque.Text) <> 0 then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND li.Cheque='+EdCheque.Text);
  end;
  if EdCPF_1.Text <> '' then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND li.CPF="'+Geral.SoNumero_TT(EdCPF_1.Text)+'"');
  end;
  if Controle <> 0 then
  begin
    Ok := Ok + 1;
    Query.SQL.Add('AND li.Controle = '+IntToStr(Controle));
  end;

  //

  (*Query.SQL.Add('AND li.Quitado in (' + IntToStr(Fq0) + ', ' +
    IntToStr(Fq1) + ', ' +IntToStr(Fq2) + ', ' +IntToStr(Fq3) + ')');*)

  case CkProrrogado.Value of
    1: Query.SQL.Add('AND li.ProrrVz>0');
    2: Query.SQL.Add('AND li.ProrrVz=0');
    3: ; // Nada
  end;
  //
  i := 1;
  j := 1;
  m := MLAGeral.CheckGroupMaxValue(CkStatus);
  if (CkStatus.Value <> m) and (CkStatus.Value > 0) then
  begin
    Query.SQL.Add('AND (');
    while i <= m do
    begin
      if MLAGeral.IntInConjunto2(i, CkStatus.Value) then
      begin
        if LIGA_OR then Query.SQL.Add('OR');
        Query.SQL.Add(ListaSQL[j]);
        LIGA_OR := True;
      end;
      j := j+ 1;
      i := i * 2;
    end;
    Query.SQL.Add(')');
  end;
 (* // Parei aqui
  case CkStatus.Value of
    1: // Devolvido
    begin
      Query.SQL.Add('AND ' + Devolvido);
    end;
    2: // Aberto
    begin
      Query.SQL.Add('AND ' + Aberto);
    end;
    3: // Devolvido + Aberto
    begin
      Query.SQL.Add('AND ( '+ Devolvido);
      Query.SQL.Add('OR ' + Aberto +')');
    end;
    4: // Liquidados
    begin
      Query.SQL.Add('AND ' + Liquidado);
    end;
    5: // Devolvidos + Liquidados
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Liquidado + ')');
    end;
    6: // Abertos + Liquidados
    begin
      Query.SQL.Add('AND (' + Aberto);
      Query.SQL.Add('OR ' + Liquidado + ')');
    end;
    7: // Devolvidos + Liquidados + Abertos
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Liquidado);
      Query.SQL.Add('OR ' + Aberto + ')');
    end;
    8: // Quitados(Devolv.)
    begin
      Query.SQL.Add('AND ' + Quitado);
    end;
    9: // Devolvido + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Quitado));
    end;
    10: // Aberto + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Aberto);
      Query.SQL.Add('OR ' + Quitado));
    end;
    11: // Aberto + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Aberto);
      Query.SQL.Add('OR ' + Devolvido);
      Query.SQL.Add('OR ' + Quitado + ')');
    end;
    12: // Liquidado + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Liquidado);
      Query.SQL.Add('OR ' + Quitado));
    end;
    13: // Devolvido + Liquidado + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Liquidado);
      Query.SQL.Add('OR ' + Quitado + ')');
    end;
    14: // Aberto + Liquidado + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Aberto);
      Query.SQL.Add('OR ' + Liquidado);
      Query.SQL.Add('OR ' + Quitado + ')');
    end;
    15: // Devolvido + Liquidado + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Aberto);
      Query.SQL.Add('OR ' + Liquidado);
      Query.SQL.Add('OR ' + Quitado + ')');
    end;
    16: // Baixado
    begin
      Query.SQL.Add('AND ' + Baixado);
    end;
    17: // Devolvido + Quitados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Baixado));
    end;
    18: // Aberto + Baixados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Aberto);
      Query.SQL.Add('OR ' + Baixado));
    end;
    19: // Aberto + Baixados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Aberto);
      Query.SQL.Add('OR ' + Devolvido);
      Query.SQL.Add('OR ' + Baixado + ')');
    end;
    20: // Liquidado + Baixados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Liquidado);
      Query.SQL.Add('OR ' + Baixado));
    end;
    21: // Devolvido + Liquidado + Baixados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Liquidado);
      Query.SQL.Add('OR ' + Baixado + ')');
    end;
    22: // Aberto + Liquidado + Baixados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Aberto);
      Query.SQL.Add('OR ' + Liquidado);
      Query.SQL.Add('OR ' + Baixado + ')');
    end;
    23: // Devolvido + Liquidado + Baixados(Devolv.)
    begin
      Query.SQL.Add('AND (' + Devolvido);
      Query.SQL.Add('OR ' + Aberto);
      Query.SQL.Add('OR ' + Liquidado);
      Query.SQL.Add('OR ' + Baixado + ')');
    end;
    // Parei aqui fazer ate 63
  end;*)

  (*if MLAGeral.IntInConjunto2(1, CGStatusManual.Value)
  or MLAGeral.IntInConjunto2(2, CGStatusManual.Value) then
  begin
    LIGA_OR := False;
    if (CGStatusAutoma.Value > 0) and
       (CGStatusAutoma.Value <> Trunc(Power(2, CGStatusAutoma.Items.Count)-1)) then
    begin
      Ok := Ok + 1;
      Query.SQL.Add('AND (');
      if MLAGeral.IntInConjunto2(1, CGStatusAutoma.Value) then
      begin
        Query.SQL.Add('  (li.Devolucao=0 AND li.DDeposito< CURRENT_DATE)');
        LIGA_OR := True;
      end;
      if MLAGeral.IntInConjunto2(2, CGStatusAutoma.Value) then
      begin
        if LIGA_OR then Query.SQL.Add('OR');
        Query.SQL.Add('  (li.Devolucao<>0 AND ai.Status=0)');
        LIGA_OR := True;
      end;
      if MLAGeral.IntInConjunto2(4, CGStatusAutoma.Value) then
      begin
        if LIGA_OR then Query.SQL.Add('OR');
        Query.SQL.Add('  (li.Devolucao=0 AND li.DDeposito>= CURRENT_DATE)');
        LIGA_OR := True;
      end;
      if MLAGeral.IntInConjunto2(8, CGStatusAutoma.Value) then
      begin
        if LIGA_OR then Query.SQL.Add('OR');
        Query.SQL.Add('  (li.Devolucao<>0 AND ai.Status=1)');
        LIGA_OR := True;
      end;
      if (
           (MLAGeral.IntInConjunto2(16, CGStatusAutoma.Value))
           and
           (MLAGeral.IntInConjunto2(32, CGStatusAutoma.Value))
          ) then
      begin
        if LIGA_OR then Query.SQL.Add('OR');
        Query.SQL.Add('  (li.Devolucao<>0 AND ai.Status=2 )');
        LIGA_OR := True;
      end else
      if MLAGeral.IntInConjunto2(16, CGStatusAutoma.Value) then
      begin
        if LIGA_OR then Query.SQL.Add('OR');
        Query.SQL.Add('  (li.Devolucao<>0 AND ai.Status=2 AND (ai.ValPago <= (ai.Valor+ai.Taxas+ai.Multa+ai.JurosV-ai.Desconto)))');
        LIGA_OR := True;
      end else
      if MLAGeral.IntInConjunto2(32, CGStatusAutoma.Value) then
      begin
        if LIGA_OR then Query.SQL.Add('OR');
        Query.SQL.Add('  (li.Devolucao<>0 AND ai.Status=2 AND (ai.ValPago >  (ai.Valor+ai.Taxas+ai.Multa+ai.JurosV-ai.Desconto)))');
        LIGA_OR := True;
      end;
      if LIGA_OR then Query.SQL.Add('OR');
      Query.SQL.Add('  (li.Quitado in (' + IntToStr(Fq2) + ',' + IntToStr(Fq3) + '))');
      Query.SQL.Add(')');
    end;
  end;*)
  Result := Ok;
end;

procedure TFmChequesGer.QrSacCHDevAAfterOpen(DataSet: TDataSet);
begin
  FCHDevOpen_Total := 0;
  while not QrSacCHDevA.Eof do
  begin
    //if Int(Date) > QrCHDevAData1.Value then
      FCHDevOpen_Total := FCHDevOpen_Total + QrSacCHDevAATUAL.Value;
    QrSacCHDevA.Next;
  end;
end;

procedure TFmChequesGer.QrSacOcorAAfterOpen(DataSet: TDataSet);
begin
  (*if QrSacOcorA.RecordCount > 0 then BtPagamento6.Enabled := True
  else BtPagamento6.Enabled := False;*)
  //
  FOcorA_Total := 0;
  while not QrSacOcorA.Eof do
  begin
    //if Int(Date) > QrOcorADataO.Value then
      FOcorA_Total := FOcorA_Total + QrSacOcorAATUALIZADO.Value;
    QrSacOcorA.Next;
  end;
end;

procedure TFmChequesGer.QrSacCHDevACalcFields(DataSet: TDataSet);
begin
  QrSacCHDevADATA1_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrSacCHDevAData1.Value);
  QrSacCHDevADATA2_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrSacCHDevAData2.Value);
  QrSacCHDevADATA3_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrSacCHDevAData3.Value);
  QrSacCHDevACPF_TXT.Value := Geral.FormataCNPJ_TT(QrSacCHDevACPF.Value);
  QrSacCHDevASALDO.Value := QrSacCHDevAValor.Value +QrSacCHDevATaxas.Value +
    QrSacCHDevAMulta.Value + QrSacCHDevAJurosV.Value - QrSacCHDevAValPago.Value -
    QrSacCHDevADesconto.Value - QrSacCHDevAPgDesc.Value;
  //
  QrSacCHDevAATUAL.Value := DMod.ObtemValorAtualizado(
    QrSacCHDevACliente.Value, QrSacCHDevAStatus.Value, QrSacCHDevAData1.Value, Date,
    QrSacCHDevAData3.Value, QrSacCHDevAValor.Value, QrSacCHDevAJurosV.Value,
    QrSacCHDevADesconto.Value, QrSacCHDevAValPago.Value +
    QrSacCHDevAPgDesc.Value, QrSacCHDevAJurosP.Value, False);
end;

procedure TFmChequesGer.QrSacCHDevABeforeClose(DataSet: TDataSet);
begin
  FCHDevOpen_Total := 0;
end;

procedure TFmChequesGer.QrSacDOpenBeforeClose(DataSet: TDataSet);
begin
  EdDV.Text := '';
  EdDR.Text := '';
  EdDT.Text := '';
  EdCV.Text := '';
  EdCR.Text := '';
  EdCT.Text := '';
  EdRT.Text := '';
  EdVT.Text := '';
  EdST.Text := '';
  EdOA.Text := '';
  EdTT.Text := '';
end;

procedure TFmChequesGer.QrSacDOpenCalcFields(DataSet: TDataSet);
begin
  QrSacDOpenSALDO_DESATUALIZ.Value := QrSacDOpenValor.Value + QrSacDOpenTotalJr.Value -
  QrSacDOpenTotalDs.Value - QrSacDOpenTotalPg.Value;
  //
  QrSacDOpenNomeStatus.Value := MLAGeral.NomeStatusPgto2(QrSacDOpenQuitado.Value,
    QrSacDOpenDDeposito.Value, Date, QrSacDOpenData3.Value, QrSacDOpenRepassado.Value);
  //
  QrSacDOpenSALDO_ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrSacDOpenCliente.Value, QrSacDOpenQuitado.Value, QrSacDOpenVencto.Value, Date,
    QrSacDOpenData3.Value, QrSacDOpenValor.Value, QrSacDOpenTotalJr.Value, QrSacDOpenTotalDs.Value,
    QrSacDOpenTotalPg.Value, 0, True);
end;

procedure TFmChequesGer.QrSacRiscoCCalcFields(DataSet: TDataSet);
begin
  QrSacRiscoCCPF_TXT.Value := Geral.FormataCNPJ_TT(QrSacRiscoCCPF.Value);
end;

procedure TFmChequesGer.QrSacOcorABeforeClose(DataSet: TDataSet);
begin
  FOcorA_Total := 0;
end;

procedure TFmChequesGer.QrSacOcorACalcFields(DataSet: TDataSet);
var
  Cliente: Integer;
begin
  if QrSacOcorACLIENTELOTE.Value > 0 then
    Cliente := QrSacOcorACLIENTELOTE.Value else
    Cliente := QrSacOcorACliente.Value;
  //
  QrSacOcorASALDO.Value := QrSacOcorAValor.Value + QrSacOcorATaxaV.Value - QrSacOcorAPago.Value;
  //
  QrSacOcorAATUALIZADO.Value := DMod.ObtemValorAtualizado(
    Cliente, 1, QrSacOcorADataO.Value, Date, QrSacOcorAData3.Value,
    QrSacOcorAValor.Value, QrSacOcorATaxaV.Value, 0 (*Desco*),
    QrSacOcorAPago.Value, QrSacOcorATaxaP.Value, False);
end;

procedure TFmChequesGer.BtDesDepClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  //
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE lotesits SET AlterWeb=1,   Depositado=0 WHERE Controle=:P0 ');
  Dmod.QrUpdM.Params[0].AsInteger := QrPesqControle.Value;
  Dmod.QrUpdM.ExecSQL;
  BtDesDep.Enabled := False;
end;

procedure TFmChequesGer.QrSomaAfterOpen(DataSet: TDataSet);
begin
  FSoma_i := 0;
  FSoma_v := 0;
  //
  while not QrSoma.Eof do
  begin
    FSoma_i := FSoma_i + QrSomaItens.Value;
    FSoma_v := FSoma_v + QrSomaValor.Value;
    QrSoma.Next;
  end;
  EdSomaI.Text := IntToStr(FSoma_i);
  EdSomaV.Text := Geral.FFT(FSoma_v, 2, siNegativo);
end;

procedure TFmChequesGer.N0ForastatusAutomtico1Click(Sender: TObject);
begin
  ForcaStatus(0);
end;

procedure TFmChequesGer.N1Forastatus1Click(Sender: TObject);
begin
  ForcaStatus(-1);
end;

procedure TFmChequesGer.N2ForastatusBaixado1Click(Sender: TObject);
begin
  ForcaStatus(-2);
end;

procedure TFmChequesGer.N3ForastatusMoroso1Click(Sender: TObject);
begin
  ForcaStatus(-3);
end;

procedure TFmChequesGer.ForcaStatus(NovoStatus: Integer);
var
  Controle: Integer;
begin
  if QrPesq.State = dsBrowse then
  begin
    if QrPesq.RecordCount > 0 then
    begin
      Controle := QrPesqControle.Value;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lotesits SET Quitado=:P0');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
      Dmod.QrUpd.Params[0].AsInteger := NovoStatus;
      Dmod.QrUpd.Params[1].AsInteger := Controle;
      Dmod.QrUpd.ExecSQL;
      //
      QrPesq.Close;
      QrPesq.Open;
      QrPesq.Locate('Controle', Controle, []);
    end;
  end;
end;


procedure TFmChequesGer.SpeedButton1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET Teste=0');
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET Teste=1 WHERE Controle=:P0');
  Query1.Close;
  Query1.SQL := QrPesq.SQL;
  Query1.Open;
  Query1.First;
  while not Query1.Eof do
  begin
    Dmod.QrUpd.Params[0].AsInteger := Query1Controle.Value;
    Dmod.QrUpd.ExecSQL;
    Query1.Next;
  end;
  Query1.EnableControls;
  Query1.First;
  Screen.Cursor := crDefault;
end;

procedure TFmChequesGer.CkStatusClick(Sender: TObject);
begin
  Pesquisa(False, False);
end;

end.

