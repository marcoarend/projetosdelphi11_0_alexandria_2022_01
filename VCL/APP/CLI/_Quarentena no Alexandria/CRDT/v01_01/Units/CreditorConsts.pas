unit CreditorConsts;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  (*DBTables,*) StdCtrls, comctrls, mySQLDbTables, dmkGeral;

type
  TCreditorConsts = class(TObject)
  private
    { Private declarations }
  public
    { Public declarations }
    procedure SetaVariaveisIniciais;
    procedure AvisaSetarOpcoes;
  end;

const
  CRD_CAMINHO_REGEDIT = 'Dermatek\Creditor';
  CRD_MAX_EMPRESAS    = 6;
var
  CRD_Consts: TCreditorConsts;

implementation

uses UnMLAGeral, Principal;

procedure TCreditorConsts.AvisaSetarOpcoes;
var
  Caminho: String;
begin
  Caminho := CRD_CAMINHO_REGEDIT + '\Opcoes';
  //
  if not Geral.ReadAppKeyLM('JaSetado', Caminho, ktBoolean, False) then
  begin
    if Geral.MB_Pergunta('O aplicativo detectou que as op��es do ' +
      'aplicativo n�o foram configuradas ainda.' + sLineBreak +
      'Deseja configur�-las agora?') = ID_YES
    then
      FmPrincipal.CadastroDeOpcoesCreditor;
  end;
end;

procedure TCreditorConsts.SetaVariaveisIniciais;
begin
  //
end;

end.
 
