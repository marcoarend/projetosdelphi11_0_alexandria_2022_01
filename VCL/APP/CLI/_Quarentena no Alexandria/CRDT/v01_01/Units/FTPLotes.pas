unit FTPLotes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, lmdrtfbase, lmdrtfrichedit,
  LMDCustomControl, LMDCustomPanel, LMDCustomBevelPanel, LMDCustomParentPanel,
  LMDCustomPanelFill, LMDPanelFill, ComCtrls, CheckLst, Menus, OleCtrls,
  SHDocVw, filectrl, UnMyLinguas;

type
  TFmFTPLotes = class(TForm)
    PainelDados: TPanel;
    PnConfirm: TPanel;
    BtBaixa: TBitBtn;
    BtCancela: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo1: TPanel;
    Label1: TLabel;
    PnCliente: TPanel;
    PainelTitulo: TLMDPanelFill;
    Image1: TImage;
    Memo: TLMDRichEdit;
    PnGerente: TPanel;
    PageControl1: TPageControl;
    TabSheet13: TTabSheet;
    Panel10: TPanel;
    GroupBox2: TGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdURL: TEdit;
    EdLog: TEdit;
    EdPwd: TEdit;
    chkPassive: TCheckBox;
    GroupBox3: TGroupBox;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    edtSocksPort: TEdit;
    edtSocksAddress: TEdit;
    edtSocksPassword: TEdit;
    edtSocksUserCode: TEdit;
    cbxSocksVersion: TComboBox;
    BtCadastro: TButton;
    LogoutBtn: TButton;
    LoginBtn: TButton;
    Panel11: TPanel;
    StaticText1: TStaticText;
    xxxReplyMemo: TMemo;
    TabSheet14: TTabSheet;
    STCaminho: TStaticText;
    Panel12: TPanel;
    lbxFiles: TListBox;
    TabSheet15: TTabSheet;
    Label17: TLabel;
    Label18: TLabel;
    SendMode1: TRadioGroup;
    ReceiveMode1: TRadioGroup;
    FileType1: TRadioGroup;
    TimeoutEdit: TEdit;
    edtRestartAt: TEdit;
    OpenDialog2: TOpenDialog;
    Splitter1: TSplitter;
    ClItens: TCheckListBox;
    BtEnvia: TBitBtn;
    Timer1: TTimer;
    Panel1: TPanel;
    Splitter2: TSplitter;
    Panel2: TPanel;
    btnRetrieve: TButton;
    btnStore: TButton;
    btnDelete: TButton;
    btnAbort: TButton;
    rgDirStyle: TRadioGroup;
    PMDir: TPopupMenu;
    Criar1: TMenuItem;
    Renomear1: TMenuItem;
    Excluir1: TMenuItem;
    BtDir: TButton;
    PnPHP: TPanel;
    Splitter3: TSplitter;
    BtAtualiza: TBitBtn;
    Panel3: TPanel;
    STTempo: TStaticText;
    Memo2: TLMDRichEdit;
    Memo3: TMemo;
    Memo1: TMemo;
    WebBrowser1: TWebBrowser;
    STNavega: TStaticText;
    procedure BtSaidaClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure IpFtpClient1FtpError(Sender: TObject; ErrorCode: Integer;
      const Error: String);
    procedure IpFtpClient1FtpReply(Sender: TObject; ReplyCode: Integer;
      const Reply: String);
    procedure IpFtpClient1FtpStatus(Sender: TObject;
      StatusCode: TIpFtpStatusCode; const Info: String);
    procedure LogoutBtnClick(Sender: TObject);
    procedure btnStoreClick(Sender: TObject);
    procedure btnAbortClick(Sender: TObject);
    procedure btnRetrieveClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure FileType1Click(Sender: TObject);
    procedure TimeoutEditExit(Sender: TObject);
    procedure rgDirStyleClick(Sender: TObject);
    procedure IpFtpDirectoryTree1Change(Sender: TObject; Node: TTreeNode);
    procedure IpFtpDirectoryTree1Changed(Sender: TObject);
    procedure LoginBtnClick(Sender: TObject);
    procedure BtCadastroClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtBaixaClick(Sender: TObject);
    procedure BtEnviaClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure IpFtpClient1Error(Sender: TObject; Socket: Cardinal;
      ErrCode: Integer; const ErrStr: String);
    procedure IpFtpClient1IdleTimeout(Sender: TObject; Socket: Cardinal;
      var Reset: Boolean);
    procedure IpFtpClient1FtpLoginError(Sender: TObject;
      ErrorCode: Integer; const Error: String);
    procedure BtDirClick(Sender: TObject);
    procedure Criar1Click(Sender: TObject);
    procedure Excluir1Click(Sender: TObject);
    procedure Renomear1Click(Sender: TObject);
    procedure IpFtpDirectoryTree1Click(Sender: TObject);
    procedure BtAtualizaClick(Sender: TObject);
    function CriaScriptPHP(Arquivo, Host, User, pwd, DB, SQL: String;
             Campos: Integer): Boolean;
  private
    { Private declarations }
    FFTPStatus: Integer;
    FFTPTempo: TDateTime;
    FFTPSeg: Integer;
    FFileSize: Int64;
    FCountFiles: Integer;
    FFTPNomeArq: String;
    //
    function LoginFTP: Boolean;
    function MudaDiretorio(DirName: String): Boolean;
    function DiretorioFTPExiste(DirName: String; AvisaNaoExiste: Boolean): Boolean;
    function ObtemArquivosPorExtensao(Extensoes: array of String): Integer;
    function ConfirmaAcao: Boolean;
    function Get_File_Size4(const S: string): Int64;
    function DefineUsuarioFTPCadastrado(AvisaErro: Boolean): Boolean;
    //
    procedure EnviaArquivosFTP;
    procedure ReplyMemoLinesAdd(Mensagem: String; AddToMemo: Boolean);
    procedure PreparaConfirmacao(Mensagem: String);
    procedure VerificaArquivosFTP;
    procedure RetrieveNext;
    procedure DeleteNext;
    procedure SaveNext;
    procedure AvisaEspera;
    function GetRemoteFileName(var R : string) : Boolean;
    function GetFileNome(const Caption : string; var FN : string) : Boolean;
    function GetRNNames(var R, N : string) : Boolean;
    procedure GravaAviso(Aviso: String; Memo: TMemo);
    procedure EnviaArquivo(Arquivo: String);
    //
    function VerificaEstrutura(Url, Host, User, pwd, DB: String;
             Memo: TMemo): Boolean;
  public
    PageStream : TMemoryStream;
    GlobAddr : TIpAddrRec;
    FTPData, GetFileName, LastDir : String;

    DirStyle : TIpFtpDirStyle;
    DownSize : LongInt;
    Started, GotList, DownLoading : Boolean;

    DnldFTP : TIpFtpClient;

    FTipoConexao: Integer;
    { Public declarations }
  end;

  var
  FmFTPLotes: TFmFTPLotes;
  FText      : TextFile;
  FTabelas   : TStringList;
  FTabLoc    : TStringList;
  FIncrement : TStringList;
  FCheque    : TStringList;
  FLCampos   : TList;
  FLIndices  : TList;
  FRCampos   : TCampos;
  FRIndices  : TIndices;
  FPergunta  : Boolean;
  FListaSQL  : TStringList;
  FPerfis    : TStringList;
  FUserSets  : TStringList;
  FImpDOS    : TStringList;
  FViuControle: Integer;

implementation

uses Module, Principal, MyListas;


const
  DefCaption = 'FTP: Ftp Cliente';
  FphpSQLDir = 'C:\Dermatek\';
  FphpSQLArq = 'slmydmk.php';

type
  TMultiFileOp = (mfoNone, mfoRetrieve, mfoDelete, mfoSave);

var
  MultiFileOp : TMultiFileOp;


{$R *.DFM}

procedure TFmFTPLotes.BtSaidaClick(Sender: TObject);
begin
  IpFtpClient1.Logout;
  Close;
end;

procedure TFmFTPLotes.BtCancelaClick(Sender: TObject);
begin
  if IpFtpClient1.Abort then
    Screen.Cursor := crDefault;
end;

procedure TFmFTPLotes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmFTPLotes.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToLMDPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
  //ReplyMemo.Width := (Panel11.Width div 3) * 2;
end;

procedure TFmFTPLotes.VerificaArquivosFTP;
begin
  if LoginFTP then
  begin
    if MudaDiretorio('Receber') then
    begin
      FCountFiles := ObtemArquivosPorExtensao(['.SCX']);
      ReplyMemoLinesAdd(IntToStr(FCountFiles) +
        ' aquivos scx encontrados.', True);
      //
      lbxFiles.Update;
      Application.ProcessMessages;
      MultiFileOp := mfoRetrieve;
      FCountFiles := 0;
      RetrieveNext;
      while MultiFileOp <> mfoNone do
      begin
        Application.ProcessMessages;
      end;
      ReplyMemoLinesAdd(IntToStr(FCountFiles) + ' aquivos scx baixados.', True);
      //
      FCountFiles := 0;
      ObtemArquivosPorExtensao(['.SCX']);
      MultiFileOp := mfoDelete;
      DeleteNext;
      while MultiFileOp <> mfoNone do
      begin
        Application.ProcessMessages;
      end;
      ReplyMemoLinesAdd(IntToStr(FCountFiles) + ' aquivos scx exclu�dos.', True);
    end;
  end;
end;

procedure TFmFTPLotes.IpFtpClient1FtpError(Sender: TObject;
  ErrorCode: Integer; const Error: String);
begin
  MessageDlg(Error, mtError, [mbOK], 0);
  Screen.Cursor := crDefault;
end;

procedure TFmFTPLotes.IpFtpClient1FtpReply(Sender: TObject;
  ReplyCode: Integer; const Reply: String);
begin
  ReplyMemoLinesAdd(IntToStr(ReplyCode)+ ' # '+ Reply, False);
  FFTPStatus := ReplyCode;
  //if ReplyCode =  FFTPCodeAtivo then FFTPAtivo := True;
end;

procedure TFmFTPLotes.IpFtpClient1FtpStatus(Sender: TObject;
  StatusCode: TIpFtpStatusCode; const Info: String);
var
  Perc: Double;
  Txt: String;
begin
  case StatusCode of
    fscClose :      STCaminho.Caption := DefCaption;
    fscOpen :       STCaminho.Caption := ' conectado em ' + IpFtpClient1.ServerAddress;
    fscComplete :
    begin
      ReplyMemoLinesAdd('Arquivo exclu�do. '+FFTPNomeArq, True);
      FCountFiles := FCountFiles + 1;
      if (MultiFileOp = mfoDelete) then
        DeleteNext;
    end;
    fscCurrentDir : STCaminho.Caption := Info;
    fscLogin :      STCaminho.Caption := IpFtpClient1.UserName + ' logado em ' + IpFtpClient1.ServerAddress;
    fscLogout :     STCaminho.Caption := IpFtpClient1.UserName + ' deslogado';
    fscDirList :
    begin
      ReplyMemoLinesAdd(Info, False);
      FTPData := Info;
    end;
    fscProgress :
    begin
      if FFileSize = 0 then Perc := 0 else Perc :=
        IpFtpClient1.BytesTransferred / FFileSize * 100;
      if Perc > 0 then
      begin
        Txt := FormatFloat('0.00', Perc) + ' %.';
        if Perc = 100 then FFileSize := 0;
      end else Txt := '';
      STCaminho.Caption := IntToStr(IpFtpClient1.BytesTransferred) +
      ' bytes transferidos de '+IntToStr(FFileSize)+'. '+ Txt;
    end;
    fscTransferOK :
    begin
      FCountFiles := FCountFiles + 1;
      STCaminho.Caption := IntToStr(IpFtpClient1.BytesTransferred) +
        ' - transfer�ncia completa';
      if (MultiFileOp = mfoRetrieve) then
      begin
        ReplyMemoLinesAdd('Download conclu�do. '+FFTPNomeArq, True);
        RetrieveNext;
      end;
      if (MultiFileOp = mfoSave) then
      begin
        ReplyMemoLinesAdd('Upload conclu�do. '+FFTPNomeArq, True);
        SaveNext;
      end;
    end;
    fscTimeout :    ShowMessage('FTP: Tempo expirado');
    else ShowMessage('Comando desconhecido: '+Info);
  end;
end;

function TFmFTPLotes.DefineUsuarioFTPCadastrado(AvisaErro: Boolean): Boolean;
var
  Senha, User, Host: String;
begin
  Result := False;
  if FTipoConexao = 1 then
  begin
    Senha := Dmod.QrControleWeb_FTPs.Value;
    Host  := Dmod.QrControleWeb_FTPh.Value;
    User  := Dmod.QrControleWeb_FTPu.Value;
  end else begin
    Senha := MLAGeral.PWDExDecode(Dmod.QrControleFTP_Pwd.Value, FmPrincipal.FCr3);
    Host  := Dmod.QrControleFTP_Hos.Value;
    User  := Dmod.QrControleFTP_Log.Value
  end;
  if Host = '' then
  begin
    if AvisaErro then Application.MessageBox(PChar('N�o h� host definido para '+
    'conex�o ao FTP!'), 'Erro', MB_OK+MB_ICONERROR);
  end else if User = '' then
  begin
    if AvisaErro then Application.MessageBox(PChar('N�o h� usu�rio definido para '+
    'conex�o ao FTP!'), 'Erro', MB_OK+MB_ICONERROR);
  end else if Senha = '' then
  begin
    if AvisaErro then Application.MessageBox(PChar('N�o h� senha definida para '+
    'conex�o ao FTP!'), 'Erro', MB_OK+MB_ICONERROR);
  end else begin
    EdURL.Text := Host;
    EdLog.Text := User;
    EdPwd.Text := Senha;
    Result := True;
  end;
  if (not Result) and AvisaErro then Application.MessageBox(PChar('N�o foi '+
    'poss�vel conectar ao FTP!'), 'Erro', MB_OK+MB_ICONERROR);
end;

function TFmFTPLotes.LoginFTP: Boolean;
begin
  if (Trim(EdURL.Text) = '') or (Trim(EdLog.Text) = '') or (Trim(EdPwd.Text) = '') then
  begin
    if not DefineUsuarioFTPCadastrado(True) then
    begin
      Application.MessageBox('Dados para conex�o FTP incompletos!', 'Erro',
        MB_OK+MB_ICONERROR);
      Result := False;
      Exit;
    end;
  end;
  Screen.Cursor := crHourGlass;
  try
    IpFtpClient1.SocksServer.Address := edtSocksAddress.Text;
    IpFtpClient1.SocksServer.Password := edtSocksPassword.Text;
    IpFtpClient1.SocksServer.Port := StrToIntDef(edtSocksPort.Text, 1080);
    IpFtpClient1.SocksServer.SocksVersion :=
      TIpSocksVersion(cbxSocksVersion.ItemIndex);
    IpFtpClient1.PassiveMode := chkPassive.Checked;
    //
    PreparaConfirmacao('Fazer login');
    IpFtpClient1.Login(EdURL.Text, EdLog.Text, EdPwd.Text, '' );
    Result := ConfirmaAcao;
  except
    raise;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmFTPLotes.LogoutBtnClick(Sender: TObject);
begin
  rgDirStyle.ItemIndex := Integer(IpFtpDirectoryTree1.DirectoryStyle); {!!.03}
  IpFtpClient1.Logout;
end;

procedure TFmFTPLotes.btnStoreClick(Sender: TObject);
var
  R, L : string;
begin
  if OpenDialog2.Execute then begin
    L := OpenDialog2.Filename;
    R := ExtractFilename(L);
    FFileSize := Get_File_Size4(L);
    if InputQuery(Application.Title, 'Nome do arquivo no servidor remoto', R) then
    begin
      FFTPNomeArq := R;
      IpFtpClient1.Store(R, L, TIpFtpStoreMode(SendMode1.ItemIndex),
        StrToIntDef(edtRestartAt.Text, 0));
    end;
  end;
end;

procedure TFmFTPLotes.btnAbortClick(Sender: TObject);
begin
  IpFtpClient1.Abort;
end;

procedure TFmFTPLotes.btnRetrieveClick(Sender: TObject);
begin
  MultiFileOp := mfoRetrieve;
  RetrieveNext;
end;

procedure TFmFTPLotes.btnDeleteClick(Sender: TObject);
begin
  MultiFileOp := mfoDelete;
  DeleteNext;
end;

procedure TFmFTPLotes.FileType1Click(Sender: TObject);
begin
  IpFtpClient1.FileType := TIpFtpFileType(FileType1.ItemIndex);
end;

procedure TFmFTPLotes.TimeoutEditExit(Sender: TObject);
begin
  IpFtpClient1.TransferTimeout := StrToIntDef(TimeoutEdit.Text, 1092);
end;

procedure TFmFTPLotes.rgDirStyleClick(Sender: TObject);
begin
  if( IpFtpDirectoryTree1.DirectoryStyle <>
                                     TIpFtpDirStyle( rgDirStyle.ItemIndex ))then
    IpFtpDirectoryTree1.DirectoryStyle := TIpFtpDirStyle(rgDirStyle.ItemIndex); {!!.03}
end;

procedure TFmFTPLotes.IpFtpDirectoryTree1Change(Sender: TObject;
  Node: TTreeNode);
begin
  Screen.Cursor := crHourGlass;
end;

procedure TFmFTPLotes.IpFtpDirectoryTree1Changed(Sender: TObject);
begin
  IpFtpDirectoryTree1.FullExpand;
  if( rgDirStyle.ItemIndex <> Integer( IpFtpDirectoryTree1.DirectoryStyle ))then
    rgDirStyle.ItemIndex := Integer( IpFtpDirectoryTree1.DirectoryStyle ); {!!.03}
  Screen.Cursor := crDefault;
end;

procedure TFmFTPLotes.LoginBtnClick(Sender: TObject);
begin
  LoginFTP;
end;

procedure TFmFTPLotes.BtCadastroClick(Sender: TObject);
begin
  DefineUsuarioFTPCadastrado(True);
end;

procedure TFmFTPLotes.FormCreate(Sender: TObject);
begin
  FFTPNomeArq := '';
  PageControl1.ActivePageIndex := 0;
  PageControl1.Align := alClient;
  Memo.Align := alClient;
  Memo1.Align := alClient;
  Panel3.DoubleBuffered := True;
  FTipoConexao := 0;
  ForceDirectories(FphpSQLDir);
end;

procedure TFmFTPLotes.RetrieveNext; { download next file }
var
  i : Integer;
  s : string;
begin
  if (lbxFiles.SelCount = 0) then
    MultiFileOp := mfoNone
  else
    for i := Pred(lbxFiles.Items.Count) downto 0 do
      if lbxFiles.Selected[i] then begin
        s := FmPrincipal.FDirR;
        if (s[Length(s)] <> '\') then
          s := s + '\';
        FFTPNomeArq := lbxFiles.Items[i];
        IpFtpClient1.Retrieve(lbxFiles.Items[i], s + lbxFiles.Items[i],
          TIpFtpRetrieveMode(SendMode1.ItemIndex), StrToIntDef(edtRestartAt.Text, 0));
        lbxFiles.Selected[i] := False;
        Break;
      end;
end;

procedure TFmFTPLotes.DeleteNext; { delete next file }
var
  i : Integer;
begin
  if (lbxFiles.SelCount = 0) then
    MultiFileOp := mfoNone
  else
    for i := Pred(lbxFiles.Items.Count) downto 0 do
      if lbxFiles.Selected[i] then
      begin
        FFTPNomeArq := lbxFiles.Items[i];
        IpFtpClient1.Delete(lbxFiles.Items[i]);
        lbxFiles.Items.Delete(i);
        Break;
      end;
end;

function TFmFTPLotes.MudaDiretorio(DirName: String): Boolean;
begin
  if DiretorioFTPExiste(DirName, True) then
  begin
    PreparaConfirmacao('Mudar diret�rio para '+DirName);
    IpFtpClient1.ChangeDir(DirName);
    Result := ConfirmaAcao;
  end else Result := False;
  if not Result then Application.MessageBox('Erro ao mudar de diret�rio',
    'Erro', MB_OK+MB_ICONERROR);
end;

function TFmFTPLotes.DiretorioFTPExiste(DirName: String;
  AvisaNaoExiste: Boolean): Boolean;
var
  i : Integer;
begin
  Result := False;
  for i := Pred(lbxFiles.Items.Count) downto 0 do
    if lbxFiles.Items[i] = DirName then Result := True;
  if not Result and AvisaNaoExiste then Application.MessageBox(
    'Diret�rio n�o encontrado', 'Erro', MB_OK+MB_ICONERROR);
    (*if lbxFiles.Selected[i] then begin
      s := GetCurrentDir;
      if (s[Length(s)] <> '\') then
        s := s + '\';
      IpFtpClient1.Retrieve(lbxFiles.Items[i], s + lbxFiles.Items[i],
        TIpFtpRetrieveMode(SendMode1.ItemIndex), StrToIntDef(edtRestartAt.Text, 0));
      lbxFiles.Selected[i] := False;
      Break;
    end;
  Result := ?*)
end;

(*function TFmFTPLotes.ContaArquivosPorExtensao(Extensoes: array of String): Integer;
var
  Conta, i, j, p, r, n: Integer;
  Nome, Ext, Exl: String;
begin
  Conta := 0;
  for i := Pred(lbxFiles.Items.Count) downto 0 do
  begin
    Ext := Uppercase(ExtractFileExt(lbxFiles.Items[i]));
    n := High(Extensoes);
    for j := 0 to n do
    begin
      Exl := Uppercase(Extensoes[j]);
      if Ext = Exl then
      begin
        Conta := Conta + 1;
        Break;
      end;
    end;
  end;
  Result := Conta;
end;*)

function TFmFTPLotes.ObtemArquivosPorExtensao(Extensoes: array of String): Integer;
var
  Conta, i, j, n: Integer;
  Ext, Exl: String;
begin
  Conta := 0;
  for i := Pred(lbxFiles.Items.Count) downto 0 do lbxFiles.Selected[i] := False;
  for i := Pred(lbxFiles.Items.Count) downto 0 do
  begin
    Ext := Uppercase(ExtractFileExt(lbxFiles.Items[i]));
    n := High(Extensoes);
    for j := 0 to n do
    begin
      Exl := Uppercase(Extensoes[j]);
      if Ext = Exl then
      begin
        Conta := Conta + 1;
        lbxFiles.Selected[i] := True;
        Break;
      end;
    end;
  end;
  Result := Conta;
end;

(*function TFmFTPLotes.BaixaArquivoPorExtensao(Extensoes: array of String;
  DestDir: String): Integer;
var
  i, j, n: Integer;
  Ext, Exl: String;
begin
  Result := 0;
  for i := Pred(lbxFiles.Items.Count) downto 0 do
    lbxFiles.Selected[i] := False;
  for i := Pred(lbxFiles.Items.Count) downto 0 do
  begin
    Ext := Uppercase(ExtractFileExt(lbxFiles.Items[i]));
    n := High(Extensoes);
    for j := 0 to n do
    begin
      Exl := Uppercase(Extensoes[j]);
      if Ext = Exl then
      begin
        lbxFiles.Selected[i] := True;
        MultiFileOp := mfoRetrieve;
        RetrieveNext;
        //
        lbxFiles.Selected[i] := True;
        MultiFileOp := mfoDelete;
        DeleteNext;
        //
        Result := 1;
      end;
    end;
  end;
end;*)

function TFmFTPLotes.ConfirmaAcao: Boolean;
var
  seg: Integer;
begin
  while not (FFTPStatus = FFTPCodeAtivo) do
  begin
    Seg := Trunc((Now - FFTPTempo) * 86400000);
    if Seg <> FFTPSeg then
    begin
      FFTPSeg := Seg;
      StTempo.Caption := 'Aguarde... (Tempo: '+ IntToStr(Seg div 1000)+' segundos).';
      StTempo.Invalidate;
    end;
    if Seg > FMaxTime then Break;
    Application.ProcessMessages;
  end;
  FFTPTempo := 0;
  StTempo.Caption := '...';
  Result := FFTPStatus = FFTPCodeAtivo;
  //Result := FFTPAtivo;
  //FFTPAtivo := False;
end;

procedure TFmFTPLotes.PreparaConfirmacao(Mensagem: String);
begin
  if Mensagem <> '' then
  begin
    ReplyMemoLinesAdd('##### ' + Mensagem, False);
    Memo.Lines.Add(Mensagem);
  end;
  //FFTPAtivo := False;
  FFTPStatus := 0;
  FFTPTempo := Now;
end;

(*function TFmFTPLotes.Get_File_Size3(const FileName: string): TULargeInteger;
// by nico
var
  Find: THandle;
  Data: TWin32FindData;
begin
  Result.QuadPart := -1;
  Find := FindFirstFile(PChar(FileName), Data);
  if (Find <> INVALID_HANDLE_VALUE) then
  begin
    Result.LowPart  := Data.nFileSizeLow;
    Result.HighPart := Data.nFileSizeHigh;
    Windows.FindClose(Find);
  end;
end;*)

function TFmFTPLotes.Get_File_Size4(const S: string): Int64;
var
  FD: TWin32FindData;
  FH: THandle;
begin
  FH := FindFirstFile(PChar(S), FD);
  if FH = INVALID_HANDLE_VALUE then Result := 0
  else
    try
      Result := FD.nFileSizeHigh;
      Result := Result shl 32;
      Result := Result + FD.nFileSizeLow;
    finally
      //CloseHandle(FH);
    end;
end;

procedure TFmFTPLotes.ReplyMemoLinesAdd(Mensagem: String; AddToMemo: Boolean);
begin
  if AddToMemo then
    Memo.Lines.Add(Mensagem);
  Label1.Caption := Mensagem;
  Label1.Update;
  Application.ProcessMessages;
end;

procedure TFmFTPLotes.BtBaixaClick(Sender: TObject);
begin
  DefineUsuarioFTPCadastrado(True);
  VerificaArquivosFTP;
end;

procedure TFmFTPLotes.BtEnviaClick(Sender: TObject);
begin
  if DefineUsuarioFTPCadastrado(True) then
    EnviaArquivosFTP;
end;

procedure TFmFTPLotes.Timer1Timer(Sender: TObject);
begin
  AvisaEspera;
end;

procedure TFmFTPLotes.AvisaEspera;
begin
  if FFTPTempo <> 0 then
  begin
    StTempo.Caption := 'Aguarde... (Tempo: '+
      IntToStr(Trunc((Now - FFTPTempo) * 86400)) + ' segundos).';
    StTempo.Invalidate;
    Application.ProcessMessages;
  end else begin
    if StTempo.Caption <> '...' then
    begin
      StTempo.Caption := '...';
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmFTPLotes.IpFtpClient1Error(Sender: TObject; Socket: Cardinal;
  ErrCode: Integer; const ErrStr: String);
begin
  ReplyMemoLinesAdd('Erro: '+ IntToStr(ErrCode)+ ' # '+
  'Socket: '+IntToStr(Socket)+' # '+MLAGeral.ErroDeSocket(ErrCode, ErrStr), False);
  Screen.Cursor := crDefault;
end;

procedure TFmFTPLotes.IpFtpClient1IdleTimeout(Sender: TObject;
  Socket: Cardinal; var Reset: Boolean);
begin
  ReplyMemoLinesAdd('Socket: '+IntToStr(Socket)+' # '+'Tempo esgotado.', True);
  Screen.Cursor := crDefault;
end;

procedure TFmFTPLotes.IpFtpClient1FtpLoginError(Sender: TObject;
  ErrorCode: Integer; const Error: String);
begin
  ReplyMemoLinesAdd('Erro: '+ IntToStr(ErrorCode)+ ' # '+
  MLAGeral.ErroDeSocket(ErrorCode, Error), False);
  Screen.Cursor := crDefault;
end;

procedure TFmFTPLotes.EnviaArquivosFTP;
var
  R, L : string;
  i: Integer;
begin
  if LoginFTP then
  begin
    if MudaDiretorio('Enviar') then
    begin
      if MudaDiretorio(Dmod.QrClientePastaTxtFTP.Value) then
      begin
        FFTPTempo := Now;
        for i := 0 to ClItens.Items.Count - 1 do
        begin
          if ClItens.Checked[i] then
          begin
            L := ClItens.Items[i];
            ReplyMemoLinesAdd('Enviando arquivo "'+L+'"', True);
            R := ExtractFilename(L);
            FFileSize := Get_File_Size4(L);
            PreparaConfirmacao('');
            FFTPNomeArq := R;
            IpFtpClient1.Store(R, L, TIpFtpStoreMode(SendMode1.ItemIndex),
              StrToIntDef(edtRestartAt.Text, 0));
            ConfirmaAcao;
            ClItens.Checked[i] := False;
          end;
        end;
        FFTPTempo := 0;
      end; // else n�o achou pasta cliente
    end; // else n�o achou Receber
  end;
end;

procedure TFmFTPLotes.SaveNext; { upload next file }
var
  i, n: Integer;
  R, L : string;
begin
  n := 0;
  for i := 0 to ClItens.Items.Count -1 do
    if ClItens.Checked[i] then
       n := n + 1;
  if n = 0 then
    MultiFileOp := mfoNone
  else begin
    for i := 0 to ClItens.Items.Count -1 do
    begin
      if ClItens.Checked[i] then
      begin
        L := ClItens.Items[i];
        R := ExtractFilename(L);
        FFileSize := Get_File_Size4(L);
        FFTPNomeArq := R;
        IpFtpClient1.Store(R, L, TIpFtpStoreMode(SendMode1.ItemIndex),
          StrToIntDef(edtRestartAt.Text, 0));
      end;
    end;
  end;
end;

procedure TFmFTPLotes.BtDirClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDir, BtDir);
end;

procedure TFmFTPLotes.Criar1Click(Sender: TObject);
var
  Pasta: String;
begin
  Pasta := '';
  if InputQuery('Nova Pasta FTP', 'Digite o nome da pasta:', Pasta) then
  begin
    if Pasta <> '' then
    begin
      Pasta := Trim(Pasta);
      IpFtpClient1.MakeDir(Pasta);
    end;
  end;
end;

procedure TFmFTPLotes.Excluir1Click(Sender: TObject);
var
  R : string;
begin
  if GetRemoteFileName(R) then
    IpFtpClient1.SendFtpCommand('RMD '+R);
end;

function TFmFTPLotes.GetRemoteFileName(var R : string) : Boolean;
begin
  Result := InputQuery(Application.Title, 'Informe o arquivo/diret�rio a excluir', R);
end;

function TFmFTPLotes.GetFileNome(const Caption : string; var FN : string) : Boolean;
begin
  Result := InputQuery('FtpDemo', Caption, FN);
end;

function TFmFTPLotes.GetRNNames(var R, N : string) : Boolean;
begin
  Result := False;
  if GetFileNome('Arquivo / diret�rio remoto:', R) then
    Result := GetFileNome('Novo nome:', N);
end;

procedure TFmFTPLotes.Renomear1Click(Sender: TObject);
var
  R, N : string;
begin
  if GetRNNames(R, N) then
    IpFtpClient1.Rename(R, N);
end;

procedure TFmFTPLotes.IpFtpDirectoryTree1Click(Sender: TObject);
begin
  (*Screen.Cursor := crHourGlass;
  IpFtpDirectoryTree1.FullExpand;
  if( rgDirStyle.ItemIndex <> Integer( IpFtpDirectoryTree1.DirectoryStyle ))then
    rgDirStyle.ItemIndex := Integer( IpFtpDirectoryTree1.DirectoryStyle ); {!!.03}
  Screen.Cursor := crDefault;*)
end;

procedure TFmFTPLotes.BtAtualizaClick(Sender: TObject);
var
  Url, Host, User, pwd, DB: String;
begin
  DefineUsuarioFTPCadastrado(True);
  if LoginFTP then
  begin
    Url  := Dmod.QrControleWeb_Page.Value;
    Host := Dmod.QrControleWeb_Host.Value;
    User := Dmod.QrControleWeb_User.Value;
    pwd  := Dmod.QrControleWeb_Pwd.Value;
    DB   := Dmod.QrControleWeb_DB.Value;
    VerificaEstrutura(Url, Host, User, pwd, DB, Memo1);

    // Parei Aqui
  end;
end;

function TFmFTPLotes.VerificaEstrutura(Url, Host, User, pwd, DB: String;
Memo: TMemo): Boolean;
var
  (*i, Resp: Integer;
  Tabela, Item,*)
  Arq: String;
begin
  //if DataBase = Dmod.MyDB then FViuControle := 0 else FViuControle := 3;
  Result := True;
  GravaAviso(Format(ivMsgEstruturaBD, [DB]), Memo);
  GravaAviso(FormatDateTime(ivFormatIni, Now), Memo);
  try
    FTabelas := TStringList.Create;
    MyList.CriaListaTabelas(nil, FTabelas);
    try
      Arq := FphpSQLDir+FphpSQLArq;
      FFTPNomeArq := ExtractFilename(Arq);
      CriaScriptPHP(Arq, Host, User, pwd, DB, 'SHOW TABLES FROM '+db, 1);
      EnviaArquivo(Arq);
      StNavega.Caption := Url+'/'+FphpSQLArq;
      Webbrowser1.Navigate(StNavega.Caption);

      (*
      Dmod.QrMas.Close;
      Dmod.QrMas.Database := DataBase;
      Dmod.QrMas.SQL.Clear;
      Dmod.QrMas.SQL.Add('SHOW TABLES FROM '+database.databasename);
      Dmod.QrMas.Open;
      while not Dmod.QrMas.Eof do
      begin
        // Fazer Primeiro controle !!!
        if FViuControle = 0 then
        begin
          Tabela := 'Controle';
          FViuControle := 1;
          try
            Dmod.QrControle.Close;
            Dmod.QrControle.Open;
          except
            Tabela := Dmod.QrMas.Fields[0].AsString;
          end;
        end else if FViuControle = 1 then
        begin
          Dmod.QrMas.Prior;
          FViuControle := 2;
          Tabela := Dmod.QrMas.Fields[0].AsString;
        end else Tabela := Dmod.QrMas.Fields[0].AsString;
        //ChangeFileExt(FArquivos[i], myco_);
        if not TabelaAtiva(Tabela) then
        begin
          Item := Format(ivTabela_Nome, [Tabela]);
          if MyList.ExcluiTab then
          begin
            if not FPergunta then Resp := ID_YES else
            Resp := Application.MessageBox(PChar(Format(ivExclTabela,
            [Application.Title, Tabela])), PChar(ivTitAvis),
            MB_YESNOCANCEL+MB_ICONWARNING);
            if Resp = ID_YES then
            begin
              try
                Dmod.QrSQL.Close;
                Dmod.QrSQL.DataBase := Database;
                Dmod.QrSQL.SQL.Clear;
                Dmod.QrSQL.SQL.Add('DROP TABLE IF EXISTS '+Tabela);
                Dmod.QrSQL.ExecSQL;
                GravaAviso(Item+ivMsgExcluida, Memo);
              except
                GravaAviso(Item+ivMsgERROExcluir, Memo);
              end;
            end else begin
              GravaAviso(Item+ivAbortExclUser, Memo);
              if Resp = ID_CANCEL then
              begin
                GravaAviso('VERIFICA��O ABORTADA ANTES DO T�RMINO.', Memo);
                Exit;
              end;
            end;
          end;
        end else begin
          if VerificaCampos(DataBase, Tabela, Memo) <> rvOK then
          begin
            GravaAviso('VERIFICA��O ABORTADA ANTES DO T�RMINO.', Memo);
            Exit;
          end;
        end;
        Dmod.QrMas.Next;
      end;
      for i := 0 to FTabelas.Count -1 do
        if not TabelaExiste(FTabelas[i]) then
          CriaTabela(Database, FTabelas[i], Memo, actCreate);
      Dmod.QrMas.Close;*)
    finally
      FTabelas.Free;
    end;
    GravaAviso(Format(ivMsgFimAnalBD, [DB]), Memo);
    GravaAviso(FormatDateTime(ivFormatFim, Now), Memo);
  except
    Result := False;
    //CloseFile(FText);
    Application.MessageBox(PChar(Format(ivMsgFimAnalBD, [DB])),
    PChar(ivTitErro), MB_OK+MB_ICONERROR);
  end;
end;

function TFmFTPLotes.CriaScriptPHP(Arquivo, Host, User, pwd, DB, SQL: String;
Campos: Integer): Boolean;
var
  i: Integer;
  t: String;
begin
  Memo3.Lines.Clear;
  Memo3.Lines.Add('<?php');
  Memo3.Lines.Add('$Serv = "'+Host+'";');
  Memo3.Lines.Add('$User = "'+User+'";');
  Memo3.Lines.Add('$Aces = "'+pwd +'";');
  Memo3.Lines.Add('$DaBa = "'+DB  +'";');
  Memo3.Lines.Add('$Cone = mysql_connect($Serv, $User, $Aces);');
  Memo3.Lines.Add('  mysql_select_db($DaBa);');
  Memo3.Lines.Add('  $resp = mysql_query("'+SQL+'");');
  Memo3.Lines.Add('  $num_linhas = mysql_num_rows($resp);');
  Memo3.Lines.Add('  for($i=0; $i<$num_linhas; $i++)');
  Memo3.Lines.Add('  {');
  Memo3.Lines.Add('    $dados = mysql_fetch_row($resp);');
  t := '    echo "';
  for i := 0 to Campos -1 do
    t := t + '$dados['+IntToStr(i)+'] <br>";';
  Memo3.Lines.Add(t);
  Memo3.Lines.Add('  }');
  Memo3.Lines.Add('mysql_close($Cone);');
  Memo3.Lines.Add('?>');
  Memo3.Lines.SaveToFile(Arquivo);
  //
  Result := True;
end;

procedure TFmFTPLotes.EnviaArquivo(Arquivo: String);
var
  R, L : string;
begin
  L := Arquivo;
  R := ExtractFilename(L);
  FFileSize := Get_File_Size4(L);
  IpFtpClient1.Store(R, L, TIpFtpStoreMode(SendMode1.ItemIndex),
    StrToIntDef(edtRestartAt.Text, 0));
end;

procedure TFmFTPLotes.GravaAviso(Aviso: String; Memo: TMemo);
begin
  if Memo <> nil then Memo.Lines.Add(Aviso);
  //WriteLn(FText, Aviso);
end;

end.

