unit LotesCartDep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral;

type
  TFmLotesCartDep = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasAgencia1: TIntegerField;
    QrCarteirasConta1: TWideStringField;
    DsCarteiras: TDataSource;
    EdCartDep: TdmkEditCB;
    LaCartDep1: TLabel;
    CBCartDep: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmLotesCartDep: TFmLotesCartDep;

implementation

uses Module, Lotes1, Principal, UnMyObjects;

{$R *.DFM}

procedure TFmLotesCartDep.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLotesCartDep.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmLotesCartDep.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLotesCartDep.FormCreate(Sender: TObject);
begin
  QrCarteiras.Open;
end;

procedure TFmLotesCartDep.BtOKClick(Sender: TObject);
var
  Controle: Integer;
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE LotesIts SET CartDep=:P0 WHERE Codigo=:P1');
  Dmod.QrUpd.Params[00].AsInteger := Geral.IMV(EdCartDep.Text);
  case FmPrincipal.FFormLotesShow of
{
    0:
    begin
      Dmod.QrUpd.Params[01].AsInteger := FmLotes0.QrLotesCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      if FmLotes0.QrLotesIts.State = dsBrowse then
      begin
        Controle := FmLotes0.QrLotesItsControle.Value;
        FmLotes0.QrLotesIts.Close;
        FmLotes0.QrLotesIts.Open;
        FmLotes0.QrLotesIts.Locate('Controle', Controle, []);
      end;
    end;
}
    1:
    begin
      Dmod.QrUpd.Params[01].AsInteger := FmLotes1.QrLotesCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      if FmLotes1.QrLotesIts.State = dsBrowse then
      begin
        Controle := FmLotes1.QrLotesItsControle.Value;
        FmLotes1.QrLotesIts.Close;
        FmLotes1.QrLotesIts.Open;
        FmLotes1.QrLotesIts.Locate('Controle', Controle, []);
      end;
    end;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (13)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  Close;
end;

end.
