unit Ocorreu1;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, Grids,
  DBGrids, Mask, ComCtrls, Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, UnDmkProcFunc, UnDmkEnums;

type
  TFmOcorreu1 = class(TForm)
    PainelDados: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    PainelData: TPanel;
    DBGrid1: TDBGrid;
    QrPesq: TmySQLQuery;
    QrOcorreu: TmySQLQuery;
    DsOcorreu: TDataSource;
    QrOcorreuCodigo: TIntegerField;
    QrOcorreuLotesIts: TIntegerField;
    QrOcorreuDataO: TDateField;
    QrOcorreuOcorrencia: TIntegerField;
    QrOcorreuValor: TFloatField;
    QrOcorreuLoteQuit: TIntegerField;
    QrOcorreuLk: TIntegerField;
    QrOcorreuDataCad: TDateField;
    QrOcorreuDataAlt: TDateField;
    QrOcorreuUserCad: TIntegerField;
    QrOcorreuUserAlt: TIntegerField;
    QrOcorreuNOMEOCORRENCIA: TWideStringField;
    PainelPesq: TPanel;
    Label75: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdDuplicata: TdmkEdit;
    BtPesquisa: TBitBtn;
    Panel3: TPanel;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DsPesq: TDataSource;
    DBEdit2: TDBEdit;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label6: TLabel;
    Label7: TLabel;
    DBEdit6: TDBEdit;
    Panel4: TPanel;
    Label8: TLabel;
    DBEdit7: TDBEdit;
    QrPesqEmissao: TDateField;
    QrPesqTxaCompra: TFloatField;
    QrPesqDCompra: TDateField;
    QrPesqValor: TFloatField;
    QrPesqDesco: TFloatField;
    QrPesqVencto: TDateField;
    QrPesqCNPJ: TWideStringField;
    QrPesqNome: TWideStringField;
    QrPesqRua: TWideStringField;
    QrPesqCompl: TWideStringField;
    QrPesqBairro: TWideStringField;
    QrPesqCidade: TWideStringField;
    QrPesqUF: TWideStringField;
    QrPesqCEP: TIntegerField;
    QrPesqTel1: TWideStringField;
    QrPesqRisco: TFloatField;
    QrPesqIE: TWideStringField;
    Label9: TLabel;
    DBEdit8: TDBEdit;
    Label10: TLabel;
    DBEdit9: TDBEdit;
    Label11: TLabel;
    DBEdit10: TDBEdit;
    Label12: TLabel;
    DBEdit11: TDBEdit;
    Label13: TLabel;
    DBEdit12: TDBEdit;
    Label14: TLabel;
    DBEdit13: TDBEdit;
    Label15: TLabel;
    DBEdit14: TDBEdit;
    Label16: TLabel;
    DBEdit15: TDBEdit;
    Label17: TLabel;
    DBEdit16: TDBEdit;
    Label18: TLabel;
    DBEdit17: TDBEdit;
    Label19: TLabel;
    DBEdit18: TDBEdit;
    QrPesqCNPJ_TXT: TWideStringField;
    QrPesqNUMERO_TXT: TWideStringField;
    QrPesqCEP_TXT: TWideStringField;
    QrPesqTEL1_TXT: TWideStringField;
    QrPesqTAXA_COMPRA: TFloatField;
    QrPesqControle: TIntegerField;
    QrOcorBank: TmySQLQuery;
    DsOcorBank: TDataSource;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    QrOcorBankLk: TIntegerField;
    QrOcorBankDataCad: TDateField;
    QrOcorBankDataAlt: TDateField;
    QrOcorBankUserCad: TIntegerField;
    QrOcorBankUserAlt: TIntegerField;
    PainelChange: TPanel;
    PainelEdit: TPanel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    PanelFill1: TPanel;
    LaTipo: TLabel;
    EdOcorrencia: TdmkEditCB;
    CBOcorrencia: TdmkDBLookupComboBox;
    TPDataO: TDateTimePicker;
    EdValor: TdmkEdit;
    GradeItens: TDBGrid;
    PainelBtns: TPanel;
    PainelCtrl: TPanel;
    PainelBtsE: TPanel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    PainelConf: TPanel;
    BtConfirma5: TBitBtn;
    BtDesiste5: TBitBtn;
    QrPesqDuplicata: TWideStringField;
    Timer1: TTimer;
    QrOcorBankBase: TFloatField;
    QrPesqNumero: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdDuplicataChange(Sender: TObject);
    procedure QrPesqCalcFields(DataSet: TDataSet);
    procedure BtDesiste5Click(Sender: TObject);
    procedure BtConfirma5Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure EdOcorrenciaExit(Sender: TObject);
    procedure QrOcorreuAfterOpen(DataSet: TDataSet);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrPesqAfterScroll(DataSet: TDataSet);
    procedure BtPesquisaClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    procedure MostraEdicao(Acao: Integer; Titulo: String);
    procedure ReopenOcorreu(Codigo: Integer);
    procedure ReabilitaTimer1;
    procedure Pesquisa;
  public
    { Public declarations }
    FCliente: Integer;
  end;

  var
  FmOcorreu1: TFmOcorreu1;

implementation

{$R *.DFM}

uses Module, UnInternalConsts, UMySQLModule, UnMyObjects;

procedure TFmOcorreu1.ReabilitaTimer1;
begin
  PainelData.Visible := False;
  PainelEdit.Visible := False;
  Timer1.Enabled := True;
end;

procedure TFmOcorreu1.MostraEdicao(Acao: Integer; Titulo: String);
begin
  LaTipo.Caption := Titulo;
  case Acao of
    0:
    begin
      GradeItens.Visible := True;
      PainelCtrl.Visible := True;
      PainelPesq.Enabled := True;
      PainelConf.Visible := False;
      PainelEdit.Visible := False;
    end;
    1:
    begin
      PainelEdit.Visible := True;
      PainelConf.Visible := True;
      GradeItens.Visible := False;
      PainelCtrl.Visible := False;
      PainelPesq.Enabled := False;
      if Titulo = CO_INCLUSAO then
      begin
        EdOcorrencia.Text := '';
        CBOcorrencia.KeyValue := NULL;
        TPDataO.Date := Date;
        EdValor.Text := '';
      end else begin
        EdOcorrencia.Text := IntToStr(QrOcorreuOcorrencia.Value);
        CBOcorrencia.KeyValue := QrOcorreuOcorrencia.Value;
        TPDataO.Date := QrOcorreuDataO.Value;
        EdValor.Text := Geral.FFT(QrOcorreuValor.Value, 2, siPositivo);
      end;
      EdOcorrencia.SetFocus;
    end;
  end;
end;

procedure TFmOcorreu1.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOcorreu1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if FCliente > 0 then
  begin
    EdCliente.Text := IntToStr(FCliente);
    CBCliente.KeyValue := FCliente;
  end;
end;

procedure TFmOcorreu1.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmOcorreu1.EdClienteChange(Sender: TObject);
begin
  ReabilitaTimer1;
end;

procedure TFmOcorreu1.FormCreate(Sender: TObject);
begin
  QrClientes.Open;
  QrOcorBank.Open;
  PainelData.Align := alClient;
  PainelBtsE.Align := alClient;
  GradeItens.Align := alClient;
end;

procedure TFmOcorreu1.EdDuplicataChange(Sender: TObject);
begin
  ReabilitaTimer1;
end;

procedure TFmOcorreu1.Pesquisa;
var
  Cliente: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Cliente := Geral.IMV(EdCliente.Text);
    QrPesq.Close;
    QrPesq.SQL.Clear;
    QrPesq.SQL.Add('SELECT li.Controle, li.Emissao, li.TxaCompra,');
    QrPesq.SQL.Add('li.DCompra, li.Valor, li.Desco, li.Vencto, ');
    QrPesq.SQL.Add('li.Duplicata, sa.Numero+0.000 Numero, sa.*');
    QrPesq.SQL.Add('FROM lotesits li');
    QrPesq.SQL.Add('LEFT JOIN lotes lo ON lo.Codigo=li.Codigo');
    QrPesq.SQL.Add('LEFT JOIN sacados sa ON sa.CNPJ=li.CPF');
    QrPesq.SQL.Add('WHERE lo.Tipo=1');
    if Cliente <> 0 then
      QrPesq.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
    if Trim(EdDuplicata.Text) <> '' then
      QrPesq.SQL.Add('AND Duplicata="'+EdDuplicata.Text+'"');
    QrPesq.SQL.Add('');
    QrPesq.Open;
    if QrPesq.RecordCount > 0 then
    begin
      PainelData.Visible := True;
      PainelEdit.Visible := True;
    end;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmOcorreu1.QrPesqCalcFields(DataSet: TDataSet);
var
  Taxas: MyArrayR07;
begin
  QrPesqTEL1_TXT.Value := Geral.FormataTelefone_TT(QrPesqTel1.Value);
  QrPesqCEP_TXT.Value := Geral.FormataCEP_NT(QrPesqCEP.Value);
  QrPesqNUMERO_TXT.Value := MLAGeral.FormataNumeroDeRua(QrPesqRua.Value, Trunc(QrPesqNumero.Value), False);
  QrPesqCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrPesqCNPJ.Value);
  //
  Taxas := DMod.ObtemTaxaDeCompraRealizada(QrPesqControle.Value);
  QrPesqTAXA_COMPRA.Value := QrPesqTxaCompra.Value + Taxas[0];
end;

procedure TFmOcorreu1.BtDesiste5Click(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO);
end;

procedure TFmOcorreu1.EdOcorrenciaExit(Sender: TObject);
begin
  if LaTipo.Caption = CO_INCLUSAO then
  EdValor.Text := Geral.FFT(QrOcorBankBase.Value, 2, siNegativo);
end;

procedure TFmOcorreu1.BtConfirma5Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Dmod.QrUpd.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpd.SQL.Add('INSERT INTO ocorreu SET AlterWeb=1, ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'Ocorreu', 'Ocorreu', 'Codigo');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET AlterWeb=1, ');
    Codigo := QrOcorreuCodigo.Value;
  end;
  Dmod.QrUpd.SQL.Add('LotesIts=:P0, DataO=:P1, Ocorrencia=:P2, Valor=:P3');
  Dmod.QrUpd.SQL.Add('');
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa');
  end else begin
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
  end;
  Dmod.QrUpd.Params[00].AsInteger := QrPesqControle.Value;
  Dmod.QrUpd.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataO.Date);
  Dmod.QrUpd.Params[02].AsInteger := Geral.IMV(EdOcorrencia.Text);
  Dmod.QrUpd.Params[03].AsFloat   := Geral.DMV(EdValor.Text);
  Dmod.QrUpd.Params[04].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  ReopenOcorreu(Codigo);
  MostraEdicao(0, CO_TRAVADO);
end;

procedure TFmOcorreu1.ReopenOcorreu(Codigo: Integer);
begin
  QrOcorreu.Close;
  QrOcorreu.Params[00].AsInteger := QrPesqControle.Value;
  QrOcorreu.Open;
  //
  if Codigo <> 0 then QrOcorreu.Locate('Codigo', Codigo, []);
end;

procedure TFmOcorreu1.QrOcorreuAfterOpen(DataSet: TDataSet);
begin
  if QrOcorreu.RecordCount = 0 then
  begin
    BtAltera.Enabled := False;
    BtExclui.Enabled := False;
  end else begin
    BtAltera.Enabled := True;
    BtExclui.Enabled := True;
  end;
end;

procedure TFmOcorreu1.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, CO_INCLUSAO);
end;

procedure TFmOcorreu1.BtAlteraClick(Sender: TObject);
begin
  MostraEdicao(1, CO_ALTERACAO);
end;

procedure TFmOcorreu1.BtExcluiClick(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o da ocorr�ncia selecionada?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM ocorreu WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrOcorreuCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    QrOcorreu.Next;
    //
    ReopenOcorreu(QrOcorreuCodigo.Value);
  end;  
end;

procedure TFmOcorreu1.QrPesqAfterScroll(DataSet: TDataSet);
begin
  ReopenOcorreu(0);
end;

procedure TFmOcorreu1.BtPesquisaClick(Sender: TObject);
begin
  Pesquisa;
end;

procedure TFmOcorreu1.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  Pesquisa;
end;

procedure TFmOcorreu1.QrPesqAfterOpen(DataSet: TDataSet);
begin
  BtInclui.Enabled := MLAGeral.IntToBool_Query(QrPesq);
end;

procedure TFmOcorreu1.QrPesqBeforeClose(DataSet: TDataSet);
begin
  BtInclui.Enabled := False;
end;

end.

