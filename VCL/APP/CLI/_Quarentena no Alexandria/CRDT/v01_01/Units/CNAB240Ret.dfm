object FmCNAB240Ret: TFmCNAB240Ret
  Left = 339
  Top = 185
  Caption = 'Retorno de Arquivos CNAB240'
  ClientHeight = 673
  ClientWidth = 1226
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1226
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Retorno de Arquivos CNAB240'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 60
      Top = 1
      Width = 1165
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
      ExplicitLeft = 1
      ExplicitWidth = 1224
    end
    object Pn_L: TPanel
      Left = 1
      Top = 1
      Width = 59
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 0
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitHeight = 59
      object BtPasta: TBitBtn
        Tag = 25
        Left = 5
        Top = 5
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtPastaClick
      end
    end
  end
  object PnCarrega: TPanel
    Left = 0
    Top = 59
    Width = 623
    Height = 614
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 1
    object PainelConfirma: TPanel
      Left = 1
      Top = 554
      Width = 621
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 25
        Top = 5
        Width = 110
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
      object Panel2: TPanel
        Left = 483
        Top = 1
        Width = 137
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Button1: TButton
        Left = 143
        Top = 15
        Width = 92
        Height = 31
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Button1'
        TabOrder = 2
        Visible = False
        OnClick = Button1Click
      end
      object BitBtn3: TBitBtn
        Tag = 150
        Left = 241
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Ger'#234'ncia'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BitBtn3Click
      end
    end
    object PnArquivos: TPanel
      Left = 1
      Top = 1
      Width = 621
      Height = 553
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object ListBox1: TListBox
        Left = 241
        Top = 64
        Width = 558
        Height = 36
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 0
        Visible = False
      end
      object MemoTam: TMemo
        Left = 0
        Top = 506
        Width = 621
        Height = 47
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Lucida Console'
        Font.Style = []
        Lines.Strings = (
          'MemoTam')
        ParentFont = False
        TabOrder = 1
        Visible = False
      end
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 621
        Height = 153
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        BevelOuter = bvNone
        Caption = 'Panel1'
        TabOrder = 2
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 194
          Height = 153
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          DataSource = DsCNAB240Dir
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Banco'
              Title.Caption = 'Bco'
              Width = 28
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEBANCO'
              Title.Caption = 'Nome do banco'
              Width = 148
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Diret'#243'rio dos arquivos retorno'
              Width = 377
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'Dir'
              Width = 28
              Visible = True
            end>
        end
        object GradeA: TStringGrid
          Left = 194
          Top = 0
          Width = 427
          Height = 153
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          ColCount = 6
          DefaultColWidth = 32
          DefaultRowHeight = 18
          RowCount = 2
          Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowSelect]
          TabOrder = 1
          OnDrawCell = GradeADrawCell
          OnSelectCell = GradeASelectCell
          ColWidths = (
            32
            118
            45
            46
            32
            32)
        end
      end
      object PageControl1: TPageControl
        Left = 0
        Top = 153
        Width = 621
        Height = 254
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 3
        object TabSheet1: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Estilo A'
          object GradeC: TStringGrid
            Left = 0
            Top = 0
            Width = 613
            Height = 223
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ColCount = 21
            DefaultColWidth = 32
            DefaultRowHeight = 18
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing]
            TabOrder = 0
            OnClick = GradeCClick
            OnDrawCell = GradeCDrawCell
            OnKeyDown = GradeCKeyDown
            OnSelectCell = GradeCSelectCell
            ColWidths = (
              32
              38
              31
              44
              45
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32)
          end
        end
        object TabSheet2: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Estilo B'
          ImageIndex = 1
          object GradeB: TStringGrid
            Left = 0
            Top = 0
            Width = 613
            Height = 223
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            ColCount = 21
            DefaultColWidth = 32
            DefaultRowHeight = 18
            RowCount = 2
            Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing]
            TabOrder = 0
            OnClick = GradeBClick
            OnDrawCell = GradeBDrawCell
            OnKeyDown = GradeBKeyDown
            OnSelectCell = GradeBSelectCell
            ColWidths = (
              32
              38
              31
              44
              45
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32
              32)
          end
        end
      end
      object MeRejeicoes: TMemo
        Left = 0
        Top = 407
        Width = 621
        Height = 99
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alBottom
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Courier New'
        Font.Style = [fsBold]
        Lines.Strings = (
          'Linha1'
          'Linha2'
          'Linha3'
          'Linha4'
          'Linha5'
          '')
        ParentFont = False
        TabOrder = 4
      end
    end
  end
  object PnMovimento: TPanel
    Left = 623
    Top = 59
    Width = 603
    Height = 614
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alRight
    BevelOuter = bvNone
    TabOrder = 2
    Visible = False
    object Panel3: TPanel
      Left = 0
      Top = 555
      Width = 603
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Panel4: TPanel
        Left = 465
        Top = 1
        Width = 137
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BitBtn2: TBitBtn
          Tag = 13
          Left = 2
          Top = 4
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object Button2: TButton
        Left = 891
        Top = 15
        Width = 92
        Height = 31
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Button1'
        TabOrder = 1
        Visible = False
        OnClick = Button1Click
      end
      object BitBtn1: TBitBtn
        Tag = 150
        Left = 241
        Top = 5
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Ger'#234'ncia'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BitBtn1Click
      end
    end
    object DBGrid6: TDBGrid
      Left = 0
      Top = 0
      Width = 603
      Height = 555
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      DataSource = DsCNAB240
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'Meu ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Title.Caption = 'Sacado'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CPF_TXT'
          Title.Caption = 'CPF / CNPJ'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Duplicata'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CNAB240L'
          Title.Caption = 'Ret. ID'
          Width = 40
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CNAB240L'
          Title.Caption = 'Ctrl ID'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Banco'
          Title.Caption = 'Bco'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Convenio'
          Title.Caption = 'Conv'#234'nio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Sequencia'
          Title.Caption = 'Seq. ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Width = 27
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Item'
          Width = 25
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataOcor'
          Title.Caption = 'Data Ocor.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEENVIO'
          Title.Caption = 'Envio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Movimento'
          Title.Caption = 'Mov.'
          Width = 30
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEMOVIMENTO'
          Title.Caption = 'Descri'#231#227'o movimento'
          Width = 193
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'IRTCLB'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Custas'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValPago'
          Title.Caption = 'Pago'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValCred'
          Title.Caption = 'Cr'#233'dito'
          Visible = True
        end>
    end
  end
  object QrCNAB240Dir: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCNAB240DirAfterOpen
    SQL.Strings = (
      'SELECT ban.Nome NOMEBANCO, dir.* '
      'FROM cnab240dir dir'
      'LEFT JOIN bancos ban ON ban.Codigo=dir.Banco'
      'WHERE dir.Envio=2')
    Left = 8
    Top = 12
    object QrCNAB240DirNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrCNAB240DirCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
      DisplayFormat = '000'
    end
    object QrCNAB240DirLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCNAB240DirDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCNAB240DirDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCNAB240DirUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCNAB240DirUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCNAB240DirNome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
    object QrCNAB240DirEnvio: TSmallintField
      FieldName = 'Envio'
      Required = True
    end
    object QrCNAB240DirBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
  end
  object DsCNAB240Dir: TDataSource
    DataSet = QrCNAB240Dir
    Left = 36
    Top = 12
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer1Timer
    Left = 108
    Top = 236
  end
  object QrDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cnab240'
      'WHERE Controle=:P0'
      'AND Lote=:P1'
      'AND Item=:P2'
      'AND DataOcor=:P3'
      'AND Envio=:P4'
      'AND Movimento=:P5'
      'AND Sequencia=:P6'
      'AND Convenio=:P7'
      '')
    Left = 120
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P6'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P7'
        ParamType = ptUnknown
      end>
  end
  object QrLotesIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial'
      'ELSE cli.Nome END NOMECLI, loi.Codigo, loi.Duplicata,'
      
        'loi.CPF, loi.Emitente, loi.Bruto, loi.Desco, loi.Quitado, loi.Va' +
        'lor, '
      'loi.Emissao, loi.DCompra, loi.Vencto, loi.DDeposito, loi.Dias, '
      
        'loi.Data3, loi.Cobranca, loi.Repassado, lot.Lote, lot.Tipo TIPOL' +
        'OTE,'
      'loi.Cliente'
      'FROM lotesits loi'
      'LEFT JOIN lotes     lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=loi.Cliente'
      'WHERE loi.Controle=:P0')
    Left = 92
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotesItsNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrLotesItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLotesItsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrLotesItsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLotesItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLotesItsBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
    end
    object QrLotesItsDesco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrLotesItsQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrLotesItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrLotesItsEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrLotesItsDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
    end
    object QrLotesItsVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrLotesItsDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
    end
    object QrLotesItsDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrLotesItsData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrLotesItsCobranca: TIntegerField
      FieldName = 'Cobranca'
      Required = True
    end
    object QrLotesItsRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrLotesItsLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrLotesItsTIPOLOTE: TSmallintField
      FieldName = 'TIPOLOTE'
    end
    object QrLotesItsCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
  end
  object QrCNAB240: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrCNAB240AfterScroll
    OnCalcFields = QrCNAB240CalcFields
    SQL.Strings = (
      'SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial'
      'ELSE cli.Nome END NOMECLI, loi.Codigo MeuLote, loi.Duplicata,'
      
        'loi.CPF, loi.Emitente, loi.Bruto, loi.Desco, loi.Quitado, loi.Va' +
        'lor, '
      'loi.Emissao, loi.DCompra, loi.Vencto, loi.DDeposito, loi.Dias, '
      'loi.Data3, loi.Cobranca, loi.Repassado, loi.Cliente,'
      'lot.Lote Bordero,  lot.Tipo TIPOLOTE, ban.Nome NOMEBANCO, cna.* '
      'FROM cnab240 cna'
      'LEFT JOIN bancos ban ON ban.Codigo=cna.Banco'
      'LEFT JOIN lotesits loi ON loi.Controle=cna.Controle'
      'LEFT JOIN lotes     lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=loi.Cliente'
      'WHERE cna.CNAB240L=:P0')
    Left = 148
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB240CNAB240L: TIntegerField
      FieldName = 'CNAB240L'
    end
    object QrCNAB240CNAB240I: TIntegerField
      FieldName = 'CNAB240I'
    end
    object QrCNAB240Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCNAB240Banco: TIntegerField
      FieldName = 'Banco'
    end
    object QrCNAB240ConfigBB: TIntegerField
      FieldName = 'ConfigBB'
    end
    object QrCNAB240Lote: TIntegerField
      FieldName = 'Lote'
    end
    object QrCNAB240Item: TIntegerField
      FieldName = 'Item'
    end
    object QrCNAB240DataOcor: TDateField
      FieldName = 'DataOcor'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB240Envio: TSmallintField
      FieldName = 'Envio'
    end
    object QrCNAB240Movimento: TSmallintField
      FieldName = 'Movimento'
    end
    object QrCNAB240Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCNAB240DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCNAB240DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCNAB240UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCNAB240UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCNAB240NOMEENVIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEENVIO'
      Size = 10
      Calculated = True
    end
    object QrCNAB240NOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrCNAB240NOMEMOVIMENTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMOVIMENTO'
      Size = 100
      Calculated = True
    end
    object QrCNAB240NOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCNAB240MeuLote: TIntegerField
      FieldName = 'MeuLote'
    end
    object QrCNAB240Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrCNAB240CPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrCNAB240Emitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrCNAB240Bruto: TFloatField
      FieldName = 'Bruto'
    end
    object QrCNAB240Desco: TFloatField
      FieldName = 'Desco'
    end
    object QrCNAB240Quitado: TIntegerField
      FieldName = 'Quitado'
    end
    object QrCNAB240Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrCNAB240Emissao: TDateField
      FieldName = 'Emissao'
    end
    object QrCNAB240DCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrCNAB240Vencto: TDateField
      FieldName = 'Vencto'
    end
    object QrCNAB240DDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrCNAB240Dias: TIntegerField
      FieldName = 'Dias'
    end
    object QrCNAB240Data3: TDateField
      FieldName = 'Data3'
    end
    object QrCNAB240Cobranca: TIntegerField
      FieldName = 'Cobranca'
    end
    object QrCNAB240Repassado: TSmallintField
      FieldName = 'Repassado'
    end
    object QrCNAB240Bordero: TIntegerField
      FieldName = 'Bordero'
    end
    object QrCNAB240TIPOLOTE: TSmallintField
      FieldName = 'TIPOLOTE'
    end
    object QrCNAB240DATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Size = 25
      Calculated = True
    end
    object QrCNAB240NOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 100
      Calculated = True
    end
    object QrCNAB240NOMETIPOLOTE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPOLOTE'
      Size = 10
      Calculated = True
    end
    object QrCNAB240CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrCNAB240Custas: TFloatField
      FieldName = 'Custas'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCNAB240IRTCLB_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IRTCLB_TXT'
      Size = 100
      Calculated = True
    end
    object QrCNAB240ValPago: TFloatField
      FieldName = 'ValPago'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCNAB240ValCred: TFloatField
      FieldName = 'ValCred'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCNAB240Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrCNAB240Acao: TIntegerField
      FieldName = 'Acao'
      Required = True
    end
    object QrCNAB240NOMEACAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEACAO'
      Size = 3
      Calculated = True
    end
    object QrCNAB240Sequencia: TIntegerField
      FieldName = 'Sequencia'
      Required = True
    end
    object QrCNAB240Convenio: TWideStringField
      FieldName = 'Convenio'
    end
    object QrCNAB240IRTCLB: TWideStringField
      FieldName = 'IRTCLB'
      Size = 10
    end
  end
  object DsCNAB240: TDataSource
    DataSet = QrCNAB240
    Left = 176
    Top = 12
  end
  object QrSumPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ad.Juros) Juros, SUM(ad.Desco) Desco, '
      'SUM(ad.Pago) Pago, li.Valor, MAX(ad.Data) MaxData'
      'FROM aduppgs ad'
      'LEFT JOIN lotesits li ON li.Controle=ad.LotesIts'
      'WHERE ad.LotesIts=:P0'
      'GROUP BY ad.LotesIts')
    Left = 204
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPgJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumPgPago: TFloatField
      FieldName = 'Pago'
    end
    object QrSumPgDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrSumPgValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSumPgMaxData: TDateField
      FieldName = 'MaxData'
    end
  end
  object QrOcorreu: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOcorreuCalcFields
    SQL.Strings = (
      'SELECT  lo.Cliente CLIENTELOTE, ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.LotesIts=:P0'
      '')
    Left = 685
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorreuNOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcorreuCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOcorreuLotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrOcorreuDataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrOcorreuValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreuLoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrOcorreuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorreuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorreuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorreuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorreuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorreuTaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrOcorreuTaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
    end
    object QrOcorreuPago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreuDataP: TDateField
      FieldName = 'DataP'
      Required = True
    end
    object QrOcorreuTaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrOcorreuATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreuCLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
    object QrOcorreuData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrOcorreuStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrOcorreuCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOcorreuSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreuATZ_TEXTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ATZ_TEXTO'
      Size = 30
      Calculated = True
    end
  end
  object DsOcorreu: TDataSource
    DataSet = QrOcorreu
    Left = 713
    Top = 9
  end
  object QrOcorDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ok.Nome NOMEOCORBANK, ok.Base ValOcorBank, od.Ocorrbase '
      'FROM ocordupl od'
      'LEFT JOIN ocorbank ok ON ok.Codigo=od.Ocorrbase'
      'WHERE od.Codigo=:P0')
    Left = 741
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorDuplNOMEOCORBANK: TWideStringField
      FieldName = 'NOMEOCORBANK'
      Size = 50
    end
    object QrOcorDuplValOcorBank: TFloatField
      FieldName = 'ValOcorBank'
    end
    object QrOcorDuplOcorrbase: TIntegerField
      FieldName = 'Ocorrbase'
      Required = True
    end
  end
  object QrOcorBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ocorbank'
      'WHERE Envio=:P0'
      'AND Movimento=:P1'
      '')
    Left = 769
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOcorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrOcorBankLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorBankDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorBankDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorBankUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorBankUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorBankBase: TFloatField
      FieldName = 'Base'
    end
    object QrOcorBankEnvio: TIntegerField
      FieldName = 'Envio'
    end
    object QrOcorBankMovimento: TIntegerField
      FieldName = 'Movimento'
    end
    object QrOcorBankFormaCNAB: TSmallintField
      FieldName = 'FormaCNAB'
    end
  end
  object Timer2: TTimer
    Enabled = False
    Interval = 100
    OnTimer = Timer2Timer
    Left = 477
    Top = 212
  end
  object QrOB2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome'
      'FROM ocorbank'
      'WHERE Codigo=:P0')
    Left = 796
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOB2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrOcorCli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM ocorcli'
      'WHERE Cliente=:P0'
      'AND Ocorrencia=:P1')
    Left = 824
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOcorCliBase: TFloatField
      FieldName = 'Base'
    end
    object QrOcorCliFormaCNAB: TSmallintField
      FieldName = 'FormaCNAB'
    end
  end
end
