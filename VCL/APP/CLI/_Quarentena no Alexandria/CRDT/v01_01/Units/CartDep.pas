unit CartDep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, Variants, dmkEdit,
  dmkEditCB, dmkDBLookupComboBox;

type
  TFmCartDep = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label1: TLabel;
    CBCarteira: TdmkDBLookupComboBox;
    DsCarteiras: TDataSource;
    EdCarteira: TdmkEditCB;
    QrCarteiras: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FmCartDep: TFmCartDep;

implementation

uses Module, UnMyObjects;

{$R *.DFM}

procedure TFmCartDep.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCartDep.BtConfirmaClick(Sender: TObject);
begin
  if CBCarteira.KeyValue <> NULL then
  begin
    VAR_CARTDEP := CBCarteira.KeyValue;
    Close;
  end else Application.MessageBox('Defina a carteira de dep�sito', 'Erro',
    MB_OK+MB_ICONERROR);
end;

procedure TFmCartDep.FormCreate(Sender: TObject);
begin
  QrCarteiras.Open;
end;

procedure TFmCartDep.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1,PainelTitulo, True,0);
end;

procedure TFmCartDep.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

end.
