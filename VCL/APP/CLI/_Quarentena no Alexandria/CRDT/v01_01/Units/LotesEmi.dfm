object FmLotesEmi: TFmLotesEmi
  Left = 419
  Top = 217
  Caption = 'Nomes por CPF'
  ClientHeight = 306
  ClientWidth = 590
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 258
    Width = 590
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object BtCancela: TBitBtn
      Tag = 15
      Left = 490
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Cancela'
      TabOrder = 1
      OnClick = BtCancelaClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 590
    Height = 48
    Align = alTop
    Caption = 'Nomes por CPF'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 588
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 594
      ExplicitHeight = 44
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 48
    Width = 590
    Height = 210
    Align = alClient
    DataSource = Dmod.DsLocCPF
    TabOrder = 2
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'Nome'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Banco'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Agencia'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Conta'
        Visible = True
      end>
  end
end
