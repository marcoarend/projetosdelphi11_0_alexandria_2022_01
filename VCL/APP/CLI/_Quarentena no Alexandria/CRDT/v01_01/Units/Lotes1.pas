unit Lotes1;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnInternalConsts3,
  UnMsgInt, UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa,
  ArquivosSCX, ComCtrls, Grids, DBGrids, Menus, (*DBIProcs,*) frxClass, frxDBSet,
  frxRich, dmkEditDateTimePicker, Variants, dmkGeral, dmkEdit, dmkLabel,
  dmkDBLookupComboBox, dmkEditCB, UnDmkProcFunc, UnDmkEnums, DMKpnfsConversao;

type
  TFmLotes1 = class(TForm)
    PainelDados: TPanel;
    DsLotes: TDataSource;
    QrLotes: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    BtRefresh: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    PainelEdit: TPanel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    QrLotesCodigo: TIntegerField;
    QrLotesTipo: TSmallintField;
    QrLotesCliente: TIntegerField;
    QrLotesLote: TSmallintField;
    QrLotesData: TDateField;
    QrLotesTotal: TFloatField;
    QrLotesDias: TFloatField;
    QrLotesTxCompra: TFloatField;
    QrLotesAdValorem: TFloatField;
    QrLotesIOC: TFloatField;
    QrLotesCPMF: TFloatField;
    QrLotesLk: TIntegerField;
    QrLotesDataCad: TDateField;
    QrLotesDataAlt: TDateField;
    QrLotesUserCad: TIntegerField;
    QrLotesUserAlt: TIntegerField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    QrLotesNOMECLIENTE: TWideStringField;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label21: TLabel;
    DsLotesIts: TDataSource;
    QrLotesIts: TmySQLQuery;
    QrLotesItsCodigo: TIntegerField;
    QrLotesItsControle: TIntegerField;
    QrLotesItsComp: TIntegerField;
    QrLotesItsBanco: TIntegerField;
    QrLotesItsAgencia: TIntegerField;
    QrLotesItsConta: TWideStringField;
    QrLotesItsCheque: TIntegerField;
    QrLotesItsEmitente: TWideStringField;
    QrLotesItsValor: TFloatField;
    QrLotesItsVencto: TDateField;
    QrLotesItsTxaCompra: TFloatField;
    QrLotesItsTxaAdValorem: TFloatField;
    QrLotesItsVlrCompra: TFloatField;
    QrLotesItsVlrAdValorem: TFloatField;
    QrLotesItsDMais: TIntegerField;
    QrLotesItsDias: TIntegerField;
    QrLotesItsLk: TIntegerField;
    QrLotesItsDataCad: TDateField;
    QrLotesItsDataAlt: TDateField;
    QrLotesItsUserCad: TIntegerField;
    QrLotesItsUserAlt: TIntegerField;
    QrLotesItsCPF: TWideStringField;
    PainelBandaMagnetica: TPanel;
    Panel4: TPanel;
    BtSair2: TBitBtn;
    BtCalendario: TBitBtn;
    PainelImporta: TPanel;
    Panel2: TPanel;
    Label14: TLabel;
    PainelPdxAdVal: TPanel;
    QrLotesTipoAdV: TIntegerField;
    Label8: TLabel;
    DBEdit6: TDBEdit;
    DBEdit8: TDBEdit;
    Label11: TLabel;
    Label10: TLabel;
    DBEdit7: TDBEdit;
    DBEdit9: TDBEdit;
    Label12: TLabel;
    Label13: TLabel;
    DBEdit10: TDBEdit;
    Label20: TLabel;
    EdDBValLiq: TDBEdit;
    DBEdit17: TDBEdit;
    Label22: TLabel;
    GradeAV: TStringGrid;
    QrLotesADVALOREM_TOTAL: TFloatField;
    QrLotesVALVALOREM_TOTAL: TFloatField;
    QrLotesValValorem: TFloatField;
    QrLotesSpread: TSmallintField;
    QrLotesTAXAVALOR_TOTAL: TFloatField;
    QrLotesTAXAPERCE_TOTAL: TFloatField;
    QrLotesIOC_VAL: TFloatField;
    QrLotesCPMF_VAL: TFloatField;
    QrLotesSUB_TOTAL: TFloatField;
    QrLotesVAL_LIQUIDO: TFloatField;
    QrLotesItsTxaJuros: TFloatField;
    QrLotesItsDCompra: TDateField;
    QrLotesItsCPF_TXT: TWideStringField;
    QrLotesItsDDeposito: TDateField;
    QrLotesISS: TFloatField;
    QrLotesISS_Val: TFloatField;
    QrLotesPIS_R: TFloatField;
    QrLotesPIS_R_Val: TFloatField;
    QrLotesIRRF: TFloatField;
    QrLotesIRRF_Val: TFloatField;
    QrLotesTOT_ISS: TFloatField;
    QrLotesTOT_IRR: TFloatField;
    QrLotesTAXAPERCE_PROPR: TFloatField;
    QrLotesVAL_LIQUIDO_MEU: TFloatField;
    QrLotesSUB_TOTAL_MEU: TFloatField;
    Pagina: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel9: TPanel;
    Label33: TLabel;
    EdCodCli: TdmkEdit;
    EdNomCli: TdmkEdit;
    Panel10: TPanel;
    Label48: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    TPCheque: TdmkEditDateTimePicker;
    EdQtdeTO: TdmkEdit;
    EdValorTO: TdmkEdit;
    BtFechar: TBitBtn;
    BtNovo: TBitBtn;
    PMNovo: TPopupMenu;
    BorderauxdeCheques1: TMenuItem;
    BorderauxdeDuplicatas1: TMenuItem;
    N2: TMenuItem;
    Importa1: TMenuItem;
    QrLotesItsDuplicata: TWideStringField;
    PainelDuplicata: TPanel;
    Panel11: TPanel;
    BtConfirma4: TBitBtn;
    BtSair4: TBitBtn;
    BtCalendario2: TBitBtn;
    PainelDigitaDup: TPanel;
    GradeDU: TStringGrid;
    QrLotesItsEmissao: TDateField;
    QrLotesCNPJCPF: TWideStringField;
    QrLotesCNPJ_CPF_TXT: TWideStringField;
    QrLotesPeCompra: TFloatField;
    QrLotesTAXA_AM_TXT: TWideStringField;
    QrLotesADVAL_TXT: TWideStringField;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    Panel1: TPanel;
    Panel12: TPanel;
    GradeC_L: TDBGrid;
    PGAbertos_CH: TPageControl;
    TabSheet3: TTabSheet;
    GradeDev: TDBGrid;
    TabSheet4: TTabSheet;
    DBGrid3: TDBGrid;
    Splitter1: TSplitter;
    PGAbertos_DU: TPageControl;
    TabSheet5: TTabSheet;
    Splitter2: TSplitter;
    GradeD_L: TDBGrid;
    QrLotesTarifas: TFloatField;
    DBEdit20: TDBEdit;
    Label74: TLabel;
    PgPrincipal: TPageControl;
    TabSheet7: TTabSheet;
    TabSheet8: TTabSheet;
    DBGrid7: TDBGrid;
    QrLotesTxs: TmySQLQuery;
    DsLotesTxs: TDataSource;
    QrLotesTxsCodigo: TIntegerField;
    QrLotesTxsControle: TIntegerField;
    QrLotesTxsTaxaCod: TIntegerField;
    QrLotesTxsTaxaTxa: TFloatField;
    QrLotesTxsTaxaVal: TFloatField;
    QrLotesTxsLk: TIntegerField;
    QrLotesTxsDataCad: TDateField;
    QrLotesTxsDataAlt: TDateField;
    QrLotesTxsUserCad: TIntegerField;
    QrLotesTxsUserAlt: TIntegerField;
    Panel13: TPanel;
    BtInclui5: TBitBtn;
    BtAltera5: TBitBtn;
    PainelTaxas: TPanel;
    Panel14: TPanel;
    BtConfirma5: TBitBtn;
    BtDesiste5: TBitBtn;
    Panel15: TPanel;
    Label75: TLabel;
    EdTaxaCod5: TdmkEditCB;
    CBTaxaCod5: TdmkDBLookupComboBox;
    EdTaxaTxa5: TdmkEdit;
    Label77: TLabel;
    QrLotesTxsNome: TWideStringField;
    QrLotesTxsTAXA_STR: TWideStringField;
    QrLotesTxsForma: TSmallintField;
    BtExclui5: TBitBtn;
    RGForma: TRadioGroup;
    Label76: TLabel;
    EdTaxaQtd5: TdmkEdit;
    QrLotesTxsTaxaQtd: TFloatField;
    Label78: TLabel;
    EdTaxaTot5: TdmkEdit;
    QrLotesTxsSysAQtd: TFloatField;
    QrLotesTxsCALCQtd: TFloatField;
    Panel16: TPanel;
    DBGrid4: TDBGrid;
    Panel17: TPanel;
    DBGrid2: TDBGrid;
    QrLotesItsDevolucao: TIntegerField;
    QrLotesItsDesco: TFloatField;
    QrLotesItsQuitado: TIntegerField;
    QrLotesItsBruto: TFloatField;
    PainelItens: TPanel;
    GradeX1: TStringGrid;
    Panel18: TPanel;
    Label80: TLabel;
    Label81: TLabel;
    EdDMaisX: TdmkEdit;
    EdTxaCompraX: TdmkEdit;
    TabSheet6: TTabSheet;
    PageControl3: TPageControl;
    TabSheet9: TTabSheet;
    TabSheet10: TTabSheet;
    Panel19: TPanel;
    BtPagtosExclui6: TBitBtn;
    QrOcorP: TmySQLQuery;
    DsOcorP: TDataSource;
    QrLotesOcorP: TFloatField;
    DBGrid6: TDBGrid;
    Label90: TLabel;
    DBEdit21: TDBEdit;
    QrLotesSUB_TOTAL_2: TFloatField;
    QrSumOP: TmySQLQuery;
    QrLotesCOFINS_R: TFloatField;
    QrLotesCOFINS_R_Val: TFloatField;
    Panel24: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    Label3: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdLote: TdmkEdit;
    Label4: TLabel;
    RGTipo: TRadioGroup;
    LaAdValorem: TLabel;
    EdAdValorem: TdmkEdit;
    TPData: TdmkEditDateTimePicker;
    Label34: TLabel;
    Panel25: TPanel;
    QrLotesPIS: TFloatField;
    QrLotesCOFINS: TFloatField;
    QrLotesMEU_PISCOFINS_VAL: TFloatField;
    QrLotesPIS_Val: TFloatField;
    QrLotesCOFINS_Val: TFloatField;
    QrLotesMEU_PISCOFINS: TFloatField;
    QrLotesMaxVencto: TDateField;
    QrContrat: TmySQLQuery;
    QrContratContrato: TIntegerField;
    QrContratDataC: TDateField;
    QrContratLimite: TFloatField;
    QrContratNOME11: TWideStringField;
    QrContratNOME12: TWideStringField;
    QrContratNOME21: TWideStringField;
    QrContratNOME22: TWideStringField;
    PMCadastraCH: TPopupMenu;
    Emitenteselecionado1: TMenuItem;
    Todosemitentesnocadastrados1: TMenuItem;
    PMCadastraDU: TPopupMenu;
    Sacadoselecionado1: TMenuItem;
    Todossacadosnocadastrados1: TMenuItem;
    Painel6PgOcor: TPanel;
    Panel20: TPanel;
    Label82: TLabel;
    Label83: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    EdJuros6: TdmkEdit;
    EdPago6: TdmkEdit;
    EdAPagar6: TdmkEdit;
    Panel21: TPanel;
    BtDesiste6: TBitBtn;
    BtConfirma6: TBitBtn;
    Panel22: TPanel;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    EdValorBase6: TdmkEdit;
    EdJurosBase6: TdmkEdit;
    EdJurosPeriodo6: TdmkEdit;
    Panel23: TPanel;
    DBGrid5: TDBGrid;
    Panel26: TPanel;
    BtPagamento6: TBitBtn;
    DBGrid8: TDBGrid;
    TabSheet11: TTabSheet;
    PageControl4: TPageControl;
    TabSheet12: TTabSheet;
    Panel27: TPanel;
    Panel31: TPanel;
    Panel32: TPanel;
    BtPagamento7: TBitBtn;
    TabSheet13: TTabSheet;
    Panel33: TPanel;
    BtExclui7: TBitBtn;
    DBGrid11: TDBGrid;
    QrCHDevP: TmySQLQuery;
    DsCHDevP: TDataSource;
    QrCHDevPData: TDateField;
    QrCHDevPJuros: TFloatField;
    QrCHDevPPago: TFloatField;
    DBGrid10: TDBGrid;
    PMBordero: TPopupMenu;
    Total1: TMenuItem;
    Contabilidade1: TMenuItem;
    Promissria1: TMenuItem;
    Aditivo1: TMenuItem;
    N3: TMenuItem;
    Coligadas1: TMenuItem;
    DBGrid9: TDBGrid;
    Panel28: TPanel;
    Label102: TLabel;
    Label103: TLabel;
    Label104: TLabel;
    Label105: TLabel;
    TPDataBase7: TdmkEditDateTimePicker;
    EdValorBase7: TdmkEdit;
    EdJurosBase7: TdmkEdit;
    EdJurosPeriodo7: TdmkEdit;
    Panel29: TPanel;
    Label106: TLabel;
    Label107: TLabel;
    Label108: TLabel;
    Label109: TLabel;
    TPPagto7: TdmkEditDateTimePicker;
    EdJuros7: TdmkEdit;
    EdPago7: TdmkEdit;
    EdAPagar7: TdmkEdit;
    Panel30: TPanel;
    BtDesiste7: TBitBtn;
    BtConfirma7: TBitBtn;
    QrCHDevPBanco: TIntegerField;
    QrCHDevPAgencia: TIntegerField;
    QrCHDevPConta: TWideStringField;
    QrCHDevPCheque: TIntegerField;
    QrCHDevPCPF: TWideStringField;
    QrCHDevPValor: TFloatField;
    QrCHDevPTaxas: TFloatField;
    QrCHDevPMulta: TFloatField;
    QrCHDevPEmitente: TWideStringField;
    QrCHDevPStatus: TSmallintField;
    QrCHDevPJurosP: TFloatField;
    QrCHDevPJurosV: TFloatField;
    QrCHDevPDesconto: TFloatField;
    QrCHDevPNOMESTATUS: TWideStringField;
    QrCHDevPAlinIts: TIntegerField;
    QrCHDevPCodigo: TIntegerField;
    QrLotesSUB_TOTAL_3: TFloatField;
    QrLotesCHDevPg: TFloatField;
    Label110: TLabel;
    DBEdit32: TDBEdit;
    QrLotesMINTC: TFloatField;
    QrLotesMINAV: TFloatField;
    QrLotesMINTC_AM: TFloatField;
    QrOcorPBanco: TIntegerField;
    QrOcorPAgencia: TIntegerField;
    QrOcorPConta: TWideStringField;
    QrOcorPCheque: TIntegerField;
    QrOcorPDuplicata: TWideStringField;
    QrOcorPTipo: TSmallintField;
    QrOcorPTIPODOC: TWideStringField;
    QrOcorPNOMEOCORRENCIA: TWideStringField;
    QrOcorPCodigo: TIntegerField;
    QrOcorPOcorreu: TIntegerField;
    QrOcorPData: TDateField;
    QrOcorPJuros: TFloatField;
    QrOcorPPago: TFloatField;
    QrOcorPLotePg: TIntegerField;
    QrOcorPLk: TIntegerField;
    QrOcorPDataCad: TDateField;
    QrOcorPDataAlt: TDateField;
    QrOcorPUserCad: TIntegerField;
    QrOcorPUserAlt: TIntegerField;
    QrOcorPOcorrencia: TIntegerField;
    QrOcorPValor: TFloatField;
    QrOcorPDataO: TDateField;
    QrOcorPSALDO: TFloatField;
    PMRefresh: TPopupMenu;
    EsteBorder1: TMenuItem;
    DesteBorderemdiante1: TMenuItem;
    Timer1: TTimer;
    QrLotesItsPraca: TIntegerField;
    PMAltera: TPopupMenu;
    Cabealhodoborderatual1: TMenuItem;
    Todositensaomesmotempo1: TMenuItem;
    PainelMassico: TPanel;
    N1: TMenuItem;
    AditivoNP1: TMenuItem;
    TabSheet14: TTabSheet;
    DsPagtos: TDataSource;
    QrPagtos: TmySQLQuery;
    QrPagtosData: TDateField;
    QrPagtosTipo: TSmallintField;
    QrPagtosCarteira: TIntegerField;
    QrPagtosSub: TSmallintField;
    QrPagtosDescricao: TWideStringField;
    QrPagtosDebito: TFloatField;
    QrPagtosCredito: TFloatField;
    QrPagtosCompensado: TDateField;
    QrPagtosDocumento: TFloatField;
    QrPagtosSit: TIntegerField;
    QrPagtosVencimento: TDateField;
    QrPagtosFatID: TIntegerField;
    QrPagtosFatID_Sub: TIntegerField;
    QrPagtosFatParcela: TIntegerField;
    QrPagtosBanco: TIntegerField;
    QrPagtosCliente: TIntegerField;
    QrPagtosDataDoc: TDateField;
    QrPagtosNOMECARTEIRA: TWideStringField;
    QrPagtosCARTEIRATIPO: TIntegerField;
    QrPagtosCNPJCPF: TWideStringField;
    QrPagtosEmitente: TWideStringField;
    QrPagtosContaCorrente: TWideStringField;
    PMExcluiPagto: TPopupMenu;
    Pagamento1: TMenuItem;
    Todospagamentosdestebordero1: TMenuItem;
    QrPagtosNOMEFORNECEI: TWideStringField;
    QrPagtosNOMECARTEIRA2: TWideStringField;
    PMPagto: TPopupMenu;
    Novo1: TMenuItem;
    Rpido1: TMenuItem;
    QrLotesPIS_T_Val: TFloatField;
    QrLotesCOFINS_T_Val: TFloatField;
    QrLotesPgLiq: TFloatField;
    QrLotesA_PG_LIQ: TFloatField;
    EdDBAPGLIQ: TDBEdit;
    Label15: TLabel;
    QrPagtosSEQ: TIntegerField;
    QrPagtosNome: TWideStringField;
    QrPagtosCPF: TWideStringField;
    QrPagtosCPF_TXT: TWideStringField;
    QrPagtosBanco1: TIntegerField;
    QrPagtosAgencia1: TIntegerField;
    QrPagtosConta1: TWideStringField;
    QrPagtosTipoDoc: TSmallintField;
    QrPagtosNOMETIPODOC: TWideStringField;
    QrPagtosAgencia: TIntegerField;
    AutorizaodeProtesto1: TMenuItem;
    AditivoNPAP1: TMenuItem;
    CartaaoSacado1: TMenuItem;
    QrSaCart: TmySQLQuery;
    QrSaCartCNPJ: TWideStringField;
    QrSaCartIE: TWideStringField;
    QrSaCartNome: TWideStringField;
    QrSaCartRua: TWideStringField;
    QrSaCartCompl: TWideStringField;
    QrSaCartBairro: TWideStringField;
    QrSaCartCidade: TWideStringField;
    QrSaCartUF: TWideStringField;
    QrSaCartCEP: TIntegerField;
    QrSaCartTel1: TWideStringField;
    QrSaCartRisco: TFloatField;
    QrSaCartCodigo: TIntegerField;
    QrLotesItsBcoCobra: TIntegerField;
    QrLotesItsAgeCobra: TIntegerField;
    TabSheet15: TTabSheet;
    Panel34: TPanel;
    BtPagtosInclui8: TBitBtn;
    BtPagtosAltera8: TBitBtn;
    BtPagtosExclui8: TBitBtn;
    GridPagtos: TDBGrid;
    PageControl5: TPageControl;
    TabSheet16: TTabSheet;
    BtCheque: TBitBtn;
    PMCheque: TPopupMenu;
    Nominal1: TMenuItem;
    Portador1: TMenuItem;
    Panel35: TPanel;
    TabSheet18: TTabSheet;
    DBGrid12: TDBGrid;
    BtPagtosInclui9: TBitBtn;
    QrDPago: TmySQLQuery;
    QrDPagoData: TDateField;
    QrDPagoJuros: TFloatField;
    QrDPagoPago: TFloatField;
    QrDPagoLotePg: TIntegerField;
    QrDPagoLk: TIntegerField;
    QrDPagoDataCad: TDateField;
    QrDPagoDataAlt: TDateField;
    QrDPagoUserCad: TIntegerField;
    QrDPagoUserAlt: TIntegerField;
    QrDPagoLotesIts: TIntegerField;
    QrDPagoControle: TIntegerField;
    QrDPagoDesco: TFloatField;
    DsDPago: TDataSource;
    DBGrid13: TDBGrid;
    QrLotesDUDevPg: TFloatField;
    DBEdit5: TDBEdit;
    Label17: TLabel;
    Panel36: TPanel;
    BtPagtosExclui9: TBitBtn;
    QrDPagoEmitente: TWideStringField;
    QrDPagoCPF: TWideStringField;
    QrDPagoValor: TFloatField;
    QrDPagoDuplicata: TWideStringField;
    QrDPagoNOMESTATUS: TWideStringField;
    QrDPagoQuitado: TIntegerField;
    QrDPagoCPF_TXT: TWideStringField;
    QrCHDevPCPF_TXT: TWideStringField;
    GroupBox1: TGroupBox;
    Label19: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    Label96: TLabel;
    Label98: TLabel;
    Label99: TLabel;
    EdDR: TdmkEdit;
    EdDT: TdmkEdit;
    EdRT: TdmkEdit;
    EdVT: TdmkEdit;
    EdST: TdmkEdit;
    EdCT: TdmkEdit;
    TabSheet17: TTabSheet;
    DBGrid14: TDBGrid;
    EdDV: TdmkEdit;
    EdCR: TdmkEdit;
    EdCV: TdmkEdit;
    Label91: TLabel;
    EdOA: TdmkEdit;
    EdTT: TdmkEdit;
    BtSinaleira: TBitBtn;
    Panel37: TPanel;
    Label66: TLabel;
    EdEmiss4: TdmkEdit;
    Label58: TLabel;
    EdSacado: TdmkEdit;
    Label62: TLabel;
    EdBairro: TdmkEdit;
    Label69: TLabel;
    EdPrazo4: TdmkEdit;
    Label63: TLabel;
    EdCidade: TdmkEdit;
    Label71: TLabel;
    EdDias4: TdmkEdit;
    EdTotalCompraPer4: TdmkEdit;
    Label72: TLabel;
    Label95: TLabel;
    DBEdit23: TDBEdit;
    EdCEP: TdmkEdit;
    Label64: TLabel;
    Label59: TLabel;
    EdRua: TdmkEdit;
    Label73: TLabel;
    EdTel1: TdmkEdit;
    Label93: TLabel;
    Label92: TLabel;
    EdNumero: TdmkEdit;
    Label60: TLabel;
    Label61: TLabel;
    EdCompl: TdmkEdit;
    EdIE: TdmkEdit;
    Label18: TLabel;
    EdCNPJ: TdmkEdit;
    Label57: TLabel;
    Label35: TLabel;
    EdTxaCompra4: TdmkEdit;
    EdDMaisD: TdmkEdit;
    Label67: TLabel;
    Label56: TLabel;
    EdVence4: TdmkEdit;
    EdDesco4: TdmkEdit;
    Label79: TLabel;
    EdValor4: TdmkEdit;
    Label55: TLabel;
    Label54: TLabel;
    EdDuplicata: TdmkEdit;
    EdData4: TdmkEdit;
    Label68: TLabel;
    Label65: TLabel;
    CBUF: TComboBox;
    PageControl6: TPageControl;
    TabSheet19: TTabSheet;
    DBGrid15: TDBGrid;
    TabSheet20: TTabSheet;
    DBGrid16: TDBGrid;
    TabSheet21: TTabSheet;
    DBGrid17: TDBGrid;
    TabSheet22: TTabSheet;
    DBGrid18: TDBGrid;
    GroupBox2: TGroupBox;
    Label7: TLabel;
    Label16: TLabel;
    Label111: TLabel;
    Label112: TLabel;
    Label113: TLabel;
    Label114: TLabel;
    Label115: TLabel;
    EdDR_S: TdmkEdit;
    EdDT_S: TdmkEdit;
    EdRT_S: TdmkEdit;
    EdVT_S: TdmkEdit;
    EdST_S: TdmkEdit;
    EdCT_S: TdmkEdit;
    EdDV_S: TdmkEdit;
    EdCR_S: TdmkEdit;
    EdCV_S: TdmkEdit;
    EdOA_S: TdmkEdit;
    EdTT_S: TdmkEdit;
    PainelBanda: TPanel;
    Painelx: TPanel;
    Label36: TLabel;
    Label37: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label51: TLabel;
    Label52: TLabel;
    Label53: TLabel;
    Label6: TLabel;
    Label25: TLabel;
    Label94: TLabel;
    EdBanda: TdmkEdit;
    EdRegiaoCompe: TdmkEdit;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    EdConta: TdmkEdit;
    EdCheque: TdmkEdit;
    EdValor: TdmkEdit;
    EdVencto: TdmkEdit;
    EdCPF: TdmkEdit;
    EdEmitente: TdmkEdit;
    EdTxaCompra: TdmkEdit;
    EdDMaisC: TdmkEdit;
    EdPrazo: TdmkEdit;
    EdCompensacao: TdmkEdit;
    EdDias: TdmkEdit;
    EdTotalCompraPer: TdmkEdit;
    EdData: TdmkEdit;
    DBEdit22: TDBEdit;
    PageControl7: TPageControl;
    TabSheet23: TTabSheet;
    DBGrid19: TDBGrid;
    TabSheet24: TTabSheet;
    DBGrid20: TDBGrid;
    TabSheet25: TTabSheet;
    DBGrid21: TDBGrid;
    TabSheet26: TTabSheet;
    DBGrid22: TDBGrid;
    GradeCH: TStringGrid;
    GroupBox3: TGroupBox;
    Label116: TLabel;
    Label117: TLabel;
    Label118: TLabel;
    Label119: TLabel;
    Label120: TLabel;
    Label121: TLabel;
    Label122: TLabel;
    EdDR_E: TdmkEdit;
    EdDT_E: TdmkEdit;
    EdRT_E: TdmkEdit;
    EdVT_E: TdmkEdit;
    EdST_E: TdmkEdit;
    EdCT_E: TdmkEdit;
    EdDV_E: TdmkEdit;
    EdCR_E: TdmkEdit;
    EdCV_E: TdmkEdit;
    EdOA_E: TdmkEdit;
    EdTT_E: TdmkEdit;
    QrLotesNOMETIPOLOTE: TWideStringField;
    Label123: TLabel;
    DBEdit11: TDBEdit;
    Label124: TLabel;
    DBEdit12: TDBEdit;
    QrDPagoSALDO: TFloatField;
    QrCHDevPValPago: TFloatField;
    QrCHDevPSALDO: TFloatField;
    DBEdNF: TDBEdit;
    Label125: TLabel;
    QrLotesNF: TIntegerField;
    PainelBco0: TPanel;
    Label126: TLabel;
    EdBanco0: TdmkEdit;
    EdAgencia0: TdmkEdit;
    Label127: TLabel;
    Label128: TLabel;
    DBEdit15: TDBEdit;
    Panel38: TPanel;
    Label101: TLabel;
    EdCOFINS_R: TdmkEdit;
    EdCOFINS: TdmkEdit;
    Label97: TLabel;
    Label100: TLabel;
    EdPIS_R: TdmkEdit;
    EdPIS: TdmkEdit;
    Label28: TLabel;
    Label27: TLabel;
    EdIRRF: TdmkEdit;
    EdISS: TdmkEdit;
    Label26: TLabel;
    PMPagamento7: TPopupMenu;
    Detalhado1: TMenuItem;
    Prconfigurado1: TMenuItem;
    Label129: TLabel;
    DBEdit16: TDBEdit;
    DBEdit18: TDBEdit;
    Label130: TLabel;
    QrLotesTAXAPERCEMENSAL: TFloatField;
    QrLotesItens: TIntegerField;
    BtCheckImp: TBitBtn;
    BtRiscoSac: TBitBtn;
    Label133: TLabel;
    EdNF: TdmkEdit;
    EdCMC_7: TdmkEdit;
    QrSaCartNUMERO_TXT: TWideStringField;
    QrSaCartLNR: TWideStringField;
    QrSaCartLN2: TWideStringField;
    QrSaCartCUC: TWideStringField;
    QrSaCartCEP_TXT: TWideStringField;
    QrOcorPDOCUM_TXT: TWideStringField;
    QrOcorPEmitente: TWideStringField;
    QrOcorPCPF: TWideStringField;
    QrSumOPOcorrencias: TLargeintField;
    QrSumOPPago: TFloatField;
    QrOcorPCPF_TXT: TWideStringField;
    Panel41: TPanel;
    BtOcorencias: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    BitBtn2: TBitBtn;
    QrLotesConferido: TIntegerField;
    QrLotesECartaSac: TSmallintField;
    QrLotesNOMECOFECARTA: TWideStringField;
    QrLotesCAPTIONCONFCARTA: TWideStringField;
    QrLotesNOMESPREAD: TWideStringField;
    Label134: TLabel;
    DBEdit19: TDBEdit;
    DBEdit29: TDBEdit;
    DBText1: TDBText;
    Panel8: TPanel;
    Panel39: TPanel;
    Panel40: TPanel;
    Label31: TLabel;
    Label132: TLabel;
    LaVerSuit2: TLabel;
    BtConfirma3: TBitBtn;
    BtPagtosExclui2: TBitBtn;
    EdQtdeDU: TdmkEdit;
    EdValorDU: TdmkEdit;
    DBEdit14: TDBEdit;
    DBEdit24: TDBEdit;
    BtEntidades2: TBitBtn;
    BtCuidado2: TBitBtn;
    BtEntiTroca2: TBitBtn;
    GradeLDU1: TDBGrid;
    GroupBox4: TGroupBox;
    Label135: TLabel;
    Label136: TLabel;
    Label137: TLabel;
    Label138: TLabel;
    Label139: TLabel;
    Label140: TLabel;
    Label141: TLabel;
    EdDR_D: TdmkEdit;
    EdDT_D: TdmkEdit;
    EdRT_D: TdmkEdit;
    EdVT_D: TdmkEdit;
    EdST_D: TdmkEdit;
    EdCT_D: TdmkEdit;
    EdDV_D: TdmkEdit;
    EdCR_D: TdmkEdit;
    EdCV_D: TdmkEdit;
    EdOA_D: TdmkEdit;
    EdTT_D: TdmkEdit;
    GradeLDU2: TDBGrid;
    GradeD: TStringGrid;
    Panel7: TPanel;
    Panel42: TPanel;
    Panel43: TPanel;
    Label29: TLabel;
    Label131: TLabel;
    LaVerSuit1: TLabel;
    BtConfirma1: TBitBtn;
    BtEntidades: TBitBtn;
    BtPagtosExclui: TBitBtn;
    EdQtdeCh: TdmkEdit;
    EdValorCH: TdmkEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    BtCuidado: TBitBtn;
    BtEntiTroca1: TBitBtn;
    DBGrid1: TDBGrid;
    GroupBox5: TGroupBox;
    Label30: TLabel;
    Label32: TLabel;
    Label142: TLabel;
    Label143: TLabel;
    Label144: TLabel;
    Label145: TLabel;
    Label146: TLabel;
    EdDR_C: TdmkEdit;
    EdDT_C: TdmkEdit;
    EdRT_C: TdmkEdit;
    EdVT_C: TdmkEdit;
    EdST_C: TdmkEdit;
    EdCT_C: TdmkEdit;
    EdDV_C: TdmkEdit;
    EdCR_C: TdmkEdit;
    EdCV_C: TdmkEdit;
    EdOA_C: TdmkEdit;
    EdTT_C: TdmkEdit;
    GradeC: TStringGrid;
    Label147: TLabel;
    Label148: TLabel;
    Label149: TLabel;
    Label150: TLabel;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    Label151: TLabel;
    QrLotesLimiCred: TFloatField;
    Label152: TLabel;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    Label153: TLabel;
    ProgressBar4: TProgressBar;
    ProgressBar3: TProgressBar;
    ProgressLab3: TLabel;
    BtIncluiocor: TBitBtn;
    PMIncluiOcor: TPopupMenu;
    Cliente1: TMenuItem;
    Cheque1: TMenuItem;
    Duplicata1: TMenuItem;
    Label154: TLabel;
    EdCBE: TdmkEdit;
    Label155: TLabel;
    DBEdit35: TDBEdit;
    QrJuros0: TmySQLQuery;
    QrJuros0Vencto: TDateField;
    QrJuros0Praca: TIntegerField;
    QrJuros0Banco: TIntegerField;
    QrJuros0Agencia: TIntegerField;
    QrJuros0Conta: TWideStringField;
    QrJuros0Cheque: TIntegerField;
    QrJuros0CPF: TWideStringField;
    QrJuros0Emitente: TWideStringField;
    QrJuros0Valor: TFloatField;
    QrJuros0TP: TIntegerField;
    QrJuros0Dias: TIntegerField;
    QrJuros0VlrCompra: TFloatField;
    MargemtOtal1: TMenuItem;
    QrJuros0VlrOutras: TFloatField;
    QrJuros0VlrTotal: TFloatField;
    QrJuros0Controle: TIntegerField;
    QrJuros0CPF_TXT: TWideStringField;
    QrJuros0ITEM: TIntegerField;
    QrJuros0DDeposito: TDateField;
    QrLotesCBECli: TIntegerField;
    QrLotesSCBCli: TIntegerField;
    CkSCB: TCheckBox;
    QrLotesCBE: TIntegerField;
    QrLotesSCB: TIntegerField;
    EdRealCC: TdmkEdit;
    BtHistorico: TBitBtn;
    PMHistorico: TPopupMenu;
    Clienteselecionado1: TMenuItem;
    Emitentesacadoselecionado1: TMenuItem;
    Ambos1: TMenuItem;
    TabSheet27: TTabSheet;
    Panel44: TPanel;
    Label156: TLabel;
    Label157: TLabel;
    Label158: TLabel;
    Label159: TLabel;
    Label160: TLabel;
    Label161: TLabel;
    Label162: TLabel;
    Label163: TLabel;
    Label164: TLabel;
    Label165: TLabel;
    Label166: TLabel;
    Label167: TLabel;
    Label168: TLabel;
    Label169: TLabel;
    Label170: TLabel;
    Label171: TLabel;
    Label172: TLabel;
    Label173: TLabel;
    Label174: TLabel;
    Label175: TLabel;
    Label176: TLabel;
    Label177: TLabel;
    DBText2: TDBText;
    Label178: TLabel;
    LM_DBEdit1: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    DBEdit48: TDBEdit;
    DBEdit49: TDBEdit;
    DBEdit50: TDBEdit;
    DBEdit51: TDBEdit;
    DBEdit52: TDBEdit;
    DBEdit53: TDBEdit;
    DBEdit54: TDBEdit;
    DBEdit55: TDBEdit;
    DBEdit56: TDBEdit;
    DBEdit57: TDBEdit;
    DBEdit58: TDBEdit;
    DBEdit59: TDBEdit;
    DBEdit60: TDBEdit;
    DBEdit61: TDBEdit;
    TabSheet28: TTabSheet;
    Panel45: TPanel;
    Label179: TLabel;
    Label180: TLabel;
    Label181: TLabel;
    Label182: TLabel;
    Label183: TLabel;
    Label184: TLabel;
    Label185: TLabel;
    Label186: TLabel;
    Label187: TLabel;
    Label188: TLabel;
    Label189: TLabel;
    Label190: TLabel;
    Label191: TLabel;
    Label192: TLabel;
    Label193: TLabel;
    Label194: TLabel;
    Label195: TLabel;
    Label196: TLabel;
    Label197: TLabel;
    Label198: TLabel;
    Label199: TLabel;
    Label200: TLabel;
    DBText3: TDBText;
    Label201: TLabel;
    LM_DBEdit2: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit62: TDBEdit;
    DBEdit63: TDBEdit;
    DBEdit64: TDBEdit;
    DBEdit65: TDBEdit;
    DBEdit66: TDBEdit;
    DBEdit67: TDBEdit;
    DBEdit68: TDBEdit;
    DBEdit69: TDBEdit;
    DBEdit70: TDBEdit;
    DBEdit71: TDBEdit;
    DBEdit72: TDBEdit;
    DBEdit73: TDBEdit;
    DBEdit74: TDBEdit;
    DBEdit75: TDBEdit;
    DBEdit76: TDBEdit;
    DBEdit77: TDBEdit;
    DBEdit78: TDBEdit;
    DBEdit79: TDBEdit;
    DBEdit80: TDBEdit;
    DBEdit81: TDBEdit;
    DBEdit82: TDBEdit;
    DBEdit83: TDBEdit;
    QrOcorPDescri: TWideStringField;
    frxNF_Tudo: TfrxReport;
    frxDsNF: TfrxDBDataset;
    N4: TMenuItem;
    NotaFiscal1: TMenuItem;
    EdDesco7: TdmkEdit;
    Label202: TLabel;
    Enviaaocliente1: TMenuItem;
    N5: TMenuItem;
    Arquivo1: TMenuItem;
    Messenger1: TMenuItem;
    DiretrioFTP1: TMenuItem;
    frxBorTotCli2: TfrxReport;
    frxDsLotes: TfrxDBDataset;
    frxDsBordero: TfrxDBDataset;
    frxBorTotCli3: TfrxReport;
    Novo2: TMenuItem;
    frxDsLotesTxs: TfrxDBDataset;
    frxDsOcorOP: TfrxDBDataset;
    frxDsCHDevP: TfrxDBDataset;
    frxDsDPago: TfrxDBDataset;
    frxDsOcorP: TfrxDBDataset;
    frxBorTotCli0: TfrxReport;
    frxBorTotCli1: TfrxReport;
    frxAditivo_NP: TfrxReport;
    frxDsAditiv2: TfrxDBDataset;
    frxRichObject1: TfrxRichObject;
    frxDsContrat: TfrxDBDataset;
    frxAditivo: TfrxReport;
    frxPromissoria: TfrxReport;
    EdCPMF: TdmkEdit;
    Label203: TLabel;
    DBLaCPMF: TLabel;
    DBEdCPMF: TDBEdit;
    TabSheet29: TTabSheet;
    Panel46: TPanel;
    BtPagTerc: TBitBtn;
    BtExclui8: TBitBtn;
    DBG1: TDBGrid;
    QrLotesSALDOAPAGAR: TFloatField;
    TabSheet30: TTabSheet;
    Memo1: TMemo;
    Panel47: TPanel;
    BitBtn5: TBitBtn;
    Preencher1: TMenuItem;
    TudoFolhaembranco1: TMenuItem;
    QrLotesQuantN1: TFloatField;
    QrLotesQuantI1: TIntegerField;
    frxNF_Dados: TfrxReport;
    QrSaCartNumero: TFloatField;
    Dasos1: TMenuItem;
    Tudo1: TMenuItem;
    QrPagtosControle: TIntegerField;
    frxCartaSacadoDados: TfrxReport;
    frxDsSaCart: TfrxDBDataset;
    frxCartaSacadoVersoDados: TfrxReport;
    TabSheet31: TTabSheet;
    Panel48: TPanel;
    QrLotesAllQuit: TSmallintField;
    QrLotesSobraIni: TFloatField;
    QrLotesSobraNow: TFloatField;
    Label205: TLabel;
    DBEdit13: TDBEdit;
    Label206: TLabel;
    DBEdit84: TDBEdit;
    PMPagTerc: TPopupMenu;
    Multiplaseleo1: TMenuItem;
    Leitora1: TMenuItem;
    BtSobra: TBitBtn;
    Label209: TLabel;
    Sobras1: TMenuItem;
    frxBorTotCli2S: TfrxReport;
    frxBorTotCli3S: TfrxReport;
    frxBorTotCli0S: TfrxReport;
    frxBorTotCli1S: TfrxReport;
    Label204: TLabel;
    EdCartDep4: TdmkEditCB;
    CBCartDep4: TdmkDBLookupComboBox;
    DBEdBanco4: TDBEdit;
    DBEdAgencia4: TDBEdit;
    Label207: TLabel;
    DBEdConta4: TDBEdit;
    QrLotesItsCartDep: TIntegerField;
    QrSumOPPagoNeg: TFloatField;
    QrOcorPPagoNeg: TFloatField;
    QrOcorPSALDONEG: TFloatField;
    QrLotesIOC_VALNEG: TFloatField;
    QrLotesCPMF_VALNEG: TFloatField;
    QrLotesTAXAVALOR_TOTALNEG: TFloatField;
    QrLotesVAVALOREM_TOTALNEG: TFloatField;
    QrLotesTxsTaxaValNEG: TFloatField;
    QrLotesCHDevPgNEG: TFloatField;
    QrLotesDUDevPgNEG: TFloatField;
    AditivoAPNP1: TMenuItem;
    frxDsPagtos: TfrxDBDataset;
    LaCartDep1: TLabel;
    EdCartDep1: TdmkEditCB;
    CBCartDep1: TdmkDBLookupComboBox;
    Diversositens1: TMenuItem;
    Carteiradedepsito1: TMenuItem;
    PMAltera2: TPopupMenu;
    Alteraototaldodocumentoatual1: TMenuItem;
    Alteraodadatadodepsito1: TMenuItem;
    frxBorTotCliE2: TfrxReport;
    frxBorTotCliE3: TfrxReport;
    frxBorTotCliE0: TfrxReport;
    frxBorTotCliE1: TfrxReport;
    QrLotesIOFd_VAL: TFloatField;
    QrLotesIOFv_VAL: TFloatField;
    Semomeuendereo1: TMenuItem;
    Comomeuendereo1: TMenuItem;
    Label70: TLabel;
    QrLotesItsDescAte: TDateField;
    QrPagtosFatNum: TFloatField;
    frxCartaSacadoVersoTudo: TfrxReport;
    frxCartaSacadoTudo: TfrxReport;
    frxReport1: TfrxReport;
    frxBorProFis0_A: TfrxReport;
    frxDsLotesIts: TfrxDBDataset;
    frxBorProFis1_A: TfrxReport;
    frxProtesto: TfrxReport;
    frxAditivo_AP_NP: TfrxReport;
    frxAditivo_NP_AP: TfrxReport;
    frxJuros0: TfrxReport;
    frxDsJuros0: TfrxDBDataset;
    frxColigadas0: TfrxReport;
    frxColigadas1: TfrxReport;
    Label210: TLabel;
    EdTipific: TdmkEdit;
    QrLotesItsTipific: TSmallintField;
    EdStatusSPC: TdmkEdit;
    EdStatusSPC_TXT: TEdit;
    Label208: TLabel;
    TabSheet32: TTabSheet;
    MeSPC: TMemo;
    QrLotesItsStatusSPC: TSmallintField;
    BtConfirma2: TBitBtn;
    Label211: TLabel;
    BtSPC_CH: TBitBtn;
    BtSPC_DU: TBitBtn;
    PMSPC_Import: TPopupMenu;
    VerificaCPFCNPJatual1: TMenuItem;
    VerificatodosCPFCNPJaindanoverificados1: TMenuItem;
    N6: TMenuItem;
    Abrejaneladeconsulta1: TMenuItem;
    VerificatodosCPFCNPJpelovalormnimoconfigurao1: TMenuItem;
    N7: TMenuItem;
    ConsultaosCPFCNPJdositensselecionados1: TMenuItem;
    TabSheet33: TTabSheet;
    Panel49: TPanel;
    DBGrid23: TDBGrid;
    TabSheet34: TTabSheet;
    Panel50: TPanel;
    DBGrid24: TDBGrid;
    StCPF_DU: TStaticText;
    StCPF_CH: TStaticText;
    N8: TMenuItem;
    Imprimeconsultaselecionada1: TMenuItem;
    frxDsSPC_Result: TfrxDBDataset;
    frxSPC_Result: TfrxReport;
    Panel51: TPanel;
    DBGrid25: TDBGrid;
    StCPF_LI: TStaticText;
    Panel52: TPanel;
    Panel6: TPanel;
    BtInclui: TBitBtn;
    BtAltera2: TBitBtn;
    BtExclui2: TBitBtn;
    ProgressBar2: TProgressBar;
    ProgressBar1: TProgressBar;
    BitBtn1: TBitBtn;
    BtFTP: TBitBtn;
    BtImportaBCO: TBitBtn;
    BitBtn7: TBitBtn;
    BtWeb: TBitBtn;
    BtImportar: TBitBtn;
    GradeCHE: TDBGrid;
    GradeDUP: TDBGrid;
    Splitter3: TSplitter;
    N9: TMenuItem;
    ResultadodaconsultaaoSPC1: TMenuItem;
    QrLotesItsStatusSPC_TXT: TWideStringField;
    BtSPC_LI: TBitBtn;
    PMSPC_LotIts: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    PanelPB_SPC: TPanel;
    PB_SPC: TProgressBar;
    dmkEdTPDescAte: TdmkEditDateTimePicker;
    FormatoBP1: TMenuItem;
    frxBorTotCliBP2: TfrxReport;
    frxBorTotCliBP3: TfrxReport;
    frxBorTotCliBP0: TfrxReport;
    frxBorTotCliBP1: TfrxReport;
    QrLotesNOMEFANCLIENTE: TWideStringField;
    QrLotesWebCod: TIntegerField;
    QrLotesAlterWeb: TSmallintField;
    QrLotesIOFd: TFloatField;
    QrLotesIOFv: TFloatField;
    QrLotesTipoIOF: TSmallintField;
    QrLotesAtivo: TSmallintField;
    Panel53: TPanel;
    BtDesiste: TBitBtn;
    BitBtn6: TBitBtn;
    NoMostrarDias1: TMenuItem;
    MostrarDias1: TMenuItem;
    TPDataBase6: TdmkEditDateTimePicker;
    TPPagto6: TdmkEditDateTimePicker;
    N10: TMenuItem;
    Emitecheque1: TMenuItem;
    QrPagtosSerieCH: TWideStringField;
    QrPagtosAgencia_1: TIntegerField;
    QrPagtosNOMECLI: TWideStringField;
    frxCartaSacadoTudo2: TfrxReport;
    ModeloA1: TMenuItem;
    ModeloB1: TMenuItem;
    CartaaoEmitentedoCheque1: TMenuItem;
    frxCartaEmitCH: TfrxReport;
    QrSaEmitCH: TmySQLQuery;
    QrSaEmitCHCodigo: TIntegerField;
    QrSaEmitCHEmitente: TWideStringField;
    frxDsSaEmitCH: TfrxDBDataset;
    QrSaEmitCHCPF: TWideStringField;
    Label212: TLabel;
    EdEmail: TdmkEdit;
    BtNFSe: TBitBtn;
    NFSe1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure QrLotesAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrLotesBeforeOpen(DataSet: TDataSet);
    procedure EdClienteChange(Sender: TObject);
    procedure EdLoteKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdBandaChange(Sender: TObject);
    procedure EdValorExit(Sender: TObject);
    procedure EdVenctoExit(Sender: TObject);
    procedure EdCPFExit(Sender: TObject);
    procedure EdAdValoremExit(Sender: TObject);
    procedure EdTxaCompraExit(Sender: TObject);
    procedure TPDataChange(Sender: TObject);
    procedure BtCalendarioClick(Sender: TObject);
    procedure EdDMaisCExit(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure QrLotesBeforeClose(DataSet: TDataSet);
    procedure QrLotesItsCalcFields(DataSet: TDataSet);
    procedure EdTxaCompraChange(Sender: TObject);
    procedure QrLotesCalcFields(DataSet: TDataSet);
    procedure GradeAVDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure GradeCHKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure GradeCHDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure GradeAVKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdBandaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdRegiaoCompeKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdDataExit(Sender: TObject);
    procedure BtImportarClick(Sender: TObject);
    procedure BtFecharClick(Sender: TObject);
    procedure BtPagtosExcluiClick(Sender: TObject);
    procedure BtPagtosExclui2Click(Sender: TObject);
    procedure BtConfirma1Click(Sender: TObject);
    procedure BtNovoClick(Sender: TObject);
    procedure BorderauxdeCheques1Click(Sender: TObject);
    procedure BorderauxdeDuplicatas1Click(Sender: TObject);
    procedure BtCalendario2Click(Sender: TObject);
    procedure BtSair4Click(Sender: TObject);
    procedure EdVence4Exit(Sender: TObject);
    procedure EdTxaCompra4Exit(Sender: TObject);
    procedure EdDMaisDExit(Sender: TObject);
    procedure EdEmiss4Exit(Sender: TObject);
    procedure EdValor4Exit(Sender: TObject);
    procedure EdData4Exit(Sender: TObject);
    procedure EdCNPJExit(Sender: TObject);
    procedure EdCEPExit(Sender: TObject);
    procedure CBUFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtConfirma4Click(Sender: TObject);
    procedure EdTel1Exit(Sender: TObject);
    procedure GradeDUDrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure GradeDUKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtConfirma3Click(Sender: TObject);
    procedure TimerSCXTimer(Sender: TObject);
    procedure GradeC_LDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure TPChequeChange(Sender: TObject);
    procedure BtEntidadesClick(Sender: TObject);
    procedure BtCuidadoClick(Sender: TObject);
    procedure GradeDevDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GradeD_LDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtEntidades2Click(Sender: TObject);
    procedure BtCuidado2Click(Sender: TObject);
    procedure BtEntiTroca2Click(Sender: TObject);
    procedure BtInclui5Click(Sender: TObject);
    procedure BtDesiste5Click(Sender: TObject);
    procedure BtConfirma5Click(Sender: TObject);
    procedure EdTaxaCod5Change(Sender: TObject);
    procedure BtAltera5Click(Sender: TObject);
    procedure QrLotesTxsCalcFields(DataSet: TDataSet);
    procedure BtExclui5Click(Sender: TObject);
    procedure QrLotesTxsAfterScroll(DataSet: TDataSet);
    procedure RGFormaClick(Sender: TObject);
    procedure BtAltera2Click(Sender: TObject);
    procedure QrLotesItsAfterOpen(DataSet: TDataSet);
    procedure EdTaxaQtd5Change(Sender: TObject);
    procedure EdTaxaTxa5Change(Sender: TObject);
    procedure EdDesco4Exit(Sender: TObject);
    procedure EdTaxaCod5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdDuplicataExit(Sender: TObject);
    procedure GradeX1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrLotesItsAfterScroll(DataSet: TDataSet);
    procedure GradeLDU1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure GradeLDU2DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtExclui2Click(Sender: TObject);
    procedure EdValorBase6Change(Sender: TObject);
    procedure TPPagto6Change(Sender: TObject);
    procedure BtDesiste6Click(Sender: TObject);
    procedure BtConfirma6Click(Sender: TObject);
    procedure BtPagamento6Click(Sender: TObject);
    procedure EdAPagar6Change(Sender: TObject);
    procedure QrOcorPCalcFields(DataSet: TDataSet);
    procedure QrOcorPAfterOpen(DataSet: TDataSet);
    procedure BtPagtosExclui6Click(Sender: TObject);
    procedure EdBancoChange(Sender: TObject);
    procedure EdBanco4Change(Sender: TObject);
    procedure Emitenteselecionado1Click(Sender: TObject);
    procedure Todosemitentesnocadastrados1Click(Sender: TObject);
    procedure BtEntiTroca1Click(Sender: TObject);
    procedure Sacadoselecionado1Click(Sender: TObject);
    procedure Todossacadosnocadastrados1Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtSair2Click(Sender: TObject);
    procedure QrLotesAfterOpen(DataSet: TDataSet);
    procedure BtPagamento7Click(Sender: TObject);
    procedure BtDesiste7Click(Sender: TObject);
    procedure EdValorBase7Change(Sender: TObject);
    procedure EdJurosBase7Change(Sender: TObject);
    procedure TPPagto7Change(Sender: TObject);
    procedure EdJuros7Change(Sender: TObject);
    procedure BtConfirma7Click(Sender: TObject);
    procedure QrCHDevPCalcFields(DataSet: TDataSet);
    procedure QrCHDevPAfterOpen(DataSet: TDataSet);
    procedure BtExclui7Click(Sender: TObject);
    procedure PMBorderoPopup(Sender: TObject);
    procedure EdJurosBase6Change(Sender: TObject);
    procedure EdJuros6Change(Sender: TObject);
    procedure EdCVChange(Sender: TObject);
    procedure EdDBValLiqChange(Sender: TObject);
    procedure EsteBorder1Click(Sender: TObject);
    procedure DesteBorderemdiante1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure Cabealhodoborderatual1Click(Sender: TObject);
    procedure BtPagtosInclui8Click(Sender: TObject);
    procedure QrPagtosCalcFields(DataSet: TDataSet);
    procedure Pagamento1Click(Sender: TObject);
    procedure Todospagamentosdestebordero1Click(Sender: TObject);
    procedure QrPagtosAfterOpen(DataSet: TDataSet);
    procedure BtPagtosExclui8Click(Sender: TObject);
    procedure Novo1Click(Sender: TObject);
    procedure Rpido1Click(Sender: TObject);
    procedure EdDBAPGLIQChange(Sender: TObject);
    procedure QrSaCartAfterScroll(DataSet: TDataSet);
    procedure BtChequeClick(Sender: TObject);
    procedure Nominal1Click(Sender: TObject);
    procedure Portador1Click(Sender: TObject);
    procedure QrSaCartAfterOpen(DataSet: TDataSet);
    procedure EdDRChange(Sender: TObject);
    procedure BtPagtosInclui9Click(Sender: TObject);
    procedure QrDPagoBeforeClose(DataSet: TDataSet);
    procedure QrDPagoAfterOpen(DataSet: TDataSet);
    procedure BtPagtosExclui9Click(Sender: TObject);
    procedure QrDPagoCalcFields(DataSet: TDataSet);
    procedure EdDVChange(Sender: TObject);
    procedure EdDTChange(Sender: TObject);
    procedure EdCRChange(Sender: TObject);
    procedure EdCTChange(Sender: TObject);
    procedure EdRTChange(Sender: TObject);
    procedure EdVTChange(Sender: TObject);
    procedure EdSTChange(Sender: TObject);
    procedure EdOAChange(Sender: TObject);
    procedure EdTTChange(Sender: TObject);
    procedure BtSinaleiraClick(Sender: TObject);
    procedure EdCNPJChange(Sender: TObject);
    procedure EdCPFChange(Sender: TObject);
    procedure GradeC_LTitleClick(Column: TColumn);
    procedure GradeD_LTitleClick(Column: TColumn);
    procedure EdBanco0Exit(Sender: TObject);
    procedure EdBanco0Change(Sender: TObject);
    procedure Detalhado1Click(Sender: TObject);
    procedure Prconfigurado1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure BtCheckImpClick(Sender: TObject);
    procedure BtRiscoSacClick(Sender: TObject);
    procedure EdNFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCMC_7Change(Sender: TObject);
    procedure EdVenctoEnter(Sender: TObject);
    procedure QrSaCartCalcFields(DataSet: TDataSet);
    procedure BtOcorenciasClick(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure EdRegiaoCompeChange(Sender: TObject);
    procedure EdAgenciaChange(Sender: TObject);
    procedure EdContaChange(Sender: TObject);
    procedure EdCodCliChange(Sender: TObject);
    procedure EdNomCliChange(Sender: TObject);
    procedure BtIncluiocorClick(Sender: TObject);
    procedure Cliente1Click(Sender: TObject);
    procedure Cheque1Click(Sender: TObject);
    procedure Duplicata1Click(Sender: TObject);
    procedure QrLotesItsBeforeClose(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrJuros0CalcFields(DataSet: TDataSet);
    procedure EdCPFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtHistoricoClick(Sender: TObject);
    procedure Clienteselecionado1Click(Sender: TObject);
    procedure Emitentesacadoselecionado1Click(Sender: TObject);
    procedure Ambos1Click(Sender: TObject);
    procedure Enviaaocliente1Click(Sender: TObject);
    procedure frxNF_TudoGetValue(const VarName: String; var Value: Variant);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtFTPClick(Sender: TObject);
    procedure Arquivo1Click(Sender: TObject);
    procedure Messenger1Click(Sender: TObject);
    procedure DiretrioFTP1Click(Sender: TObject);
    function frxBorTotCli2UserFunction(const MethodName: String;
      var Params: Variant): Variant;
    procedure frxBorTotCli2GetValue(const VarName: String;
      var Value: Variant);
    procedure Novo2Click(Sender: TObject);
    procedure frxAditivo_NPGetValue(const VarName: String;
      var Value: Variant);
    procedure BtPagTercClick(Sender: TObject);
    procedure BtExclui8Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure TudoFolhaembranco1Click(Sender: TObject);
    procedure frxCartaSacadoDadosGetValue(const VarName: String;
      var Value: Variant);
    procedure BtImportaBCOClick(Sender: TObject);
    procedure Multiplaseleo1Click(Sender: TObject);
    procedure Leitora1Click(Sender: TObject);
    procedure BtSobraClick(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure DBEdBanco4Change(Sender: TObject);
    procedure QrSumOPCalcFields(DataSet: TDataSet);
    procedure BtWebClick(Sender: TObject);
    procedure Diversositens1Click(Sender: TObject);
    procedure Carteiradedepsito1Click(Sender: TObject);
    procedure Alteraototaldodocumentoatual1Click(Sender: TObject);
    procedure Alteraodadatadodepsito1Click(Sender: TObject);
    procedure Semomeuendereo1Click(Sender: TObject);
    procedure Comomeuendereo1Click(Sender: TObject);
    procedure Dasos1Click(Sender: TObject);
    procedure frxCartaSacadoVersoTudoGetValue(const VarName: String;
      var Value: Variant);
    procedure Novo11Click(Sender: TObject);
    procedure Contabilidade1Click(Sender: TObject);
    procedure AditivoNPAP1Click(Sender: TObject);
    procedure AditivoAPNP1Click(Sender: TObject);
    procedure AditivoNP1Click(Sender: TObject);
    procedure Aditivo1Click(Sender: TObject);
    procedure Promissria1Click(Sender: TObject);
    procedure AutorizaodeProtesto1Click(Sender: TObject);
    procedure Coligadas1Click(Sender: TObject);
    procedure MargemtOtal1Click(Sender: TObject);
    procedure EdCNPJKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure EdStatusSPCChange(Sender: TObject);
    procedure EdStatusSPCKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdStatusSPC_TXTKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtSPC_CHClick(Sender: TObject);
    procedure BtSPC_DUClick(Sender: TObject);
    procedure VerificaCPFCNPJatual1Click(Sender: TObject);
    procedure VerificatodosCPFCNPJaindanoverificados1Click(Sender: TObject);
    procedure Abrejaneladeconsulta1Click(Sender: TObject);
    procedure VerificatodosCPFCNPJpelovalormnimoconfigurao1Click(
      Sender: TObject);
    procedure ConsultaosCPFCNPJdositensselecionados1Click(Sender: TObject);
    procedure Imprimeconsultaselecionada1Click(Sender: TObject);
    procedure ResultadodaconsultaaoSPC1Click(Sender: TObject);
    procedure BtSPC_LIClick(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure FormatoBP1Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure NoMostrarDias1Click(Sender: TObject);
    procedure MostrarDias1Click(Sender: TObject);
    procedure Emitecheque1Click(Sender: TObject);
    procedure ModeloA1Click(Sender: TObject);
    procedure ModeloB1Click(Sender: TObject);
    procedure CartaaoEmitentedoCheque1Click(Sender: TObject);
    procedure QrSaEmitCHAfterOpen(DataSet: TDataSet);
    procedure QrSaEmitCHAfterScroll(DataSet: TDataSet);
    procedure QrLotesTxsAfterOpen(DataSet: TDataSet);
    procedure QrLotesTxsBeforeClose(DataSet: TDataSet);
    procedure QrOcorPBeforeClose(DataSet: TDataSet);
    procedure QrCHDevPBeforeClose(DataSet: TDataSet);
    procedure QrPagtosBeforeClose(DataSet: TDataSet);
    procedure BtNFSeClick(Sender: TObject);
    procedure NFSe1Click(Sender: TObject);
    procedure EdCEPEnter(Sender: TObject);
  private
    FLogo1Path, FECEP: String;
    FLogo1Exists: Boolean;
    FConferido, FMostra, FGradeFocused, FCasasAdValorem, FLoteWebDone: Integer;
    FIncluindoCh, FIncluindoDU: Boolean;
    //FValor1, FValor2: Double;
    FTaxa, FJuro, FValr: array[0..6] of Double;
    FDadosJurosColigadas: MyArray07ArrayR03;
    FImprimindoColigadas: Boolean;
    FControlIts, FControlTxs, FOcorP, FOcorA, FCHDevA, FCHDevP: Integer;
    FContaST: Integer;
    FEscapeEnabed: Boolean;
    FDPago: Integer;
    //FNomeSacado,
    FCPFSacado: String;
    FOrelha: Integer;
    FTempo, FUltim: TDateTime;
    FMEU_ENDERECO: Boolean;
    //
    procedure InfoTempo(Tempo: TDateTime; Texto: String; Inicial: Boolean);
    function CriaBAC_Pesq(Banco, Agencia, Conta: String; DVCC: Integer): String;
    procedure VerificaNF(NF: Integer);
    function RiscoSacado(CPF: String): Double;
    procedure AtualizaAcumulado;
    procedure RecalculaBordero(Lote: Integer);
    procedure DefineVarDup;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure IncluiRegistro(Tipo, TipoLote: Integer);
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status : String; Codigo : Integer);
    procedure DefParams;
    function GetTitle(TipoLote: Integer): String;
    function LocalizaEmitente(MudaNome: Boolean): Boolean;
    function LocalizaSacado(MudaNome: Boolean): Boolean;
    procedure ConfiguraTaxas;
    procedure CalculaDiasCH;
    procedure CalculaDiasDU;
    procedure ReopenIts;
    procedure ReopenTxs;
    procedure ReopenOcorA(Cliente: Integer);
    procedure ReopenOcorP;
    procedure ReopenCHDevA(Cliente: Integer);
    procedure ReopenCHDevP;
    procedure ReopenDOpen(Cliente: Integer);
    procedure ReopenDPago;
    procedure ReopenRiscoC(Cliente: Integer);
    procedure ImprimeBorderoClienteBP;
    //
    procedure ReopenSacOcorA;
    procedure ReopenSacCHDevA;
    procedure ReopenSacDOpen;
    procedure ReopenSacRiscoC;
    procedure FechaTudoSac;
    //
    procedure IncluiCheque;
    procedure IncluiDuplicata;
    procedure CalculaValoresChequeAlterado;
    procedure CalculaValoresCH_DU_Refresh(Controle, Dias: Integer;
              TxaCompra, Valor: Double);
    procedure CalculaValoresDuplicataAlterado;
    procedure CalculaValoresChequeImportando(Dias: Integer);
    procedure CalculaValoresDuplicataImportando(Dias: Integer);
    procedure ImportaDuplicata(Codigo, Cliente: Integer);
    procedure ImportaCheque(Codigo, Cliente, CBE: Integer);
    procedure ExcluiImportLote(Tipo, Ordem: Integer);
    procedure IgnoraImportLote(Tipo, Ordem: Integer);
    function NaoPermiteCHs: Boolean;
    function NaoPermiteDUs: Boolean;
    procedure LancaTaxasNovoLote(Cliente, TipoLote, NumeroDoLote: Integer);
    //function GetTipoLote(TipoLote: Integer): TTipoLote;
    procedure CalculaTotalTaxa;
    function LoteExiste(Cliente, Codigo, Lote: Integer): Boolean;
    procedure DefineNovoLote;
    procedure DefineNovaNF;
    procedure CalculaJurosOcor;
    procedure CalculaJurosCHDev;
    procedure CalculaAPagarOcor;
    procedure CalculaAPagarCHDev;
    procedure ReopenBanco0;
    procedure ReopenBanco4;
    procedure ConfiguraPgCH(Data: TDateTime);
    procedure ConfiguraPgOC(Data: TDateTime);
    procedure CalculaPagtoAlinIts(Quitacao: TDateTime; AlinIts: Integer);
    procedure CalculaPagtoLotesIts(LotesIts: Integer);
    procedure CalculaPagtoOcorrPg(Quitacao: TDateTime; OcorrPg: Integer);
    procedure RefreshEmBorderoCH(Lote: Integer; Unico: Boolean);
    procedure RefreshEmBorderoDU(Lote: Integer; Unico: Boolean);
    procedure RefreshCheque;
    procedure RefreshDuplicata;
    function ReopenContrat: Boolean;
    procedure AtualizaPagamento(Lote, Cliente: Integer; Data: TDateTime;
              Liquido: Double);
    procedure ExcluiItemPagamento;
    procedure ExcluiTodasParcelas;
    procedure IncluiPagamento;
    procedure PesquisaRiscoSacado(CPF: String);
    procedure PagaChequeDevolvido;
    //
    procedure ExecutaListaDeImpressoes;
    procedure ImprimeBorderoClienteE2;
    //procedure EnviaBorderoCliente2;
    procedure ImprimeBorderoContabilNovo;
    procedure ImprimeAditivoNPAPNovo;
    procedure ImprimeAditivoAPNPNovo;
    procedure ImprimeAditivoNPNovo;
    procedure ImprimeAditivoNovo;
    procedure ImprimeNotaPromissoriaNovo;
    procedure ImprimeAutorizacaodeProtestoNovo;
    procedure ImprimeMargemColigadasNovo;
    procedure AtualizaImpressaoDeCarta(Lote, Tipo: Integer);
    procedure DefineRiscos(Cliente: Integer);
    procedure VerificaNomeimport;
    procedure InsereImportLote;
    procedure MostraHistorico(Quem: Integer);
    procedure ExcluiChequeSelecionado;
    procedure LocalizaProximoChNaoVerde;
    procedure LocalizaProximoDUNaoVerde;
    procedure ReopenRepCli;
    procedure ImprimeBorderoClienteSobra(Dias: Boolean);
    procedure ObtemTipoEPercDeIOF2008(const TipoEnt, EhSimples: Integer;
              const ValOperacao: Double; var Perc: Double; var TipoIOF: Integer);
    procedure ImprimeNF(MeuEndereco: Boolean);
    procedure ConsultaCNPJCPF_CH();
    procedure VerificaCPFCNP_XX_LotesIts(SelType: TSelType);
    function DefineDataSetsNF(frx: TfrxReport): Boolean;
  public
    { Public declarations }
    FCHDevOpen_Total, FOcorA_Total: Double;
    FLoteMSN, FLoteWeb: Integer;
    FTipoLote: Integer;
    FImprime, FRelatorios: Integer;
    FMudouSaldo: Boolean;
    FArrayMySQLEE: array[0..5] of TmySQLQuery;
    FArrayMySQLLO: array[0..5] of TmySQLQuery;
    FArrayMySQLSI: array[0..5] of TmySQLQuery;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ReopenPagtos;
    procedure CalculaLote(Lote: Double; AvisaLoteAntigo: Boolean);
    procedure DefineAdValoremEmpresas;
    procedure DefineCompraEmpresas(Codigo, Controle: Integer);
    function SQL_CH(Controle, Comp, Banco, Agencia, Cheque, DMais, Dias,
              Codigo, Praca, Tipific, StatusSPC: Integer; Conta, CPF, Emitente, Vencto, DCompra,
              DDeposito: String; Valor: Double; LaTipoCaption: String;
              Cliente: Integer): Integer;
    function SQL_DU(Codigo, Controle, DMais, Dias, Banco, Agencia: Integer;
              Valor, Desco, Bruto: Double; Duplic, CPF, Sacado, Vencto, DCompra,
              DDeposito, Emissao, DescAte, LaTipoCaption: String; Cliente, CartDep:
              Integer): Integer;
    procedure ImprimeCartaAoSacadoTudoNovo(Modelo: Integer);
    function ReopenEmitCH(SelType: TSelType): Boolean;
    procedure ImprimeCartaAoSacadoDados;
    procedure Importar(DarIni: String; Tipo: Integer);
    function ObtemEmitente(Banco, Agencia, Conta: String;
             var Emitente, CPF: String; AvisaNaoEncontrado: Boolean): Boolean;
    function ReopenSaCart(SelType: TSelType): Boolean;
    procedure HabilitaBotoes(Meu_Sit, TipoDoc: Integer);
  end;

var
  FmLotes1: TFmLotes1;
const
  FFormatFloat = '00000000';

implementation

uses Module, ModuleGeral, Principal, SomaDiasCred, UCreate, UCashier, LotesPg,
  LotesLoc, RiscoCli, LotesDlg, OcorreuC, EntiJur1, LotesEmi, HistCliEmi,
  RiscoAll, Modulc, LotesRepCliMul, LotesWeb, LotesCartDep, Duplicatas2,
  GetData, QuaisItens, MyDBCheck, PesqCPFCNPJ, ModuleLotes, UnitSPC, SPC_Pesq,
  UnMyObjects, MyListas, UnMyPrinters, ChequesGer, NFSe_PF_0000, NFSe_PF_0201,
  UnCEP;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmLotes1.LocCod(Atual, Codigo: Integer);
begin
  InfoTempo(Now, 'Reabertura de Border�', True);
  Screen.Cursor := crHourGlass;
  try
    DefParams;
    GOTOy.LC(Atual, Codigo);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLotes1.Va(Para: TVaiPara);
begin
  Screen.Cursor := crHourGlass;
  try
    DefParams;
    LaRegistro.Caption := GOTOy.Go(Para, QrLotesCodigo.Value, LaRegistro.Caption[2]);
  finally
    Screen.Cursor := crDefault;
  end;
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmLotes1.DefParams;
begin
  VAR_GOTOTABELA := 'Lotes';
  VAR_GOTOMYSQLTABLE := QrLotes;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := ' TxCompra+ValValorem+NF+'+
                   Geral.FF0(FmPrincipal.FConnections)+'<>0';

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  VAR_SQLx.Add('ELSE en.Nome END NOMECLIENTE, ');
  VAR_SQLx.Add('CASE WHEN en.Tipo=0 THEN en.CNPJ');
  VAR_SQLx.Add('ELSE en.CPF END CNPJCPF, ');
  VAR_SQLx.Add('CASE WHEN en.Tipo=0 THEN en.Fantasia '); 
  VAR_SQLx.Add('ELSE en.Apelido END NOMEFANCLIENTE, ');
  VAR_SQLx.Add('lo.*, en.LimiCred, ');
  VAR_SQLx.Add('en.CBE CBECli, en.SCB SCBCli, en.QuantN1, en.QuantI1');
  VAR_SQLx.Add('FROM lotes lo');
  VAR_SQLx.Add('LEFT JOIN entidades en ON en.Codigo=lo.Cliente');
  VAR_SQLx.Add('WHERE lo.TxCompra+lo.ValValorem+lo.NF+'+
    Geral.FF0(FmPrincipal.FConnections)+'<>0');
  //
  VAR_SQL1.Add('AND lo.Codigo=:P0');
  //
  VAR_SQLa.Add('');
  //
end;

procedure TFmLotes1.ModeloA1Click(Sender: TObject);
begin
  ImprimeCartaAoSacadoTudoNovo(1);
end;

procedure TFmLotes1.ModeloB1Click(Sender: TObject);
begin
  ImprimeCartaAoSacadoTudoNovo(2);
end;

procedure TFmLotes1.MostraEdicao(Mostra : Integer; Status: String; Codigo: Integer);
var
  i, Lins: Integer;
  Taxas: MyArrayR07;
begin
  PainelBco0.Visible := False;
  case Mostra of
    0: // Trava
    begin
      PainelControle.Visible       := True;
      PainelDados.Visible          := True;
      PainelEdita.Visible          := False;
      PainelBandaMagnetica.Visible := False;
      PainelImporta.Visible        := False;
      PainelDuplicata.Visible      := False;
      PainelTaxas.Visible          := False;
      Painel6PgOcor.Visible        := False;
      //
      PainelItens.Visible          := False;
      PainelMassico.Visible        := False;
      //
      PainelControle.Enabled       := True;
      PainelTitulo.Enabled         := True;
      Panel23.Visible              := True;
      Panel31.Visible              := True;
      Panel27.Visible              := False;
      //
    end;
    1: // Inclui /Altera lote
    begin
      //////////////////////////////////////////////////////////////////////////
      PainelEdita.Visible          := True;
      PainelDados.Visible          := False;
      PainelControle.Visible       := False;
      PainelBandaMagnetica.Visible := False;
      PainelImporta.Visible        := False;
      PainelDuplicata.Visible      := False;
      PainelTaxas.Visible          := False;
      Painel6PgOcor.Visible        := False;
      //
      EdCliente.Text        := CO_VAZIO;
      CBCliente.KeyValue    := NULL;
      EdLote.Text           := '';
      EdAdValorem.Text      := '';
      GradeAV.RowCount := 2;
      MyObjects.LimpaGrade(GradeAV, 1, 1, True);
      Lins := FmPrincipal.FMyDBs.MaxDBs +1;
      if Lins < 2 then Lins := 2;
      GradeAV.RowCount := Lins;
      if Status = CO_INCLUSAO then
      begin
        LaCartDep1.Visible := True;
        EdCartDep1.Visible := True;
        CBCartDep1.Visible := True;
        //
        if FTipoLote = 0 then
        begin
          EdCartDep1.Text     := FormatFloat('0', Dmod.QrControleCartCheques.Value);
          CBCartDep1.KeyValue := Dmod.QrControleCartCheques.Value;
        end else
        begin
          EdCartDep1.Text     := FormatFloat('0', Dmod.QrControleCartDuplic.Value);
          CBCartDep1.KeyValue := Dmod.QrControleCartDuplic.Value;
        end;
        //
        EdCodigo.Text    := FormatFloat(FFormatFloat, Codigo);
        EdPIS.Text       := Geral.FFT(Dmod.QrControlePIS.Value, 4, siPositivo);
        EdPIS_R.Text     := Geral.FFT(Dmod.QrControlePIS_R.Value, 4, siPositivo);
        EdCOFINS.Text    := Geral.FFT(Dmod.QrControleCOFINS.Value, 4, siPositivo);
        EdCOFINS_R.Text  := Geral.FFT(Dmod.QrControleCOFINS_R.Value, 4, siPositivo);
        EdISS.Text       := Geral.FFT(Dmod.QrControleISS.Value, 4, siPositivo);
        EdCPMF.Text      := Geral.FFT(Dmod.QrControleCPMF.Value, 4, siPositivo);
        EdIRRF.Text      := '0,0000';
        EdNF.Text        := '';
        EdCBE.Text       := '';
        CkSCB.Checked    := False;
        EdNF.Enabled     := True;
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
          if FmPrincipal.FMyDBs.Connected[i] = '1' then
            GradeAV.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];

      end else begin
        LaCartDep1.Visible := False;
        EdCartDep1.Visible := False;
        CBCartDep1.Visible := False;
        //
        EdCodigo.Text      := DBEdCodigo.Text;
        EdCliente.Text     := Geral.FF0(QrLotesCliente.Value);
        CBCliente.KeyValue := QrLotesCliente.Value;
        EdLote.Text        := Geral.FF0(QrLotesLote.Value);
        TPData.Date        := QrLotesData.Value;
        RGTipo.ItemIndex   := 0;
        //
        EdPIS.Text       := Geral.FFT(QrLotesPIS.Value, 4, siPositivo);
        EdPIS_R.Text     := Geral.FFT(QrLotesPIS_R.Value, 4, siPositivo);
        EdCOFINS.Text    := Geral.FFT(QrLotesCOFINS.Value, 4, siPositivo);
        EdCOFINS_R.Text  := Geral.FFT(QrLotesCOFINS_R.Value, 4, siPositivo);
        EdISS.Text       := Geral.FFT(QrLotesISS.Value, 4, siPositivo);
        EdCPMF.Text      := Geral.FFT(QrLotesCPMF.Value, 4, siPositivo);
        EdIRRF.Text      := Geral.FFT(QrLotesIRRF.Value, 4, siPositivo);
        EdNF.Text        := MLAGeral.FFD(QrLotesNF.Value, 6, siNegativo);
        EdCBE.Text       := MLAGeral.FFD(QrLotesCBE.Value, 0, siNegativo);
        CkSCB.Checked    := Geral.IntToBool_0(QrLotesSCB.Value);
        //EdCartDep1.Text  := CartDep � nos itens
        if QrLotesNF.Value = 0 then EdNF.Enabled := True
        else EdNF.Enabled := False;
        //
        EdAdValorem.Text := Geral.FFT(
          QrLotesAdValorem.Value, FCasasAdValorem, siPositivo);
        //
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          if FmPrincipal.FMyDBs.Connected[i] = '1' then
          begin
            GradeAV.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
            GradeAV.Cells[01, i+1] := Geral.FFT(
              FArrayMySQLLO[i].FieldByName('AdValorem').AsFloat, 6, siPositivo);
          end;
        end;
      end;
      //GradeAV.Height := (GradeAV.RowCount * GradeAV.DefaultRowHeight) + 9;
      EdCliente.SetFocus;
    end;
    2: // inclui / altera cheque
    begin
      PainelBandaMagnetica.Visible := True;
      PainelControle.Visible       := False;
      PainelDados.Visible          := False;
      PainelEdita.Visible          := False;
      PainelImporta.Visible        := False;
      PainelDuplicata.Visible      := False;
      PainelTaxas.Visible          := False;
      Painel6PgOcor.Visible        := False;
      //
      GradeCH.RowCount := 2;
      MyObjects.LimpaGrade(GradeCH, 1, 1, True);
      Lins := FmPrincipal.FMyDBs.MaxDBs +1;
      if Lins < 2 then Lins := 2;
      GradeCH.RowCount := Lins;
      if Status = CO_INCLUSAO then
      begin
        EdBanda.Text := '';
        EdTipific.Text := '';
        EdRegiaoCompe.Text := '';
        EdBanco.Text := '';
        EdAgencia.Text := '';
        EdConta.Text := '';
        EdCheque.Text := '';
        EdValor.Text := '';
        EdVencto.Text := '000000';
        EdData.Text := FormatDateTime('dd/mm/yyyy', QrLotesData.Value);
        edCPF.Text := '';
        EdEmitente.Text := '';
        EdTxaCompra.Text := Geral.FFT(DmLotes.QrClientesFatorCompra.Value, 4, siPositivo);
        EdDMaisC.Text := Geral.FFT(DmLotes.QrClientesDMaisC.Value, 0, siNegativo);
        EdStatusSPC.ValueVariant := -1;
        EdBanda.SetFocus;
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          if FmPrincipal.FMyDBs.Connected[i] = '1' then
          begin
            GradeCH.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
            FArrayMySQLEE[i].Close;
            FArrayMySQLEE[i].Params[0].AsInteger := QrLotesCliente.Value;
            FArrayMySQLEE[i].Open;
            //
            if QrLotesSpread.Value = 0 then
              GradeCH.Cells[01, i+1] := Geral.FFT(
                FArrayMySQLEE[i].FieldByName('Maior').AsFloat, 6, siPositivo)
            else
              GradeCH.Cells[01, i+1] := Geral.FFT(
                FArrayMySQLEE[i].FieldByName('Menor').AsFloat, 6, siPositivo);
          end;
         end;
      end else begin
        EdBanda.Text := '';
        EdTipific.Text     := MLAGeral.FFD(QrLotesItsTipific.Value, 1, siPositivo);
        EdRegiaoCompe.Text := MLAGeral.FFD(QrLotesItsPraca.Value, 3, siPositivo);
        EdBanco.Text       := MLAGeral.FFD(QrLotesItsBanco.Value, 3, siPositivo);
        EdAgencia.Text     := MLAGeral.FFD(QrLotesItsAgencia.Value, 4, siPositivo);
        EdConta.Text       := QrLotesItsConta.Value;
        EdCheque.Text      := MLAGeral.FFD(QrLotesItsCheque.Value, 6, siPositivo);
        EdValor.Text       := Geral.FFT(QrLotesItsValor.Value, 2, siPositivo);
        EdVencto.Text      := FormatDateTime('dd/mm/yyyy', QrLotesItsVencto.Value);
        EdData.Text        := FormatDateTime('dd/mm/yyyy', QrLotesItsDCompra.Value);
        EdCPF.Text         := Geral.FormataCNPJ_TT(QrLotesItsCPF.Value);
        EdEmitente.Text    := QrLotesItsEmitente.Value;
        EdStatusSPC.Text   := FormatFloat('0', QrLotesItsStatusSPC.Value);
        //
        EdTxaCompra.Text   := Geral.FFT(QrLotesItsTxaCompra.Value, 4, siPositivo);
        EdDMaisC.Text      := Geral.FFT(QrLotesItsDMais.Value, 0, siNegativo);
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          Taxas := Dmod.ObtemTaxaDeCompraRealizada(QrLotesItsControle.Value);
          if FmPrincipal.FMyDBs.Connected[i] = '1' then
          begin
            GradeCH.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
            GradeCH.Cells[01, i+1] := Geral.FFT(Taxas[i+1], 6, siPositivo);
          end;
        end;
        EdValor.SetFocus;
        PesquisaRiscoSacado(EdCPF.Text);
        BtConfirma2.Enabled := True;
      end;
    end;
    3:
    begin
      PainelImporta.Visible        := True;
      PainelBandaMagnetica.Visible := False;
      PainelControle.Visible       := False;
      PainelDados.Visible          := False;
      PainelEdita.Visible          := False;
      PainelDuplicata.Visible      := False;
      PainelTaxas.Visible          := False;
      Painel6PgOcor.Visible        := False;
      //
      ArqSCX.DefineContagens(GradeC, GradeD);
      if FmPrincipal.FCheque > 0 then
      begin
        BtConfirma1.Enabled    := True;
        BtPagtosExclui.Enabled := True;
        BtSPC_CH.Enabled       := True;
      end else begin
        BtConfirma1.Enabled    := False;
        BtPagtosExclui.Enabled := False;
        BtSPC_CH.Enabled       := False;
      end;
      if FmPrincipal.FDuplicata > 0 then
      begin
        BtConfirma3.Enabled     := True;
        BtPagtosExclui2.Enabled := True;
        BtSPC_DU.Enabled        := True;
      end else begin
        BtConfirma3.Enabled     := False;
        BtPagtosExclui2.Enabled := False;
        BtSPC_DU.Enabled        := False;
      end;
    end;
    4: // inclui / altera Duplicata
    begin
      PainelDuplicata.Visible      := True;
      PainelBandaMagnetica.Visible := False;
      PainelControle.Visible       := False;
      PainelDados.Visible          := False;
      PainelEdita.Visible          := False;
      PainelImporta.Visible        := False;
      PainelTaxas.Visible          := False;
      Painel6PgOcor.Visible        := False;
      //
      GradeDU.RowCount := 2;
      MyObjects.LimpaGrade(GradeDU, 1, 1, True);
      Lins := FmPrincipal.FMyDBs.MaxDBs +1;
      if Lins < 2 then Lins := 2;
      GradeDU.RowCount := Lins;
      if Status = CO_INCLUSAO then
      begin
        EdEmiss4.Text       := FormatDateTime('dd/mm/yyyy', QrLotesData.Value);
        EdData4.Text        := FormatDateTime('dd/mm/yyyy', QrLotesData.Value);
        EdDuplicata.Text    := '';
        EdValor4.Text       := '';
        EdDesco4.Text       := '';
        dmkEdTPDescAte.Date := 0;
        EdVence4.Text       := '000000';
        EdTxaCompra4.Text   := Geral.FFT(DmLotes.QrClientesFatorCompra.Value, 4, siPositivo);
        EdDMaisD.Text       := Geral.FFT(DmLotes.QrClientesDMaisD.Value, 0, siNegativo);
        EdSacado.Text       := '';
        EdCNPJ.Text         := '';
        EdRua.Text          := '';
        EdNumero.Text       := '';
        EdBairro.Text       := '';
        EdCidade.Text       := '';
        EdCEP.Text          := '';
        CBUF.Text           := '';
        EdTel1.Text         := '';
        EdEmail.Text        := '';
        EdCartDep4.Text     := FormatFloat('0', Dmod.QrControleCartDuplic.Value);
        CBCartDep4.KeyValue := Dmod.QrControleCartDuplic.Value;
        //
        EdEmiss4.SetFocus;
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          if FmPrincipal.FMyDBs.Connected[i] = '1' then
          begin
            GradeDU.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
            FArrayMySQLEE[i].Close;
            FArrayMySQLEE[i].Params[0].AsInteger := QrLotesCliente.Value;
            FArrayMySQLEE[i].Open;
            if QrLotesSpread.Value = 0 then
              GradeDU.Cells[01, i+1] := Geral.FFT(
                FArrayMySQLEE[i].FieldByName('Maior').AsFloat, 6, siPositivo)
            else
              GradeDU.Cells[01, i+1] := Geral.FFT(
                FArrayMySQLEE[i].FieldByName('Menor').AsFloat, 6, siPositivo);
          end;
        end;
      end else begin
        EdEmiss4.Text       := FormatDateTime('dd/mm/yyyy', QrLotesItsEmissao.Value);
        EdData4.Text        := FormatDateTime('dd/mm/yyyy', QrLotesItsDCompra.Value);
        EdDuplicata.Text    := QrLotesItsDuplicata.Value;
        EdValor4.Text       := Geral.FFT(QrLotesItsBruto.Value, 2, siPositivo);
        EdDesco4.Text       := Geral.FFT(QrLotesItsDesco.Value, 2, siPositivo);
        dmkEdTPDescAte.Date := QrLotesItsDescAte.Value;
        EdVence4.Text       := FormatDateTime('dd/mm/yyyy', QrLotesItsVencto.Value);
        EdTxaCompra4.Text   := Geral.FFT(QrLotesItsTxaCompra.Value, 4, siPositivo);
        EdDMaisD.Text       := Geral.FFT(QrLotesItsDMais.Value, 0, siNegativo);
        EdSacado.Text       := QrLotesItsEmitente.Value;
        EdCNPJ.Text         := QrLotesItsCPF_TXT.Value;
        EdCartDep4.Text     := Geral.FF0(QrLotesItsCartDep.Value);
        CBCartDep4.KeyValue := QrLotesItsCartDep.Value;
        LocalizaSacado(True);
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          if FmPrincipal.FMyDBs.Connected[i] = '1' then
          begin
            DmLotes.QrEmLotIts.Close;
            DmLotes.QrEmLotIts.Database := Dmod.FArrayMySQLBD[i];
            DmLotes.QrEmLotIts.Params[0].AsInteger := QrLotesItsControle.Value;
            DmLotes.QrEmLotIts.Open;
            GradeDU.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
            GradeDU.Cells[01, i+1] := Geral.FFT(
              DmLotes.QrEmLotItsTaxaPer.Value, 4, siPositivo);
          end;
        end;
        EdEmiss4.SetFocus;
      end;
    end;
    5:
    begin
      PainelImporta.Visible        := True;
      PainelBandaMagnetica.Visible := False;
      PainelControle.Visible       := False;
      PainelDados.Visible          := False;
      PainelEdita.Visible          := False;
      PainelDuplicata.Visible      := False;
      PainelTaxas.Visible          := False;
      Painel6PgOcor.Visible        := False;
      //
      Invalidate;
      Refresh;
      Update;
    end;
    6:
    begin
      PainelImporta.Visible        := True;
      PainelBandaMagnetica.Visible := False;
      PainelControle.Visible       := False;
      PainelDados.Visible          := False;
      PainelEdita.Visible          := False;
      PainelDuplicata.Visible      := False;
      PainelTaxas.Visible          := False;
      Painel6PgOcor.Visible        := False;
      //
      Invalidate;
      Refresh;
      Update;
    end;
    7:
    begin
      PainelTaxas.Visible          := True;
      PainelImporta.Visible        := False;
      PainelBandaMagnetica.Visible := False;
      PainelControle.Visible       := False;
      PainelDados.Visible          := False;
      PainelEdita.Visible          := False;
      PainelDuplicata.Visible      := False;
      Painel6PgOcor.Visible        := False;
      //
      if Status = CO_INCLUSAO then
      begin
        EdTaxaCod5.Text := '';
        CBTaxaCod5.KeyValue := NULL;
        EdTaxaTxa5.Text := '0,000000';
        EdTaxaQtd5.Text := '0,00';
        RGForma.ItemIndex := 0;
      end else begin
        EdTaxaCod5.Text     := Geral.FF0(QrLotesTxsTaxaCod.Value);
        CBTaxaCod5.KeyValue := QrLotesTxsTaxaCod.Value;
        EdTaxaTxa5.Text     := Geral.FFT(QrLotesTxsTaxaTxa.Value, 6, siPositivo);
        RGForma.ItemIndex   := QrLotesTxsForma.Value;
        EdTaxaQtd5.Text     := Geral.FFT(QrLotesTxsTaxaQtd.Value, 2, siPositivo);
      end;
      EdTaxaCod5.SetFocus;
      Invalidate;
      Refresh;
      Update;
      //
    end;
    8: // Inclui lote importando
    begin
      //////////////////////////////////////////////////////////////////////////
      PainelEdita.Visible          := True;
      PainelDados.Visible          := False;
      PainelControle.Visible       := False;
      PainelBandaMagnetica.Visible := False;
      PainelImporta.Visible        := False;
      PainelDuplicata.Visible      := False;
      PainelTaxas.Visible          := False;
      Painel6PgOcor.Visible        := False;
      //
      PainelItens.Visible   := True;
      EdCliente.Text        := CO_VAZIO;
      CBCliente.KeyValue    := NULL;
      EdLote.Text           := '';
      EdAdValorem.Text      := '';
      EdNF.Text        := '';
      EdCBE.Text       := '';
      CkSCB.Checked    := False;
      EdNF.Enabled     := True;
      GradeAV.RowCount := 2;
      MyObjects.LimpaGrade(GradeAV, 1, 1, True);
      Lins := FmPrincipal.FMyDBs.MaxDBs +1;
      if Lins < 2 then Lins := 2;
      GradeAV.RowCount := Lins;
      EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
      EdPIS.Text  := Geral.FFT(Dmod.QrControlePIS.Value, 4, siPositivo);
      EdPIS_R.Text  := Geral.FFT(Dmod.QrControlePIS_R.Value, 4, siPositivo);
      EdCOFINS.Text  := Geral.FFT(Dmod.QrControleCOFINS.Value, 4, siPositivo);
      EdCOFINS_R.Text  := Geral.FFT(Dmod.QrControleCOFINS_R.Value, 4, siPositivo);
      EdISS.Text    := Geral.FFT(Dmod.QrControleISS.Value, 4, siPositivo);
      EdIRRF.Text   := '0,0000';
      for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
      begin
        if FmPrincipal.FMyDBs.Connected[i] = '1' then
        begin
          GradeAV.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
        end;
      end;
      EdCliente.SetFocus;
      if FIncluindoDU then
      begin
        EdBanco0.Text := '';
        EdAgencia0.Text := '';
        PainelBco0.Visible := True;
      end;
    end;
    9:
    begin
      Painel6PgOcor.Visible        := True;
      Panel23.Visible              := False;
      PainelControle.Enabled       := False;
      PainelTitulo.Enabled         := False;
      //
      Invalidate; Refresh; Update;
      //
      ConfiguraPgOC(QrLotesData.Value);
      CalculaJurosOcor;
      CalculaAPagarOcor;
      EdJurosBase6.SetFocus;
    end;
    10:
    begin
      Panel27.Visible              := True;
      Panel31.Visible              := False;
      PainelControle.Enabled       := False;
      PainelTitulo.Enabled         := False;
      //
      Invalidate; Refresh; Update;
      //
      ConfiguraPgCH(QrLotesData.Value);
      CalculaJurosCHDev;
      CalculaAPagarCHDev;
      EdJurosBase7.SetFocus;
    end;
    11: // Altera lote e todos itens
    begin
      //////////////////////////////////////////////////////////////////////////
      PainelEdita.Visible          := True;
      PainelDados.Visible          := False;
      PainelControle.Visible       := False;
      PainelBandaMagnetica.Visible := False;
      PainelImporta.Visible        := False;
      PainelDuplicata.Visible      := False;
      PainelTaxas.Visible          := False;
      Painel6PgOcor.Visible        := False;
      //
      LaCartDep1.Visible := False;
      EdCartDep1.Visible := False;
      CBCartDep1.Visible := False;
      //
      EdCliente.Text        := CO_VAZIO;
      CBCliente.KeyValue    := NULL;
      EdLote.Text           := '';
      EdAdValorem.Text      := '';
      GradeAV.RowCount := 2;
      MyObjects.LimpaGrade(GradeAV, 1, 1, True);
      Lins := FmPrincipal.FMyDBs.MaxDBs +1;
      if Lins < 2 then Lins := 2;
      GradeAV.RowCount := Lins;
      if Status <> CO_ALTERACAO then
        Geral.MB_Erro('Erro em A��o!')
      else begin
        EdCodigo.Text      := DBEdCodigo.Text;
        EdCliente.Text     := Geral.FF0(QrLotesCliente.Value);
        CBCliente.KeyValue := QrLotesCliente.Value;
        EdLote.Text        := Geral.FF0(QrLotesLote.Value);
        TPData.Date        := QrLotesData.Value;
        RGTipo.ItemIndex   := 0;
        //
        EdPIS.Text       := Geral.FFT(QrLotesPIS.Value, 4, siPositivo);
        EdPIS_R.Text     := Geral.FFT(QrLotesPIS_R.Value, 4, siPositivo);
        EdCOFINS.Text    := Geral.FFT(QrLotesCOFINS.Value, 4, siPositivo);
        EdCOFINS_R.Text  := Geral.FFT(QrLotesCOFINS_R.Value, 4, siPositivo);
        EdISS.Text       := Geral.FFT(QrLotesISS.Value, 4, siPositivo);
        EdIRRF.Text      := Geral.FFT(QrLotesIRRF.Value, 4, siPositivo);
        //
        EdNF.Text        := MLAGeral.FFD(QrLotesNF.Value, 6, siNegativo);
        EdCBE.Text       := MLAGeral.FFD(QrLotesCBECli.Value, 0, siNegativo);
        CkSCB.Checked    := Geral.IntToBool(QrLotesSCBCli.Value);
        //
        EdAdValorem.Text := Geral.FFT(
          QrLotesAdValorem.Value, FCasasAdValorem, siPositivo);
        //
       for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          if FmPrincipal.FMyDBs.Connected[i] = '1' then
          begin
            GradeAV.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
            GradeAV.Cells[01, i+1] := Geral.FFT(
              FArrayMySQLLO[i].FieldByName('AdValorem').AsFloat, 6, siPositivo);
          end;
        end;
      end;
      ////////////////////////////////////////////////////////////////////////
      PainelItens.Visible := True;
      PainelMassico.Visible := True;
      ////////////////////////////////////////////////////////////////////////
      EdTxaCompraX.Text  := Geral.FFT(QrLotesItsTxaCompra.Value, 4, siPositivo);
      EdDMaisX.Text      := Geral.FFT(QrLotesItsDMais.Value, 0, siNegativo);
      GradeX1.RowCount := 2;
      MyObjects.LimpaGrade(GradeX1, 1, 1, True);
      Lins := FmPrincipal.FMyDBs.MaxDBs +1;
      if Lins < 2 then Lins := 2;
      GradeX1.RowCount := Lins;
      for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
      begin
        Taxas := Dmod.ObtemTaxaDeCompraRealizada(QrLotesItsControle.Value);
        if FmPrincipal.FMyDBs.Connected[i] = '1' then
        begin
          GradeX1.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
          GradeX1.Cells[01, i+1] := Geral.FFT(Taxas[i+1], 6, siPositivo);
        end;
      end;
      ////////////////////////////////////////////////////////////////////////
      EdCliente.SetFocus;
    end;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
  FMostra := Mostra;
end;

procedure TFmLotes1.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  //Va(vpLast);
end;

procedure TFmLotes1.IncluiRegistro(Tipo, TipoLote: Integer);
var
  MyCursor: TCursor;
  Lotes, Mostra: Integer;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Lotes := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Lotes', 'Lotes', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Lotes))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro('Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := MyCursor;
      Exit;
    end;
    FTipoLote := TipoLote;
    if Tipo = 0 then Mostra := 1 else Mostra := Tipo;
    MostraEdicao(Mostra, CO_INCLUSAO, Lotes);
  finally
    Screen.Cursor := MyCursor;
  end;
end;

procedure TFmLotes1.ReopenOcorA(Cliente: Integer);
begin
  DmLotes.QrOcorA.Close;
  DmLotes.QrOcorA.Params[0].AsInteger := Cliente;
  DmLotes.QrOcorA.Params[1].AsInteger := Cliente;
  DmLotes.QrOcorA.Open;
  //
  if FOcorA > 0 then DmLotes.QrOcorA.Locate('Codigo', FOcorA, []);
end;

procedure TFmLotes1.ReopenOcorP;
begin
  QrOcorP.Close;
  QrOcorP.Params[0].AsInteger := QrLotesCodigo.Value;
  QrOcorP.Open;
  //
  if FOcorP > 0 then QrOcorP.Locate('Codigo', FOcorP, []);
end;

procedure TFmLotes1.ReopenCHDevA(Cliente: Integer);
begin
  DmLotes.QrCHDevA.Close;
  DmLotes.QrCHDevA.Params[0].AsInteger := Cliente;
  DmLotes.QrCHDevA.Params[1].AsInteger := FmPrincipal.FConnections;
  DmLotes.QrCHDevA.Open;
  //
  if FCHDevA > 0 then DmLotes.QrCHDevA.Locate('Codigo', FCHDevA, []);
  //
  DmLotes.QrCHDevT.Close;
  DmLotes.QrCHDevT.Params[0].AsInteger := Cliente;
  DmLotes.QrCHDevT.Params[1].AsInteger := FmPrincipal.FConnections;
  DmLotes.QrCHDevT.Open;
  //
end;

procedure TFmLotes1.ReopenCHDevP;
begin
  QrCHDevP.Close;
  QrCHDevP.Params[0].AsInteger := QrLotesCodigo.Value;
  QrCHDevP.Open;
  //
  if FCHDevP > 0 then QrCHDevP.Locate('Codigo', FCHDevP, []);
end;

procedure TFmLotes1.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmLotes1.SpeedButton1Click(Sender: TObject);
begin
  InfoTempo(Now, 'Primeiro registro', True);
  Va(vpFirst);
end;

procedure TFmLotes1.SpeedButton2Click(Sender: TObject);
begin
  InfoTempo(Now, 'Registro anterior', True);
  Va(vpPrior);
end;

procedure TFmLotes1.SpeedButton3Click(Sender: TObject);
begin
  InfoTempo(Now, 'Pr�ximo registro', True);
  Va(vpNext);
end;

procedure TFmLotes1.SpeedButton4Click(Sender: TObject);
begin
  InfoTempo(Now, '�ltimo registro', True);
  Va(vpLast);
end;

procedure TFmLotes1.BtIncluiClick(Sender: TObject);
begin
  FControlIts := QrLotesItsControle.Value;
  if QrLotesCodigo.Value < 1 then BtInclui.Enabled := False
  else begin
    if QrLotesTipo.Value = 0 then IncluiCheque else IncluiDuplicata;
  end;
end;

procedure TFmLotes1.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera, BtAltera);
end;

procedure TFmLotes1.AlteraRegistro;
var
  Lotes : Integer;
begin
  Lotes := QrLotesCodigo.Value;
  if QrLotesCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(Lotes, Dmod.MyDB, 'Lotes', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Lotes, Dmod.MyDB, 'Lotes', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmLotes1.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrLotesCodigo.Value;
  Close;
end;

function TFmLotes1.LoteExiste(Cliente, Codigo, Lote: Integer): Boolean;
begin
  DmLotes.QrLotExist.Close;
  DmLotes.QrLotExist.Params[00].AsInteger := Cliente;
  DmLotes.QrLotExist.Params[01].AsInteger := Codigo;
  DmLotes.QrLotExist.Params[02].AsInteger := Lote;
  DmLotes.QrLotExist.Open;
  //
  if DmLotes.QrLotExist.RecordCount > 0 then
  begin
    Result := True;
    Geral.MB_Aviso('Este lote j� existe!');
    Screen.Cursor := crDefault;
  end else Result := False;
end;

procedure TFmLotes1.BtConfirmaClick(Sender: TObject);
var
  SCB, CBE, NF, Codigo, Cliente, Lote, TipoLote, DMais, i, TipoIOF: Integer;
  AdValorem, TxaCompra, IOF: Double;
  Data, OldFile, NewFile: String;
  //RecalculaItens: Boolean;
begin
  Screen.Cursor := crHourGlass;
  try
    //RecalculaItens := False;
    Lote := Geral.IMV(EdLote.Text);
    Codigo := Geral.IMV(EdCodigo.Text);
    Cliente := Geral.IMV(EdCliente.Text);
    if Cliente = 0 then
    begin
      Geral.MB_Aviso('Defina um cliente.');
      Screen.Cursor := crDefault;
      Exit;
    end else begin
      if LaTipo.Caption = CO_ALTERACAO then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1, Cliente=:P0 WHERE Codigo=:Pa');
        Dmod.QrUpd.Params[0].AsInteger := Cliente;
        Dmod.QrUpd.Params[1].AsInteger := Codigo;
        Dmod.QrUpd.ExecSQL;
      end;
    end;
    {if DmLotes.QrClientesTipo.Value = 0 then
    begin
      if (DmLotes.QrClientesSimples.Value = 1) then
      begin
        IOF := Dmod.QrControleIOF_ME.Value;
        TipoIOF := 1;
      end else begin
        IOF := Dmod.QrControleIOF_PJ.Value;
        TipoIOF := 2;
      end;
    end else begin
      IOF := Dmod.QrControleIOF_PF.Value;
      TipoIOF := 3;
    end;}
    ObtemTipoEPercDeIOF2008(DmLotes.QrClientesTipo.Value, DmLotes.QrClientesSimples.Value,
      0, IOF, TipoIOF);
    IOF := IOF / 365; // o IOF � por dia
    AdValorem := Geral.DMV(EdAdValorem.Text);
    Data := FormatDateTime(VAR_FORMATDATE, TPData.Date);
    Dmod.QrUpdU.SQL.Clear;
    if LoteExiste(Cliente, Codigo, Lote) then Exit;
    if LaTipo.Caption = CO_INCLUSAO then
    begin
      Dmod.QrUpdU.SQL.Add('INSERT INTO lotes SET ');
      TipoLote := FTipoLote;
      if TipoLote = 1 then FConferido := 1; // Duplicatas n�o confere?
    end else
    begin
      Dmod.QrUpdU.SQL.Add('UPDATE lotes SET AlterWeb=1,   ');
      TipoLote := QrLotesTipo.Value;
      FConferido := QrLotesConferido.Value;
    end;
    Dmod.QrUpdU.SQL.Add('Cliente=:P0, Tipo=:P1, Lote=:P2, AdValorem=:P3,');
    Dmod.QrUpdU.SQL.Add('Data=:P4, TipoAdV=:P5, Spread=:P6, IRRF=:P7, ');
    Dmod.QrUpdU.SQL.Add('PIS_R=:P8, ISS=:P9, Cofins_R=:P10, PIS=:P11, ');
    Dmod.QrUpdU.SQL.Add('COFINS=:P12, NF=:P13, Conferido=:P14, CBE=:P15, ');
    Dmod.QrUpdU.SQL.Add('SCB=:P16, CPMF=:P17, IOFd=:P18, IOFv=:P19, ');
    Dmod.QrUpdU.SQL.Add('TipoIOF=:P20, ');
    //
    NF := Geral.IMV(EdNF.Text);
    CBE := Geral.IMV(EdCBE.Text);
    SCB := Geral.BoolToInt(CkSCB.Checked);
    if (NF=0) and (FmPrincipal.FConnections=0) and
    (FmPrincipal.FMyDBs.MaxDBs > 0) then
    begin
      Geral.MB_Erro('Erro em A��o!');
      Exit;
    end;
    if LaTipo.Caption = CO_INCLUSAO then
    begin
      Dmod.QrUpdU.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc');
      Dmod.QrUpdU.SQL.Add(', WebCod=:Pd');
    end else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
    Dmod.QrUpdU.Params[00].AsInteger := Cliente;
    Dmod.QrUpdU.Params[01].AsInteger := TipoLote;
    Dmod.QrUpdU.Params[02].AsInteger := Lote;
    Dmod.QrUpdU.Params[03].AsFloat   := AdValorem;
    Dmod.QrUpdU.Params[04].AsString  := Data;
    Dmod.QrUpdU.Params[05].AsInteger := Dmod.QrControleTipoAdValorem.Value;
    Dmod.QrUpdU.Params[06].AsInteger := RGTipo.ItemIndex;
    Dmod.QrUpdU.Params[07].AsFloat   := Geral.DMV(EdIRRF.Text);
    Dmod.QrUpdU.Params[08].AsFloat   := Geral.DMV(EdPIS_R.Text);
    Dmod.QrUpdU.Params[09].AsFloat   := Geral.DMV(EdISS.Text);
    Dmod.QrUpdU.Params[10].AsFloat   := Geral.DMV(EdCOFINS_R.Text);
    Dmod.QrUpdU.Params[11].AsFloat   := Geral.DMV(EdPIS.Text);
    Dmod.QrUpdU.Params[12].AsFloat   := Geral.DMV(EdCOFINS.Text);
    Dmod.QrUpdU.Params[13].AsInteger := NF;
    Dmod.QrUpdU.Params[14].AsInteger := FConferido;
    Dmod.QrUpdU.Params[15].AsInteger := CBE;
    Dmod.QrUpdU.Params[16].AsInteger := SCB;
    Dmod.QrUpdU.Params[17].AsFloat   := Geral.DMV(EdCPMF.Text);
    Dmod.QrUpdU.Params[18].AsFloat   := IOF;
    Dmod.QrUpdU.Params[19].AsFloat   := Dmod.QrControleIOF_Ex.Value;
    Dmod.QrUpdU.Params[20].AsInteger := TipoIOF;
    //
    Dmod.QrUpdU.Params[21].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdU.Params[22].AsInteger := VAR_USUARIO;
    Dmod.QrUpdU.Params[23].AsInteger := Codigo;
    if LaTipo.Caption = CO_INCLUSAO then
    begin
      Dmod.QrUpdU.Params[24].AsInteger := FloteWebDone;
      if FLoteWebDone > 0 then
      begin
        Dmod.QrUpdM.SQL.Clear;
        Dmod.QrUpdM.SQL.Add('UPDATE llotecab SET Baixado=3 WHERE Codigo=:P0');
        Dmod.QrUpdM.Params[0].AsInteger := FLoteWebDone;
        Dmod.QrUpdM.ExecSQL;
      end;
    end;
    Dmod.QrUpdU.ExecSQL;
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Lotes', 'Codigo');
    DefineAdValoremEmpresas;
    if LaTipo.Caption = CO_INCLUSAO then
    begin
      LancaTaxasNovoLote(Cliente, TipoLote, Codigo);
      FmPrincipal.AtualizaSB2(1);
      FmPrincipal.AtualizaSB2(2);
    end;
    ////////////////////////////////////////////////////////////////////////////
    if FIncluindoCh then
    begin
      Dmoc.QrLCHs.First;
      ProgressBar4.Position := 0;
      ProgressBar4.Max := Dmoc.QrLCHs.RecordCount;
      ProgressBar4.Visible := True;
      ProgressBar4.Refresh;
      Application.ProcessMessages;
      while not Dmoc.QrLCHs.Eof do
      begin
        ProgressBar4.Position := ProgressBar4.Position + 1;
        ProgressBar4.Refresh;
        Application.ProcessMessages;
        ImportaCheque(Codigo, Cliente, CBE);
        Dmoc.QrLCHs.Next;
      end;
      FIncluindoCH := False;
      FGradeFocused := FGradeFocused -1;
      if FGradeFocused = 2 then
      begin
        Dmod.QrUpdL.SQL.Clear;
        Dmod.QrUpdL.SQL.Add('DELETE FROM importlote WHERE Tipo=0');
        Dmod.QrUpdL.ExecSQL;
        Dmoc.ReopenImportLote(0,0);
        MostraEdicao(6, CO_INCLUSAO, 0)
      end else
        MostraEdicao(0, CO_TRAVADO, 0);
    end else if FIncluindoDU then
    begin
      ProgressBar4.Position := 0;
      ProgressBar4.Max := Dmoc.QrLDUs.RecordCount;
      ProgressBar4.Visible := True;
      ProgressBar4.Refresh;
      Application.ProcessMessages;
      Dmoc.QrLDUs.First;
      while not Dmoc.QrLDUs.Eof do
      begin
        ProgressBar4.Position := ProgressBar4.Position +1;
        ProgressBar4.Refresh;
        Application.ProcessMessages;
        ImportaDuplicata(Codigo, Cliente);
        Dmoc.QrLDUs.Next;
      end;
      FIncluindoDU := False;
      FGradeFocused := FGradeFocused -2;
      if FGradeFocused = 1 then
      begin
        Dmod.QrUpdL.SQL.Clear;
        Dmod.QrUpdL.SQL.Add('DELETE FROM importlote WHERE Tipo=1');
        Dmod.QrUpdL.ExecSQL;
        Dmoc.ReopenImportLote(0,0);
        MostraEdicao(5, CO_INCLUSAO, 0)
      end else
        MostraEdicao(0, CO_TRAVADO, 0);
    end else begin
      if PainelMassico.Visible then
      begin
        TxaCompra := Geral.DMV(EdTxaCompraX.Text);
        DMais     := Geral.IMV(EdDMaisX.Text);
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1, TxaCompra=:P0, ');
        Dmod.QrUpd.SQL.Add('DMais=:P1, DCompra=:P2 WHERE Codigo=:Pa');
        //Dmod.QrUpd.SQL.Add('DMais=:P1, DCompra=:P2, CartDep=:P3 WHERE Codigo=:Pa');
        Dmod.QrUpd.Params[0].AsFloat   := TxaCompra;
        Dmod.QrUpd.Params[1].AsInteger := DMais;
        Dmod.QrUpd.Params[2].AsString  := Data;
        //Dmod.QrUpd.Params[3].AsInteger := Geral.IMV(EdCartDep1.Text);
        //
        Dmod.QrUpd.Params[3].AsInteger := Codigo;
        Dmod.QrUpd.ExecSQL;
        //
        for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
        begin
          if FmPrincipal.FMyDBs.Connected[i] = '1' then
          begin
           Dmod.QrUpd_.Database := Dmod.FArrayMySQLBD[i];
           //
           Dmod.QrUpd_.SQL.Clear;
           Dmod.QrUpd_.SQL.Add('UPDATE emlotits SET TaxaPer=:P0, TaxaVal=:P1, ');
           Dmod.QrUpd_.SQL.Add('JuroPer=:P2 WHERE Codigo=:Pa');
           //
           Dmod.QrUpd_.Params[0].AsFloat   := Geral.DMV(GradeX1.Cells[01, i+1]);
           Dmod.QrUpd_.Params[1].AsFloat   := 0;
           Dmod.QrUpd_.Params[2].AsFloat   := 0;
           //
           Dmod.QrUpd_.Params[3].AsInteger := Codigo;
           Dmod.QrUpd_.ExecSQL;
           //
          end;
        end;
        //RecalculaItens := true;
      end;
      MostraEdicao(0, CO_TRAVADO, 0);
      //CalculaLote(Codigo);
      LocCod(Codigo,Codigo);
      Update;
      Refresh;
      Application.ProcessMessages;
      //if RecalculaItens then EsteBorder1Click(Self);
    end;
    RecalculaBordero(Codigo);
    LocCod(Codigo, Codigo);
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
  VerificaNF(NF);
  FmPrincipal.AtualizaLastEditLote(Data);
  ProgressBar4.Visible := False;
  if FmPrincipal.FTipoImport = 3 then
  begin
    OldFile := FmPrincipal.FDirR;
    if OldFile[Length(OldFile)] <> '\' then OldFile := OldFile + '\';
    OldFile := OldFile + ExtractFileName(FmPrincipal.OpenDialog1.FileName);
    NewFile := Dmod.QrControleFTP_Lix.Value;
    If Trim(NewFile) = '' then
    begin
      Geral.MB_Aviso('Defina o diret�rio de arquivos scx baixados e processados em ' +
        'Op��es -> Espec�ficos, na orelha Exporta\Importa');
      Exit;
    end;
    if NewFile[Length(NewFile)] <> '\' then NewFile := NewFile + '\';
    NewFile := NewFile + ExtractFileName(FmPrincipal.OpenDialog1.FileName);
    if FileExists(OldFile) and (DirectoryExists(Dmod.QrControleFTP_Lix.Value)) then
    begin
      if Geral.MB_Pergunta('Deseja mover o arquivo "' + OldFile +
        '" para o diret�rio "' + Dmod.QrControleFTP_Lix.Value+'"?') = ID_YES then
      begin
        MoveFile(PChar(OldFile), PChar(NewFile));
        FmPrincipal.SB2.Panels[7].Text :=
          Geral.FF0(dmkPF.GetAllFiles(False, FmPrincipal.FDirR+'*.scx',
          FmPrincipal.ListBox1, True));
      end;
    end;
  end;
  FmPrincipal.FTipoImport := 0;
  FLoteWebDone := 0;
end;

procedure TFmLotes1.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    FLoteWebDone := 0;
    if LaTipo.Caption = CO_INCLUSAO then
    begin
      Codigo := Geral.IMV(EdCodigo.Text);
      UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Lotes', Codigo);
    end else begin
      Codigo := QrLotesCodigo.Value;
      UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Lotes', 'Codigo');
      CalculaLote(Codigo, False);
      LocCod(Codigo, Codigo);
    end;
    MostraEdicao(0, CO_TRAVADO, 0);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLotes1.FormCreate(Sender: TObject);
var
  Cam: String;
begin
  DmLotes.QrExEnti1.Database := Dmod.MyDB1;
  DmLotes.QrEmLot1.Database := Dmod.MyDB1;
  DmLotes.QrSum1.Database := Dmod.MyDB1;
  //
  DmLotes.QrExEnti2.Database := Dmod.MyDB2;
  DmLotes.QrEmLot2.Database := Dmod.MyDB2;
  DmLotes.QrSum2.Database := Dmod.MyDB2;
  //
  DmLotes.QrExEnti3.Database := Dmod.MyDB3;
  DmLotes.QrEmLot3.Database := Dmod.MyDB3;
  DmLotes.QrSum3.Database := Dmod.MyDB3;
  //
  DmLotes.QrExEnti4.Database := Dmod.MyDB4;
  DmLotes.QrEmLot4.Database := Dmod.MyDB4;
  DmLotes.QrSum4.Database := Dmod.MyDB4;
  //
  DmLotes.QrExEnti5.Database := Dmod.MyDB5;
  DmLotes.QrEmLot5.Database := Dmod.MyDB5;
  DmLotes.QrSum5.Database := Dmod.MyDB5;
  //
  DmLotes.QrExEnti6.Database := Dmod.MyDB6;
  DmLotes.QrEmLot6.Database := Dmod.MyDB6;
  DmLotes.QrSum6.Database := Dmod.MyDB6;
  //
  if FmPrincipal.FConnections < FmPrincipal.FMyDBs.MaxDBs then
  begin
    Total1.Visible := False;
    Contabilidade1.Caption := '&Border�';
    BtCheckImp.Visible := False;
    //
    DBLaCPMF.Visible := False;
    DBEdCPMF.Visible := True;
    DBEdNF.Width := DBEdNF.Width + DBEdCPMF.Width + 4;
    //
  end else begin
    Total1.Visible := True;
    Contabilidade1.Caption := 'Border� &Cont�bil';
    BtCheckImp.Visible := True;
  end;
  Cam                 := Application.Title + '\XYConfig\PGAbertos';
  PGAbertos_CH.Height := Geral.ReadAppKeyLM('Altura', Cam, ktInteger, PGAbertos_CH.Height);
  //
  FIncluindoCh := False;
  FIncluindoDU := False;
  //if Screen.Width >= 1024 then FmLotes1.Width := 1024;
  //FmLotes1.Height := 1024;
  //
  FArrayMySQLEE[00] := DmLotes.QrExEnti1;
  FArrayMySQLEE[01] := DmLotes.QrExEnti2;
  FArrayMySQLEE[02] := DmLotes.QrExEnti3;
  FArrayMySQLEE[03] := DmLotes.QrExEnti4;
  FArrayMySQLEE[04] := DmLotes.QrExEnti5;
  FArrayMySQLEE[05] := DmLotes.QrExEnti6;
  //
  FArrayMySQLLO[00] := DmLotes.QrEmLot1;
  FArrayMySQLLO[01] := DmLotes.QrEmLot2;
  FArrayMySQLLO[02] := DmLotes.QrEmLot3;
  FArrayMySQLLO[03] := DmLotes.QrEmLot4;
  FArrayMySQLLO[04] := DmLotes.QrEmLot5;
  FArrayMySQLLO[05] := DmLotes.QrEmLot6;
  //
  FArrayMySQLSI[00] := DmLotes.QrSum1;
  FArrayMySQLSI[01] := DmLotes.QrSum2;
  FArrayMySQLSI[02] := DmLotes.QrSum3;
  FArrayMySQLSI[03] := DmLotes.QrSum4;
  FArrayMySQLSI[04] := DmLotes.QrSum5;
  FArrayMySQLSI[05] := DmLotes.QrSum6;
  //
  //for i := 0 to FmPrincipal.FMyDBs.MaxDBs-1 do
    //if FmPrincipal.FMyDBs.Connected[i] = '1' then FArrayMySQLEE[i].Open;
  //
  PainelEdita.Align     := alClient;
  PainelDados.Align     := alClient;
  PainelBanda.Align     := alClient;
  PainelPdxAdVal.Align  := alClient;
  GradeCHE.Align        := alClient;
  GradeDUP.Align        := alClient;
  PainelDigitaDup.Align := alClient;
  Pagina.Align          := alClient;
  Panel15.Align         := alClient;
  Panel20.Align         := alClient;
  Panel28.Align         := alClient;
  DBGrid5.Align         := alClient;
  DBGrid10.Align        := alClient;
  Painel6PgOcor.Align   := alClient;
  Panel23.Align         := alClient;
  //
  CriaOForm;
  DmLotes.QrClientes.Close;
  DmLotes.QrClientes.Open;
  DmLotes.QrCarteiras4.Open;
  DmLotes.QrCarteiras4.Open;
  TPData.Date := Date;
  //
  GradeAV.ColCount := 2;
  GradeAV.RowCount := 2;
  //
  GradeAV.ColWidths[00] := 240;
  GradeAV.ColWidths[01] := 80;
  //
  GradeAV.Cells[00,00] := 'Coligada';
  if Dmod.QrControleTipoAdValorem.Value = 0 then
  begin
    LaAdValorem.Caption := '% Ad Valorem';
    GradeAV.Cells[01,00] := '% Ad Valorem';
    FCasasAdValorem := 4;
  end else begin
    LaAdValorem.Caption := '$ Ad Valorem';
    GradeAV.Cells[01,00] := '$ Ad Valorem';
    FCasasAdValorem := 2;
  end;
  //
  GradeCH.ColCount := 2;
  GradeCH.RowCount := 2;
  GradeCH.ColWidths[00] := 240;
  GradeCH.ColWidths[01] := 80;
  GradeCH.Cells[00,00] := 'Coligada';
  GradeCH.Cells[01,00] := '% Compra';
  //
  GradeDU.ColCount := 2;
  GradeDU.RowCount := 2;
  GradeDU.ColWidths[00] := 240;
  GradeDU.ColWidths[01] := 80;
  GradeDU.Cells[00,00] := 'Coligada';
  GradeDU.Cells[01,00] := '% Compra';
  //
  GradeX1.ColCount := 2;
  GradeX1.RowCount := 2;
  GradeX1.ColWidths[00] := 240;
  GradeX1.ColWidths[01] := 80;
  GradeX1.Cells[00,00] := 'Coligada';
  GradeX1.Cells[01,00] := '% Compra';
  //
  ArqSCX.ConfiguraGrades(GradeC, GradeD, Pagina);
  //
  PgAbertos_DU.ActivePageIndex := 0;
  PgPrincipal.ActivePageIndex  := 0;
  PageControl3.ActivePageIndex := 0;
  PageControl4.ActivePageIndex := 0;
  PageControl5.ActivePageIndex := 0;
  PageControl6.ActivePageIndex := 0;
  PgPrincipal.Align            := alClient;
  //
  DmLotes.QrTaxas.Close;
  DmLotes.QrTaxas.Open;
  //
  Dmoc.FORDA_LCHs := 'il.Ordem';
  Dmoc.FORDB_LCHs := '';
  Dmoc.FORDA_LDUs := 'il.Ordem';
  Dmoc.FORDB_LDUs := '';
  //
  FLogo1Path   := Geral.ReadAppKeyLM('Logo1', Application.Title, ktString, '');
  FLogo1Exists := FileExists(FLogo1Path);
  FLoteWeb     := 0;
  FLoteWebDone := 0;
end;

procedure TFmLotes1.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrLotesCodigo.Value,LaRegistro.Caption);
end;

procedure TFmLotes1.SbNomeClick(Sender: TObject);
begin
  FmPrincipal.FLoteLoc := 0;
  Application.CreateForm(TFmLotesLoc, FmLotesLoc);
  FmLotesLoc.FFormCall := 1;
  FmLotesLoc.ShowModal;
  FmLotesLoc.Destroy;
  if FmPrincipal.FLoteLoc <> 0 then
  LocCod(FmPrincipal.FLoteLoc, FmPrincipal.FLoteLoc);
end;

procedure TFmLotes1.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  Cam: String;
begin
  Cam := Application.Title + '\XYConfig\PGAbertos';
  //
  Geral.WriteAppKeyLM2('Altura', Cam, PGAbertos_CH.Height, ktInteger);
  //
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmLotes1.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Lotes', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmLotes1.FormatoBP1Click(Sender: TObject);
begin
  ImprimeBorderoClienteBP;
end;

procedure TFmLotes1.QrLotesAfterScroll(DataSet: TDataSet);
begin
  if QrLotesTipo.Value = 0 then
  begin
    GradeCHE.Visible := True;
    GradeDUP.Visible := False;
  end else begin
    GradeDUP.Visible := True;
    GradeCHE.Visible := False;
  end;
  (*
  BtAltera.Enabled := GOTOy.BtEnabled(QrLotesCodigo.Value, False);
  SbImprime.Enabled := GOTOy.BtEnabled(QrLotesCodigo.Value, False);
  BtCheckImp.Enabled := GOTOy.BtEnabled(QrLotesCodigo.Value, False);
  BtInclui.Enabled := Geral.IntToBool_0(QrLotesLote.Value);
  *)
  //
  //InfoTempo(Now, 'Rolagem', False);  // n�o precisa. � zero.
  DefineRiscos(QrLotesCliente.Value);
  Memo1.Lines.Add('========================================== Fim reaberturas');
  PgPrincipal.ActivePageIndex := FOrelha;
end;

procedure TFmLotes1.DefineRiscos(Cliente: Integer);
begin
  InfoTempo(Now, 'IN�CIO DEFINI��O DE RISOS', True);
  //
  // soma antes do �ltimo !!!!
  ReopenCHDevA(Cliente);
  InfoTempo(Now, 'Cheques devolvidos abertos', False);
  ReopenOcorA(Cliente);
  InfoTempo(Now, 'Ocorrr�ncias abertas', False);
  // fim somas.
  //
  // Normais
  ReopenIts;
  InfoTempo(Now, 'Itens do border�', False);
  ReopenTxs;
  InfoTempo(Now, 'Taxas do border�', False);
  ReopenOcorP;
  InfoTempo(Now, 'Ocorr�ncias pagas', False);
  ReopenCHDevP;
  InfoTempo(Now, 'Cheques devolvidos pagos', False);
  ReopenPagtos;
  InfoTempo(Now, 'Pagamentos ao cliente', False);
  ReopenDPago;
  InfoTempo(Now, 'Duplicatas vencidas pagas', False);
  ReopenRiscoC(Cliente);
  InfoTempo(Now, 'Risco cliente', False);
  ReopenRepCli;
  InfoTempo(Now, 'Pagamentos com cheques de terceiros', False);
  DmLotes.ReopenSPC_Cli(QrLotesCliente.Value);
  InfoTempo(Now, 'Configura��es de consulta ao SPC', False);
  //
  // precisa ser o �ltimo !!!! para Ed?.Text
  ReopenDOpen(Cliente);
  InfoTempo(Now, 'Duplicatas vencidas abertas', False);
  //
  Memo1.Lines.Add('');
  Memo1.Lines.Add('==============================================================================');
  Memo1.Lines.Add('');
end;

procedure TFmLotes1.SbQueryClick(Sender: TObject);
begin
  SbNomeClick(Self);
end;

procedure TFmLotes1.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption,
    Image1, PainelTitulo, True, 30);
end;

procedure TFmLotes1.QrLotesBeforeOpen(DataSet: TDataSet);
var
  i: Integer;
begin
  QrLotesCodigo.DisplayFormat := FFormatFloat;
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    FArrayMySQLLO[i].Close;
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      FArrayMySQLLO[i].Params[0].AsInteger := QrLotes.Params[0].AsInteger;
      FArrayMySQLLO[i].Open;
    end;
  end;
end;

function TFmLotes1.GetTitle(TipoLote: Integer): String;
var
  Titulo: String;
  //Lote: String;
begin
  (*if PainelEdita.Visible and (LaTipo.Caption = CO_INCLUSAO)
  then Lote := 'Novo lote de '
  else Lote := 'Lote '+FormatFloat('000', QrLotesLote.Value)+': ';*)
  case TipoLote of
    0: Titulo := 'Cheques';
    1: Titulo := 'Duplicatas';
    else Titulo := '???'
  end;
  Result := (*Lote + *)Titulo;
end;

procedure TFmLotes1.EdLoteKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_F4 then DefineNovoLote;
end;

procedure TFmLotes1.DefineNovoLote;
begin
  DmLotes.QrLocLote.Close;
  DmLotes.QrLocLote.Params[0].AsInteger := Geral.IMV(EdCliente.Text);
  DmLotes.QrLocLote.Params[1].AsInteger := Geral.IMV(EdCodigo.Text);
  DmLotes.QrLocLote.Open;
  EdLote.Text := MLAGeral.FFD(DmLotes.QrLocLoteLote.Value + 1, 3, siNegativo);
end;

procedure TFmLotes1.EdBandaChange(Sender: TObject);
begin
  BtConfirma2.Enabled := False;
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
    EdCMC_7.Text := Geral.SoNumero_TT(EdBanda.Text);
end;

procedure TFmLotes1.EdValorExit(Sender: TObject);
begin
  CalculaDiasCH;
end;

procedure TFmLotes1.EdVenctoExit(Sender: TObject);
var
  Data, Emis: TDateTime;
  Casas: Integer;
begin
  if Geral.DMV(EdValor.Text) = 0 then
  begin
    EdValor.SetFocus;
    Exit;
  end;  
  Casas := Length(EdVencto.Text);
  Data := Geral.ValidaDataSimples(EdVencto.Text, False);
  Emis := Geral.ValidaDataSimples(EdData.Text, False);
  if Data > 2 then
  begin
    if Data <= Emis then
      if Casas < 6 then Data := IncMonth(Data, 12);
    if Data < Emis then
    begin
      if Geral.MB_Pergunta('Data j� passada! Deseja incrementar um ano?') = ID_YES then
      begin
        Data := IncMonth(Data, 12);
        EdVencto.SetFocus;
      end else begin
        EdVencto.SetFocus;
        Exit;
      end;
    end;
    EdVencto.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdVencto.SetFocus;
  CalculaDiasCH;
end;

procedure TFmLotes1.EdCPFExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  Screen.Cursor := crHourGlass;
  CPF := Geral.SoNumero_TT(EdCPF.Text);
  if CPF <> CO_VAZIO then
  begin
    Num := Geral.CalculaCNPJCPF(CPF);
    if Geral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Geral.MB_Aviso('N�mero inv�lido para CNPJ/CPF!');
      EdCPF.SetFocus;
    end else begin
      EdCPF.Text := Geral.FormataCNPJ_TT(CPF);
      if EdEmitente.Text = '' then
      begin
        Dmoc.QrLocEmiCPF.Close;
        Dmoc.QrLocEmiCPF.Params[0].AsString := CPF;
        Dmoc.QrLocEmiCPF.Open;
        //
        if Dmoc.QrLocEmiCPF.RecordCount > 0 then
          EdEmitente.Text := Dmoc.QrLocEmiCPFNome.Value;
      end;
      PesquisaRiscoSacado(EdCPF.Text);
    end;
  end else EdCPF.Text := CO_VAZIO;
  Screen.Cursor := crDefault;
end;

function TFmLotes1.LocalizaEmitente(MudaNome: Boolean): Boolean;
var
  B,A,C: String;
begin
  Result := False;
  if  (Geral.IMV(EdBanco.Text) > 0)
  and (Geral.IMV(EdAgencia.Text) > 0)
  and (Geral.DMV(EdConta.Text) > 0.1) then
  begin
    Screen.Cursor := crHourGlass;
    try
      (*B := EdBanco.Text;
      A := EdAgencia.Text;
      C := EdConta.Text;
      //
      QrLocEmit.Close;
      QrLocEmit.Params[0].AsInteger := QrBancoDVCC.Value;
      QrLocEmit.Params[1].AsString  := CriaBAC_Pesq(B,A,C,QrBancoDVCC.Value);
      QrLocEmit.Open;*)
      //
      if MudaNome then
      begin
        B := EdBanco.Text;
        A := EdAgencia.Text;
        C := EdConta.Text;
        //
        (*QrLocEmit.Close;
        QrLocEmit.Params[0].AsInteger := QrBancoDVCC.Value;
        QrLocEmit.Params[1].AsString  := CriaBAC_Pesq(B,A,C,QrBancoDVCC.Value);
        QrLocEmit.Open;*)
        Dmoc.QrLocEmiBAC.Close;
        Dmoc.QrLocEmiBAC.Params[0].AsInteger := Dmoc.QrBancoDVCC.Value;
        Dmoc.QrLocEmiBAC.Params[1].AsString  := CriaBAC_Pesq(B,A,C,Dmoc.QrBancoDVCC.Value);
        Dmoc.QrLocEmiBAC.Open;
        //
        if Dmoc.QrLocEmiBAC.RecordCount = 0 then
        begin
          EdEmitente.Text := '';
          EdCPF.Text := '';
        end else begin
          Dmoc.QrLocEmiCPF.Close;
          Dmoc.QrLocEmiCPF.Params[0].AsString := Dmoc.QrLocEmiBACCPF.Value;
          Dmoc.QrLocEmiCPF.Open;
          //
          if Dmoc.QrLocEmiCPF.RecordCount = 0 then
          begin
            EdEmitente.Text := '';
            EdCPF.Text := '';
          end else begin
            EdEmitente.Text := Dmoc.QrLocEmiCPFNome.Value;
            EdCPF.Text      := Geral.FormataCNPJ_TT(Dmoc.QrLocEmiCPFCPF.Value);
            PesquisaRiscoSacado(EdCPF.Text);
          end;
        end;
      end;
      Screen.Cursor := crDefault;
      Result := True;
    except
      Screen.Cursor := crDefault;
      raise;
    end;
  end;
end;

function TFmLotes1.LocalizaSacado(MudaNome: Boolean): Boolean;
var
  i: Integer;
begin
  Result        := False;
  Screen.Cursor := crHourGlass;
  try
    DmLotes.QrLocSaca.Close;
    DmLotes.QrLocSaca.Params[0].AsString := Geral.SoNumero_TT(EdCNPJ.Text);
    DmLotes.QrLocSaca.Open;
    //
    if MudaNome then
    begin
      if DmLotes.QrLocSaca.RecordCount = 0 then
      begin
        EdSacado.Text := '';
        //
        Dmoc.QrLocEmiCPF.Close;
        Dmoc.QrLocEmiCPF.Params[0].AsString := Geral.SoNumero_TT(EdCNPJ.Text);
        Dmoc.QrLocEmiCPF.Open;
        //
        if Dmoc.QrLocEmiCPF.RecordCount > 0 then
          EdSacado.Text := Dmoc.QrLocEmiCPFNome.Value;
      end else
      begin
        EdSacado.Text := DmLotes.QrLocSacaNome.Value;
        EdIE.Text     := DmLotes.QrLocSacaIE.Value;
        EdRua.Text    := DmLotes.QrLocSacaRua.Value;
        EdNumero.Text := Geral.FF0(Trunc(DmLotes.QrLocSacaNumero.Value));
        EdCompl.Text  := DmLotes.QrLocSacaCompl.Value;
        EdBairro.Text := DmLotes.QrLocSacaBairro.Value;
        EdCidade.Text := DmLotes.QrLocSacaCidade.Value;
        EdCEP.Text    := Geral.FormataCEP_NT(DmLotes.QrLocSacaCEP.Value);
        //
        for i := 0 to CBUF.Items.Count do
        begin
          if CBUF.Items[i] = DmLotes.QrLocSacaUF.Value then
          begin
            CBUF.ItemIndex := i;
            Break;
          end;
        end;
        EdTel1.Text  := Geral.FormataTelefone_TT(DmLotes.QrLocSacaTel1.Value);
        EdEmail.Text := DmLotes.QrLocSacaEmail.Value;
      end;
    end;
    Screen.Cursor := crDefault;
    Result := True;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmLotes1.EdAdValoremExit(Sender: TObject);
begin
  EdAdValorem.LeftZeros := FCasasAdValorem;
end;

procedure TFmLotes1.EdClienteChange(Sender: TObject);
begin
  ConfiguraTaxas;
  if LaTipo.Caption = CO_INCLUSAO then DefineNovoLote;
end;

procedure TFmLotes1.ConfiguraTaxas;
var
  i: Integer;
begin
  if LaTipo.Caption <> CO_INCLUSAO then Exit;
  EdAdValorem.Text := Geral.FFT(
    DmLotes.QrClientesAdValorem.Value, FCasasAdValorem, siPositivo);
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      GradeAV.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
      FArrayMySQLEE[i].Close;
      FArrayMySQLEE[i].Params[0].AsInteger := Geral.IMV(EdCliente.Text);
      FArrayMySQLEE[i].Open;
      GradeAV.Cells[01, i+1] := Geral.FFT(
        FArrayMySQLEE[i].FieldByName('AdVal').AsFloat, 6, siPositivo);
    end;
  end;
  EdDMaisC.Text := Geral.FF0(DmLotes.QrClientesDMaisC.Value);
  EdDMaisD.Text := Geral.FF0(DmLotes.QrClientesDMaisD.Value);
  if Dmod.QrControleCBEMinimo.Value > 0 then
    EdCBE.Text := Geral.FF0(Dmod.QrControleCBEMinimo.Value)
  else
    EdCBE.Text := Geral.FF0(DmLotes.QrClientesCBE.Value);
  if Dmod.QrControleSBCPadrao.Value = 2 then
    CkSCB.Checked := Geral.IntToBool_0(DmLotes.QrClientesSCB.Value)
  else
    CkSCB.Checked := Geral.IntToBool_0(Dmod.QrControleSBCPadrao.Value);
  if Dmod.QrControleCPMF_Padrao.Value = 0 then
    EdCPMF.Text := Geral.FFT(DmLotes.QrClientesCPMF.Value, 4, siPositivo);
end;

procedure TFmLotes1.EdTxaCompraExit(Sender: TObject);
begin
  CalculaValoresChequeAlterado;
end;

procedure TFmLotes1.TPDataChange(Sender: TObject);
begin
  CalculaDiasCH;
end;

procedure TFmLotes1.CalculaDiasCH;
var
  Comp, DMai: Integer;
  Vcto, Data: TDateTime;
begin
  Vcto := Geral.ValidaDataSimples(EdVencto.Text, True);
  Data := Geral.ValidaDataSimples(EdData.Text, True);
  DMai := Geral.IMV(EdDMaisC.Text);
  Comp := FmPrincipal.VerificaDiasCompensacao(Geral.IMV(EdregiaoCompe.Text),
          QrLotesCBE.Value, Geral.DMV(EdValor.Text), Data, Vcto,
          QrLotesSCB.Value); // busca CBE e SBC do Lote pois j� est� definido
  EdDias.Text := Geral.FF0(UMyMod.CalculaDias(Int(Data), Int(Vcto), DMai, Comp,
          Dmod.QrControleTipoPrazoDesc.Value, QrLotesCBE.Value));
  EdCompensacao.Text := Geral.FF0(Comp);
  EdPrazo.Text := Geral.FF0(Trunc(Int(Vcto)-Int(Data)));
  //
  CalculaValoresChequeAlterado;
end;

procedure TFmLotes1.CalculaDiasDU;
var
  Comp, DMai: Integer;
  Vcto, Data: TDateTime;
begin
  Vcto := Geral.ValidaDataSimples(EdVence4.Text, True);
  Data := Geral.ValidaDataSimples(EdData4.Text, True);
  DMai := Geral.IMV(EdDMaisD.Text);
  Comp := 0;  // S� para cheques
  EdDias4.Text := Geral.FF0(UMyMod.CalculaDias(Int(Data), Int(Vcto), DMai, Comp,
    Dmod.QrControleTipoPrazoDesc.Value, 0));
  EdPrazo4.Text := Geral.FF0(Trunc(Int(Vcto)-Int(Data)));
  //
  CalculaValoresDuplicataAlterado;
end;

procedure TFmLotes1.BtCalendarioClick(Sender: TObject);
begin
  Application.CreateForm(TFmSomaDiasCred, FmSomaDiasCred);
  FmSomaDiasCred.TPDataI.Date := Geral.ValidaDataSimples(EdData.Text, True);
  FmSomaDiasCred.TPDataF.Date := Geral.ValidaDataSimples(EdVencto.Text, True);
  FmSomaDiasCred.EdComp.Text  := EdCompensacao.Text;
  FmSomaDiasCred.EdDMais.Text := EdDMaisC.Text;
  FmSomaDiasCred.ShowModal;
  FmSomaDiasCred.Destroy;
end;

procedure TFmLotes1.ReopenIts;
begin
  QrLotesIts.Close;
  QrLotesIts.Params[0].AsInteger := QrLotesCodigo.Value;
  QrLotesIts.Open;
  //
  if FControlIts <> 0 then QrLotesIts.Locate('Controle', FControlIts, []);
end;

procedure TFmLotes1.ReopenTxs;
begin
  QrLotesTxs.Close;
  QrLotesTxs.Params[0].AsInteger := QrLotesCodigo.Value;
  QrLotesTxs.Open;
  //
  if FControlTxs <> 0 then QrLotesTxs.Locate('Controle', FControlTxs, []);
end;

procedure TFmLotes1.ResultadodaconsultaaoSPC1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxSPC_Result, 'Resultado de consulta ao SPC / SERASA');
end;

procedure TFmLotes1.EdDMaisCExit(Sender: TObject);
begin
  CalculaDiasCH;
end;

procedure TFmLotes1.BtConfirma2Click(Sender: TObject);
var
  Vence, Data_: TDateTime;
  i, z, Erros, DMais, Dias, Codigo, Cheque, Comp, Banco, Lote,
  Agencia, Praca, Controle, Tipific, StatusSPC: Integer;
  Valor: Double;
  DDeposito, DCompra, Vencto, Conta, CPF, Emitente: String;
begin
  DmLotes.QrCHsDev.Close;
  DmLotes.QrCHsDev.SQL.Clear;
  DmLotes.QrCHsDev.SQL.Add('SELECT ai.*');
  DmLotes.QrCHsDev.SQL.Add('FROM alinits ai');
  (*DmLotes.QrCHsDev.SQL.Add('LEFT JOIN lotesits li ON li.Controle=ai.ChequeOrigem');
  DmLotes.QrCHsDev.SQL.Add('LEFT JOIN lotes lo ON li.Codigo=lo.Codigo');*)
  DmLotes.QrCHsDev.SQL.Add('LEFT JOIN lotes lo ON ai.LoteOrigem=lo.Codigo');
  DmLotes.QrCHsDev.SQL.Add('WHERE ai.CPF="'+Geral.SoNumero_TT(EdCPF.Text) + '"');
  if FmPrincipal.FConnections = 0 then
    DmLotes.QrCHsDev.SQL.Add('AND lo.TxCompra+lo.ValValorem+ 1 >= 0.01');
  DmLotes.QrCHsDev.Open;
  if DmLotes.QrCHsDev.RecordCount > 0 then
  begin
    PageControl7.ActivePageIndex := 1;
    if Geral.MB_Pergunta('Aten��o! Emitente com movimenta��o de ' +
      'cheque(s) devolvido(s). Deseja aceitar assim mesmo?') <> ID_YES then
    begin
      Application.CreateForm(TFmChequesGer, FmChequesGer);
      FmChequesGer.EdCPF_1.Text := EdCPF.Text;
      FmChequesGer.EdCPF_1Exit(Self);
      FmChequesGer.WindowState := wsMaximized;
      FmChequesGer.ShowModal;
      FmChequesGer.Destroy;
      Exit;
    end;
  end;
  Screen.Cursor := crHourGlass;
  try
    CalculaValoresChequeAlterado;
    Codigo    := QrLotesCodigo.Value;
    Erros     := 0;
    Valor     := Geral.DMV(EdValor.Text);
    Vence     := Geral.ValidaDataSimples(EdVencto.Text, True);
    Vencto    := FormatDateTime(VAR_FORMATDATE, Vence);
    Data_     := Geral.ValidaDataSimples(EdData.Text, True);
    DCompra   := FormatDateTime(VAR_FORMATDATE, Data_);
    Banco     := Geral.IMV(EdBanco.Text);
    Agencia   := Geral.IMV(EdAgencia.Text);
    Conta     := EdConta.Text;
    Cheque    := Geral.IMV(EdCheque.Text);
    CPF       := Geral.SoNumero_TT(EdCPF.Text);
    Emitente  := EdEmitente.Text;
    Praca     := Geral.IMV(EdRegiaoCompe.Text);
    Tipific   := Geral.IMV(EdTipific.Text);
    StatusSPC := Geral.IMV(EdStatusSPC.Text);
    Comp      := FmPrincipal.VerificaDiasCompensacao(Praca, QrLotesCBECli.Value,
                 Valor, Data_, Vence, QrLotesSCBCli.Value);
    //
    z := 9;
    for i := 1 to z do
    begin
      case i of
        1: if Valor <= 0 then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o valor!', EdValor);
        2: if Vence < Data_ then Erros := Erros +
          MLAGeral.MostraErroControle('Prazo inv�lido!', EdVencto);
        //3: if Comp < 0 then Erros := Erros +
          //MLAGeral.MostraErroControle('Erro! Compensa��o negativa', EdRegiaoCompe);
        4: if Banco <= 0 then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o banco!', EdBanco);
        5: if Agencia <= 0 then Erros := Erros +
          MLAGeral.MostraErroControle('Informe a ag�ncia!', EdAgencia);
        6: if Conta = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o n�mero da conta !', EdConta);
        7: if Cheque <= 0 then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o n�mero do cheque!', EdCheque);
        8: if CPF = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o CPF/CNPJ!', EdCPF);
        9: if Emitente = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o emitente!', EdCPF);
      end;
      if Erros > 0 then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    DMais := Geral.IMV(EdDMaisC.Text);
    Dias := Geral.IMV(EdDias.Text);
    DDeposito := FormatDateTime(VAR_FORMATDATE, UMyMod.CalculaDataDeposito(Vence));
    //
    if LaTipo.Caption = CO_ALTERACAO then Controle := QrLotesItsControle.Value
    else Controle := 0;
    SQL_CH(Controle, Comp, Banco, Agencia, Cheque,
                DMais, Dias, Codigo, Praca, Tipific, StatusSPC, Conta, CPF,
                Emitente, Vencto, DCompra, DDeposito, Valor, LaTipo.Caption,
                QrLotesCliente.Value);
    //
    Dmoc.AtualizaEmitente(MLAGeral.FFD(Banco, 3, siPositivo),
      MLAGeral.FFD(Agencia, 4, siPositivo), Conta, CPF, Emitente, -1);
    EdBanda.Text := '';
    EdBanda.SetFocus;
    if LaTipo.Caption = CO_ALTERACAO then BtSair2Click(Self)
    else begin
      Lote := QrLotesCodigo.Value;
      CalculaLote(Lote, True);
      LocCod(Lote, Lote);
      if Lote <> QrLotesCodigo.Value then
      begin
        Geral.MB_Erro('Erro no refresh do lote. O aplicativo ' +
          'dever� ser encerrado!');
        Application.Terminate;
      end;
    end;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
  FmPrincipal.AtualizaLastEditLote(DCompra);
end;

procedure TFmLotes1.RefreshCheque;
var
  Vence, Data_: TDateTime;
  DMais, Dias, Controle, Codigo, Cheque, Comp, Banco,
  Agencia, Praca, Tipific, StatusSPC: Integer;
  Valor: Double;
  DDeposito, DCompra, Vencto, Conta, CPF, Emitente: String;
begin
  Controle  := DmLotes.QrLIControle.Value;
  Codigo    := DmLotes.QrLICodigo.Value;
  Valor     := DmLotes.QrLIValor.Value;
  Vence     := DmLotes.QrLIVencto.Value;
  Vencto    := FormatDateTime(VAR_FORMATDATE, Vence);
  Data_     := DmLotes.QrLIDCompra.Value;
  DCompra   := FormatDateTime(VAR_FORMATDATE, Data_);
  Banco     := DmLotes.QrLIBanco.Value;
  Agencia   := DmLotes.QrLIAgencia.Value;
  Conta     := DmLotes.QrLIConta.Value;
  Cheque    := DmLotes.QrLICheque.Value;
  CPF       := DmLotes.QrLICPF.Value;
  Emitente  := DmLotes.QrLIEmitente.Value;
  DMais     := DmLotes.QrLIDMais.Value;
  Praca     := DmLotes.QrLIPraca.Value;
  Tipific   := DmLotes.QrLITipific.Value;
  StatusSPC := DmLotes.QrLIStatusSPC.Value;
  Comp      := FmPrincipal.VerificaDiasCompensacao(Praca, DmLotes.QrLICBE.Value, Valor, Data_, Vence,
               DmLotes.QrLISCB.Value);
  Dias      := UMyMod.CalculaDias(Data_, Vence, DMais, Comp,
               Dmod.QrControleTipoPrazoDesc.Value, DmLotes.QrLICBE.Value);
  DDeposito := FormatDateTime(VAR_FORMATDATE, UMyMod.CalculaDataDeposito(Vence));
  //
  CalculaValoresCH_DU_Refresh(Controle, Dias, DmLotes.QrLITxaCompra.Value, Valor);
  //
  SQL_CH(Controle, Comp, Banco, Agencia, Cheque, DMais, Dias, Codigo, Praca,
    Tipific, StatusSPC, Conta, CPF, Emitente, Vencto, DCompra, DDeposito,
    Valor, CO_ALTERACAO, DmLotes.QrLICliente.Value);
end;

procedure TFmLotes1.RefreshDuplicata;
var
  Codigo, Controle, DMais, Dias, Banco, Agencia, CartDep: Integer;
  Valor, Desco, Bruto: Double;
  Duplic, CPF, Sacado, Vencto, DCompra, DDeposito, Emissao, DescAte: String;
  Vence, Data_, Emiss_: TDateTime;
begin
  Controle := DmLotes.QrLIControle.Value;
  Codigo  := DmLotes.QrLICodigo.Value;
  Bruto   := DmLotes.QrLIBruto.Value;
  Desco   := DmLotes.QrLIDesco.Value;
  DescAte := Geral.FDT(DmLotes.QrLIDescAte.Value, 1);
  Valor   := Bruto-Desco;
  Vence   := DmLotes.QrLIVencto.Value;
  Vencto  := FormatDateTime(VAR_FORMATDATE, Vence);
  Data_   := DmLotes.QrLIDCompra.Value;
  DCompra := FormatDateTime(VAR_FORMATDATE, Data_);
  Emiss_  := DmLotes.QrLIEmissao.Value;
  Emissao := FormatDateTime(VAR_FORMATDATE, Emiss_);
  Duplic  := DmLotes.QrLIDuplicata.Value;
  CPF     := DmLotes.QrLICPF.Value;
  Sacado  := DmLotes.QrLIEmitente.Value;
  Banco   := DmLotes.QrLIBanco.Value;
  Agencia := DmLotes.QrLIAgencia.Value;
  CartDep := DmLotes.QrLICartDep.Value;
  //
  DMais := DmLotes.QrLIDMais.Value;
  Dias  := UMyMod.CalculaDias(Data_, Vence, DMais, 0(*Comp*),
    Dmod.QrControleTipoPrazoDesc.Value, 0(*CBE*));
  DDeposito := FormatDateTime(VAR_FORMATDATE, UMyMod.CalculaDataDeposito(Vence));
  //
  CalculaValoresCH_DU_Refresh(Controle, Dias, DmLotes.QrLITxaCompra.Value, Valor);
  //
  SQL_DU(Codigo, Controle, DMais, Dias, Banco, Agencia, Valor, Desco, Bruto,
    Duplic, CPF, Sacado, Vencto, DCompra, DDeposito, Emissao, DescAte,
    CO_ALTERACAO, DmLotes.QrLICliente.Value, CartDep);
  FmPrincipal.AtualizaLastEditLote(DCompra);
end;

procedure TFmLotes1.RGTipoClick(Sender: TObject);
begin
  ConfiguraTaxas;
end;

procedure TFmLotes1.QrLotesBeforeClose(DataSet: TDataSet);
var
  i: Integer;
begin
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do FArrayMySQLLO[i].Close;
  //
  FOrelha := PgPrincipal.ActivePageIndex;
  PgPrincipal.ActivePageIndex := 7;
  Application.ProcessMessages;
  //
  BtAltera.Enabled   := False;
  BtExclui.Enabled   := False;
  SbImprime.Enabled  := False;
  BtCheckImp.Enabled := False;
end;

procedure TFmLotes1.IncluiCheque;
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    MostraEdicao(2, CO_INCLUSAO, 0);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmLotes1.IncluiDuplicata;
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    MostraEdicao(4, CO_INCLUSAO, 0);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmLotes1.QrLotesItsCalcFields(DataSet: TDataSet);
begin
  QrLotesItsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrLotesItsCPF.Value);
  QrLotesItsStatusSPC_TXT.Value := MLAGeral.StatusSPC_TXT(QrLotesItsStatusSPC.Value);
end;

procedure TFmLotes1.EdTxaCompraChange(Sender: TObject);
begin
  CalculaValoresChequeAlterado;
end;

procedure TFmLotes1.CalculaValoresChequeAlterado;
var
  TaxaT, Base, JuroT: Double;
  Dias, i: Integer;
begin
  FTaxa[0] := Geral.DMV(EdTxaCompra.Text);
  for i := 1 to GradeCH.RowCount -1 do FTaxa[i] :=
    Geral.DMV(GradeCH.Cells[1, i]);
  // zerar n�o setados
  for i := GradeCH.RowCount to 6 do FTaxa[i] := 0;
  TaxaT := 0;
  for i := 0 to 6 do TaxaT := TaxaT +FTaxa[i];
  Base := Geral.DMV(EdValor.Text);
  Dias := Geral.IMV(EdDias.Text);
  //juros compostos ?
  JuroT   := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(TaxaT, Dias);
  //
  EdTotalCompraPer.Text := Geral.FFT(TaxaT, 6, siPositivo);
  //
  if TaxaT > FTaxa[0] then
  begin
    if Dmod.QrControleCalcMyJuro.Value = 1 then
    begin
      for i := 0 to 6 do
      begin
        if TaxaT = 0 then FJuro[i] := 0 else
          FJuro[i] := FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 0 to 6 do
          FValr[i] := FJuro[i] * Base / 100;
    end else begin
      FJuro[0] := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(FTaxa[0], Dias);
      FValr[0] := FJuro[0] * Base / 100;
      JuroT := JuroT - FJuro[0];
      TaxaT := TaxaT - FTaxa[0];
      for i := 1 to 6 do
      begin
        if TaxaT = 0 then FJuro[i] := 0 else
          FJuro[i] := FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 1 to 6 do
          FValr[i] := FJuro[i] * Base / 100;
    end;
  end else begin
    FJuro[0] := JuroT;
    FValr[0] := JuroT * Base / 100;
  end;
  //////////////////////////////////////////////////////////////////////////////
end;

procedure TFmLotes1.CalculaValoresCH_DU_Refresh(Controle, Dias: Integer;
TxaCompra, Valor: Double);
var
  TaxaT, JuroT: Double;
  i: Integer;
  Taxas: MyArrayR07;
begin
  Taxas := Dmod.ObtemTaxaDeCompraRealizada(Controle);
  FTaxa[0] := TxaCompra;
  TaxaT    := TxaCompra;
  for i := 1 to 6 do
  begin
    FTaxa[i] := Taxas[i];
    TaxaT := TaxaT + Taxas[i];
  end;
  //
  //juros compostos ?
  JuroT   := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(TaxaT, Dias);
  //
  if TaxaT > FTaxa[0] then
  begin
    if Dmod.QrControleCalcMyJuro.Value = 1 then
    begin
      for i := 0 to 6 do
      begin
        if TaxaT = 0 then FJuro[i] := 0 else
          FJuro[i] := FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 0 to 6 do
          FValr[i] := FJuro[i] * Valor / 100;
    end else begin
      FJuro[0] := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(FTaxa[0], Dias);
      FValr[0] := FJuro[0] * Valor / 100;
      JuroT := JuroT - FJuro[0];
      TaxaT := TaxaT - FTaxa[0];
      for i := 1 to 6 do
      begin
        if TaxaT = 0 then FJuro[i] := 0 else
          FJuro[i] := FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 1 to 6 do
          FValr[i] := FJuro[i] * Valor / 100;
    end;
  end else begin
    FJuro[0] := JuroT;
    FValr[0] := JuroT * Valor / 100;
  end;
end;

procedure TFmLotes1.CalculaValoresChequeImportando(Dias: Integer);
var
  TaxaT, Valor, JuroT: Double;
  i: Integer;
begin
  FTaxa[0] := Geral.DMV(EdTxaCompraX.Text);
  for i := 1 to GradeX1.RowCount -1 do FTaxa[i] :=
    Geral.DMV(GradeX1.Cells[1, i]);
  // Setar, n�o setados
  for i := GradeX1.RowCount to 6 do FTaxa[i] := 0;
  TaxaT := 0;
  for i := 0 to 6 do TaxaT := TaxaT +FTaxa[i];
  Valor := Dmoc.QrLCHsValor.Value;
  //juros compostos ?
  JuroT   := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(TaxaT, Dias);
  //
  (*for i := 0 to 6 do
  begin
    if TaxaT = 0 then FJuro[i] := 0 else
      FJuro[i] := FTaxa[i] / TaxaT * JuroT;
  end;
  for i := 0 to 6 do
      FValr[i] := FJuro[i] * Base / 100;*)
  //////////////////////////////////////////////////////////////////////////////
  if TaxaT > FTaxa[0] then
  begin
    if Dmod.QrControleCalcMyJuro.Value = 1 then
    begin
      for i := 0 to 6 do
      begin
        if TaxaT = 0 then FJuro[i] := 0 else
          FJuro[i] := FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 0 to 6 do
          FValr[i] := FJuro[i] * Valor / 100;
    end else begin
      FJuro[0] := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(FTaxa[0], Dias);
      FValr[0] := FJuro[0] * Valor / 100;
      JuroT := JuroT - FJuro[0];
      TaxaT := TaxaT - FTaxa[0];
      for i := 1 to 6 do
      begin
        if TaxaT = 0 then FJuro[i] := 0 else
          FJuro[i] := FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 1 to 6 do
          FValr[i] := FJuro[i] * Valor / 100;
    end;
  end else begin
    FJuro[0] := JuroT;
    FValr[0] := JuroT * Valor / 100;
  end;
end;

procedure TFmLotes1.CalculaValoresDuplicataAlterado;
var
  TaxaT, Valor, JuroT: Double;
  Dias, i: Integer;
begin
  FTaxa[0] := Geral.DMV(EdTxaCompra4.Text);
  for i := 1 to GradeDU.RowCount -1 do FTaxa[i] :=
    Geral.DMV(GradeDU.Cells[1, i]);
  // serar n�o setados
  for i := GradeDU.RowCount to 6 do FTaxa[i] := 0;
  TaxaT := 0;
  for i := 0 to 6 do TaxaT := TaxaT +FTaxa[i];
  Valor := Geral.DMV(EdValor4.Text);
  Dias := Geral.IMV(EdDias4.Text);
  //juros compostos ?
  JuroT   := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(TaxaT, Dias);
  //
  EdTotalCompraPer4.Text := Geral.FFT(TaxaT, 6, siPositivo);
  //
  (*for i := 0 to 6 do
  begin
    if TaxaT = 0 then FJuro[i] := 0 else
      FJuro[i] := FTaxa[i] / TaxaT * JuroT;
  end;
  for i := 0 to 6 do
      FValr[i] := FJuro[i] * Base / 100;*)
  //////////////////////////////////////////////////////////////////////////////
  if TaxaT > FTaxa[0] then
  begin
    if Dmod.QrControleCalcMyJuro.Value = 1 then
    begin
      for i := 0 to 6 do
      begin
        if TaxaT = 0 then FJuro[i] := 0 else
          FJuro[i] := FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 0 to 6 do
          FValr[i] := FJuro[i] * Valor / 100;
    end else begin
      FJuro[0] := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(FTaxa[0], Dias);
      FValr[0] := FJuro[0] * Valor / 100;
      JuroT := JuroT - FJuro[0];
      TaxaT := TaxaT - FTaxa[0];
      for i := 1 to 6 do
      begin
        if TaxaT = 0 then FJuro[i] := 0 else
          FJuro[i] := FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 1 to 6 do
          FValr[i] := FJuro[i] * Valor / 100;
    end;
  end else begin
    FJuro[0] := JuroT;
    FValr[0] := JuroT * Valor / 100;
  end;
end;

procedure TFmLotes1.CalculaValoresDuplicataImportando(Dias: Integer);
var
  TaxaT, Valor, JuroT: Double;
  i: Integer;
begin
  FTaxa[0] := Geral.DMV(EdTxaCompraX.Text);
  for i := 1 to GradeX1.RowCount -1 do FTaxa[i] :=
    Geral.DMV(GradeX1.Cells[1, i]);
  // setar, n�o setados
  for i := GradeX1.RowCount to 6 do FTaxa[i] := 0;
  TaxaT := 0;
  for i := 0 to 6 do TaxaT := TaxaT +FTaxa[i];
  Valor := Dmoc.QrLDUsValor.Value;
  //juros compostos ?
  JuroT   := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(TaxaT, Dias);
  //
  (*for i := 0 to 6 do
  begin
    if TaxaT = 0 then FJuro[i] := 0 else
      FJuro[i] := FTaxa[i] / TaxaT * JuroT;
  end;
  for i := 0 to 6 do
      FValr[i] := FJuro[i] * Base / 100;*)
  //////////////////////////////////////////////////////////////////////////////
  if TaxaT > FTaxa[0] then
  begin
    if Dmod.QrControleCalcMyJuro.Value = 1 then
    begin
      for i := 0 to 6 do
      begin
        if TaxaT = 0 then FJuro[i] := 0 else
          FJuro[i] := FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 0 to 6 do
          FValr[i] := FJuro[i] * Valor / 100;
    end else begin
      FJuro[0] := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(FTaxa[0], Dias);
      FValr[0] := FJuro[0] * Valor / 100;
      JuroT := JuroT - FJuro[0];
      TaxaT := TaxaT - FTaxa[0];
      for i := 1 to 6 do
      begin
        if TaxaT = 0 then FJuro[i] := 0 else
          FJuro[i] := FTaxa[i] / TaxaT * JuroT;
      end;
      for i := 1 to 6 do
          FValr[i] := FJuro[i] * Valor / 100;
    end;
  end else begin
    FJuro[0] := JuroT;
    FValr[0] := JuroT * Valor / 100;
  end;
end;

procedure TFmLotes1.CalculaLote(Lote: Double; AvisaLoteAntigo: Boolean);
var
  CPMF, IOFd, IOCt, IOFDt, IOFVt, IRRF, IRRF_Val, ISS, ISS_Val,
  PIS, PIS_VAL, PIS_R, PIS_R_Val,
  AdValor, TaxaValX, Valor, TaxaT, COFINS, COFINS_VAL, COFINS_R, COFINS_R_VAL,
  Dias, Total, TaxaVal0, AdValMin, MINTxCompraVal, MINTxCompraPer, MINAV,
  MINTC, MINTC_AM, PIS_T_VAL, COFINS_T_VAL, Liquido, TaxaMedia,
  SobraIni, APagar, PgLiq: Double;
  TipoIOF, i: Integer;
  MaxVencto, Data: String;
begin
  // Usado no lugar de QrLotes
  DmLotes.QrLCalc.Close;
  DmLotes.QrLCalc.Params[0].AsFloat := Lote;
  DmLotes.QrLCalc.Open;
  Data := Geral.FDT(DmLotes.QrLCalcdata.Value, 1);
  //
  DmLotes.QrSum0.Close;
  DmLotes.QrSum0.Params[0].AsFloat := Lote;
  DmLotes.QrSum0.Open;
  Dias := DmLotes.QrSum0PRAZO_MEDIO.Value;
  Total := DmLotes.QrSum0Valor.Value;
  TaxaVal0 := DmLotes.QrSum0VlrCompra.Value;
  MaxVencto := FormatDateTime(VAR_FORMATDATE, DmLotes.QrSum0Vencto.Value);
  //
  DmLotes.QrTxs.Close;
  DmLotes.QrTxs.Params[0].AsFloat := Lote;
  DmLotes.QrTxs.Open;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotestxs SET AlterWeb=1, TaxaVal=:P0, SysAQtd=:P1');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P2');
  while not DmLotes.QrTxs.Eof do
  begin
    if (DmLotes.QrTxsForma.Value = 1) or (DmLotes.QrTxsBase.Value = 1) then
    begin
      if DmLotes.QrTxsTaxaQtd.Value <> 0 then
        TaxaT := DmLotes.QrTxsTaxaTxa.Value * DmLotes.QrTxsTaxaQtd.Value else
        if DmLotes.QrSum0ITENS.Value <> 0 then
          TaxaT := DmLotes.QrTxsTaxaTxa.Value * DmLotes.QrSum0ITENS.Value else
            TaxaT := DmLotes.QrTxsTaxaTxa.Value;
      case DmLotes.QrTxsBase.Value of
        0:
        begin
          if DmLotes.QrTxsForma.Value = 1 then
            Valor := Int((TaxaT * Total)+0.51) / 100
          else Valor := TaxaT;
        end;
        1: Valor := Int((TaxaT * 100)+0.51) / 100;
        else Valor := 0;
      end;
      Dmod.QrUpd.Params[0].AsFloat   := Valor;
      Dmod.QrUpd.Params[1].AsFloat   := DmLotes.QrSum0ITENS.Value;
      //
      Dmod.QrUpd.Params[2].AsInteger := DmLotes.QrTxsControle.Value;
      Dmod.QrUpd.ExecSQL;
    end;
    DmLotes.QrTxs.Next;
  end;
  //
  IRRF     := DmLotes.QrLCalcIRRF.Value;
  PIS      := DmLotes.QrLCalcPIS.Value;
  PIS_R    := DmLotes.QrLCalcPIS_R.Value;
  COFINS   := DmLotes.QrLCalcCOFINS.Value;
  COFINS_R := DmLotes.QrLCalcCOFINS_R.Value;
  ISS      := DmLotes.QrLCalcISS.Value;
  //
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1'  then
    begin
      FArrayMySQLSI[i].Close;
      FArrayMySQLSI[i].Params[0].AsFloat := Lote;
      FArrayMySQLSI[i].Open;
      //
      AdValor      := Total * FArrayMySQLLO[i].FieldByName('AdValorem').AsFloat / 100;
      TaxaValX     := FArrayMySQLSI[i].FieldByName('TaxaVal').AsFloat;
      IRRF_Val     := Trunc(IRRF * AdValor+0.5)/100;
      ISS_Val      := Trunc(ISS * AdValor+0.5)/100;
      PIS_Val      := Trunc(PIS * (AdValor+TaxaValX)+0.5)/100;
      PIS_R_Val    := Trunc(PIS_R * (AdValor+TaxaValX)+0.5)/100;
      COFINS_Val   := Trunc(COFINS * (AdValor+TaxaValX)+0.5)/100;
      COFINS_R_Val := Trunc(COFINS_R * (AdValor+TaxaValX)+0.5)/100;
      //
      Dmod.QrUpd_.Database := Dmod.FArrayMySQLBD[i];
      Dmod.QrUpd_.SQL.Clear;
      Dmod.QrUpd_.SQL.Add('UPDATE emlot SET AlterWeb=1, ValValorem=:P0, TaxaVal=:P1, ');
      Dmod.QrUpd_.SQL.Add('IRRF=:P2, IRRF_Val=:P3, ISS=:P4, ISS_Val=:P5, ');
      Dmod.QrUpd_.SQL.Add('PIS_R=:P6, PIS_R_Val=:P7, COFINS_R=:P8, ');
      Dmod.QrUpd_.SQL.Add('COFINS_R_VAL=:P9, PIS=:P10, PIS_VAL=:P11, ');
      Dmod.QrUpd_.SQL.Add('COFINS=:P12, COFINS_VAL=:P13 ');
      Dmod.QrUpd_.SQL.Add('WHERE Codigo=:Pa');
      //
      Dmod.QrUpd_.Params[00].AsFloat := AdValor;
      Dmod.QrUpd_.Params[01].AsFloat := TaxaValX;
      Dmod.QrUpd_.Params[02].AsFloat := IRRF;
      Dmod.QrUpd_.Params[03].AsFloat := IRRF_Val;
      Dmod.QrUpd_.Params[04].AsFloat := ISS;
      Dmod.QrUpd_.Params[05].AsFloat := ISS_Val;
      Dmod.QrUpd_.Params[06].AsFloat := PIS_R;
      Dmod.QrUpd_.Params[07].AsFloat := PIS_R_Val;
      Dmod.QrUpd_.Params[08].AsFloat := COFINS_R;
      Dmod.QrUpd_.Params[09].AsFloat := COFINS_R_Val;
      Dmod.QrUpd_.Params[10].AsFloat := PIS;
      Dmod.QrUpd_.Params[11].AsFloat := PIS_Val;
      Dmod.QrUpd_.Params[12].AsFloat := COFINS;
      Dmod.QrUpd_.Params[13].AsFloat := COFINS_Val;
      ////
      Dmod.QrUpd_.Params[14].AsFloat := Lote;
      Dmod.QrUpd_.ExecSQL;
    end;
  end;
  //
  if Dmod.QrControleTipoAdValorem.Value = 0 then
  begin
    AdValor := DmLotes.QrLCalcAdValorem.Value * Total / 100;
  end else AdValor := DmLotes.QrLCalcAdValorem.Value;
  //////////////////////////////////////////////////////////////////////////////
  CPMF := DmLotes.QrLCalcCPMF.Value * Total;
  CPMF := Int(CPMF + 0.999999) / 100;
  //
  DmLotes.QrSumTxs.Close;
  DmLotes.QrSumTxs.Params[0].AsFloat := Lote;
  DmLotes.QrSumTxs.Open;
  //
  DmLotes.QrSumOco.Close;
  DmLotes.QrSumOco.Params[0].AsFloat := Lote;
  DmLotes.QrSumOco.Open;
  //
  DmLotes.QrSumCHP.Close;
  DmLotes.QrSumCHP.Params[0].AsFloat := Lote;
  DmLotes.QrSumCHP.Open;
  //
  DmLotes.QrSumDUP.Close;
  DmLotes.QrSumDUP.Params[0].AsFloat := Lote;
  DmLotes.QrSumDUP.Open;
  //
  DmLotes.QrSumP.Close;
  DmLotes.QrSumP.Params[0].AsFloat := Lote;
  DmLotes.QrSumP.Open;
  //
  DmLotes.QrSumRepCli.Close;
  DmLotes.QrSumRepCli.Params[0].AsFloat := Lote;
  DmLotes.QrSumRepCli.Open;
  //

  //////////////////////////////////////////////////////////////////////////////

  ///////// M�nimos ///////////////////
  if (Total >= 0.01) and (TaxaVal0+AdValor>=0.01) then
  begin
    if Dmod.QrControleAdValMinTip.Value = 0 then
      AdValMin :=
        Trunc(Dmod.QrControleAdValMinVal.Value * Total) / 100
    else AdValMin := Dmod.QrControleAdValMinVal.Value;
    if AdValMin > AdValor then MINAV := Trunc(AdValMin*100)/100
    else MINAV := Trunc(AdValor*100)/100;
    //
    MINTxCompraPer := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(
      Dmod.QrControleFatorCompraMin.Value, Dias);
    MINTxCompraVal := Trunc(MINTxCompraPer * Total) / 100;
    if MINTxCompraVal > TaxaVal0 then MINTC := Round(MINTxCompraVal*100)/100
    else MINTC := Round(TaxaVal0*100)/100;
    //
    MINTC_AM := MLAGeral.DescobreJuroComposto(MINTC/Total*100, Dias, 2);
  end else begin
    MINAV := 0;
    MINTC := 0;
    MINTC_AM := 0;
  end;
  IOCt := DmLotes.QrLCalcIOC.Value * Dias;
  IOCt := Int((IOCt * Total)+ 0.999999) / 100;

  ObtemTipoEPercDeIOF2008(DmLotes.QrLCalcTipo.Value, DmLotes.QrLCalcSimples.Value, Total,
    IOFd, TipoIOF);
  IOFd := IOFd / 365;

  IOFdt := IOFd * Dias;
  IOFdt := Int((IOFdt * Total)+ 0.999999) / 100;

  IOFvt := Int((DmLotes.QrLCalcIOFv.Value * (Total - MINTC))+ 0.999999) / 100;

  IRRF_Val  := Trunc((IRRF * MINAV) + 0.4)/100;
  ////////////////////////////////////////////////////////////////////////////
  ISS_Val      := Trunc(ISS * MINAV+0.5)/100;
  PIS_Val      := Trunc(PIS * (MINAV+MINTC)+0.5)/100;
  PIS_R_Val    := Trunc(PIS_R * (MINAV+MINTC)+0.5)/100;
  PIS_T_Val    := Trunc((PIS+PIS_R) * (MINAV+MINTC)+0.5)/100;
  COFINS_Val   := Trunc(COFINS * (MINAV+MINTC)+0.5)/100;
  COFINS_R_Val := Trunc(COFINS_R * (MINAV+MINTC)+0.5)/100;
  COFINS_T_Val := Trunc((COFINS+COFINS_R) * (MINAV+MINTC)+0.5)/100;
  //
  // Sobras
  Dmoc.QrSobraIni.Close;
  Dmoc.QrSobraIni.Params[00].AsInteger := DmLotes.QrLCalcCliente.Value;
  Dmoc.QrSobraIni.Params[01].AsString  := Data;
  Dmoc.QrSobraIni.Params[02].AsString  := Data;
  Dmoc.QrSobraIni.Params[03].AsFloat   := Lote;
  Dmoc.QrSobraIni.Open;
  //
  APagar := Total - MINAV - MINTC - IOCt - IOFDt - IOFVt;
  PGLiq  := DmLotes.QrSumPDebito.Value + DmLotes.QrSumRepCliValor.Value;
  //
  SobraIni := Dmoc.QrSobraIniSobraNow.Value;
  // Fim Sobras
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotes SET AlterWeb=1,   Total=:P0, Dias=:P1, TxCompra=:P2, ');
  Dmod.QrUpd.SQL.Add('ValValorem=:P3,');
  //
  Dmod.QrUpd.SQL.Add('IRRF=:P4, IRRF_Val=:P5, ISS=:P6, ISS_Val=:P7, ');
  Dmod.QrUpd.SQL.Add('PIS_R=:P8, PIS_R_Val=:P9, Tarifas=:P10, OcorP=:P11, ');
  Dmod.QrUpd.SQL.Add('PIS=:P12, PIS_Val=:P13, COFINS=:P14, COFINS_VAL=:P15, ');
  Dmod.QrUpd.SQL.Add('COFINS_R=:P16, COFINS_R_VAL=:P17, IOC_VAL=:P18, ');
  Dmod.QrUpd.SQL.Add('CPMF_VAL=:P19, MaxVencto=:P20, CHDevPg=:P21, ');
  Dmod.QrUpd.SQL.Add('MINAV=:P22, MINTC=:P23, MINTC_AM=:P24, PIS_T_Val=:P25, ');
  Dmod.QrUpd.SQL.Add('COFINS_T_Val=:P26, PgLiq=:P27, DUDevPg=:P28, ');
  Dmod.QrUpd.SQL.Add('Itens=:P29, SobraIni=:P30, ');
  Dmod.QrUpd.SQL.Add('IOFd_Val=:P31, IOFv_Val=:P32, IOFd=:P33, TipoIOF=:P34 ');
  //
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa ');
  //
  Dmod.QrUpd.Params[00].AsFloat  := Total;
  Dmod.QrUpd.Params[01].AsFloat  := Dias;
  Dmod.QrUpd.Params[02].AsFloat  := TaxaVal0;
  Dmod.QrUpd.Params[03].AsFloat  := AdValor;
  //
  Dmod.QrUpd.Params[04].AsFloat := IRRF;
  Dmod.QrUpd.Params[05].AsFloat := IRRF_Val;
  Dmod.QrUpd.Params[06].AsFloat := ISS;
  Dmod.QrUpd.Params[07].AsFloat := ISS_Val;
  Dmod.QrUpd.Params[08].AsFloat := PIS_R;
  Dmod.QrUpd.Params[09].AsFloat := PIS_R_Val;
  Dmod.QrUpd.Params[10].AsFloat := DmLotes.QrSumTxsTaxaVal.Value;
  Dmod.QrUpd.Params[11].AsFloat := DmLotes.QrSumOcoPAGO.Value;
  Dmod.QrUpd.Params[12].AsFloat := PIS;
  Dmod.QrUpd.Params[13].AsFloat := PIS_VAL;
  Dmod.QrUpd.Params[14].AsFloat := COFINS;
  Dmod.QrUpd.Params[15].AsFloat := COFINS_VAL;
  Dmod.QrUpd.Params[16].AsFloat := COFINS_R;
  Dmod.QrUpd.Params[17].AsFloat := COFINS_R_VAL;
  Dmod.QrUpd.Params[18].AsFloat := IOCt;
  Dmod.QrUpd.Params[19].AsFloat := CPMF;
  Dmod.QrUpd.Params[20].AsString  := MaxVencto;
  Dmod.QrUpd.Params[21].AsFloat   := DmLotes.QrSumCHPPago.Value;
  Dmod.QrUpd.Params[22].AsFloat   := MINAV;
  Dmod.QrUpd.Params[23].AsFloat   := MINTC;
  Dmod.QrUpd.Params[24].AsFloat   := MINTC_AM;
  Dmod.QrUpd.Params[25].AsFloat   := PIS_T_VAL;
  Dmod.QrUpd.Params[26].AsFloat   := COFINS_T_VAL;
  Dmod.QrUpd.Params[27].AsFloat   := PgLiq;
  Dmod.QrUpd.Params[28].AsFloat   := DmLotes.QrSumDUPPago.Value;
  Dmod.QrUpd.Params[29].AsInteger := DmLotes.QrSum0ITENS.Value;
  Dmod.QrUpd.Params[30].AsFloat   := SobraIni;
  Dmod.QrUpd.Params[31].AsFloat   := IOFDt;
  Dmod.QrUpd.Params[32].AsFloat   := IOFVt;
  Dmod.QrUpd.Params[33].AsFloat   := IOFd;
  Dmod.QrUpd.Params[34].AsFloat   := TipoIOF;
  //
  Dmod.QrUpd.Params[35].AsFloat   := Lote;
  Dmod.QrUpd.ExecSQL;
  //LocCod(Codigo, Codigo);
  DmLotes.QrLCalc.Close;
  DmLotes.QrLCalc.Params[0].AsFloat := Lote;
  DmLotes.QrLCalc.Open;
  //
  if AvisaLoteAntigo then
  begin
    Dmoc.QrOutros.Close;
    Dmoc.QrOutros.Params[00].AsInteger := DmLotes.QrLCalcCliente.Value;
    Dmoc.QrOutros.Params[01].AsString  := Data;
    Dmoc.QrOutros.Params[02].AsString  := Data;
    Dmoc.QrOutros.Params[03].AsFloat   := Lote;
    Dmoc.QrOutros.Open;
    if Dmoc.QrOutros.RecordCount > 0 then
      MyObjects.frxMostra(Dmoc.frxOutros, 'Lotes a recalcular');
  end;
  //
end;

procedure TFmLotes1.QrLotesCalcFields(DataSet: TDataSet);
var
 VaValT, AdValT, VaTaxT, TotISS, TotIRR, TotPIS: Double;
 i: Integer;
begin
  QrLotesNOMETIPOLOTE.Value := GetTitle(QrLotesTipo.Value);
  //
  QrLotesCNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrLotesCNPJCPF.Value);
  AdValT := QrLotesAdValorem.Value ;
  VaValT := QrLotesValValorem.Value;
  VaTaxT := QrLotesTxCompra.Value;
  TotISS := QrLotesISS_Val.Value;
  TotIRR := QrLotesIRRF_Val.Value;
  TotPIS := QrLotesPIS_R_Val.Value;
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      AdValT := AdValT + FArrayMySQLLO[i].FieldByName('AdValorem').AsFloat;
      VaValT := VaValT + FArrayMySQLLO[i].FieldByName('ValValorem').AsFloat;
      VaTaxT := VaTaxT + FArrayMySQLLO[i].FieldByName('TaxaVal').AsFloat;
      TotISS := TotISS + FArrayMySQLLO[i].FieldByName('ISS_Val').AsFloat;
      TotIRR := TotIRR + FArrayMySQLLO[i].FieldByName('IRRF_Val').AsFloat;
      TotPIS := TotPIS + FArrayMySQLLO[i].FieldByName('PIS_R_Val').AsFloat;
    end;
  end;
  // IMPOSTOS
  QrLotesTOT_ISS.Value  := TotISS;
  QrLotesTOT_IRR.Value := TotIRR;
  //
  QrLotesADVALOREM_TOTAL.Value := AdValT;
  QrLotesVALVALOREM_TOTAL.Value := VaValT;
  QrLotesTAXAVALOR_TOTAL.Value := VaTaxT;
  //
  if QrLotesTotal.Value = 0 then
  begin
    QrLotesTAXAPERCE_TOTAL.Value := 0;
    QrLotesTAXAPERCE_PROPR.Value := 0;
    QrLotesTAXAPERCEMENSAL.Value := 0;
  end else begin
    QrLotesTAXAPERCE_TOTAL.Value := VaTaxT / QrLotesTotal.Value * 100;
    QrLotesTAXAPERCE_PROPR.Value := QrLotesTxCompra.Value / QrLotesTotal.Value * 100;
    QrLotesTAXAPERCEMENSAL.Value := MLAGeral.DescobreJuroComposto(
      QrLotesTAXAPERCE_TOTAL.Value, QrLotesDias.Value, 2);
  end;
  //////////////////////////////////////////////////////////////////////////////
  QrLotesSUB_TOTAL.Value := Round((QrLotesTotal.Value - QrLotesVALVALOREM_TOTAL.Value -
    QrLotesTAXAVALOR_TOTAL.Value - QrLotesIRRF_VAL.Value - QrLotesIOC_VAL.Value -
    QrLotesIOFd_VAL.Value - QrLotesIOFv_VAL.Value - QrLotesCPMF_VAL.Value) * 100)/100;
  //////////////////////////////////////////////////////////////////////////////
  QrLotesSUB_TOTAL_2.Value := QrLotesSUB_TOTAL.Value - QrLotesTarifas.Value;
  //////////////////////////////////////////////////////////////////////////////
  QrLotesSUB_TOTAL_3.Value := QrLotesSUB_TOTAL_2.Value - QrLotesOcorP.Value;
  //////////////////////////////////////////////////////////////////////////////
  QrLotesVAL_LIQUIDO.Value := Round((QrLotesSUB_TOTAL_3.Value -
    QrLotesCHDevPG.Value - QrLotesDUDevPG.Value)*100)/100;
  //
  QrLotesSALDOAPAGAR.Value := QrLotesVAL_LIQUIDO.Value - QrLotesPgLiq.Value
   + QrLotesSobraIni.Value;
  //
  QrLotesMEU_PISCOFINS_VAL.Value := QrLotesPIS_Val.Value + QrLotesPIS_R_Val.Value +
    QrLotesCOFINS_Val.Value + QrLotesCOFINS_R_Val.Value;
  QrLotesMEU_PISCOFINS.Value := QrLotesPIS.Value + QrLotesPIS_R.Value +
    QrLotesCOFINS.Value + QrLotesCOFINS_R.Value;

  ///////// M�nimos ///////////////////
  if QrLotesTotal.Value > 0 then
  begin
    QrLotesADVAL_TXT.Value := Geral.FFT(QrLotesMINAV.Value /
      QrLotesTotal.Value * 100, 2, siPositivo);
  end else begin
    QrLotesADVAL_TXT.Value := '0,00';
  end;
  QrLotesTAXA_AM_TXT.Value := Geral.FFT(QrLotesMINTC_AM.Value, 2, siPositivo);
  //
  QrLotesSUB_TOTAL_MEU.Value := QrLotesTotal.Value -
    QrLotesMINAV.Value - QrLotesMINTC.Value;
  QrLotesVAL_LIQUIDO_MEU.Value := QrLotesSUB_TOTAL_MEU.Value -
    QrLotesIOC_VAL.Value - QrLotesIOFd_VAL.Value - QrLotesIOFv_VAL.Value;
  //
  QrLotesA_PG_LIQ.Value := QrLotesVAL_LIQUIDO_MEU.Value - QrLotesPgLiq.Value;
  ///////// Fim M�nimos ///////////////////
  if QrLotesTipo.Value = 0 then
  begin
    QrLotesCAPTIONCONFCARTA.Value := 'Malote conferido:';
    if QrLotesConferido.Value > 0 then
      QrLotesNOMECOFECARTA.Value := 'SIM'
    else QrLotesNOMECOFECARTA.Value := 'N�O';
  end else begin
    QrLotesCAPTIONCONFCARTA.Value := 'Carta sacado impressa:';
    if QrLotesECartaSac.Value > 0 then
      QrLotesNOMECOFECARTA.Value := 'SIM'
    else QrLotesNOMECOFECARTA.Value := 'N�O';
  end;
  if QrLotesSpread.Value = 0 then QrLotesNOMESPREAD.Value := '+'
  else QrLotesNOMESPREAD.Value := '-';
  if QrLotesSCB.Value = 0 then
    QrLotesNOMESPREAD.Value := 'CCB' + QrLotesNOMESPREAD.Value
  else
    QrLotesNOMESPREAD.Value := 'SCB' + QrLotesNOMESPREAD.Value;
  // Valores Negativos - Ponto de vista cliente da factoring
  QrLotesIOC_VALNEG.Value         := - QrLotesIOC_VAL.Value;
  QrLotesCPMF_VALNEG.Value        := - QrLotesCPMF_VAL.Value;
  QrLotesTAXAVALOR_TOTALNEG.Value := - QrLotesTAXAVALOR_TOTAL.Value;
  QrLotesVAVALOREM_TOTALNEG.Value := - QrLotesVALVALOREM_TOTAL.Value;
  QrLotesCHDevPgNEG.Value         := - QrLotesCHDevPg.Value;
  QrLotesDUDevPgNEG.Value         := - QrLotesDUDevPg.Value;
end;

procedure TFmLotes1.DefineAdValoremEmpresas;
var
  i: Integer;
begin
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
     Dmod.QrUpd_.Database := Dmod.FArrayMySQLBD[i];
     Dmod.QrUpd_.SQL.Clear;
     Dmod.QrUpd_.SQL.Add('DELETE FROM emlot WHERE Codigo=:P0');
     Dmod.QrUpd_.Params[0].AsInteger := Geral.IMV(EdCodigo.Text);
     Dmod.QrUpd_.ExecSQL;
     //
     Dmod.QrUpd_.SQL.Clear;
     Dmod.QrUpd_.SQL.Add('INSERT INTO emlot SET AlterWeb=1, AdValorem=:P0, ');
     Dmod.QrUpd_.SQL.Add('Codigo=:Pa');
     //
     Dmod.QrUpd_.Params[0].AsFloat   := Geral.DMV(GradeAV.Cells[1, i+1]);
     //
     Dmod.QrUpd_.Params[1].AsInteger := Geral.IMV(EdCodigo.Text);
     Dmod.QrUpd_.ExecSQL;
     //
    end;
  end;
end;

procedure TFmLotes1.DefineCompraEmpresas(Codigo, Controle: Integer);
var
  i: Integer;
  //Valor: Double;
begin
  //Valor := Geral.DMV(EdValor.Text);
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
     Dmod.QrUpd_.Database := Dmod.FArrayMySQLBD[i];
     Dmod.QrUpd_.SQL.Clear;
     Dmod.QrUpd_.SQL.Add('DELETE FROM emlotits WHERE Controle=:P0');
     Dmod.QrUpd_.Params[0].AsInteger := Controle;
     Dmod.QrUpd_.ExecSQL;
     //
     Dmod.QrUpd_.SQL.Clear;
     Dmod.QrUpd_.SQL.Add('INSERT INTO emlotits SET TaxaPer=:P0, TaxaVal=:P1, ');
     Dmod.QrUpd_.SQL.Add('JuroPer=:P2, Codigo=:Pa, Controle=:Pb');
     //
     Dmod.QrUpd_.Params[0].AsFloat   := FTaxa[i+1];
     Dmod.QrUpd_.Params[1].AsFloat   := FValr[i+1];
     Dmod.QrUpd_.Params[2].AsFloat   := FJuro[i+1];
     //
     Dmod.QrUpd_.Params[3].AsInteger := Codigo;
     Dmod.QrUpd_.Params[4].AsInteger := Controle;
     Dmod.QrUpd_.ExecSQL;
     //
    end;
  end;
end;

function TFmLotes1.DefineDataSetsNF(frx: TfrxReport): Boolean;
var
  I: Integer;
begin
  Result := False;
  //
  Dmod.frxDsControle.DataSet  := Dmod.QrControle;
  Dmod.frxDsDono.DataSet      := Dmod.QrDono;
  DModG.frxDsEndereco.DataSet := DModG.QrEndereco;
  frxDsNF.DataSet             := QrLotes;
  //
  frx.Datasets.Clear;
  frx.DataSets.Add(Dmod.frxDsControle);
  frx.DataSets.Add(Dmod.frxDsDono);
  frx.DataSets.Add(DModG.frxDsEndereco);
  frx.DataSets.Add(frxDsNF);
  //
  for I := 0 to frx.Datasets.Count -1 do
    frx.DataSets.Items[I].DataSet.Enabled := True;
  //
  Result := True;
end;

procedure TFmLotes1.GradeAVDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  OldAlign, Color: Integer;
begin
  if GradeAV = nil then Exit;
  Color := clBlack;
  SetTextColor(GradeAV.Canvas.Handle, Color);
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(GradeAV.Canvas.Handle, TA_LEFT);
      GradeAV.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeAV.Cells[Acol, ARow]);
      SetTextAlign(GradeAV.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(GradeAV.Canvas.Handle, TA_RIGHT);
      GradeAV.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Geral.TFT(GradeAV.Cells[Acol, ARow], 6, siPositivo));
      SetTextAlign(GradeAV.Canvas.Handle, OldAlign);
  end //else if ACol = 02 then begin
end;

procedure TFmLotes1.GradeCHKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  CalculaValoresChequeAlterado;
  if key=VK_RETURN then
  begin
    if GradeCH.EditorMode = True then
    begin
      if (GradeCH.Row < GradeCH.RowCount-1) then
        GradeCH.Row := GradeCH.Row +1
      else BtConfirma2.SetFocus;
    end;
  end
end;

procedure TFmLotes1.GradeCHDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  OldAlign, Color: Integer;
begin
  if GradeCH = nil then Exit;
  Color := clBlack;
  SetTextColor(GradeCH.Canvas.Handle, Color);
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(GradeCH.Canvas.Handle, TA_LEFT);
      GradeCH.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeCH.Cells[Acol, ARow]);
      SetTextAlign(GradeCH.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(GradeCH.Canvas.Handle, TA_RIGHT);
      GradeCH.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Geral.TFT(GradeCH.Cells[Acol, ARow], 6, siPositivo));
      SetTextAlign(GradeCH.Canvas.Handle, OldAlign);
  end //else if ACol = 02 then begin
end;

procedure TFmLotes1.GradeAVKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_RETURN then
  begin
    if GradeAV.EditorMode = True then
    begin
      if (GradeAV.Row < GradeAV.RowCount-1) then
        GradeAV.Row := GradeAV.Row +1
      else if PainelItens.Visible then EdDMaisX.SetFocus
      else BtConfirma.SetFocus;
    end;
  end
end;

procedure TFmLotes1.EdBandaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then BtDesisteClick(Self);
end;

procedure TFmLotes1.EdRegiaoCompeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then BtDesisteClick(Self);
end;

procedure TFmLotes1.EdDataExit(Sender: TObject);
var
  Data: TDateTime;
  Casas: Integer;
begin
  Casas := Length(EdData.Text);
  Data := Geral.ValidaDataSimples(EdData.Text, False);
  if Data > 2 then
  begin
    if Data <= Date then
      if Casas < 6 then Data := IncMonth(Data, 12);
    EdData.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdData.SetFocus;
  CalculaDiasCH;
end;

procedure TFmLotes1.BtImportarClick(Sender: TObject);
begin
  //Importar('', 2);
end;

procedure TFmLotes1.Importar(DarIni: String; Tipo: Integer);
var
  ver, Doc: Integer;
  Importou: Boolean;
  i, j, k: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
  Importou := False;
  FmPrincipal.FTipoImport := Tipo;
  FControlIts := QrLotesItsControle.Value;
  case Tipo of
    4:
    begin
      TPCheque.Date := FmPrincipal.TPCheque.Date;
      ArqSCX.LimpaGrade(GradeC, 1, 1);
      ArqSCX.LimpaGrade(GradeD, 1, 1);
      j := FmPrincipal.GradeC.RowCount;
      GradeC.RowCount := j;
      for i := 1 to j -1 do
      begin
        for k := 1 to 10 do
          GradeC.Cells[k,i] := FmPrincipal.GradeC.Cells[k,i];
      end;
      if GradeC.RowCount > 2 then
        Importou := True
      else
        Geral.MB_Aviso('"' + BtImportaBCO.Caption +
          '" deve ter mais de um documento!');
    end;
    5:
    begin
      Dmoc.QrLLI.Close;
      Dmoc.QrLLI.Params[0].AsInteger := FLoteWeb;
      FLoteWebDone := FLoteWeb;
      FLoteWeb := 0;
      Dmoc.QrLLI.Open;
      Dmoc.QrLLI.First;

      TPCheque.Date := Dmoc.QrLLCData.Value;
      ArqSCX.LimpaGrade(GradeC, 1, 1);
      ArqSCX.LimpaGrade(GradeD, 1, 1);

      while not Dmoc.QrLLI.Eof do
      begin
        Doc := Dmoc.QrLLI.RecNo;
        if Dmoc.QrLLCTipo.Value = 0 then
        begin
          with GradeC do
          begin
            if Doc > RowCount-1 then RowCount := Doc + 1;
            Cells[00,Doc] := Geral.FF0(Doc);
            Cells[01,Doc] := FormatFloat('000', Dmoc.QrLLIPraca.Value);
            Cells[02,Doc] := FormatFloat('000', Dmoc.QrLLIBanco.Value);
            Cells[03,Doc] := FormatFloat('0000', Dmoc.QrLLIAgencia.Value);
            Cells[04,Doc] := Dmoc.QrLLIConta.Value;
            Cells[05,Doc] := FormatFloat('000000', Dmoc.QrLLICheque.Value);
            Cells[06,Doc] := Geral.FFT(Dmoc.QrLLIBruto.Value, 2, siPositivo);
            Cells[07,Doc] := Geral.FDT(Dmoc.QrLLIVencto.Value, 2);
            Cells[09,Doc] := Geral.FormataCNPJ_TT(Dmoc.QrLLICPF.Value);
            Cells[10,Doc] := Dmoc.QrLLIEmitente.Value;
          end;
        end else begin
          // Parei Aqui
          with GradeD do
          begin
            if Doc > RowCount-1 then RowCount := Doc + 1;
            Cells[00,Doc] := Geral.FF0(Doc);
            Cells[01,Doc] := Geral.FDT(Dmoc.QrLLIEmissao.Value, 2);
            Cells[02,Doc] := Dmoc.QrLLIDuplicata.Value;
            Cells[03,Doc] := Geral.FFT(Dmoc.QrLLIBruto.Value, 2, siPositivo);
            Cells[04,Doc] := Geral.FFT(Dmoc.QrLLIDesco.Value, 2, siPositivo);
            Cells[05,Doc] := Geral.FFT(Dmoc.QrLLIValor.Value, 2, siPositivo);
            Cells[06,Doc] := Geral.FDT(Dmoc.QrLLIVencto.Value, 2);
            Cells[07,Doc] := Geral.FormataCNPJ_TT(Dmoc.QrLLICPF.Value);
            Cells[08,Doc] := Dmoc.QrLLIEmitente.Value;
            Cells[09,Doc] := Dmoc.QrLLIRua.Value;
            Cells[10,Doc] := Geral.FF0(Dmoc.QrLLINumero.Value);
            Cells[11,Doc] := Dmoc.QrLLICompl.Value;
            Cells[12,Doc] := Dmoc.QrLLIBairro.Value;
            Cells[13,Doc] := Dmoc.QrLLICidade.Value;
            Cells[14,Doc] := Geral.FormataCEP_NT(Dmoc.QrLLICEP.Value);
            Cells[15,Doc] := Dmoc.QrLLIUF.Value;
            Cells[16,Doc] := Geral.FormataTelefone_TT_Curto(Dmoc.QrLLITel1.Value);
            Cells[17,Doc] := Dmoc.QrLLIIE.Value;
          end;
        end;
        Dmoc.QrLLI.Next;
      end;
      EdCodCli.Text := Geral.FF0(Dmoc.QrLLCCliente.Value);
      EdNomCli.Text := Dmoc.QrLLCNOMECLI.Value;
      TPCheque.Date := Dmoc.QrLLCData.Value;
      FmPrincipal.FVerArqSCX := 0;//Geral.IMV(Dmoc.QrLLCVersao.Value);
      Importou := True;
    end;
    else begin
      Importou := ArqSCX.AbreArquivoSCX(GradeC, GradeD, EdCodCli, EdNomCli,
      TPCheque, ProgressBar1, DarIni);
    end;
  end;
  if Importou then
  begin
    Application.ProcessMessages;
    // � mais r�pido para excluir dados ?
    UCriar.RecriaTabelaLocal('ImportLote', 1);
    Application.ProcessMessages;
    ArqSCX.CalculaGradeC(EdQtdeCh, EdValorCH, EdQtdeTo, EdValorTO,
      EdValorDU, GradeC, GradeD, TPCheque);
    Application.ProcessMessages;
    ArqSCX.CalculaGradeD(EdQtdeDU, EdValorDU, EdQtdeTo, EdValorTO,
      EdValorCH, GradeC, GradeD, TPCheque);
    Application.ProcessMessages;
    MostraEdicao(3, CO_INCLUSAO, 0);
    Application.ProcessMessages;
    DefineRiscos(Geral.IMV(EdCodCli.Text));
    Application.ProcessMessages;
    //TimerSCX.Enabled := True;
    InsereImportLote;
  end;
  LaVerSuit1.Caption := Geral.VersaoTxt2006(FmPrincipal.FVerArqSCX);
  LaVerSuit2.Caption := Geral.VersaoTxt2006(FmPrincipal.FVerArqSCX);
  Ver := Geral.VersaoInt2006(FVersaoSuitCash);
  if FmPrincipal.FVerArqSCX < Ver then
  begin
    LaVerSuit1.Font.Color := clRed;
    LaVerSuit2.Font.Color := clRed;
  end else begin
    LaVerSuit1.Font.Color := clNavy;
    LaVerSuit2.Font.Color := clNavy;
  end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLotes1.BtFecharClick(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO, 0);
  Application.ProcessMessages;
  DefineRiscos(QrLotesCliente.Value);
end;

procedure TFmLotes1.BtPagtosExcluiClick(Sender: TObject);
begin
  ExcluiImportLote(Dmoc.QrLCHsTipo.Value, Dmoc.QrLCHsOrdem.Value);
  //LocalizaProximoChNaoVerde;
end;

procedure TFmLotes1.BtPagtosExclui2Click(Sender: TObject);
begin
  ExcluiImportLote(Dmoc.QrLDUsTipo.Value, Dmoc.QrLDUsOrdem.Value);
  (*MLAGeral.ExcluiLinha(GradeD);
  ArqSCX.CalculaGradeC(EdQtdeCh, EdValorCH, EdQtdeTo, EdValorTO, EdValorDU,
    GradeC, GradeD, TPCheque);
  ArqSCX.CalculaGradeD(EdQtdeDU, EdValorDU, EdQtdeTo, EdValorTO, EdValorCH,
    GradeC, GradeD,TPCheque);*)
  //LocalizaProximoDUNaoVerde;
end;

procedure TFmLotes1.BtNFSeClick(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_NFSePesq(False, nil, nil);
end;

procedure TFmLotes1.BtNovoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNovo, BtNovo);
end;

procedure TFmLotes1.BorderauxdeCheques1Click(Sender: TObject);
begin
  FConferido := 1;
  FmPrincipal.FTipoImport := 1;
  IncluiRegistro(0, 0);
end;

procedure TFmLotes1.BorderauxdeDuplicatas1Click(Sender: TObject);
begin
  FmPrincipal.FTipoImport := 1;
  FConferido := 1;
  IncluiRegistro(1, 1);
end;

procedure TFmLotes1.BtCalendario2Click(Sender: TObject);
begin
  Application.CreateForm(TFmSomaDiasCred, FmSomaDiasCred);
  FmSomaDiasCred.TPDataI.Date := Geral.ValidaDataSimples(EdData4.Text, True);
  FmSomaDiasCred.TPDataF.Date := Geral.ValidaDataSimples(EdVence4.Text, True);
  FmSomaDiasCred.EdComp.Text  := '0';
  FmSomaDiasCred.EdDMais.Text := EdDMaisD.Text;
  FmSomaDiasCred.ShowModal;
  FmSomaDiasCred.Destroy;
end;

procedure TFmLotes1.BtSair4Click(Sender: TObject);
var
  Codigo : Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Codigo := QrLotesCodigo.Value;
    CalculaLote(Codigo, True);
    LocCod(Codigo, Codigo);
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Lotes', 'Codigo');
    MostraEdicao(0, CO_TRAVADO, 0);
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmLotes1.EdVence4Exit(Sender: TObject);
var
  Data, Emis: TDateTime;
  Casas: Integer;
begin
  Casas := Length(EdVence4.Text);
  Data := Geral.ValidaDataSimples(EdVence4.Text, False);
  Emis := Geral.ValidaDataSimples(EdEmiss4.Text, False);
  if Data > 2 then
  begin
    if Data <= Emis then
      if Casas < 6 then Data := IncMonth(Data, 12);
    if Data < Emis then
    begin
      if Geral.MB_Pergunta('Data j� passada! Deseja incrementar um ano?') = ID_YES then
      begin
        Data := IncMonth(Data, 12);
        EdVence4.SetFocus;
      end else
      begin
        EdVence4.SetFocus;
        Exit;
      end;
    end;
    EdVence4.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdVence4.SetFocus;
  CalculaDiasDU;
end;

procedure TFmLotes1.EdTxaCompra4Exit(Sender: TObject);
begin
  CalculaValoresDuplicataAlterado;
end;

procedure TFmLotes1.EdDMaisDExit(Sender: TObject);
begin
  CalculaDiasDU;
end;

procedure TFmLotes1.EdEmiss4Exit(Sender: TObject);
var
  Data: TDateTime;
begin
  Data := Geral.ValidaDataSimples(EdEmiss4.Text, False);
  if Data > 2 then
  begin
    EdEmiss4.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdEmiss4.SetFocus;
  CalculaDiasDU;
end;

procedure TFmLotes1.EdValor4Exit(Sender: TObject);
begin
  CalculaDiasDU;
end;

procedure TFmLotes1.EdData4Exit(Sender: TObject);
var
  Data: TDateTime;
begin
  Data := Geral.ValidaDataSimples(EdData4.Text, False);
  if Data > 2 then
  begin
    EdData4.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdData4.SetFocus;
  CalculaDiasDU;
end;

procedure TFmLotes1.EdCNPJExit(Sender: TObject);
var
  Num : String;
  CNPJ : String;
begin
  CNPJ := Geral.SoNumero_TT(EdCNPJ.Text);
  if CNPJ <> CO_VAZIO then
    begin
    Num := Geral.CalculaCNPJCPF(CNPJ);
    if Geral.FormataCNPJ_TFT(CNPJ) <> Num then
    begin
      Geral.MB_Aviso('N�mero inv�lido para CNPJ/CNPJ!');
      EdCNPJ.SetFocus;
    end else begin
      EdCNPJ.Text := Geral.FormataCNPJ_TT(CNPJ);
      LocalizaSacado(True);
    end;
    PesquisaRiscoSacado(EdCNPJ.Text);
  end else EdCNPJ.Text := CO_VAZIO;
end;

procedure TFmLotes1.EdCNPJKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F8 then
  begin
    Application.CreateForm(TFmPesqCPFCNPJ, FmPesqCPFCNPJ);
    FmPesqCPFCNPJ.ShowModal;
    FmPesqCPFCNPJ.Destroy;
    if VAR_CPF_PESQ <> '' then EdCNPJ.Text := VAR_CPF_PESQ;
    {
    Dmod.QrLocCPF.Close;
    Dmod.QrLocCPF.Params[0].AsString := Geral.SoNumero_TT(EdCNPJ.Text);
    Dmod.QrLocCPF.Open;
    if Dmod.QrLocCPF.RecordCount = 0 then EdEmitente1.Text := ''
    else EdEmitente1.Text := Dmod.QrLocCPFNome.Value;
    }
  end;
end;

procedure TFmLotes1.EdCEPEnter(Sender: TObject);
begin
  FECEP := Geral.SoNumero_TT(EdCEP.Text);
end;

procedure TFmLotes1.EdCEPExit(Sender: TObject);
var
  CEP: String;
begin
  EdCEP.Text := Geral.FormataCEP_TT(EdCEP.Text);
  //
  CEP := Geral.SoNumero_TT(EdCEP.Text);
  //
  if CEP <> FECEP then
  begin
    if Trim(CEP) <> '' then
    begin
      U_CEP.ConsultaCEP(EdCEP, nil, EdRua, EdNumero, EdBairro, EdCidade, nil,
        nil, EdNomCli, EdCompl, nil, nil, nil, nil, nil, CBUF);
    end;
  end;

end;

procedure TFmLotes1.CBUFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_RETURN then
    if CBUF.DroppedDown = False then
      SendMessage(FmLotes1.Handle, WM_NEXTDLGCTL, 0, 0);
end;

procedure TFmLotes1.BtConfirma4Click(Sender: TObject);
var
  Vence, Data_, Emiss_: TDateTime;
  Lote, i, z, Erros, DMais, Dias, Controle, Codigo, Numero, Banco, Agencia,
  CartDep: Integer;
  Valor, Desco, Bruto: Double;
  Duplic, DDeposito, DCompra, Emissao, Vencto, CPF, Sacado, Rua,
  CEP, Compl, Bairro, Cidade, UF, IE, DescAte: String;
begin
  DmLotes.QrDupsAtencao.Close;
  DmLotes.QrDupsAtencao.SQL.Clear;
  DmLotes.QrDupsAtencao.SQL.Add('SELECT oc.* ');
  DmLotes.QrDupsAtencao.SQL.Add('FROM ocorreu oc ');
  DmLotes.QrDupsAtencao.SQL.Add('LEFT JOIN ocorbank ob ON ob.Codigo=oc.Ocorrencia ');
  DmLotes.QrDupsAtencao.SQL.Add('LEFT JOIN lotesits it ON it.Controle=oc.LotesIts ');
  DmLotes.QrDupsAtencao.SQL.Add('WHERE ob.Atencao = 1 ');
  DmLotes.QrDupsAtencao.SQL.Add('AND it.CPF="' + Geral.SoNumero_TT(EdCNPJ.Text) + '"');
  DmLotes.QrDupsAtencao.Open;
  if DmLotes.QrDupsAtencao.RecordCount > 0 then
  begin
    PageControl7.ActivePageIndex := 2;
    if Geral.MB_Pergunta('Aten��o! Sacado com movimenta��o de ' +
      'duplicata(s) com al�nea(s) que exigem aten��o. Deseja aceitar assim mesmo?') <> ID_YES then
    begin
      Application.CreateForm(TFmDuplicatas2, FmDuplicatas2);
      FmDuplicatas2.EdCPF.Text := EdCNPJ.Text;
      FmDuplicatas2.EdCPFExit(Self);
      FmDuplicatas2.WindowState := wsMaximized;
      FmDuplicatas2.ShowModal;
      FmDuplicatas2.Destroy;
      Exit;
    end;
  end;
  Screen.Cursor := crHourGlass;
  try
    CalculaValoresDuplicataAlterado;
    Codigo  := QrLotesCodigo.Value;
    Erros   := 0;
    Bruto   := Geral.DMV(EdValor4.Text);
    Desco   := Geral.DMV(EdDesco4.Text);
    DescAte := Geral.FDT(dmkEdTPDescAte.Date, 1);
    Valor   := Bruto-Desco;
    Vence   := Geral.ValidaDataSimples(EdVence4.Text, True);
    Vencto  := FormatDateTime(VAR_FORMATDATE, Vence);
    Data_   := Geral.ValidaDataSimples(EdData4.Text, True);
    DCompra := FormatDateTime(VAR_FORMATDATE, Data_);
    Emiss_  := Geral.ValidaDataSimples(EdEmiss4.Text, True);
    Emissao := FormatDateTime(VAR_FORMATDATE, Emiss_);
    Duplic  := EdDuplicata.Text;
    CPF     := Geral.SoNumero_TT(EdCNPJ.Text);
    IE      := Geral.SoNumero_TT(EdIE.Text);
    Sacado  := EdSacado.Text;
    Rua     := EdRua.Text;
    Numero  := Geral.IMV(EdNumero.Text);
    Compl   := EdCompl.Text;
    Bairro  := EdBairro.Text;
    Cidade  := EdCidade.Text;
    CEP     := Geral.SoNumero_TT(EdCEP.Text);
    UF      := CBUF.Text;
    CartDep := Geral.IMV(EdCartDep4.Text);
    // Manter informa��es de antes de 01/04/2007 no LotesIts
    Banco   := Geral.IMV(DBEdBanco4.Text);
    Agencia := Geral.IMV(DBEdAgencia4.Text);
    // F I M   Manter informa��es
    //
    Geral.Valida_IE(EdIE.Text, Geral.GetCodigoUF_da_SiglaUF(CBUF.Text), '??');
    //
    z := 10;
    for i := 1 to z do
    begin
      case i of
        1: if Valor <= 0 then Erros := Erros +
          MLAGeral.MostraErroControle('Informe um valor v�lido!', EdValor4);
        2: if Bruto <= 0 then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o valor!', EdValor4);
        3: if Vence < Data_ then Erros := Erros +
          MLAGeral.MostraErroControle('Prazo inv�lido!', EdVence4);
        4: if Duplic = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe a duplicata!', EdDuplicata);
        5: if CPF = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o CNPJ!', EdCNPJ);
        6: if Sacado = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o Sacado!', EdSacado);
        7: if Rua = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o logradouro!', EdRua);
        8: if Cidade = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe a Cidade!', EdCidade);
        9: if CEP = '' then Erros := Erros +
          MLAGeral.MostraErroControle('Informe o CEP!', EdCEP);
       10: if CartDep = 0 then Erros := Erros +
          MLAGeral.MostraErroControle('Informe a carteira de recebimento!', EdCartDep4);
      end;
      if Erros > 0 then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    DMais := Geral.IMV(EdDMaisD.Text);
    Dias := Geral.IMV(EdDias4.Text);
    DDeposito := FormatDateTime(VAR_FORMATDATE, UMyMod.CalculaDataDeposito(Vence));
    //
    if LaTipo.Caption = CO_ALTERACAO then Controle := QrLotesItsControle.Value
    else Controle := 0;
    Controle := SQL_DU(Codigo, Controle, DMais, Dias, Banco, Agencia, Valor,
                Desco, Bruto, Duplic, CPF, Sacado, Vencto, DCompra, DDeposito,
                Emissao, DescAte, LaTipo.Caption, QrLotesCliente.Value, CartDep);
    //
    FControlIts := Controle;
    FmPrincipal.AtualizaSacado(CPF, Sacado, Rua, Numero, Compl, Bairro, Cidade, UF, CEP,
    Geral.SoNumeroESinal_TT(EdTel1.Text), IE, EdEmail.Text, -1);
    //CalculaLote;
    //LocCod(Codigo, Codigo);
    EdEmiss4.SetFocus;
    if LaTipo.Caption = CO_ALTERACAO then BtSair4Click(Self)
    else begin
      Lote := QrLotesCodigo.Value;
      CalculaLote(Lote, True);
      LocCod(Lote, Lote);
      if Lote <> QrLotesCodigo.Value then
      begin
        Geral.MB_Erro('Erro no refresh do lote. O aplicativo dever� ser encerrado!');
        Application.Terminate;
      end;
    end;
    Screen.Cursor := crDefault;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLotes1.EdTel1Exit(Sender: TObject);
begin
  EdTel1.Text := Geral.FormataTelefone_TT(Geral.SoNumero_TT(EdTel1.Text));
end;

procedure TFmLotes1.GradeDUDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  OldAlign, Color: Integer;
begin
  if GradeDU = nil then Exit;
  Color := clBlack;
  SetTextColor(GradeDU.Canvas.Handle, Color);
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(GradeDU.Canvas.Handle, TA_LEFT);
      GradeDU.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        GradeDU.Cells[Acol, ARow]);
      SetTextAlign(GradeDU.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(GradeDU.Canvas.Handle, TA_RIGHT);
      GradeDU.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Geral.TFT(GradeDU.Cells[Acol, ARow], 6, siPositivo));
      SetTextAlign(GradeDU.Canvas.Handle, OldAlign);
  end //else if ACol = 02 then begin
end;

procedure TFmLotes1.GradeDUKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  CalculaValoresDuplicataAlterado;
  if key=VK_RETURN then
  begin
    if GradeDU.EditorMode = True then
    begin
      if (GradeDU.Row < GradeDU.RowCount-1) then
        GradeDU.Row := GradeDU.Row +1
      else BtConfirma4.SetFocus;
    end;
  end
end;

procedure TFmLotes1.BtConfirma1Click(Sender: TObject);
begin
  if NaoPermiteCHs then Exit;
  IncluiRegistro(8, 0);
  EdCliente.Text := EdCodCli.Text;
  CBCliente.KeyValue := Geral.IMV(EdCodCli.Text);
  TPData.Date := TPCheque.Date;
  FIncluindoCh := True;
end;

procedure TFmLotes1.BtConfirma3Click(Sender: TObject);
begin
  if NaoPermiteDUs then Exit;
  FIncluindoDU := True;
  IncluiRegistro(8, 1);
  EdCliente.Text := EdCodCli.Text;
  CBCliente.KeyValue := Geral.IMV(EdCodCli.Text);
  TPData.Date := TPCheque.Date;
end;

procedure TFmLotes1.ImportaDuplicata(Codigo, Cliente: Integer);
var
  StatusSPC, Comp, DMais, Dias, CartDep, Controle: Integer;
  Valor, Desco, Bruto: Double;
  Duplic, DDeposito, DCompra, Emissao, Vencto, CPF, Sacado, Rua,
  IE: String;
begin
  Screen.Cursor := crHourGlass;
  try
    StatusSPC := Dmoc.QrLDUsStatusSPC.Value;
    Emissao   := FormatDateTime(VAR_FORMATDATE, Dmoc.QrLDUsDEmiss.Value);
    CartDep   := Geral.IMV(EdCartDep1.Text);
    Duplic    := Dmoc.QrLDUsDuplicata.Value;
    Valor     := Dmoc.QrLDUsValor.Value;
    Desco     := Dmoc.QrLDUsDesco.Value;
    Bruto     := Dmoc.QrLDUsBruto.Value;
    Vencto    := FormatDateTime(VAR_FORMATDATE, Dmoc.QrLDUsDVence.Value);
    CPF       := Geral.SoNumero_TT(Dmoc.QrLDUsCPF.Value);
    Sacado    := Dmoc.QrLDUsEmitente.Value;
    //
    DCompra   := FormatDateTime(VAR_FORMATDATE, TPCheque.Date);
    //
    Rua       := Dmoc.QrLDUsRua.Value;
    IE        := Dmoc.QrLDUsIE.Value;
    DMais     := Geral.IMV(EdDMaisX.Text);
    ////////////////////////////////////////////////////////////////////////////
    Comp      := 0; // S� para Cheques
    Dias := UMyMod.CalculaDias(int(TPData.Date), int(Dmoc.QrLDUsDVence.Value),
      DMais, Comp, Dmod.QrControleTipoPrazoDesc.Value, 0(*CBE*));
    ////////////////////////////////////////////////////////////////////////////
    CalculaValoresDuplicataImportando(Dias);
    DDeposito := FormatDateTime(VAR_FORMATDATE,
      UMyMod.CalculaDataDeposito(Dmoc.QrLDUsDVence.Value));
    Dmod.QrUpd.SQL.Clear;
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'LotesIts', 'LotesIts', 'Controle');
    Dmod.QrUpd.SQL.Add('INSERT INTO lotesits SET ');
    Dmod.QrUpd.SQL.Add('Duplicata=:P0, CPF=:P1, Emitente=:P2, Valor=:P3, ');
    Dmod.QrUpd.SQL.Add('Vencto=:P4, TxaCompra=:P5, VlrCompra=:P6, DMais=:P7, ');
    Dmod.QrUpd.SQL.Add('Dias=:P8, TxaJuros=:P9, DCompra=:P10, DDeposito=:P11, ');
    Dmod.QrUpd.SQL.Add('Emissao=:P12, Desco=:P13, Bruto=:P14, Banco=:P15, ');
    Dmod.QrUpd.SQL.Add('Agencia=:P16, Cliente=:P17, CartDep=:P18, ');
    Dmod.QrUpd.SQL.Add('StatusSPC=:P19 ');
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb');
    Dmod.QrUpd.Params[00].AsString  := Duplic;
    Dmod.QrUpd.Params[01].AsString  := CPF;
    Dmod.QrUpd.Params[02].AsString  := Sacado;
    Dmod.QrUpd.Params[03].AsFloat   := Valor;
    Dmod.QrUpd.Params[04].AsString  := Vencto;
    //
    Dmod.QrUpd.Params[05].AsFloat   := FTaxa[0];
    Dmod.QrUpd.Params[06].AsFloat   := FValr[0];
    Dmod.QrUpd.Params[07].AsInteger := DMais;
    Dmod.QrUpd.Params[08].AsInteger := Dias;
    Dmod.QrUpd.Params[09].AsFloat   := FJuro[0];
    Dmod.QrUpd.Params[10].AsString  := DCompra;
    Dmod.QrUpd.Params[11].AsString  := DDeposito;
    Dmod.QrUpd.Params[12].AsString  := Emissao;
    Dmod.QrUpd.Params[13].AsFloat   := Desco;
    Dmod.QrUpd.Params[14].AsFloat   := Bruto;
    Dmod.QrUpd.Params[15].AsInteger := Geral.IMV(EdBanco0.Text);
    Dmod.QrUpd.Params[16].AsInteger := Geral.IMV(EdAgencia0.Text);
    Dmod.QrUpd.Params[17].AsInteger := Cliente;
    Dmod.QrUpd.Params[18].AsInteger := CartDep;
    Dmod.QrUpd.Params[19].AsInteger := StatusSPC;
    //
    Dmod.QrUpd.Params[20].AsInteger := Codigo;
    Dmod.QrUpd.Params[21].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    DefineCompraEmpresas(Codigo, Controle);
    //
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmLotes1.TimerSCXTimer(Sender: TObject);
begin
  //TimerSCX.Enabled := False;
  //InsereImportLote
end;

procedure TFmLotes1.InsereImportLote;
var
  i: Integer;
  CPF, DEmiss, DCompra, DVence, Deposit, BAC: String;
begin
  Dmoc.QrLCHs.Close;
  Application.ProcessMessages;
  //
  FGradeFocused := 0;
  Screen.Cursor := crHourGlass;
  try
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('INSERT INTO importlote SET Sit=0, ');
    Dmod.QrUpdL.SQL.Add('Tipo        =:P00,');
    Dmod.QrUpdL.SQL.Add('Ordem       =:P01,');
    Dmod.QrUpdL.SQL.Add('DEmiss      =:P02,');
    Dmod.QrUpdL.SQL.Add('DCompra     =:P03,');
    Dmod.QrUpdL.SQL.Add('DVence      =:P04,');
    Dmod.QrUpdL.SQL.Add('Deposito    =:P05,');
    Dmod.QrUpdL.SQL.Add('Comp        =:P06,');
    Dmod.QrUpdL.SQL.Add('Banco       =:P07,');
    Dmod.QrUpdL.SQL.Add('Agencia     =:P08,');
    Dmod.QrUpdL.SQL.Add('Conta       =:P09,');
    Dmod.QrUpdL.SQL.Add('CPF         =:P10,');
    Dmod.QrUpdL.SQL.Add('Emitente    =:P11,');
    Dmod.QrUpdL.SQL.Add('Cheque      =:P12,');
    Dmod.QrUpdL.SQL.Add('Duplicata   =:P13,');
    Dmod.QrUpdL.SQL.Add('Valor       =:P14,');
    Dmod.QrUpdL.SQL.Add('Rua         =:P15,');
    Dmod.QrUpdL.SQL.Add('Numero      =:P16,');
    Dmod.QrUpdL.SQL.Add('Compl       =:P17,');
    Dmod.QrUpdL.SQL.Add('Birro       =:P18,');
    Dmod.QrUpdL.SQL.Add('Cidade      =:P19,');
    Dmod.QrUpdL.SQL.Add('UF          =:P20,');
    Dmod.QrUpdL.SQL.Add('CEP         =:P21,');
    Dmod.QrUpdL.SQL.Add('Tel1        =:P22,');
    Dmod.QrUpdL.SQL.Add('Aberto      =:P23,');
    Dmod.QrUpdL.SQL.Add('OcorrA      =:P24,');
    Dmod.QrUpdL.SQL.Add('OcorrT      =:P25,');
    Dmod.QrUpdL.SQL.Add('QtdeCH      =:P26,');
    Dmod.QrUpdL.SQL.Add('AberCH      =:P27,');
    Dmod.QrUpdL.SQL.Add('IE          =:P28,');
    Dmod.QrUpdL.SQL.Add('Desco       =:P29,');
    Dmod.QrUpdL.SQL.Add('Bruto       =:P30,');
    Dmod.QrUpdL.SQL.Add('Duplicado   =:P31,');
    Dmod.QrUpdL.SQL.Add('Praca       =:P32,');
    Dmod.QrUpdL.SQL.Add('BAC         =:P33,');
    Dmod.QrUpdL.SQL.Add('CPF_2       =:P34,');
    Dmod.QrUpdL.SQL.Add('Nome_2      =:P35,');
    Dmod.QrUpdL.SQL.Add('RISCOEM     =:P36,');
    Dmod.QrUpdL.SQL.Add('RISCOSA     =:P37,');
    Dmod.QrUpdL.SQL.Add('Rua_2       =:P38,');
    Dmod.QrUpdL.SQL.Add('Numero_2    =:P39,');
    Dmod.QrUpdL.SQL.Add('Compl_2     =:P40,');
    Dmod.QrUpdL.SQL.Add('IE_2        =:P41,');
    Dmod.QrUpdL.SQL.Add('Bairro_2    =:P42,');
    Dmod.QrUpdL.SQL.Add('Cidade_2    =:P43,');
    Dmod.QrUpdL.SQL.Add('UF_2        =:P44,');
    Dmod.QrUpdL.SQL.Add('CEP_2       =:P45,');
    Dmod.QrUpdL.SQL.Add('TEL1_2      =:P46,');
    Dmod.QrUpdL.SQL.Add('StatusSPC   =:P47 ');
    //
    ProgressLab3.Visible := True;
    ProgressBar3.Visible := True;
    ProgressBar3.Position := 0;
    ProgressBar3.Max := GradeC.RowCount-1;
    ProgressBar3.Refresh;
    ProgressBar3.Update;
    Application.ProcessMessages;
    if (GradeC.RowCount > 2) or (Geral.DMV(GradeC.Cells[06, 1]) > 0) then
    begin
      FGradeFocused := 1;
      for i := 1 to GradeC.RowCount -1 do
      begin
        ProgressBar3.Position := ProgressBar3.Position + 1;
        Update;
        Application.ProcessMessages;
        CPF := Geral.SoNumero_TT(GradeC.Cells[09, i]);
        //
        DmLotes.QrSAlin.Close;
        DmLotes.QrSAlin.Params[0].AsString := CPF;
        DmLotes.QrSAlin.Open;
        //
        DEmiss  := '0000-00-00';
        DCompra := FormatDateTime(VAR_FORMATDATE, TPCheque.Date);
        DVence  := FormatDateTime(VAR_FORMATDATE, Geral.ValidaDataSimples(GradeC.Cells[07, i], True));
        Deposit := '0000-00-00';
        //
        DmLotes.QrLocCH.Close;
        DmLotes.QrLocCH.Params[00].AsInteger := Geral.IMV(GradeC.Cells[02, i]);
        DmLotes.QrLocCH.Params[01].AsInteger := Geral.IMV(GradeC.Cells[03, i]);
        DmLotes.QrLocCH.Params[02].AsString  := GradeC.Cells[04, i];
        DmLotes.QrLocCH.Params[03].AsInteger := Geral.IMV(GradeC.Cells[05, i]);
        DmLotes.QrLocCH.Open;
        //
        Dmoc.QrLocEmiCPF.Close;
        Dmoc.QrLocEmiCPF.Params[0].AsString := CPF;
        Dmoc.QrLocEmiCPF.Open;
        //
        Dmoc.QrLocSacados.Close;
        Dmoc.QrLocSacados.Params[0].AsString := CPF;
        Dmoc.QrLocSacados.Open;
        //
        BAC := FormatFloat('000', Geral.IMV(GradeC.Cells[02, i])) +
               FormatFloat('0000', Geral.IMV(GradeC.Cells[03, i])) +
               GradeC.Cells[04, i];
        //
        Dmod.QrUpdL.Params[00].AsInteger := 0;
        Dmod.QrUpdL.Params[01].AsInteger := i;
        Dmod.QrUpdL.Params[02].AsString  := DEmiss;
        Dmod.QrUpdL.Params[03].AsString  := DCompra;
        Dmod.QrUpdL.Params[04].AsString  := DVence;
        Dmod.QrUpdL.Params[05].AsString  := Deposit;
        Dmod.QrUpdL.Params[06].AsInteger := FmPrincipal.VerificaDiasCompensacao(
          Geral.IMV(GradeC.Cells[01, i]), QrLotesCBECli.Value,
          Geral.DMV(GradeC.Cells[06, i]), TPCheque.Date,
          Geral.ValidaDataSimples(GradeC.Cells[07, i], True),
          QrLotesSCBCli.Value);
        Dmod.QrUpdL.Params[07].AsInteger := Geral.IMV(GradeC.Cells[02, i]);
        Dmod.QrUpdL.Params[08].AsInteger := Geral.IMV(GradeC.Cells[03, i]);
        Dmod.QrUpdL.Params[09].AsString  := GradeC.Cells[04, i];
        Dmod.QrUpdL.Params[10].AsString  := CPF;
        Dmod.QrUpdL.Params[11].AsString  := GradeC.Cells[10, i];
        Dmod.QrUpdL.Params[12].AsInteger := Geral.IMV( GradeC.Cells[05, i]);
        Dmod.QrUpdL.Params[13].AsString  := '';
        Dmod.QrUpdL.Params[14].AsFloat   := Geral.DMV(GradeC.Cells[06, i]);
        Dmod.QrUpdL.Params[15].AsString  := '';
        Dmod.QrUpdL.Params[16].AsInteger := 0;
        Dmod.QrUpdL.Params[17].AsString  := '';
        Dmod.QrUpdL.Params[18].AsString  := '';
        Dmod.QrUpdL.Params[19].AsString  := '';
        Dmod.QrUpdL.Params[20].AsString  := '';
        Dmod.QrUpdL.Params[21].AsString  := '';
        Dmod.QrUpdL.Params[22].AsString  := '';
        Dmod.QrUpdL.Params[23].AsFloat   := RiscoSacado(CPF);
        Dmod.QrUpdL.Params[24].AsFloat   := DmLotes.QrSAlinAberto.Value;
        Dmod.QrUpdL.Params[25].AsFloat   := DmLotes.QrSAlinValor.Value;
        Dmod.QrUpdL.Params[26].AsInteger := DmLotes.QrSAlinCheques.Value;
        Dmod.QrUpdL.Params[27].AsInteger := 0;//QrSCHOpenCheques.Value;
        Dmod.QrUpdL.Params[28].AsString  := '';
        Dmod.QrUpdL.Params[29].AsFloat   := 0;
        Dmod.QrUpdL.Params[30].AsFloat   := 0;
        Dmod.QrUpdL.Params[31].AsInteger := DmLotes.QrLocCHCheques.Value;
        Dmod.QrUpdL.Params[32].AsInteger := Geral.IMV(GradeC.Cells[01, i]);
        Dmod.QrUpdL.Params[33].AsString  := BAC;
        Dmod.QrUpdL.Params[34].AsString  := Dmoc.QrLocEmiCPFCPF.Value;
        Dmod.QrUpdL.Params[35].AsString  := Dmoc.QrLocEmiCPFNome.Value;
        Dmod.QrUpdL.Params[36].AsFloat   := Dmoc.QrLocEmiCPFLimite.Value;
        Dmod.QrUpdL.Params[37].AsFloat   := Dmoc.QrLocSacadosRisco.Value;
        Dmod.QrUpdL.Params[38].AsString  := '';
        Dmod.QrUpdL.Params[39].AsInteger := 0;
        Dmod.QrUpdL.Params[40].AsString  := '';
        Dmod.QrUpdL.Params[41].AsString  := '';
        Dmod.QrUpdL.Params[42].AsString  := '';
        Dmod.QrUpdL.Params[43].AsString  := '';
        Dmod.QrUpdL.Params[44].AsString  := '';
        Dmod.QrUpdL.Params[45].AsInteger := 0;
        Dmod.QrUpdL.Params[46].AsString  := '';
        Dmod.QrUpdL.Params[47].AsInteger := -1;
        Dmod.QrUpdL.ExecSQL;
      end;
    end;
    ProgressBar3.Position := 0;
    ProgressBar3.Max := GradeD.RowCount-1;
    ProgressBar3.Refresh;
    ProgressBar3.Update;
    Application.ProcessMessages;
    if (GradeD.RowCount > 2) or (Geral.DMV(GradeD.Cells[03, 1]) > 0) then
    begin
      FGradeFocused := FGradeFocused + 2;
      for i := 1 to GradeD.RowCount -1 do
      begin
        ProgressBar3.Position := ProgressBar3.Position + 1;
        Update;
        Application.ProcessMessages;
        CPF := Geral.SoNumero_TT(GradeD.Cells[07, i]);
        DmLotes.QrSDUOpen.Close;
        DmLotes.QrSDUOpen.Params[0].AsString  := CPF;
        DmLotes.QrSDUOpen.Open;
        //
        DEmiss  := FormatDateTime(VAR_FORMATDATE, Geral.ValidaDataSimples(GradeD.Cells[01, i], True));
        DCompra := FormatDateTime(VAR_FORMATDATE, TPCheque.Date);
        DVence  := FormatDateTime(VAR_FORMATDATE, Geral.ValidaDataSimples(GradeD.Cells[06, i], True));
        Deposit := '0000-00-00';
        //
        DmLotes.QrLocDU.Close;
        DmLotes.QrLocDU.Params[00].AsString  := CPF;
        DmLotes.QrLocDU.Params[01].AsString  := GradeD.Cells[02, i];
        DmLotes.QrLocDU.Open;
        //
        Dmod.QrUpdL.Params[00].AsInteger := 1;
        Dmod.QrUpdL.Params[01].AsInteger := i;
        Dmod.QrUpdL.Params[02].AsString  := DEmiss;
        Dmod.QrUpdL.Params[03].AsString  := DCompra;
        Dmod.QrUpdL.Params[04].AsString  := DVence;
        Dmod.QrUpdL.Params[05].AsString  := Deposit;
        Dmod.QrUpdL.Params[06].AsInteger := 0;//Geral.IMV(GradeD.Cells[01, i]);
        Dmod.QrUpdL.Params[07].AsInteger := 0;//Geral.IMV(GradeD.Cells[02, i]);
        Dmod.QrUpdL.Params[08].AsInteger := 0;//Geral.IMV(GradeD.Cells[03, i]);
        Dmod.QrUpdL.Params[09].AsString  := '';//GradeD.Cells[04, i];
        Dmod.QrUpdL.Params[10].AsString  := CPF;
        Dmod.QrUpdL.Params[11].AsString  := GradeD.Cells[08, i];
        Dmod.QrUpdL.Params[12].AsInteger := 0;//Geral.IMV( GradeD.Cells[05, i]);
        Dmod.QrUpdL.Params[13].AsString  := GradeD.Cells[02, i];
        Dmod.QrUpdL.Params[14].AsFloat   := Geral.DMV(GradeD.Cells[03, i]);
        Dmod.QrUpdL.Params[15].AsString  := GradeD.Cells[09, i];
        Dmod.QrUpdL.Params[16].AsInteger := Geral.IMV(GradeD.Cells[10, i]);
        Dmod.QrUpdL.Params[17].AsString  := GradeD.Cells[11, i]; //Compl
        Dmod.QrUpdL.Params[18].AsString  := GradeD.Cells[12, i];
        Dmod.QrUpdL.Params[19].AsString  := GradeD.Cells[13, i];
        Dmod.QrUpdL.Params[20].AsString  := GradeD.Cells[15, i];
        Dmod.QrUpdL.Params[21].AsString  := Geral.SoNumero_TT(GradeD.Cells[14, i]);
        Dmod.QrUpdL.Params[22].AsString  := GradeD.Cells[16, i]; //Telefone
        Dmod.QrUpdL.Params[23].AsFloat   := DmLotes.QrSDUOpenValor.Value;
        Dmod.QrUpdL.Params[24].AsFloat   := 0;
        Dmod.QrUpdL.Params[25].AsFloat   := 0;
        Dmod.QrUpdL.Params[26].AsInteger := 0;
        Dmod.QrUpdL.Params[27].AsInteger := 0;
        Dmod.QrUpdL.Params[28].AsString  := GradeD.Cells[17, i];
        Dmod.QrUpdL.Params[29].AsFloat   := Geral.DMV(GradeD.Cells[04, i]);
        Dmod.QrUpdL.Params[30].AsFloat   := Geral.DMV(GradeD.Cells[05, i]);
        Dmod.QrUpdL.Params[31].AsInteger := DmLotes.QrLocDUDuplicatas.Value;
        Dmod.QrUpdL.Params[32].AsInteger := 0;
        Dmod.QrUpdL.Params[33].AsString  := '';

        Dmod.QrUpdL.Params[34].AsString  := Dmoc.QrLocSacadosCNPJ.Value;
        Dmod.QrUpdL.Params[35].AsString  := Dmoc.QrLocSacadosNome.Value;
        Dmod.QrUpdL.Params[36].AsFloat   := Dmoc.QrLocEmiCPFLimite.Value;
        Dmod.QrUpdL.Params[37].AsFloat   := Dmoc.QrLocSacadosRisco.Value;

        Dmod.QrUpdL.Params[38].AsString  := Dmoc.QrLocSacadosRua.Value;
        Dmod.QrUpdL.Params[39].AsInteger := Dmoc.QrLocSacadosNumero.Value;
        Dmod.QrUpdL.Params[40].AsString  := Dmoc.QrLocSacadosCompl.Value;
        Dmod.QrUpdL.Params[41].AsString  := Dmoc.QrLocSacadosIE.Value;
        Dmod.QrUpdL.Params[42].AsString  := Dmoc.QrLocSacadosBairro.Value;
        Dmod.QrUpdL.Params[43].AsString  := Dmoc.QrLocSacadosCidade.Value;
        Dmod.QrUpdL.Params[44].AsString  := Dmoc.QrLocSacadosUF.Value;
        Dmod.QrUpdL.Params[45].AsInteger := Dmoc.QrLocSacadosCEP.Value;
        Dmod.QrUpdL.Params[46].AsString  := Dmoc.QrLocSacadosTel1.Value;
        Dmod.QrUpdL.Params[47].AsInteger := -1;
        Dmod.QrUpdL.ExecSQL;
      end;
    end;
    AtualizaAcumulado;
    Dmoc.ReopenImportLote(0, 0);
    if FGradeFocused = 2 then
      Pagina.ActivePageIndex := 1 else Pagina.ActivePageIndex := 0;
    Screen.Cursor := crDefault;
  except
    raise;
    Screen.Cursor := crDefault;
  end;
  ProgressLab3.Visible := False;
  ProgressBar3.Visible := False;
end;

procedure TFmLotes1.GradeC_LDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Cor1, Cor2: TColor;
  OldAlign: Integer;
  Fon: Boolean;
begin
  if (Column.FieldName = 'Disponiv') then
  begin
    if Dmoc.QrLCHsDisponiv.Value < 0 then Cor1 := clRed
    else if Dmoc.QrLCHsDisponiv.Value < Dmoc.QrLCHsValor.Value then Cor1 := $000080FF // Laranja intenso
    else Cor1 := clGreen;
  end else if (Column.FieldName = 'OcorrT')
  or (Column.FieldName = 'OcorrA') 
  or (Column.FieldName = 'QtdeCH') then
  begin
    if Dmoc.QrLCHsOcorrA.Value > 0.009 then Cor1 := clRed
    else if Dmoc.QrLCHsQtdeCH.Value > 0 then Cor1 := $000080FF // Laranja intenso
    else Cor1 := clGreen;
  end else begin
    case Dmoc.QrLCHsMEU_SIT.Value of
      0: Cor1 := clBlue;
      1: Cor1 := $0023AAEF; // Laranja
      2: Cor1 := clGreen;
      else Cor1 := clFuchsia;
    end;
  end;
  if Dmoc.QrLCHsValor.Value >= Dmod.QrControleCHValAlto.Value then Cor2 := clPurple else
  if Dmoc.QrLCHsOcorrA.Value > 0 then Cor2 := clRed else
  if Dmoc.QrLCHsOcorrT.Value > 0 then
  Cor2 := clFuchsia else
  if Dmoc.QrLCHsAberto.Value > 0 then Cor2 := $0023AAEF // Laranja
  else Cor2 := clGreen;
  //
  //Fon := False;
  if Cor1 = clBlack then Fon := False else Fon := True;
  //
  if (Column.FieldName = 'Valor')
  then begin
    with GradeC_L.Canvas do
    begin
      Font.Color := Cor2;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_RIGHT);
      TextOut(Rect.Right-2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'Ordem')
  //or (Column.FieldName = 'Comp')
  //or (Column.FieldName = 'Banco')
  //or (Column.FieldName = 'Agencia')
  //or (Column.FieldName = 'Cheque')
  or (Column.FieldName = 'AberCH')
  or (Column.FieldName = 'Risco')
  or (Column.FieldName = 'Aberto')
  or (Column.FieldName = 'Disponiv')
  or (Column.FieldName = 'DIAS')
  or (Column.FieldName = 'OcorrT')
  or (Column.FieldName = 'OcorrA')
  or (Column.FieldName = 'QtdeCH')
  then begin
    with GradeC_L.Canvas do
    begin
      Font.Color := Cor1;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_RIGHT);
      TextOut(Rect.Right-2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'Conta')
  or (Column.FieldName = 'DVence')
  then begin
    with GradeC_L.Canvas do
    begin
      Font.Color := Cor1;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_CENTER);
      TextOut((Rect.Left+Rect.Right) div 2,Rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'CPF_TXT')
  or (Column.FieldName = 'Emitente')
  then begin
    with GradeC_L.Canvas do
    begin
      Font.Color := Cor1;
      if Fon then Font.Style := [fsBold] else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_LEFT);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'NOMEDUPLICADO')
  then begin
    with GradeC_L.Canvas do
    begin
      if Dmoc.QrLCHsDuplicado.Value = 0 then Font.Color := clGreen
      else Font.Color := clRed;
      if Fon then Font.Style := [fsBold] else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_CENTER);
      TextOut((Rect.Left+Rect.Right) div 2,Rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'TEXTO_SPC')
  then begin
    with GradeC_L.Canvas do
    begin
      case Dmoc.QrLCHsStatusSPC.Value of
        -1: Font.Color := clGray;
         0: Font.Color := clGreen;
         1: Font.Color := clBlue;
         2: Font.Color := clRed;
        else Font.Color := clFuchsia;
      end;
      Font.Style := [fsBold];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_LEFT);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end;
end;

procedure TFmLotes1.TPChequeChange(Sender: TObject);
begin
  Dmoc.ReopenImportLote(Dmoc.QrLCHsOrdem.Value, Dmoc.QrLDUsOrdem.Value);
end;

procedure TFmLotes1.BtEntidadesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCadastraCH, BtEntidades);
  LocalizaProximoChNaoVerde;
end;

procedure TFmLotes1.ExcluiImportLote(Tipo, Ordem: Integer);
var
  CH, DU: Integer;
begin
  if Geral.MB_Pergunta('Confirma a exclus�o da seq. n� ' + Geral.FF0(Ordem) + '?') = ID_YES
  then begin
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM importlote WHERE Tipo=:P0 AND Ordem=:P1');
    Dmod.QrUpdL.Params[00].AsInteger := Tipo;
    Dmod.QrUpdL.Params[01].AsInteger := Ordem;
    Dmod.QrUpdL.ExecSQL;
    //
    if Tipo = 0 then
    begin
      CH := Ordem;
      DU := Dmoc.QrLDUsOrdem.Value;
    end else begin
      DU := Ordem;
      CH := Dmoc.QrLCHsOrdem.Value;
    end;
    Dmoc.ReopenImportLote(CH, DU);
  end;
end;

procedure TFmLotes1.IgnoraImportLote(Tipo, Ordem: Integer);
var
  CH, DU: Integer;
begin
  if Geral.MB_Pergunta('Deseja realmente ignorar as diferen�as da' +
    ' seq. n� '+ Geral.FF0(Ordem)+'?') = ID_YES
  then begin
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('UPDATE importlote SET Sit=2 WHERE Tipo=:P0 AND Ordem=:P1');
    Dmod.QrUpdL.Params[00].AsInteger := Tipo;
    Dmod.QrUpdL.Params[01].AsInteger := Ordem;
    Dmod.QrUpdL.ExecSQL;
    //
    if Tipo = 0 then
    begin
      CH := Ordem;
      DU := Dmoc.QrLDUsOrdem.Value;
    end else begin
      DU := Ordem;
      CH := Dmoc.QrLCHsOrdem.Value;
    end;
    Dmoc.ReopenImportLote(CH, DU);
  end;
end;

procedure TFmLotes1.BtCuidadoClick(Sender: TObject);
begin
  IgnoraImportLote(Dmoc.QrLCHsTipo.Value, Dmoc.QrLCHsOrdem.Value);
  LocalizaProximoChNaoVerde;
end;

function TFmLotes1.NaoPermiteCHs: Boolean;
var
  Impede, Duplic: Integer;
begin
  if FmPrincipal.FTipoImport = 4 then
  begin
    if Geral.MB_Pergunta('Deseja confirmar sem cadastrar os CPFs ' +
      'e/ou os nomes dos emitentes n�o informados?') = ID_YES then
    begin
      Result := False;
      Exit;
    end;
  end;
  Screen.Cursor := crHourGlass;
  try
    Impede := 0;
    Duplic := 0;
    Dmoc.QrLCHs.DisableControls;
    Dmoc.QrLCHs.First;
    while not Dmoc.QrLCHs.Eof do
    begin
      if Dmoc.QrLCHsMEU_SIT.Value <> 2 then Impede := Impede + 1;
      if Dmoc.QrLCHsDuplicado.Value <> 0 then Duplic := Duplic + 1;
      Dmoc.QrLCHs.Next;
    end;
    if Impede <> 0 then
    begin
      Geral.MB_Aviso('H� ' + Geral.FF0(Impede) + ' itens impedindo ' +
        'a inclus�o destes cheques!');
      Result := True;
    end else if Duplic > 0 then begin
      if Geral.MB_Pergunta('H� ' + Geral.FF0(Duplic) + ' Cheques deste ' +
        'lote que j� est�o digitados! Deseja continuar assim mesmo?') <> ID_YES
      then
        Result := True else
      Result := False;
    end else Result := False;
    Dmoc.QrLCHs.EnableControls;
    Screen.Cursor := crDefault;
  except
    Dmoc.QrLCHs.EnableControls;
    Screen.Cursor := crDefault;
    raise;
  end;
end;

function TFmLotes1.NaoPermiteDUs: Boolean;
var
  Impede, Duplic: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Impede := 0;
    Duplic := 0;
    Dmoc.QrLDUs.DisableControls;
    Dmoc.QrLDUs.First;
    while not Dmoc.QrLDUs.Eof do
    begin
      if Dmoc.QrLDUsMEU_SIT.Value <> 2 then Impede := Impede + 1;
      if Dmoc.QrLDUsDuplicado.Value <> 0 then Duplic := Duplic + 1;
      Dmoc.QrLDUs.Next;
    end;
    if Impede <> 0 then
    begin
      Geral.MB_Aviso('H� ' + Geral.FF0(Impede) + ' itens impedindo ' +
        'a inclus�o destas duplicatas!');
      Result := True;
    end else if Duplic > 0 then
    begin
      if Geral.MB_Pergunta('H� ' + Geral.FF0(Duplic) + ' duplicatas deste ' +
        'lote que j� est�o digitadas! Deseja continuar assim mesmo?') <> ID_YES
      then
        Result := True
      else
        Result := False;
    end else Result := False;
    Dmoc.QrLDUs.EnableControls;
    Screen.Cursor := crDefault;
  except
    Dmoc.QrLDUs.EnableControls;
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmLotes1.NFSe1Click(Sender: TObject);
var
  NumNF, Codigo: Integer;
  SerieNF: String;
  TotNF: Double;
begin
  TotNF := QrLotesMINAV.Value;
  //
  UnNFSe_PF_0201.MostraFormNFSe(stIns, VAR_FilialUnica, QrLotesCliente.Value, 0,
    0, 0, '', True, 0, 0, fgnLoteRPS, nil, TotNF, SerieNF, NumNF, nil);
  //
  if (SerieNF <> '') and (NumNF <> 0) then
  begin
    Codigo := QrLotesCodigo.Value;
    //
    if not UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'lotes', False,
      ['SerNF', 'NF'], ['Codigo'], [SerieNF, NumNF], [Codigo], True)
    then
      Geral.MB_Info('Falha ao gerar NFS-e!')
    else
      LocCod(Codigo, Codigo);
  end;
end;

procedure TFmLotes1.GradeDevDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Cor: TColor;
  OldAlign: Integer;
  Fon: Boolean;
begin
  case DmLotes.QrAlinItsStatus.Value of
    0: Cor := clRed;
    1: Cor := $0023AAEF; // Laranja
    2: Cor := clNavy;
    else
    Cor := clFuchsia;
  end;
  //
  Fon := False;
  //
  if (Column.FieldName = 'Alinea1')
  or (Column.FieldName = 'Alinea2')
  or (Column.FieldName = 'Banco')
  or (Column.FieldName = 'Agencia')
  or (Column.FieldName = 'Cheque')
  or (Column.FieldName = 'Valor')
  or (Column.FieldName = 'Taxas')
  or (Column.FieldName = 'ValPago')
  then begin
    with GradeDev.Canvas do
    begin
      Font.Color := Cor;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_RIGHT);
      TextOut(Rect.Right-2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'Conta')
  or (Column.FieldName = 'DATA1_TXT')
  or (Column.FieldName = 'DATA2_TXT')
  or (Column.FieldName = 'DATA3_TXT')
  then begin
    with GradeDev.Canvas do
    begin
      Font.Color := Cor;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_CENTER);
      TextOut((Rect.Left+Rect.Right) div 2,Rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'NOMECLIENTE')
  or (Column.FieldName = 'CPF_TXT')
  or (Column.FieldName = 'Emitente')
  then begin
    with GradeDev.Canvas do
    begin
      Font.Color := Cor;
      if Fon then Font.Style := [fsBold] else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_LEFT);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end;
end;

procedure TFmLotes1.GradeD_LDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
var
  Cor1, Cor2: TColor;
  OldAlign: Integer;
  Fon: Boolean;
begin
  if (Column.FieldName = 'Disponiv') then
  begin
    if Dmoc.QrLDUsDisponiv.Value < 0 then Cor1 := clRed
    //else if (Dmoc.QrLDUsDISPONIVEL.Value - Dmoc.QrLDUsValor.Value) < 0 then Cor1 := $000080FF // Laranja intenso
    else if Dmoc.QrLDUsDisponiv.Value < Dmoc.QrLDUsValor.Value then Cor1 := $000080FF // Laranja intenso
    else Cor1 := clGreen;
  end else if (Column.FieldName = 'OcorrT')
  or (Column.FieldName = 'OcorrA')
  or (Column.FieldName = 'QtdeCH') then
  begin
    if Dmoc.QrLDUsOcorrA.Value > 0.009 then Cor1 := clRed
    else if Dmoc.QrLDUsQtdeCH.Value > 0 then Cor1 := $000080FF // Laranja intenso
    else Cor1 := clGreen;
  end else begin
    case Dmoc.QrLDUsMEU_SIT.Value of
      0: Cor1 := clBlue;
      1: Cor1 := $0023AAEF; // Laranja
      2: Cor1 := clGreen;
      else
       Cor1 := clFuchsia;
    end;
  end;
  if Dmoc.QrLDUsValor.Value >= Dmod.QrControleCHValAlto.Value then Cor2 := clPurple else
  if Dmoc.QrLDUsOcorrA.Value > 0 then Cor2 := clRed else
  if Dmoc.QrLDUsOcorrT.Value > 0 then
  Cor2 := clFuchsia else
  if Dmoc.QrLDUsAberto.Value > 0 then Cor2 := $0023AAEF // Laranja
  else Cor2 := clGreen;
  //
  //Fon := False;
  if Cor1 = clBlack then Fon := False else Fon := True;
  //
  if (Column.FieldName = 'Valor')
  or (Column.FieldName = 'Desco')
  or (Column.FieldName = 'Bruto')
  then begin
    with GradeD_L.Canvas do
    begin
      Font.Color := Cor2;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_RIGHT);
      TextOut(Rect.Right-2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'Ordem')
  //or (Column.FieldName = 'Comp')
  //or (Column.FieldName = 'Banco')
  //or (Column.FieldName = 'Agencia')
  //or (Column.FieldName = 'Cheque')
  or (Column.FieldName = 'AberCH')
  or (Column.FieldName = 'Risco')
  or (Column.FieldName = 'Aberto')
  or (Column.FieldName = 'Disponiv')
  or (Column.FieldName = 'DIAS')
  or (Column.FieldName = 'OcorrT')
  or (Column.FieldName = 'OcorrA')
  or (Column.FieldName = 'QtdeCH')
  then begin
    with GradeD_L.Canvas do
    begin
      Font.Color := Cor1;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_RIGHT);
      TextOut(Rect.Right-2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'Conta')
  or (Column.FieldName = 'DVence')
  then begin
    with GradeD_L.Canvas do
    begin
      Font.Color := Cor1;
      if Fon then Font.Style := [fsBold]
      else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_CENTER);
      TextOut((Rect.Left+Rect.Right) div 2,Rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'CPF_TXT')
  or (Column.FieldName = 'Emitente')
  or (Column.FieldName = 'IE')
  then begin
    with GradeD_L.Canvas do
    begin
      Font.Color := Cor1;
      if Fon then Font.Style := [fsBold] else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_LEFT);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'NOMEDUPLICADO')
  then begin
    with GradeD_L.Canvas do
    begin
      if Dmoc.QrLDUsDuplicado.Value = 0 then Font.Color := clGreen
      else Font.Color := clRed;
      if Fon then Font.Style := [fsBold] else Font.Style := [];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_CENTER);
      TextOut((Rect.Left+Rect.Right) div 2,Rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end else if (Column.FieldName = 'TEXTO_SPC')
  then begin
    with GradeD_L.Canvas do
    begin
      case Dmoc.QrLDUsStatusSPC.Value of
        -1: Font.Color := clGray;
         0: Font.Color := clGreen;
         1: Font.Color := clBlue;
         2: Font.Color := clRed;
        else Font.Color := clFuchsia;
      end;
      Font.Style := [fsBold];
      FillRect(Rect);
      OldAlign := SetTextAlign(Handle, TA_LEFT);
      TextOut(Rect.Left+2,rect.Top+2,Column.Field.DisplayText);
      SetTextAlign(Handle, OldAlign);
    end;
  end;
end;

procedure TFmLotes1.BtEntidades2Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCadastraDU, BtEntidades2);
  LocalizaProximoDUNaoVerde;
end;

procedure TFmLotes1.BtCuidado2Click(Sender: TObject);
begin
  IgnoraImportLote(Dmoc.QrLDUsTipo.Value, Dmoc.QrLDUsOrdem.Value);
  LocalizaProximoDUNaoVerde;
end;

procedure TFmLotes1.BtEntiTroca2Click(Sender: TObject);
begin
  FmPrincipal.AtualizaSacado(Dmoc.QrLDUsCPF.Value,
                             Dmoc.QrLDUsEmitente.Value,
                             Dmoc.QrLDUsRua.Value,
                             Dmoc.QrLDUsNumero.Value,
                             Dmoc.QrLDUsCompl.Value,
                             Dmoc.QrLDUsBirro.Value,
                             Dmoc.QrLDUsCidade.Value,
                             Dmoc.QrLDUsUF.Value,
                             Geral.SoNumero_TT(Geral.FormataCEP_NT(Dmoc.QrLDUsCEP.Value)),
                             Geral.SoNumeroESinal_TT(Dmoc.QrLDUsTel1.Value),
                             Dmoc.QrLDUsIE.Value, '', -1);
  Dmoc.ReopenImportLote(Dmoc.QrLCHsOrdem.Value, Dmoc.QrLDUsOrdem.Value);
  LocalizaProximoDUNaoVerde;
end;

procedure TFmLotes1.BtDesiste5Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    CalculaLote(QrLotesCodigo.Value, True);
    LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value);
    MostraEdicao(0, CO_TRAVADO, 0);
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmLotes1.BtConfirma5Click(Sender: TObject);
var
  TaxaCod, Controle: Integer;
  TaxaTxa, TaxaVal, TaxaQtd: Double;
begin
  TaxaCod := Geral.IMV(EdTaxaCod5.Text);
  if TaxaCod < 1 then
  begin
    Geral.MB_Aviso('Informe o tipo de taxa a ser utilizada!');
    Exit;
  end;
  TaxaTxa := Geral.DMV(EdTaxaTxa5.Text);
  if TaxaTxa <= 0 then
  begin
    Geral.MB_Aviso('Informe o valor da taxa a ser utilizada!');
    Exit;
  end;
  TaxaQtd := Geral.DMV(EdTaxaQtd5.Text);
  if RGForma.ItemIndex = 0 then
    TaxaVal := TaxaTxa * TaxaQtd
  else
    TaxaVal := 0;
  Screen.Cursor := crHourGlass;
  try
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('');
    if LaTipo.Caption = CO_INCLUSAO then
    begin
      Dmod.QrUpd.SQL.Add('INSERT INTO lotestxs SET AlterWeb=1, ');
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'LotesTxs', 'LotesTxs', 'Controle');
    end else begin
      Dmod.QrUpd.SQL.Add('UPDATE lotestxs SET AlterWeb=1, ');
      Controle := QrLotesTxsControle.Value;
    end;
    Dmod.QrUpd.SQL.Add('TaxaCod=:P0, TaxaTxa=:P1, TaxaVal=:P2, Forma=:P3, ');
    Dmod.QrUpd.SQL.Add('TaxaQtd=:P4, ');
    if LaTipo.Caption = CO_INCLUSAO then
      Dmod.QrUpd.SQL.Add('UserCad=:Pa, DataCad=:Pb, Codigo=:Pc, Controle=:Pd')
    else
      Dmod.QrUpd.SQL.Add('UserAlt=:Pa, DataAlt=:Pb WHERE Codigo=:Pc AND Controle=:Pd');
    Dmod.QrUpd.Params[00].AsInteger := TaxaCod;
    Dmod.QrUpd.Params[01].AsFloat   := TaxaTxa;
    Dmod.QrUpd.Params[02].AsFloat   := TaxaVal;
    Dmod.QrUpd.Params[03].AsInteger := RGForma.ItemIndex;
    Dmod.QrUpd.Params[04].AsFloat   := Geral.DMV(EdTaxaQtd5.Text);
    //
    Dmod.QrUpd.Params[05].AsInteger := VAR_USUARIO;
    Dmod.QrUpd.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpd.Params[07].AsInteger := QrLotesCodigo.Value;
    Dmod.QrUpd.Params[08].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    FControlTxs := Controle; 
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmLotes1.EdTaxaCod5Change(Sender: TObject);
begin
  if EdTaxaCod5.Focused or CBTaxaCod5.Focused then
  begin
    EdTaxaTxa5.Text := Geral.FFT(DmLotes.QrTaxasValor.Value, 6, siPositivo);
    RGForma.ItemIndex := DmLotes.QrTaxasForma.Value;
  end;
end;

procedure TFmLotes1.BtInclui5Click(Sender: TObject);
begin
  FControlTxs := QrLotesTxsControle.Value;
  MostraEdicao(7, CO_INCLUSAO, 0);
end;

procedure TFmLotes1.BtAltera5Click(Sender: TObject);
begin
  FControlTxs := QrLotesTxsControle.Value;
  MostraEdicao(7, CO_ALTERACAO, 0);
end;

procedure TFmLotes1.QrLotesTxsCalcFields(DataSet: TDataSet);
begin
  QrLotesTxsTAXA_STR.Value := MLAGeral.FormataTaxa(
    QrLotesTxsForma.Value, Dmod.QrControleMoeda.Value);
  //
  if QrLotesTxsTaxaQtd.Value > 0 then
    QrLotesTxsCALCQtd.Value := QrLotesTxsTaxaQtd.Value else
    if QrLotesTxsSysAQtd.Value > 0 then
      QrLotesTxsCALCQtd.Value := QrLotesTxsSysAQtd.Value else
      if QrLotesTxsTaxaTxa.Value > 0 then
        QrLotesTxsCALCQtd.Value := 1 else QrLotesTxsCALCQtd.Value := 0;
  //
  QrLotesTxsTaxaValNEG.Value := - QrLotesTxsTaxaVal.Value;      
end;

procedure TFmLotes1.BtExclui5Click(Sender: TObject);
begin
  FControlTxs := QrLotesTxsControle.Value;
  if Geral.MB_Pergunta('Confirma a exclus�o da taxa selecionada?') = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM lotestxs WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrLotesTxsControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrLotesTxs.Next;
    FControlTxs := QrLotesTxsControle.Value;
    CalculaLote(QrLotesCodigo.Value, True);
    LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value);
  end;
end;

procedure TFmLotes1.QrLotesTxsAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrLotes.State <> dsInactive) and (QrLotes.RecordCount > 0);
  Enab2 := (QrLotesTxs.State <> dsInactive) and (QrLotesTxs.RecordCount > 0);
  //
  BtInclui5.Enabled := Enab;
  BtAltera5.Enabled := Enab and Enab2;
  BtExclui5.Enabled := Enab and Enab2;
end;

procedure TFmLotes1.QrLotesTxsAfterScroll(DataSet: TDataSet);
begin
  if QrLotesTxs.RecordCount = 0 then
  begin
    BtAltera5.Enabled := False;
    BtExclui5.Enabled := False;
  end else begin
    BtAltera5.Enabled := True;
    BtExclui5.Enabled := True;
  end;
end;

procedure TFmLotes1.QrLotesTxsBeforeClose(DataSet: TDataSet);
begin
  BtInclui5.Enabled := False;
  BtAltera5.Enabled := False;
  BtExclui5.Enabled := False;
end;

procedure TFmLotes1.RGFormaClick(Sender: TObject);
begin
  Label77.Caption := RGForma.Items[RGForma.ItemIndex]+': ';
  Label78.Caption := 'Total '+RGForma.Items[RGForma.ItemIndex]+': ';
end;

procedure TFmLotes1.BtAltera2Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAltera2, BtAltera2);
end;

procedure TFmLotes1.QrLotesItsAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  Enab := (QrLotes.State <> dsInactive) and (QrLotes.RecordCount > 0);
  Enab2 := (QrLotesIts.State <> dsInactive) and (QrLotesIts.RecordCount > 0);
  //
  BtInclui.Enabled     := Enab;
  BtAltera2.Enabled    := Enab and Enab2;
  BtExclui2.Enabled    := Enab and Enab2;
  BitBtn7.Enabled      := Enab and Enab2;
  BtSPC_LI.Enabled     := Enab and Enab2;
  BtImportaBCO.Enabled := Enab;
  BtWeb.Enabled        := Enab;
end;

procedure TFmLotes1.LancaTaxasNovoLote(Cliente, TipoLote, NumeroDoLote: Integer);
var
  Controle: Integer;
begin
  TipoLote := TipoLote+1;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO lotestxs SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('TaxaCod=:P0, TaxaTxa=:P1, TaxaVal=:P2, Forma=:P3, ');
  Dmod.QrUpd.SQL.Add('UserCad=:Pa, DataCad=:Pb, Codigo=:Pc, Controle=:Pd');
  //
  if Dmod.QrControleTaxasAutom.Value = 0 then
  begin
    DmLotes.QrNovoTxs.Close;
    DmLotes.QrNovoTxs.Open;
    while not DmLotes.QrNovoTxs.Eof do
    begin
      if Geral.IntInConjunto(TipoLote, DmLotes.QrNovoTxsGenero.Value) then
      if Geral.IntInConjunto(TipoLote, DmLotes.QrNovoTxsAutomatico.Value) then
      begin
        Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'LotesTxs', 'LotesTxs', 'Controle');
        Dmod.QrUpd.Params[00].AsInteger := DmLotes.QrNovoTxsCodigo.Value;
        Dmod.QrUpd.Params[01].AsFloat   := DmLotes.QrNovoTxsValor.Value;
        Dmod.QrUpd.Params[02].AsFloat   := DmLotes.QrNovoTxsValor.Value;
        Dmod.QrUpd.Params[03].AsInteger := DmLotes.QrNovoTxsForma.Value;
        //
        Dmod.QrUpd.Params[04].AsInteger := VAR_USUARIO;
        Dmod.QrUpd.Params[05].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpd.Params[06].AsInteger := NumeroDoLote;
        Dmod.QrUpd.Params[07].AsInteger := Controle;
        Dmod.QrUpd.ExecSQL;
      end;
      DmLotes.QrNovoTxs.Next;
    end;
  end else begin
    DmLotes.QrTaxasCli.Close;
    DmLotes.QrTaxasCli.Params[0].AsInteger := Cliente;
    DmLotes.QrTaxasCli.Open;
    while not DmLotes.QrTaxasCli.Eof do
    begin
      if Geral.IntInConjunto(TipoLote, DmLotes.QrTaxasCliGenero.Value) then
      begin
        Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'LotesTxs', 'LotesTxs', 'Controle');
        Dmod.QrUpd.Params[00].AsInteger := DmLotes.QrTaxasCliTaxa.Value;
        Dmod.QrUpd.Params[01].AsFloat   := DmLotes.QrTaxasCliValor.Value;
        Dmod.QrUpd.Params[02].AsFloat   := DmLotes.QrTaxasCliValor.Value;
        Dmod.QrUpd.Params[03].AsInteger := DmLotes.QrTaxasCliForma.Value;
        //
        Dmod.QrUpd.Params[04].AsInteger := VAR_USUARIO;
        Dmod.QrUpd.Params[05].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
        Dmod.QrUpd.Params[06].AsInteger := NumeroDoLote;
        Dmod.QrUpd.Params[07].AsInteger := Controle;
        Dmod.QrUpd.ExecSQL;
      end;
      DmLotes.QrTaxasCli.Next;
    end;
  end;
end;

procedure TFmLotes1.EdTaxaQtd5Change(Sender: TObject);
begin
  CalculaTotalTaxa;
end;

procedure TFmLotes1.EdTaxaTxa5Change(Sender: TObject);
begin
  CalculaTotalTaxa;
end;

procedure TFmLotes1.CalculaTotalTaxa;
var
  Qtd, Txa, Tot: Double;
begin
  Qtd := EdTaxaQtd5.ValueVariant;
  Txa := EdTaxaTxa5.ValueVariant;
  Tot := Qtd * Txa;
  EdTaxaTot5.ValueVariant := Tot;
end;

procedure TFmLotes1.EdDesco4Exit(Sender: TObject);
begin
  CalculaDiasDU;
end;

procedure TFmLotes1.EdTaxaCod5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_ESCAPE then
  begin
    BtDesiste5Click(Self);
  end;
end;

procedure TFmLotes1.EdDuplicataExit(Sender: TObject);
begin
  DmLotes.QrDupNeg.Close;
  DmLotes.QrDupNeg.Params[00].AsInteger := QrLotesCliente.Value;
  DmLotes.QrDupNeg.Params[01].AsString  := EdDuplicata.Text;
  DmLotes.QrDupNeg.Params[02].AsInteger := QrLotesCodigo.Value;
  DmLotes.QrDupNeg.Open;
  if DmLotes.QrDupNeg.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Duplicata j� negociada em ' + FormatDateTime(VAR_FORMATDATE2,
      DmLotes.QrDupNegDCompra.Value) + ' - Lote: ' + Geral.FF0(DmLotes.QrDupNegLote.Value) + '.');
    EdDuplicata.SetFocus;
  end;
end;

procedure TFmLotes1.GradeX1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_RETURN then
  begin
    if GradeX1.EditorMode = True then
    begin
      if (GradeX1.Row < GradeX1.RowCount-1) then
        GradeX1.Row := GradeX1.Row +1
      else BtConfirma.SetFocus;
    end;
  end
end;

procedure TFmLotes1.ImportaCheque(Codigo, Cliente, CBE: Integer);
var
  DMais, Dias, Praca: Integer;
  Vence, Data_: TDateTime;
  Controle, Cheque, Comp, Banco, Agencia, StatusSPC: Integer;
  Valor: Double;
  DDeposito, DCompra, Vencto, Conta, CPF, Emitente: String;
begin
  Screen.Cursor := crHourGlass;
  try
    StatusSPC := Dmoc.QrLCHsStatusSPC.Value;
    Valor     := Dmoc.QrLCHsValor.Value;
    Vence     := Dmoc.QrLCHsDVence.Value;
    Vencto    := FormatDateTime(VAR_FORMATDATE, Vence);
    Data_     := Dmoc.QrLCHsDCompra.Value;
    DCompra   := FormatDateTime(VAR_FORMATDATE, Data_);
    Comp      := Dmoc.QrLCHsComp.Value;
    Banco     := Dmoc.QrLCHsBanco.Value;
    Agencia   := Dmoc.QrLCHsAgencia.Value;
    Conta     := Dmoc.QrLCHsConta.Value;
    Cheque    := Dmoc.QrLCHsCheque.Value;
    CPF       := Geral.SoNumero_TT(Dmoc.QrLCHsCPF.Value);
    Emitente  := Dmoc.QrLCHsEmitente.Value;
    Praca     := Dmoc.QrLCHsPraca.Value;
    //
    DMais     := Geral.IMV(EdDMaisX.Text);
    //
    ////////////////////////////////////////////////////////////////////////////
    if Comp = Dmod.QrControleRegiaoCompe.Value then
      Comp := Dmod.QrControleMinhaCompe.Value
    else
      Comp := Dmod.QrControleOutraCompe.Value;
    if Valor < DMod.QrControleChequeMaior.Value then Comp := Comp + 1;
    Dias := UMyMod.CalculaDias(int(TPData.Date), int(Dmoc.QrLCHsDVence.Value), DMais,
      Comp, Dmod.QrControleTipoPrazoDesc.Value, CBE);
    ////////////////////////////////////////////////////////////////////////////
    DDeposito := FormatDateTime(VAR_FORMATDATE, UMyMod.CalculaDataDeposito(Vence));
    CalculaValoresChequeImportando(Dias);
    Dmod.QrUpd.SQL.Clear;
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'LotesIts', 'LotesIts', 'Controle');
    Dmod.QrUpd.SQL.Add('INSERT INTO lotesits SET Quitado=0, ');
    Dmod.QrUpd.SQL.Add('Comp=:P0, Banco=:P1, Agencia=:P2, Conta=:P3, ');
    Dmod.QrUpd.SQL.Add('Cheque=:P4, CPF=:P5, Emitente=:P6, Valor=:P7, ');
    Dmod.QrUpd.SQL.Add('Vencto=:P8, TxaCompra=:P9, TxaAdValorem=:P10, ');
    Dmod.QrUpd.SQL.Add('VlrCompra=:P11, VlrAdValorem=:P12, DMais=:P13, ');
    Dmod.QrUpd.SQL.Add('Dias=:P14, TxaJuros=:P15, DCompra=:P16, ');
    Dmod.QrUpd.SQL.Add('DDeposito=:P17, Praca=:P18, Cliente=:P19, ');
    Dmod.QrUpd.SQL.Add('Data3=:P20, StatusSPC=:P21 ');
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb');
    //
    Dmod.QrUpd.Params[00].AsInteger := Comp;
    Dmod.QrUpd.Params[01].AsInteger := Banco;
    Dmod.QrUpd.Params[02].AsInteger := Agencia;
    Dmod.QrUpd.Params[03].AsString  := Conta;
    Dmod.QrUpd.Params[04].AsInteger := Cheque;
    Dmod.QrUpd.Params[05].AsString  := CPF;
    Dmod.QrUpd.Params[06].AsString  := Emitente;
    Dmod.QrUpd.Params[07].AsFloat   := Valor;
    Dmod.QrUpd.Params[08].AsString  := Vencto;
    //
    Dmod.QrUpd.Params[09].AsFloat   := FTaxa[0];
    Dmod.QrUpd.Params[10].AsFloat   := 0;
    Dmod.QrUpd.Params[11].AsFloat   := FValr[0];
    Dmod.QrUpd.Params[12].AsFloat   := 0;
    Dmod.QrUpd.Params[13].AsInteger := DMais;
    Dmod.QrUpd.Params[14].AsInteger := Dias;
    Dmod.QrUpd.Params[15].AsFloat   := FJuro[0];
    Dmod.QrUpd.Params[16].AsString  := DCompra;
    Dmod.QrUpd.Params[17].AsString  := DDeposito;
    Dmod.QrUpd.Params[18].AsInteger := Praca;
    Dmod.QrUpd.Params[19].AsInteger := Cliente;
    Dmod.QrUpd.Params[20].AsString  := DDeposito; // Quita autom�tico at� voltar
    Dmod.QrUpd.Params[21].AsInteger := StatusSPC;
    //
    Dmod.QrUpd.Params[22].AsInteger := Codigo;
    Dmod.QrUpd.Params[23].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    DefineCompraEmpresas(Codigo, Controle);
    //
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmLotes1.QrLotesItsAfterScroll(DataSet: TDataSet);
begin
  if FImprimindoColigadas then
    FDadosJurosColigadas := Dmod.ObtemEmLotIts(QrLotesItsControle.Value,
      QrLotesItsTxaCompra.Value, QrLotesItsTxaJuros.Value,
      QrLotesItsVlrCompra.Value);
  //
  DmLotes.ReopenSPC_Result(QrLotesItsCPF.Value, 0);
end;

procedure TFmLotes1.GradeLDU1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if (Column.FieldName = 'Emitente')
  then begin
    with GradeLDU1.Canvas do
    begin
      if Dmoc.QrLDUsEmitente.Value <> Dmoc.QrLDUsNOME_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'Rua')
  then begin
    with GradeLDU1.Canvas do
    begin
      if Dmoc.QrLDUsRua.Value <> Dmoc.QrLDUsRUA_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'NUMERO_TXT')
  then begin
    with GradeLDU1.Canvas do
    begin
      if Dmoc.QrLDUsNUMERO_TXT.Value <> Dmoc.QrLDUsNUMERO_2_TXT.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'Compl')
  then begin
    with GradeLDU1.Canvas do
    begin
      if Dmoc.QrLDUsCompl.Value <> Dmoc.QrLDUsCOMPL_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'Birro')
  then begin
    with GradeLDU1.Canvas do
    begin
      if Dmoc.QrLDUsBirro.Value <> Dmoc.QrLDUsBAIRRO_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'Cidade')
  then begin
    with GradeLDU1.Canvas do
    begin
      if Dmoc.QrLDUsCidade.Value <> Dmoc.QrLDUsCIDADE_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'CEP_TXT')
  then begin
    with GradeLDU1.Canvas do
    begin
      if Dmoc.QrLDUsCEP_TXT.Value <> Dmoc.QrLDUsCEP_2_TXT.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Trim(Column.Field.DisplayText));
    end;
  end else if (Column.FieldName = 'UF')
  then begin
    with GradeLDU1.Canvas do
    begin
      if Dmoc.QrLDUsUF.Value <> Dmoc.QrLDUsUF_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Trim(Column.Field.DisplayText));
    end;
  end else if (Column.FieldName = 'TEL1_TXT')
  then begin
    with GradeLDU1.Canvas do
    begin
      if Dmoc.QrLDUsTEL1_TXT.Value <> Dmoc.QrLDUsTEL1_2_TXT.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU1, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end
end;

procedure TFmLotes1.GradeLDU2DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if (Column.FieldName = 'NOME_2')
  then begin
    with GradeLDU2.Canvas do
    begin
      if Dmoc.QrLDUsEmitente.Value <> Dmoc.QrLDUsNOME_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'RUA_2')
  then begin
    with GradeLDU2.Canvas do
    begin
      if Dmoc.QrLDUsRua.Value <> Dmoc.QrLDUsRUA_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'NUMERO_2_TXT')
  then begin
    with GradeLDU2.Canvas do
    begin
      if Dmoc.QrLDUsNUMERO_TXT.Value <> Dmoc.QrLDUsNUMERO_2_TXT.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'COMPL_2')
  then begin
    with GradeLDU2.Canvas do
    begin
      if Dmoc.QrLDUsCompl.Value <> Dmoc.QrLDUsCOMPL_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'BAIRRO_2')
  then begin
    with GradeLDU2.Canvas do
    begin
      if Dmoc.QrLDUsBirro.Value <> Dmoc.QrLDUsBAIRRO_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'CIDADE_2')
  then begin
    with GradeLDU2.Canvas do
    begin
      if Dmoc.QrLDUsCidade.Value <> Dmoc.QrLDUsCIDADE_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'CEP_2_TXT')
  then begin
    with GradeLDU2.Canvas do
    begin
      if Dmoc.QrLDUsCEP_TXT.Value <> Dmoc.QrLDUsCEP_2_TXT.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'UF_2')
  then begin
    with GradeLDU2.Canvas do
    begin
      if Dmoc.QrLDUsUF.Value <> Dmoc.QrLDUsUF_2.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end else if (Column.FieldName = 'TEL1_2_TXT')
  then begin
    with GradeLDU2.Canvas do
    begin
      if Dmoc.QrLDUsTEL1_TXT.Value <> Dmoc.QrLDUsTEL1_2_TXT.Value then
        Font.Color := clRed else Font.Color := clBlack;
      MLAGeral.DesenhaTextoEmGrade(GradeLDU2, Rect, TA_LEFT, Column.Field.DisplayText);
    end;
  end
end;

procedure TFmLotes1.BtExclui2Click(Sender: TObject);
var
  i: integer;
  Exclui: Boolean;
  Grade: TDBGrid;
  x: String;
begin
  Exclui := False;
  Screen.Cursor := crHourGlass;
  try
    if QrLotesTipo.Value = 0 then Grade := GradeCHE else Grade := GradeDUP;
    if Grade.SelectedRows.Count > 0 then
    begin
      if QrLotesTipo.Value = 0 then
        x := 'todos cheques selecionados'
      else
        x := 'todas duplicatas selecionadas';
      if Geral.MB_Pergunta('Confirma a exclus�o de ' + x + '?') = ID_YES then
      begin
        with Grade.DataSource.DataSet do
        for i:= 0 to Grade.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
          Excluichequeselecionado;
        end;
        Exclui := True;
      end;
    end else begin
      if QrLotesTipo.Value = 0 then
        x := 'do cheque ' + Geral.FF0(QrLotesItsCheque.Value)
      else
        x := 'da duplicata ' + QrLotesItsDuplicata.Value;
      if Geral.MB_Pergunta('Confirma a exclus�o ' + x + '?') = ID_YES then
      begin
        Excluichequeselecionado;
        Exclui := True;
      end;
    end;
    if Exclui then
    begin
      QrLotesIts.Next;
      FControlIts := QrLotesItsControle.Value;
      CalculaLote(QrLotesCodigo.Value, True);
      LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value);
    end;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmLotes1.EdValorBase6Change(Sender: TObject);
begin
  CalculaAPagarOcor;
end;

procedure TFmLotes1.TPPagto6Change(Sender: TObject);
begin
  CalculaJurosOcor;
end;

procedure TFmLotes1.CalculaJurosOcor;
var
  Prazo: Integer;
  Taxa, Juros, Valor: Double;
begin
  Juros := 0;
  Prazo := Trunc(Int(TPPagto6.Date) - Int(TPDataBase6.Date));
  if Prazo > 0 then
  begin
    Taxa  := Geral.DMV(EdJurosBase6.Text);
    Juros := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(Taxa, Prazo);
  end;
  Valor := Geral.DMV(EdValorBase6.Text);
  EdJurosPeriodo6.Text := Geral.FFT(Juros, 6, siPositivo);
  EdJuros6.Text := Geral.FFT(Juros * Valor / 100, 2, siPositivo);
end;

procedure TFmLotes1.CalculaJurosCHDev;
var
  Prazo: Integer;
  Taxa, Juros, Valor: Double;
begin
  Juros := 0;
  Prazo := Trunc(Int(TPPagto7.Date) - Int(TPDataBase7.Date));
  if Prazo > 0 then
  begin
    Taxa  := Geral.DMV(EdJurosBase7.Text);
    Juros := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(Taxa, Prazo);
  end;
  Valor := Geral.DMV(EdValorBase7.Text);
  EdJurosPeriodo7.Text := Geral.FFT(Juros, 6, siPositivo);
  EdJuros7.Text := Geral.FFT(Juros * Valor / 100, 2, siPositivo);
end;

procedure TFmLotes1.CalculaAPagarOcor;
var
  Base, Juro: Double;
begin
  Base := EdValorBase6.ValueVariant;
  Juro := EdJuros6.ValueVariant;
  //
  EdAPagar6.ValueVariant := Base + Juro;
end;

procedure TFmLotes1.BtDesiste6Click(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmLotes1.BtConfirma6Click(Sender: TObject);
var
  OcorrPg: Integer;
begin
  OcorrPg := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'OcorrPG', 'OcorrPG', 'Codigo');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO ocorrpg SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2, LotePg=:P3');
  Dmod.QrUpd.SQL.Add(', Ocorreu=:Pa, Codigo=:Pb');
  Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPPagto6.Date);
  Dmod.QrUpd.Params[01].AsFloat   := Geral.DMV(EdJuros6.Text);
  Dmod.QrUpd.Params[02].AsFloat   := Geral.DMV(EdPago6.Text);
  Dmod.QrUpd.Params[03].AsInteger := QrLotesCodigo.Value;
  //
  Dmod.QrUpd.Params[04].AsInteger := DmLotes.QrOcorACodigo.Value;
  Dmod.QrUpd.Params[05].AsInteger := OcorrPg;
  Dmod.QrUpd.ExecSQL;
  //
  FOcorP := OcorrPg;
  ReopenOcorP;
  //
  CalculaPagtoOcorrPg(TPPagto6.Date, DmLotes.QrOcorACodigo.Value);
  ///
  FOcorP := DmLotes.QrOcorACodigo.Value;
  CalculaLote(QrLotesCodigo.Value, False);
  LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value);
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmLotes1.BtPagamento6Click(Sender: TObject);
begin
  MostraEdicao(9, CO_INCLUSAO, 0);
end;

procedure TFmLotes1.EdAPagar6Change(Sender: TObject);
begin
  EdPago6.Text := EdAPagar6.Text;
end;

procedure TFmLotes1.QrOcorPCalcFields(DataSet: TDataSet);
begin
  if QrOcorPDescri.Value <> '' then
    QrOcorPDOCUM_TXT.Value := QrOcorPDescri.Value
  else if QrOcorPTIPODOC.Value = 'CH' then
    QrOcorPDOCUM_TXT.Value := FormatFloat('000', QrOcorPBanco.Value) + '/' +
    FormatFloat('0000', QrOcorPAgencia.Value) + '/' +
    QrOcorPConta.Value + '-' + FormatFloat('000000', QrOcorPCheque.Value)
  else if QrOcorPTIPODOC.Value = 'DU' then
    QrOcorPDOCUM_TXT.Value := QrOcorPDuplicata.Value
  else QrOcorPDOCUM_TXT.Value := '';
  //
  QrOcorPCPF_TXT.Value := Geral.FormataCNPJ_TT(QrOcorPCPF.Value);
  //
  QrOcorPPagoNeg.Value := - QrOcorPPago.Value;
  QrOcorPSALDONEG.Value := - QrOcorPSALDO.Value;
end;

procedure TFmLotes1.QrOcorPAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  Enab := (QrLotes.State <> dsInactive) and (QrLotes.RecordCount > 0);
  Enab2 := (QrOcorP.State <> dsInactive) and (QrOcorP.RecordCount > 0);
  //
  BtPagtosExclui6.Enabled := Enab and Enab2;  
end;

procedure TFmLotes1.QrOcorPBeforeClose(DataSet: TDataSet);
begin
  BtPagtosExclui6.Enabled := False;
end;

procedure TFmLotes1.BtPagtosExclui6Click(Sender: TObject);
var
  Ocorreu: Integer;
begin
  DmLotes.QrLastOcor.Close;
  DmLotes.QrLastOcor.Params[0].AsInteger := QrOcorPOcorreu.Value;
  DmLotes.QrLastOcor.Open;
  if (DmLotes.QrLastOcorData.Value > QrOcorPData.Value)
  or ((DmLotes.QrLastOcorData.Value = QrOcorPData.Value)
  and (DmLotes.QrLastOcorCodigo.Value > QrOcorPCodigo.Value))
  then
     Geral.MB_Aviso('Somente o �ltimo pagamento pode ser exclu�do!')
  else begin
    if Geral.MB_Pergunta('Confirma a exclus�o deste pagamento?') = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        Ocorreu := QrOcorPOcorreu.Value;
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM ocorrpg WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrOcorPCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        CalculaLote(QrLotesCodigo.Value, False);
        DmLotes.QrLastOcor.Close;
        DmLotes.QrLastOcor.Params[0].AsInteger := Ocorreu;
        DmLotes.QrLastOcor.Open;
        CalculaPagtoOcorrPg(DmLotes.QrLastOcorData.Value, Ocorreu);
        //
        QrOcorP.Next;
        FOcorP := QrOcorPCodigo.Value;
        LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value);
        ReopenOcorP;
        ReopenOcorA(QrLotesCliente.Value);
        Screen.Cursor := crDefault;
      except
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmLotes1.EdBancoChange(Sender: TObject);
begin
  LocalizaEmitente(True);
  if not EdBanco.Focused then Dmoc.ReopenBanco(EdBanco.Text);
end;

procedure TFmLotes1.ReopenBanco0;
begin
  DmLotes.QrBanco0.Close;
  DmLotes.QrBanco0.Params[0].AsString := EdBanco0.Text;
  DmLotes.QrBanco0.Open;
end;

procedure TFmLotes1.ReopenBanco4;
begin
  DmLotes.QrBanco4.Close;
  DmLotes.QrBanco4.Params[0].AsString := DBEdBanco4.Text;
  DmLotes.QrBanco4.Open;
end;

procedure TFmLotes1.EdBanco4Change(Sender: TObject);
begin
  if not DBEdBanco4.Focused then ReopenBanco4;
end;

procedure TFmLotes1.Emitenteselecionado1Click(Sender: TObject);
var
  Banco, Agencia, Conta, CPF, Nome: String;
  Ordem: Integer;
begin
  Ordem    := Dmoc.QrLCHsOrdem.Value;
  Banco    := MLAGeral.FFD(Dmoc.QrLCHsBanco.Value, 3, siPositivo);
  Agencia  := MLAGeral.FFD(Dmoc.QrLCHsAgencia.Value, 4, siPositivo);
  Conta    := Dmoc.QrLCHsConta.Value;
  CPF      := Geral.SoNumero_TT(Dmoc.QrLCHsCPF.Value);
  Nome     := Dmoc.QrLCHsEmitente.Value;
  Dmoc.AtualizaEmitente(Banco, Agencia, Conta, CPF, Nome, -1);
  AtualizaAcumulado;
  //
  Dmoc.ReopenImportLote(Ordem, Dmoc.QrLDUsOrdem.Value);
end;

procedure TFmLotes1.Todosemitentesnocadastrados1Click(Sender: TObject);
var
  Banco, Agencia, Conta, CPF, Nome: String;
  Ordem: Integer;
begin
  Ordem := Dmoc.QrLCHsOrdem.Value;
  Dmoc.QrLCHs.First;
  while not Dmoc.QrLCHs.Eof do
  begin
    if Dmoc.QrLCHsMEU_SIT.Value = 0 then
    begin
      Banco    := MLAGeral.FFD(Dmoc.QrLCHsBanco.Value, 3, siPositivo);
      Agencia  := MLAGeral.FFD(Dmoc.QrLCHsAgencia.Value, 4, siPositivo);
      Conta    := Dmoc.QrLCHsConta.Value;
      CPF      := Geral.SoNumero_TT(Dmoc.QrLCHsCPF.Value);
      Nome     := Dmoc.QrLCHsEmitente.Value;
      Dmoc.AtualizaEmitente(Banco, Agencia, Conta, CPF, Nome, -1);
    end;
    Dmoc.QrLCHs.Next;
  end;
  AtualizaAcumulado;
  Dmoc.ReopenImportLote(Ordem, Dmoc.QrLDUsOrdem.Value);
end;

procedure TFmLotes1.BtEntiTroca1Click(Sender: TObject);
begin
  Emitenteselecionado1Click(Self);
  LocalizaProximoChNaoVerde;
end;

procedure TFmLotes1.Sacadoselecionado1Click(Sender: TObject);
begin
  FmPrincipal.AtualizaSacado(Dmoc.QrLDUsCPF.Value, Dmoc.QrLDUsEmitente.Value, Dmoc.QrLDUsRua.Value,
    Dmoc.QrLDUsNumero.Value, Dmoc.QrLDUsCompl.Value, Dmoc.QrLDUsBirro.Value,
    Dmoc.QrLDUsCidade.Value, Dmoc.QrLDUsUF.Value, Geral.FF0(Dmoc.QrLDUsCEP.Value),
    Geral.SoNumeroESinal_TT(Dmoc.QrLDUsTel1.Value), Dmoc.QrLDUsIE.Value, '', -1);
  //
  AtualizaAcumulado;
  Dmoc.ReopenImportLote(Dmoc.QrLCHsOrdem.Value, Dmoc.QrLDUsOrdem.Value);
end;

procedure TFmLotes1.Todossacadosnocadastrados1Click(Sender: TObject);
var
  Ordem: Integer;
begin
  Ordem := Dmoc.QrLDUsOrdem.Value;
  Dmoc.QrLDUs.First;
  while not Dmoc.QrLDUs.Eof do
  begin
    if Dmoc.QrLDUsMEU_SIT.Value = 0 then
    begin
      FmPrincipal.AtualizaSacado(Dmoc.QrLDUsCPF.Value, Dmoc.QrLDUsEmitente.Value, Dmoc.QrLDUsRua.Value,
        Dmoc.QrLDUsNumero.Value, Dmoc.QrLDUsCompl.Value, Dmoc.QrLDUsBirro.Value,
        Dmoc.QrLDUsCidade.Value, Dmoc.QrLDUsUF.Value, Geral.FF0(Dmoc.QrLDUsCEP.Value),
        Geral.SoNumeroESinal_TT(Dmoc.QrLDUsTel1.Value), Dmoc.QrLDUsIE.Value, '', -1);
    end;
    Dmoc.QrLDUs.Next;
  end;
  AtualizaAcumulado;
  Dmoc.ReopenImportLote(Dmoc.QrLDUsOrdem.Value, Ordem);
end;

procedure TFmLotes1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F12 then
  begin
    if PainelBandaMagnetica.Visible then BtConfirma2Click(Self);
    if PainelDuplicata.Visible then BtConfirma4Click(Self);
  end else
  if Key = VK_ESCAPE then begin
    FEscapeEnabed := True;
    if PainelBandaMagnetica.Visible then BtSair2Click(Self);
    if PainelDuplicata.Visible then BtSair4Click(Self);
  end;
end;

procedure TFmLotes1.BtSair2Click(Sender: TObject);
var
  Codigo : Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Codigo := QrLotesCodigo.Value;
    CalculaLote(Codigo, True);
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Lotes', 'Codigo');
    MostraEdicao(0, CO_TRAVADO, 0);
    LocCod(Codigo, Codigo);
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmLotes1.QrLotesAfterOpen(DataSet: TDataSet);
var
  Enab: Boolean;
begin
  InfoTempo(Now, 'Lote reaberto', False);
  //
  Enab := (QrLotes.State <> dsInactive) and (QrLotes.RecordCount > 0);
  //
  BtAltera.Enabled   := Enab;
  BtExclui.Enabled   := Enab;
  SbImprime.Enabled  := Enab;
  BtCheckImp.Enabled := Enab;
end;

procedure TFmLotes1.BtPagamento7Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPagamento7, BtPagamento7);
end;

procedure TFmLotes1.CalculaAPagarCHDev;
var
  Base, Juro: Double;
begin
  Base := Geral.DMV(EdValorBase7.Text);
  Juro := Geral.DMV(EdJuros7.Text);
  //
  EdAPagar7.Text := Geral.FFT(Base+Juro, 2, siPositivo);
  EdPago7.Text   := Geral.FFT(Base+Juro, 2, siPositivo);
end;

procedure TFmLotes1.ConfiguraPgCH(Data: TDateTime);
var
  ValorBase: Double;
begin
  DmLotes.QrLocPg.Close;
  DmLotes.QrLocPg.Params[0].AsInteger := DmLotes.QrCHDevACodigo.Value;
  DmLotes.QrLocPg.Open;
  TPPagto7.MinDate := 0;
  if DmLotes.QrLocPg.RecordCount > 0 then
  begin
    TPDataBase7.Date := Int(DmLotes.QrLocPgData.Value);
    TPPagto7.Date    := Int(DmLotes.QrLocPgData.Value);
  end else begin
    TPDataBase7.Date := Int(DmLotes.QrCHDevAData1.Value);
    TPPagto7.Date    := Int(DmLotes.QrCHDevAData1.Value);
  end;
  EdJurosBase7.Text := Geral.FFT(DmLotes.QrCHDevAJurosP.Value, 6, siPositivo);
  ValorBase := DmLotes.QrCHDevAValor.Value +DmLotes.QrCHDevATaxas.Value +
    DmLotes.QrCHDevAMulta.Value + DmLotes.QrCHDevAJurosV.Value - DmLotes.QrCHDevAValPago.Value -
    DmLotes.QrCHDevADesconto.Value - DmLotes.QrCHDevAPgDesc.Value;
  EdValorBase7.Text := Geral.FFT(ValorBase, 2, siPositivo);
  TPPagto7.MinDate := TPPagto7.Date;
  if Int(Date) > TPPagto7.Date then TPPagto7.Date := Int(Date);
  TPPagto7.SetFocus;
end;

procedure TFmLotes1.ConfiguraPgOc(Data: TDateTime);
var
  ValorBase: Double;
begin
  DmLotes.QrLocOc.Close;
  DmLotes.QrLocOc.Params[0].AsInteger := DmLotes.QrOcorACodigo.Value;
  DmLotes.QrLocOc.Open;
  TPPagto6.MinDate := 0;
  if DmLotes.QrLocOc.RecordCount > 0 then
  begin
    TPPagto6.Date     := Int(DmLotes.QrLocOcData.Value);
    TPDataBase6.Date  := Int(DmLotes.QrLocOcData.Value);
  end else begin
    TPPagto6.Date    := Int(DmLotes.QrOcorADataO.Value);
    TPDataBase6.Date := Int(DmLotes.QrOcorADataO.Value);
  end;
  EdJurosBase6.Text := Geral.FFT(
    Dmod.ObtemTaxaDeCompraCliente(QrLotesCliente.Value), 6, siPositivo);
  ValorBase := DmLotes.QrOcorAValor.Value + DmLotes.QrOcorATaxaV.Value - DmLotes.QrOcorAPago.Value;
  EdValorBase6.Text := Geral.FFT(ValorBase, 2, siNegativo);
  TPPagto6.MinDate := Int(TPPagto6.Date);
  if Date > TPPagto6.MinDate then TPPagto6.Date := Int(Date);
  TPPagto6.SetFocus;
end;

procedure TFmLotes1.BtDesiste7Click(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmLotes1.EdValorBase7Change(Sender: TObject);
begin
  CalculaAPagarCHDev;
end;

procedure TFmLotes1.EdJurosBase7Change(Sender: TObject);
begin
  CalculaJurosCHDev;
end;

procedure TFmLotes1.TPPagto7Change(Sender: TObject);
begin
  CalculaJurosCHDev;
end;

procedure TFmLotes1.EdJuros7Change(Sender: TObject);
begin
  CalculaAPagarCHDev;
end;

procedure TFmLotes1.BtConfirma7Click(Sender: TObject);
var
  Codigo: Integer;
begin
  Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'AlinPgs', 'AlinPgs', 'Codigo');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO alinpgs SET AlterWeb=1, LotePg=' + Geral.FF0(QrLotesCodigo.Value) + ',');
  Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2, Desco=:P3');
  Dmod.QrUpd.SQL.Add(', AlinIts=:Pa, Codigo=:Pb');
  Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPPagto7.Date);
  Dmod.QrUpd.Params[01].AsFloat   := Geral.DMV(EdJuros7.Text);
  Dmod.QrUpd.Params[02].AsFloat   := Geral.DMV(EdPago7.Text);
  Dmod.QrUpd.Params[03].AsFloat   := Geral.DMV(EdDesco7.Text);
  //
  Dmod.QrUpd.Params[04].AsInteger := DmLotes.QrCHDevACodigo.Value;
  Dmod.QrUpd.Params[05].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  CalculaLote(QrLotesCodigo.Value, False);
  CalculaPagtoAlinIts(TPPagto7.Date, DmLotes.QrCHDevACodigo.Value);
  FCHDevA := DmLotes.QrCHDevACodigo.Value;
  FCHDevP := Codigo;
  LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value);
  ReopenCHDevA(QrLotesCliente.Value);
  ReopenCHDevP;
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmLotes1.CalculaPagtoAlinIts(Quitacao: TDateTime; AlinIts: Integer);
var
  Sit: Integer;
  Data3: String;
begin
  DmLotes.QrSumPg.Close;
  DmLotes.QrSumPg.Params[0].AsInteger := AlinIts;
  DmLotes.QrSumPg.Open;
  if (DmLotes.QrSumPgPago.Value + DmLotes.QrSumPgDesco.Value)< 0.01 then Sit := 0 else
    if (DmLotes.QrSumPgPago.Value + DmLotes.QrSumPgDesco.Value) <
    (DmLotes.QrSumPgJuros.Value + DmLotes.QrCHDevAValor.Value + DmLotes.QrCHDevATaxas.Value +
    DmLotes.QrCHDevAMulta.Value - DmLotes.QrCHDevADesconto.Value)-0.009 then
    Sit := 1 else Sit := 2;
  // Novo 2007 08 21
  if Sit > 1 then Data3 := FormatDateTime(VAR_FORMATDATE, Quitacao)
  else Data3 := '0000-00-00';
  // Fim Novo
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE alinits SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('JurosV=:P0, ValPago=:P1, PgDesc=:P2, Status=:P3, ');
  Dmod.QrUpd.SQL.Add('Data3=:P4, Data4=:P5 WHERE Codigo=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := DmLotes.QrSumPgJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := DmLotes.QrSumPgPago.Value;
  Dmod.QrUpd.Params[02].AsFloat   := DmLotes.QrSumPgDesco.Value;
  Dmod.QrUpd.Params[03].AsInteger := Sit;
  Dmod.QrUpd.Params[04].AsString  := Data3;
  Dmod.QrUpd.Params[05].AsString  := FormatDateTime(VAR_FORMATDATE, Quitacao);
  Dmod.QrUpd.Params[06].AsInteger := AlinIts;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TFmLotes1.CalculaPagtoLotesIts(LotesIts: Integer);
var
  Sit: Integer;
  Cred, Debi: Double;
  Data3, Data4: String;
begin
  DmLotes.QrPgD.Close;
  DmLotes.QrPgD.Params[0].AsInteger := LotesIts;
  DmLotes.QrPgD.Open;
  Cred := DmLotes.QrPgDPago.Value + DmLotes.QrPgDDesco.Value;
  Debi := DmLotes.QrPgDValor.Value + DmLotes.QrPgDJuros.Value;
  if DmLotes.QrPgDPago.Value < 0.01 then Sit := 0 else
  begin
    if Cred < (Debi - 0.009) then Sit := 1 else
    begin
      if Cred > (Debi + 0.009) then Sit := 3 else Sit := 2;
    end;
  end;
  //

  //Data := FormatDateTime(VAR_FORMATDATE, DmLotes.QrPgDMaxData.Value);
  // novo 2007 08 21
  Data4 := FormatDateTime(VAR_FORMATDATE, DmLotes.QrPgDMaxData.Value);
  if Sit > 1 then Data3 := Data4 else Data3 := '0000-00-00';
  // fim novo
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   ');
  Dmod.QrUpd.SQL.Add('TotalJr=:P0, TotalDs=:P1, TotalPg=:P2, ');
  Dmod.QrUpd.SQL.Add('Data3=:P3, Data4=:P4, Quitado=:P5 WHERE Controle=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := DmLotes.QrPgDJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := DmLotes.QrPgDDesco.Value;
  Dmod.QrUpd.Params[02].AsFloat   := DmLotes.QrPgDPago.Value;
  Dmod.QrUpd.Params[03].AsString  := Data3;
  Dmod.QrUpd.Params[04].AsString  := Data4;
  Dmod.QrUpd.Params[05].AsInteger := Sit;
  //
  Dmod.QrUpd.Params[06].AsInteger := LotesIts;
  Dmod.QrUpd.ExecSQL;
  //
  FmPrincipal.AtualizaLastEditLote(Data4);
end;

procedure TFmLotes1.CalculaPagtoOcorrPg(Quitacao: TDateTime; OcorrPg: Integer);
var
  Sit: Integer;
  Valor, TaxaV: Double;
begin
  DmLotes.QrSumOc.Close;
  DmLotes.QrSumOc.Params[0].AsInteger := OcorrPg;
  DmLotes.QrSumOc.Open;
  Sit := 0;
  if DmLotes.QrSumOcPago.Value <> 0 then
  begin
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT Valor, TaxaV');
    Dmod.QrAux.SQL.Add('FROM ocorreu');
    Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
    Dmod.QrAux.Params[0].AsInteger := OcorrPg;
    Dmod.QrAux.Open;
    //
    Valor := Dmod.QrAux.FieldByName('Valor').AsFloat;
    TaxaV := Dmod.QrAux.FieldByName('TaxaV').AsFloat;
    //
    {
    Valor := DmLotes.QrOcorAValor.Value;
    TaxaV := DmLotes.QrOcorATaxaV.Value;
    //
    }
    if Valor = 0 then
      Valor := QrOcorPValor.Value;
    if TaxaV = 0 then
      TaxaV := QrOcorPJuros.Value;
    //
    if (DmLotes.QrSumOcPago.Value < (Valor + TaxaV)-0.009)
//   or (DmLotes.QrSumOcPago.Value > (DmLotes.QrOcorAValor.Value + DmLotes.QrOcorATaxaV.Value)+0.009) n�o funcionou
    then Sit := 1 else Sit := 2;
  end;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('TaxaV=:P0, Pago=:P1, Status=:P2, ');
  Dmod.QrUpd.SQL.Add('Data3=:P3 WHERE Codigo=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := DmLotes.QrSumOcJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := DmLotes.QrSumOcPago.Value;
  Dmod.QrUpd.Params[02].AsInteger := Sit;
  Dmod.QrUpd.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Quitacao);
  Dmod.QrUpd.Params[04].AsInteger := OcorrPg;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TFmLotes1.QrCHDevPCalcFields(DataSet: TDataSet);
begin
  QrCHDevPNOMESTATUS.Value := MLAGeral.NomeStatusPgto(QrCHDevPStatus.Value);
  QrCHDevPCPF_TXT.Value := Geral.FormataCNPJ_TT(QrCHDevPCPF.Value);
end;

procedure TFmLotes1.QrCHDevPAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrLotes.State <> dsInactive) and (QrLotes.RecordCount > 0);
  Enab2 := (QrCHDevP.State <> dsInactive) and (QrCHDevP.RecordCount > 0);
  //
  BtExclui7.Enabled := Enab and Enab2;
end;

procedure TFmLotes1.QrCHDevPBeforeClose(DataSet: TDataSet);
begin
  BtExclui7.Enabled := False;
end;

procedure TFmLotes1.BtExclui7Click(Sender: TObject);
var
  AlinIts: Integer;
begin
  DmLotes.QrLastCHDV.Close;
  DmLotes.QrLastCHDV.Params[0].AsInteger := QrCHDevPAlinIts.Value;
  DmLotes.QrLastCHDV.Open;
  if DmLotes.QrLastCHDVData.Value > QrCHDevPData.Value then
     Geral.MB_Aviso('Somente o �ltimo pagamento pode ser exclu�do!')
  else begin
    if Geral.MB_Pergunta('Confirma a exclus�o do pagamento selecionado?') = ID_YES then
    begin
      AlinIts := QrCHDevPAlinIts.Value;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM alinpgs WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrCHDevPCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      CalculaLote(QrLotesCodigo.Value, False);
      DmLotes.QrLastCHDV.Close;
      DmLotes.QrLastCHDV.Params[0].AsInteger := AlinIts;
      DmLotes.QrLastCHDV.Open;
      CalculaPagtoAlinIts(DmLotes.QrLastCHDVData.Value, AlinIts);
      //
      QrCHDevP.Next;
      FCHDevP := QrCHDevPCodigo.Value;
      LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value);
      ReopenCHDevP;
      ReopenCHDevA(QrLotesCliente.Value);
    end;
  end;
end;

procedure TFmLotes1.PMBorderoPopup(Sender: TObject);
var
  OK: Boolean;
begin
  if QrLotesMINAV.Value+QrLotesMINTC.Value>=0.01 then
  begin
    Contabilidade1.Visible := True;
    MargemtOtal1.Visible   := True;
  end else begin
    Contabilidade1.Visible := False;
    MargemtOtal1.Visible   := False;
  end;
  OK := Geral.IntToBool(QrLotesTipo.Value);
  AditivoNPAP1.Visible         := OK;
  AutorizaodeProtesto1.Visible := OK;
  CartaaoSacado1.Visible       := OK;
  CartaaoEmitentedoCheque1.Visible := not OK;
end;

procedure TFmLotes1.EdJurosBase6Change(Sender: TObject);
begin
  CalculaJurosOcor;
  CalculaAPagarOcor;
end;

procedure TFmLotes1.EdJuros6Change(Sender: TObject);
var
  VBase, Juros: Double;
begin
  VBase := Geral.DMV(EdValorBase6.Text);
  Juros := Geral.DMV(EdJuros6.Text);
  EdAPagar6.Text := Geral.FFT(VBase+Juros, 2, siNegativo);
end;

procedure TFmLotes1.EsteBorder1Click(Sender: TObject);
begin
  RecalculaBordero(QrLotesCodigo.Value);
end;

procedure TFmLotes1.RecalculaBordero(Lote: Integer);
begin
  DmLotes.QrLI.Close;
  DmLotes.QrLI.Params[0].AsInteger := Lote;
  DmLotes.QrLI.Open;
  if DmLotes.QrLITipo.Value = 0 then
    RefreshEmBorderoCH(Lote, True)
  else
    RefreshEmBorderoDU(Lote, True);
end;

procedure TFmLotes1.DesteBorderemdiante1Click(Sender: TObject);
begin
  Timer1.Enabled := True;
end;

procedure TFmLotes1.RefreshEmBorderoCH(Lote: Integer; Unico: Boolean);
begin
  Screen.Cursor := crHourGlass;
  try
    if DmLotes.QrLI.RecordCount > 0 then
    begin
      ProgressBar1.Position := 0;
      ProgressBar1.Max := DmLotes.QrLI.RecordCount;
      if Unico then ProgressBar1.Visible := True;
      while not DmLotes.QrLI.Eof do
      begin
        ProgressBar1.Position := ProgressBar1.Position + 1;
        Update;
        Application.ProcessMessages;
        RefreshCheque;
        DmLotes.QrLI.Next;
      end;
    end;
    //
    CalculaLote(Lote, True);
    if Unico then
    begin
      ProgressBar1.Visible := False;
      LocCod(Lote, Lote);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLotes1.RefreshEmBorderoDU(Lote: Integer; Unico: Boolean);
begin
  Screen.Cursor := crHourGlass;
  try
    if DmLotes.QrLI.RecordCount > 0 then
    begin
      ProgressBar1.Max := DmLotes.QrLI.RecordCount;
      ProgressBar1.Position := 0;
      if Unico then ProgressBar1.Visible := True;
      while not DmLotes.QrLI.Eof do
      begin
        ProgressBar1.Position := ProgressBar1.Position + 1;
        Update;
        Application.ProcessMessages;
        RefreshDuplicata;
        DmLotes.QrLI.Next;
      end;
    end;
    //
    CalculaLote(Lote, True);
    if Unico then
    begin
      ProgressBar1.Visible := False;
      LocCod(Lote, Lote);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLotes1.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FEscapeEnabed := False;
  DmLotes.QrLocs.Close;
  DmLotes.QrLocs.Params[0].AsInteger := QrLotesCodigo.Value;
  DmLotes.QrLocs.Open;
  ProgressBar2.Position := 0;
  ProgressBar2.Visible := True;
  ProgressBar2.Max := DmLotes.QrLocs.RecordCount;
  ProgressBar1.Visible := True;
  while not DmLotes.QrLocs.Eof do
  begin
    ProgressBar1.Position := 0;
    ProgressBar2.Position := ProgressBar2.Position +1;
    ProgressBar2.Update;
    Application.ProcessMessages;
    DmLotes.QrLI.Close;
    DmLotes.QrLI.Params[0].AsInteger := DmLotes.QrLocsCodigo.Value;
    DmLotes.QrLI.Open;
    if DmLotes.QrLocsTipo.Value = 0 then
      RefreshEmBorderoCH(DmLotes.QrLocsCodigo.Value, False)
    else
      RefreshEmBorderoDU(DmLotes.QrLocsCodigo.Value, False);
    Application.ProcessMessages;
    if FEscapeEnabed then
    begin
      Geral.MB_Aviso('Processo abortado!');
      Exit;
    end;
    DmLotes.QrLocs.Next;
  end;
  LocCod(DmLotes.QrLocsCodigo.Value, DmLotes.QrLocsCodigo.Value);
  ProgressBar1.Visible := False;
  ProgressBar2.Visible := False;
end;

procedure TFmLotes1.BtRefreshClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRefresh, BtRefresh);
end;

function TFmLotes1.SQL_CH(Controle, Comp, Banco, Agencia, Cheque, DMais, Dias,
              Codigo, Praca, Tipific, StatusSPC: Integer; Conta, CPF, Emitente,
              Vencto, DCompra, DDeposito: String; Valor: Double; LaTipoCaption:
              String; Cliente: Integer): Integer;
begin
  Dmod.QrUpd.SQL.Clear;
  if LaTipoCaption = CO_INCLUSAO then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'LotesIts', 'LotesIts', 'Controle');
    Dmod.QrUpd.SQL.Add('INSERT INTO lotesits SET Quitado=0, ');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   ');
  end;
  Dmod.QrUpd.SQL.Add('Comp=:P0, Banco=:P1, Agencia=:P2, Conta=:P3, ');
  Dmod.QrUpd.SQL.Add('Cheque=:P4, CPF=:P5, Emitente=:P6, Valor=:P7, ');
  Dmod.QrUpd.SQL.Add('Vencto=:P8, TxaCompra=:P9, TxaAdValorem=:P10, ');
  Dmod.QrUpd.SQL.Add('VlrCompra=:P11, VlrAdValorem=:P12, DMais=:P13, ');
  Dmod.QrUpd.SQL.Add('Dias=:P14, TxaJuros=:P15, DCompra=:P16, ');
  Dmod.QrUpd.SQL.Add('DDeposito=:P17, Praca=:P18, Cliente=:P19, Data3=:P20, ');
  Dmod.QrUpd.SQL.Add('Tipific=:P21, StatusSPC=:P22 ');
  if LaTipoCaption = CO_INCLUSAO then
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb')
  else
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa AND Controle=:Pb');
  //
  Dmod.QrUpd.Params[00].AsInteger := Comp;
  Dmod.QrUpd.Params[01].AsInteger := Banco;
  Dmod.QrUpd.Params[02].AsInteger := Agencia;
  Dmod.QrUpd.Params[03].AsString  := Conta;
  Dmod.QrUpd.Params[04].AsInteger := Cheque;
  Dmod.QrUpd.Params[05].AsString  := CPF;
  Dmod.QrUpd.Params[06].AsString  := Emitente;
  Dmod.QrUpd.Params[07].AsFloat   := Valor;
  Dmod.QrUpd.Params[08].AsString  := Vencto;
  //
  Dmod.QrUpd.Params[09].AsFloat   := FTaxa[0];
  Dmod.QrUpd.Params[10].AsFloat   := 0;
  Dmod.QrUpd.Params[11].AsFloat   := FValr[0];
  Dmod.QrUpd.Params[12].AsFloat   := 0;
  Dmod.QrUpd.Params[13].AsInteger := DMais;
  Dmod.QrUpd.Params[14].AsInteger := Dias;
  Dmod.QrUpd.Params[15].AsFloat   := FJuro[0];
  Dmod.QrUpd.Params[16].AsString  := DCompra;
  Dmod.QrUpd.Params[17].AsString  := DDeposito;
  Dmod.QrUpd.Params[18].AsInteger := Praca;
  Dmod.QrUpd.Params[19].AsInteger := Cliente;
  Dmod.QrUpd.Params[20].AsString  := DDeposito; // Quita autom�tico at� voltar
  Dmod.QrUpd.Params[21].AsInteger := Tipific;
  Dmod.QrUpd.Params[22].AsInteger := StatusSPC;
  //
  Dmod.QrUpd.Params[23].AsInteger := Codigo;
  Dmod.QrUpd.Params[24].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  DefineCompraEmpresas(Codigo, Controle);
  //
  FControlIts := Controle;
  Result := Controle;
end;

function TFmLotes1.SQL_DU(Codigo, Controle, DMais, Dias, Banco, Agencia: Integer;
  Valor, Desco, Bruto: Double; Duplic, CPF, Sacado, Vencto, DCompra, DDeposito,
  Emissao, DescAte, LaTipoCaption: String; Cliente, CartDep: Integer): Integer;
begin
  Dmod.QrUpd.SQL.Clear;
  if LaTipoCaption = CO_INCLUSAO then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'LotesIts', 'LotesIts', 'Controle');
    Dmod.QrUpd.SQL.Add('INSERT INTO lotesits SET ');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   ');
  end;
  Dmod.QrUpd.SQL.Add('Duplicata=:P0, CPF=:P1, Emitente=:P2, Valor=:P3, ');
  Dmod.QrUpd.SQL.Add('Vencto=:P4, TxaCompra=:P5, VlrCompra=:P6, DMais=:P7, ');
  Dmod.QrUpd.SQL.Add('Dias=:P8, TxaJuros=:P9, DCompra=:P10, DDeposito=:P11, ');
  Dmod.QrUpd.SQL.Add('Emissao=:P12, Desco=:P13, Bruto=:P14, Banco=:P15, ');
  Dmod.QrUpd.SQL.Add('Agencia=:P16, Cliente=:P17, CartDep=:P18, DescAte=:P19 ');
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa, Controle=:Pb')
  else
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa AND Controle=:Pb');
  //
  Dmod.QrUpd.Params[00].AsString  := Duplic;
  Dmod.QrUpd.Params[01].AsString  := CPF;
  Dmod.QrUpd.Params[02].AsString  := Sacado;
  Dmod.QrUpd.Params[03].AsFloat   := Valor;
  Dmod.QrUpd.Params[04].AsString  := Vencto;
  //
  Dmod.QrUpd.Params[05].AsFloat   := FTaxa[0];
  Dmod.QrUpd.Params[06].AsFloat   := FValr[0];
  Dmod.QrUpd.Params[07].AsInteger := DMais;
  Dmod.QrUpd.Params[08].AsInteger := Dias;
  Dmod.QrUpd.Params[09].AsFloat   := FJuro[0];
  Dmod.QrUpd.Params[10].AsString  := DCompra;
  Dmod.QrUpd.Params[11].AsString  := DDeposito;
  Dmod.QrUpd.Params[12].AsString  := Emissao;
  Dmod.QrUpd.Params[13].AsFloat   := Desco;
  Dmod.QrUpd.Params[14].AsFloat   := Bruto;
  Dmod.QrUpd.Params[15].AsInteger := Banco;
  Dmod.QrUpd.Params[16].AsInteger := Agencia;
  Dmod.QrUpd.Params[17].AsInteger := Cliente;
  Dmod.QrUpd.Params[18].AsInteger := CartDep;
  Dmod.QrUpd.Params[19].AsString  := DescAte;
  //
  Dmod.QrUpd.Params[20].AsInteger := Codigo;
  Dmod.QrUpd.Params[21].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  DefineCompraEmpresas(Codigo, Controle);
  //
  FControlIts := Controle;
  Result := Controle;
end;

procedure TFmLotes1.Cabealhodoborderatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

function TFmLotes1.ReopenContrat: Boolean;
begin
  QrContrat.Close;
  QrContrat.Params[0].AsInteger := QrLotesCliente.Value;
  QrContrat.Params[1].AsInteger := QrLotesCliente.Value;
  QrContrat.Open;
  if QrContrat.RecordCount = 0 then
  begin
    Result := False;
    Geral.MB_Aviso('Cliente sem contrato!');
  end else Result := True;
end;

procedure TFmLotes1.IncluiPagamento;
var
  Terceiro, Cod : Integer;
  Valor: Double;
begin
  DefineVarDup;
  Cod       := QrLotesCodigo.Value;
  Terceiro  := QrLotesCliente.Value;
  //
  Valor := QrLotesA_PG_LIQ.Value;
  //
  UCash.Pagto(QrPagtos, tpDeb, Cod, Terceiro, VAR_FATID_0300, -300, stIns,
  'Pagamento de Compra de Direitos', Valor, VAR_USUARIO,
  0, -1, mmNenhum, 0, 0, True, False, 0, 0, 0, 0);
  LocCod(Cod, Cod);
end;

procedure TFmLotes1.BtPagtosInclui8Click(Sender: TObject);
begin
  if Dmod.QrControleCartEspecie.Value = 0 then
    IncluiPagamento
  else
    MyObjects.MostraPopUpDeBotao(PMPagto, BtPagtosInclui8);
end;

procedure TFmLotes1.DefineVarDup;
begin
  IC3_ED_FatNum := QrLotesCodigo.Value;
  IC3_ED_NF := 0;
  IC3_ED_Data := Date;
end;

procedure TFmLotes1.AtualizaPagamento(Lote, Cliente: Integer; Data: TDateTime;
Liquido: Double);
var
  Controle: Integer;
  DataStr: String;
begin
  if Dmod.QrControleCartEspecie.Value <> 0 then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      VAR_LCT, VAR_LCT, 'Controle');
    DataStr := FormatDateTime(VAR_FORMATDATE, Data);
    VAR_LANCTO2 := Controle;
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('INSERT INTO ' + VAR_LCT  + ' SET Documento=:P0, Data=:P1, ');
    Dmod.QrUpdU.SQL.Add('Tipo=:P2, Carteira=:P3, Credito=:P4, Debito=:P5, ');
    Dmod.QrUpdU.SQL.Add('Genero=:P6, NotaFiscal=:P7, Vencimento=:P8, Mez=:P9, ');
    Dmod.QrUpdU.SQL.Add('Descricao=:P10, Sit=:P11, Controle=:P12, Cartao=:P13, ');
    Dmod.QrUpdU.SQL.Add('Compensado=:P14, Linha=:P15, Fornecedor=:P16, ');
    Dmod.QrUpdU.SQL.Add('Cliente=:P17, MoraDia=:P18, Multa=:P19,DataCad=:P20, ');
    Dmod.QrUpdU.SQL.Add('UserCad=:P21, DataDoc=:P22, Vendedor=:P23, ');
    Dmod.QrUpdU.SQL.Add('Account=:P24, FatID=:P25, FatNum=:P26, FatParcela=:P27');
    Dmod.QrUpdU.Params[00].AsFloat   := 0;
    Dmod.QrUpdU.Params[01].AsString  := DataStr;
    Dmod.QrUpdU.Params[02].AsInteger := DmLotes.QrCartTipo.Value;
    Dmod.QrUpdU.Params[03].AsInteger := Dmod.QrControleCartEspecie.Value;
    Dmod.QrUpdU.Params[04].AsFloat   := 0;
    Dmod.QrUpdU.Params[05].AsFloat   := QrLotesA_PG_LIQ.Value;
    Dmod.QrUpdU.Params[06].AsInteger := -399;
    Dmod.QrUpdU.Params[07].AsInteger := 0;
    Dmod.QrUpdU.Params[08].AsString  := DataStr;
    Dmod.QrUpdU.Params[09].AsString  := '';
    Dmod.QrUpdU.Params[10].AsString  := 'CD';
    Dmod.QrUpdU.Params[11].AsInteger := 2;
    Dmod.QrUpdU.Params[12].AsFloat   := Controle;
    Dmod.QrUpdU.Params[13].AsInteger := 0;
    Dmod.QrUpdU.Params[14].AsString  := '';
    Dmod.QrUpdU.Params[15].AsInteger := 0;
    Dmod.QrUpdU.Params[16].AsInteger := 0;
    Dmod.QrUpdU.Params[17].AsInteger := Cliente;
    Dmod.QrUpdU.Params[18].AsFloat   := 0;
    Dmod.QrUpdU.Params[19].AsFloat   := 0;
    Dmod.QrUpdU.Params[20].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdU.Params[21].AsInteger := VAR_USUARIO;
    Dmod.QrUpdU.Params[22].AsString  := FormatDateTime(VAR_FORMATDATE, QrLotesData.Value);
    Dmod.QrUpdU.Params[23].AsInteger := 0;
    Dmod.QrUpdU.Params[24].AsInteger := 0;
    Dmod.QrUpdU.Params[25].AsInteger := 300;
    Dmod.QrUpdU.Params[26].AsInteger := QrLotesCodigo.Value;
    Dmod.QrUpdU.Params[27].AsInteger := 999;
    Dmod.QrUpdU.ExecSQL;
    //
    CalculaLote(QrLotesCodigo.Value, True);
    LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value);
  end else Geral.MB_Aviso('Carteira para pagamento r�pido n�o definida!');
end;

procedure TFmLotes1.QrPagtosCalcFields(DataSet: TDataSet);
begin
  (*if QrPagtosSit.Value = -1 then
     QrPagtosNOMESIT.Value := CO_IMPORTACAO
  else
    if QrPagtosSit.Value = 1 then
     QrPagtosNOMESIT.Value := CO_PAGTOPARCIAL
  else
    if QrPagtosSit.Value = 2 then
       QrPagtosNOMESIT.Value := CO_QUITADA
  else
    if QrPagtosSit.Value = 3 then
       QrPagtosNOMESIT.Value := CO_COMPENSADA
  else
    if QrPagtosVencimento.Value < Date then
       QrPagtosNOMESIT.Value := CO_VENCIDA
  else
       QrPagtosNOMESIT.Value := CO_EMABERTO;

  case QrPagtosTipo.Value of
    0: QrPagtosNOMETIPO.Value := CO_CAIXA;
    1: QrPagtosNOMETIPO.Value := CO_BANCO;
    2: QrPagtosNOMETIPO.Value := CO_EMISS;
  end;*)
  QrPagtosSEQ.Value := QrPagtos.RecNo;
  QrPagtosCPF_TXT.Value := Geral.FormataCNPJ_TT(QrPagtosCPF.Value);
  case QrPagtosTipoDoc.Value of
    0: QrPagtosNOMETIPODOC.Value := '';
    1: QrPagtosNOMETIPODOC.Value := 'Cheque';
    2: QrPagtosNOMETIPODOC.Value := 'DOC';
    3: QrPagtosNOMETIPODOC.Value := 'TED';
    4: QrPagtosNOMETIPODOC.Value := 'Esp�cie';
    else QrPagtosNOMETIPODOC.Value := '***DESCONHECIDO***';
  end;
end;

procedure TFmLotes1.Pagamento1Click(Sender: TObject);
begin
  ExcluiItemPagamento;
end;

procedure TFmLotes1.ExcluiItemPagamento;
var
  FatNum: Double;
  FatID, FatParcela: Integer;
begin
  if QrPagtos.RecordCount = 0 then
  begin
    Geral.MB_Aviso('N�o h� pagamento cadastrado!');
    Exit;
  end;
  FatParcela := QrPagtosFatParcela.Value;
  FatNum := QrPagtosFatNum.Value;
  FatID := QrPagtosFatID.Value;
  if (FatParcela = 0) and (FatNum=0) and (FatID=0) then Exit;
  if Geral.MB_Pergunta('Confirma a exclus�o deste pagamento?') = ID_YES then
  begin
    Dmod.QrUpdU.Close;
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM ' + VAR_LCT  + ' WHERE FatID=:P0');
    Dmod.QrUpdU.SQL.Add('AND FatNum=:P1 AND FatParcela=:P2 AND Controle=:P3');
    Dmod.QrUpdU.Params[0].AsInteger := FatID;
    Dmod.QrUpdU.Params[1].AsFloat   := FatNum;
    Dmod.QrUpdU.Params[2].AsInteger := FatParcela;
    Dmod.QrUpdU.Params[3].AsInteger := QrPagtosControle.Value;
    Dmod.QrUpdU.ExecSQL;

    Dmod.QrUpdU.Close;
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('UPDATE ' + VAR_LCT  + ' SET FatParcela=FatParcela-1');
    Dmod.QrUpdU.SQL.Add('WHERE FatParcela>:P0 AND FatID=:P1 AND FatNum=:P2');
    Dmod.QrUpdU.Params[0].AsInteger := FatParcela;
    Dmod.QrUpdU.Params[1].AsInteger := FatID;
    Dmod.QrUpdU.Params[2].AsFloat   := FatNum;
    Dmod.QrUpdU.ExecSQL;

    Dmod.RecalcSaldoCarteira(QrPagtosTipo.Value, QrPagtosCarteira.Value, 0);

    CalculaLote(FatNum, True);
    LocCod(Trunc(FatNum), Trunc(FatNum));
  end;
end;

procedure TFmLotes1.Todospagamentosdestebordero1Click(Sender: TObject);
begin
  ExcluiTodasParcelas;
end;

procedure TFmLotes1.ExcluiTodasParcelas;
var
  FatNum: Double;
  FatID: Integer;
begin
  if QrPagtos.RecordCount = 0 then
  begin
    Geral.MB_Aviso('N�o h� pagamento cadastrado!');
    Exit;
  end;
  if Geral.MB_Pergunta('Confirma a exclus�o de todas parcelas de pagamento?') = ID_YES then
  begin
    FatNum := QrPagtosFatNum.Value;
    FatID := QrPagtosFatID.Value;
    Dmod.QrUpdU.Close;
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM ' + VAR_LCT  + ' WHERE FatID=:P0');
    Dmod.QrUpdU.SQL.Add('AND FatNum=:P1');
    Dmod.QrUpdU.Params[0].AsInteger := FatID;
    Dmod.QrUpdU.Params[1].AsFloat   := FatNum;
    Dmod.QrUpdU.ExecSQL;
    //
    Dmod.RecalcSaldoCarteira(QrPagtosTipo.Value, QrPagtosCarteira.Value, 0);
    //
    CalculaLote(FatNum, True);
    LocCod(Trunc(FatNum), Trunc(FatNum));
  end;
end;

procedure TFmLotes1.ReopenPagtos;
begin
  QrPagtos.Close;
  QrPagtos.Params[0].AsInteger := QrLotesCodigo.Value;
  QrPagtos.Open;
end;

procedure TFmLotes1.QrPagtosAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrLotes.State <> dsInactive) and (QrLotes.RecordCount > 0);
  Enab2 := (QrPagtos.State <> dsInactive) and (QrPagtos.RecordCount > 0);
  //
  BtPagtosInclui8.Enabled := Enab;
  BtPagtosAltera8.Enabled := Enab and Enab2;
  BtPagtosExclui8.Enabled := Enab and Enab2;
  BtCheque.Enabled        := Enab and Enab2;
end;

procedure TFmLotes1.QrPagtosBeforeClose(DataSet: TDataSet);
begin
  BtPagtosInclui8.Enabled := False;
  BtPagtosAltera8.Enabled := False;
  BtPagtosExclui8.Enabled := False;
  BtCheque.Enabled        := False;
end;

procedure TFmLotes1.BtPagtosExclui8Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExcluiPagto, BtPagtosExclui8);
end;

procedure TFmLotes1.Novo1Click(Sender: TObject);
var
  Valor: Double;
begin
  Valor := QrLotesVAL_LIQUIDO.Value - QrLotesPgLiq.Value;
  if Valor <=0 then Valor := QrLotesA_PG_LIQ.Value;
  Application.CreateForm(TFmLotesPg, FmLotesPg);
  FmLotesPg.FValor      := Valor;
  FmLotesPg.FCliente    := QrLotesCliente.Value;
  FmLotesPg.FFatNum     := QrLotesCodigo.Value;
  FmLotesPg.FFatParcela := QrPagtos.RecNo + 1;
  FmLotesPg.TPData.Date := QrLotesData.Value;
  FmLotesPg.EdTxt.Text  := 'CD';
  FmLotesPg.FNomeCreditado := QrLotesNOMECLIENTE.Value;
  FmLotesPg.FCNPJCreditado := QrLotesCNPJCPF.Value;
  FmLotesPg.ReopenCreditados;
  FmLotesPg.EdValor.Text   := Geral.FFT(Valor, 2, siPositivo);
  //
  FmLotesPg.ShowModal;
  FmLotesPg.Destroy;
  if FMudouSaldo then CalculaLote(QrLotesCodigo.Value, True);
end;

procedure TFmLotes1.Rpido1Click(Sender: TObject);
begin
  AtualizaPagamento(QrLotesCodigo.Value, QrLotesCliente.Value, QrLotesData.Value,
    QrLotesVAL_LIQUIDO.Value);
end;

procedure TFmLotes1.EdDBAPGLIQChange(Sender: TObject);
begin
  if QrLotesA_PG_LIQ.Value >= 0.01 then
    EdDBAPGLIQ.Font.Color := clRed
  else
    EdDBAPGLIQ.Font.Color := clSilver;
end;

procedure TFmLotes1.QrSaCartAfterScroll(DataSet: TDataSet);
begin
  DmLotes.QrDupSac.Close;
  DmLotes.QrDupSac.Params[0].AsInteger := QrSaCartCodigo.Value;
  DmLotes.QrDupSac.Params[1].AsString  := QrSaCartCNPJ.Value;
  DmLotes.QrDupSac.Open;
end;

procedure TFmLotes1.BtChequeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCheque, BtCheque);
end;

procedure TFmLotes1.Nominal1Click(Sender: TObject);
begin
  FmPrincipal.ImprimeChequeSimples(Geral.FFT(QrPagtosDebito.Value, 2, siPositivo),
    QrPagtosNome.Value, Dmod.QrDonoCIDADE.Value, QrPagtosData.Value, True);
end;

procedure TFmLotes1.NoMostrarDias1Click(Sender: TObject);
begin
  ImprimeBorderoClienteSobra(False);
end;

procedure TFmLotes1.Portador1Click(Sender: TObject);
begin
  FmPrincipal.ImprimeChequeSimples(Geral.FFT(QrPagtosDebito.Value, 2, siPositivo),
    QrPagtosNome.Value, Dmod.QrDonoCIDADE.Value, QrPagtosData.Value, False);
end;

procedure TFmLotes1.ReopenDOpen(Cliente: Integer);
begin
  DmLotes.QrDOpen.Close;
  DmLotes.QrDOpen.Params[0].AsInteger := Cliente;
  DmLotes.QrDOpen.Params[1].AsInteger := FmPrincipal.FConnections;
  DmLotes.QrDOpen.Open;
end;

procedure TFmLotes1.ReopenRiscoC(Cliente: Integer);
begin
  DmLotes.QrRiscoC.Close;
  DmLotes.QrRiscoC.Params[0].AsInteger := Cliente;
  DmLotes.QrRiscoC.Params[1].AsInteger := FmPrincipal.FConnections;
  DmLotes.QrRiscoC.Open;
  //
  DmLotes.QrRiscoTC.Close;
  DmLotes.QrRiscoTC.Params[0].AsInteger := Cliente;
  DmLotes.QrRiscoTC.Params[1].AsInteger := FmPrincipal.FConnections;
  DmLotes.QrRiscoTC.Open;
  //
end;

procedure TFmLotes1.ReopenDPago;
begin
  QrDPago.Close;
  QrDPago.Params[0].AsInteger := QrLotesCodigo.Value;
  QrDPago.Open;
  //
  if FDPago > 0 then QrDPago.Locate('Controle', FDPago, []);
end;

function TFmLotes1.ReopenEmitCH(SelType: TSelType): Boolean;
var
  Quais: TSelType;
  i: Integer;
  Grade: TDBGrid;
  Txt: String;
begin
  Quais := SelType;
  if SelType = istPergunta then
  begin
    if DBCheck.CriaFm(TFmQuaisItens, FmQuaisItens, afmoNegarComAviso) then
    begin
      FmQuaisItens.ShowModal;
      Quais := FmQuaisItens.FEscolha;
      FmQuaisItens.Destroy;
    end;
  end;
  QrSaEmitCH.Close;
  QrSaEmitCH.SQL.Clear;
  QrSaEmitCH.SQL.Add('SELECT li.Codigo, li.Emitente, li.CPF');
  QrSaEmitCH.SQL.Add('FROM lotesits li');
  QrSaEmitCH.SQL.Add('');
  if QrLotesTipo.Value = 0 then Grade := GradeCHE else Grade := GradeDUP;
  if (Quais = istAtual) or (
    (Quais = istSelecionados) and (Grade.SelectedRows.Count < 2)) then
      QrSaEmitCH.SQL.Add('WHERE li.Controle=' +
      FormatFloat('0', QrLotesItsControle.Value)) else
  if Quais = istSelecionados then
  begin
    Txt := '';
    with Grade.DataSource.DataSet do
    for i := 0 to Grade.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
      if Txt <> '' then Txt := Txt + ',';
      Txt := Txt + FormatFloat('0', QrLotesItsControle.Value);
    end;
    QrSaEmitCH.SQL.Add('WHERE li.Controle in (' + Txt + ')');
  end else
  if Quais = istTodos then QrSaEmitCH.SQL.Add('WHERE li.Codigo=' +
      FormatFloat('0', QrLotesCodigo.Value))
  else begin
    Result := False;
    if Quais <> istNenhum then
      Geral.MB_Aviso('"SelType" n�o definido em "ReopenSaCart"!');
    Exit;
  end;
  QrSaEmitCH.SQL.Add('GROUP BY li.CPF');
  QrSaEmitCH.Open;
  Result := True;
end;

procedure TFmLotes1.QrSaCartAfterOpen(DataSet: TDataSet);
begin
  DmLotes.QrDupSac.Close;
  DmLotes.QrDupSac.Params[0].AsInteger := QrSaCartCodigo.Value;
  DmLotes.QrDupSac.Params[1].AsString  := QrSaCartCNPJ.Value;
  DmLotes.QrDupSac.Open;
end;

procedure TFmLotes1.EdDRChange(Sender: TObject);
begin
  if Geral.DMV(EdDR.Text) >= 0.01 then
  begin
    EdDR.Font.Color := $000080FF; // Laranja intenso
    EdDR_C.Font.Color := $000080FF; // Laranja intenso
    EdDR_D.Font.Color := $000080FF; // Laranja intenso
  end else begin
    EdDR.Font.Color := clSilver;
    EdDR_C.Font.Color := clSilver;
    EdDR_D.Font.Color := clSilver;
  end;
end;

procedure TFmLotes1.BtPagtosInclui9Click(Sender: TObject);
begin
  Application.CreateForm(TFmDuplicatas2, FmDuplicatas2);
  //
  FmDuplicatas2.PainelPesq.Enabled := False;
  FmDuplicatas2.EdCliente.Text     := Geral.FF0(QrLotesCliente.Value);
  FmDuplicatas2.CBCliente.KeyValue := QrLotesCliente.Value;
  FmDuplicatas2.EdDuplicata.Text   := DmLotes.QrDOpenDuplicata.Value;
  FmDuplicatas2.EdCPF.Text         := Geral.FormataCNPJ_TT(DmLotes.QrDOpenCPF.Value);
  FmDuplicatas2.EdEmitente.Text    := DmLotes.QrDOpenEmitente.Value;
  FmDuplicatas2.FLotePgOrigem      := QrLotesCodigo.Value;
  FmDuplicatas2.FCallFromLotes     := True;
  //
  FmDuplicatas2.ReabrirTabelas;
  FmDuplicatas2.PreparaDuplicataPg(3, FmDuplicatas2.QrPesqControle.Value);
  if FmDuplicatas2.ForcaOcorBank > 0 then
  begin
    FmDuplicatas2.ShowModal;
  end;
  FmDuplicatas2.Destroy;
  //
  CalculaLote(QrLotesCodigo.Value, True);
  LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value);
end;

procedure TFmLotes1.QrDPagoCalcFields(DataSet: TDataSet);
begin
  QrDPagoNOMESTATUS.Value := MLAGeral.NomeStatusPgto(QrDPagoQuitado.Value);
  QrDPagoCPF_TXT.Value := Geral.FormataCNPJ_TT(QrDPagoCPF.Value);
end;

procedure TFmLotes1.QrDPagoBeforeClose(DataSet: TDataSet);
begin
  BtPagtosExclui9.Enabled := False;
end;

procedure TFmLotes1.QrDPagoAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  Enab  := (QrLotes.State <> dsInactive) and (QrLotes.RecordCount > 0);
  Enab2 := (QrDPago.State <> dsInactive) and (QrDPago.RecordCount > 0);
  //
  BtPagtosExclui9.Enabled := Enab  and Enab2;
end;

procedure TFmLotes1.BtPagtosExclui9Click(Sender: TObject);
var
  LotesIts: Integer;
begin
  DmLotes.QrLastDPg.Close;
  DmLotes.QrLastDPg.Params[0].AsInteger := QrDPagoLotesIts.Value;
  DmLotes.QrLastDPg.Open;
  if (DmLotes.QrLastDPgData.Value > QrDPagoData.Value)
  or ((DmLotes.QrLastDPgData.Value = QrDPagoData.Value) and
      (DmLotes.QrLastDPgControle.Value > QrDPagoControle.Value))
  then
     Geral.MB_Aviso('Somente o �ltimo pagamento pode ser exclu�do!')
  else begin
    if Geral.MB_Aviso('Confirma a exclus�o do pagamento selecionado?') = ID_YES then
    begin
      LotesIts := QrDPagoLotesIts.Value;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM aduppgs WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrDPagoControle.Value;
      Dmod.QrUpd.ExecSQL;
      //
      CalculaLote(QrLotesCodigo.Value, True);
      DmLotes.QrLastDPg.Close;
      DmLotes.QrLastDPg.Params[0].AsInteger := LotesIts;
      DmLotes.QrLastDPg.Open;
      CalculaPagtoLotesIts(LotesIts);
      //
      QrDPago.Next;
      FDPago := QrDPagoControle.Value;
      LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value);
      ReopenDOpen(QrLotesCliente.Value);
      ReopenDPago;
    end;
  end;
end;

procedure TFmLotes1.EdDVChange(Sender: TObject);
begin
  if Geral.DMV(EdDV.Text) >= 0.01 then
  begin
    EdDV.Font.Color := clRed;
    EdDV_C.Font.Color := clRed;
    EdDV_D.Font.Color := clRed;
  end else begin
    EdDV.Font.Color := clSilver;
    EdDV_C.Font.Color := clSilver;
    EdDV_D.Font.Color := clSilver;
  end;
end;

procedure TFmLotes1.EdDTChange(Sender: TObject);
begin
  Exit;
  if Geral.DMV(EdDT.Text) >= 0.01 then
  begin
    EdDT.Font.Color := clRed;
    EdDT_C.Font.Color := clRed;
    EdDT_D.Font.Color := clRed;
  end else begin
    EdDT.Font.Color := clSilver;
    EdDT_S.Font.Color := clSilver;
    EdDT_D.Font.Color := clSilver;
  end;
end;

procedure TFmLotes1.EdCRChange(Sender: TObject);
begin
  if DmLotes.QrRiscoTCValor.Value >= 0.01 then
  begin
    EdCR.Font.Color := $000080FF; // Laranja intenso
    EdCR_C.Font.Color := $000080FF; // Laranja intenso
    EdCR_D.Font.Color := $000080FF; // Laranja intenso
  end else begin
    EdCR.Font.Color := clSilver;
    EdCR_C.Font.Color := clSilver;
    EdCR_D.Font.Color := clSilver;
  end;
end;

procedure TFmLotes1.EdCVChange(Sender: TObject);
begin
  if FCHDevOpen_Total >= 0.01 then
  begin
    EdCV.Font.Color := clRed;
    EdCV_C.Font.Color := clRed;
    EdCV_D.Font.Color := clRed;
  end else begin
    EdCV.Font.Color := clSilver;
    EdCV_C.Font.Color := clSilver;
    EdCV_D.Font.Color := clSilver;
  end;
end;

procedure TFmLotes1.EdDBValLiqChange(Sender: TObject);
begin
  if QrLotesVAL_LIQUIDO.Value >= 0.01 then
    EdDBValLiq.Font.Color := clGreen
  else
    EdDBValLiq.Font.Color := clFuchsia;
end;

procedure TFmLotes1.EdCTChange(Sender: TObject);
begin
  Exit;
  if Geral.DMV(EdCT.Text) >= 0.01 then
  begin
    EdCT.Font.Color := clRed;
    EdCT_C.Font.Color := clRed;
    EdCT_D.Font.Color := clRed;
  end else begin
    EdCT.Font.Color := clSilver;
    EdCT_C.Font.Color := clSilver;
    EdCT_D.Font.Color := clSilver;
  end;
end;

procedure TFmLotes1.EdRTChange(Sender: TObject);
begin
  if Geral.DMV(EdRT.Text) >= 0.01 then
  begin
    EdRT.Font.Color := $000080FF; // Laranja intenso
    EdRT_C.Font.Color := $000080FF; // Laranja intenso
    EdRT_D.Font.Color := $000080FF; // Laranja intenso
  end else begin
    EdRT.Font.Color := clSilver;
    EdRT_C.Font.Color := clSilver;
    EdRT_D.Font.Color := clSilver;
  end;
end;

procedure TFmLotes1.EdVTChange(Sender: TObject);
begin
  if Geral.DMV(EdVT.Text) >= 0.01 then
  begin
    EdVT.Font.Color := clRed;
    EdVT_C.Font.Color := clRed;
    EdVT_D.Font.Color := clRed;
  end else begin
    EdVT.Font.Color := clSilver;
    EdVT_C.Font.Color := clSilver;
    EdVT_D.Font.Color := clSilver;
  end;
end;

procedure TFmLotes1.EdStatusSPCChange(Sender: TObject);
var
  Status: Integer;
begin
  Status := Geral.IMV(EdStatusSPC.Text);
  EdStatusSPC_TXT.Text := MLAGeral.StatusSPC_TXT(Status);
  EdStatusSPC_TXT.Font.Color := MLAGeral.StatusSPC_Color(Status);
end;

procedure TFmLotes1.EdStatusSPCKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F6 then
    ConsultaCNPJCPF_CH;
end;

procedure TFmLotes1.EdStatusSPC_TXTKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F6 then
    ConsultaCNPJCPF_CH;
end;

procedure TFmLotes1.EdSTChange(Sender: TObject);
begin
  Exit;
  if Geral.DMV(EdST.Text) >= 0.01 then
  begin
    EdST.Font.Color := clRed;
    EdST_C.Font.Color := clRed;
    EdST_D.Font.Color := clRed;
  end else begin
    EdST.Font.Color := clSilver;
    EdST_C.Font.Color := clSilver;
    EdST_D.Font.Color := clSilver;
  end;
end;

procedure TFmLotes1.EdOAChange(Sender: TObject);
begin
  if FOcorA_Total >= 0.01 then
  begin
    EdOA.Font.Color := clRed;
    EdOA_C.Font.Color := clRed;
    EdOA_D.Font.Color := clRed;
  end else begin
    EdOA.Font.Color := clSilver;
    EdOA_C.Font.Color := clSilver;
    EdOA_D.Font.Color := clSilver;
  end;
end;

procedure TFmLotes1.EdTTChange(Sender: TObject);
begin
  if Geral.DMV(EdTT.Text) >= 0.01 then
  begin
    EdTT.Font.Color := clRed;
    EdTT_C.Font.Color := clRed;
    EdTT_D.Font.Color := clRed;
  end else begin
    EdTT.Font.Color := clSilver;
    EdTT_C.Font.Color := clSilver;
    EdTT_D.Font.Color := clSilver;
  end;
end;

procedure TFmLotes1.BtSinaleiraClick(Sender: TObject);
begin
  Application.CreateForm(TFmRiscoCli, FmRiscoCli);
  FmRiscoCli.EdCliente.Text := Geral.FF0(QrLotesCliente.Value);
  FmRiscoCli.CBCliente.KeyValue := QrLotesCliente.Value;
  FmRiscoCli.ShowModal;
  FmRiscoCli.Destroy;
end;

procedure TFmLotes1.PesquisaRiscoSacado(CPF: String);
begin
  FCPFSacado := Geral.SoNumero_TT(CPF);
  //FNomeSacado := '';
  // soma antes do �ltimo !!!!
  ReopenSacCHDevA;
  ReopenSacOcorA;
  // fim somas.
  //
  ReopenSacRiscoC;
  //
  // precisa ser o �ltimo !!!! para Ed?.Text
  ReopenSacDOpen;
  //
end;

procedure TFmLotes1.ReopenSacOcorA;
begin
  DmLotes.QrSacOcorA.Close;
  DmLotes.QrSacOcorA.SQL.Clear;
  DmLotes.QrSacOcorA.SQL.Add('SELECT li.Controle, li.Emitente, li.CPF, ');
  DmLotes.QrSacOcorA.SQL.Add('lo.Tipo, IF(oc.Cliente > 0,"CL",IF(lo.Tipo=0,"CH","DU"))TIPODOC,');
  DmLotes.QrSacOcorA.SQL.Add('ob.Nome NOMEOCORRENCIA, lo.Cliente CLIENTELOTE, oc.*');
  DmLotes.QrSacOcorA.SQL.Add('FROM ocorreu oc');
  DmLotes.QrSacOcorA.SQL.Add('LEFT JOIN lotesits li ON oc.LotesIts = li.Controle');
  DmLotes.QrSacOcorA.SQL.Add('LEFT JOIN lotes    lo ON lo.Codigo   = li.Codigo');
  DmLotes.QrSacOcorA.SQL.Add('LEFT JOIN ocorbank ob ON ob.Codigo   = oc.Ocorrencia');
  DmLotes.QrSacOcorA.SQL.Add('WHERE oc.Status<2');
  //if FNomeSacado <> '' then
    //DmLotes.QrSacOcorA.SQL.Add('AND li.Emitente LIKE "'+FNomeSacado+'"');
  if FCPFSacado <> '' then
    DmLotes.QrSacOcorA.SQL.Add('AND li.CPF = "'+FCPFSacado+'"');
  DmLotes.QrSacOcorA.SQL.Add('');
  DmLotes.QrSacOcorA.Open;
  //
  //if FOcorA > 0 then DmLotes.QrSacOcorA.Locate('Codigo', FOcorA, []);
end;

procedure TFmLotes1.ReopenSacCHDevA;
begin
  DmLotes.QrSacCHDevA.Close;
  DmLotes.QrSacCHDevA.SQL.Clear;
  DmLotes.QrSacCHDevA.SQL.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  DmLotes.QrSacCHDevA.SQL.Add('ELSE en.Nome END NOMECLIENTE, ai.*');
  DmLotes.QrSacCHDevA.SQL.Add('FROM alinits ai');
  DmLotes.QrSacCHDevA.SQL.Add('LEFT JOIN entidades en ON en.Codigo=ai.Cliente');
  if FmPrincipal.FConnections = 0 then
  begin
    //DmLotes.QrSacCHDevA.SQL.Add('LEFT JOIN lotesits li ON li.Controle=ai.ChequeOrigem');
    DmLotes.QrSacCHDevA.SQL.Add('LEFT JOIN lotes lo ON ai.LoteOrigem=lo.Codigo');
  end;
  DmLotes.QrSacCHDevA.SQL.Add('WHERE ai.Status<2');
  if FmPrincipal.FConnections = 0 then
  begin
    DmLotes.QrSacCHDevA.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      Geral.FF0(FmPrincipal.FConnections)+' >= 0.01');
  end;
  if FCPFSacado <> '' then
    DmLotes.QrSacCHDevA.SQL.Add('AND ai.CPF = "'+FCPFSacado+'"');
  DmLotes.QrSacCHDevA.SQL.Add('ORDER BY ai.Data1, ai.Data2');
  DmLotes.QrSacCHDevA.SQL.Add('');
  DmLotes.QrSacCHDevA.Open;
end;

procedure TFmLotes1.ReopenSacDOpen;
begin
  DmLotes.QrSacDOpen.Close;
  DmLotes.QrSacDOpen.SQL.Clear;
  DmLotes.QrSacDOpen.SQL.Add('SELECT od.Nome STATUS, li.Controle, li.Duplicata, li.Repassado, ');
  DmLotes.QrSacDOpen.SQL.Add('li.DCompra, li.Valor, li.DDeposito, li.Emitente, li.CPF,');
  DmLotes.QrSacDOpen.SQL.Add('lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg,');
  DmLotes.QrSacDOpen.SQL.Add('li.Vencto, li.Data3');
  DmLotes.QrSacDOpen.SQL.Add('FROM lotesits li');
  DmLotes.QrSacDOpen.SQL.Add('LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo');
  DmLotes.QrSacDOpen.SQL.Add('LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao');
  DmLotes.QrSacDOpen.SQL.Add('WHERE lo.Tipo=1');
  DmLotes.QrSacDOpen.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      Geral.FF0(FmPrincipal.FConnections)+' >= 0.01');
  DmLotes.QrSacDOpen.SQL.Add('AND li.Quitado <> 2');
  if FCPFSacado <> '' then
    DmLotes.QrSacDOpen.SQL.Add('AND li.CPF = "'+FCPFSacado+'"');
  DmLotes.QrSacDOpen.SQL.Add('ORDER BY li.Vencto');
  DmLotes.QrSacDOpen.SQL.Add('');
  DmLotes.QrSacDOpen.Open;
end;

procedure TFmLotes1.ReopenSacRiscoC;
begin
  DmLotes.QrSacRiscoC.Close;
  DmLotes.QrSacRiscoC.SQL.Clear;
  DmLotes.QrSacRiscoC.SQL.Add('SELECT lo.Tipo, li.Banco, li.Agencia, li.Conta, li.Cheque, li.Valor,');
  DmLotes.QrSacRiscoC.SQL.Add('li.DCompra, li.DDeposito, li.Emitente, li.CPF');
  DmLotes.QrSacRiscoC.SQL.Add('FROM lotesits li');
  DmLotes.QrSacRiscoC.SQL.Add('LEFT JOIN lotes lo ON lo.Codigo=li.Codigo');
  DmLotes.QrSacRiscoC.SQL.Add('WHERE lo.Tipo = 0');
  DmLotes.QrSacRiscoC.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
    Geral.FF0(FmPrincipal.FConnections)+' >= 0.01');
  DmLotes.QrSacRiscoC.SQL.Add('AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())');
  if FCPFSacado <> '' then
    DmLotes.QrSacRiscoC.SQL.Add('AND li.CPF = "'+FCPFSacado+'"');
  DmLotes.QrSacRiscoC.SQL.Add('ORDER BY DDeposito');
  DmLotes.QrSacRiscoC.Open;
  //
  DmLotes.QrSacRiscoTC.Close;
  DmLotes.QrSacRiscoTC.SQL.Clear;
  DmLotes.QrSacRiscoTC.SQL.Add('SELECT SUM(li.Valor) Valor');
  DmLotes.QrSacRiscoTC.SQL.Add('FROM lotesits li');
  DmLotes.QrSacRiscoTC.SQL.Add('LEFT JOIN lotes lo ON lo.Codigo=li.Codigo');
  DmLotes.QrSacRiscoTC.SQL.Add('WHERE lo.Tipo = 0');
  DmLotes.QrSacRiscoTC.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      Geral.FF0(FmPrincipal.FConnections)+' >= 0.01');
  DmLotes.QrSacRiscoTC.SQL.Add('AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())');
  if FCPFSacado <> '' then
    DmLotes.QrSacRiscoTC.SQL.Add('AND li.CPF = "'+FCPFSacado+'"');
  DmLotes.QrSacRiscoTC.Open;
  //
end;

procedure TFmLotes1.FechaTudoSac;
begin
  DmLotes.QrSacOcorA.Close;
  DmLotes.QrSacCHDevA.Close;
  DmLotes.QrSacRiscoC.Close;
  DmLotes.QrSacRiscoTC.Close;
  DmLotes.QrSacDOpen.Close;
end;

procedure TFmLotes1.EdCNPJChange(Sender: TObject);
begin
  EdIE.Text      := '';
  EdSacado.Text  := '';
  EdRua.Text     := '';
  EdNumero.Text  := '';
  EdCompl.Text   := '';
  EdBairro.Text  := '';
  EdCidade.Text  := '';
  CBUF.ItemIndex := -1;
  CBUF.Text      := '';
  EdCEP.Text     := '';
  FechaTudoSac;
end;

procedure TFmLotes1.EdCPFChange(Sender: TObject);
begin
  FechaTudoSac;
  EdStatusSPC.ValueVariant := -1;
end;

procedure TFmLotes1.GradeC_LTitleClick(Column: TColumn);
var
  Antigo: String;
begin
  Antigo := Dmoc.FORDA_LCHs;
  Dmoc.FORDA_LCHs := Column.FieldName;
  if Dmoc.FORDA_LCHs = 'CPF_TXT' then Dmoc.FORDA_LCHs := 'CPF';
  if Dmoc.FORDA_LCHs = 'NOMECHPLICADO' then Dmoc.FORDA_LCHs := 'Duplicado';
  if Dmoc.FORDA_LCHs = 'DIAS' then Dmoc.FORDA_LCHs := 'DVence';
  if Antigo = Dmoc.FORDA_LCHs then
  Dmoc.FORDB_LCHs := MLAGeral.InverteOrdemAsc(Dmoc.FORDB_LCHs);
  Dmoc.ReopenImportLote(Dmoc.QrLCHsOrdem.Value, Dmoc.QrLDUsOrdem.Value);
end;

procedure TFmLotes1.GradeD_LTitleClick(Column: TColumn);
var
  Antigo: String;
begin
  Antigo := Dmoc.FORDA_LDUs;
  Dmoc.FORDA_LDUs := Column.FieldName;
  if Dmoc.FORDA_LDUs = 'CPF_TXT' then Dmoc.FORDA_LDUs := 'CPF';
  if Dmoc.FORDA_LDUs = 'NOMEDUPLICADO' then Dmoc.FORDA_LDUs := 'Duplicado';
  if Dmoc.FORDA_LDUs = 'DIAS' then Dmoc.FORDA_LDUs := 'DVence';
  if Antigo = Dmoc.FORDA_LDUs then
  Dmoc.FORDB_LDUs := MLAGeral.InverteOrdemAsc(Dmoc.FORDB_LDUs);
  Dmoc.ReopenImportLote(Dmoc.QrLCHsOrdem.Value, Dmoc.QrLDUsOrdem.Value);
end;

procedure TFmLotes1.AtualizaAcumulado;
var
  Disponiv: Double;
begin
  Dmoc.QrValAcum.Close;
  Dmoc.QrValAcum.Open;
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE importlote SET ValAcum=:P0, Disponiv=:P1');
  Dmod.QrUpdL.SQL.Add('WHERE CPF=:Px');
  Dmod.QrUpdL.SQL.Add('');
  Dmod.QrUpdL.SQL.Add('');
  //
  ProgressBar1.Visible := True;
  ProgressBar1.Position := 0;
  ProgressBar1.Max := Dmoc.QrValAcum.RecordCount;
  ProgressBar1.Refresh;
  ProgressBar1.Update;
  Application.ProcessMessages;
  while not Dmoc.QrValAcum.Eof do
  begin
    ProgressBar1.Position := ProgressBar1.Position + 1;
    Application.ProcessMessages;
    Disponiv := (Dmoc.QrValAcumRISCOSA.Value + Dmoc.QrValAcumRISCOEM.Value) -
      Dmoc.QrValAcumAberto.Value - Dmoc.QrValAcumValor.Value;
    Dmod.QrUpdL.Params[0].AsFloat   := Dmoc.QrValAcumValor.Value;
    Dmod.QrUpdL.Params[1].AsFloat   := Disponiv;
    Dmod.QrUpdL.Params[2].AsString  := Dmoc.QrValAcumCPF.Value;
    Dmod.QrUpdL.ExecSQL;
    Dmoc.QrValAcum.Next;
  end;
  ProgressBar1.Visible := False;
end;

procedure TFmLotes1.EdBanco0Exit(Sender: TObject);
begin
  ReopenBanco0;
end;

procedure TFmLotes1.EdBanco0Change(Sender: TObject);
begin
  if not EdBanco0.Focused then ReopenBanco0;
end;

procedure TFmLotes1.Detalhado1Click(Sender: TObject);
begin
  MostraEdicao(10, CO_INCLUSAO, 0);
end;

procedure TFmLotes1.Prconfigurado1Click(Sender: TObject);
var
  i: integer;
begin
  if DBGrid10.SelectedRows.Count > 0 then
  begin
    if Geral.MB_Pergunta('Confirma o pagamento de todos cheques selecionados?') = ID_YES then
    begin
      with DBGrid10.DataSource.DataSet do
      for i:= 0 to DBGrid10.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGrid10.SelectedRows.Items[i]));
        PagaChequeDevolvido;
      end;
    end;
  end else PagaChequeDevolvido;
  CalculaLote(QrLotesCodigo.Value, False);
  LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value);
  ReopenCHDevA(QrLotesCliente.Value);
  ReopenCHDevP;
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmLotes1.PagaChequeDevolvido;
var
  Codigo: Integer;
begin
  Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'AlinPgs', 'AlinPgs', 'Codigo');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO alinpgs SET AlterWeb=1, LotePg=' + Geral.FF0(QrLotesCodigo.Value)+',');
  Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2');
  Dmod.QrUpd.SQL.Add(', AlinIts=:Pa, Codigo=:Pb');
  Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, QrLotesData.Value);
  Dmod.QrUpd.Params[01].AsFloat   := DmLotes.QrCHDevAATUAL.Value-DmLotes.QrCHDevASALDO.Value;
  Dmod.QrUpd.Params[02].AsFloat   := DmLotes.QrCHDevAATUAL.Value;
  Dmod.QrUpd.Params[03].AsInteger := DmLotes.QrCHDevACodigo.Value;
  Dmod.QrUpd.Params[04].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  CalculaPagtoAlinIts(QrLotesData.Value, DmLotes.QrCHDevACodigo.Value);
  FCHDevA := DmLotes.QrCHDevACodigo.Value;
  FCHDevP := Codigo;
end;

procedure TFmLotes1.SbImprimeClick(Sender: TObject);
begin
  FImprime := 2;
  MyObjects.MostraPopUpDeBotao(PMBordero, SbImprime);
end;

procedure TFmLotes1.BtCheckImpClick(Sender: TObject);
begin
  FImprime := 0;
  Application.CreateForm(TFmLotesDlg, FmLotesDlg);
  FmLotesDlg.ShowModal;
  try
    if FImprime > 0 then
    begin
      FmLotesDlg.Hide;
      ExecutaListaDeImpressoes;
    end;
  finally
    FmLotesDlg.Destroy;
  end;
end;

procedure TFmLotes1.ImprimeBorderoClienteBP;
begin
  FContaST := 0;
  QrSumOP.Close;
  QrSumOP.Params[0].AsInteger := QrLotesCodigo.Value;
  QrSumOP.Open;
  //
  if QrLotesMINAV.Value+QrLotesMINTC.Value>=0.01 then
  begin
    if QrLotesTipo.Value = 0 then
    begin
      frxBorTotCliE2.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE2.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE2.Print; //PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliBP2, 'Border� cliente n� ' + Geral.FF0(QrLotesLote.Value));
      end;
    end else begin
      frxBorTotCliE3.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE3.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE3.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliBP3, 'Border� cliente n� ' + Geral.FF0(QrLotesLote.Value));
      end;
    end;
  end else begin
    if QrLotesTipo.Value = 0 then
    begin
      frxBorTotCliE0.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE0.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE0.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliBP0, 'Border� cliente n� ' + Geral.FF0(QrLotesLote.Value));
      end;
    end else begin
      frxBorTotCliE1.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE1.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE1.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliBP1, 'Border� cliente n� ' + Geral.FF0(QrLotesLote.Value));
      end;
    end;
  end;
end;

procedure TFmLotes1.ImprimeBorderoClienteE2;
begin
  FContaST := 0;
  QrSumOP.Close;
  QrSumOP.Params[0].AsInteger := QrLotesCodigo.Value;
  QrSumOP.Open;
  //
  if QrLotesMINAV.Value+QrLotesMINTC.Value>=0.01 then
  begin
    if QrLotesTipo.Value = 0 then
    begin
      frxBorTotCliE2.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE2.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE2.Print; //PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliE2, 'Border� cliente n� ' + Geral.FF0(QrLotesLote.Value));
      end;
    end else begin
      frxBorTotCliE3.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE3.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE3.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliE3, 'Border� cliente n� ' + Geral.FF0(QrLotesLote.Value));
      end;
    end;
  end else begin
    if QrLotesTipo.Value = 0 then
    begin
      frxBorTotCliE0.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE0.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE0.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliE0, 'Border� cliente n� ' + Geral.FF0(QrLotesLote.Value));
      end;
    end else begin
      frxBorTotCliE1.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCliE1.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCliE1.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCliE1, 'Border� cliente n� ' + Geral.FF0(QrLotesLote.Value));
      end;
    end;
  end;
end;

procedure TFmLotes1.ImprimeBorderoContabilNovo;
begin
  if QrLotesMINAV.Value+QrLotesMINTC.Value>=0.01 then
  begin
    if QrLotesTipo.Value = 0 then
    begin
      case FImprime of
        1: MyObjects.frxImprime(frxBorProFis0_A, 'Border� Cont�bil');
        2: MyObjects.frxMostra(frxBorProFis0_A, 'Border� Cont�bil');
      end;
    end else begin
      case FImprime of
        1: MyObjects.frxImprime(frxBorProFis1_A, 'Border� Cont�bil');
        2: MyObjects.frxMostra(frxBorProFis1_A, 'Border� Cont�bil');
      end;
    end;
  end;
end;

procedure TFmLotes1.ImprimeAditivoNPAPNovo;
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(2);
  Dmod.VerificaDefinicaoDeTextoPadrao(3);
  ReopenContrat;
  ReopenSaCart(istTodos);
  //
  case FImprime of
    1: MyObjects.frxImprime(frxAditivo_NP_AP, 'Aditivo + Nota promiss�ria + Autoriza��o de protesto');
    2: MyObjects.frxMostra(frxAditivo_NP_AP, 'Aditivo + Nota promiss�ria + Autoriza��o de protesto');
  end;
end;

procedure TFmLotes1.ImprimeAditivoAPNPNovo;
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(2);
  Dmod.VerificaDefinicaoDeTextoPadrao(3);
  ReopenContrat;
  ReopenSaCart(istTodos);
  //
  case FImprime of
    1: MyObjects.frxImprime(frxAditivo_AP_NP, 'Aditivo + Autoriza��o de protesto + Nota Promoss�ria');
    2: MyObjects.frxMostra(frxAditivo_AP_NP, 'Aditivo + Autoriza��o de protesto + Nota Promoss�ria');
  end;
end;

procedure TFmLotes1.ImprimeAditivoNPNovo;
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(2);
  ReopenContrat;
  //
  frxAditivo_NP.PrepareReport;
  case FImprime of
    1: frxAditivo_NP.Print;//PreparedReport('', 1, True, frAll);
    2: MyObjects.frxMostra(frxAditivo_NP, 'Aditivo + NP');
  end;
end;

procedure TFmLotes1.ImprimeAditivoNovo;
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(2);
  ReopenContrat;
  //
  frxAditivo.PrepareReport;
  case FImprime of
    1: frxAditivo.Print;//PreparedReport('', 1, True, frAll);
    2: MyObjects.frxMostra(frxAditivo, 'Aditivo');
  end;
end;

procedure TFmLotes1.ImprimeNotaPromissoriaNovo;
begin
  frxPromissoria.PrepareReport;
  case FImprime of
    1: frxPromissoria.Print;//PreparedReport('', 1, True, frAll);
    2: MyObjects.frxMostra(frxPromissoria, 'Nota promiss�ria');
  end;
end;

procedure TFmLotes1.ImprimeAutorizacaodeProtestoNovo;
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(3);
  ReopenSaCart(istTodos);
  //
  case FImprime of
    1: MyObjects.frxImprime(frxProtesto, 'Autoriza��o de Protesto');
    2: MyObjects.frxMostra(frxProtesto, 'Autoriza��o de Protesto');
  end;
end;

procedure TFmLotes1.ImprimeMargemColigadasNovo;
begin
  FImprimindoColigadas := True;
  FDadosJurosColigadas := Dmod.ObtemEmLotIts(QrLotesItsControle.Value,
    QrLotesItsTxaCompra.Value, QrLotesItsTxaJuros.Value,
    QrLotesItsVlrCompra.Value);
  if QrLotesTipo.Value = 0 then
  begin
    case FImprime of
      1: MyObjects.frxImprime(frxColigadas0, 'Margem de Coligadas');
      2: MyObjects.frxMostra(frxColigadas0, 'Margem de Coligadas');
    end;
  end else begin
    case FImprime of
      1: MyObjects.frxImprime(frxColigadas1, 'Margem de Coligadas');
      2: MyObjects.frxMostra(frxColigadas1, 'Margem de Coligadas');
    end;
  end;
  FImprimindoColigadas := False;
end;

procedure TFmLotes1.ExecutaListaDeImpressoes;
begin
  Screen.Cursor := crHourGlass;
  try
    if MLAGeral.IntInConjunto2(002, FRelatorios) then ImprimeBorderoClienteE2;
    if MLAGeral.IntInConjunto2(004, FRelatorios) then ImprimeBorderoContabilNovo;
    if MLAGeral.IntInConjunto2(008, FRelatorios) then ImprimeAditivoNPAPNovo;
    if MLAGeral.IntInConjunto2(016, FRelatorios) then ImprimeAditivoNPNovo;
    if MLAGeral.IntInConjunto2(032, FRelatorios) then ImprimeAditivoNovo;
    if MLAGeral.IntInConjunto2(064, FRelatorios) then ImprimeNotaPromissoriaNovo;
    if MLAGeral.IntInConjunto2(128, FRelatorios) then ImprimeAutorizacaodeProtestoNovo;
    if MLAGeral.IntInConjunto2(512, FRelatorios) then ImprimeMargemColigadasNovo;
    // cuidado com envelopes !!!
    if MLAGeral.IntInConjunto2(256, FRelatorios) then ImprimeCartaAoSacadoDados;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLotes1.BtRiscoSacClick(Sender: TObject);
begin
  Application.CreateForm(TFmRiscoAll, FmRiscoAll);
  if PainelImporta.Visible then
  begin
    if Pagina.ActivePageIndex = 0 then
      FmRiscoAll.EdCPF.Text := Geral.FormataCNPJ_TT(Dmoc.QrLCHsCPF.Value)
    else FmRiscoAll.EdCPF.Text := Geral.FormataCNPJ_TT(Dmoc.QrLDUsCPF.Value)
  end else FmRiscoAll.EdCPF.Text := Geral.FormataCNPJ_TT(QrLotesItsCPF.Value);
  FmRiscoAll.BtPesquisaClick(Self);
  FmRiscoAll.ShowModal;
  FmRiscoAll.Destroy;
end;

function TFmLotes1.RiscoSacado(CPF: String): Double;
var
  Valor: Double;
  Cliente: Integer;
begin
  Valor := 0;
  DmLotes.QrSRS.Close;
  DmLotes.QrSRS.Params[0].AsString := CPF;
  DmLotes.QrSRS.Open;
  //
  DmLotes.QrTCD.Close;
  DmLotes.QrTCD.Params[0].AsString := CPF;
  DmLotes.QrTCD.Open;
  while not DmLotes.QrTCD.Eof do
  begin
    Valor := Valor + DMod.ObtemValorAtualizado(
    DmLotes.QrTCDCliente.Value, DmLotes.QrTCDStatus.Value, DmLotes.QrTCDData1.Value, Date,
    DmLotes.QrTCDData3.Value, DmLotes.QrTCDValor.Value, DmLotes.QrTCDJurosV.Value,
    DmLotes.QrTCDDesconto.Value, DmLotes.QrTCDValPago.Value, DmLotes.QrTCDJurosP.Value, False);
    //
    DmLotes.QrTCD.Next;
  end;
  //
  DmLotes.QrSDO.Close;
  DmLotes.QrSDO.Params[0].AsString := CPF;
  DmLotes.QrSDO.Open;
  while not DmLotes.QrSDO.Eof do
  begin
    Valor := Valor + DMod.ObtemValorAtualizado(
    DmLotes.QrSDOCliente.Value, DmLotes.QrSDOQuitado.Value, DmLotes.QrSDOVencto.Value, Date,
    DmLotes.QrSDOData3.Value, DmLotes.QrSDOValor.Value, DmLotes.QrSDOTotalJr.Value, DmLotes.QrSDOTotalDs.Value,
    DmLotes.QrSDOTotalPg.Value, 0, True);
    //
    DmLotes.QrSDO.Next;
  end;
  //
  DmLotes.QrSOA.Close;
  DmLotes.QrSOA.Params[0].AsString := CPF;
  DmLotes.QrSOA.Open;
  while not DmLotes.QrSOA.Eof do
  begin
    if DmLotes.QrSOACLIENTELOTE.Value > 0 then
      Cliente := DmLotes.QrSOACLIENTELOTE.Value else
      Cliente := DmLotes.QrSOACliente.Value;
    //
    Valor := Valor + DMod.ObtemValorAtualizado(
    Cliente, 1, DmLotes.QrSOADataO.Value, Date, DmLotes.QrSOAData3.Value,
    DmLotes.QrSOAValor.Value, DmLotes.QrSOATaxaV.Value, 0 (*Desco*),
    DmLotes.QrSOAPago.Value, DmLotes.QrSOATaxaP.Value, False);
    //
    DmLotes.QrSOA.Next;
  end;
  //
  Result := Valor + DmLotes.QrSRSValor.Value;
end;

procedure TFmLotes1.EdNFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key=VK_F4 then DefineNovaNF;
end;

procedure TFmLotes1.DefineNovaNF;
begin
  DmLotes.QrLocNF.Close;
  DmLotes.QrLocNF.Open;
  EdNF.Text := MLAGeral.FFD(DmLotes.QrLocNFCodigo.Value + 1, 6, siNegativo);
end;

procedure TFmLotes1.VerificaCPFCNPJatual1Click(Sender: TObject);
begin
  Dmoc.VerificaCPFCNP_XX_Importacao(EdNomCli.Text, Geral.IMV(EdCodCli.Text),
    Pagina.ActivePageIndex, istAtual);
end;

procedure TFmLotes1.VerificaNF(NF: Integer);
begin
  if NF = 0 then Exit;
  DmLotes.QrVerNF.Close;
  DmLotes.QrVerNF.Params[0].AsInteger := NF;
  DmLotes.QrVerNF.Open;
  //
  if DmLotes.QrVerNFCodigo.Value = 0 then
  begin
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('INSERT INTO nfs SET ');
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz');
    Dmod.QrUpdU.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
    Dmod.QrUpdU.Params[01].AsInteger := VAR_USUARIO;
    Dmod.QrUpdU.Params[02].AsInteger := NF;
    Dmod.QrUpdU.ExecSQL;
  end;
end;

procedure TFmLotes1.EdCMC_7Change(Sender: TObject);
var
  Banda: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC_7.Text) = 0 then
  begin
    Banda := TBandaMagnetica.Create;
    Banda.BandaMagnetica := EdCMC_7.Text;
    EdTipific.Text       := Banda.Tipo;
    EdRegiaoCompe.Text   := Banda.Compe;
    EdBanco.Text   := Banda.Banco;
    EdAgencia.Text := Banda.Agencia;
    EdConta.Text   := Banda.Conta;
    EdCheque.Text  := Banda.Numero;
    // precisa para n�o pular direto para a data
    EdValor.Text := '';
    //
    EdValor.SetFocus;
    BtConfirma2.Enabled := True;
  end;
end;

procedure TFmLotes1.EdVenctoEnter(Sender: TObject);
begin
  if Geral.DMV(EdValor.Text) = 0 then EdValor.SetFocus;
end;

procedure TFmLotes1.QrSaCartCalcFields(DataSet: TDataSet);
begin
  QrSaCartNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrSaCartRua.Value, Trunc(QrSaCartNumero.Value), False);
  QrSaCartLNR.Value := '';//QrSaCartNOMELOGRAD.Value;
  if Trim(QrSaCartLNR.Value) <> '' then QrSaCartLNR.Value :=
    QrSaCartLNR.Value + ' ';
  QrSaCartLNR.Value := QrSaCartLNR.Value + QrSaCartRua.Value;
  if Trim(QrSaCartRua.Value) <> '' then QrSaCartLNR.Value :=
    QrSaCartLNR.Value + ', ' + QrSaCartNUMERO_TXT.Value;
  if Trim(QrSaCartCompl.Value) <>  '' then QrSaCartLNR.Value :=
    QrSaCartLNR.Value + ' ' + QrSaCartCompl.Value;
  if Trim(QrSaCartBairro.Value) <>  '' then QrSaCartLNR.Value :=
    QrSaCartLNR.Value + ' - ' + QrSaCartBairro.Value;
  //
  QrSaCartLN2.Value := '';
  if Trim(QrSaCartCidade.Value) <>  '' then QrSaCartLN2.Value :=
    QrSaCartLN2.Value + QrSaCartCIDADE.Value;
  QrSaCartLN2.Value := QrSaCartLN2.Value + ' - '+QrSaCartUF.Value;
  if QrSaCartCEP.Value > 0 then QrSaCartLN2.Value :=
    QrSaCartLN2.Value + '     CEP ' + Geral.FormataCEP_NT(QrSaCartCEP.Value);
  //
  QrSaCartCUC.Value := QrSaCartLNR.Value+ '   ' +QrSaCartLN2.Value;
  //
  QrSaCartCEP_TXT.Value := Geral.FormataCEP_NT(QrSaCartCEP.Value);
  //
end;

procedure TFmLotes1.QrSaEmitCHAfterOpen(DataSet: TDataSet);
begin
  DmLotes.QrDupSac.Close;
  DmLotes.QrDupSac.Params[0].AsInteger := QrSaEmitCHCodigo.Value;
  DmLotes.QrDupSac.Params[1].AsString  := QrSaEmitCHCPF.Value;
  DmLotes.QrDupSac.Open;
end;

procedure TFmLotes1.QrSaEmitCHAfterScroll(DataSet: TDataSet);
begin
  DmLotes.QrDupSac.Close;
  DmLotes.QrDupSac.Params[0].AsInteger := QrSaEmitCHCodigo.Value;
  DmLotes.QrDupSac.Params[1].AsString  := QrSaEmitCHCPF.Value;
  DmLotes.QrDupSac.Open;
end;

procedure TFmLotes1.BtOcorenciasClick(Sender: TObject);
begin
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmEntiJur1, FmEntiJur1);
  finally
    Screen.Cursor := crDefault;
  end;
  FmEntiJur1.ShowModal;
  FmEntiJur1.Destroy;
end;

procedure TFmLotes1.BitBtn3Click(Sender: TObject);
begin
  Application.CreateForm(TFmChequesGer, FmChequesGer);
  FmChequesGer.ShowModal;
  FmChequesGer.Destroy;
end;

procedure TFmLotes1.BitBtn4Click(Sender: TObject);
begin
  Application.CreateForm(TFmDuplicatas2, FmDuplicatas2);
  FmDuplicatas2.ShowModal;
  FmDuplicatas2.Destroy;
end;

procedure TFmLotes1.BitBtn2Click(Sender: TObject);
begin
  Application.CreateForm(TFmOcorreuC, FmOcorreuC);
  FmOcorreuC.EdCliente.Text := Geral.FF0(QrLotesCliente.Value);
  FmOcorreuC.CBCliente.KeyValue := QrLotesCliente.Value;
  FmOcorreuC.ShowModal;
  FmOcorreuC.Destroy;
end;

procedure TFmLotes1.AtualizaImpressaoDeCarta(Lote, Tipo: Integer);
var
  Campo: String;
begin
  if Tipo = 0 then
    Campo := 'ECartaSac' //Cheques
  else
    Campo := 'ECartaEmCH';//Duplicatas
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotes SET AlterWeb=1, ' + Campo + '=' + Campo + '+1');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=' + Geral.FF0(Lote));
  Dmod.QrUpd.ExecSQL;
  FmPrincipal.AtualizaSB2(2);
end;

procedure TFmLotes1.EdRegiaoCompeChange(Sender: TObject);
begin
  CalculaDiasCH;
end;

procedure TFmLotes1.EdAgenciaChange(Sender: TObject);
begin
  LocalizaEmitente(True);
end;

procedure TFmLotes1.EdContaChange(Sender: TObject);
begin
  LocalizaEmitente(True);
end;

procedure TFmLotes1.EdCodCliChange(Sender: TObject);
begin
  VerificaNomeimport;
end;

procedure TFmLotes1.EdNomCliChange(Sender: TObject);
begin
  VerificaNomeimport;
end;

procedure TFmLotes1.VerificaNomeimport;
begin
  DmLotes.QrLocCliImp.Close;
  DmLotes.QrLocCliImp.Params[0].AsInteger := Geral.IMV(EdCodCli.Text);
  DmLotes.QrLocCliImp.Open;
  if DmLotes.QrLocCliImpNOMECLI.Value <> EdNomCli.Text then
  begin
    EdNomCli.Font.Color := clRed;
    EdCodCli.Font.Color := clRed;
    EdNomCli.Color := clYellow;
    EdCodCli.Color := clYellow;
  end else begin
    EdNomCli.Font.Color := clBlue;
    EdCodCli.Font.Color := clBlue;
    EdNomCli.Color := clWhite;
    EdCodCli.Color := clWhite;
  end;
end;

procedure TFmLotes1.VerificatodosCPFCNPJaindanoverificados1Click(
  Sender: TObject);
begin
  Dmoc.VerificaCPFCNP_XX_Importacao(EdNomCli.Text, Geral.IMV(EdCodCli.Text),
    Pagina.ActivePageIndex, istExtra1);
end;

procedure TFmLotes1.VerificatodosCPFCNPJpelovalormnimoconfigurao1Click(
  Sender: TObject);
begin
  Dmoc.VerificaCPFCNP_XX_Importacao(EdNomCli.Text, Geral.IMV(EdCodCli.Text),
    Pagina.ActivePageIndex, istExtra2);
end;

procedure TFmLotes1.BtIncluiocorClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMIncluiOcor, BtIncluiOcor);
end;

procedure TFmLotes1.Cliente1Click(Sender: TObject);
begin
  Application.CreateForm(TFmOcorreuC, FmOcorreuC);
  FmOcorreuC.FCliente := QrLotesCliente.Value;
  FmOcorreuC.ShowModal;
  FmOcorreuC.Destroy;
  //
  ReopenOcorA(QrLotesCliente.Value);
end;

procedure TFmLotes1.Cheque1Click(Sender: TObject);
begin
  Application.CreateForm(TFmChequesGer, FmChequesGer);
  FmChequesGer.FCliente := QrLotesCliente.Value;
  FmChequesGer.ShowModal;
  FmChequesGer.Destroy;
  //
  ReopenOcorA(QrLotesCliente.Value);
end;

procedure TFmLotes1.Duplicata1Click(Sender: TObject);
begin
  FmPrincipal.MostraDuplicatas2(0, QrLotesCliente.Value);
  //
  ReopenOcorA(QrLotesCliente.Value);
end;

procedure TFmLotes1.QrLotesItsBeforeClose(DataSet: TDataSet);
begin
  BtInclui.Enabled     := False;
  BtAltera2.Enabled    := False;
  BtExclui2.Enabled    := False;
  BitBtn7.Enabled      := False;
  BtSPC_LI.Enabled     := False;
  BtImportaBCO.Enabled := False;
  BtWeb.Enabled        := False;
end;

procedure TFmLotes1.BtExcluiClick(Sender: TObject);
begin
  if QrPagtos.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Este lote n�o pode ser exclu�do pois possui pagamentos!');
    Exit;
  end;
  if DmLotes.QrRepCli.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Este lote n�o pode ser exclu�do pois possui pagamentos!');
    Exit;
  end;
  if QrDPago.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Este lote n�o pode ser exclu�do pois possui duplicatas pagas nele!');
    Exit;
  end;
  if QrCHDevP.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Este lote n�o pode ser exclu�do pois possui cheques pagos nele!');
    Exit;
  end;
  if QrOcorP.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Este lote n�o pode ser exclu�do pois possui ocorr�ncias pagas nele!');
    Exit;
  end;
  if QrLotesTxs.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Este lote n�o pode ser exclu�do pois possui taxas inseridas nele!');
    Exit;
  end;
  if (QrLotesIts.RecordCount = 0) and (QrLotesCodigo.Value > 0) then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o do Border� - (' +
      'controle ' + Geral.FF0(QrLotesCodigo.Value) + ')?') = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM lotes WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrLotesCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value);
    end;
  end;
end;

procedure TFmLotes1.QrJuros0CalcFields(DataSet: TDataSet);
var
  Total: MyArrayR07;
begin
  Total := Dmod.ObtemValorDeCompraRealizado(QrJuros0Controle.Value);
  QrJuros0VlrOutras.Value := Total[0];
  QrJuros0VlrTotal.Value := Total[0] + QrJuros0VlrCompra.Value;
  //
  QrJuros0CPF_TXT.Value := Geral.FormataCNPJ_TT(QrJuros0CPF.Value);
  //
  QrJuros0ITEM.Value := 1;
end;

function TFmLotes1.CriaBAC_Pesq(Banco, Agencia, Conta: String; DVCC: Integer): String;
begin
  EdRealCC.Text := Copy(Conta, 10-DVCC+1, DVCC);
  Result := Banco+Agencia+EdRealCC.Text;
end;

procedure TFmLotes1.EdCPFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    Dmod.QrLocCPF.Close;
    Dmod.QrLocCPF.Params[0].AsString := Geral.SoNumero_TT(EdCPF.Text);
    Dmod.QrLocCPF.Open;
    case Dmod.QrLocCPF.RecordCount of
      0: Geral.MB_Aviso('N�o foi localizado emitente para este CPF!');
      1: EdEmitente.Text := Dmod.QrLocCPFNome.Value;
      else begin
        Application.CreateForm(TFmLotesEmi, FmLotesEmi);
        FmLotesEmi.ShowModal;
        if FmLotesEmi.FEmitenteNome <> '' then
          EdEmitente.Text := FmLotesEmi.FEmitenteNome;
        FmLotesEmi.Destroy;
      end;
    end;
  end;
end;

procedure TFmLotes1.BtHistoricoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMHistorico, BtHistorico);
end;

procedure TFmLotes1.MostraHistorico(Quem: Integer);
begin
  Application.CreateForm(TFmHistCliEmi, FmHistCliEmi);
  if Quem in ([1,3]) then
  begin
    FmHistCliEmi.EdCliente.Text := Geral.FF0(QrLotesCliente.Value);
    FmHistCliEmi.CBCliente.KeyValue := QrLotesCliente.Value;
  end;
  if Quem in ([2,3]) then
  begin
    if PainelImporta.Visible then
    begin
      if Pagina.ActivePageIndex = 0 then
        FmHistCliEmi.EdCPF1.Text := Geral.FormataCNPJ_TT(Dmoc.QrLCHsCPF.Value)
      else FmHistCliEmi.EdCPF1.Text := Geral.FormataCNPJ_TT(Dmoc.QrLDUsCPF.Value)
    end else FmHistCliEmi.EdCPF1.Text := Geral.FormataCNPJ_TT(QrLotesItsCPF.Value);
  end;
  FmHistCliEmi.Pesquisa;
  FmHistCliEmi.ShowModal;
  FmHistCliEmi.Destroy;
end;

procedure TFmLotes1.MostrarDias1Click(Sender: TObject);
begin
  ImprimeBorderoClienteSobra(True);
end;

procedure TFmLotes1.Clienteselecionado1Click(Sender: TObject);
begin
  MostraHistorico(1);
end;

procedure TFmLotes1.Emitecheque1Click(Sender: TObject);
begin
  MyPrinters.EmiteCheque(
    QrPagtosControle.Value, QrPagtosSub.Value, QrPagtosBanco1.Value,
    0, '', 0, '', QrPagtosSerieCH.Value, QrPagtosDocumento.Value,
    QrPagtosData.Value, QrPagtosCarteira.Value, 'NOMECLI', GridPagtos,
    LAN_CTOS, -1, 0, 0);
end;

procedure TFmLotes1.Emitentesacadoselecionado1Click(Sender: TObject);
begin
  MostraHistorico(2);
end;

procedure TFmLotes1.Ambos1Click(Sender: TObject);
begin
  MostraHistorico(3);
end;

procedure TFmLotes1.ExcluiChequeSelecionado;
var
  i: Integer;
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM lotesits WHERE Codigo=:P0 AND Controle=:P1');
  Dmod.QrUpd.Params[0].AsInteger := QrLotesItsCodigo.Value;
  Dmod.QrUpd.Params[1].AsInteger := QrLotesItsControle.Value;
  Dmod.QrUpd.ExecSQL;
  //
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      Dmod.QrUpd_.Database := Dmod.FArrayMySQLBD[i];
      Dmod.QrUpd_.SQL.Clear;
      Dmod.QrUpd_.SQL.Add('DELETE FROM emlotits WHERE Controle=:P0');
      //
      Dmod.QrUpd_.Params[0].AsInteger := QrLotesItsControle.Value;
      Dmod.QrUpd_.ExecSQL;
    end;
  end;
end;

procedure TFmLotes1.LocalizaProximoChNaoVerde;
begin
  while not Dmoc.QrLCHs.Eof do
  begin
    if Dmoc.QrLCHsMEU_SIT.Value <> 2 then Exit;
    Dmoc.QrLCHs.Next;
  end;
end;

procedure TFmLotes1.LocalizaProximoDUNaoVerde;
begin
  while not Dmoc.QrLDUs.Eof do
  begin
    if Dmoc.QrLDUsMEU_SIT.Value <> 2 then Exit;
    Dmoc.QrLDUs.Next;
  end;
end;

procedure TFmLotes1.Enviaaocliente1Click(Sender: TObject);
begin
  //EnviaBorderoCliente2;
end;

(*procedure TFmLotes1.EnviaBorderoCliente;
var
  Pasta, Arq1, Arq2, Nome: String;
  Continua, Tipo: Integer;
begin
  Continua := ID_YES;
  if not ReopenContrat then
  begin
    Continua := Geral.MB_Pergunta('Deseja enviar os arquivos assim mesmo?');
  end;
  if Continua <> ID_YES then Exit;
  Screen.Cursor := crHourGlass;
  //
  try
    Nome := 'Border� ' + Geral.FF0(QrLotesLote.Value);
    //
    FContaST := 0;
    QrSumOP.Close;
    QrSumOP.Params[0].AsInteger := QrLotesCodigo.Value;
    QrSumOP.Open;
    //
    Pasta := FDermatekCamCli+FormatFloat('000000', QrLotesCliente.Value)+'\';
    ForceDirectories(Pasta);
    Arq1 := FormatFloat('000000', QrLotesLote.Value)+'Bordero.fp3';
    begin
      if QrLotesTipo.Value = 0 then
      begin
        frBorTotCli2.PrepareReport;
        frBorTotCli2.SavePreparedReport(Pasta+Arq1);
      end else begin
        frBorTotCli3.PrepareReport;
        frBorTotCli3.SavePreparedReport(Pasta+Arq1);
      end;
    end else begin
      if QrLotesTipo.Value = 0 then
      begin
        frBorTotCli0.PrepareReport;
        frBorTotCli0.SavePreparedReport(Pasta+Arq1);
      end else begin
        frBorTotCli1.PrepareReport;
        frBorTotCli1.SavePreparedReport(Pasta+Arq1);
      end;
    end;
    Dmod.VerificaDefinicaoDeTextoPadrao(2);
    //
    Arq2 := FormatFloat('000000', QrLotesLote.Value)+'Aditivo_NP.fp3';
    frAditivo_NP.PrepareReport;
    frAditivo_NP.SavePreparedReport(Pasta+Arq2);
    //
    Dmod.ReopenCliente(QrLotesCliente.Value);
    if (DMod.QrClientePastaTxtFTP.Value <> '') then
    begin
      Application.CreateForm(TFmFTPLotes, FmFTPLotes);
      FmFTPLotes.ClItens.Items.Clear;
      FmFTPLotes.ClItens.Items.Add(Pasta+Arq1);
      FmFTPLotes.ClItens.Items.Add(Pasta+Arq2);
      //
      FmFTPLotes.ClItens.Checked[0] := True;
      FmFTPLotes.ClItens.Checked[1] := True;
      //
      FmFTPLotes.ClItens.Visible := True;
      FmFTPLotes.BtEnvia.Enabled := True;
      //
      FmFTPLotes.ShowModal;
      FmFTPLotes.Destroy;
    end else Geral.MB_Aviso('Arquivos gerados mas n�o enviados ' +
    'por falta de senha de envio para o cliente:' + sLineBreak +
    Pasta + Arq1 + sLineBreak + Pasta + Arq2);
    //
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;*)

{
procedure TFmLotes1.EnviaBorderoCliente2;
var
  Nome, Pasta, Arq1, Arq2, b: String;
  Continua, Tipo: Integer;
begin
  b := FormatFloat('000000', QrLotesLote.Value);
  Continua := ID_YES;
  if not ReopenContrat then
  begin
    Continua := Geral.MB_Pergunta('Deseja enviar os arquivos assim mesmo?');
  end;
  if Continua <> ID_YES then Exit;
  Screen.Cursor := crHourGlass;
  //
  try
    Nome := 'Border� ' + Geral.FF0(QrLotesLote.Value);
    //
    FContaST := 0;
    QrSumOP.Close;
    QrSumOP.Params[0].AsInteger := QrLotesCodigo.Value;
    QrSumOP.Open;
    //
    Pasta := FDermatekCamCli+FormatFloat('000000', QrLotesCliente.Value)+'\';
    ForceDirectories(Pasta);
    Arq1 := b+'Bordero.fp3';
    if QrLotesMINAV.Value+QrLotesMINTC.Value>=0.01 then Tipo := 0 else Tipo := 10;
    Tipo := Tipo + QrLotesTipo.Value;
    case Tipo of
       0: MLAGeral.frxSalva(frxBorTotCli2, Nome, Pasta + Arq1);
       1: MLAGeral.frxSalva(frxBorTotCli3, Nome, Pasta + Arq1);
      10: MLAGeral.frxSalva(frxBorTotCli0, Nome, Pasta + Arq1);
      11: MLAGeral.frxSalva(frxBorTotCli1, Nome, Pasta + Arq1);
    end;
    (*if QrLotesMINAV.Value+QrLotesMINTC.Value>=0.01 then
    begin
      case QrLotesTipo.Value of
        0: MLAGeral.frxSalva(frxBorTotCli2, 'Border� '+ b, Pasta+Arq1);
        1: MLAGeral.frxSalva(frxBorTotCli3, 'Border� '+ b, Pasta+Arq1);
      end;
    end else begin
      case QrLotesTipo.Value of
        0: MLAGeral.frxSalva(frxBorTotCli0, 'Border� '+ b, Pasta+Arq1);
        1: MLAGeral.frxSalva(frxBorTotCli1, 'Border� '+ b, Pasta+Arq1);
      end;
    end;*)
    Dmod.VerificaDefinicaoDeTextoPadrao(2);
    //
    Arq2 := b+'Aditivo_NP.fp3';
    MLAGeral.frxSalva(frxAditivo_NP, 'Aditivo e Nota Promiss�ria '+ b, Pasta+Arq2);
    //
    Dmod.ReopenCliente(QrLotesCliente.Value);
    if (DMod.QrClientePastaTxtFTP.Value <> '') then
    begin
      Application.CreateForm(TFmFTPLotes, FmFTPLotes);
      FmFTPLotes.ClItens.Items.Clear;
      FmFTPLotes.ClItens.Items.Add(Pasta+Arq1);
      FmFTPLotes.ClItens.Items.Add(Pasta+Arq2);
      //
      FmFTPLotes.ClItens.Checked[0] := True;
      FmFTPLotes.ClItens.Checked[1] := True;
      //
      FmFTPLotes.ClItens.Visible := True;
      FmFTPLotes.BtEnvia.Enabled := True;
      //
      FmFTPLotes.ShowModal;
      FmFTPLotes.Destroy;
    end else
      Geral.MB_Aviso('Arquivos gerados mas n�o enviados '+
        'por falta de senha de envio para o cliente:' + sLineBreak +
        Pasta + Arq1 + sLineBreak + Pasta + Arq2);
    //
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;
}

procedure TFmLotes1.frxNF_TudoGetValue(const VarName: String;
  var Value: Variant);
var
  Txt: String;
begin
  if AnsiCompareText(VarName, 'VARF_NFLINHA1') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFLinha1.Value,
      '[TAXA_ADVAL]', QrLotesADVAL_TXT.Value);
    Txt := MLAGeral.TrocaTexto(Txt, '[VALOR_DOC]',
      Geral.FFT(QrLotesTotal.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[NUM_BORDERO]',
      Geral.FF0(QrLotesLote.Value));
    Txt := MLAGeral.TrocaTexto(Txt, '[IMPOSTO]',
      Geral.FFT(QrLotesISS.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'VARF_NFLINHA2') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFLinha2.Value,
      '[TAXA_ADVAL]', QrLotesADVAL_TXT.Value);
    Txt := MLAGeral.TrocaTexto(Txt, '[VALOR_DOC]',
      Geral.FFT(QrLotesTotal.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[NUM_BORDERO]',
      Geral.FF0(QrLotesLote.Value));
    Txt := MLAGeral.TrocaTexto(Txt, '[IMPOSTO]',
      Geral.FFT(QrLotesISS.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'VARF_NFLINHA3') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFLinha3.Value,
      '[TAXA_ADVAL]', QrLotesADVAL_TXT.Value);
    Txt := MLAGeral.TrocaTexto(Txt, '[VALOR_DOC]',
      Geral.FFT(QrLotesTotal.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[NUM_BORDERO]',
      Geral.FF0(QrLotesLote.Value));
    Txt := MLAGeral.TrocaTexto(Txt, '[IMPOSTO]',
      Geral.FFT(QrLotesISS.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'IMPOSTO') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFAliq.Value, '[IMPOSTO]',
      Geral.FFT(QrLotesISS.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'AlturaInicial') = 0 then
  begin
    Value := Int(Dmod.QrControleNF_Cabecalho.Value / VAR_frCM);//37.7953);
  end
  else if AnsiCompareText(VarName, 'VARF_MEUENDERECO') = 0 then
  begin
    if FMEU_ENDERECO then Value := Dmod.QrDonoE_CUC.Value
    else Value := ' ';
  end
end;

procedure TFmLotes1.BitBtn1Click(Sender: TObject);
//var
  //ver: Integer;
begin
{
  FmPrincipal.FTipoImport := 4;
  FLoteMSN := 0;
  FControlIts := QrLotesItsControle.Value;
  Application.CreateForm(TFmLotes1MSN, FmLotes1MSN);
  FmLotes1MSN.ShowModal;
  FmLotes1MSN.Destroy;
  if FLoteMSN = 0 then Exit;
  if MeuMSN.AbreArquivoSCX(GradeC, GradeD, EdCodCli, EdNomCli, TPCheque,
    ProgressBar1, FLoteMSN) then
  begin
    Application.ProcessMessages;
    // � mais r�pido para excluir dados ?
    UCriar.RecriaTabelaLocal('ImportLote', 1);
    Application.ProcessMessages;
    ArqSCX.CalculaGradeC(EdQtdeCh, EdValorCH, EdQtdeTo, EdValorTO,
      EdValorDU, GradeC, GradeD, TPCheque);
    Application.ProcessMessages;
    ArqSCX.CalculaGradeD(EdQtdeDU, EdValorDU, EdQtdeTo, EdValorTO,
      EdValorCH, GradeC, GradeD, TPCheque);
    Application.ProcessMessages;
    MostraEdicao(3, CO_INCLUSAO, 0);
    Application.ProcessMessages;
    DefineRiscos(Geral.IMV(EdCodCli.Text));
    Application.ProcessMessages;
    //TimerSCX.Enabled := True;
    InsereImportLote;
  end;
  LaVerSuit1.Caption := Geral.VersaoTxt2006(FmPrincipal.FVerArqSCX);
  LaVerSuit2.Caption := Geral.VersaoTxt2006(FmPrincipal.FVerArqSCX);
  Ver := Geral.VersaoInt2006(FVersaoSuitCash);
  if FmPrincipal.FVerArqSCX < Ver then
  begin
    LaVerSuit1.Font.Color := clRed;
    LaVerSuit2.Font.Color := clRed;
  end else if FmPrincipal.FVerArqSCX > Ver then
  begin
    LaVerSuit1.Font.Color := clGreen;
    LaVerSuit2.Font.Color := clGreen;
  end else begin
    LaVerSuit1.Font.Color := clNavy;
    LaVerSuit2.Font.Color := clNavy;
  end;
}
end;

procedure TFmLotes1.BtFTPClick(Sender: TObject);
begin
  //Importar(FmPrincipal.FDirR, 3);
end;

procedure TFmLotes1.Arquivo1Click(Sender: TObject);
begin
  FConferido := 0;
  BtImportarClick(Self);
end;

procedure TFmLotes1.MenuItem1Click(Sender: TObject);
begin
  VerificaCPFCNP_XX_LotesIts(istExtra2);
end;

procedure TFmLotes1.MenuItem2Click(Sender: TObject);
begin
  VerificaCPFCNP_XX_LotesIts(istExtra1);
end;

procedure TFmLotes1.MenuItem4Click(Sender: TObject);
begin
  VerificaCPFCNP_XX_LotesIts(istAtual);
end;

procedure TFmLotes1.MenuItem5Click(Sender: TObject);
begin
  VerificaCPFCNP_XX_LotesIts(istSelecionados);
end;

procedure TFmLotes1.Messenger1Click(Sender: TObject);
begin
  FConferido := 0;
  BitBtn1Click(Self);
end;

procedure TFmLotes1.DiretrioFTP1Click(Sender: TObject);
begin
  FConferido := 0;
  BtFTPClick(Self);
end;

function TFmLotes1.frxBorTotCli2UserFunction(const MethodName: String;
  var Params: Variant): Variant;
begin
  if MethodName = 'VARF_CHDEV' then Params := QrCHDevP.RecordCount
  else if MethodName = 'VARF_DUDEV' then Params := QrDPago.RecordCount
end;

procedure TFmLotes1.frxBorTotCli2GetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_HOJE' then
  begin
    Value := FormatDateTime(VAR_FORMATDATE2, Date);
    FContaST := 0;
  end
  else if VarName = 'VARF_CHQS' then Value := QrLotesIts.RecordCount
  else if VarName = 'VARF_MOEDA' then Value := Dmod.QrControleMoeda.Value
  else if VarName = 'VAR_CONTAST' then
  begin
    FContaST := FContaST +1;
    Value := FContaST;// repasse
  end else if VarName = 'VARF_CHDEV' then Value := QrCHDevP.RecordCount
  else if VarName = 'VARF_DUDEV' then Value := QrDPago.RecordCount
  else if VarName = 'VARF_NOMEEMP' then
  begin
    if Dmod.QrControleCodCliRel.Value = 1 then
      Value := FormatFloat('0', QrLotescliente.Value) + ' - '
    else
      Value := '';
    case Dmod.QrControleTipNomeEmp.Value of
      0: Value := Value + QrLotesNOMECLIENTE.Value;
      1: Value := Value + QrLotesNOMEFANCLIENTE.Value;
    end;
  end;
end;

procedure TFmLotes1.Novo2Click(Sender: TObject);
begin
  ImprimeBorderoClienteE2;
end;

procedure TFmLotes1.frxAditivo_NPGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_HOJE' then Value := FormatDateTime(VAR_FORMATDATE2, Date)
  else if VarName = 'VARF_CHQS' then Value := QrLotesIts.RecordCount
  else if VarName = 'VARF_MOEDA' then Value := Dmod.QrControleMoeda.Value
  else if VarName = 'Coligada0' then Value := Dmod.QrMasterEm.Value
  else if VarName = 'Coligada1' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 1 then
    begin
      if FmPrincipal.FMyDBs.Connected[0] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[0]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'Coligada2' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 2 then
    begin
      if FmPrincipal.FMyDBs.Connected[1] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[1]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'Coligada3' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 3then
    begin
      if FmPrincipal.FMyDBs.Connected[2] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[2]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'Coligada4' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 4 then
    begin
      if FmPrincipal.FMyDBs.Connected[3] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[3]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'Coligada5' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 5 then
    begin
      if FmPrincipal.FMyDBs.Connected[4] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[4]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'Coligada6' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 6 then
    begin
      if FmPrincipal.FMyDBs.Connected[5] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[5]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'TaxaJuros0' then Value := QrLotesItsTxaCompra.Value
  else if VarName = 'TaxaJuros1' then Value := FDadosJurosColigadas[1][0]
  else if VarName = 'TaxaJuros2' then Value := FDadosJurosColigadas[2][0]
  else if VarName = 'TaxaJuros3' then Value := FDadosJurosColigadas[3][0]
  else if VarName = 'TaxaJuros4' then Value := FDadosJurosColigadas[4][0]
  else if VarName = 'TaxaJuros5' then Value := FDadosJurosColigadas[5][0]
  else if VarName = 'TaxaJuros6' then Value := FDadosJurosColigadas[6][0]
  else if VarName = 'TaxaJuros7' then Value := FDadosJurosColigadas[0][0]

  else if VarName = 'JurosPeriodo0' then Value := QrLotesItsTxaJuros.Value
  else if VarName = 'JurosPeriodo1' then Value := FDadosJurosColigadas[1][1]
  else if VarName = 'JurosPeriodo2' then Value := FDadosJurosColigadas[2][1]
  else if VarName = 'JurosPeriodo3' then Value := FDadosJurosColigadas[3][1]
  else if VarName = 'JurosPeriodo4' then Value := FDadosJurosColigadas[4][1]
  else if VarName = 'JurosPeriodo5' then Value := FDadosJurosColigadas[5][1]
  else if VarName = 'JurosPeriodo6' then Value := FDadosJurosColigadas[6][1]
  else if VarName = 'JurosPeriodo7' then Value := FDadosJurosColigadas[0][1]

  else if VarName = 'ValorJuros0' then Value := QrLotesItsVlrCompra.Value
  else if VarName = 'ValorJuros1' then Value := FDadosJurosColigadas[1][2]
  else if VarName = 'ValorJuros2' then Value := FDadosJurosColigadas[2][2]
  else if VarName = 'ValorJuros3' then Value := FDadosJurosColigadas[3][2]
  else if VarName = 'ValorJuros4' then Value := FDadosJurosColigadas[4][2]
  else if VarName = 'ValorJuros5' then Value := FDadosJurosColigadas[5][2]
  else if VarName = 'ValorJuros6' then Value := FDadosJurosColigadas[6][2]
  else if VarName = 'ValorJuros7' then Value := FDadosJurosColigadas[0][2]
  else if VarName = 'VARF_TOTAL_EXTENSO' then Value :=
    dmkPF.ExtensoMoney(Geral.FFT(QrLotesTotal.Value, 2, siPositivo))
  else if VarName = 'VARF_CIDADE_E_DATA' then
    Value := Dmod.QrDonoCIDADE.Value
    + ', '+FormatDateTime('dd" de "mmmm" de "yyyy"."', QrLotesData.Value)
  else if VarName = 'VARF_ENDERECO_CLIENTE' then Value :=
    GOTOy.EnderecoDeEntidade(QrLotesCliente.Value, 0)
  else if VarName = 'NUM_CONT' then
    Value := QrContratContrato.Value
  else if VarName = 'DATA_CON' then Value :=
    FormatDateTime(VAR_FORMATDATE2, QrContratDataC.Value)
  else if VarName = 'LIMITE  ' then Value := QrContratLimite.Value
  else if VarName = 'ADITIVO ' then Value := QrLotesLote.Value
  else if VarName = 'PLU1_TIT' then
  begin
    if DmLotes.QrDupSac.RecordCount < 2 then Value := '' else Value := 's'
  end
  else if VarName = 'PLU2_TIT' then
  begin
    if DmLotes.QrDupSac.RecordCount < 2 then Value := 'i' else Value := 'ram'
  end
  else if VarName = 'PLU3_TIT' then
  begin
    if DmLotes.QrDupSac.RecordCount < 2 then Value := '�' else Value := '�o'
  end
  else if VarName = 'PLU4_TIT' then
  begin
    if DmLotes.QrDupSac.RecordCount < 2 then Value := '' else Value := 'em'
  end
  else if VarName = 'PLU5_TIT' then                     
  begin
    if DmLotes.QrDupSac.RecordCount < 2 then Value := 'l' else Value := 'is'
  end
  else if VarName = 'PLU6_TIT' then                     
  begin
    if DmLotes.QrDupSac.RecordCount < 2 then Value := '�o' else Value := '�es'
  end
  else if VarName = 'PLU7_TIT' then
  begin
    if DmLotes.QrDupSac.RecordCount < 2 then Value := '' else Value := 'es'
  end
  else if VarName = 'NOME_SAC' then Value := QrSaCartNome.Value
  else if VarName = 'NOME_EMIT' then Value := QrSaEmitCHEmitente.Value
  else if VarName = 'DUPLISAC' then
  begin
    DmLotes.QrDupSac.First;
    Value :=
    'N� Duplicata  Emitida em   Valor de R$   Vencimento Banco e ag�ncia para pagamento' + sLineBreak +
    '------------------------------------------------------------------------------------------------' + sLineBreak;
    while not DmLotes.QrDupSac.Eof do
    begin
      Value := Value +
      Geral.CompletaString(DmLotes.QrDupSacDuplicata.Value,' ', 14, taLeftJustify, True)+
      Geral.CompletaString(FormatDateTime(VAR_FORMATDATE2, DmLotes.QrDupSacEmissao.Value), ' ', 11, taLeftJustify, True)+
      Geral.CompletaString(Geral.FFT(DmLotes.QrDupSacValor.Value, 2, siPositivo), ' ', 13, taRightJustify, True)+ '   '+
      Geral.CompletaString(FormatDateTime(VAR_FORMATDATE2, DmLotes.QrDupSacVencto.Value), ' ', 10, taLeftJustify, True)+ ' '+
      Geral.CompletaString(MLAGeral.FFD(DmLotes.QrDupSacBanco.Value, 3, siPositivo), ' ', 3, taRightJustify, True)+'/'+
      Geral.CompletaString(MLAGeral.FFD(DmLotes.QrDupSacAgencia.Value, 4, siPositivo), ' ', 4, taRightJustify, True)+ ' ' +
      Geral.CompletaString(DmLotes.QrDupSacNOMEBANCO.Value, ' ', 35, taLeftJustify, True)+
      sLineBreak;
      //
      DmLotes.QrDupSac.Next;
    end;
  end
  else if VarName = 'CHEQEMIT' then
  begin
    DmLotes.QrDupSac.First;
    Value :=                                       
    'N� Cheque     Banco Ag�ncia     Conta         Emitido em  Valor de R$    Vencimento ' + sLineBreak +
    '------------------------------------------------------------------------------------------------' + sLineBreak;
    while not DmLotes.QrDupSac.Eof do
    begin
      Value := Value +
      Geral.CompletaString(Geral.FF0(DmLotes.QrDupSacCheque.Value),' ', 14, taLeftJustify, True) +
      Geral.CompletaString(Geral.FF0(DmLotes.QrDupSacBanco.Value), ' ', 6, taLeftJustify, True) +
      Geral.CompletaString(Geral.FF0(DmLotes.QrDupSacAgencia.Value), ' ', 12, taLeftJustify, True)+ ' ' +
      Geral.CompletaString(DmLotes.QrDupSacConta.Value, ' ', 12, taLeftJustify, True)+ ' ' +
      Geral.CompletaString(FormatDateTime(VAR_FORMATDATE2, DmLotes.QrDupSacDCompra.Value), ' ', 11, taLeftJustify, True)+
      Geral.CompletaString(Geral.FFT(DmLotes.QrDupSacValor.Value, 2, siPositivo), ' ', 13, taRightJustify, True)+ '   '+
      Geral.CompletaString(FormatDateTime(VAR_FORMATDATE2, DmLotes.QrDupSacVencto.Value), ' ', 11, taLeftJustify, True)+ ' '+
      sLineBreak;
      //
      DmLotes.QrDupSac.Next;
    end;
  end
  else if VarName = 'NOME_CLI' then Value := QrLotesNOMECLIENTE.Value
  else if AnsiCompareText(VarName, 'VARF_NOTEOF') = 0 then
  begin
    if QrSaCart.RecNo < QrSaCart.RecordCount then Value := True else Value := False;
  end
  else if AnsiCompareText(VarName, 'VARF_NOTEOF2') = 0 then
  begin
    if QrSaEmitCH.RecNo < QrSaEmitCH.RecordCount then Value := True else Value := False;
  end
  else if AnsiCompareText(VarName, 'VARF_CIDADE_E_DATA') = 0 then
    Value := Dmod.QrDonoCIDADE.Value + ', ' +
    FormatDateTime('dd" de "mmmm" de "yyyy"."', QrLotesData.Value)
  else FmPrincipal.TextosCartasGetValue(VarName, Value);
end;

procedure TFmLotes1.BtPagTercClick(Sender: TObject);
begin
  Multiplaseleo1Click(Self);
  //MyObjects.MostraPopUpDeBotao(PMPagterc, BtPagTerc);
end;

procedure TFmLotes1.ReopenRepCli;
begin
  DmLotes.QrRepCli.Close;
  DmLotes.QrRepCli.Params[0].AsInteger := QrLotesCodigo.Value;
  DmLotes.QrRepCli.Open;
end;

procedure TFmLotes1.BtExclui8Click(Sender: TObject);
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do item n� ' +
    Geral.FF0(DmLotes.QrRepCliSEQ.Value) + ' de cheques de terceiros?') = ID_YES
  then begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   RepCli = 0 WHERE Controle=:P0');
    Dmod.QrUpd.Params[00].AsInteger := DmLotes.QrRepCliControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    CalculaLote(QrLotesCodigo.Value, True);
    LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value);
  end;
end;

procedure TFmLotes1.BitBtn5Click(Sender: TObject);
begin
  Memo1.Lines.Clear;
end;

procedure TFmLotes1.BitBtn6Click(Sender: TObject);
begin
  FmPrincipal.CadastroDeOpcoesCreditor;
end;

procedure TFmLotes1.BtSPC_LIClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSPC_LotIts, BtSPC_LI);
end;

procedure TFmLotes1.InfoTempo(Tempo: TDateTime; Texto: String; Inicial: Boolean);
begin
  if Inicial then
  begin
    FTempo := Tempo;
    FUltim := Tempo;
    Memo1.Lines.Add('');
    Memo1.Lines.Add('==============================================================================');
    Memo1.Lines.Add('');
    Memo1.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss ', Tempo) +
      '- [ Total ] [Unit�ri] '+ Texto);
  end else begin
    Memo1.Lines.Add(FormatDateTime('dd/mm/yyyy  hh:nn:ss - ', Tempo)+
    FormatDateTime('nn:ss:zzz ', Tempo-FTempo) +
    FormatDateTime('nn:ss:zzz ', Tempo-FUltim) + Texto);
    FUltim := Tempo;
  end;
end;

procedure TFmLotes1.TudoFolhaembranco1Click(Sender: TObject);
begin
  if DefineDataSetsNF(frxNF_Tudo) then
  begin
    GOTOy.EnderecoDeEntidade(QrLotesCliente.Value, 0);
    MyObjects.frxMostra(frxNF_Tudo, 'Nota Fiscal n� ' + Geral.FF0(QrLotesNF.Value));
  end;
end;

procedure TFmLotes1.frxCartaSacadoDadosGetValue(const VarName: String;
  var Value: Variant);
var
  Altura: Integer;
begin
  if AnsiCompareText(VarName, 'VARF_CSD_AltuHeader') = 0 then
    Value := Int(Dmod.QrControleCSD_TopoCidata.Value / VAR_frCM)
  else if AnsiCompareText(VarName, 'VARF_CSD_AltuCidata') = 0 then
  begin
    Altura := Dmod.QrControleCSD_TopoDestin.Value - Dmod.QrControleCSD_TopoCidata.Value;
    Value := Int(Altura / VAR_frCM)
  end
  else if AnsiCompareText(VarName, 'VARF_CSD_AltuDestin') = 0 then
  begin
    Altura := Dmod.QrControleCSD_TopoDuplic.Value - Dmod.QrControleCSD_TopoDestin.Value;
    Value := Int(Altura / VAR_frCM)
  end else if AnsiCompareText(VarName, 'VARF_CSD_AltuDuplic') = 0 then
  begin
    Altura := Dmod.QrControleCSD_TopoClient.Value - Dmod.QrControleCSD_TopoDuplic.Value;
    Value := Int(Altura / VAR_frCM)
  end else if AnsiCompareText(VarName, 'VARF_CSD_AltuFooter') = 0 then
  begin
    Altura := 29700 - 500 - 4500 - Dmod.QrControleCSD_TopoClient.Value;
    Value := Int(Altura / VAR_frCM)
  //
  end else if AnsiCompareText(VarName, 'VARF_CSD_AltuHeade2') = 0 then
    Value := Int(Dmod.QrControleCSD_TopoDesti2.Value / VAR_frCM)
  else if AnsiCompareText(VarName, 'VARF_CSD_AltuDesti2') = 0 then
  begin
    Altura := 26000 (*- 4500*) - Dmod.QrControleCSD_TopoDesti2.Value;
    Value := Int(Altura / VAR_frCM)
  //
  end else if AnsiCompareText(VarName, 'VARF_CSD_MEsqCidata') = 0 then
    Value := Dmod.QrControleCSD_MEsqCidata.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_CSD_MEsqDestin') = 0 then
    Value := Dmod.QrControleCSD_MEsqDestin.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_CSD_MEsqDuplic') = 0 then
    Value := Dmod.QrControleCSD_MEsqDuplic.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_CSD_MEsqClient') = 0 then
    Value := Dmod.QrControleCSD_MEsqClient.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_CSD_MEsqDesti2') = 0 then
    Value := Dmod.QrControleCSD_MEsqDesti2.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_CIDADE_E_DATA') = 0 then
    Value := Dmod.QrDonoCIDADE.Value + ', '+
    FormatDateTime('dd" de "mmmm" de "yyyy"."', QrLotesData.Value)
  else if AnsiCompareText(VarName, 'DUPLISACDADOS') = 0  then
  begin
    DmLotes.QrDupSac.First;
    Value := '';
    //'N� Duplicata  Emitida em   Valor de R$   Vencimento Banco e ag�ncia para pagamento' + sLineBreak +
    //'------------------------------------------------------------------------------------------------' + sLineBreak;
    while not DmLotes.QrDupSac.Eof do
    begin
      Value := Value +
      Geral.CompletaString(DmLotes.QrDupSacDuplicata.Value,' ', 16, taLeftJustify, True)+
      Geral.CompletaString(FormatDateTime(VAR_FORMATDATE2, DmLotes.QrDupSacEmissao.Value), ' ', 10, taLeftJustify, True)+
      Geral.CompletaString(Geral.FFT(DmLotes.QrDupSacValor.Value, 2, siPositivo), ' ', 14, taRightJustify, True)+ '     '+
      Geral.CompletaString(FormatDateTime(VAR_FORMATDATE2, DmLotes.QrDupSacVencto.Value), ' ', 10, taLeftJustify, True)+ ' '+
      '    ' +
      Geral.CompletaString(MLAGeral.FFD(DmLotes.QrDupSacBanco.Value, 3, siPositivo), ' ', 3, taRightJustify, True)+'/'+
      Geral.CompletaString(MLAGeral.FFD(DmLotes.QrDupSacAgencia.Value, 4, siPositivo), ' ', 4, taRightJustify, True)+ ' ' +
      Geral.CompletaString(DmLotes.QrDupSacNOMEBANCO.Value, ' ', 35, taLeftJustify, True)
      + sLineBreak;
      //
      DmLotes.QrDupSac.Next;
    end;
  end
  else if AnsiCompareText(VarName, 'NOME_CLI') = 0 then Value := QrLotesNOMECLIENTE.Value
  else if AnsiCompareText(VarName, 'VARF_NOTEOF') = 0 then
  begin
    if QrSaCart.RecNo < QrSaCart.RecordCount then Value := True else Value := False;
  end
  else ;
end;

procedure TFmLotes1.BtImportaBCOClick(Sender: TObject);
begin
  Importar('', 4);
end;

procedure TFmLotes1.Multiplaseleo1Click(Sender: TObject);
begin
  Application.CreateForm(TFmLotesRepCliMul, FmLotesRepCliMul);
  FmLotesRepCliMul.FRepCli := QrLotesCodigo.Value;
  FmLotesRepCliMul.ShowModal;
  FmLotesRepCliMul.Destroy;
  //
  CalculaLote(QrLotesCodigo.Value, True);
  LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value);
end;

procedure TFmLotes1.Leitora1Click(Sender: TObject);
begin
  (*Application.CreateForm(TFmLotes1RepCliLei, FmLotes1RepCliLei);
  FmLotes1RepCliLei.FRepCli := QrLotesCodigo.Value;
  FmLotes1RepCliLei.FAcao   := 0;
  FmLotes1RepCliLei.ShowModal;
  FmLotes1RepCliLei.Destroy;
  //
  CalculaLote(QrLotesCodigo.Value);
  LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value); *)
end;

procedure TFmLotes1.BtSobraClick(Sender: TObject);
var
  Sobra: String;
begin
  Sobra := Geral.FFT(QrLotesSALDOAPAGAR.Value, 2, siNegativo);
  if InputQuery('Sobra de Border�', 'Informe o valor da sobra:', Sobra) then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lotes SET AlterWeb=1,  SobraNow=:P0 WHERE Codigo=:P1');
    Dmod.QrUpd.Params[00].AsFloat   := Geral.DMV(Sobra);
    Dmod.QrUpd.Params[01].AsInteger := QrLotesCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    CalculaLote(QrLotesCodigo.Value, True);
    LocCod(QrLotesCodigo.Value, QrLotesCodigo.Value);
  end;
end;

procedure TFmLotes1.BtSPC_CHClick(Sender: TObject);
begin
  PGAbertos_CH.ActivePageIndex := 2;
  MyObjects.MostraPopUpDeBotao(PMSPC_Import, BtSPC_CH);
end;

procedure TFmLotes1.BtSPC_DUClick(Sender: TObject);
begin
  PGAbertos_DU.ActivePageIndex := 1;
  MyObjects.MostraPopUpDeBotao(PMSPC_Import, BtSPC_DU);
end;

procedure TFmLotes1.ImprimeBorderoClienteSobra(Dias: Boolean);
begin
  FContaST := 0;
  QrSumOP.Close;
  QrSumOP.Params[0].AsInteger := QrLotesCodigo.Value;
  QrSumOP.Open;
  //
  if QrLotesMINAV.Value+QrLotesMINTC.Value>=0.01 then
  begin
    if QrLotesTipo.Value = 0 then
    begin
      frxBorTotCli2S.Variables['VARF_DIAS']  := Geral.BoolToInt(Dias);
      frxBorTotCli2S.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCli2S.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCli2S.Print; //PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCli2S, 'Border� cliente n� ' + Geral.FF0(QrLotesLote.Value));
      end;
    end else begin
      frxBorTotCli3S.Variables['VARF_DIAS']  := Geral.BoolToInt(Dias);
      frxBorTotCli3S.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCli3S.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCli3S.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCli3S, 'Border� cliente n� ' + Geral.FF0(QrLotesLote.Value));
      end;
    end;
  end else begin
    if QrLotesTipo.Value = 0 then
    begin
      frxBorTotCli0S.Variables['VARF_DIAS']  := Geral.BoolToInt(Dias);
      frxBorTotCli0S.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCli0S.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCli0S.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCli0S, 'Border� cliente n� ' + Geral.FF0(QrLotesLote.Value));
      end;
    end else begin
      frxBorTotCli1S.Variables['VARF_DIAS']  := Geral.BoolToInt(Dias);
      frxBorTotCli1S.Variables['VARF_CHDEV'] := QrCHDevP.RecordCount;
      frxBorTotCli1S.Variables['VARF_DUDEV'] := QrDPago.RecordCount;
      case FImprime of
        1: frxBorTotCli1S.Print;//PreparedReport('', 1, True, frAll);
        2: MyObjects.frxMostra(frxBorTotCli1S, 'Border� cliente n� '+ Geral.FF0(QrLotesLote.Value));
      end;
    end;
  end;
end;

procedure TFmLotes1.BitBtn7Click(Sender: TObject);
var
  Emitente, CPF, Banco, Agencia, Conta: String;
begin
  Screen.Cursor := crHourGlass;
  FControlIts := QrLotesItsControle.Value;
  Emitente := '';
  CPF      := '';
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1, Emitente=:P0, CPF=:P1 WHERE Controle=:P2');
  QrLotesIts.First;
  while not QrLotesIts.Eof do
  begin
    if (QrLotesItsCPF.Value = '') or (QrLotesItsEmitente.Value = '') then
    begin
      Banco   := MLAGeral.FFD(QrLotesItsBanco.Value, 3, siPositivo);
      Agencia := MLAGeral.FFD(QrLotesItsAgencia.Value, 4, siPositivo);
      Conta   := QrLotesItsConta.Value;
      if ObtemEmitente(Banco, Agencia, Conta, Emitente, CPF, False) then
      begin
        Dmod.QrUpd.Params[00].AsString  := Emitente;
        Dmod.QrUpd.Params[01].AsString  := CPF;
        Dmod.QrUpd.Params[02].AsInteger := QrLotesItsControle.Value;
        Dmod.QrUpd.ExecSQL;
      end;
    end;
    QrLotesIts.Next;
  end;
  ReopenIts;
  Screen.Cursor := crDefault;
end;

function TFmLotes1.ObtemEmitente(Banco, Agencia, Conta: String;
  var Emitente, CPF: String; AvisaNaoEncontrado: Boolean): Boolean;
var
  B,A,C: String;
begin
  B := Banco;
  A := Agencia;
  C := Conta;
  //
  Dmoc.QrBanco.Close;
  Dmoc.QrBanco.Params[0].AsInteger := Geral.IMV(Banco);
  Dmoc.QrBanco.Open;
  //
  Dmoc.QrLocEmiBAC.Close;
  Dmoc.QrLocEmiBAC.Params[0].AsInteger := Dmoc.QrBancoDVCC.Value;
  Dmoc.QrLocEmiBAC.Params[1].AsString  := CriaBAC_Pesq(B,A,C,Dmoc.QrBancoDVCC.Value);
  Dmoc.QrLocEmiBAC.Open;
  //
  if Dmoc.QrLocEmiBAC.RecordCount = 0 then
  begin
    Emitente   := '';
    CPF        := '';
    Result     := False;
  end else begin
    Dmoc.QrLocEmiCPF.Close;
    Dmoc.QrLocEmiCPF.Params[0].AsString := Dmoc.QrLocEmiBACCPF.Value;
    Dmoc.QrLocEmiCPF.Open;
    //
    if Dmoc.QrLocEmiCPF.RecordCount = 0 then
    begin
      Emitente := '';
      CPF      := '';
      Result   := False;
    end else begin
      Emitente := Dmoc.QrLocEmiCPFNome.Value;
      CPF      := {Geral.FormataCNPJ_TT(}Dmoc.QrLocEmiCPFCPF.Value{)};
      Result   := True;
    end;
  end;
  if AvisaNaoEncontrado then
    Geral.MB_Aviso('Emitente n�o encontrado!');
end;

procedure TFmLotes1.DBEdBanco4Change(Sender: TObject);
begin
  ReopenBanco4;
end;

procedure TFmLotes1.QrSumOPCalcFields(DataSet: TDataSet);
begin
  QrSumOPPagoNeg.Value := - QrSumOPPago.Value;
end;

procedure TFmLotes1.BtWebClick(Sender: TObject);
begin
  FLoteWeb := 0;
  Dmoc.BaixaLotesWeb;
  Dmoc.QrLLC.Close;
  Dmoc.QrLLC.Open;
  if Dmoc.QrLLC.RecordCount > 0 then
  begin
    Application.CreateForm(TFmLotesWeb, FmLotesWeb);
    FmLotesWeb.ShowModal;
    FmLotesWeb.Destroy;
  end;
  if Dmoc.QrLLC.Locate('Codigo', FLoteWeb, []) then
  begin
     Importar('', 5);
  end else begin
    if FLoteWeb > 0 then
      Geral.MB_Aviso('O lote web ' + Geral.FF0(FLoteWeb)+' n�o foi localizado!');
  end;
  Dmoc.QrLLC.Close;
end;

procedure TFmLotes1.Diversositens1Click(Sender: TObject);
begin
  MostraEdicao(11, CO_ALTERACAO, 0);
end;

procedure TFmLotes1.CartaaoEmitentedoCheque1Click(Sender: TObject);
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(5);
  if ReopenEmitCH(istPergunta) then
  begin
    MyObjects.frxMostra(frxCartaEmitCH, 'Carta ao emitente do cheque');
    AtualizaImpressaoDeCarta(QrLotesCodigo.Value, 0);
  end;
end;

procedure TFmLotes1.Carteiradedepsito1Click(Sender: TObject);
begin
  Application.CreateForm(TFmLotesCartDep, FmLotesCartDep);
  if QrLotesIts.State = dsBrowse then
  begin
    FmLotesCartDep.EdCartDep.Text := Geral.FF0(QrLotesItsCartDep.Value);
    FmLotesCartDep.CBCartDep.KeyValue := QrLotesItsCartDep.Value;
  end;
  FmLotesCartDep.ShowModal;
  FmLotesCartDep.Destroy;
end;

procedure TFmLotes1.Alteraototaldodocumentoatual1Click(Sender: TObject);
begin
  FControlIts := QrLotesItsControle.Value;
  if QrLotesTipo.Value = 0 then
    MostraEdicao(2, CO_ALTERACAO, 0)
  else
    MostraEdicao(4, CO_ALTERACAO, 0);
end;

procedure TFmLotes1.Alteraodadatadodepsito1Click(Sender: TObject);
  function AlteraDepositoDocAtual(): Boolean;
  begin
    Dmod.QrUpd.Params[00].AsString  := Geral.FDT(VAR_GETDATA, 1);
    Dmod.QrUpd.Params[01].AsInteger := QrLotesItsControle.Value;
    Dmod.QrUpd.ExecSQL;
    Result := true;
  end;
var
  Grade: TDBGrid;
  i: Integer;
begin
  Application.CreateForm(TFmGetData, FmGetData);
  FmGetData.TPData.Date := Date;
  FmGetData.ShowModal;
  FmGetData.Destroy;
  if VAR_GETDATA = 0 then Exit;
  //
  if QrLotesTipo.Value = 0 then Grade := GradeCHE else Grade := GradeDUP;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET DDeposito=:P0 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  if Grade.SelectedRows.Count > 0 then
  begin
    with Grade.DataSource.DataSet do
    for i:= 0 to Grade.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
      AlteraDepositoDocAtual();
    end;
  end else AlteraDepositoDocAtual();
  ReopenIts();
  //
  Geral.MB_Aviso('As altera��es da data de dep�sito ser�o ' +
    'desfeitas caso o item for re-editado!');
end;

procedure TFmLotes1.ObtemTipoEPercDeIOF2008(const TipoEnt, EhSimples: Integer;
              const ValOperacao: Double; var Perc: Double; var TipoIOF: Integer);
begin
  Perc := 0;
  TipoIOF := 0;
  //
  if TipoEnt = 0 then
  begin
    if EhSimples = 1 then
    begin
      if ValOperacao <= Dmod.QrControleIOF_Li.Value then
      begin
        Perc := Dmod.QrControleIOF_ME.Value;
        TipoIOF := 1;
      end else begin
        Perc := Dmod.QrControleIOF_PJ.Value;
        TipoIOF := 2;
      end;
    end else begin
      Perc := Dmod.QrControleIOF_PJ.Value;
      TipoIOF := 3;
    end;
  end else begin
    Perc := Dmod.QrControleIOF_PF.Value;
    TipoIOF := 4;
  end;
end;

procedure TFmLotes1.ImprimeNF(MeuEndereco: Boolean);
begin
  if DefineDataSetsNF(frxNF_Dados) then
  begin
    FMEU_ENDERECO := MeuEndereco;
    GOTOy.EnderecoDeEntidade(QrLotesCliente.Value, 0);
    MyObjects.frxMostra(frxNF_Dados, 'Nota Fiscal n� ' + Geral.FF0(QrLotesNF.Value));
  end;
end;

procedure TFmLotes1.Semomeuendereo1Click(Sender: TObject);
begin
  ImprimeNF(False);
end;

procedure TFmLotes1.Comomeuendereo1Click(Sender: TObject);
begin
  ImprimeNF(True);
end;

function TFmLotes1.ReopenSaCart(SelType: TSelType): Boolean;
var
  Quais: TSelType;
  i: Integer;
  Grade: TDBGrid;
  Txt: String;
begin
  Quais := SelType;
  if SelType = istPergunta then
  begin
    if DBCheck.CriaFm(TFmQuaisItens, FmQuaisItens, afmoNegarComAviso) then
    begin
      FmQuaisItens.ShowModal;
      Quais := FmQuaisItens.FEscolha;
      FmQuaisItens.Destroy;
    end;
  end;
  QrSaCart.Close;
  QrSaCart.SQL.Clear;
  QrSaCart.SQL.Add('SELECT li.Codigo, sa.Numero+0.000 Numero, sa.*');
  QrSaCart.SQL.Add('FROM lotesits li');
  QrSaCart.SQL.Add('LEFT JOIN sacados sa ON sa.CNPJ=li.CPF');
  QrSaCart.SQL.Add('');
  if QrLotesTipo.Value = 0 then Grade := GradeCHE else Grade := GradeDUP;
  if (Quais = istAtual) or (
    (Quais = istSelecionados) and (Grade.SelectedRows.Count < 2)) then
      QrSaCart.SQL.Add('WHERE li.Controle=' +
      FormatFloat('0', QrLotesItsControle.Value)) else
  if Quais = istSelecionados then
  begin
    Txt := '';
    with Grade.DataSource.DataSet do
    for i := 0 to Grade.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
      if Txt <> '' then Txt := Txt + ',';
      Txt := Txt + FormatFloat('0', QrLotesItsControle.Value);
    end;
    QrSaCart.SQL.Add('WHERE li.Controle in (' + Txt + ')');
  end else
  if Quais = istTodos then QrSaCart.SQL.Add('WHERE li.Codigo=' +
      FormatFloat('0', QrLotesCodigo.Value))
  else begin
    Result := False;
    if Quais <> istNenhum then
      Geral.MB_Aviso('"SelType" n�o definido em "ReopenSaCart"!');
    Exit;
  end;
  QrSaCart.Open;
  Result := True;
end;

procedure TFmLotes1.ImprimeCartaAoSacadoDados;
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(4);
  if ReopenSaCart(istPergunta) then
  begin
    //frCartaSacadoDados.PrepareReport;
    case FImprime of
      1: MyObjects.frxImprime(frxCartaSacadoDados, 'Carta ao sacado (frente)');//.PrintPreparedReport('', 1, True, frAll);
      2: MyObjects.frxMostra(frxCartaSacadoDados, 'Carta ao sacado (frente)');
      //2: frCartaSacadoDados.ShowPreparedReport;
    end;
    if Dmod.QrControleEnvelopeSacado.Value = 1 then
    begin
      Dmod.QrDono.Close;
      Dmod.QrDono.Open;
      //frCartaSacadoVersoDados.PrepareReport;
      //frCartaSacadoVersoDados.ShowPreparedReport;
      MyObjects.frxMostra(frxCartaSacadoVersoDados, 'Carta ao sacado (verso)');
    end;
    AtualizaImpressaoDeCarta(QrLotesCodigo.Value, 1);
  end;
end;

procedure TFmLotes1.Dasos1Click(Sender: TObject);
begin
  ImprimeCartaAoSacadoDados;
end;

procedure TFmLotes1.ImprimeCartaAoSacadoTudoNovo(Modelo: Integer);
begin
  Dmod.VerificaDefinicaoDeTextoPadrao(4);
  if ReopenSaCart(istPergunta) then
  begin
    case FImprime of
      1:
      begin
        if Modelo = 1 then
          MyObjects.frxImprime(frxCartaSacadoTudo, 'Carta ao sacado')
        else
          MyObjects.frxImprime(frxCartaSacadoTudo2, 'Carta ao sacado');
      end;
      2:
      begin
        if Modelo = 1 then
          MyObjects.frxMostra(frxCartaSacadoTudo, 'Carta ao sacado')
        else
          MyObjects.frxMostra(frxCartaSacadoTudo2, 'Carta ao sacado')
      end;
    end;
    if Dmod.QrControleEnvelopeSacado.Value = 1 then
    begin
      Dmod.QrDono.Close;
      Dmod.QrDono.Open;
      MyObjects.frxMostra(frxCartaSacadoVersoTudo, 'Verso da carta ao sacado');
    end;
    AtualizaImpressaoDeCarta(QrLotesCodigo.Value, 1);
  end;
end;

procedure TFmLotes1.Imprimeconsultaselecionada1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxSPC_Result, 'Resultado de consulta ao SPC / SERASA');
end;

procedure TFmLotes1.frxCartaSacadoVersoTudoGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_COLORENV') = 0 then
  begin
    if Dmod.QrControleColoreEnvelope.Value = 1 then Value := $EBEBEB
    else Value := $FFFFFF;
  end
  else ;
end;

procedure TFmLotes1.Novo11Click(Sender: TObject);
begin
end;

//falta fazer frjuros0, frColigadas0, frcoligadas1,  (e outros?)
procedure TFmLotes1.Contabilidade1Click(Sender: TObject);
begin
  ImprimeBorderoContabilNovo;
end;

procedure TFmLotes1.AditivoNPAP1Click(Sender: TObject);
begin
  ImprimeAditivoNPAPNovo;
end;

procedure TFmLotes1.AditivoAPNP1Click(Sender: TObject);
begin
  ImprimeAditivoAPNPNovo;
end;

procedure TFmLotes1.AditivoNP1Click(Sender: TObject);
begin
  ImprimeAditivoNPNovo;
end;

procedure TFmLotes1.Abrejaneladeconsulta1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPC_Pesq, FmSPC_Pesq, afmoNegarComAviso) then
  begin
    FmSPC_Pesq.ShowModal;
    FmSPC_Pesq.Destroy;
  end;
end;

procedure TFmLotes1.Aditivo1Click(Sender: TObject);
begin
  ImprimeAditivoNovo;
end;

procedure TFmLotes1.Promissria1Click(Sender: TObject);
begin
  ImprimeNotaPromissoriaNovo;
end;

procedure TFmLotes1.AutorizaodeProtesto1Click(Sender: TObject);
begin
  ImprimeAutorizacaodeProtestoNovo;
end;

procedure TFmLotes1.Coligadas1Click(Sender: TObject);
begin
  ImprimeMargemColigadasNovo;
end;

procedure TFmLotes1.MargemtOtal1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  QrJuros0.Close;
  QrJuros0.Params[0].AsInteger := QrLotesCodigo.Value;
  QrJuros0.Open;
  Screen.Cursor := crDefault;
  //
  MyObjects.frxMostra(frxJuros0, 'Margem Total');
end;

procedure TFmLotes1.ConsultaCNPJCPF_CH();
var
  Solicitante, RG, UF, Conta, Endereco, Telefone, CEP: String;
  Nascimento: TDateTime;
  Banco, Agencia, Cheque, DigitoCC, DigitoCH, Qtde: Integer;
  Valor, ValMin, ValMax: Double;
  //Continua: Boolean;
begin
  Solicitante := VAR_LOGIN + '#' + FormatFloat('0', VAR_USUARIO);
  RG          := '';
  UF          := '';
  Nascimento  := 0;
  //
  Banco       := Geral.IMV(EdBanco.Text);
  Agencia     := Geral.IMV(EdAgencia.Text);
  Conta       := EdRealCC.Text;
  Cheque      := Geral.IMV(EdCheque.Text);
  //
  DigitoCC    := 0;
  DigitoCH    := 0;
  Qtde        := 1;
  Valor       := Geral.DMV(EdValor.Text);
  //
  Endereco    := '';
  Telefone    := '';
  CEP         := '';
  //
  ValMin      := 0;
  ValMax      := 0;
  //
  EdStatusSPC.Text := Geral.FF0(
  UnSPC.ConsultaSPC_Rapida(Dmlotes.QrSPC_CliCodigo.Value, True, EdCPF.Text,
    EdEmitente.Text, DmLotes.QrSPC_CliServidor.Value,
    DmLotes.QrSPC_CliSPC_Porta.Value, DmLotes.QrSPC_CliCodigSocio.Value,
    DmLotes.QrSPC_CliSenhaSocio.Value, DmLotes.QrSPC_CliModalidade.Value,
    Solicitante, QrLotesData.Value, DmLotes.QrSPC_CliBAC_CMC7.Value,
    EdBanda.Text, RG, UF, Nascimento, Banco, FormatFloat('0', Agencia), Conta, DigitoCC,
    Cheque, DigitoCH, Qtde, DmLotes.QrSPC_CliTipoCred.Value, Valor,
    DmLotes.QrSPC_CliValAviso.Value, DmLotes.QrSPC_CliVALCONSULTA.Value,
    Endereco, Telefone, CEP, ValMin, ValMax, nil, MeSPC, True));
end;

procedure TFmLotes1.ConsultaosCPFCNPJdositensselecionados1Click(Sender: TObject);
begin
  Dmoc.VerificaCPFCNP_XX_Importacao(EdNomCli.Text, Geral.IMV(EdCodCli.Text),
    Pagina.ActivePageIndex, istSelecionados);
end;

procedure TFmLotes1.HabilitaBotoes(Meu_Sit, TipoDoc: Integer);
begin
  case Meu_Sit of
    0:
    begin
      BtEntidades.Enabled  := TipoDoc = 0;
      BtEntidades2.Enabled := TipoDoc = 1;
      BtCuidado.Enabled    := False;
      BtEntiTroca1.Enabled := False;
    end;
    1:
    begin
      BtEntidades.Enabled  := False;
      BtEntidades2.Enabled := False;
      BtCuidado.Enabled    := True;
      BtEntiTroca1.Enabled := True;
    end;
    2:
    begin
      BtEntidades.Enabled  := False;
      BtEntidades2.Enabled := False;
      BtCuidado.Enabled    := False;
      BtEntiTroca1.Enabled := False;
    end;
    else
    begin
      BtEntidades.Enabled  := False;
      BtCuidado.Enabled    := True;
      BtEntiTroca1.Enabled := False;
    end;
  end;
end;

procedure TFmLotes1.VerificaCPFCNP_XX_LotesIts(SelType: TSelType);
  procedure VerificaCPF_Atual(const MinMax: Boolean; var Contagem: Integer;
  AvisaValor: Boolean);
  var
    TipoDoc, Solicitante, RG, UF, Endereco, Telefone, CEP, Banda: String;
    Nascimento: TDateTime;
    DigitoCC, DigitoCH, Qtde: Integer;
    //
    StatusSPC: Integer;
    ValMax, ValMin: Double;
  begin
    TipoDoc := MLAGeral.ObtemTipoDocTxtDeCPFCNPJ(QrLotesItsCPF.Value);
    StCPF_LI.Caption := 'Aguarde... consultando ' + TipoDoc +
      Geral.FormataCNPJ_TT(QrLotesItsCPF.Value);
    StCPF_LI.Update;
    //
    Application.ProcessMessages;
    //
    Solicitante := VAR_LOGIN + '#' + FormatFloat('0', VAR_USUARIO);
    RG          := '';
    UF          := '';
    Nascimento  := 0;
    //
    //MLAGeral.GeraCMC7_30(Comp, Banco, Agencia, Conta, Cheque, Tipif, Banda);
    Banda       := '';
    //
    DigitoCC    := 0;
    DigitoCH    := 0;
    Qtde        := 1;
    //
    Endereco    := '';
    Telefone    := '';
    CEP         := '';
    //
    if MinMax then
    begin
      ValMin := DmLotes.QrSPC_CfgSPC_ValMin.Value;
      ValMax := DmLotes.QrSPC_CfgSPC_ValMax.Value;
    end else begin
      ValMin := 0;
      ValMax := 0;
    end;
    StatusSPC :=
    UnSPC.ConsultaSPC_Rapida(DmLotes.QrSPC_CfgCodigo.Value, True, QrLotesItsCPF.Value,
      QrLotesItsEmitente.Value, DmLotes.QrSPC_CfgServidor.Value,
      DmLotes.QrSPC_CfgSPC_Porta.Value, DmLotes.QrSPC_CfgCodigSocio.Value,
      DmLotes.QrSPC_CfgSenhaSocio.Value, DmLotes.QrSPC_CfgModalidade.Value,
      Solicitante, QrLotesItsDCompra.Value, DmLotes.QrSPC_CfgBAC_CMC7.Value,
      Banda, RG, UF, Nascimento, QrLotesItsBanco.Value, FormatFloat('0', QrLotesItsAgencia.Value),
      QrLotesItsConta.Value, DigitoCC, QrLotesItsCheque.Value, DigitoCH, Qtde,
      DmLotes.QrSPC_CfgTipoCred.Value, QrLotesItsValor.Value,
      DmLotes.QrSPC_CfgValAviso.Value, DmLotes.QrSPC_CfgVALCONSULTA.Value,
      Endereco, Telefone, CEP, ValMax, ValMin, nil, nil, AvisaValor);
    Dmod.QrUpd.Params[00].AsInteger := StatusSPC;
    Dmod.QrUpd.Params[01].AsInteger := QrLotesItsCodigo.Value;
    Dmod.QrUpd.Params[02].AsString  := QrLotesItsCPF.Value;
    Dmod.QrUpd.ExecSQL;
    //
    if StatusSPC = 2 then Contagem := Contagem + 1;
  end;
var
  Controle, i, Conta: Integer;
  Grade: TDBGrid;
begin
  DmLotes.QrSPC_Cfg.Close;
  DmLotes.QrSPC_Cfg.Params[0].AsInteger := QrLotesCliente.Value;
  DmLotes.QrSPC_Cfg.Open;
  //
  if DmLotes.QrSPC_CfgCodigo.Value = 0 then
  begin
    Geral.MB_Aviso('O cliente ' + Geral.FF0(QrLotesCliente.Value) +
      ' - ' + QrLotesNOMECLIENTE.Value +
      ' n�o possui configura��o de pesquisa ao SPC em seu cadastro!');
    Exit;
  end;
  //
  {
  case Pagina of
    0: VerificaCPFCNP_CH_Importacao(SelType);
    1: VerificaCPFCNP_DU_Importacao(SelType);
    else Geral.MB_Aviso('Aba n�o definida para consulta de CPF ' +
      '/CNPJ na importa��o de lote!');
  end;
  }
  StCPF_LI.Visible := True;
  Controle := QrLotesItsControle.Value;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET StatusSPC=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1 AND CPF=:P2');
  //
  Conta := 0;
  case SelType of
    istAtual: VerificaCPF_Atual(False, Conta, True);
    istExtra1, istExtra2:
    begin
      DmLotes.QrILi.Close;
      DmLotes.QrILi.Params[0].AsInteger := QrLotesItsCodigo.Value;
      DmLotes.QrILi.Open;
      //
      if UnSPC.AceitaValorSPC(DmLotes.QrSPC_CfgCodigo.Value,
      DmLotes.QrSPC_CfgVALCONSULTA.Value, DmLotes.QrILi.RecordCount,
      DmLotes.QrSPC_CfgValAviso.Value) =
      ID_YES then
      begin
        ProgressBar1.Position := 0;
        ProgressBar1.Max := DmLotes.QrILi.RecordCount;
        ProgressBar1.Visible := True;
        //
        while not DmLotes.QrILi.Eof do
        begin
          ProgressBar1.Position := ProgressBar1.Position + 1;
          ProgressBar1.Update;
          Application.ProcessMessages;
          //
          if QrLotesIts.Locate('CPF', DmLotes.QrILiCPF.Value, [])  then
          begin
            if SelType = istExtra1 then
              VerificaCPF_Atual(False, Conta, False)
            else
              VerificaCPF_Atual(True, Conta, False);
          end;
          //
          DmLotes.QrILi.Next;
        end;
      end;
    end;
    istSelecionados:
    begin
      if QrLotesTipo.Value = 0 then
        Grade := GradeCHE
      else
        Grade := GradeDUP;
      if Grade.SelectedRows.Count > 0 then
      begin
        //
        if UnSPC.AceitaValorSPC(DmLotes.QrSPC_CfgCodigo.Value,
        DmLotes.QrSPC_CfgVALCONSULTA.Value, DmLotes.QrILi.RecordCount,
        DmLotes.QrSPC_CfgValAviso.Value) =
        ID_YES then
        begin
          ProgressBar1.Position := 0;
          ProgressBar1.Max := Grade.SelectedRows.Count;
          ProgressBar1.Visible := True;
          //
          with Grade.DataSource.DataSet do
          for i:= 0 to Grade.SelectedRows.Count-1 do
          begin
            ProgressBar1.Position := ProgressBar1.Position + 1;
            ProgressBar1.Update;
            Application.ProcessMessages;
            //
            GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
            VerificaCPF_Atual(False, Conta, False);
          end;
        end;
      end else VerificaCPF_Atual(False, Conta, True);
    end;
  end;
  //
  FControlIts := Controle;
  ReopenIts;
  StCPF_LI.Visible := False;
  ProgressBar1.Visible := False;
  case Conta of
    0:
      ; // Nada
    1:
      Geral.MB_Aviso('Foi localizado um documento com informa��es!');
    else
      Geral.MB_Aviso('Foram localizados ' + Geral.FF0(Conta) + ' documentos com informa��es!');
  end;
end;

end.



