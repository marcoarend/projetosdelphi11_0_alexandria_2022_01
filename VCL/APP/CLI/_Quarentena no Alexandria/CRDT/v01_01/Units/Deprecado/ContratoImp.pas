unit ContratoImp;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt, UnDmkProcFunc,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, Grids, DBGrids, dmkEditCB,
  dmkDBLookupComboBox, ComCtrls, dmkEditDateTimePicker, frxClass, frxDBSet,
  Variants, UnDmkEnums, DmkDAC_PF;

type
  TFmContratoImp = class(TForm)
    PainelDados: TPanel;
    DsContratos: TDataSource;
    QrContratos: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PnPesq: TPanel;
    SbImprime: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    Panel3: TPanel;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Panel4: TPanel;
    RGOrdem2: TRadioGroup;
    RGOrdem1: TRadioGroup;
    DBGrid1: TDBGrid;
    QrContratosNOMECLIENTE: TWideStringField;
    QrContratosNOMEF11: TWideStringField;
    QrContratosNOMEF12: TWideStringField;
    QrContratosNOMEF21: TWideStringField;
    QrContratosNOMEF22: TWideStringField;
    QrContratosCodigo: TIntegerField;
    QrContratosCliente: TIntegerField;
    QrContratosContrato: TIntegerField;
    QrContratosDataC: TDateField;
    QrContratosLimite: TFloatField;
    QrContratosFiador11: TIntegerField;
    QrContratosFiador12: TIntegerField;
    QrContratosFiador21: TIntegerField;
    QrContratosFiador22: TIntegerField;
    QrContratosTestem1Nome: TWideStringField;
    QrContratosTestem1CPF: TWideStringField;
    QrContratosTestem2Nome: TWideStringField;
    QrContratosTestem2CPF: TWideStringField;
    QrContratosTexto: TIntegerField;
    QrContratosLk: TIntegerField;
    QrContratosDataCad: TDateField;
    QrContratosDataAlt: TDateField;
    QrContratosUserCad: TIntegerField;
    QrContratosUserAlt: TIntegerField;
    QrContratosAlterWeb: TSmallintField;
    QrContratosAtivo: TSmallintField;
    QrContratosTestem1CPF_TXT: TWideStringField;
    QrContratosTestem2CPF_TXT: TWideStringField;
    QrContratosDATAC_TXT: TWideStringField;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    QrClientes: TmySQLQuery;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    DsClientes: TDataSource;
    Qr21: TmySQLQuery;
    Qr21NOMEFIADOR: TWideStringField;
    Qr21Codigo: TIntegerField;
    Ds21: TDataSource;
    QrCartaG: TmySQLQuery;
    QrCartaGCodigo: TIntegerField;
    QrCartaGNome: TWideStringField;
    QrCartaGLk: TIntegerField;
    QrCartaGDataCad: TDateField;
    QrCartaGDataAlt: TDateField;
    QrCartaGUserCad: TIntegerField;
    QrCartaGUserAlt: TIntegerField;
    DsCartaG: TDataSource;
    Ds11: TDataSource;
    Ds12: TDataSource;
    Ds22: TDataSource;
    CBCliente: TdmkDBLookupComboBox;
    EdCliente: TdmkEditCB;
    Label19: TLabel;
    TPDataC: TdmkEditDateTimePicker;
    Label5: TLabel;
    EdLimite: TdmkEdit;
    Label20: TLabel;
    EdContrato: TdmkEdit;
    Label21: TLabel;
    Label1: TLabel;
    Ed11: TdmkEditCB;
    CB11: TdmkDBLookupComboBox;
    CB21: TdmkDBLookupComboBox;
    Ed21: TdmkEditCB;
    Label2: TLabel;
    Label3: TLabel;
    Ed12: TdmkEditCB;
    CB12: TdmkDBLookupComboBox;
    Label4: TLabel;
    Ed22: TdmkEditCB;
    CB22: TdmkDBLookupComboBox;
    EdNome1: TdmkEdit;
    Label6: TLabel;
    EdNome2: TdmkEdit;
    Label8: TLabel;
    Label9: TLabel;
    EdCPF1: TdmkEdit;
    Label10: TLabel;
    EdCPF2: TdmkEdit;
    Label11: TLabel;
    EdTexto: TdmkEditCB;
    CBTexto: TdmkDBLookupComboBox;
    frxDsEmpresa: TfrxDBDataset;
    DsEmpresa: TDataSource;
    QrEmpresa: TmySQLQuery;
    QrEmpresaCodigo: TIntegerField;
    QrEmpresaRazaoSocial: TWideStringField;
    QrEmpresaCadastro: TDateField;
    QrEmpresaENatal: TDateField;
    QrEmpresaCNPJ: TWideStringField;
    QrEmpresaIE: TWideStringField;
    QrEmpresaSimples: TSmallintField;
    QrEmpresaELograd: TSmallintField;
    QrEmpresaERua: TWideStringField;
    QrEmpresaECompl: TWideStringField;
    QrEmpresaEBairro: TWideStringField;
    QrEmpresaECidade: TWideStringField;
    QrEmpresaECEP: TIntegerField;
    QrEmpresaETe1: TWideStringField;
    QrEmpresaEFax: TWideStringField;
    QrEmpresaETE1_TXT: TWideStringField;
    QrEmpresaFAX_TXT: TWideStringField;
    QrEmpresaFormaSociet: TWideStringField;
    QrEmpresaCNPJ_TXT: TWideStringField;
    QrEmpresaE_LNR: TWideStringField;
    QrEmpresaNOMEUF: TWideStringField;
    QrEmpresaNUMERO_TXT: TWideStringField;
    QrEmpresaNOMELOGRAD: TWideStringField;
    QrEmpresaECEP_TXT: TWideStringField;
    QrEmpresaAtividade: TWideStringField;
    QrEmpresaENATAL_TXT: TWideStringField;
    QrEmpresaENumero: TIntegerField;
    QrEmpresaNIRE: TWideStringField;
    frxDsSocios: TfrxDBDataset;
    QrSocios: TmySQLQuery;
    QrSociosNOMESOCIO: TWideStringField;
    QrSociosCodigo: TIntegerField;
    QrSociosSexo: TWideStringField;
    QrSociosPai: TWideStringField;
    QrSociosMae: TWideStringField;
    QrSociosPNatal: TDateField;
    QrSociosCidadeNatal: TWideStringField;
    QrSociosNacionalid: TWideStringField;
    QrSociosConjugeNome: TWideStringField;
    QrSociosCPF: TWideStringField;
    QrSociosRG: TWideStringField;
    QrSociosSSP: TWideStringField;
    QrSociosDataRG: TDateField;
    QrSociosPRua: TWideStringField;
    QrSociosPCompl: TWideStringField;
    QrSociosPBairro: TWideStringField;
    QrSociosPCidade: TWideStringField;
    QrSociosPCEP: TIntegerField;
    QrSociosPTe1: TWideStringField;
    QrSociosProfissao: TWideStringField;
    QrSociosCargo: TWideStringField;
    QrSociosNOMEPUF: TWideStringField;
    QrSociosNOMEPLOGRAD: TWideStringField;
    QrSociosNOMEECIVIL: TWideStringField;
    QrSociosPNATAL_TXT: TWideStringField;
    QrSociosDATARG_TXT: TWideStringField;
    QrSociosETE1_TXT: TWideStringField;
    QrSociosCNPJ_TXT: TWideStringField;
    QrSociosE_LNR: TWideStringField;
    QrSociosNUMERO_TXT: TWideStringField;
    QrSociosECEP_TXT: TWideStringField;
    QrSociosITEM: TIntegerField;
    QrSociosNOMESEXO: TWideStringField;
    QrSociosRazaoSocial: TWideStringField;
    QrSociosFantasia: TWideStringField;
    QrSociosRespons1: TWideStringField;
    QrSociosRespons2: TWideStringField;
    QrSociosCNPJ: TWideStringField;
    QrSociosIE: TWideStringField;
    QrSociosFormaSociet: TWideStringField;
    QrSociosSimples: TSmallintField;
    QrSociosIEST: TWideStringField;
    QrSociosAtividade: TWideStringField;
    QrSociosNome: TWideStringField;
    QrSociosApelido: TWideStringField;
    QrSociosCPF_Pai: TWideStringField;
    QrSociosEstCivil: TSmallintField;
    QrSociosUFNatal: TSmallintField;
    QrSociosELograd: TSmallintField;
    QrSociosERua: TWideStringField;
    QrSociosECompl: TWideStringField;
    QrSociosEBairro: TWideStringField;
    QrSociosECidade: TWideStringField;
    QrSociosEUF: TSmallintField;
    QrSociosECEP: TIntegerField;
    QrSociosEPais: TWideStringField;
    QrSociosETe1: TWideStringField;
    QrSociosEte2: TWideStringField;
    QrSociosEte3: TWideStringField;
    QrSociosECel: TWideStringField;
    QrSociosEFax: TWideStringField;
    QrSociosEEmail: TWideStringField;
    QrSociosEContato: TWideStringField;
    QrSociosENatal: TDateField;
    QrSociosPLograd: TSmallintField;
    QrSociosPUF: TSmallintField;
    QrSociosPPais: TWideStringField;
    QrSociosPte2: TWideStringField;
    QrSociosPte3: TWideStringField;
    QrSociosPCel: TWideStringField;
    QrSociosPFax: TWideStringField;
    QrSociosPEmail: TWideStringField;
    QrSociosPContato: TWideStringField;
    QrSociosResponsavel: TWideStringField;
    QrSociosRecibo: TSmallintField;
    QrSociosDiaRecibo: TSmallintField;
    QrSociosAjudaEmpV: TFloatField;
    QrSociosAjudaEmpP: TFloatField;
    QrSociosCliente1: TWideStringField;
    QrSociosCliente2: TWideStringField;
    QrSociosFornece1: TWideStringField;
    QrSociosFornece2: TWideStringField;
    QrSociosFornece3: TWideStringField;
    QrSociosFornece4: TWideStringField;
    QrSociosFornece5: TWideStringField;
    QrSociosFornece6: TWideStringField;
    QrSociosTerceiro: TWideStringField;
    QrSociosCadastro: TDateField;
    QrSociosInformacoes: TWideStringField;
    QrSociosLogo: TBlobField;
    QrSociosVeiculo: TIntegerField;
    QrSociosMensal: TWideStringField;
    QrSociosObservacoes: TWideMemoField;
    QrSociosTipo: TSmallintField;
    QrSociosCLograd: TSmallintField;
    QrSociosCRua: TWideStringField;
    QrSociosCCompl: TWideStringField;
    QrSociosCBairro: TWideStringField;
    QrSociosCCidade: TWideStringField;
    QrSociosCUF: TSmallintField;
    QrSociosCCEP: TIntegerField;
    QrSociosCPais: TWideStringField;
    QrSociosCTel: TWideStringField;
    QrSociosCCel: TWideStringField;
    QrSociosCFax: TWideStringField;
    QrSociosCContato: TWideStringField;
    QrSociosLLograd: TSmallintField;
    QrSociosLRua: TWideStringField;
    QrSociosLCompl: TWideStringField;
    QrSociosLBairro: TWideStringField;
    QrSociosLCidade: TWideStringField;
    QrSociosLUF: TSmallintField;
    QrSociosLCEP: TIntegerField;
    QrSociosLPais: TWideStringField;
    QrSociosLTel: TWideStringField;
    QrSociosLCel: TWideStringField;
    QrSociosLFax: TWideStringField;
    QrSociosLContato: TWideStringField;
    QrSociosComissao: TFloatField;
    QrSociosSituacao: TSmallintField;
    QrSociosNivel: TWideStringField;
    QrSociosGrupo: TIntegerField;
    QrSociosAccount: TIntegerField;
    QrSociosLogo2: TBlobField;
    QrSociosConjugeNatal: TDateField;
    QrSociosNome1: TWideStringField;
    QrSociosNatal1: TDateField;
    QrSociosNome2: TWideStringField;
    QrSociosNatal2: TDateField;
    QrSociosNome3: TWideStringField;
    QrSociosNatal3: TDateField;
    QrSociosNome4: TWideStringField;
    QrSociosNatal4: TDateField;
    QrSociosCreditosI: TIntegerField;
    QrSociosCreditosL: TIntegerField;
    QrSociosCreditosF2: TFloatField;
    QrSociosCreditosD: TDateField;
    QrSociosCreditosU: TDateField;
    QrSociosCreditosV: TDateField;
    QrSociosMotivo: TIntegerField;
    QrSociosQuantI1: TIntegerField;
    QrSociosQuantI2: TIntegerField;
    QrSociosQuantI3: TIntegerField;
    QrSociosQuantI4: TIntegerField;
    QrSociosQuantN1: TFloatField;
    QrSociosQuantN2: TFloatField;
    QrSociosAgenda: TWideStringField;
    QrSociosSenhaQuer: TWideStringField;
    QrSociosSenha1: TWideStringField;
    QrSociosLimiCred: TFloatField;
    QrSociosDesco: TFloatField;
    QrSociosCasasApliDesco: TSmallintField;
    QrSociosTempD: TFloatField;
    QrSociosBanco: TIntegerField;
    QrSociosAgencia: TWideStringField;
    QrSociosContaCorrente: TWideStringField;
    QrSociosFatorCompra: TFloatField;
    QrSociosAdValorem: TFloatField;
    QrSociosDMaisC: TIntegerField;
    QrSociosDMaisD: TIntegerField;
    QrSociosEmpresa: TIntegerField;
    QrSociosLk: TIntegerField;
    QrSociosDataCad: TDateField;
    QrSociosDataAlt: TDateField;
    QrSociosUserCad: TIntegerField;
    QrSociosUserAlt: TIntegerField;
    QrSociosCPF_Conjuge: TWideStringField;
    QrSociosEmpresa_1: TIntegerField;
    QrSociosSocio: TIntegerField;
    QrSociosOrdem: TIntegerField;
    QrSociosENumero: TIntegerField;
    QrSociosPNumero: TIntegerField;
    QrSociosNIRE: TWideStringField;
    DsSocios: TDataSource;
    QrFiadores: TmySQLQuery;
    IntegerField1: TIntegerField;
    DsFiadores: TDataSource;
    frxDsFiadores: TfrxDBDataset;
    QrRepr: TmySQLQuery;
    QrReprCodigo: TIntegerField;
    QrReprNome: TWideStringField;
    QrReprSexo: TWideStringField;
    QrReprPai: TWideStringField;
    QrReprMae: TWideStringField;
    QrReprPNatal: TDateField;
    QrReprNOMEPUF: TWideStringField;
    QrReprNOMEPLOGRAD: TWideStringField;
    QrReprNOMEECIVIL: TWideStringField;
    QrReprITEM: TIntegerField;
    QrReprNOMESEXO: TWideStringField;
    QrReprCidadeNatal: TWideStringField;
    QrReprNacionalid: TWideStringField;
    QrReprConjugeNome: TWideStringField;
    QrReprCPF: TWideStringField;
    QrReprRG: TWideStringField;
    QrReprSSP: TWideStringField;
    QrReprDataRG: TDateField;
    QrReprPRua: TWideStringField;
    QrReprPCompl: TWideStringField;
    QrReprPBairro: TWideStringField;
    QrReprPCidade: TWideStringField;
    QrReprPCEP: TIntegerField;
    QrReprPTe1: TWideStringField;
    QrReprProfissao: TWideStringField;
    QrReprCargo: TWideStringField;
    QrReprECEP_TXT: TWideStringField;
    QrReprNUMERO_TXT: TWideStringField;
    QrReprE_LNR: TWideStringField;
    QrReprCNPJ_TXT: TWideStringField;
    QrReprETE1_TXT: TWideStringField;
    QrReprDATARG_TXT: TWideStringField;
    QrReprPNATAL_TXT: TWideStringField;
    QrReprPNumero: TIntegerField;
    DsRepr: TDataSource;
    frxDsRepr: TfrxDBDataset;
    frxContrato: TfrxReport;
    QrFiadoresITEM: TIntegerField;
    QrFiadoresSexo: TWideStringField;
    QrFiadoresNOMESEXO: TWideStringField;
    QrFiadoresETE1_TXT: TWideStringField;
    QrFiadoresPTe1: TWideStringField;
    QrFiadoresCNPJ_TXT: TWideStringField;
    QrFiadoresCPF: TWideStringField;
    QrFiadoresNUMERO_TXT: TWideStringField;
    QrFiadoresPNumero: TIntegerField;
    QrFiadoresECEP_TXT: TWideStringField;
    QrFiadoresPCEP: TIntegerField;
    QrFiadoresE_LNR: TWideStringField;
    QrFiadoresNOMEPLOGRAD: TWideStringField;
    QrFiadoresPRua: TWideStringField;
    QrFiadoresPCompl: TWideStringField;
    QrFiadoresPBairro: TWideStringField;
    QrFiadoresDataRG: TDateField;
    QrFiadoresDATARG_TXT: TWideStringField;
    QrFiadoresPNatal: TDateField;
    QrFiadoresPNATAL_TXT: TWideStringField;
    QrFiadoresNOMETIPOFIADOR: TWideStringField;
    QrFiadoresTIPOFIADOR: TIntegerField;
    QrCartas: TmySQLQuery;
    QrCartasCodigo: TIntegerField;
    QrCartasTitulo: TWideStringField;
    QrCartasTexto: TWideMemoField;
    QrCartasLk: TIntegerField;
    QrCartasDataCad: TDateField;
    QrCartasDataAlt: TDateField;
    QrCartasUserCad: TIntegerField;
    QrCartasUserAlt: TIntegerField;
    frxDsCartas: TfrxDBDataset;
    Qr11: TmySQLQuery;
    StringField1: TWideStringField;
    IntegerField2: TIntegerField;
    Qr12: TmySQLQuery;
    StringField2: TWideStringField;
    IntegerField3: TIntegerField;
    Qr22: TmySQLQuery;
    StringField3: TWideStringField;
    IntegerField4: TIntegerField;
    frxDsContratos: TfrxDBDataset;
    Label12: TLabel;
    Ed13: TdmkEditCB;
    CB13: TdmkDBLookupComboBox;
    Label13: TLabel;
    Ed14: TdmkEditCB;
    CB14: TdmkDBLookupComboBox;
    Qr13: TmySQLQuery;
    StringField4: TWideStringField;
    IntegerField5: TIntegerField;
    Ds13: TDataSource;
    Qr14: TmySQLQuery;
    StringField5: TWideStringField;
    IntegerField6: TIntegerField;
    Ds14: TDataSource;
    QrContratosNOMEF13: TWideStringField;
    QrContratosNOMEF14: TWideStringField;
    QrContratosFiador13: TIntegerField;
    QrContratosFiador14: TIntegerField;
    QrFiadoresCPF_Conjuge: TWideStringField;
    QrFiadoresCPF_TXT: TWideStringField;
    QrFiadoresCPF_CONJUGE_TXT: TWideStringField;
    SpeedButton5: TSpeedButton;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton6: TSpeedButton;
    BtEmpresa: TBitBtn;
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrContratosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrContratosBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure QrContratosCalcFields(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure RGOrdem2Click(Sender: TObject);
    procedure QrEmpresaCalcFields(DataSet: TDataSet);
    procedure QrSociosCalcFields(DataSet: TDataSet);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrFiadoresCalcFields(DataSet: TDataSet);
    procedure QrReprAfterOpen(DataSet: TDataSet);
    procedure QrReprCalcFields(DataSet: TDataSet);
    procedure frxContratoGetValue(const VarName: string; var Value: Variant);
    procedure EdNome2Change(Sender: TObject);
    procedure LaTipoTextChange(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure BtEmpresaClick(Sender: TObject);
    procedure frxContratoClickObject(Sender: TfrxView; Button: TMouseButton;
      Shift: TShiftState; var Modified: Boolean);
  private
    Fiadores: string;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure ReopenContratos(Codigo: Integer);
    procedure MostraEntidades(EditCB: TdmkEditCB; ComboBox: TdmkDBLookupComboBox;
              Query: TmySQLQuery);
    procedure ReopenEmpresa(Entidade: Integer);
  public
    { Public declarations }
  end;

var
  FmContratoImp: TFmContratoImp;
const
  FFormatFloat = '00000';

implementation

uses Module, ModuleGeral, UCreate, Entidades, CartaG, UnMyObjects, Principal;

{$R *.DFM}

procedure TFmContratoImp.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant   := FormatFloat(FFormatFloat, Codigo);
        EdCliente.ValueVariant  := 0;
        CBCliente.KeyValue      := Null;
        TPDataC.Date            := Date;
        EdLimite.ValueVariant   := 0;
        EdContrato.ValueVariant := 0;
        Ed11.ValueVariant       := 0;
        CB11.KeyValue           := Null;
        Ed12.ValueVariant       := 0;
        CB12.KeyValue           := Null;
        Ed13.ValueVariant       := 0;
        CB13.KeyValue           := Null;
        Ed14.ValueVariant       := 0;
        CB14.KeyValue           := Null;
        Ed21.ValueVariant       := 0;
        CB21.KeyValue           := Null;
        Ed22.ValueVariant       := 0;
        CB22.KeyValue           := Null;
        EdNome1.ValueVariant    := '';
        EdCPF1.ValueVariant     := '';
        EdNome2.ValueVariant    := '';
        EdCPF2.ValueVariant     := '';
        EdTexto.ValueVariant    := 0;
        CBTexto.KeyValue        := Null;
     end else
     begin
        EdCodigo.ValueVariant   := QrContratosCodigo.Value;
        EdCliente.ValueVariant  := QrContratosCliente.Value;
        CBCliente.KeyValue      := QrContratosCliente.Value;
        TPDataC.Date            := QrContratosDataC.Value;
        EdLimite.ValueVariant   := QrContratosLimite.Value;
        EdContrato.ValueVariant := QrContratosContrato.Value;
        Ed11.ValueVariant       := QrContratosFiador11.Value;
        CB11.KeyValue           := QrContratosFiador11.Value;
        Ed12.ValueVariant       := QrContratosFiador12.Value;
        CB12.KeyValue           := QrContratosFiador12.Value;
        Ed13.ValueVariant       := QrContratosFiador13.Value;
        CB13.KeyValue           := QrContratosFiador13.Value;
        Ed14.ValueVariant       := QrContratosFiador14.Value;
        CB14.KeyValue           := QrContratosFiador14.Value;
        Ed21.ValueVariant       := QrContratosFiador21.Value;
        CB21.KeyValue           := QrContratosFiador21.Value;
        Ed22.ValueVariant       := QrContratosFiador22.Value;
        CB22.KeyValue           := QrContratosFiador22.Value;
        EdNome1.ValueVariant    := QrContratosTestem1Nome.Value;
        EdCPF1.ValueVariant     := QrContratosTestem1CPF.Value;
        EdNome2.ValueVariant    := QrContratosTestem2Nome.Value;
        EdCPF2.ValueVariant     := QrContratosTestem2CPF.Value;
        EdTexto.ValueVariant    := QrContratosTexto.Value;
        CBTexto.KeyValue        := QrContratosTexto.Value;
     end;
     EdCliente.SetFocus;
    end;
    else Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.SQLType);
end;

procedure TFmContratoImp.MostraEntidades(EditCB: TdmkEditCB;
  ComboBox: TdmkDBLookupComboBox; Query: TmySQLQuery);
begin
  VAR_CADASTRO := 0;
  //
  DModG.CadastroDeEntidade(EditCB.ValueVariant, fmcadEntidade2, fmcadEntidade2);
  {
  Application.CreateForm(TFmEntidades, FmEntidades);
  FmEntidades.ShowModal;
  FmEntidades.Destroy;
  }
  if VAR_CADASTRO <> 0 then
  begin
    Query.Close;
    Query.Open;
    //
    EditCB.ValueVariant := VAR_ENTIDADE;
    ComboBox.KeyValue   := VAR_ENTIDADE;
    EditCB.SetFocus;
  end;
end;

procedure TFmContratoImp.QrFiadoresCalcFields(DataSet: TDataSet);
begin
  QrFiadoresITEM.Value := QrFiadores.RecNo;
  //
  if QrFiadoresSexo.Value = 'M' then QrFiadoresNOMESEXO.Value := 'MASCULINO'
  else if QrFiadoresSexo.Value = 'F' then QrFiadoresNOMESEXO.Value := 'FEMININO'
  else QrFiadoresNOMESEXO.Value := '';
  //
  QrFiadoresETE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrFiadoresPTe1.Value);
  QrFiadoresCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrFiadoresCPF.Value);
  QrFiadoresNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrFiadoresPRua.Value, QrFiadoresPNumero.Value, True);
  QrFiadoresECEP_TXT.Value := Geral.FormataCEP_NT(QrFiadoresPCEP.Value);
  QrFiadoresE_LNR.Value := QrFiadoresNOMEPLOGRAD.Value;
  if Trim(QrFiadoresE_LNR.Value) <> '' then QrFiadoresE_LNR.Value :=
    QrFiadoresE_LNR.Value + ' ';
  QrFiadoresE_LNR.Value := QrFiadoresE_LNR.Value + QrFiadoresPRua.Value;
  if Trim(QrFiadoresPRua.Value) <> '' then QrFiadoresE_LNR.Value :=
    QrFiadoresE_LNR.Value + ', ' + QrFiadoresNUMERO_TXT.Value;
  if Trim(QrFiadoresPCompl.Value) <>  '' then QrFiadoresE_LNR.Value :=
    QrFiadoresE_LNR.Value + ' ' + QrFiadoresPCompl.Value;
  if Trim(QrFiadoresPBairro.Value) <>  '' then QrFiadoresE_LNR.Value :=
    QrFiadoresE_LNR.Value + ' - ' + QrFiadoresPBairro.Value;
  if QrFiadoresPCEP.Value > 0 then QrFiadoresE_LNR.Value :=
    QrFiadoresE_LNR.Value + ' CEP ' + QrFiadoresECEP_TXT.Value;
  //
  if QrFiadoresDataRG.Value < 2 then QrFiadoresDATARG_TXT.Value := ''
  else QrFiadoresDATARG_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE2, QrFiadoresDataRG.Value);
  if QrFiadoresPNatal.Value < 2 then QrFiadoresPNATAL_TXT.Value := ''
  else QrFiadoresPNATAL_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE2, QrFiadoresPNatal.Value);
  //
  case QrFiadoresTIPOFIADOR.Value of
    1: QrFiadoresNOMETIPOFIADOR.Value := 'Nome do Fiador:';
    2: QrFiadoresNOMETIPOFIADOR.Value := 'Nome do Conjuge:';
    3: QrFiadoresNOMETIPOFIADOR.Value := 'Nome do Fiador:';
    4: QrFiadoresNOMETIPOFIADOR.Value := 'Nome do Conjuge:';
    else QrFiadoresNOMETIPOFIADOR.Value := ' * NOME * :';
  end;
  //
  QrFiadoresCPF_TXT.Value         := Geral.FormataCNPJ_TT(QrFiadoresCPF.Value);
  QrFiadoresCPF_CONJUGE_TXT.Value := Geral.FormataCNPJ_TT(QrFiadoresCPF_Conjuge.Value);
end;

procedure TFmContratoImp.CriaOForm;
begin
  DefineONomeDoForm;
end;

procedure TFmContratoImp.QueryPrincipalAfterOpen;
begin
end;

procedure TFmContratoImp.ReopenContratos(Codigo: Integer);
var
  Ordem, Ordem2: String;
begin
  case RGOrdem2.ItemIndex of
      0: Ordem2 := ' ASC';
    else Ordem2 := ' DESC';
  end;
  case RGOrdem1.ItemIndex of
    0: Ordem := 'NOMECLIENTE'+ Ordem2 +', DataC'+ Ordem2 +', Contrato'+ Ordem2;
    1: Ordem := 'Contrato'+ Ordem2 +', NOMECLIENTE'+ Ordem2 +', DataC'+ Ordem2;
    2: Ordem := 'DataC'+ Ordem2 +', NOMECLIENTE'+ Ordem2 +', Contrato'+ Ordem2;
    3: Ordem := 'Limite'+ Ordem2 +', NOMECLIENTE'+ Ordem2 +', DataC'+ Ordem2 +', Contrato'+ Ordem2;
    else Ordem := ' * ERRO ORDEM * ';
  end;
  Ordem := 'ORDER BY ' + Ordem;
  QrContratos.Close;
  QrContratos.SQL.Clear;
  QrContratos.SQL.Add('SELECT');
  QrContratos.SQL.Add('CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOMECLIENTE,');
  QrContratos.SQL.Add('CASE WHEN f11.Tipo=0 THEN f11.RazaoSocial ELSE f11.Nome END NOMEF11,');
  QrContratos.SQL.Add('CASE WHEN f12.Tipo=0 THEN f12.RazaoSocial ELSE f12.Nome END NOMEF12,');
  QrContratos.SQL.Add('CASE WHEN f13.Tipo=0 THEN f13.RazaoSocial ELSE f13.Nome END NOMEF13,');
  QrContratos.SQL.Add('CASE WHEN f14.Tipo=0 THEN f14.RazaoSocial ELSE f14.Nome END NOMEF14,');
  QrContratos.SQL.Add('CASE WHEN f21.Tipo=0 THEN f21.RazaoSocial ELSE f21.Nome END NOMEF21,');
  QrContratos.SQL.Add('CASE WHEN f22.Tipo=0 THEN f22.RazaoSocial ELSE f22.Nome END NOMEF22,');
  QrContratos.SQL.Add('con.*');
  QrContratos.SQL.Add('FROM contratos con');
  QrContratos.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=con.Cliente');
  QrContratos.SQL.Add('LEFT JOIN entidades f11 ON f11.Codigo=con.Fiador11');
  QrContratos.SQL.Add('LEFT JOIN entidades f12 ON f12.Codigo=con.Fiador12');
  QrContratos.SQL.Add('LEFT JOIN entidades f13 ON f13.Codigo=con.Fiador13');
  QrContratos.SQL.Add('LEFT JOIN entidades f14 ON f14.Codigo=con.Fiador14');
  QrContratos.SQL.Add('LEFT JOIN entidades f21 ON f21.Codigo=con.Fiador21');
  QrContratos.SQL.Add('LEFT JOIN entidades f22 ON f22.Codigo=con.Fiador22');
  QrContratos.SQL.Add(Ordem);
  QrContratos.Open;
  //
  if Codigo <> 0 then QrContratos.Locate('Codigo', Codigo, []);
end;

procedure TFmContratoImp.ReopenEmpresa(Entidade: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmpresa, Dmod.MyDB, [
    'SELECT en.Codigo, en.RazaoSocial, en.Cadastro, en.ENatal, en.CNPJ, ',
    'en.IE, en.NIRE, en.Simples, en.ELograd, en.ERua, en.ENumero, en.ECompl, ',
    'en.EBairro, en.ECEP, en.ETe1, en.EFax, en.FormaSociet, en.Atividade, ',
    'uf.Nome NOMEUF, ll.Nome NOMELOGRAD, IF(mu.Nome <> "", mu.Nome, en.ECidade) ECidade ',
    'FROM entidades en ',
    'LEFT JOIN ufs uf ON uf.Codigo=en.EUF ',
    'LEFT JOIN listalograd ll ON ll.Codigo=en.ELograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici mu ON mu.Codigo=en.ECodMunici ',
    'WHERE en.Codigo=' + Geral.FF0(Entidade),
    '']);
end;

procedure TFmContratoImp.RGOrdem1Click(Sender: TObject);
begin
  ReopenContratos(QrContratosCodigo.Value);
end;

procedure TFmContratoImp.RGOrdem2Click(Sender: TObject);
begin
  ReopenContratos(QrContratosCodigo.Value);
end;

procedure TFmContratoImp.SbImprimeClick(Sender: TObject);
begin
  Fiadores := UCriar.RecriaTempTable('Fiadores', DModG.QrUpdPID1, False);
  //
  DModG.QrUpdPID1.Close;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO '+ Fiadores +' SET Codigo=:P0, Tipo=:P1');
  if QrContratosFiador11.Value <> 0 then
  begin
    DModG.QrUpdPID1.Params[0].AsInteger := QrContratosFiador11.Value;
    DModG.QrUpdPID1.Params[1].AsInteger := 1;
    DModG.QrUpdPID1.ExecSQL;
  end;
  if QrContratosFiador12.Value <> 0 then
  begin
    DModG.QrUpdPID1.Params[0].AsInteger := QrContratosFiador12.Value;
    DModG.QrUpdPID1.Params[1].AsInteger := 2;
    DModG.QrUpdPID1.ExecSQL;
  end;
  if QrContratosFiador21.Value <> 0 then
  begin
    DModG.QrUpdPID1.Params[0].AsInteger := QrContratosFiador21.Value;
    DModG.QrUpdPID1.Params[1].AsInteger := 3;
    DModG.QrUpdPID1.ExecSQL;
  end;
  if QrContratosFiador22.Value <> 0 then
  begin
    DModG.QrUpdPID1.Params[0].AsInteger := QrContratosFiador22.Value;
    DModG.QrUpdPID1.Params[1].AsInteger := 4;
    DModG.QrUpdPID1.ExecSQL;
  end;
  if QrContratosFiador13.Value <> 0 then
  begin
    DModG.QrUpdPID1.Params[0].AsInteger := QrContratosFiador13.Value;
    DModG.QrUpdPID1.Params[1].AsInteger := 5;
    DModG.QrUpdPID1.ExecSQL;
  end;
  if QrContratosFiador14.Value <> 0 then
  begin
    DModG.QrUpdPID1.Params[0].AsInteger := QrContratosFiador14.Value;
    DModG.QrUpdPID1.Params[1].AsInteger := 6;
    DModG.QrUpdPID1.ExecSQL;
  end;
  //
  ReopenEmpresa(QrContratosCliente.Value);
  //
  QrSocios.Close;
  QrSocios.Params[00].AsInteger := QrContratosCliente.Value;
  QrSocios.Open;
  //
  QrRepr.Close;
  QrRepr.Open;
  //
  QrFiadores.Close;
  QrFiadores.Open;
  //
  QrCartas.Close;
  QrCartas.Params[0].AsInteger := QrContratosTexto.Value;
  QrCartas.Open;
  //
  Dmod.ReopenDono;
  //
  MyObjects.frxMostra(frxContrato, 'Contrato');
end;

procedure TFmContratoImp.SpeedButton1Click(Sender: TObject);
begin
  MostraEntidades(Ed21, CB21, Qr21);
end;

procedure TFmContratoImp.SpeedButton2Click(Sender: TObject);
begin
  MostraEntidades(Ed13, CB13, Qr13);
end;

procedure TFmContratoImp.SpeedButton3Click(Sender: TObject);
begin
  MostraEntidades(Ed14, CB14, Qr14);
end;

procedure TFmContratoImp.SpeedButton4Click(Sender: TObject);
begin
  Application.CreateForm(TFmCartaG, FmCartaG);
  FmCartaG.ShowModal;
  FmCartaG.Destroy;
  //
  QrCartaG.Close;
  QrCartaG.Open;
  //
  EdTexto.ValueVariant := VAR_CADASTRO;
  CBTexto.KeyValue     := VAR_CADASTRO;
  //
  EdTexto.SetFocus;
end;

procedure TFmContratoImp.SpeedButton5Click(Sender: TObject);
begin
  MostraEntidades(Ed11, CB11, Qr11);
end;

procedure TFmContratoImp.SpeedButton6Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.MostraEntiJur1(EdCliente.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    QrClientes.Close;
    QrClientes.Open;
    //
    EdCliente.ValueVariant := VAR_CADASTRO;
    CBCliente.KeyValue     := VAR_CADASTRO;
    EdCliente.SetFocus;
  end;
end;

procedure TFmContratoImp.DefineONomeDoForm;
begin
end;

procedure TFmContratoImp.EdNome2Change(Sender: TObject);
begin

end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmContratoImp.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrContratos, [PainelDados],
  [PainelEdita], EdCliente, LaTipo, 'Contratos');
end;

procedure TFmContratoImp.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrContratosCodigo.Value;
  Close;
end;

procedure TFmContratoImp.BtConfirmaClick(Sender: TObject);
var
  Codigo, Cliente, Contrato, Texto: Integer;
  Limite: Double;
begin
  Cliente  := EdCliente.ValueVariant;
  Contrato := EdContrato.ValueVariant;
  Limite   := EdLimite.ValueVariant;
  Texto    := EdTexto.ValueVariant;
  //
  if MyObjects.FIC(Cliente = 0, EdCliente, 'Informe o cliente!') then Exit;
  if MyObjects.FIC(Contrato = 0, EdContrato, 'Informe o n�mero do contrato!') then Exit;
  if MyObjects.FIC(Limite = 0, EdLimite, 'Informe o limite de cr�dito!') then Exit;
  if MyObjects.FIC(Texto = 0, EdTexto, 'Informe o modelo de Contrato!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('Contratos', 'Codigo', LaTipo.SQLType,
    QrContratosCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmContratoImp, PainelEdit,
    'Contratos', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    MostraEdicao(0, stLok, 0);
    ReopenContratos(Codigo);
  end;
end;

procedure TFmContratoImp.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmContratoImp.BtEmpresaClick(Sender: TObject);
begin
  DModG.CadastroDeEntidade(Dmod.QrDonoCodigo.Value, fmcadEntidade2, fmcadEntidade2, True);
end;

procedure TFmContratoImp.BtExcluiClick(Sender: TObject);
begin
  if Application.MessageBox(PChar('Confirma a exclus�o do contrato selecionado?'),
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM contratos WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrContratosCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrContratos.Next;
    ReopenContratos(QrContratosCodigo.Value);
  end;
end;

procedure TFmContratoImp.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrContratos, [PainelDados],
  [PainelEdita], EdCliente, LaTipo, 'Contratos');
end;

procedure TFmContratoImp.FormCreate(Sender: TObject);
begin
  QrFiadores.Close;
  QrFiadores.Database := DModG.MyPID_DB;
  //
  QrClientes.Open;
  QrCartaG.Open;
  Qr11.Open;
  Qr12.Open;
  Qr13.Open;
  Qr14.Open;
  Qr21.Open;
  Qr22.Open;
  ReopenContratos(0);
  //
  PainelEdit.Align := alClient;
  DBGrid1.Align    := alClient;
  CriaOForm;
end;

procedure TFmContratoImp.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.SQLType);
end;

procedure TFmContratoImp.QrContratosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  //
  if QrContratos.RecordCount > 0 then
  begin
    BtAltera.Enabled  := True;
    BtExclui.Enabled  := True;
    SbImprime.Enabled := True;
  end else begin
    BtAltera.Enabled  := False;
    BtExclui.Enabled  := False;
    SbImprime.Enabled := False;
  end;
end;

procedure TFmContratoImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmContratoImp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmContratoImp.frxContratoClickObject(Sender: TfrxView;
  Button: TMouseButton; Shift: TShiftState; var Modified: Boolean);
var
  Codigo: Integer;
begin
  Codigo := 0;
  //
  if (Sender.Name = 'Memo106') or (Sender.Name = 'Memo100') or
    (Sender.Name = 'Memo140') or (Sender.Name = 'Memo155') or
    (Sender.Name = 'Memo175')
  then
    Codigo := Geral.IMV(Sender.TagStr);
  if Codigo <> 0 then
  begin
    DModG.CadastroDeEntidade(Codigo, fmcadEntidade2, fmcadEntidade2, True);
    //
    if frxContrato.Preview <> nil then
    begin
      TForm(frxContrato.Preview.Parent).Close;
    end;
  end;
end;

procedure TFmContratoImp.frxContratoGetValue(const VarName: string;
  var Value: Variant);
var
  ValorTxt: String;
begin
  if VarName = 'LIMITE' then
  begin
    ValorTxt := Geral.FFT(QrContratosLimite.Value, 2, siPositivo);
    Value :=  ValorTxt + ' ('+dmkPF.ExtensoMoney(ValorTxt)+')';
  end
  else if VarName = 'NUM_CONT' then
    Value := QrContratosContrato.Value
  else if VarName = 'DATA_CON' then
    Value := FormatDateTime(VAR_FORMATDATE2, QrContratosDataC.Value)
  else if VarName = 'LIMITE  ' then
    Value := Geral.FFT(QrContratosLimite.Value, 2, siPositivo) + ' (' +
      dmkPF.ExtensoMoney(Geral.FFT(QrContratosLimite.Value, 2, siPositivo)) +') '
  else if VarName = 'ADITIVO ' then
    Value := 0
end;

procedure TFmContratoImp.LaTipoTextChange(Sender: TObject);
begin
  SbImprime.Enabled := LaTipo.SQLType = stLok;
end;

procedure TFmContratoImp.QrContratosBeforeOpen(DataSet: TDataSet);
begin
  QrContratosCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmContratoImp.QrContratosCalcFields(DataSet: TDataSet);
begin
  QrContratosTestem1CPF_TXT.Value := 
    Geral.FormataCNPJ_TT(QrContratosTestem1CPF.Value);
  QrContratosTestem2CPF_TXT.Value :=
    Geral.FormataCNPJ_TT(QrContratosTestem2CPF.Value);
  //
  QrContratosDataC_TXT.Value := Geral.Maiusculas(
    FormatDateTime('dd" de "mmmm" de "yyyy', QrContratosDataC.Value), True);
end;

procedure TFmContratoImp.QrEmpresaCalcFields(DataSet: TDataSet);
begin
  QrEmpresaETE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEmpresaETe1.Value);
  QrEmpresaFAX_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrEmpresaEFax.Value);
  QrEmpresaCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrEmpresaCNPJ.Value);
  //
  QrEmpresaNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrEmpresaERua.Value, QrEmpresaENumero.Value, False);
  QrEmpresaE_LNR.Value := QrEmpresaNOMELOGRAD.Value;
  if Trim(QrEmpresaE_LNR.Value) <> '' then QrEmpresaE_LNR.Value :=
    QrEmpresaE_LNR.Value + ' ';
  QrEmpresaE_LNR.Value := QrEmpresaE_LNR.Value + QrEmpresaERua.Value;
  if Trim(QrEmpresaERua.Value) <> '' then QrEmpresaE_LNR.Value :=
    QrEmpresaE_LNR.Value + ', ' + QrEmpresaNUMERO_TXT.Value;
  if Trim(QrEmpresaECompl.Value) <>  '' then QrEmpresaE_LNR.Value :=
    QrEmpresaE_LNR.Value + ' ' + QrEmpresaECompl.Value;
  if Trim(QrEmpresaEBairro.Value) <>  '' then QrEmpresaE_LNR.Value :=
    QrEmpresaE_LNR.Value + ' - ' + QrEmpresaEBairro.Value;
  //
  QrEmpresaECEP_TXT.Value := Geral.FormataCEP_NT(QrEmpresaECEP.Value);
  //
  if QrEmpresaENatal.Value < 2 then QrEmpresaENATAL_TXT.Value := ''
  else QrEmpresaENATAL_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE2, QrEmpresaENatal.Value);
end;

procedure TFmContratoImp.QrReprAfterOpen(DataSet: TDataSet);
begin
  if QrRepr.RecordCount = 0 then
    Geral.MB_Aviso('N�o h� ' +
      'representante cadastrado para a compradora-contratada (Quadro 5).' +
      sLineBreak + 'Para cadastro, preencha o campo "Empresa" do cadastro pessoal da '+
      'entidade representante da empresa compradora-contratante!');
end;

procedure TFmContratoImp.QrReprCalcFields(DataSet: TDataSet);
begin
  QrReprITEM.Value := QrRepr.RecNo;
  //
  if QrReprSexo.Value = 'M' then QrReprNOMESEXO.Value := 'MASCULINO'
  else if QrReprSexo.Value = 'F' then QrReprNOMESEXO.Value := 'FEMININO'
  else QrReprNOMESEXO.Value := '';
  //
  QrReprETE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrReprPTe1.Value);
  QrReprCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrReprCPF.Value);
  QrReprNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrReprPRua.Value, QrReprPNumero.Value, True);
  QrReprECEP_TXT.Value := Geral.FormataCEP_NT(QrReprPCEP.Value);
  QrReprE_LNR.Value := QrReprNOMEPLOGRAD.Value;
  if Trim(QrReprE_LNR.Value) <> '' then QrReprE_LNR.Value :=
    QrReprE_LNR.Value + ' ';
  QrReprE_LNR.Value := QrReprE_LNR.Value + QrReprPRua.Value;
  if Trim(QrReprPRua.Value) <> '' then QrReprE_LNR.Value :=
    QrReprE_LNR.Value + ', ' + QrReprNUMERO_TXT.Value;
  if Trim(QrReprPCompl.Value) <>  '' then QrReprE_LNR.Value :=
    QrReprE_LNR.Value + ' ' + QrReprPCompl.Value;
  if Trim(QrReprPBairro.Value) <>  '' then QrReprE_LNR.Value :=
    QrReprE_LNR.Value + ' - ' + QrReprPBairro.Value;
  if QrReprPCEP.Value > 0 then QrReprE_LNR.Value :=
    QrReprE_LNR.Value + ' CEP ' + QrReprECEP_TXT.Value;
  //
  if QrReprDataRG.Value < 2 then QrReprDATARG_TXT.Value := ''
  else QrReprDATARG_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE2, QrReprDataRG.Value);
  if QrReprPNatal.Value < 2 then QrReprPNATAL_TXT.Value := ''
  else QrReprPNATAL_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE2, QrReprPNatal.Value);
end;

procedure TFmContratoImp.QrSociosCalcFields(DataSet: TDataSet);
begin
  QrSociosITEM.Value := QrSocios.RecNo;
  //
  if QrSociosSexo.Value = 'M' then QrSociosNOMESEXO.Value := 'MASCULINO'
  else if QrSociosSexo.Value = 'F' then QrSociosNOMESEXO.Value := 'FEMININO'
  else QrSociosNOMESEXO.Value := '';
  //
  QrSociosETE1_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrSociosPTe1.Value);
  QrSociosCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrSociosCPF.Value);
  QrSociosNUMERO_TXT.Value :=
    Geral.FormataNumeroDeRua(QrSociosPRua.Value, QrSociosPNumero.Value, True);
  QrSociosECEP_TXT.Value := Geral.FormataCEP_NT(QrSociosPCEP.Value);
  QrSociosE_LNR.Value := QrSociosNOMEPLOGRAD.Value;
  if Trim(QrSociosE_LNR.Value) <> '' then QrSociosE_LNR.Value :=
    QrSociosE_LNR.Value + ' ';
  QrSociosE_LNR.Value := QrSociosE_LNR.Value + QrSociosPRua.Value;
  if Trim(QrSociosPRua.Value) <> '' then QrSociosE_LNR.Value :=
    QrSociosE_LNR.Value + ', ' + QrSociosNUMERO_TXT.Value;
  if Trim(QrSociosPCompl.Value) <>  '' then QrSociosE_LNR.Value :=
    QrSociosE_LNR.Value + ' ' + QrSociosPCompl.Value;
  if Trim(QrSociosPBairro.Value) <>  '' then QrSociosE_LNR.Value :=
    QrSociosE_LNR.Value + ' - ' + QrSociosPBairro.Value;
  if QrSociosPCEP.Value > 0 then QrSociosE_LNR.Value :=
    QrSociosE_LNR.Value + ' CEP ' + QrSociosECEP_TXT.Value;
  //
  if QrSociosDataRG.Value < 2 then QrSociosDATARG_TXT.Value := ''
  else QrSociosDATARG_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE2, QrSociosDataRG.Value);
  if QrSociosPNatal.Value < 2 then QrSociosPNATAL_TXT.Value := ''
  else QrSociosPNATAL_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE2, QrSociosPNatal.Value);
end;

end.

