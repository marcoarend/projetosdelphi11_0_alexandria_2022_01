object FmContratoImp: TFmContratoImp
  Left = 368
  Top = 194
  Caption = 'Contrato de Fomento Mercantil'
  ClientHeight = 694
  ClientWidth = 975
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelEdita: TPanel
    Left = 0
    Top = 59
    Width = 975
    Height = 635
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 575
      Width = 973
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 10
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 839
        Top = 1
        Width = 133
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 110
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 973
      Height = 512
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 0
      object Label7: TLabel
        Left = 5
        Top = 5
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
      end
      object Label19: TLabel
        Left = 5
        Top = 57
        Width = 44
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cliente:'
      end
      object Label5: TLabel
        Left = 498
        Top = 57
        Width = 102
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data do contrato:'
      end
      object Label20: TLabel
        Left = 643
        Top = 57
        Width = 96
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Limite de fian'#231'a:'
        FocusControl = EdLimite
      end
      object Label21: TLabel
        Left = 749
        Top = 57
        Width = 71
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'N'#186' Contrato:'
        FocusControl = EdContrato
      end
      object Label1: TLabel
        Left = 5
        Top = 110
        Width = 57
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '1'#186' Fiador:'
      end
      object Label2: TLabel
        Left = 5
        Top = 162
        Width = 57
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '2'#186' Fiador:'
      end
      object Label3: TLabel
        Left = 498
        Top = 110
        Width = 129
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Conjuge do 1'#186' Fiador:'
        Visible = False
      end
      object Label4: TLabel
        Left = 498
        Top = 162
        Width = 129
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Conjuge do 2'#186' Fiador:'
        Visible = False
      end
      object Label6: TLabel
        Left = 5
        Top = 326
        Width = 122
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Nome testemunha 1:'
        FocusControl = EdNome1
      end
      object Label8: TLabel
        Left = 5
        Top = 382
        Width = 122
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Nome testemunha 2:'
        FocusControl = EdNome2
      end
      object Label9: TLabel
        Left = 469
        Top = 326
        Width = 111
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'CPF testemunha 1:'
        FocusControl = EdCPF1
      end
      object Label10: TLabel
        Left = 469
        Top = 382
        Width = 111
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'CPF testemunha 2:'
        FocusControl = EdCPF2
      end
      object Label11: TLabel
        Left = 5
        Top = 438
        Width = 119
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Modelo de contrato:'
      end
      object Label12: TLabel
        Left = 5
        Top = 218
        Width = 57
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '3'#186' Fiador:'
      end
      object Label13: TLabel
        Left = 5
        Top = 271
        Width = 57
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '4'#186' Fiador:'
      end
      object SpeedButton5: TSpeedButton
        Left = 465
        Top = 130
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        OnClick = SpeedButton5Click
      end
      object SpeedButton1: TSpeedButton
        Left = 465
        Top = 183
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        OnClick = SpeedButton1Click
      end
      object SpeedButton2: TSpeedButton
        Left = 465
        Top = 239
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        OnClick = SpeedButton2Click
      end
      object SpeedButton3: TSpeedButton
        Left = 465
        Top = 292
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        OnClick = SpeedButton3Click
      end
      object SpeedButton4: TSpeedButton
        Left = 585
        Top = 459
        Width = 25
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        OnClick = SpeedButton4Click
      end
      object SpeedButton6: TSpeedButton
        Left = 465
        Top = 77
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        OnClick = SpeedButton6Click
      end
      object EdCodigo: TdmkEdit
        Left = 5
        Top = 25
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 74
        Top = 78
        Width = 388
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMECLIENTE'
        ListSource = DsClientes
        TabOrder = 2
        dmkEditCB = EdCliente
        QryCampo = 'Cliente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCliente: TdmkEditCB
        Left = 5
        Top = 78
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cliente'
        UpdCampo = 'Cliente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object TPDataC: TdmkEditDateTimePicker
        Left = 498
        Top = 78
        Width = 138
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 39328.122587314800000000
        Time = 39328.122587314800000000
        TabOrder = 3
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        QryCampo = 'DataC'
        UpdCampo = 'DataC'
        UpdType = utYes
      end
      object EdLimite: TdmkEdit
        Left = 643
        Top = 78
        Width = 99
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'Limite'
        UpdCampo = 'Limite'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdContrato: TdmkEdit
        Left = 749
        Top = 78
        Width = 74
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Contrato'
        UpdCampo = 'Contrato'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object Ed11: TdmkEditCB
        Left = 5
        Top = 130
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fiador11'
        UpdCampo = 'Fiador11'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CB11
        IgnoraDBLookupComboBox = False
      end
      object CB11: TdmkDBLookupComboBox
        Left = 74
        Top = 130
        Width = 388
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEFIADOR'
        ListSource = Ds11
        TabOrder = 7
        dmkEditCB = Ed11
        QryCampo = 'Fiador11'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CB21: TdmkDBLookupComboBox
        Left = 74
        Top = 183
        Width = 388
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEFIADOR'
        ListSource = Ds21
        TabOrder = 11
        dmkEditCB = Ed21
        QryCampo = 'Fiador21'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object Ed21: TdmkEditCB
        Left = 5
        Top = 183
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fiador21'
        UpdCampo = 'Fiador21'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CB21
        IgnoraDBLookupComboBox = False
      end
      object Ed12: TdmkEditCB
        Left = 498
        Top = 130
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 8
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fiador12'
        UpdCampo = 'Fiador12'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CB12
        IgnoraDBLookupComboBox = False
      end
      object CB12: TdmkDBLookupComboBox
        Left = 567
        Top = 130
        Width = 388
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEFIADOR'
        ListSource = Ds12
        TabOrder = 9
        Visible = False
        dmkEditCB = Ed12
        QryCampo = 'Fiador12'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object Ed22: TdmkEditCB
        Left = 498
        Top = 183
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 12
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fiador22'
        UpdCampo = 'Fiador22'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CB22
        IgnoraDBLookupComboBox = False
      end
      object CB22: TdmkDBLookupComboBox
        Left = 567
        Top = 183
        Width = 388
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEFIADOR'
        ListSource = Ds22
        TabOrder = 13
        Visible = False
        dmkEditCB = Ed22
        QryCampo = 'Fiador22'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdNome1: TdmkEdit
        Left = 5
        Top = 347
        Width = 457
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 18
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Testem1Nome'
        UpdCampo = 'Testem1Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNome2: TdmkEdit
        Left = 5
        Top = 402
        Width = 457
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 20
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Testem2Nome'
        UpdCampo = 'Testem2Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdNome2Change
      end
      object EdCPF1: TdmkEdit
        Left = 469
        Top = 347
        Width = 141
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 19
        FormatType = dmktfString
        MskType = fmtCPFJ
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Testem1CPF'
        UpdCampo = 'Testem1CPF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCPF2: TdmkEdit
        Left = 469
        Top = 402
        Width = 141
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 21
        FormatType = dmktfString
        MskType = fmtCPFJ
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Testem2CPF'
        UpdCampo = 'Testem2CPF'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdTexto: TdmkEditCB
        Left = 5
        Top = 459
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 22
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Texto'
        UpdCampo = 'Texto'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTexto
        IgnoraDBLookupComboBox = False
      end
      object CBTexto: TdmkDBLookupComboBox
        Left = 74
        Top = 459
        Width = 507
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCartaG
        TabOrder = 23
        dmkEditCB = EdTexto
        QryCampo = 'Texto'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object Ed13: TdmkEditCB
        Left = 5
        Top = 239
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fiador13'
        UpdCampo = 'Fiador13'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CB13
        IgnoraDBLookupComboBox = False
      end
      object CB13: TdmkDBLookupComboBox
        Left = 74
        Top = 239
        Width = 388
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEFIADOR'
        ListSource = Ds13
        TabOrder = 15
        dmkEditCB = Ed13
        QryCampo = 'Fiador13'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object Ed14: TdmkEditCB
        Left = 5
        Top = 292
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Fiador14'
        UpdCampo = 'Fiador14'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CB14
        IgnoraDBLookupComboBox = False
      end
      object CB14: TdmkDBLookupComboBox
        Left = 74
        Top = 292
        Width = 388
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NOMEFIADOR'
        ListSource = Ds14
        TabOrder = 17
        dmkEditCB = Ed14
        QryCampo = 'Fiador14'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 975
    Height = 635
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 575
      Width = 973
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object Panel3: TPanel
        Left = 395
        Top = 1
        Width = 577
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 443
          Top = 0
          Width = 134
          Height = 57
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 20
        Top = 4
        Width = 110
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtIncluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 132
        Top = 4
        Width = 110
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtAlteraClick
      end
      object BtExclui: TBitBtn
        Tag = 12
        Left = 244
        Top = 4
        Width = 110
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtExcluiClick
      end
      object BtEmpresa: TBitBtn
        Tag = 10049
        Left = 356
        Top = 4
        Width = 110
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'E&mpresa'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtEmpresaClick
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 973
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 1
      object RGOrdem2: TRadioGroup
        Left = 331
        Top = 1
        Width = 222
        Height = 62
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Caption = ' Ordem 2: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'Crescente'
          'Decrescente')
        TabOrder = 0
        OnClick = RGOrdem2Click
      end
      object RGOrdem1: TRadioGroup
        Left = 1
        Top = 1
        Width = 330
        Height = 62
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Caption = ' Ordem 1: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Cliente'
          'Contrato'
          'Data C.'
          'Limite')
        TabOrder = 1
        OnClick = RGOrdem1Click
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 65
      Width = 973
      Height = 125
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      DataSource = DsContratos
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -15
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMECLIENTE'
          Title.Caption = 'Cliente'
          Width = 144
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Contrato'
          Width = 44
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataC'
          Title.Caption = 'Data C.'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Limite'
          Width = 80
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEF11'
          Title.Caption = '1'#186' Fiador'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEF12'
          Title.Caption = 'Conjuge 1'#186' Fiador'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEF21'
          Title.Caption = '2'#186' Fiador'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEF22'
          Title.Caption = 'Conjuge 2'#186' Fiador'
          Width = 100
          Visible = True
        end>
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 975
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Contrato de Fomento Mercantil'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 873
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      OnTextChange = LaTipoTextChange
    end
    object Image1: TImage
      Left = 62
      Top = 1
      Width = 811
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
      ExplicitTop = -4
    end
    object PnPesq: TPanel
      Left = 1
      Top = 1
      Width = 61
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 2
        Width = 49
        Height = 50
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
  end
  object DsContratos: TDataSource
    DataSet = QrContratos
    Left = 272
    Top = 12
  end
  object QrContratos: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrContratosBeforeOpen
    AfterOpen = QrContratosAfterOpen
    OnCalcFields = QrContratosCalcFields
    SQL.Strings = (
      'SELECT '
      
        'CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOME' +
        'CLIENTE,'
      
        'CASE WHEN f11.Tipo=0 THEN f11.RazaoSocial ELSE f11.Nome END NOME' +
        'F11,'
      
        'CASE WHEN f12.Tipo=0 THEN f12.RazaoSocial ELSE f12.Nome END NOME' +
        'F12,'
      
        'CASE WHEN f13.Tipo=0 THEN f13.RazaoSocial ELSE f13.Nome END NOME' +
        'F13,'
      
        'CASE WHEN f14.Tipo=0 THEN f14.RazaoSocial ELSE f14.Nome END NOME' +
        'F14,'
      
        'CASE WHEN f21.Tipo=0 THEN f21.RazaoSocial ELSE f21.Nome END NOME' +
        'F21,'
      
        'CASE WHEN f22.Tipo=0 THEN f22.RazaoSocial ELSE f22.Nome END NOME' +
        'F22,'
      'con.*'
      'FROM contratos con'
      'LEFT JOIN entidades cli ON cli.Codigo=con.Cliente'
      'LEFT JOIN entidades f11 ON f11.Codigo=con.Fiador11'
      'LEFT JOIN entidades f12 ON f12.Codigo=con.Fiador12'
      'LEFT JOIN entidades f13 ON f13.Codigo=con.Fiador13'
      'LEFT JOIN entidades f14 ON f14.Codigo=con.Fiador14'
      'LEFT JOIN entidades f21 ON f21.Codigo=con.Fiador21'
      'LEFT JOIN entidades f22 ON f22.Codigo=con.Fiador22')
    Left = 244
    Top = 12
    object QrContratosTestem1CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Testem1CPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrContratosTestem2CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Testem2CPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrContratosDATAC_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATAC_TXT'
      Size = 255
      Calculated = True
    end
    object QrContratosNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrContratosNOMEF11: TWideStringField
      FieldName = 'NOMEF11'
      Size = 100
    end
    object QrContratosNOMEF12: TWideStringField
      FieldName = 'NOMEF12'
      Size = 100
    end
    object QrContratosNOMEF21: TWideStringField
      FieldName = 'NOMEF21'
      Size = 100
    end
    object QrContratosNOMEF22: TWideStringField
      FieldName = 'NOMEF22'
      Size = 100
    end
    object QrContratosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'contratos.Codigo'
    end
    object QrContratosCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'contratos.Cliente'
    end
    object QrContratosContrato: TIntegerField
      FieldName = 'Contrato'
      Origin = 'contratos.Contrato'
    end
    object QrContratosDataC: TDateField
      FieldName = 'DataC'
      Origin = 'contratos.DataC'
    end
    object QrContratosLimite: TFloatField
      FieldName = 'Limite'
      Origin = 'contratos.Limite'
    end
    object QrContratosFiador11: TIntegerField
      FieldName = 'Fiador11'
      Origin = 'contratos.Fiador11'
    end
    object QrContratosFiador12: TIntegerField
      FieldName = 'Fiador12'
      Origin = 'contratos.Fiador12'
    end
    object QrContratosFiador21: TIntegerField
      FieldName = 'Fiador21'
      Origin = 'contratos.Fiador21'
    end
    object QrContratosFiador22: TIntegerField
      FieldName = 'Fiador22'
      Origin = 'contratos.Fiador22'
    end
    object QrContratosTestem1Nome: TWideStringField
      FieldName = 'Testem1Nome'
      Origin = 'contratos.Testem1Nome'
      Size = 100
    end
    object QrContratosTestem1CPF: TWideStringField
      FieldName = 'Testem1CPF'
      Origin = 'contratos.Testem1CPF'
      Size = 15
    end
    object QrContratosTestem2Nome: TWideStringField
      FieldName = 'Testem2Nome'
      Origin = 'contratos.Testem2Nome'
      Size = 100
    end
    object QrContratosTestem2CPF: TWideStringField
      FieldName = 'Testem2CPF'
      Origin = 'contratos.Testem2CPF'
      Size = 15
    end
    object QrContratosTexto: TIntegerField
      FieldName = 'Texto'
      Origin = 'contratos.Texto'
    end
    object QrContratosLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'contratos.Lk'
    end
    object QrContratosDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'contratos.DataCad'
    end
    object QrContratosDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'contratos.DataAlt'
    end
    object QrContratosUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'contratos.UserCad'
    end
    object QrContratosUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'contratos.UserAlt'
    end
    object QrContratosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'contratos.AlterWeb'
    end
    object QrContratosAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'contratos.Ativo'
    end
    object QrContratosNOMEF13: TWideStringField
      FieldName = 'NOMEF13'
      Size = 100
    end
    object QrContratosNOMEF14: TWideStringField
      FieldName = 'NOMEF14'
      Size = 100
    end
    object QrContratosFiador13: TIntegerField
      FieldName = 'Fiador13'
      Origin = 'contratos.Fiador13'
    end
    object QrContratosFiador14: TIntegerField
      FieldName = 'Fiador14'
      Origin = 'contratos.Fiador14'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtExclui
    CanUpd01 = BtAltera
    CanDel01 = BtInclui
    Left = 300
    Top = 12
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo'
      'FROM entidades'
      'WHERE Cliente1='#39'V'#39
      'ORDER BY NOMECLIENTE')
    Left = 624
    Top = 196
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 652
    Top = 196
  end
  object Qr21: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEFIADOR, Codigo'
      'FROM entidades'
      'WHERE Fornece4='#39'V'#39
      'ORDER BY NOMEFIADOR')
    Left = 624
    Top = 224
    object Qr21NOMEFIADOR: TWideStringField
      FieldName = 'NOMEFIADOR'
      Size = 100
    end
    object Qr21Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object Ds21: TDataSource
    DataSet = Qr21
    Left = 652
    Top = 224
  end
  object QrCartaG: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cartag'
      'ORDER BY Nome')
    Left = 624
    Top = 392
    object QrCartaGCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartaGNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCartaGLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCartaGDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCartaGDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCartaGUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCartaGUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsCartaG: TDataSource
    DataSet = QrCartaG
    Left = 652
    Top = 392
  end
  object Ds11: TDataSource
    DataSet = Qr11
    Left = 652
    Top = 252
  end
  object Ds12: TDataSource
    DataSet = Qr12
    Left = 652
    Top = 280
  end
  object Ds22: TDataSource
    DataSet = Qr22
    Left = 652
    Top = 308
  end
  object frxDsEmpresa: TfrxDBDataset
    UserName = 'frxDsEmpresa'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'RazaoSocial=RazaoSocial'
      'Cadastro=Cadastro'
      'ENatal=ENatal'
      'CNPJ=CNPJ'
      'IE=IE'
      'Simples=Simples'
      'ELograd=ELograd'
      'ERua=ERua'
      'ECompl=ECompl'
      'EBairro=EBairro'
      'ECidade=ECidade'
      'ECEP=ECEP'
      'ETe1=ETe1'
      'EFax=EFax'
      'ETE1_TXT=ETE1_TXT'
      'FAX_TXT=FAX_TXT'
      'FormaSociet=FormaSociet'
      'CNPJ_TXT=CNPJ_TXT'
      'E_LNR=E_LNR'
      'NOMEUF=NOMEUF'
      'NUMERO_TXT=NUMERO_TXT'
      'NOMELOGRAD=NOMELOGRAD'
      'ECEP_TXT=ECEP_TXT'
      'Atividade=Atividade'
      'ENATAL_TXT=ENATAL_TXT'
      'ENumero=ENumero'
      'NIRE=NIRE')
    DataSet = QrEmpresa
    BCDToCurrency = False
    Left = 184
    Top = 152
  end
  object DsEmpresa: TDataSource
    DataSet = QrEmpresa
    Left = 156
    Top = 152
  end
  object QrEmpresa: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmpresaCalcFields
    SQL.Strings = (
      
        'SELECT en.Codigo, en.RazaoSocial, en.Cadastro, en.ENatal, en.CNP' +
        'J, '
      
        'en.IE, en.NIRE, en.Simples, en.ELograd, en.ERua, en.ENumero, en.' +
        'ECompl, '
      
        'en.EBairro, en.ECidade, en.ECEP, en.ETe1, en.EFax, en.FormaSocie' +
        't,'
      'en.Atividade, uf.Nome NOMEUF, ll.Nome NOMELOGRAD'
      'FROM entidades en'
      'LEFT JOIN ufs uf ON uf.Codigo=en.EUF'
      'LEFT JOIN listalograd ll ON ll.Codigo=en.ELograd'
      'WHERE en.Codigo=:P0')
    Left = 128
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmpresaCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrEmpresaRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Origin = 'entidades.RazaoSocial'
      Size = 100
    end
    object QrEmpresaCadastro: TDateField
      FieldName = 'Cadastro'
      Origin = 'entidades.Cadastro'
    end
    object QrEmpresaENatal: TDateField
      FieldName = 'ENatal'
      Origin = 'entidades.ENatal'
    end
    object QrEmpresaCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'entidades.CNPJ'
      Size = 18
    end
    object QrEmpresaIE: TWideStringField
      FieldName = 'IE'
      Origin = 'entidades.IE'
    end
    object QrEmpresaSimples: TSmallintField
      FieldName = 'Simples'
      Origin = 'entidades.Simples'
    end
    object QrEmpresaELograd: TSmallintField
      FieldName = 'ELograd'
      Origin = 'entidades.ELograd'
    end
    object QrEmpresaERua: TWideStringField
      FieldName = 'ERua'
      Origin = 'entidades.ERua'
      Size = 30
    end
    object QrEmpresaECompl: TWideStringField
      FieldName = 'ECompl'
      Origin = 'entidades.ECompl'
      Size = 30
    end
    object QrEmpresaEBairro: TWideStringField
      FieldName = 'EBairro'
      Origin = 'entidades.EBairro'
      Size = 30
    end
    object QrEmpresaECidade: TWideStringField
      FieldName = 'ECidade'
      Origin = 'entidades.ECidade'
      Size = 25
    end
    object QrEmpresaECEP: TIntegerField
      FieldName = 'ECEP'
      Origin = 'entidades.ECEP'
    end
    object QrEmpresaETe1: TWideStringField
      FieldName = 'ETe1'
      Origin = 'entidades.ETe1'
    end
    object QrEmpresaEFax: TWideStringField
      FieldName = 'EFax'
      Origin = 'entidades.EFax'
    end
    object QrEmpresaETE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEmpresaFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEmpresaFormaSociet: TWideStringField
      FieldName = 'FormaSociet'
      Origin = 'entidades.FormaSociet'
    end
    object QrEmpresaCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEmpresaE_LNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LNR'
      Size = 255
      Calculated = True
    end
    object QrEmpresaNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrEmpresaNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrEmpresaNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Origin = 'listalograd.Nome'
      Size = 10
    end
    object QrEmpresaECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrEmpresaAtividade: TWideStringField
      FieldName = 'Atividade'
      Origin = 'entidades.Atividade'
      Size = 50
    end
    object QrEmpresaENATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENATAL_TXT'
      Calculated = True
    end
    object QrEmpresaENumero: TIntegerField
      FieldName = 'ENumero'
      Origin = 'entidades.ENumero'
    end
    object QrEmpresaNIRE: TWideStringField
      FieldName = 'NIRE'
      Origin = 'entidades.NIRE'
      Size = 15
    end
  end
  object frxDsSocios: TfrxDBDataset
    UserName = 'frxDsSocios'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMESOCIO=NOMESOCIO'
      'Codigo=Codigo'
      'Sexo=Sexo'
      'Pai=Pai'
      'Mae=Mae'
      'PNatal=PNatal'
      'CidadeNatal=CidadeNatal'
      'Nacionalid=Nacionalid'
      'ConjugeNome=ConjugeNome'
      'CPF=CPF'
      'RG=RG'
      'SSP=SSP'
      'DataRG=DataRG'
      'PRua=PRua'
      'PCompl=PCompl'
      'PBairro=PBairro'
      'PCidade=PCidade'
      'PCEP=PCEP'
      'PTe1=PTe1'
      'Profissao=Profissao'
      'Cargo=Cargo'
      'NOMEPUF=NOMEPUF'
      'NOMEPLOGRAD=NOMEPLOGRAD'
      'NOMEECIVIL=NOMEECIVIL'
      'PNATAL_TXT=PNATAL_TXT'
      'DATARG_TXT=DATARG_TXT'
      'ETE1_TXT=ETE1_TXT'
      'CNPJ_TXT=CNPJ_TXT'
      'E_LNR=E_LNR'
      'NUMERO_TXT=NUMERO_TXT'
      'ECEP_TXT=ECEP_TXT'
      'ITEM=ITEM'
      'NOMESEXO=NOMESEXO'
      'RazaoSocial=RazaoSocial'
      'Fantasia=Fantasia'
      'Respons1=Respons1'
      'Respons2=Respons2'
      'CNPJ=CNPJ'
      'IE=IE'
      'FormaSociet=FormaSociet'
      'Simples=Simples'
      'IEST=IEST'
      'Atividade=Atividade'
      'Nome=Nome'
      'Apelido=Apelido'
      'CPF_Pai=CPF_Pai'
      'EstCivil=EstCivil'
      'UFNatal=UFNatal'
      'ELograd=ELograd'
      'ERua=ERua'
      'ECompl=ECompl'
      'EBairro=EBairro'
      'ECidade=ECidade'
      'EUF=EUF'
      'ECEP=ECEP'
      'EPais=EPais'
      'ETe1=ETe1'
      'Ete2=Ete2'
      'Ete3=Ete3'
      'ECel=ECel'
      'EFax=EFax'
      'EEmail=EEmail'
      'EContato=EContato'
      'ENatal=ENatal'
      'PLograd=PLograd'
      'PUF=PUF'
      'PPais=PPais'
      'Pte2=Pte2'
      'Pte3=Pte3'
      'PCel=PCel'
      'PFax=PFax'
      'PEmail=PEmail'
      'PContato=PContato'
      'Responsavel=Responsavel'
      'Recibo=Recibo'
      'DiaRecibo=DiaRecibo'
      'AjudaEmpV=AjudaEmpV'
      'AjudaEmpP=AjudaEmpP'
      'Cliente1=Cliente1'
      'Cliente2=Cliente2'
      'Fornece1=Fornece1'
      'Fornece2=Fornece2'
      'Fornece3=Fornece3'
      'Fornece4=Fornece4'
      'Fornece5=Fornece5'
      'Fornece6=Fornece6'
      'Terceiro=Terceiro'
      'Cadastro=Cadastro'
      'Informacoes=Informacoes'
      'Logo=Logo'
      'Veiculo=Veiculo'
      'Mensal=Mensal'
      'Observacoes=Observacoes'
      'Tipo=Tipo'
      'CLograd=CLograd'
      'CRua=CRua'
      'CCompl=CCompl'
      'CBairro=CBairro'
      'CCidade=CCidade'
      'CUF=CUF'
      'CCEP=CCEP'
      'CPais=CPais'
      'CTel=CTel'
      'CCel=CCel'
      'CFax=CFax'
      'CContato=CContato'
      'LLograd=LLograd'
      'LRua=LRua'
      'LCompl=LCompl'
      'LBairro=LBairro'
      'LCidade=LCidade'
      'LUF=LUF'
      'LCEP=LCEP'
      'LPais=LPais'
      'LTel=LTel'
      'LCel=LCel'
      'LFax=LFax'
      'LContato=LContato'
      'Comissao=Comissao'
      'Situacao=Situacao'
      'Nivel=Nivel'
      'Grupo=Grupo'
      'Account=Account'
      'Logo2=Logo2'
      'ConjugeNatal=ConjugeNatal'
      'Nome1=Nome1'
      'Natal1=Natal1'
      'Nome2=Nome2'
      'Natal2=Natal2'
      'Nome3=Nome3'
      'Natal3=Natal3'
      'Nome4=Nome4'
      'Natal4=Natal4'
      'CreditosI=CreditosI'
      'CreditosL=CreditosL'
      'CreditosF2=CreditosF2'
      'CreditosD=CreditosD'
      'CreditosU=CreditosU'
      'CreditosV=CreditosV'
      'Motivo=Motivo'
      'QuantI1=QuantI1'
      'QuantI2=QuantI2'
      'QuantI3=QuantI3'
      'QuantI4=QuantI4'
      'QuantN1=QuantN1'
      'QuantN2=QuantN2'
      'Agenda=Agenda'
      'SenhaQuer=SenhaQuer'
      'Senha1=Senha1'
      'LimiCred=LimiCred'
      'Desco=Desco'
      'CasasApliDesco=CasasApliDesco'
      'TempD=TempD'
      'Banco=Banco'
      'Agencia=Agencia'
      'ContaCorrente=ContaCorrente'
      'FatorCompra=FatorCompra'
      'AdValorem=AdValorem'
      'DMaisC=DMaisC'
      'DMaisD=DMaisD'
      'Empresa=Empresa'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'CPF_Conjuge=CPF_Conjuge'
      'Empresa_1=Empresa_1'
      'Socio=Socio'
      'Ordem=Ordem'
      'ENumero=ENumero'
      'PNumero=PNumero'
      'NIRE=NIRE')
    DataSet = QrSocios
    BCDToCurrency = False
    Left = 184
    Top = 180
  end
  object QrSocios: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSociosCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome '
      'END NOMESOCIO, en.*, so.*,'
      'uf.Nome NOMEPUF, ll.Nome NOMEPLOGRAD, le.Nome NOMEECIVIL'
      'FROM socios so'
      'LEFT JOIN entidades   en ON en.Codigo=so.Socio'
      'LEFT JOIN ufs uf         ON uf.Codigo=en.PUF'
      'LEFT JOIN listalograd ll ON ll.Codigo=en.PLograd'
      'LEFT JOIN listaecivil le ON le.Codigo=en.EstCivil'
      'WHERE so.Empresa=:P0'
      'ORDER BY so.Ordem, NOMESOCIO')
    Left = 128
    Top = 180
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSociosNOMESOCIO: TWideStringField
      FieldName = 'NOMESOCIO'
      Size = 100
    end
    object QrSociosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSociosSexo: TWideStringField
      FieldName = 'Sexo'
      Size = 1
    end
    object QrSociosPai: TWideStringField
      FieldName = 'Pai'
      Size = 60
    end
    object QrSociosMae: TWideStringField
      FieldName = 'Mae'
      Size = 60
    end
    object QrSociosPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrSociosCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrSociosNacionalid: TWideStringField
      FieldName = 'Nacionalid'
      Size = 15
    end
    object QrSociosConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrSociosCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrSociosRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrSociosSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrSociosDataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrSociosPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrSociosPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrSociosPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrSociosPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrSociosPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrSociosPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrSociosProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrSociosCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrSociosNOMEPUF: TWideStringField
      FieldName = 'NOMEPUF'
      Required = True
      Size = 2
    end
    object QrSociosNOMEPLOGRAD: TWideStringField
      FieldName = 'NOMEPLOGRAD'
      Size = 10
    end
    object QrSociosNOMEECIVIL: TWideStringField
      FieldName = 'NOMEECIVIL'
      Required = True
      Size = 10
    end
    object QrSociosPNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PNATAL_TXT'
      Calculated = True
    end
    object QrSociosDATARG_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATARG_TXT'
      Calculated = True
    end
    object QrSociosETE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrSociosCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrSociosE_LNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LNR'
      Size = 255
      Calculated = True
    end
    object QrSociosNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrSociosECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrSociosITEM: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'ITEM'
      Calculated = True
    end
    object QrSociosNOMESEXO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESEXO'
      Calculated = True
    end
    object QrSociosRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Size = 100
    end
    object QrSociosFantasia: TWideStringField
      FieldName = 'Fantasia'
      Size = 60
    end
    object QrSociosRespons1: TWideStringField
      FieldName = 'Respons1'
      Size = 60
    end
    object QrSociosRespons2: TWideStringField
      FieldName = 'Respons2'
      Size = 60
    end
    object QrSociosCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrSociosIE: TWideStringField
      FieldName = 'IE'
    end
    object QrSociosFormaSociet: TWideStringField
      FieldName = 'FormaSociet'
    end
    object QrSociosSimples: TSmallintField
      FieldName = 'Simples'
    end
    object QrSociosIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrSociosAtividade: TWideStringField
      FieldName = 'Atividade'
      Size = 50
    end
    object QrSociosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrSociosApelido: TWideStringField
      FieldName = 'Apelido'
      Size = 60
    end
    object QrSociosCPF_Pai: TWideStringField
      FieldName = 'CPF_Pai'
      Size = 18
    end
    object QrSociosEstCivil: TSmallintField
      FieldName = 'EstCivil'
    end
    object QrSociosUFNatal: TSmallintField
      FieldName = 'UFNatal'
    end
    object QrSociosELograd: TSmallintField
      FieldName = 'ELograd'
    end
    object QrSociosERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrSociosECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrSociosEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrSociosECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 25
    end
    object QrSociosEUF: TSmallintField
      FieldName = 'EUF'
    end
    object QrSociosECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrSociosEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrSociosETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrSociosEte2: TWideStringField
      FieldName = 'Ete2'
    end
    object QrSociosEte3: TWideStringField
      FieldName = 'Ete3'
    end
    object QrSociosECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrSociosEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrSociosEEmail: TWideStringField
      FieldName = 'EEmail'
      Size = 100
    end
    object QrSociosEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrSociosENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrSociosPLograd: TSmallintField
      FieldName = 'PLograd'
    end
    object QrSociosPUF: TSmallintField
      FieldName = 'PUF'
    end
    object QrSociosPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrSociosPte2: TWideStringField
      FieldName = 'Pte2'
    end
    object QrSociosPte3: TWideStringField
      FieldName = 'Pte3'
    end
    object QrSociosPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrSociosPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrSociosPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrSociosPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrSociosResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrSociosRecibo: TSmallintField
      FieldName = 'Recibo'
    end
    object QrSociosDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
    end
    object QrSociosAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
    end
    object QrSociosAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
    end
    object QrSociosCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrSociosCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrSociosFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrSociosFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrSociosFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrSociosFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrSociosFornece5: TWideStringField
      FieldName = 'Fornece5'
      Size = 1
    end
    object QrSociosFornece6: TWideStringField
      FieldName = 'Fornece6'
      Size = 1
    end
    object QrSociosTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrSociosCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrSociosInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrSociosLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrSociosVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrSociosMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrSociosObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrSociosTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrSociosCLograd: TSmallintField
      FieldName = 'CLograd'
    end
    object QrSociosCRua: TWideStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrSociosCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrSociosCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrSociosCCidade: TWideStringField
      FieldName = 'CCidade'
      Size = 25
    end
    object QrSociosCUF: TSmallintField
      FieldName = 'CUF'
    end
    object QrSociosCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrSociosCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrSociosCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrSociosCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrSociosCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrSociosCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrSociosLLograd: TSmallintField
      FieldName = 'LLograd'
    end
    object QrSociosLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrSociosLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrSociosLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrSociosLCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 25
    end
    object QrSociosLUF: TSmallintField
      FieldName = 'LUF'
    end
    object QrSociosLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrSociosLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrSociosLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrSociosLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrSociosLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrSociosLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrSociosComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrSociosSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrSociosNivel: TWideStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrSociosGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrSociosAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrSociosLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrSociosConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
    end
    object QrSociosNome1: TWideStringField
      FieldName = 'Nome1'
      Size = 30
    end
    object QrSociosNatal1: TDateField
      FieldName = 'Natal1'
    end
    object QrSociosNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 30
    end
    object QrSociosNatal2: TDateField
      FieldName = 'Natal2'
    end
    object QrSociosNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 30
    end
    object QrSociosNatal3: TDateField
      FieldName = 'Natal3'
    end
    object QrSociosNome4: TWideStringField
      FieldName = 'Nome4'
      Size = 30
    end
    object QrSociosNatal4: TDateField
      FieldName = 'Natal4'
    end
    object QrSociosCreditosI: TIntegerField
      FieldName = 'CreditosI'
    end
    object QrSociosCreditosL: TIntegerField
      FieldName = 'CreditosL'
    end
    object QrSociosCreditosF2: TFloatField
      FieldName = 'CreditosF2'
    end
    object QrSociosCreditosD: TDateField
      FieldName = 'CreditosD'
    end
    object QrSociosCreditosU: TDateField
      FieldName = 'CreditosU'
    end
    object QrSociosCreditosV: TDateField
      FieldName = 'CreditosV'
    end
    object QrSociosMotivo: TIntegerField
      FieldName = 'Motivo'
    end
    object QrSociosQuantI1: TIntegerField
      FieldName = 'QuantI1'
    end
    object QrSociosQuantI2: TIntegerField
      FieldName = 'QuantI2'
    end
    object QrSociosQuantI3: TIntegerField
      FieldName = 'QuantI3'
    end
    object QrSociosQuantI4: TIntegerField
      FieldName = 'QuantI4'
    end
    object QrSociosQuantN1: TFloatField
      FieldName = 'QuantN1'
    end
    object QrSociosQuantN2: TFloatField
      FieldName = 'QuantN2'
    end
    object QrSociosAgenda: TWideStringField
      FieldName = 'Agenda'
      Size = 1
    end
    object QrSociosSenhaQuer: TWideStringField
      FieldName = 'SenhaQuer'
      Size = 1
    end
    object QrSociosSenha1: TWideStringField
      FieldName = 'Senha1'
      Size = 6
    end
    object QrSociosLimiCred: TFloatField
      FieldName = 'LimiCred'
    end
    object QrSociosDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrSociosCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
    end
    object QrSociosTempD: TFloatField
      FieldName = 'TempD'
    end
    object QrSociosBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrSociosAgencia: TWideStringField
      FieldName = 'Agencia'
      Size = 11
    end
    object QrSociosContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrSociosFatorCompra: TFloatField
      FieldName = 'FatorCompra'
    end
    object QrSociosAdValorem: TFloatField
      FieldName = 'AdValorem'
    end
    object QrSociosDMaisC: TIntegerField
      FieldName = 'DMaisC'
    end
    object QrSociosDMaisD: TIntegerField
      FieldName = 'DMaisD'
    end
    object QrSociosEmpresa: TIntegerField
      FieldName = 'Empresa'
    end
    object QrSociosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSociosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSociosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSociosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSociosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSociosCPF_Conjuge: TWideStringField
      FieldName = 'CPF_Conjuge'
      Size = 18
    end
    object QrSociosEmpresa_1: TIntegerField
      FieldName = 'Empresa_1'
      Required = True
    end
    object QrSociosSocio: TIntegerField
      FieldName = 'Socio'
      Required = True
    end
    object QrSociosOrdem: TIntegerField
      FieldName = 'Ordem'
      Required = True
    end
    object QrSociosENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrSociosPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrSociosNIRE: TWideStringField
      FieldName = 'NIRE'
      Size = 15
    end
  end
  object DsSocios: TDataSource
    DataSet = QrSocios
    Left = 156
    Top = 180
  end
  object QrFiadores: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFiadoresCalcFields
    SQL.Strings = (
      
        'SELECT fi.Tipo TIPOFIADOR, en.Codigo, en.Nome, en.Sexo, en.Pai, ' +
        'en.Mae, en.PNatal,'
      
        'en.CidadeNatal, en.Nacionalid, en.ConjugeNome, en.CPF_Conjuge, e' +
        'n.CPF, en.RG, '
      'en.SSP, en.DataRG, en.PRua, en.PNumero, en.PCompl, en.PBairro, '
      'en.PCidade, en.PCEP, en.PTe1, en.Profissao, en.Cargo, '
      'uf.Nome NOMEPUF, ll.Nome NOMEPLOGRAD, le.Nome NOMEECIVIL'
      'FROM fiadores fi'
      'LEFT JOIN creditor.entidades   en ON fi.Codigo=en.Codigo'
      'LEFT JOIN creditor.ufs uf         ON uf.Codigo=en.PUF'
      'LEFT JOIN creditor.listalograd ll ON ll.Codigo=en.PLograd'
      'LEFT JOIN creditor.listaecivil le ON le.Codigo=en.EstCivil'
      'ORDER BY fi.Tipo')
    Left = 128
    Top = 208
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFiadoresSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrFiadoresNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrFiadoresITEM: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'ITEM'
      Calculated = True
    end
    object QrFiadoresPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrFiadoresMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrFiadoresPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrFiadoresNOMEPUF: TWideStringField
      FieldName = 'NOMEPUF'
      Size = 2
    end
    object QrFiadoresNOMEPLOGRAD: TWideStringField
      FieldName = 'NOMEPLOGRAD'
      Size = 10
    end
    object QrFiadoresNOMEECIVIL: TWideStringField
      FieldName = 'NOMEECIVIL'
      Size = 10
    end
    object QrFiadoresNOMESEXO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESEXO'
      Calculated = True
    end
    object QrFiadoresCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrFiadoresNacionalid: TWideStringField
      FieldName = 'Nacionalid'
      Size = 15
    end
    object QrFiadoresConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrFiadoresCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrFiadoresRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrFiadoresSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrFiadoresDataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrFiadoresPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrFiadoresPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrFiadoresPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrFiadoresPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrFiadoresPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrFiadoresPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrFiadoresProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrFiadoresCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrFiadoresECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrFiadoresNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrFiadoresE_LNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LNR'
      Size = 255
      Calculated = True
    end
    object QrFiadoresCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrFiadoresETE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrFiadoresDATARG_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATARG_TXT'
      Calculated = True
    end
    object QrFiadoresPNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PNATAL_TXT'
      Calculated = True
    end
    object QrFiadoresTIPOFIADOR: TIntegerField
      FieldName = 'TIPOFIADOR'
    end
    object QrFiadoresNOMETIPOFIADOR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPOFIADOR'
      Size = 30
      Calculated = True
    end
    object QrFiadoresPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrFiadoresCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
    object QrFiadoresCPF_CONJUGE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_CONJUGE_TXT'
      Size = 30
      Calculated = True
    end
    object QrFiadoresCPF_Conjuge: TWideStringField
      FieldName = 'CPF_Conjuge'
      Size = 18
    end
  end
  object DsFiadores: TDataSource
    DataSet = QrFiadores
    Left = 156
    Top = 208
  end
  object frxDsFiadores: TfrxDBDataset
    UserName = 'frxDsFiadores'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Sexo=Sexo'
      'Nome=Nome'
      'ITEM=ITEM'
      'Pai=Pai'
      'Mae=Mae'
      'PNatal=PNatal'
      'NOMEPUF=NOMEPUF'
      'NOMEPLOGRAD=NOMEPLOGRAD'
      'NOMEECIVIL=NOMEECIVIL'
      'NOMESEXO=NOMESEXO'
      'CidadeNatal=CidadeNatal'
      'Nacionalid=Nacionalid'
      'ConjugeNome=ConjugeNome'
      'CPF=CPF'
      'RG=RG'
      'SSP=SSP'
      'DataRG=DataRG'
      'PRua=PRua'
      'PCompl=PCompl'
      'PBairro=PBairro'
      'PCidade=PCidade'
      'PCEP=PCEP'
      'PTe1=PTe1'
      'Profissao=Profissao'
      'Cargo=Cargo'
      'ECEP_TXT=ECEP_TXT'
      'NUMERO_TXT=NUMERO_TXT'
      'E_LNR=E_LNR'
      'CNPJ_TXT=CNPJ_TXT'
      'ETE1_TXT=ETE1_TXT'
      'DATARG_TXT=DATARG_TXT'
      'PNATAL_TXT=PNATAL_TXT'
      'TIPOFIADOR=TIPOFIADOR'
      'NOMETIPOFIADOR=NOMETIPOFIADOR'
      'PNumero=PNumero'
      'CPF_TXT=CPF_TXT'
      'CPF_CONJUGE_TXT=CPF_CONJUGE_TXT'
      'CPF_Conjuge=CPF_Conjuge')
    DataSet = QrFiadores
    BCDToCurrency = False
    Left = 184
    Top = 208
  end
  object QrRepr: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrReprAfterOpen
    OnCalcFields = QrReprCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.Nome, en.Sexo, en.Pai, en.Mae, en.PNatal,'
      'en.CidadeNatal, en.Nacionalid, en.ConjugeNome, en.CPF, en.RG, '
      'en.SSP, en.DataRG, en.PRua, en.PNumero, en.PCompl, en.PBairro, '
      'en.PCidade, en.PCEP, en.PTe1, en.Profissao, en.Cargo, '
      'uf.Nome NOMEPUF, ll.Nome NOMEPLOGRAD, le.Nome NOMEECIVIL'
      'FROM entidades en'
      'LEFT JOIN controle co    ON en.Empresa=co.Dono'
      'LEFT JOIN ufs uf         ON uf.Codigo=en.PUF'
      'LEFT JOIN listalograd ll ON ll.Codigo=en.PLograd'
      'LEFT JOIN listaecivil le ON le.Codigo=en.EstCivil'
      'WHERE en.Empresa=co.Dono'
      'ORDER BY en.Nome')
    Left = 128
    Top = 236
    object QrReprCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrReprNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrReprSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrReprPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrReprMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrReprPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrReprNOMEPUF: TWideStringField
      FieldName = 'NOMEPUF'
      Size = 2
    end
    object QrReprNOMEPLOGRAD: TWideStringField
      FieldName = 'NOMEPLOGRAD'
      Size = 10
    end
    object QrReprNOMEECIVIL: TWideStringField
      FieldName = 'NOMEECIVIL'
      Size = 10
    end
    object QrReprITEM: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'ITEM'
      Calculated = True
    end
    object QrReprNOMESEXO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESEXO'
      Calculated = True
    end
    object QrReprCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrReprNacionalid: TWideStringField
      FieldName = 'Nacionalid'
      Size = 15
    end
    object QrReprConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrReprCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrReprRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrReprSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrReprDataRG: TDateField
      FieldName = 'DataRG'
    end
    object QrReprPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrReprPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrReprPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrReprPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrReprPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrReprPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrReprProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrReprCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrReprECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrReprNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrReprE_LNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LNR'
      Size = 255
      Calculated = True
    end
    object QrReprCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrReprETE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ETE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrReprDATARG_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATARG_TXT'
      Calculated = True
    end
    object QrReprPNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PNATAL_TXT'
      Calculated = True
    end
    object QrReprPNumero: TIntegerField
      FieldName = 'PNumero'
    end
  end
  object DsRepr: TDataSource
    DataSet = QrRepr
    Left = 156
    Top = 236
  end
  object frxDsRepr: TfrxDBDataset
    UserName = 'frxDsRepr'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome'
      'Sexo=Sexo'
      'Pai=Pai'
      'Mae=Mae'
      'PNatal=PNatal'
      'NOMEPUF=NOMEPUF'
      'NOMEPLOGRAD=NOMEPLOGRAD'
      'NOMEECIVIL=NOMEECIVIL'
      'ITEM=ITEM'
      'NOMESEXO=NOMESEXO'
      'CidadeNatal=CidadeNatal'
      'Nacionalid=Nacionalid'
      'ConjugeNome=ConjugeNome'
      'CPF=CPF'
      'RG=RG'
      'SSP=SSP'
      'DataRG=DataRG'
      'PRua=PRua'
      'PCompl=PCompl'
      'PBairro=PBairro'
      'PCidade=PCidade'
      'PCEP=PCEP'
      'PTe1=PTe1'
      'Profissao=Profissao'
      'Cargo=Cargo'
      'ECEP_TXT=ECEP_TXT'
      'NUMERO_TXT=NUMERO_TXT'
      'E_LNR=E_LNR'
      'CNPJ_TXT=CNPJ_TXT'
      'ETE1_TXT=ETE1_TXT'
      'DATARG_TXT=DATARG_TXT'
      'PNATAL_TXT=PNATAL_TXT'
      'PNumero=PNumero')
    DataSet = QrRepr
    BCDToCurrency = False
    Left = 184
    Top = 236
  end
  object frxContrato: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39717.948385231500000000
    ReportOptions.LastChange = 39717.948385231500000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnClickObject = frxContratoClickObject
    OnGetValue = frxContratoGetValue
    Left = 184
    Top = 124
    Datasets = <
      item
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
      end
      item
        DataSet = frxDsContratos
        DataSetName = 'frxDsContratos'
      end
      item
        DataSet = DModG.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEmpresa
        DataSetName = 'frxDsEmpresa'
      end
      item
        DataSet = frxDsFiadores
        DataSetName = 'frxDsFiadores'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsRepr
        DataSetName = 'frxDsRepr'
      end
      item
        DataSet = frxDsSocios
        DataSetName = 'frxDsSocios'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object Band5: TfrxDetailData
        FillType = ftBrush
        Height = 83.779530000000000000
        Top = 313.700990000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsSocios
        DataSetName = 'frxDsSocios'
        RowCount = 0
        object Memo98: TfrxMemoView
          Left = 21.559060000000000000
          Top = 4.503710000000012000
          Width = 684.000000000000000000
          Height = 76.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        end
        object Memo99: TfrxMemoView
          Left = 33.779530000000000000
          Top = 8.503710000000013000
          Width = 37.795278030000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo100: TfrxMemoView
          Left = 71.338590000000000000
          Top = 8.503710000000013000
          Width = 394.960629920000000000
          Height = 17.000000000000000000
          Cursor = crHandPoint
          TagStr = '[frxDsSocios."Socio"]'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsSocios."NOMESOCIO"]')
          ParentFont = False
        end
        object Memo101: TfrxMemoView
          Left = 33.779530000000000000
          Top = 26.503710000000010000
          Width = 37.795275590551200000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CPF:')
          ParentFont = False
        end
        object Memo102: TfrxMemoView
          Left = 71.338590000000000000
          Top = 26.503710000000010000
          Width = 152.440940000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsSocios."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo103: TfrxMemoView
          Left = 225.779530000000000000
          Top = 26.503710000000010000
          Width = 140.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cart. de Identidade/Emissor:')
          ParentFont = False
        end
        object Memo104: TfrxMemoView
          Left = 365.779530000000000000
          Top = 26.503710000000010000
          Width = 332.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsSocios."RG"] - [frxDsSocios."SSP"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 469.779530000000000000
          Top = 8.503710000000013000
          Width = 67.338590000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Qualifica'#231#227'o:')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 537.118120000000000000
          Top = 8.503710000000013000
          Width = 160.661410000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsSocios."Cargo"]')
          ParentFont = False
        end
        object Memo129: TfrxMemoView
          Left = 33.779530000000000000
          Top = 62.503710000000010000
          Width = 75.559060000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Nacionalidade:')
          ParentFont = False
        end
        object Memo130: TfrxMemoView
          Left = 109.338590000000000000
          Top = 62.503710000000010000
          Width = 148.440940000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsSocios."Nacionalid"]')
          ParentFont = False
        end
        object Memo133: TfrxMemoView
          Left = 257.779530000000000000
          Top = 62.503710000000010000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Estado civil:')
          ParentFont = False
        end
        object Memo134: TfrxMemoView
          Left = 321.779530000000000000
          Top = 62.503710000000010000
          Width = 156.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsSocios."NOMEECIVIL"]')
          ParentFont = False
        end
        object Memo135: TfrxMemoView
          Left = 477.779530000000000000
          Top = 62.503710000000010000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo136: TfrxMemoView
          Left = 525.779530000000000000
          Top = 62.503710000000010000
          Width = 172.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsSocios."ETE1_TXT"]')
          ParentFont = False
        end
        object Memo131: TfrxMemoView
          Left = 97.779530000000000000
          Top = 44.503710000000010000
          Width = 600.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsSocios."E_LNR"]')
          ParentFont = False
        end
        object Memo132: TfrxMemoView
          Left = 33.779530000000000000
          Top = 44.503710000000010000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Endere'#231'o: ')
          ParentFont = False
        end
      end
      object Band6: TfrxMasterData
        FillType = ftBrush
        Height = 273.559060000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsEmpresa
        DataSetName = 'frxDsEmpresa'
        RowCount = 0
        object Line2: TfrxLineView
          Left = 2.000000000000000000
          Top = 61.102350000000000000
          Width = 720.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Memo96: TfrxMemoView
          Left = 14.000000000000000000
          Top = 121.102350000000000000
          Width = 196.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'QUADRO I - VENDEDORA-CONTRATANTE')
          ParentFont = False
        end
        object Memo97: TfrxMemoView
          Left = 14.000000000000000000
          Top = 248.102350000000000000
          Width = 688.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'QUADRO II - REPRESENTANTE(S) DA VENDEDORA-CONTRATANTE')
          ParentFont = False
        end
        object Memo105: TfrxMemoView
          Left = 14.000000000000000000
          Top = 141.102350000000000000
          Width = 684.000000000000000000
          Height = 100.000000000000000000
          Frame.Style = fsDot
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        end
        object Memo106: TfrxMemoView
          Left = 90.000000000000000000
          Top = 145.102350000000000000
          Width = 600.000000000000000000
          Height = 17.000000000000000000
          Cursor = crHandPoint
          TagStr = '[frxDsEmpresa."Codigo"]'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEmpresa."RazaoSocial"]')
          ParentFont = False
        end
        object Memo107: TfrxMemoView
          Left = 26.000000000000000000
          Top = 145.102350000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Raz. Social: ')
          ParentFont = False
        end
        object Memo108: TfrxMemoView
          Left = 90.000000000000000000
          Top = 163.102350000000000000
          Width = 168.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEmpresa."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo109: TfrxMemoView
          Left = 26.000000000000000000
          Top = 163.102350000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo110: TfrxMemoView
          Left = 322.000000000000000000
          Top = 163.102350000000000000
          Width = 168.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEmpresa."IE"]')
          ParentFont = False
        end
        object Memo111: TfrxMemoView
          Left = 258.000000000000000000
          Top = 163.102350000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'I.E.:')
          ParentFont = False
        end
        object Memo112: TfrxMemoView
          Left = 554.000000000000000000
          Top = 163.102350000000000000
          Width = 136.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEmpresa."NIRE"]')
          ParentFont = False
        end
        object Memo113: TfrxMemoView
          Left = 490.000000000000000000
          Top = 163.102350000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'NIRE:')
          ParentFont = False
        end
        object Memo114: TfrxMemoView
          Left = 90.000000000000000000
          Top = 181.102350000000000000
          Width = 600.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEmpresa."E_LNR"]')
          ParentFont = False
        end
        object Memo115: TfrxMemoView
          Left = 26.000000000000000000
          Top = 181.102350000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Endere'#231'o: ')
          ParentFont = False
        end
        object Memo116: TfrxMemoView
          Left = 274.000000000000000000
          Top = 199.102350000000000000
          Width = 272.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEmpresa."ECidade"]')
          ParentFont = False
        end
        object Memo117: TfrxMemoView
          Left = 210.000000000000000000
          Top = 199.102350000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cidade: ')
          ParentFont = False
        end
        object Memo118: TfrxMemoView
          Left = 610.000000000000000000
          Top = 199.102350000000000000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEmpresa."NOMEUF"]')
          ParentFont = False
        end
        object Memo119: TfrxMemoView
          Left = 546.000000000000000000
          Top = 199.102350000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Estado: ')
          ParentFont = False
        end
        object Memo120: TfrxMemoView
          Left = 90.000000000000000000
          Top = 199.102350000000000000
          Width = 120.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEmpresa."ECEP_TXT"]')
          ParentFont = False
        end
        object Memo121: TfrxMemoView
          Left = 26.000000000000000000
          Top = 199.102350000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
        end
        object Memo122: TfrxMemoView
          Left = 90.000000000000000000
          Top = 217.102350000000000000
          Width = 196.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEmpresa."ETE1_TXT"]')
          ParentFont = False
        end
        object Memo123: TfrxMemoView
          Left = 26.000000000000000000
          Top = 217.102350000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo124: TfrxMemoView
          Left = 350.000000000000000000
          Top = 217.102350000000000000
          Width = 156.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEmpresa."FAX_TXT"]')
          ParentFont = False
        end
        object Memo125: TfrxMemoView
          Left = 286.000000000000000000
          Top = 217.102350000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
        end
        object Memo126: TfrxMemoView
          Left = 2.000000000000000000
          Top = 21.102350000000000000
          Width = 716.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo127: TfrxMemoView
          Left = 538.000000000000000000
          Top = 5.102350000000001000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Folha: [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
        object Memo128: TfrxMemoView
          Left = 2.000000000000000000
          Top = 65.102350000000000000
          Width = 716.000000000000000000
          Height = 48.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            
              'INSTRUMENTO PARTICULAR DE CONTRATO DE FOMENTO MERCANTIL, NA MODA' +
              'LIDADE CONVENCIONAL, '
            'QUE ENTRE SI FAZEM AS PARTES ABAIXO NOMEADAS E QUALIFICADAS, '
            'MEDIANTE AS CL'#193'USULAS E CONDI'#199#213'ES PACTUADAS E ACEITAS, A SABER: '
            ' ')
          ParentFont = False
        end
        object Memo189: TfrxMemoView
          Left = 2.000000000000000000
          Top = 41.102350000000000000
          Width = 716.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'CONTRATO DE FOMENTO MERCANTIL N'#186' [frxDsContratos."Contrato"]')
          ParentFont = False
        end
        object Line4: TfrxLineView
          Left = 2.000000000000000000
          Top = 113.102350000000000000
          Width = 720.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
      end
      object Band7: TfrxHeader
        FillType = ftBrush
        Height = 24.000000000000000000
        Top = 419.527830000000000000
        Width = 718.110700000000000000
        object Memo137: TfrxMemoView
          Left = 10.000000000000000000
          Top = 1.897339999999986000
          Width = 688.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'QUADRO III - FIADOR(ES)')
          ParentFont = False
        end
      end
      object Band8: TfrxDetailData
        FillType = ftBrush
        Height = 80.000000000000000000
        Top = 464.882190000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsFiadores
        DataSetName = 'frxDsFiadores'
        RowCount = 0
        object Memo138: TfrxMemoView
          Left = 14.000000000000000000
          Top = 1.322510000000023000
          Width = 684.000000000000000000
          Height = 76.000000000000000000
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        end
        object Memo139: TfrxMemoView
          Left = 30.000000000000000000
          Top = 5.322510000000022000
          Width = 99.338590000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsFiadores."NOMETIPOFIADOR"]')
          ParentFont = False
        end
        object Memo140: TfrxMemoView
          Left = 129.338590000000000000
          Top = 5.322510000000022000
          Width = 564.661410000000100000
          Height = 17.000000000000000000
          Cursor = crHandPoint
          TagStr = '[frxDsFiadores."Codigo"]'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsFiadores."Nome"]')
          ParentFont = False
        end
        object Memo141: TfrxMemoView
          Left = 30.000000000000000000
          Top = 23.322510000000020000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CPF:')
          ParentFont = False
        end
        object Memo142: TfrxMemoView
          Left = 62.000000000000000000
          Top = 23.322510000000020000
          Width = 156.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsFiadores."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo143: TfrxMemoView
          Left = 218.000000000000000000
          Top = 23.322510000000020000
          Width = 140.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cart. de Identidade/Emissor:')
          ParentFont = False
        end
        object Memo144: TfrxMemoView
          Left = 358.000000000000000000
          Top = 23.322510000000020000
          Width = 336.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsFiadores."RG"] - [frxDsFiadores."SSP"]')
          ParentFont = False
        end
        object Memo145: TfrxMemoView
          Left = 30.000000000000000000
          Top = 59.322510000000030000
          Width = 75.559060000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Nacionalidade:')
          ParentFont = False
        end
        object Memo146: TfrxMemoView
          Left = 105.559060000000000000
          Top = 59.322510000000030000
          Width = 148.440940000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsFiadores."Nacionalid"]')
          ParentFont = False
        end
        object Memo149: TfrxMemoView
          Left = 254.000000000000000000
          Top = 59.322510000000030000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Estado civil:')
          ParentFont = False
        end
        object Memo150: TfrxMemoView
          Left = 318.000000000000000000
          Top = 59.322510000000030000
          Width = 156.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsFiadores."NOMEECIVIL"]')
          ParentFont = False
        end
        object Memo151: TfrxMemoView
          Left = 474.000000000000000000
          Top = 59.322510000000030000
          Width = 47.779530000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo152: TfrxMemoView
          Left = 521.779530000000000000
          Top = 59.322510000000030000
          Width = 172.220470000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsFiadores."ETE1_TXT"]')
          ParentFont = False
        end
        object Memo147: TfrxMemoView
          Left = 94.000000000000000000
          Top = 41.322510000000030000
          Width = 600.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsFiadores."E_LNR"]')
          ParentFont = False
        end
        object Memo148: TfrxMemoView
          Left = 30.000000000000000000
          Top = 41.322510000000030000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Endere'#231'o: ')
          ParentFont = False
        end
      end
      object Band9: TfrxHeader
        FillType = ftBrush
        Height = 28.000000000000000000
        Top = 566.929499999999900000
        Width = 718.110700000000000000
        object Memo153: TfrxMemoView
          Left = 10.000000000000000000
          Top = 7.054729999999950000
          Width = 688.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'QUADRO IV - COMPRADORA-CONTRATADA')
          ParentFont = False
        end
      end
      object Band10: TfrxDetailData
        FillType = ftBrush
        Height = 108.000000000000000000
        Top = 616.063390000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
        RowCount = 0
        object Memo154: TfrxMemoView
          Left = 14.000000000000000000
          Top = 1.920839999999998000
          Width = 684.000000000000000000
          Height = 100.000000000000000000
          Frame.Style = fsDot
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
        end
        object Memo155: TfrxMemoView
          Left = 90.000000000000000000
          Top = 5.920839999999999000
          Width = 600.000000000000000000
          Height = 17.000000000000000000
          Cursor = crHandPoint
          TagStr = '[frxDsDono."Codigo"]'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo156: TfrxMemoView
          Left = 26.000000000000000000
          Top = 5.920839999999999000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Raz. Social: ')
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Left = 90.000000000000000000
          Top = 23.920840000000000000
          Width = 168.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Left = 26.000000000000000000
          Top = 23.920840000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Left = 322.000000000000000000
          Top = 23.920840000000000000
          Width = 168.000000000000000000
          Height = 17.000000000000000000
          DataField = 'IE_RG'
          DataSet = Dmod.frxDsDono
          DataSetName = 'frxDsDono'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."IE_RG"]')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Left = 258.000000000000000000
          Top = 23.920840000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'I.E.:')
          ParentFont = False
        end
        object Memo161: TfrxMemoView
          Left = 554.000000000000000000
          Top = 23.920840000000000000
          Width = 136.000000000000000000
          Height = 17.000000000000000000
          DataField = 'NIRE_'
          DataSet = Dmod.frxDsDono
          DataSetName = 'frxDsDono'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."NIRE_"]')
          ParentFont = False
        end
        object Memo162: TfrxMemoView
          Left = 490.000000000000000000
          Top = 23.920840000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'NIRE:')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Left = 90.000000000000000000
          Top = 41.920840000000000000
          Width = 600.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."E_LNR"]')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Left = 274.000000000000000000
          Top = 59.920840000000000000
          Width = 272.000000000000000000
          Height = 17.000000000000000000
          DataField = 'CIDADE'
          DataSet = Dmod.frxDsDono
          DataSetName = 'frxDsDono'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."CIDADE"]')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Left = 210.000000000000000000
          Top = 59.920840000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cidade: ')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Left = 610.000000000000000000
          Top = 59.920840000000000000
          Width = 32.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."NOMEUF"]')
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Left = 546.000000000000000000
          Top = 59.920840000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Estado: ')
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Left = 90.000000000000000000
          Top = 59.920840000000000000
          Width = 120.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."ECEP_TXT"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 26.000000000000000000
          Top = 41.920840000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 26.000000000000000000
          Top = 59.920840000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Left = 90.000000000000000000
          Top = 77.920840000000000000
          Width = 196.000000000000000000
          Height = 17.000000000000000000
          DataField = 'TE1_TXT'
          DataSet = Dmod.frxDsDono
          DataSetName = 'frxDsDono'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."TE1_TXT"]')
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Left = 26.000000000000000000
          Top = 77.920840000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo171: TfrxMemoView
          Left = 350.000000000000000000
          Top = 77.920840000000000000
          Width = 156.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."FAX_TXT"]')
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Left = 286.000000000000000000
          Top = 77.920840000000000000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
        end
      end
      object Band11: TfrxHeader
        FillType = ftBrush
        Height = 28.000000000000000000
        Top = 748.346940000000000000
        Width = 718.110700000000000000
        object Memo188: TfrxMemoView
          Left = 10.000000000000000000
          Top = 1.637290000000007000
          Width = 688.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'QUADRO V - REPRESENTANTE(S) DA COMPRADORA-CONTRATADA')
          ParentFont = False
        end
      end
      object Band12: TfrxDetailData
        FillType = ftBrush
        Height = 80.000000000000000000
        Top = 797.480830000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsRepr
        DataSetName = 'frxDsRepr'
        RowCount = 0
        object Memo173: TfrxMemoView
          Left = 14.000000000000000000
          Top = 0.503400000000056000
          Width = 684.000000000000000000
          Height = 76.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          ParentFont = False
        end
        object Memo174: TfrxMemoView
          Left = 30.000000000000000000
          Top = 4.503400000000056000
          Width = 35.779530000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Nome:')
          ParentFont = False
        end
        object Memo175: TfrxMemoView
          Left = 65.779530000000000000
          Top = 4.503400000000056000
          Width = 628.220470000000000000
          Height = 17.000000000000000000
          Cursor = crHandPoint
          TagStr = '[frxDsRepr."Codigo"]'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsRepr."Nome"]')
          ParentFont = False
        end
        object Memo176: TfrxMemoView
          Left = 30.000000000000000000
          Top = 22.503400000000060000
          Width = 35.779530000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CPF:')
          ParentFont = False
        end
        object Memo177: TfrxMemoView
          Left = 65.779530000000000000
          Top = 22.503400000000060000
          Width = 156.220470000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsRepr."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo178: TfrxMemoView
          Left = 222.000000000000000000
          Top = 22.503400000000060000
          Width = 140.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cart. de Identidade/Emissor:')
          ParentFont = False
        end
        object Memo179: TfrxMemoView
          Left = 362.000000000000000000
          Top = 22.503400000000060000
          Width = 332.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsRepr."RG"] - [frxDsRepr."SSP"]')
          ParentFont = False
        end
        object Memo180: TfrxMemoView
          Left = 30.000000000000000000
          Top = 54.723870000000030000
          Width = 75.559060000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Nacionalidade:')
          ParentFont = False
        end
        object Memo181: TfrxMemoView
          Left = 105.559060000000000000
          Top = 54.723870000000030000
          Width = 148.440940000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsRepr."Nacionalid"]')
          ParentFont = False
        end
        object Memo182: TfrxMemoView
          Left = 94.000000000000000000
          Top = 40.503400000000060000
          Width = 600.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsRepr."E_LNR"]')
          ParentFont = False
        end
        object Memo183: TfrxMemoView
          Left = 30.000000000000000000
          Top = 40.503400000000060000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Endere'#231'o: ')
          ParentFont = False
        end
        object Memo184: TfrxMemoView
          Left = 254.000000000000000000
          Top = 54.723870000000030000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Estado civil:')
          ParentFont = False
        end
        object Memo185: TfrxMemoView
          Left = 318.000000000000000000
          Top = 54.723870000000030000
          Width = 156.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsRepr."NOMEECIVIL"]')
          ParentFont = False
        end
        object Memo186: TfrxMemoView
          Left = 474.000000000000000000
          Top = 54.723870000000030000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo187: TfrxMemoView
          Left = 522.000000000000000000
          Top = 54.723870000000030000
          Width = 172.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsRepr."ETE1_TXT"]')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 38.000000000000000000
        Top = 124.724490000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
        RowCount = 0
        Stretched = True
        object Rich1: TfrxRichView
          Left = 83.149606299212600000
          Top = 9.070809999999995000
          Width = 620.000000000000000000
          Height = 20.000000000000000000
          StretchMode = smMaxHeight
          DataField = 'Texto'
          DataSet = frxDsCartas
          DataSetName = 'frxDsCartas'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C204D532053616E732053657269663B7D7D0D0A7B5C636F6C6F72
            74626C203B5C726564305C677265656E305C626C7565303B7D0D0A7B5C2A5C67
            656E657261746F722052696368656432302031302E302E31343339337D5C7669
            65776B696E64345C756331200D0A5C706172645C6366315C66305C667331365C
            7061720D0A7D0D0A00}
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        Height = 4.000000000000000000
        Top = 98.267780000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
        RowCount = 0
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 20.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo1: TfrxMemoView
          Left = 538.000000000000000000
          Top = 1.543289999999999000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'Folha: [PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 90.897650000000000000
        Top = 498.897960000000000000
        Width = 718.110700000000000000
        object Memo10: TfrxMemoView
          Left = 83.149660000000000000
          Width = 96.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Testemunhas:')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 83.149660000000000000
          Top = 40.000000000000000000
          Width = 284.000000000000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            'Nome: [frxDsContratos."Testem1Nome"]'
            'CPF: [frxDsContratos."Testem1CPF_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 419.527612750000000000
          Top = 40.000000000000000000
          Width = 284.000000000000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            'Nome: [frxDsContratos."Testem2Nome"]'
            'CPF: [frxDsContratos."Testem2CPF_TXT"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 102.897650000000000000
        Top = 185.196970000000000000
        Width = 718.110700000000000000
        RowCount = 1
        object Memo21: TfrxMemoView
          Left = 83.149606299212600000
          Top = 52.000000000000000000
          Width = 284.000000000000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '[frxDsEmpresa."RazaoSocial"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 419.149660000000000000
          Top = 52.000000000000000000
          Width = 284.000000000000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 183.149660000000000000
          Width = 516.000000000000000000
          Height = 20.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsDono."CIDADE"], [frxDsContratos."DATAC_TXT"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 16.000000000000000000
        Top = 309.921460000000000000
        Width = 718.110700000000000000
        object Memo5: TfrxMemoView
          Left = 83.149660000000000000
          Width = 96.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Fiadores:')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 88.692950000000000000
        Top = 347.716760000000000000
        Width = 718.110700000000000000
        DataSet = frxDsFiadores
        DataSetName = 'frxDsFiadores'
        RowCount = 0
        object Memo6: TfrxMemoView
          Left = 83.149660000000000000
          Top = 37.795300000000000000
          Width = 284.000000000000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '[frxDsFiadores."Nome"]'
            'CPF: [frxDsFiadores."CPF_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 419.149660000000000000
          Top = 37.795300000000000000
          Width = 284.000000000000000000
          Height = 32.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '[frxDsFiadores."ConjugeNome"]'
            'CPF: [frxDsFiadores."CPF_CONJUGE_TXT"]')
          ParentFont = False
          WordWrap = False
        end
      end
    end
  end
  object QrCartas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cartas'
      'WHERE Tipo=1'
      'AND CartaG=:P0')
    Left = 156
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCartasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cartas.Codigo'
    end
    object QrCartasTitulo: TWideStringField
      FieldName = 'Titulo'
      Origin = 'cartas.Titulo'
      Size = 100
    end
    object QrCartasTexto: TWideMemoField
      FieldName = 'Texto'
      Origin = 'cartas.Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCartasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cartas.Lk'
    end
    object QrCartasDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cartas.DataCad'
    end
    object QrCartasDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cartas.DataAlt'
    end
    object QrCartasUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cartas.UserCad'
    end
    object QrCartasUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cartas.UserAlt'
    end
  end
  object frxDsCartas: TfrxDBDataset
    UserName = 'frxDsCartas'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Titulo=Titulo'
      'Texto=Texto'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt')
    DataSet = QrCartas
    BCDToCurrency = False
    Left = 184
    Top = 264
  end
  object Qr11: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEFIADOR, Codigo'
      'FROM entidades'
      'WHERE Fornece4='#39'V'#39
      'ORDER BY NOMEFIADOR')
    Left = 624
    Top = 252
    object StringField1: TWideStringField
      FieldName = 'NOMEFIADOR'
      Size = 100
    end
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object Qr12: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEFIADOR, Codigo'
      'FROM entidades'
      'WHERE Fornece4='#39'V'#39
      'ORDER BY NOMEFIADOR')
    Left = 624
    Top = 280
    object StringField2: TWideStringField
      FieldName = 'NOMEFIADOR'
      Size = 100
    end
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object Qr22: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEFIADOR, Codigo'
      'FROM entidades'
      'WHERE Fornece4='#39'V'#39
      'ORDER BY NOMEFIADOR')
    Left = 624
    Top = 308
    object StringField3: TWideStringField
      FieldName = 'NOMEFIADOR'
      Size = 100
    end
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object frxDsContratos: TfrxDBDataset
    UserName = 'frxDsContratos'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Testem1CPF_TXT=Testem1CPF_TXT'
      'Testem2CPF_TXT=Testem2CPF_TXT'
      'DATAC_TXT=DATAC_TXT'
      'NOMECLIENTE=NOMECLIENTE'
      'NOMEF11=NOMEF11'
      'NOMEF12=NOMEF12'
      'NOMEF21=NOMEF21'
      'NOMEF22=NOMEF22'
      'Codigo=Codigo'
      'Cliente=Cliente'
      'Contrato=Contrato'
      'DataC=DataC'
      'Limite=Limite'
      'Fiador11=Fiador11'
      'Fiador12=Fiador12'
      'Fiador21=Fiador21'
      'Fiador22=Fiador22'
      'Testem1Nome=Testem1Nome'
      'Testem1CPF=Testem1CPF'
      'Testem2Nome=Testem2Nome'
      'Testem2CPF=Testem2CPF'
      'Texto=Texto'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo')
    DataSet = QrContratos
    BCDToCurrency = False
    Left = 328
    Top = 12
  end
  object Qr13: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEFIADOR, Codigo'
      'FROM entidades'
      'WHERE Fornece4='#39'V'#39
      'ORDER BY NOMEFIADOR')
    Left = 624
    Top = 336
    object StringField4: TWideStringField
      FieldName = 'NOMEFIADOR'
      Size = 100
    end
    object IntegerField5: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object Ds13: TDataSource
    DataSet = Qr13
    Left = 652
    Top = 336
  end
  object Qr14: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEFIADOR, Codigo'
      'FROM entidades'
      'WHERE Fornece4='#39'V'#39
      'ORDER BY NOMEFIADOR')
    Left = 624
    Top = 364
    object StringField5: TWideStringField
      FieldName = 'NOMEFIADOR'
      Size = 100
    end
    object IntegerField6: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object Ds14: TDataSource
    DataSet = Qr14
    Left = 652
    Top = 364
  end
end
