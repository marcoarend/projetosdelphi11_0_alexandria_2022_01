unit EmitSacImport;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, ComCtrls, dmkEdit,
  dmkGeral;

type
  TFmEmitSacImport = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Memo1: TMemo;
    Panel3: TPanel;
    Label1: TLabel;
    Edit1: TdmkEdit;
    BtImportar: TBitBtn;
    OpenDialog1: TOpenDialog;
    GroupBox1: TGroupBox;
    Label2: TLabel;
    EdFonte00: TdmkEdit;
    Label3: TLabel;
    EdFonte01: TdmkEdit;
    Label4: TLabel;
    EdFonte02: TdmkEdit;
    QrEmi: TmySQLQuery;
    QrBAC: TmySQLQuery;
    Label5: TLabel;
    EdFonte09: TdmkEdit;
    QrPesq: TmySQLQuery;
    QrPesqCodigo: TAutoIncField;
    QrPesqCPF: TWideStringField;
    QrPesqNome: TWideStringField;
    GroupBox2: TGroupBox;
    Ed001s: TdmkEdit;
    Ed001n: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Ed002s: TdmkEdit;
    Ed002n: TdmkEdit;
    Label9: TLabel;
    CkVerificaCPF: TCheckBox;
    Label10: TLabel;
    EdDIBe: TdmkEdit;
    Label11: TLabel;
    PB1: TProgressBar;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtImportarClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    F_DIB, F001s, F001n, F002s, F002n, FDIBe: Integer;
    procedure Define_DIB_Fonte(Linha: Integer);
    procedure ImportaEmi(Linha: Integer);
    procedure ImportaBAC(Linha: Integer);
  public
    { Public declarations }
  end;

  var
  FmEmitSacImport: TFmEmitSacImport;

implementation

uses Module, UnMyObjects;

{$R *.DFM}

procedure TFmEmitSacImport.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmitSacImport.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmEmitSacImport.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmEmitSacImport.BtImportarClick(Sender: TObject);
var
  j, k, n: string;
  i, C0, C1, C2, C9: Integer;
begin
  if OpenDialog1.Execute then
  begin
    Screen.Cursor := crHourGlass;
    Edit1.Text := OpenDialog1.Filename;
    Memo1.Lines.LoadFromFile(OpenDialog1.Filename);
    //
    EdFonte00.Text := '';
    EdFonte01.Text := '';
    EdFonte02.Text := '';
    EdFonte09.Text := '';
    C0 := 0;
    C1 := 0;
    C2 := 0;
    C9 := 0;
    //
    for i := 0 to Memo1.Lines.Count -1 do
    begin
      j := Copy(Memo1.Lines[i], 1, 3);
      case Geral.IMV(j) of
        0:
        begin
          if j = '000' then
          begin
            C0 := C0 + 1;
            k := Trim(Copy(Memo1.Lines[i], 4, 15));
            n := Trim(Copy(Memo1.Lines[i], 19, 100));
            k := Geral.SoNumero_TT(Geral.FormataCNPJ_TT(k));
            if k <> '' then
            begin
              QrPesq.Close;
              QrPesq.Params[0].AsString := k;
              QrPesq.Open;
              if QrPesq.RecordCount = 0 then
              begin
                if Application.MessageBox(PChar('Deseja cadastrar o novo '+
                'parceiro abaixo?'+Chr(13)+Chr(10)+k+' - '+n), 'Pergunta',
                MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
                begin
                  Dmod.QrUpd.SQL.Clear;
                  Dmod.QrUpd.SQL.Add('INSERT INTO emitpar SET ');
                  Dmod.QrUpd.SQL.Add('CPF=:P0, Nome=:P1');
                  Dmod.QrUpd.SQL.Add('');
                  Dmod.QrUpd.Params[00].AsString := k;
                  Dmod.QrUpd.Params[01].AsString := n;
                  Dmod.QrUpd.ExecSQL;
                end;
              end;
            end;
          end else
          begin
            if Trim(Memo1.Lines[i]) <> '' then C9 := C9 + 1;
          end;
        end;
        1: C1 := C1 + 1;
        2: C2 := C2 + 1;
        else C9 := C9 + 1;
      end;
    end;
    EdFonte00.Text := IntToStr(C0);
    EdFonte01.Text := IntToStr(C1);
    EdFonte02.Text := IntToStr(C2);
    EdFonte09.Text := IntToStr(C9);
    Screen.Cursor := crDefault;
    if C9 > 1 then
    begin
      Application.MessageBox(PChar('AVISO: Verifique se o arquivo realmente � '+
      'de emitentes antes de importar!'), 'Aviso', MB_OK+MB_ICONWARNING);
    end;
  end;
end;

procedure TFmEmitSacImport.BtOKClick(Sender: TObject);
var
  j: string;
  i: Integer;
begin
  Screen.Cursor := crHourGlass;
  Ed001n.Text := '';
  Ed001s.Text := '';
  Ed002n.Text := '';
  Ed002s.Text := '';
  EdDIBe.Text := '';
  //
  F001s := 0;
  F001n := 0;
  F002s := 0;
  F002n := 0;
  FDIBe := 0;
  //
  QrEmi.SQL.Clear;
  QrEmi.SQL.Add('INSERT INTO emitcpf SET ');
  QrEmi.SQL.Add('CPF=:P0, Nome=:P1, LastAtz=:P2, Limite=:P3, Fonte=:P4');
  QrEmi.SQL.Add('');
  //
  QrBAC.SQL.Clear;
  QrBAC.SQL.Add('INSERT INTO emitbac SET ');
  QrBAC.SQL.Add('CPF=:P0, BAC=:P1, Fonte=:P2');
  QrBAC.SQL.Add('');
  PB1.Position := 0;
  PB1.Max := Memo1.Lines.Count;
  for i := 0 to Memo1.Lines.Count -1 do
  begin
    PB1.Position := PB1.Position + 1;
    Application.ProcessMessages;
    j := Copy(Memo1.Lines[i], 1, 3);
    case Geral.IMV(j) of
      0: Define_DIB_Fonte(i);
      1: ImportaEmi(i);
      2: ImportaBAC(i);
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmEmitSacImport.Define_DIB_Fonte(Linha: Integer);
var
  k: String;
begin
  k := Trim(Copy(Memo1.Lines[Linha], 4, 15));
  k := Geral.SoNumero_TT(Geral.FormataCNPJ_TT(k));
  if k <> '' then
  begin
    QrPesq.Close;
    QrPesq.Params[0].AsString := k;
    QrPesq.Open;
    if QrPesq.RecordCount > 0 then F_DIB := QrPesqCodigo.Value else F_DIB := -1;
  end;
end;

procedure TFmEmitSacImport.ImportaEmi(Linha: Integer);
var
  DIB, Txt, Num, Dta: String;
begin
  try
    DIB := Trim(Copy(Memo1.Lines[Linha], 004, 15));
    if CkVerificaCPF.Checked then
    begin
      Num := MLAGeral.CalculaCNPJCPF(DIB);
      if DIB <> Num then
      begin
        FDIBe := FDIBe + 1;
        EdDIBe.Text := IntToStr(FDIBe);
        Exit;
      end;
    end;
    Txt := Trim(Copy(Memo1.Lines[Linha], 019, 50));
    Dta := Trim(Copy(Memo1.Lines[Linha], 069, 10));
    QrEmi.Params[00].AsString  := DIB;
    QrEmi.Params[01].AsString  := Txt;
    QrEmi.Params[02].AsString  := Dta;
    QrEmi.Params[03].AsFloat   := Dmod.QrControleCHRisco.Value;
    QrEmi.Params[04].AsInteger := F_DIB;
    QrEmi.ExecSQL;
    F001s := F001s + 1;
    Ed001s.Text := IntToStr(F001s);
  except
    F001n := F001n + 1;
    Ed001n.Text := IntToStr(F001n);
  end;
end;

procedure TFmEmitSacImport.ImportaBAC(Linha: Integer);
var
  Num, DIB, Txt: String;
begin
  try
    DIB := Trim(Copy(Memo1.Lines[Linha], 004, 15));
    if CkVerificaCPF.Checked then
    begin
      Num := MLAGeral.CalculaCNPJCPF(DIB);
      if DIB <> Num then
      begin
        FDIBe := FDIBe + 1;
        EdDIBe.Text := IntToStr(FDIBe);
        Exit;
      end;
    end;
    Txt := Trim(Copy(Memo1.Lines[Linha], 019, 20));
    QrBAC.Params[00].AsString  := DIB;
    QrBAC.Params[01].AsString  := Txt;
    QrBAC.Params[02].AsInteger := F_DIB;
    QrBAC.ExecSQL;
    F002s := F002s + 1;
    Ed002s.Text := IntToStr(F002s);
  except
    F002n := F002n + 1;
    Ed002n.Text := IntToStr(F002n);
  end;
end;

end.

