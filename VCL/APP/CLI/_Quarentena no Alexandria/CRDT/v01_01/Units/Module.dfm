object Dmod: TDmod
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 549
  Width = 800
  object QrFields: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SHOW FIELDS FROM :p0')
    Left = 428
    Top = 159
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrMaster: TmySQLQuery
    Database = MyDB
    AfterOpen = QrMasterAfterOpen
    OnCalcFields = QrMasterCalcFields
    SQL.Strings = (
      'SELECT ma.Em, te.Tipo, te.Logo,  te.Logo2,cm.Dono, cm.Versao, '
      'cm.CNPJ, te.IE, te.ECidade, uf.Nome NOMEUF, te.EFax,'
      'te.ERua, te.ENumero, te.EBairro, te.ECEP, te.ECompl,'
      'te.EContato, te.ECel, te.ETe1, te.ETe2, te.ETe3, te.EPais,'
      'te.Respons1, te.Respons2, ma.Limite, ma.SolicitaSenha,'
      'ma.UsaAccMngr'
      'FROM entidades te, Controle cm, Ufs uf, Master ma'
      'WHERE te.Codigo=cm.Dono'
      
        'AND ((te.CNPJ=cm.CNPJ AND te.Tipo=0) OR (te.CPF=cm.CNPJ AND te.T' +
        'ipo=1))'
      'AND uf.Codigo=te.EUF'
      '')
    Left = 12
    Top = 143
    object QrMasterCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 30
      Calculated = True
    end
    object QrMasterCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 15
      Calculated = True
    end
    object QrMasterEm: TWideStringField
      FieldName = 'Em'
      Origin = 'master.Em'
      Required = True
      Size = 100
    end
    object QrMasterTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrMasterLogo: TBlobField
      FieldName = 'Logo'
      Origin = 'entidades.Logo'
      Size = 4
    end
    object QrMasterLogo2: TBlobField
      FieldName = 'Logo2'
      Origin = 'entidades.Logo2'
      Size = 4
    end
    object QrMasterDono: TIntegerField
      FieldName = 'Dono'
      Origin = 'controle.Dono'
      Required = True
    end
    object QrMasterVersao: TIntegerField
      FieldName = 'Versao'
      Origin = 'controle.Versao'
      Required = True
    end
    object QrMasterCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'controle.CNPJ'
      Required = True
      Size = 18
    end
    object QrMasterIE: TWideStringField
      FieldName = 'IE'
      Origin = 'entidades.IE'
    end
    object QrMasterECidade: TWideStringField
      FieldName = 'ECidade'
      Origin = 'entidades.ECidade'
      Size = 25
    end
    object QrMasterNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrMasterEFax: TWideStringField
      FieldName = 'EFax'
      Origin = 'entidades.EFax'
    end
    object QrMasterERua: TWideStringField
      FieldName = 'ERua'
      Origin = 'entidades.ERua'
      Size = 30
    end
    object QrMasterENumero: TIntegerField
      FieldName = 'ENumero'
      Origin = 'entidades.ENumero'
    end
    object QrMasterEBairro: TWideStringField
      FieldName = 'EBairro'
      Origin = 'entidades.EBairro'
      Size = 30
    end
    object QrMasterECEP: TIntegerField
      FieldName = 'ECEP'
      Origin = 'entidades.ECEP'
    end
    object QrMasterECompl: TWideStringField
      FieldName = 'ECompl'
      Origin = 'entidades.ECompl'
      Size = 30
    end
    object QrMasterEContato: TWideStringField
      FieldName = 'EContato'
      Origin = 'entidades.EContato'
      Size = 60
    end
    object QrMasterECel: TWideStringField
      FieldName = 'ECel'
      Origin = 'entidades.ECel'
    end
    object QrMasterETe1: TWideStringField
      FieldName = 'ETe1'
      Origin = 'entidades.ETe1'
    end
    object QrMasterETe2: TWideStringField
      FieldName = 'ETe2'
      Origin = 'entidades.Ete2'
    end
    object QrMasterETe3: TWideStringField
      FieldName = 'ETe3'
      Origin = 'entidades.Ete3'
    end
    object QrMasterEPais: TWideStringField
      FieldName = 'EPais'
      Origin = 'entidades.EPais'
    end
    object QrMasterRespons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Required = True
      Size = 60
    end
    object QrMasterRespons2: TWideStringField
      FieldName = 'Respons2'
      Origin = 'entidades.Respons2'
      Required = True
      Size = 60
    end
    object QrMasterLimite: TSmallintField
      FieldName = 'Limite'
      Origin = 'master.Limite'
      Required = True
    end
    object QrMasterUsaAccMngr: TSmallintField
      FieldName = 'UsaAccMngr'
    end
    object QrMasterSolicitaSenha: TSmallintField
      FieldName = 'SolicitaSenha'
      Origin = 'master.SolicitaSenha'
      Required = True
    end
  end
  object QrLivreY_D: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SELECT Controle Codigo FROM lanctos')
    Left = 320
    Top = 59
    object QrLivreY_DCodigo: TLargeintField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrRecCountX: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SELECT COUNT(*) Record From  ArtigosGrupos')
    Left = 392
    Top = 247
    object QrRecCountXRecord: TIntegerField
      FieldName = 'Record'
    end
  end
  object QrUser: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Login FROM senhas'
      'WHERE Numero=:Usuario')
    Left = 12
    Top = 95
    ParamData = <
      item
        DataType = ftInteger
        Name = 'Usuario'
        ParamType = ptUnknown
      end>
    object QrUserLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'DNTEACH.senhas.Login'
      Size = 128
    end
  end
  object QrSelX: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SELECT Lk FROM lanctos')
    Left = 272
    Top = 55
    object QrSelXLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DBMMONEY.lanctos.Lk'
    end
  end
  object QrSB: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Left = 204
    Top = 7
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSBCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrSBNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMBWET.pq.Nome'
      Size = 41
    end
  end
  object DsSB: TDataSource
    DataSet = QrSB
    Left = 232
    Top = 7
  end
  object QrSB2: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SELECT :P0 Codigo, :P1 Nome'
      'FROM :p2'
      'WHERE :P3 LIKE :P4')
    Left = 204
    Top = 51
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrSB2Codigo: TFloatField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.lanctos.Controle'
    end
    object QrSB2Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.lanctos.Descricao'
      Size = 128
    end
  end
  object DsSB2: TDataSource
    DataSet = QrSB2
    Left = 232
    Top = 51
  end
  object QrSB3: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SELECT fa.Data Codigo, ca.Nome Nome'
      'FROM faturas fa, Carteiras ca'
      'WHERE ca.Codigo=fa.Emissao'
      'AND fa.Emissao=15')
    Left = 204
    Top = 95
    object QrSB3Codigo: TDateField
      FieldName = 'Codigo'
      Origin = 'DBMMONEY.faturas.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSB3Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'DBMMONEY.carteiras.Nome'
      Size = 128
    end
  end
  object DsSB3: TDataSource
    DataSet = QrSB3
    Left = 232
    Top = 95
  end
  object QrDuplicIntX: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SELECT NF INTEIRO1, Cliente INTEIRO2, Codigo CODIGO'
      'FROM pqpse'
      'WHERE NF=1'
      'AND Cliente=1')
    Left = 392
    Top = 159
    object QrDuplicIntXINTEIRO1: TIntegerField
      FieldName = 'INTEIRO1'
      Origin = 'DBMBWET.pqpse.NF'
    end
    object QrDuplicIntXINTEIRO2: TIntegerField
      FieldName = 'INTEIRO2'
      Origin = 'DBMBWET.pqpse.Cliente'
    end
    object QrDuplicIntXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pqpse.Codigo'
    end
  end
  object QrDuplicStrX: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SELECT Nome NOME, Codigo CODIGO, IDCodigo ANTERIOR'
      'FROM pq'
      'WHERE Nome=:Nome')
    Left = 364
    Top = 159
    ParamData = <
      item
        DataType = ftString
        Name = 'Nome'
        ParamType = ptUnknown
      end>
    object QrDuplicStrXNOME: TWideStringField
      FieldName = 'NOME'
      Origin = 'DBMBWET.pq.Nome'
      Size = 128
    end
    object QrDuplicStrXCODIGO: TIntegerField
      FieldName = 'CODIGO'
      Origin = 'DBMBWET.pq.Codigo'
    end
    object QrDuplicStrXANTERIOR: TIntegerField
      FieldName = 'ANTERIOR'
      Origin = 'DBMBWET.pq.IDCodigo'
    end
  end
  object QrInsLogX: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'INSERT INTO logs SET'
      'Data=NOW(),'
      'Tipo=:P0,'
      'Usuario=:P1,'
      'ID=:P2')
    Left = 392
    Top = 203
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrDelLogX: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'DELETE FROM logs'
      'WHERE Tipo=:P0'
      'AND Usuario=:P1'
      'AND ID=:P2')
    Left = 364
    Top = 203
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrDataBalY: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SELECT MAX(Data) Data FROM pesagem'
      'WHERE Tipo=4')
    Left = 272
    Top = 7
    object QrDataBalYData: TDateField
      FieldName = 'Data'
      Origin = 'DBMBWET.pesagem.Data'
    end
  end
  object QrSenha: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM senhas'
      'WHERE Login=:P0'
      'AND Senha=:P1')
    Left = 8
    Top = 239
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftString
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhaLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'DNTEACH.senhas.Login'
      Size = 128
    end
    object QrSenhaNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'DNTEACH.senhas.Numero'
    end
    object QrSenhaSenha: TWideStringField
      FieldName = 'Senha'
      Origin = 'DNTEACH.senhas.Senha'
      Size = 128
    end
    object QrSenhaPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'DNTEACH.senhas.Perfil'
    end
    object QrSenhaLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'DNTEACH.senhas.Lk'
    end
  end
  object QrMaster2: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM master')
    Left = 8
    Top = 195
    object QrMaster2Em: TWideStringField
      FieldName = 'Em'
      Required = True
      Size = 100
    end
    object QrMaster2CNPJ: TWideStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 30
    end
    object QrMaster2Monitorar: TSmallintField
      FieldName = 'Monitorar'
      Required = True
    end
    object QrMaster2Distorcao: TIntegerField
      FieldName = 'Distorcao'
      Required = True
    end
    object QrMaster2DataI: TDateField
      FieldName = 'DataI'
    end
    object QrMaster2DataF: TDateField
      FieldName = 'DataF'
    end
    object QrMaster2Hoje: TDateField
      FieldName = 'Hoje'
    end
    object QrMaster2Hora: TTimeField
      FieldName = 'Hora'
    end
    object QrMaster2MasLogin: TWideStringField
      FieldName = 'MasLogin'
      Required = True
      Size = 30
    end
    object QrMaster2MasSenha: TWideStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrMaster2MasAtivar: TWideStringField
      FieldName = 'MasAtivar'
      Size = 1
    end
    object QrMaster2SolicitaSenha: TSmallintField
      FieldName = 'SolicitaSenha'
    end
    object QrMaster2Limite: TSmallintField
      FieldName = 'Limite'
    end
    object QrMaster2Licenca: TWideStringField
      FieldName = 'Licenca'
      Size = 50
    end
  end
  object QrSomaM: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT (SUM(Credito) - SUM(Debito)) Valor'
      'FROM lanctos')
    Left = 448
    Top = 91
    object QrSomaMValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DBMMONEY.lanctos.Credito'
    end
  end
  object QrProduto: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Codigo, Controla'
      'FROM produtos'
      'WHERE Codigo=:P0')
    Left = 168
    Top = 275
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdutoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutoControla: TWideStringField
      FieldName = 'Controla'
      Size = 1
    end
  end
  object QrVendas: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(Custo) Valor'
      'FROM produtossits'
      'WHERE Produto=:P0')
    Left = 140
    Top = 275
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVendasPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'DNSTORE001.produtossits.Pecas'
    end
    object QrVendasValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DNSTORE001.produtossits.Total'
    end
  end
  object QrEntrada: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Pecas) Pecas, SUM(Custo) Valor'
      'FROM produtoscits'
      'WHERE Produto=:P0'
      '')
    Left = 112
    Top = 275
    ParamData = <
      item
        DataType = ftString
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntradaPecas: TFloatField
      FieldName = 'Pecas'
      Origin = 'DNSTORE001.produtoscits.Pecas'
    end
    object QrEntradaValor: TFloatField
      FieldName = 'Valor'
      Origin = 'DNSTORE001.produtoscits.Total'
    end
  end
  object QrUpdM: TmySQLQuery
    Database = MyDB
    Left = 448
    Top = 47
  end
  object QrUpdU: TmySQLQuery
    Database = MyDB
    Left = 12
    Top = 47
  end
  object QrUpdL: TmySQLQuery
    Database = MyLocDatabase
    Left = 160
    Top = 95
  end
  object QrUpdY: TmySQLQuery
    Database = MyDB
    Left = 64
    Top = 239
  end
  object QrLivreY: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Codigo FROM livres')
    Left = 60
    Top = 191
    object QrLivreYCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrCountY: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SELECT COUNT(*) Record From  Entidades')
    Left = 452
    Top = 247
    object QrCountYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrLocY: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SELECT MIN(Codigo) Record FROM carteiras'
      '')
    Left = 452
    Top = 203
    object QrLocYRecord: TIntegerField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrLocDataY: TmySQLQuery
    Database = MyDBn
    SQL.Strings = (
      'SELECT MIN(Data) Record FROM lanctos'
      '')
    Left = 320
    Top = 7
    object QrLocDataYRecord: TDateField
      FieldName = 'Record'
      Required = True
    end
  end
  object QrIdx: TmySQLQuery
    Database = MyDB
    Left = 120
    Top = 320
  end
  object QrMas: TmySQLQuery
    Database = MyDB
    Left = 148
    Top = 320
  end
  object QrUpd: TmySQLQuery
    Database = MyDB
    Left = 176
    Top = 320
  end
  object QrAux: TmySQLQuery
    Database = MyDB
    Left = 176
    Top = 364
  end
  object QrSQL: TmySQLQuery
    Database = MyDB
    Left = 120
    Top = 364
  end
  object MyDB: TmySQLDatabase
    DatabaseName = 'creditor'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=creditor'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 12
    Top = 7
  end
  object MyLocDatabase: TmySQLDatabase
    DatabaseName = 'LocCredr'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=LocCredr'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 156
    Top = 7
  end
  object QrSB4: TmySQLQuery
    Database = MyLocDatabase
    Left = 200
    Top = 143
    object QrSB4Codigo: TWideStringField
      FieldName = 'Codigo'
      Size = 255
    end
    object QrSB4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 255
    end
  end
  object DsSB4: TDataSource
    DataSet = QrSB4
    Left = 232
    Top = 143
  end
  object QrPriorNext: TmySQLQuery
    Database = MyDB
    Left = 148
    Top = 364
  end
  object QrAuxL: TmySQLQuery
    Database = MyLocDatabase
    Left = 160
    Top = 51
  end
  object QrSoma1: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(TotalC*Tipo) TOTAL, '
      'SUM(Qtde*Tipo) Qtde '
      'FROM mov'
      'WHERE Produto=:P0')
    Left = 260
    Top = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSoma1TOTAL: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'TOTAL'
    end
    object QrSoma1Qtde: TFloatField
      FieldKind = fkInternalCalc
      FieldName = 'Qtde'
    end
  end
  object QrControle: TmySQLQuery
    Database = MyDB
    AfterOpen = QrControleAfterOpen
    SQL.Strings = (
      'SELECT *, SoMaiusculas,  Moeda, Dono, '
      'UFPadrao, Cidade, IdleMinutos, '
      'CartVen, CartCom,CartDeS, CartReS,'
      'CartDeG, CartReG,CartCoE, CartCoC,'
      'CartEmD, CartEmA,ContraSenha,'
      'PaperTop, PaperLef, PaperWid, PaperHei, PaperFcl,'
      'TravaCidade, MoraDD, Multa, MensalSempre,'
      'EntraSemValor, MyPagTip, MyPagCar,'
      'CPMF, ChequeMaior, ISS, COFINS, COFINS_R, PIS, PIS_R, '
      'RegiaoCompe, TipoAdValorem, AdValoremP, AdValoremV, '
      'TipoPrazoDesc, CHValAlto, DUValAlto, CHRisco, DURisco,'
      'ImportPath, AdValMinVal, AdValMinTip, FatorCompraMin,'
      'CalcMyJuro, CartEspecie, AditAtu, CartAtu, ProtAtu, TaxasAutom,'
      'CorRecibo, EnvelopeSacado, ColoreEnvelope,'
      'MyPgParc, MyPgQtdP, MyPgPeri, MyPgDias,'
      'MeuLogoPath, LastEditLote, ContabIOF, ContabAdV, ContabFaC,'
      'LogoNF, CMC, msnID, AES_DECRYPT(msnPW, :P0) msnPW, msnCA,'
      'NFLinha1, NFLinha2, NFLinha3, NFAliq, NFLei, '
      'FTP_Pwd, FTP_Hos, FTP_Log, FTP_Lix, FTP_Min, FTP_Aut,'
      'CPMF_Padrao, ddArqMorto, NF_Cabecalho,'
      'CSD_TopoCidata, CSD_TopoDestin, CSD_TopoDuplic, CSD_TopoClient,'
      'CSD_MEsqCidata, CSD_MEsqDestin, CSD_MEsqDuplic, CSD_MEsqClient,'
      'CSD_TopoDesti2, CSD_MEsqDesti2,'
      'VendaCartPg, VendaParcPg, VendaPeriPg, VendaDiasPg,'
      'MinhaCompe, OutraCompe, CBEMinimo, SBCPadrao,'
      'PathBBRet, LiqOcoCDi, LiqOcoShw, LiqOcoCOc,'
      'Web_Page, Web_Host, Web_User, Web_Pwd, Web_DB, Web_FTPu, '
      'Web_FTPs, Web_FTPh, Web_MySQL,'
      'LiqStaAnt, LiqStaVct, LiqStaVcd, CartSacPe, ColigEsp,'
      'NewWebScan, NewWebMins,'
      'IOF_PF, IOF_PJ, IOF_ME, IOF_EX, IOF_Li,'
      'CNABCtaJur, CNABCtaMul, CNABCtaTar, '
      'MyPerJuros, MyPerMulta, LastBco, VerBcoTabs, LogoBig1,'
      'MoedaBr, TipNomeEmp'
      'FROM controle')
    Left = 8
    Top = 287
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrControleSoMaiusculas: TWideStringField
      FieldName = 'SoMaiusculas'
      Size = 1
    end
    object QrControleMoeda: TWideStringField
      FieldName = 'Moeda'
      Size = 4
    end
    object QrControleUFPadrao: TIntegerField
      FieldName = 'UFPadrao'
    end
    object QrControleCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 100
    end
    object QrControleCartVen: TIntegerField
      FieldName = 'CartVen'
    end
    object QrControleCartCom: TIntegerField
      FieldName = 'CartCom'
    end
    object QrControleCartDeS: TIntegerField
      FieldName = 'CartDeS'
    end
    object QrControleCartReS: TIntegerField
      FieldName = 'CartReS'
    end
    object QrControleCartDeG: TIntegerField
      FieldName = 'CartDeG'
    end
    object QrControleCartReG: TIntegerField
      FieldName = 'CartReG'
    end
    object QrControleCartCoE: TIntegerField
      FieldName = 'CartCoE'
    end
    object QrControleCartCoC: TIntegerField
      FieldName = 'CartCoC'
    end
    object QrControleCartEmD: TIntegerField
      FieldName = 'CartEmD'
    end
    object QrControleCartEmA: TIntegerField
      FieldName = 'CartEmA'
    end
    object QrControleContraSenha: TWideStringField
      FieldName = 'ContraSenha'
      Size = 50
    end
    object QrControlePaperTop: TIntegerField
      FieldName = 'PaperTop'
    end
    object QrControlePaperLef: TIntegerField
      FieldName = 'PaperLef'
    end
    object QrControlePaperWid: TIntegerField
      FieldName = 'PaperWid'
    end
    object QrControlePaperHei: TIntegerField
      FieldName = 'PaperHei'
    end
    object QrControlePaperFcl: TIntegerField
      FieldName = 'PaperFcl'
    end
    object QrControleTravaCidade: TSmallintField
      FieldName = 'TravaCidade'
    end
    object QrControleMoraDD: TFloatField
      FieldName = 'MoraDD'
    end
    object QrControleMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrControleMensalSempre: TIntegerField
      FieldName = 'MensalSempre'
    end
    object QrControleEntraSemValor: TIntegerField
      FieldName = 'EntraSemValor'
    end
    object QrControleMyPagTip: TIntegerField
      FieldName = 'MyPagTip'
    end
    object QrControleMyPagCar: TIntegerField
      FieldName = 'MyPagCar'
    end
    object QrControleCPMF: TFloatField
      FieldName = 'CPMF'
    end
    object QrControleChequeMaior: TFloatField
      FieldName = 'ChequeMaior'
    end
    object QrControleISS: TFloatField
      FieldName = 'ISS'
    end
    object QrControleCOFINS: TFloatField
      FieldName = 'COFINS'
    end
    object QrControleCOFINS_R: TFloatField
      FieldName = 'COFINS_R'
    end
    object QrControlePIS: TFloatField
      FieldName = 'PIS'
    end
    object QrControlePIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrControleRegiaoCompe: TIntegerField
      FieldName = 'RegiaoCompe'
    end
    object QrControleTipoAdValorem: TSmallintField
      FieldName = 'TipoAdValorem'
    end
    object QrControleAdValoremP: TFloatField
      FieldName = 'AdValoremP'
    end
    object QrControleAdValoremV: TFloatField
      FieldName = 'AdValoremV'
    end
    object QrControleTipoPrazoDesc: TSmallintField
      FieldName = 'TipoPrazoDesc'
    end
    object QrControleCHValAlto: TFloatField
      FieldName = 'CHValAlto'
    end
    object QrControleDUValAlto: TFloatField
      FieldName = 'DUValAlto'
    end
    object QrControleCHRisco: TFloatField
      FieldName = 'CHRisco'
    end
    object QrControleDURisco: TFloatField
      FieldName = 'DURisco'
    end
    object QrControleDono: TIntegerField
      FieldName = 'Dono'
    end
    object QrControleAditAtu: TIntegerField
      FieldName = 'AditAtu'
    end
    object QrControleImportPath: TWideStringField
      FieldName = 'ImportPath'
      Size = 255
    end
    object QrControleAdValMinVal: TFloatField
      FieldName = 'AdValMinVal'
    end
    object QrControleAdValMinTip: TSmallintField
      FieldName = 'AdValMinTip'
    end
    object QrControleFatorCompraMin: TFloatField
      FieldName = 'FatorCompraMin'
    end
    object QrControleCalcMyJuro: TIntegerField
      FieldName = 'CalcMyJuro'
    end
    object QrControleCartEspecie: TIntegerField
      FieldName = 'CartEspecie'
    end
    object QrControleCartAtu: TIntegerField
      FieldName = 'CartAtu'
    end
    object QrControleProtAtu: TIntegerField
      FieldName = 'ProtAtu'
    end
    object QrControleTaxasAutom: TIntegerField
      FieldName = 'TaxasAutom'
    end
    object QrControleIdleMinutos: TIntegerField
      FieldName = 'IdleMinutos'
    end
    object QrControleCorRecibo: TIntegerField
      FieldName = 'CorRecibo'
    end
    object QrControleEnvelopeSacado: TSmallintField
      FieldName = 'EnvelopeSacado'
    end
    object QrControleColoreEnvelope: TSmallintField
      FieldName = 'ColoreEnvelope'
    end
    object QrControleMyPgParc: TIntegerField
      FieldName = 'MyPgParc'
    end
    object QrControleMyPgQtdP: TIntegerField
      FieldName = 'MyPgQtdP'
    end
    object QrControleMyPgPeri: TIntegerField
      FieldName = 'MyPgPeri'
    end
    object QrControleMyPgDias: TIntegerField
      FieldName = 'MyPgDias'
    end
    object QrControleMeuLogoPath: TWideStringField
      FieldName = 'MeuLogoPath'
      Size = 255
    end
    object QrControleLastEditLote: TDateField
      FieldName = 'LastEditLote'
    end
    object QrControleContabIOF: TWideStringField
      FieldName = 'ContabIOF'
    end
    object QrControleContabAdV: TWideStringField
      FieldName = 'ContabAdV'
    end
    object QrControleContabFaC: TWideStringField
      FieldName = 'ContabFaC'
    end
    object QrControleLogoNF: TWideStringField
      FieldName = 'LogoNF'
      Size = 255
    end
    object QrControleCMC: TWideStringField
      FieldName = 'CMC'
    end
    object QrControlemsnID: TWideStringField
      FieldName = 'msnID'
      Size = 255
    end
    object QrControlemsnPW: TWideStringField
      FieldName = 'msnPW'
      Size = 255
    end
    object QrControlemsnCA: TIntegerField
      FieldName = 'msnCA'
    end
    object QrControleNFLinha1: TWideStringField
      FieldName = 'NFLinha1'
      Size = 255
    end
    object QrControleNFLinha2: TWideStringField
      FieldName = 'NFLinha2'
      Size = 255
    end
    object QrControleNFLinha3: TWideStringField
      FieldName = 'NFLinha3'
      Size = 255
    end
    object QrControleNFAliq: TWideStringField
      FieldName = 'NFAliq'
      Size = 255
    end
    object QrControleNFLei: TWideStringField
      FieldName = 'NFLei'
      Size = 255
    end
    object QrControleFTP_Pwd: TWideStringField
      FieldName = 'FTP_Pwd'
      Size = 255
    end
    object QrControleFTP_Hos: TWideStringField
      FieldName = 'FTP_Hos'
      Size = 255
    end
    object QrControleFTP_Log: TWideStringField
      FieldName = 'FTP_Log'
      Size = 255
    end
    object QrControleFTP_Lix: TWideStringField
      FieldName = 'FTP_Lix'
      Size = 255
    end
    object QrControleFTP_Min: TSmallintField
      FieldName = 'FTP_Min'
    end
    object QrControleFTP_Aut: TSmallintField
      FieldName = 'FTP_Aut'
    end
    object QrControleCPMF_Padrao: TSmallintField
      FieldName = 'CPMF_Padrao'
    end
    object QrControleddArqMorto: TIntegerField
      FieldName = 'ddArqMorto'
    end
    object QrControleNF_Cabecalho: TIntegerField
      FieldName = 'NF_Cabecalho'
    end
    object QrControleCSD_TopoCidata: TIntegerField
      FieldName = 'CSD_TopoCidata'
    end
    object QrControleCSD_TopoDestin: TIntegerField
      FieldName = 'CSD_TopoDestin'
    end
    object QrControleCSD_TopoDuplic: TIntegerField
      FieldName = 'CSD_TopoDuplic'
    end
    object QrControleCSD_TopoClient: TIntegerField
      FieldName = 'CSD_TopoClient'
    end
    object QrControleCSD_MEsqCidata: TIntegerField
      FieldName = 'CSD_MEsqCidata'
    end
    object QrControleCSD_MEsqDestin: TIntegerField
      FieldName = 'CSD_MEsqDestin'
    end
    object QrControleCSD_MEsqDuplic: TIntegerField
      FieldName = 'CSD_MEsqDuplic'
    end
    object QrControleCSD_MEsqClient: TIntegerField
      FieldName = 'CSD_MEsqClient'
    end
    object QrControleCSD_TopoDesti2: TIntegerField
      FieldName = 'CSD_TopoDesti2'
    end
    object QrControleCSD_MEsqDesti2: TIntegerField
      FieldName = 'CSD_MEsqDesti2'
    end
    object QrControleVendaCartPg: TIntegerField
      FieldName = 'VendaCartPg'
    end
    object QrControleVendaParcPg: TIntegerField
      FieldName = 'VendaParcPg'
    end
    object QrControleVendaPeriPg: TIntegerField
      FieldName = 'VendaPeriPg'
    end
    object QrControleVendaDiasPg: TIntegerField
      FieldName = 'VendaDiasPg'
    end
    object QrControleMinhaCompe: TIntegerField
      FieldName = 'MinhaCompe'
    end
    object QrControleOutraCompe: TIntegerField
      FieldName = 'OutraCompe'
    end
    object QrControleCBEMinimo: TIntegerField
      FieldName = 'CBEMinimo'
    end
    object QrControleSBCPadrao: TIntegerField
      FieldName = 'SBCPadrao'
    end
    object QrControlePathBBRet: TWideStringField
      FieldName = 'PathBBRet'
      Size = 255
    end
    object QrControleLiqOcoCDi: TSmallintField
      FieldName = 'LiqOcoCDi'
      Required = True
    end
    object QrControleLiqOcoCOc: TIntegerField
      FieldName = 'LiqOcoCOc'
      Required = True
    end
    object QrControleLiqOcoShw: TSmallintField
      FieldName = 'LiqOcoShw'
      Required = True
    end
    object QrControleWeb_Page: TWideStringField
      FieldName = 'Web_Page'
      Size = 255
    end
    object QrControleWeb_Host: TWideStringField
      FieldName = 'Web_Host'
      Size = 255
    end
    object QrControleWeb_User: TWideStringField
      FieldName = 'Web_User'
      Size = 30
    end
    object QrControleWeb_Pwd: TWideStringField
      FieldName = 'Web_Pwd'
      Size = 30
    end
    object QrControleWeb_DB: TWideStringField
      FieldName = 'Web_DB'
      Size = 50
    end
    object QrControleWeb_FTPu: TWideStringField
      FieldName = 'Web_FTPu'
      Size = 50
    end
    object QrControleWeb_FTPs: TWideStringField
      FieldName = 'Web_FTPs'
      Size = 50
    end
    object QrControleWeb_FTPh: TWideStringField
      FieldName = 'Web_FTPh'
      Size = 255
    end
    object QrControleWeb_MySQL: TSmallintField
      FieldName = 'Web_MySQL'
    end
    object QrControleLiqStaAnt: TSmallintField
      FieldName = 'LiqStaAnt'
    end
    object QrControleLiqStaVct: TSmallintField
      FieldName = 'LiqStaVct'
    end
    object QrControleLiqStaVcd: TSmallintField
      FieldName = 'LiqStaVcd'
    end
    object QrControleCartSacPe: TWideMemoField
      FieldName = 'CartSacPe'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrControleColigEsp: TIntegerField
      FieldName = 'ColigEsp'
    end
    object QrControleNewWebScan: TIntegerField
      FieldName = 'NewWebScan'
    end
    object QrControleNewWebMins: TIntegerField
      FieldName = 'NewWebMins'
    end
    object QrControleIOF_PF: TFloatField
      FieldName = 'IOF_PF'
    end
    object QrControleIOF_PJ: TFloatField
      FieldName = 'IOF_PJ'
    end
    object QrControleIOF_ME: TFloatField
      FieldName = 'IOF_ME'
    end
    object QrControleIOF_Ex: TFloatField
      FieldName = 'IOF_Ex'
    end
    object QrControleIOF_Li: TFloatField
      FieldName = 'IOF_Li'
    end
    object QrControleCNABCtaJur: TIntegerField
      FieldName = 'CNABCtaJur'
    end
    object QrControleCNABCtaMul: TIntegerField
      FieldName = 'CNABCtaMul'
    end
    object QrControleCNABCtaTar: TIntegerField
      FieldName = 'CNABCtaTar'
    end
    object QrControleMyPerJuros: TFloatField
      FieldName = 'MyPerJuros'
    end
    object QrControleMyPerMulta: TFloatField
      FieldName = 'MyPerMulta'
    end
    object QrControleLastBco: TIntegerField
      FieldName = 'LastBco'
    end
    object QrControleVerBcoTabs: TIntegerField
      FieldName = 'VerBcoTabs'
    end
    object QrControleLogoBig1: TWideStringField
      FieldName = 'LogoBig1'
      Size = 255
    end
    object QrControleMoedaBr: TIntegerField
      FieldName = 'MoedaBr'
    end
    object QrControleTipNomeEmp: TSmallintField
      FieldName = 'TipNomeEmp'
    end
    object QrControleCorrDtaVct: TIntegerField
      FieldName = 'CorrDtaVct'
    end
    object QrControleCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrControleCartCheques: TIntegerField
      FieldName = 'CartCheques'
    end
    object QrControleCartDuplic: TIntegerField
      FieldName = 'CartDuplic'
    end
    object QrControleCodCliRel: TIntegerField
      FieldName = 'CodCliRel'
    end
    object QrControleIOF_F_PJ: TFloatField
      FieldName = 'IOF_F_PJ'
    end
    object QrControleIOF_D_PJ: TFloatField
      FieldName = 'IOF_D_PJ'
    end
    object QrControleIOF_Max_Per_PJ: TFloatField
      FieldName = 'IOF_Max_Per_PJ'
    end
    object QrControleIOF_Max_Usa_PJ: TSmallintField
      FieldName = 'IOF_Max_Usa_PJ'
    end
    object QrControleIOF_F_PF: TFloatField
      FieldName = 'IOF_F_PF'
    end
    object QrControleIOF_D_PF: TFloatField
      FieldName = 'IOF_D_PF'
    end
    object QrControleIOF_Max_Per_PF: TFloatField
      FieldName = 'IOF_Max_Per_PF'
    end
    object QrControleIOF_Max_Usa_PF: TSmallintField
      FieldName = 'IOF_Max_Usa_PF'
    end
    object QrControleCartEmiCH: TIntegerField
      FieldName = 'CartEmiCH'
    end
    object QrControleKndJrs: TSmallintField
      FieldName = 'KndJrs'
    end
  end
  object QrEstoque: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pm.Tipo, SUM(pmi.Qtde) Qtde,'
      'SUM(pmi.ValorCus) ValorCus'
      'FROM produtosm pm, ProdutosMIts pmi'
      'WHERE pm.Codigo=pmi.Codigo'
      'AND pmi.Produto=:P0'
      'AND pm.DataM>=:P1'
      'GROUP BY pm.Tipo')
    Left = 396
    Top = 345
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEstoqueTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrEstoqueQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstoqueValorCus: TFloatField
      FieldName = 'ValorCus'
    end
  end
  object QrPeriodoBal: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Max(Periodo) Periodo'
      'FROM balancos')
    Left = 312
    Top = 345
    object QrPeriodoBalPeriodo: TIntegerField
      FieldKind = fkInternalCalc
      FieldName = 'Periodo'
    end
  end
  object QrMin: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Min(Periodo) Periodo'
      'FROM balancos')
    Left = 340
    Top = 345
    object QrMinPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'DBMBWET.balancos.Periodo'
    end
  end
  object QrBalancosIts: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT '
      'SUM(EstqQ) EstqQ, SUM(EstqV) EstqV,'
      'SUM(EstqQ_G) EstqQ_G, SUM(EstqV_G) EstqV_G'
      'FROM balancosits'
      'WHERE Produto=:P0'
      'AND Periodo=:P1'
      '')
    Left = 368
    Top = 345
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftInteger
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBalancosItsEstqQ: TFloatField
      FieldName = 'EstqQ'
      Origin = 'DBMBWET.balancosits.EstqQ'
    end
    object QrBalancosItsEstqV: TFloatField
      FieldName = 'EstqV'
      Origin = 'DBMBWET.balancosits.EstqV'
    end
    object QrBalancosItsEstqQ_G: TFloatField
      FieldName = 'EstqQ_G'
    end
    object QrBalancosItsEstqV_G: TFloatField
      FieldName = 'EstqV_G'
    end
  end
  object QrEstqperiodo: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pm.Tipo, SUM(pmi.Qtde) Qtde,'
      'SUM(pmi.ValorCus) ValorCus'
      'FROM produtosm pm, ProdutosMIts pmi'
      'WHERE pm.Codigo=pmi.Codigo'
      'AND pmi.Produto=:P0'
      'AND pm.DataM BETWEEN :P1 AND :P2'
      'GROUP BY pm.Tipo')
    Left = 424
    Top = 345
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEstqperiodoTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrEstqperiodoQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEstqperiodoValorCus: TFloatField
      FieldName = 'ValorCus'
    end
  end
  object QrTerminal: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM terminais'
      'WHERE IP=:P0')
    Left = 292
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerminalIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminalTerminal: TIntegerField
      FieldName = 'Terminal'
    end
  end
  object QrNTV: TmySQLQuery
    Database = MyLocDatabase
    Left = 60
    Top = 4
  end
  object QrNTI: TmySQLQuery
    Database = MyLocDatabase
    Left = 60
    Top = 48
  end
  object ZZDB: TmySQLDatabase
    DatabaseName = 'Creditor'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=Creditor'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 104
    Top = 3
  end
  object QrPerfis: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT pip.Libera, pit.Janela '
      'FROM perfisits pit'
      
        'LEFT JOIN perfisitsperf pip ON pip.Janela=pit.Janela AND pip.Cod' +
        'igo=:P0')
    Left = 300
    Top = 292
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPerfisLibera: TSmallintField
      FieldName = 'Libera'
    end
    object QrPerfisJanela: TWideStringField
      FieldName = 'Janela'
      Required = True
      Size = 100
    end
  end
  object QrTerceiro: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT ufp.Nome NOMEpUF, ufe.Nome NOMEeUF, en.* '
      'FROM entidades en'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'WHERE en.Codigo=:P0')
    Left = 220
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTerceiroNOMEpUF: TWideStringField
      FieldName = 'NOMEpUF'
      Required = True
      Size = 2
    end
    object QrTerceiroNOMEeUF: TWideStringField
      FieldName = 'NOMEeUF'
      Required = True
      Size = 2
    end
    object QrTerceiroCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTerceiroRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Required = True
      Size = 100
    end
    object QrTerceiroFantasia: TWideStringField
      FieldName = 'Fantasia'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrTerceiroRespons2: TWideStringField
      FieldName = 'Respons2'
      Required = True
      Size = 60
    end
    object QrTerceiroPai: TWideStringField
      FieldName = 'Pai'
      Required = True
      Size = 60
    end
    object QrTerceiroMae: TWideStringField
      FieldName = 'Mae'
      Required = True
      Size = 60
    end
    object QrTerceiroCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 18
    end
    object QrTerceiroIE: TWideStringField
      FieldName = 'IE'
    end
    object QrTerceiroIEST: TWideStringField
      FieldName = 'IEST'
    end
    object QrTerceiroNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrTerceiroApelido: TWideStringField
      FieldName = 'Apelido'
      Required = True
      Size = 60
    end
    object QrTerceiroCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
    object QrTerceiroRG: TWideStringField
      FieldName = 'RG'
      Size = 15
    end
    object QrTerceiroELograd: TSmallintField
      FieldName = 'ELograd'
      Required = True
    end
    object QrTerceiroERua: TWideStringField
      FieldName = 'ERua'
      Size = 30
    end
    object QrTerceiroECompl: TWideStringField
      FieldName = 'ECompl'
      Size = 30
    end
    object QrTerceiroEBairro: TWideStringField
      FieldName = 'EBairro'
      Size = 30
    end
    object QrTerceiroECidade: TWideStringField
      FieldName = 'ECidade'
      Size = 25
    end
    object QrTerceiroEUF: TSmallintField
      FieldName = 'EUF'
      Required = True
    end
    object QrTerceiroECEP: TIntegerField
      FieldName = 'ECEP'
    end
    object QrTerceiroEPais: TWideStringField
      FieldName = 'EPais'
    end
    object QrTerceiroETe1: TWideStringField
      FieldName = 'ETe1'
    end
    object QrTerceiroEte2: TWideStringField
      FieldName = 'Ete2'
    end
    object QrTerceiroEte3: TWideStringField
      FieldName = 'Ete3'
    end
    object QrTerceiroECel: TWideStringField
      FieldName = 'ECel'
    end
    object QrTerceiroEFax: TWideStringField
      FieldName = 'EFax'
    end
    object QrTerceiroEEmail: TWideStringField
      FieldName = 'EEmail'
      Size = 100
    end
    object QrTerceiroEContato: TWideStringField
      FieldName = 'EContato'
      Size = 60
    end
    object QrTerceiroENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrTerceiroPLograd: TSmallintField
      FieldName = 'PLograd'
      Required = True
    end
    object QrTerceiroPRua: TWideStringField
      FieldName = 'PRua'
      Size = 30
    end
    object QrTerceiroPCompl: TWideStringField
      FieldName = 'PCompl'
      Size = 30
    end
    object QrTerceiroPBairro: TWideStringField
      FieldName = 'PBairro'
      Size = 30
    end
    object QrTerceiroPCidade: TWideStringField
      FieldName = 'PCidade'
      Size = 25
    end
    object QrTerceiroPUF: TSmallintField
      FieldName = 'PUF'
      Required = True
    end
    object QrTerceiroPCEP: TIntegerField
      FieldName = 'PCEP'
    end
    object QrTerceiroPPais: TWideStringField
      FieldName = 'PPais'
    end
    object QrTerceiroPTe1: TWideStringField
      FieldName = 'PTe1'
    end
    object QrTerceiroPte2: TWideStringField
      FieldName = 'Pte2'
    end
    object QrTerceiroPte3: TWideStringField
      FieldName = 'Pte3'
    end
    object QrTerceiroPCel: TWideStringField
      FieldName = 'PCel'
    end
    object QrTerceiroPFax: TWideStringField
      FieldName = 'PFax'
    end
    object QrTerceiroPEmail: TWideStringField
      FieldName = 'PEmail'
      Size = 100
    end
    object QrTerceiroPContato: TWideStringField
      FieldName = 'PContato'
      Size = 60
    end
    object QrTerceiroPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrTerceiroSexo: TWideStringField
      FieldName = 'Sexo'
      Required = True
      Size = 1
    end
    object QrTerceiroResponsavel: TWideStringField
      FieldName = 'Responsavel'
      Size = 60
    end
    object QrTerceiroProfissao: TWideStringField
      FieldName = 'Profissao'
      Size = 60
    end
    object QrTerceiroCargo: TWideStringField
      FieldName = 'Cargo'
      Size = 60
    end
    object QrTerceiroRecibo: TSmallintField
      FieldName = 'Recibo'
      Required = True
    end
    object QrTerceiroDiaRecibo: TSmallintField
      FieldName = 'DiaRecibo'
      Required = True
    end
    object QrTerceiroAjudaEmpV: TFloatField
      FieldName = 'AjudaEmpV'
      Required = True
    end
    object QrTerceiroAjudaEmpP: TFloatField
      FieldName = 'AjudaEmpP'
      Required = True
    end
    object QrTerceiroCliente1: TWideStringField
      FieldName = 'Cliente1'
      Size = 1
    end
    object QrTerceiroCliente2: TWideStringField
      FieldName = 'Cliente2'
      Size = 1
    end
    object QrTerceiroFornece1: TWideStringField
      FieldName = 'Fornece1'
      Size = 1
    end
    object QrTerceiroFornece2: TWideStringField
      FieldName = 'Fornece2'
      Size = 1
    end
    object QrTerceiroFornece3: TWideStringField
      FieldName = 'Fornece3'
      Size = 1
    end
    object QrTerceiroFornece4: TWideStringField
      FieldName = 'Fornece4'
      Size = 1
    end
    object QrTerceiroTerceiro: TWideStringField
      FieldName = 'Terceiro'
      Size = 1
    end
    object QrTerceiroCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrTerceiroInformacoes: TWideStringField
      FieldName = 'Informacoes'
      Size = 255
    end
    object QrTerceiroLogo: TBlobField
      FieldName = 'Logo'
      Size = 4
    end
    object QrTerceiroVeiculo: TIntegerField
      FieldName = 'Veiculo'
    end
    object QrTerceiroMensal: TWideStringField
      FieldName = 'Mensal'
      Size = 1
    end
    object QrTerceiroObservacoes: TWideMemoField
      FieldName = 'Observacoes'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrTerceiroTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTerceiroCLograd: TSmallintField
      FieldName = 'CLograd'
      Required = True
    end
    object QrTerceiroCRua: TWideStringField
      FieldName = 'CRua'
      Size = 30
    end
    object QrTerceiroCCompl: TWideStringField
      FieldName = 'CCompl'
      Size = 30
    end
    object QrTerceiroCBairro: TWideStringField
      FieldName = 'CBairro'
      Size = 30
    end
    object QrTerceiroCCidade: TWideStringField
      FieldName = 'CCidade'
      Size = 25
    end
    object QrTerceiroCUF: TSmallintField
      FieldName = 'CUF'
      Required = True
    end
    object QrTerceiroCCEP: TIntegerField
      FieldName = 'CCEP'
    end
    object QrTerceiroCPais: TWideStringField
      FieldName = 'CPais'
    end
    object QrTerceiroCTel: TWideStringField
      FieldName = 'CTel'
    end
    object QrTerceiroCCel: TWideStringField
      FieldName = 'CCel'
    end
    object QrTerceiroCFax: TWideStringField
      FieldName = 'CFax'
    end
    object QrTerceiroCContato: TWideStringField
      FieldName = 'CContato'
      Size = 60
    end
    object QrTerceiroLLograd: TSmallintField
      FieldName = 'LLograd'
      Required = True
    end
    object QrTerceiroLRua: TWideStringField
      FieldName = 'LRua'
      Size = 30
    end
    object QrTerceiroLCompl: TWideStringField
      FieldName = 'LCompl'
      Size = 30
    end
    object QrTerceiroLBairro: TWideStringField
      FieldName = 'LBairro'
      Size = 30
    end
    object QrTerceiroLCidade: TWideStringField
      FieldName = 'LCidade'
      Size = 25
    end
    object QrTerceiroLUF: TSmallintField
      FieldName = 'LUF'
      Required = True
    end
    object QrTerceiroLCEP: TIntegerField
      FieldName = 'LCEP'
    end
    object QrTerceiroLPais: TWideStringField
      FieldName = 'LPais'
    end
    object QrTerceiroLTel: TWideStringField
      FieldName = 'LTel'
    end
    object QrTerceiroLCel: TWideStringField
      FieldName = 'LCel'
    end
    object QrTerceiroLFax: TWideStringField
      FieldName = 'LFax'
    end
    object QrTerceiroLContato: TWideStringField
      FieldName = 'LContato'
      Size = 60
    end
    object QrTerceiroComissao: TFloatField
      FieldName = 'Comissao'
    end
    object QrTerceiroSituacao: TSmallintField
      FieldName = 'Situacao'
    end
    object QrTerceiroNivel: TWideStringField
      FieldName = 'Nivel'
      Size = 1
    end
    object QrTerceiroGrupo: TIntegerField
      FieldName = 'Grupo'
    end
    object QrTerceiroAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrTerceiroLogo2: TBlobField
      FieldName = 'Logo2'
      Size = 4
    end
    object QrTerceiroConjugeNome: TWideStringField
      FieldName = 'ConjugeNome'
      Size = 35
    end
    object QrTerceiroConjugeNatal: TDateField
      FieldName = 'ConjugeNatal'
    end
    object QrTerceiroNome1: TWideStringField
      FieldName = 'Nome1'
      Size = 30
    end
    object QrTerceiroNatal1: TDateField
      FieldName = 'Natal1'
    end
    object QrTerceiroNome2: TWideStringField
      FieldName = 'Nome2'
      Size = 30
    end
    object QrTerceiroNatal2: TDateField
      FieldName = 'Natal2'
    end
    object QrTerceiroNome3: TWideStringField
      FieldName = 'Nome3'
      Size = 30
    end
    object QrTerceiroNatal3: TDateField
      FieldName = 'Natal3'
    end
    object QrTerceiroNome4: TWideStringField
      FieldName = 'Nome4'
      Size = 30
    end
    object QrTerceiroNatal4: TDateField
      FieldName = 'Natal4'
    end
    object QrTerceiroCreditosI: TIntegerField
      FieldName = 'CreditosI'
    end
    object QrTerceiroCreditosL: TIntegerField
      FieldName = 'CreditosL'
    end
    object QrTerceiroCreditosF2: TFloatField
      FieldName = 'CreditosF2'
    end
    object QrTerceiroCreditosD: TDateField
      FieldName = 'CreditosD'
    end
    object QrTerceiroCreditosU: TDateField
      FieldName = 'CreditosU'
    end
    object QrTerceiroCreditosV: TDateField
      FieldName = 'CreditosV'
    end
    object QrTerceiroMotivo: TIntegerField
      FieldName = 'Motivo'
      Required = True
    end
    object QrTerceiroQuantI1: TIntegerField
      FieldName = 'QuantI1'
    end
    object QrTerceiroQuantI2: TIntegerField
      FieldName = 'QuantI2'
    end
    object QrTerceiroQuantI3: TIntegerField
      FieldName = 'QuantI3'
    end
    object QrTerceiroQuantI4: TIntegerField
      FieldName = 'QuantI4'
    end
    object QrTerceiroQuantN1: TFloatField
      FieldName = 'QuantN1'
    end
    object QrTerceiroQuantN2: TFloatField
      FieldName = 'QuantN2'
    end
    object QrTerceiroAgenda: TWideStringField
      FieldName = 'Agenda'
      Required = True
      Size = 1
    end
    object QrTerceiroSenhaQuer: TWideStringField
      FieldName = 'SenhaQuer'
      Required = True
      Size = 1
    end
    object QrTerceiroSenha1: TWideStringField
      FieldName = 'Senha1'
      Size = 6
    end
    object QrTerceiroLimiCred: TFloatField
      FieldName = 'LimiCred'
      Required = True
    end
    object QrTerceiroDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrTerceiroCasasApliDesco: TSmallintField
      FieldName = 'CasasApliDesco'
      Required = True
    end
    object QrTerceiroTempD: TFloatField
      FieldName = 'TempD'
    end
    object QrTerceiroLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTerceiroDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTerceiroDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTerceiroUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTerceiroUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTerceiroCPF_Pai: TWideStringField
      FieldName = 'CPF_Pai'
      Size = 18
    end
    object QrTerceiroSSP: TWideStringField
      FieldName = 'SSP'
      Size = 10
    end
    object QrTerceiroCidadeNatal: TWideStringField
      FieldName = 'CidadeNatal'
      Size = 30
    end
    object QrTerceiroUFNatal: TSmallintField
      FieldName = 'UFNatal'
      Required = True
    end
    object QrTerceiroENumero: TIntegerField
      FieldName = 'ENumero'
    end
    object QrTerceiroPNumero: TIntegerField
      FieldName = 'PNumero'
    end
    object QrTerceiroCNumero: TIntegerField
      FieldName = 'CNumero'
    end
    object QrTerceiroLNumero: TIntegerField
      FieldName = 'LNumero'
    end
  end
  object QlLocal: TMySQLBatchExecute
    Action = baContinue
    Database = MyLocDatabase
    Delimiter = ';'
    Left = 368
    Top = 4
  end
  object QrAgora: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT YEAR(NOW()) ANO, MONTH(NOW()) MES,'
      'DAYOFMONTH(NOW()) DIA,'
      'HOUR(NOW()) HORA, MINUTE(NOW()) MINUTO,'
      'SECOND(NOW()) SEGUNDO, NOW() AGORA')
    Left = 60
    Top = 95
    object QrAgoraANO: TLargeintField
      FieldName = 'ANO'
    end
    object QrAgoraMES: TLargeintField
      FieldName = 'MES'
    end
    object QrAgoraDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrAgoraHORA: TLargeintField
      FieldName = 'HORA'
    end
    object QrAgoraMINUTO: TLargeintField
      FieldName = 'MINUTO'
    end
    object QrAgoraSEGUNDO: TLargeintField
      FieldName = 'SEGUNDO'
    end
  end
  object MyDB1: TmySQLDatabase
    DatabaseName = 'Cred1'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=Cred1'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 496
    Top = 19
  end
  object MyDB2: TmySQLDatabase
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 496
    Top = 67
  end
  object MyDB3: TmySQLDatabase
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 496
    Top = 115
  end
  object MyDB4: TmySQLDatabase
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 496
    Top = 163
  end
  object MyDB5: TmySQLDatabase
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 496
    Top = 215
  end
  object MyDB6: TmySQLDatabase
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 496
    Top = 263
  end
  object QrUpd_: TmySQLQuery
    Database = MyDB
    Left = 116
    Top = 187
  end
  object QrExEnti1: TmySQLQuery
    Database = MyDB1
    SQL.Strings = (
      'SELECT * FROM exenti'
      'WHERE Codigo=:P0')
    Left = 532
    Top = 20
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrExEnti1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrExEnti1Maior: TFloatField
      FieldName = 'Maior'
    end
    object QrExEnti1Menor: TFloatField
      FieldName = 'Menor'
    end
    object QrExEnti1AdVal: TFloatField
      FieldName = 'AdVal'
    end
  end
  object QrExEnti2: TmySQLQuery
    Database = MyDB2
    SQL.Strings = (
      'SELECT * FROM exenti'
      'WHERE Codigo=:P0')
    Left = 532
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrExEnti2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrExEnti2Maior: TFloatField
      FieldName = 'Maior'
    end
    object QrExEnti2Menor: TFloatField
      FieldName = 'Menor'
    end
    object QrExEnti2AdVal: TFloatField
      FieldName = 'AdVal'
    end
  end
  object QrExEnti3: TmySQLQuery
    Database = MyDB3
    SQL.Strings = (
      'SELECT * FROM exenti'
      'WHERE Codigo=:P0')
    Left = 532
    Top = 120
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object FloatField1: TFloatField
      FieldName = 'Maior'
    end
    object FloatField2: TFloatField
      FieldName = 'Menor'
    end
    object QrExEnti3AdVal: TFloatField
      FieldName = 'AdVal'
    end
  end
  object QrExEnti4: TmySQLQuery
    Database = MyDB4
    SQL.Strings = (
      'SELECT * FROM exenti'
      'WHERE Codigo=:P0')
    Left = 532
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object FloatField3: TFloatField
      FieldName = 'Maior'
    end
    object FloatField4: TFloatField
      FieldName = 'Menor'
    end
    object QrExEnti4AdVal: TFloatField
      FieldName = 'AdVal'
    end
  end
  object QrExEnti5: TmySQLQuery
    Database = MyDB5
    SQL.Strings = (
      'SELECT * FROM exenti'
      'WHERE Codigo=:P0')
    Left = 532
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object FloatField5: TFloatField
      FieldName = 'Maior'
    end
    object FloatField6: TFloatField
      FieldName = 'Menor'
    end
    object QrExEnti5AdVal: TFloatField
      FieldName = 'AdVal'
    end
  end
  object QrExEnti6: TmySQLQuery
    Database = MyDB6
    SQL.Strings = (
      'SELECT * FROM exenti'
      'WHERE Codigo=:P0')
    Left = 532
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
    end
    object FloatField7: TFloatField
      FieldName = 'Maior'
    end
    object FloatField8: TFloatField
      FieldName = 'Menor'
    end
    object QrExEnti6AdVal: TFloatField
      FieldName = 'AdVal'
    end
  end
  object QrEmLotIts: TmySQLQuery
    Database = MyDB1
    SQL.Strings = (
      'SELECT * FROM emlotits'
      'WHERE Controle=:P0')
    Left = 500
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLotItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLotItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEmLotItsTaxaPer: TFloatField
      FieldName = 'TaxaPer'
    end
    object QrEmLotItsJuroPer: TFloatField
      FieldName = 'JuroPer'
    end
    object QrEmLotItsTaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
  end
  object QrDono: TmySQLQuery
    Database = MyDB
    OnCalcFields = QrDonoCalcFields
    SQL.Strings = (
      
        'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, EContato, E' +
        'Cel,'
      'Respons1, Respons2, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEDON' +
        'O, '
      
        'CASE WHEN en.Tipo=0 THEN en.Fantasia   ELSE en.Apelido END APELI' +
        'DO, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F, '
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, '
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, '
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA, '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero+0.000     ELSE en.PNumero+0.' +
        '000 END NUMERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Logr' +
        'ad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END CEP,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,' +
        ' '
      'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX '
      'FROM controle co'
      'LEFT JOIN entidades en ON en.Codigo=co.Dono'
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd')
    Left = 12
    Top = 330
    object QrDonoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrDonoCadastro: TDateField
      FieldName = 'Cadastro'
      Origin = 'entidades.Cadastro'
    end
    object QrDonoNOMEDONO: TWideStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
    object QrDonoCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrDonoIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrDonoNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrDonoRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrDonoCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrDonoBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrDonoCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrDonoNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrDonoNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrDonoPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrDonoTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'entidades.Tipo'
    end
    object QrDonoTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrDonoFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrDonoENatal: TDateField
      FieldName = 'ENatal'
      Origin = 'entidades.ENatal'
    end
    object QrDonoPNatal: TDateField
      FieldName = 'PNatal'
      Origin = 'entidades.PNatal'
    end
    object QrDonoECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrDonoNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrDonoCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrDonoFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrDonoE_LNR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LNR'
      Size = 255
      Calculated = True
    end
    object QrDonoE_CUC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_CUC'
      Size = 255
      Calculated = True
    end
    object QrDonoEContato: TWideStringField
      FieldName = 'EContato'
      Origin = 'entidades.EContato'
      Size = 60
    end
    object QrDonoECel: TWideStringField
      FieldName = 'ECel'
      Origin = 'entidades.ECel'
    end
    object QrDonoCEL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEL_TXT'
      Size = 40
      Calculated = True
    end
    object QrDonoE_LN2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_LN2'
      Size = 255
      Calculated = True
    end
    object QrDonoAPELIDO: TWideStringField
      FieldName = 'APELIDO'
      Size = 60
    end
    object QrDonoNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrDonoRespons1: TWideStringField
      FieldName = 'Respons1'
      Origin = 'entidades.Respons1'
      Size = 60
    end
    object QrDonoRespons2: TWideStringField
      FieldName = 'Respons2'
      Origin = 'entidades.Respons2'
      Size = 60
    end
    object QrDonoLograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrDonoCEP: TFloatField
      FieldName = 'CEP'
    end
  end
  object QrEndereco: TmySQLQuery
    Database = MyDB
    OnCalcFields = QrEnderecoCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, en.Cadastro, ENatal, PNatal, Tipo, Respons1, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOMEDON' +
        'O, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CP' +
        'F, '
      'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG, '
      'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_, '
      'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA, '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero+0.000     ELSE en.PNumero+0.' +
        '000 END NUMERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIR' +
        'RO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDA' +
        'DE, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOME' +
        'LOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOME' +
        'UF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Logr' +
        'ad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END CEP,' +
        ' '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,' +
        ' '
      'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX '
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 80
    Top = 342
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEnderecoNO_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NO_TIPO_DOC'
      Calculated = True
    end
    object QrEnderecoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEnderecoCadastro: TDateField
      FieldName = 'Cadastro'
    end
    object QrEnderecoNOMEDONO: TWideStringField
      FieldName = 'NOMEDONO'
      Size = 100
    end
    object QrEnderecoCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrEnderecoIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrEnderecoNIRE_: TWideStringField
      FieldName = 'NIRE_'
      Size = 53
    end
    object QrEnderecoRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrEnderecoCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrEnderecoBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrEnderecoCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrEnderecoNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrEnderecoNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrEnderecoPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrEnderecoLograd: TLargeintField
      FieldName = 'Lograd'
    end
    object QrEnderecoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEnderecoCEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrEnderecoTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrEnderecoFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrEnderecoENatal: TDateField
      FieldName = 'ENatal'
    end
    object QrEnderecoPNatal: TDateField
      FieldName = 'PNatal'
    end
    object QrEnderecoECEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ECEP_TXT'
      Calculated = True
    end
    object QrEnderecoNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrEnderecoE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 255
      Calculated = True
    end
    object QrEnderecoCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrEnderecoFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrEnderecoTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrEnderecoNATAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NATAL_TXT'
      Size = 30
      Calculated = True
    end
    object QrEnderecoRespons1: TWideStringField
      FieldName = 'Respons1'
      Required = True
      Size = 60
    end
    object QrEnderecoE_MIN: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_MIN'
      Size = 255
      Calculated = True
    end
    object QrEnderecoNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
  end
  object QrAditiv2: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Texto FROM cartas'
      'WHERE Codigo=:P0')
    Left = 220
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAditiv2Texto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT DISTINCT ca.*, '
      'CASE WHEN ba.Codigo=0 THEN "" ELSE ba.Nome END NOMEDOBANCO, '
      
        'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ELSE fo.Nome END NOMEFOR' +
        'NECEI'
      'FROM carteiras ca'
      'LEFT JOIN carteiras ba ON ba.Codigo=ca.Banco'
      'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI'
      'WHERE ca.Codigo>0')
    Left = 568
    Top = 21
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'carteiras.Codigo'
      DisplayFormat = '000'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
      Origin = 'carteiras.Tipo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'carteiras.Nome'
      Size = 128
    end
    object QrCarteirasInicial: TFloatField
      FieldName = 'Inicial'
      Origin = 'carteiras.Inicial'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'carteiras.Banco'
    end
    object QrCarteirasID: TWideStringField
      FieldName = 'ID'
      Origin = 'carteiras.ID'
      Size = 128
    end
    object QrCarteirasFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'carteiras.Fatura'
      FixedChar = True
      Size = 128
    end
    object QrCarteirasID_Fat: TWideStringField
      FieldName = 'ID_Fat'
      Origin = 'carteiras.ID_Fat'
      Size = 128
    end
    object QrCarteirasSaldo: TFloatField
      FieldName = 'Saldo'
      Origin = 'carteiras.Saldo'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'carteiras.Lk'
    end
    object QrCarteirasNOMEDOBANCO: TWideStringField
      FieldName = 'NOMEDOBANCO'
      Origin = 'carteiras.Nome'
      FixedChar = True
      Size = 100
    end
    object QrCarteirasEmCaixa: TFloatField
      FieldName = 'EmCaixa'
      Origin = 'carteiras.EmCaixa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrCarteirasDIFERENCA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'DIFERENCA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrCarteirasFechamento: TIntegerField
      FieldName = 'Fechamento'
      Origin = 'carteiras.Fechamento'
    end
    object QrCarteirasPrazo: TSmallintField
      FieldName = 'Prazo'
      Origin = 'carteiras.Prazo'
    end
    object QrCarteirasTIPOPRAZO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOPRAZO'
      Size = 1
      Calculated = True
    end
    object QrCarteirasDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'carteiras.DataCad'
    end
    object QrCarteirasDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'carteiras.DataAlt'
    end
    object QrCarteirasUserCad: TSmallintField
      FieldName = 'UserCad'
      Origin = 'carteiras.UserCad'
    end
    object QrCarteirasUserAlt: TSmallintField
      FieldName = 'UserAlt'
      Origin = 'carteiras.UserAlt'
    end
    object QrCarteirasNOMEPAGREC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEPAGREC'
      Size = 50
      Calculated = True
    end
    object QrCarteirasPagRec: TSmallintField
      FieldName = 'PagRec'
      Origin = 'carteiras.PagRec'
    end
    object QrCarteirasDiaMesVence: TSmallintField
      FieldName = 'DiaMesVence'
    end
    object QrCarteirasExigeNumCheque: TSmallintField
      FieldName = 'ExigeNumCheque'
      Required = True
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
      Required = True
    end
    object QrCarteirasNome2: TWideStringField
      FieldName = 'Nome2'
      Required = True
      Size = 100
    end
    object QrCarteirasNOMEFORNECEI: TWideStringField
      FieldName = 'NOMEFORNECEI'
      Size = 100
    end
    object QrCarteirasTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
      Required = True
    end
    object QrCarteirasBanco1: TIntegerField
      FieldName = 'Banco1'
      DisplayFormat = '000'
    end
    object QrCarteirasAgencia1: TIntegerField
      FieldName = 'Agencia1'
      DisplayFormat = '0000'
    end
    object QrCarteirasConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCarteirasCheque1: TIntegerField
      FieldName = 'Cheque1'
      DisplayFormat = '000000'
    end
    object QrCarteirasContato1: TWideStringField
      FieldName = 'Contato1'
      Required = True
      Size = 100
    end
    object QrCarteirasAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrCarteirasContab: TWideStringField
      FieldName = 'Contab'
    end
    object QrCarteirasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 596
    Top = 21
  end
  object DsLct: TDataSource
    DataSet = QrLct
    Left = 596
    Top = 68
  end
  object QrLct: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT MOD(la.Mez, 100) Mes2, '
      '((la.Mez-MOD(la.Mez, 100)) / 100)+2000 Ano, '
      'la.*, ct.Codigo CONTA, '
      'ct.Nome NOMECONTA, sg.Nome NOMESUBGRUPO,'
      'gr.Nome NOMEGRUPO, cj.Nome NOMECONJUNTO,'
      'CASE 1 WHEN em.Tipo=0 THEN em.RazaoSocial '
      'ELSE em.Nome END NOMEEMPRESA, '
      'CASE 1 WHEN cl.Tipo=0 THEN cl.RazaoSocial '
      'ELSE cl.Nome END NOMECLIENTE,'
      'CASE 1 WHEN fo.Tipo=0 THEN fo.RazaoSocial '
      'ELSE fo.Nome END NOMEFORNECEDOR'
      'FROM lanctos la'
      
        'LEFT JOIN contas ct    ON ct.Codigo=la.Genero AND ct.Terceiro = ' +
        '0'
      'LEFT JOIN subgrupos sg ON sg.Codigo=ct.Subgrupo'
      'LEFT JOIN grupos gr    ON gr.Codigo=sg.Grupo'
      'LEFT JOIN conjuntos cj ON cj.Codigo=gr.Conjunto'
      'LEFT JOIN entidades em ON em.Codigo=ct.Empresa'
      'LEFT JOIN entidades cl ON cl.Codigo=la.Cliente'
      'LEFT JOIN entidades fo ON fo.Codigo=la.Fornecedor'
      'ORDER BY la.Data, la.Controle')
    Left = 568
    Top = 68
    object QrLctData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
    end
    object QrLctCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
    end
    object QrLctAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
      DisplayFormat = '000000; ; '
    end
    object QrLctGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrLctDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 25
    end
    object QrLctNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
      DisplayFormat = '000000; ; '
    end
    object QrLctDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
      DisplayFormat = '000000; ; '
    end
    object QrLctSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrLctVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLctLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrLctFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrLctFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrLctCONTA: TIntegerField
      FieldName = 'CONTA'
      Origin = 'contas.Codigo'
    end
    object QrLctNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      FixedChar = True
      Size = 128
    end
    object QrLctNOMEEMPRESA: TWideStringField
      FieldName = 'NOMEEMPRESA'
      Origin = 'contas.Nome'
      Size = 128
    end
    object QrLctNOMESUBGRUPO: TWideStringField
      FieldName = 'NOMESUBGRUPO'
      Origin = 'subgrupos.Nome'
      Size = 4
    end
    object QrLctNOMEGRUPO: TWideStringField
      FieldName = 'NOMEGRUPO'
      Origin = 'grupos.Nome'
      Size = 4
    end
    object QrLctNOMECONJUNTO: TWideStringField
      FieldName = 'NOMECONJUNTO'
      Origin = 'conjuntos.Nome'
      Size = 4
    end
    object QrLctNOMESIT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESIT'
      Size = 15
      Calculated = True
    end
    object QrLctAno: TFloatField
      FieldName = 'Ano'
    end
    object QrLctMENSAL: TWideStringField
      DisplayWidth = 7
      FieldKind = fkCalculated
      FieldName = 'MENSAL'
      Size = 7
      Calculated = True
    end
    object QrLctMENSAL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MENSAL2'
      Calculated = True
    end
    object QrLctBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLctLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrLctFatura: TWideStringField
      FieldName = 'Fatura'
      FixedChar = True
      Size = 128
    end
    object QrLctSub: TSmallintField
      FieldName = 'Sub'
      DisplayFormat = '00; ; '
    end
    object QrLctCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrLctLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrLctPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLctSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrLctID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrLctMez: TIntegerField
      FieldName = 'Mez'
    end
    object QrLctFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrLctcliente: TIntegerField
      FieldName = 'cliente'
    end
    object QrLctMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrLctNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      FixedChar = True
      Size = 45
    end
    object QrLctNOMEFORNECEDOR: TWideStringField
      FieldName = 'NOMEFORNECEDOR'
      FixedChar = True
      Size = 50
    end
    object QrLctTIPOEM: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TIPOEM'
      Size = 1
      Calculated = True
    end
    object QrLctNOMERELACIONADO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMERELACIONADO'
      Size = 50
      Calculated = True
    end
    object QrLctOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrLctLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrLctMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrLctATRASO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATRASO'
      Calculated = True
    end
    object QrLctJUROS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JUROS'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrLctDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrLctNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrLctVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrLctAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrLctMes2: TLargeintField
      FieldKind = fkInternalCalc
      FieldName = 'Mes2'
    end
    object QrLctProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrLctDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLctDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLctUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrLctUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrLctControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLctID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrLctCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Required = True
    end
    object QrLctFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrLctICMS_P: TFloatField
      FieldName = 'ICMS_P'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctICMS_V: TFloatField
      FieldName = 'ICMS_V'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrLctDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 10
    end
    object QrLctCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrLctCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrLctDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrLctDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrLctForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrLctQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrLctFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrTransf: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      
        'SELECT Data, Tipo, Carteira, Controle, Genero, Debito, Credito, ' +
        'Documento FROM lanctos'
      'WHERE Controle=:P0'
      '')
    Left = 104
    Top = 140
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTransfData: TDateField
      FieldName = 'Data'
    end
    object QrTransfTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrTransfCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrTransfControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrTransfGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrTransfDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrTransfCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrTransfDocumento: TFloatField
      FieldName = 'Documento'
    end
  end
  object QrAditiv3: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Texto FROM cartas'
      'WHERE Codigo=:P0')
    Left = 220
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAditiv3Texto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrAditiv4: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Texto FROM cartas'
      'WHERE Codigo=:P0')
    Left = 220
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAditiv4Texto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrEnti: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT FatorCompra '
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 164
    Top = 228
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntiFatorCompra: TFloatField
      FieldName = 'FatorCompra'
    end
  end
  object QrTerminais: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM terminais')
    Left = 292
    Top = 228
    object QrTerminaisIP: TWideStringField
      FieldName = 'IP'
      Size = 15
    end
    object QrTerminaisTerminal: TIntegerField
      FieldName = 'Terminal'
    end
    object QrTerminaisLicenca: TWideStringField
      FieldName = 'Licenca'
      Size = 50
    end
  end
  object QrSSit: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM senhas'
      'WHERE login=:P0'
      '')
    Left = 416
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object QrSenhas: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT AES_DECRYPT(se.Senha, :P0) SenhaNew, se.Senha SenhaOld, '
      'se.login, se.Numero, se.Perfil, se.Funcionario, se.IP_Default'
      'FROM senhas se'
      'WHERE se.Login=:P1')
    Left = 388
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSenhasIP_Default: TWideStringField
      FieldName = 'IP_Default'
      Size = 50
    end
    object QrSenhasSenhaNew: TWideStringField
      FieldName = 'SenhaNew'
      Size = 30
    end
    object QrSenhasSenhaOld: TWideStringField
      FieldName = 'SenhaOld'
      Size = 30
    end
    object QrSenhaslogin: TWideStringField
      FieldName = 'login'
      Required = True
      Size = 30
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrSenhasPerfil: TIntegerField
      FieldName = 'Perfil'
    end
    object QrSenhasFuncionario: TIntegerField
      FieldName = 'Funcionario'
    end
  end
  object QrBSit: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SitSenha'
      'FROM master')
    Left = 360
    Top = 412
    object QrBSitSitSenha: TSmallintField
      FieldName = 'SitSenha'
    end
  end
  object QrBoss: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT MasSenha MasZero, '
      'AES_DECRYPT(MasSenha, :P0) MasSenha, MasLogin, Em, CNPJ'
      'FROM master')
    Left = 332
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBossMasSenha: TWideStringField
      FieldName = 'MasSenha'
      Size = 30
    end
    object QrBossMasLogin: TWideStringField
      FieldName = 'MasLogin'
      Required = True
      Size = 30
    end
    object QrBossEm: TWideStringField
      FieldName = 'Em'
      Size = 100
    end
    object QrBossCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 30
    end
    object QrBossMasZero: TWideStringField
      FieldName = 'MasZero'
      Size = 30
    end
  end
  object QrLocCPF: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM emitcpf'
      'WHERE CPF=:P0'
      '')
    Left = 554
    Top = 354
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCPFCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLocCPFNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrLocCPFLimite: TFloatField
      FieldName = 'Limite'
    end
    object QrLocCPFLastAtz: TDateField
      FieldName = 'LastAtz'
    end
    object QrLocCPFAcumCHComV: TFloatField
      FieldName = 'AcumCHComV'
    end
    object QrLocCPFAcumCHComQ: TIntegerField
      FieldName = 'AcumCHComQ'
    end
    object QrLocCPFAcumCHDevV: TFloatField
      FieldName = 'AcumCHDevV'
    end
    object QrLocCPFAcumCHDevQ: TIntegerField
      FieldName = 'AcumCHDevQ'
    end
  end
  object DsLocCPF: TDataSource
    DataSet = QrLocCPF
    Left = 582
    Top = 354
  end
  object QrEmLot: TmySQLQuery
    Database = MyDB1
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 500
    Top = 356
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLotCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLotAdValorem: TFloatField
      FieldName = 'AdValorem'
    end
    object QrEmLotValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLotTaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLotIRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLotIRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLotISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLotISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLotPIS: TFloatField
      FieldName = 'PIS'
    end
    object QrEmLotPIS_Val: TFloatField
      FieldName = 'PIS_Val'
    end
    object QrEmLotPIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLotPIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLotCOFINS: TFloatField
      FieldName = 'COFINS'
    end
    object QrEmLotCOFINS_Val: TFloatField
      FieldName = 'COFINS_Val'
    end
    object QrEmLotCOFINS_R: TFloatField
      FieldName = 'COFINS_R'
    end
    object QrEmLotCOFINS_R_Val: TFloatField
      FieldName = 'COFINS_R_Val'
    end
  end
  object frxDsDono: TfrxDBDataset
    UserName = 'frxDsDono'
    CloseDataSource = False
    DataSet = QrDono
    BCDToCurrency = False
    Left = 12
    Top = 376
  end
  object frxDsEndereco: TfrxDBDataset
    UserName = 'frxDsEndereco'
    CloseDataSource = False
    DataSet = QrEndereco
    BCDToCurrency = False
    Left = 88
    Top = 384
  end
  object frxDsControle: TfrxDBDataset
    UserName = 'frxDsControle'
    CloseDataSource = False
    FieldAliases.Strings = (
      'SoMaiusculas=SoMaiusculas'
      'Moeda=Moeda'
      'UFPadrao=UFPadrao'
      'Cidade=Cidade'
      'CartVen=CartVen'
      'CartCom=CartCom'
      'CartDeS=CartDeS'
      'CartReS=CartReS'
      'CartDeG=CartDeG'
      'CartReG=CartReG'
      'CartCoE=CartCoE'
      'CartCoC=CartCoC'
      'CartEmD=CartEmD'
      'CartEmA=CartEmA'
      'ContraSenha=ContraSenha'
      'PaperTop=PaperTop'
      'PaperLef=PaperLef'
      'PaperWid=PaperWid'
      'PaperHei=PaperHei'
      'PaperFcl=PaperFcl'
      'TravaCidade=TravaCidade'
      'MoraDD=MoraDD'
      'Multa=Multa'
      'MensalSempre=MensalSempre'
      'EntraSemValor=EntraSemValor'
      'MyPagTip=MyPagTip'
      'MyPagCar=MyPagCar'
      'CPMF=CPMF'
      'ChequeMaior=ChequeMaior'
      'ISS=ISS'
      'COFINS=COFINS'
      'COFINS_R=COFINS_R'
      'PIS=PIS'
      'PIS_R=PIS_R'
      'RegiaoCompe=RegiaoCompe'
      'TipoAdValorem=TipoAdValorem'
      'AdValoremP=AdValoremP'
      'AdValoremV=AdValoremV'
      'TipoPrazoDesc=TipoPrazoDesc'
      'CHValAlto=CHValAlto'
      'DUValAlto=DUValAlto'
      'CHRisco=CHRisco'
      'DURisco=DURisco'
      'Dono=Dono'
      'AditAtu=AditAtu'
      'ImportPath=ImportPath'
      'AdValMinVal=AdValMinVal'
      'AdValMinTip=AdValMinTip'
      'FatorCompraMin=FatorCompraMin'
      'CalcMyJuro=CalcMyJuro'
      'CartEspecie=CartEspecie'
      'CartAtu=CartAtu'
      'ProtAtu=ProtAtu'
      'TaxasAutom=TaxasAutom'
      'IdleMinutos=IdleMinutos'
      'CorRecibo=CorRecibo'
      'EnvelopeSacado=EnvelopeSacado'
      'ColoreEnvelope=ColoreEnvelope'
      'MyPgParc=MyPgParc'
      'MyPgQtdP=MyPgQtdP'
      'MyPgPeri=MyPgPeri'
      'MyPgDias=MyPgDias'
      'MeuLogoPath=MeuLogoPath'
      'LastEditLote=LastEditLote'
      'ContabIOF=ContabIOF'
      'ContabAdV=ContabAdV'
      'ContabFaC=ContabFaC'
      'LogoNF=LogoNF'
      'CMC=CMC'
      'msnID=msnID'
      'msnPW=msnPW'
      'msnCA=msnCA'
      'NFLinha1=NFLinha1'
      'NFLinha2=NFLinha2'
      'NFLinha3=NFLinha3'
      'NFAliq=NFAliq'
      'NFLei=NFLei'
      'FTP_Pwd=FTP_Pwd'
      'FTP_Hos=FTP_Hos'
      'FTP_Log=FTP_Log'
      'FTP_Lix=FTP_Lix'
      'FTP_Min=FTP_Min'
      'FTP_Aut=FTP_Aut'
      'CPMF_Padrao=CPMF_Padrao'
      'ddArqMorto=ddArqMorto'
      'NF_Cabecalho=NF_Cabecalho'
      'CSD_TopoCidata=CSD_TopoCidata'
      'CSD_TopoDestin=CSD_TopoDestin'
      'CSD_TopoDuplic=CSD_TopoDuplic'
      'CSD_TopoClient=CSD_TopoClient'
      'CSD_MEsqCidata=CSD_MEsqCidata'
      'CSD_MEsqDestin=CSD_MEsqDestin'
      'CSD_MEsqDuplic=CSD_MEsqDuplic'
      'CSD_MEsqClient=CSD_MEsqClient'
      'CSD_TopoDesti2=CSD_TopoDesti2'
      'CSD_MEsqDesti2=CSD_MEsqDesti2'
      'VendaCartPg=VendaCartPg'
      'VendaParcPg=VendaParcPg'
      'VendaPeriPg=VendaPeriPg'
      'VendaDiasPg=VendaDiasPg'
      'MinhaCompe=MinhaCompe'
      'OutraCompe=OutraCompe'
      'CBEMinimo=CBEMinimo'
      'SBCPadrao=SBCPadrao'
      'PathBBRet=PathBBRet'
      'LiqOcoCDi=LiqOcoCDi'
      'LiqOcoCOc=LiqOcoCOc'
      'LiqOcoShw=LiqOcoShw'
      'Web_Page=Web_Page'
      'Web_Host=Web_Host'
      'Web_User=Web_User'
      'Web_Pwd=Web_Pwd'
      'Web_DB=Web_DB'
      'Web_FTPu=Web_FTPu'
      'Web_FTPs=Web_FTPs'
      'Web_FTPh=Web_FTPh'
      'Web_MySQL=Web_MySQL'
      'LiqStaAnt=LiqStaAnt'
      'LiqStaVct=LiqStaVct'
      'LiqStaVcd=LiqStaVcd'
      'CartSacPe=CartSacPe'
      'ColigEsp=ColigEsp'
      'NewWebScan=NewWebScan'
      'NewWebMins=NewWebMins'
      'IOF_PF=IOF_PF'
      'IOF_PJ=IOF_PJ'
      'IOF_ME=IOF_ME'
      'IOF_Ex=IOF_Ex'
      'IOF_Li=IOF_Li'
      'CNABCtaJur=CNABCtaJur'
      'CNABCtaMul=CNABCtaMul'
      'CNABCtaTar=CNABCtaTar'
      'MyPerJuros=MyPerJuros'
      'MyPerMulta=MyPerMulta'
      'LastBco=LastBco'
      'VerBcoTabs=VerBcoTabs'
      'LogoBig1=LogoBig1'
      'MoedaBr=MoedaBr'
      'TipNomeEmp=TipNomeEmp'
      'CorrDtaVct=CorrDtaVct'
      'CNPJ=CNPJ'
      'CartCheques=CartCheques'
      'CartDuplic=CartDuplic'
      'CodCliRel=CodCliRel'
      'IOF_F_PJ=IOF_F_PJ'
      'IOF_D_PJ=IOF_D_PJ'
      'IOF_Max_Per_PJ=IOF_Max_Per_PJ'
      'IOF_Max_Usa_PJ=IOF_Max_Usa_PJ'
      'IOF_F_PF=IOF_F_PF'
      'IOF_D_PF=IOF_D_PF'
      'IOF_Max_Per_PF=IOF_Max_Per_PF'
      'IOF_Max_Usa_PF=IOF_Max_Usa_PF'
      'CartEmiCH=CartEmiCH'
      'KndJrs=KndJrs')
    DataSet = QrControle
    BCDToCurrency = False
    Left = 36
    Top = 288
  end
  object QrMSNsIts: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM msnsits'
      'WHERE Codigo=:P0'
      'ORDER BY Linha')
    Left = 572
    Top = 408
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMSNsItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMSNsItsLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrMSNsItsTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrMSNsItsRecebido: TFloatField
      FieldName = 'Recebido'
    end
  end
  object QrMSNs: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT ms.*'
      'FROM msns ms'
      'WHERE ms.Codigo=:P0'
      'ORDER BY ms.Codigo')
    Left = 540
    Top = 409
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMSNsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrMSNsCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrMSNsRecebido: TFloatField
      FieldName = 'Recebido'
      Required = True
    end
    object QrMSNsVersao: TWideStringField
      FieldName = 'Versao'
      Size = 50
    end
    object QrMSNsID_Envio: TWideStringField
      FieldName = 'ID_Envio'
      Size = 50
    end
    object QrMSNsItens: TIntegerField
      FieldName = 'Itens'
      Required = True
    end
    object QrMSNsStatus: TIntegerField
      FieldName = 'Status'
      Required = True
    end
    object QrMSNsUltimo: TIntegerField
      FieldName = 'Ultimo'
      Required = True
    end
    object QrMSNsDataV: TDateField
      FieldName = 'DataV'
      Required = True
    end
    object QrMSNsNomeInfo: TWideStringField
      FieldName = 'NomeInfo'
      Size = 100
    end
  end
  object QrCliente: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT PastaTxtFTP, PastaPwdFTP'
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 180
    Top = 412
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrClientePastaTxtFTP: TWideStringField
      FieldName = 'PastaTxtFTP'
      Size = 8
    end
    object QrClientePastaPwdFTP: TWideStringField
      FieldName = 'PastaPwdFTP'
      Size = 100
    end
  end
  object MyDBn: TmySQLDatabase
    DatabaseName = 'WebCred5'
    UserName = 'root'
    UserPassword = 'wkljweryhvbirt'
    Host = '127.0.0.1'
    ConnectOptions = []
    KeepConnection = False
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30'
      'DatabaseName=WebCred5'
      'UID=root'
      'PWD=wkljweryhvbirt'
      'Host=127.0.0.1')
    DatasetOptions = []
    Left = 104
    Top = 51
  end
  object QrWeb: TmySQLQuery
    Database = MyDBn
    Left = 104
    Top = 96
  end
  object QrUpdZ: TmySQLQuery
    Database = MyDB
    Left = 448
    Top = 3
  end
  object QrAPL: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) Credito, SUM(Debito) Debito, '
      'SUM(MoraVal) MoraVal, SUM(MultaVal) MultaVal,'
      'MAX(Data) Data '
      'FROM lanctos WHERE ID_Pgto=:P0')
    Left = 584
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAPLCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrAPLDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrAPLData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrAPLMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrAPLMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
  end
  object QrVLA: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Credito, Debito '
      'FROM lanctos '
      'WHERE Sub=0 '
      'AND Controle=:P0')
    Left = 616
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVLACredito: TFloatField
      FieldName = 'Credito'
    end
    object QrVLADebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrErrQuit: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT lan.Controle Controle1, la2.Controle Controle2, '
      'la2.Data, lan.Sit, lan.Descricao'
      'FROM lanctos lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN lanctos   la2 ON la2.ID_pgto=lan.Controle'
      ''
      'WHERE lan.Tipo=2'
      'AND lan.Sit=2'
      'AND lan.Credito-lan.Pago = 0'
      'AND lan.Controle<>la2.Controle'
      'AND lan.ID_Quit=0'
      '')
    Left = 600
    Top = 160
    object QrErrQuitControle1: TIntegerField
      FieldName = 'Controle1'
      Required = True
    end
    object QrErrQuitControle2: TIntegerField
      FieldName = 'Controle2'
      Required = True
    end
    object QrErrQuitData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrErrQuitSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrErrQuitDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
  end
  object frxDsAditiv4: TfrxDBDataset
    UserName = 'frxDsAditiv4'
    CloseDataSource = False
    DataSet = QrAditiv4
    BCDToCurrency = False
    Left = 248
    Top = 292
  end
  object frxDsMaster: TfrxDBDataset
    UserName = 'frxDsMaster'
    CloseDataSource = False
    DataSet = QrMaster
    BCDToCurrency = False
    Left = 60
    Top = 144
  end
  object frxDsAditiv3: TfrxDBDataset
    UserName = 'frxDsAditiv3'
    CloseDataSource = False
    DataSet = QrAditiv3
    BCDToCurrency = False
    Left = 248
    Top = 248
  end
  object QrUpd2: TmySQLQuery
    Database = MyDB
    Left = 164
    Top = 187
  end
  object QrAditiv5: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT Texto FROM cartas'
      'WHERE Codigo=:P0')
    Left = 236
    Top = 480
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAditiv5Texto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object frxDsAditiv5: TfrxDBDataset
    UserName = 'frxDsAditiv5'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Texto=Texto')
    DataSet = QrAditiv5
    BCDToCurrency = False
    Left = 264
    Top = 480
  end
  object QrLocPg: TmySQLQuery
    Database = MyDB
    SQL.Strings = (
      'SELECT * FROM aduppgs'
      'WHERE Data=('
      'SELECT Max(Data) FROM aduppgs'
      'WHERE LotesIts=:P0)'
      'ORDER BY Controle DESC')
    Left = 696
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPgData: TDateField
      FieldName = 'Data'
    end
    object QrLocPgJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrLocPgPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLocPgLotePg: TIntegerField
      FieldName = 'LotePg'
    end
    object QrLocPgLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocPgDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocPgDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocPgUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocPgUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLocPgLotesIts: TIntegerField
      FieldName = 'LotesIts'
    end
    object QrLocPgControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocPgDesco: TFloatField
      FieldName = 'Desco'
    end
  end
end
