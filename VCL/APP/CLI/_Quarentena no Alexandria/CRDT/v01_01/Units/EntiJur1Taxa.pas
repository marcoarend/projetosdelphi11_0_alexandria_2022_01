unit EntiJur1Taxa;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, Variants,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmEntiJur1Taxa = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    BtOK: TBitBtn;
    Panel2: TPanel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    PainelTaxas: TPanel;
    Label28: TLabel;
    Label35: TLabel;
    EdTaxa: TdmkEditCB;
    CBTaxa: TdmkDBLookupComboBox;
    EdValor: TdmkEdit;
    QrTaxas: TmySQLQuery;
    QrTaxasCodigo: TIntegerField;
    QrTaxasNome: TWideStringField;
    QrTaxasGenero: TSmallintField;
    QrTaxasForma: TSmallintField;
    QrTaxasMostra: TSmallintField;
    QrTaxasBase: TSmallintField;
    QrTaxasAutomatico: TSmallintField;
    QrTaxasValor: TFloatField;
    QrTaxasLk: TIntegerField;
    QrTaxasDataCad: TDateField;
    QrTaxasDataAlt: TDateField;
    QrTaxasUserCad: TIntegerField;
    QrTaxasUserAlt: TIntegerField;
    DsTaxas: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrTaxasAfterScroll(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEntiJur1Taxa: TFmEntiJur1Taxa;

implementation

uses Module, UnMyObjects, EntiJur1, UMySQLModule;

{$R *.DFM}

procedure TFmEntiJur1Taxa.BtOKClick(Sender: TObject);
var
  Taxa, Controle: Integer;
  Valor: Double;
begin
  if not MLAGeral.SQLValI(EdTaxa.Text, False, True, Taxa) then Exit;
  if not MLAGeral.SQLValD(EdValor.Text, False, True, Valor) then Exit;
  //
  Dmod.QrUpd.SQL.Clear;
  if ImgTipo.SQLType = stIns then
  begin
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'TaxasCli',
      'TaxasCli', 'Controle');
    Dmod.QrUpd.SQL.Add('INSERT INTO taxascli SET ');
  end else begin
    Controle := FmEntiJur1.QrTaxasCliControle.Value;
    Dmod.QrUpd.SQL.Add('UPDATE taxascli SET ');
  end;
  Dmod.QrUpd.SQL.Add('Cliente=:P0, Taxa=:P1, Valor=:P2 ');
  if ImgTipo.SQLType = stIns then
    Dmod.QrUpd.SQL.Add(', Controle=:Pa ')
  else
    Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa ');
  //
  Dmod.QrUpd.Params[00].AsInteger := FmEntiJur1.QrEntidadesCodigo.Value;
  Dmod.QrUpd.Params[01].AsInteger := Taxa;
  Dmod.QrUpd.Params[02].AsFloat   := Valor;
  //
  Dmod.QrUpd.Params[03].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  FmEntiJur1.FTaxaCli := Controle;
  if ImgTipo.SQLType = stIns then
  begin
    Geral.MensagemBox('Taxa incluida com sucesso!',
    'Avisa', MB_OK+MB_ICONWARNING);
    //
    FmEntiJur1.ReopenTaxasCli();
    EdTaxa.ValueVariant := 0;
    CBTaxa.KeyValue := Null;
    EdValor.ValueVariant := 0;
    //
    EdTaxa.SetFocus;
  end else
    Close;
end;

procedure TFmEntiJur1Taxa.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiJur1Taxa.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmEntiJur1Taxa.FormCreate(Sender: TObject);
begin
  QrTaxas.Open;
end;

procedure TFmEntiJur1Taxa.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiJur1Taxa.QrTaxasAfterScroll(DataSet: TDataSet);
begin
  if ImgTipo.SQLType = stIns then
    EdValor.ValueVariant := QrTaxasValor.Value;
end;

end.
