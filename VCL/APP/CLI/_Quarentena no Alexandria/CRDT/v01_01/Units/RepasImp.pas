unit RepasImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, ComCtrls,
  frxClass, frxDBSet, Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB;

type
  TFmRepasImp = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtImprime: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Label3: TLabel;
    EdColigado: TdmkEditCB;
    CBColigado: TdmkDBLookupComboBox;
    QrColigados: TmySQLQuery;
    DsColigados: TDataSource;
    QrRepasIts: TmySQLQuery;
    QrColigadosNOMECOLIGADO: TWideStringField;
    QrColigadosCodigo: TIntegerField;
    QrRepasItsNOMECOLIGADO: TWideStringField;
    QrRepasItsColigado: TIntegerField;
    QrRepasItsBanco: TIntegerField;
    QrRepasItsAgencia: TIntegerField;
    QrRepasItsConta: TWideStringField;
    QrRepasItsCheque: TIntegerField;
    QrRepasItsEmitente: TWideStringField;
    QrRepasItsCPF: TWideStringField;
    QrRepasItsDDeposito: TDateField;
    QrRepasItsVencto: TDateField;
    QrRepasItsLIQUIDO: TFloatField;
    QrRepasItsCodigo: TIntegerField;
    QrRepasItsControle: TIntegerField;
    QrRepasItsOrigem: TIntegerField;
    QrRepasItsDias: TIntegerField;
    QrRepasItsValor: TFloatField;
    QrRepasItsTaxa: TFloatField;
    QrRepasItsJurosP: TFloatField;
    QrRepasItsJurosV: TFloatField;
    QrRepasItsLk: TIntegerField;
    QrRepasItsDataCad: TDateField;
    QrRepasItsDataAlt: TDateField;
    QrRepasItsUserCad: TIntegerField;
    QrRepasItsUserAlt: TIntegerField;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem4: TRadioGroup;
    RGAgrupa: TRadioGroup;
    QrRepasItsCPF_TXT: TWideStringField;
    QrRepasItsDDeposito_TXT: TWideStringField;
    QrRepasItsValor_TXT: TWideStringField;
    QrRepasItsCONTAGEM: TIntegerField;
    QrRepasItsMESD_TXT: TWideStringField;
    RGRepasses: TRadioGroup;
    QrRepasItsDOCUMENTO: TWideStringField;
    QrRepasItsTipo: TSmallintField;
    QrRepasItsDuplicata: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Label34: TLabel;
    TPIniD: TDateTimePicker;
    TPFimD: TDateTimePicker;
    Label1: TLabel;
    Label2: TLabel;
    TPIniR: TDateTimePicker;
    TPFimR: TDateTimePicker;
    Label4: TLabel;
    QrRepasItsDATA_REP: TDateField;
    QrRepasItsDATA_REP_TXT: TWideStringField;
    QrRepasItsMESR_TXT: TWideStringField;
    QrRepasItsPERIODOD: TFloatField;
    QrRepasItsPERIODOR: TFloatField;
    QrRepasItsPERIODO: TFloatField;
    QrRepasItsDATADEF_TXT: TWideStringField;
    RGTipo: TRadioGroup;
    QrRepasItsVALDIAS: TFloatField;
    RGMediaDias: TRadioGroup;
    QrRepasItslote: TIntegerField;
    QrRepasItsNOMECLIENTE: TWideStringField;
    frxRepasCH: TfrxReport;
    frxDsRepasIts: TfrxDBDataset;
    frxRepasDU: TfrxReport;
    frxRepasCD_Anal: TfrxReport;
    frxRepasCD_Sint: TfrxReport;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrRepasItsCalcFields(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PageControl1Change(Sender: TObject);
    procedure frxRepasCHGetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
    procedure ImprimeDepositos;
    procedure ImprimeRepasses;
    procedure MostraRelatorio;
  public
    { Public declarations }
  end;

  var
  FmRepasImp: TFmRepasImp;

implementation

{$R *.DFM}

uses Module, UnInternalConsts, UnMyObjects;

procedure TFmRepasImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRepasImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmRepasImp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmRepasImp.FormCreate(Sender: TObject);
var
  Cam: String;
begin
  Cam := Application.Title + '\XYConfig\RepasImp';
  RGOrdem1.ItemIndex := Geral.ReadAppKeyLM('Ordem1', Cam, ktInteger, 0);
  RGOrdem2.ItemIndex := Geral.ReadAppKeyLM('Ordem2', Cam, ktInteger, 2);
  RGOrdem3.ItemIndex := Geral.ReadAppKeyLM('Ordem3', Cam, ktInteger, 1);
  RGOrdem4.ItemIndex := Geral.ReadAppKeyLM('Ordem4', Cam, ktInteger, 3);
  RGAgrupa.ItemIndex := Geral.ReadAppKeyLM('Agrupa', Cam, ktInteger, 0);
  //
  QrColigados.Open;
  TPIniD.Date := Date;
  TPFimD.Date := Date + 365;
  //
  TPIniR.Date := Date - 30;
  TPFimR.Date := Date;
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmRepasImp.BtImprimeClick(Sender: TObject);
begin
  case PageControl1.ActivePageindex of
    0: ImprimeDepositos;
    1: ImprimeRepasses;
  end;
end;

procedure TFmRepasImp.ImprimeDepositos;
const
  ItensOrdem: array[0..5] of String = ('NOMECOLIGADO', 'DDeposito', 'PERIODOD',
  'Emitente', 'CPF', 'lo.Cliente, lo.Lote, li.Codigo');
var
  Coligado: Integer;
begin
  Coligado := Geral.IMV(EdColigado.Text);
  //
  QrRepasIts.Close;
  QrRepasIts.SQL.Clear;
  QrRepasIts.SQL.Add('SELECT MONTH(re.Data)+YEAR(re.Data)*100+0.00 PERIODOR,');
  QrRepasIts.SQL.Add('MONTH(li.DDeposito)+YEAR(li.DDeposito)*100+0.00 PERIODOD,');
  QrRepasIts.SQL.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial');
  QrRepasIts.SQL.Add('ELSE cl.Nome END NOMECLIENTE, ');
  QrRepasIts.SQL.Add('CASE WHEN co.Tipo=0 THEN co.RazaoSocial');
  QrRepasIts.SQL.Add('ELSE co.Nome END NOMECOLIGADO, re.Coligado, re.Data DATA_REP, ');
  QrRepasIts.SQL.Add('li.Banco, li.Agencia, li.Conta, li.Cheque, li.Duplicata,');
  QrRepasIts.SQL.Add('li.Emitente, li.CPF, li.DDeposito, li.Vencto,');
  QrRepasIts.SQL.Add('(ri.Valor - ri.JurosV) LIQUIDO, lo.Tipo, lo.Lote, lo.Codigo, ri.*');
  QrRepasIts.SQL.Add('FROM repasits ri');
  QrRepasIts.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  QrRepasIts.SQL.Add('LEFT JOIN lotesits  li ON li.Controle=ri.Origem');
  QrRepasIts.SQL.Add('LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo');
  QrRepasIts.SQL.Add('LEFT JOIN entidades co ON co.Codigo=re.Coligado');
  QrRepasIts.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  QrRepasIts.SQL.Add('WHERE li.DDeposito BETWEEN :P0 AND :P1');
  if Coligado <> 0 then QrRepasIts.SQL.Add('AND re.Coligado='+IntToStr(Coligado));
  case RGRepasses.ItemIndex of
    0: QrRepasIts.SQL.Add('AND lo.Tipo = 0');
    1: QrRepasIts.SQL.Add('AND lo.Tipo = 1');
    2: ;
  end;
  QrRepasIts.SQL.Add('ORDER BY '+
    ItensOrdem[RGOrdem1.ItemIndex]+', '+
    ItensOrdem[RGOrdem2.ItemIndex]+', '+
    ItensOrdem[RGOrdem3.ItemIndex]+', '+
    ItensOrdem[RGOrdem4.ItemIndex]);
  QrRepasIts.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIniD.Date);
  QrRepasIts.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFimD.Date);
  QrRepasIts.Open;
  //
  MostraRelatorio;
end;

procedure TFmRepasImp.ImprimeRepasses;
const
  ItensRepas: array[0..5] of String = ('NOMECOLIGADO', 're.Data', 'PERIODOR',
  'Emitente', 'CPF', 'lo.Cliente, lo.Lote, li.Codigo');
var
  Coligado: Integer;
begin
  Coligado := Geral.IMV(EdColigado.Text);
  //
  QrRepasIts.Close;
  QrRepasIts.SQL.Clear;
  QrRepasIts.SQL.Add('SELECT MONTH(re.Data)+YEAR(re.Data)*100+0.00 PERIODOR,');
  QrRepasIts.SQL.Add('MONTH(li.DDeposito)+YEAR(li.DDeposito)*100+0.00 PERIODOD,');
  QrRepasIts.SQL.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial');
  QrRepasIts.SQL.Add('ELSE cl.Nome END NOMECLIENTE, ');
  QrRepasIts.SQL.Add('CASE WHEN co.Tipo=0 THEN co.RazaoSocial');
  QrRepasIts.SQL.Add('ELSE co.Nome END NOMECOLIGADO, re.Coligado, re.Data DATA_REP, ');
  QrRepasIts.SQL.Add('li.Banco, li.Agencia, li.Conta, li.Cheque, li.Duplicata,');
  QrRepasIts.SQL.Add('li.Emitente, li.CPF, li.DDeposito, li.Vencto,');
  QrRepasIts.SQL.Add('(ri.Valor - ri.JurosV) LIQUIDO, lo.Tipo, lo.Lote, lo.Codigo, ri.*');
  QrRepasIts.SQL.Add('FROM repasits ri');
  QrRepasIts.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  QrRepasIts.SQL.Add('LEFT JOIN lotesits  li ON li.Controle=ri.Origem');
  QrRepasIts.SQL.Add('LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo');
  QrRepasIts.SQL.Add('LEFT JOIN entidades co ON co.Codigo=re.Coligado');
  QrRepasIts.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  QrRepasIts.SQL.Add('WHERE re.Data BETWEEN :P0 AND :P1');
  if Coligado <> 0 then QrRepasIts.SQL.Add('AND re.Coligado='+IntToStr(Coligado));
  case RGRepasses.ItemIndex of
    0: QrRepasIts.SQL.Add('AND lo.Tipo = 0');
    1: QrRepasIts.SQL.Add('AND lo.Tipo = 1');
    2: ;
  end;
  QrRepasIts.SQL.Add('ORDER BY '+
    ItensRepas[RGOrdem1.ItemIndex]+', '+
    ItensRepas[RGOrdem2.ItemIndex]+', '+
    ItensRepas[RGOrdem3.ItemIndex]+', '+
    ItensRepas[RGOrdem4.ItemIndex]);
  QrRepasIts.Params[00].AsString := FormatDateTime(VAR_FORMATDATE, TPIniR.Date);
  QrRepasIts.Params[01].AsString := FormatDateTime(VAR_FORMATDATE, TPFimR.Date);
  QrRepasIts.Open;
  //
  MostraRelatorio;
end;

procedure TFmRepasImp.MostraRelatorio;
begin
  if RGTipo.ItemIndex = 0 then
  begin
    case RGRepasses.ItemIndex of
      0: MyObjects.frxMostra(frxRepasCH, 'Repasse de cheques');
      1: MyObjects.frxMostra(frxRepasDU, 'Repasse de duplicatas');
      2: MyObjects.frxMostra(frxRepasCD_Anal, 'Repasse de documentos (anal�tico)');
    end;
  end else
    MyObjects.frxMostra(frxRepasCD_Sint, 'Repasse de documentos (sint�tico)');
end;

procedure TFmRepasImp.QrRepasItsCalcFields(DataSet: TDataSet);
var
  Ano, Mes: Integer;
begin
  QrRepasItsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrRepasItsCPF.Value);
  //
  QrRepasItsDDeposito_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE3, QrRepasItsDDeposito.Value);
  QrRepasItsDATA_REP_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE3, QrRepasItsDATA_REP.Value);
  QrRepasItsValor_TXT.Value :=
    FormatFloat('#,###,##0.00', QrRepasItsValor.Value);
  //
  QrRepasItsCONTAGEM.Value := 1;
  //
  Ano := Trunc(QrRepasItsPERIODOD.Value) div 100;
  Mes := Trunc(QrRepasItsPERIODOD.Value) mod 100;
  QrRepasItsMESD_TXT.Value := FormatDateTime(VAR_FORMATDATE7,
    EncodeDate(Ano, Mes, 1));
  //
  Ano := Trunc(QrRepasItsPERIODOR.Value) div 100;
  Mes := Trunc(QrRepasItsPERIODOR.Value) mod 100;
  QrRepasItsMESR_TXT.Value := FormatDateTime(VAR_FORMATDATE7,
    EncodeDate(Ano, Mes, 1));
  //
  if PageControl1.ActivePageIndex = 0 then
  begin
     QrRepasItsPERIODO.Value := QrRepasItsPERIODOD.Value;
     QrRepasItsDATADEF_TXT.Value := QrRepasItsDDeposito_TXT.Value;
  end else begin
     QrRepasItsPERIODO.Value := QrRepasItsPERIODOR.Value;
     QrRepasItsDATADEF_TXT.Value := QrRepasItsDATA_REP_TXT.Value;
  end;
  //
  if QrRepasItsTipo.Value = 0 then QrRepasItsDOCUMENTO.Value := 'C '+
    FormatFloat('000', QrRepasItsBanco.Value)+'/'+FormatFloat('0000',
    QrRepasItsAgencia.Value)+'  '+QrRepasItsConta.Value+' - '+
    FormatFloat('000000', QrRepasItsCheque.Value)
  else QrRepasItsDOCUMENTO.Value := 'D ' +
    FormatFloat('000', QrRepasItsBanco.Value)+'/'+FormatFloat('0000',
    QrRepasItsAgencia.Value)+' - '+QrRepasItsDuplicata.Value;
  //
  QrRepasItsVALDIAS.Value := QrRepasItsDias.Value * QrRepasItsValor.Value;  
end;

procedure TFmRepasImp.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Cam: String;
begin
  Cam := Application.Title + '\XYConfig\RepasImp';
  //
  Geral.WriteAppKeyLM2('Ordem1', Cam, RGOrdem1.ItemIndex, ktInteger);
  Geral.WriteAppKeyLM2('Ordem2', Cam, RGOrdem2.ItemIndex, ktInteger);
  Geral.WriteAppKeyLM2('Ordem3', Cam, RGOrdem3.ItemIndex, ktInteger);
  Geral.WriteAppKeyLM2('Ordem4', Cam, RGOrdem4.ItemIndex, ktInteger);
  Geral.WriteAppKeyLM2('Agrupa', Cam, RGAgrupa.ItemIndex, ktInteger);
end;

procedure TFmRepasImp.PageControl1Change(Sender: TObject);
var
  i: Integer;
  r: TRadioGroup;
begin
  for i := 1 to 4 do
  begin
    case i of
      1: r := RGOrdem1;
      2: r := RGOrdem2;
      3: r := RGOrdem3;
      4: r := RGOrdem4;
      else r := nil;
    end;
    if r <> nil then
    begin
      r.Items[1] := 'Dia '+Copy(PageControl1.ActivePage.Caption, 1, Length(PageControl1.ActivePage.Caption)-1);
      r.Items[2] := 'M�s '+Copy(PageControl1.ActivePage.Caption, 1, Length(PageControl1.ActivePage.Caption)-1);
    end;
  end;
end;

procedure TFmRepasImp.frxRepasCHGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_COLIGADO' then
  begin
    if CBColigado.KeyValue = NULL then Value := ' ' else
    Value := CBColigado.Text;
  end else if VarName = 'VARF_TODOS' then
  begin
    if CBColigado.KeyValue = NULL then Value := 'TODOS COLIGADOS' else
    Value := ' ';
  end else if VarName = 'VARF_PERIODO_D' then Value :=
    FormatDateTime(VAR_FORMATDATE3, TPIniD.Date) + CO_ATE +
    FormatDateTime(VAR_FORMATDATE3, TPFimD.Date)
  else if VarName = 'VARF_PERIODO_R' then Value :=
    FormatDateTime(VAR_FORMATDATE3, TPIniR.Date) + CO_ATE +
    FormatDateTime(VAR_FORMATDATE3, TPFimR.Date)
  else if VarName = 'VARF_QTD_CHEQUES' then Value := QrRepasIts.RecordCount
  else if VarName = 'VFR_LA1NOME' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Coligado: '+QrRepasItsNOMECOLIGADO.Value;
      1:
      begin
        case PageControl1.ActivePageIndex of
          0: Value := 'Dep�sito no dia  '+QrRepasItsDDeposito_TXT.Value;
          1: Value := 'Repasse no dia  '+QrRepasItsDATA_REP_TXT.Value;
        end;
      end;
      2:
      begin
        case PageControl1.ActivePageIndex of
          0: Value := 'Dep�sito no m�s de '+QrRepasItsMESD_TXT.Value;
          1: Value := 'Repasse no m�s de '+QrRepasItsMESR_TXT.Value;
        end;
      end;
      3: Value := 'Emitido por: '+QrRepasItsEmitente.Value;
      4: Value := 'CPF/CNPJ: '+QrRepasItsCPF_TXT.Value;
      5: Value := 'Border� n� '+ FormatFloat('000000', QrRepasItsCodigo.Value) +
                     ' - Lote '+FormatFloat('000000', QrRepasItslote.Value) +
                     ': '+QrRepasItsNOMECLIENTE.Value;
    end;
  end
  else if VarName = 'VFR_LA2NOME' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Coligado: '+QrRepasItsNOMECOLIGADO.Value;
      1:
      begin
        case PageControl1.ActivePageIndex of
          0: Value := 'Dep�sito no dia  '+QrRepasItsDDeposito_TXT.Value;
          1: Value := 'Repasse no dia  '+QrRepasItsDATA_REP_TXT.Value;
        end;
      end;
      2:
      begin
        case PageControl1.ActivePageIndex of
          0: Value := 'Dep�sito no m�s de '+QrRepasItsMESD_TXT.Value;
          1: Value := 'Repasse no m�s de '+QrRepasItsMESR_TXT.Value;
        end;
      end;
      3: Value := 'Emitido por: '+QrRepasItsEmitente.Value;
      4: Value := 'CPF/CNPJ: '+QrRepasItsCPF_TXT.Value;
      5: Value := 'Border� n� '+ FormatFloat('000000', QrRepasItsCodigo.Value) +
                     ' - Lote '+FormatFloat('000000', QrRepasItslote.Value) +
                     ': '+QrRepasItsNOMECLIENTE.Value;
    end;
  end
  else if VarName = 'VFR_LA3NOME' then
  begin
    case RGOrdem3.ItemIndex of
      0: Value := 'Coligado: '+QrRepasItsNOMECOLIGADO.Value;
      1:
      begin
        case PageControl1.ActivePageIndex of
          0: Value := 'Dep�sito no dia  '+QrRepasItsDDeposito_TXT.Value;
          1: Value := 'Repasse no dia  '+QrRepasItsDATA_REP_TXT.Value;
        end;
      end;
      2:
      begin
        case PageControl1.ActivePageIndex of
          0: Value := 'Dep�sito no m�s de '+QrRepasItsMESD_TXT.Value;
          1: Value := 'Repasse no m�s de '+QrRepasItsMESR_TXT.Value;
        end;
      end;
      3: Value := 'Emitido por: '+QrRepasItsEmitente.Value;
      4: Value := 'CPF/CNPJ: '+QrRepasItsCPF_TXT.Value;
      5: Value := 'Border� n� '+ FormatFloat('000000', QrRepasItsCodigo.Value) +
                     ' - Lote '+FormatFloat('000000', QrRepasItslote.Value) +
                     ': '+QrRepasItsNOMECLIENTE.Value;
    end;
  end else if VarName = 'TITULO' then
  begin
    case PageControl1.ActivePageIndex of
      0:
      begin
        case RGRepasses.ItemIndex of
          0: Value := 'Dep�sitos de Cheques Repassados';
          1: Value := 'Resgate de Duplicatas Repassadas';
          2: Value := 'Dep�sitos de Cheques e Resgate de Duplicatas Repassadas';
          else Value := '???????';
        end;
      end;
      1:
      begin
        case RGRepasses.ItemIndex of
          0: Value := 'Repasse de Cheques';
          1: Value := 'Repasse de Duplicatas';
          2: Value := 'Repasse de Cheques e Duplicatas';
          else Value := '???????';
        end;
      end;
      else Value := '???????';
    end;
  end


  // user function

  else if VarName = 'VFR_ORD1' then
  begin
    if RGAgrupa.ItemIndex < 1 then Value := 0 else
    Value := RGOrdem1.ItemIndex + 1;
  end else
  if VarName = 'VFR_ORD2' then
  begin
    if RGAgrupa.ItemIndex < 2 then Value := 0 else
    Value := RGOrdem2.ItemIndex + 1;
  end else
  if VarName = 'VFR_ORD3' then
  begin
    if RGAgrupa.ItemIndex < 3 then Value := 0 else
    Value := RGOrdem3.ItemIndex + 1;
  end else
  if VarName = 'VARF_SINTETICO' then
  begin
    Value := False;
  end else
  if VarName = 'VARF_AGRUPA' then
  begin
    Value := RGAgrupa.ItemIndex;
  end
  else if VarName = 'VARF_VERDIAS' then
  begin
    Value := Geral.IntToBool_0(RGMediaDias.ItemIndex);
  end
  else if VarName = 'VARF_LAGURA_MEMO' then
  begin
    //Value := 0;
    {if RGMediaDias.ItemIndex = 1 then
    begin
      a := StrToInt(frParser.Calc(p1));
      b := StrToInt(frParser.Calc(p2));
      Value := b-a;
    end else begin
      a := StrToInt(frParser.Calc(p1));
      b := StrToInt(frParser.Calc(p2));
      c := StrToInt(frParser.Calc(p3));
      Value := b-a+c;
    end;
    }
  end
end;

end.

