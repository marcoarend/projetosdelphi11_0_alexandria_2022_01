unit NFs;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  OleCtrls, SHDocVw, Grids, DBGrids, Menus, frxClass, frxDBSet, dmkGeral,
  dmkEdit, UnDmkProcFunc, UnDmkEnums;

type
  TFmNFs = class(TForm)
    PainelDados: TPanel;
    DsNFs: TDataSource;
    QrNFs: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BTNF: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TDMKEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    OpenDialog1: TOpenDialog;
    QrNFsCodigo: TIntegerField;
    QrNFsLk: TIntegerField;
    QrNFsDataCad: TDateField;
    QrNFsDataAlt: TDateField;
    QrNFsUserCad: TIntegerField;
    QrNFsUserAlt: TIntegerField;
    BtEuro: TBitBtn;
    DBGrid1: TDBGrid;
    QrLotes: TmySQLQuery;
    QrLotesCodigo: TIntegerField;
    QrLotesTipo: TSmallintField;
    QrLotesCliente: TIntegerField;
    QrLotesLote: TSmallintField;
    QrLotesData: TDateField;
    QrLotesTotal: TFloatField;
    QrLotesDias: TFloatField;
    QrLotesPeCompra: TFloatField;
    QrLotesTxCompra: TFloatField;
    QrLotesAdValorem: TFloatField;
    QrLotesIOC: TFloatField;
    QrLotesCPMF: TFloatField;
    QrLotesLk: TIntegerField;
    QrLotesDataCad: TDateField;
    QrLotesDataAlt: TDateField;
    QrLotesUserCad: TIntegerField;
    QrLotesUserAlt: TIntegerField;
    QrLotesNOMECLIENTE: TWideStringField;
    QrLotesTipoAdV: TIntegerField;
    QrLotesValValorem: TFloatField;
    QrLotesSpread: TSmallintField;
    QrLotesIOC_VAL: TFloatField;
    QrLotesCPMF_VAL: TFloatField;
    QrLotesISS: TFloatField;
    QrLotesISS_Val: TFloatField;
    QrLotesPIS_R: TFloatField;
    QrLotesPIS_R_Val: TFloatField;
    QrLotesIRRF: TFloatField;
    QrLotesIRRF_Val: TFloatField;
    QrLotesCNPJCPF: TWideStringField;
    QrLotesTarifas: TFloatField;
    QrLotesOcorP: TFloatField;
    QrLotesCOFINS_R: TFloatField;
    QrLotesCOFINS_R_Val: TFloatField;
    QrLotesPIS: TFloatField;
    QrLotesCOFINS: TFloatField;
    QrLotesPIS_Val: TFloatField;
    QrLotesCOFINS_Val: TFloatField;
    QrLotesMaxVencto: TDateField;
    QrLotesCHDevPg: TFloatField;
    QrLotesMINTC: TFloatField;
    QrLotesMINAV: TFloatField;
    QrLotesMINTC_AM: TFloatField;
    QrLotesPIS_T_Val: TFloatField;
    QrLotesCOFINS_T_Val: TFloatField;
    QrLotesPgLiq: TFloatField;
    QrLotesDUDevPg: TFloatField;
    DsLotes: TDataSource;
    QrSUM: TmySQLQuery;
    QrSUMMINAV: TFloatField;
    QrSUMISS_Val: TFloatField;
    QrNFsAdValor: TFloatField;
    QrNFsISS_Val: TFloatField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    PMBordero: TPopupMenu;
    Adiona1: TMenuItem;
    Retira1: TMenuItem;
    QrMax: TmySQLQuery;
    QrMaxCodigo: TIntegerField;
    frxNF_Dados: TfrxReport;
    frxDsNF: TfrxDBDataset;
    PMNF: TPopupMenu;
    IncluinovaNF1: TMenuItem;
    AlteraNFatual1: TMenuItem;
    ExcluiNFatual1: TMenuItem;
    frxNF_Tudo: TfrxReport;
    QrLotesNF: TIntegerField;
    QrLotesItens: TIntegerField;
    QrLotesConferido: TIntegerField;
    QrLotesECartaSac: TSmallintField;
    QrLotesCBE: TIntegerField;
    QrLotesSCB: TIntegerField;
    QrLotesAllQuit: TSmallintField;
    QrLotesSobraIni: TFloatField;
    QrLotesSobraNow: TFloatField;
    QrLotesWebCod: TIntegerField;
    QrLotesAlterWeb: TSmallintField;
    QrLotesIOFd: TFloatField;
    QrLotesIOFd_VAL: TFloatField;
    QrLotesIOFv: TFloatField;
    QrLotesIOFv_VAL: TFloatField;
    QrLotesTipoIOF: TSmallintField;
    QrLotesAtivo: TSmallintField;
    QrLotesADVAL_TXT: TWideStringField;
    QrLotesTAXA_AM_TXT: TWideStringField;
    QrLotesSUB_TOTAL_MEU: TFloatField;
    QrLotesVAL_LIQUIDO_MEU: TFloatField;
    QrLotesA_PG_LIQ: TFloatField;
    PMImprime: TPopupMenu;
    DadosApenaspreencherimpresso1: TMenuItem;
    udoFolhaembranco1: TMenuItem;
    Semomeuendereo1: TMenuItem;
    Comomeuendereo1: TMenuItem;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BTNFClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrNFsAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrNFsAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrNFsBeforeOpen(DataSet: TDataSet);
    procedure BtEuroClick(Sender: TObject);
    procedure Adiona1Click(Sender: TObject);
    procedure Retira1Click(Sender: TObject);
    procedure IncluinovaNF1Click(Sender: TObject);
    procedure AlteraNFatual1Click(Sender: TObject);
    procedure ExcluiNFatual1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure frxNF_TudoGetValue(const VarName: string; var Value: Variant);
    procedure PMNFPopup(Sender: TObject);
    procedure QrLotesCalcFields(DataSet: TDataSet);
    procedure udoFolhaembranco1Click(Sender: TObject);
    procedure Semomeuendereo1Click(Sender: TObject);
    procedure Comomeuendereo1Click(Sender: TObject);
  private
    FMEU_ENDERECO: Boolean;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenLotes;
    procedure AtualizaNF;
    procedure ImprimeNF(MeuEndereco: Boolean);
  public
    { Public declarations }
  end;

var
  FmNFs: TFmNFs;
const
  FFormatFloat = '000000';

implementation

uses Module, Principal, LotesLoc, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmNFs.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmNFs.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrNFsCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmNFs.DefParams;
begin
  VAR_GOTOTABELA := 'NFs';
  VAR_GOTOMYSQLTABLE := QrNFs;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM nfs');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmNFs.ExcluiNFatual1Click(Sender: TObject);
begin
  if QrLotes.RecordCount > 0 then
  begin
    Application.MessageBox('Esta Nota Fiscal n�o pode ser excluida pois '+
    'cont�m border�s vinculados!', 'Erro', MB_OK+MB_ICONERROR);
  end else begin
    if Application.MessageBox('Confirma a exclus�o desta Nota Fiscal?', 'Pergunta',
      MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM nfs WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrNFsCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      LocCod(QrNFsCodigo.Value, QrNFsCodigo.Value);
    end;
  end;
end;

procedure TFmNFs.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    PainelControle.Visible:=False;
    if Status = CO_INCLUSAO then
    begin
      QrMax.Close;
      QrMax.Open;
      EdCodigo.Text := IntToStr(QrMaxCodigo.Value+1);
      EdCodigo.ReadOnly := False;
      EdCodigo.SetFocus;
    end else begin
      EdCodigo.ReadOnly := True;
      EdCodigo.Text := DBEdCodigo.Text;
    end;
  end else begin
    PainelControle.Visible:=True;
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmNFs.PMNFPopup(Sender: TObject);
begin
  AlteraNFAtual1.Enabled := QrNFsCodigo.Value > 0;
end;

procedure TFmNFs.Comomeuendereo1Click(Sender: TObject);
begin
  ImprimeNF(True);
end;

procedure TFmNFs.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmNFs.AlteraNFatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmNFs.AlteraRegistro;
var
  NFs : Integer;
begin
  NFs := QrNFsCodigo.Value;
  if not UMyMod.SelLockY(NFs, Dmod.MyDB, 'NFs', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(NFs, Dmod.MyDB, 'NFs', 'Codigo');
      MostraEdicao(True, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmNFs.IncluinovaNF1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmNFs.IncluiRegistro;
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    MostraEdicao(True, CO_INCLUSAO, 0);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmNFs.QueryPrincipalAfterOpen;
begin
end;

procedure TFmNFs.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmNFs.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmNFs.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmNFs.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmNFs.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmNFs.udoFolhaembranco1Click(Sender: TObject);
begin
  GOTOy.EnderecoDeEntidade(QrLotesCliente.Value, 0);
  MyObjects.frxMostra(frxNF_Tudo, 'Nota Fiscal n� '+IntToStr(QrLotesNF.Value));
end;

procedure TFmNFs.BTNFClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMNF, BtNF);
end;

procedure TFmNFs.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrNFsCodigo.Value;
  Close;
end;

procedure TFmNFs.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO nfs SET ')
  else Dmod.QrUpdU.SQL.Add('UPDATE nfs SET ');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  //
  Dmod.QrUpdU.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[01].AsInteger := VAR_USUARIO;
  //
  Dmod.QrUpdU.Params[02].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFs', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
end;

procedure TFmNFs.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'NFs', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFs', 'Codigo');
  MostraEdicao(False, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'NFs', 'Codigo');
end;

procedure TFmNFs.FormCreate(Sender: TObject);
begin
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  DBGrid1.Align     := alClient; 
  CriaOForm;
end;

procedure TFmNFs.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrNFsCodigo.Value,LaRegistro.Caption);
end;

procedure TFmNFs.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmNFs.SbNomeClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
  FmPrincipal.FLoteLoc := 0;
  Application.CreateForm(TFmLotesLoc, FmLotesLoc);
  FmLotesLoc.FFormCall := 3;
  FmLotesLoc.ShowModal;
  FmLotesLoc.Destroy;
  if FmPrincipal.FLoteLoc <> 0 then
  LocCod(FmPrincipal.FLoteLoc, FmPrincipal.FLoteLoc);
end;

procedure TFmNFs.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmNFs.QrLotesCalcFields(DataSet: TDataSet);
begin
  ///////// M�nimos ///////////////////
  if QrLotesTotal.Value > 0 then
  begin
    QrLotesADVAL_TXT.Value := Geral.FFT(QrLotesMINAV.Value /
      QrLotesTotal.Value * 100, 2, siPositivo);
  end else begin
    QrLotesADVAL_TXT.Value := '0,00';
  end;
  QrLotesTAXA_AM_TXT.Value := Geral.FFT(QrLotesMINTC_AM.Value, 2, siPositivo);
  //
  QrLotesSUB_TOTAL_MEU.Value := QrLotesTotal.Value -
    QrLotesMINAV.Value - QrLotesMINTC.Value;
  QrLotesVAL_LIQUIDO_MEU.Value := QrLotesSUB_TOTAL_MEU.Value -
    QrLotesIOC_VAL.Value - QrLotesIOFd_VAL.Value - QrLotesIOFv_VAL.Value;
  //
  QrLotesA_PG_LIQ.Value := QrLotesVAL_LIQUIDO_MEU.Value - QrLotesPgLiq.Value;
  ///////// Fim M�nimos ///////////////////
end;

procedure TFmNFs.QrNFsAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  ReopenLotes;
end;

procedure TFmNFs.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'NFs', 'Livres', 99) then
  //BtInclui.Enabled := False;
  EdCodigo.MaxLength := Length(FFormatFloat);
end;

procedure TFmNFs.QrNFsAfterScroll(DataSet: TDataSet);
begin
  ReopenLotes;
end;

procedure TFmNFs.SbQueryClick(Sender: TObject);
begin
  SbNomeClick(Self);
  //LocCod(QrNFsCodigo.Value,
  //CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'NFs', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmNFs.Semomeuendereo1Click(Sender: TObject);
begin
  ImprimeNF(False);
end;

procedure TFmNFs.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmNFs.frxNF_TudoGetValue(const VarName: string; var Value: Variant);
var
  Txt: String;
begin
  if AnsiCompareText(VarName, 'VARF_NFLINHA1') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFLinha1.Value,
      '[TAXA_ADVAL]', QrLotesADVAL_TXT.Value);
    Txt := MLAGeral.TrocaTexto(Txt, '[VALOR_DOC]',
      Geral.FFT(QrLotesTotal.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[NUM_BORDERO]',
      IntToStr(QrLotesLote.Value));
    Txt := MLAGeral.TrocaTexto(Txt, '[IMPOSTO]',
      Geral.FFT(QrLotesISS.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'VARF_NFLINHA2') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFLinha2.Value,
      '[TAXA_ADVAL]', QrLotesADVAL_TXT.Value);
    Txt := MLAGeral.TrocaTexto(Txt, '[VALOR_DOC]',
      Geral.FFT(QrLotesTotal.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[NUM_BORDERO]',
      IntToStr(QrLotesLote.Value));
    Txt := MLAGeral.TrocaTexto(Txt, '[IMPOSTO]',
      Geral.FFT(QrLotesISS.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'VARF_NFLINHA3') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFLinha3.Value,
      '[TAXA_ADVAL]', QrLotesADVAL_TXT.Value);
    Txt := MLAGeral.TrocaTexto(Txt, '[VALOR_DOC]',
      Geral.FFT(QrLotesTotal.Value, 2, siPositivo));
    Txt := MLAGeral.TrocaTexto(Txt, '[NUM_BORDERO]',
      IntToStr(QrLotesLote.Value));
    Txt := MLAGeral.TrocaTexto(Txt, '[IMPOSTO]',
      Geral.FFT(QrLotesISS.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'IMPOSTO') = 0 then
  begin
    Txt := MLAGeral.TrocaTexto(Dmod.QrControleNFAliq.Value, '[IMPOSTO]',
      Geral.FFT(QrLotesISS.Value, 2, siPositivo));
    Value := Txt;
  end
  else if AnsiCompareText(VarName, 'AlturaInicial') = 0 then
  begin
    Value := Int(Dmod.QrControleNF_Cabecalho.Value / VAR_frCM);//37.7953);
  end
  else if AnsiCompareText(VarName, 'VARF_MEUENDERECO') = 0 then
  begin
    if FMEU_ENDERECO then Value := Dmod.QrDonoE_CUC.Value
    else Value := ' ';
  end
end;

procedure TFmNFs.QrNFsBeforeOpen(DataSet: TDataSet);
begin
  QrNFsCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmNFs.ReopenLotes;
begin
  QrLotes.Close;
  if QrNFsCodigo.Value > 0 then
  begin
    QrLotes.Params[0].AsInteger := QrNFsCodigo.Value;
    QrLotes.Open;
  end;  
end;

procedure TFmNFs.BtEuroClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBordero, BtEuro);
end;

procedure TFmNFs.AtualizaNF;
begin
  QrSum.Close;
  QrSum.Params[0].AsInteger := QrNFsCodigo.Value;
  QrSum.Open;
  //
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE nfs SET AdValor=:P0, ISS_Val=:P1');
  DMod.QrUpd.SQL.Add('WHERE Codigo=:P2');
  //
  DMod.QrUpd.Params[0].AsFloat   := QrSUMMINAV.Value;
  DMod.QrUpd.Params[1].AsFloat   := QrSUMISS_Val.Value;
  DMod.QrUpd.Params[2].AsInteger := QrNFsCodigo.Value;
  //
  DMod.QrUpd.ExecSQL;
  QrSum.Close;
end;

procedure TFmNFs.Adiona1Click(Sender: TObject);
begin
  try
    FmPrincipal.FLoteLoc := 0;
    Application.CreateForm(TFmLotesLoc, FmLotesLoc);
    FmLotesLoc.FFormCall := 2;
    FmLotesLoc.FNF := QrNFsCodigo.Value;
    FmLotesLoc.ReopenLoc;
    FmLotesLoc.ShowModal;
  finally
    Screen.Cursor := crDefault;
  end;
  FmLotesLoc.Destroy;
  Screen.Cursor := crHourGlass;
  try
    AtualizaNF;
    LocCod(QrNFsCodigo.Value, QrNFsCodigo.Value);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmNFs.Retira1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a retirada do Border� selecionado desta '+
  'Nota Fiscal?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE lotes SET AlterWeb=1,  NF=0 WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrLotesCodigo.Value;
      Dmod.QrUpd.ExecSQL;
      //
      AtualizaNF;
      LocCod(QrNFsCodigo.Value, QrNFsCodigo.Value);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmNFs.ImprimeNF(MeuEndereco: Boolean);
begin
  FMEU_ENDERECO := MeuEndereco;
  GOTOy.EnderecoDeEntidade(QrLotesCliente.Value, 0);
  MyObjects.frxMostra(frxNF_Dados, 'Nota Fiscal n� '+IntToStr(QrLotesNF.Value));
end;

end.

