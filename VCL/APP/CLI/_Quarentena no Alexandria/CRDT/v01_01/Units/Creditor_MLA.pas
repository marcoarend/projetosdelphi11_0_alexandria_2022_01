unit Creditor_MLA;

interface

uses
  Forms, Db, (*DBTables,*) Menus, Controls, Classes, ExtCtrls, UnMLAGeral,
  UnInternalConsts, Messages, Dialogs, Windows, SysUtils, Graphics, StdCtrls,
  Mask, DBCtrls, UnInternalConsts2, jpeg, mySQLDbTables, UnGOTOy, UnMyLinguas,
  Buttons, ComCtrls, dmkGeral, dmkLed;

type
  TFmCreditor_MLA = class(TForm)
    PopupMenu1: TPopupMenu;
    Close1: TMenuItem;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Panel2: TPanel;
    BtEntra: TBitBtn;
    EdLogin: TEdit;
    EdSenha: TEdit;
    EdTerminal: TEdit;
    Label3: TLabel;
    BtSai: TBitBtn;
    StaticText1: TStaticText;
    Panel3: TPanel;
    EdLED: TdmkLED;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    ProgressBar1: TProgressBar;
    LaSenhas: TLabel;
    LaConexao: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure Close1Click(Sender: TObject);
    procedure EdSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdLoginKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtEntraClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormHide(Sender: TObject);
    procedure BtSaiClick(Sender: TObject);
    procedure EdSenhaExit(Sender: TObject);
    procedure LaSenhasClick(Sender: TObject);
    procedure LaSenhasMouseEnter(Sender: TObject);
    procedure LaSenhasMouseLeave(Sender: TObject);
    procedure LaConexaoClick(Sender: TObject);
    procedure LaConexaoMouseEnter(Sender: TObject);
    procedure LaConexaoMouseLeave(Sender: TObject);

  private
    { Private declarations }
    procedure ControleMudou(Sender: TObject);
    procedure BtInstallVisible;

  public
    { Public declarations }
    procedure DefineCores;
    procedure ReloadSkin;

  end;

var
  FmCreditor_MLA: TFmCreditor_MLA;
  SkinConta : Integer;

implementation

uses Principal, Module, ZCF2, UMySQLModule, UnMyObjects, VerificaConexoes,
  MyListas;

{$R *.DFM}

procedure TFmCreditor_MLA.ControleMudou(Sender: TObject);
begin
  if APP_LIBERADO then
  begin
    MyObjects.ControleCor(Self);
    //MLAGeral.SistemaMetrico;
    //MLAGeral.CustoSincronizado;
  end;
end;

procedure TFmCreditor_MLA.FormCreate(Sender: TObject);
var
  WinPath : Array[0..144] of Char;
  Cam: String;
begin
  Cam := Application.Title+'\Opcoes';
  //
  VAR_CAMINHOTXTBMP := 'C:\Dermatek\Skins\VCLSkin\mxskin33.skn';
  MyLinguas.GetInternationalStrings(myliPortuguesBR);
  TMeuDB := 'Creditor';
  // 2011/12/13
  UMyMod.DefineDatabase(TMeuDB, Application.ExeName) ;
  // Fim 2011/12/13
  //
  TLocDB := 'LocCredr';
  TPdxDB := 'Fomento';
  VAR_SQLUSER := 'root';
  Geral.WriteAppKeyCU('Cashier_Database', 'Dermatek', TMeuDB, ktString);
  Geral.WriteAppKeyCU('CashLoc_Database', 'Dermatek', TLocDB, ktString);
  Geral.WriteAppKeyCU('Cashier_DmkID_APP', 'Dermatek', CO_DMKID_APP, ktInteger);
  //
  DefineCores;
  Screen.OnActiveControlChange := ControleMudou;
  (*if not MLAGeral.MySQL_Path then
  begin
    Application.Terminate;
    Exit;
  end;*)
  IC2_LOpcoesTeclaEnter := Geral.ReadAppKeyLM('TeclaEnter', Application.Title, ktInteger, -1);
  if IC2_LOpcoesTeclaEnter = -1 then
  begin
    Geral.WriteAppKeyLM2('TeclaEnter', Application.Title, 13, ktInteger);
    IC2_LOpcoesTeclaEnter := 13;
  end;
  IC2_LOpcoesTeclaTab := Geral.ReadAppKeyLM('TeclaTAB', Application.Title, ktInteger, -1);
  if IC2_LOpcoesTeclaTab = -1 then
  begin
    Geral.WriteAppKeyLM2('TeclaTAB', Application.Title, 9, ktInteger);
    IC2_LOpcoesTeclaTab := 9;
  end;
  GetWindowsDirectory(WinPath,144);
  ReloadSkin;
  IC2_COMPANT := nil;
  VAR_BD := 0;
end;

procedure TFmCreditor_MLA.Close1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmCreditor_MLA.EdSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_RETURN) then
  GOTOy.AnaliseSenha(EdLogin, EdSenha, FmCreditor_MLA, FmPrincipal, BtEntra,
  False(*ForcaSenha*), LaAviso1, LaAviso2, nil);

  if (Key = VK_ESCAPE) then Close;
end;

procedure TFmCreditor_MLA.EdLoginKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_RETURN) then
  GOTOy.AnaliseSenha(EdLogin, EdSenha, FmCreditor_MLA, FmPrincipal, BtEntra,
  False(*ForcaSenha*), LaAviso1, LaAviso2, nil);

  if (Key = VK_ESCAPE) then Close;
end;

procedure TFmCreditor_MLA.DefineCores;
//var
  //Aparencia: Integer;
begin
  IC2_AparenciasTituloPainelItem   := clBtnFace;
  IC2_AparenciasTituloPainelTexto  := clWindowText;
  IC2_AparenciasDadosPainel        := clBtnFace;
  IC2_AparenciasDadosTexto         := clWindowText;
  IC2_AparenciasGradeAItem         := clWindow;
  IC2_AparenciasGradeATitulo       := clWindowText;
  IC2_AparenciasGradeATexto        := clWindowText;
  IC2_AparenciasGradeAGrade        := clBtnFace;
  IC2_AparenciasGradeBItem         := clWindow;
  IC2_AparenciasGradeBTitulo       := clWindowText;
  IC2_AparenciasGradeBTexto        := clWindowText;
  IC2_AparenciasGradeBGrade        := clBtnFace;
  IC2_AparenciasEditAItem          := clWindow;
  IC2_AparenciasEditATexto         := clWindowText;
  IC2_AparenciasEditBItem          := clBtnFace;
  IC2_AparenciasEditBTexto         := clWindowText;
  IC2_AparenciasConfirmaPainelItem := clBtnFace;
  IC2_AparenciasControlePainelItem := clBtnFace;
  IC2_AparenciasFormPesquisa       := clBtnFace;
  IC2_AparenciasLabelPesquisa      := clWindowText;
  IC2_AparenciasLabelDigite        := clWindowText;
  IC2_AparenciasEditPesquisaItem   := clWindow;
  IC2_AparenciasEditPesquisaTexto  := clWindowText;
  IC2_AparenciasEditItemIn         := $00FF8000;
  IC2_AparenciasEditItemOut        := clWindow;
  IC2_AparenciasEditTextIn         := $00D5FAFF;
  IC2_AparenciasEditTextOut        := clWindowText;
(*  if Geral.ReadAppKeyLM('Versao', Application.Title, ktInteger, 0) <> 0 then
  begin
    Aparencia := Geral.IMV(Geral.ReadAppKeyLM('Aparencia',
    Application.Title, ktString, '-1000'));
    if Aparencia = -1000 then
       Geral.WriteAppKeyLM2('Aparencia', Application.Title, '0', ktString);
    QrAparencias.Close;
    QrAparencias.Params[0].AsInteger := Aparencia;
    QrAparencias.Open;
    if GOTOy.Registros(QrAparencias)>0 then
    begin
      IC2_AparenciasTituloPainelItem := QrAparenciasTituloPainelItem.Value;
      IC2_AparenciasTituloPainelTexto := QrAparenciasTituloPainelTexto.Value;
      IC2_AparenciasDadosPainel := QrAparenciasDadosPainel.Value;
      IC2_AparenciasDadosTexto := QrAparenciasDadosTexto.Value;
      IC2_AparenciasGradeAItem := QrAparenciasGradeAItem.Value;
      IC2_AparenciasGradeATitulo := QrAparenciasGradeATitulo.Value;
      IC2_AparenciasGradeATexto := QrAparenciasGradeATexto.Value;
      IC2_AparenciasGradeAGrade := QrAparenciasGradeAGrade.Value;
      IC2_AparenciasGradeBItem := QrAparenciasGradeBItem.Value;
      IC2_AparenciasGradeBTitulo := QrAparenciasGradeBTitulo.Value;
      IC2_AparenciasGradeBTexto := QrAparenciasGradeBTexto.Value;
      IC2_AparenciasGradeBGrade := QrAparenciasGradeBGrade.Value;
      IC2_AparenciasEditAItem := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditATexto := QrAparenciasEditATexto.Value;
      IC2_AparenciasEditBItem := QrAparenciasEditBItem.Value;
      IC2_AparenciasEditBTexto := QrAparenciasEditBTexto.Value;
      IC2_AparenciasConfirmaPainelItem := QrAparenciasConfirmaPainelItem.Value;
      IC2_AparenciasControlePainelItem := QrAparenciasControlePainelItem.Value;
      IC2_AparenciasFormPesquisa := QrAparenciasFormPesquisa.Value;
      IC2_AparenciasLabelPesquisa := QrAparenciasLabelPesquisa.Value;
      IC2_AparenciasLabelDigite := QrAparenciasLabelDigite.Value;
      IC2_AparenciasEditPesquisaItem := QrAparenciasEditPesquisaItem.Value;
      IC2_AparenciasEditPesquisaTexto := QrAparenciasEditPesquisaTexto.Value;
      IC2_AparenciasEditItemIn         := $00D9FFFF;
      IC2_AparenciasEditItemOut        := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditTextIn         := $00A00000;
      IC2_AparenciasEditTextOut        := QrAparenciasEditATexto.Value;
      IC2_AparenciasEditItemIn         := QrAparenciasEditATexto.Value;
      IC2_AparenciasEditItemOut        := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditTextIn         := QrAparenciasEditAItem.Value;
      IC2_AparenciasEditTextOut        := QrAparenciasEditATexto.Value;
    QrAparencias.Close;
  end;*)
end;

procedure TFmCreditor_MLA.ReloadSkin;
begin
end;

procedure TFmCreditor_MLA.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  FmPrincipal.Close;
end;

procedure TFmCreditor_MLA.BtEntraClick(Sender: TObject);
begin
  GOTOy.AnaliseSenha(EdLogin, EdSenha, FmCreditor_MLA, FmPrincipal, BtEntra,
    False(*ForcaSenha*), LaAviso1, LaAviso2, nil);
end;

procedure TFmCreditor_MLA.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCreditor_MLA.FormHide(Sender: TObject);
begin
  if VAR_TERMINATE = False then
  begin
    Cursor := crHourGlass;
    try
      FmPrincipal.BringToFront;
      FmPrincipal.SetFocus;
      //
      if ZZterminate then
        Exit;
      //
      if VAR_USUARIO <> 0 then
        FmPrincipal.AcoesIniciaisDoAplicativo();
    finally
      Cursor := crDefault;
    end;
  end;
end;

procedure TFmCreditor_MLA.LaConexaoClick(Sender: TObject);
begin
  Application.CreateForm(TFmVerificaConexoes,  FmVerificaConexoes);
  FmVerificaConexoes.ShowModal;
  FmVerificaConexoes.Destroy;
end;

procedure TFmCreditor_MLA.LaConexaoMouseEnter(Sender: TObject);
begin
  LaConexao.Font.Color := clBlue;
  LaConexao.Font.Style := [fsUnderline];
end;

procedure TFmCreditor_MLA.LaConexaoMouseLeave(Sender: TObject);
begin
  LaConexao.Font.Color := clBlue;
  LaConexao.Font.Style := [];
end;

procedure TFmCreditor_MLA.LaSenhasClick(Sender: TObject);
begin
  Geral.AbrirAppAuxiliar(12);
end;

procedure TFmCreditor_MLA.LaSenhasMouseEnter(Sender: TObject);
begin
  LaSenhas.Font.Color := clBlue;
  LaSenhas.Font.Style := [fsUnderline];
end;

procedure TFmCreditor_MLA.LaSenhasMouseLeave(Sender: TObject);
begin
  LaSenhas.Font.Color := clBlue;
  LaSenhas.Font.Style := [];
end;

procedure TFmCreditor_MLA.BtSaiClick(Sender: TObject);
begin
  VAR_TERMINATE := True;
  Close;
end;

procedure TFmCreditor_MLA.BtInstallVisible;
begin
end;

procedure TFmCreditor_MLA.EdSenhaExit(Sender: TObject);
begin
  BtInstallVisible;
end;

end.
