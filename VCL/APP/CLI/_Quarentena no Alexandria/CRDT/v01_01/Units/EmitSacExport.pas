unit EmitSacExport;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, dmkGeral, dmkEdit,
  UnDmkProcFunc;

type
  TFmEmitSacExport = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrEmitCPF: TmySQLQuery;
    QrEmitCPFCPF: TWideStringField;
    QrEmitCPFNome: TWideStringField;
    QrEmitCPFLastAtz: TDateField;
    Memo1: TMemo;
    QrEmitBAC: TmySQLQuery;
    QrEmitBACBAC: TWideStringField;
    QrEmitBACCPF: TWideStringField;
    EdPath: TdmkEdit;
    BtAbrir: TBitBtn;
    EdBackFile: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtAbrirClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmEmitSacExport: TFmEmitSacExport;

implementation

uses Module, UnInternalConsts, UnMyObjects;

{$R *.DFM}

procedure TFmEmitSacExport.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmitSacExport.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmEmitSacExport.FormCreate(Sender: TObject);
begin
  EdPath.Text := Geral.ReadAppKeyLM('BackUpPath', 'Dermatek', ktString, '');
end;

procedure TFmEmitSacExport.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmEmitSacExport.BtOKClick(Sender: TObject);
var
  DIB, Txt, Dta, Path: String;
begin
  Path := Trim(EdPath.Text);
  if Trim(EdPath.Text) = '' then
  begin
    Geral.MB_Aviso('Caminho n�o informado!');
    Exit;
  end;
  if not DirectoryExists(Path) then ForceDirectories(Path);
  if not DirectoryExists(Path) then
  begin
    Geral.MB_Erro('Caminho n�o pode ser criado!');
    Exit;
  end;
  Path := dmkPF.CaminhoArquivo(Path, 'EmiSac_' +
    dmkPF.ObtemDataStr(Date, False) + '.', '.txt');

  //

  Screen.Cursor := crHourGlass;
  Memo1.Lines.Clear;
  DIB := Geral.CompletaString(Dmod.QrDonoCNPJ_CPF.Value,     ' ', 15, taLeftJustify, True);
  Txt := Geral.CompletaString(Dmod.QrDonoNOMEDONO.Value,    ' ', 100, taLeftJustify, True);
  Memo1.Lines.Add('000' + DIB + Txt);

  //

  QrEmitCPF.Close;
  QrEmitCPF.Open;
  while not QrEmitCPF.Eof do
  begin
    DIB := Geral.CompletaString(QrEmitCPFCPF.Value,     ' ', 15, taLeftJustify, True);
    Txt := Geral.CompletaString(QrEmitCPFNome.Value,    ' ', 50, taLeftJustify, True);
    Dta := FormatDateTime(VAR_FORMATDATE, QrEmitCPFLastAtz.Value);
    Memo1.Lines.Add('001' + DIB + Txt + Dta);
    QrEmitCPF.Next;
  end;

  //

  QrEmitBAC.Close;
  QrEmitBAC.Open;
  while not QrEmitBAC.Eof do
  begin
    DIB := Geral.CompletaString(QrEmitBACCPF.Value, ' ', 15, taLeftJustify, True);
    Txt := Geral.CompletaString(QrEmitBACBAC.Value, ' ', 20, taLeftJustify, True);
    Memo1.Lines.Add('002' + DIB + Txt);
    QrEmitBAC.Next;
  end;

  //


  if MLAGeral.ExportaMemoToFile(Memo1, Path, True, False, False) then
    Geral.MB_Aviso('Arquivo salvo em "' + Path + '"!')
  else
    Geral.MB_Erro('Arquivo n�o pode ser salvo!');


  Screen.Cursor := crDefault;

end;

procedure TFmEmitSacExport.BtAbrirClick(Sender: TObject);
var
  Arquivo: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', Label2.Caption, '', [], Arquivo) then
  begin
    EdPath.Text := ExtractFileDir(Arquivo) + '\';
    //
    Geral.WriteAppKeyLM2('BackUpPath', 'Dermatek', EdPath.Text, ktString);
  end;
end;

end.
