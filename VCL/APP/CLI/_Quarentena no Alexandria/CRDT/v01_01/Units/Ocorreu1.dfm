object FmOcorreu1: TFmOcorreu1
  Left = 356
  Top = 178
  Caption = 'Ocorr'#234'ncias em Duplicatas'
  ClientHeight = 496
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 448
    Align = alClient
    BevelOuter = bvNone
    Color = clAppWorkSpace
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 301
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object PainelPesq: TPanel
        Left = 0
        Top = 0
        Width = 792
        Height = 49
        Align = alTop
        TabOrder = 0
        object Label75: TLabel
          Left = 8
          Top = 4
          Width = 35
          Height = 13
          Caption = 'Cliente:'
        end
        object Label1: TLabel
          Left = 348
          Top = 4
          Width = 45
          Height = 13
          Caption = 'Duplicata'
        end
        object EdCliente: TdmkEditCB
          Left = 8
          Top = 20
          Width = 65
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdClienteChange
          DBLookupComboBox = CBCliente
          IgnoraDBLookupComboBox = False
        end
        object CBCliente: TdmkDBLookupComboBox
          Left = 76
          Top = 20
          Width = 269
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMECLIENTE'
          ListSource = DsClientes
          TabOrder = 1
          dmkEditCB = EdCliente
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdDuplicata: TdmkEdit
          Left = 348
          Top = 20
          Width = 109
          Height = 21
          Alignment = taCenter
          CharCase = ecUpperCase
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnChange = EdDuplicataChange
        end
        object BtPesquisa: TBitBtn
          Tag = 22
          Left = 532
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Pesquisa'
          NumGlyphs = 2
          TabOrder = 3
          Visible = False
          OnClick = BtPesquisaClick
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 49
        Width = 792
        Height = 48
        Align = alTop
        Enabled = False
        TabOrder = 1
        object Label2: TLabel
          Left = 8
          Top = 4
          Width = 42
          Height = 13
          Caption = 'Emiss'#227'o:'
          FocusControl = DBEdit1
        end
        object Label3: TLabel
          Left = 160
          Top = 4
          Width = 50
          Height = 13
          Caption = 'Opera'#231#227'o:'
          FocusControl = DBEdit2
        end
        object Label4: TLabel
          Left = 228
          Top = 4
          Width = 27
          Height = 13
          Caption = 'Valor:'
          FocusControl = DBEdit3
        end
        object Label5: TLabel
          Left = 76
          Top = 4
          Width = 80
          Height = 13
          Caption = 'Taxa de compra:'
          FocusControl = DBEdit4
        end
        object Label6: TLabel
          Left = 312
          Top = 4
          Width = 74
          Height = 13
          Caption = 'Desconto Vcto:'
          FocusControl = DBEdit5
        end
        object Label7: TLabel
          Left = 396
          Top = 4
          Width = 59
          Height = 13
          Caption = 'Vencimento:'
          FocusControl = DBEdit6
        end
        object DBEdit1: TDBEdit
          Left = 8
          Top = 20
          Width = 64
          Height = 21
          DataField = 'Emissao'
          DataSource = DsPesq
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 160
          Top = 20
          Width = 64
          Height = 21
          DataField = 'DCompra'
          DataSource = DsPesq
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 228
          Top = 20
          Width = 80
          Height = 21
          DataField = 'Valor'
          DataSource = DsPesq
          TabOrder = 2
        end
        object DBEdit4: TDBEdit
          Left = 76
          Top = 20
          Width = 80
          Height = 21
          DataField = 'TAXA_COMPRA'
          DataSource = DsPesq
          TabOrder = 3
        end
        object DBEdit5: TDBEdit
          Left = 312
          Top = 20
          Width = 80
          Height = 21
          DataField = 'Desco'
          DataSource = DsPesq
          TabOrder = 4
        end
        object DBEdit6: TDBEdit
          Left = 396
          Top = 20
          Width = 64
          Height = 21
          DataField = 'Vencto'
          DataSource = DsPesq
          TabOrder = 5
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 97
        Width = 792
        Height = 128
        Align = alTop
        Enabled = False
        TabOrder = 2
        object Label8: TLabel
          Left = 8
          Top = 4
          Width = 89
          Height = 13
          Caption = 'Nome do emitente:'
          FocusControl = DBEdit7
        end
        object Label9: TLabel
          Left = 316
          Top = 4
          Width = 57
          Height = 13
          Caption = 'Logradouro:'
          FocusControl = DBEdit8
        end
        object Label10: TLabel
          Left = 560
          Top = 4
          Width = 40
          Height = 13
          Caption = 'N'#250'mero:'
          FocusControl = DBEdit9
        end
        object Label11: TLabel
          Left = 8
          Top = 44
          Width = 67
          Height = 13
          Caption = 'Complemento:'
          FocusControl = DBEdit10
        end
        object Label12: TLabel
          Left = 196
          Top = 44
          Width = 30
          Height = 13
          Caption = 'Bairro:'
          FocusControl = DBEdit11
        end
        object Label13: TLabel
          Left = 384
          Top = 44
          Width = 36
          Height = 13
          Caption = 'Cidade:'
          FocusControl = DBEdit12
        end
        object Label14: TLabel
          Left = 8
          Top = 84
          Width = 17
          Height = 13
          Caption = 'UF:'
          FocusControl = DBEdit13
        end
        object Label15: TLabel
          Left = 544
          Top = 44
          Width = 24
          Height = 13
          Caption = 'CEP:'
          FocusControl = DBEdit14
        end
        object Label16: TLabel
          Left = 40
          Top = 84
          Width = 45
          Height = 13
          Caption = 'Telefone:'
          FocusControl = DBEdit15
        end
        object Label17: TLabel
          Left = 232
          Top = 84
          Width = 88
          Height = 13
          Caption = 'Limite operacional:'
          FocusControl = DBEdit16
        end
        object Label18: TLabel
          Left = 344
          Top = 84
          Width = 27
          Height = 13
          Caption = 'CNPJ'
          FocusControl = DBEdit17
        end
        object Label19: TLabel
          Left = 472
          Top = 84
          Width = 85
          Height = 13
          Caption = 'Incri'#231#227'o Estadual:'
          FocusControl = DBEdit18
        end
        object DBEdit7: TDBEdit
          Left = 8
          Top = 20
          Width = 304
          Height = 21
          DataField = 'Nome'
          DataSource = DsPesq
          TabOrder = 0
        end
        object DBEdit8: TDBEdit
          Left = 316
          Top = 20
          Width = 241
          Height = 21
          DataField = 'Rua'
          DataSource = DsPesq
          TabOrder = 1
        end
        object DBEdit9: TDBEdit
          Left = 560
          Top = 20
          Width = 64
          Height = 21
          DataField = 'NUMERO_TXT'
          DataSource = DsPesq
          TabOrder = 2
        end
        object DBEdit10: TDBEdit
          Left = 8
          Top = 60
          Width = 184
          Height = 21
          DataField = 'Compl'
          DataSource = DsPesq
          TabOrder = 3
        end
        object DBEdit11: TDBEdit
          Left = 196
          Top = 60
          Width = 184
          Height = 21
          DataField = 'Bairro'
          DataSource = DsPesq
          TabOrder = 4
        end
        object DBEdit12: TDBEdit
          Left = 384
          Top = 60
          Width = 157
          Height = 21
          DataField = 'Cidade'
          DataSource = DsPesq
          TabOrder = 5
        end
        object DBEdit13: TDBEdit
          Left = 8
          Top = 100
          Width = 29
          Height = 21
          DataField = 'UF'
          DataSource = DsPesq
          TabOrder = 6
        end
        object DBEdit14: TDBEdit
          Left = 544
          Top = 60
          Width = 81
          Height = 21
          DataField = 'CEP_TXT'
          DataSource = DsPesq
          TabOrder = 7
        end
        object DBEdit15: TDBEdit
          Left = 40
          Top = 100
          Width = 189
          Height = 21
          DataField = 'TEL1_TXT'
          DataSource = DsPesq
          TabOrder = 8
        end
        object DBEdit16: TDBEdit
          Left = 232
          Top = 100
          Width = 109
          Height = 21
          DataField = 'Risco'
          DataSource = DsPesq
          TabOrder = 9
        end
        object DBEdit17: TDBEdit
          Left = 344
          Top = 100
          Width = 125
          Height = 21
          DataField = 'CNPJ_TXT'
          DataSource = DsPesq
          TabOrder = 10
        end
        object DBEdit18: TDBEdit
          Left = 472
          Top = 100
          Width = 154
          Height = 21
          DataField = 'IE'
          DataSource = DsPesq
          TabOrder = 11
        end
      end
      object PainelChange: TPanel
        Left = 0
        Top = 225
        Width = 792
        Height = 76
        Align = alClient
        TabOrder = 3
        object PainelEdit: TPanel
          Left = 77
          Top = 1
          Width = 714
          Height = 74
          Align = alClient
          TabOrder = 0
          Visible = False
          object Label20: TLabel
            Left = 12
            Top = 32
            Width = 55
            Height = 13
            Caption = 'Ocorr'#234'ncia:'
          end
          object Label21: TLabel
            Left = 356
            Top = 32
            Width = 26
            Height = 13
            Caption = 'Data:'
          end
          object Label22: TLabel
            Left = 464
            Top = 32
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object PanelFill1: TPanel
            Left = 1
            Top = 1
            Width = 712
            Height = 24
            Align = alTop
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -32
            Font.Name = 'Times New Roman'
            Font.Style = []
            ParentColor = True
            ParentFont = False
            TabOrder = 0
            TabStop = True
            object LaTipo: TLabel
              Left = 629
              Top = 1
              Width = 82
              Height = 22
              Align = alRight
              Alignment = taCenter
              AutoSize = False
              Caption = 'Travado'
              Font.Charset = ANSI_CHARSET
              Font.Color = 8281908
              Font.Height = -15
              Font.Name = 'Times New Roman'
              Font.Style = [fsBold]
              ParentFont = False
              Transparent = True
              ExplicitLeft = 628
              ExplicitTop = 2
              ExplicitHeight = 20
            end
          end
          object EdOcorrencia: TdmkEditCB
            Left = 12
            Top = 48
            Width = 65
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            OnExit = EdOcorrenciaExit
            DBLookupComboBox = CBOcorrencia
            IgnoraDBLookupComboBox = False
          end
          object CBOcorrencia: TdmkDBLookupComboBox
            Left = 80
            Top = 48
            Width = 273
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsOcorBank
            TabOrder = 2
            dmkEditCB = EdOcorrencia
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object TPDataO: TDateTimePicker
            Left = 356
            Top = 48
            Width = 105
            Height = 21
            Date = 38711.818866817100000000
            Time = 38711.818866817100000000
            TabOrder = 3
          end
          object EdValor: TdmkEdit
            Left = 464
            Top = 48
            Width = 85
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
        object GradeItens: TDBGrid
          Left = 1
          Top = 1
          Width = 76
          Height = 74
          Align = alLeft
          DataSource = DsPesq
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 277
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJ_TXT'
              Title.Caption = 'CNPJ/CPF'
              Width = 121
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DCompra'
              Title.Caption = 'Data C.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencto'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Duplicata'
              Visible = True
            end>
        end
      end
    end
    object PainelData: TPanel
      Left = 0
      Top = 340
      Width = 632
      Height = 60
      BevelOuter = bvLowered
      TabOrder = 1
      Visible = False
      object DBGrid1: TDBGrid
        Left = 1
        Top = 1
        Width = 630
        Height = 58
        Align = alClient
        DataSource = DsOcorreu
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'DataO'
            Title.Caption = 'Data'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEOCORRENCIA'
            Title.Caption = 'Ocorr'#234'ncia'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end>
      end
    end
    object PainelBtns: TPanel
      Left = 0
      Top = 400
      Width = 792
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object PainelConf: TPanel
        Left = 0
        Top = 0
        Width = 792
        Height = 48
        Align = alClient
        TabOrder = 1
        Visible = False
        object BtConfirma5: TBitBtn
          Tag = 14
          Left = 12
          Top = 5
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirma5Click
        end
        object BtDesiste5: TBitBtn
          Tag = 15
          Left = 652
          Top = 5
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtDesiste5Click
        end
      end
      object PainelCtrl: TPanel
        Left = 0
        Top = 0
        Width = 792
        Height = 48
        Align = alClient
        Color = clAppWorkSpace
        TabOrder = 0
        object PainelBtsE: TPanel
          Left = 1
          Top = 1
          Width = 612
          Height = 46
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object BtInclui: TBitBtn
            Tag = 10
            Left = 12
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = 'I&nclui'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtIncluiClick
          end
          object BtAltera: TBitBtn
            Tag = 11
            Left = 104
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = 'A&ltera'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtAlteraClick
          end
          object BtExclui: TBitBtn
            Tag = 12
            Left = 196
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = 'E&xclui'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtExcluiClick
          end
        end
        object Panel6: TPanel
          Left = 675
          Top = 1
          Width = 116
          Height = 46
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 11
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Ocorr'#234'ncias em Duplicatas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMECLIENTE')
    Left = 269
    Top = 61
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 297
    Top = 61
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    AfterScroll = QrPesqAfterScroll
    OnCalcFields = QrPesqCalcFields
    SQL.Strings = (
      'SELECT li.Controle, li.Emissao, li.TxaCompra, '
      'li.DCompra, li.Valor, li.Desco, li.Vencto, '
      'li.Duplicata, sa.Numero+0.000 Numero, sa.*'
      'FROM lotesits li'
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN sacados sa ON sa.CNPJ=li.CPF'
      'WHERE lo.Tipo=1'
      'AND lo.Cliente=:P0'
      'AND Duplicata=:P1')
    Left = 469
    Top = 113
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPesqEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrPesqDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesqDesco: TFloatField
      FieldName = 'Desco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesqVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 15
    end
    object QrPesqNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPesqRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrPesqCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrPesqBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrPesqCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrPesqUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrPesqCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrPesqTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrPesqRisco: TFloatField
      FieldName = 'Risco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesqIE: TWideStringField
      FieldName = 'IE'
      Size = 25
    end
    object QrPesqCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 40
      Calculated = True
    end
    object QrPesqNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 10
      Calculated = True
    end
    object QrPesqCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrPesqTEL1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL1_TXT'
      Size = 40
      Calculated = True
    end
    object QrPesqTAXA_COMPRA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TAXA_COMPRA'
      DisplayFormat = '#,##0.000000'
      Calculated = True
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPesqDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrPesqNumero: TFloatField
      FieldName = 'Numero'
    end
  end
  object QrOcorreu: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrOcorreuAfterOpen
    SQL.Strings = (
      'SELECT ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.LotesIts=:P0'
      '')
    Left = 113
    Top = 317
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorreuNOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcorreuCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOcorreuLotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrOcorreuDataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrOcorreuValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreuLoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrOcorreuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorreuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorreuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorreuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorreuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsOcorreu: TDataSource
    DataSet = QrOcorreu
    Left = 141
    Top = 317
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 497
    Top = 113
  end
  object QrOcorBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ocorbank'
      'ORDER BY Nome')
    Left = 269
    Top = 91
    object QrOcorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrOcorBankLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorBankDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorBankDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorBankUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorBankUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorBankBase: TFloatField
      FieldName = 'Base'
    end
  end
  object DsOcorBank: TDataSource
    DataSet = QrOcorBank
    Left = 297
    Top = 91
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 544
    Top = 56
  end
end
