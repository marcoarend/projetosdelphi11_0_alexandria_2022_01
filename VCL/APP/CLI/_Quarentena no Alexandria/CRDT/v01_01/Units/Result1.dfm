object FmResult1: TFmResult1
  Left = 329
  Top = 177
  Caption = 'Resultados L'#237'quidos de Operac'#245'es'
  ClientHeight = 496
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 49
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 0
    object Label3: TLabel
      Left = 8
      Top = 8
      Width = 35
      Height = 13
      Caption = 'Cliente:'
    end
    object Label34: TLabel
      Left = 604
      Top = 8
      Width = 55
      Height = 13
      Caption = 'Data inicial:'
    end
    object Label1: TLabel
      Left = 696
      Top = 8
      Width = 48
      Height = 13
      Caption = 'Data final:'
    end
    object Label2: TLabel
      Left = 308
      Top = 8
      Width = 86
      Height = 13
      Caption = 'Coligado especial:'
    end
    object EdCliente: TdmkEditCB
      Left = 8
      Top = 24
      Width = 52
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdClienteChange
      DBLookupComboBox = CbCliente
      IgnoraDBLookupComboBox = False
    end
    object CbCliente: TdmkDBLookupComboBox
      Left = 64
      Top = 24
      Width = 240
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMECLIENTE'
      ListSource = DsClientes
      TabOrder = 1
      dmkEditCB = EdCliente
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object TPIni: TDateTimePicker
      Left = 604
      Top = 24
      Width = 90
      Height = 21
      Date = 38675.714976851900000000
      Time = 38675.714976851900000000
      TabOrder = 2
      OnChange = TPIniChange
    end
    object TPFim: TDateTimePicker
      Left = 696
      Top = 24
      Width = 90
      Height = 21
      Date = 38675.714976851900000000
      Time = 38675.714976851900000000
      TabOrder = 3
      OnChange = TPFimChange
    end
    object EdColigado: TdmkEditCB
      Left = 308
      Top = 24
      Width = 52
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdColigadoChange
      DBLookupComboBox = CBColigado
      IgnoraDBLookupComboBox = False
    end
    object CBColigado: TdmkDBLookupComboBox
      Left = 364
      Top = 24
      Width = 236
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMECOLIGADO'
      ListSource = DsColigado
      TabOrder = 5
      dmkEditCB = EdColigado
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtImprime: TBitBtn
      Tag = 5
      Left = 212
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Imprime'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtImprimeClick
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 682
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BtSaidaClick
    end
    object BtPesquisa: TBitBtn
      Tag = 22
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Pesquisa'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtPesquisaClick
    end
    object ProgressBar1: TProgressBar
      Left = 456
      Top = 16
      Width = 213
      Height = 17
      TabOrder = 4
      Visible = False
    end
    object BtReabre: TBitBtn
      Tag = 20
      Left = 116
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Reabre'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtReabreClick
    end
    object BitBtn1: TBitBtn
      Tag = 263
      Left = 308
      Top = 4
      Width = 90
      Height = 40
      Caption = 'Con&figura'
      NumGlyphs = 2
      TabOrder = 5
      OnClick = BitBtn1Click
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Resultados L'#237'quidos de Operac'#245'es'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 281
    Width = 777
    Height = 128
    DataSource = DsOpera
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Visible = False
    OnTitleClick = DBGrid1TitleClick
    Columns = <
      item
        Expanded = False
        FieldName = 'Data'
        Width = 52
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMECLIENTE'
        Title.Caption = 'Cliente'
        Width = 140
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Lote'
        Title.Caption = 'Border'#244
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TOTAL'
        Title.Caption = 'Val.Negoc.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PROPRIO'
        Title.Caption = 'Val.Pr'#243'prio'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TXCOMPRAT'
        Title.Caption = 'Tx.Compra'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VALVALOREMT'
        Title.Caption = 'Ad Valorem'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MARGEM_TODOS'
        Title.Caption = 'Marg. todos'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'REPAS_V'
        Title.Caption = 'Repasse'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MARGEM_BRUTA'
        Title.Caption = 'Marg. l'#237'q.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PIS_T_VAL'
        Title.Caption = 'PIS'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ISS_VAL'
        Title.Caption = 'ISS'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'COFINS_T_VAL'
        Title.Caption = 'Cofins'
        Visible = True
      end>
  end
  object Panel1: TPanel
    Left = 0
    Top = 97
    Width = 792
    Height = 176
    Align = alTop
    BevelOuter = bvLowered
    TabOrder = 4
    object Bevel1: TBevel
      Left = 592
      Top = 9
      Width = 197
      Height = 75
      Shape = bsFrame
    end
    object LaOrd1: TLabel
      Left = 612
      Top = 23
      Width = 8
      Height = 13
      Caption = '?'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LaOrd2: TLabel
      Left = 612
      Top = 43
      Width = 8
      Height = 13
      Caption = '?'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LaOrd3: TLabel
      Left = 612
      Top = 63
      Width = 8
      Height = 13
      Caption = '?'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LaNO3: TLabel
      Left = 596
      Top = 63
      Width = 12
      Height = 13
      Caption = '3.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LaNO2: TLabel
      Left = 596
      Top = 43
      Width = 12
      Height = 13
      Caption = '2.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LaNO1: TLabel
      Left = 596
      Top = 23
      Width = 12
      Height = 13
      Caption = '1.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 600
      Top = 4
      Width = 101
      Height = 13
      Caption = ' Defini'#231#227'o da ordem: '
    end
    object Bevel2: TBevel
      Left = 592
      Top = 89
      Width = 197
      Height = 75
      Shape = bsFrame
    end
    object LaNA1: TLabel
      Left = 597
      Top = 103
      Width = 12
      Height = 13
      Caption = '1.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LaNA2: TLabel
      Left = 597
      Top = 123
      Width = 12
      Height = 13
      Caption = '2.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LaAgr2: TLabel
      Left = 613
      Top = 123
      Width = 8
      Height = 13
      Caption = '?'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LaAgr1: TLabel
      Left = 613
      Top = 103
      Width = 8
      Height = 13
      Caption = '?'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LaNA3: TLabel
      Left = 597
      Top = 143
      Width = 12
      Height = 13
      Caption = '3.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LaAgr3: TLabel
      Left = 613
      Top = 143
      Width = 8
      Height = 13
      Caption = '?'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clInactiveCaption
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label15: TLabel
      Left = 601
      Top = 84
      Width = 134
      Height = 13
      Caption = ' Defini'#231#227'o do agrupamento: '
    end
    object RGOrdem1: TRadioGroup
      Left = 4
      Top = 4
      Width = 117
      Height = 80
      Caption = ' Ordem A: '
      ItemIndex = 2
      Items.Strings = (
        'Nome do cliente'
        'Data da opera'#231#227'o'
        'M'#234's da opera'#231#227'o'
        'Valor negociado')
      TabOrder = 0
      OnClick = RGOrdem1Click
    end
    object RGOrdem2: TRadioGroup
      Left = 119
      Top = 4
      Width = 117
      Height = 80
      Caption = ' Ordem B: '
      ItemIndex = 3
      Items.Strings = (
        'Nome do cliente'
        'Data da opera'#231#227'o'
        'M'#234's da opera'#231#227'o'
        'Valor negociado')
      TabOrder = 1
      OnClick = RGOrdem2Click
    end
    object RGOrdem3: TRadioGroup
      Left = 234
      Top = 4
      Width = 117
      Height = 80
      Caption = ' Ordem C: '
      ItemIndex = 1
      Items.Strings = (
        'Nome do cliente'
        'Data da opera'#231#227'o'
        'M'#234's da opera'#231#227'o'
        'Valor negociado')
      TabOrder = 2
      OnClick = RGOrdem3Click
    end
    object RGGrupos: TRadioGroup
      Left = 352
      Top = 4
      Width = 237
      Height = 39
      Caption = ' Impress'#227'o: '
      Columns = 3
      ItemIndex = 1
      Items.Strings = (
        'Simples'
        '1 N'#237'vel'
        '2 N'#237'veis')
      TabOrder = 3
      OnClick = RGGruposClick
    end
    object RGSintetico: TRadioGroup
      Left = 352
      Top = 45
      Width = 237
      Height = 39
      Caption = ' Tipo: '
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'Anal'#237'tico'
        'Sint'#233'tico')
      TabOrder = 4
      OnClick = RGSinteticoClick
    end
    object RGAgrup3: TRadioGroup
      Left = 234
      Top = 108
      Width = 117
      Height = 64
      Caption = ' 3'#186' Agrupamento: '
      ItemIndex = 1
      Items.Strings = (
        'Nome do cliente'
        'Data da opera'#231#227'o'
        'M'#234's da opera'#231#227'o')
      TabOrder = 5
      OnClick = RGAgrup3Click
    end
    object RGAgrup2: TRadioGroup
      Left = 119
      Top = 108
      Width = 117
      Height = 64
      Caption = ' 2'#186' Agrupamento: '
      ItemIndex = 0
      Items.Strings = (
        'Nome do cliente'
        'Data da opera'#231#227'o'
        'M'#234's da opera'#231#227'o')
      TabOrder = 6
      OnClick = RGAgrup2Click
    end
    object RGAgrup1: TRadioGroup
      Left = 4
      Top = 108
      Width = 117
      Height = 64
      Caption = ' 1'#186' Agrupamento: '
      ItemIndex = 2
      Items.Strings = (
        'Nome do cliente'
        'Data da opera'#231#227'o'
        'M'#234's da opera'#231#227'o')
      TabOrder = 7
      OnClick = RGAgrup1Click
    end
    object CkDescA: TCheckBox
      Left = 12
      Top = 84
      Width = 97
      Height = 17
      Caption = 'Descentente'
      TabOrder = 8
      OnClick = CkDescAClick
    end
    object CkDescB: TCheckBox
      Left = 127
      Top = 84
      Width = 97
      Height = 17
      Caption = 'Descentente'
      Checked = True
      State = cbChecked
      TabOrder = 9
      OnClick = CkDescBClick
    end
    object CkDescC: TCheckBox
      Left = 242
      Top = 84
      Width = 97
      Height = 17
      Caption = 'Descentente'
      TabOrder = 10
      OnClick = CkDescCClick
    end
    object CkDescG: TCheckBox
      Left = 354
      Top = 84
      Width = 135
      Height = 17
      Caption = 'Descentente na grade'
      TabOrder = 11
      OnClick = CkDescGClick
    end
    object CkRepasse: TCheckBox
      Left = 360
      Top = 152
      Width = 97
      Height = 17
      Caption = 'Imprimir repasse.'
      TabOrder = 12
    end
  end
  object DBGrid2: TDBGrid
    Left = 35
    Top = 320
    Width = 746
    Height = 112
    DataSource = DsOpera
    TabOrder = 5
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Visible = False
    OnTitleClick = DBGrid1TitleClick
    Columns = <
      item
        Expanded = False
        FieldName = 'NOMECLIENTE'
        Title.Caption = 'Cliente'
        Width = 140
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TOTAL'
        Title.Caption = 'Val.Negoc.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PROPRIO'
        Title.Caption = 'Val.Pr'#243'prio'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'TXCOMPRAT'
        Title.Caption = 'Tx.Compra'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'VALVALOREMT'
        Title.Caption = 'Ad Valorem'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MARGEM_TODOS'
        Title.Caption = 'Marg. todos'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'REPAS_V'
        Title.Caption = 'Repasse'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'MARGEM_BRUTA'
        Title.Caption = 'Marg. l'#237'q.'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PIS_T_VAL'
        Title.Caption = 'PIS'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'ISS_VAL'
        Title.Caption = 'ISS'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'COFINS_T_VAL'
        Title.Caption = 'Cofins'
        Visible = True
      end>
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo'
      'FROM entidades'
      'WHERE Cliente1='#39'V'#39
      'ORDER BY NOMECLIENTE')
    Left = 224
    Top = 64
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 252
    Top = 64
  end
  object QrOpera: TmySQLQuery
    Database = Dmod.MyLocDatabase
    AfterOpen = QrOperaAfterOpen
    AfterClose = QrOperaAfterClose
    SQL.Strings = (
      'SELECT * FROM resliqfac')
    Left = 64
    Top = 348
    object QrOperaNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrOperaData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOperaLote: TIntegerField
      FieldName = 'Lote'
      DisplayFormat = '000000;-000000; '
    end
    object QrOperaCodigo: TIntegerField
      FieldName = 'Codigo'
      DisplayFormat = '000000;-000000; '
    end
    object QrOperaPROPRIO: TFloatField
      FieldName = 'PROPRIO'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOperaTXCOMPRAT: TFloatField
      FieldName = 'TXCOMPRAT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOperaVALVALOREMT: TFloatField
      FieldName = 'VALVALOREMT'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOperaMARGEM_TODOS: TFloatField
      FieldName = 'MARGEM_TODOS'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOperaREPAS_V: TFloatField
      FieldName = 'REPAS_V'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOperaMARGEM_BRUTA: TFloatField
      FieldName = 'MARGEM_BRUTA'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOperaNREP_T: TFloatField
      FieldName = 'NREP_T'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOperaPRO_COL: TFloatField
      FieldName = 'PRO_COL'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOperaMARGEM_COL: TFloatField
      FieldName = 'MARGEM_COL'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOperaPIS_T_VAL: TFloatField
      FieldName = 'PIS_T_VAL'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOperaISS_VAL: TFloatField
      FieldName = 'ISS_VAL'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOperaCOFINS_T_VAL: TFloatField
      FieldName = 'COFINS_T_VAL'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOperaTOTAL: TFloatField
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrOperaNOMEMES: TWideStringField
      FieldName = 'NOMEMES'
    end
  end
  object QrRepassados: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT loi.Codigo, SUM(rit.Valor) Valor,'
      'SUM(rit.JurosV) JurosV '
      'FROM repasits rit'
      'LEFT JOIN lotesits loi ON loi.Controle=rit.Origem'
      'GROUP BY loi.Codigo'
      '')
    Left = 36
    Top = 348
    object QrRepassadosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRepassadosJurosV: TFloatField
      FieldName = 'JurosV'
    end
    object QrRepassadosValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrEmLot1: TmySQLQuery
    Database = Dmod.MyDB1
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 8
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot1IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot1IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot1ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot1ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot1PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot1PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot1TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot1ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot1AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrEmLot2: TmySQLQuery
    Database = Dmod.MyDB2
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 36
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot2IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot2IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot2ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot2ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot2PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot2PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot2TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot2ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot2AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrEmLot3: TmySQLQuery
    Database = Dmod.MyDB3
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 64
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot3IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot3IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot3ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot3ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot3PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot3PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot3TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot3ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot3AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrEmLot4: TmySQLQuery
    Database = Dmod.MyDB4
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 92
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot4IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot4IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot4ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot4ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot4PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot4PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot4TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot4ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot4Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot4AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrEmLot5: TmySQLQuery
    Database = Dmod.MyDB5
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 120
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot5IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot5IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot5ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot5ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot5PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot5PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot5TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot5ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot5Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot5AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrEmLot6: TmySQLQuery
    Database = Dmod.MyDB6
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 144
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot6IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot6IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot6ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot6ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot6PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot6PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot6TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot6ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot6Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot6AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrColigado: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECOLIGADO'
      'FROM entidades'
      'WHERE Fornece2="V"')
    Left = 520
    Top = 61
    object QrColigadoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrColigadoNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
  end
  object DsColigado: TDataSource
    DataSet = QrColigado
    Left = 548
    Top = 61
  end
  object QrNRep: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT loi.Codigo, SUM(rit.Valor) Valor,'
      'SUM(rit.JurosV) JurosV '
      'FROM repasits rit'
      'LEFT JOIN repas    rep ON rep.Codigo=rit.Codigo'
      'LEFT JOIN lotesits loi ON loi.Controle=rit.Origem'
      'WHERE rep.Coligado=:P0'
      'GROUP BY loi.Codigo')
    Left = 8
    Top = 348
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrNRepCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNRepValor: TFloatField
      FieldName = 'Valor'
    end
    object QrNRepJurosV: TFloatField
      FieldName = 'JurosV'
    end
  end
  object DsOpera: TDataSource
    DataSet = QrOpera
    Left = 92
    Top = 348
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPesqCalcFields
    SQL.Strings = (
      'SELECT MONTH(lo.Data)+YEAR(lo.Data)*100+0.00  Mes, '
      'lo.*, CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLIENTE'
      'FROM lotes lo'
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente'
      'WHERE lo.data BETWEEN :P0 AND :P1'
      'AND (TxCompra+ValValorem)>=0.01'
      'ORDER BY lo.Data, NOMECLIENTE, lo.Lote')
    Left = 64
    Top = 320
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPesqNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPesqTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrPesqSpread: TSmallintField
      FieldName = 'Spread'
      Required = True
    end
    object QrPesqCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrPesqLote: TSmallintField
      FieldName = 'Lote'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrPesqData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqTotal: TFloatField
      FieldName = 'Total'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqDias: TFloatField
      FieldName = 'Dias'
      Required = True
    end
    object QrPesqPeCompra: TFloatField
      FieldName = 'PeCompra'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqTxCompra: TFloatField
      FieldName = 'TxCompra'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqValValorem: TFloatField
      FieldName = 'ValValorem'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqAdValorem: TFloatField
      FieldName = 'AdValorem'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqIOC: TFloatField
      FieldName = 'IOC'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqCPMF: TFloatField
      FieldName = 'CPMF'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqTipoAdV: TIntegerField
      FieldName = 'TipoAdV'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesqIRRF: TFloatField
      FieldName = 'IRRF'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqIRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqISS: TFloatField
      FieldName = 'ISS'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqISS_Val: TFloatField
      FieldName = 'ISS_Val'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqPIS_R: TFloatField
      FieldName = 'PIS_R'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqPIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPesqDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPesqDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPesqUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPesqUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPesqTarifas: TFloatField
      FieldName = 'Tarifas'
      Required = True
    end
    object QrPesqOcorP: TFloatField
      FieldName = 'OcorP'
      Required = True
    end
    object QrPesqCOFINS_R: TFloatField
      FieldName = 'COFINS_R'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqCOFINS_R_Val: TFloatField
      FieldName = 'COFINS_R_Val'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqPIS: TFloatField
      FieldName = 'PIS'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqPIS_Val: TFloatField
      FieldName = 'PIS_Val'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqCOFINS: TFloatField
      FieldName = 'COFINS'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqCOFINS_Val: TFloatField
      FieldName = 'COFINS_Val'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqPIS_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PIS_TOTAL'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesqCOFINS_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'COFINS_TOTAL'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesqIOC_VAL: TFloatField
      FieldName = 'IOC_VAL'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqCPMF_VAL: TFloatField
      FieldName = 'CPMF_VAL'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqMaxVencto: TDateField
      FieldName = 'MaxVencto'
      Required = True
    end
    object QrPesqCHDevPg: TFloatField
      FieldName = 'CHDevPg'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqMINTC: TFloatField
      FieldName = 'MINTC'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqMINAV: TFloatField
      FieldName = 'MINAV'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqMINTC_AM: TFloatField
      FieldName = 'MINTC_AM'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqPIS_T_Val: TFloatField
      FieldName = 'PIS_T_Val'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqCOFINS_T_Val: TFloatField
      FieldName = 'COFINS_T_Val'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqPgLiq: TFloatField
      FieldName = 'PgLiq'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqDUDevPg: TFloatField
      FieldName = 'DUDevPg'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqNF: TIntegerField
      FieldName = 'NF'
      Required = True
      DisplayFormat = '000000;-000000; '
    end
    object QrPesqItens: TIntegerField
      FieldName = 'Itens'
      Required = True
    end
    object QrPesqREPAS_V: TFloatField
      FieldKind = fkLookup
      FieldName = 'REPAS_V'
      LookupDataSet = QrRepassados
      LookupKeyFields = 'Codigo'
      LookupResultField = 'JurosV'
      KeyFields = 'Codigo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Lookup = True
    end
    object QrPesqREPAS_T: TFloatField
      FieldKind = fkLookup
      FieldName = 'REPAS_T'
      LookupDataSet = QrRepassados
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Valor'
      KeyFields = 'Codigo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Lookup = True
    end
    object QrPesqTXCOMPRAT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TXCOMPRAT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesqVALVALOREMT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALVALOREMT'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesqMARGEM_BRUTA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MARGEM_BRUTA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesqMARGEM_TODOS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MARGEM_TODOS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesqPROPRIO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PROPRIO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesqNREP_V: TFloatField
      FieldKind = fkLookup
      FieldName = 'NREP_V'
      LookupDataSet = QrNRep
      LookupKeyFields = 'Codigo'
      LookupResultField = 'JurosV'
      KeyFields = 'Codigo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Lookup = True
    end
    object QrPesqNREP_T: TFloatField
      FieldKind = fkLookup
      FieldName = 'NREP_T'
      LookupDataSet = QrNRep
      LookupKeyFields = 'Codigo'
      LookupResultField = 'Valor'
      KeyFields = 'Codigo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Lookup = True
    end
    object QrPesqPRO_COL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRO_COL'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesqMes: TFloatField
      FieldName = 'Mes'
    end
    object QrPesqNOMEMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMES'
      Calculated = True
    end
    object QrPesqMARGEM_COL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MARGEM_COL'
      Calculated = True
    end
  end
  object frxOperaCli: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.345689733800000000
    ReportOptions.LastChange = 39718.345689733800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Sub1Dias  , Sub2Dias  ,                   '
      '  Sub1Total , Sub2Total ,                       '
      '  Sub1TTC   , Sub2TTC   : Extended;'
      '  '
      'begin'
      '  if <VFR_ORD1> > 0 then'
      '  GH1.Visible := True else GH1.Visible := False;'
      '  if <VFR_ORD1> > 0 then'
      '  GF1.Visible := True else GF1.Visible := False;'
      '  //'
      '  //'
      
        '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsOpera."NOMECLIEN' +
        'TE"'#39';'
      '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsOpera."Data"'#39';'
      
        '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsOpera."NOMEMES"'#39 +
        ';'
      '  if <VFR_ORD1> = 4 then GH1.Condition := '#39'frxDsOpera."Total"'#39';'
      '  //'
      '  Sub1Dias  := 0;'
      '  Sub1Total := 0;'
      '  Sub1TTC   := 0;'
      '  '
      '  if <VFR_ORD2> > 0 then'
      '  GH2.Visible := True else GH2.Visible := False;'
      '  if <VFR_ORD2> > 0 then'
      '  GF2.Visible := True else GF2.Visible := False;'
      '  //'
      
        '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsOpera."NOMECLIEN' +
        'TE"'#39';'
      '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsOpera."Data"'#39';'
      
        '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsOpera."NOMEMES"'#39 +
        ';'
      '  if <VFR_ORD2> = 4 then GH2.Condition := '#39'frxDsOpera."Total"'#39';'
      '  //'
      '  Sub2Dias  := 0;'
      '  Sub2Total := 0;'
      '  Sub2TTC   := 0;'
      ''
      ''
      '  if <VFR_SINTETICO> then'
      '  begin'
      
        '    Memo50.Visible := False;                                    ' +
        '              '
      
        '    Memo51.Visible := False;                                    ' +
        '              '
      '    Memo52.Visible := False;'
      
        '    Memo12.Left    := Memo50.Left;                              ' +
        '                                '
      
        '    Memo12.Width   := Memo50.Width + Memo51.Width + Memo52.Width' +
        ' + Memo12.Width;                                                ' +
        '              '
      '    //    '
      
        '    Memo54.Visible := False;                                    ' +
        '              '
      
        '    Memo55.Visible := False;                                    ' +
        '              '
      
        '    Memo56.Visible := False;                                    ' +
        '              '
      
        '    Memo10.Left    := Memo54.Left;                              ' +
        '                                '
      
        '    Memo10.Width   := Memo54.Width + Memo55.Width + Memo56.Width' +
        ' + Memo10.Width;                                                ' +
        '              '
      '  end;'
      'end.')
    OnGetValue = frxOperaCliGetValue
    Left = 484
    Top = 372
    Datasets = <
      item
        DataSet = Dmod.frxDsControle
        DataSetName = 'frxDsControle'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsOpera
        DataSetName = 'frxDsOpera'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 554.000000000000000000
          Top = 4.661410000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 2.000000000000000000
          Top = 24.661410000000000000
          Width = 720.000000000000000000
          Height = 26.000000000000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 536.693260000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 585.779530000000000000
          Top = 7.653059999999980000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 673.779530000000000000
          Top = 7.653059999999980000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 75.590551180000000000
        Top = 98.267780000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'PageHeader1OnBeforePrint'
        object Memo19: TfrxMemoView
          Left = 10.000000000000000000
          Top = 2.614099999999990000
          Width = 700.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RESULTADO DAS OPERA'#199#213'ES REALIZADAS')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 109.606370000000000000
          Top = 49.133858270000000000
          Width = 177.637724490000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VFR_LA3TITU]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 287.244094490000000000
          Top = 49.133858267716500000
          Width = 60.472440940000000000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'negociado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 408.188976377953000000
          Top = 49.133858267716500000
          Width = 49.133858267716500000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fator de'
            'compra')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 457.322834645669000000
          Top = 49.133858267716500000
          Width = 41.574803149606300000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ad'
            'Valorem')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 498.897637800000000000
          Top = 49.133858267716500000
          Width = 41.574803150000000000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsControle."Moeda"]'
            'PIS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 540.472440944882000000
          Top = 49.133858267716500000
          Width = 41.574803149606300000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsControle."Moeda"]'
            'ISS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 58.000000000000000000
          Top = 26.614100000000000000
          Width = 412.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 10.000000000000000000
          Top = 26.614100000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 522.000000000000000000
          Top = 26.614100000000000000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 474.000000000000000000
          Top = 26.614100000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 582.000000000000000000
          Top = 49.133858267716500000
          Width = 37.795275590551200000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsControle."Moeda"]'
            'COFINS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 668.976377952756000000
          Top = 49.133858267716500000
          Width = 49.133858267716500000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Margem'
            'L'#237'quida')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 619.842519685039000000
          Top = 49.133858267716500000
          Width = 49.133858267716500000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Margem'
            'Todos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 347.716535433071000000
          Top = 49.133858267716500000
          Width = 60.472440944881890000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Pr'#243'prio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 49.133858270000000000
          Width = 34.015748030000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data'
            'opera'#231#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 34.015770000000000000
          Top = 49.133890000000000000
          Width = 37.795275590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 71.811070000000000000
          Top = 49.133890000000000000
          Width = 37.795275590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        Height = 13.228378190000000000
        Top = 317.480520000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsOpera
        DataSetName = 'frxDsOpera'
        RowCount = 0
        object Memo10: TfrxMemoView
          Left = 109.606370000000000000
          Width = 177.637724490000000000
          Height = 13.228346456692900000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VFR_LA3NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 287.244094488189000000
          Width = 60.472440944881890000
          Height = 13.228346456692900000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."Total"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 408.188976377953000000
          Width = 49.133858267716500000
          Height = 13.228346456692900000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."TXCOMPRAT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 457.322834645669000000
          Width = 41.574803149606300000
          Height = 13.228346456692900000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."VALVALOREMT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 498.897637795276000000
          Width = 41.574803149606300000
          Height = 13.228346456692900000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."PIS_T_VAL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 582.000000000000000000
          Width = 37.795275590551200000
          Height = 13.228346456692900000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."COFINS_T_VAL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 540.472440944882000000
          Width = 41.574803149606300000
          Height = 13.228346456692900000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."ISS_Val"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 668.976377950000000000
          Width = 49.133858270000000000
          Height = 13.228346456692900000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."MARGEM_BRUTA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 619.842519685039000000
          Width = 49.133858267716500000
          Height = 13.228346456692900000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."MARGEM_TODOS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 347.716535433071000000
          Width = 60.472440944881890000
          Height = 13.228346456692900000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."PROPRIO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo54: TfrxMemoView
          Width = 34.015748030000000000
          Height = 13.228346460000000000
          DataField = 'Data'
          DataSet = frxDsOpera
          DataSetName = 'frxDsOpera'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOpera."Data"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo55: TfrxMemoView
          Left = 34.015770000000000000
          Top = 0.000031729999989238
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsOpera
          DataSetName = 'frxDsOpera'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsOpera."Codigo">)]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo56: TfrxMemoView
          Left = 71.811070000000000000
          Top = 0.000031729999989238
          Width = 37.795275590000000000
          Height = 13.228346460000000000
          DataSet = frxDsOpera
          DataSetName = 'frxDsOpera'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsOpera."Lote">)]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 23.779530000000000000
        Top = 491.338900000000000000
        Width = 718.110700000000000000
        object Memo36: TfrxMemoView
          Top = 3.779527559999960000
          Width = 287.244096930000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 287.244280000000000000
          Top = 3.779527559999960000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 408.189161890000000000
          Top = 3.779527559999960000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."TXCOMPRAT">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 457.323020160000000000
          Top = 3.779527559999960000
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."VALVALOREMT">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 498.897823310000000000
          Top = 3.779527559999960000
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."PIS_T_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 582.000185510000000000
          Top = 3.779527559999960000
          Width = 37.795275590000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."COFINS_T_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 540.472626450000000000
          Top = 3.779527559999960000
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."ISS_Val">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 619.842705200000000000
          Top = 3.779527559999960000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."MARGEM_TODOS">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 668.976563460000000000
          Top = 3.779527559999960000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."MARGEM_BRUTA">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 347.716720940000000000
          Top = 3.779527559999960000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."PROPRIO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 234.330860000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsOpera."NOMECLIENTE"'
        object Memo34: TfrxMemoView
          Width = 718.110236220000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 275.905690000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsOpera."NOMEMES"'
        object Memo46: TfrxMemoView
          Left = 37.795300000000000000
          Width = 718.110236220000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 355.275820000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo47: TfrxMemoView
          Left = 37.795300000000000000
          Top = 7.559055118110220000
          Width = 249.448794490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA2NOME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          Left = 287.244094490000000000
          Top = 7.559055119999980000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 408.188976380000000000
          Top = 7.559055119999980000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."TXCOMPRAT">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 457.322834650000000000
          Top = 7.559055119999980000
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."VALVALOREMT">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 498.897637800000000000
          Top = 7.559055119999980000
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."PIS_T_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 582.000000000000000000
          Top = 7.559055119999980000
          Width = 37.795275590000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."COFINS_T_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 540.472440940000000000
          Top = 7.559055119999980000
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."ISS_Val">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 619.842519690000000000
          Top = 7.559055119999980000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."MARGEM_TODOS">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 668.976377950000000000
          Top = 7.559055119999980000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."MARGEM_BRUTA">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 347.716535430000000000
          Top = 7.559055119999980000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."PROPRIO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 404.409710000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo57: TfrxMemoView
          Top = 7.559055118110220000
          Width = 287.244094490000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA1NOME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 287.244280000000000000
          Top = 7.559059999999990000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 408.189161890000000000
          Top = 7.559059999999990000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."TXCOMPRAT">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 457.323020160000000000
          Top = 7.559059999999990000
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."VALVALOREMT">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 498.897823310000000000
          Top = 7.559059999999990000
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."PIS_T_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 582.000185510000000000
          Top = 7.559059999999990000
          Width = 37.795275590000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."COFINS_T_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 540.472626450000000000
          Top = 7.559059999999990000
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."ISS_Val">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 619.842705200000000000
          Top = 7.559059999999990000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."MARGEM_TODOS">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 668.976563460000000000
          Top = 7.559059999999990000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."MARGEM_BRUTA">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 347.716720940000000000
          Top = 7.559059999999990000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."PROPRIO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
  object frxDsOpera: TfrxDBDataset
    UserName = 'frxDsOpera'
    CloseDataSource = False
    DataSet = QrOpera
    BCDToCurrency = False
    Left = 540
    Top = 372
  end
  object frxOperaCol: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.409977928200000000
    ReportOptions.LastChange = 39718.409977928200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'var'
      '  Sub1Dias  , Sub2Dias  ,                   '
      '  Sub1Total , Sub2Total ,                       '
      '  Sub1TTC   , Sub2TTC   : Extended;'
      '  '
      'begin'
      '  if <VFR_ORD1> > 0 then'
      '  GH1.Visible := True else GH1.Visible := False;'
      '  if <VFR_ORD1> > 0 then'
      '  GF1.Visible := True else GF1.Visible := False;'
      '  //'
      '  //'
      
        '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsOpera."NOMECLIEN' +
        'TE"'#39';'
      '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsOpera."Data"'#39';'
      
        '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsOpera."NOMEMES"'#39 +
        ';'
      '  if <VFR_ORD1> = 4 then GH1.Condition := '#39'frxDsOpera."Total"'#39';'
      '  //'
      '  Sub1Dias  := 0;'
      '  Sub1Total := 0;'
      '  Sub1TTC   := 0;'
      '  '
      '  if <VFR_ORD2> > 0 then'
      '  GH2.Visible := True else GH2.Visible := False;'
      '  if <VFR_ORD2> > 0 then'
      '  GF2.Visible := True else GF2.Visible := False;'
      '  //'
      
        '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsOpera."NOMECLIEN' +
        'TE"'#39';'
      '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsOpera."Data"'#39';'
      
        '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsOpera."NOMEMES"'#39 +
        ';'
      '  if <VFR_ORD2> = 4 then GH2.Condition := '#39'frxDsOpera."Total"'#39';'
      '  //'
      '  Sub2Dias  := 0;'
      '  Sub2Total := 0;'
      '  Sub2TTC   := 0;'
      ''
      ''
      '  if <VFR_SINTETICO> then'
      '  begin'
      
        '    Memo75.Visible := False;                                    ' +
        '              '
      
        '    Memo76.Visible := False;                                    ' +
        '              '
      '    Memo77.Visible := False;'
      
        '    Memo12.Left    := Memo75.Left;                              ' +
        '                                '
      
        '    Memo12.Width   := Memo75.Width + Memo76.Width + Memo77.Width' +
        ' + Memo12.Width;                                                ' +
        '              '
      '    //    '
      
        '    Memo78.Visible := False;                                    ' +
        '              '
      
        '    Memo79.Visible := False;                                    ' +
        '              '
      
        '    Memo80.Visible := False;                                    ' +
        '              '
      
        '    Memo10.Left    := Memo78.Left;                              ' +
        '                                '
      
        '    Memo10.Width   := Memo78.Width + Memo79.Width + Memo80.Width' +
        ' + Memo10.Width;                                                ' +
        '              '
      '  end;  '
      'end.')
    OnGetValue = frxOperaCliGetValue
    Left = 512
    Top = 372
    Datasets = <
      item
        DataSet = Dmod.frxDsControle
        DataSetName = 'frxDsControle'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsOpera
        DataSetName = 'frxDsOpera'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Memo32: TfrxMemoView
          Left = 822.000000000000000000
          Top = 4.661410000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 26.000000000000000000
          Top = 24.661410000000000000
          Width = 960.000000000000000000
          Height = 26.000000000000000000
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 540.472790000000000000
        Width = 1046.929810000000000000
        object Memo53: TfrxMemoView
          Left = 838.000000000000000000
          Top = 5.070500000000040000
          Width = 148.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de [TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 75.590551180000000000
        Top = 98.267780000000000000
        Width = 1046.929810000000000000
        object Memo19: TfrxMemoView
          Left = 26.000000000000000000
          Top = 2.614099999999990000
          Width = 960.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RESULTADO DAS OPERA'#199#213'ES REALIZADAS')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 132.283532910000000000
          Top = 49.133858270000000000
          Width = 207.873944960000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VFR_LA3TITU]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 340.157480314961000000
          Top = 49.133858267716500000
          Width = 64.251968503937000000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'negociado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 597.165354330000000000
          Top = 49.133858270000000000
          Width = 52.913385830000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fator de'
            'compra')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 650.000000000000000000
          Top = 49.133858270000000000
          Width = 49.133858270000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ad'
            'Valorem')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 699.212598425197100000
          Top = 49.133858267716500000
          Width = 45.354330708661400000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsControle."Moeda"]'
            'PIS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 744.566929133858000000
          Top = 49.133858267716500000
          Width = 45.354330708661400000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsControle."Moeda"]'
            'ISS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 74.000000000000000000
          Top = 26.614100000000000000
          Width = 344.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo5: TfrxMemoView
          Left = 26.000000000000000000
          Top = 26.614100000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 470.000000000000000000
          Top = 26.614100000000000000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 422.000000000000000000
          Top = 26.614100000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 789.779530000000000000
          Top = 49.133858267716500000
          Width = 45.354330708661400000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsControle."Moeda"]'
            'COFINS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 888.188976377953000000
          Top = 49.133858267716500000
          Width = 52.913385826771700000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Margem c/'
            'Coligado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo42: TfrxMemoView
          Left = 835.275590551181100000
          Top = 49.133858267716500000
          Width = 52.913385826771700000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Margem'
            'Todos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo11: TfrxMemoView
          Left = 404.409448818898000000
          Top = 49.133858267716500000
          Width = 64.251968503937000000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Pr'#243'prio')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 468.661417322835000000
          Top = 49.133858267716500000
          Width = 64.251968503937000000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Coligado')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo46: TfrxMemoView
          Left = 532.913385826772000000
          Top = 49.133858267716500000
          Width = 64.251968503937000000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Pr'#243'p+Col')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 714.000000000000000000
          Top = 26.614100000000000000
          Width = 272.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          Memo.UTF8W = (
            '[VARF_COLIGADO]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 662.000000000000000000
          Top = 26.614100000000000000
          Width = 52.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Coligado:')
          ParentFont = False
          WordWrap = False
        end
        object Memo56: TfrxMemoView
          Left = 941.102362204724000000
          Top = 49.133858267716500000
          Width = 52.913385826771700000
          Height = 26.456692913385800000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Margem'
            'L'#237'quida')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo75: TfrxMemoView
          Top = 49.133890000000000000
          Width = 41.574808030000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data'
            'opera'#231#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo76: TfrxMemoView
          Left = 41.574830000000000000
          Top = 49.133921730000000000
          Width = 45.354335590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lote')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo77: TfrxMemoView
          Left = 86.929190000000000000
          Top = 49.133921730000000000
          Width = 45.354335590000000000
          Height = 26.456692910000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        Height = 17.000000000000000000
        Top = 317.480520000000000000
        Width = 1046.929810000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsOpera
        DataSetName = 'frxDsOpera'
        RowCount = 0
        object Memo14: TfrxMemoView
          Left = 340.157480314961000000
          Width = 64.251968503937000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."Total"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 597.165354330709000000
          Width = 52.913385826771700000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."TXCOMPRAT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 650.000000000000000000
          Width = 49.133858267716500000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."VALVALOREMT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 699.212598425197100000
          Width = 45.354330708661400000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."PIS_T_VAL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 789.779530000000000000
          Width = 45.354330708661400000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."COFINS_T_VAL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 744.566929133858000000
          Width = 45.354330708661400000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."ISS_Val"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo2: TfrxMemoView
          Left = 888.188976377953000000
          Width = 52.913385826771700000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."MARGEM_COL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 835.275590551181100000
          Width = 52.913385826771700000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."MARGEM_TODOS"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 404.409448818898000000
          Width = 64.251968503937000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."PROPRIO"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 468.661417322835000000
          Width = 64.251968503937000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."NREP_T"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 532.913385826772000000
          Width = 64.251968503937000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."PRO_COL"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo57: TfrxMemoView
          Left = 941.102362204724000000
          Width = 52.913385826771700000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOpera."MARGEM_BRUTA"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 132.283532910000000000
          Width = 207.873944960000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VFR_LA3NOME]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo78: TfrxMemoView
          Width = 41.574808030000000000
          Height = 17.007874020000000000
          DataField = 'Data'
          DataSet = frxDsOpera
          DataSetName = 'frxDsOpera'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOpera."Data"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo79: TfrxMemoView
          Left = 41.574830000000000000
          Top = 0.000031729999989238
          Width = 45.354335590000000000
          Height = 17.007874020000000000
          DataSet = frxDsOpera
          DataSetName = 'frxDsOpera'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsOpera."Codigo">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo80: TfrxMemoView
          Left = 86.929190000000000000
          Top = 0.000031729999989238
          Width = 45.354335590000000000
          Height = 17.007874020000000000
          DataSet = frxDsOpera
          DataSetName = 'frxDsOpera'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsOpera."Lote">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 22.559060000000000000
        Top = 495.118430000000000000
        Width = 1046.929810000000000000
        object Memo36: TfrxMemoView
          Left = 26.456692913385800000
          Top = 3.779527560000020000
          Width = 313.700787401575000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779530000000020000
          Width = 64.251968500000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo41: TfrxMemoView
          Left = 597.165574020000000000
          Top = 3.779530000000020000
          Width = 52.913385830000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."TXCOMPRAT">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo44: TfrxMemoView
          Left = 650.000219690000000000
          Top = 3.779530000000020000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."VALVALOREMT">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo45: TfrxMemoView
          Left = 699.212818120000000000
          Top = 3.779530000000020000
          Width = 45.354330710000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."PIS_T_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 789.779749690000000000
          Top = 3.779530000000020000
          Width = 45.354330710000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."COFINS_T_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo58: TfrxMemoView
          Left = 744.567148820000000000
          Top = 3.779530000000020000
          Width = 45.354330710000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."ISS_Val">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo59: TfrxMemoView
          Left = 835.275810240000000000
          Top = 3.779530000000020000
          Width = 52.913385830000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."MARGEM_TODOS">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo60: TfrxMemoView
          Left = 888.189196070000000000
          Top = 3.779530000000020000
          Width = 52.913385830000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."MARGEM_COL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Left = 404.409668510000000000
          Top = 3.779530000000020000
          Width = 64.251968500000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."PROPRIO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo69: TfrxMemoView
          Left = 468.661637010000000000
          Top = 3.779530000000020000
          Width = 64.251968500000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."NREP_T">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo73: TfrxMemoView
          Left = 532.913605520000000000
          Top = 3.779530000000020000
          Width = 64.251968500000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."PRO_COL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo74: TfrxMemoView
          Left = 941.102581890000000000
          Top = 3.779530000000020000
          Width = 52.913385830000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."MARGEM_BRUTA">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 234.330860000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsOpera."NOMECLIENTE"'
        object Memo31: TfrxMemoView
          Left = 26.456692913385800000
          Width = 960.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 275.905690000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsOpera."NOMEMES"'
        object Memo52: TfrxMemoView
          Left = 58.000000000000000000
          Width = 928.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 359.055350000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo54: TfrxMemoView
          Left = 56.692913385826800000
          Top = 3.810760000000020000
          Width = 283.464566929134000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA2NOME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo61: TfrxMemoView
          Left = 340.157480310000000000
          Top = 3.810760000000020000
          Width = 64.251968500000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo62: TfrxMemoView
          Left = 597.165354330000000000
          Top = 3.810760000000020000
          Width = 52.913385830000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."TXCOMPRAT">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo63: TfrxMemoView
          Left = 650.000000000000000000
          Top = 3.810760000000020000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."VALVALOREMT">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 699.212598430000000000
          Top = 3.810760000000020000
          Width = 45.354330710000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."PIS_T_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 789.779530000000000000
          Top = 3.810760000000020000
          Width = 45.354330710000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."COFINS_T_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 744.566929130000000000
          Top = 3.810760000000020000
          Width = 45.354330710000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."ISS_Val">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo70: TfrxMemoView
          Left = 835.275590550000000000
          Top = 3.810760000000020000
          Width = 52.913385830000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."MARGEM_TODOS">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo71: TfrxMemoView
          Left = 888.188976380000000000
          Top = 3.810760000000020000
          Width = 52.913385830000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."MARGEM_COL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo72: TfrxMemoView
          Left = 404.409448820000000000
          Top = 3.810760000000020000
          Width = 64.251968500000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."PROPRIO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo84: TfrxMemoView
          Left = 468.661417320000000000
          Top = 3.810760000000020000
          Width = 64.251968500000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."NREP_T">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo85: TfrxMemoView
          Left = 532.913385830000000000
          Top = 3.810760000000020000
          Width = 64.251968500000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."PRO_COL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo86: TfrxMemoView
          Left = 941.102362200000000000
          Top = 3.810760000000020000
          Width = 52.913385830000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."MARGEM_BRUTA">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 408.189240000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo55: TfrxMemoView
          Left = 26.456692910000000000
          Top = 3.779527559055110000
          Width = 313.700784960000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total [VFR_LA1NOME]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 340.157700000000000000
          Top = 3.779527559055110000
          Width = 64.251968500000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."Total">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 597.165574020000000000
          Top = 3.779527559055110000
          Width = 52.913385830000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."TXCOMPRAT">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo26: TfrxMemoView
          Left = 650.000219690000000000
          Top = 3.779527559055110000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."VALVALOREMT">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 699.212818120000000000
          Top = 3.779527559055110000
          Width = 45.354330710000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."PIS_T_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 789.779749690000000000
          Top = 3.779527559055110000
          Width = 45.354330710000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."COFINS_T_VAL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 744.567148820000000000
          Top = 3.779527559055110000
          Width = 45.354330710000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."ISS_Val">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo33: TfrxMemoView
          Left = 835.275810240000000000
          Top = 3.779527559055110000
          Width = 52.913385830000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."MARGEM_TODOS">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 888.189196070000000000
          Top = 3.779527559055110000
          Width = 52.913385830000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."MARGEM_COL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 404.409668510000000000
          Top = 3.779527559055110000
          Width = 64.251968500000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."PROPRIO">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 468.661637010000000000
          Top = 3.779527559055110000
          Width = 64.251968500000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."NREP_T">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 532.913605520000000000
          Top = 3.779527559055110000
          Width = 64.251968500000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."PRO_COL">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo39: TfrxMemoView
          Left = 941.102581890000000000
          Top = 3.779527559055110000
          Width = 52.913385830000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOpera."MARGEM_BRUTA">)]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
      end
    end
  end
end
