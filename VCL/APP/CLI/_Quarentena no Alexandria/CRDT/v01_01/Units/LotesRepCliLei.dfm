object FmLotesRepCliLei: TFmLotesRepCliLei
  Left = 360
  Top = 177
  Caption = 'Pagamento com Cheques de Terceiros'
  ClientHeight = 496
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Pagar'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel1: TPanel
      Left = 676
      Top = 1
      Width = 115
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 6
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BitBtn1: TBitBtn
      Left = 360
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Depositar'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BitBtn1Click
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Pagamento com Cheques de Terceiros'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 400
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 792
      Height = 400
      Align = alClient
      TabOrder = 0
      object DBG1: TDBGrid
        Left = 1
        Top = 1
        Width = 790
        Height = 301
        Align = alClient
        DataSource = DsCheques
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBG1CellClick
        OnKeyDown = DBG1KeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'Vencto'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cheque'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Banco'
            Title.Caption = 'Bco'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Agencia'
            Title.Caption = 'Agen.'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Conta'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Emitente'
            Width = 208
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECLI'
            Title.Caption = 'Cliente'
            Width = 208
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Lote'
            Title.Caption = 'Border'#244
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Lote'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Visible = True
          end>
      end
      object Panel9: TPanel
        Left = 1
        Top = 302
        Width = 790
        Height = 97
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 1
        TabStop = True
        object Panel10: TPanel
          Left = 0
          Top = 0
          Width = 649
          Height = 97
          Align = alLeft
          TabOrder = 0
          object Label43: TLabel
            Left = 4
            Top = 4
            Width = 143
            Height = 13
            Caption = 'Leitura pela banda magn'#233'tica:'
          end
          object Label49: TLabel
            Left = 4
            Top = 44
            Width = 129
            Height = 13
            Caption = 'CGC / CPF [F4 - Pesquisa]:'
          end
          object Label50: TLabel
            Left = 164
            Top = 44
            Width = 44
            Height = 13
            Caption = 'Emitente:'
          end
          object Label44: TLabel
            Left = 256
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Com.:'
          end
          object Label45: TLabel
            Left = 288
            Top = 4
            Width = 22
            Height = 13
            Caption = 'Bco:'
          end
          object Label46: TLabel
            Left = 320
            Top = 4
            Width = 34
            Height = 13
            Caption = 'Ag'#234'nc:'
          end
          object Label47: TLabel
            Left = 360
            Top = 4
            Width = 55
            Height = 13
            Caption = 'Conta corr.:'
          end
          object Label48: TLabel
            Left = 436
            Top = 4
            Width = 45
            Height = 13
            Caption = 'N'#186' cheq.:'
          end
          object Label42: TLabel
            Left = 492
            Top = 4
            Width = 27
            Height = 13
            Caption = 'Valor:'
          end
          object Label3: TLabel
            Left = 572
            Top = 4
            Width = 59
            Height = 13
            Caption = 'Vencimento:'
          end
          object EdCMC_7_1: TEdit
            Left = 132
            Top = 16
            Width = 121
            Height = 21
            MaxLength = 30
            TabOrder = 0
            Visible = False
            OnChange = EdCMC_7_1Change
          end
          object EdCPF_2: TdmkEdit
            Left = 4
            Top = 60
            Width = 157
            Height = 21
            Alignment = taCenter
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdEmitente2: TdmkEdit
            Left = 164
            Top = 60
            Width = 481
            Height = 21
            CharCase = ecUpperCase
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdRegiaoCompe: TdmkEdit
            Left = 256
            Top = 20
            Width = 29
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdBanco: TdmkEdit
            Left = 288
            Top = 20
            Width = 29
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 4
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdAgencia: TdmkEdit
            Left = 320
            Top = 20
            Width = 37
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 5
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 4
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdConta: TdmkEdit
            Left = 360
            Top = 20
            Width = 73
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 6
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 10
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdRealCC: TdmkEdit
            Left = 360
            Top = 40
            Width = 73
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 10
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0000000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdCheque: TdmkEdit
            Left = 436
            Top = 20
            Width = 53
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 8
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdValor: TdmkEdit
            Left = 492
            Top = 20
            Width = 77
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 9
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdVencto: TdmkEdit
            Left = 572
            Top = 20
            Width = 73
            Height = 21
            Alignment = taRightJustify
            Enabled = False
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 10
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 6
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
          end
          object EdBanda: TdmkEdit
            Left = 4
            Top = 20
            Width = 249
            Height = 21
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            MaxLength = 34
            ParentFont = False
            TabOrder = 11
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnChange = EdBandaChange
            OnKeyDown = EdBandaKeyDown
          end
        end
        object Panel8: TPanel
          Left = 649
          Top = 0
          Width = 141
          Height = 97
          Align = alClient
          Enabled = False
          TabOrder = 1
          object Label24: TLabel
            Left = 6
            Top = 4
            Width = 95
            Height = 13
            Caption = 'Soma selecionados:'
          end
          object Label1: TLabel
            Left = 6
            Top = 50
            Width = 9
            Height = 13
            Caption = '(-)'
          end
          object Label2: TLabel
            Left = 6
            Top = 76
            Width = 6
            Height = 13
            Caption = '='
          end
          object EdSoma: TdmkEdit
            Left = 6
            Top = 20
            Width = 97
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdSomaChange
          end
          object EdSaldo: TdmkEdit
            Left = 20
            Top = 72
            Width = 83
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdFalta: TDBEdit
            Left = 20
            Top = 46
            Width = 83
            Height = 21
            DataField = 'SALDOAPAGAR'
            DataSource = FmLotes1.DsLotes
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 2
            OnChange = EdFaltaChange
          end
        end
      end
    end
  end
  object QrCheques: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMECLI, lot.Lote, loi.*'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE lot.Tipo = 0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Depositado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.Vencto <= :P0')
    Left = 212
    Top = 68
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrChequesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrChequesControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrChequesComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrChequesBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrChequesAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrChequesConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrChequesCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrChequesCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrChequesEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrChequesBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
    end
    object QrChequesDesco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrChequesValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrChequesEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrChequesDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
    end
    object QrChequesDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
    end
    object QrChequesVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequesTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrChequesTxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrChequesTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrChequesVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrChequesVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrChequesDMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrChequesDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrChequesDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrChequesDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrChequesQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrChequesLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrChequesDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChequesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChequesUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrChequesUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrChequesPraca: TIntegerField
      FieldName = 'Praca'
      Required = True
    end
    object QrChequesBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
      Required = True
    end
    object QrChequesAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
      Required = True
    end
    object QrChequesTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrChequesTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrChequesTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrChequesData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrChequesProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrChequesProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrChequesRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrChequesDepositado: TSmallintField
      FieldName = 'Depositado'
      Required = True
    end
    object QrChequesValQuit: TFloatField
      FieldName = 'ValQuit'
      Required = True
    end
    object QrChequesValDeposito: TFloatField
      FieldName = 'ValDeposito'
      Required = True
    end
    object QrChequesTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrChequesAliIts: TIntegerField
      FieldName = 'AliIts'
      Required = True
    end
    object QrChequesAlinPgs: TIntegerField
      FieldName = 'AlinPgs'
      Required = True
    end
    object QrChequesNaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
      Required = True
    end
    object QrChequesReforcoCxa: TSmallintField
      FieldName = 'ReforcoCxa'
      Required = True
    end
    object QrChequesCartDep: TIntegerField
      FieldName = 'CartDep'
      Required = True
    end
    object QrChequesCobranca: TIntegerField
      FieldName = 'Cobranca'
      Required = True
    end
    object QrChequesRepCli: TIntegerField
      FieldName = 'RepCli'
      Required = True
    end
    object QrChequesNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrChequesLote: TIntegerField
      FieldName = 'Lote'
    end
  end
  object DsCheques: TDataSource
    DataSet = QrCheques
    Left = 240
    Top = 68
  end
  object Timer1: TTimer
    Interval = 300
    OnTimer = Timer1Timer
    Left = 692
    Top = 9
  end
end
