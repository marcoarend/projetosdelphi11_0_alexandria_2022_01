object FmEntiJur1Ocor: TFmEntiJur1Ocor
  Left = 482
  Top = 200
  Caption = 'Ocorr'#234'ncias Banc'#225'rias (CNAB)'
  ClientHeight = 236
  ClientWidth = 568
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 188
    Width = 568
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 456
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 568
    Height = 48
    Align = alTop
    Caption = 'Ocorr'#234'ncias Banc'#225'rias (CNAB)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 484
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 490
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 485
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 492
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 568
    Height = 140
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 28
      Top = 8
      Width = 55
      Height = 13
      Caption = 'Ocorr'#234'ncia:'
    end
    object Label4: TLabel
      Left = 464
      Top = 8
      Width = 53
      Height = 13
      Caption = 'Valor base:'
    end
    object EdOcor: TdmkEditCB
      Left = 28
      Top = 24
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdOcorChange
      DBLookupComboBox = CBOcor
    end
    object CBOcor: TdmkDBLookupComboBox
      Left = 88
      Top = 24
      Width = 373
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsOcorBank
      TabOrder = 1
      OnClick = CBOcorClick
      dmkEditCB = EdOcor
      UpdType = utYes
    end
    object RGFormaCNAB: TRadioGroup
      Left = 28
      Top = 49
      Width = 521
      Height = 72
      Caption = ' Forma de cobran'#231'a em arquivos remessa / retorno (CNAB240): '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o cobrar'
        'Cobrar o mesmo valor do banco'
        'Cobrar o valor base deste cadastro'
        'Cobrar o valor base do cadastro da ocorr'#234'ncia')
      TabOrder = 3
    end
    object EdBase: TdmkEdit
      Left = 464
      Top = 24
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
  end
  object QrOcorBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Base'
      'FROM ocorbank'
      'ORDER BY Nome'
      '')
    Left = 8
    Top = 8
    object QrOcorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrOcorBankBase: TFloatField
      FieldName = 'Base'
    end
  end
  object DsOcorBank: TDataSource
    DataSet = QrOcorBank
    Left = 36
    Top = 8
  end
end
