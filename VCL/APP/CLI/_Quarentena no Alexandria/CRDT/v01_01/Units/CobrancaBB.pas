unit CobrancaBB;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  Grids, DBGrids, Menus, Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, UnDmkProcFunc, UnDmkEnums;

type
  TTipoGera = (tgEnvio, tgTeste);
  dceAlinha = (posEsquerda, posCentro, posDireita);
  TFmCobrancaBB = class(TForm)
    PainelDados: TPanel;
    DsCobrancaBB: TDataSource;
    QrCobrancaBB: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtGera: TBitBtn;
    BtTitulos: TBitBtn;
    BtLotes: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    TPDataG: TDateTimePicker;
    Label3: TLabel;
    QrCobrancaBBCodigo: TIntegerField;
    QrCobrancaBBDataG: TDateField;
    QrCobrancaBBLk: TIntegerField;
    QrCobrancaBBDataCad: TDateField;
    QrCobrancaBBDataAlt: TDateField;
    QrCobrancaBBUserCad: TIntegerField;
    QrCobrancaBBUserAlt: TIntegerField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    DBGrid1: TDBGrid;
    QrCobrancaBBIts: TmySQLQuery;
    DsCobrancaBBIts: TDataSource;
    PnTitulos: TPanel;
    Panel2: TPanel;
    BitBtn1: TBitBtn;
    PMLotes: TPopupMenu;
    Crianovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    PMTitulos: TPopupMenu;
    Inclui1: TMenuItem;
    Retira1: TMenuItem;
    QrCobrancaBBMyDATAG: TWideStringField;
    Panel1: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit01: TDBEdit;
    DBEdit2: TDBEdit;
    DBGrid2: TDBGrid;
    QrTitulos: TmySQLQuery;
    DsTitulos: TDataSource;
    QrTitulosCliente: TIntegerField;
    QrTitulosLote: TIntegerField;
    QrTitulosNOMECLI: TWideStringField;
    QrTitulosCodigo: TIntegerField;
    QrTitulosControle: TIntegerField;
    QrTitulosComp: TIntegerField;
    QrTitulosBanco: TIntegerField;
    QrTitulosAgencia: TIntegerField;
    QrTitulosConta: TWideStringField;
    QrTitulosCheque: TIntegerField;
    QrTitulosCPF: TWideStringField;
    QrTitulosEmitente: TWideStringField;
    QrTitulosBruto: TFloatField;
    QrTitulosDesco: TFloatField;
    QrTitulosValor: TFloatField;
    QrTitulosEmissao: TDateField;
    QrTitulosDCompra: TDateField;
    QrTitulosDDeposito: TDateField;
    QrTitulosVencto: TDateField;
    QrTitulosTxaCompra: TFloatField;
    QrTitulosTxaJuros: TFloatField;
    QrTitulosTxaAdValorem: TFloatField;
    QrTitulosVlrCompra: TFloatField;
    QrTitulosVlrAdValorem: TFloatField;
    QrTitulosDMais: TIntegerField;
    QrTitulosDias: TIntegerField;
    QrTitulosDuplicata: TWideStringField;
    QrTitulosDevolucao: TIntegerField;
    QrTitulosQuitado: TIntegerField;
    QrTitulosLk: TIntegerField;
    QrTitulosDataCad: TDateField;
    QrTitulosDataAlt: TDateField;
    QrTitulosUserCad: TIntegerField;
    QrTitulosUserAlt: TIntegerField;
    QrTitulosPraca: TIntegerField;
    QrTitulosBcoCobra: TIntegerField;
    QrTitulosAgeCobra: TIntegerField;
    QrTitulosTotalJr: TFloatField;
    QrTitulosTotalDs: TFloatField;
    QrTitulosTotalPg: TFloatField;
    QrTitulosData3: TDateField;
    QrTitulosProrrVz: TIntegerField;
    QrTitulosProrrDd: TIntegerField;
    QrTitulosRepassado: TSmallintField;
    QrTitulosDepositado: TSmallintField;
    QrTitulosValQuit: TFloatField;
    QrTitulosValDeposito: TFloatField;
    QrTitulosTipo: TIntegerField;
    QrTitulosAliIts: TIntegerField;
    QrTitulosAlinPgs: TIntegerField;
    QrTitulosNaoDeposita: TSmallintField;
    QrTitulosReforcoCxa: TSmallintField;
    QrTitulosCartDep: TIntegerField;
    QrTitulosCobranca: TIntegerField;
    Panel4: TPanel;
    BitBtn2: TBitBtn;
    Panel6: TPanel;
    BtDesiste: TBitBtn;
    QrCobrancaBBItsCliente: TIntegerField;
    QrCobrancaBBItsLote: TIntegerField;
    QrCobrancaBBItsNOMECLI: TWideStringField;
    QrCobrancaBBItsCodigo: TIntegerField;
    QrCobrancaBBItsControle: TIntegerField;
    QrCobrancaBBItsComp: TIntegerField;
    QrCobrancaBBItsBanco: TIntegerField;
    QrCobrancaBBItsAgencia: TIntegerField;
    QrCobrancaBBItsConta: TWideStringField;
    QrCobrancaBBItsCheque: TIntegerField;
    QrCobrancaBBItsCPF: TWideStringField;
    QrCobrancaBBItsEmitente: TWideStringField;
    QrCobrancaBBItsBruto: TFloatField;
    QrCobrancaBBItsDesco: TFloatField;
    QrCobrancaBBItsValor: TFloatField;
    QrCobrancaBBItsEmissao: TDateField;
    QrCobrancaBBItsDCompra: TDateField;
    QrCobrancaBBItsDDeposito: TDateField;
    QrCobrancaBBItsVencto: TDateField;
    QrCobrancaBBItsTxaCompra: TFloatField;
    QrCobrancaBBItsTxaJuros: TFloatField;
    QrCobrancaBBItsTxaAdValorem: TFloatField;
    QrCobrancaBBItsVlrCompra: TFloatField;
    QrCobrancaBBItsVlrAdValorem: TFloatField;
    QrCobrancaBBItsDMais: TIntegerField;
    QrCobrancaBBItsDias: TIntegerField;
    QrCobrancaBBItsDuplicata: TWideStringField;
    QrCobrancaBBItsDevolucao: TIntegerField;
    QrCobrancaBBItsQuitado: TIntegerField;
    QrCobrancaBBItsLk: TIntegerField;
    QrCobrancaBBItsDataCad: TDateField;
    QrCobrancaBBItsDataAlt: TDateField;
    QrCobrancaBBItsUserCad: TIntegerField;
    QrCobrancaBBItsUserAlt: TIntegerField;
    QrCobrancaBBItsPraca: TIntegerField;
    QrCobrancaBBItsBcoCobra: TIntegerField;
    QrCobrancaBBItsAgeCobra: TIntegerField;
    QrCobrancaBBItsTotalJr: TFloatField;
    QrCobrancaBBItsTotalDs: TFloatField;
    QrCobrancaBBItsTotalPg: TFloatField;
    QrCobrancaBBItsData3: TDateField;
    QrCobrancaBBItsProrrVz: TIntegerField;
    QrCobrancaBBItsProrrDd: TIntegerField;
    QrCobrancaBBItsRepassado: TSmallintField;
    QrCobrancaBBItsDepositado: TSmallintField;
    QrCobrancaBBItsValQuit: TFloatField;
    QrCobrancaBBItsValDeposito: TFloatField;
    QrCobrancaBBItsTipo: TIntegerField;
    QrCobrancaBBItsAliIts: TIntegerField;
    QrCobrancaBBItsAlinPgs: TIntegerField;
    QrCobrancaBBItsNaoDeposita: TSmallintField;
    QrCobrancaBBItsReforcoCxa: TSmallintField;
    QrCobrancaBBItsCartDep: TIntegerField;
    QrCobrancaBBItsCobranca: TIntegerField;
    Label6: TLabel;
    Memo1: TMemo;
    QrConfigBB: TmySQLQuery;
    QrConfigBBCodigo: TIntegerField;
    QrConfigBBNome: TWideStringField;
    QrConfigBBCarteira: TWideStringField;
    QrConfigBBVariacao: TWideStringField;
    QrConfigBBSiglaEspecie: TWideStringField;
    QrConfigBBMoeda: TWideStringField;
    QrConfigBBAceite: TSmallintField;
    QrConfigBBProtestar: TSmallintField;
    QrConfigBBMsgLinha1: TWideStringField;
    QrConfigBBConvenio: TIntegerField;
    QrConfigBBPgAntes: TSmallintField;
    QrConfigBBMultaCodi: TSmallintField;
    QrConfigBBMultaDias: TSmallintField;
    QrConfigBBMultaValr: TFloatField;
    QrConfigBBMultaPerc: TFloatField;
    QrConfigBBMultaTiVe: TSmallintField;
    QrConfigBBImpreLoc: TSmallintField;
    QrCobrancaBBItsMultaCodi: TSmallintField;
    QrCobrancaBBItsMultaDias: TSmallintField;
    QrCobrancaBBItsMultaValr: TFloatField;
    QrCobrancaBBItsMultaPerc: TFloatField;
    QrCobrancaBBItsCNPJ: TWideStringField;
    QrCobrancaBBItsIE: TWideStringField;
    QrCobrancaBBItsNome: TWideStringField;
    QrCobrancaBBItsRua: TWideStringField;
    QrCobrancaBBItsCompl: TWideStringField;
    QrCobrancaBBItsBairro: TWideStringField;
    QrCobrancaBBItsCidade: TWideStringField;
    QrCobrancaBBItsUF: TWideStringField;
    QrCobrancaBBItsCEP: TIntegerField;
    QrCobrancaBBItsTel1: TWideStringField;
    QrCobrancaBBItsRisco: TFloatField;
    QrCobrancaBBItsMultaTiVe: TSmallintField;
    QrCobrancaBBItsProtestar: TSmallintField;
    QrCobrancaBBItsJuroSacado: TFloatField;
    QrCobrancaBBItsPUF: TSmallintField;
    QrCobrancaBBItsEUF: TSmallintField;
    QrCobrancaBBItsUFE: TWideStringField;
    QrCobrancaBBItsUFP: TWideStringField;
    QrCobrancaBBItsCNPJCLI: TWideStringField;
    QrCobrancaBBItsRuaCLI: TWideStringField;
    QrCobrancaBBItsCplCLI: TWideStringField;
    QrCobrancaBBItsBrrCLI: TWideStringField;
    QrCobrancaBBItsCEPCLI: TLargeintField;
    QrCobrancaBBItsCidCLI: TWideStringField;
    QrCobrancaBBItsTipoCLI: TSmallintField;
    QrConfigBBModalidade: TIntegerField;
    QrConfigBBclcAgencNr: TWideStringField;
    QrConfigBBclcAgencDV: TWideStringField;
    QrConfigBBclcContaNr: TWideStringField;
    QrConfigBBclcContaDV: TWideStringField;
    QrConfigBBcedAgencNr: TWideStringField;
    QrConfigBBcedAgencDV: TWideStringField;
    QrConfigBBcedContaNr: TWideStringField;
    QrConfigBBcedContaDV: TWideStringField;
    QrConfigBBEspecie: TSmallintField;
    QrConfigBBCorrido: TSmallintField;
    Splitter1: TSplitter;
    QrCobrancaBBItsCorrido: TIntegerField;
    QrCobrancaBBItsNumero: TFloatField;
    QrCobrancaBBItsNumCLI: TFloatField;
    Label7: TLabel;
    EdConfigBB: TdmkEditCB;
    CBConfigBB: TdmkDBLookupComboBox;
    QrConfigs: TmySQLQuery;
    DsConfigs: TDataSource;
    QrConfigsCodigo: TIntegerField;
    QrConfigsNome: TWideStringField;
    QrCobrancaBBConfigBB: TIntegerField;
    QrCobrancaBBNOMECONFIG: TWideStringField;
    Label8: TLabel;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label10: TLabel;
    Label11: TLabel;
    QrTotal: TmySQLQuery;
    DsTotal: TDataSource;
    QrTotalTOTTIT: TLargeintField;
    QrTotalVALTOT: TFloatField;
    QrCobrancaBBItsDescAte: TDateField;
    QrCobrancaBBDiretorio: TWideStringField;
    QrConfigBBDiretorio: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtLotesClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCobrancaBBAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrCobrancaBBAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCobrancaBBBeforeOpen(DataSet: TDataSet);
    procedure Crianovolote1Click(Sender: TObject);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Excluiloteatual1Click(Sender: TObject);
    procedure PMLotesPopup(Sender: TObject);
    procedure BtTitulosClick(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure QrCobrancaBBCalcFields(DataSet: TDataSet);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtGeraClick(Sender: TObject);
    procedure Retira1Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenCobrancaBBIts(Controle: Integer);
    procedure ReopenTitulos(Controle: Integer);
    procedure AdicionaItem;
    procedure CalculaLote(Lote: Integer);
    function CompletaString(Texto, Compl: String; Tamanho: Integer;
             Alinhamento: dceAlinha): String;
    function AjustaString(Texto, Compl: String; Tamanho: Integer;
             Alinhamento: dceAlinha): String;
    //function SubstituiString(TextoPai, TextoIns: String; PosI, PosF: Integer):
    //         String;
    function GeraHeader(Tipo: TTipoGera): String;
    function GeraDetalhe(Tipo: TTipoGera): String;
    function GeraTrailer(Tipo: TTipoGera): String;
    procedure GeraArquivo(Tipo: TTipoGera);

  public
    { Public declarations }
    FSeq: Integer;
  end;

var
  FmCobrancaBB: TFmCobrancaBB;
const
  FFormatFloat = '00000';
  FArquivoSalvaDir = 'C:\Dermatek\Titulos\';
  FArquivoSalvaArq = 'CBR641.txt';

implementation

uses Module, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCobrancaBB.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCobrancaBB.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCobrancaBBCodigo.Value, LaRegistro.Caption[2]);
  if QrCobrancaBBCodigo.Value < 1 then
  BtGera.Enabled := False
else
  BtGera.Enabled := True;
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCobrancaBB.DefParams;
begin
  VAR_GOTOTABELA := 'CobrancaBB';
  VAR_GOTOMYSQLTABLE := QrCobrancaBB;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT con.Nome NOMECONFIG, cob.*');
  VAR_SQLx.Add('FROM cobrancabb cob');
  VAR_SQLx.Add('LEFT JOIN configbb con ON con.Codigo=cob.ConfigBB');
  VAR_SQLx.Add('WHERE cob.Codigo > -1000');
  //
  VAR_SQL1.Add('AND cob.Codigo=:P0');
  //
  VAR_SQLa.Add(''); //AND Nome Like :P0
  //
end;

procedure TFmCobrancaBB.MostraEdicao(Mostra : Integer; Status : String; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      PnTitulos.Visible   := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.Text       := '';
        TPDataG.Date        := Date;
        EdConfigBB.Text     := '';
        CBConfigBB.KeyValue := NULL;
      end else begin
        EdCodigo.Text       := IntToStr(QrCobrancaBBCodigo.Value);
        TPDataG.Date        := QrCobrancaBBDataG.Value;
        EdConfigBB.Text     := IntToStr(QrCobrancaBBConfigBB.Value);
        CBConfigBB.KeyValue := QrCobrancaBBConfigBB.Value;
      end;
      TPDataG.SetFocus;
    end;
    2:
    begin
      PnTitulos.Visible      := True;
      PainelDados.Visible    := False;
      PainelControle.Visible :=False;
      ReopenTitulos(0);
    end;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmCobrancaBB.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCobrancaBB.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCobrancaBB.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCobrancaBB.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCobrancaBB.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCobrancaBB.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCobrancaBB.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCobrancaBB.BtLotesClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLotes, BtLotes);
end;

procedure TFmCobrancaBB.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCobrancaBBCodigo.Value;
  Close;
end;

procedure TFmCobrancaBB.BtConfirmaClick(Sender: TObject);
var
  Codigo, ConfigBB: Integer;
  DataG: String;
begin
  DataG := Geral.FDT(TPDataG.Date, 1);
  ConfigBB := Geral.IMV(EdConfigBB.Text);
  if ConfigBB = 0 then
  begin
    Application.MessageBox('Defina a conta de cobran�a!', 'Erro', MB_OK+
    MB_ICONERROR);
    EdConfigBB.SetFocus;
  end;
  //
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO cobrancabb SET ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'CobrancaBB', 'CobrancaBB', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE cobrancabb SET ');
    Codigo := QrCobrancaBBCodigo.Value;
  end;
  Dmod.QrUpdU.SQL.Add('DataG=:P0, ConfigBB=:P1, ');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := DataG;
  Dmod.QrUpdU.Params[01].AsInteger := ConfigBB;
  //
  Dmod.QrUpdU.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[03].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[04].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CobrancaBB', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
  if FSeq = 1 then Close;
      if QrCobrancaBBCodigo.Value < 1 then
        BtGera.Enabled := False
      else
        BtGera.Enabled := True;
end;

procedure TFmCobrancaBB.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'CobrancaBB', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CobrancaBB', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'CobrancaBB', 'Codigo');
  if QrCobrancaBBCodigo.Value < 1 then
    BtGera.Enabled := False
  else
    BtGera.Enabled := True;
end;

procedure TFmCobrancaBB.FormCreate(Sender: TObject);
begin
  FSeq := 0;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  DBGrid1.Align     := alClient;
  DBGrid2.Align     := alClient;
  CriaOForm;
  QrConfigs.Open;
  if QrCobrancaBBCodigo.Value < 1 then
    BtGera.Enabled := False
  else
    BtGera.Enabled := True;
end;

procedure TFmCobrancaBB.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCobrancaBBCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCobrancaBB.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCobrancaBB.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrCobrancaBBCodigo.Value;
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmCobrancaBB.QrCobrancaBBAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCobrancaBB.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'CobrancaBB', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, CO_INCLUSAO, 0);
end;

procedure TFmCobrancaBB.QrCobrancaBBAfterScroll(DataSet: TDataSet);
begin
  ReopenCobrancaBBIts(0);
end;

procedure TFmCobrancaBB.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCobrancaBBCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'CobrancaBB', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCobrancaBB.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmCobrancaBB.QrCobrancaBBBeforeOpen(DataSet: TDataSet);
begin
  QrCobrancaBBCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCobrancaBB.ReopenCobrancaBBIts(Controle: Integer);
begin
  QrCobrancaBBIts.Close;
  QrCobrancaBBIts.Params[0].AsInteger := QrCobrancaBBCodigo.Value;
  QrCobrancaBBIts.Open;
  //
  if Controle > 0 then QrCobrancaBBIts.Locate('Controle', Controle, []);
  //
  QrTotal.Close;
  QrTotal.Params[0].AsInteger := QrCobrancaBBCodigo.Value;
  QrTotal.Open;
end;

procedure TFmCobrancaBB.PMLotesPopup(Sender: TObject);
begin
  if QrCobrancaBBCodigo.Value = 0 then
  begin
    Alteraloteatual1.Enabled := False;
    Excluiloteatual1.Enabled := False;
  end else begin
    Alteraloteatual1.Enabled := True;
    if QrCobrancaBBIts.RecordCount = 0 then
      Excluiloteatual1.Enabled := True
    else
      Excluiloteatual1.Enabled := False;
  end;
end;

procedure TFmCobrancaBB.Crianovolote1Click(Sender: TObject);
begin
  MostraEdicao(1, CO_INCLUSAO, 0);
end;

procedure TFmCobrancaBB.Alteraloteatual1Click(Sender: TObject);
var
  CobrancaBB : Integer;
begin
  CobrancaBB := QrCobrancaBBCodigo.Value;
  if not UMyMod.SelLockY(CobrancaBB, Dmod.MyDB, 'CobrancaBB', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(CobrancaBB, Dmod.MyDB, 'CobrancaBB', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCobrancaBB.Excluiloteatual1Click(Sender: TObject);
var
  Atual, Proximo: Integer;
begin
  if Application.MessageBox('Confirma a exclus�o deste lote?', 'Pergunta',
  MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Atual := QrCobrancaBBCodigo.Value;
    Proximo := UMyMod.ProximoRegistro(QrCobrancaBB, 'Codigo', Atual);
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM cobrancabb WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Atual;
    Dmod.QrUpd.ExecSQL;
    //
    LocCod(Atual, Proximo);
    Screen.Cursor := crDefault;
  end;
  if QrCobrancaBBCodigo.Value < 1 then
    BtGera.Enabled := False
  else
    BtGera.Enabled := True;
end;

procedure TFmCobrancaBB.BtTitulosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTitulos, BtTitulos);
end;

procedure TFmCobrancaBB.Inclui1Click(Sender: TObject);
begin
  MostraEdicao(2, CO_INCLUSAO, 0);
end;

procedure TFmCobrancaBB.ReopenTitulos(Controle: Integer);
begin
  QrTitulos.Close;
  QrTitulos.Open;
  //
  if Controle > 0 then QrTitulos.Locate('Controle', Controle, []);
end;

procedure TFmCobrancaBB.QrCobrancaBBCalcFields(DataSet: TDataSet);
begin
  if QrCobrancaBBDataG.Value = 0 then
    QrCobrancaBBMyDataG.Value := '00/00/0000'
  else QrCobrancaBBMyDataG.Value := Geral.FDT(QrCobrancaBBDataG.Value, 2);
end;

procedure TFmCobrancaBB.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmCobrancaBB.BitBtn1Click(Sender: TObject);
var
  i: integer;
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   Cobranca=:P0 WHERE Controle=:P1');
  if DBGrid2.SelectedRows.Count > 0 then
  begin
    if Application.MessageBox('Confirma a adi��o dos itens selecionados?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      with DBGrid2.DataSource.DataSet do
      for i:= 0 to DBGrid2.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGrid2.SelectedRows.Items[i]));
        AdicionaItem;
      end;
      Screen.Cursor := crDefault;
    end;
  end else
    if Application.MessageBox('Confirma a adi��o do item selecionado?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then AdicionaItem;
  CalculaLote(QrCobrancaBBCodigo.Value);
  LocCod(QrCobrancaBBCodigo.Value, QrCobrancaBBCodigo.Value);
  ReopenTitulos(0);
end;

procedure TFmCobrancaBB.AdicionaItem;
begin
  Dmod.QrUpd.Params[0].AsInteger := QrCobrancaBBCodigo.Value;
  Dmod.QrUpd.Params[1].AsInteger := QrTitulosControle.Value;
  Dmod.QrUpd.ExecSQL;
end;

procedure TFmCobrancaBB.CalculaLote;
begin
  (*Dmod.QrUpd.Params[0].AsInteger := QrCobrancaBBCodigo.Value;
  Dmod.QrUpd.Params[1].AsInteger := QrTitulosControle.Value;
  Dmod.QrUpd.ExecSQL;*)
end;

function TFmCobrancaBB.AjustaString(Texto, Compl: String; Tamanho: Integer;
  Alinhamento: dceAlinha): String;
var
  Txt: String;
  Direita: Boolean;
begin
  Direita := True;
  Txt := CompletaString(Texto, Compl, Tamanho, Alinhamento);
  while Length(Txt) > Tamanho do
  begin
    case Alinhamento of
      posEsquerda: Txt := Copy(Txt, 1, Length(Txt)-1);
      posCentro  :
      begin
        if Direita then
          Txt := Copy(Txt, 2, Length(Txt)-1)
        else
          Txt := Copy(Txt, 1, Length(Txt)-1);
        Direita := not Direita;
      end;
      posDireita: Txt := Copy(Txt, 2, Length(Txt)-1);
    end;
  end;
  Result := Txt;
end;

function TFmCobrancaBB.CompletaString(Texto, Compl: String; Tamanho: Integer;
  Alinhamento: dceAlinha): String;
var
  Txt: String;
  Direita: Boolean;
begin
  Direita := True;
  Txt := Texto;
  while Length(Txt) < Tamanho do
  begin
    case Alinhamento of
      posEsquerda: Txt := Txt + Compl;
      posCentro  :
      begin
        if Direita then
          Txt := Txt + Compl
        else
          Txt := Compl + Txt;
        Direita := not Direita;
      end;
      posDireita:  Txt := Compl + Txt;
    end;
  end;
  Result := Txt;
end;

{
  N�O USA
function TFmCobrancaBB.SubstituiString(TextoPai, TextoIns: String;
  PosI, PosF: Integer): String;
var
  i, j, n: Integer;
begin
  j := 0;
  n := Length(TextoIns);
  for i := PosI to PosF do
  begin
    j := j + 1;
    if j <= n then
      TextoPai[i] := TextoIns[j];
  end;
  Result := TextoPai;
end;
}

procedure TFmCobrancaBB.BtGeraClick(Sender: TObject);
begin
  GeraArquivo(tgEnvio);
end;

procedure TFmCobrancaBB.GeraArquivo(Tipo: TTipoGera);
var
 Arquivo: String;
begin
  QrConfigBB.Close;
  QrConfigBB.Params[0].AsInteger := QrCobrancaBBConfigBB.Value;
  QrConfigBB.Open;
  if QrConfigBBProtestar.Value = 0 then
  begin
    Application.MessageBox(PChar('Configura��o de dias para protesto inv�lido ('+
    IntToStr(QrConfigBBProtestar.Value)+').'+
    Chr(13)+Chr(10)+'Fa�a a corre��o em Op��es -> Cobran�a de t�tulos'),
    'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  if Trim(QrConfigBBDiretorio.Value) = '' then
  begin
    Application.MessageBox('Diret�rio para salvamento do arquivo n�o informado!',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Memo1.Lines.Clear;
  Memo1.Lines.Add(GeraHeader(Tipo));
  QrCobrancaBBIts.First;
  while not QrCobrancaBBIts.Eof do
  begin
    Memo1.Lines.Add(GeraDetalhe(Tipo));
    QrCobrancaBBIts.Next;
  end;
  Memo1.Lines.Add(GeraTrailer(Tipo));
  //
  ForceDirectories(ExtractFileDir(FArquivoSalvaDir));
  Arquivo := FArquivoSalvaDir +
             FormatFloat('000000', QrCobrancaBBCodigo.Value)+'_'+
             FArquivoSalvaArq;
    //FormatFloat('00000000', QrCobrancaBBCodigo.Value)+'.Txt';
  if MLAgeral.ExportaMemoToFile(Memo1, Arquivo, True, False, True) then
    Application.MessageBox(PChar('Arquivo salvo com sucesso!'+Chr(13)+Chr(10)+
    Arquivo), 'Arquivo Salvo', MB_OK+MB_ICONINFORMATION)
  else
    Application.MessageBox(PChar('O arquivo n�o pode ser salvo!'+Chr(13)+Chr(10)+
    Arquivo), 'ERRO', MB_OK+MB_ICONERROR);
end;

procedure TFmCobrancaBB.Retira1Click(Sender: TObject);
var
  Atual, Proximo: Integer;
begin
  Atual := QrCobrancaBBItsControle.Value;
  Proximo := UMyMod.ProximoRegistro(QrCobrancaBBIts, 'Controle', Atual);
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   Cobranca=0 WHERE Controle=:P0');
  Dmod.QrUpd.Params[0].AsInteger := Atual;
  Dmod.QrUpd.ExecSQL;
  //
  ReopenCobrancaBBIts(Proximo);
end;

function TFmCobrancaBB.GeraHeader(Tipo: TTipoGera): String;
const
  MaxS = 19;
var
  i: Integer;
  Txt: array[1..MaxS] of String;
  s: String;
begin
  for i := 1 to MaxS do Txt[i] := '';
(*
Nota.. a/ nos campos do arquivo remessa que possuirem DATA no
          formato /DDMMAA/, deverao conter:
          I  - para ano igual a 2001 /DDMMAA/ - /AA/ igual a 01.,
          II - para ano igual a 2002 /DDMMAA/ - /AA/ igual a 02, e
               assim sucessivamente ateh o ano 2069
       b/ nas instrucoes e mensagens, utilizar somente letras
          maiusculas, sem acentuacao e sem /ce cedilha/.
       c/ Caracteristicas do Arquivo Remessa..
          I  - Nome do Arquivo /DATA SET NAME/ = CBR641.,
          II - Tamanho do registro= 400 bytes.,
          III- Organizacao = sequencial.,
          IV - Formato dos registros =
               - FB /Fixos Blocados/
               - Formato dos campos /9/ - numerico
                                    /X/ - alfanumerico
       d/ eh composto de tres tipos de registro..
          I  - HEADER - registro de abertura do arquivo. Contem as
               informacoes da empresa convenente e agencia
               centralizadora do convenio;
          II - DETALHE - contem as informacoes detalhadas do
               titulo, cedente, sacado, e instrucoes para o
               processamento.,
          III- TRAILER - eh o registro de encerramento do
               arquivo.
       e/ Informacoes gerais..
          I  - o arquivo conterah relacao de titulos para cobranca
          II - pode ser remetido mais de um arquivo, desde que com
               dados diferentes, para processamento no mesmo dia."


01. Descricao dos campos..

HEADER
..................................................................
N.  POSICOES  PICTURE  USAGE    CONTEUDO
............................................................. ....
01  001 a 001 9/001/   Display  0 /zero/
02  002 a 002 9/001/   Display  1 /um/
03  003 a 009 X/007/   Display  REMESSA: para envio de arquivo para
                                         processamento
                                TESTE  : para envio de arquivo para
                                         teste - Vide observa��es
04  010 a 011 9/002/   Display  01
05  012 a 019 X/008/   Display  COBRANCA
06  020 a 026 X/007/   Display  Brancos
07  027 a 030 9/004/   Display  Prefixo da agencia /onde estah
                                cadastrado o convenente lider do
                                cedente/
08  031 a 031 X/001/   Display  DV-prefixo da agencia
09  032 a 039 9/008/   Display  Codigo do cedente/nr. da conta
                                corrente que estah cadastrado o
                                convenio lider do cedente/
10  040 a 040 X/001/   Display  DV-codigo do cedente
11  041 a 046 9/006/   Display  Zeros
12  047 a 076 X/030/   Display  Nome da empresa
13  077 a 094 X/018/   Display  001BANCO DO BRASIL
14  095 a 100 9/006/   Display  Data da gravacao /DDMMAA/. nao
                                serao aceitas datas de gravacao
                                futuras
15  101 a 107 9/007/   Display  Sequencial da remessa/vide obs/
16  108 a 129 X/022/   Display  Brancos
17  130 a 136 9/007/   Display  N�mero do Conv�nio com o Banco (acima de
                                1.000.000 (um milh�o)
18  137 a 394 X/258/   Display  Brancos
19  395 a 400 9/006/   Display  Sequencial do registro-informar..
                                000001

OBSERVACOES
O Banco fornecerah ao cedente, obrigatoriamente, os dados referentes
aos campos 07, 08, 09, 10, 11 17 e 18
............................................................
N.  POSICOES   OBSERVACOES
............................................................
03  003 a 007  TESTE: Neste caso, o arquivo somente poder� conter o
                      comando "01" - Registro de T�tulo. Ser� gerado
                      arquivo-retorno contendo as ocorr�ncias de
                      processamento verificadas no arquivo-remessa de
                      teste.
15  101 a 107  SEQUENCIAL DE REMESSA.. Numero da remessa efetuada
               pelo cliente. O Sistema nao controla tal numeracao,
               admitindo quebra na sequencia e repeticao de numero
               jah processado. Pode ser utilizado pelo cliente
               para seu proprio controle.


*)
//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//01  001 a 001 9/001/   Display  0 /zero/
  Txt[01] := '0';

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//02  002 a 002 9/001/   Display  1 /um/
  Txt[02] := '1';

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//03  003 a 009 X/007/   Display  REMESSA: para envio de arquivo para
//                                         processamento
//                                TESTE  : para envio de arquivo para
//                                         teste - Vide observa��es
  if Tipo = tgTeste then
    Txt[03] := 'TESTE  '
  else
    Txt[03] := 'REMESSA';

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//04  010 a 011 9/002/   Display  01
  Txt[04] := '01';

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//05  012 a 019 X/008/   Display  COBRANCA

  Txt[05] := 'COBRANCA';

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//06  020 a 026 X/007/   Display  Brancos
  Txt[06] := AjustaString(' ', ' ', 007, posEsquerda);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//07  027 a 030 9/004/   Display  Prefixo da agencia /onde estah
//                                cadastrado o convenente lider do
//                                cedente/
  Txt[07] := AjustaString(QrConfigBBclcAgencNr.Value, '0', 004, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//08  031 a 031 X/001/   Display  DV-prefixo da agencia
  Txt[08] := AjustaString(QrConfigBBclcAgencDV.Value, ' ', 001, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//09  032 a 039 9/008/   Display  Codigo do cedente/nr. da conta
//                                corrente que estah cadastrado o
//                                convenio lider do cedente/
  Txt[09] := AjustaString(QrConfigBBclcContaNr.Value, '0', 008, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//10  040 a 040 X/001/   Display  DV-codigo do cedente
  Txt[10] := AjustaString(QrConfigBBclcContaDV.Value, ' ', 001, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//11  041 a 046 9/006/   Display  Zeros
  Txt[11] := AjustaString('', '0', 006, posDireita);


//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//12  047 a 076 X/030/   Display  Nome da empresa
  Txt[12] := AjustaString(Dmod.QrDonoNOMEDONO.Value, ' ', 30, posEsquerda);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//13  077 a 094 X/018/   Display  001BANCO DO BRASIL
  Txt[13] := AjustaString('001BANCO DO BRASIL', ' ', 18, posEsquerda);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//14  095 a 100 9/006/   Display  Data da gravacao /DDMMAA/. nao
//                                serao aceitas datas de gravacao
//                                futuras
  Txt[14] := FormatDateTime('ddmmyy', QrCobrancaBBDataG.Value);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//15  101 a 107 9/007/   Display  Sequencial da remessa/vide obs/
  Txt[15] := FormatFloat('0000000', QrCobrancaBBCodigo.Value);
  i := Length(Txt[15]);
  if (i > 7) then
  begin
    s := Txt[15][1];
    Txt[15] := Copy(Txt[15], i-7+1, 7);
    if s = '-' then Txt[15][1] := '-';
  end;

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//16  108 a 129 X/022/   Display  Brancos
  Txt[16] := AjustaString(' ', ' ', 022, posEsquerda);


//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//17  130 a 136 9/007/   Display  N�mero do Conv�nio com o Banco (acima de
//                                1.000.000 (um milh�o)
  Txt[17] := AjustaString(Geral.SoNumero_TT(IntToStr(
    QrConfigBBConvenio.Value)), '0', 007, posDireita);


//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//18  137 a 394 X/258/   Display  Brancos
  Txt[18] := AjustaString(' ', ' ', 258, posEsquerda);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//............................................................. ....
//19  395 a 400 9/006/   Display  Sequencial do registro-informar..
//                                000001
  Txt[19] := AjustaString('1', '0', 006, posDireita);

{OBSERVACOES
O Banco fornecerah ao cedente, obrigatoriamente, os dados referentes
aos campos 07, 08, 09, 10, 11 17 e 18
............................................................
N.  POSICOES   OBSERVACOES
............................................................
03  003 a 007  TESTE: Neste caso, o arquivo somente poder� conter o
                      comando "01" - Registro de T�tulo. Ser� gerado
                      arquivo-retorno contendo as ocorr�ncias de
                      processamento verificadas no arquivo-remessa de
                      teste.
15  101 a 107  SEQUENCIAL DE REMESSA.. Numero da remessa efetuada
               pelo cliente. O Sistema nao controla tal numeracao,
               admitindo quebra na sequencia e repeticao de numero
               jah processado. Pode ser utilizado pelo cliente
               para seu proprio controle.

}
  //////////////////////////////////////////////////////////////////////////////
  Result := '';
  for i := 1 to MaxS do
    Result := Result + Txt[i];
end;

function TFmCobrancaBB.GeraDetalhe(Tipo: TTipoGera): String;
const
  MaxS = 50;
var
  Txt: array[1..MaxS] of String;
  aux, cpf, Val: String;
  i: Integer;
  d: Double;
begin
  for i := 1 to MaxS do Txt[i] := '';
(*
DETALHE (OBRIGAT�RIO)
..................................................................    
N.  POSICOES  PICTURE  USAGE    CONTEUDO                              
..................................................................
01  001 a 001 9/001/   Display  7
02  002 a 003 9/002/   Display  Tipo de inscricao da empresa          
                                01-CPF                                
                                02-CNPJ                               
03  004 a 017 9/014/   Display  Inscricao da empresa                  
04  018 a 021 9/004/   Display  Prefixo da agencia                    
05  022 a 022 X/001/   Display  DV-prefixo da agencia
06  023 a 030 9/008/   Display  Codigo do cedente/nr. conta           
                                corrente da empresa/                  
07  031 a 031 X/001/   Display  DV-codigo do cedente                  
08  032 a 038 9/007/   Display  Numero do convenio                    
09  039 a 063 X/025/   Display  Nr. controle do participante. Pode    
                                conter qualquer dado de interesse     
                                do cliente.Serah mantido nos
                                arquivos do Banco sem qualquer        
                                tratamento.                           
                                Toda vez que o titulo for incluido    
                                no arquivo-retorno, este numero       
                                tambem serah.                         
10  064 a 080 9/017/   Display  Nosso numero. Vide /observacoes/      
11  081 a 082 9/002/   Display  Numero da prestacao-informar ZEROS
12  083 a 084 9/002/   Display  Indicativo de grupo de valor          
                                informar ZEROS
13  085 a 087 X/003/   Display  Brancos                               
14  088 a 088 X/001/   Display  Indicativo de SACADOR..               
                                BRANCOS-nas pos.352 a 391             
                                mensagens a criterio do cedente       
                                A - nas pos.352 a 391 informar
                                nome e CPF/CNPJ do sacador.           
15  089 a 091 X/003/   Display  Prefixo do titulo                     
                                CARTEIRAS 31,51.. Preencher com SD
                                CARTEIRA 12.. Preencher com AIU.      
                                DEMAIS CARTEIRAS.. Preencher com      
                                AI.                                   
16  092 a 094 9/003/   Display Variacao da carteira                   
17  095 a 095 9/001/   Display Conta caucao.preencher com /ZEROS/.    
18  096 a 101 9/006/   Display Numero do bordero..                   
                               a/ carteira 51..                       
                                  - ZEROS -caso o cedente envie       
                                    mais de uma remessa para o CBR,   
                                    na mesma previa, suas remessas se 
                                    tornarao uma unica no CIOPE.,
                                  - 900.000 a 999.000 - a remessa     
                                    serah subdividida no CIOPE por
                                    bordero.                          
                               b/ registro de titulos nas carteiras   
                                  31/51 emitido contra empresas coli  
                                  gadas - 900.000 a 999.999.,         
                               c/ demais carteiras - ZEROS
19  102 a 106 X/005/   Display Tipo de Cobran�a:                      
                               /04DSC/-solicitacao de registro como   
                                Cobran�a DESCONTADA, quando utilizada 
                                a  Carteira 11 ou 17.
                               /08VDR/-solicitacao de registro como   
                                Cobran�a VENDOR, quando utilizada a   
                                Carteira 11 ou 17.                    
                               /02VIN/-solicita��o de registro como   
                                Cobran�a VINCULADA, quando utilizada  
                                a Carteira 17.
                               /BRANCOS/ para:
                                Carteira 11, 12 e 17 - registrar como
                                Cobran�a Simples
                                Carteira 31- registrar como Cobran�a
                                Vinculada
                                Carteira 51- registrar como Cobran�a
                                Descontada
20  107 a 108 9/002/   Display Carteira
                               11-Cobranca Simples
                               12-Cobranca Indexada
                               17-Cobranca Direta Especial
                               31-Cobranca Vinculada
                               51-Cobranca Descontada
21  109 a 110 9/002/   Display Comando
                               01-Registro de titulos/Ver Primeira
                                  Inst. Cod.-Posicao 157 a 158/
                               02-Solicitacao de Baixa /Ver
                                  Primeira Inst. Cod.-Pos. 157 a
                                  158/
                               03-Pedido de debito em conta           
                               04-Concessao de abatimento             
                               05-Cancel. de abatimento               
                               06-Alteracao de vcto. de titulo        
                               07-Alteracao do numero de controle do
                                  participante
                               08-Alteracao do numero do titulo dado  
                                  pelo cedente                        
                               09-Instrucao para protestar /Ver 1a.   
                                  inst. Cod.-Pos. 157 a 158/          
                               10-Instrucao para sustar protesto      
                               11-Instrucao para dispensar juros      
                               12-Alteracao de nome e endereco do
                                  sacado                              
                               16-Altera��o de juros mora             
                               31-Conceder desconto                   
                               32-Nao conceder desconto               
                               33-Retificar desconto                  
                               34-Alterar data para desconto          
                               35-Cobrar Multa                        
                                  - Vide Observacao do campo 35       
                               36-Dispensar Multa                     
                                  - Vide Observacao do campo  35      
                               37-Dispensar Indexador                 
                                  - Vide Observacao do campo 35
                               38-Dispensar Prazo Limite de
                                  Recebimento
                                  - Vide Observacao do campo  35      
                               39-Alterar Prazo Limite de             
                                  Recebimento.                        
                                  - Vide Observacao do campo 35       
                               40-Alterar Modalidade de Cobran�a:     
                                  Observa�oes:                        
                                 a)v�lida apenas para a Carteira 17,
                                   para alterar o t�tulo da Cobran�a
                                   Simples para Cobran�a Vinculada ou 
                                   da Cobran�a Vinculada para         
                                   Cobran�a Simples;                  
                                 b)a modalidade de Cobran�a para a    
                                   qual se destina a altera��o deve   
                                   ser informada no campo "Tipo de    
                                   Cobran�a" - posi��o 102 a 106.     
                                 c)a efetiva��o dessa instru��o, ap�s
                                   o seu processamento pelo Sistema   
                                   do Banco, depende de libera��o     
                                   da ag�ncia de relacionamento com a 
                                   empresa cedente.                   
22  111 a 120 X/010/   Display Seu Numero - Nr. titulo dado pelo
                               cedente - vide obs. do campo 9         
23  121 a 126 9/006/   Display Data de vencimento /DDMMAA/            
                               888888-a vista                         
                               999999-na apresentacao                 
                               Obs.. Em ambos os casos, o vencimento  
                                     ocorrerah 15 dias apos a data    
                                     do registro no Banco.
24  127 a 139 9/011/v99Display Valor do titulo
                               Carteira 12- em reais, na data de      
                               emissao do titulo                      
25  140 a 142 9/003/   Display Numero do Banco - /001/                
26  143 a 146 9/004/   Display Prefixo da ag. Cobradora               
                               preencher com /ZEROS/.O Sistema        
                               indicarah a agencia cobradora pelo     
                               CEP do sacado.                         
27  147 a 147 X/001/   Display DV-pref. agencia cobradora..
28  148 a 149 9/002/   Display Especie de titulo
                               01-Duplicata Mercantil                 
                               02-Nota Promissoria                    
                               03-Nota de Seguro                      
                               05-Recibo
                               08-Letra de Cambio                     
                               09-Warrant                             
                               10-Cheque                              
                               12-Duplicata de Servico
                               13-Nota de Debito                      
                               15-Apolice de Seguro                   
                               21-Duplicata Rural
                               25-Divida Ativa da Uniao     /OBS./    
                               26-Divida Ativa de Estado    /OBS./    
                               27-Divida Ativa de Municipio /OBS./    
29  150 a 150 X/001/   Display Aceite                                 
                               N-sem aceite                           
                               A-com aceite                           
30  151 a 156 9/006/   Display Data de emissao /DDMMAA/               
31  157 a 158 9/002/   Display 1a instrucao codificada - vide         
                               /OBSERVACOES/
32  159 a 160 9/002/   Display 2a instrucao codificada
33  161 a 173 9/011/v99Display Juros de mora por dia-vide             
                               /OBSERVACOES/                          
34  174 a 179 9/006/   Display Data limite para Concessao de desconto 
                               /DDMMAA/ . O registro serah
                               recusado sempre que a data de          
                               desconto for informada e o desconto    
                               nao o for, ou a data do desconto for   
                               superior a data do vencimento.
                               Data da operacao BBVENDOR, no formato
                               DDMMAA.                                
35  180 a 192 9/011/v99Display Valor do desconto
                               Se desconto por dia de antecipacao,    
                               informar valor por dia e 777777 no     
                               campo /data limite para concessao de   
                               desconto/                              
                               Para BBVENDOR - vide observacoes       
36  193 a 205 9/011/v99Display Cart. 15.. Valor do IOF
                               Cart. 12..                             
                               1/ pos. 193 a 204.. quantidade de      
                                  unidades variaveis na data de       
                                  emissao do titulo. Ate sete         
                                  inteiros e, obrigatoriamente,cinco  
                                  decimais.,                          
                               2/ pos. 205.. especie de unidade       
                                  variavel..
                                  1-FAJTR 5-MARCO ALEMAO              
                                  2-DOLAR 6-FTR                       
                                  3-EURO  7-IDTR                      
                                  4-IENE
                               3/ Demais carteiras-preencher com
                                  ZEROS.                              
37  206 a 218 9/011/v99Display Valor do abatimento permitido no
                               registro das carteiras.. 11, 17 e 31   
38  219 a 220 9/002/   Display Tipo de inscricao do sacado            
                               00 - ISENTO                            
                               01 - CPF                               
                               02 - CNPJ                              
39  221 a 234 9/014/   Display CNPJ ou CPF do sacado                  
40  235 a 271 X/037/   Display Nome do sacado
41  272 a 274 X/003/   Display Brancos
42  275 a 311 X/037/   Display Endereco do sacado
43  312 a 326 X/015/   Display Brancos
44  327 a 334 9/008/   Display CEP do endereco do sacado
45  335 a 349 X/015/   Display Cidade do sacado
46  350 a 351 X/002/   Display UF da cidade do sacado
47  352 a 391 X/040/   Display Observacoes
                               Se a posicao 82 for informado
                               BRANCO..
                               Os dados informados nesse campo
                               serao listados no campo /instrucoes/
                               do boleto de cobranca e NAO SERAO
                               IMPRESSOS NO CASO DE EMISSAO DE 2.
                               VIA NAS AGENCIAS DO BB OU ATRAVES DO
                               BB OFFICE BANKING. Nao informar dados
                               conflitantes com as informacoes   dos
                               demais campos, como juros,IOF,
                               desconto, protesto, etc.
                               Se a posicao 82 for informado A..      
                               Os dados informados nesse campo serao
                               listados no campo/SACADOR-AVALISTA/
                               do fichamento de cobranca.             
                               Neste caso, o campo deverah ser        
                               preenchido com o nome do SACADOR/AVA   
                               LISTA, seguido do literal CNPJ ou CPF  
                               e do numero do documento, sem traco    
                               ou barra.                              
                               Entre o nome do SACADOR/AVALISTA e o   
                               literal CNPJ/CPF deverah ser dado um   
                               /espaco/ em branco.
48  392 a 393 X/002/   Display Numero de dias para protesto-caso o    
                               campo /comando/esteja preenchido com   
                               /01-registro de titulos/ e o campo     
                               /instrucao codificada 1/ tenha sido
                               preenchido com /06/,informar o numero
                               numero de dias para protesto, de       
                               /06 a 29, 35 ou 40/dias,senao ZEROS    
49  394 a 394 X/001/   Display BRANCO
50  395 a 400 9/006/   Display Sequencial de registro- incrementar    
                               em 1 a cada novo registro              

                                                                      
DETALHE OPCIONAL (REMESSA) - ENVIO DE BLOQUETO POR EMAIL              
....................................................................  
N.  POSICOES  PICTURE  USAGE    CONTEUDO                              
....................................................................  
01  001 a 001 X/001/   Display  5 /cinco/                             
02  002 a 003 9/002/   Display  1-Bloqueto por e-mail                 
03  004 a 139 X/136/   Display  135 /cento e trinta e cinco/ primei   
                                ras posi��es                          
                                Obs: os endere�os de e-mail dever�o
                                ser separados por ";" (ponto e v�r    
                                gula), sem espa�os                    
04  140 a 394 X/255/   Display  Brancos                               
05  395 a 400 X/006/   Display  Sequencial Registro                   
......................................................................
                                                                      
OBS:                                                                  
a) Este registro somente ser� validado se o cliente estiver           
   previamente autorizado no sistema de cobran�a do BB;
b) Os e-mails dever�o conter obrigatoriamente o caracter "@"          
   (arroba);                                                          
c) O envio de bloqueto por e-mail � valido somente para cobran�a com  
   registro, de conv�nios em que o Banco imprime e expede o bloqueto. 
d) N�o permitido para registro de t�tulos nas modalidades descontadas 
   ou Vendor;                                                         
e) O e-mail n�o ser� enviado quando o sacado, cliente do Banco, optar 
   pelo "Bloqueto Eletr�nico";
f) Poder� ser informado mais de um endere�o de e-mail para o mesmo    
   bloqueto, dentro dos limites do campo do arquivo remessa destinado 
   a essa finalidade. (campo 03)                                      
g) O bloqueto ser� impresso e postado caso o n�mero do documento      
   (CPF/CNPJ)e/ou endere�o de e-mail n�o sejam v�lidos (n�o cont�m o  
   caracter @).                                                       
h) O bloqueto ser� impresso e postado, caso n�o seja acessado num     
   prazo de tr�s dias a partir do registro do t�tulo. No bloqueto de  
   cobran�a impresso e postado constar� informa��o de que o t�tulo foi
   disponibilizado por e-mail. Ap�s este prazo, caso o bloqueto seja  
   acessado, ser� informado que j� foi impresso e postado. A op��o de 
   impress�o continuar� dispon�vel. Se for impresso, ser� inclu�da a  
   mensagem de 2� via no bloqueto.
i) Ap�s a liquida��o ou baixa  do t�tulo, o bloqueto n�o ficar� mais  
   dispon�vel no site do Banco.                                       
                                                                      
                                                                      
                                                                      
                          OBSERVACOES                                 
                                                                      
....................................................................  
N.    POSICOES    OBSERVACOES
....................................................................  
10/11 064 a 080   NOSSO NUMERO/DV..CARTEIRAS 11,31 e 51..             
                  Preencher com zeros.                                
                  CARTEIRAS 12,15 e 17 ..
                  a/ Se numeracao a cargo do Banco.. Preencher com
                     zeros.
                  b/ Se numeracao a cargo da empresa, compor da
                     seguinte forma..
                     Pos. 064/070-Numero do Convenio acima de 1.000.000
                     (um milh�o)                    .
                     Pos. 071/080-Numero sequencial a partir de
                     0000000001, nao sendo admitida reutilizacao ou
                     duplicidade
18    089 a 089   CONTA CAUCAO.. Preencher com zero
24    109 a 110   COMANDO- Quando o comando for igual "16" - Alterar Juros de
                  Mora - preencher as posi��es 174 a 192 conforme abaixo:
                  Campo       Conte�do
                  174 a 174   Cod Juros
                              1 - Valor
                              2 - Percentual
                  175 a 180   Zeros Informar zeros.
                  181 a 192   Vlr-Per-Juros
                              C�digo Multa "1", informar valor da multa.
                              C�digo Multa "2", informar percentual.
25    111 a 120   NUMERO DO TITULO DADO PELO CEDENTE.. Poderah        
                  conter qualquer dado de interesse do cliente.       
                  Serah mantido nos arquivos do Banco sem qualquer    
                  tratamento. Toda a vez que o titulo for incluido    
                  no arquivo-retorno, esse numero tambem serah.       
26    121 a 126   VENCIMENTO DO TITULO..CARTEIRAS 11, 12, 15, 17 e
                  31.. Admitidos prazos ateh 1.100 dias.              
                  CARTEIRA 51.. Admitidos prazos ateh 179 dias.       
                  Obs.. O Sistema aceita o registro de titulos        
                        vencidos nas carteiras 11, 12, 15, 17 e
                        31 ateh um dia util anterior ao prazo
                        cadastrado no Sistema para baixa automatica.  
29/30 143 a 147   PREFIXO/DV DA AGENCIA COBRADORA.. Essas             
                  posicoes poderao ser preenchidas com zeros e o      
                  Sistema indicarah a cobradora atraves do CEP do     
                  sacado /Pos. 327 a 334/.                            
31    148 a 149   ESPECIE DE TITULO.. As especies..                   
                  25 - Divida Ativa da Uniao.,                        
                  26 - Divida Ativa de Estado.,                       
                  27 - Divida Ativa de Municipio.,                    
                  somente sao admissiveis nas Carteiras 11 /Cobranca  
                  Simples/ e Carteira 17 /Cobranca Direta especial/
34    157 a 158   PRIMEIRA INSTRUCAO CODIFICADA
                  Se comando 01/IGUAL/Registro de titulo /Pos. 109 a
                  a 110/, codigos admissiveis..
                  00-Ausencia de instrucoes
                  01-Cobrar juros - Dispensavel se informado
                     o valor a ser cobrado por dia de atraso.
                  03-Protestar no terceiro dia util apos vencido
                  04-Protestar no quarto dia util apos vencido
                  05-Protestar no quinto dia util apos vencido
                  06-INDICA PROTESTO EM DIAS CORRIDOS, COM PRAZO DE
                     06 A 29, 35 OU 40 DIAS CORRIDOS. Obrigatorio
                     impostar, nas posicoes 392 a 393 o prazo de
                     protesto desejado.. 6 a 29, 35 ou 40 dias.
                  07-Nao protestar
                  10-Protestar no 10. dia corrido apos vencido
                  15-Protestar no 15. dia corrido apos vencido
                  20-Protestar no 20. dia corrido apos vencido
                  22-Conceder desconto soh ateh a data estipulada
                  25-Protestar no 25. dia corrido apos vencido
                  30-Protestar no 30. dia corrido apos vencido
                  45-Protestar no 45. dia corrido apos vencido
                  Se comando 02/IGUAL/Solicitacao de Baixa /Pos.
                  109 a 110/, codigos admissiveis no campo primeira
                  instrucao codificada.. Seu preenchimento nao eh
                  obrigatorio.
                  42-Devolver
                  44-Baixar
                  46-Entregar ao sacado franco de pagamento
                  Se comando 09/IGUAL/Instrucao para Protestar
                  /Pos. 109 a 110/, codigos admissiveis no campo
                  primeira instrucao codificada/caso esteja em bran
                  co o Sistema assume 05 dias uteis apos vencido/..
                  03-Protestar no 3. dia util apos vencido
                  04-Protestar no 4. dia util apos vencido
                  05-Protestar no 5. dia util apos vencido
                  06 a 30 - Protestar no xx dia corrido apos vencido
                  35-Protestar no 35. dia corrido apos vencido
                  40-Protestar no 40. dia corrido apos vencido
                  45-Protestar no 45. dia corrido apos vencido
                  Obs... a/ Os titulos com vencimento aa vista ou na
                            apresentacao e com instrucao para
                            protesto 03, 04, 05, 10, 15, 20, 25 e 30
                            dias apos o vencimento serao efetivamen
                            te protestados com 18, 19,20,25,30,35,40
                            45 dias respectivamente apos a data do
                            seu registro.,
                         b/ Notas de Debito, Recibos,Notas Promisso
                            rias/exclusivamente em cobranca simples/
                            titulos das carteiras 12/cobranca de
                            premios de seguro/ e 15 nao serao
                            passiveis de protesto.
                            Portanto, nao admitem as instrucoes
                            codificadas 03, 04, 05, 10, 15, 20,25,
                            30 e 45.
36    161 a 173   JUROS DE MORA POR DIA/COMISSAO DE PERMANENCIA..
                  Este dado prevalecera sobre a taxa de juros
                  cadastrada para o cedente. Se nao informado e
                  houver instrucao para cobranca de juros
                  /instrucao 01/, o sistema assumirah a taxa
                  cadastrada para o cedente e, na ausencia desta, a
                  taxa normalmente praticada pelo Banco, na data da
                  liquidacao.
37/38 174 a 192   Quando o comando for igual "35" ou "36", preencher
                  as posi��es 174 a 192 conforme abaixo:
                  Campo                  Conte�do
      174 a 174   Cod Multa              1 - Valor
                                         2 - Percentual
                                         9 - Dispensar Multa
      175 a 180   Multa                      C�digo Multa "1" ou
                                             "2", informar quantidade
                                             de dias para inicio da
                                             da cobran�a da multa
                                             C�digo Multa "9",
                                             informar zeros.
      181 a 192   Vlr-Per-Multa              C�digo Multa  "1",
                                             informar valor da multa.
                                             C�digo Multa  "2",
                                             informar percentual.
                                             C�digo Multa  "9"
                                             informar zeros.
37/38 174 a 176   Preencher as posi��es 174 a  176 conforme abaixo:
                  Comando "38", informar zeros.
                  Comando "39", informar quantidade de dias ap�s o
                  vencimento para recebimento do t�tulo.
38    180 a 192   VALOR DO DESCONTO.. Valor do desconto a ser conce
                  dido. Para que o desconto seja concedido por dia
                  de antecipacao, informar 777777 na /data para des
                  conto/, e o valor do desconto por dia de antecipa
                  pacao.
                  Para BBVENDOR, as informacoes desse campo deverao
                  ser encaminhadas no seguinte layout..
      180 a 184 9(02)V999  Taxa de juros do vendedor, com dois in
                           teiros e tres decimais.
      185 a 189 9(02)V999  Taxa de juros do comprador, com dois in
                           teiros e tres decimais.
      190 a 190 9(01)      Indicativo de IOF Financiado.
                           1 - IOF Financiado
                           0 - IOF nao Financiado
      191 a 192 9(02)      Informar zeros.
51    392 a 393   PRAZO DE PROTESTO..Caso o campo /comando/,Pos. 109
                  a 110, esteja preenchido com /01-registro de
                  titulos/ e o campo /instrucao codificada 1/,
                  Pos. 157 a 158, tenha sido preenchido com /06/,
                  obrigatorio impostar o prazo em dias corridos para
                  protesto.. de 06 a 29, 35 ou 40 dias.



*)
//DETALHE (OBRIGAT�RIO)
//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//01  001 a 001 9/001/   Display  7
  Txt[01] := '7';

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//02  002 a 003 9/002/   Display  Tipo de inscricao da empresa
//                                01-CPF
//                                02-CNPJ
  case Dmod.QrDonoTipo.Value of
    0: Txt[02] := '02';
    1: Txt[02] := '01';
    else Txt[02] := '00'
  end;

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//03  004 a 017 9/014/   Display  Inscricao da empresa
  cpf := Geral.SoNumero_TT(Dmod.QrDonoCNPJ_CPF.Value);
  Txt[03] := AjustaString(cpf, '0', 14, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//04  018 a 021 9/004/   Display  Prefixo da agencia
  Txt[04] := AjustaString(QrConfigBBcedAgencNr.Value, '0', 004, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//05  022 a 022 X/001/   Display  DV-prefixo da agencia
  Txt[05] := AjustaString(QrConfigBBcedAgencDV.Value, ' ', 001, posDireita);


//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//06  023 a 030 9/008/   Display  Codigo do cedente/nr. conta
//                                corrente da empresa/
  Txt[06] := AjustaString(QrConfigBBcedContaNr.Value, '0', 008, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//07  031 a 031 X/001/   Display  DV-codigo do cedente
  Txt[07] := AjustaString(QrConfigBBcedContaDV.Value, ' ', 001, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//08  032 a 038 9/007/   Display  Numero do convenio
  Txt[08] := AjustaString(IntToStr(QrConfigBBConvenio.Value), '0', 007, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//09  039 a 063 X/025/   Display  Nr. controle do participante. Pode
//                                conter qualquer dado de interesse
//                                do cliente.Serah mantido nos
//                                arquivos do Banco sem qualquer
//                                tratamento.
//                                Toda vez que o titulo for incluido
//                                no arquivo-retorno, este numero
//                                tambem serah.
  Txt[09] := AjustaString(IntToStr(QrCobrancaBBItsControle.Value), '0', 025, posEsquerda);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//10  064 a 080 9/017/   Display  Nosso numero. Vide /observacoes/
  Txt[10] := AjustaString('0', '0', 017, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//11  081 a 082 9/002/   Display  Numero da prestacao-informar ZEROS
  Txt[11] := AjustaString('0', '0', 002, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//12  083 a 084 9/002/   Display  Indicativo de grupo de valor
//                                informar ZEROS
  Txt[12] := AjustaString('0', '0', 002, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//13  085 a 087 X/003/   Display  Brancos
  Txt[13] := AjustaString(' ', ' ', 003, posEsquerda);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//14  088 a 088 X/001/   Display  Indicativo de SACADOR..
//                                BRANCOS-nas pos.352 a 391
//                                mensagens a criterio do cedente
//                                A - nas pos.352 a 391 informar
//                                nome e CPF/CNPJ do sacador.
  Txt[14] := AjustaString('A', ' ', 001, posEsquerda);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//15  089 a 091 X/003/   Display  Prefixo do titulo
//                                CARTEIRAS 31,51.. Preencher com SD
//                                CARTEIRA 12.. Preencher com AIU.
//                                DEMAIS CARTEIRAS.. Preencher com
//                                AI.
  case Geral.IMV(Trim(QrConfigBBCarteira.Value)) of
    12: aux := 'AIU';
    31: aux := 'SD';
    51: aux := 'SD';
    else aux := 'AI';
  end;
  Txt[15] := AjustaString(aux, ' ', 003, posEsquerda);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//16  092 a 094 9/003/   Display Variacao da carteira
  aux := Trim(QrConfigBBVariacao.Value);
  Txt[16] := AjustaString(aux, '0', 003, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//17  095 a 095 9/001/   Display Conta caucao.preencher com /ZEROS/.
  Txt[17] := AjustaString('0', '0', 001, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//18  096 a 101 9/006/   Display Numero do bordero..
//                               a/ carteira 51..
//                                  - ZEROS -caso o cedente envie
//                                    mais de uma remessa para o CBR,
//                                    na mesma previa, suas remessas se
//                                    tornarao uma unica no CIOPE.,
//                                  - 900.000 a 999.000 - a remessa
//                                    serah subdividida no CIOPE por
//                                    bordero.
//                               b/ registro de titulos nas carteiras
//                                  31/51 emitido contra empresas coli
//                                  gadas - 900.000 a 999.999.,
//                               c/ demais carteiras - ZEROS
  Txt[18] := AjustaString('0', '0', 006, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//19  102 a 106 X/005/   Display Tipo de Cobran�a:
//                               /04DSC/-solicitacao de registro como
//                                Cobran�a DESCONTADA, quando utilizada
//                                a  Carteira 11 ou 17.
//                               /08VDR/-solicitacao de registro como
//                                Cobran�a VENDOR, quando utilizada a
//                                Carteira 11 ou 17.
//                               /02VIN/-solicita��o de registro como
//                                Cobran�a VINCULADA, quando utilizada
//                                a Carteira 17.
//                               /BRANCOS/ para:
//                                Carteira 11, 12 e 17 - registrar como
//                                Cobran�a Simples
//                                Carteira 31- registrar como Cobran�a
//                                Vinculada
//                                Carteira 51- registrar como Cobran�a
//                                Descontada
  Txt[19] := AjustaString(' ' , ' ', 005, posEsquerda);


//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//20  107 a 108 9/002/   Display Carteira
//                               11-Cobranca Simples
//                               12-Cobranca Indexada
//                               17-Cobranca Direta Especial
//                               31-Cobranca Vinculada
//                               51-Cobranca Descontada
  Txt[20] := AjustaString(Trim(QrConfigBBCarteira.Value), '0', 2, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//21  109 a 110 9/002/   Display Comando
//                               01-Registro de titulos/Ver Primeira
//                                  Inst. Cod.-Posicao 157 a 158/
//                               02-Solicitacao de Baixa /Ver
//                                  Primeira Inst. Cod.-Pos. 157 a
//                                  158/
//                               03-Pedido de debito em conta
//                               04-Concessao de abatimento
//                               05-Cancel. de abatimento
//                               06-Alteracao de vcto. de titulo
//                               07-Alteracao do numero de controle do
//                                  participante
//                               08-Alteracao do numero do titulo dado
//                                  pelo cedente
//                               09-Instrucao para protestar /Ver 1a.
//                                  inst. Cod.-Pos. 157 a 158/
//                               10-Instrucao para sustar protesto
//                               11-Instrucao para dispensar juros
//                               12-Alteracao de nome e endereco do
//                                  sacado
//                               16-Altera��o de juros mora
//                               31-Conceder desconto
//                               32-Nao conceder desconto
//                               33-Retificar desconto
//                               34-Alterar data para desconto
//                               35-Cobrar Multa
//                                  - Vide Observacao do campo 35
//                               36-Dispensar Multa
//                                  - Vide Observacao do campo  35
//                               37-Dispensar Indexador
//                                  - Vide Observacao do campo 35
//                               38-Dispensar Prazo Limite de
//                                  Recebimento
//                                  - Vide Observacao do campo  35
//                               39-Alterar Prazo Limite de
//                                  Recebimento.
//                                  - Vide Observacao do campo 35
//                               40-Alterar Modalidade de Cobran�a:
//                                  Observa�oes:
//                                 a)v�lida apenas para a Carteira 17,
//                                   para alterar o t�tulo da Cobran�a
//                                   Simples para Cobran�a Vinculada ou
//                                   da Cobran�a Vinculada para
//                                   Cobran�a Simples;
//                                 b)a modalidade de Cobran�a para a
//                                   qual se destina a altera��o deve
//                                   ser informada no campo "Tipo de
//                                   Cobran�a" - posi��o 102 a 106.
//                                 c)a efetiva��o dessa instru��o, ap�s
//                                   o seu processamento pelo Sistema
//                                   do Banco, depende de libera��o
//                                   da ag�ncia de relacionamento com a
//                                   empresa cedente.

////////////////////////////////////////////////////////////////////////////////
//                               01-Registro de titulos/Ver Primeira
//                                  Inst. Cod.-Posicao 157 a 158/
  Txt[21] := '01';


//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//22  111 a 120 X/010/   Display Seu Numero - Nr. titulo dado pelo
//                               cedente - vide obs. do campo 9
  Txt[22] := AjustaString(Trim(QrCobrancaBBItsDuplicata.Value), ' ', 10, posEsquerda);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//23  121 a 126 9/006/   Display Data de vencimento /DDMMAA/
//                               888888-a vista
//                               999999-na apresentacao
//                               Obs.. Em ambos os casos, o vencimento
//                                     ocorrerah 15 dias apos a data
//                                     do registro no Banco.
  Txt[23] := FormatDateTime('ddmmyy', QrCobrancaBBItsVencto.Value);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//24  127 a 139 9/011/v99Display Valor do titulo
//                               Carteira 12- em reais, na data de
//                               emissao do titulo
  Val := Geral.SoNumero_TT(Geral.FFT(
    Trunc(QrCobrancaBBItsBruto.Value * 100), 0, siPositivo));
  Txt[24] := AjustaString(val, '0', 13, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//25  140 a 142 9/003/   Display Numero do Banco - /001/
  Txt[25] := '001';

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//26  143 a 146 9/004/   Display Prefixo da ag. Cobradora
//                               preencher com /ZEROS/.O Sistema
//                               indicarah a agencia cobradora pelo
//                               CEP do sacado.
  Txt[26] := AjustaString('0', '0', 004, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//27  147 a 147 X/001/   Display DV-pref. agencia cobradora..
  Txt[27] := AjustaString('0', '0', 001, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//28  148 a 149 9/002/   Display Especie de titulo
//                               01-Duplicata Mercantil
//                               02-Nota Promissoria
//                               03-Nota de Seguro
//                               05-Recibo
//                               08-Letra de Cambio
//                               09-Warrant
//                               10-Cheque
//                               12-Duplicata de Servico
//                               13-Nota de Debito
//                               15-Apolice de Seguro
//                               21-Duplicata Rural
//                               25-Divida Ativa da Uniao     /OBS./
//                               26-Divida Ativa de Estado    /OBS./
//                               27-Divida Ativa de Municipio /OBS./
  //Parei Aqui !!
  Txt[28] := AjustaString(IntToStr(QrConfigBBEspecie.Value), '0', 002, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//29  150 a 150 X/001/   Display Aceite
//                               N-sem aceite
//                               A-com aceite
  case QrConfigBBAceite.Value of
    0: Txt[29] := 'N';
    1: Txt[29] := 'A';
    else Txt[29] := '?';
  end;

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//30  151 a 156 9/006/   Display Data de emissao /DDMMAA/
  Txt[30] := FormatDateTime('ddmmyy', QrCobrancaBBItsEmissao.Value);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//31  157 a 158 9/002/   Display 1a instrucao codificada - vide
//                               /OBSERVACOES/
  Txt[31] :=
    AjustaString(IntToStr(QrCobrancaBBItsProtestar.Value), '0', 2, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//32  159 a 160 9/002/   Display 2a instrucao codificada
  Txt[32] := '01';

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//33  161 a 173 9/011/v99Display Juros de mora por dia-vide
//                               /OBSERVACOES/
  d := (QrCobrancaBBItsJuroSacado.Value / 30);
  d := d * QrCobrancaBBItsBruto.Value; { / 100}
  Txt[33] := AjustaString(IntToStr(Trunc(d)), '0', 013, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//34  174 a 179 9/006/   Display Data limite para Concessao de desconto
//                               /DDMMAA/ . O registro serah
//                               recusado sempre que a data de
//                               desconto for informada e o desconto
//                               nao o for, ou a data do desconto for
//                               superior a data do vencimento.
//                               Data da operacao BBVENDOR, no formato
//                               DDMMAA.
  if (QrCobrancaBBItsDescAte.Value > 2) and (QrCobrancaBBItsDesco.Value > 0) then
    Txt[34] := FormatDateTime('ddmmyy', QrCobrancaBBItsDescAte.Value)
  else
    Txt[34] := AjustaString('0', '0', 006, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//35  180 a 192 9/011/v99Display Valor do desconto
//                               Se desconto por dia de antecipacao,
//                               informar valor por dia e 777777 no
//                               campo /data limite para concessao de
//                               desconto/
//                               Para BBVENDOR - vide observacoes
  if (QrCobrancaBBItsDescAte.Value > 2) and (QrCobrancaBBItsDesco.Value > 0) then
  begin
    d := QrCobrancaBBItsDesco.Value * 100;
    Txt[35] := AjustaString(IntToStr(Trunc(d)), '0', 013, posDireita);
  end else
    Txt[35] := AjustaString('0', '0', 013, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//36  193 a 205 9/011/v99Display Cart. 15.. Valor do IOF
//                               Cart. 12..
//                               1/ pos. 193 a 204.. quantidade de
//                                  unidades variaveis na data de
//                                  emissao do titulo. Ate sete
//                                  inteiros e, obrigatoriamente,cinco
//                                  decimais.,
//                               2/ pos. 205.. especie de unidade
//                                  variavel..
//                                  1-FAJTR 5-MARCO ALEMAO
//                                  2-DOLAR 6-FTR
//                                  3-EURO  7-IDTR
//                                  4-IENE
//                               3/ Demais carteiras-preencher com
//                                  ZEROS.
  Txt[36] := AjustaString('0', '0', 013, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//37  206 a 218 9/011/v99Display Valor do abatimento permitido no
//                               registro das carteiras.. 11, 17 e 31
  Txt[37] := AjustaString('0', '0', 013, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//38  219 a 220 9/002/   Display Tipo de inscricao do sacado
//                               00 - ISENTO
//                               01 - CPF
//                               02 - CNPJ
//
  cpf := Geral.SoNumero_TT(QrCobrancaBBItsCPF.Value);
  case Length(cpf) of
    11: Txt[38] := '01';
    14: Txt[38] := '02';
    else Txt[38] := '00';
  end;

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//39  221 a 234 9/014/   Display CNPJ ou CPF do sacado
  Txt[39] := AjustaString(cpf, '0', 14, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//40  235 a 271 X/037/   Display Nome do sacado
  Txt[40] := AjustaString(QrCobrancaBBItsNome.Value, ' ', 037, posEsquerda);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//41  272 a 274 X/003/   Display Brancos
  Txt[41] := AjustaString(' ', ' ', 003, posEsquerda);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//42  275 a 311 X/037/   Display Endereco do sacado
  aux := QrCobrancaBBItsRua.Value;
  if QrCobrancaBBItsNumero.Value > 0 then
    aux := aux + ', ' + IntToStr(Trunc(QrCobrancaBBItsNumero.Value));
  if QrCobrancaBBItsCompl.Value <> '' then
    aux := aux + ' ' + QrCobrancaBBItsCompl.Value;
  if QrCobrancaBBItsBairro.Value <> '' then
    aux := aux + ' ' + QrCobrancaBBItsBairro.Value;
  aux := AjustaString(aux, ' ', 37, posEsquerda);
  Txt[42] := AjustaString(aux, ' ', 037, posEsquerda);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//43  312 a 326 X/015/   Display Brancos
  Txt[43] := AjustaString(' ', ' ', 015, posEsquerda);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//44  327 a 334 9/008/   Display CEP do endereco do sacado
  aux := Geral.FormataCEP_NT(QrCobrancaBBItsCEP.Value);
  aux := Geral.SoNumero_TT(Aux);
  aux := AjustaString(aux, '0', 8, posDireita);
  Txt[44] := aux;

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//45  335 a 349 X/015/   Display Cidade do sacado
  Txt[45] := AjustaString(QrCobrancaBBItsCidade.Value, ' ', 15, posEsquerda);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//46  350 a 351 X/002/   Display UF da cidade do sacado
  Txt[46] := AjustaString(QrCobrancaBBItsUF.Value, '?', 02, posEsquerda);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//47  352 a 391 X/040/   Display Observacoes
//                               Se a posicao 82 for informado
//                               BRANCO..
//                               Os dados informados nesse campo
//                               serao listados no campo /instrucoes/
//                               do boleto de cobranca e NAO SERAO
//                               IMPRESSOS NO CASO DE EMISSAO DE 2.
//                               VIA NAS AGENCIAS DO BB OU ATRAVES DO
//                               BB OFFICE BANKING. Nao informar dados
//                               conflitantes com as informacoes   dos
//                               demais campos, como juros,IOF,
//                               desconto, protesto, etc.
//                               Se a posicao 82 for informado A..
//                               Os dados informados nesse campo serao
//                               listados no campo/SACADOR-AVALISTA/
//                               do fichamento de cobranca.
//                               Neste caso, o campo deverah ser
//                               preenchido com o nome do SACADOR/AVA
//                               LISTA, seguido do literal CNPJ ou CPF
//                               e do numero do documento, sem traco
//                               ou barra.
//                               Entre o nome do SACADOR/AVALISTA e o
//                               literal CNPJ/CPF deverah ser dado um
//                               /espaco/ em branco.
  aux := Geral.SoNumero_TT(QrCobrancaBBItsCNPJCLI.Value);
  if aux <> cpf then
  begin
    case Length(aux) of
      11: aux := ' CPF' + aux;
      14: aux := ' CNPJ' + aux;
      else aux := '';
    end;
    i := Length(aux);
    Txt[47] := AjustaString(QrCobrancaBBItsNOMECLI.Value, ' ', 40 - i,
        posEsquerda) + aux;
  end else Txt[47] := AjustaString(' ', ' ', 40 , posEsquerda);
//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//48  392 a 393 X/002/   Display Numero de dias para protesto-caso o
//                               campo /comando/esteja preenchido com
//                               /01-registro de titulos/ e o campo
//                               /instrucao codificada 1/ tenha sido
//                               preenchido com /06/,informar o numero
//                               numero de dias para protesto, de
//                               /06 a 29, 35 ou 40/dias,senao ZEROS
  if Txt[31] <> '06' then Txt[48] := '00' else Txt[48] :=
    AjustaString(IntToStr(QrConfigBBCorrido.Value), ' ', 2, posDireita);

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//49  394 a 394 X/001/   Display BRANCO
  Txt[49] := ' ';

//..................................................................
//N.  POSICOES  PICTURE  USAGE    CONTEUDO
//..................................................................
//50  395 a 400 9/006/   Display Sequencial de registro- incrementar
//                               em 1 a cada novo registro
  Txt[50] := FormatFloat('000000', Memo1.Lines.Count+1);

  //////////////////////////////////////////////////////////////////////////////
  Result := '';
  for i := 1 to MaxS do
    Result := Result + Txt[i];
end;

function TFmCobrancaBB.GeraTrailer(Tipo: TTipoGera): String;
const
  MaxS = 3;
var
  i: Integer;
  Txt: array[1..MaxS] of String;
begin
  for i := 1 to MaxS do Txt[i] := '';

//TRAILER
//....................................................................
//N.  POSICOES  PICTURE USAGE     CONTEUDO
//....................................................................
//01  001 a 001 9/001/  Display   9
  Txt[01] := '9';

//02  002 a 394 X/393/  Display   Brancos
  Txt[02] := AjustaString(' ', ' ', 393, posEsquerda);

//03  395 a 400 9/006/  Display   Sequencial do ultimo registro*)
  Txt[03] := FormatFloat('000000', Memo1.Lines.Count+1);

  //////////////////////////////////////////////////////////////////////////////
  Result := '';
  for i := 1 to MaxS do
    Result := Result + Txt[i];
end;

end.

