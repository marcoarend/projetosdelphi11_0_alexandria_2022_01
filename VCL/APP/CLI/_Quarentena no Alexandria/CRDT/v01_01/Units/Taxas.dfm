object FmTaxas: TFmTaxas
  Left = 368
  Top = 194
  Caption = 'Cadastro de Taxas'
  ClientHeight = 425
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 377
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    ExplicitHeight = 288
    object PainelConfirma: TPanel
      Left = 1
      Top = 328
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 1
      ExplicitTop = 239
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 681
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 304
      Align = alTop
      TabOrder = 0
      ExplicitLeft = 0
      ExplicitTop = 2
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label9: TLabel
        Left = 64
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 369
        Top = 4
        Width = 53
        Height = 13
        Caption = '% ou Valor:'
        FocusControl = DBEdNome
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 64
        Top = 20
        Width = 300
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdValor: TdmkEdit
        Left = 369
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        QryCampo = 'Valor'
        UpdCampo = 'Valor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object RGForma: TdmkRadioGroup
        Left = 4
        Top = 196
        Width = 445
        Height = 45
        Caption = ' Forma de cobran'#231'a: '
        Columns = 2
        Items.Strings = (
          'Valor'
          'Porcentagem')
        TabOrder = 6
        QryCampo = 'Forma'
        UpdCampo = 'Forma'
        UpdType = utYes
        OldValor = 0
      end
      object RGBase: TdmkRadioGroup
        Left = 4
        Top = 246
        Width = 445
        Height = 45
        Caption = ' Base da multiplicadora da cobran'#231'a: '
        Columns = 2
        Items.Strings = (
          'Valor total dos itens'
          'Contagem dos Itens')
        TabOrder = 7
        QryCampo = 'Base'
        UpdCampo = 'Base'
        UpdType = utYes
        OldValor = 0
      end
      object CGAutoma: TdmkCheckGroup
        Left = 4
        Top = 146
        Width = 445
        Height = 45
        Caption = ' Insere autom'#225'tico '
        Columns = 2
        Items.Strings = (
          'Cheques'
          'Duplicatas')
        TabOrder = 5
        QryCampo = 'Automatico'
        UpdCampo = 'Automatico'
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
      object CGMostra: TdmkCheckGroup
        Left = 4
        Top = 97
        Width = 445
        Height = 45
        Caption = 'Modo de reportar'
        Columns = 2
        Items.Strings = (
          'Cont'#225'bil'
          'Extrato')
        TabOrder = 4
        QryCampo = 'Mostra'
        UpdCampo = 'Mostra'
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
      object CGGenero: TdmkCheckGroup
        Left = 4
        Top = 47
        Width = 445
        Height = 45
        Caption = 'G'#234'nero'
        Columns = 2
        Items.Strings = (
          'Cheques'
          'Duplicatas')
        TabOrder = 3
        QryCampo = 'Genero'
        UpdCampo = 'Genero'
        UpdType = utYes
        Value = 0
        OldValor = 0
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 377
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    ExplicitLeft = -2
    ExplicitTop = 50
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 304
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 64
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBText1: TDBText
        Left = 370
        Top = 4
        Width = 65
        Height = 13
        DataField = 'FORMA_TXT'
        DataSource = DsTaxas
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsTaxas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 64
        Top = 20
        Width = 300
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsTaxas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 370
        Top = 20
        Width = 80
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Valor'
        DataSource = DsTaxas
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBCheckGroup1: TdmkDBCheckGroup
        Left = 4
        Top = 47
        Width = 446
        Height = 45
        Caption = 'G'#234'nero'
        Columns = 2
        DataField = 'Genero'
        DataSource = DsTaxas
        Items.Strings = (
          'Cheques'
          'Duplicatas')
        ParentBackground = False
        TabOrder = 3
      end
      object dmkDBCheckGroup2: TdmkDBCheckGroup
        Left = 4
        Top = 97
        Width = 446
        Height = 45
        Caption = 'Modo de reportar'
        Columns = 2
        DataField = 'Mostra'
        DataSource = DsTaxas
        Items.Strings = (
          'Cont'#225'bil'
          'Extrato')
        ParentBackground = False
        TabOrder = 4
      end
      object dmkDBCheckGroup3: TdmkDBCheckGroup
        Left = 4
        Top = 146
        Width = 446
        Height = 45
        Caption = 'Insere autom'#225'tico'
        Columns = 2
        DataField = 'Automatico'
        DataSource = DsTaxas
        Items.Strings = (
          'Cheques'
          'Duplicatas')
        ParentBackground = False
        TabOrder = 5
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 4
        Top = 196
        Width = 446
        Height = 45
        Caption = 'Forma de cobran'#231'a'
        Columns = 2
        DataField = 'Forma'
        DataSource = DsTaxas
        Items.Strings = (
          'Valor'
          'Porcentagem')
        ParentBackground = True
        TabOrder = 6
        Values.Strings = (
          '0'
          '1')
      end
      object DBRadioGroup2: TDBRadioGroup
        Left = 4
        Top = 246
        Width = 446
        Height = 45
        Caption = 'Base multiplicadora da cobran'#231'a'
        Columns = 2
        DataField = 'Base'
        DataSource = DsTaxas
        Items.Strings = (
          'Valor total dos itens'
          'Contagem dos Itens')
        ParentBackground = True
        TabOrder = 7
        Values.Strings = (
          '0'
          '1')
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 328
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 239
      object LaRegistro: TStaticText
        Left = 173
        Top = 1
        Width = 30
        Height = 17
        Align = alClient
        BevelKind = bkTile
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 320
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Cadastro de Taxas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 709
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 483
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PnPesq: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsTaxas: TDataSource
    DataSet = QrTaxas
    Left = 40
    Top = 12
  end
  object QrTaxas: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrTaxasBeforeOpen
    AfterOpen = QrTaxasAfterOpen
    OnCalcFields = QrTaxasCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM taxas')
    Left = 12
    Top = 12
    object QrTaxasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTaxasFORMA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FORMA_TXT'
      Size = 5
      Calculated = True
    end
    object QrTaxasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrTaxasGenero: TSmallintField
      FieldName = 'Genero'
    end
    object QrTaxasForma: TSmallintField
      FieldName = 'Forma'
    end
    object QrTaxasMostra: TSmallintField
      FieldName = 'Mostra'
    end
    object QrTaxasBase: TSmallintField
      FieldName = 'Base'
    end
    object QrTaxasAutomatico: TSmallintField
      FieldName = 'Automatico'
    end
    object QrTaxasValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,##0.000000'
    end
    object QrTaxasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTaxasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTaxasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTaxasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTaxasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTaxasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrTaxasAtivo: TSmallintField
      FieldName = 'Ativo'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 68
    Top = 12
  end
end
