object FmOcorreuC: TFmOcorreuC
  Left = 199
  Top = 178
  Caption = 'Ocorr'#234'ncias em Clientes'
  ClientHeight = 533
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 485
    Align = alClient
    BevelOuter = bvNone
    Color = clAppWorkSpace
    TabOrder = 0
    object PnEdit: TPanel
      Left = 0
      Top = 245
      Width = 1008
      Height = 192
      Align = alBottom
      TabOrder = 2
      Visible = False
      object PanelFill1: TPanel
        Left = 1
        Top = 1
        Width = 1006
        Height = 24
        Align = alTop
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -32
        Font.Name = 'Times New Roman'
        Font.Style = []
        ParentColor = True
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object LaTipo: TLabel
          Left = 923
          Top = 1
          Width = 82
          Height = 22
          Align = alRight
          Alignment = taCenter
          AutoSize = False
          Caption = 'Travado'
          Font.Charset = ANSI_CHARSET
          Font.Color = 8281908
          Font.Height = -15
          Font.Name = 'Times New Roman'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          ExplicitLeft = 930
          ExplicitTop = 2
          ExplicitHeight = 20
        end
      end
      object PnOcor: TPanel
        Left = 1
        Top = 25
        Width = 1006
        Height = 52
        Align = alTop
        TabOrder = 1
        object Label22: TLabel
          Left = 460
          Top = 8
          Width = 27
          Height = 13
          Caption = 'Valor:'
        end
        object Label21: TLabel
          Left = 352
          Top = 8
          Width = 26
          Height = 13
          Caption = 'Data:'
        end
        object Label20: TLabel
          Left = 8
          Top = 8
          Width = 55
          Height = 13
          Caption = 'Ocorr'#234'ncia:'
        end
        object Label6: TLabel
          Left = 636
          Top = 8
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label7: TLabel
          Left = 548
          Top = 8
          Width = 84
          Height = 13
          Caption = 'Taxa atualiza'#231#227'o:'
        end
        object EdValor: TdmkEdit
          Left = 460
          Top = 24
          Width = 85
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object TPDataO: TDateTimePicker
          Left = 352
          Top = 24
          Width = 105
          Height = 21
          Date = 38711.818866817100000000
          Time = 38711.818866817100000000
          TabOrder = 2
        end
        object CBOcorrencia: TdmkDBLookupComboBox
          Left = 76
          Top = 24
          Width = 273
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsOcorBank
          TabOrder = 1
          dmkEditCB = EdOcorrencia
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdOcorrencia: TdmkEditCB
          Left = 8
          Top = 24
          Width = 65
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          OnChange = EdOcorrenciaChange
          DBLookupComboBox = CBOcorrencia
          IgnoraDBLookupComboBox = False
        end
        object EdDescri: TdmkEdit
          Left = 636
          Top = 24
          Width = 285
          Height = 21
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdTaxaP: TdmkEdit
          Left = 548
          Top = 24
          Width = 85
          Height = 21
          Alignment = taRightJustify
          TabOrder = 5
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 4
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object PnBase: TPanel
        Left = 1
        Top = 77
        Width = 1006
        Height = 46
        Align = alTop
        TabOrder = 2
        TabStop = True
        Visible = False
        object Label86: TLabel
          Left = 8
          Top = 4
          Width = 52
          Height = 13
          Caption = 'Data base:'
        end
        object Label87: TLabel
          Left = 100
          Top = 4
          Width = 53
          Height = 13
          Caption = 'Valor base:'
        end
        object Label88: TLabel
          Left = 196
          Top = 4
          Width = 89
          Height = 13
          Caption = '% Taxa juros base:'
        end
        object Label89: TLabel
          Left = 292
          Top = 4
          Width = 79
          Height = 13
          Caption = '% Juros per'#237'odo:'
        end
        object TPDataBase6: TDateTimePicker
          Left = 8
          Top = 20
          Width = 89
          Height = 21
          Date = 38698.785142685200000000
          Time = 38698.785142685200000000
          Color = clBtnFace
          TabOrder = 0
          TabStop = False
        end
        object EdValorBase6: TdmkEdit
          Left = 100
          Top = 20
          Width = 93
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdValorBase6Change
        end
        object EdJurosBase6: TdmkEdit
          Left = 196
          Top = 20
          Width = 93
          Height = 21
          Alignment = taRightJustify
          Color = clWhite
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdJurosBase6Change
        end
        object EdJurosPeriodo6: TdmkEdit
          Left = 292
          Top = 20
          Width = 93
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object PnPaga: TPanel
        Left = 1
        Top = 132
        Width = 1006
        Height = 59
        Align = alBottom
        TabOrder = 3
        Visible = False
        object Label82: TLabel
          Left = 8
          Top = 4
          Width = 26
          Height = 13
          Caption = 'Data:'
        end
        object Label83: TLabel
          Left = 100
          Top = 4
          Width = 37
          Height = 13
          Caption = '$ Juros:'
        end
        object Label84: TLabel
          Left = 292
          Top = 4
          Width = 75
          Height = 13
          Caption = '$ Valor a pagar:'
        end
        object Label85: TLabel
          Left = 196
          Top = 4
          Width = 66
          Height = 13
          Caption = 'Total a pagar:'
        end
        object TPPagto6: TDateTimePicker
          Left = 8
          Top = 20
          Width = 89
          Height = 21
          Date = 38698.785142685200000000
          Time = 38698.785142685200000000
          TabOrder = 0
          OnChange = TPPagto6Change
        end
        object EdJuros6: TdmkEdit
          Left = 100
          Top = 20
          Width = 93
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdJuros6Change
        end
        object EdPago6: TdmkEdit
          Left = 296
          Top = 20
          Width = 93
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object EdAPagar6: TdmkEdit
          Left = 196
          Top = 20
          Width = 93
          Height = 21
          TabStop = False
          Alignment = taRightJustify
          Color = clBtnFace
          ReadOnly = True
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnChange = EdAPagar6Change
        end
      end
    end
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 104
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object PainelPesq: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 52
        Align = alTop
        TabOrder = 0
        object Label75: TLabel
          Left = 8
          Top = 4
          Width = 35
          Height = 13
          Caption = 'Cliente:'
        end
        object EdCliente: TdmkEditCB
          Left = 8
          Top = 20
          Width = 52
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBCliente
          IgnoraDBLookupComboBox = False
        end
        object CBCliente: TdmkDBLookupComboBox
          Left = 61
          Top = 20
          Width = 377
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMECLIENTE'
          ListSource = DsClientes
          TabOrder = 1
          dmkEditCB = EdCliente
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CGTipo: TdmkCheckGroup
          Left = 444
          Top = 1
          Width = 225
          Height = 50
          Caption = ' Tipo de ocorr'#234'ncia: '
          Columns = 3
          Items.Strings = (
            'Direto'
            'Cheques'
            'Duplicatas')
          TabOrder = 2
          UpdType = utYes
          Value = 0
          OldValor = 0
        end
        object CkAbertas: TCheckBox
          Left = 674
          Top = 20
          Width = 105
          Height = 17
          Caption = 'Somente abertas.'
          Checked = True
          State = cbChecked
          TabOrder = 3
        end
        object TPIni: TDateTimePicker
          Left = 780
          Top = 23
          Width = 109
          Height = 21
          Date = 38698.582684641200000000
          Time = 38698.582684641200000000
          TabOrder = 5
        end
        object TPFim: TDateTimePicker
          Left = 892
          Top = 23
          Width = 109
          Height = 21
          Date = 38698.582684641200000000
          Time = 38698.582684641200000000
          TabOrder = 7
        end
        object CkIni: TCheckBox
          Left = 780
          Top = 4
          Width = 109
          Height = 17
          Caption = 'Data Ocorr. inicial:'
          TabOrder = 4
        end
        object CkFim: TCheckBox
          Left = 892
          Top = 4
          Width = 109
          Height = 17
          Caption = 'Data Ocorr. final:'
          TabOrder = 6
        end
      end
      object BtReabre: TBitBtn
        Tag = 18
        Left = 12
        Top = 58
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Reabre'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtReabreClick
      end
      object BtDesfazOrdenacao: TBitBtn
        Tag = 329
        Left = 105
        Top = 58
        Width = 150
        Height = 40
        Cursor = crHandPoint
        Caption = 'Desfazer ordena'#231#227'o'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtDesfazOrdenacaoClick
      end
    end
    object PainelData: TPanel
      Left = 0
      Top = 104
      Width = 1008
      Height = 132
      Align = alTop
      BevelOuter = bvLowered
      TabOrder = 1
      object DBGrid5: TdmkDBGrid
        Left = 2
        Top = 1
        Width = 790
        Height = 60
        Columns = <
          item
            Expanded = False
            FieldName = 'TIPODOC'
            Title.Caption = 'TD'
            Width = 20
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DOCUM_TXT'
            Title.Caption = 'Documento / Descri'#231#227'o'
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DCompra'
            Title.Caption = 'Compra'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEOCORRENCIA'
            Title.Caption = 'Ocorr'#234'ncia'
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataO'
            Title.Caption = 'Data'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TaxaV'
            Title.Caption = '$ Taxa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pago'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SALDO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA3_TXT'
            Title.Caption = 'Ult.Pg'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ATUALIZADO'
            Title.Caption = 'Atualiz.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Emitente'
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CPF'
            Title.Caption = 'CNPJ / CPF'
            Width = 113
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Emissao'
            Title.Caption = 'Emitido'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECLIENTE'
            Title.Caption = 'Cliente'
            Width = 250
            Visible = True
          end>
        Color = clWindow
        DataSource = DsOcorreu
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        FieldsCalcToOrder.Strings = (
          'DOCUM_TXT=Descri'
          'DATA3_TXT=Data3'
          'ATUALIZADO=DataO')
        Columns = <
          item
            Expanded = False
            FieldName = 'TIPODOC'
            Title.Caption = 'TD'
            Width = 20
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DOCUM_TXT'
            Title.Caption = 'Documento / Descri'#231#227'o'
            Width = 160
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DCompra'
            Title.Caption = 'Compra'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEOCORRENCIA'
            Title.Caption = 'Ocorr'#234'ncia'
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DataO'
            Title.Caption = 'Data'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TaxaV'
            Title.Caption = '$ Taxa'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Pago'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'SALDO'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'DATA3_TXT'
            Title.Caption = 'Ult.Pg'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'ATUALIZADO'
            Title.Caption = 'Atualiz.'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Emitente'
            Width = 180
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CPF'
            Title.Caption = 'CNPJ / CPF'
            Width = 113
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Emissao'
            Title.Caption = 'Emitido'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECLIENTE'
            Title.Caption = 'Cliente'
            Width = 250
            Visible = True
          end>
      end
      object Panel2: TPanel
        Left = 1
        Top = 36
        Width = 1006
        Height = 95
        Align = alBottom
        BevelOuter = bvLowered
        TabOrder = 1
        object DBGrid3_: TDBGrid
          Left = 1
          Top = 1
          Width = 333
          Height = 93
          Align = alLeft
          DataSource = DsOcorrPg
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Juros'
              Width = 88
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Pago'
              Width = 88
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LotePg'
              Title.Caption = 'Lote pagto.'
              Width = 60
              Visible = True
            end>
        end
        object Panel3: TPanel
          Left = 334
          Top = 1
          Width = 671
          Height = 93
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object GroupBox1: TGroupBox
            Left = 0
            Top = 0
            Width = 671
            Height = 54
            Align = alTop
            Caption = ' Totais pesquisa: '
            TabOrder = 0
            object Label1: TLabel
              Left = 5
              Top = 14
              Width = 27
              Height = 13
              Caption = 'Valor:'
            end
            object Label2: TLabel
              Left = 91
              Top = 14
              Width = 41
              Height = 13
              Caption = '$ Taxas:'
            end
            object Label3: TLabel
              Left = 177
              Top = 14
              Width = 28
              Height = 13
              Caption = 'Pago:'
            end
            object Label4: TLabel
              Left = 263
              Top = 14
              Width = 39
              Height = 13
              Caption = 'SALDO:'
            end
            object Label5: TLabel
              Left = 351
              Top = 15
              Width = 71
              Height = 13
              Caption = 'ATUALIZADO:'
            end
            object EdVal: TdmkEdit
              Left = 5
              Top = 30
              Width = 85
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdTxV: TdmkEdit
              Left = 91
              Top = 30
              Width = 85
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdPag: TdmkEdit
              Left = 177
              Top = 30
              Width = 85
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdSal: TdmkEdit
              Left = 263
              Top = 30
              Width = 85
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object EdAtz: TdmkEdit
              Left = 351
              Top = 30
              Width = 85
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 4
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object BtBordero: TBitBtn
            Tag = 14
            Left = 4
            Top = 57
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Border'#244
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtBorderoClick
          end
          object BitBtn1: TBitBtn
            Tag = 14
            Left = 96
            Top = 57
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Recibo'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BitBtn1Click
          end
        end
      end
    end
    object PainelBtns: TPanel
      Left = 0
      Top = 437
      Width = 1008
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object PainelCtrl: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 48
        Align = alClient
        Color = clAppWorkSpace
        TabOrder = 0
        object PainelBtsE: TPanel
          Left = 1
          Top = 1
          Width = 612
          Height = 46
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object BtIncluiOcor: TBitBtn
            Tag = 10
            Left = 12
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = 'I&nclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtIncluiOcorClick
          end
          object BtAltera: TBitBtn
            Tag = 11
            Left = 104
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = 'A&ltera'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtAlteraClick
          end
          object BtExclui: TBitBtn
            Tag = 12
            Left = 196
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = 'E&xclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtExcluiClick
          end
        end
        object Panel6: TPanel
          Left = 891
          Top = 1
          Width = 116
          Height = 46
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 11
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
      object PnConf: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 48
        Align = alClient
        TabOrder = 1
        Visible = False
        object BtConfirma5: TBitBtn
          Tag = 14
          Left = 12
          Top = 5
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirma5Click
        end
        object BtDesiste5: TBitBtn
          Tag = 15
          Left = 688
          Top = 5
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtDesiste5Click
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Ocorr'#234'ncias em Clientes'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitWidth = 1012
      ExplicitHeight = 44
    end
    object SbImprime: TBitBtn
      Tag = 5
      Left = 4
      Top = 3
      Width = 40
      Height = 40
      NumGlyphs = 2
      TabOrder = 0
      OnClick = SbImprimeClick
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrClientesCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo, FatorCompra'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMECLIENTE')
    Left = 161
    Top = 33
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
    end
    object QrClientesTAXA_COMPRA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TAXA_COMPRA'
      Calculated = True
    end
    object QrClientesFatorCompra: TFloatField
      FieldName = 'FatorCompra'
      Origin = 'entidades.FatorCompra'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 189
    Top = 33
  end
  object DsOcorreu: TDataSource
    DataSet = QrOcorreu
    Left = 593
    Top = 9
  end
  object QrOcorBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ocorbank'
      'ORDER BY Nome')
    Left = 161
    Top = 63
    object QrOcorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrOcorBankLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorBankDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorBankDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorBankUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorBankUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorBankBase: TFloatField
      FieldName = 'Base'
    end
  end
  object DsOcorBank: TDataSource
    DataSet = QrOcorBank
    Left = 189
    Top = 63
  end
  object QrLastOcor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Codigo'
      'FROM ocorrpg'
      'WHERE Ocorreu=:P0'
      'ORDER BY Data Desc, Codigo Desc')
    Left = 57
    Top = 169
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLastOcorData: TDateField
      FieldName = 'Data'
    end
    object QrLastOcorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSumOc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Juros) Juros, SUM(Pago) Pago'
      'FROM ocorrpg'
      'WHERE Ocorreu=:P0')
    Left = 29
    Top = 169
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumOcJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumOcPago: TFloatField
      FieldName = 'Pago'
    end
  end
  object QrLocOc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ocorrpg'
      'WHERE Data=('
      'SELECT Max(Data) FROM ocorrpg'
      'WHERE Ocorreu=:P0)'
      'ORDER BY Codigo DESC')
    Left = 1
    Top = 169
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocOcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocOcOcorreu: TIntegerField
      FieldName = 'Ocorreu'
    end
    object QrLocOcData: TDateField
      FieldName = 'Data'
    end
    object QrLocOcJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrLocOcPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLocOcLotePg: TIntegerField
      FieldName = 'LotePg'
    end
    object QrLocOcLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocOcDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocOcDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocOcUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocOcUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object QrOcorreu: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrOcorreuAfterOpen
    BeforeClose = QrOcorreuBeforeClose
    AfterScroll = QrOcorreuAfterScroll
    OnCalcFields = QrOcorreuCalcFields
    SQL.Strings = (
      'SELECT IF(oc.Cliente > 0,'#39'CL'#39',IF(lo.Tipo=0,"CH","DU"))TIPODOC, '
      'ob.Nome NOMEOCORRENCIA, li.Banco, li.Agencia, li.Cheque, '
      
        'li.Duplicata, li.Conta, lo.Tipo, li.Emissao, li.DCompra, li.Venc' +
        'to, '
      'li.DDeposito, li.Emitente, li.CPF, lo.Cliente CLIENTELOTE, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE (lo.Cliente=:P0 OR oc.Cliente=:P1)'
      ''
      ''
      ''
      '')
    Left = 565
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOcorreuTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lotes.Tipo'
    end
    object QrOcorreuTIPODOC: TWideStringField
      FieldName = 'TIPODOC'
      Size = 2
    end
    object QrOcorreuNOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Origin = 'ocorbank.Nome'
      Size = 50
    end
    object QrOcorreuCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ocorreu.Codigo'
      Required = True
    end
    object QrOcorreuLotesIts: TIntegerField
      FieldName = 'LotesIts'
      Origin = 'ocorreu.LotesIts'
      Required = True
    end
    object QrOcorreuDataO: TDateField
      FieldName = 'DataO'
      Origin = 'ocorreu.DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Origin = 'ocorreu.Ocorrencia'
      Required = True
    end
    object QrOcorreuValor: TFloatField
      FieldName = 'Valor'
      Origin = 'ocorreu.Valor'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrOcorreuLoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Origin = 'ocorreu.LoteQuit'
      Required = True
    end
    object QrOcorreuLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'ocorreu.Lk'
    end
    object QrOcorreuDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'ocorreu.DataCad'
    end
    object QrOcorreuDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'ocorreu.DataAlt'
    end
    object QrOcorreuUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'ocorreu.UserCad'
    end
    object QrOcorreuUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'ocorreu.UserAlt'
    end
    object QrOcorreuTaxaP: TFloatField
      FieldName = 'TaxaP'
      Origin = 'ocorreu.TaxaP'
      Required = True
    end
    object QrOcorreuTaxaV: TFloatField
      FieldName = 'TaxaV'
      Origin = 'ocorreu.TaxaV'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrOcorreuPago: TFloatField
      FieldName = 'Pago'
      Origin = 'ocorreu.Pago'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrOcorreuDataP: TDateField
      FieldName = 'DataP'
      Origin = 'ocorreu.DataP'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuTaxaB: TFloatField
      FieldName = 'TaxaB'
      Origin = 'ocorreu.TaxaB'
      Required = True
    end
    object QrOcorreuData3: TDateField
      FieldName = 'Data3'
      Origin = 'ocorreu.Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuStatus: TSmallintField
      FieldName = 'Status'
      Origin = 'ocorreu.Status'
      Required = True
    end
    object QrOcorreuATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrOcorreuDOCUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOCUM_TXT'
      Size = 100
      Calculated = True
    end
    object QrOcorreuBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lotesits.Banco'
    end
    object QrOcorreuAgencia: TIntegerField
      FieldName = 'Agencia'
      Origin = 'lotesits.Agencia'
    end
    object QrOcorreuCheque: TIntegerField
      FieldName = 'Cheque'
      Origin = 'lotesits.Cheque'
    end
    object QrOcorreuDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Origin = 'lotesits.Duplicata'
      Size = 12
    end
    object QrOcorreuConta: TWideStringField
      FieldName = 'Conta'
      Origin = 'lotesits.Conta'
    end
    object QrOcorreuEmissao: TDateField
      FieldName = 'Emissao'
      Origin = 'lotesits.Emissao'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuDCompra: TDateField
      FieldName = 'DCompra'
      Origin = 'lotesits.DCompra'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'lotesits.Vencto'
    end
    object QrOcorreuDDeposito: TDateField
      FieldName = 'DDeposito'
      Origin = 'lotesits.DDeposito'
    end
    object QrOcorreuEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lotesits.Emitente'
      Size = 50
    end
    object QrOcorreuCPF: TWideStringField
      FieldName = 'CPF'
      Origin = 'lotesits.CPF'
      Size = 15
    end
    object QrOcorreuCLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
      Origin = 'lotes.Cliente'
    end
    object QrOcorreuCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'ocorreu.Cliente'
      Required = True
    end
    object QrOcorreuDATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Size = 10
      Calculated = True
    end
    object QrOcorreuDescri: TWideStringField
      FieldName = 'Descri'
      Origin = 'ocorreu.Descri'
      Size = 30
    end
    object QrOcorreuNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrOcorreuSALDO: TFloatField
      FieldName = 'SALDO'
    end
  end
  object PMIncluiOcor: TPopupMenu
    Left = 33
    Top = 432
    object Incluiocorrnciadecliente1: TMenuItem
      Caption = 'Inclui ocorr'#234'ncia de &Cliente'
      OnClick = Incluiocorrnciadecliente1Click
    end
    object IncluiocorrnciadeCheque1: TMenuItem
      Caption = 'Inclui ocorr'#234'ncia de C&heque'
      Enabled = False
      Visible = False
    end
    object IncluiocorrnciadeDuplicata1: TMenuItem
      Caption = '&Inclui ocorr'#234'ncia de &Duplicata'
      Enabled = False
      Visible = False
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object IncluipagamentodeOcorrncia1: TMenuItem
      Caption = 'Inclui &Pagamento de Ocorr'#234'ncia'
      OnClick = IncluipagamentodeOcorrncia1Click
    end
  end
  object PMAlteraOcor: TPopupMenu
    Left = 129
    Top = 428
    object Alteraocorrnciaselecionada1: TMenuItem
      Caption = '&Altera ocorr'#234'ncia selecionada'
      OnClick = Alteraocorrnciaselecionada1Click
    end
  end
  object PMExcluiOcor: TPopupMenu
    OnPopup = PMExcluiOcorPopup
    Left = 205
    Top = 432
    object ExcluiOcorrnciaselecionada1: TMenuItem
      Caption = 'Exclui &Ocorr'#234'ncia selecionada'
      OnClick = ExcluiOcorrnciaselecionada1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Excluipagamentodeocorrnciaselecionado1: TMenuItem
      Caption = 'Exclui pagamento de ocorr'#234'ncia selecionado'
      OnClick = Excluipagamentodeocorrnciaselecionado1Click
    end
  end
  object QrOcorrPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ocorrpg'
      'WHERE Ocorreu =:P0')
    Left = 441
    Top = 374
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorrPgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorrPgOcorreu: TIntegerField
      FieldName = 'Ocorreu'
    end
    object QrOcorrPgData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorrPgJuros: TFloatField
      FieldName = 'Juros'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorrPgPago: TFloatField
      FieldName = 'Pago'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorrPgLotePg: TIntegerField
      FieldName = 'LotePg'
      DisplayFormat = '0000;-0000; '
    end
    object QrOcorrPgLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorrPgDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorrPgDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorrPgUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorrPgUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsOcorrPg: TDataSource
    DataSet = QrOcorrPg
    Left = 469
    Top = 374
  end
  object QrLocLote: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo'
      'FROM lotes'
      'WHERE Codigo=:P0')
    Left = 789
    Top = 365
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocLoteCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocLoteTipo: TSmallintField
      FieldName = 'Tipo'
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT loi.Banco, loi.Agencia, loi.Conta, loi.Cheque, '
      'loi.Duplicata, loi.Emitente, loi.Vencto, lot.Cliente'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'WHERE loi.Controle=:P0')
    Left = 195
    Top = 194
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrPesqAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrPesqConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrPesqCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
    end
    object QrPesqDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrPesqCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPesqEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrPesqVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
  end
  object QrBco1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome FROM bancos'
      'WHERE Codigo=:P0')
    Left = 165
    Top = 193
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBco1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object frxOcorreuC: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40420.448944745400000000
    ReportOptions.LastChange = 40420.695853043980000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 49
    Top = 9
    Datasets = <
      item
        DataSet = frxDsClientes
        DataSetName = 'frxDsClientes'
      end
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsPesq1
        DataSetName = 'frxDsPesq1'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 64.252010000000000000
        Top = 18.897650000000000000
        Width = 1028.032160000000000000
        object Shape1: TfrxShapeView
          Width = 1028.032160000000000000
          Height = 37.795300000000000000
          ShowHint = False
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 7.559060000000000000
          Width = 1012.914040000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 1028.032160000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 725.669760000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'OCORR'#202'NCIAS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 876.850960000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Top = 45.354360000000000000
          Width = 1020.473100000000000000
          Height = 18.897650000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsClientes."NOMECLIENTE"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        Height = 17.000000000000000000
        Top = 143.622140000000000000
        Width = 1028.032160000000000000
        object Memo1: TfrxMemoView
          Width = 26.456692910000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TD')
          ParentFont = False
          WordWrap = False
        end
        object Memo4: TfrxMemoView
          Left = 26.456607480000000000
          Width = 113.385826770000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Documen. / Descri.')
          ParentFont = False
          WordWrap = False
        end
        object Memo22: TfrxMemoView
          Left = 321.259700940000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
          WordWrap = False
        end
        object Memo9: TfrxMemoView
          Left = 370.393488430000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
          WordWrap = False
        end
        object Memo40: TfrxMemoView
          Left = 491.338409370000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'SALDO')
          ParentFont = False
          WordWrap = False
        end
        object Memo48: TfrxMemoView
          Left = 551.810945510000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ult.Pg')
          ParentFont = False
          WordWrap = False
        end
        object Memo5: TfrxMemoView
          Left = 139.842610000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Compra')
          ParentFont = False
          WordWrap = False
        end
        object Memo6: TfrxMemoView
          Left = 188.976500000000000000
          Width = 132.283464570000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Ocorr'#234'ncia')
          ParentFont = False
          WordWrap = False
        end
        object Memo10: TfrxMemoView
          Left = 430.866420000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
          WordWrap = False
        end
        object Memo20: TfrxMemoView
          Left = 876.850676850000000000
          Width = 102.047244090000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
          WordWrap = False
        end
        object Memo11: TfrxMemoView
          Left = 672.755905510000000000
          Width = 204.094534570000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emitente')
          ParentFont = False
          WordWrap = False
        end
        object Memo8: TfrxMemoView
          Left = 978.898270000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Emitido')
          ParentFont = False
          WordWrap = False
        end
        object Memo57: TfrxMemoView
          Left = 612.283860000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Atualiz.')
          ParentFont = False
          WordWrap = False
        end
      end
      object Footer1: TfrxFooter
        Height = 28.338582680000000000
        Top = 226.771800000000000000
        Width = 1028.032160000000000000
        object Memo31: TfrxMemoView
          Left = 370.393940000000000000
          Top = 11.338582680000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."Valor">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo33: TfrxMemoView
          Left = 430.866420000000000000
          Top = 11.338582680000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."Pago">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo34: TfrxMemoView
          Left = 491.338900000000000000
          Top = 11.338582680000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."SALDO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Memo35: TfrxMemoView
          Left = 612.283860000000000000
          Top = 11.338582680000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq1."ATUALIZADO">)]')
          ParentFont = False
          WordWrap = False
        end
        object Line2: TfrxLineView
          Top = 6.000000000000000000
          Width = 1029.921460000000000000
          ShowHint = False
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo36: TfrxMemoView
          Top = 11.338582680000000000
          Width = 370.393900940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
          WordWrap = False
        end
        object Memo38: TfrxMemoView
          Left = 672.755866460000000000
          Top = 11.338582680000000000
          Width = 355.275780940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
          WordWrap = False
        end
        object Memo7: TfrxMemoView
          Left = 551.811380000000000000
          Top = 11.338590000000010000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '')
          ParentFont = False
          WordWrap = False
        end
      end
      object MasterData1: TfrxMasterData
        Height = 17.000000000000000000
        Top = 185.196970000000000000
        Width = 1028.032160000000000000
        DataSet = frxDsPesq1
        DataSetName = 'frxDsPesq1'
        RowCount = 0
        object Memo2: TfrxMemoView
          Width = 26.456692910000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq1."TIPODOC"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456607480000000000
          Width = 113.385826770000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq1."DOCUM_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo12: TfrxMemoView
          Left = 321.259700940000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq1."DataO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo18: TfrxMemoView
          Left = 370.393488430000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."Valor"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo19: TfrxMemoView
          Left = 491.338409370000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."SALDO"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo21: TfrxMemoView
          Left = 551.810945510000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsPesq1
          DataSetName = 'frxDsPesq1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq1."Data3_TXT"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo24: TfrxMemoView
          Left = 139.842610000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'DCompra'
          DataSet = frxDsPesq1
          DataSetName = 'frxDsPesq1'
          DisplayFormat.FormatStr = 'mm/dd/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq1."DCompra"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo25: TfrxMemoView
          Left = 188.976500000000000000
          Width = 132.283464570000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq1."NOMEOCORRENCIA"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo28: TfrxMemoView
          Left = 430.866420000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."Pago"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo29: TfrxMemoView
          Left = 876.850676850000000000
          Width = 102.047244090000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq1."CPF"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo30: TfrxMemoView
          Left = 672.755905510000000000
          Width = 204.094534570000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq1."Emitente"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo27: TfrxMemoView
          Left = 978.898270000000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Emissao'
          DataSet = frxDsPesq1
          DataSetName = 'frxDsPesq1'
          DisplayFormat.FormatStr = 'mm/dd/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq1."Emissao"]')
          ParentFont = False
          WordWrap = False
        end
        object Memo23: TfrxMemoView
          Left = 612.283860000000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = FmDuplicatas2.frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq1."ATUALIZADO"]')
          ParentFont = False
          WordWrap = False
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 22.677180000000000000
        Top = 317.480520000000000000
        Width = 1028.032160000000000000
        OnAfterPrint = 'PageFooter1OnAfterPrint'
        object Memo26: TfrxMemoView
          Width = 1028.032160000000000000
          Height = 15.118110240000000000
          ShowHint = False
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
    end
  end
  object frxDsClientes: TfrxDBDataset
    UserName = 'frxDsClientes'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMECLIENTE=NOMECLIENTE'
      'Codigo=Codigo'
      'TAXA_COMPRA=TAXA_COMPRA'
      'FatorCompra=FatorCompra')
    DataSource = DsClientes
    BCDToCurrency = False
    Left = 217
    Top = 33
  end
  object PMImprime: TPopupMenu
    Left = 16
    Top = 8
    object Selecionados1: TMenuItem
      Caption = '&Selecionados'
      OnClick = Selecionados1Click
    end
    object odos1: TMenuItem
      Caption = '&Todos'
      OnClick = odos1Click
    end
  end
  object QrPesq1: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPesq1CalcFields
    SQL.Strings = (
      'SELECT IF(oc.Cliente > 0,'#39'CL'#39',IF(lo.Tipo=0,"CH","DU"))TIPODOC, '
      'ob.Nome NOMEOCORRENCIA, li.Banco, li.Agencia, li.Cheque, '
      
        'li.Duplicata, li.Conta, lo.Tipo, li.Emissao, li.DCompra, li.Venc' +
        'to, '
      
        'IF(oc.Data3 <= "1899-12-30", "", DATE_FORMAT(oc.Data3,"%d/%m/%y"' +
        ')) Data3_TXT,'
      'li.DDeposito, li.Emitente, li.CPF, lo.Cliente CLIENTELOTE, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE (lo.Cliente=:P0 OR oc.Cliente=:P1)')
    Left = 269
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPesq1TIPODOC: TWideStringField
      FieldName = 'TIPODOC'
      Required = True
      Size = 2
    end
    object QrPesq1NOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrPesq1Banco: TIntegerField
      FieldName = 'Banco'
    end
    object QrPesq1Agencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrPesq1Cheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrPesq1Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrPesq1Conta: TWideStringField
      FieldName = 'Conta'
    end
    object QrPesq1Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPesq1SALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesq1Emissao: TDateField
      FieldName = 'Emissao'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq1DCompra: TDateField
      FieldName = 'DCompra'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq1Vencto: TDateField
      FieldName = 'Vencto'
    end
    object QrPesq1DDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrPesq1Emitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrPesq1CPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrPesq1CLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
    object QrPesq1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesq1LotesIts: TIntegerField
      FieldName = 'LotesIts'
    end
    object QrPesq1Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPesq1DataO: TDateField
      FieldName = 'DataO'
    end
    object QrPesq1Ocorrencia: TIntegerField
      FieldName = 'Ocorrencia'
    end
    object QrPesq1Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrPesq1LoteQuit: TIntegerField
      FieldName = 'LoteQuit'
    end
    object QrPesq1TaxaB: TFloatField
      FieldName = 'TaxaB'
    end
    object QrPesq1TaxaP: TFloatField
      FieldName = 'TaxaP'
    end
    object QrPesq1TaxaV: TFloatField
      FieldName = 'TaxaV'
    end
    object QrPesq1Pago: TFloatField
      FieldName = 'Pago'
    end
    object QrPesq1DataP: TDateField
      FieldName = 'DataP'
    end
    object QrPesq1Data3: TDateField
      FieldName = 'Data3'
    end
    object QrPesq1Status: TSmallintField
      FieldName = 'Status'
    end
    object QrPesq1Descri: TWideStringField
      FieldName = 'Descri'
      Size = 30
    end
    object QrPesq1MoviBank: TIntegerField
      FieldName = 'MoviBank'
    end
    object QrPesq1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPesq1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPesq1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPesq1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPesq1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPesq1AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrPesq1ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesq1DOCUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOCUM_TXT'
      Size = 100
      Calculated = True
    end
    object QrPesq1Ativo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrPesq1Data3_TXT: TWideStringField
      FieldName = 'Data3_TXT'
      Size = 10
    end
  end
  object frxDsPesq1: TfrxDBDataset
    UserName = 'frxDsPesq1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'TIPODOC=TIPODOC'
      'NOMEOCORRENCIA=NOMEOCORRENCIA'
      'Banco=Banco'
      'Agencia=Agencia'
      'Cheque=Cheque'
      'Duplicata=Duplicata'
      'Conta=Conta'
      'Tipo=Tipo'
      'SALDO=SALDO'
      'Emissao=Emissao'
      'DCompra=DCompra'
      'Vencto=Vencto'
      'DDeposito=DDeposito'
      'Emitente=Emitente'
      'CPF=CPF'
      'CLIENTELOTE=CLIENTELOTE'
      'Codigo=Codigo'
      'LotesIts=LotesIts'
      'Cliente=Cliente'
      'DataO=DataO'
      'Ocorrencia=Ocorrencia'
      'Valor=Valor'
      'LoteQuit=LoteQuit'
      'TaxaB=TaxaB'
      'TaxaP=TaxaP'
      'TaxaV=TaxaV'
      'Pago=Pago'
      'DataP=DataP'
      'Data3=Data3'
      'Status=Status'
      'Descri=Descri'
      'MoviBank=MoviBank'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'ATUALIZADO=ATUALIZADO'
      'DOCUM_TXT=DOCUM_TXT'
      'Ativo=Ativo'
      'Data3_TXT=Data3_TXT')
    DataSet = QrPesq1
    BCDToCurrency = False
    Left = 297
    Top = 9
  end
end
