unit RetornoBB;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, ComCtrls, Grids,
  Mask, DBCtrls, DBGrids, Math, dmkEdit, UnDmkProcFunc, dmkGeral, UnDmkEnums;

type
  TFmRetornoBB = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel3: TPanel;
    Label31: TLabel;
    BtAbre: TSpeedButton;
    OpenDialog1: TOpenDialog;
    EdPathBBRet: TdmkEdit;
    QrBanco: TmySQLQuery;
    QrBancoCodigo: TIntegerField;
    QrBancoNome: TWideStringField;
    QrBancoSite: TWideStringField;
    QrBancoLk: TIntegerField;
    QrBancoDataCad: TDateField;
    QrBancoDataAlt: TDateField;
    QrBancoUserCad: TIntegerField;
    QrBancoUserAlt: TIntegerField;
    QrBancoDVCC: TSmallintField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    QrLotesIts: TmySQLQuery;
    DsLotesIts: TDataSource;
    QrLotesItsNOMECLI: TWideStringField;
    QrLotesItsCodigo: TIntegerField;
    QrLotesItsDuplicata: TWideStringField;
    QrLotesItsCPF: TWideStringField;
    QrLotesItsEmitente: TWideStringField;
    QrLotesItsBruto: TFloatField;
    QrLotesItsDesco: TFloatField;
    QrLotesItsValor: TFloatField;
    QrLotesItsEmissao: TDateField;
    QrLotesItsDCompra: TDateField;
    QrLotesItsVencto: TDateField;
    QrLotesItsDDeposito: TDateField;
    QrLotesItsDias: TIntegerField;
    QrLotesItsData3: TDateField;
    QrLotesItsCobranca: TIntegerField;
    QrLotesItsLote: TIntegerField;
    QrLotesItsTIPOLOTE: TSmallintField;
    QrLotesItsNOMETIPOLOTE: TWideStringField;
    QrLotesItsDATA3_TXT: TWideStringField;
    QrLotesItsCPF_TXT: TWideStringField;
    QrLotesItsNOMESTATUS: TWideStringField;
    QrLotesItsQuitado: TIntegerField;
    QrLotesItsRepassado: TSmallintField;
    TabSheet8: TTabSheet;
    QrCNAB240: TmySQLQuery;
    DsCNAB240: TDataSource;
    QrCNAB240CNAB240L: TIntegerField;
    QrCNAB240CNAB240I: TIntegerField;
    QrCNAB240Controle: TIntegerField;
    QrCNAB240Banco: TIntegerField;
    QrCNAB240ConfigBB: TIntegerField;
    QrCNAB240Lote: TIntegerField;
    QrCNAB240Item: TIntegerField;
    QrCNAB240DataOcor: TDateField;
    QrCNAB240Envio: TSmallintField;
    QrCNAB240Movimento: TSmallintField;
    QrCNAB240Lk: TIntegerField;
    QrCNAB240DataCad: TDateField;
    QrCNAB240DataAlt: TDateField;
    QrCNAB240UserCad: TIntegerField;
    QrCNAB240UserAlt: TIntegerField;
    QrDupl: TmySQLQuery;
    QrCNAB240NOMEENVIO: TWideStringField;
    QrCNAB240NOMEBANCO: TWideStringField;
    QrCNAB240NOMEMOVIMENTO: TWideStringField;
    QrCNAB240NOMECLI: TWideStringField;
    QrCNAB240MeuLote: TIntegerField;
    QrCNAB240Duplicata: TWideStringField;
    QrCNAB240CPF: TWideStringField;
    QrCNAB240Emitente: TWideStringField;
    QrCNAB240Bruto: TFloatField;
    QrCNAB240Desco: TFloatField;
    QrCNAB240Quitado: TIntegerField;
    QrCNAB240Valor: TFloatField;
    QrCNAB240Emissao: TDateField;
    QrCNAB240DCompra: TDateField;
    QrCNAB240Vencto: TDateField;
    QrCNAB240DDeposito: TDateField;
    QrCNAB240Dias: TIntegerField;
    QrCNAB240Data3: TDateField;
    QrCNAB240Cobranca: TIntegerField;
    QrCNAB240Repassado: TSmallintField;
    QrCNAB240Bordero: TIntegerField;
    QrCNAB240TIPOLOTE: TSmallintField;
    QrCNAB240CPF_TXT: TWideStringField;
    QrCNAB240DATA3_TXT: TWideStringField;
    QrCNAB240NOMESTATUS: TWideStringField;
    QrCNAB240NOMETIPOLOTE: TWideStringField;
    QrCNAB240Custas: TFloatField;
    QrCNAB240IRTCLB_TXT: TWideStringField;
    QrCNAB240ValPago: TFloatField;
    QrCNAB240ValCred: TFloatField;
    QrSumPg: TmySQLQuery;
    QrSumPgJuros: TFloatField;
    QrSumPgPago: TFloatField;
    QrSumPgDesco: TFloatField;
    QrSumPgValor: TFloatField;
    QrSumPgMaxData: TDateField;
    QrCNAB240Cliente: TIntegerField;
    QrCNAB240Acao: TIntegerField;
    QrCNAB240NOMEACAO: TWideStringField;
    TabSheet9: TTabSheet;
    GradeA: TStringGrid;
    QrNext: TmySQLQuery;
    QrNextCNAB240L: TIntegerField;
    Memo2: TMemo;
    Panel7: TPanel;
    Label22: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label30: TLabel;
    Label40: TLabel;
    Label34: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label35: TLabel;
    Label33: TLabel;
    Label32: TLabel;
    Label41: TLabel;
    Label23: TLabel;
    Label38: TLabel;
    Label39: TLabel;
    EdBcoCodL: TdmkEdit;
    EdBcoNomL: TdmkEdit;
    EdEmpresaCNPJL: TdmkEdit;
    EdMensagem1: TdmkEdit;
    EdMensagem2: TdmkEdit;
    EdRemRet: TdmkEdit;
    EdConvenioL: TdmkEdit;
    EdEmpresaNomeL: TdmkEdit;
    EdAgenciaNUL: TdmkEdit;
    EdAgenciaDVL: TdmkEdit;
    EdContaNUL: TdmkEdit;
    EdContaDVL: TdmkEdit;
    EdAgenCcDVL: TdmkEdit;
    EdCNABLot2: TEdit;
    EdHoraGeraL: TdmkEdit;
    EdDataGeraL: TdmkEdit;
    EdLayoutLot: TdmkEdit;
    EdCNABLot1: TdmkEdit;
    EdOperacao: TdmkEdit;
    EdOperacaoTxt: TdmkEdit;
    EdServico: TdmkEdit;
    EdServicoTxt: TdmkEdit;
    EdFormLanc: TdmkEdit;
    EdFormLancTxt: TdmkEdit;
    PainelControle: TPanel;
    LaRegistroB: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel2: TPanel;
    Label45: TLabel;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    TabSheet7: TTabSheet;
    Panel11: TPanel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    Label90: TLabel;
    Label91: TLabel;
    Label92: TLabel;
    Label93: TLabel;
    Label94: TLabel;
    Label95: TLabel;
    Label96: TLabel;
    Label97: TLabel;
    Label98: TLabel;
    Label99: TLabel;
    Label100: TLabel;
    Label101: TLabel;
    Label102: TLabel;
    Label103: TLabel;
    Label104: TLabel;
    Label105: TLabel;
    EdLoiControle: TdmkEdit;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    Panel4: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label116: TLabel;
    Label117: TLabel;
    Label118: TLabel;
    EdBcoCodH: TdmkEdit;
    EdBcoNomH: TdmkEdit;
    EdTipoArq: TdmkEdit;
    EdEmpresaCNPJH: TdmkEdit;
    EdEmpresaNomeH: TdmkEdit;
    EdConvenioH: TdmkEdit;
    EdAgenciaNUH: TdmkEdit;
    EdAgenciaDVH: TdmkEdit;
    EdContaNUH: TdmkEdit;
    EdContaDVH: TdmkEdit;
    EdAgenCcDVH: TdmkEdit;
    EdTipoRem: TdmkEdit;
    EdHoraGeraH: TdmkEdit;
    EdDataGeraH: TdmkEdit;
    EdSeqArq: TdmkEdit;
    EdLayoutArq: TdmkEdit;
    EdDensiGrava: TdmkEdit;
    EdReservaBanco: TdmkEdit;
    EdReservaEmpresa: TdmkEdit;
    EdCSP: TdmkEdit;
    EdVans: TdmkEdit;
    EdTipoServico: TdmkEdit;
    EdCodigoOcorr: TdmkEdit;
    EdNomeServico: TdmkEdit;
    EdArqQtdLotes: TdmkEdit;
    EdArqQtdRegistros: TdmkEdit;
    EdArqQtdContas: TdmkEdit;
    Memo1: TMemo;
    Panel12: TPanel;
    Label119: TLabel;
    Label120: TLabel;
    Label121: TLabel;
    Label122: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    Panel14: TPanel;
    Panel15: TPanel;
    BitBtn1: TBitBtn;
    Panel16: TPanel;
    BitBtn4: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn5: TBitBtn;
    BitBtn6: TBitBtn;
    BitBtn7: TBitBtn;
    BitBtn8: TBitBtn;
    Panel13: TPanel;
    Label123: TLabel;
    Label124: TLabel;
    Label125: TLabel;
    Label126: TLabel;
    Label127: TLabel;
    Label128: TLabel;
    Label129: TLabel;
    Label130: TLabel;
    Label131: TLabel;
    Label132: TLabel;
    Label133: TLabel;
    Label134: TLabel;
    Label135: TLabel;
    Label136: TLabel;
    Label137: TLabel;
    Label138: TLabel;
    Label139: TLabel;
    Label140: TLabel;
    Label141: TLabel;
    Label143: TLabel;
    Label144: TLabel;
    Label145: TLabel;
    Label147: TLabel;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    DBEdit37: TDBEdit;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    DBEdit47: TDBEdit;
    DBEdit48: TDBEdit;
    DBEdit49: TDBEdit;
    DBGrid1: TDBGrid;
    GradeB: TStringGrid;
    LaRegistroA: TLabel;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    BitBtn9: TBitBtn;
    BitBtn10: TBitBtn;
    BitBtn11: TBitBtn;
    BitBtn12: TBitBtn;
    BtGrava: TBitBtn;
    QrDup1: TmySQLQuery;
    QrDup1CNAB240L: TIntegerField;
    StatusBar: TStatusBar;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet10: TTabSheet;
    TabSheet11: TTabSheet;
    Label106: TLabel;
    EdBcoCodTra5: TdmkEdit;
    EdBcoNomTra5: TdmkEdit;
    EdLotTra5: TdmkEdit;
    Label107: TLabel;
    Label108: TLabel;
    EdCNABLot7: TdmkEdit;
    EdRegistTra5: TdmkEdit;
    EdNuAvLaTra5: TdmkEdit;
    Label115: TLabel;
    Label109: TLabel;
    Label114: TLabel;
    EdCNABLot8: TdmkEdit;
    Label113: TLabel;
    Label112: TLabel;
    Label111: TLabel;
    Label110: TLabel;
    EdQtTit1Tra5: TdmkEdit;
    EdVaTit1Tra5: TdmkEdit;
    EdVaTit2Tra5: TdmkEdit;
    EdQtTit2Tra5: TdmkEdit;
    EdQtTit3Tra5: TdmkEdit;
    EdVaTit3Tra5: TdmkEdit;
    EdVaTit4Tra5: TdmkEdit;
    EdQtTit4Tra5: TdmkEdit;
    Label64: TLabel;
    EdBcoCodDetU: TdmkEdit;
    EdBcoNomDetU: TdmkEdit;
    Label69: TLabel;
    EdValJMEDetU: TdmkEdit;
    Label77: TLabel;
    EdDatOcoDetU: TdmkEdit;
    EdCodOcoDetU: TdmkEdit;
    Label79: TLabel;
    EdTexOcoDetU: TdmkEdit;
    EdValDesDetU: TdmkEdit;
    Label70: TLabel;
    EdLotDetU: TdmkEdit;
    Label65: TLabel;
    Label66: TLabel;
    EdSeqDetU: TdmkEdit;
    EdCNABLot5: TdmkEdit;
    Label67: TLabel;
    Label68: TLabel;
    EdCodMovDetU: TdmkEdit;
    EdTexMovDetU: TdmkEdit;
    Label71: TLabel;
    EdValAbaDetU: TdmkEdit;
    EdValIOFDetU: TdmkEdit;
    Label72: TLabel;
    EdValPagDetU: TdmkEdit;
    Label73: TLabel;
    Label74: TLabel;
    EdValLiqDetU: TdmkEdit;
    EdValOuDDetU: TdmkEdit;
    Label75: TLabel;
    Label82: TLabel;
    EdComOcSDetU: TdmkEdit;
    EdValOuCDetU: TdmkEdit;
    Label76: TLabel;
    Label78: TLabel;
    EdDatCreDetU: TdmkEdit;
    EdDatOcSDetU: TdmkEdit;
    Label80: TLabel;
    Label81: TLabel;
    EdValOcSDetU: TdmkEdit;
    EdNNBCoCDetU: TdmkEdit;
    Label84: TLabel;
    EdCBcCoCDetU: TdmkEdit;
    EdCNABLot6: TdmkEdit;
    EdNBcCoCDetU: TdmkEdit;
    Label85: TLabel;
    Label83: TLabel;
    Panel8: TPanel;
    Label36: TLabel;
    EdBcoCodDetT: TdmkEdit;
    EdBcoNomDetT: TdmkEdit;
    EdLotDetT: TdmkEdit;
    Label37: TLabel;
    Label42: TLabel;
    EdSeqDetT: TdmkEdit;
    EdCNABLot3: TdmkEdit;
    Label43: TLabel;
    EdCodMovDetT: TdmkEdit;
    Label44: TLabel;
    EdTexMovDetT: TdmkEdit;
    EdAgenciaNUT: TdmkEdit;
    Label46: TLabel;
    EdAgenciaDVT: TdmkEdit;
    EdContaNUT: TdmkEdit;
    Label47: TLabel;
    EdContaDVT: TdmkEdit;
    EdAgenCcDVT: TdmkEdit;
    Label48: TLabel;
    Label49: TLabel;
    EdIdeTitDetT: TdmkEdit;
    Label58: TLabel;
    EdNumInsDetT: TdmkEdit;
    EdTipInsDetT: TdmkEdit;
    EdTxtMoeDetT: TdmkEdit;
    EdCodMoeDetT: TdmkEdit;
    Label57: TLabel;
    EdAgenciaDVT2: TdmkEdit;
    EdAgenciaNUT2: TdmkEdit;
    Label55: TLabel;
    EdBcoNomDetT2: TdmkEdit;
    EdIRTCLB_TXT: TdmkEdit;
    Label142: TLabel;
    Label63: TLabel;
    EdCNABLot4: TdmkEdit;
    EdIDTiEDetT: TdmkEdit;
    Label56: TLabel;
    Label86: TLabel;
    Label54: TLabel;
    EdBcoCodDetT2: TdmkEdit;
    EdValNomDetT: TdmkEdit;
    Label53: TLabel;
    Label62: TLabel;
    EdIdRTCLDetT: TdmkEdit;
    EdTarCusDetT: TdmkEdit;
    Label61: TLabel;
    EdVenTitDetT: TdmkEdit;
    Label52: TLabel;
    Label60: TLabel;
    EdNConOCDetT: TdmkEdit;
    EdNomeDetT: TdmkEdit;
    EdDocCobDetT: TdmkEdit;
    Label51: TLabel;
    EdTexCarDetT: TdmkEdit;
    EdCodCarDetT: TdmkEdit;
    Label50: TLabel;
    Label59: TLabel;
    Panel9: TPanel;
    Label146: TLabel;
    Label148: TLabel;
    Label149: TLabel;
    Label150: TLabel;
    Label151: TLabel;
    Label152: TLabel;
    Label153: TLabel;
    Label154: TLabel;
    Label155: TLabel;
    Label158: TLabel;
    Label161: TLabel;
    Label164: TLabel;
    Label167: TLabel;
    Label169: TLabel;
    Label170: TLabel;
    EdBcoCodDetP: TdmkEdit;
    EdBcoNomDetP: TdmkEdit;
    EdLotDetP: TdmkEdit;
    EdSeqDetP: TdmkEdit;
    EdCNABLot009: TdmkEdit;
    EdCodMovDetP: TdmkEdit;
    EdTexMovDetP: TdmkEdit;
    EdAgenciaNUP: TdmkEdit;
    EdAgenciaDVP: TdmkEdit;
    EdContaNUP: TdmkEdit;
    EdContaDVP: TdmkEdit;
    EdAgenCcDVP: TdmkEdit;
    EdIdeTitDetP: TdmkEdit;
    EdAgenciaDVP2: TdmkEdit;
    EdAgenciaNUP2: TdmkEdit;
    EdIDTiEDetP: TdmkEdit;
    EdValNomDetP: TdmkEdit;
    EdVenTitDetP: TdmkEdit;
    EdDocCobDetP: TdmkEdit;
    EdTexCarDetP: TdmkEdit;
    EdCodCarDetP: TdmkEdit;
    TabSheet12: TTabSheet;
    Label172: TLabel;
    EdForCadTitP: TdmkEdit;
    EdNomCadTitP: TdmkEdit;
    Label173: TLabel;
    EdTipoDocumP: TdmkEdit;
    EdNomeDocumP: TdmkEdit;
    Label174: TLabel;
    EdIDEmitBloP: TdmkEdit;
    EdNoEmitBloP: TdmkEdit;
    Label175: TLabel;
    EdIDDistribP: TdmkEdit;
    EdNoDistribP: TdmkEdit;
    Label176: TLabel;
    EdEspeciTitP: TdmkEdit;
    EdNomEspTitP: TdmkEdit;
    Label177: TLabel;
    EdIDTitAceiP: TdmkEdit;
    EdNoTitAceiP: TdmkEdit;
    Label178: TLabel;
    EdDatEmiTitP: TdmkEdit;
    Label179: TLabel;
    EdCodJurMorP: TdmkEdit;
    EdNomJurMorP: TdmkEdit;
    EdDatJurMorP: TdmkEdit;
    Label180: TLabel;
    Label181: TLabel;
    EdJurMorDiaP: TdmkEdit;
    Label182: TLabel;
    EdCodDesco1P: TdmkEdit;
    EdNomDesco1P: TdmkEdit;
    EdDatDesco1P: TdmkEdit;
    Label183: TLabel;
    Label184: TLabel;
    EdValPerDe1P: TdmkEdit;
    Label185: TLabel;
    EdValIOFDe1P: TdmkEdit;
    Label186: TLabel;
    EdValDesco1P: TdmkEdit;
    Label187: TLabel;
    EdCodParProP: TdmkEdit;
    EdNomParProP: TdmkEdit;
    EdNddParProP: TdmkEdit;
    Label188: TLabel;
    Label189: TLabel;
    EdNomParBaiP: TdmkEdit;
    EdCodParBaiP: TdmkEdit;
    EdNddParBaiP: TdmkEdit;
    Label190: TLabel;
    Label156: TLabel;
    EdBcoCodDetQ: TdmkEdit;
    EdBcoNomDetQ: TdmkEdit;
    EdLotDetQ: TdmkEdit;
    Label157: TLabel;
    Label159: TLabel;
    EdSeqDetQ: TdmkEdit;
    EdCNABLot010: TdmkEdit;
    Label160: TLabel;
    Label162: TLabel;
    EdCodMovDetQ: TdmkEdit;
    EdTexMovDetQ: TdmkEdit;
    Label163: TLabel;
    EdCPFCNPDetQ: TdmkEdit;
    EdEmiCliDetQ: TdmkEdit;
    Label165: TLabel;
    EdEmiEndDetQ: TdmkEdit;
    Label166: TLabel;
    Label168: TLabel;
    EdEmiBaiDetQ: TdmkEdit;
    Label171: TLabel;
    EdEmiCEPDetQ: TdmkEdit;
    EdEmiSufDetQ: TdmkEdit;
    Label191: TLabel;
    EdEmiCidDetQ: TdmkEdit;
    Label192: TLabel;
    EdEmi_UFDetQ: TdmkEdit;
    Label193: TLabel;
    EdEmiSacDetQ: TdmkEdit;
    Label194: TLabel;
    EdEmiCPFDetQ: TdmkEdit;
    Label195: TLabel;
    EdEmiBCCDetQ: TdmkEdit;
    EdEmiNBCDetQ: TdmkEdit;
    EdEmiNNBDetQ: TdmkEdit;
    EdCNABLot011: TdmkEdit;
    Label196: TLabel;
    Label198: TLabel;
    Label197: TLabel;
    EdBcoCodDetR: TdmkEdit;
    EdBcoNomDetR: TdmkEdit;
    EdLotDetR: TdmkEdit;
    Label199: TLabel;
    Label200: TLabel;
    EdSeqDetR: TdmkEdit;
    EdCNABLot012: TdmkEdit;
    Label201: TLabel;
    EdCodMovDetR: TdmkEdit;
    Label202: TLabel;
    EdTexMovDetR: TdmkEdit;
    Label203: TLabel;
    EdCodDesco2R: TdmkEdit;
    EdNomDesco2R: TdmkEdit;
    Label204: TLabel;
    EdDatDesco2R: TdmkEdit;
    EdValPerDe2R: TdmkEdit;
    Label205: TLabel;
    Label206: TLabel;
    EdCodDesco3R: TdmkEdit;
    EdNomDesco3R: TdmkEdit;
    Label207: TLabel;
    EdDatDesco3R: TdmkEdit;
    EdValPerDe3R: TdmkEdit;
    Label208: TLabel;
    Label209: TLabel;
    EdCodiMultaR: TdmkEdit;
    EdNomeMultaR: TdmkEdit;
    EdDataMultaR: TdmkEdit;
    Label210: TLabel;
    Label211: TLabel;
    EdVaPeMultaR: TdmkEdit;
    Label212: TLabel;
    EdInfBcoSacR: TEdit;
    EdMensagem3R: TEdit;
    Label213: TLabel;
    Label214: TLabel;
    EdMensagem4R: TEdit;
    Label215: TLabel;
    EdConDebBcoR: TdmkEdit;
    EdConDebBNoR: TdmkEdit;
    EdConDebAgeR: TdmkEdit;
    Label216: TLabel;
    EdConDebCCoR: TdmkEdit;
    Label217: TLabel;
    Label218: TLabel;
    EdCodOcoSacR: TdmkEdit;
    EdCNABLot013: TdmkEdit;
    Label219: TLabel;
    Memo3: TMemo;
    SpeedButton5: TSpeedButton;
    QrCNAB240Sequencia: TIntegerField;
    QrCNAB240Convenio: TWideStringField;
    QrCNAB240IRTCLB: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdBcoCodHChange(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure EdBcoCodLChange(Sender: TObject);
    procedure EdOperacaoChange(Sender: TObject);
    procedure EdServicoChange(Sender: TObject);
    procedure EdFormLancChange(Sender: TObject);
    procedure EdBcoCodDetTChange(Sender: TObject);
    procedure EdBcoCodDetT2Change(Sender: TObject);
    procedure EdBcoCodDetUChange(Sender: TObject);
    procedure EdCodMovDetTChange(Sender: TObject);
    procedure EdCodMovDetUChange(Sender: TObject);
    procedure EdCodOcoDetUChange(Sender: TObject);
    procedure EdCBcCoCDetUChange(Sender: TObject);
    procedure EdCodMoeDetTChange(Sender: TObject);
    procedure EdTipInsDetTChange(Sender: TObject);
    procedure EdCodCarDetTChange(Sender: TObject);
    procedure EdIDTiEDetTChange(Sender: TObject);
    procedure QrLotesItsCalcFields(DataSet: TDataSet);
    procedure EdBcoCodTra5Change(Sender: TObject);
    procedure QrCNAB240CalcFields(DataSet: TDataSet);
    procedure EdIdRTCLDetTChange(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure BitBtn6Click(Sender: TObject);
    procedure BitBtn7Click(Sender: TObject);
    procedure BitBtn8Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn9Click(Sender: TObject);
    procedure BitBtn10Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure BitBtn12Click(Sender: TObject);
    procedure GradeBClick(Sender: TObject);
    procedure BtGravaClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
    procedure Memo1KeyUp(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Memo1Click(Sender: TObject);
    procedure Memo1Enter(Sender: TObject);
    procedure Memo1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdBcoCodDetPChange(Sender: TObject);
    procedure EdCodMovDetPChange(Sender: TObject);
    procedure EdCodCarDetPChange(Sender: TObject);
    procedure EdForCadTitPChange(Sender: TObject);
    procedure EdTipoDocumPChange(Sender: TObject);
    procedure EdIDEmitBloPChange(Sender: TObject);
    procedure EdIDDistribPChange(Sender: TObject);
    procedure EdEspeciTitPChange(Sender: TObject);
    procedure EdIDTitAceiPChange(Sender: TObject);
    procedure EdCodJurMorPChange(Sender: TObject);
    procedure EdCodDesco1PChange(Sender: TObject);
    procedure EdCodParProPChange(Sender: TObject);
    procedure EdCodParBaiPChange(Sender: TObject);
    procedure EdIDTiEDetPChange(Sender: TObject);
    procedure EdBcoCodDetQChange(Sender: TObject);
    procedure EdCodMovDetQChange(Sender: TObject);
    procedure EdEmiBCCDetQChange(Sender: TObject);
    procedure EdBcoCodDetRChange(Sender: TObject);
    procedure EdCodMovDetRChange(Sender: TObject);
    procedure EdCodDesco2RChange(Sender: TObject);
    procedure EdCodDesco3RChange(Sender: TObject);
    procedure EdCodiMultaRChange(Sender: TObject);
    procedure EdConDebBcoRChange(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure EdTipoServicoChange(Sender: TObject);
  private
    { Private declarations }
    FShiftPressed: Boolean;
    FPosIniCol, FPosIniRow, FPosIniTam: Integer;
    //
    procedure VaiLote(Item: Integer);
    procedure VaiRegistro(Item: Integer; InsereSQL: Boolean);
    procedure LeHeaderArquivo;
    procedure LeTrailerArquivo;
    procedure LeLote(Linha: Integer);
    procedure LeRegistro(Linha, LinhaF, Registro: Integer; InsereNoLote: Integer);

    function TipoDeInscricao(Codigo: String; Tipo: Integer): String;
    function TipoDeMoeda(Codigo: String; Tipo: Integer): String;
    function TipoDeCarteira(Codigo: String): String;
    function TipoDeOperacao(Codigo: String): String;
    function TipoDeServico(Codigo, Tipo: String): String;
    function TipoDeOcorrenciaDoSacado(Codigo: String): String;
    function TipoDeEmissaoDoBloqueto(Codigo: String): String;
    function TipoDeEspecieDoTitulo(Codigo: String): String;
    function TipoDeMora(Codigo: String): String;
    function TipoDeDesconto(Codigo: String): String;
    function FormaDeLancamento(Codigo, Tipo: Integer): String;
    function InformacaoComplementar(Codigo: String; Tipo: Integer;
             EMail: Boolean): String;
    procedure Liquidacao;
    procedure LimpaTextos;
    procedure CalculaPagamento(LotesIts: Integer);
    procedure ImprimeRecibodeQuitacao;
    procedure VaiLoteRetorno(Para: TVaiPara);
    procedure UpdateCursorPos;
    procedure ReabreLotesIts(Sender: TdmkEdit);
    procedure DescreveCaracteres(Arquivo: String);
  public
    { Public declarations }
  end;

  var
  FmRetornoBB: TFmRetornoBB;

implementation

uses Module, UMySQLModule, UnGOTOy, Duplicatas2, UnMyObjects, UnBancos;

{$R *.DFM}

procedure TFmRetornoBB.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRetornoBB.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmRetornoBB.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmRetornoBB.LeHeaderArquivo;
const
  TamLine = 240;
var
  Name, s: String;
  IEDCBR: TextFile;
  i, c, k, m: Integer;
begin
(*
Livro: 095     - Processamento de Serv. e Informa��es por Meio Eletronico
Cap�tulo: 0002   - Arquivos - Remessa e Retorno
T�tulo: 00008 - CNAB240 - Caracter�sticas
Subt�tulo: 9991   - Documentos
Vers�o: 0001


01. APRESENTACAO..  eh  o  CONJUNTO DE  INFORMACOES  que  define  e
    orienta  o intercambio entre bancos e as empresas, na prestacao de
    servicos bancarios. A padronizacao eh da FEBRABAN e adotada por
    todos  os  bancos, e qualquer alteracao das especificacoes
    apresentadas  nestas  rotinas deverah ser aprovada  pelo
    CNABFEBRABAN.


02. SERVICOS BANCARIOS OFERECIDOS..                             /-/
    a) pagamentos de salarios, fornecedores, dividendos e  etc,  na
       forma de credito em conta corrente ou DOC e pagamento   com
       autenticacao.,
    b) liquidacao  de titulos em carteira de cobranca  /no  proprio
       Banco/ e pagamento de titulos em  outros  bancos /no  banco do
       cedente, via compensacao/.,
    c) geracao de extrato de conta para conciliacao bancaria.,
    d) informacoes  para  titulos em carteira de cobranca  /entrada de
       titulos, pedido de baixa, etc./.,
    e) informacoes  ao  sacado dos titulos capturados  em  carteira
       /bloqueto eletronico/.


03. CARACTERISTICAS DO ARQUIVO..
    a) Modalidade - registros fixos.,
    b) Tamanho dos registros - 240 bytes.,
*)

  Screen.Cursor := crHourGlass;
  Memo1.Lines.Clear;
  Memo2.Lines.Clear;
  Name := EdPathBBRet.Text;
  if not FileExists(Name) then
  begin
    Application.MessageBox(PChar('O arquivo "'+Name+'" n�o foi localizado!'),
      'Erro', MB_OK+MB_ICONERROR);
    Screen.Cursor := crDefault;
    Exit;
  end;
  AssignFile(IEDCBR, Name);
  Reset(IEDCBR);
  while not Eof(IEDCBR) do
  begin
    Readln(IEDCBR, s);
    if Length(s) = TamLine then
      Memo1.Lines.Add(s)
    else begin
      m := 0;
      while m < Length(s) do
      begin
        Memo1.Lines.Add(Copy(s, m+1, TamLine));
        m := m + TamLine + 1;
      end;
    end;
  end;
  CloseFile(IEDCBR);
  c := 0;
  for i := 0 to Memo1.Lines.Count -1 do
  begin
    if Length(Memo1.Lines[i]) <> TamLine then c := c + 1;
  end;
  if c > 0 then
  begin
    Application.MessageBox(PChar('O arquivo "'+Name+'" possui '+
      IntToStr(c)+' linhas com quantidades de caracteres diferentes  de '+
      IntToStr(TamLine)+'!'), 'Erro', MB_OK+MB_ICONERROR);
    Screen.Cursor := crDefault;
    Exit;
  end;
(*
    c) Tipos de registros..
       I      - 0 /IGUAL/  Header de arquivo.,
       II     - 1 /IGUAL/  Header de lote.,
       III    - 3 /IGUAL/  Detalhe.,
       IV     - 5 /IGUAL/  Trailer de lote.,
       V      - 9 /IGUAL/  Trailer de arquivo.,
    d) Alinhamento  de  campos  numericos  -  sempre  a  direita  e
       preenchidos com zeros a esquerda.,
    e) Alinhamento  de campos alfanumericos - sempre a  esquerda  e
       preenchidos com brancos a direita.


04. COMPOSICAO DO ARQUIVO..
    a) Header de Arquivo.,                                            
    b) Lotes de Servico / tipo de servico /forma de lancamento/.,     
    c) Trailer de Arquivo.                                            
       OBS: OBS.. um  arquivo  pode conter lotes de servicos          
            distintos.  Os registros  Header  e  Trailer de arquivo
            tem  composicao fixa/padrao.
                                                                      
                                                                      
05. COMPOSICAO DO LOTE DE SERVICO..                                   
    a) Um registro Header de lote.,
    b) Registro de detalhe /lancamento/.,                             
    c) Um registro de Trailer de lote.                                
       OBS: OBS.. um  lote  de servico deve conter um unico tipo de   
            servico e uma unica forma de lancamento.
                                                                      
                                                                      
06. O  registro de Header /1/ e Trailer /5/ de lote e os de Detalhe
    /3/  sao compostos de campos fixos, comuns a todos os Tipos  de   
    Servicos   e  Formas  de  Lancamentos,  e  campos  especificos,   
    padroes  para  cada  um  dos Tipos  de  Servicos  e  Formas  de   
    Lancamentos.                                                      
                                                                      
                                                                      
07. Um  registro  de detalhe eh composto de 17 tipos  de  segmentos
    definidos conforme o Tipo de Servico e a Forma de Lancamento.     
                                                                      
                                                                      
08. Leiaute do arquivo..                                        /-/
    REGISTRO DE HEADER DE ARQUIVO - Registro                          
                                                                      
LOTE.. CREDITO EM CONTA CORRENTE                                      
Registro Header de lote                                               
Registro Detalhe - Segmento A /Obrigatorio/                           
Registro Detalhe - Segmento B /Opcional/
Registro Detalhe - Segmento C /Opcional/                              
Registro Trailer de lote                                              
                                                                      
LOTE.. PAGAMENTO ATRAVES DE CHEQUE, OP, DOC E PAGAMENTO COM           
AUTENTICACAO                                                          
Registro Header de lote
Registro Detalhe - Segmento A /Obrigatorio/                           
Registro Detalhe - Segmento B /Opcional/
Registro Detalhe - Segmento C /Opcional/                              
Registro Trailer de lote                                              
                                                                      
LOTE.. EXTRATO CONTA CORRENTE PARA CONCILIACAO BANCARIA Registro
Header de lote                                                        
Registro Detalhe - Segmento E /Obrigatorio/                           
Registro Trailer de lote
                                                                      
LOTE.. LIQUIDACAO DE TITULOS EM CARTEIRA DE COBRANCA         /-/
Registro Header de lote                                               
Registro Detalhe - Segmento J /Obrigatorio/                           
OU                                                                    
Registro Trailer de lote                                              
                                                                      
LOTE.. PAGAMENTOS DE TITULOS EM OUTROS BANCOS                /-/      
Registro Header de lote                                               
Registro Detalhe - Segmento J /Obrigatorio/                           
OU                                                                    
Registro Trailer de lote                                              
                                                                      
LOTE.. TITULOS EM CARTEIRA DE COBRANCA                                
Registro Header de lote
Registro Detalhe - Segmento P /Obrigatorio/                           
Registro Detalhe - Segmento Q /Obrigatorio/                           
Registro Detalhe - Segmento R /Opcional/                              
Registro Detalhe - Segmento S /Opcional/                              
Registro Detalhe - Segmento T /Obrigatorio/                           
Registro Detalhe - Segmento U /Obrigatorio/
Registro Trailer de lote                                              
                                                                      
                                                                      
LOTE.. TITULOS CAPTURADOS EM CARTEIRA /BLOQUETO ELETRONICO/ Registro
Header de lote                                                        
Registro Detalhe - Segmento G /Obrigatorio/                           
Registro Detalhe - Segmento H /Opcional/                              
Registro Trailer de lote
                                                                      
REGISTRO TRAILER DE ARQUIVO - REGISTRO 9                              
/-/
                                                                      
09. As  notas e informacoes complementares apresentadas em codigos    
    alfanumericos  nos  leiautes  dos  arquivos estao esclarecidas no 
    titulo Quadros e Tabelas.                                         
                                                                      

Livro: 095     - Processamento de Serv. e Informa��es por Meio Eletronico  
Cap�tulo: 0002   - Arquivos - Remessa e Retorno  
T�tulo: 00009 - CNAB240 - Registro Header de Arquivo  
Subt�tulo: 8020   - Leiaute  
Vers�o: 0001

01. HEADER DE ARQUIVO   - REGISTRO 0
*)

  if Memo1.Lines[0][8] <> '0' then
  begin
    Application.MessageBox(PChar('A linha 1 do arquivo "'+Name+'" n�o � '+
      'cabe�alho de arquivo!'), 'Erro', MB_OK+MB_ICONERROR);
    Screen.Cursor := crDefault;
    Exit;
  end;

(*
    -----------------------------------------------------------------
*-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-!
!                               !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BANCO NA COMPENSACAO !  1!  3!  3! - !NUMERICO     !54 !
!-------------------------------!---!---!---!---!-------------!---!*)

  EdBcoCodH.Text := Copy(Memo1.Lines[0], 1, 3);

(*
!LOTE DE SERVICO                !  4!  7!  4! - ! 0000        ! 1 !
!-------------------------------!---!---!---!---!-------------!---!
*)

  EdTipoArq.Text := UBancos.CNAB240TipoArqRetBco(Copy(Memo1.Lines[0], 4, 4));

(*
!REGISTRO HEADER DE ARQUIVO     !  8!  8!  1! - ! 0           ! 2 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !  9! 17!  9! - !BRANCOS      !   !
!-------------------------------!---!---!---!---!-------------!---!
!TIPO DE INSCRICAO DA EMPRESA   ! 18! 18!  1! - !1-CPF, 2-CGC ! 6 !
!-------------------------------!---!---!---!---!-------------!---!
!N  DE INSCRICAO DA EMPRESA     ! 19! 32! 14! - !NUMERICO     ! 6 !
!-------------------------------!---!---!---!---!-------------!---!
*)
  Memo2.Lines.Add(Copy(Memo1.Lines[0], 009, 009));
  case Geral.IMV(Copy(Memo1.Lines[0], 018, 001)) of
    1: Label3.Caption := 'CPF:';
    2: Label3.Caption := 'CNPJ:';
    else Label3.Caption := 'CPF / CNPJ:';
  end;
  EdEmpresaCNPJH.Text := Geral.FormataCNPJ_TT(Copy(Memo1.Lines[0], 019, 014));

(*
!CODIGO DO CONVENIO NO BANCO    ! 33! 52! 20! - !ALFANUMERICO !6/7!
!-------------------------------!---!---!---!---!-------------!---!
*)

  EdConvenioH.Text := Copy(Memo1.Lines[0], 33, 20);

(*
!AGENCIA MANTENEDORA DA CONTA   ! 53! 57!  5! - !NUMERICO     !6/8!
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA AGENCIA  ! 58! 58!  1! - !ALFANUMERICO !6/8!
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DA CONTA CORRENTE       ! 59! 70! 12! - !NUMERICO     !6/8!
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA CONTA    ! 71! 71!  1! - !ALFANUMERICO !6/8!
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA AG/CONTA ! 72! 72!  1! - !ALFANUMERICO !6/8!
!-------------------------------!---!---!---!---!-------------!---!
*)

  EdAgenciaNUH.Text := Copy(Memo1.Lines[0], 53, 05);
  EdAgenciaDVH.Text := Copy(Memo1.Lines[0], 58, 01);
  EdContaNUH.Text   := Copy(Memo1.Lines[0], 59, 12);
  EdContaDVH.Text   := Copy(Memo1.Lines[0], 71, 01);
  EdAgenCcDVH.Text  := Copy(Memo1.Lines[0], 72, 01);

(*
!NOME DA EMPRESA                ! 73!102! 30! - !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!
!NOME DO BANCO                  !103!132! 30! - !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !133!142! 10! - !BRANCOS      !   !
!-----------------------------------------------------------------!
*)

  EdEmpresaNomeH.Text := Copy(Memo1.Lines[0], 073, 30);
  EdBcoNomH.Text      := Copy(Memo1.Lines[0], 103, 30);
  Memo2.Lines.Add(Copy(Memo1.Lines[0], 133, 010));

(*
!CODIGO REMESSA / RETORNO       !143!143!  1! - !1-REM e 2-RET!   !
!-------------------------------!---!---!---!---!-------------!---!
!DATA DE GERACAO DO ARQUIVO     !144!151!  8! - !NUM/DDMMAAAA/!   !
!-------------------------------!---!---!---!---!-------------!---!
!HORA DE GERACAO DO ARQUIVO     !152!157!  6! - !NUM /HHMMSS/ !   !
!-------------------------------!---!---!---!---!-------------!---!
*)

  case Geral.IMV(Copy(Memo1.Lines[0], 143, 01)) of
    1: EdTipoRem.Text := 'Remessa';
    2: EdTipoRem.Text := 'Retorno';
    else EdTipoRem.Text := '------';
  end;
  EdDataGeraH.Text := MLAGeral.DataCBRTxt(Copy(Memo1.Lines[0], 144, 08));
  EdHoraGeraH.Text := MLAGeral.HoraCBRTxt(Copy(Memo1.Lines[0], 152, 06));

(*
!N  SEQUENCIAL DO ARQUIVO       !158!163!  6! - !NUMERICO     !17 !
!-------------------------------!---!---!---!---!-------------!---!
!N DA VERSAO DO LAYOUT DO ARQUIV!164!166!  3! - ! 030         ! 9 !
!-------------------------------!---!---!---!---!-------------!---!
!DENSIDADE DE GRAVACAO DO ARQUIV!167!171!  5! - !NUMERICO/BPI/!   !
!-------------------------------!---!---!---!---!-------------!---!
!PARA USO RESERVADO DO BANCO    !172!191! 20! - !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!
!PARA USO RESERVADO DA EMPRESA  !192!211! 20! - !ALFANUMERICO !   !
*-----------------------------------------------------------------*
*)

  EdSeqArq.Text         := Copy(Memo1.Lines[0], 158, 06);
  EdLayoutArq.Text      := Copy(Memo1.Lines[0], 164, 03);
  EdDensiGrava.Text     := Copy(Memo1.Lines[0], 167, 05);
  EdReservaBanco.Text   := Copy(Memo1.Lines[0], 172, 20);
  EdReservaEmpresa.Text := Copy(Memo1.Lines[0], 192, 20);

(*

HEADER DE ARQUIVO   - REGISTRO 0                            /CONT./
------------------------------------------------------------------
*-----------------------------------------------------------------*
!USO EXCLUSIVO FEBRABAN/CNAB    !212!222! 11! - !BRANCOS      !   !
!-------------------------------!---!---!---!---!-------------!---!
!IDENTIFICACAO  COBRANCA S/PAPEL!223!225!  3! - !'CSP'        !   !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO DAS VANS         !226!228!  3! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!TIPO DE SERVICO                !229!230!  2! - !ALFANUMERICO !52 !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGOS DAS OCORRENCIAS        !231!240! 10! - !ALFANUMERICO !53 !
*-----------------------------------------------------------------*
*)

  Memo2.Lines.Add(      Copy(Memo1.Lines[0], 212, 011));
  EdCSP.Text         := Copy(Memo1.Lines[0], 223, 003);
  EdVans.Text        := Copy(Memo1.Lines[0], 226, 003);
  EdTipoServico.Text := Copy(Memo1.Lines[0], 229, 002);
  EdCodigoOcorr.Text := Copy(Memo1.Lines[0], 231, 010);
  // FIM HEADER ARQUIVO
  //
  MyObjects.LimpaGrade(GradeA, 0, 0, True);
  c := 0;
  for i := 1 to Memo1.Lines.Count - 1 do
  begin
    k := Geral.IMV(Copy(Memo1.Lines[i], 8, 1));
    //ShowMessage(IntToStr(i)+' = '+IntToStr(k));
    if k = 1 then
    begin
      c := c + 1;
      GradeA.RowCount := c + 1;
      GradeA.Cells[00,c] := IntToStr(c);
      GradeA.Cells[01,c] := IntToStr(i);
    end;
  end;
end;


procedure TFmRetornoBB.FormCreate(Sender: TObject);
begin
  FShiftPressed := False;
  FPosIniCol := 0;
  FPosIniRow := 0;
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  Memo1.Lines.Clear;
  Memo2.Lines.Clear;
  QrBanco.Open;
end;

procedure TFmRetornoBB.EdBcoCodHChange(Sender: TObject);
begin
  if QrBanco.Locate('Codigo', Geral.IMV(EdBcoCodH.Text), []) then
    EdBcoNomH.Text := QrBancoNome.Value else EdBcoNomH.Text := '';
end;

procedure TFmRetornoBB.BtAbreClick(Sender: TObject);
begin
  OpenDialog1.InitialDir := Dmod.QrControlePathBBRet.Value;
  if OpenDialog1.Execute then
  begin
    Screen.Cursor := crHourGlass;
    EdPathBBRet.Text := OpenDialog1.FileName;
    Application.ProcessMessages;
    LeHeaderArquivo;
    LeTrailerArquivo;
    VaiLote(-2);
    Screen.Cursor := crDefault;
  end;
  Screen.Cursor := crDefault;
  BtGrava.Enabled := True;
  PageControl1.ActivePageIndex := 2;
end;

procedure TFmRetornoBB.SpeedButton1Click(Sender: TObject);
begin
  VaiLote(-2);
end;

procedure TFmRetornoBB.SpeedButton2Click(Sender: TObject);
begin
  VaiLote(-1);
end;

procedure TFmRetornoBB.SpeedButton3Click(Sender: TObject);
begin
  VaiLote(+1);
end;

procedure TFmRetornoBB.SpeedButton4Click(Sender: TObject);
begin
  VaiLote(+2);
end;

procedure TFmRetornoBB.VaiLote(Item: Integer);
var
  LoteN, LoteA, LoteF, i: Integer;
begin
  LimpaTextos;
  LoteN := 0;
  LoteA := Geral.IMV(Copy(LaRegistroA.Caption, 1, 4));
  LoteF := GradeA.RowCount - 1;
  case Item of
    -2: LoteN := 0;
    -1: LoteN := LoteA - 1;
    +1: LoteN := LoteA + 1;
    +2: LoteN := LoteF;
  end;
  if LoteN < 1 then LoteN := 1;
  if LoteN > LoteF then LoteN := LoteF;
  //
  i := Geral.IMV(GradeA.Cells[1, LoteN]);
  if i = 0 then
  begin
    Application.MessageBox(PChar('Erro ao localizar lote '+IntToStr(LoteN)+'!'),
      'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end else LeLote(i);
end;

procedure TFmRetornoBB.LimpaTextos;
begin
  // Segmento P
  EdBcoCodDetP.Text  := '';
  EdBcoNomDetP.Text  := '';
  EdLotDetP.Text     := '';
  EdSeqDetP.Text     := '';
  EdCNABLot009.Text  := '';
  EdForCadTitP.Text  := '';
  EdTipoDocumP.Text  := '';
  EdIDEmitBloP.Text  := '';
  EdIDDistribP.Text  := '';
  EdDocCobDetP.Text  := '';
  EdVenTitDetP.Text  := '';
  EdValNomDetP.Text  := '';
  EdCodMovDetP.Text  := '';
  EdAgenciaNUP.Text  := '';
  EdAgenciaDVP.Text  := '';
  EdContaNUP.Text    := '';
  EdContaDVP.Text    := '';
  EdAgenCcDVP.Text   := '';
  EdIdeTitDetP.Text  := '';
  EdCodCarDetP.Text  := '';
  EdAgenciaNUP2.Text := '';
  EdAgenciaDVP2.Text := '';
  EdEspeciTitP.Text  := '';
  EdIDTitAceiP.Text  := '';
  EdDatEmiTitP.Text  := '';
  EdCodJurMorP.Text  := '';
  EdDatJurMorP.Text  := '';
  EdJurMorDiaP.Text  := '';
  EdCodDesco1P.Text  := '';
  EdDatDesco1P.Text  := '';
  EdValPerDe1P.Text  := '';
  EdValIOFDe1P.Text  := '';
  EdValDesco1P.Text  := '';
  EdIDTiEDetP.Text   := '';
  EdCodParProP.Text  := '';
  EdNddParProP.Text  := '';
  EdCodParBaiP.Text  := '';
  EdNddParBaiP.Text  := '';
  // Segmento Q
  EdBcoCodDetQ.Text := '';
  EdLotDetQ.Text    := '';
  EdSeqDetQ.Text    := '';
  EdCNABLot010.Text := '';
  EdCPFCNPDetQ.Text := '';
  EdEmiCliDetQ.Text := '';
  EdEmiEndDetQ.Text := '';
  EdEmiBaiDetQ.Text := '';
  EdEmiCEPDetQ.Text := '';
  EdEmiSufDetQ.Text := '';
  EdEmiCidDetQ.Text := '';
  EdEmi_UFDetQ.Text := '';
  EdEmiCPFDetQ.Text := '';
  EdEmiSacDetQ.Text := '';
  EdEmiBCCDetQ.Text := '';
  EdEmiNNBDetQ.Text := '';
  EdCNABLot011.Text := '';
  // Segmento R
  EdBcoCodDetR.Text := '';
  EdLotDetR.Text    := '';
  EdSeqDetR.Text    := '';
  EdCNABLot012.Text := '';
  EdCodMovDetR.Text := '';
  EdCodDesco2R.Text := '';
  EdDatDesco2R.Text := '';
  EdValPerDe2R.Text := '';
  EdCodDesco3R.Text := '';
  EdDatDesco3R.Text := '';
  EdValPerDe3R.Text := '';
  EdCodiMultaR.Text := '';
  EdDataMultaR.Text := '';
  EdVaPeMultaR.Text := '';
  EdInfBcoSacR.Text := '';
  EdMensagem3R.Text := '';
  EdMensagem4R.Text := '';
  EdConDebBcoR.Text := '';
  EdConDebAgeR.Text := '';
  EdConDebCCoR.Text := '';
  EdCodOcoSacR.Text := '';
  EdCNABLot013.Text := '';
  // Segmento S
              // Parei aqui
  // Segmento T
  EdBcoCodDetT.Text  := '';
  EdBcoNomDetT.Text  := '';
  EdLotDetT.Text     := '';
  EdSeqDetT.Text     := '';
  EdCNABLot3.Text    := '';
  EdCodMovDetT.Text  := '';
  EdAgenciaNUT.Text  := '';
  EdAgenciaDVT.Text  := '';
  EdContaNUT.Text    := '';
  EdContaDVT.Text    := '';
  EdAgenCcDVT.Text   := '';
  EdIdeTitDetT.Text  := '';
  EdCodCarDetT.Text  := '';
  EdDocCobDetT.Text  := '';
  EdVenTitDetT.Text  := '';
  EdValNomDetT.Text  := '';
  EdBcoCodDetT2.Text := '';
  EdBcoNomDetT2.Text := '';
  EdAgenciaNUT2.Text := '';
  EdAgenciaDVT2.Text := '';
  EdIDTiEDetT.Text   := '';
  EdCodMoeDetT.Text  := '';
  EdTipInsDetT.Text  := '';
  EdNumInsDetT.Text  := '';
  EdNomeDetT.Text    := '';
  EdNConOCDetT.Text  := '';
  EdTarCusDetT.Text  := '';
  EdIdRTCLDetT.Text  := '';
  EdCNABLot4.Text    := '';
  // Segmento U
  EdBcoCodDetU.Text := '';
  EdLotDetU.Text    := '';
  EdSeqDetU.Text    := '';
  EdCNABLot5.Text   := '';
  EdCodMovDetU.Text := '';
  EdValJMEDetU.Text := '';
  EdValDesDetU.Text := '';
  EdValAbaDetU.Text := '';
  EdValIOFDetU.Text := '';
  EdValPagDetU.Text := '';
  EdValLiqDetU.Text := '';
  EdValOuDDetU.Text := '';
  EdValOuCDetU.Text := '';
  EdDatOcoDetU.Text := '';
  EdDatCreDetU.Text := '';
  EdCodOcoDetU.Text := '';
  EdDatOcSDetU.Text := '';
  EdValOcSDetU.Text := '';
  EdComOcSDetU.Text := '';
  EdCBcCoCDetU.Text := '';
  EdNBcCoCDetU.Text := '';
  EdNNBCoCDetU.Text := '';
  EdCNABLot6.Text   := '';
end;

procedure TFmRetornoBB.EdBcoCodLChange(Sender: TObject);
begin
  if QrBanco.Locate('Codigo', Geral.IMV(EdBcoCodL.Text), []) then
    EdBcoNomL.Text := QrBancoNome.Value else EdBcoNomL.Text := '';
end;

procedure TFmRetornoBB.LeLote(Linha: Integer);
var
  c, i, z: Integer;
  x: String;
begin
(*

Livro: 095     - Processamento de Serv. e Informa��es por Meio Eletronico
Cap�tulo: 0002   - Arquivos - Remessa e Retorno
T�tulo: 00013 - CNAB240 - T�tulos em Carteira de Cobran�a
Subt�tulo: 8020   - Leiaute
Vers�o: 0001

HEADER DE LOTE  -  REGISTRO 1
-----------------------------------------------------------------
*-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-!
!                               !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BANCO NA COMPENSACAO !  1!  3!  3! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!LOTE DE SERVICO                !  4!  7!  4! - !NUMERICO     ! 1 !
!-------------------------------!---!---!---!---!------------ !---!
!REGISTRO HEADER DO LOTE        !  8!  8!  1! - ! 1           ! 2 !
!-------------------------------!---!---!---!---!-------------!---!
*)

  EdBcoCodL.Text      := Copy(Memo1.Lines[Linha], 001, 003);
  LaRegistroA.Caption := Copy(Memo1.Lines[Linha], 004, 004) + '/' +
    FormatFloat('0000', Geral.IMV(GradeA.Cells[0, GradeA.RowCount-1]));
(*
!TIPO DE OPERACAO               !  9!  9!  1! - !ALFANUMERICO ! 3 !
!-------------------------------!---!---!---!---!-------------!---!
!TIPO DE SERVICO                ! 10! 11!  2! - ! 01          ! 4 !
!-------------------------------!---!---!---!---!-------------!---!
!FORMA DE LANCAMENTO            ! 12! 13!  2! - !ZEROS        !39 !
!-------------------------------!---!---!---!---!-------------!---!
!N  DA VERSAO DO LAYOUT DO LOTE ! 14! 16!  3! - !'020'        !45 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    ! 17! 17!  1! - !BRANCOS      !   !
!-----------------------------------------------------------------!
*)

  EdOperacao.Text   := Copy(Memo1.Lines[Linha], 009, 001);
  EdServico.Text    := Copy(Memo1.Lines[Linha], 010, 002);
  EdFormLanc.Text   := Copy(Memo1.Lines[Linha], 012, 002);
  EdLayoutLot.Text  := Copy(Memo1.Lines[Linha], 014, 003);
  EdCNABLot1.Text   := Copy(Memo1.Lines[Linha], 017, 001);

(*
!TIPO DE INSCRICAO DA EMPRESA   ! 18! 18!  1! - !1-CPF, 2-CGC !   !
!-------------------------------!---!---!---!---!-------------!---!
!N  DE INSCRICAO DA EMPRESA     ! 19! 33! 15! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO CONVENIO NO BANCO    ! 34! 53! 20! - !ALFANUMERICO ! 7 !
!-------------------------------!---!---!---!---!-------------!---!
!AGENCIA MANTENEDORA DA CONTA   ! 54! 58!  5! _ !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA AGENCIA  ! 59! 59!  1! - !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DA CONTA CORRENTE       ! 60! 71! 12! _ !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA CONTA    ! 72! 72!  1! _ !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA AG/CONTA ! 73! 73!  1! _ !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!
!NOME DA EMPRESA                ! 74!103! 30! - !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!*)

  case Geral.IMV(Copy(Memo1.Lines[Linha], 018, 001)) of
    1: Label24.Caption := 'CPF:';
    2: Label24.Caption := 'CNPJ:';
    else Label24.Caption := 'CPF / CNPJ:';
  end;
  EdEmpresaCNPJL.Text := Geral.FormataCNPJ_TT(Copy(Memo1.Lines[Linha], 019, 015));
  EdConvenioL.Text    := Copy(Memo1.Lines[Linha], 034, 020);
  EdAgenciaNUL.Text   := Copy(Memo1.Lines[Linha], 054, 005);
  EdAgenciaDVL.Text   := Copy(Memo1.Lines[Linha], 059, 001);
  EdContaNUL.Text     := Copy(Memo1.Lines[Linha], 060, 012);
  EdContaDVL.Text     := Copy(Memo1.Lines[Linha], 072, 001);
  EdAgenCcDVL.Text    := Copy(Memo1.Lines[Linha], 073, 001);
  EdEmpresaNomeL.Text := Copy(Memo1.Lines[Linha], 074, 030);

(*
!MENSAGEM 1                     !104!143! 40! - !ALFANUMERICO !19 !
!-------------------------------!---!---!---!---!-------------!---!
!MENSAGEM 2                     !144!183! 40! - !ALFANUMERICO !19 !
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO REMESSA/RETORNO         !184!191!  8! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!DATA DE GRAVACAO REMESSA/RETORN!192!199!  8! - !DDMMAAAA     !   !
!-------------------------------!---!---!---!---!-------------!---!
!DATA DO CREDITO                !200!207!  8! - !DDMMAAAA     !   !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !208!240! 33! - !BRANCOS      !   !
*-----------------------------------------------------------------* *)

  EdMensagem1.Text := Copy(Memo1.Lines[Linha], 104, 040);
  EdMensagem2.Text := Copy(Memo1.Lines[Linha], 144, 040);
  EdRemRet.Text    := Copy(Memo1.Lines[Linha], 184, 008);
  EdDataGeraL.Text := MLAGeral.DataCBRTxt(Copy(Memo1.Lines[Linha], 192, 08));
  EdHoraGeraL.Text := MLAGeral.DataCBRTxt(Copy(Memo1.Lines[Linha], 200, 08));
  EdCNABLot2.Text  := Copy(Memo1.Lines[Linha], 208, 33);
  //
  MyObjects.LimpaGrade(GradeB, 0, 0, True);
  c := 0;
  i := 0;
  z := 0;
  while c <> 5 do
  begin
    Application.ProcessMessages;
    i := i + 1;
    c := Geral.IMV(Copy(Memo1.Lines[Linha+i], 008, 001));
    x := Copy(Memo1.Lines[Linha+i], 014, 001);
    //ShowMessage(IntToStr(c));
    if c = 3 then
    begin
      if (x = 'T') or (x = 'P') then
      begin
        z := z + 1;
        GradeB.RowCount := z + 1;
        GradeB.Cells[00,z] := IntToStr(z);
        GradeB.Cells[01,z] := IntToStr(Linha+i);
        if z > 1 then
          GradeB.Cells[02,z-1] := IntToStr(Linha+i-1);
      end;
    end;
  end;
  if z > 0 then
    GradeB.Cells[02,z] := IntToStr(Linha+i-1);
  //5:
  begin
(*
TRAILER DE LOTE - REGISTRO 5
*-----------------------------------------------------------------*
*-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-!
!                               !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BANCO NA COMPENSACAO !  1!  3!  3! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!LOTE DE SERVICO                !  4!  7!  4! - !NUMERICO     ! 1 !
!-------------------------------!---!---!---!---!-------------!---!
!REGISTRO TRAILER DO LOTE       !  8!  8!  1! - ! 5           ! 2 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !  9! 17!  9! - !BRANCOS      !   !
!-------------------------------!---!---!---!---!-------------!---!
!QUANTIDADE DE REGISTROS DO LOTE! 18! 23!  6! - !NUMERICO     !38 !
!-------------------------------!---!---!---!---!-------------!---!
*)
    EdBcoCodTra5.Text := Copy(Memo1.Lines[Linha+i], 001, 003);
    EdLotTra5.Text    := Copy(Memo1.Lines[Linha+i], 004, 004);
    EdCNABLot7.Text   := Copy(Memo1.Lines[Linha+i], 009, 009);
    EdRegistTra5.Text := Copy(Memo1.Lines[Linha+i], 018, 006);
(*
!QUANTIDADE DE TIT. EM COBRANCA ! 24! 29!  6! - !NUMERICO     !41 !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR TOT. DOS TIT. EM CARTEIRA! 30! 46! 15! 2 !NUMERICO     !41 !
!-------------------------------!---!---!---!---!-------------!---!
!QUANTIDADE DE TIT. EM COBRANCA ! 47! 52!  6! - !NUMERICO     !41 !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR TOT DOS TIT. EM CARTEIRAS! 53! 69! 15! 2 !NUMERICO     !41 !
!-------------------------------!---!---!---!---!-------------!---!
!QUANTIDADE DE TIT. EM COBRANCA ! 70! 75!  6! - !NUMERICO     !41 !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR TOT DOS TIT. EM CARTEIRAS! 76! 92! 15! 2 !NUMERICO     !41 !
!-------------------------------!---!---!---!---!-------------!---!
!QUANTIDADE DE TIT. EM COBRANCA ! 93! 98!  6! - !NUMERICO     !41 !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR TOT DOS TIT. EM CARTEIRAS! 99!115! 15! 2 !NUMERICO     !41 !
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DO AVISO DE LANCAMENTO  !116!123!  8! - !ALFANUMERICO !41 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !124!240!117! - !BRANCOS      !   !
*-----------------------------------------------------------------*
*)
    EdQtTit1Tra5.Text := Copy(Memo1.Lines[Linha+i], 024, 006);
    EdVaTit1Tra5.Text := MLAGeral.XFT(Copy(Memo1.Lines[Linha+i], 030, 017), 2, siPositivo);
    EdQtTit2Tra5.Text := Copy(Memo1.Lines[Linha+i], 047, 006);
    EdVaTit2Tra5.Text := MLAGeral.XFT(Copy(Memo1.Lines[Linha+i], 053, 017), 2, siPositivo);
    EdQtTit3Tra5.Text := Copy(Memo1.Lines[Linha+i], 070, 006);
    EdVaTit3Tra5.Text := MLAGeral.XFT(Copy(Memo1.Lines[Linha+i], 076, 017), 2, siPositivo);
    EdQtTit4Tra5.Text := Copy(Memo1.Lines[Linha+i], 093, 006);
    EdVaTit4Tra5.Text := MLAGeral.XFT(Copy(Memo1.Lines[Linha+i], 099, 017), 2, siPositivo);
    EdNuAvLaTra5.Text := Copy(Memo1.Lines[Linha+i], 116, 008);
    EdCNABLot8.Text   := Copy(Memo1.Lines[Linha+i], 124, 117);
  end;
  VaiRegistro(-2, False);
end;

procedure TFmRetornoBB.LeRegistro(Linha, LinhaF, Registro: Integer; InsereNoLote: Integer);
var
  c, i, Envio, Controle, Lote, Movimento, IRTCLB, Item, Continua, Sequencia,
  CNAB240I: Integer;
  x, DataOcor: String;
  Custas, ValPago, ValCred: Double;
begin
  QrCNAB240.Close;
  //
  if (Linha = 0) or (LinhaF = 0) then  Registro := 0 else
  begin
    if InsereNoLote > 0 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO cnab240 SET ');
      Dmod.QrUpd.SQL.Add('Controle=:P0, Banco=:P1, ConfigBB=:P2, Lote=:P3, ');
      Dmod.QrUpd.SQL.Add('Item=:P4, DataOcor=:P5, Envio=:P6, Movimento=:P7, ');
      Dmod.QrUpd.SQL.Add('IRTCLB=:P8, Custas=:P9, ValPago=:P10, ');
      Dmod.QrUpd.SQL.Add('ValCred=:P11, Sequencia=:P12, Convenio=:P13,');
      Dmod.QrUpd.SQL.Add('CNAB240L=:Pa, CNAB240I=:Pb');
    end;
  //
(*LOTE.. TITULOS EM CARTEIRA DE COBRANCA
Registro Header de lote
Registro Detalhe - Segmento P /Obrigatorio/
Registro Detalhe - Segmento Q /Obrigatorio/
Registro Detalhe - Segmento R /Opcional/
Registro Detalhe - Segmento S /Opcional/
Registro Detalhe - Segmento T /Obrigatorio/
Registro Detalhe - Segmento U /Obrigatorio/
Registro Trailer de lote*)
(*

REMESSA
DETALHE -  REGISTRO 3  -  SEGMENTO P  /OBRIGATORIO/
-----------------------------------------------------------------
*-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-!
!                               !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BANCO NA COMPENSACAO !  1!  3!  3! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!LOTE DE SERVICO                !  4!  7!  4! - !NUMERICO     ! 1 !
!-------------------------------!---!---!---!---!-------------!---!
!REGISTRO DETALHE               !  8!  8!  1! - !Igual a 3    ! 2 !
!-------------------------------!---!---!---!---!-------------!---!
!N  SEQUENCIAL DO REG. NO LOTE  !  9! 13!  5! - !NUMERICO     !10 !
!-------------------------------!---!---!---!---!-------------!---!
!COD. SEGMENTO DO REG. DETALHE  ! 14! 14!  1! - !Igual a P    !11 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    ! 15! 15!  1! - !BRANCOS      !   !
!-------------------------------!---!---!---!---!-------------!---!
*)
    if EdOperacao.Text = 'R' then
    begin
      //Envio := 1;//Remessa  Parei Aqui Para SQL. Fazer ?
      for i := Linha to LinhaF do
      begin
        Application.ProcessMessages;
        c := Geral.IMV(Copy(Memo1.Lines[i], 008, 001));
        x := Copy(Memo1.Lines[i], 014, 001);
        //ShowMessage(IntToStr(c));
        case c of
          3:
          begin
            if x = 'P' then
            begin
              EdBcoCodDetP.Text := Copy(Memo1.Lines[i], 001, 003);
              EdLotDetP.Text    := Copy(Memo1.Lines[i], 004, 004);
              EdSeqDetP.Text    := Copy(Memo1.Lines[i], 009, 005);
              EdCNABLot009.Text := Copy(Memo1.Lines[i], 015, 001);
(*
!CODIGO DE MOVIMENTO            ! 16! 17!  2! - !NUMERICO     !20 !
!-----------------------------------------------------------------!
!AGENCIA MANTENEDORA DA CONTA   ! 18! 22!  5! _ !NUMERICO     ! 8 !
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA AGENCIA  ! 23! 23!  1! _ !ALFANUMERICO ! 8 !
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DA CONTA CORRENTE       ! 24! 35! 12! _ !NUMERICO     ! 8 !
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA CONTA    ! 36! 36!  1! _ !ALFANUMERICO ! 8 !
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA AG/CONTA ! 37! 37!  1! - !ALFANUMERICO ! 8 !
!-------------------------------!---!---!---!---!-------------!---!
*)
              EdCodMovDetP.Text := Copy(Memo1.Lines[i], 016, 002);
              EdAgenciaNUP.Text := Copy(Memo1.Lines[i], 018, 005);
              EdAgenciaDVP.Text := Copy(Memo1.Lines[i], 023, 001);
              EdContaNUP.Text   := Copy(Memo1.Lines[i], 024, 012);
              EdContaDVP.Text   := Copy(Memo1.Lines[i], 036, 001);
              EdAgenCcDVP.Text  := Copy(Memo1.Lines[i], 037, 001);
(*
!IDENTIFICACAO DO TITULO NO BANC! 38! 57! 20! - !ALFANUMERICO !21 !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DA CARTEIRA             ! 58! 58!  1! _ !NUMERICO     !22 !
!-------------------------------!---!---!---!---!-------------!---!
!FORMA DE CADASTRAMENTO DO TITU-! 59! 59!  1! _ !1-COM CADAST !   !
!LO NO BANCO                    !   !   !   !   !2-SEM CADAST !   !
!-------------------------------!---!---!---!---!-------------!---!
!TIPO DE DOCUMENTO              ! 60! 60!  1! _ !1-TRADICIONAL!   !
!                               !   !   !   !   !2-ESCRITURAL !   !
!-------------------------------!---!---!---!---!-------------!---!
!IDENTIFI DA EMISSAO DO BLOQUETO! 61! 61!  1! _ !NUMERICO     !23 !
!-------------------------------!---!---!---!---!-------------!---!
!IDENTIFICACAO DA DISTRIBUICAO  ! 62! 62!  1! - !1-BANCO      !   !
!                               !   !   !   !   !2-CLIENTE    !   !
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DO DOCUMENTO DE COBRANCA! 63! 77! 15! - !ALFANUMERICO !24 !
!-------------------------------!---!---!---!---!-------------!---!
!DATA DE VENCIMENTO DO TITULO   ! 78! 85!  8! - !DDMMAAAA     !48 !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR NOMINAL DO TITULO        ! 86!100! 13! 2 !NUMERICO     !   !
*-----------------------------------------------------------------*
*)
              EdIdeTitDetP.Text  := Copy(Memo1.Lines[i], 038, 020);
              EdCodCarDetP.Text  := Copy(Memo1.Lines[i], 058, 001);
              EdForCadTitP.Text  := Copy(Memo1.Lines[i], 059, 001);
              EdTipoDocumP.Text  := Copy(Memo1.Lines[i], 060, 001);
              EdIDEmitBloP.Text  := Copy(Memo1.Lines[i], 061, 001);
              EdIDDistribP.Text  := Copy(Memo1.Lines[i], 062, 001);
              EdDocCobDetP.Text  := Copy(Memo1.Lines[i], 063, 015);
              EdVenTitDetP.Text  := MLAGeral.DataCBRTxt(Copy(Memo1.Lines[i], 078, 008));
              EdValNomDetP.Text  := MLAGeral.XFT(Copy(Memo1.Lines[i], 086, 015), 2, siPositivo);
(*
DETALHE -  REGISTRO 3  -  SEGMENTO P  /OBRIGATORIO/         /CONT./
-----------------------------------------------------------------
*-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-!
!                               !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!AGENCIA ENCARREGADA DA COBRANCA!101!105!  5! - !NUMERICO     !49 !
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA AGENCIA  !106!106!  1! - !ALFANUMERICO !49 !
!-------------------------------!---!---!---!---!-------------!---!
!ESPECIE DO TITULO              !107!108!  2! - !NUMERICO     !25 !
!-------------------------------!---!---!---!---!-------------!---!
!IDENTIFICACAO DE TITULO ACEITO/!109!109!  1! - !A - ACEITE   !   !
!NAO ACEITO                     !   !   !   !   !N - NAO ACEIT!   !
!-------------------------------!---!---!---!---!-------------!---!
!DATA DA EMISSAO DO TITULO      !110!117!  8! - !DDMMAAAA     !   !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO JUROS DE MORA        !118!118!  1! - !NUMERICO     !26 !
!-------------------------------!---!---!---!---!-------------!---!
!DATA DO JUROS DE MORA          !119!126!  8! - !DDMMAAAA     !27 !
!-------------------------------!---!---!---!---!-------------!---!
!JUROS DE MORA POR DIA/TAXA     !127!141! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
*)
              EdAgenciaNUP2.Text := Copy(Memo1.Lines[i], 101, 005);
              EdAgenciaDVP2.Text := Copy(Memo1.Lines[i], 106, 001);
              EdEspeciTitP.Text  := Copy(Memo1.Lines[i], 107, 002);
              EdIDTitAceiP.Text  := Copy(Memo1.Lines[i], 109, 001);
              EdDatEmiTitP.Text  := MLAGeral.DataCBRTxt(Copy(Memo1.Lines[i], 110, 008));
              EdCodJurMorP.Text  := Copy(Memo1.Lines[i], 118, 001);
              EdDatJurMorP.Text  := MLAGeral.DataCBRTxt(Copy(Memo1.Lines[i], 119, 008));
              EdJurMorDiaP.Text  := MLAGeral.XFT(Copy(Memo1.Lines[i], 127, 015), 2, siPositivo);
(*
!CODIGO DO DESCONTO 1           !142!142!  1! - !NUMERICO     !28 !
!-------------------------------!---!---!---!---!-------------!---!
!DATA DO DESCONTO 1             !143!150!  8! - !DDMMAAAA     !   !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR/PERCENTUAL A SER CONCEDID!151!165! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR DO IOF A SER RECOLHIDO   !166!180! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR DO ABATIMENTO            !181!195! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
*)
              EdCodDesco1P.Text  := Copy(Memo1.Lines[i], 142, 001);
              EdDatDesco1P.Text  := MLAGeral.DataCBRTxt(Copy(Memo1.Lines[i], 143, 008));
              EdValPerDe1P.Text  := MLAGeral.XFT(Copy(Memo1.Lines[i], 151, 015), 2, siPositivo);
              EdValIOFDe1P.Text  := MLAGeral.XFT(Copy(Memo1.Lines[i], 166, 015), 2, siPositivo);
              EdValDesco1P.Text  := MLAGeral.XFT(Copy(Memo1.Lines[i], 181, 015), 2, siPositivo);
(*
!IDENTIFICACAO DO TIT. NA EMPRES!196!220! 25! - !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO PARA PROTESTO           !221!221!  1! - !1-DIAS CORRID!   !
!                               !   !   !   !   !2-DIAS UTEIS !   !
!                               !   !   !   !   !3-NAO PROTES !   !
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DE DIAS PARA PROTESTO   !222!223!  2! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO PARA BAIXA/DEVOLUCAO    !224!224!  1! - !1-BAIXAR/DEV !   !
!                               !   !   !   !   !2-NAO BAIXAR/!   !
!                               !   !   !   !   ! NAO DEVOLVER!   !
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DE DIAS PARA BAIXA/DEVOL!225!227!  3! - !DIAS CORRIDOS!   !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DA MOEDA                !228!229!  2! - !NUMERICO     !29 !
!-------------------------------!---!---!---!---!-------------!---!
!N. DO CONTR. DA OPERACAO D CRED!230!239! 10! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !240!240!  1! - !BRANCOS      !   !
*-----------------------------------------------------------------*
*)
              EdIDTiEDetP.Text   := Copy(Memo1.Lines[i], 196, 025);
              EdCodParProP.Text  := Copy(Memo1.Lines[i], 221, 001);
              EdNddParProP.Text  := Copy(Memo1.Lines[i], 222, 002);
              EdCodParBaiP.Text  := Copy(Memo1.Lines[i], 224, 001);
              EdNddParBaiP.Text  := Copy(Memo1.Lines[i], 225, 003);
            end
(*
REMESSA

DETALHE -  REGISTRO 3  -  SEGMENTO Q    /OBRIGATORIO/
-----------------------------------------------------------------
*-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-!
!                               !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BANCO NA COMPENSACAO !  1!  3!  3! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!LOTE DE SERVICO                !  4!  7!  4! - !NUMERICO     ! 1 !
!-------------------------------!---!---!---!---!-------------!---!
!REGISTRO DETALHE               !  8!  8!  1! - ! 3           ! 2 !
!-------------------------------!---!---!---!---!-------------!---!
!N  SEQUENCIAL DO REG. NO LOTE  !  9! 13!  5! - !NUMERICO     !10 !
!-------------------------------!---!---!---!---!-------------!---!
!COD. SEGMENTO DO REG. DETALHE  ! 14! 14!  1! - ! Q           !11 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    ! 15! 15!  1! - !BRANCOS      !   !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DE MOVIMENTO            ! 16! 17!  2! - !NUMERICO     !20 !
!-----------------------------------------------------------------!
*)
            else if x = 'Q' then
            begin
              EdBcoCodDetQ.Text := Copy(Memo1.Lines[i], 001, 003);
              EdLotDetQ.Text    := Copy(Memo1.Lines[i], 004, 004);
              EdSeqDetQ.Text    := Copy(Memo1.Lines[i], 009, 005);
              EdCNABLot010.Text := Copy(Memo1.Lines[i], 015, 001);
              EdCodMovDetQ.Text := Copy(Memo1.Lines[i], 016, 002);
(*
!TIPO DE INSCRICAO              ! 18! 18!  1! - !NUMERICO     !30 !
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DE INSCRICAO            ! 19! 33! 15! - !NUMERICO     !30 !
!-------------------------------!---!---!---!---!-------------!---!
*)
              case Geral.IMV(Copy(Memo1.Lines[i], 018, 001)) of
                1: Label163.Caption := 'CPF:';
                2: Label163.Caption := 'CNPJ:';
                else Label163.Caption := 'CPF / CNPJ:';
              end;
              EdCPFCNPDetQ.Text := Geral.FormataCNPJ_TT(Copy(Memo1.Lines[1], 019, 015));
(*
!NOME                           ! 34! 73! 40! - !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!
!ENDERECO                       ! 74!113! 40! - !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!
!BAIRRO                         !114!128! 15! - !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!
!CEP                            !129!133!  5! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!SUFIXO DO CEP                  !134!136!  3! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
*)
              EdEmiCliDetQ.Text := Copy(Memo1.Lines[i], 034, 040);
              EdEmiEndDetQ.Text := Copy(Memo1.Lines[i], 074, 040);
              EdEmiBaiDetQ.Text := Copy(Memo1.Lines[i], 114, 015);
              EdEmiCEPDetQ.Text := Copy(Memo1.Lines[i], 129, 005);
              EdEmiSufDetQ.Text := Copy(Memo1.Lines[i], 134, 003);
(*
!CIDADE                         !137!151! 15! - !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!
!UNIDADE DA FEDERACAO           !152!153!  2! - !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!
!TIPO DE INSCRICAO              !154!154!  1! - !NUMERICO     !30 !
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DE INSCRICAO            !155!169! 15! - !NUMERICO     !30 !
!-------------------------------!---!---!---!---!-------------!---!
!NOME DO SACADOR/AVALISTA       !170!209! 40! - !ALFANUMERICO !31 !
!-------------------------------!---!---!---!---!-------------!---!
!COD. BCO CORRESP. NA COMPENSACA!210!212!  3! - !NUMERICO     !32 !
!-------------------------------!---!---!---!---!-------------!---!
!NOSSO NUM. NO BCO CORRESPONDENT!213!232! 20! - !ALFANUMERICO !32 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !233!240!  8! - !BRANCOS      !   !
*-----------------------------------------------------------------*
*)
              EdEmiCidDetQ.Text := Copy(Memo1.Lines[i], 137, 015);
              EdEmi_UFDetQ.Text := Copy(Memo1.Lines[i], 152, 002);
              EdEmiCPFDetQ.Text := Geral.FormataCNPJ_TT(Copy(Memo1.Lines[i], 155, 015));
              EdEmiSacDetQ.Text := Copy(Memo1.Lines[i], 170, 040);
              EdEmiBCCDetQ.Text := Copy(Memo1.Lines[i], 210, 003);
              EdEmiNNBDetQ.Text := Copy(Memo1.Lines[i], 213, 020);
              EdCNABLot011.Text := Copy(Memo1.Lines[i], 233, 008);
            end
(*
REMESSA

DETALHE  -  REGISTRO 3  -  SEGMENTO R   /OPCIONAL/
-----------------------------------------------------------------
*-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-!
!                               !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BANCO NA COMPENSACAO !  1!  3!  3! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!LOTE DE SERVICO                !  4!  7!  4! - !NUMERICO     ! 1 !
!-------------------------------!---!---!---!---!-------------!---!
!REGISTRO DETALHE               !  8!  8!  1! - ! 3           ! 2 !
!-------------------------------!---!---!---!---!-------------!---!
!N  SEQUENCIAL DO REG. NO LOTE  !  9! 13!  5! - !NUMERICO     !10 !
!-------------------------------!---!---!---!---!-------------!---!
!COD. SEGMENTO DO REG. DETALHE  ! 14! 14!  1! - !'R'          !11 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    ! 15! 15!  1! - !BRANCOS      !   !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DE MOVIMENTO            ! 16! 17!  2! - !NUMERICO     !20 !
!-------------------------------!---!---!---!---!-------------!---!
*)
            else if x = 'R' then
            begin
              EdBcoCodDetR.Text := Copy(Memo1.Lines[i], 001, 003);
              EdLotDetR.Text    := Copy(Memo1.Lines[i], 004, 004);
              EdSeqDetR.Text    := Copy(Memo1.Lines[i], 009, 005);
              EdCNABLot012.Text := Copy(Memo1.Lines[i], 015, 001);
              EdCodMovDetR.Text := Copy(Memo1.Lines[i], 016, 002);
(*
!CODIGO DO DESCONTO 2           ! 18! 18!  1! - !NUMERICO     !28 !
!-------------------------------!---!---!---!---!-------------!---!
!DATA DO DESCONTO 2             ! 19! 26!  8! - !DDMMAAAA     !   !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR/PERCENTUAL A SER CONCEDID! 27! 41! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO DESCONTO 3           ! 42! 42!  1! - !NUMERICO     !28 !
!-------------------------------!---!---!---!---!-------------!---!
!DATA DO DESCONTO 3             ! 43! 50!  8! - !DDMMAAAA     !   !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR/PERCENTUAL A SER CONCEDID! 51! 65! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
*)
              EdCodDesco2R.Text := Copy(Memo1.Lines[i], 018, 001);
              EdDatDesco2R.Text := MLAGeral.DataCBRTxt(Copy(Memo1.Lines[i], 019, 008));
              EdValPerDe2R.Text := MLAGeral.XFT(Copy(Memo1.Lines[i], 027, 015), 2, siPositivo);
              EdCodDesco3R.Text := Copy(Memo1.Lines[i], 042, 001);
              EdDatDesco3R.Text := MLAGeral.DataCBRTxt(Copy(Memo1.Lines[i], 043, 008));
              EdValPerDe3R.Text := MLAGeral.XFT(Copy(Memo1.Lines[i], 051, 015), 2, siPositivo);
(*
!CODIGO DA MULTA                ! 66! 66!  1! - !1-VALOR FIXO !   !
!                               !   !   !   !   !2-PERCENTUAL !   !
!-------------------------------!---!---!---!---!-------------!---!
!DATA DA MULTA                  ! 67! 74!  8! - !DDMMAAAA     !   !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR/PERCENTUAL A SER APLICADO! 75! 89! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!INFORMACAO DO BANCO AO SACADO  ! 90! 99! 10! - !ALFANUMERICO !33 !
!-------------------------------!---!---!---!---!-------------!---!
!MENSAGEM 3                     !100!139! 40! - !ALFANUMERICO !34 !
!-------------------------------!---!---!---!---!-------------!---!
!MENSAGEM 4                     !140!179! 40! - !ALFANUMERICO !34 !
!-------------------------------!---!---!---!---!-------------!---!
!COD. DO BANCO DA CONTA DO DEBIT!180!182!  3! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DA AGENCIA DO DEBITO    !183!186!  4! - !NUMERICO     !   !
*-----------------------------------------------------------------*
!CONTA CORRENTE/DV DO DEBITO    !187!199! 13! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGOS DE OCORRENCIA DO SACADO!200!207!  8! - !NUMERICO     !54 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !208!240! 33! - !BRANCOS      !   !
*-----------------------------------------------------------------*
*)
              EdCodiMultaR.Text := Copy(Memo1.Lines[i], 066, 001);
              EdDataMultaR.Text := MLAGeral.DataCBRTxt(Copy(Memo1.Lines[i], 067, 008));
              EdVaPeMultaR.Text := MLAGeral.XFT(Copy(Memo1.Lines[i], 075, 015), 2, siPositivo);
              EdInfBcoSacR.Text := Copy(Memo1.Lines[i], 090, 010);
              EdMensagem3R.Text := Copy(Memo1.Lines[i], 100, 040);
              EdMensagem4R.Text := Copy(Memo1.Lines[i], 140, 040);
              EdConDebBcoR.Text := Copy(Memo1.Lines[i], 180, 003);
              EdConDebAgeR.Text := Copy(Memo1.Lines[i], 183, 004);
              EdConDebCCoR.Text := Copy(Memo1.Lines[i], 187, 013);
              EdCodOcoSacR.Text := Copy(Memo1.Lines[i], 200, 008);
              EdCNABLot013.Text := Copy(Memo1.Lines[i], 208, 033);
            //end
            // N�o pode ser aqui pois � registro 5
(*
DETALHE  -  REGISTRO  5  -  SEGMENTO S  /OPCIONAL/
*-----------------------------------------------------------------*
*-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-!
!                               !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BANCO NA COMPENSACAO !  1!  3!  3! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!LOTE DE SERVICO                !  4!  7!  4! - !NUMERICO     ! 1 !
!-------------------------------!---!---!---!---!-------------!---!
!REGISTRO DETALHE               !  8!  8!  1! - ! 3           ! 2 !
!-------------------------------!---!---!---!---!-------------!---!
!N  SEQUENCIAL DO REG. NO LOTE  !  9! 13!  5! - !NUMERICO     !10 !
!-------------------------------!---!-------!---!-------------!---!
!COD. SEGMENTO DO REG. DETALHE  ! 14! 14!  1! - ! S           !11 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    ! 15! 15!  1! - !BRANCOS      !   !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DE MOVIMENTO            ! 16! 17!  2! - !NUMERICO     !20 !
*-----------------------------------------------------------------*
*)
            (*else if x = 'S' then
            begin
              EdBcoCodDetS.Text := Copy(Memo1.Lines[i], 001, 003);
              EdLotDetS.Text    := Copy(Memo1.Lines[i], 004, 004);
              EdSeqDetS.Text    := Copy(Memo1.Lines[i], 009, 005);
              EdCNABLot014.Text := Copy(Memo1.Lines[i], 015, 001);
              EdCodMovDetS.Text := Copy(Memo1.Lines[i], 016, 002);
              // Parei Aqui

              *)
(*
PARA TIPO DE IMPRESSAO 1 OU 2 :
*-----------------------------------------------------------------*
!IDENTIFICACAO DA IMPRESSAO     ! 18! 18!  1! - !NUMERICO     !35 !
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DA LINHA A SER IMPRESSA ! 19! 20!  2! - !NUMERICO     !36 !
!-------------------------------!---!---!---!---!-------------!---!
!MENSAGEM A SER IMPRESSA        ! 21!160!140! - !ALFANUMERICO !37 !
!-------------------------------!---!---!---!---!-------------!---!
!TIPO DO CARACTER A SER IMPRESSO!161!162!  2! - !NUMERICO     !47 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !163!240! 78! - !BRANCOS      !   !
*-----------------------------------------------------------------*





PARA TIPO DE IMPRESSAO 3 :
*-----------------------------------------------------------------*
!IDENTIFICACAO DA IMPRESSAO     ! 18! 18!  1! - !NUMERICO     !35 !
*-----------------------------------------------------------------*

*-----------------------------------------------------------------*
DETALHE  -  REGISTRO  5  -  SEGMENTO S  /OPCIONAL/          /CONT./
*-----------------------------------------------------------------*
*-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-!
!                               !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!MENSAGEM 5                     ! 19! 58! 40! - !ALFANUMERICO !34 !
!-------------------------------!---!---!---!---!-------------!---!
!MENSAGEM 6                     ! 59! 98! 40! - !ALFANUMERICO !34 !
!-------------------------------!---!---!---!---!-------------!---!
!MENSAGEM 7                     ! 99!138! 40! - !ALFANUMERICO !34 !
!-------------------------------!---!---!---!---!-------------!---!
!MENSAGEM 8                     !139!178! 40! - !ALFANUMERICO !34 !
!-------------------------------!---!---!---!---!-------------!---!
!MENSAGEM 9                     !179!218! 40! - !ALFANUMERICO !34 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !219!240! 22! - !BRANCOS      !   !
*-----------------------------------------------------------------*
*)
            end;
          end;
        end;
      end;
    end;
(*
RETORNO

DETALHE  -  REGISTRO 3  -  SEGMENTO T   /OBRIGATORIO/
*-----------------------------------------------------------------*
*-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-!
!                               !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BANCO NA COMPENSACAO !  1!  3!  3! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!LOTE DE SERVICO                !  4!  7!  4! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!REGISTRO DETALHE               !  8!  8!  1! - ! 3           !   !
!-------------------------------!---!---!---!---!-------------!---!
!N  SEQUENCIAL DO REG. NO LOTE  !  9! 13!  5! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!COD. SEGMENTO DO REG. DETALHE  ! 14! 14!  1! - ! T           !11 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    ! 15! 15!  1! - !BRANCOS      !   !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DE MOVIMENTO            ! 16! 17!  2! - !NUMERICO     !40 !
!-------------------------------!---!---!---!---!-------------!---!
!AGENCIA MANTENEDORA DA CONTA   ! 18! 22!  5! - !NUMERICO     ! 8 !
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA AGENCIA  ! 23! 23!  1! - !NUMERICO     ! 8 !
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DA CONTA CORRENTE       ! 24! 35! 12! - !NUMERICO     ! 8 !
*-------------------------------!---!---!---!---!-------------!---*
!DIGITO VERIFICADOR DA CONTA    ! 36! 36!  1! - !NUMERICO     ! 8 !
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA AG/CONTA ! 37! 37!  1! - !NUMERICO     ! 8 !
!-----------------------------------------------------------------!*)
    if EdOperacao.Text = 'T' then
    begin
      Envio := 2;//Retorno
      for i := Linha to LinhaF do
      begin
        Application.ProcessMessages;
        c := Geral.IMV(Copy(Memo1.Lines[i], 008, 001));
        x := Copy(Memo1.Lines[i], 014, 001);
        //ShowMessage(IntToStr(c));
        case c of
          3:
          begin
            if x = 'T' then
            begin
              EdBcoCodDetT.Text := Copy(Memo1.Lines[i], 001, 003);
              EdLotDetT.Text    := Copy(Memo1.Lines[i], 004, 004);
              EdSeqDetT.Text    := Copy(Memo1.Lines[i], 009, 005);
              EdCNABLot3.Text   := Copy(Memo1.Lines[i], 015, 001);
              EdCodMovDetT.Text := Copy(Memo1.Lines[i], 016, 002);
              EdAgenciaNUT.Text := Copy(Memo1.Lines[i], 018, 005);
              EdAgenciaDVT.Text := Copy(Memo1.Lines[i], 023, 001);
              EdContaNUT.Text   := Copy(Memo1.Lines[i], 024, 012);
              EdContaDVT.Text   := Copy(Memo1.Lines[i], 036, 001);
              EdAgenCcDVT.Text  := Copy(Memo1.Lines[i], 037, 001);
(*
!IDENTIFICACAO DO TITULO NO BANC! 38! 57! 20! - !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DA CARTEIRA             ! 58! 58!  1! _ !NUMERICO     !22 !
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DO DOCUMENTO DE COBRANCA! 59! 73! 15! _ !ALFANUMERICO !24 !
!-------------------------------!---!---!---!---!-------------!---!
!DATA DO VENCIMENTO DO TITULO   ! 74! 81!  8! _ !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR NOMINAL DO TITULO        ! 82! 96! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DO BANCO                ! 97! 99!  3! - !NUMERICO     !43 !
!-------------------------------!---!---!---!---!-------------!---!
!AGENCIA COBRADORA/RECEBEDORA   !100!104!  5! - !NUMERICO     !43 !
!-------------------------------!---!---!---!---!-------------!---!
!DIGITO VERIFICADOR DA AGENCIA  !105!105!  1! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!IDENTIF. DO TITULO NA EMPRESA  !106!130! 25! - !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!*)
              EdIdeTitDetT.Text  := Copy(Memo1.Lines[i], 038, 020);
              EdCodCarDetT.Text  := Copy(Memo1.Lines[i], 058, 001);
              EdDocCobDetT.Text  := Copy(Memo1.Lines[i], 059, 015);
              EdVenTitDetT.Text  := MLAGeral.DataCBRTxt(Copy(Memo1.Lines[i], 074, 008));
              EdValNomDetT.Text  := MLAGeral.XFT(Copy(Memo1.Lines[i], 082, 015), 2, siPositivo);
              EdBcoCodDetT2.Text := Copy(Memo1.Lines[i], 097, 003);
              EdAgenciaNUT2.Text := Copy(Memo1.Lines[i], 100, 005);
              EdAgenciaDVT2.Text := Copy(Memo1.Lines[i], 105, 001);
              EdIDTiEDetT.Text   := Copy(Memo1.Lines[i], 106, 025);
(*
!CODIGO DA MOEDA                !131!132!  2!   !NUMERICO     !29 !
!-------------------------------!---!---!---!---!-------------!---!
!TIPO DE INSCRICAO              !133!133!  1! - !NUMERICO     !30 !
!-------------------------------!---!---!---!---!-------------!---!
!NUMERO DE INSCRICAO            !134!148! 15! - !NUMERICO     !30 !
!-------------------------------!---!---!---!---!-------------!---!
!NOME                           !149!188! 40! - !ALFANUMERICO !   !
!-------------------------------!---!---!---!---!-------------!---!
!N. DO CONTR. DA OPERACAO D CRED!189!198! 10! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR DA TARIFA/CUSTAS         !199!213! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!IDENTIFICACAO PARA REJEICOES,  !214!223! 10! - !NUMERICO     !42 !
!TARIFAS, CUSTAS, LIQUID. BAIXAS!   !   !   !   !             !   !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !224!240! 17! - !NUMERICO     !55 !
*-----------------------------------------------------------------*
*)
              EdCodMoeDetT.Text := Copy(Memo1.Lines[i], 131, 002);
              EdTipInsDetT.Text := Copy(Memo1.Lines[i], 133, 001);
              EdNumInsDetT.Text := Copy(Memo1.Lines[i], 134, 015);
              EdNomeDetT.Text   := Copy(Memo1.Lines[i], 149, 040);
              EdNConOCDetT.Text := Copy(Memo1.Lines[i], 189, 010);
              EdTarCusDetT.Text := MLAGeral.XFT(Copy(Memo1.Lines[i], 199, 015), 2, siPositivo);
              EdIdRTCLDetT.Text := Copy(Memo1.Lines[i], 214, 010);
              EdCNABLot4.Text   := Copy(Memo1.Lines[i], 224, 017);
            end else if x = 'U' then
            begin
(*RETORNO

DETALHE  -  REGISTRO 3  -  SEGMENTO U  /OBRIGATORIO/
*-----------------------------------------------------------------*
*-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-!
!                               !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BANCO NA COMPENSACAO !  1!  3!  3! - !NUMERICO     !   !
*-----------------------------------------------------------------*
!LOTE DE SERVICO                !  4!  7!  4! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!REGISTRO DETALHE               !  8!  8!  1! - ! 3           !   !
!-------------------------------!---!---!---!---!-------------!---!
!N  SEQUENCIAL DO REG. NO LOTE  !  9! 13!  5! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!COD. SEGMENTO DO REG. DETALHE  ! 14! 14!  1! - ! U           !11 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    ! 15! 15!  1! - !BRANCOS      !   !
!-------------------------------!---!---!---!---!-------------!---!
*)
              EdBcoCodDetU.Text := Copy(Memo1.Lines[i], 001, 003);
              EdLotDetU.Text    := Copy(Memo1.Lines[i], 004, 004);
              EdSeqDetU.Text    := Copy(Memo1.Lines[i], 009, 005);
              EdCNABLot5.Text   := Copy(Memo1.Lines[i], 015, 001);
(*
!CODIGO DE MOVIMENTO            ! 16! 17!  2! - !NUMERICO     !40 !
!-------------------------------!---!---!---!---!-------------!---!
!JUROS/MULTA/ENCARGOS           ! 18! 32! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR DO DESCONTO CONCEDIDO    ! 33! 47! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR DO ABAT. CONCEDIDO/CANCEL! 48! 62! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR DO IOF RECOLHIDO         ! 63! 77! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR PAGO PELO SACADO         ! 78! 92! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR LIQUIDO A SER CREDITADO  ! 93!107! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR DE OUTRAS DESPESAS       !108!122! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR DE OUTROS CREDITOS       !123!137! 13! 2 !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
*)
              EdCodMovDetU.Text := Copy(Memo1.Lines[i], 016, 002);
              EdValJMEDetU.Text := MLAGeral.XFT(Copy(Memo1.Lines[i], 018, 015), 2, siPositivo);
              EdValDesDetU.Text := MLAGeral.XFT(Copy(Memo1.Lines[i], 033, 015), 2, siPositivo);
              EdValAbaDetU.Text := MLAGeral.XFT(Copy(Memo1.Lines[i], 048, 015), 2, siPositivo);
              EdValIOFDetU.Text := MLAGeral.XFT(Copy(Memo1.Lines[i], 063, 015), 2, siPositivo);
              EdValPagDetU.Text := MLAGeral.XFT(Copy(Memo1.Lines[i], 078, 015), 2, siPositivo);
              EdValLiqDetU.Text := MLAGeral.XFT(Copy(Memo1.Lines[i], 093, 015), 2, siPositivo);
              EdValOuDDetU.Text := MLAGeral.XFT(Copy(Memo1.Lines[i], 108, 015), 2, siPositivo);
              EdValOuCDetU.Text := MLAGeral.XFT(Copy(Memo1.Lines[i], 123, 015), 2, siPositivo);
(*
!DATA DA OCORRENCIA             !138!145!  8! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!DATA DA EFETIVACAO DO CREDITO  !146!153!  8! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DA OCORRENCIA DO SACADO !154!157!  4! - !ALFANUMERICO !44 !
!-------------------------------!---!---!---!---!-------------!---!
!DATA DA OCORRENCIA DO SACADO   !158!165!  8! - !ALFANUMERICO !44 !
!-------------------------------!---!---!---!---!-------------!---!
!VALOR DA OCORRENCIA DO SACADO  !166!180! 13! 2 !NUMERICO     !44 !
!-------------------------------!---!---!---!---!-------------!---!
!COMPLEM.DA OCORRENCIA DO SACADO!181!210! 30! - !ALFANUMERICO !44 !
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BCO CORRESP. COMPENS.!211!213!  3! - !NUMERICO     !32 !
!-------------------------------!---!---!---!---!-------------!---!
!NOSSO NUM. BCO CORRESPONDENTE  !214!233! 20! - !NUMERICO     !32 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !234!240!  7! - !BRANCOS      !   !
*-----------------------------------------------------------------*
*)
              EdDatOcoDetU.Text := MLAGeral.DataCBRTxt(Copy(Memo1.Lines[i], 138, 008));
              EdDatCreDetU.Text := MLAGeral.DataCBRTxt(Copy(Memo1.Lines[i], 146, 008));
              EdCodOcoDetU.Text := Copy(Memo1.Lines[i], 154, 004);
              EdDatOcSDetU.Text := MLAGeral.DataCBRTxt(Copy(Memo1.Lines[i], 158, 008));
              EdValOcSDetU.Text := MLAGeral.XFT(Copy(Memo1.Lines[i], 166, 015), 2, siPositivo);
              EdComOcSDetU.Text := Copy(Memo1.Lines[i], 181, 030);
              EdCBcCoCDetU.Text := Copy(Memo1.Lines[i], 211, 003);
              EdNBcCoCDetU.Text := Copy(Memo1.Lines[i], 211, 003);
              EdNNBCoCDetU.Text := Copy(Memo1.Lines[i], 214, 020);
              EdCNABLot6.Text   := Copy(Memo1.Lines[i], 234, 007);
              //
              Controle := Geral.IMV(EdLoiControle.Text);
              Lote     := Geral.IMV(EdLotDetU.Text);
              Item     := Geral.IMV(EdSeqDetU.Text);
              DataOcor := Geral.FDT(Geral.ValidaDataSimples(EdDatOcoDetU.Text, True), 1);
              Movimento:= Geral.IMV(EdCodMovDetU.Text);
              IRTCLB   := Geral.IMV(EdIdRTCLDetT.Text);
              Custas   := Geral.DMV(EdTarCusDetT.Text);
              ValPago  := Geral.DMV(EdValPagDetU.Text);
              ValCred  := Geral.DMV(EdValLiqDetU.Text);
              Sequencia:= Geral.IMV(EdSeqArq.Text);
              //
              if InsereNoLote > 0 then
              begin
                QrDupl.Close;
                QrDupl.Params[00].AsInteger := Controle;
                QrDupl.Params[01].AsInteger := Lote;
                QrDupl.Params[02].AsInteger := Item;
                QrDupl.Params[03].AsString  := DataOcor;
                QrDupl.Params[04].AsInteger := Envio;
                QrDupl.Params[05].AsInteger := Movimento;
                QrDupl.Open;
                if QrDupl.RecordCount = 0 then Continua := ID_YES else
                begin
                  Continua :=  Application.MessageBox(PChar('O item '+IntToStr(Item)+
                  ' do lote '+IntToStr(Lote)+' deste arquivo retorno (Controle '+
                  IntToStr(Controle)+' do Border� '+IntToStr(QrLotesItsLote.Value)+
                  ' do lote creditor '+IntToStr(QrLotesItsCodigo.Value)+
                  ') j� foi registrado. Deseja registr�-lo assim mesmo?'), 'Pergunta',
                  MB_YESNOCANCEL+MB_ICONQUESTION);
                end;
                case Continua of
                  ID_YES:
                  begin
                    CNAB240I := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                      'CNAB240', 'CNAB240I', 'CNAB240I');
                    Dmod.QrUpd.Params[00].AsInteger := Controle;
                    Dmod.QrUpd.Params[01].AsInteger := Geral.IMV(EdBcoCodDetU.Text);
                    Dmod.QrUpd.Params[02].AsInteger := 0; // Parei Aqui nao usado ainda
                    Dmod.QrUpd.Params[03].AsInteger := Lote;
                    Dmod.QrUpd.Params[04].AsInteger := Item;
                    Dmod.QrUpd.Params[05].AsString  := DataOcor;
                    Dmod.QrUpd.Params[06].AsInteger := Envio; // 2 - Retorno
                    Dmod.QrUpd.Params[07].AsInteger := Movimento;
                    Dmod.QrUpd.Params[08].AsInteger := IRTCLB;
                    Dmod.QrUpd.Params[09].AsFloat   := Custas;
                    Dmod.QrUpd.Params[10].AsFloat   := ValPago;
                    Dmod.QrUpd.Params[11].AsFloat   := ValCred;
                    Dmod.QrUpd.Params[12].AsInteger := Sequencia;
                    Dmod.QrUpd.Params[13].AsString  := EdConvenioL.Text;
                    //
                    Dmod.QrUpd.Params[14].AsInteger := InsereNoLote;
                    Dmod.QrUpd.Params[15].AsInteger := CNAB240I;
                    Dmod.QrUpd.ExecSQL;
                  end;
                  ID_CANCEL: Exit;
                end;
              end;
            end else
            begin
              Application.MessageBox(PChar('C�digo de segmento do registro de '+
              'detalhe desconhecido: '+x), 'Erro', MB_OK+MB_ICONERROR);
            end;
          end;
          //ver Tipo de registro desconhecido: 0 -> Quando n�o tem lote
          else Application.MessageBox(PChar('Tipo de registro desconhecido: '+
          IntToStr(c)+'!'), 'Erro', MB_OK+MB_ICONERROR);
        end;
      end;
    end;
  end;
  //
  (*if FArquivo > 0 then
  begin
    Application.MessageBox(PChar('Criado lote de arquivo retorno: '+
    IntToStr(FArquivo)+'.'), 'Mensagem', MB_OK+MB_ICONINFORMATION);
    QrCNAB240.Params[0].AsInteger := FArquivo;
    QrCNAB240.Open;
    PageControl1.ActivePageIndex := 0;
  end;*)
  LaRegistroB.Caption := FormatFloat('00000', Registro) + '/' +
    FormatFloat('00000', Geral.IMV(GradeB.Cells[0, GradeB.RowCount-1]));
end;

procedure TFmRetornoBB.LeTrailerArquivo;
var
  f: Integer;
begin
  f := Memo1.Lines.Count - 1;
(*
Livro: 095     - Processamento de Serv. e Informa��es por Meio Eletronico
Cap�tulo: 0002   - Arquivos - Remessa e Retorno
T�tulo: 00016 - CNAB240 - Registro Trailler de Arquivo
Subt�tulo: 8020   - Leiaute
Vers�o: 0001

TRAILER DE ARQUIVO  -  REGISTRO  9
 *----------------------------------------------------------------*
 *-----------------------------------------------------------------*
!CAMPO                          !POSICAO! N !N  ! CONTEUDO    !NO-! !
                                !DE !ATE!DIG!DEC!             !TAS!
!-------------------------------!---!---!---!---!-------------!---!
!CODIGO DO BANCO NA COMPENSACAO !  1!  3!  3! - !NUMERICO     !   !
!-------------------------------!---!---!---!---!-------------!---!
!LOTE DE SERVICO                !  4!  7!  4! - ! 9999        ! 1 !
!-------------------------------!---!---!---!---!-------------!---!
!REGISTRO TRAILER DE ARQUIVO    !  8!  8!  1! - ! 9           ! 2 !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    !  9! 17!  9! - !BRANCOS      !   !
!-------------------------------!---!---!---!---!-------------!---!
*)
            //Parei Aqui
  Memo2.Lines.Add(Copy(Memo1.Lines[f], 009, 009));


(*
!QUANTID. DE LOTES DO ARQUIVO   ! 18! 23!  6! - !NUM. REGIST. !   ! !
                                !   !   !   !   !TIPO - 1     !   !
!-------------------------------!---!---!---!---!-------------!---!
!QUANTID. DE REGISTROS DO ARQUIV! 24! 29!  6! - !NUM.REG.TIPOS!   ! !
                                !   !   !   !   ! 0+1+3+5+9   !   !
!-------------------------------!---!---!---!---!-------------!---!
!QTDADE DE CONTAS P/CONC.- LOTES! 30! 35!  6! - !NUM.REG.     !   ! !
                                !   !   !   !   !TIPO-1 OPER-E! E !
!-------------------------------!---!---!---!---!-------------!---!
!USO EXCLUSIVO FEBRABAN/CNAB    ! 36!240!205! - !BRANCOS      !   !
*-----------------------------------------------------------------*
*)
  EdArqQtdLotes    .Text := Copy(Memo1.Lines[f], 018, 006);
  EdArqQtdRegistros.Text := Copy(Memo1.Lines[f], 024, 006);
  EdArqQtdcontas   .Text := Copy(Memo1.Lines[f], 030, 006);
  Memo2.Lines.Add(Copy(Memo1.Lines[f], 036, 205));
end;

(*
Livro: 095     - Processamento de Serv. e Informa��es por Meio Eletronico
Cap�tulo: 0002   - Arquivos - Remessa e Retorno
T�tulo: 00017 - CNAB240 - Notas e Informa��es Complementares
Subt�tulo: 9993   - Quadros e Tabelas
Vers�o: 0004

*-----------------------------------------------------------------*
!    NOTAS        !   INFORMACOES COMPLEMENTARES                  !
!-----------------------------------------------------------------!
!/B/              !Especificacoes do Codigo de Barras do Bloqueto !
!CODIGO DE BARRAS !de Cobranca-Ficha de  Compensacao /Modelo CADOC!
!                 !24044-4, Carta Circular N  2414, de 07.10.93,  !
!                 !do Banco Central do Brasil/.                   !
!                 !Alteracao nas  especificacoes do Codigo  de    !
!                 !Barras - Introducao  de Digito de Auto Confe-  !
!                 !rencia /Carta Circular N  2531, de 24.02.95/.  !
!-----------------!-----------------------------------------------!
!/E/              !Campo especifico para o servico Conciliacao    !
!                 !Bancaria.                                      !
!-----------------!-----------------------------------------------!
!/R/              !Campos a serem preenchidos quando o arquivo eh !
!                 !Retorno /Codigo - 2 no Header de Arquivo/      !
!-----------------------------------------------------------------!
!/01/             !Identifica um Lote de Servico.                 !
!LOTE             !Sequencial e nao deve ser repetido dentro do   !   
!                 !arquivo.                                       !
!                 !As numeracoes 0000 e 9999 sao exclusivas para  !
!                 !o Header e Trailer do Arquivo respectivamente. !   
!-----------------!-----------------------------------------------!   
!/02/             !Identifica o tipo do Registro. Ver 4.0         !   
!REGISTRO         !Composicao do Arquivo                          !
!-----------------!-----------------------------------------------!   
!/03/             !Indica a operacao que devera ser realizada com !
!TIPO DE OPERACAO !os registros Detalhe do Lote.                  !
!                 !Deve constar apenas um tipo por Lote:          !
!                 ! C - Lancamento a Credito                      !
!                 ! D - Lancamento a Debito                       !
!                 ! E - Extrato para Conciliacao                  !
!                 ! I - Informacoes de titulos capturados do      !
!                 !     proprio banco                             !
!                 ! R - Arquivo Remessa                           !
!                 ! T - Arquivo Retorno.                          !
!-----------------!-----------------------------------------------!
!/4/              !Indica o tipo de servico que o lote contem :   !
!TIPO DE SERVICO  ! 01 - Cobranca                                 !   
!                 ! 02 - Cobranca sem Papel                       !   
!                 ! 03 - Bloqueto Eletronico                      !   
!                 ! 04 - Conciliacao Bancaria                     !   
!                 ! 05 - Debitos                                  !   
!                 ! 10 - Pagamento Dividendos                     !
!                 ! 20 - Pagamento Fornecedor                     !   
!                 ! 30 - Pagamento Salarios                       !
!                 ! 50 - Pagamento Sinistros Segurados            !   
!                 ! 60 - Pagamento Despesas Viajante em Transito  !   
*-----------------------------------------------------------------*   
                                                                      
                                                                      
                                                                      
                                                                      
*-----------------------------------------------------------------*
!    NOTAS        !   INFORMACOES COMPLEMENTARES                  !
!-----------------------------------------------------------------!
!/4/              !Indica o tipo de servico que o lote contem :   !
!TIPO DE SERVICO  ! 70 - Pagamento Autorizado                     !
!     /CONT./     ! 75 - Pagamento Credenciados                   !
!                 ! 80 - Pagamento Representantes/Vendedores      !
!                 !      Autorizados                              !
!                 ! 90 - Pagamento Beneficios                     !
!                 ! 98 - Pagamento Diversos.                      !
!-----------------!-----------------------------------------------!
!/5/              !Indica a forma de lancamento que o lote contem:!
!FORMA DE LANCA-  ! 01 - Credito em Conta Corrente                !
!MENTO            ! 02 - Cheque Pagamento/Administrativo          !
!                 ! 03 - DOC/TED                                  !
!                 ! 04 - Cartao Salario /Servico Tipo 30/         !
!                 ! 05 - Credito em Conta Poupanca                !
!                 ! 10 - OP � disposi��o                          !
!                 ! 30 - Liquidacao de Titulos do proprio Banco   !
!                 ! 31 - Pagamento de Titulos de outros Bancos    !
!-----------------!-----------------------------------------------!
!/6/              !Identificacao da Empresa no Banco.             !
!EMPRESA          !Inscricao: Tipo e Numero /CPF ou CGC/, e/ou.   !
!                 !Convenio: Codigo do convenio Empresa/Banco,    !
!                 !          e/ou Conta Corrente                  !
!                 !Obs.: No registro header de arquivo pode ser da!
!                 !empresa "mae"do grupo ou da matriz. a identifi-!
!                 !cacao da  empresa  no registro header de lote  !
!                 !pode ser por empresa coligada ou por filial. a !
!                 !identificacao pode se repetir quando for unica !
!-----------------!-----------------------------------------------!
!/7/              !Identifica a Empresa no Banco para determinados!
!CONVENIO         !tipos de servicos. Observar as regras de preen-!   
!                 !chimento abaixo no que se refere ao header  de !   
!                 !servico/lote:                                  !   
!                 !"9999999994444CCVVV  " /20 bytes/, onde:       !   
!                 !                                               !
!                 !999999999 - codigo do convenio,numerico,alinha-!   
!                 !            do a direita,preenchido com zeros a!   
!                 !            esquerda.                          !   
!                 !            Para servicos de pagamentos PGT  e !   
!                 !            cobranca cedente eh obrigatorio    !   
!                 !            Para servicos de BBCHEQUE e cobran-!   
!                 !            ca sacada sera utilizado o numero  !   
!                 !            do contrato respectivo.            !   
!                 !                                               !   
!                 !4444      - codigo do produto:                 !   
*-----------------------------------------------------------------*   
*-----------------------------------------------------------------*   
!    NOTAS        !   INFORMACOES COMPLEMENTARES                  !   
!-----------------------------------------------------------------!
!/7/              !            -cobranca cedente - 0014           !
!CONVENIO /CONT./ !            -cobranca sacada  - 0019           !   
!                 !            -pagamento fornecedores-PGT - 0126 !
!                 !            -captura de cheques - 0024         !   
!                 !CC        - carteira de cobranca para o caso de!   
!                 !            cobranca cedente - obrigatorio     !
!                 !VVV       - variacao da carteira de cobranca   !   
!                 !            para o caso de cobranca cedente -  !
!                 !            obrigatorio.                       !
!                 !                                               !
!                 !As duas ultimas posicoes deixar em branco.     !   
!                 !Para usuarios do sistema SAP R.3, informar os  !   
!                 !campos acima apenas nas remessas de cobranca   !   
!                 !cedente. Nos lotes de Credito em conta, DOC  ou!   
!                 !pagamento eletronico de titulos, deixar obriga-!   
!                 !toriamente em branco                           !   
!-----------------!-----------------------------------------------!   
!/8/              !Codigo da Agencia mantenedora da Conta e seu DV!   
!CONTA CORRENTE   !Numero da Conta e seu DV.                      !   
!                 !DV dos campos Agencia e Conta.                 !   
!                 !Obs.: Para os bancos que utilizam duas posicoes!   
!                 !para o DV /digito verificador/, preencher a 1  !   
!                 !posicao no campo DV da conta e a 2  posicao no !
!                 !campo DV da agencia/conta.                     !
!-----------------!-----------------------------------------------!   
!/9/              !Identifica o N  da Versao do Layout do Arquivo,!
!LEIAUTE          !composto de : Versao  - 2 digitos              !   
!                 !              Release - 1 digito               !   
!                 !Obs.: Somente podera ser modificado quando     !   
!                 !houver alteracoes no header/trailer de arquivo !   
!                 !e desde que aprovadas  pelo cnab.              !   
!-----------------!-----------------------------------------------!
!/10/             !Numero de sequencia do registro no lote inicia-!   
!N  DO REGISTRO   !do sempre em 1.                                !   
!DETALHE          !                                               !   
!-----------------!-----------------------------------------------!   
!/11/             !Ver - COMPOSICAO DO ARQUIVO.                   !
!COD. DE SEGMENTO !                                               !
!-----------------!-----------------------------------------------!
!/12/             !TIPO: Indica o tipo de movimentacao que se     !   
!MOVIMENTO        !destina :                                      !   
!                 !  0 - Indica INCLUSAO                          !   
!                 !  3 -   "    ESTORNO                           !   
!                 !  5 -   "    ALTERACAO                         !   
!                 !  9 -   "    EXCLUSAO.                         !   
!                 !CODIGO: Indica a movimentacao a ser efetuada,  !
!                 !conforme tabela:                               !
!                 !  00 - Inclusao de Registro Detalhe Liberado   !   
!                 !  09 - Inclusao do Registro Detalhe Bloqueado  !
*-----------------------------------------------------------------*   
*-----------------------------------------------------------------*   
!    NOTAS        !   INFORMACOES COMPLEMENTARES                  !   
!-----------------------------------------------------------------!   
!/12/             !  10 - Alteracao  do  Pagamento  Liberado  para!   
!MOVIMENTO /CONT./!       Bloqueado /Bloqueio/                    !
!                 !  11 - Alteracao  do  Pagamento  Bloqueado para!   
!                 !       liberado /Liberacao/                    !   
!                 !  17 - Alteracao do Valor do Titulo            !   
!                 !  19 - Alteracao da Data de Pagamento          !   
!                 !  23 - Pagamento Direto ao Fornecedor - Baixar !   
!                 !  25 - Manutencao em Carteira - Nao Pagar      !   
!                 !  27 - Retirada de Carteira - Nao Pagar        !   
!                 !  33 - Estorno por Devolucao da Camara  Centra-!   
!                 !       lizadora /somente para Movimento do tipo!   
!                 !       3/                                      !   
!                 !  40 - Alegacao do Sacado                      !
!                 !  99 - Exclusao do registro detalhe incluido   !   
!                 !       anteriormente                           !
!-----------------!-----------------------------------------------!
!/13/             !BTN - Bonus do Tesouro Nacional + TR           !
!MOEDA            !BRL - Real                                     !   
!                 !USD - Dolar Americano                          !
!                 !PTE - Escudo Portugues                         !   
!                 !FRF - Franco Frances                           !   
!                 !CHF - Franco Suico                             !   
!                 !JPY - Ien Japones                              !   
!                 !IGP - Indice Geral de Precos                   !   
!-----------------!-----------------------------------------------!
!/13/             !IGM - Indice Geral de Precos de Mercado        !   
!MOEDA            !GBP - Libra Esterlina                          !   
!/cont./          !ITL - Lira Italiana                            !   
!                 !DEM - Marco Alemao                             !   
!                 !TRD - Taxa Referencial Diaria                  !   
!                 !UPC - Unidade Padrao de Capital                !   
!                 !UPF - Unidade Padrao de Financiamento          !   
!                 !UFR - Unidade Fiscal de Referencia             !   
!                 !XEU - Unidade Monetaria Europeia.              !   
!                 !Obs.: Moeda baseada em tabela padrao S.W.I.F.T.!   
!                 !acrescida dos principais indices nacionais.    !   
!-----------------!-----------------------------------------------!   
!/14/             !Campo utilizado no Retorno, para informacao das!   
!OCORRENCIAS      !ocorrencias detectadas no processamento do ar- !
!                 !quivo Remessa, enviado pela empresa. Pode-se in!
!                 !formar ate 5 ocorrencias simultaneamente, cada !   
!                 !uma codificada com dois digitos, cfe abaixo:   !
!                 !Codigos de Ocorrencia :                        !   
!                 ! 00 - Credito ou Debito Efetuado               !
!                 ! 01 - Insuficiencia de Fundos - Debito Nao     !   
!                 !      Efetuado                                 !   
!                 ! 02 - Credito ou Debito Cancelado pelo         !   
!                 !      Pagador/Credor                           !
!                 ! 03 - Debito Autorizado pela Agencia - Efetuado!
*-----------------------------------------------------------------*   
*-----------------------------------------------------------------*   
!    NOTAS        !   INFORMACOES COMPLEMENTARES                  !   
!-----------------------------------------------------------------!   
!/14/             ! HA - Lote Nao Aceito                          !   
!OCORRENCIAS      ! HB - Inscricao da Empresa Invalida para o     !   
!    /CONT./      !      Contrato                                 !   
!                 ! HC - Convenio com a Empresa Inexistente/      !   
!                 !      Invalido para o Contrato                 !   
!                 ! HD - Agencia/Conta da Empresa Inexistente/    !   
!                 !      Invalida para o Contrato                 !   
!                 ! HE - Tipo de Servico Invalido para o Contrato !   
!                 ! HF - Conta Corrente da Empresa com Saldo      !
!                 !      Insuficiente                             !
!                 ! HG - Lote de servico fora de sequencia        !   
!                 ! HH - Lote de servico invalido                 !
!                 ! AA - Controle Invalido                        !   
!                 ! AB - Tipo de Operacao Invalido                !   
!                 ! AC - Tipo de Servico Invalido                 !   
!                 ! AD - Forma de Lancamento Invalida             !   
!                 ! AE - Tipo/Numero de Inscricao Invalido        !   
!                 ! AF - Codigo de Convenio Invalido              !
!                 ! AG - Agencia/Conta Corrente/DV Invalido       !   
!                 ! AH - N  Sequencial do Registro no Lote        !
!                 !      Invalido                                 !   
!                 ! AI - Codigo de Segmento de Detalhe Invalido   !   
!                 ! AJ - Tipo de Movimento Invalido               !   
!                 ! AK - Codigo da Camara de Compensacao do Banco !   
!                 !      do Favorecido Depositario Invalido       !   
!                 ! AL - Codigo do Banco do Favorecido ou         !   
!                 !      Depositario Invalido                     !   
!                 ! AM - Agencia Mantenedora da Conta C/C do      !
!                 !      Favorecido Invalida                      !   
!                 ! AN - Conta Corrente/DV do Favorecido Invalido !   
!                 ! AO - Nome do Favorecido nao Informado         !   
!                 ! AP - Data Lancamento Invalida                 !
!                 ! AQ - Tipo/Quantidade da Moeda Invalido        !
!                 ! AR - Valor do Lancamento Invalido             !   
!                 ! AS - Aviso ao Favorecido - Identificacao      !
!                 !      Invalida                                 !   
!                 ! AT - Tipo/NUmero de Inscricao do Favorecido   !   
!                 !      Invalido                                 !   
!                 ! AU - Logradouro do Favorecido nao Informado   !   
!                 ! AV - N  do Local do Favorecido nao Informado  !   
!                 ! AW - Cidade do Favorecido nao Informada       !
!                 ! AX - CEP/Complemento do Favorecido Invalido   !   
!                 ! AY - Sigla do Estado do Favorecido Invalida   !   
!                 ! AZ - Codigo/Nome do Banco Depositario Invalido!   
!                 ! BA - Codigo/Nome da Agencia Depositaria nao   !   
!                 !      Informado                                !   
!                 ! BB - Seu NUmero Invalido                      !
!                 ! BC - Nosso NUmero Invalido                    !   
*-----------------------------------------------------------------*
                                                                      
*-----------------------------------------------------------------*   
!    NOTAS        !   INFORMACOES COMPLEMENTARES                  !   
!-----------------------------------------------------------------!   
!/14/             ! BD - Confirmacao de Pagamento Agendado        !   
!OCORRENCIAS      ! BE - CPF/CGC do creditado nao correspnde a    !
!/CONT./          !      agencia/conta informadas                 !
!                 ! CA - Codigo de barras - codigo do banco       !   
!                 !      invalido                                 !
!                 ! CB - Codigo de barras - codigo da moeda       !   
!                 !      invalido                                 !
!                 ! CC - Codigo de barras - digito verificador    !   
!                 !      geral invalido                           !   
!                 ! CD - Codigo de barras - valor do titulo       !   
!                 !      invalido                                 !
!                 ! CE - Codigo de barras - campo livre invalido  !   
!                 ! CF - Valor do documento invalido              !   
!                 ! CG - Valor do abatimento invalido             !   
!                 ! CH - Valor do desconto invalido               !   
!                 ! CI - Valor de mora invalido                   !   
!                 ! CJ - Valor da multa invalido                  !   
!                 ! CK - Valor do IR invalido                     !   
!                 ! CL - Valor do ISS invalido                    !   
!                 ! CM - Valor do IOF invalido                    !   
!                 ! CN - Valor de outras deducoes invalido        !   
!                 ! CO - Valor de outros acrescimos invalido      !   
!                 ! TA - Lote nao Aceito - Totais do Lote com     !
!                 !      Diferenca.                               !   
!-----------------!-----------------------------------------------!
!/15/             !DEBITOS :                                      !
!CATEGORIA  DO    !  101 - Cheques                                !   
!LANCAMENTO       !  102 - Encargos                               !
!                 !  103 - Estornos                               !   
!                 !  104 - Lancamento Avisado                     !   
!                 !  105 - Tarifas.                               !   
!                 !CREDITOS :                                     !   
!                 !  201 - Depositos                              !   
!                 !  202 - Liquido de Cobranca                    !
!                 !  203 - Devolucao de Cheques                   !   
!                 !  204 - Estornos                               !   
!                 !  205 - Lancamento Avisado.                    !   
!-----------------!-----------------------------------------------!   
!/16/             !Campo de livre uso da empresa para o envio de  !
!INFORMACAO       !mensagem que devera constar nos  avisos e/ou   !   
!                 !nos documentos a serem emitidos.               !   
!                 !Informacao 1 - Header de Lote. Gen�rica. Quando!   
!                 !informada constar� em todos os avisos e/ou     !   
!                 !documentos originados dos detalhes desse lote. !   
!                 !                                               !   
!                 !Informacao 2 - Segmento A. Especifica. Quando  !   
!                 !informada constar� apenas naquele aviso ou     !   
!                 !documento identificado pelo detalhe.           !
!                 !Para "Forma de Lan�amento" 10-OP � Disposi��o  !
!                 !preencher os campos 178 a 199 com o n�mero do  !   
!                 !CPF do(s) preposto(s), da seguinte forma:      !
!                 !Posi��es              Cont�udo                 !
!                 !178 a 188             CPF do primeiro preposto !   
!                 !189 a 199             CPF do segundo preposto  !
!                 !OBS: As posi��es 178 a 199 dever�o ser preen-  !   
!                 !chidas com zeros quando n�o houver a indica��o !   
!                 !de prepostos.                                  !
!                 !                                               !   
!-----------------------------------------------------------------!   
!/17/             !Evoluir um numero sequencial a cada Header de  !   
!SEQUENCIA        !arquivo.                                       !   
!-----------------!-----------------------------------------------!   
!/18/             !Preencher com o codigo da Camara Centralizadora!   
!CAMARA CENTRALI- !para transferencia do recurso  via  TED ou DOC,!   
!ZADORA           !observando os seguintes dominios..             !   
!                 !*018* = TED                                    !   
!                 !*700* = DOC /COMPE/                            !   
!                 !Obs... 1./ Se *em  branco*  ou  preenchido  com!   
!                 !           *000*, o envio eh realizado via DOC.!   
!                 !       2./ Apenas a informacao *018* nao garan-!   
!                 !           te o envio via  TED,  devendo  serem!
!                 !           atendidas outras condicoes, tais co-!
!                 !           como horario-limite  para  liberacao!   
!                 !           do arquivo-remessa  pela  agencia  e!
!                 !           valor minimo por operacao.          !   
!-----------------!-----------------------------------------------!   
!/19/             !As mensagens serao impressas em todos os       !   
!MENSAGENS        !bloquetos referentes 1 e 2 ao mesmo lote. Estes!   
!                 !campos nao serao utilizados no arquivo retorno.!   
!-----------------!-----------------------------------------------!
!/20/             !01 - Entrada de Titulos.                       !
!CODIGOS DE MOVI- !02 - Pedido de Baixa.                          !
!MENTO PARA       !04 - Concessao de Abatimento.                  !
!REMESSA          !05 - Cancelamento de Abatimento.               !
!                 !06 - Alteracao de Vencimento.                  !
!                 !07 - Concessao de Desconto.                    !
!                 !08 - Cancelamento de Desconto.                 !
!                 !09 - Protestar.                                !
!                 !10 - Cancela/Sustacao da Instrucao de protesto !
!                 !30 - Recusa da Alegacao do Sacado.             !
!                 !31 - Alteracao de Outros Dados.                !
!                 !40 - Alteracao de modalidade.                  !
!                 !Obs.: Cada banco definira os  campos a serem   !
!                 !alterados para o codigo de movimento 31        !
!-----------------!-----------------------------------------------!
!/21/             !Codigo de movimento igual a '01' /entrada de   !
!NOSSO NUMERO     !titulos/, caso esteja preenchida com zeros, a  !
!                 !numeracao sera feita pelo banco.               !   
!                  Nosso-N�mero com 11 posi��es: informar as onze !   
!                  posi��es mais o DV - D�gito Verificador.       !   
!                  Nosso-N�mero vinculado a conv�nios com sete    !   
!                  posi��es - faixa num�rica acima de 1.000.000   !   
!                  (um milh�o): informar o nosso-n�mero sem DV  - !
!                  D�gito Verificador. +                          !   
!-----------------!-----------------------------------------------!   
!/22/             !1 - Cobranca Simples.                          !   
!CARTEIRA         !2 - Cobranca Vinculada.                        !   
!                 !3 - Cobranca Caucionada.                       !   
!                 !4 - Cobranca Descontada.                       !   
!                 !7 - Cobranca Direta Especial /carteira 17/     !
*-----------------------------------------------------------------*   


*-----------------------------------------------------------------*   
!    NOTAS        !   INFORMACOES COMPLEMENTARES                  !   
!-----------------------------------------------------------------!   
!/23/             !1 - Banco emite.                               !
!EMISSAO DO BLO-  !2 - Cliente emite.                             !
!QUETO            !3 - Banco pre-emite e cliente complementa.     !   
!                 !4 - Banco reemite.                             !
!                 !5 - Banco nao reemite.                         !   
!                 !6 - Cobranca sem Papel.                        !   
!                 !Obs.: Os codigos 4 e 5 so serao aceitos para   !   
!                 !      codigo de movimento para remessa 31      !   
!-----------------!-----------------------------------------------!   
!/24/             !Numero utilizado pelo cliente para identifica- !
!NUMERO DO DOCU-  !cao do titulo.                                 !   
!MENTO            !                                               !   
!-----------------!-----------------------------------------------!   
!/25/             !01 - CH  Cheque.                               !
!ESPECIE DOS      !02 - DM  Duplicata Mercantil.                  !
!TITULOS          !03 - DMI Duplicata Mercantil p/ Indicacao.     !
!                 !04 - DS  Duplicata de Servico.                 !
!                 !05 - DSI Duplicata de Servico p/ Indicacao.    !
!                 !06 - DR  Duplicata Rural.                      !
!                 !07 - LC  Letra de Cambio.                      !
!                 !08 - NCC Nota de Credito Comercial.            !
!                 !09 - NCE Nota de Credito A Exportacao.         !
!                 !10 - NCI Nota de Credito Industrial.           !
!                 !11 - NCR Nota de Credito Rural.                !
!                 !12 - NP  Nota Promissoria.                     !
!                 !13 - NPR Nota Promissoria Rural.               !
!                 !14 - TM  Triplicata Mercantil.                 !
!                 !15 - TS  Triplicata de Servico.                !
!                 !16 - NS  Nota de Seguro.                       !
!                 !17 - RC  Recibo.                               !
!                 !18 - FAT Fatura.                               !
!                 !19 - ND  Nota de Debito.                       !
!                 !20 - AP  Apolice de Seguro.                    !
!                 !21 - ME  Mensalidade Escolar                   !
!                 !22 - PC  Parcela de Consorcio                  !
!                 !99 - Outros                                    !
!-----------------!-----------------------------------------------!
!/26/             !1 - Valor por Dia;     2 - Taxa Mensal e       !
!CODIGO DE MORA   !3 - Isento                                     !
!-----------------!-----------------------------------------------!
!/27/             !Se invalida ou nao informada, sera assumida a  !
!DATA DO JUROS    !data do vencimento.                            !
!DE MORA          !                                               !
!-----------------!-----------------------------------------------!
!/28/             !1 - Valor Fixo ate a data informada.           !
!CODIGO DO DES-   !2 - Percentual ate a data informada.           !
!CONTO            !3 - Valor Por Antecipacao Dia Corrido.         !
!                 !4 - Valor Por Antecipacao Dia Util.            !
!                 !5 - Percentual sobre o Valor Nominal Dia       !
*-----------------------------------------------------------------*
*-----------------------------------------------------------------*
!    NOTAS        !   INFORMACOES COMPLEMENTARES                  !
!-----------------------------------------------------------------!
!/28/             !    Corrido.                                   !
!CODIGO DO DES-   !6 - Percentual sobre o Valor Nominal Dia Util. !
!CONTO /CONT./    !Obs.: Para os  codigos 1 e 2  sera obrigatorio !
!                 !a informacao da data.                          !
!-----------------!-----------------------------------------------!
!/29/             !01 - Reservado para uso futuro.                !
!CODIGO DA MOEDA  !02 - Dolar Americano Comercial /Venda/.        !   
!                 !03 - Dolar Americano Turismo /Venda/.          !
!                 !04 - ITRD.                                     !   
!                 !05 - IDTR.                                     !   
!                 !06 - UFIR Diaria.                              !   
!                 !07 - UFIR Mensal.                              !   
!                 !08 - FAJ-TR.                                   !   
!                 !09 - REAL.                                     !   
!-----------------!-----------------------------------------------!   
!/30/             !1 - CPF; 2 - CGC; 0 - Isento/Nao Informado e   !   
!TIPO DE INSCRICAO!9 - Outros.                                    !
!NUMERO DE INCRI- !Se tipo de inscricao igual a zeros /nao        !
! CAO             !informado/, preencher com zeros                !   
!-----------------!-----------------------------------------------!
!/31/             !Informacao obrigatoria quando se tratar de     !   
!SACADOR/AVALISTA !titulo negociado com terceiros.                !   
!-----------------------------------------------------------------!   
!    NOTAS        !   INFORMACOES COMPLEMENTARES                  !   
!-----------------------------------------------------------------!   
!/32/             !Somente para troca de arquivos entre Bancos.   !
!USO DOS BANCOS   !                                               !   
!-----------------!-----------------------------------------------!
!/33/             !Este campo so podera ser utilizado, caso haja  !
!INFORMACAO AO    !troca de arquivos magneticos entre o Banco e o !
!SACADO           !sacado.                                        !   
!-----------------!-----------------------------------------------!   
!/34/             !Mensagem livre a ser impressa no campo         !   
!MENSAGENS 3   9  !instrucoes da ficha de compensacao do bloqueto.!   
!                 !Obs.: As mensagens 3 e 4  prevalecem sobre as  !   
!                 !mensagens 1 e 2.                               !   
!                 !Obs.: Atualmente, utilizar somente o campo de  !   
!                 !      mensagem 3, com, no maximo, 35 posicoes.     
!-----------------!-----------------------------------------------!   
!/35/             !1 - Frente do bloqueto.                        !
!CODIGO DE IDENTI-!2 - Verso do bloqueto.                         !
!FICACAO PARA IM- !3 - Campo de instrucoes da ficha de compensacao!   
!PRESSAO DA MENSA-!    do bloqueto.                               !
!GEM              !8 - Bloqueto por e-mail                        !   
!-----------------!-----------------------------------------------!   
!/36/             !F - Frente : podera variar de "01"   "36".     !   
!NUMERO DE LINHA  !V - Verso  : podera variar de "01"   "24".     !   
!A SER IMPRESSA   !Zeros      : para envio de bloqueto por e-mail !   
*-----------------------------------------------------------------*
                                                                      
*-----------------------------------------------------------------*   
!    NOTAS        !   INFORMACOES COMPLEMENTARES                  !   
!-----------------------------------------------------------------!   
!/37/             !Esta linha devera ser enviada no formato imagem!   
!MENSAGEM A SER   !de impressao/tamanho maximo 140 posicoes/.     !   
!IMPRESSA         !Para bloqueto por e-mail: os endere�os de      !   
!                 !e-mail dever�o ser separados por ";" (ponto e  !
!                 !v�rgula), sem espa�os.                         !   
!-----------------!-----------------------------------------------!
!/38/             !Somatoria dos registros do lote, incluindo     !   
!QUANTIDADE DE RE-!Header e Trailer.                              !   
!GISTRO DE LOTE   !                                               !
!-----------------!-----------------------------------------------!
!/39/             !Este campo nao sera utilizado para a cobranca. !
!FORMA DE LANCA-  !                                               !
!MENTO            !                                               !
!-----------------!-----------------------------------------------!
!/40/             !02 - Entrada confirmada.                       !
!CODIGOS DE MOVI- !03 - Entrada rejeitada.                        !
!MENTO PARA RETOR-!04 - Transferencia de carteira/entrada.        !
!NO               !05 - Transferencia de carteira/baixa.          !
!                 !06 - Liquidacao.                               !
!                 !09 - Baixa.                                    !   
!                 !11 - Titulos em carteira /em ser/.             !   
!                 !12 - Confirmacao recebimento instrucao de      !   
!                 !     abatimento.                               !   
!                 !13 - Confirmacao recebimento instrucao de      !   
!                 !     cancelamento abatimento.                  !   
!                 !14 - Confirmacao recebimento instrucao altera- !   
!                 !     cao de vencimento.                        !   
!                 !15 - Franco de pagamento.                      !   
!                 !17 - Liquidacao apos baixa ou liquidacao titulo!   
!                 !     nao registrado.                           !   
!                 !19 - Confirmacao recebimento instrucao de      !   
!                 !     protesto.                                 !   
!                 !20 - Confirmacao recebimento instrucao de      !
!                 !     sustacao/cancelamento de protesto.        !
!                 !23 - Remessa a cartorio /aponte em cartorio/.  !
!                 !24 - Retirada de cartorio e manutencao em      !
!                 !     carteira.                                 !   
!                 !25 - Protestado e baixado /baixa por ter sido  !   
!                 !     protestado/.                              !   
!                 !26 - Instrucao rejeitada.                      !   
!                 !27 - Confirmacao do pedido de alteracao de     !
!                       outros dados.                             !
!                 !28 - Debito de tarifas/custas.                 !   
!                 !29 - Ocorrencias do sacado.                    !   
!                 !30 - Alteracao de dados rejeitada.             !   
!                 !Obs.: Os codigos 03, 26 e 30 estao relacionados!
!                 !com a nota 42-a.                               !
!                 !O codigo 28 esta relacionado com a nota 42-b.  !
!                 !Os codigos 06, 09 e 17 estao relacionados com a!
!                 !nota 42-c.                                     !
*-----------------------------------------------------------------*

*-----------------------------------------------------------------*
!    NOTAS        !   INFORMACOES COMPLEMENTARES                  !
!-----------------------------------------------------------------!
!/41/             !So serao utilizados para informacao do arquivo !
!TOTALIZACAO DA   !retorno                                        !
!COBRANCA         !                                               !
!-----------------!-----------------------------------------------!
!/42/             !42-A-Codigos de rejeicoes de 01 a 64 associados!
!REJEICOES DE RE- !ao codigo de movimento 03, 26 e 30 /nota 40/   !
!GISTRO DETALHE,  ! 01 - Codigo do banco invalido.                !
!CODIGOS DE TARI- ! 02 - Codigo do registro detalhe invalido.     !
!FAS/CUSTAS E     ! 03 - Codigo do segmento invalido.             !
!ORIGEM DA LIQUI- ! 04 - Codigo do movimento nao permitido para   !
!DACAO/BAIXA      !      carteira.                                !   
!                 ! 05 - Codigo de movimento invalido.            !   
!                 ! 06 - Tipo/nUmero de inscricao do cedente      !   
!                 !      Invalidos.                               !   
!                 ! 07 - Agencia/Conta/DV invalido.               !   
!                 ! 08 - Nosso nUmero invalido.                   !   
!                 ! 09 - Nosso nUmero duplicado.                  !   
!                 ! 10 - Carteira invalida.                       !
!                 ! 11 - Forma de cadastramento do titulo invalido!   
!                 ! 12 - Tipo de documento invalido.              !   
!                 ! 13 - Identificacao da emissao do bloqueto     !   
!                 !      invalida.                                !   
!                 ! 14 - Identificacao da distribuicao do bloqueto!   
!                 !      invalida.                                !
!                 ! 15 - Caracteristicas da cobranca incompativeis!
!                 ! 16 - Data de vencimento invalida.             !   
!                 ! 17 - Data de vencimento anterior a data de    !
!                 !      emissao.                                 !   
!                 ! 18 - Vencimento fora do prazo de operacao.    !   
!                 ! 19 - Titulo a cargo de Bancos Correspondentes !   
!                 !      com vencimento inferior   XX dias        !
!                 ! 20 - Valor do titulo invalido.                !   
!                 ! 21 - Especie do titulo invalida.              !
!                 ! 22 - Especie nao permitida para a carteira.   !   
!                 ! 23 - Aceite invalido.                         !   
!                 ! 24 - Data da emissao invalida.                !
!                 ! 25 - Data da emissao posterior a data         !   
!                 ! 26 - Codigo de juros de mora invalido.        !
!                 ! 27 - Valor/Taxa de juros de mora invalido.    !   
!                 ! 28 - Codigo do desconto invalido.             !   
!                 ! 29 - Valor do desconto maior ou igual ao valor!   
!                 ! do titulo.                                    !   
!                 ! 30 - Desconto a conceder nao confere.         !   
!                 ! 31 - Concessao de desconto - ja existe        !   
!                 !      desconto anterior.                       !   
!                 ! 32 - Valor do IOF invalido.                   !   
!                 ! 33 - Valor do abatimento invalido.            !
!                 ! 34 - Valor do abatimento maior ou igual ao    !
!                 !      valor do titulo.                         !   
*-----------------------------------------------------------------*
*-----------------------------------------------------------------*   
!    NOTAS        !   INFORMACOES COMPLEMENTARES                  !   
!-----------------------------------------------------------------!   
!/42/             ! 35 - Abatimento a conceder nao confere.       !
!REJEICOES DE RE- ! 36 - Concessao de abatimento - ja existe      !
!GISTRO DETALHE,  !      abatimento anterior.                     !
!CODIGOS DE TARI- ! 37 - Codigo para protesto invalido.           !
!FAS/CUSTAS E     ! 38 - Prazo para protesto invalido.            !
!ORIGEM DA LIQUI- ! 39 - Pedido de protesto nao permitido para o  !
!DACAO/BAIXA      !      titulo.                                  !
!/CONT./          ! 40 - Titulo com ordem de protesto emitida.    !
!                 ! 41 - Pedido de cancelamento/sustacao para     !
!                 !      titulos sem instrucao de protesto.       !
!                 ! 42 - Codigo para baixa/devolucao invalido.    !   
!                 ! 43 - Prazo para baixa/devolucao invalido.     !
!                 ! 44 - Codigo da moeda invalido.                !   
!                 ! 45 - Nome do sacado nao informado.            !
!                 ! 46 - Tipo/nUmero de inscricao do sacado       !   
!                 !      invalidos.                               !   
!                 ! 47 - Endereco do sacado nao informado.        !
!                 ! 48 - CEP invalido.                            !
!                 ! 49 - CEP sem praca de cobranca /nao localizado!
!                 ! 50 - CEP referente a um Banco Correspondente. !
!                 ! 51 - CEP incompativel com a unidade da        !
!                 !      federaCcao                               !
!                 ! 52 - Unidade da federacao invalida.           !
!                 ! 53 - Tipo/nUmero de inscricao do sacador/     !
!                 !      avalista invalidos.                      !
!                 ! 54 - Sacador/Avalista nao informado.          !
!                 ! 55 - Nosso nUmero no Banco Correspondente nao !
!                 !      informado.                               !
!                 ! 56 - Codigo do Banco Correspondente nao       !
!                 !      informado.                               !
!                 ! 57 - Codigo da multa invalido.                !
!                 ! 58 - Data da multa invalida.                  !
!                 ! 59 - Valor/Percentual da multa invalido.      !
!                 ! 60 - Movimento para titulo nao cadastrado.    !
!                 ! 61 - Alteracao da agencia cobradora/dv        !
!                 !      invalida.                                !
!                 ! 62 - Tipo de impressao invalido.              !
!                 ! 63 - Entrada para titulo ja cadastrado.       !
!                 ! 64 - Numero da linha invalido.                !
!                 ! 65 - Codigo do banco para debito invalido.    !
!                 ! 66 - Agencia/conta/DV para debito invalido.   !
!                 ! 67 - Dados para debito incompativel com a     !
!                 !      identificacao da emissao do bloqueto.    !
!                 ! 88 - Arquivo em duplicidade                   !
!                 ! 99 - Contrato inexistente.                    !
!                 !                                               !
!                 !Os c�digos abaixo s�o exclusivos do bloqueto   !
!                 !por e-mail                                     !
!                 !46 - Documento de identifica��o do sacado inv�-!
!                 !     lido - bloqueto ser� impresso.            !
!                 !47 - Endere�o e-mail do sacado inv�lido - Blo- !
!                 !     queto ser� impresso.                      !
!                 !14 - Sacado optante por Cobran�a Eletr�nica -  !
!                 !     Bloqueto ser� eletr�nico                  !
!                 !15 - Tipo de conv�nio n�o permite envio de     !
!                 !     e-mail - Bloqueto ser� impresso           !
!                 !15 - Modalidade de cobran�a inv�lida para en-  !
!                 !     vio de e-mail - bloqueto ser� impresso.   !
!                 !13 - Cedente n�o autorizado a enviar e-mail -  !
!                 !     Bloqueto ser� impresso                    !
*-----------------------------------------------------------------*




*-----------------------------------------------------------------*
!    NOTAS        !   INFORMACOES COMPLEMENTARES                  !
!-----------------------------------------------------------------!
!                 !42-B - Codigos de tarifas/custas de 01 a 11    !
!                 !associados ao codigo de movimento 28. /nota 40/!
!                 ! 01 - Tarifa de extrato de posicao.            !
!                 ! 02 - Tarifa de manutencao de titulo vencido.  !
!                 ! 03 - Tarifa de sustacao.                      !
!                 ! 04 - Tarifa de protesto.                      !
!                 ! 05 - Tarifa de outras instrucoes.             !
!                 ! 06 - Tarifa de outras ocorrencias.            !
!                 ! 07 - Tarifa de envio de duplicata ao sacado.  !
!                 ! 08 - Custas de protesto.                      !
!                 ! 09 - Custas de sustacao de protesto.          !
!                 ! 10 - Custas do cartorio distribuidor.         !
!                 ! 11 - Custas de edital.                        !
!-----------------!-----------------------------------------------!
!                 !42-C - Codigos de liquidacao/baixa de 01 a 13  !
!                 !associados ao codigo de  movimento 06, 09 e 17 !
!                 !/nota 40/                                      !
!                 ! Liquidacao:                                   !
!                 !   01 - Por saldo.                             !
!                 !   02 - Por conta.                             !
!                 !   03 - No proprio banco.                      !
!                 !   04 - Compensacao eletronica.                !
!                 !   05 - Compensacao convencional.              !
!                 !   06 - Por meio eletronico.                   !
!                 !   07 - Apos feriado local.                    !
!                 !   08 - Em cartorio.                           !
!                 ! Baixa:                                        !
!                 !   09 - Comandada banco.                       !
!                 !   10 - Comandada cliente arquivo.             !
!                 !   11 - Comandada cliente on-line.             !
!                 !   12 - Decurso prazo - cliente.               !
!                 !   13 - Decurso prazo - banco.                 !
!-----------------!-----------------------------------------------!
!/43/             !Serah  informado o prefixo da Agencia do Banco !
!AGENCIA COBRADO- !recebedor.  Quando   este  prefixo  nao   for  !
!RA/RECEBEDORA    !informado, o campo serah preenchido com /ZERO/ !   
!-----------------!-----------------------------------------------!
!/44/            !SIGNIFICADO           !COD.! DATA !VALOR!COMPLE-!
!OCORRENCIAS DO  !                      !    !      !     !MENTO  !
!SACADO          !----------------------!----!------!-----!-------!
!                !Sacado alega que nao  !0101!branco!zeros!brancos!
!                ! recebeu a mercadoria !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que merca!0102!branco!zeros!brancos!
!                !doria chegou atrasada !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que merca!0103!branco!zeros!brancos!
!                !chegou avariada       !    !      !     !       !
*-----------------------------------------------------------------*

*-----------------------------------------------------------------*   
!    NOTAS       !   INFORMACOES COMPLEMENTARES                   !   
!-----------------------------------------------------------------!   
!/44/            !SIGNIFICADO           !COD.! DATA !VALOR!COMPLE-!
!OCORRENCIAS DO  !                      !    !      !     !MENTO  !   
!SACADO /CONT./  !----------------------!----!------!-----!-------!   
!                !Sacado alega que merca!0104!branco!zeros!brancos!
!                !doria nao confere com !    !      !     !       !   
!                !o pedido              !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que merca!0105!branco!zeros!brancos!   
!                !doria chegou incomplet!    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado alega que merca!0106!branco!zeros!brancos!
!                !doria esta   disposi- !    !      !     !       !
!                !cao do cedente        !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que      !0107!branco!zeros!brancos!   
!                !devolveu a mercadoria !    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Sacado alega que merca!0108!branco!zeros!brancos!   
!                !doria esta em desacor-!    !      !     !       !   
!                !do com a Nota Fiscal  !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado alega que nada !0109!branco!zeros!brancos!   
!                !deve/comprou          !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que nao  !0201!branco!zeros!brancos!   
!                !recebeu a fatura      !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que o    !0202!branco!zeros!brancos!   
!                !pedido de compra foi  !    !      !     !       !   
!                !cancelado             !    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Sacado alega que a du-!0203!branco!zeros!brancos!   
!                !plicata foi cancelada !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Sacado alega nao ter  !0204!branco!zeros!brancos!
!                !recebido mercadoria,  !    !      !     !       !
!                !nota fiscal e a fatura!    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que a du-!0205!branco!zeros!brancos!   
!                !plicata fatura esta   !    !      !     !       !   
!                !incorreta             !    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Sacado alega que o    !0206!branco!zeros!brancos!
!                !valor esta incorreto  !    !      !     !       !   
!SACADO /CONT./  !----------------------!----!------!-----!-------!   
!                !Sacado alega que o    !0207!branco!zeros!brancos!   
!                !faturamento e indevido!    !      !     !       !
*-----------------------------------------------------------------*   
                                                                      

*-----------------------------------------------------------------*   
!    NOTAS       !   INFORMACOES COMPLEMENTARES                   !   
!-----------------------------------------------------------------!   
!/44/            !SIGNIFICADO           !COD.! DATA !VALOR!COMPLE-!   
!OCORRENCIAS DO  !                      !    !      !     !MENTO  !   
!SACADO /CONT./  !----------------------!----!------!-----!-------!   
!                !Sacado alega que nao  !0208!branco!zeros!brancos!
!                !localizou o pedido de !    !      !     !       !
!                !compra                !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que o    !0301! DATA !zeros!brancos!   
!                !vencimento correto e: !    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Sacado solicita a     !0302! DATA !zeros!brancos!   
!                !prorrogacao de venci- !    !      !     !       !
!                !mento para:           !    !      !     !       !
!                !Sacado aceita se venci!0303! DATA !zeros!brancos!
!                !mento prorrogado para:!    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado alega que paga-!0304! DATA !zeros!brancos!
!                !ra titulo em:         !    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Sacado pagou o titulo !0305! DATA !zeros!brancos!
!                !ao cedente em:        !    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Sacado pagara o titulo!0306! DATA !zeros!brancos!   
!                !ao cedente em:        !    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Sacado nao localizado,!0401!branco!zeros!brancos!   
!                !confirmar o endereco  !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado mudou-se, trans!0402!branco!zeros!brancos!   
!                !feriu de domicilio    !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado nao recebe no  !0403!branco!zeros!brancos!   
!                !endereco indicado     !    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Sacado desconhecido   !0404!branco!zeros!brancos!   
!                !no local              !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado reside fora do !0405!branco!zeros!brancos!   
!                !perimetro             !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Sacado com endereco   !0406!branco!zeros!brancos!
!                !incompleto            !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Nao foi localizado o  !0407!branco!zeros!brancos!   
!                !numero constante no   !    !      !     !       !   
!                !endereco do titulo    !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Endereco nao localiza-!0408!branco!zeros!brancos!   
!                !do/nao consta nos     !    !      !     !       !   
*-----------------------------------------------------------------*
*-----------------------------------------------------------------*
!    NOTAS       !   INFORMACOES COMPLEMENTARES                   !   
!-----------------------------------------------------------------!
!/44/            !SIGNIFICADO           !COD.! DATA !VALOR!COMPLE-!   
!OCORRENCIAS DO  !                      !    !      !     !MENTO  !   
!SACADO /CONT./  !----------------------!----!------!-----!-------!   
!                !guias  da  cidade     !    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Endereco do sacado    !0409!branco!zeros!nov end!
!                !alterado para:        !    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Sacado alega ter des- !0501!branco!VALOR!brancos!   
!                !conto ou abatimento de!    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado solicita descon!0502!branco!VALOR!brancos!   
!                !to ou abatimento de:  !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado solicita dispen!0503!branco!zeros!brancos!   
!                !sa dos juros de mora  !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Sacado se recusa a    !0504!branco!zeros!brancos!   
!                !pagar juros           !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado se recusa a pa-!0505!branco!zeros!brancos!
!                !gar comissao de perma-!    !      !     !       !   
!                !nencia                !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado esta em regime !0601!branco!zeros!brancos!   
!                !de concordata         !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado esta em regime !0602!branco!zeros!brancos!   
!                !de falencia           !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado alega que man- !0603!branco!zeros!brancos!   
!                !tem entendimentos com !    !      !     !       !   
!                !o sacador             !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado esta em entendi!0604!branco!zeros!brancos!   
!                !mentos com o cedente  !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado esta viajando  !0605!branco!zeros!brancos!   
!                !----------------------!----!------!-----!-------!   
!                !Sacado recusou-se a   !0606!branco!zeros!brancos!   
!                !aceitar o titulo      !    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Sacado sustou protesto!0607!branco!zeros!brancos!
!                !judicialmente         !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Empregado recusou-se a!0608!branco!zeros!brancos!
!                !receber o titulo      !    !      !     !       !   
!SACADO /CONT./  !----------------------!----!------!-----!-------!
!                !Titulo reapresentado  !0609!branco!zeros!brancos!   
*-----------------------------------------------------------------*   
*-----------------------------------------------------------------*   
!    NOTAS       !   INFORMACOES COMPLEMENTARES                   !
!-----------------------------------------------------------------!   
!/44/            !SIGNIFICADO           !COD.! DATA !VALOR!COMPLE-!   
!OCORRENCIAS DO  !                      !    !      !     !MENTO  !   
!SACADO /CONT./  !----------------------!----!------!-----!-------!
!                !ao sacado             !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Estamos nos dirigindo !0610!branco!zeros!brancos!
!                !ao nosso correspondent!    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Correspondente nao se !0611!branco!zeros!brancos!   
!                !interessa pelo protest!    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Sacado nao atende aos !0612!branco!zeros!brancos!   
!                !avisos de nossos      !    !      !     !       !
!                !correspondentes       !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Titulo esta sendo     !0613!branco!zeros!brancos!
!                !encaminhado ao corres-!    !      !     !       !   
!                !pondente              !    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Entrega franco de paga!0614!branco!zeros!brancos!   
!                !mento ao sacado       !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Entrega franco de paga!0615!branco!zeros!brancos!   
!                !mento ao representante!    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !A entrega franco de   !0616!branco!zeros!brancos!
!                !pagamento e dificil   !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Titulo recusado pelo  !0617!branco!zeros!mot.rec!
!                !cartorio:             !    !      !     !       !
!                !OBS.: Somente serao informadas o codigo de      !
!                !movimento 29.                                   !
!                !O campo complemento podera ser utilizado de     !
!                !acordo com a estrutura de cada banco.           !
!-----------------------------------------------------------------!
!/45/            !Identifica o N  da Versao do Layout do Lote,    !
!LEIAUTE DO LOTE !composto de :                                   !
!                ! Versao  - 2 digitos                            !   
!                ! Release - 1 digito.                            !
!                !Obs.: Somente podera ser modificado quando  hou-!   
!                !      alteracao nos lotes de servico e desde que!   
!                !      aprovadas pelo CNAB                       !   
!----------------!------------------------------------------------!   
!/46/            !Para uso futuro da Caracteristica da Cobranca.  !
!CNAB            !                                                !   
!-----------------------------------------------------------------!   
!/47/            !Formato do caracter a ser impresso.             !   
!TIPO DE FONTE   ! 01 - Normal                                    !   
!                ! 02 - Italico                                   !   
!                ! Zeros - para envio de bloqueto por e-mail      !   
*-----------------------------------------------------------------*
*-----------------------------------------------------------------*
!    NOTAS       !   INFORMACOES COMPLEMENTARES                   !
!-----------------------------------------------------------------!   
!/47/            ! 03 - Normal negrito                            !   
!TIPO DE FONTE   ! 04 - Italico negrito                           !   
!     /CONT./    !                                                !   
!----------------!------------------------------------------------!   
!/48/            !A vista             - preencher com 11111111    !
!VENCIMENTO      !Contra-apresentacao - preencher com 99999999    !
!                !Obs.: O prazo legal para vencimento "a vista"   !   
!                !ou "contra-apresentacao" e de 15 dias da data do!
!                !registro no banco                               !   
!----------------!------------------------------------------------!   
!/49/            !Informacao opcional - na ausencia sera atribuida!   
!AGENCIA COBRA-  !pelo CEP.                                       !   
!DORA            !                                                !   
!----------------!------------------------------------------------!   
!/50/            !Ocorrencias especificas para o servico cobranca !
!OCORRENCIAS     !sem papel.                                      !   
!                ! Codigos de Ocorrencia :                        !   
!                !  HA - Lote Nao Aceito                          !   
!                !  AA - Controle Invalido                        !   
!                !  AB - Tipo de Operacao Invalido                !   
!                !  AC - Tipo de Servico Invalido                 !
!                !  AE - Tipo/NUmero de Inscricao do Cedente      !   
!                !       Invalido                                 !   
!                !  AG - Agencia/Conta Corrente/DV do Sacado      !   
!                !       Invalido                                 !   
!                !  AH - N  Sequencial do Registro no Lote        !
!                !       Invalido                                 !   
!                !  AI - Codigo de Segmento de Detalhe Invalido   !
!                !  AL - Codigo do Banco do Sacado Invalido       !
!                !  AO - Nome do Cedente nao Informado            !   
!                !  AP - Data para o debito Invalida              !
!                !  AR - Valor do Lancamento para Debito Invalido !   
!                !  AT - Tipo/NUmero de Inscricao do Sacado       !   
!                !       Invalido                                 !   
!                !  BF - Codigo de Barras Invalido                !   
!                !  BG - Codigo do Banco Remetente Invalido       !   
!                !  BH - Remessa de debito para agendamento em    !   
!                !       duplicidade /codigo de barras/data de    !   
!                !       vencimento existentes/                   !   
!                !  BI - Retirada de debito para agendamento      !   
!                !       inexistente/codigo de barras             !   
!                !       inexistente/                             !   
!                !  BJ - Recusa do debito pelo banco sacado       !   
!                !  BK - Contra ordem do debito pelo sacado       !
!                !  BL - Conta corrente para debito encerrada     !   
!                !  BM - Conta corrente para debito bloqueada     !   
!                !  TA - Lote nao Aceito - Totais do Lote com     !
!                !       Diferenca.                               !   
*-----------------------------------------------------------------*   
                                                                      
*-----------------------------------------------------------------*
!    NOTAS       !   INFORMACOES COMPLEMENTARES                   !
!-----------------------------------------------------------------!   
!/51/            !01 - Remessa para debito                        !
!CODIGO DE MOVI- !02 - Retirada de debito                         !
!MENTO           !03 - Remessa para debito rejeitada              !   
!                !04 - Retirada de debito rejeitada               !
!----------------!------------------------------------------------!   
!/52/            !Indica o tipo de servico que o arquivo contem : !   
!TIPO DE SERVICO !   '02' - Cobranca sem Papel                    !   
!                !Obs.: Este campo so devera ser utilizado para a !   
!                !cobranca sem papel.                             !   
!----------------!------------------------------------------------!   
!/53/            !Ocorrencias especificas para o arquivo com lote !
!OCORRENCIAS CO- !de servico cobranca sem papel:                  !
!BRANCA SEM PAPEL!01- Controle do registro header invalido        !
!                !02- Codigo de remessa diferente de "1"          !
!                !03- Data de geracao invalida ou diferente de D+0!   
!                !04- Numero sequencial do arq.nao numerico ou    !   
!                !    fora de sequencia                           !   
!                !05- Numero da versao do layout do arquivo       !   
!                !    invalido                                    !   
!                !06- Tipo de servico diferente de "02"           !   
!                !07- Controle do registro trailer invalido       !
!                !08- Arquivo nao aceito - Totais do arquivo com  !
!                !    diferenca                                   !   
!----------------!------------------------------------------------!
!/54/            !Devera conter o/s/ codigos/s/ da ocorrencia do  !   
!CODIGO DA OCOR- !sacado /NOTA 44/ a/s/ qual/is/ o cedente nao    !
!RENCIA DO SACADO!concorda. Somente sera utilizado para o codigo  !   
!                !de movimento 30 /NOTA 20/.                      !   
!----------------!------------------------------------------------!   
!/55/            ! posicoes   picture                             !   
!USO EXCLUSIVO   !235 a 235  9/01/                                !
!DA FEBRABAN/CNAB! codigo da carteira anterior. Especifico para   !   
!                !tipo de movimento 4 - transferencia de          !
!                !carteira/entrada, para titulos das carteiras    !   
!                !11 e 17. Conteudo como no campo 58 a 58         !   
!-----------------------------------------------------------------!   
!  INFORMACOES COMPLEMENTARES                                     !
!-----------------------------------------------------------------!   
!  OBSERVACOES GERAIS PARA TITULOS EM CARTEIRA DE COBRANCA        !   
!-----------------------------------------------------------------!   
!  O controle entre um  grupo de  segmentos  para um mesmo titulo,!   
!  sera pelos campos 'codigo de movimento' e 'numero do registro" !   
!-----------------------------------------------------------------!   
!  Campos numericos nao utilizados     : preencher com zeros.     !
!-----------------------------------------------------------------!
!  Campos alfanumericos nao utilizados : preencher com brancos    !   
!-----------------------------------------------------------------!
!  Os campos referentes as taxas/percentuais deverao ser preen-   !   
!  chidos com duas casas decimais e serao impressos no bloqueto   !   
*-----------------------------------------------------------------*   
*-----------------------------------------------------------------*   
!  INFORMACOES COMPLEMENTARES                                     !   
!-----------------------------------------------------------------!   
!  OBSERVACOES GERAIS PARA TITULOS EM CARTEIRA DE COBRANCA /CONT./!   
!-----------------------------------------------------------------!   
!  em valor da moeda corrente ou quantidade /para moeda variavel/.!   
!-----------------------------------------------------------------!   
!  Rejeicoes do arquivo :                                         !
!    - Codigo do banco invalido                                   !   
!    - Codigo de servico invalido                                 !
!    - Codigo do convenio invalido                                !   
!    - Codigo da agencia/conta invalido                           !
!    - Codigo do nUmero de remessa invalida                       !   
!    - NUmero sequencial do registro dentro do arquivo invalido   !   
!    - Quantidade de registros do lote invalido ou divergente.    !   
!-----------------------------------------------------------------!   
!  Sugestao para impressao nos bloquetos : /instrucoes de recebi- !
!  mento/                                                         !
!  - Ate dd/mm/aaaa desconto de zzzzzzz.zzz.zz9,99                !   
!  - Apos dd/mm/aaaa juros dia de zzzzzzzzz.zz9,99                !
!  - Apos dd/mm/aaaa multa de z.zzz.zzz.zzz.zz9,99                !   
!  - Conceder abatimento de z.zzz.zzz.zzz.zz9,99                  !   
!  - Desc. zzzzzzz.zzz.zz9,99 p/dia corrido antec.                !   
!  - Desc. zzzzzzz.zzz.zz9,99 p/dia util    antec.                !   
!-----------------------------------------------------------------!   
!                                                                 !
!  Utilizacao dos segmentos 'P' a 'U'.                            !
!  - O segmento 'P' e obrigatorio.                                !
!  - O segmento 'Q' e obrigatorio somente para o codigo de movi-  !
!    mento '01'/entrada de titulos/.                              !
!  - O segmento 'R' e opcional.                                   !
!  - O segmento 'S' so sera utilizado paraa mensagens nos bloque- !
!    tos.                                                         !
!  - O segmento 'T' e obrigatorio.                                !
!  - O segmento 'U' e obrigatorio.                                !
!  - Utilizacao de moeda variavel:                                !
!    - todos os valores deverao estar na mesma moeda e a edicao   !
!      terah 5 /cinco/ casas decimais.                            !
*------------------------------------------------------------------*
*)


function TFmRetornoBB.TipoDeOperacao(Codigo: String): String;
begin
(*
!/03/             !Indica a operacao que devera ser realizada com !
!TIPO DE OPERACAO !os registros Detalhe do Lote.                  !
!                 !Deve constar apenas um tipo por Lote:          !
!                 ! C - Lancamento a Credito                      !
!                 ! D - Lancamento a Debito                       !
!                 ! E - Extrato para Conciliacao                  !
!                 ! I - Informacoes de titulos capturados do      !
!                 !     proprio banco                             !
!                 ! R - Arquivo Remessa                           !
!                 ! T - Arquivo Retorno.                          !
!-----------------!-----------------------------------------------!
*)
  if Codigo = 'C' then Result := 'Lan�amento a Cr�dito' else
  if Codigo = 'D' then Result := 'Lan�amento a D�bito' else
  if Codigo = 'E' then Result := 'Extrato para Concilia��o' else
  if Codigo = 'I' then Result := 'Informa��es de t�tulos capturados do pr�prio banco' else
  if Codigo = 'R' then Result := 'Arquivo Remessa' else
  if Codigo = 'T' then Result := 'Arquivo Retorno' else
  Result := '';
end;

function TFmRetornoBB.TipoDeServico(Codigo, Tipo: String): String;
begin
  case Geral.IMV(Tipo) of
    4:
    begin
(*
!/4/              !Indica o tipo de servico que o lote contem :   !
!TIPO DE SERVICO  ! 01 - Cobranca                                 !
!                 ! 02 - Cobranca sem Papel                       !
!                 ! 03 - Bloqueto Eletronico                      !
!                 ! 04 - Conciliacao Bancaria                     !
!                 ! 05 - Debitos                                  !
!                 ! 10 - Pagamento Dividendos                     !
!                 ! 20 - Pagamento Fornecedor                     !
!                 ! 30 - Pagamento Salarios                       !
!                 ! 50 - Pagamento Sinistros Segurados            !
!                 ! 60 - Pagamento Despesas Viajante em Transito  !
!TIPO DE SERVICO  ! 70 - Pagamento Autorizado                     !
!     /CONT./     ! 75 - Pagamento Credenciados                   !
!                 ! 80 - Pagamento Representantes/Vendedores      !
!                 !      Autorizados                              !
!                 ! 90 - Pagamento Beneficios                     !
!                 ! 98 - Pagamento Diversos.                      !
*)
      case Geral.IMV(Codigo) of
        01: Result := 'Cobran�a';
        02: Result := 'Cobran�a sem Papel';
        03: Result := 'Bloqueto Eletr�nico';
        04: Result := 'Concilia��o Banc�ria';
        05: Result := 'D�bitos';
        10: Result := 'Pagamento Dividendos';
        20: Result := 'Pagamento Fornecedor';
        30: Result := 'Pagamento Sal�rios';
        50: Result := 'Pagamento Sinistros Segurados';
        60: Result := 'Pagamento Despesas Viajante em Tr�nsito';
        70: Result := 'Pagamento Autorizado';
        75: Result := 'Pagamento Credenciados';
        80: Result := 'Pagamento Representantes/Vendedores Autorizados';
        90: Result := 'Pagamento Beneficios';
        98: Result := 'Pagamento Diversos';
        else Result := '';
      end;
    end;
    53:
    begin
(*
!----------------!------------------------------------------------!
!/53/            !Ocorrencias especificas para o arquivo com lote !
!OCORRENCIAS CO- !de servico cobranca sem papel:                  !
!BRANCA SEM PAPEL!01- Controle do registro header invalido        !
!                !02- Codigo de remessa diferente de "1"          !
!                !03- Data de geracao invalida ou diferente de D+0!
!                !04- Numero sequencial do arq.nao numerico ou    !
!                !    fora de sequencia                           !
!                !05- Numero da versao do layout do arquivo       !
!                !    invalido                                    !
!                !06- Tipo de servico diferente de "02"           !
!                !07- Controle do registro trailer invalido       !
!                !08- Arquivo nao aceito - Totais do arquivo com  !
!                !    diferenca                                   !
!----------------!------------------------------------------------!
*)
      case Geral.IMV(Codigo) of
        01: Result := 'Controle do registro header inv�lido';
        02: Result := 'C�digo de remessa diferente de "1"';
        03: Result := 'Data de gera��o inv�lida ou diferente de D+0';
        04: Result := 'N�mero seq�encial do arq. n�o num�rico ou fora de seq��ncia';
        05: Result := 'N�mero da vers�o do layout do arquivo inv�lido';
        06: Result := 'Tipo de servi�o diferente de "02"';
        07: Result := 'Controle do registro trailer inv�lido';
        08: Result := 'Arquivo n�o aceito - Totais do arquivo com diferen�a';
        else Result := '';
      end;
    end;
    else Result := '';
  end;
end;

function TFmRetornoBB.FormaDeLancamento(Codigo, Tipo: Integer): String;
begin
  case Tipo of
    5:
    begin
  (*
  !/5/              !Indica a forma de lancamento que o lote contem:!
  !FORMA DE LANCA-  ! 01 - Credito em Conta Corrente                !
  !MENTO            ! 02 - Cheque Pagamento/Administrativo          !
  !                 ! 03 - DOC/TED                                  !
  !                 ! 04 - Cartao Salario /Servico Tipo 30/         !
  !                 ! 05 - Credito em Conta Poupanca                !
  !                 ! 10 - OP � disposi��o                          !
  !                 ! 30 - Liquidacao de Titulos do proprio Banco   !
  !                 ! 31 - Pagamento de Titulos de outros Bancos    !
  *)
    case Codigo of
      (*Dermatek*)00: Result := '�nica';
      01: Result := 'Cr�dito em Conta Corrente';
      02: Result := 'Cheque Pagamento/Administrativo';
      03: Result := 'DOC/TED';
      04: Result := 'Cart�o Sal�rio (Servi�o Tipo 30)';
      05: Result := 'Cr�dito em Conta Poupan�a';
      10: Result := 'OP � disposi��o';
      30: Result := 'Liquida��o de T�tulos do pr�prio Banco';
      31: Result := 'Pagamento de T�tulos de outros Bancos';
      else Result := '';
    end;
    end else begin
(*
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
!/39/             !Este campo nao sera utilizado para a cobranca. ! +
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*)
      Result := '';
    end;
  end;

end;

function TFmRetornoBB.TipoDeOcorrenciaDoSacado(Codigo: String): String;
var
  Cod: Integer;
begin
(*
!-----------------!-----------------------------------------------!
!/44/            !SIGNIFICADO           !COD.! DATA !VALOR!COMPLE-!
!OCORRENCIAS DO  !                      !    !      !     !MENTO  !
!SACADO          !----------------------!----!------!-----!-------!
!                !Sacado alega que nao  !0101!branco!zeros!brancos!
!                ! recebeu a mercadoria !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que merca!0102!branco!zeros!brancos!
!                !doria chegou atrasada !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que merca!0103!branco!zeros!brancos!
!                !chegou avariada       !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que merca!0104!branco!zeros!brancos!
!                !doria nao confere com !    !      !     !       !
!                !o pedido              !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que merca!0105!branco!zeros!brancos!
!                !doria chegou incomplet!    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que merca!0106!branco!zeros!brancos!
!                !doria esta   disposi- !    !      !     !       !
!                !cao do cedente        !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que      !0107!branco!zeros!brancos!
!                !devolveu a mercadoria !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que merca!0108!branco!zeros!brancos!
!                !doria esta em desacor-!    !      !     !       !
!                !do com a Nota Fiscal  !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que nada !0109!branco!zeros!brancos!
!                !deve/comprou          !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que nao  !0201!branco!zeros!brancos!
!                !recebeu a fatura      !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que o    !0202!branco!zeros!brancos!
!                !pedido de compra foi  !    !      !     !       !
!                !cancelado             !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que a du-!0203!branco!zeros!brancos!
!                !plicata foi cancelada !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega nao ter  !0204!branco!zeros!brancos!
!                !recebido mercadoria,  !    !      !     !       !
!                !nota fiscal e a fatura!    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que a du-!0205!branco!zeros!brancos!
!                !plicata fatura esta   !    !      !     !       !
!                !incorreta             !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que o    !0206!branco!zeros!brancos!
!                !valor esta incorreto  !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que o    !0207!branco!zeros!brancos!
!                !faturamento e indevido!    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que nao  !0208!branco!zeros!brancos!
!                !localizou o pedido de !    !      !     !       !
!                !compra                !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que o    !0301! DATA !zeros!brancos!
!                !vencimento correto e: !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado solicita a     !0302! DATA !zeros!brancos!
!                !prorrogacao de venci- !    !      !     !       !
!                !mento para:           !    !      !     !       !
!                !Sacado aceita se venci!0303! DATA !zeros!brancos!
!                !mento prorrogado para:!    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado alega que paga-!0304! DATA !zeros!brancos!
!                !ra titulo em:         !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado pagou o titulo !0305! DATA !zeros!brancos!
!                !ao cedente em:        !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado pagara o titulo!0306! DATA !zeros!brancos!
!                !ao cedente em:        !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado nao localizado,!0401!branco!zeros!brancos!
!                !confirmar o endereco  !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado mudou-se, trans!0402!branco!zeros!brancos!
!                !feriu de domicilio    !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado nao recebe no  !0403!branco!zeros!brancos!
!                !endereco indicado     !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado desconhecido   !0404!branco!zeros!brancos!
!                !no local              !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado reside fora do !0405!branco!zeros!brancos!
!                !perimetro             !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado com endereco   !0406!branco!zeros!brancos!
!                !incompleto            !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Nao foi localizado o  !0407!branco!zeros!brancos!
!                !numero constante no   !    !      !     !       !
!                !endereco do titulo    !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Endereco nao localiza-!0408!branco!zeros!brancos!
!                !do/nao consta nos     !    !      !     !       !
!                !guias  da  cidade     !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Endereco do sacado    !0409!branco!zeros!nov end!
!                !alterado para:        !    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Sacado alega ter des- !0501!branco!VALOR!brancos!   
!                !conto ou abatimento de!    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado solicita descon!0502!branco!VALOR!brancos!   
!                !to ou abatimento de:  !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Sacado solicita dispen!0503!branco!zeros!brancos!   
!                !sa dos juros de mora  !    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !Sacado se recusa a    !0504!branco!zeros!brancos!
!                !pagar juros           !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado se recusa a pa-!0505!branco!zeros!brancos!
!                !gar comissao de perma-!    !      !     !       !   
!                !nencia                !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado esta em regime !0601!branco!zeros!brancos!   
!                !de concordata         !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado esta em regime !0602!branco!zeros!brancos!   
!                !de falencia           !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado alega que man- !0603!branco!zeros!brancos!   
!                !tem entendimentos com !    !      !     !       !   
!                !o sacador             !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Sacado esta em entendi!0604!branco!zeros!brancos!   
!                !mentos com o cedente  !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Sacado esta viajando  !0605!branco!zeros!brancos!   
!                !----------------------!----!------!-----!-------!   
!                !Sacado recusou-se a   !0606!branco!zeros!brancos!   
!                !aceitar o titulo      !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado sustou protesto!0607!branco!zeros!brancos!
!                !judicialmente         !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Empregado recusou-se a!0608!branco!zeros!brancos!
!                !receber o titulo      !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Titulo reapresentado  !0609!branco!zeros!brancos!
!                !ao sacado             !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Estamos nos dirigindo !0610!branco!zeros!brancos!
!                !ao nosso correspondent!    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Correspondente nao se !0611!branco!zeros!brancos!
!                !interessa pelo protest!    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Sacado nao atende aos !0612!branco!zeros!brancos!
!                !avisos de nossos      !    !      !     !       !
!                !correspondentes       !    !      !     !       !
!                !----------------------!----!------!-----!-------!   
!                !Titulo esta sendo     !0613!branco!zeros!brancos!
!                !encaminhado ao corres-!    !      !     !       !   
!                !pondente              !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Entrega franco de paga!0614!branco!zeros!brancos!   
!                !mento ao sacado       !    !      !     !       !   
!                !----------------------!----!------!-----!-------!
!                !Entrega franco de paga!0615!branco!zeros!brancos!   
!                !mento ao representante!    !      !     !       !   
!                !----------------------!----!------!-----!-------!   
!                !A entrega franco de   !0616!branco!zeros!brancos!
!                !pagamento e dificil   !    !      !     !       !
!                !----------------------!----!------!-----!-------!
!                !Titulo recusado pelo  !0617!branco!zeros!mot.rec!
!                !cartorio:             !    !      !     !       !
!                !OBS.: Somente serao informadas o codigo de      !
!                !movimento 29.                                   !
!                !O campo complemento podera ser utilizado de     !
!                !acordo com a estrutura de cada banco.           !
!-----------------------------------------------------------------!
*)
  Cod := Geral.IMV(Codigo);
  case Cod of
    0101: Result := 'Sacado alega que n�o recebeu a mercadoria.';
    0102: Result := 'Sacado alega que a mercadoria chegou atrasada.';
    0103: Result := 'Sacado alega que a mercadoria chegou avariada.';
    0104: Result := 'Sacado alega que mercadoria n�o confere com o pedido';
    0105: Result := 'Sacado alega que mercadoria chegou incompleta';
    0106: Result := 'Sacado alega que mercadoria est� disposi��o do cedente';
    0107: Result := 'Sacado alega que devolveu a mercadoria';
    0108: Result := 'Sacado alega que mercadoria esta em desacordo com a Nota Fiscal';
    0109: Result := 'Sacado alega que nada deve/comprou';
    0201: Result := 'Sacado alega que n�o recebeu a fatura';
    0202: Result := 'Sacado alega que o pedido de compra foi cancelado';
    0203: Result := 'Sacado alega que a duplicata foi cancelada';
    0204: Result := 'Sacado alega n�o ter recebido mercadoria, nota fiscal e a fatura';
    0205: Result := 'Sacado alega que a duplicata fatura est� incorreta';
    0206: Result := 'Sacado alega que o valor est� incorreto';
    0207: Result := 'Sacado alega que o faturamento � indevido';
    0208: Result := 'Sacado alega que n�o localizou o pedido de compra ';
    0301: Result := 'Sacado alega que o vencimento correto �: ';
    0302: Result := 'Sacado solicita a prorroga��o de vencimento para:';
    0303: Result := 'Sacado aceita se vencimento prorrogado para:';
    0304: Result := 'Sacado alega que pagar� t�tulo em:';
    0305: Result := 'Sacado pagou o t�tulo ao cedente em:';
    0306: Result := 'Sacado pagar� o t�tulo ao cedente em:';
    0401: Result := 'Sacado n�o localizado, confirmar o endere�o';
    0402: Result := 'Sacado mudou-se, transferiu de domic�lio';
    0403: Result := 'Sacado n�o recebe no endere�o indicado';
    0404: Result := 'Sacado desconhecido no local ';
    0405: Result := 'Sacado reside fora do per�metro';
    0406: Result := 'Sacado com endere�o incompleto';
    0407: Result := 'N�o foi localizado o n�mero constante no endere�o do t�tulo';
    0408: Result := 'Endere�o n�o localizado/n�o consta nos guias da cidade';
    0409: Result := 'Endere�o do sacado alterado para:';
    0501: Result := 'Sacado alega ter desconto ou abatimento de ';
    0502: Result := 'Sacado solicita desconto ou abatimento de: ';
    0503: Result := 'Sacado solicita dispensa dos juros de mora ';
    0504: Result := 'Sacado se recusa a pagar juros ';
    0505: Result := 'Sacado se recusa a pagar comiss�o de perman�ncia';
    0601: Result := 'Sacado est� em regime de concordata';
    0602: Result := 'Sacado est� em regime de fal�ncia';
    0603: Result := 'Sacado alega que mantem entendimentos com o sacador';
    0604: Result := 'Sacado est� em entendimentos com o cedente';
    0605: Result := 'Sacado est� viajando';
    0606: Result := 'Sacado recusou-se a aceitar o t�tulo';
    0607: Result := 'Sacado sustou protesto judicialmente';
    0608: Result := 'Empregado recusou-se a receber o t�tulo';
    0609: Result := 'T�tulo reapresentado ao sacado';
    0610: Result := 'Estamos nos dirigindo ao nosso correspondente';
    0611: Result := 'Correspondente nao se interessa pelo protesto';
    0612: Result := 'Sacado nao atende aos avisos de nossos correspondentes';
    0613: Result := 'T�tulo esta sendo encaminhado ao correspondente';
    0614: Result := 'Entrega franco de pagamento ao sacado';
    0615: Result := 'Entrega franco de pagamento ao representante';
    0616: Result := 'A entrega franco de pagamento � dificil ';
    0617: Result := 'T�tulo recusado pelo cart�rio';
(*
!                !OBS.: Somente serao informadas o codigo de      !
!                !movimento 29.                                   !
!                !O campo complemento podera ser utilizado de     !
!                !acordo com a estrutura de cada banco.           !
*)
    else  Result := '';
  end;
end;

function TFmRetornoBB.TipoDeCarteira(Codigo: String): String;
var
  Cod: Integer;
begin
(*
!-----------------!-----------------------------------------------!
!/22/             !1 - Cobranca Simples.                          !
!CARTEIRA         !2 - Cobranca Vinculada.                        !
!                 !3 - Cobranca Caucionada.                       !
!                 !4 - Cobranca Descontada.                       !
!                 !7 - Cobranca Direta Especial /carteira 17/     !
*-----------------------------------------------------------------*
*)
  Cod := Geral.IMV(Codigo);
  case Cod of
    1: Result := 'Cobran�a simples.';
    2: Result := 'Cobran�a Vinculada.';
    3: Result := 'Cobran�a Caucionada.';
    4: Result := 'Cobran�a Descontada.';
    7: Result := 'Cobran�a Direta Especial (carteira 17)';
    else  Result := '';
  end;
end;

function TFmRetornoBB.TipoDeEmissaoDoBloqueto(Codigo: String): String;
var
  Cod: Integer;
begin
  Cod := Geral.IMV(Codigo);
  case Cod of
(*
!/23/             !1 - Banco emite.                               !
!EMISSAO DO BLO-  !2 - Cliente emite.                             !
!QUETO            !3 - Banco pre-emite e cliente complementa.     !
!                 !4 - Banco reemite.                             !
!                 !5 - Banco nao reemite.                         !
!                 !6 - Cobranca sem Papel.                        !
!                 !Obs.: Os codigos 4 e 5 so serao aceitos para   !
!                 !      codigo de movimento para remessa 31      !
!-----------------!-----------------------------------------------!
*)
    1: Result := 'Banco emite.';
    2: Result := 'Cliente emite.';
    3: Result := 'Banco pr�-emite e cliente complementa.';
    4: Result := 'Banco reemite.';
    5: Result := 'Banco n�o reemite.';
    6: Result := 'Cobran�a sem Papel.';
    else  Result := '';
  end;
end;

function TFmRetornoBB.TipoDeEspecieDoTitulo(Codigo: String): String;
var
  Cod: Integer;
begin
  Cod := Geral.IMV(Codigo);
  case Cod of
(*
!/25/             !01 - CH  Cheque.                               !
!ESPECIE DOS      !02 - DM  Duplicata Mercantil.                  !
!TITULOS          !03 - DMI Duplicata Mercantil p/ Indicacao.     !
!                 !04 - DS  Duplicata de Servico.                 !
!                 !05 - DSI Duplicata de Servico p/ Indicacao.    !
!                 !06 - DR  Duplicata Rural.                      !
!                 !07 - LC  Letra de Cambio.                      !
!                 !08 - NCC Nota de Credito Comercial.            !
!                 !09 - NCE Nota de Credito A Exportacao.         !
!                 !10 - NCI Nota de Credito Industrial.           !
!                 !11 - NCR Nota de Credito Rural.                !
!                 !12 - NP  Nota Promissoria.                     !
!                 !13 - NPR Nota Promissoria Rural.               !
!                 !14 - TM  Triplicata Mercantil.                 !
!                 !15 - TS  Triplicata de Servico.                !
!                 !16 - NS  Nota de Seguro.                       !
!                 !17 - RC  Recibo.                               !
!                 !18 - FAT Fatura.                               !
!                 !19 - ND  Nota de Debito.                       !
!                 !20 - AP  Apolice de Seguro.                    !
!                 !21 - ME  Mensalidade Escolar                   !
!                 !22 - PC  Parcela de Consorcio                  !
!                 !99 - Outros                                    !
!-----------------!-----------------------------------------------!
*)
    01: Result := 'CH  Cheque.';
    02: Result := 'DM  Duplicata Mercantil.';
    03: Result := 'DMI Duplicata Mercantil p/ Indica��o.';
    04: Result := 'DS  Duplicata de Servi�o.';
    05: Result := 'DSI Duplicata de Servi�o p/ Indica��o.';
    06: Result := 'DR  Duplicata Rural.';
    07: Result := 'LC  Letra de C�mbio.';
    08: Result := 'NCC Nota de Cr�dito Comercial.';
    09: Result := 'NCE Nota de Cr�dito A Exporta��o.';
    10: Result := 'NCI Nota de Cr�dito Industrial.';
    11: Result := 'NCR Nota de Cr�dito Rural.';
    12: Result := 'NP  Nota Promiss�ria.';
    13: Result := 'NPR Nota Promiss�ria Rural.';
    14: Result := 'TM  Triplicata Mercantil.';
    15: Result := 'TS  Triplicata de Servi�o.';
    16: Result := 'NS  Nota de Seguro.';
    17: Result := 'RC  Recibo.';
    18: Result := 'FAT Fatura.';
    19: Result := 'ND  Nota de D�bito.';
    20: Result := 'AP  Ap�lice de Seguro.';
    21: Result := 'ME  Mensalidade Escolar';
    22: Result := 'PC  Parcela de Consorcio';
    99: Result := 'Outros';
    else  Result := '';
  end;
end;

function TFmRetornoBB.TipoDeMora(Codigo: String): String;
var
  Cod: Integer;
begin
  Cod := Geral.IMV(Codigo);
  case cod of
(*
!/26/             !1 - Valor por Dia;     2 - Taxa Mensal e       !
!CODIGO DE MORA   !3 - Isento                                     !
*)
    01: Result := 'Valor por Dia.';
    02: Result := 'DM  Taxa Mensal.';
    03: Result := 'Isento.';
    else Result := '';
  end;
end;

function TFmRetornoBB.TipoDeDesconto(Codigo: String): String;
var
  Cod: Integer;
begin
  Cod := Geral.IMV(Codigo);
  case cod of
(*
!/28/             !1 - Valor Fixo ate a data informada.           !
!CODIGO DO DES-   !2 - Percentual ate a data informada.           !
!CONTO            !3 - Valor Por Antecipacao Dia Corrido.         !
!                 !4 - Valor Por Antecipacao Dia Util.            !
!                 !5 - Percentual sobre o Valor Nominal Dia       !
!                 !    Corrido.                                   !
!                 !6 - Percentual sobre o Valor Nominal Dia Util. !
!                 !Obs.: Para os  codigos 1 e 2  sera obrigatorio !
!                 !a informacao da data.                          !
!-----------------!-----------------------------------------------!
*)
    01: Result := 'Valor Fixo at� a data informada.';
    02: Result := 'Percentual at� a data informada.';
    03: Result := 'Valor Por Antecipa��o Dia Corrido.';
    04: Result := 'Valor Por Antecipa��o Dia �til.';
    05: Result := 'Percentual sobre o Valor Nominal Dia Corrido.';
    06: Result := 'Percentual sobre o Valor Nominal Dia �til.';
    else Result := '';
  end;
end;

function TFmRetornoBB.TipoDeMoeda(Codigo: String; Tipo: Integer): String;
var
  Cod: Integer;
begin
  Cod := Geral.IMV(Codigo);
  case Tipo of
    29:
    begin
(*
!-----------------!-----------------------------------------------!
!/29/             !01 - Reservado para uso futuro.                !
!CODIGO DA MOEDA  !02 - Dolar Americano Comercial /Venda/.        !
!                 !03 - Dolar Americano Turismo /Venda/.          !
!                 !04 - ITRD.                                     !
!                 !05 - IDTR.                                     !
!                 !06 - UFIR Diaria.                              !
!                 !07 - UFIR Mensal.                              !
!                 !08 - FAJ-TR.                                   !
!                 !09 - REAL.                                     !
!-----------------!-----------------------------------------------!
*)
      case Cod of
        01: Result := 'Reservado para uso futuro.';
        02: Result := 'D�lar Americano Comercial (Venda).';
        03: Result := 'D�lar Americano Turismo (Venda).';
        04: Result := 'ITRD.';
        05: Result := 'IDTR.';
        06: Result := 'UFIR Di�ria.';
        07: Result := 'UFIR Mensal.';
        08: Result := 'FAJ-TR.';
        09: Result := 'REAL.';
        else Result := '';
      end;
    end;
    else Result := '';
  end;
end;

function TFmRetornoBB.TipoDeInscricao(Codigo: String; Tipo: Integer): String;
var
  Cod: Integer;
begin
  Cod := Geral.IMV(Codigo);
  case Tipo of
    30:
(*
!-----------------!-----------------------------------------------!
!/30/             !1 - CPF; 2 - CGC; 0 - Isento/Nao Informado e   !
!TIPO DE INSCRICAO!9 - Outros.                                    !
!NUMERO DE INCRI- !Se tipo de inscricao igual a zeros /nao        !
! CAO             !informado/, preencher com zeros                !
!-----------------!-----------------------------------------------!
*)
    begin
      case Cod of
        0: Result := 'Isento/N�o Informado';
        1: Result := 'CPF';
        2: Result := 'CGC';
        9: Result := 'Outros';
        else Result := '';
      end;
    end;
    else Result := '';
  end;
end;

function TFmRetornoBB.InformacaoComplementar(Codigo: String; Tipo: Integer;
EMail: Boolean): String;
var
  Cod, Tip: Integer;
begin
  Cod := Geral.IMV(Codigo);
  if (Tipo in ([03,26,30]))      then Tip := 1
  else if (Tipo = 28)            then Tip := 2
  else if (Tipo in ([06,09,17])) then Tip := 3
  else                                Tip := 0;
  //
  case Tip of
    1:
    begin
(*
!-----------------!-----------------------------------------------!
!/42/             !42-A-Codigos de rejeicoes de 01 a 64 associados!
!REJEICOES DE RE- !ao codigo de movimento 03, 26 e 30 /nota 40/   !
!GISTRO DETALHE,  ! 01 - Codigo do banco invalido.                !
!CODIGOS DE TARI- ! 02 - Codigo do registro detalhe invalido.     !
!FAS/CUSTAS E     ! 03 - Codigo do segmento invalido.             !
!ORIGEM DA LIQUI- ! 04 - Codigo do movimento nao permitido para   !
!DACAO/BAIXA      !      carteira.                                !
!                 ! 05 - Codigo de movimento invalido.            !
!                 ! 06 - Tipo/nUmero de inscricao do cedente      !
!                 !      Invalidos.                               !
!                 ! 07 - Agencia/Conta/DV invalido.               !
!                 ! 08 - Nosso nUmero invalido.                   !
!                 ! 09 - Nosso nUmero duplicado.                  !
!                 ! 10 - Carteira invalida.                       !
*)
      case Cod of
        01: Result := 'C�digo do banco inv�lido.';
        02: Result := 'C�digo do registro detalhe inv�lido.';
        03: Result := 'C�digo do segmento inv�lido.';
        04: Result := 'C�digo do movimento nao permitido para carteira.';
        05: Result := 'C�digo de movimento inv�lido.';
        06: Result := 'Tipo/n�mero de inscri��o do cedente inv�lidos.';
        07: Result := 'Ag�ncia/Conta/DV inv�lido.';
        08: Result := 'Nosso n�mero inv�lido.';
        09: Result := 'Nosso n�mero duplicado.';
        10: Result := 'Carteira inv�lida.';
(*
!                 ! 11 - Forma de cadastramento do titulo invalido!
!                 ! 12 - Tipo de documento invalido.              !
!                 ! 13 - Identificacao da emissao do bloqueto     !
!                 !      invalida.                                !
!                 ! 14 - Identificacao da distribuicao do bloqueto!
!                 !      invalida.                                !
!                 ! 15 - Caracteristicas da cobranca incompativeis!
!                 ! 16 - Data de vencimento invalida.             !
!                 ! 17 - Data de vencimento anterior a data de    !
!                 !      emissao.                                 !
!                 ! 18 - Vencimento fora do prazo de operacao.    !
!                 ! 19 - Titulo a cargo de Bancos Correspondentes !
!                 !      com vencimento inferior   XX dias        !
!                 ! 20 - Valor do titulo invalido.                !
*)
        11: Result := 'Forma de cadastramento do t�tulo inv�lido.';
        12: Result := 'Tipo de documento inv�lido.';
        13: Result := 'Identifica��o da emiss�o do bloqueto inv�lida.';
        14: Result := 'Identificacao da distribuicao do bloqueto inv�lida.';
        15: Result := 'Caracter�sticas da cobran�a incompat�veis.';
        16: Result := 'Data de vencimento inv�lida.';
        17: Result := 'Data de vencimento anterior a data de emiss�o.';
        18: Result := 'Vencimento fora do prazo de opera��o.';
        19: Result := 'T�tulo a cargo de Bancos Correspondentes com vencimento inferior   XX dias';
        20: Result := 'Valor do t�tulo inv�lido.';
(*
!                 ! 21 - Especie do titulo invalida.              !
!                 ! 22 - Especie nao permitida para a carteira.   !
!                 ! 23 - Aceite invalido.                         !
!                 ! 24 - Data da emissao invalida.                !
!                 ! 25 - Data da emissao posterior a data         !
!                 ! 26 - Codigo de juros de mora invalido.        !
!                 ! 27 - Valor/Taxa de juros de mora invalido.    !
!                 ! 28 - Codigo do desconto invalido.             !
!                 ! 29 - Valor do desconto maior ou igual ao valor!
!                 ! do titulo.                                    !
!                 ! 30 - Desconto a conceder nao confere.         !
*)
        21: Result := 'Esp�cie do t�tulo inv�lida.';
        22: Result := 'Esp�cie n�o permitida para a carteira.';
        23: Result := 'Aceite inv�lido.';
        24: Result := 'Data da emiss�o inv�lida.';
        25: Result := 'Data da emiss�o posterior a data.';
        26: Result := 'C�digo de juros de mora inv�lido.';
        27: Result := 'Valor/Taxa de juros de mora inv�lido.';
        28: Result := 'C�digo do desconto inv�lido.';
        29: Result := 'Valor do desconto maior ou igual ao valor do t�tulo.';
        30: Result := 'Desconto a conceder n�o confere.';
(*
!                 ! 31 - Concessao de desconto - ja existe        !
!                 !      desconto anterior.                       !
!                 ! 32 - Valor do IOF invalido.                   !
!                 ! 33 - Valor do abatimento invalido.            !
!                 ! 34 - Valor do abatimento maior ou igual ao    !
!                 !      valor do titulo.                         !
!                 ! 35 - Abatimento a conceder nao confere.       !
!                 ! 36 - Concessao de abatimento - ja existe      !
!                 !      abatimento anterior.                     !
!                 ! 37 - Codigo para protesto invalido.           !
!                 ! 38 - Prazo para protesto invalido.            !
!                 ! 39 - Pedido de protesto nao permitido para o  !
!                 !      titulo.                                  !
!                 ! 40 - Titulo com ordem de protesto emitida.    !
*)
        31: Result := 'Concess�o de desconto - j� existe desconto anterior.';
        32: Result := 'Valor do IOF inv�lido.';
        33: Result := 'Valor do abatimento inv�lido.';
        34: Result := 'Valor do abatimento maior ou igual ao valor do t�tulo.';
        35: Result := 'Abatimento a conceder n�o confere.';
        36: Result := 'Concess�o de abatimento - j� existe abatimento anterior.';
        37: Result := 'C�digo para protesto inv�lido.';
        38: Result := 'Prazo para protesto inv�lido.';
        39: Result := 'Pedido de protesto n�o permitido para o t�tulo.';
        40: Result := 'T�tulo com ordem de protesto emitida.';
(*
!                 ! 41 - Pedido de cancelamento/sustacao para     !
!                 !      titulos sem instrucao de protesto.       !
!                 ! 42 - Codigo para baixa/devolucao invalido.    !
!                 ! 43 - Prazo para baixa/devolucao invalido.     !
!                 ! 44 - Codigo da moeda invalido.                !
!                 ! 45 - Nome do sacado nao informado.            !
!                 ! 46 - Tipo/nUmero de inscricao do sacado       !
!                 !      invalidos.                               !
!                 ! 47 - Endereco do sacado nao informado.        !
!                 ! 48 - CEP invalido.                            !
!                 ! 49 - CEP sem praca de cobranca /nao localizado!
!                 ! 50 - CEP referente a um Banco Correspondente. !
*)
        41: Result := 'Pedido de cancelamento/susta��o para t�tulos sem instru��o de protesto.';
        42: Result := 'C�digo para baixa/devolu��o inv�lido.';
        43: Result := 'Prazo para baixa/devolu��o inv�lido.';
        44: Result := 'C�digo da moeda inv�lido.';
        45: Result := 'Nome do sacado n�o informado.';
        46: Result := 'Tipo/n�mero de inscri��o do sacado inv�lidos.';
        47: Result := 'Endere�o do sacado n�o informado.';
        48: Result := 'CEP inv�lido.';
        49: Result := 'CEP sem pra�a de cobran�a /n�o localizado.';
        50: Result := 'CEP referente a um Banco Correspondente.';
(*
!                 ! 51 - CEP incompativel com a unidade da        !
!                 !      federaCcao                               !
!                 ! 52 - Unidade da federacao invalida.           !
!                 ! 53 - Tipo/nUmero de inscricao do sacador/     !
!                 !      avalista invalidos.                      !
!                 ! 54 - Sacador/Avalista nao informado.          !
!                 ! 55 - Nosso nUmero no Banco Correspondente nao !
!                 !      informado.                               !
!                 ! 56 - Codigo do Banco Correspondente nao       !
!                 !      informado.                               !
!                 ! 57 - Codigo da multa invalido.                !
!                 ! 58 - Data da multa invalida.                  !
!                 ! 59 - Valor/Percentual da multa invalido.      !
!                 ! 60 - Movimento para titulo nao cadastrado.    !
*)
        51: Result := 'CEP incompat�el com a unidade da federa��o';
        52: Result := 'Unidade da federa��o inv�lida.';
        53: Result := 'Tipo/n�mero de inscri��o do sacador/avalista inv�lidos.';
        54: Result := 'Sacador/Avalista n�o informado.';
        55: Result := 'Nosso n�mero no Banco Correspondente n�o informado.';
        56: Result := 'C�digo do Banco Correspondente n�o informado.';
        57: Result := 'C�digo da multa inv�lido.';
        58: Result := 'Data da multa inv�lida.';
        59: Result := 'Valor/Percentual da multa inv�lido.';
        60: Result := 'Movimento para t�tulo n�o cadastrado.';
(*
!                 ! 61 - Alteracao da agencia cobradora/dv        !
!                 !      invalida.                                !
!                 ! 62 - Tipo de impressao invalido.              !
!                 ! 63 - Entrada para titulo ja cadastrado.       !
!                 ! 64 - Numero da linha invalido.                !
!                 ! 65 - Codigo do banco para debito invalido.    !
!                 ! 66 - Agencia/conta/DV para debito invalido.   !
!                 ! 67 - Dados para debito incompativel com a     !
!                 !      identificacao da emissao do bloqueto.    !
!                 ! 88 - Arquivo em duplicidade                   !
!                 ! 99 - Contrato inexistente.                    !
*)

        61: Result := 'Altera��o da ag�ncia cobradora/dv inv�lida.';
        62: Result := 'Tipo de impress�o inv�lido.';
        63: Result := 'Entrada para t�tulo j� cadastrado.';
        64: Result := 'N�mero da linha inv�lido.';
        65: Result := 'C�digo do banco para d�bito inv�lido.';
        66: Result := 'Ag�ncia/conta/DV para d�bito inv�lido.';
        67: Result := 'Dados para d�bito incompat�vel com a identifica��o da emiss�o do bloqueto.';
        88: Result := 'Arquivo em duplicidade';
        99: Result := 'Contrato inexistente.';
        else Result := '';
      end;
      if EMail then
      begin
        case Cod of
(*
!                 !                                               !
!                 !Os c�digos abaixo s�o exclusivos do bloqueto   !
!                 !por e-mail                                     !
!                 !46 - Documento de identifica��o do sacado inv�-!
!                 !     lido - bloqueto ser� impresso.            !
!                 !47 - Endere�o e-mail do sacado inv�lido - Blo- !
!                 !     queto ser� impresso.                      !
!                 !14 - Sacado optante por Cobran�a Eletr�nica -  !
!                 !     Bloqueto ser� eletr�nico                  !
!                 !15 - Tipo de conv�nio n�o permite envio de     !
!                 !     e-mail - Bloqueto ser� impresso           !
!                 !15 - Modalidade de cobran�a inv�lida para en-  !
!                 !     vio de e-mail - bloqueto ser� impresso.   !
!                 !13 - Cedente n�o autorizado a enviar e-mail -  !
!                 !     Bloqueto ser� impresso                    !
*)
          46: Result := 'Documento de identifica��o do sacado inv�lido - bloqueto ser� impresso.';
          47: Result := 'Endere�o e-mail do sacado inv�lido - Bloqueto ser� impresso.';
          14: Result := 'Sacado optante por Cobran�a Eletr�nica -  Bloqueto ser� eletr�nico.';
          15: Result := 'Tipo de conv�nio n�o permite envio de e-mail - Bloqueto ser� impresso. '+
                        'Modalidade de cobran�a inv�lida para envio de e-mail - bloqueto ser� impresso.';
          13: Result := 'Cedente n�o autorizado a enviar e-mail - Bloqueto ser� impresso';
        end;
      end;
    end;
    2:
    begin
      case Cod of
(*
!                 !42-B - Codigos de tarifas/custas de 01 a 11    !
!                 !associados ao codigo de movimento 28. /nota 40/!
!                 ! 01 - Tarifa de extrato de posicao.            !
!                 ! 02 - Tarifa de manutencao de titulo vencido.  !
!                 ! 03 - Tarifa de sustacao.                      !
!                 ! 04 - Tarifa de protesto.                      !
!                 ! 05 - Tarifa de outras instrucoes.             !
!                 ! 06 - Tarifa de outras ocorrencias.            !
!                 ! 07 - Tarifa de envio de duplicata ao sacado.  !
!                 ! 08 - Custas de protesto.                      !
!                 ! 09 - Custas de sustacao de protesto.          !
!                 ! 10 - Custas do cartorio distribuidor.         !
!                 ! 11 - Custas de edital.                        !
!-----------------!-----------------------------------------------!
*)
        01: Result := 'Tarifa de extrato de posi��o.';
        02: Result := 'Tarifa de manuten��o de t�tulo vencido.';
        03: Result := 'Tarifa de susta��o.';
        04: Result := 'Tarifa de protesto.';
        05: Result := 'Tarifa de outras instru��es.';
        06: Result := 'Tarifa de outras ocorr�ncias.';
        07: Result := 'Tarifa de envio de duplicata ao sacado.';
        08: Result := 'Custas de protesto.';
        09: Result := 'Custas de susta��o de protesto.';
        10: Result := 'Custas do cart�rio distribuidor.';
        11: Result := 'Custas de edital.';
        else Result := '';
      end;
    end;
    3:
    begin
      case Cod of
(*
!                 !42-C - Codigos de liquidacao/baixa de 01 a 13  !
!                 !associados ao codigo de  movimento 06, 09 e 17 !
!                 !/nota 40/                                      !
!                 ! Liquidacao:                                   !
!                 !   01 - Por saldo.                             !
!                 !   02 - Por conta.                             !
!                 !   03 - No proprio banco.                      !
!                 !   04 - Compensacao eletronica.                !
!                 !   05 - Compensacao convencional.              !
!                 !   06 - Por meio eletronico.                   !
!                 !   07 - Apos feriado local.                    !
!                 !   08 - Em cartorio.                           !
!                 ! Baixa:                                        !
!                 !   09 - Comandada banco.                       !
!                 !   10 - Comandada cliente arquivo.             !
!                 !   11 - Comandada cliente on-line.             !
!                 !   12 - Decurso prazo - cliente.               !
!                 !   13 - Decurso prazo - banco.                 !
!-----------------!-----------------------------------------------!
*)
        // Liquidacao:
        01: Result := 'Por saldo.';
        02: Result := 'Por conta.';
        03: Result := 'No pr�prio banco.';
        04: Result := 'Compensa��o eletr�nica.';
        05: Result := 'Compensa��o convencional.';
        06: Result := 'Por meio eletr�nico.';
        07: Result := 'Ap�s feriado local.';
        08: Result := 'Em cart�rio.';
        // Baixa
        09: Result := 'Comandada banco.';
        10: Result := 'Comandada cliente arquivo.';
        11: Result := 'Comandada cliente on-line.';
        12: Result := 'Decurso prazo - cliente.';
        13: Result := 'Decurso prazo - banco.';
        else Result := '';
      end;
      //else Result := '';
    end;
  end;
end;

procedure TFmRetornoBB.EdOperacaoChange(Sender: TObject);
begin
  EdOperacaotxt.Text := TipoDeOperacao(EdOperacao.Text);
end;

procedure TFmRetornoBB.EdServicoChange(Sender: TObject);
begin
  EdServicotxt.Text := TipoDeServico(EdServico.Text, '04');
end;

procedure TFmRetornoBB.EdFormLancChange(Sender: TObject);
begin
  EdFormLancTxt.Text := FormaDeLancamento(Geral.IMV(EdFormLanc.Text), 39);
end;

procedure TFmRetornoBB.EdBcoCodDetTChange(Sender: TObject);
begin
  if QrBanco.Locate('Codigo', Geral.IMV(EdBcoCodDetT.Text), []) then
    EdBcoNomDetT.Text := QrBancoNome.Value else EdBcoNomDetT.Text := '';
end;

procedure TFmRetornoBB.EdBcoCodDetT2Change(Sender: TObject);
begin
  if QrBanco.Locate('Codigo', Geral.IMV(EdBcoCodDetT2.Text), []) then
    EdBcoNomDetT2.Text := QrBancoNome.Value else EdBcoNomDetT2.Text := '';
end;

procedure TFmRetornoBB.EdBcoCodDetUChange(Sender: TObject);
begin
  if QrBanco.Locate('Codigo', Geral.IMV(EdBcoCodDetU.Text), []) then
    EdBcoNomDetU.Text := QrBancoNome.Value else EdBcoNomDetU.Text := '';
end;

procedure TFmRetornoBB.EdCodMovDetTChange(Sender: TObject);
begin
  EdTexMovDetT.Text := UBancos.CNABTipoDeMovimento(1, ecnabRetorno,
    Geral.IMV(EdCodMovDetT.Text), 0, False, '');
  EdIRTCLB_TXT.Text := InformacaoComplementar(EdIdRTCLDetT.Text,
    Geral.IMV(EdCodMovDetT.Text), False);
end;

procedure TFmRetornoBB.EdCodMovDetUChange(Sender: TObject);
begin
  EdTexMovDetU.Text := UBancos.CNABTipoDeMovimento(1, ecnabRetorno,
    Geral.IMV(EdCodMovDetT.Text), 0, False, '');
end;

procedure TFmRetornoBB.EdCodOcoDetUChange(Sender: TObject);
begin
  EdTexOcoDetU.Text := TipoDeOcorrenciaDoSacado(EdCodOcoDetU.Text);
end;

procedure TFmRetornoBB.EdCBcCoCDetUChange(Sender: TObject);
begin
  if QrBanco.Locate('Codigo', Geral.IMV(EdCBcCoCDetU.Text), []) then
    EdNBcCoCDetU.Text := QrBancoNome.Value else EdNBcCoCDetU.Text := '';
end;

procedure TFmRetornoBB.EdCodMoeDetTChange(Sender: TObject);
begin
  EdTxtMoeDetT.Text := TipoDeMoeda(EdCodMoeDetT.Text, 29);
end;

procedure TFmRetornoBB.EdTipInsDetTChange(Sender: TObject);
begin
  Label86.Caption := TipoDeInscricao(EdTipInsDetT.Text, 30);
end;

procedure TFmRetornoBB.EdCodCarDetTChange(Sender: TObject);
begin
  EdTexCarDetT.Text := TipoDeCarteira(EdCodCarDetT.Text);
end;

procedure TFmRetornoBB.EdIDTiEDetTChange(Sender: TObject);
begin
  ReabreLotesIts(EdIDTiEDetT);
end;

procedure TFmRetornoBB.ReabreLotesIts(Sender: TdmkEdit);
var
  Controle: Integer;
begin
  QrLotesIts.Close;
  Controle := Geral.IMV(Sender.Text);
  if Controle <> 0 then
  begin
    EdLoiControle.Text := IntToStr(Controle);
    QrLotesIts.Params[0].AsInteger := Controle;
    QrLotesIts.Open;
  end else begin
    EdLoiControle.Text := '';
  end;
  // Parei Aqui
end;

procedure TFmRetornoBB.QrLotesItsCalcFields(DataSet: TDataSet);
begin
  case QrLotesItsTIPOLOTE.Value of
    0: QrLotesItsNOMETIPOLOTE.Value := 'Cheque';
    1: QrLotesItsNOMETIPOLOTE.Value := 'Duplicata';
    else QrLotesItsNOMETIPOLOTE.Value := 'Desconhecido';
  end;
  QrLotesItsDATA3_TXT.Value := Geral.FDT(QrLotesItsData3.Value, 2);
  QrLotesItsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrLotesItsCPF.Value);
  //////////////////////////////////////////////////////////////////////////////
  QrLotesItsNOMESTATUS.Value := MLAGeral.NomeStatusPgto2(QrLotesItsQuitado.Value,
    QrLotesItsDDeposito.Value, Date, QrLotesItsData3.Value, QrLotesItsRepassado.Value);
  //////////////////////////////////////////////////////////////////////////////
end;

procedure TFmRetornoBB.EdBcoCodTra5Change(Sender: TObject);
begin
  if QrBanco.Locate('Codigo', Geral.IMV(EdBcoCodTra5.Text), []) then
    EdBcoNomTra5.Text := QrBancoNome.Value else EdBcoNomTra5.Text := '';
end;

procedure TFmRetornoBB.QrCNAB240CalcFields(DataSet: TDataSet);
var
  Envio: TEnvioCNAB;
begin
  Envio := ecnabIndefinido;
  case QrCNAB240Envio.Value of
    1: Envio := ecnabRemessa;
    2: Envio := ecnabRetorno;
  end;
  case QrCNAB240TIPOLOTE.Value of
    0: QrCNAB240NOMETIPOLOTE.Value := 'Cheque';
    1: QrCNAB240NOMETIPOLOTE.Value := 'Duplicata';
    else QrCNAB240NOMETIPOLOTE.Value := 'Desconhecido';
  end;
  QrCNAB240DATA3_TXT.Value := Geral.FDT(QrCNAB240Data3.Value, 2);
  QrCNAB240CPF_TXT.Value := Geral.FormataCNPJ_TT(QrCNAB240CPF.Value);
  //////////////////////////////////////////////////////////////////////////////
  QrCNAB240NOMESTATUS.Value := MLAGeral.NomeStatusPgto2(QrCNAB240Quitado.Value,
    QrCNAB240DDeposito.Value, Date, QrCNAB240Data3.Value, QrCNAB240Repassado.Value);
  //////////////////////////////////////////////////////////////////////////////
  QrCNAB240NOMEMOVIMENTO.Value := UBancos.CNABTipoDeMovimento(1,
    Envio,  QrCNAB240Movimento.Value, 0, False, '');
  QrCNAB240NOMEENVIO.Value := UBancos.CNAB240Envio(QrCNAB240Envio.Value);
  QrCNAB240IRTCLB_TXT.Value := InformacaoComplementar(QrCNAB240IRTCLB.Value,
    QrCNAB240Movimento.Value, False);
  if QrCNAB240Acao.Value = 1 then QrCNAB240NOMEACAO.Value := 'Sim'
  else QrCNAB240NOMEACAO.Value := '';
end;

procedure TFmRetornoBB.EdIdRTCLDetTChange(Sender: TObject);
begin
  EdIRTCLB_TXT.Text := InformacaoComplementar(EdIdRTCLDetT.Text,
    Geral.IMV(EdCodMovDetT.Text), False);
end;

procedure TFmRetornoBB.Liquidacao;
var
  ValQuit: Double;
  ADupPgs, Controle: Integer;
  Data: String;
begin
  if QrCNAB240Quitado.Value > 1 then
  begin
    Application.MessageBox('Esta duplicata j� foi quitada anteriormente!',
      'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    Controle := QrCNAB240Controle.Value;
    ValQuit := QrCNAB240ValPago.Value;
    if ValQuit > 0 then
    begin
      ADupPgs := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
            'ADupPgs', 'ADupPgs', 'Controle');
      Data := Geral.FDT(QrCNAB240DataOcor.Value, 1);
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO aduppgs SET AlterWeb=1, ');
      Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Desco=:P2, Pago=:P3, LotePg=:P4');
      Dmod.QrUpd.SQL.Add(', LotesIts=:Pa, Controle=:Pb');
      Dmod.QrUpd.Params[00].AsString  := Geral.FDT(Date, 1);
      Dmod.QrUpd.Params[01].AsFloat   := 0;
      Dmod.QrUpd.Params[02].AsFloat   := 0;
      Dmod.QrUpd.Params[03].AsFloat   := ValQuit;
      Dmod.QrUpd.Params[04].AsInteger := 0;
      //
      Dmod.QrUpd.Params[05].AsInteger := Controle;
      Dmod.QrUpd.Params[06].AsInteger := ADupPgs;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE cnab240 SET Acao=1');
      Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa');
      Dmod.QrUpd.Params[00].AsInteger := Controle;
      Dmod.QrUpd.ExecSQL;
      //
      CalculaPagamento(Controle);
      //
      QrCNAB240.Close;
      QrCNAB240.Open;
      QrCNAB240.Locate('Controle', Controle, []);
    end;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   ValQuit=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
    Dmod.QrUpd.Params[0].AsFloat   := ValQuit;
    Dmod.QrUpd.Params[1].AsInteger := Controle;
    Dmod.QrUpd.ExecSQL;
    //
    if Application.MessageBox(PChar('Deseja imprimir um recibo com a soma '+
    'dos recebimentos efetuados?'), 'Emiss�o de Recibo', MB_YESNO+MB_ICONQUESTION)
    = ID_YES then ImprimeRecibodeQuitacao;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmRetornoBB.BitBtn4Click(Sender: TObject);
begin
  if QrCNAB240Acao.Value > 0 then
  begin
    Application.MessageBox('Este movimento j� foi executado anteriormente!',
      'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  case QrCNAB240Movimento.Value of
    06: Liquidacao;
    else Application.MessageBox('N�o h� a��o para este movimento!', 'Aviso',
      MB_OK+MB_ICONEXCLAMATION);
  end;
end;

procedure TFmRetornoBB.CalculaPagamento(LotesIts: Integer);
var
  Sit: Integer;
  Cred, Debi: Double;
  Data3, Data4: String;
begin
  QrSumPg.Close;
  QrSumPg.Params[0].AsInteger := LotesIts;
  QrSumPg.Open;
  Cred := QrSumPgPago.Value + QrSumPgDesco.Value;
  Debi := QrSumPgValor.Value + QrSumPgJuros.Value;
  if QrSumPgPago.Value < 0.01 then Sit := 0 else
  begin
    if Cred < (Debi - 0.009) then Sit := 1 else
    begin
      if Cred > (Debi + 0.009) then Sit := 3 else Sit := 2;
    end;
  end;
  //
  // Novo 2007 08 21
  Data4 := Geral.FDT(QrSumPgMaxData.Value, 1);
  if Sit > 1 then Data3 := Data4 else Data3 := '0000-00-00';
  // Fim Novo
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   ');
  Dmod.QrUpd.SQL.Add('TotalJr=:P0, TotalDs=:P1, TotalPg=:P2, ');
  Dmod.QrUpd.SQL.Add('Data3=:P3, Data4=:P4, Quitado=:P5 WHERE Controle=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := QrSumPgJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumPgDesco.Value;
  Dmod.QrUpd.Params[02].AsFloat   := QrSumPgPago.Value;
  Dmod.QrUpd.Params[03].AsString  := Data3;
  Dmod.QrUpd.Params[04].AsString  := Data4;
  Dmod.QrUpd.Params[05].AsInteger := Sit;
  //
  Dmod.QrUpd.Params[06].AsInteger := LotesIts;
  Dmod.QrUpd.ExecSQL;
end;

procedure TFmRetornoBB.ImprimeRecibodeQuitacao;
var
  Pagamento, Texto: String;
begin
  Pagamento := 'Quita��o de d�bitos gerados at� esta data relativos a';
  Texto := Pagamento + ' duplicata n� ' + QrCNAB240Duplicata.Value+
    ' emitida por '+ QrCNAB240Emitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrCNAB240Vencto.Value);
  (*if QrOcorreu.RecordCount > 0 then
  begin
    if QrOcorreu.RecordCount = 1 then Texto := Texto + ' e a ocorr�ncia'
    else Texto := Texto + ' e as ocorr�ncias';
    QrOcorreu.First;
    Liga := ' ';
    while not QrOcorreu.Eof do
    begin
      Texto := Texto + Liga + '"'+QrOcorreuNOMEOCORRENCIA.Value + '"';
      if QrOcorreu.RecNo+1= QrOcorreu.RecordCount then Liga := ' e '
      else liga := ', ';
      QrOcorreu.Next;
    end;
  end;*)
  //
  GOTOy.EmiteRecibo(QrCNAB240Cliente.Value, Dmod.QrMasterDono.Value,
    QrCNAB240ValPago.Value, 0, 0, 'DUX-'+FormatFloat('000000',
    QrCNAB240Controle.Value), Texto, '', '', Date, 0);
end;

procedure TFmRetornoBB.BitBtn5Click(Sender: TObject);
begin
  VaiLoteRetorno(vpFirst);
end;

procedure TFmRetornoBB.BitBtn6Click(Sender: TObject);
begin
  VaiLoteRetorno(vpPrior);
end;

procedure TFmRetornoBB.BitBtn7Click(Sender: TObject);
begin
  VaiLoteRetorno(vpNext);
end;

procedure TFmRetornoBB.BitBtn8Click(Sender: TObject);
begin
  VaiLoteRetorno(vpLast);
end;

procedure TFmRetornoBB.VaiLoteRetorno(Para: TVaiPara);
var
  Atual: Integer;
begin
  if QrCNAB240.State = dsInactive then
  begin
    if Para = vpPrior then Para := vpFirst;
    if Para = vpNext  then Para := vpLast;
    Atual := 0;
  end else Atual := QrCNAB240CNAB240L.Value;
  //
  case Para of
    vpFirst:
    begin
      QrNext.Close;
      QrNext.SQL.Clear;
      QrNext.SQL.Add('SELECT MIN(CNAB240L) CNAB240L');
      QrNext.SQL.Add('FROM cnab240');
      QrNext.Open;
    end;
    vpPrior:
    begin
      QrNext.Close;
      QrNext.SQL.Clear;
      QrNext.SQL.Add('SELECT MAX(CNAB240L) CNAB240L');
      QrNext.SQL.Add('FROM cnab240');
      QrNext.SQL.Add('WHERE CNAB240L < '+IntToStr(Atual));
      QrNext.Open;
    end;
    vpNext:
    begin
      QrNext.Close;
      QrNext.SQL.Clear;
      QrNext.SQL.Add('SELECT MIN(CNAB240L) CNAB240L');
      QrNext.SQL.Add('FROM cnab240');
      QrNext.SQL.Add('WHERE CNAB240L > '+IntToStr(Atual));
      QrNext.Open;
    end;
    vpLast:
    begin
      QrNext.Close;
      QrNext.SQL.Clear;
      QrNext.SQL.Add('SELECT MAX(CNAB240L) CNAB240L');
      QrNext.SQL.Add('FROM cnab240');
      QrNext.Open;
    end;
  end;
  if (Atual <> QrNextCNAB240L.Value) and (QrNextCNAB240L.Value > 0) then
  begin
    QrCNAB240.Close;
    QrCNAB240.Params[0].AsInteger := QrNextCNAB240L.Value;
    QrCNAB240.Open;
  end;  
end;

procedure TFmRetornoBB.BitBtn2Click(Sender: TObject);
begin
  Application.CreateForm(TFmDuplicatas2, FmDuplicatas2);
  FmDuplicatas2.CkStatus.Value :=
    Trunc(Power(2, FmDuplicatas2.CkStatus.Items.Count)-1);
  if QrCNAB240CNAB240L.Value > 0 then
  begin
    FmDuplicatas2.EdControle.Text    := IntToStr(QrCNAB240Controle.Value);
    FmDuplicatas2.ReabrirTabelas;
  end;
  FmDuplicatas2.ShowModal;
  FmDuplicatas2.Destroy;
end;

procedure TFmRetornoBB.BitBtn9Click(Sender: TObject);
begin
  VaiRegistro(-2, False);
end;

procedure TFmRetornoBB.BitBtn10Click(Sender: TObject);
begin
  VaiRegistro(-1, False);
end;

procedure TFmRetornoBB.BitBtn11Click(Sender: TObject);
begin
  VaiRegistro(+1, False);
end;

procedure TFmRetornoBB.BitBtn12Click(Sender: TObject);
begin
  VaiRegistro(+2, False);
end;

procedure TFmRetornoBB.VaiRegistro(Item: Integer; InsereSQL: Boolean);
var
  LoteN, LoteA, LoteF, i, f: Integer;
begin
  LoteN := 0;
  LoteA := Geral.IMV(Copy(LaRegistroB.Caption, 1, 5));
  LoteF := GradeB.RowCount - 1;
  case Item of
    -2: LoteN := 0;
    -1: LoteN := LoteA - 1;
    +1: LoteN := LoteA + 1;
    +2: LoteN := LoteF;
  end;
  if LoteN < 1 then LoteN := 1;
  if LoteN > LoteF then LoteN := LoteF;
  //
  i := Geral.IMV(GradeB.Cells[1, LoteN]);
  f := Geral.IMV(GradeB.Cells[2, LoteN]);
  LeRegistro(i, f, LoteN, 0);
end;

procedure TFmRetornoBB.GradeBClick(Sender: TObject);
begin
  //Memo2.Lines.Add(Memo1.Lines[Geral.IMV(GradeB.Cells[1, GradeB.Row])]);
end;

procedure TFmRetornoBB.BtGravaClick(Sender: TObject);
var
  i, j, Ai, Af, Bi, Bf, ini, fim, Lote: Integer;
begin
  QrDup1.Close;
  QrDup1.Params[00].AsInteger := Geral.IMV(EdSeqArq.Text);
  QrDup1.Params[01].AsString  := EdConvenioL.Text;
  QrDup1.Open;
  if QrDup1.RecordCount > 0 then
  begin
    Application.MessageBox(PChar('Este arquivo j� foi gravado com com lote '+
    'de arquivo retorno '+ IntToStr(QrDup1CNAB240L.Value)+'!'),
    'Aviso de Duplicidade', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  Ai := Geral.IMV(GradeA.Cells[0, 1]);
  Af := Geral.IMV(GradeA.Cells[0, GradeA.RowCount-1]);
  Lote := 0;
  for i := Ai to Af do
  begin
    LeLote(Geral.IMV(GradeA.Cells[1,i]));
    Bi := Geral.IMV(GradeB.Cells[0, 1]);
    Bf := Geral.IMV(GradeB.Cells[0, GradeB.RowCount-1]);
    for j := Bi to Bf do
    begin
      ini := Geral.IMV(GradeB.Cells[1, j]);
      fim := Geral.IMV(GradeB.Cells[2, j]);
      if (ini > 0) and (fim > 0) then
      begin
        if Lote = 0 then Lote := UMyMod.BuscaEmLivreY(Dmod.MyDB,
          'Livres', 'Controle', 'CNAB240', 'CNAB240L', 'CNAB240L');
        LeRegistro(ini, fim, j, Lote);
      end else begin
        Application.MessageBox('Lote sem movimento!', 'Aviso',
          MB_OK+MB_ICONINFORMATION);
      end;
    end;
  end;
  BtGrava.Enabled := False;
  PageControl1.ActivePageIndex := 0;
  VaiLoteRetorno(vpLast);
  Screen.Cursor := crDefault;
end;

procedure TFmRetornoBB.UpdateCursorPos;
var
  t, c, r, n: Integer;
begin
  with Memo1.CaretPos do
  begin
    c := x;
    r := y;
    t := Memo1.SelLength;
    if t > 0 then
    begin
      c := FPosIniCol;
      r := FPosIniRow;
    end;
    n := 2 * (y - r);
    t := t - n;
    //
    StatusBar.Panels[1].Text := IntToStr(c+1);
    StatusBar.Panels[3].Text := IntToStr(r+1);
    StatusBar.Panels[5].Text := IntToStr(x+1);
    StatusBar.Panels[7].Text := IntToStr(y+1);
    StatusBar.Panels[9].Text := IntToStr(t);
  end;
end;

procedure TFmRetornoBB.FormShow(Sender: TObject);
begin
  UpdateCursorPos;
end;

procedure TFmRetornoBB.Memo1Change(Sender: TObject);
begin
  UpdateCursorPos;
end;

procedure TFmRetornoBB.Memo1KeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_SHIFT then
    FShiftPressed := False;
  UpdateCursorPos;
end;

procedure TFmRetornoBB.Memo1Click(Sender: TObject);
begin
  UpdateCursorPos;
end;

procedure TFmRetornoBB.Memo1Enter(Sender: TObject);
begin
  UpdateCursorPos;
end;

procedure TFmRetornoBB.Memo1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_SHIFT then
  begin
    FShiftPressed := True;
    FPosIniCol := Memo1.CaretPos.X;
    FPosIniRow := Memo1.CaretPos.Y;
    FPosIniTam := Memo1.SelStart;
  end
end;

procedure TFmRetornoBB.EdBcoCodDetPChange(Sender: TObject);
begin
  if QrBanco.Locate('Codigo', Geral.IMV(EdBcoCodDetP.Text), []) then
    EdBcoNomDetP.Text := QrBancoNome.Value else EdBcoNomDetP.Text := '';
end;

procedure TFmRetornoBB.EdCodMovDetPChange(Sender: TObject);
begin
  EdTexMovDetP.Text := UBancos.CNABTipoDeMovimento(1, ecnabRemessa,
    Geral.IMV(EdCodMovDetP.Text), 0, False, '');
  // Parei Aqui
  (*EdIRTCLB_TxP.Text := InformacaoComplementar(EdIdRTCLDetP.Text,
    Geral.IMV(EdCodMovDetP.Text), False);*)
end;

procedure TFmRetornoBB.EdCodCarDetPChange(Sender: TObject);
begin
  EdTexCarDetP.Text := TipoDeCarteira(EdCodCarDetP.Text);
end;

procedure TFmRetornoBB.EdForCadTitPChange(Sender: TObject);
var
  i: Integer;
begin
  i := Geral.IMV(EdForCadTitP.Text);
  case i of
    1: EdNomCadTitP.Text := 'Com cadastro.';
    2: EdNomCadTitP.Text := 'Sem cadastro.';
    else EdNomCadTitP.Text := '';
  end;
end;

procedure TFmRetornoBB.EdTipoDocumPChange(Sender: TObject);
var
  i: Integer;
begin
  i := Geral.IMV(EdTipoDocumP.Text);
  case i of
    1: EdNomeDocumP.Text := 'Tradicional.';
    2: EdNomeDocumP.Text := 'Escritural.';
    else EdNomeDocumP.Text := '';
  end;
end;

procedure TFmRetornoBB.EdIDEmitBloPChange(Sender: TObject);
begin
  EdNoEmitBloP.Text := TipoDeEmissaoDoBloqueto(EdIDEmitBloP.Text);
end;

procedure TFmRetornoBB.EdIDDistribPChange(Sender: TObject);
var
  i: Integer;
begin
  i := Geral.IMV(EdIDDistribP.Text);
  case i of
    1: EdNoDistribP.Text := 'Banco.';
    2: EdNoDistribP.Text := 'Cliente.';
    else EdNoDistribP.Text := '';
  end;
end;

procedure TFmRetornoBB.EdEspeciTitPChange(Sender: TObject);
begin
  EdNomEspTitP.Text := TipoDeEspecieDoTitulo(EdEspeciTitP.Text);
end;

procedure TFmRetornoBB.EdIDTitAceiPChange(Sender: TObject);
begin
  if EdIDTitAceiP.Text = 'S' then EdNoTitAceiP.Text := 'Aceite.' else
  if EdIDTitAceiP.Text = 'N' then EdNoTitAceiP.Text := 'N�o aceite.' else
                                  EdNoTitAceiP.Text := '';
end;

procedure TFmRetornoBB.EdCodJurMorPChange(Sender: TObject);
begin
  EdNomJurMorP.Text := TipodeMora(EdCodJurMorP.Text);
end;

procedure TFmRetornoBB.EdCodDesco1PChange(Sender: TObject);
begin
  EdNomDesco1P.Text := TipoDeDesconto(EdCodDesco1P.Text);
end;

procedure TFmRetornoBB.EdCodParProPChange(Sender: TObject);
var
  i: Integer;
begin
  i := Geral.IMV(EdCodParProP.Text);
  case i of
    1: EdNomParProP.Text := 'Dias corridos.';
    2: EdNomParProP.Text := 'Dias �teis';
    3: EdNomParProP.Text := 'N�o protestar.';
    else EdNomParProP.Text := '';
  end;
end;

procedure TFmRetornoBB.EdCodParBaiPChange(Sender: TObject);
var
  i: Integer;
begin
  i := Geral.IMV(EdCodParBaiP.Text);
  case i of
    1: EdNomParBaiP.Text := 'Baixar / devolver';
    2: EdNomParBaiP.Text := 'N�o baixar / n�o devolver';
    else EdNomParBaiP.Text := '';
  end;
end;

procedure TFmRetornoBB.EdIDTiEDetPChange(Sender: TObject);
begin
  ReabreLotesIts(EdIDTiEDetP);
end;

procedure TFmRetornoBB.EdBcoCodDetQChange(Sender: TObject);
begin
  if QrBanco.Locate('Codigo', Geral.IMV(EdBcoCodDetQ.Text), []) then
    EdBcoNomDetQ.Text := QrBancoNome.Value else EdBcoNomDetQ.Text := '';
end;

procedure TFmRetornoBB.EdCodMovDetQChange(Sender: TObject);
begin
  EdTexMovDetQ.Text := UBancos.CNABTipoDeMovimento(1, ecnabRemessa,
    Geral.IMV(EdCodMovDetQ.Text), 0, False, '');
end;

procedure TFmRetornoBB.EdEmiBCCDetQChange(Sender: TObject);
begin
  if QrBanco.Locate('Codigo', Geral.IMV(EdEmiBCCDetQ.Text), []) then
    EdEmiNBCDetQ.Text := QrBancoNome.Value else EdEmiNBCDetQ.Text := '';
end;

procedure TFmRetornoBB.EdBcoCodDetRChange(Sender: TObject);
begin
  if QrBanco.Locate('Codigo', Geral.IMV(EdBcoCodDetR.Text), []) then
    EdBcoNomDetR.Text := QrBancoNome.Value else EdBcoNomDetR.Text := '';
end;

procedure TFmRetornoBB.EdCodMovDetRChange(Sender: TObject);
begin
  EdTexMovDetR.Text := UBancos.CNABTipoDeMovimento(1, ecnabRemessa,
    Geral.IMV(EdCodMovDetR.Text), 0, False, '');
end;

procedure TFmRetornoBB.EdCodDesco2RChange(Sender: TObject);
begin
  EdNomDesco2R.Text := TipoDeDesconto(EdCodDesco2R.Text);
end;

procedure TFmRetornoBB.EdCodDesco3RChange(Sender: TObject);
begin
  EdNomDesco3R.Text := TipoDeDesconto(EdCodDesco3R.Text);
end;

procedure TFmRetornoBB.EdCodiMultaRChange(Sender: TObject);
var
  i: Integer;
begin
  i := Geral.IMV(EdCodiMultaR.Text);
  case i of
    1: EdNomeMultaR.Text := 'Valor fixo.';
    2: EdNomeMultaR.Text := 'Percentual';
    else EdNomeMultaR.Text := '';
  end;
end;

procedure TFmRetornoBB.EdConDebBcoRChange(Sender: TObject);
begin
  if QrBanco.Locate('Codigo', Geral.IMV(EdConDebBcoR.Text), []) then
    EdConDebBNoR.Text := QrBancoNome.Value else EdConDebBNoR.Text := '';
end;

procedure TFmRetornoBB.DescreveCaracteres(Arquivo: String);
const
  TamLine = 240;
var
  s: String;
  IEDCBR: TextFile;
  i, c: Integer;
begin
  Screen.Cursor := crHourGlass;
  Memo3.Lines.Clear;
  if not FileExists(Arquivo) then
  begin
    Application.MessageBox(PChar('O arquivo "'+Name+'" n�o foi localizado!'),
      'Erro', MB_OK+MB_ICONERROR);
    Screen.Cursor := crDefault;
    Exit;
  end;
  AssignFile(IEDCBR, Arquivo);
  Reset(IEDCBR);
  c := 0;
  while not Eof(IEDCBR) do
  begin
    Readln(IEDCBR, s);
    for i := 1 to Length(s) do
    begin
      Memo3.Lines.Add('L'+FormatFloat('000', c) + ' P'+ FormatFloat('0000', i) +
      ' = '+ FormatFloat('000', Ord(s[i]))+' ' + s[i]);
    end;
    c := c + 1;
  end;
  CloseFile(IEDCBR);
  Screen.Cursor := crDefault;
end;

procedure TFmRetornoBB.SpeedButton5Click(Sender: TObject);
begin
  OpenDialog1.InitialDir := Dmod.QrControlePathBBRet.Value;
  if OpenDialog1.Execute then
  begin
    Screen.Cursor := crHourGlass;
    PageControl1.ActivePageIndex := 3;
    DescreveCaracteres(OpenDialog1.FileName);
    Screen.Cursor := crDefault;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmRetornoBB.EdTipoServicoChange(Sender: TObject);
begin
  // Cobran�a sem papel
end;

end.

