unit MortoLotes;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, Db, mySQLDbTables;

type
  TFmMortoLotes = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    BtCancela: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Label137: TLabel;
    TPLimite: TDateTimePicker;
    QrLotes: TmySQLQuery;
    QrLotesCodigo: TIntegerField;
    QrLotesTipo: TSmallintField;
    QrLotesSpread: TSmallintField;
    QrLotesCliente: TIntegerField;
    QrLotesLote: TIntegerField;
    QrLotesData: TDateField;
    QrLotesTotal: TFloatField;
    QrLotesDias: TFloatField;
    QrLotesPeCompra: TFloatField;
    QrLotesTxCompra: TFloatField;
    QrLotesValValorem: TFloatField;
    QrLotesAdValorem: TFloatField;
    QrLotesIOC: TFloatField;
    QrLotesIOC_VAL: TFloatField;
    QrLotesTarifas: TFloatField;
    QrLotesCPMF: TFloatField;
    QrLotesCPMF_VAL: TFloatField;
    QrLotesTipoAdV: TIntegerField;
    QrLotesIRRF: TFloatField;
    QrLotesIRRF_Val: TFloatField;
    QrLotesISS: TFloatField;
    QrLotesISS_Val: TFloatField;
    QrLotesPIS: TFloatField;
    QrLotesPIS_Val: TFloatField;
    QrLotesPIS_R: TFloatField;
    QrLotesPIS_R_Val: TFloatField;
    QrLotesCOFINS: TFloatField;
    QrLotesCOFINS_Val: TFloatField;
    QrLotesCOFINS_R: TFloatField;
    QrLotesCOFINS_R_Val: TFloatField;
    QrLotesOcorP: TFloatField;
    QrLotesMaxVencto: TDateField;
    QrLotesCHDevPg: TFloatField;
    QrLotesLk: TIntegerField;
    QrLotesDataCad: TDateField;
    QrLotesDataAlt: TDateField;
    QrLotesUserCad: TIntegerField;
    QrLotesUserAlt: TIntegerField;
    QrLotesMINTC: TFloatField;
    QrLotesMINAV: TFloatField;
    QrLotesMINTC_AM: TFloatField;
    QrLotesPIS_T_Val: TFloatField;
    QrLotesCOFINS_T_Val: TFloatField;
    QrLotesPgLiq: TFloatField;
    QrLotesDUDevPg: TFloatField;
    QrLotesNF: TIntegerField;
    QrLotesItens: TIntegerField;
    QrLotesConferido: TIntegerField;
    QrLotesECartaSac: TSmallintField;
    QrLotesCBE: TIntegerField;
    QrLotesSCB: TIntegerField;
    QrLotesIts: TmySQLQuery;
    procedure BtSaidaClick(Sender: TObject);
    procedure BtCancelaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmMortoLotes: TFmMortoLotes;

implementation

{$R *.DFM}

uses Module, UnMyObjects;

procedure TFmMortoLotes.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMortoLotes.BtCancelaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMortoLotes.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmMortoLotes.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmMortoLotes.FormCreate(Sender: TObject);
var
  Limite: TDateTime;
begin
  Limite := Int(Date) - 90;
  TPLimite.Date    := Limite;
  TPLimite.MaxDate := Limite;
end;

end.
