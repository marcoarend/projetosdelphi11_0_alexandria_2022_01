unit Decomp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit, dmkGeral, UnDmkEnums;

type
  TFmDecomp = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Label11: TLabel;
    EdJuros: TdmkEdit;
    Label1: TLabel;
    EdPrazo: TdmkEdit;
    EdCasas: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdTaxa: TdmkEdit;
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmDecomp: TFmDecomp;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmDecomp.BtOKClick(Sender: TObject);
var
  Casas: Integer;
  Juros, Prazo, TaxaM: Double;
begin
  Casas := Geral.IMV(EdCasas.Text);
  Juros := Geral.DMV(EdJuros.Text);
  Prazo := Geral.DMV(EdPrazo.Text);
  //
  TaxaM := MLAGeral.DescobreJuroComposto(Juros, Prazo, Casas);
  EdTaxa.DecimalSize := Casas;
  EdTaxa.Text := Geral.FFT(TaxaM, Casas, siNegativo);
end;

procedure TFmDecomp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDecomp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmDecomp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
