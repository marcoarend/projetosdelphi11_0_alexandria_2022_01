object FmMaloteConf: TFmMaloteConf
  Left = 329
  Top = 170
  Caption = 'Confer'#234'ncia de Malotes de Cheques'
  ClientHeight = 481
  ClientWidth = 795
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 40
    Width = 795
    Height = 393
    Align = alClient
    TabOrder = 0
    object PainelConfig: TPanel
      Left = 1
      Top = 1
      Width = 793
      Height = 116
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 12
        Top = 4
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object EdCliente: TdmkEditCB
        Left = 12
        Top = 20
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdClienteChange
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 80
        Top = 20
        Width = 433
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTIDADE'
        ListSource = DsTransportadoras
        TabOrder = 1
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGOQue: TRadioGroup
        Left = 658
        Top = 1
        Width = 134
        Height = 114
        Align = alRight
        Caption = ' Conferir: '
        ItemIndex = 0
        Items.Strings = (
          'CMC-7 e Valor'
          'S'#243' o CMC-7'
          'S'#243' o Valor')
        TabOrder = 4
        OnClick = RGOQueClick
      end
      object RGStatus: TRadioGroup
        Left = 524
        Top = 1
        Width = 134
        Height = 114
        Align = alRight
        Caption = ' Status: '
        ItemIndex = 0
        Items.Strings = (
          'N'#227'o conferido'
          'J'#225' conferido'
          'Ambos')
        TabOrder = 3
        OnClick = RGStatusClick
      end
      object GroupBox1: TGroupBox
        Left = 12
        Top = 44
        Width = 501
        Height = 65
        Caption = '   '
        TabOrder = 2
        object Label34: TLabel
          Left = 8
          Top = 20
          Width = 55
          Height = 13
          Caption = 'Data inicial:'
        end
        object Label2: TLabel
          Left = 112
          Top = 20
          Width = 48
          Height = 13
          Caption = 'Data final:'
        end
        object CkPeriodo: TCheckBox
          Left = 12
          Top = 0
          Width = 65
          Height = 17
          Caption = 'Per'#237'odo: '
          TabOrder = 0
          OnClick = CkPeriodoClick
        end
        object TPIni: TDateTimePicker
          Left = 8
          Top = 36
          Width = 101
          Height = 21
          Date = 38675.714976851900000000
          Time = 38675.714976851900000000
          TabOrder = 1
          OnChange = TPIniChange
        end
        object TPFim: TDateTimePicker
          Left = 112
          Top = 36
          Width = 101
          Height = 21
          Date = 38675.714976851900000000
          Time = 38675.714976851900000000
          TabOrder = 2
          OnChange = TPFimChange
        end
      end
    end
    object PainelConfere: TPanel
      Left = 1
      Top = 145
      Width = 793
      Height = 247
      Align = alBottom
      TabOrder = 1
      Visible = False
      object Panel3: TPanel
        Left = 1
        Top = 1
        Width = 791
        Height = 88
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label36: TLabel
          Left = 8
          Top = 4
          Width = 143
          Height = 13
          Caption = 'Leitura pela banda magn'#233'tica:'
        end
        object Label7: TLabel
          Left = 8
          Top = 44
          Width = 74
          Height = 13
          Caption = 'Nome emitente:'
        end
        object Label3: TLabel
          Left = 296
          Top = 4
          Width = 34
          Height = 13
          Caption = 'Banco:'
        end
        object Label4: TLabel
          Left = 340
          Top = 4
          Width = 42
          Height = 13
          Caption = 'Ag'#234'ncia:'
        end
        object Label5: TLabel
          Left = 392
          Top = 4
          Width = 31
          Height = 13
          Caption = 'Conta:'
        end
        object Label32: TLabel
          Left = 508
          Top = 4
          Width = 40
          Height = 13
          Caption = 'Cheque:'
        end
        object Label6: TLabel
          Left = 576
          Top = 4
          Width = 67
          Height = 13
          Caption = 'CPF Emitente:'
        end
        object Label11: TLabel
          Left = 692
          Top = 4
          Width = 27
          Height = 13
          Caption = 'Valor:'
        end
        object Label8: TLabel
          Left = 648
          Top = 44
          Width = 124
          Height = 13
          Caption = 'Itens e valores conferidos:'
        end
        object EdBanda: TdmkEdit
          Left = 8
          Top = 20
          Width = 285
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 34
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
          OnChange = EdBandaChange
        end
        object EdEmitente: TdmkEdit
          Left = 8
          Top = 60
          Width = 637
          Height = 21
          Enabled = False
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdBanco: TdmkEdit
          Left = 296
          Top = 20
          Width = 41
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdAgencia: TdmkEdit
          Left = 340
          Top = 20
          Width = 49
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 4
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdConta: TdmkEdit
          Left = 392
          Top = 20
          Width = 113
          Height = 21
          Enabled = False
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdCheque: TdmkEdit
          Left = 508
          Top = 20
          Width = 65
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 6
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
        end
        object EdCPF: TdmkEdit
          Left = 576
          Top = 20
          Width = 113
          Height = 21
          Enabled = False
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object EdValor: TdmkEdit
          Left = 692
          Top = 20
          Width = 89
          Height = 21
          Alignment = taRightJustify
          TabOrder = 7
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
          OnExit = EdValorExit
        end
        object DBEdItens: TDBEdit
          Left = 648
          Top = 60
          Width = 41
          Height = 21
          TabStop = False
          DataField = 'CHEQUES'
          DataSource = DsSumM
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 8
          OnChange = DBEdItensChange
        end
        object DBEdValor: TDBEdit
          Left = 692
          Top = 60
          Width = 89
          Height = 21
          TabStop = False
          DataField = 'VALOR'
          DataSource = DsSumM
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 9
          OnChange = DBEdValorChange
        end
      end
      object DBG1: TDBGrid
        Left = 1
        Top = 89
        Width = 791
        Height = 157
        Align = alClient
        DataSource = DsLotesIts
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgCancelOnExit]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnCellClick = DBG1CellClick
        OnDrawColumnCell = DBG1DrawColumnCell
        Columns = <
          item
            Expanded = False
            FieldName = 'LOCCHEQUE'
            Title.Caption = 'CH'
            Width = 18
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CONF_V'
            Title.Caption = 'R$'
            Width = 18
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Width = 49
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Banco'
            Title.Caption = 'Bco'
            Width = 28
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Agencia'
            Title.Caption = 'Ag.'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Conta'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cheque'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CPF'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Emitente'
            Width = 220
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Width = 80
            Visible = True
          end>
      end
    end
    object DBG2: TDBGrid
      Left = 1
      Top = 117
      Width = 793
      Height = 28
      Align = alClient
      DataSource = DsLotes
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 412
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'C'#243'digo'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Total'
          Title.Caption = 'Valor'
          Width = 84
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Itens'
          Width = 36
          Visible = True
        end>
    end
  end
  object Painel1: TPanel
    Left = 0
    Top = 433
    Width = 795
    Height = 48
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object PainelConfirma: TPanel
      Left = 0
      Top = 0
      Width = 795
      Height = 48
      Align = alClient
      TabOrder = 1
      Visible = False
      object BtRatifica: TBitBtn
        Tag = 166
        Left = 14
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Ratifica'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtRatificaClick
      end
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 686
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtDesisteClick
      end
      object BtDesfaz: TBitBtn
        Tag = 167
        Left = 198
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desfaz'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtDesfazClick
      end
    end
    object PainelControle: TPanel
      Left = 0
      Top = 0
      Width = 795
      Height = 48
      Align = alClient
      TabOrder = 0
      object BtConfere: TBitBtn
        Tag = 168
        Left = 14
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Conferir'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfereClick
      end
      object BtSair: TBitBtn
        Tag = 13
        Left = 682
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sair'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtSairClick
      end
      object BtLimpar: TBitBtn
        Tag = 169
        Left = 110
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Limpar'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtLimparClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 795
    Height = 40
    Align = alTop
    Caption = 'Confer'#234'ncia de Malotes de Cheques'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 793
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 36
    end
  end
  object DsTransportadoras: TDataSource
    DataSet = QrClientes
    Left = 340
    Top = 58
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE 1 WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE, Codigo'
      'FROM entidades '
      'WHERE Cliente1="V"'
      'ORDER BY NOMEENTIDADE')
    Left = 312
    Top = 58
    object QrClientesNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrLotes: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrLotesAfterScroll
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLI, lo.Codigo, lo.Cliente, '
      'lo.Lote, lo.Data, lo.Total, lo.Itens'
      'FROM lotes lo'
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente'
      'WHERE lo.Tipo=0'
      'AND lo.Conferido=0'
      'AND lo.Codigo>0'
      'ORDER BY NOMECLI, Lote DESC')
    Left = 285
    Top = 109
    object QrLotesNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrLotesCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLotesCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrLotesLote: TSmallintField
      FieldName = 'Lote'
      Required = True
    end
    object QrLotesData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotesTotal: TFloatField
      FieldName = 'Total'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotesItens: TIntegerField
      FieldName = 'Itens'
      Required = True
    end
  end
  object DsLotes: TDataSource
    DataSet = QrLotes
    Left = 313
    Top = 109
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Controle, CPF, Emitente, Valor FROM lotesits'
      'WHERE Codigo=:P0'
      'AND Banco=:P1'
      'AND Agencia=:P2 '
      'AND Conta=:P3'
      'AND Cheque=:P4')
    Left = 141
    Top = 160
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrLocCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLocEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLocValor: TFloatField
      FieldName = 'Valor'
    end
    object QrLocCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrLotesIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLotesItsCalcFields
    SQL.Strings = (
      'SELECT ma.Valor VALORMALOTE, ma.Cheque LOCCHEQUE, '
      
        'li.Controle, li.Banco, li.Agencia, li.Conta, li.Cheque, li.Codig' +
        'o,'
      'li.CPF, li.Emitente, li.Valor'
      'FROM lotesits li'
      'LEFT JOIN malotes ma ON ma.Controle=li.Controle'
      'WHERE li.Codigo=:P0'
      '')
    Left = 341
    Top = 109
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotesItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrLotesItsBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrLotesItsAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrLotesItsConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrLotesItsCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrLotesItsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLotesItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLotesItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotesItsCONF_V: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CONF_V'
      Calculated = True
    end
    object QrLotesItsDADOS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DADOS'
      Size = 255
      Calculated = True
    end
    object QrLotesItsVALORMALOTE: TFloatField
      FieldName = 'VALORMALOTE'
    end
    object QrLotesItsLOCCHEQUE: TIntegerField
      FieldName = 'LOCCHEQUE'
    end
    object QrLotesItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsLotesIts: TDataSource
    DataSet = QrLotesIts
    Left = 369
    Top = 109
  end
  object QrSumM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ma.Valor) VALOR, SUM(ma.Cheque) CHEQUES'
      'FROM malotes ma '
      'WHERE ma.Codigo=:P0'
      '')
    Left = 397
    Top = 109
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumMVALOR: TFloatField
      FieldName = 'VALOR'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSumMCHEQUES: TFloatField
      FieldName = 'CHEQUES'
    end
  end
  object DsSumM: TDataSource
    DataSet = QrSumM
    Left = 425
    Top = 109
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 300
    OnTimer = Timer1Timer
    Left = 321
    Top = 327
  end
end
