object FmPdxOrfao: TFmPdxOrfao
  Left = 390
  Top = 198
  Width = 480
  Height = 344
  Caption = 'Registros �rf�os'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 472
    Height = 214
    Align = alClient
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 470
      Height = 195
      Align = alClient
      DataSource = DsOrfaos
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object Progress: TProgressBar
      Left = 1
      Top = 196
      Width = 470
      Height = 17
      Align = alBottom
      Min = 0
      Max = 100
      TabOrder = 1
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 262
    Width = 472
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtPesquisa: TBitBtn
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Pesquisa'
      TabOrder = 0
      OnClick = BtPesquisaClick
      NumGlyphs = 2
    end
    object BtSaida: TBitBtn
      Left = 362
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa�da'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
      NumGlyphs = 2
    end
    object BtExclui: TBitBtn
      Left = 192
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Exclui'
      TabOrder = 2
      OnClick = BtExcluiClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TLMDPanelFill
    Left = 0
    Top = 0
    Width = 472
    Height = 48
    Align = alTop
    Bevel.StyleInner = bvFrameRaised
    Bevel.BorderColor = clWindow
    Bevel.EdgeStyle = etBump
    Bevel.Mode = bmCustom
    Caption = 'Registros �rf�os'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    Transparent = True
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 468
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object QrMovto2: TQuery
    DatabaseName = 'DBNPdx'
    SQL.Strings = (
      'SELECT Codigo, Controle'
      'FROM movto2')
    Left = 220
    Top = 96
    object QrMovto2Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBNPDX."Movto2.DB".Codigo'
    end
    object QrMovto2Controle: TIntegerField
      FieldName = 'Controle'
      Origin = 'DBNPDX."Movto2.DB".Controle'
    end
    object QrMovto2Existe: TIntegerField
      FieldKind = fkLookup
      FieldName = 'Existe'
      LookupDataSet = QrLotesIts
      LookupKeyFields = 'Controle'
      LookupResultField = 'Controle'
      KeyFields = 'Controle'
      Lookup = True
    end
  end
  object QrLotesIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Controle'
      'FROM lotesits')
    Left = 168
    Top = 96
    object QrLotesItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLotesItsControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrOrfaos: TQuery
    DatabaseName = 'DBNPdx'
    SQL.Strings = (
      'SELECT * FROM movto2'
      'WHERE Orfao=1')
    Left = 224
    Top = 152
    object QrOrfaosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'DBNPDX."Movto2.DB".Codigo'
    end
    object QrOrfaosControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'DBNPDX."Movto2.DB".Controle'
    end
    object QrOrfaosNumA: TFloatField
      FieldName = 'NumA'
      Origin = 'DBNPDX."Movto2.DB".NumA'
    end
    object QrOrfaosNumAV: TFloatField
      FieldName = 'NumAV'
      Origin = 'DBNPDX."Movto2.DB".NumAV'
    end
  end
  object DsOrfaos: TDataSource
    DataSet = QrOrfaos
    Left = 252
    Top = 152
  end
end

