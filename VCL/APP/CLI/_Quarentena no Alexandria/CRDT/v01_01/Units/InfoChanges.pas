unit InfoChanges;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, dmkGeral;

type
  TFmInfoChanges = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    TreeView1: TTreeView;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmInfoChanges: TFmInfoChanges;

implementation

{$R *.DFM}

uses MyListas, UnMyObjects;

procedure TFmInfoChanges.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmInfoChanges.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmInfoChanges.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmInfoChanges.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  for i := 0 to TreeView1.Items.Count -1 do
  begin
    if Geral.SoNumero_TT(TreeView1.Items[i].Text) = '' then
      TreeView1.Items[i].Expanded := True;
    if Geral.IMV(Geral.SoNumero_TT(TreeView1.Items[i].Text)) =
       CO_VERSAO then
       TreeView1.Items[i].Expanded := True;
  end;
end;

end.
