unit OcorreuC;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, Grids,
  DBGrids, Mask, ComCtrls, Menus, Variants, dmkGeral, frxClass, frxDBSet,
  dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkCheckGroup, UnDmkProcFunc,
  UnDmkEnums, DmkDAC_PF, dmkDBGrid;

type
  TFmOcorreuC = class(TForm)
    PainelDados: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    PainelData: TPanel;
    DsOcorreu: TDataSource;
    PainelPesq: TPanel;
    Label75: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    QrOcorBank: TmySQLQuery;
    DsOcorBank: TDataSource;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    QrOcorBankLk: TIntegerField;
    QrOcorBankDataCad: TDateField;
    QrOcorBankDataAlt: TDateField;
    QrOcorBankUserCad: TIntegerField;
    QrOcorBankUserAlt: TIntegerField;
    PainelBtns: TPanel;
    PainelCtrl: TPanel;
    PainelBtsE: TPanel;
    BtIncluiOcor: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    PnConf: TPanel;
    BtConfirma5: TBitBtn;
    BtDesiste5: TBitBtn;
    QrClientesTAXA_COMPRA: TFloatField;
    QrClientesFatorCompra: TFloatField;
    PnEdit: TPanel;
    PanelFill1: TPanel;
    LaTipo: TLabel;
    QrOcorBankBase: TFloatField;
    PnOcor: TPanel;
    EdValor: TdmkEdit;
    Label22: TLabel;
    Label21: TLabel;
    TPDataO: TDateTimePicker;
    CBOcorrencia: TdmkDBLookupComboBox;
    EdOcorrencia: TdmkEditCB;
    Label20: TLabel;
    PnBase: TPanel;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    TPDataBase6: TDateTimePicker;
    EdValorBase6: TdmkEdit;
    EdJurosBase6: TdmkEdit;
    EdJurosPeriodo6: TdmkEdit;
    PnPaga: TPanel;
    Label82: TLabel;
    Label83: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    TPPagto6: TDateTimePicker;
    EdJuros6: TdmkEdit;
    EdPago6: TdmkEdit;
    EdAPagar6: TdmkEdit;
    QrLastOcor: TmySQLQuery;
    QrLastOcorData: TDateField;
    QrLastOcorCodigo: TIntegerField;
    QrSumOc: TmySQLQuery;
    QrSumOcJuros: TFloatField;
    QrSumOcPago: TFloatField;
    QrLocOc: TmySQLQuery;
    QrLocOcCodigo: TIntegerField;
    QrLocOcOcorreu: TIntegerField;
    QrLocOcData: TDateField;
    QrLocOcJuros: TFloatField;
    QrLocOcPago: TFloatField;
    QrLocOcLotePg: TIntegerField;
    QrLocOcLk: TIntegerField;
    QrLocOcDataCad: TDateField;
    QrLocOcDataAlt: TDateField;
    QrLocOcUserCad: TIntegerField;
    QrLocOcUserAlt: TIntegerField;
    DBGrid5: TdmkDBGrid;
    QrOcorreu: TmySQLQuery;
    QrOcorreuTipo: TSmallintField;
    QrOcorreuTIPODOC: TWideStringField;
    QrOcorreuNOMEOCORRENCIA: TWideStringField;
    QrOcorreuCodigo: TIntegerField;
    QrOcorreuLotesIts: TIntegerField;
    QrOcorreuDataO: TDateField;
    QrOcorreuOcorrencia: TIntegerField;
    QrOcorreuValor: TFloatField;
    QrOcorreuLoteQuit: TIntegerField;
    QrOcorreuLk: TIntegerField;
    QrOcorreuDataCad: TDateField;
    QrOcorreuDataAlt: TDateField;
    QrOcorreuUserCad: TIntegerField;
    QrOcorreuUserAlt: TIntegerField;
    QrOcorreuTaxaP: TFloatField;
    QrOcorreuTaxaV: TFloatField;
    QrOcorreuPago: TFloatField;
    QrOcorreuDataP: TDateField;
    QrOcorreuTaxaB: TFloatField;
    QrOcorreuData3: TDateField;
    QrOcorreuStatus: TSmallintField;
    QrOcorreuATUALIZADO: TFloatField;
    QrOcorreuDOCUM_TXT: TWideStringField;
    QrOcorreuBanco: TIntegerField;
    QrOcorreuAgencia: TIntegerField;
    QrOcorreuCheque: TIntegerField;
    QrOcorreuDuplicata: TWideStringField;
    QrOcorreuConta: TWideStringField;
    QrOcorreuEmissao: TDateField;
    QrOcorreuDCompra: TDateField;
    QrOcorreuVencto: TDateField;
    QrOcorreuDDeposito: TDateField;
    QrOcorreuEmitente: TWideStringField;
    QrOcorreuCPF: TWideStringField;
    QrOcorreuCLIENTELOTE: TIntegerField;
    QrOcorreuCliente: TIntegerField;
    CGTipo: TdmkCheckGroup;
    PMIncluiOcor: TPopupMenu;
    Incluiocorrnciadecliente1: TMenuItem;
    IncluiocorrnciadeCheque1: TMenuItem;
    IncluiocorrnciadeDuplicata1: TMenuItem;
    PMAlteraOcor: TPopupMenu;
    Alteraocorrnciaselecionada1: TMenuItem;
    PMExcluiOcor: TPopupMenu;
    ExcluiOcorrnciaselecionada1: TMenuItem;
    N1: TMenuItem;
    IncluipagamentodeOcorrncia1: TMenuItem;
    QrOcorrPg: TmySQLQuery;
    QrOcorrPgCodigo: TIntegerField;
    QrOcorrPgOcorreu: TIntegerField;
    QrOcorrPgData: TDateField;
    QrOcorrPgJuros: TFloatField;
    QrOcorrPgPago: TFloatField;
    QrOcorrPgLotePg: TIntegerField;
    QrOcorrPgLk: TIntegerField;
    QrOcorrPgDataCad: TDateField;
    QrOcorrPgDataAlt: TDateField;
    QrOcorrPgUserCad: TIntegerField;
    QrOcorrPgUserAlt: TIntegerField;
    DsOcorrPg: TDataSource;
    N2: TMenuItem;
    Excluipagamentodeocorrnciaselecionado1: TMenuItem;
    Panel2: TPanel;
    DBGrid3_: TDBGrid;
    QrLocLote: TmySQLQuery;
    QrLocLoteCodigo: TIntegerField;
    QrLocLoteTipo: TSmallintField;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdVal: TdmkEdit;
    EdTxV: TdmkEdit;
    EdPag: TdmkEdit;
    EdSal: TdmkEdit;
    BtBordero: TBitBtn;
    EdAtz: TdmkEdit;
    Label5: TLabel;
    QrPesq: TmySQLQuery;
    QrBco1: TmySQLQuery;
    QrBco1Nome: TWideStringField;
    QrPesqBanco: TIntegerField;
    QrPesqAgencia: TIntegerField;
    QrPesqConta: TWideStringField;
    QrPesqCheque: TIntegerField;
    QrPesqDuplicata: TWideStringField;
    QrPesqCliente: TIntegerField;
    QrPesqEmitente: TWideStringField;
    QrPesqVencto: TDateField;
    BitBtn1: TBitBtn;
    CkAbertas: TCheckBox;
    TPIni: TDateTimePicker;
    TPFim: TDateTimePicker;
    CkIni: TCheckBox;
    CkFim: TCheckBox;
    Label6: TLabel;
    EdDescri: TdmkEdit;
    QrOcorreuDescri: TWideStringField;
    EdTaxaP: TdmkEdit;
    Label7: TLabel;
    SbImprime: TBitBtn;
    frxOcorreuC: TfrxReport;
    frxDsClientes: TfrxDBDataset;
    PMImprime: TPopupMenu;
    Selecionados1: TMenuItem;
    odos1: TMenuItem;
    QrPesq1: TmySQLQuery;
    QrPesq1TIPODOC: TWideStringField;
    QrPesq1NOMEOCORRENCIA: TWideStringField;
    QrPesq1Banco: TIntegerField;
    QrPesq1Agencia: TIntegerField;
    QrPesq1Cheque: TIntegerField;
    QrPesq1Duplicata: TWideStringField;
    QrPesq1Conta: TWideStringField;
    QrPesq1Tipo: TSmallintField;
    QrPesq1Emissao: TDateField;
    QrPesq1DCompra: TDateField;
    QrPesq1Vencto: TDateField;
    QrPesq1DDeposito: TDateField;
    QrPesq1Emitente: TWideStringField;
    QrPesq1CPF: TWideStringField;
    QrPesq1CLIENTELOTE: TIntegerField;
    QrPesq1Codigo: TIntegerField;
    QrPesq1LotesIts: TIntegerField;
    QrPesq1Cliente: TIntegerField;
    QrPesq1DataO: TDateField;
    QrPesq1Ocorrencia: TIntegerField;
    QrPesq1Valor: TFloatField;
    QrPesq1LoteQuit: TIntegerField;
    QrPesq1TaxaB: TFloatField;
    QrPesq1TaxaP: TFloatField;
    QrPesq1TaxaV: TFloatField;
    QrPesq1Pago: TFloatField;
    QrPesq1DataP: TDateField;
    QrPesq1Data3: TDateField;
    QrPesq1Status: TSmallintField;
    QrPesq1Descri: TWideStringField;
    QrPesq1MoviBank: TIntegerField;
    QrPesq1Lk: TIntegerField;
    QrPesq1DataCad: TDateField;
    QrPesq1DataAlt: TDateField;
    QrPesq1UserCad: TIntegerField;
    QrPesq1UserAlt: TIntegerField;
    QrPesq1AlterWeb: TSmallintField;
    QrPesq1Ativo: TSmallintField;
    QrPesq1SALDO: TFloatField;
    QrPesq1ATUALIZADO: TFloatField;
    QrPesq1DOCUM_TXT: TWideStringField;
    frxDsPesq1: TfrxDBDataset;
    QrOcorreuDATA3_TXT: TWideStringField;
    QrPesq1Data3_TXT: TWideStringField;
    BtReabre: TBitBtn;
    QrOcorreuNOMECLIENTE: TWideStringField;
    QrOcorreuSALDO: TFloatField;
    BtDesfazOrdenacao: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtDesiste5Click(Sender: TObject);
    procedure BtConfirma5Click(Sender: TObject);
    procedure EdOcorrenciaChange(Sender: TObject);
    procedure QrOcorreuAfterOpen(DataSet: TDataSet);
    procedure QrClientesCalcFields(DataSet: TDataSet);
    procedure QrOcorreuCalcFields(DataSet: TDataSet);
    procedure CGTipoChange(Sender: TObject; ButtonIndex: Integer);
    procedure EdValorBase6Change(Sender: TObject);
    procedure EdJurosBase6Change(Sender: TObject);
    procedure TPPagto6Change(Sender: TObject);
    procedure EdJuros6Change(Sender: TObject);
    procedure EdAPagar6Change(Sender: TObject);
    procedure BtIncluiOcorClick(Sender: TObject);
    procedure Incluiocorrnciadecliente1Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure Alteraocorrnciaselecionada1Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure ExcluiOcorrnciaselecionada1Click(Sender: TObject);
    procedure IncluipagamentodeOcorrncia1Click(Sender: TObject);
    procedure QrOcorreuAfterScroll(DataSet: TDataSet);
    procedure PMExcluiOcorPopup(Sender: TObject);
    procedure Excluipagamentodeocorrnciaselecionado1Click(Sender: TObject);
    procedure BtBorderoClick(Sender: TObject);
    procedure QrOcorreuBeforeClose(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure Selecionados1Click(Sender: TObject);
    procedure odos1Click(Sender: TObject);
    procedure QrPesq1CalcFields(DataSet: TDataSet);
    procedure BtReabreClick(Sender: TObject);
    procedure BtDesfazOrdenacaoClick(Sender: TObject);
  private
    { Private declarations }
    FOcorrPg, FOcorreu: Integer;
    procedure CalculaPagtoOcorrPg(Quitacao: TDateTime; OcorrPg: Integer);
    procedure MostraEdicao(Acao: Integer; Titulo: String);
    procedure ReopenOcorreu(Codigo: Integer);
    procedure CalculaJurosOcor;
    procedure ConfiguraPgOc(Data: TDateTime);
    procedure CalculaAPagarOcor;
    procedure ConfirmaOcor;
    procedure ConfirmaPgto;
    procedure ReopenOcorrPg;
    procedure ReciboDePagamentoDeOcorCheque;
    procedure ReciboDePagamentoDeOcorDuplic;
    procedure ReciboDePagamentoDeOcorClient;
    procedure ReopenPesq1(Comp: String);
  public
    { Public declarations }
    FCliente: Integer;
  end;

  var
  FmOcorreuC: TFmOcorreuC;

implementation

{$R *.DFM}

uses Module, UnInternalConsts, UMySQLModule, Principal, UnGOTOy, UnMyObjects;

procedure TFmOcorreuC.MostraEdicao(Acao: Integer; Titulo: String);
begin
  LaTipo.Caption := Titulo;
  case Acao of
    0:
    begin
      //GradeItens.Visible := True;
      PainelCtrl.Visible := True;
      PainelPesq.Enabled := True;
      PnConf.Visible := False;
      PnEdit.Visible := False;
    end;
    1:
    begin
      PnEdit.Visible := True;
      PnConf.Visible := True;
      PnOcor.Visible := True;
      PnBase.Visible := False;
      PnPaga.Visible := False;
      PainelCtrl.Visible := False;
      PainelPesq.Enabled := False;
      if Titulo = CO_INCLUSAO then
      begin
        EdOcorrencia.Text := '';
        CBOcorrencia.KeyValue := NULL;
        TPDataO.Date := Date;
        EdValor.Text := '';
        //
        EdTaxaP.Text := Geral.FFT(Dmod.ObtemTaxaDeCompraCliente(
          Geral.IMV(EdCliente.Text)), 4, siPositivo);
      end else begin
        EdOcorrencia.Text := IntToStr(QrOcorreuOcorrencia.Value);
        CBOcorrencia.KeyValue := QrOcorreuOcorrencia.Value;
        TPDataO.Date := QrOcorreuDataO.Value;
        EdValor.Text := Geral.FFT(QrOcorreuValor.Value, 2, siPositivo);
        EdTaxaP.Text := Geral.FFT(QrOcorreuTaxaP.Value, 4, siPositivo);
      end;
      EdOcorrencia.SetFocus;
    end;
    2:
    begin
      FOcorreu := QrOcorreuCodigo.Value;
      PnEdit.Visible := True;
      PnConf.Visible := True;
      PnBase.Visible := True;
      PnPaga.Visible := True;
      PnOcor.Visible := False;
      PainelCtrl.Visible := False;
      PainelPesq.Enabled := False;
      //
      ConfiguraPgOc(Int(Date));
      CalculaJurosOcor;
      CalculaAPagarOcor;
      EdJurosBase6.SetFocus;
    end;
  end;
end;

procedure TFmOcorreuC.odos1Click(Sender: TObject);
begin
  frxDsPesq1.DataSet := QrOcorreu;
  MyObjects.frxMostra(frxOcorreuC, 'Relat�rio de Ocorr�ncias')
end;

procedure TFmOcorreuC.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmOcorreuC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if FCliente > 0 then
  begin
    EdCliente.Text := IntToStr(FCliente);
    CBCliente.KeyValue := FCliente;
    //
    MostraEdicao(1, CO_INCLUSAO);
  end;
end;

procedure TFmOcorreuC.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmOcorreuC.FormCreate(Sender: TObject);
begin
  TPIni.Date := IncMonth(Int(Date), -1);
  TPFim.Date := Int(Date);
  QrClientes.Open;
  QrOcorBank.Open;
  Panel2.Align    := alBottom;
  Panel2.Height   := 104;
  DBGrid5.Align    := alClient;
  PainelData.Align := alClient;
  PainelBtsE.Align := alClient;
  //GradeItens.Align := alClient;
end;

procedure TFmOcorreuC.BtDesfazOrdenacaoClick(Sender: TObject);
var
  Query: TmySQLQuery;
  Controle: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    if DBGrid5.DataSource.DataSet is TmySQLQuery then
      Query := TmySQLQuery(DBGrid5.DataSource.DataSet)
    else
      Query := nil;
    if Query <> nil then
    begin
      Query.SortFieldNames := '';
      if Query.State <> dsInactive then
      begin
        Controle := Query.FieldByName('Codigo').AsInteger;
        Query.Close;
        Query.Open;
        Query.Locate('Codigo', Controle, []);
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOcorreuC.BtDesiste5Click(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO);
  if FCliente > 0 then Close;
end;

procedure TFmOcorreuC.EdOcorrenciaChange(Sender: TObject);
begin
  if LaTipo.Caption = CO_INCLUSAO then
  EdValor.Text := Geral.FFT(QrOcorBankBase.Value, 2, siNegativo);
end;

procedure TFmOcorreuC.BtConfirma5Click(Sender: TObject);
begin
  if PnOcor.Visible then ConfirmaOcor else ConfirmaPgto;
end;

procedure TFmOcorreuC.ConfirmaPgto;
begin
  Screen.Cursor := crHourGlass;
  FOcorrPg := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
        'OcorrPG', 'OcorrPG', 'Codigo');
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO ocorrpg SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Pago=:P2, LotePg=:P3');
  Dmod.QrUpd.SQL.Add(', Ocorreu=:Pa, Codigo=:Pb');
  Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPPagto6.Date);
  Dmod.QrUpd.Params[01].AsFloat   := Geral.DMV(EdJuros6.Text);
  Dmod.QrUpd.Params[02].AsFloat   := Geral.DMV(EdPago6.Text);
  Dmod.QrUpd.Params[03].AsInteger := 0;
  //
  Dmod.QrUpd.Params[04].AsInteger := FOcorreu;
  Dmod.QrUpd.Params[05].AsInteger := FOcorrPg;
  Dmod.QrUpd.ExecSQL;
  //
  CalculaPagtoOcorrPg(TPPagto6.Date, FOcorreu);
  //
  ReopenOcorreu(FOcorreu);
  //
  MostraEdicao(0, CO_TRAVADO);
  Screen.Cursor := crDefault;
end;

procedure TFmOcorreuC.ConfirmaOcor;
var
  Codigo: Integer;
begin
  Dmod.QrUpd.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpd.SQL.Add('INSERT INTO ocorreu SET AlterWeb=1, ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
      'Ocorreu', 'Ocorreu', 'Codigo');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET AlterWeb=1, ');
    Codigo := QrOcorreuCodigo.Value;
  end;
  Dmod.QrUpd.SQL.Add('LotesIts=:P0, DataO=:P1, Ocorrencia=:P2, Valor=:P3, ');
  Dmod.QrUpd.SQL.Add('Cliente=:P4, Descri=:P5, TaxaP=:P6 ');
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpd.SQL.Add(', Codigo=:Pa');
  end else begin
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
  end;
  Dmod.QrUpd.Params[00].AsInteger := 0;
  Dmod.QrUpd.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPDataO.Date);
  Dmod.QrUpd.Params[02].AsInteger := Geral.IMV(EdOcorrencia.Text);
  Dmod.QrUpd.Params[03].AsFloat   := Geral.DMV(EdValor.Text);
  Dmod.QrUpd.Params[04].AsInteger := QrClientesCodigo.Value;
  Dmod.QrUpd.Params[05].AsString  := EdDescri.Text;
  Dmod.QrUpd.Params[06].AsFloat   := Geral.DMV(EdTaxaP.Text);
  //
  Dmod.QrUpd.Params[07].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  if FCliente > 0 then Close else
  begin
    ReopenOcorreu(Codigo);
    MostraEdicao(0, CO_TRAVADO);
  end;
end;

procedure TFmOcorreuC.ReopenOcorreu(Codigo: Integer);
var
  Cliente: Integer;
  DtaIni, DtaFim: Boolean;
  SQLCliente, SQLTipo, SQLAbertos, SQLPeriodo: String;
begin
  Cliente := EdCliente.ValueVariant;
  DtaIni  := CkIni.Checked;
  DtaFim  := CkFim.Checked;
  //
  if (Cliente = 0) and (not DtaIni) and (not DtaFim) then
  begin
    if Geral.MB_Pergunta('Voc� n�o selecionou nenhum cliente e nenhum per�odo!' +
      sLineBreak + 'A gera��o do relat�rio poder� demorar. Deseja continuar?') <> ID_YES
    then
      Exit;
  end;
  //
  Screen.Cursor := crHourGlass;
  try
    if Cliente <> 0 then
      SQLCliente := 'WHERE (lo.Cliente=' + Geral.FF0(Cliente) + ' OR oc.Cliente=' + Geral.FF0(Cliente) + ')'
    else
      SQLCliente := 'WHERE (lo.Cliente <> 0 OR oc.Cliente <> 0) ';
    //
    case CGTipo.Value of
        0: SQLTipo := '';
        1: SQLTipo := 'AND oc.Cliente > 0';
        2: SQLTipo := 'AND ((oc.Cliente = 0) AND (lo.Tipo=0))';
        3: SQLTipo := 'AND ((oc.Cliente > 0) OR  ((oc.Cliente = 0) AND (lo.Tipo=0)))';
        4: SQLTipo := 'AND ((oc.Cliente = 0) AND (lo.Tipo=1))';
        5: SQLTipo := 'AND ((oc.Cliente > 0) OR  ((oc.Cliente = 0) AND (lo.Tipo=1)))';
        6: SQLTipo := 'AND oc.Cliente = 0';
        7: SQLTipo := '';
      else SQLTipo := '';
    end;
    //
    if CkAbertas.Checked then
      SQLAbertos := 'AND oc.Status<2'
    else
      SQLAbertos := '';
    //
    SQLPeriodo := dmkPF.SQL_Periodo('AND oc.DataO', TPIni.Date, TPFim.Date,
                    CkIni.Checked, CkFim.Checked);
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrOcorreu, Dmod.MyDB, [
      'SELECT IF(oc.Cliente > 0, "CL", IF(lo.Tipo=0, "CH", "DU")) TIPODOC, ',
      'ob.Nome NOMEOCORRENCIA, li.Banco, li.Agencia, li.Cheque, ',
      'li.Duplicata, li.Conta, lo.Tipo, li.Emissao, li.DCompra, li.Vencto, ',
      'li.DDeposito, li.Emitente, li.CPF, lo.Cliente CLIENTELOTE, ',
      'IF(en.Tipo = 0, en.RazaoSocial, en.Nome) NOMECLIENTE, ',
      'oc.Valor + oc.TaxaV - oc.Pago SALDO, oc.* ',
      'FROM ocorreu oc ',
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle ',
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo ',
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia ',
      'LEFT JOIN entidades en ON en.Codigo = IF(oc.Cliente <> 0, oc.Cliente, lo.Cliente) ',
      SQLCliente,
      SQLTipo,
      SQLAbertos,
      SQLPeriodo,
      '']);
    //
    if Codigo <> 0 then QrOcorreu.Locate('Codigo', Codigo, []);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmOcorreuC.QrOcorreuAfterOpen(DataSet: TDataSet);
var
  Val, TxV, Pag, Sal, Atz: Double;
begin
  if QrOcorreu.RecordCount = 0 then
  begin
    Alteraocorrnciaselecionada1.Enabled := False;
    BtExclui.Enabled := False;
  end else begin
    Alteraocorrnciaselecionada1.Enabled := True;
    BtExclui.Enabled := True;
  end;
  Val := 0;
  TxV := 0;
  Pag := 0;
  Sal := 0;
  Atz := 0;
  QrOcorreu.DisableControls;
  while not QrOcorreu.Eof do
  begin
    Val := Val + QrOcorreuValor.Value;
    TxV := TxV + QrOcorreuTaxaV.Value;
    Pag := Pag + QrOcorreuPago.Value;
    Sal := Sal + QrOcorreuSALDO.Value;
    Atz := Atz + QrOcorreuATUALIZADO.Value;
    QrOcorreu.Next;
  end;
  EdVal.Text := Geral.FFT(Val, 2, siNegativo);
  EdTxV.Text := Geral.FFT(TxV, 2, siNegativo);
  EdPag.Text := Geral.FFT(Pag, 2, siNegativo);
  EdSal.Text := Geral.FFT(Sal, 2, siNegativo);
  EdAtz.Text := Geral.FFT(Atz, 2, siNegativo);
  QrOcorreu.EnableControls;
end;

procedure TFmOcorreuC.QrClientesCalcFields(DataSet: TDataSet);
//var
  //Taxas: MyArrayR07;
begin
  //Taxas := DMod.ObtemTaxaDeCompraRealizada(QrPesqControle.Value);
  //QrClientesTAXA_COMPRA.Value := QrClientesFatorCompra.Value + Taxas[0];
end;

procedure TFmOcorreuC.CalculaJurosOcor;
var
  Prazo: Integer;
  Taxa, Juros, Valor: Double;
begin
  Juros := 0;
  Prazo := Trunc(Int(TPPagto6.Date) - Int(TPDataBase6.Date));
  if Prazo > 0 then
  begin
    Taxa  := Geral.DMV(EdJurosBase6.Text);
    Juros := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(Taxa, Prazo);
  end;
  Valor := Geral.DMV(EdValorBase6.Text);
  EdJurosPeriodo6.Text := Geral.FFT(Juros, 6, siNegativo);
  EdJuros6.Text := Geral.FFT(Juros * Valor / 100, 2, siNegativo);
end;

procedure TFmOcorreuC.ConfiguraPgOc(Data: TDateTime);
var
  ValorBase: Double;
begin
  QrLocOc.Close;
  QrLocOc.Params[0].AsInteger := QrOcorreuCodigo.Value;
  QrLocOc.Open;
  TPPagto6.MinDate := 0;
  if QrLocOc.RecordCount > 0 then
  begin
    TPPagto6.Date     := Int(QrLocOcData.Value);
    TPDataBase6.Date  := Int(QrLocOcData.Value);
  end else begin
    TPPagto6.Date    := Int(QrOcorreuDataO.Value);
    TPDataBase6.Date := Int(QrOcorreuDataO.Value);
  end;
  EdJurosBase6.Text := Geral.FFT(
    Dmod.ObtemTaxaDeCompraCliente(Geral.IMV(EdCliente.Text)), 6, siPositivo);
  ValorBase := QrOcorreuValor.Value + QrOcorreuTaxaV.Value - QrOcorreuPago.Value;
  EdValorBase6.Text := Geral.FFT(ValorBase, 2, siNegativo);
  TPPagto6.MinDate := TPPagto6.Date;
  if Date > TPPagto6.MinDate then TPPagto6.Date := Int(Date);
  TPPagto6.SetFocus;
end;

procedure TFmOcorreuC.QrOcorreuCalcFields(DataSet: TDataSet);
var
  Cliente: Integer;
begin
  if QrOcorreuCLIENTELOTE.Value > 0 then
    Cliente := QrOcorreuCLIENTELOTE.Value
  else
    Cliente := QrOcorreuCliente.Value;
  //
  QrOcorreuATUALIZADO.Value := DMod.ObtemValorAtualizado(
    Cliente, 1, QrOcorreuDataO.Value, Date, QrOcorreuData3.Value,
    QrOcorreuValor.Value, QrOcorreuTaxaV.Value, 0 (*Desco*),
    QrOcorreuPago.Value, QrOcorreuTaxaP.Value, False);
  //
  if QrOcorreuDescri.Value <> '' then
    QrOcorreuDOCUM_TXT.Value := QrOcorreuDescri.Value
  else if QrOcorreuTIPODOC.Value = 'CH' then
    QrOcorreuDOCUM_TXT.Value := FormatFloat('000', QrOcorreuBanco.Value)+'/'+
    FormatFloat('0000', QrOcorreuAgencia.Value)+'/'+ QrOcorreuConta.Value+'-'+
    FormatFloat('000000', QrOcorreuCheque.Value)
  else if QrOcorreuTIPODOC.Value = 'DU' then
    QrOcorreuDOCUM_TXT.Value := QrOcorreuDuplicata.Value
  else
    QrOcorreuDOCUM_TXT.Value := '';
  //
  QrOcorreuDATA3_TXT.Value := dmkPF.FDT_NULO(QrOcorreuData3.Value, 3);
end;

procedure TFmOcorreuC.QrPesq1CalcFields(DataSet: TDataSet);
var
  Cliente: Integer;
begin
  if QrPesq1CLIENTELOTE.Value > 0 then
    Cliente := QrPesq1CLIENTELOTE.Value else
    Cliente := QrPesq1Cliente.Value;
  //
  QrPesq1SALDO.Value := QrPesq1Valor.Value + QrPesq1TaxaV.Value - QrPesq1Pago.Value;
  //
  QrPesq1ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    Cliente, 1, QrPesq1DataO.Value, Date, QrPesq1Data3.Value,
    QrPesq1Valor.Value, QrPesq1TaxaV.Value, 0 (*Desco*),
    QrPesq1Pago.Value, QrPesq1TaxaP.Value, False);
  //
  if QrPesq1Descri.Value <> '' then
    QrPesq1DOCUM_TXT.Value := QrPesq1Descri.Value
  else if QrPesq1TIPODOC.Value = 'CH' then
    QrPesq1DOCUM_TXT.Value := FormatFloat('000', QrPesq1Banco.Value)+'/'+
    FormatFloat('0000', QrPesq1Agencia.Value)+'/'+ QrPesq1Conta.Value+'-'+
    FormatFloat('000000', QrPesq1Cheque.Value)
  else if QrPesq1TIPODOC.Value = 'DU' then
    QrPesq1DOCUM_TXT.Value := QrPesq1Duplicata.Value
  else QrPesq1DOCUM_TXT.Value := '';
end;

procedure TFmOcorreuC.CGTipoChange(Sender: TObject; ButtonIndex: Integer);
begin
  if CGTipo.Value = 0 then CGTipo.Value := 7;
  ReopenOcorreu(0);
  PainelData.Visible := True;
end;

procedure TFmOcorreuC.EdValorBase6Change(Sender: TObject);
begin
  CalculaAPagarOcor;
end;

procedure TFmOcorreuC.EdJurosBase6Change(Sender: TObject);
begin
  CalculaJurosOcor;
  CalculaAPagarOcor;
end;

procedure TFmOcorreuC.TPPagto6Change(Sender: TObject);
begin
  CalculaJurosOcor;
end;

procedure TFmOcorreuC.EdJuros6Change(Sender: TObject);
var
  VBase, Juros: Double;
begin
  VBase := Geral.DMV(EdValorBase6.Text);
  Juros := Geral.DMV(EdJuros6.Text);
  EdAPagar6.Text := Geral.FFT(VBase+Juros, 2, siNegativo);
end;

procedure TFmOcorreuC.EdAPagar6Change(Sender: TObject);
begin
  EdPago6.Text := EdAPagar6.Text;
end;

procedure TFmOcorreuC.CalculaAPagarOcor;
var
  Base, Juro: Double;
begin
  Base := Geral.DMV(EdValorBase6.Text);
  Juro := Geral.DMV(EdJuros6.Text);
  //
  EdAPagar6.Text := Geral.FFT(Base+Juro, 2, siNegativo);
end;

procedure TFmOcorreuC.BtIncluiOcorClick(Sender: TObject);
begin
  if MyObjects.FIC(EdCliente.ValueVariant = 0, EdCliente, 'Voc� deve selecionar um cliente!') then Exit;
  //
  MyObjects.MostraPopUpDeBotao(PMIncluiOcor, BtIncluiOcor);
end;

procedure TFmOcorreuC.BtReabreClick(Sender: TObject);
begin
  ReopenOcorreu(0);
  PainelData.Visible := True;
end;

procedure TFmOcorreuC.Incluiocorrnciadecliente1Click(Sender: TObject);
begin
  MostraEdicao(1, CO_INCLUSAO);
end;

procedure TFmOcorreuC.BtAlteraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAlteraOcor, BtAltera);
end;

procedure TFmOcorreuC.Alteraocorrnciaselecionada1Click(Sender: TObject);
begin
  MostraEdicao(1, CO_ALTERACAO);
end;

procedure TFmOcorreuC.BtExcluiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMExcluiOcor, BtExclui);
end;

procedure TFmOcorreuC.ExcluiOcorrnciaselecionada1Click(Sender: TObject);
begin
  if QrOcorrPg.RecordCount > 0 then
  begin
    Application.MessageBox('A ocorr�ncia selecionada n�o pode ser exclu�da '+
    'pois possui pagamento(s)!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Application.MessageBox('Confirma a exclus�o da ocorr�ncia selecionada?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM ocorreu WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrOcorreuCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    QrOcorreu.Next;
    //
    ReopenOcorreu(QrOcorreuCodigo.Value);
  end;
end;

procedure TFmOcorreuC.IncluipagamentodeOcorrncia1Click(Sender: TObject);
begin
  MostraEdicao(2, CO_INCLUSAO);
end;

procedure TFmOcorreuC.QrOcorreuAfterScroll(DataSet: TDataSet);
begin
  ReopenOcorrPg;
end;

procedure TFmOcorreuC.ReopenOcorrPg;
begin
  QrOcorrPg.Close;
  QrOcorrPg.Params[0].AsInteger := QrOcorreuCodigo.Value;
  QrOcorrPg.Open;
  //
  if FOcorrPg <> 0 then QrOcorrPg.Locate('Codigo', FOcorrPg, []);
end;

procedure TFmOcorreuC.ReopenPesq1(Comp: String);
begin
  QrPesq1.Close;
  QrPesq1.SQL.Clear;
  QrPesq1.SQL.Add('SELECT IF(oc.Cliente > 0,"CL",IF(lo.Tipo=0,"CH","DU"))TIPODOC,');
  QrPesq1.SQL.Add('ob.Nome NOMEOCORRENCIA, li.Banco, li.Agencia, li.Cheque,');
  QrPesq1.SQL.Add('li.Duplicata, li.Conta, lo.Tipo, li.Emissao, li.DCompra, li.Vencto,');
  QrPesq1.SQL.Add('IF(oc.Data3 <= "1899-12-30", "", DATE_FORMAT(oc.Data3,"%d/%m/%y")) Data3_TXT,');
  QrPesq1.SQL.Add('li.DDeposito, li.Emitente, li.CPF, lo.Cliente CLIENTELOTE, oc.*');
  QrPesq1.SQL.Add('FROM ocorreu oc');
  QrPesq1.SQL.Add('LEFT JOIN lotesits li ON oc.LotesIts = li.Controle');
  QrPesq1.SQL.Add('LEFT JOIN lotes lo ON lo.Codigo = li.Codigo');
  QrPesq1.SQL.Add('LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia');
  QrPesq1.SQL.Add('WHERE oc.Codigo > 0');
  if Length(Comp) > 0 then
    QrPesq1.SQL.Add('AND oc.Codigo IN('+ Comp +')');
  QrPesq1.Open;
end;

procedure TFmOcorreuC.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmOcorreuC.Selecionados1Click(Sender: TObject);
var
  Txt: String;
  i: Integer;
begin
  Txt := '';
  frxDsPesq1.DataSet := QrPesq1;
  //
  if DBGrid5.SelectedRows.Count > 1 then
  begin
    with DBGrid5.DataSource.DataSet do
    for i:= 0 to DBGrid5.SelectedRows.Count-1 do
    begin
      GotoBookmark(pointer(DBGrid5.SelectedRows.Items[i]));
      if DBGrid5.SelectedRows.Count - 1 = i then
        Txt := Txt + IntToStr(QrOcorreuCodigo.Value)
      else
        Txt := Txt + IntToStr(QrOcorreuCodigo.Value) + ', '; 
    end;
  end else
      Txt := Txt + IntToStr(QrOcorreuCodigo.Value);
  ReopenPesq1(Txt);
  MyObjects.frxMostra(frxOcorreuC, 'Relat�rio de Ocorr�ncias');
end;

procedure TFmOcorreuC.CalculaPagtoOcorrPg(Quitacao: TDateTime; OcorrPg: Integer);
var
  Sit: Integer;
begin
  QrSumOc.Close;
  QrSumOc.Params[0].AsInteger := OcorrPg;
  QrSumOc.Open;
  //
  Sit := 0;
  //Corrigido em 12/02/2013 => Para n�o aparecer ocorr�ncias sem valor como abertas
  //if QrSumOcPago.Value <> 0 then
  if QrSumOc.RecordCount > 0 then  
  begin
   //Errado corrigido em 09/08/2012 
   //if (QrSumOcPago.Value < (QrOcorreuValor.Value + ==>QrOcorreuTaxaV.Value<==)-0.009)
   if (QrSumOcPago.Value < (QrOcorreuValor.Value + QrSumOcJuros.Value)-0.009) then
     Sit := 1
   else
     Sit := 2;
  end;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET AlterWeb=1, ');
  Dmod.QrUpd.SQL.Add('TaxaV=:P0, Pago=:P1, Status=:P2, ');
  Dmod.QrUpd.SQL.Add('Data3=:P3 WHERE Codigo=:Pa');
  //
  Dmod.QrUpd.Params[00].AsFloat   := QrSumOcJuros.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumOcPago.Value;
  Dmod.QrUpd.Params[02].AsInteger := Sit;
  Dmod.QrUpd.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Quitacao);
  Dmod.QrUpd.Params[04].AsInteger := OcorrPg;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TFmOcorreuC.PMExcluiOcorPopup(Sender: TObject);
begin
  ExcluiOcorrnciaselecionada1.Enabled := Geral.IntToBool_0(QrOcorreu.RecordCount);
  Excluipagamentodeocorrnciaselecionado1.Enabled := Geral.IntToBool_0(QrOcorrPg.RecordCount);
end;

procedure TFmOcorreuC.Excluipagamentodeocorrnciaselecionado1Click(
  Sender: TObject);
var
  Ocorreu: Integer;
begin
  if QrOcorrPgLotePg.Value > 0 then
  begin
    Application.MessageBox(PChar('O pagamento da ocorr�ncia selecionada n�o '+
    'pode ser exclu�do pois foi paga em border�!. Para exclu�-lo acesse o '+
    'Border� correspondente.'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  QrLastOcor.Close;
  QrLastOcor.Params[0].AsInteger := QrOcorrPgOcorreu.Value;
  QrLastOcor.Open;
  if (QrLastOcorData.Value > QrOcorrPgData.Value)
  or ((QrLastOcorData.Value = QrOcorrPgData.Value)
  and (QrLastOcorCodigo.Value > QrOcorrPgCodigo.Value))
  then
     Application.MessageBox('Somente o �ltimo pagamento pode ser exclu�do!',
    'Erro', MB_OK+MB_ICONERROR)
  else begin
    if Application.MessageBox('Confirma a exclus�o deste pagamento?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        Ocorreu := QrOcorrPgOcorreu.Value;
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM ocorrpg WHERE Codigo=:P0');
        Dmod.QrUpd.Params[0].AsInteger := QrOcorrPgCodigo.Value;
        Dmod.QrUpd.ExecSQL;
        //
        QrLastOcor.Close;
        QrLastOcor.Params[0].AsInteger := Ocorreu;
        QrLastOcor.Open;
        CalculaPagtoOcorrPg(QrLastOcorData.Value, Ocorreu);
        //
        QrOcorrPg.Next;
        FOcorrPg := QrOcorrPgCodigo.Value;
        ReopenOcorreu(QrOcorreuCodigo.Value);
        Screen.Cursor := crDefault;
      except
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmOcorreuC.BtBorderoClick(Sender: TObject);
var
  Ocorreu: Integer;
begin
  Ocorreu := QrOcorreuCodigo.Value;
  FOcorrPg := QrOcorrPgCodigo.Value;
  //
  QrLocLote.Close;
  QrLocLote.Params[0].AsInteger := QrOcorrPgLotePg.Value;
  QrLocLote.Open;
  FmPrincipal.CriaFormLotes(1, QrLocLoteTipo.Value, QrOcorrPgLotePg.Value, 0);
  ReopenOcorreu(Ocorreu);
end;

procedure TFmOcorreuC.QrOcorreuBeforeClose(DataSet: TDataSet);
begin
  EdVal.Text := '0,00';
  EdTxV.Text := '0,00';
  EdPag.Text := '0,00';
  EdSal.Text := '0,00';
  EdAtz.Text := '0,00';
end;

procedure TFmOcorreuC.BitBtn1Click(Sender: TObject);
begin
  if (QrOcorreu.State = dsBrowse) and (QrOcorrPg.State = dsBrowse)
  and (QrOcorrPg.RecordCount > 0) then
  begin
    if QrOcorreuTIPODOC.Value = 'CH' then ReciboDePagamentoDeOcorCheque else
    if QrOcorreuTIPODOC.Value = 'DU' then ReciboDePagamentoDeOcorDuplic else
                                          ReciboDePagamentoDeOcorClient;
  end else
  GOTOy.EmiteRecibo(0, Dmod.QrMasterDono.Value,
    0, 0, 0, '----000', '', '', '', Int(Date), 0);
end;

procedure TFmOcorreuC.ReciboDePagamentoDeOcorCheque;
var
  Pagamento, Texto: String;
begin
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT loi.Banco, loi.Agencia, loi.Conta, loi.Cheque,');
  QrPesq.SQL.Add('loi.Duplicata, loi.Emitente, loi.Vencto, lot.Cliente');
  QrPesq.SQL.Add('FROM lotesits loi');
  QrPesq.SQL.Add('LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo');
  QrPesq.SQL.Add('LEFT JOIN ocorreu oco ON oco.LotesIts = loi.Controle');
  QrPesq.SQL.Add('WHERE oco.Codigo=:P0');
  QrPesq.Params[0].AsInteger := QrOcorreuCodigo.Value;
  QrPesq.Open;
  //
  QrBco1.Close;
  QrBco1.Params[0].AsInteger := QrPesqBanco.Value;
  QrBco1.Open;
  //
  Pagamento := MLAGeral.NomeStatusPgto6(QrOcorreuATUALIZADO.Value,
    QrOcorrPg.RecNo)+' da ocorr�ncia "'+ QrOcorreuNOMEOCORRENCIA.Value+
    '" cobrada sobre';
  Texto := Pagamento + ' o cheque n� ' + FormatFloat('000000', QrPesqCheque.Value)
    +' do banco n� '+FormatFloat('000', QrPesqBanco.Value) + ' ' +
    QrBco1Nome.Value+ ' ag�ncia n� '+ FormatFloat('0000', QrPesqAgencia.Value) +
    ' emitida por '+ QrPesqEmitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqVencto.Value)+'. A quita��o desta '+
    'ocorr�ncia n�o quita o cheque';
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value,
    QrOcorrPgPago.Value, 0, 0, 'CHO-'+FormatFloat('000',
    QrOcorrPgCodigo.Value), Texto, '', '', QrOcorrPgData.Value, 0);
end;

procedure TFmOcorreuC.ReciboDePagamentoDeOcorDuplic;
var
  Pagamento, Texto: String;
begin
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT loi.Banco, loi.Agencia, loi.Conta, loi.Cheque,');
  QrPesq.SQL.Add('loi.Duplicata, loi.Emitente, loi.Vencto, lot.Cliente');
  QrPesq.SQL.Add('FROM lotesits loi');
  QrPesq.SQL.Add('LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo');
  QrPesq.SQL.Add('LEFT JOIN ocorreu oco ON oco.LotesIts = loi.Controle');
  QrPesq.SQL.Add('WHERE oco.Codigo=:P0');
  QrPesq.Params[0].AsInteger := QrOcorreuCodigo.Value;
  QrPesq.Open;
  //
  Pagamento := MLAGeral.NomeStatusPgto6(QrOcorreuATUALIZADO.Value,
    QrOcorrPg.RecNo)+' da ocorr�ncia "'+ QrOcorreuNOMEOCORRENCIA.Value+
    '" cobrada sobre';
  Texto := Pagamento + ' a duplicata n� ' + QrPesqDuplicata.Value +
    ' emitida por '+ QrPesqEmitente.Value + ' para o dia '+ FormatDateTime(
    'dd" de "mmmm" de "yyyy', QrPesqVencto.Value)+'. A quita��o desta '+
    'ocorr�ncia n�o quita a duplicata';
  GOTOy.EmiteRecibo(QrPesqCliente.Value, Dmod.QrMasterDono.Value,
    QrOcorrPgPago.Value, 0, 0, 'DUO-'+FormatFloat('000',
    QrOcorrPgCodigo.Value), Texto, '', '', QrOcorrPgData.Value, 0);
end;

procedure TFmOcorreuC.ReciboDePagamentoDeOcorClient;
var
  Texto: String;
begin
  {
  QrPesq.Close;
  QrPesq.Params[0].AsInteger := QrOcorreuCodigo.Value;
  QrPesq.Open;
  }
  //
  Texto := MLAGeral.NomeStatusPgto6(QrOcorreuATUALIZADO.Value,
    QrOcorrPg.RecNo)+' referente a ocorr�ncia "'+ QrOcorreuNOMEOCORRENCIA.Value;
  GOTOy.EmiteRecibo(QrOcorreuCliente.Value, Dmod.QrMasterDono.Value,
    QrOcorrPgPago.Value, 0, 0, 'CLO-'+FormatFloat('000',
    QrOcorrPgCodigo.Value), Texto, '', '', QrOcorrPgData.Value, 0);
end;

end.

