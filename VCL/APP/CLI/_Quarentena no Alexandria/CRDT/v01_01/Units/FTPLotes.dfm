object FmFTPLotes: TFmFTPLotes
  Left = 258
  Top = 56
  Caption = 'Transfer'#234'ncia de Arquivos'
  ClientHeight = 496
  ClientWidth = 631
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 631
    Height = 383
    Align = alClient
    TabOrder = 0
    object PnGerente: TPanel
      Left = 1
      Top = 1
      Width = 629
      Height = 361
      Align = alClient
      Color = clAppWorkSpace
      TabOrder = 1
      Visible = False
      object PageControl1: TPageControl
        Left = -87
        Top = 29
        Width = 622
        Height = 323
        ActivePage = TabSheet14
        TabOrder = 0
        object TabSheet13: TTabSheet
          Caption = 'Informa'#231#227'o de login'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 265
            Height = 295
            Align = alLeft
            BevelOuter = bvNone
            TabOrder = 0
            object GroupBox2: TGroupBox
              Left = 8
              Top = 8
              Width = 249
              Height = 121
              Caption = ' Ftp Server '
              TabOrder = 0
              object Label9: TLabel
                Left = 16
                Top = 20
                Width = 22
                Height = 13
                Caption = 'URL'
              end
              object Label10: TLabel
                Left = 16
                Top = 46
                Width = 36
                Height = 13
                Caption = 'Usu'#225'rio'
              end
              object Label11: TLabel
                Left = 16
                Top = 70
                Width = 31
                Height = 13
                Caption = 'Senha'
              end
              object EdURL: TEdit
                Left = 72
                Top = 16
                Width = 161
                Height = 21
                TabOrder = 0
              end
              object EdLog: TEdit
                Left = 72
                Top = 41
                Width = 161
                Height = 21
                TabOrder = 1
              end
              object EdPwd: TEdit
                Left = 72
                Top = 66
                Width = 161
                Height = 21
                TabOrder = 2
              end
              object chkPassive: TCheckBox
                Left = 14
                Top = 94
                Width = 159
                Height = 17
                Alignment = taLeftJustify
                Caption = 'Conex'#227'o passiva de dados'
                TabOrder = 3
              end
            end
            object GroupBox3: TGroupBox
              Left = 4
              Top = 168
              Width = 225
              Height = 153
              Caption = ' Servidor Socket '
              TabOrder = 1
              object Label12: TLabel
                Left = 16
                Top = 24
                Width = 46
                Height = 13
                Caption = 'Endere'#231'o'
              end
              object Label13: TLabel
                Left = 16
                Top = 48
                Width = 31
                Height = 13
                Caption = 'Senha'
              end
              object Label14: TLabel
                Left = 16
                Top = 72
                Width = 25
                Height = 13
                Caption = 'Porta'
              end
              object Label15: TLabel
                Left = 16
                Top = 96
                Width = 54
                Height = 13
                Caption = 'C'#243'd. usuar.'
              end
              object Label16: TLabel
                Left = 16
                Top = 120
                Width = 33
                Height = 13
                Caption = 'Vers'#227'o'
              end
              object edtSocksPort: TEdit
                Left = 72
                Top = 68
                Width = 57
                Height = 21
                TabOrder = 0
              end
              object edtSocksAddress: TEdit
                Left = 72
                Top = 20
                Width = 137
                Height = 21
                TabOrder = 1
              end
              object edtSocksPassword: TEdit
                Left = 72
                Top = 44
                Width = 137
                Height = 21
                TabOrder = 2
              end
              object edtSocksUserCode: TEdit
                Left = 72
                Top = 92
                Width = 121
                Height = 21
                TabOrder = 3
              end
              object cbxSocksVersion: TComboBox
                Left = 72
                Top = 116
                Width = 121
                Height = 21
                Style = csDropDownList
                ItemHeight = 0
                TabOrder = 4
                Items.Strings = (
                  'Socks 4'
                  'Socks 4a'
                  'Socks 5')
              end
            end
            object BtCadastro: TButton
              Left = 176
              Top = 142
              Width = 73
              Height = 25
              Caption = 'Cadastro'
              TabOrder = 2
              OnClick = BtCadastroClick
            end
            object LogoutBtn: TButton
              Left = 92
              Top = 142
              Width = 73
              Height = 25
              Caption = 'Logout'
              TabOrder = 3
              OnClick = LogoutBtnClick
            end
            object LoginBtn: TButton
              Left = 8
              Top = 142
              Width = 73
              Height = 25
              Caption = 'Login'
              TabOrder = 4
              OnClick = LoginBtnClick
            end
          end
          object Panel11: TPanel
            Left = 265
            Top = 0
            Width = 349
            Height = 295
            Align = alClient
            BevelOuter = bvNone
            Caption = 'Panel11'
            TabOrder = 1
            object StaticText1: TStaticText
              Left = 0
              Top = 0
              Width = 112
              Height = 17
              Align = alTop
              Alignment = taCenter
              Caption = 'Respostas do servidor:'
              TabOrder = 0
            end
            object xxxReplyMemo: TMemo
              Left = 0
              Top = 17
              Width = 349
              Height = 278
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              ScrollBars = ssVertical
              TabOrder = 1
            end
          end
        end
        object TabSheet14: TTabSheet
          Caption = 'Diret'#243'rios / arquivos'
          ImageIndex = 1
          object STCaminho: TStaticText
            Left = 0
            Top = 0
            Width = 614
            Height = 17
            Align = alTop
            Caption = '/'
            TabOrder = 0
          end
          object Panel12: TPanel
            Left = 0
            Top = 17
            Width = 614
            Height = 278
            Align = alClient
            TabOrder = 1
            object Splitter2: TSplitter
              Left = 251
              Top = 1
              Width = 5
              Height = 276
              Align = alRight
            end
            object lbxFiles: TListBox
              Left = 256
              Top = 1
              Width = 264
              Height = 276
              Align = alRight
              ItemHeight = 13
              MultiSelect = True
              TabOrder = 0
            end
            object Panel1: TPanel
              Left = 520
              Top = 1
              Width = 93
              Height = 276
              Align = alRight
              TabOrder = 1
              object Panel2: TPanel
                Left = 1
                Top = 1
                Width = 91
                Height = 187
                Align = alClient
                BevelOuter = bvNone
                TabOrder = 0
                object btnRetrieve: TButton
                  Left = 8
                  Top = 12
                  Width = 75
                  Height = 25
                  Caption = '&Recupera'
                  TabOrder = 0
                  OnClick = btnRetrieveClick
                end
                object btnStore: TButton
                  Left = 8
                  Top = 44
                  Width = 75
                  Height = 25
                  Caption = '&Salva'
                  TabOrder = 1
                  OnClick = btnStoreClick
                end
                object btnDelete: TButton
                  Left = 8
                  Top = 76
                  Width = 75
                  Height = 25
                  Caption = '&Exclui'
                  TabOrder = 2
                  OnClick = btnDeleteClick
                end
                object btnAbort: TButton
                  Left = 8
                  Top = 108
                  Width = 75
                  Height = 25
                  Caption = '&Aborta'
                  TabOrder = 3
                  OnClick = btnAbortClick
                end
                object BtDir: TButton
                  Left = 8
                  Top = 142
                  Width = 75
                  Height = 25
                  Caption = '&Pasta'
                  TabOrder = 4
                  OnClick = BtDirClick
                end
              end
              object rgDirStyle: TRadioGroup
                Left = 1
                Top = 188
                Width = 91
                Height = 87
                Align = alBottom
                Caption = ' Stilo: '
                ItemIndex = 0
                Items.Strings = (
                  'None'
                  'Unix'
                  'Dos'
                  'Multinet'
                  'Vms')
                TabOrder = 1
                OnClick = rgDirStyleClick
              end
            end
          end
        end
        object TabSheet15: TTabSheet
          Caption = 'Op'#231#245'es de transfer'#234'ncia'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label17: TLabel
            Left = 348
            Top = 26
            Width = 55
            Height = 13
            Caption = 'Reiniciar '#224's'
          end
          object Label18: TLabel
            Left = 340
            Top = 52
            Width = 61
            Height = 13
            Caption = 'Milisegundos'
          end
          object SendMode1: TRadioGroup
            Left = 8
            Top = 16
            Width = 97
            Height = 145
            Caption = ' Send Mode '
            ItemIndex = 1
            Items.Strings = (
              'Append'
              'Replace'
              'Unique'
              'Restart')
            TabOrder = 0
          end
          object ReceiveMode1: TRadioGroup
            Left = 120
            Top = 16
            Width = 97
            Height = 145
            Caption = ' Receive Mode '
            Ctl3D = True
            ItemIndex = 1
            Items.Strings = (
              'Append'
              'Replace'
              'Restart')
            ParentCtl3D = False
            TabOrder = 1
          end
          object FileType1: TRadioGroup
            Left = 232
            Top = 16
            Width = 81
            Height = 145
            Caption = ' File Type '
            ItemIndex = 0
            Items.Strings = (
              'Ascii'
              'Binary')
            TabOrder = 2
            OnClick = FileType1Click
          end
          object TimeoutEdit: TEdit
            Left = 406
            Top = 48
            Width = 57
            Height = 21
            TabOrder = 3
            OnExit = TimeoutEditExit
          end
          object edtRestartAt: TEdit
            Left = 406
            Top = 22
            Width = 57
            Height = 21
            TabOrder = 4
          end
        end
      end
    end
    object PnCliente: TPanel
      Left = 1
      Top = 1
      Width = 629
      Height = 361
      Align = alClient
      Color = clAppWorkSpace
      TabOrder = 0
      object Splitter1: TSplitter
        Left = 1
        Top = 98
        Width = 627
        Height = 3
        Cursor = crVSplit
        Align = alTop
      end
      object Memo: TLMDRichEdit
        Left = 61
        Top = 37
        Width = 520
        Height = 323
        TabStop = False
        ReadOnly = True
        TabOrder = 0
      end
      object ClItens: TCheckListBox
        Left = 1
        Top = 1
        Width = 627
        Height = 97
        Align = alTop
        ItemHeight = 13
        TabOrder = 1
      end
    end
    object PnPHP: TPanel
      Left = 1
      Top = 1
      Width = 629
      Height = 361
      Align = alClient
      Color = clAppWorkSpace
      TabOrder = 2
      Visible = False
      object Splitter3: TSplitter
        Left = 1
        Top = 49
        Width = 627
        Height = 3
        Cursor = crVSplit
        Align = alTop
      end
      object Memo2: TLMDRichEdit
        Left = 1
        Top = 1
        Width = 627
        Height = 48
        TabStop = False
        Align = alTop
        ReadOnly = True
        TabOrder = 0
      end
      object Memo3: TMemo
        Left = 1
        Top = 277
        Width = 627
        Height = 83
        Align = alBottom
        TabOrder = 1
        WantReturns = False
      end
      object Memo1: TMemo
        Left = 152
        Top = 72
        Width = 328
        Height = 52
        TabOrder = 2
        WantReturns = False
      end
      object WebBrowser1: TWebBrowser
        Left = 1
        Top = 141
        Width = 627
        Height = 136
        Align = alBottom
        TabOrder = 3
        ControlData = {
          4C000000CD4000000E0E00000000000000000000000000000000000000000000
          000000004C000000000000000000000001000000E0D057007335CF11AE690800
          2B2E126208000000000000004C0000000114020000000000C000000000000046
          8000000000000000000000000000000000000000000000000000000000000000
          00000000000000000100000000000000000000000000000000000000}
      end
      object STNavega: TStaticText
        Left = 1
        Top = 137
        Width = 627
        Height = 4
        Align = alBottom
        TabOrder = 4
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 362
      Width = 629
      Height = 20
      Align = alBottom
      TabOrder = 3
      object STTempo: TStaticText
        Left = 1
        Top = 2
        Width = 627
        Height = 17
        Align = alBottom
        Alignment = taCenter
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
    end
  end
  object PnConfirm: TPanel
    Left = 0
    Top = 448
    Width = 631
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtBaixa: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Baixa'
      Enabled = False
      TabOrder = 0
      OnClick = BtBaixaClick
      NumGlyphs = 2
    end
    object BtCancela: TBitBtn
      Tag = 15
      Left = 358
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Cancela'
      TabOrder = 1
      OnClick = BtCancelaClick
      NumGlyphs = 2
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 518
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtSaidaClick
      NumGlyphs = 2
    end
    object BtEnvia: TBitBtn
      Tag = 14
      Left = 112
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Envia'
      Enabled = False
      TabOrder = 3
      OnClick = BtEnviaClick
      NumGlyphs = 2
    end
    object BtAtualiza: TBitBtn
      Tag = 125
      Left = 204
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Atualiza'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 4
      OnClick = BtAtualizaClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo1: TPanel
    Left = 0
    Top = 431
    Width = 631
    Height = 17
    Align = alBottom
    TabOrder = 2
    object Label1: TLabel
      Left = 1
      Top = 1
      Width = 629
      Height = 15
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ExplicitWidth = 3
      ExplicitHeight = 13
    end
  end
  object PainelTitulo: TLMDPanelFill
    Left = 0
    Top = 0
    Width = 631
    Height = 48
    Align = alTop
    Bevel.StyleInner = bvFrameRaised
    Bevel.BorderColor = clWindow
    Bevel.EdgeStyle = etBump
    Bevel.Mode = bmCustom
    Caption = 'Transfer'#234'ncia de Arquivos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 3
    Transparent = True
    object Image1: TImage
      Left = 2
      Top = 2
      Width = 627
      Height = 44
      Align = alClient
      Transparent = True
    end
  end
  object OpenDialog2: TOpenDialog
    Left = 36
    Top = 13
  end
  object Timer1: TTimer
    OnTimer = Timer1Timer
    Left = 365
    Top = 121
  end
  object PMDir: TPopupMenu
    Left = 567
    Top = 265
    object Criar1: TMenuItem
      Caption = '&Criar'
      OnClick = Criar1Click
    end
    object Renomear1: TMenuItem
      Caption = '&Renomear'
      OnClick = Renomear1Click
    end
    object Excluir1: TMenuItem
      Caption = '&Excluir'
      OnClick = Excluir1Click
    end
  end
end
