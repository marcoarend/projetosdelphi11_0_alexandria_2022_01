unit wclients;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, Menus, ComCtrls, dmkEdit, dmkGeral, UnDmkProcFunc, UnDmkEnums;

type
  TFmwclients = class(TForm)
    PainelDados: TPanel;
    Dswclients: TDataSource;
    Qrwclients: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtUsuario: TBitBtn;
    BtCliente: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    PainelWeb: TPanel;
    Panel4: TPanel;
    BtConf2: TBitBtn;
    Panel6: TPanel;
    BitBtn2: TBitBtn;
    DBGClientes: TDBGrid;
    QrClientes: TmySQLQuery;
    Panel7: TPanel;
    DsClientes: TDataSource;
    EdPesq: TdmkEdit;
    Label3: TLabel;
    SpeedButton5: TSpeedButton;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLI: TWideStringField;
    QrClientesCNPJCPF: TWideStringField;
    QrWPesq: TmySQLQuery;
    QrWPesqCodigo: TIntegerField;
    QrClientesIERG: TWideStringField;
    QrClientesRua: TWideStringField;
    QrClientesNumero: TLargeintField;
    QrClientesCompl: TWideStringField;
    QrClientesBairro: TWideStringField;
    QrClientesCidade: TWideStringField;
    QrClientesCEP: TLargeintField;
    QrClientesUF: TLargeintField;
    QrClientesLimiCred: TFloatField;
    QrClientesTel1: TWideStringField;
    QrwclientsCNPJ: TWideStringField;
    QrwclientsIE: TWideStringField;
    QrwclientsNome: TWideStringField;
    QrwclientsRua: TWideStringField;
    QrwclientsNumero: TLargeintField;
    QrwclientsCompl: TWideStringField;
    QrwclientsBairro: TWideStringField;
    QrwclientsCidade: TWideStringField;
    QrwclientsUF: TWideStringField;
    QrwclientsCEP: TIntegerField;
    QrwclientsTel1: TWideStringField;
    QrwclientsRisco: TFloatField;
    QrwclientsCodigo: TIntegerField;
    Label2: TLabel;
    DBEdit2: TDBEdit;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    Label7: TLabel;
    Label97: TLabel;
    DBEdit106: TDBEdit;
    DBEdit9: TDBEdit;
    Label1: TLabel;
    Label4: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit12: TDBEdit;
    Label14: TLabel;
    DBEdit14: TDBEdit;
    DBEdit15: TDBEdit;
    Label104: TLabel;
    Label17: TLabel;
    DBEdit17: TDBEdit;
    DBEdit81: TDBEdit;
    Label80: TLabel;
    QrwclientsCNPJ_TXT: TWideStringField;
    QrwclientsCEP_TXT: TWideStringField;
    QrwclientsTEL1_TXT: TWideStringField;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    EdNome: TdmkEdit;
    EdCNPJ: TdmkEdit;
    EdIE: TdmkEdit;
    Label15: TLabel;
    EdRua: TdmkEdit;
    Label16: TLabel;
    EdNumero: TdmkEdit;
    Label18: TLabel;
    EdCompl: TdmkEdit;
    Label19: TLabel;
    EdBairro: TdmkEdit;
    Label20: TLabel;
    EdCidade: TdmkEdit;
    EdCEP: TdmkEdit;
    EdTel1: TdmkEdit;
    Label8: TLabel;
    Label10: TLabel;
    Label13: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label23: TLabel;
    Label24: TLabel;
    EdUF: TdmkEdit;
    EdRisco: TdmkEdit;
    Label25: TLabel;
    EdPassword: TdmkEdit;
    Label26: TLabel;
    EdUsername: TdmkEdit;
    QrwclientsPassword: TWideStringField;
    QrwclientsUsername: TWideStringField;
    QrwclientsLk: TIntegerField;
    QrwclientsDataCad: TDateField;
    QrwclientsDataAlt: TDateField;
    QrwclientsUserCad: TIntegerField;
    QrwclientsUserAlt: TIntegerField;
    PainelUser: TPanel;
    Panel8: TPanel;
    BitBtn1: TBitBtn;
    Panel9: TPanel;
    BitBtn3: TBitBtn;
    Panel10: TPanel;
    Label27: TLabel;
    Label28: TLabel;
    Label33: TLabel;
    Label34: TLabel;
    EdCodigo2: TdmkEdit;
    EdULogin: TdmkEdit;
    EdUSenha: TdmkEdit;
    EdUObs: TdmkEdit;
    PMCliente: TPopupMenu;
    Incluinovocliente1: TMenuItem;
    Alteraclienteatual1: TMenuItem;
    PMUsuario: TPopupMenu;
    Incluinovousurio1: TMenuItem;
    Excluiusurioatual1: TMenuItem;
    QrUsers: TmySQLQuery;
    DsUsers: TDataSource;
    QrUsersUser_ID: TAutoIncField;
    QrUsersCodigo: TIntegerField;
    QrUsersPassword: TWideStringField;
    QrUsersUsername: TWideStringField;
    QrUsersLoginID: TWideStringField;
    QrUsersObs: TWideStringField;
    QrUsersLk: TIntegerField;
    QrUsersDataCad: TDateField;
    QrUsersDataAlt: TDateField;
    QrUsersUserCad: TIntegerField;
    QrUsersUserAlt: TIntegerField;
    QrUsersLastAcess: TDateTimeField;
    EdUser2: TdmkEdit;
    EdPass2: TdmkEdit;
    Label29: TLabel;
    Label30: TLabel;
    Alterausurioatual1: TMenuItem;
    CkCriaLote: TCheckBox;
    QrUsersCriaLote: TSmallintField;
    QrUsersNOMECRIALOTE: TWideStringField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGUsuarios: TDBGrid;
    QrVisitascli: TmySQLQuery;
    DsVisitascli: TDataSource;
    DBGrid1: TDBGrid;
    QrVisitascliControle: TAutoIncField;
    QrVisitascliIP: TWideStringField;
    QrVisitascliDataHora: TDateTimeField;
    QrVisitascliUsuario: TIntegerField;
    QrLoc: TmySQLQuery;
    QrWPesqUsers: TmySQLQuery;
    QrWPesqUsersUser_ID: TIntegerField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtClienteClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrwclientsAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrwclientsAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrwclientsBeforeOpen(DataSet: TDataSet);
    procedure EdPesqKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton5Click(Sender: TObject);
    procedure EdPesqChange(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtConf2Click(Sender: TObject);
    procedure QrwclientsCalcFields(DataSet: TDataSet);
    procedure EdCNPJExit(Sender: TObject);
    procedure EdCEPExit(Sender: TObject);
    procedure EdTel1Exit(Sender: TObject);
    procedure QrClientesAfterOpen(DataSet: TDataSet);
    procedure QrClientesBeforeClose(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure Alteraclienteatual1Click(Sender: TObject);
    procedure Incluinovocliente1Click(Sender: TObject);
    procedure Incluinovousurio1Click(Sender: TObject);
    procedure PMUsuarioPopup(Sender: TObject);
    procedure Excluiusurioatual1Click(Sender: TObject);
    procedure QrwclientsBeforeClose(DataSet: TDataSet);
    procedure BtUsuarioClick(Sender: TObject);
    procedure Alterausurioatual1Click(Sender: TObject);
    procedure QrUsersCalcFields(DataSet: TDataSet);
    procedure PageControl1Change(Sender: TObject);
    procedure EdPass2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUser2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUsernameKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdPasswordKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdULoginKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdUSenhaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    ////
    procedure ReabreClientes;
    procedure ReopenUsers(User_ID: Integer);
    function GeralSenhaAleatoria(Tabela, Campo: String): String;
    function ObtemIdSenhaMaster(Ususario, Senha: String; Codigo: Integer): Integer;
    procedure AtualizarUserMaster(User_ID, Codigo: Integer; Pass, User: String);

  public
    { Public declarations }
    FSeq: Integer;
  end;

var
  Fmwclients: TFmwclients;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmwclients.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmwclients.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrwclientsCodigo.Value, LaRegistro.Caption[2]);
end;

/////////////////////////////////////////////////////////////////////////////////////

procedure TFmwclients.DefParams;
begin
  VAR_GOTOTABELA := 'wclients';
  VAR_GOTOMYSQLTABLE := Qrwclients;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := nil;
  VAR_GOTOMySQLDBNAME2 := Dmod.MyDBn;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM wclients');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmwclients.MostraEdicao(Mostra : Integer; Status : String; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
      PainelWeb.Visible   := False;
      PainelUser.Visible  := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if Status = CO_INCLUSAO then
      begin
        //N�o inclui direto no site
      end else begin
        EdCodigo.Text  := IntToStr(QrwclientsCodigo.Value);
        EdNome.Text    := QrwclientsNome.Value;
        EdCNPJ.Text    := QrwclientsCNPJ_TXT.Value;
        EdIE.Text      := QrwclientsIE.Value;
        EdRua.Text     := QrwclientsRua.Value;
        EdNumero.Text  := IntToStr(QrwclientsNumero.Value);
        EdCompl.Text   := QrwclientsCompl.Value;
        EdBairro.Text  := QrwclientsBairro.Value;
        EdCidade.Text  := QrwclientsCidade.Value;
        EdUF.Text      := QrwclientsUF.Value;
        EdCEP.Text     := QrwclientsCEP_TXT.Value;
        EdTel1.Text    := QrwclientsTEL1_TXT.Value;
        EdRisco.Text   := Geral.FFT(QrwclientsRisco.Value, 2, siPositivo);
        EdUser2.Text   := QrwclientsUsername.Value;
        EdPass2.Text   := QrwclientsPassword.Value;
        //
      end;
      EdNome.SetFocus;
    end;
    2:
    begin
      PainelWeb.Visible   := True;
      PainelDados.Visible := False;
      EdPesq.SetFocus;
      ReabreClientes;
    end;
    3:
    begin
      PainelUser.Visible   := True;
      PainelDados.Visible := False;
      EdULogin.SetFocus;
      if Status = CO_ALTERACAO then
      begin
        EdULogin.Text := QrUsersUsername.Value;
        EdUSenha.Text := QrUsersPassword.Value;
        EdUObs.Text   := QrUsersObs.Value;
      end;
    end;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmwclients.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmwclients.QueryPrincipalAfterOpen;
begin
end;

procedure TFmwclients.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmwclients.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmwclients.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmwclients.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmwclients.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmwclients.BtClienteClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCliente, BtCliente);
end;

procedure TFmwclients.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrwclientsCodigo.Value;
  Close;
end;

procedure TFmwclients.BtConfirmaClick(Sender: TObject);
var
  User_ID, Codigo: Integer;
  Nome, User, Pass: String;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  Nome   := EdNome.ValueVariant;
  User   := EdUser2.ValueVariant;
  Pass   := EdPass2.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  if MyObjects.FIC(Length(User) = 0, EdUser2, 'Login n�o definido!') then Exit;
  if MyObjects.FIC(Length(Pass) = 0, EdPass2, 'Senha n�o definida!') then Exit;
  //
  Dmod.QrWeb.Close;
  Dmod.QrWeb.SQL.Clear;
  Dmod.QrWeb.SQL.Add('UPDATE wclients SET CNPJ=:P0, IE=:P1, ');
  Dmod.QrWeb.SQL.Add('Username=:P2, Nome=:P3, Rua=:P4, Numero=:P5, Compl=:P6,');
  Dmod.QrWeb.SQL.Add('Bairro=:P7, Cidade=:P8, UF=:P9, CEP=:P10, Tel1=:P11, ');
  Dmod.QrWeb.SQL.Add('Risco=:P12, Password=:P13 WHERE Codigo=:Pa');
  //
  Dmod.QrWeb.Params[00].AsString  := Geral.SoNumero_TT(EdCNPJ.Text);
  Dmod.QrWeb.Params[01].AsString  := EdIE.Text;
  Dmod.QrWeb.Params[02].AsString  := User;
  Dmod.QrWeb.Params[03].AsString  := EdNome.Text;
  Dmod.QrWeb.Params[04].AsString  := EdRua.Text;
  Dmod.QrWeb.Params[05].AsInteger := Geral.IMV(EdNumero.Text);
  Dmod.QrWeb.Params[06].AsString  := EdCompl.Text;
  Dmod.QrWeb.Params[07].AsString  := EdBairro.Text;
  Dmod.QrWeb.Params[08].AsString  := EdCidade.Text;
  Dmod.QrWeb.Params[09].AsString  := EdUF.Text;
  Dmod.QrWeb.Params[10].AsString  := Geral.SoNumero_TT(EdCEP.Text);
  Dmod.QrWeb.Params[11].AsString  := EdTel1.Text;
  Dmod.QrWeb.Params[12].AsFloat   := Geral.DMV(EdRisco.Text);
  Dmod.QrWeb.Params[13].AsString  := Pass;
  //
  Dmod.QrWeb.Params[14].AsInteger := Codigo;
  Dmod.QrWeb.ExecSQL;
  //
  User_ID := ObtemIdSenhaMaster(EdUser2.Text, EdPass2.Text, Codigo);
  AtualizarUserMaster(User_ID, Codigo, Pass, User);
  //
  MostraEdicao(0, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
  if FSeq = 1 then Close;
end;

procedure TFmwclients.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmwclients.FormCreate(Sender: TObject);
begin
  FSeq := 0;
  Panel10.Align      := alClient;
  PainelEdita.Align  := alClient;
  PainelDados.Align  := alClient;
  PainelEdit.Align   := alClient;
  DBGClientes.Align  := alClient;
  PageControl1.Align := alClient;
  PainelUser.Align   := alClient;
  PageControl1.ActivePageIndex := 0;
  CriaOForm;
end;

procedure TFmwclients.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrwclientsCodigo.Value,LaRegistro.Caption);
end;

procedure TFmwclients.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmwclients.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrwclientsCodigo.Value;
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmwclients.QrwclientsAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  if Qrwclients.RecordCount > 0 then
    BtUsuario.Enabled := True
  else
    BtUsuario.Enabled := False;
end;

procedure TFmwclients.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'wclients', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, CO_INCLUSAO, 0);
end;

procedure TFmwclients.QrwclientsAfterScroll(DataSet: TDataSet);
begin
  ReopenUsers(0);
end;

procedure TFmwclients.SbQueryClick(Sender: TObject);
begin
  LocCod(QrwclientsCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'wclients', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmwclients.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

function TFmwclients.ObtemIdSenhaMaster(Ususario, Senha: String; Codigo: Integer): Integer;
begin
  QrLoc.Close;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT User_ID');
  QrLoc.SQL.Add('FROM users');
  QrLoc.SQL.Add('WHERE UserName=:P0');
  QrLoc.SQL.Add('AND PassWord=:P1');
  QrLoc.SQL.Add('AND Codigo=:P2');
  QrLoc.Params[0].AsString  := Ususario;
  QrLoc.Params[1].AsString  := Senha;
  QrLoc.Params[2].AsInteger := Codigo;
  QrLoc.Open;
  if QrLoc.RecordCount > 0 then
    Result := QrLoc.FieldByName('User_ID').Value
  else
    Result := 0;
end;

function TFmwclients.GeralSenhaAleatoria(Tabela, Campo: String): String;
  function SenhaExiste(Tabela, Campo, Senha: String): Boolean;
  begin
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT ' + Campo + '');
    QrLoc.SQL.Add('FROM ' + Tabela);
    QrLoc.SQL.Add('WHERE Password="' + Senha + '"');
    QrLoc.Open;
    if QrLoc.RecordCount > 0 then
      Result := True
    else
      Result := False;
  end;
var
  Existe: Boolean;
  Senha: String;
begin
  repeat
    Senha  := dmkPF.GeraPassword(True, False, True, 8);
    Existe := SenhaExiste(Tabela, Campo, Senha);
  until
    Existe = False;
  Result := Senha;
end;

procedure TFmwclients.QrwclientsBeforeOpen(DataSet: TDataSet);
begin
  QrwclientsCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmwclients.EdPesqKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  ReabreClientes;
end;

procedure TFmwclients.SpeedButton5Click(Sender: TObject);
begin
  ReabreClientes;
end;

procedure TFmwclients.ReabreClientes;
begin
  QrClientes.Close;
  QrClientes.Params[0].AsString := '%'+EdPesq.Text+'%';
  QrClientes.Open;
end;

procedure TFmwclients.EdPass2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
    EdPass2.ValueVariant := GeralSenhaAleatoria('wclients', 'Password');
end;

procedure TFmwclients.EdPasswordKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
    EdPassword.ValueVariant := GeralSenhaAleatoria('wclients', 'Password');
end;

procedure TFmwclients.EdPesqChange(Sender: TObject);
begin
  QrClientes.Close;
end;

procedure TFmwclients.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmwclients.BtConf2Click(Sender: TObject);
var
  User_ID: Integer;
  User, Pass: String; 
begin
  User := Trim(EdUsername.Text);
  Pass := Trim(EdPassword.Text); 
  //
  if QrClientes.RecordCount = 0 then
  begin
    Geral.MensagemBox('Nenhum cliente foi selecionado!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  QrWPesq.Close;
  QrWPesq.Params[0].AsInteger := QrClientesCodigo.Value;
  QrWPesq.Open;
  if QrWPesq.RecordCount > 0 then
  begin
    Geral.MensagemBox('Este cliente j� est� cadastrado na web!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if (Trim(EdUsername.Text) = '') or (Trim(EdPassword.Text) = '') then
  begin
    Geral.MensagemBox('Informa��es de Login e/ou senha inicial incompletas!',
      'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  try
    Dmod.QrWeb.Close;
    Dmod.QrWeb.SQL.Clear;
    Dmod.QrWeb.SQL.Add('INSERT INTO wclients SET Codigo=:P0, CNPJ=:P1, IE=:P2, ');
    Dmod.QrWeb.SQL.Add('Password=:P3, Nome=:P4, Rua=:P5, Numero=:P6, ');
    Dmod.QrWeb.SQL.Add('Compl=:P7, Bairro=:P8, Cidade=:P9, UF=:P10, CEP=:P11, ');
    Dmod.QrWeb.SQL.Add('Tel1=:P12, Risco=:P13, Username=:P14');
    //
    Dmod.QrWeb.Params[00].AsInteger := QrClientesCodigo.Value;
    Dmod.QrWeb.Params[01].AsString  := QrClientesCNPJCPF.Value;
    Dmod.QrWeb.Params[02].AsString  := QrClientesIERG.Value;
    Dmod.QrWeb.Params[03].AsString  := Pass;
    Dmod.QrWeb.Params[04].AsString  := QrClientesNOMECLI.Value;
    Dmod.QrWeb.Params[05].AsString  := QrClientesRua.Value;
    Dmod.QrWeb.Params[06].AsInteger := QrClientesNumero.Value;
    Dmod.QrWeb.Params[07].AsString  := QrClientesCompl.Value;
    Dmod.QrWeb.Params[08].AsString  := QrClientesBairro.Value;
    Dmod.QrWeb.Params[09].AsString  := QrClientesCidade.Value;
    Dmod.QrWeb.Params[10].AsString  := MLAGeral.GetSiglaUF_do_CodigoUF(QrClientesUF.Value);
    Dmod.QrWeb.Params[11].AsInteger := QrClientesCEP.Value;
    Dmod.QrWeb.Params[12].AsString  := QrClientesTel1.Value;
    Dmod.QrWeb.Params[13].AsFloat   := QrClientesLimiCred.Value;
    Dmod.QrWeb.Params[14].AsString  := User;
    Dmod.QrWeb.ExecSQL;
    //
    User_ID := ObtemIdSenhaMaster(User, Pass, QrClientesCodigo.Value);
    AtualizarUserMaster(User_ID, QrClientesCodigo.Value, Pass, User);
    //
    Va(vpLast);
    //
    Geral.MensagemBox('Cliente adicionado � web!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  except
    Geral.MensagemBox('N�o foi poss�vel adicionar o cliente � web!',
      'Aviso', MB_OK+MB_ICONWARNING);
    raise;
  end;
end;

procedure TFmwclients.QrwclientsCalcFields(DataSet: TDataSet);
begin
  QrwclientsCNPJ_TXT.Value := Geral.FormataCNPJ_TT(QrwclientsCNPJ.Value);
  QrwclientsCEP_TXT.Value  := Geral.FormataCEP_NT(QrwclientsCEP.Value);
  QrwclientsTEL1_TXT.value := Geral.FormataTelefone_TT(QrwclientsTel1.Value);
end;

procedure TFmwclients.EdCNPJExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCNPJ.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if MLAGeral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Geral.MensagemBox(PChar(SMLA_NUMEROINVALIDO2), 'Aviso', MB_OK+MB_ICONWARNING);
      EdCNPJ.SetFocus;
    end else EdCNPJ.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCNPJ.Text := CO_VAZIO;
end;

procedure TFmwclients.EdCEPExit(Sender: TObject);
begin
  EdCEP.Text := MLAGeral.FormataCEP_TT(EdCEP.Text);
end;

procedure TFmwclients.EdTel1Exit(Sender: TObject);
begin
  EdTel1.Text := Geral.FormataTelefone_TT(Geral.SoNumero_TT(EdTel1.Text));
end;

procedure TFmwclients.EdULoginKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
    EdULogin.ValueVariant := GeralSenhaAleatoria('users', 'Username');
end;

procedure TFmwclients.EdUSenhaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
    EdUSenha.ValueVariant := GeralSenhaAleatoria('users', 'Password');
end;

procedure TFmwclients.EdUser2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
    EdUser2.ValueVariant := GeralSenhaAleatoria('wclients', 'Username');
end;

procedure TFmwclients.EdUsernameKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
    EdUsername.ValueVariant := GeralSenhaAleatoria('wclients', 'Username');
end;

procedure TFmwclients.QrClientesAfterOpen(DataSet: TDataSet);
begin
  if QrClientes.RecordCount > 0 then BtConf2.Enabled := True
  else BtConf2.Enabled := False;
end;

procedure TFmwclients.QrClientesBeforeClose(DataSet: TDataSet);
begin
  BtConf2.Enabled := False;
end;

procedure TFmwclients.BitBtn1Click(Sender: TObject);
  function VerificaSeUsuarioExiste(Usuario, Senha: String; Codigo: Integer): Integer;
  begin
    Result := 0;
    //
    QrWPesqUsers.Close;
    QrWPesqUsers.Params[0].AsString  := Usuario;
    QrWPesqUsers.Params[1].AsString  := Senha;
    QrWPesqUsers.Params[2].AsInteger := Codigo;
    QrWPesqUsers.Open;
    if QrWPesqUsers.RecordCount > 0 then
    begin
      if LaTipo.Caption = CO_ALTERACAO then
      begin
        if QrUsersUser_ID.Value <> QrWPesqUsersUser_ID.Value then
          Result := QrWPesqUsersUser_ID.Value;
      end else
        Result := QrWPesqUsersUser_ID.Value;
    end;
  end;
var
  User, Pass: String;
  Codigo: Integer;
begin
  Codigo := QrwclientsCodigo.Value;
  User   := EdULogin.ValueVariant;
  Pass   := EdUSenha.ValueVariant;
  //
  if MyObjects.FIC(Length(User) = 0, EdULogin, 'Login n�o definido!') then Exit;
  if MyObjects.FIC(Length(Pass) = 0, EdUSenha, 'Senha n�o definida!') then Exit;
  if VerificaSeUsuarioExiste(User, Pass, Codigo) > 0 then
  begin
    Geral.MensagemBox('Este usu�rio j� est� cadastrado na web!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  Dmod.QrWeb.SQL.Clear;
  Dmod.QrWeb.SQL.Add('INSERT INTO users SET ');
  Dmod.QrWeb.SQL.Add('Username=:P0, ');
  Dmod.QrWeb.SQL.Add('Password=:P1, ');
  Dmod.QrWeb.SQL.Add('Codigo=:P2, ');
  Dmod.QrWeb.SQL.Add('Obs=:P3,');
  Dmod.QrWeb.SQL.Add('CriaLote=:P4');
  Dmod.QrWeb.Params[00].AsString  := User;
  Dmod.QrWeb.Params[01].AsString  := Pass;
  Dmod.QrWeb.Params[02].AsInteger := Codigo;
  Dmod.QrWeb.Params[03].AsString  := EdUObs.Text;
  Dmod.QrWeb.Params[04].AsInteger := MLAGeral.BoolToInt(CkCriaLote.Checked);
  //
  if LaTipo.Caption = CO_ALTERACAO then
  begin
    Dmod.QrWeb.SQL[0] := 'UPDATE users SET ';
    Dmod.QrWeb.SQL.Add('WHERE User_ID=:Pa');
    Dmod.QrWeb.Params[05].AsInteger := QrUsersUser_ID.Value;
    Dmod.QrWeb.ExecSQL;
    //
    ReopenUsers(QrUsersUser_ID.Value);
    MostraEdicao(0, CO_TRAVADO, 0);
  end else begin
    Dmod.QrWeb.ExecSQL;
    //
    Geral.MensagemBox('Usu�rio adicionado!', 'Adi��o de usu�rio', MB_OK+
    MB_ICONWARNING);
    //
    EdULogin.Text := '';
    EdUSenha.Text := '';
    EdUObs.Text := '';
    //
    EdULogin.SetFocus;
    //
    ReopenUsers(0);
  end;
end;

procedure TFmwclients.Alteraclienteatual1Click(Sender: TObject);
begin
  MostraEdicao(1, CO_ALTERACAO, 0);
end;

procedure TFmwclients.Incluinovocliente1Click(Sender: TObject);
begin
  MostraEdicao(2, CO_INCLUSAO, 0);
end;

procedure TFmwclients.PMUsuarioPopup(Sender: TObject);
begin
  if QrUsers.RecordCount > 0 then
    Excluiusurioatual1.Enabled := True
  else
    Excluiusurioatual1.Enabled := False;

end;

procedure TFmwclients.Excluiusurioatual1Click(Sender: TObject);
begin
  if Geral.MensagemBox(PChar('Confirma a exclus�o do usu�rio "'+
    QrUsersUsername.Value + '"'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
    ID_YES then
    begin
      Dmod.QrWeb.SQL.Clear;
      Dmod.QrWeb.SQL.Add('DELETE FROM users WHERE ');
      Dmod.QrWeb.SQL.Add('User_ID=:P0');
      //
      Dmod.QrWeb.Params[00].AsInteger := QrUsersUser_ID.Value;
      Dmod.QrWeb.ExecSQL;
      //
      ReopenUsers(0);
    end;
end;

procedure TFmwclients.ReopenUsers(User_ID: Integer);
begin
  QrUsers.Close;
  QrUsers.Params[0].AsInteger := QrwclientsCodigo.Value;
  QrUsers.Params[1].AsString  := QrwclientsUsername.Value;
  QrUsers.Params[2].AsString  := QrwclientsPassword.Value;
  QrUsers.Open;
  if User_ID <> 0 then
    QrUsers.Locate('User_ID', User_ID, []);
end;

procedure TFmwclients.QrwclientsBeforeClose(DataSet: TDataSet);
begin
  BtUsuario.Enabled := False;
end;

procedure TFmwclients.BtUsuarioClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMUsuario, BtUsuario);
end;

procedure TFmwclients.Incluinovousurio1Click(Sender: TObject);
begin
  MostraEdicao(3, CO_INCLUSAO, 0);
end;

procedure TFmwclients.Alterausurioatual1Click(Sender: TObject);
begin
  MostraEdicao(3, CO_ALTERACAO, 0);
end;

procedure TFmwclients.AtualizarUserMaster(User_ID, Codigo: Integer; Pass, User: String);
begin
  if User_ID > 0 then
  begin
    Dmod.QrWeb.Close;
    Dmod.QrWeb.SQL.Clear;
    Dmod.QrWeb.SQL.Add('UPDATE users SET Password=:P0, Username=:P1 WHERE User_ID=:P2');
    Dmod.QrWeb.Params[0].AsString  := Pass;
    Dmod.QrWeb.Params[1].AsString  := User;
    Dmod.QrWeb.Params[2].AsInteger := User_ID;
    Dmod.QrWeb.ExecSQL;
  end else
  begin
    Dmod.QrWeb.Close;
    Dmod.QrWeb.SQL.Clear;
    Dmod.QrWeb.SQL.Add('INSERT INTO users SET Password=:P0, Username=:P1, ');
    Dmod.QrWeb.SQL.Add('CriaLote=1, Codigo=:P2');
    Dmod.QrWeb.Params[0].AsString  := Pass;
    Dmod.QrWeb.Params[1].AsString  := User;
    Dmod.QrWeb.Params[2].AsInteger := Codigo;
    Dmod.QrWeb.ExecSQL;
  end;
end;

procedure TFmwclients.QrUsersCalcFields(DataSet: TDataSet);
begin
  if QrUsersCriaLote.Value = 0 then
    QrUsersNOMECRIALOTE.Value := 'N�o'
  else
    QrUsersNOMECRIALOTE.Value := 'Sim';
end;

procedure TFmwclients.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 1 then
  begin
    QrVisitascli.Close;
    QrVisitascli.Params[0].AsInteger := QrUsersUser_ID.Value;
    QrVisitascli.Open;
  end;
end;

end.

