unit EmitentesEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, Mask, DBCtrls, Db, mySQLDbTables, dmkEdit,
  dmkGeral, UnDmkEnums;

type
  TFmEmitentesedit = class(TForm)
    Panel2: TPanel;
    Label1: TLabel;
    EdBanda: TdmkEdit;
    Label6: TLabel;
    EdComp: TdmkEdit;
    EdBanco: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdAgencia: TdmkEdit;
    Label4: TLabel;
    EdConta: TdmkEdit;
    Label5: TLabel;
    EdCheque: TdmkEdit;
    Label9: TLabel;
    EdCPF: TdmkEdit;
    Label10: TLabel;
    EdEmitente: TdmkEdit;
    Panel3: TPanel;
    BtConfirma: TBitBtn;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    BtDesiste: TBitBtn;
    EdRisco: TdmkEdit;
    Label7: TLabel;
    QrBanco: TmySQLQuery;
    QrBancoCodigo: TIntegerField;
    QrBancoNome: TWideStringField;
    QrBancoLk: TIntegerField;
    QrBancoDataCad: TDateField;
    QrBancoDataAlt: TDateField;
    QrBancoUserCad: TIntegerField;
    QrBancoUserAlt: TIntegerField;
    QrBancoSite: TWideStringField;
    DsBanco: TDataSource;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    EdCMC_7: TdmkEdit;
    Label11: TLabel;
    EdRealCC: TdmkEdit;
    procedure EdBandaChange(Sender: TObject);
    procedure EdCompExit(Sender: TObject);
    procedure EdBancoExit(Sender: TObject);
    procedure EdAgenciaExit(Sender: TObject);
    procedure EdChequeExit(Sender: TObject);
    procedure EdContaExit(Sender: TObject);
    procedure EdCPFExit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdBancoChange(Sender: TObject);
    procedure ReopenBanco;
    procedure EdCMC_7Change(Sender: TObject);
  private
    { Private declarations }
    procedure LimpaEdits;
    procedure LocalizaEmitente;
    function CriaBAC_Pesq(Banco, Agencia, Conta: String; DVCC: Integer): String;
  public
    { Public declarations }
    FLaTipo: String;
    FIncluiu: Boolean;
  end;

var
  FmEmitentesedit: TFmEmitentesedit;

implementation

uses Principal, UnMLAGeral, UnInternalConsts, Emitentes,
Module, Modulc, UnMyObjects;

{$R *.DFM}

procedure TFmEmitentesedit.EdBandaChange(Sender: TObject);
begin
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
    EdCMC_7.Text := Geral.SoNumero_TT(EdBanda.Text);
end;

procedure TFmEmitentesedit.EdCompExit(Sender: TObject);
begin
  EdComp.Text := MLAGeral.TFD2(EdComp.Text, 3, False);
end;

procedure TFmEmitentesedit.EdBancoExit(Sender: TObject);
begin
  EdBanco.Text := MLAGeral.TFD2(EdBanco.Text, 3, False);
  ReopenBanco;
end;

procedure TFmEmitentesedit.EdAgenciaExit(Sender: TObject);
begin
  EdAgencia.Text := MLAGeral.TFD2(EdAgencia.Text, 4, False);
end;

procedure TFmEmitentesedit.EdContaExit(Sender: TObject);
begin
  EdConta.Text := MLAGeral.TFD2(EdConta.Text, 10, False);
end;

procedure TFmEmitentesedit.EdChequeExit(Sender: TObject);
begin
  EdCheque.Text := MLAGeral.TFD2(EdCheque.Text, 6, False);
end;

procedure TFmEmitentesedit.EdCPFExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCPF.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if MLAGeral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Application.MessageBox(PChar('N�mero inv�lido para CNPJ/CPF'), 'Erro',
      MB_OK+MB_ICONERROR);
      EdCPF.SetFocus;
    end else EdCPF.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCPF.Text := CO_VAZIO;
end;

procedure TFmEmitentesedit.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_ESCAPE then LimpaEdits;
end;

procedure TFmEmitentesedit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmEmitentesedit.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEmitentesedit.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmEmitentesedit.BtConfirmaClick(Sender: TObject);
begin
  Dmoc.AtualizaEmitente(EdBanco.Text, EdAgencia.Text, EdConta.Text,
    EdCPF.Text, EdEmitente.Text, Geral.DMV(EdRisco.Text));
  FmEmitentes.ReopenEmitentes(EdBanco.Text+EdAgencia.Text+EdConta.Text);
  //Close;

end;

procedure TFmEmitentesedit.LimpaEdits;
begin
  Edbanda.Text   := '';
  EdComp.Text    := '000';
  EdBanco.Text   := '000';
  EdConta.Text   := '0000000000';
  EdAgencia.Text := '0000';
  EdCheque.Text  := '000000';
  //
  EdCPF.Text     := '';
  EdEmitente.Text := '';
  EdBanda.SetFocus;
end;

procedure TFmEmitentesedit.FormCreate(Sender: TObject);
begin
  EdRisco.Text := Geral.FFT(Dmod.QrControleCHRisco.Value, 2, siPositivo);
end;

procedure TFmEmitentesedit.ReopenBanco;
begin
  QrBanco.Close;
  QrBanco.Params[0].AsString := EdBanco.Text;
  QrBanco.Open;
end;

procedure TFmEmitentesedit.EdBancoChange(Sender: TObject);
begin
  if not EdBanco.Focused then ReopenBanco;
end;

procedure TFmEmitentesedit.EdCMC_7Change(Sender: TObject);
var
  Banda: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC_7.Text) = 0 then
  begin
    Banda := TBandaMagnetica.Create;
    Banda.BandaMagnetica := EdCMC_7.Text;
    EdComp.Text    := Banda.Compe;
    EdBanco.Text   := Banda.Banco;
    EdAgencia.Text := Banda.Agencia;
    EdConta.Text   := Banda.Conta;
    EdCheque.Text  := Banda.Numero;
    //
    LocalizaEmitente;
    EdCPF.SetFocus;
  end;
end;

procedure TFmEmitentesEdit.LocalizaEmitente;
var
  B,A,C: String;
begin
  if  (Geral.IMV(EdBanco.Text) > 0)
  and (Geral.IMV(EdAgencia.Text) > 0)
  and (Geral.DMV(EdConta.Text) > 0.1) then
  begin
    Screen.Cursor := crHourGlass;
    try
      B := EdBanco.Text;
      A := EdAgencia.Text;
      C := EdConta.Text;
      Dmoc.QrLocEmiBAC.Close;
      Dmoc.QrLocEmiBAC.Params[0].AsInteger := Dmoc.QrBancoDVCC.Value;
      Dmoc.QrLocEmiBAC.Params[1].AsString  := CriaBAC_Pesq(B,A,C,Dmoc.QrBancoDVCC.Value);
      Dmoc.QrLocEmiBAC.Open;
      //
      if Dmoc.QrLocEmiBAC.RecordCount = 0 then
      begin
        EdEmitente.Text := '';
        EdCPF.Text := '';
      end else begin
        Dmoc.QrLocEmiCPF.Close;
        Dmoc.QrLocEmiCPF.Params[0].AsString := Dmoc.QrLocEmiBACCPF.Value;
        Dmoc.QrLocEmiCPF.Open;
        //
        if Dmoc.QrLocEmiCPF.RecordCount = 0 then
        begin
          EdEmitente.Text := '';
          EdCPF.Text := '';
        end else begin
          EdEmitente.Text := Dmoc.QrLocEmiCPFNome.Value;
          EdCPF.Text      := Geral.FormataCNPJ_TT(Dmoc.QrLocEmiCPFCPF.Value);
        end;
      end;
      Screen.Cursor := crDefault;
    except
      Screen.Cursor := crDefault;
      raise;
    end;
  end;
end;

function TFmEmitentesEdit.CriaBAC_Pesq(Banco, Agencia, Conta: String; DVCC: Integer): String;
begin
  EdRealCC.Text := Copy(Conta, 10-DVCC+1, DVCC);
  Result := Banco+Agencia+EdRealCC.Text;
end;

end.
