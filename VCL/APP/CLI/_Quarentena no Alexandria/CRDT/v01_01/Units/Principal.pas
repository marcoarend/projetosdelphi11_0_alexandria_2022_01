unit Principal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  UnMLAGeral, UMySQLModule, ExtCtrls, Menus, Grids, DBGrids, Db, (*DBTables,*)
  TypInfo, StdCtrls, ZCF2, ToolWin, ComCtrls, UnInternalConsts, ExtDlgs, jpeg,
  mySQLDbTables, Buttons, WinSkinData, ArquivosSCX, Backup3, WinSkinStore,
  MyVCLSkin, Urlmon, Registry, IniFiles, ImgList, Winsock, frxClass, dmkEdit,
  Sockets, dmkGeral, UnMyObjects, Variants, LinkRankSkin, UnDmkProcFunc,
  UnDmkEnums, DMKpnfsConversao;

type
  TcpCalc = (cpJurosMes, cpMulta);
  TBandaMagnetica = class(TObject)
  private
    FBandaMagnetica: String;
    FCompe: String;
    FBanco: String;
    FAgencia: String;
    FConta: String;
    FNumero: String;
    FDv1: Char;
    FDv2: Char;
    FDv3: Char;
    FTipo: Char;
    procedure DestrincharBanda(Banda: String);
  public
    property BandaMagnetica: String read FBandaMagnetica write DestrincharBanda;
    property Compe: String read FCompe;
    property Banco: String read FBanco;
    property Agencia: String read FAgencia;
    property Conta: String read FConta;
    property Numero: String read FNumero;
    property Dv1: Char read FDv1;
    property Dv2: Char read FDv2;
    property Dv3: Char read FDv3;
    property Tipo: Char read FTipo;
  end;
  TFmPrincipal = class(TForm)
    MainMenu1: TMainMenu;
    Arquivo1: TMenuItem;
    Sair1: TMenuItem;
    Logoffde1: TMenuItem;
    Cadastros1: TMenuItem;
    OpenPictureDialog1: TOpenPictureDialog;
    Ferramentas1: TMenuItem;
    Imagemdefundo1: TMenuItem;
    Entidades1: TMenuItem;
    BD1: TMenuItem;
    Verifica1: TMenuItem;
    VCLSkin1: TMenuItem;
    Timer1: TTimer;
    Dump1: TMenuItem;
    Backup1: TMenuItem;
    Diversos1: TMenuItem;
    Panel1: TPanel;
    BtEntidades2: TBitBtn;
    BtCNAB240: TBitBtn;
    BtProtecao: TBitBtn;
    PMProdImp: TPopupMenu;
    Estoque1: TMenuItem;
    Histrico1: TMenuItem;
    Vendas1: TMenuItem;
    BtBorderos: TBitBtn;
    BtDownLotes: TBitBtn;
    BtCalendario: TBitBtn;
    QrView1: TmySQLQuery;
    QrView1Codigo: TIntegerField;
    QrView1Campo: TIntegerField;
    QrView1Topo: TIntegerField;
    QrView1MEsq: TIntegerField;
    QrView1Larg: TIntegerField;
    QrView1FTam: TIntegerField;
    QrView1Negr: TIntegerField;
    QrView1Ital: TIntegerField;
    QrView1Lk: TIntegerField;
    QrView1DataCad: TDateField;
    QrView1DataAlt: TDateField;
    QrView1UserCad: TIntegerField;
    QrView1UserAlt: TIntegerField;
    QrView1Altu: TIntegerField;
    QrView1Pref: TWideStringField;
    QrView1Sufi: TWideStringField;
    QrView1Padr: TWideStringField;
    QrTopos1: TmySQLQuery;
    QrTopos1TopR: TIntegerField;
    QrView1AliV: TIntegerField;
    QrView1AliH: TIntegerField;
    QrView1TopR: TIntegerField;
    QrView1PrEs: TIntegerField;
    StatusBar: TStatusBar;
    PMGeral: TPopupMenu;
    Entidades2: TMenuItem;
    VerificaPdx1: TMenuItem;
    Bancos1: TMenuItem;
    Alneasdedevoluo1: TMenuItem;
    Ocorrnciasbancrias1: TMenuItem;
    Feriados1: TMenuItem;
    BtEvolucapi: TBitBtn;
    Especficos1: TMenuItem;
    OpenDialog1: TOpenDialog;
    QrSacado: TmySQLQuery;
    QrSacadoCNPJ: TWideStringField;
    QrSacadoNome: TWideStringField;
    QrSacadoRua: TWideStringField;
    QrSacadoCompl: TWideStringField;
    QrSacadoBairro: TWideStringField;
    QrSacadoCidade: TWideStringField;
    QrSacadoUF: TWideStringField;
    QrSacadoCEP: TIntegerField;
    QrSacadoTel1: TWideStringField;
    QrSacadoRisco: TFloatField;
    Pagina: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Grade: TStringGrid;
    ImgPrincipal: TImage;
    Taxas1: TMenuItem;
    BtDuplicataOcorr: TBitBtn;
    BtCheque: TBitBtn;
    Impresses1: TMenuItem;
    Scios1: TMenuItem;
    Operaes1: TMenuItem;
    ContratodeFomentoMercantil1: TMenuItem;
    Textos1: TMenuItem;
    ContratosdeFomentoMercantil1: TMenuItem;
    BtEuroExport: TBitBtn;
    N1: TMenuItem;
    Taxadejurocomposto1: TMenuItem;
    Jurocomposto1: TMenuItem;
    Juros1: TMenuItem;
    BtEntidadesExclamacao: TBitBtn;
    SkinStore1: TSkinStore;
    sd1: TSkinData;
    Financeiros1: TMenuItem;
    Basicos1: TMenuItem;
    Contratos1: TMenuItem;
    Contas1: TMenuItem;
    QrCreditado: TmySQLQuery;
    QrSumN: TmySQLQuery;
    QrSumNDebito: TFloatField;
    QrSumL: TmySQLQuery;
    QrSumLTotal: TFloatField;
    QrCreditadoBAC: TWideStringField;
    QrCreditadoNome: TWideStringField;
    QrCreditadoCPF: TWideStringField;
    QrCreditadoBanco: TIntegerField;
    QrCreditadoAgencia: TIntegerField;
    QrCreditadoConta: TWideStringField;
    Depsitos1: TMenuItem;
    AlneasdeDuplicatas1: TMenuItem;
    BtDuplicata: TBitBtn;
    BtChequeDeposito: TBitBtn;
    Serviosdowindows1: TMenuItem;
    Risco1: TMenuItem;
    Cheque1: TMenuItem;
    TabSheet3: TTabSheet;
    Emitentes1: TMenuItem;
    Sacados1: TMenuItem;
    Movimento1: TMenuItem;
    NotasFiscais1: TMenuItem;
    ClientesRpido1: TMenuItem;
    TimerIdle: TTimer;
    BtMalote: TBitBtn;
    SB2: TStatusBar;
    QrMalotes: TmySQLQuery;
    BtRiscoAll: TBitBtn;
    QrECartaSac: TmySQLQuery;
    QrECartaSacCodigo: TIntegerField;
    QrECartaSacLote: TSmallintField;
    TabSheet4: TTabSheet;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    TabSheet6: TTabSheet;
    DsMalotes: TDataSource;
    DsECartaSac: TDataSource;
    DBGrid2: TDBGrid;
    QrECartaSacData: TDateField;
    QrECartaSacTotal: TFloatField;
    QrECartaSacNOMECLIENTE: TWideStringField;
    QrMalotesLote: TSmallintField;
    QrMalotesData: TDateField;
    QrMalotesTotal: TFloatField;
    QrMalotesCodigo: TIntegerField;
    QrMalotesNOMECLIENTE: TWideStringField;
    DBGrid1: TDBGrid;
    N2: TMenuItem;
    Verificarnovaverso1: TMenuItem;
    Master1: TMenuItem;
    DB1: TMenuItem;
    Mostraestruturadecriao1: TMenuItem;
    BtVersao: TBitBtn;
    MySQL1: TMenuItem;
    Testes1: TMenuItem;
    PMRepasses: TPopupMenu;
    Cheques2: TMenuItem;
    Duplicatas1: TMenuItem;
    Resultado1: TMenuItem;
    TabSheet7: TTabSheet;
    BitBtn2: TBitBtn;
    QrLotes: TmySQLQuery;
    QrLotesCodigo: TIntegerField;
    QrLotesIts: TmySQLQuery;
    QrLotesItsITENS: TLargeintField;
    ProgressBar1: TProgressBar;
    Inadimplncia1: TMenuItem;
    BtResultados: TBitBtn;
    BtInadimplencia: TBitBtn;
    Conexo1: TMenuItem;
    N3: TMenuItem;
    Relatorios1: TMenuItem;
    Alneas1: TMenuItem;
    Pagamentodechequesdevolvidos1: TMenuItem;
    N4: TMenuItem;
    Ajuda1: TMenuItem;
    Histricodealteraes1: TMenuItem;
    Histrico2: TMenuItem;
    BtHistorico: TBitBtn;
    Seleciona1: TMenuItem;
    Original1: TMenuItem;
    Bruto1: TMenuItem;
    PMResultados: TPopupMenu;
    Bruto2: TMenuItem;
    SoundEx1: TMenuItem;
    N5: TMenuItem;
    LquidoNovo1: TMenuItem;
    GruposdeSacadosEmitentes1: TMenuItem;
    LocalizarPagamento1: TMenuItem;
    N6: TMenuItem;
    N7: TMenuItem;
    Excluiritensdelote1: TMenuItem;
    QrLoc: TmySQLQuery;
    QrLocITENS: TLargeintField;
    Carteiras2: TMenuItem;
    Exportacontabilidade1: TMenuItem;
    Exportadadoscontabilidade1: TMenuItem;
    Messenger1: TMenuItem;
    Conecta1: TMenuItem;
    State: TImageList;
    Timer2: TTimer;
    Desconecta1: TMenuItem;
    TabSheet9: TTabSheet;
    ImageList1: TImageList;
    odSend: TOpenDialog;
    odAvatar: TOpenDialog;
    pmBuddy: TPopupMenu;
    Block1: TMenuItem;
    Unblock1: TMenuItem;
    Delete2: TMenuItem;
    MenuItem1: TMenuItem;
    SendMessage1: TMenuItem;
    SendFile1: TMenuItem;
    SendShake1: TMenuItem;
    pmGroup: TPopupMenu;
    MenuItem2: TMenuItem;
    Delete1: TMenuItem;
    pmRoot: TPopupMenu;
    ChangeNickname1: TMenuItem;
    ChangeStatus1: TMenuItem;
    QrSender: TmySQLQuery;
    QrSenderCodigo: TIntegerField;
    DBGrid5: TDBGrid;
    DsMSNs: TDataSource;
    QrLocMSN: TmySQLQuery;
    QrLocMSNCodigo: TIntegerField;
    QrLocMSNItens: TIntegerField;
    Panel4: TPanel;
    QrMSNs: TmySQLQuery;
    QrMSNsNOMECLI: TWideStringField;
    QrMSNsCodigo: TIntegerField;
    QrMSNsCliente: TIntegerField;
    QrMSNsRecebido: TFloatField;
    QrMSNsVersao: TWideStringField;
    QrMSNsID_Envio: TWideStringField;
    QrMSNsItens: TIntegerField;
    QrMSNsStatus: TIntegerField;
    QrMSNsUltimo: TIntegerField;
    QrMSNsENVIADO_TXT: TWideStringField;
    QrMSNsNOMESTATUS: TWideStringField;
    QrMSNsPERCREC: TFloatField;
    BtRefresh: TBitBtn;
    ImageList2: TImageList;
    PMItensLV: TPopupMenu;
    Exclui1: TMenuItem;
    Criadiretrio1: TMenuItem;
    N10: TMenuItem;
    Mudadiretrio1: TMenuItem;
    Refresh1: TMenuItem;
    N11: TMenuItem;
    TabSheet11: TTabSheet;
    ListBox1: TListBox;
    ProgressBar2: TProgressBar;
    ExportaImporta1: TMenuItem;
    CobranaBB1: TMenuItem;
    N9: TMenuItem;
    Opes2: TMenuItem;
    Cobranadettulos1: TMenuItem;
    Janeladeimpresso1: TMenuItem;
    frxReport1: TfrxReport;
    Qr1: TmySQLQuery;
    Qr1Codigo: TIntegerField;
    Qr1Nome: TWideStringField;
    Qr1RazaoSocial: TWideStringField;
    Qr1Apelido: TWideStringField;
    Qr1Fantasia: TWideStringField;
    Timer3: TTimer;
    QrAlinx: TmySQLQuery;
    QrAlinxDDeposito: TDateField;
    QrAlinxVencto: TDateField;
    QrAlinxCOD_ALIN: TIntegerField;
    QrAlinxCOD_LOTE: TIntegerField;
    QrEmitx: TmySQLQuery;
    QrEmity: TmySQLQuery;
    QrEmityCPF: TWideStringField;
    QrEmityBAC: TWideStringField;
    QrEmitxCPF: TWideStringField;
    QrEmitxNome: TWideStringField;
    QrEmitxRisco: TFloatField;
    Button9: TButton;
    ProgressBar3: TProgressBar;
    Button10: TButton;
    CkCPF: TCheckBox;
    CkBAC: TCheckBox;
    ArquivoMorto1: TMenuItem;
    Timer4: TTimer;
    QrSacadoIE: TWideStringField;
    QrSacadoNUMERO: TFloatField;
    TabSheet10: TTabSheet;
    Panel5: TPanel;
    Label5: TLabel;
    Label8: TLabel;
    EdQtdeCh: TdmkEdit;
    EdValorCH: TdmkEdit;
    BtImporta: TBitBtn;
    Panel6: TPanel;
    Memo1: TMemo;
    Memo2: TMemo;
    GradeC: TStringGrid;
    OpenDialog3: TOpenDialog;
    Panel2: TPanel;
    ProgressBar4: TProgressBar;
    Label15: TLabel;
    EdCodCli: TdmkEdit;
    EdNomCli: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    TPCheque: TDateTimePicker;
    Label36: TLabel;
    EdBanda: TdmkEdit;
    Label44: TLabel;
    EdCPF: TdmkEdit;
    EdEmitente: TdmkEdit;
    Label45: TLabel;
    Panel3: TPanel;
    BtConfAlt: TBitBtn;
    Chequesabertos1: TMenuItem;
    N13: TMenuItem;
    Chequesavulsos1: TMenuItem;
    QrUpdCli2: TmySQLQuery;
    Button11: TButton;
    QrUpdCli1: TmySQLQuery;
    PMDumptab: TPopupMenu;
    Novorestauraorpida1: TMenuItem;
    Button12: TButton;
    QrAtzCh: TmySQLQuery;
    QrAI: TmySQLQuery;
    QrAIChequeOrigem: TIntegerField;
    QrAIData3: TDateField;
    ProgressBar5: TProgressBar;
    N14: TMenuItem;
    RemessaCNAB2401: TMenuItem;
    N15: TMenuItem;
    RetornoCNAB240emdesenvolvimento1: TMenuItem;
    DiretriosCNAB2401: TMenuItem;
    N16: TMenuItem;
    Automtico1: TMenuItem;
    Manual1: TMenuItem;
    PMCNAB240: TPopupMenu;
    RemessaCNAB2402: TMenuItem;
    RetornoCNAB2401: TMenuItem;
    N17: TMenuItem;
    Remessa1: TMenuItem;
    N18: TMenuItem;
    Verificaremoto1: TMenuItem;
    Web1: TMenuItem;
    Clientes1: TMenuItem;
    Prprio1: TMenuItem;
    Terceiros1: TMenuItem;
    Web2: TMenuItem;
    Sincronizar1: TMenuItem;
    Verificanovoslotes1: TMenuItem;
    PMEvolucap: TPopupMenu;
    Antigo2: TMenuItem;
    NovoEmconstruo1: TMenuItem;
    QrProrr: TmySQLQuery;
    QrProrrCodigo: TIntegerField;
    QrProrrITENS: TLargeintField;
    TabSheet12: TTabSheet;
    Memo3: TMemo;
    Importar1: TMenuItem;
    N19: TMenuItem;
    Exportaemitentesesacados1: TMenuItem;
    ImportaEmitentes1: TMenuItem;
    N20: TMenuItem;
    Atualizaclientesdeitensdeborder1: TMenuItem;
    QrNeWeb: TmySQLQuery;
    Timer5: TTimer;
    Emitentes2: TMenuItem;
    Exportaemitentes1: TMenuItem;
    Importaemitentes2: TMenuItem;
    N21: TMenuItem;
    Info1: TMenuItem;
    ltimocontroledeLLoteIts1: TMenuItem;
    frxChequeSimples: TfrxReport;
    BtDeposito: TBitBtn;
    PmDeposito: TPopupMenu;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    TabSheet8: TTabSheet;
    EdCMC7_1: TdmkEdit;
    Label9: TLabel;
    Ed_Com: TdmkEdit;
    Label10: TLabel;
    Label11: TLabel;
    Ed_Bco: TdmkEdit;
    Ed_Age: TdmkEdit;
    Label12: TLabel;
    Ed_Cta: TdmkEdit;
    Label13: TLabel;
    Ed_Chq: TdmkEdit;
    Label14: TLabel;
    Label16: TLabel;
    Ed_Tip: TdmkEdit;
    Label17: TLabel;
    Button13: TButton;
    Button14: TButton;
    EdCMC7_2: TdmkEdit;
    N12: TMenuItem;
    ConsultaSPC1: TMenuItem;
    SPC1: TMenuItem;
    Configuraesdeconsulta1: TMenuItem;
    Modalidadesdeconsultas1: TMenuItem;
    SockSPC: TTcpClient;
    Edit4: TEdit;
    Edit5: TEdit;
    Edit6: TEdit;
    Button15: TButton;
    BtFinancas: TBitBtn;
    PMRiscoAll: TPopupMenu;
    AtualsnoComputadorservidor1: TMenuItem;
    Novo1: TMenuItem;
    Panel7: TPanel;
    Label1: TLabel;
    EdComando: TEdit;
    Label2: TLabel;
    EdResposta: TEdit;
    Label3: TLabel;
    MeResposta: TdmkEdit;
    GroupBox1: TGroupBox;
    Label4: TLabel;
    Ed1: TEdit;
    Ed2: TEdit;
    Button4: TButton;
    LM_Edit1: TdmkEdit;
    EdA: TdmkEdit;
    EdB: TdmkEdit;
    LM_Edit2: TdmkEdit;
    Button5: TButton;
    Button6: TButton;
    BtComando: TButton;
    BtRefrPrto: TButton;
    BtNegrito: TButton;
    BtNormal: TButton;
    BancodoBrasil240posies1: TMenuItem;
    NovoRetornoeenvio1: TMenuItem;
    RetornoCNABNovo1: TMenuItem;
    RemessaCNABNovo1: TMenuItem;
    Remessa240400Novo1: TMenuItem;
    Lerarquivodetransfernciadedados1: TMenuItem;
    RetornoNovo1: TMenuItem;
    Diretriosderetorno1: TMenuItem;
    N22: TMenuItem;
    Button1: TButton;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    N23: TMenuItem;
    Auxiliares1: TMenuItem;
    CorrigeEmpresa1: TMenuItem;
    N8: TMenuItem;
    GerenciaResultados1: TMenuItem;
    Planodecontas1: TMenuItem;
    Planos1: TMenuItem;
    Nvel4Conjuntos1: TMenuItem;
    Nvel3Grupos1: TMenuItem;
    Nvel2Subgrupos1: TMenuItem;
    odosNveis1: TMenuItem;
    N24: TMenuItem;
    Corrigecompensaodelanamentos1: TMenuItem;
    QrLoc2: TmySQLQuery;
    Corrigestatusdeocorrncias1: TMenuItem;
    TmSuporte: TTimer;
    TySuporte: TTrayIcon;
    N25: TMenuItem;
    N26: TMenuItem;
    VerificaTabelasTerceiros1: TMenuItem;
    Matriz1: TMenuItem;
    N27: TMenuItem;
    Filiais1: TMenuItem;
    N28: TMenuItem;
    Premail1: TMenuItem;
    NFSe1: TMenuItem;
    Cadastros2: TMenuItem;
    RegrasFiscais1: TMenuItem;
    ConfiguraoEmissesMensais1: TMenuItem;
    DeclaraodeServio1: TMenuItem;
    GerenciaNFSe1: TMenuItem;
    GerenciaRPS1: TMenuItem;
    N29: TMenuItem;
    N30: TMenuItem;
    FaturamentoMensal1: TMenuItem;
    LotesdeRPS1: TMenuItem;
    BalloonHint1: TBalloonHint;
    TmVersao: TTimer;
    Empresa1: TMenuItem;
    CentraldeSuporte1: TMenuItem;
    N31: TMenuItem;
    SuporteRemoto1: TMenuItem;
    Sobre1: TMenuItem;
    N32: TMenuItem;
    procedure Sair1Click(Sender: TObject);
    procedure Logoffde1Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Entidades1Click(Sender: TObject);
    procedure SBCadEntidadesClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Backup1Click(Sender: TObject);
    procedure Diversos1Click(Sender: TObject);
    procedure BtEntidades2Click(Sender: TObject);
    procedure BtProtecaoClick(Sender: TObject);
    procedure BtBorderosClick(Sender: TObject);
    procedure Entidades2Click(Sender: TObject);
    procedure Bancos1Click(Sender: TObject);
    procedure Alneasdedevoluo1Click(Sender: TObject);
    procedure Ocorrnciasbancrias1Click(Sender: TObject);
    procedure Feriados1Click(Sender: TObject);
    procedure BtEvolucapiClick(Sender: TObject);
    procedure Especficos1Click(Sender: TObject);
    procedure BtCalendarioClick(Sender: TObject);
    procedure VerificaPdx1Click(Sender: TObject);
    procedure Taxas1Click(Sender: TObject);
    procedure BtChequeClick(Sender: TObject);
    procedure Scios1Click(Sender: TObject);
    procedure Operaes1Click(Sender: TObject);
    procedure ContratodeFomentoMercantil1Click(Sender: TObject);
    procedure Textos1Click(Sender: TObject);
    procedure Taxadejurocomposto1Click(Sender: TObject);
    procedure Jurocomposto1Click(Sender: TObject);
    procedure Juros1Click(Sender: TObject);
    procedure BtEntidadesExclamacaoClick(Sender: TObject);
    procedure Contratos1Click(Sender: TObject);
    procedure Contas1Click(Sender: TObject);
    procedure Depsitos1Click(Sender: TObject);
    procedure AlneasdeDuplicatas1Click(Sender: TObject);
    procedure BtDuplicataClick(Sender: TObject);
    procedure BtChequeDepositoClick(Sender: TObject);
    procedure Serviosdowindows1Click(Sender: TObject);
    procedure Cheque1Click(Sender: TObject);
    procedure BtComandoClick(Sender: TObject);
    procedure BtRefrPrtoClick(Sender: TObject);
    procedure Emitentes1Click(Sender: TObject);
    procedure Sacados1Click(Sender: TObject);
    procedure Movimento1Click(Sender: TObject);
    procedure NotasFiscais1Click(Sender: TObject);
    procedure ClientesRpido1Click(Sender: TObject);
    procedure TimerIdleTimer(Sender: TObject);
    procedure BtMaloteClick(Sender: TObject);
    procedure BtRiscoAllClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure DBGrid2DblClick(Sender: TObject);
    procedure Verificarnovaverso1Click(Sender: TObject);
    procedure BtVersaoClick(Sender: TObject);
    procedure MySQL1Click(Sender: TObject);
    procedure Cheques2Click(Sender: TObject);
    procedure Duplicatas1Click(Sender: TObject);
    procedure BtEuroExportClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure Inadimplncia1Click(Sender: TObject);
    procedure BtResultadosClick(Sender: TObject);
    procedure BtInadimplenciaClick(Sender: TObject);
    procedure Conexo1Click(Sender: TObject);
    procedure Relatorios1Click(Sender: TObject);
    procedure Pagamentodechequesdevolvidos1Click(Sender: TObject);
    procedure Histricodealteraes1Click(Sender: TObject);
    procedure Histrico2Click(Sender: TObject);
    procedure BtHistoricoClick(Sender: TObject);
    procedure Seleciona1Click(Sender: TObject);
    procedure Original1Click(Sender: TObject);
    procedure Bruto1Click(Sender: TObject);
    procedure Bruto2Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure LquidoNovo1Click(Sender: TObject);
    procedure GruposdeSacadosEmitentes1Click(Sender: TObject);
    procedure BtNegritoClick(Sender: TObject);
    procedure LocalizarPagamento1Click(Sender: TObject);
    procedure Excluiritensdelote1Click(Sender: TObject);
    procedure Carteiras2Click(Sender: TObject);
    procedure Exportacontabilidade1Click(Sender: TObject);
    procedure Exportadadoscontabilidade1Click(Sender: TObject);
    { M S N
    procedure Desconecta1Click(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure Conecta1Click(Sender: TObject);
    procedure msnLoginStatus(Sender: TObject; const Status: String);
    procedure msnError(Sender: TObject; const ErrorCode: Integer);
    procedure msnLogout(Sender: TObject);
    procedure msnBlockBuddy(Sender: TObject; Buddy: TMSNBuddy);
    procedure msnBuddyAddYouRequest(Sender: TObject; Buddy: String);
    procedure msnBuddyStatusChanged(Sender: TObject; Buddy: TMSNBuddy);
    procedure msnDeleteBuddy(Sender: TObject; Buddy: TMSNBuddy);
    procedure msnDeleteGroup(Sender: TObject; Group: TMSNGroup);
    procedure msnLogin(Sender: TObject);
    procedure msnLoginProgress(Sender: TObject; const Progress: Integer);
    procedure msnNicknameChanged(Sender: TObject);
    procedure msnReceiveBuddy(Sender: TObject; Buddy: TMSNBuddy);
    procedure msnReceiveGroup(Sender: TObject; Group: TMSNGroup);
    procedure msnRenameGroup(Sender: TObject; Group: TMSNGroup);
    procedure msnShakeEvent(Sender: TObject; Buddy: String);
    procedure msnStatusChanged(Sender: TObject);
    procedure msnUnblockBuddy(Sender: TObject; Buddy: TMSNBuddy);
    procedure tvMSNMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure ChangeNickname1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure Delete1Click(Sender: TObject);
    procedure Block1Click(Sender: TObject);
    procedure Unblock1Click(Sender: TObject);
    procedure Delete2Click(Sender: TObject);
    procedure SendMessage1Click(Sender: TObject);
    procedure SendFile1Click(Sender: TObject);
    procedure SendShake1Click(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure QrMSNsCalcFields(DataSet: TDataSet);
    // MSN private/public
    procedure ReopenMSNs;
    function StatusMSN: Boolean;
    // Fim MSN private/public
    }
    procedure ChangeStatus1Click(Sender: TObject);
    procedure BtDownLotesClick(Sender: TObject);
    procedure BtCNAB240Click(Sender: TObject);
    procedure CobranaBB1Click(Sender: TObject);
    procedure Janeladeimpresso1Click(Sender: TObject);
    procedure BtNormalClick(Sender: TObject);
    procedure Button9Click(Sender: TObject);
    procedure Button10Click(Sender: TObject);
    procedure ArquivoMorto1Click(Sender: TObject);
    procedure BtImportaClick(Sender: TObject);
    procedure EdBandaChange(Sender: TObject);
    procedure EdCPFEnter(Sender: TObject);
    procedure EdEmitenteEnter(Sender: TObject);
    procedure BtConfAltClick(Sender: TObject);
    procedure EdCPFExit(Sender: TObject);
    procedure EdCPFChange(Sender: TObject);
    procedure EdCPFKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Chequesabertos1Click(Sender: TObject);
    procedure Chequesavulsos1Click(Sender: TObject);
    procedure Button11Click(Sender: TObject);
    procedure Novorestauraorpida1Click(Sender: TObject);
    procedure RemessaCNAB2401Click(Sender: TObject);
    procedure DiretriosCNAB2401Click(Sender: TObject);
    procedure Manual1Click(Sender: TObject);
    procedure Automtico1Click(Sender: TObject);
    procedure RemessaCNAB2402Click(Sender: TObject);
    procedure RetornoCNAB2401Click(Sender: TObject);
    procedure Remessa1Click(Sender: TObject);
    procedure Verificaremoto1Click(Sender: TObject);
    procedure Clientes1Click(Sender: TObject);
    procedure Prprio1Click(Sender: TObject);
    procedure Terceiros1Click(Sender: TObject);
    procedure Web2Click(Sender: TObject);
    procedure Sincronizar1Click(Sender: TObject);
    procedure Verificanovoslotes1Click(Sender: TObject);
    procedure Antigo2Click(Sender: TObject);
    procedure NovoEmconstruo1Click(Sender: TObject);
    procedure Importar1Click(Sender: TObject);
    procedure Exportaemitentesesacados1Click(Sender: TObject);
    procedure ImportaEmitentes1Click(Sender: TObject);
    procedure Atualizaclientesdeitensdeborder1Click(Sender: TObject);
    procedure Timer5Timer(Sender: TObject);
    procedure Exportaemitentes1Click(Sender: TObject);
    procedure ltimocontroledeLLoteIts1Click(Sender: TObject);
    procedure frxChequeSimplesGetValue(const VarName: String;
      var Value: Variant);
    procedure Risco1Click(Sender: TObject);
    procedure BtDepositoClick(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure Button13Click(Sender: TObject);
    procedure Button14Click(Sender: TObject);
    procedure ConsultaSPC1Click(Sender: TObject);
    procedure Configuraesdeconsulta1Click(Sender: TObject);
    procedure Modalidadesdeconsultas1Click(Sender: TObject);
    procedure Button15Click(Sender: TObject);
    procedure BtFinancasClick(Sender: TObject);
    procedure Importaemitentes2Click(Sender: TObject);
    procedure AtualsnoComputadorservidor1Click(Sender: TObject);
    procedure Novo1Click(Sender: TObject);
    procedure BancodoBrasil240posies1Click(Sender: TObject);
    procedure NovoRetornoeenvio1Click(Sender: TObject);
    procedure RetornoCNABNovo1Click(Sender: TObject);
    procedure Remessa240400Novo1Click(Sender: TObject);
    procedure Lerarquivodetransfernciadedados1Click(Sender: TObject);
    procedure RetornoNovo1Click(Sender: TObject);
    procedure Diretriosderetorno1Click(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure CorrigeEmpresa1Click(Sender: TObject);
    procedure GerenciaResultados1Click(Sender: TObject);
    procedure VCLSkin1Click(Sender: TObject);
    procedure Planos1Click(Sender: TObject);
    procedure Nvel4Conjuntos1Click(Sender: TObject);
    procedure Nvel3Grupos1Click(Sender: TObject);
    procedure Nvel2Subgrupos1Click(Sender: TObject);
    procedure odosNveis1Click(Sender: TObject);
    procedure Corrigecompensaodelanamentos1Click(Sender: TObject);
    procedure Corrigestatusdeocorrncias1Click(Sender: TObject);
    procedure TmSuporteTimer(Sender: TObject);
    procedure VerificaTabelasTerceiros1Click(Sender: TObject);
    procedure Matriz1Click(Sender: TObject);
    procedure Filiais1Click(Sender: TObject);
    procedure Premail1Click(Sender: TObject);
    procedure RegrasFiscais1Click(Sender: TObject);
    procedure ConfiguraoEmissesMensais1Click(Sender: TObject);
    procedure DeclaraodeServio1Click(Sender: TObject);
    procedure GerenciaNFSe1Click(Sender: TObject);
    procedure GerenciaRPS1Click(Sender: TObject);
    procedure FaturamentoMensal1Click(Sender: TObject);
    procedure LotesdeRPS1Click(Sender: TObject);
    procedure TmVersaoTimer(Sender: TObject);
    procedure CentraldeSuporte1Click(Sender: TObject);
    procedure SuporteRemoto1Click(Sender: TObject);
    procedure Sobre1Click(Sender: TObject);
  private
    { Private declarations }
    FWinsc: TIniFile;
    FOldVersion: Integer;
    FALiberar: Boolean;
    FTamLinha, FFoiExpand: Integer;
    FArqPrn: TextFile;
    FPrintedLine: Boolean;
    function  AvancaCarro(PosicaoAtual, NovaPosicao: Integer): Integer;
    function  EspacoEntreTextos(QrView: TmySQLQuery; CPI: Integer): String;
    function  Formata(Texto, Prefixo, Sufixo: String; EspacosPrefixo, Formato:
              Integer; Nulo: Boolean): String;
    function  FormataTexto(Texto, Prefixo, Sufixo, Substituicao: String;
              EspacosPrefixo, Substitui, Alinhamento, Comprimento, Fonte,
              Formatacao, CPI: Integer; Nulo: Boolean): String;
    function  VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
  public
    { Public declarations }
    FCliInt_, FFormLotesShow: Integer;
    FChangeData: Boolean;
    FCr3: String;
    FDirR: String;
    FVerArqSCX: Integer;
    FLoteLoc, FloteItsLoc: Integer;
    FConnections: Integer;
    FAplicaInfoRegEdit: Boolean;
    FReiniciarAppDB: Boolean;
    FForcaDesconexao: Boolean;
    FMyDBs: TMeusDBs;
    FMinhaGrade1: TStringGrid;
    FImportPath: String;
    // Compatibilidade
    FCheque, FDuplicata: Integer;
    FTipoNovoEnti: Integer;
    //
    FImpCHSimVal, FImpCHSimExt, FImpCHSimEnt, FImpCHSimCid,
    FImpCHSimDia, FImpCHSimMes, FImpCHSimAno: String;
    FFTPExiste: Boolean;
    FFTPDirArq: String;
    FTipoImport: Integer;
    FEntInt: Integer;
    //
    //function  FecharOutraAplicacao(Nome: String): Boolean;
    //function  GetWindows(Handle: HWND; Info: Pointer): Boolean; stdcall;
    procedure ShowHint(Sender: TObject);
    function  PreparaMinutosSQL: Boolean;
    function  AdicionaMinutosSQL(HI, HF: TTime): Boolean;

    procedure CobrancaBB;
    procedure RetornoBB;
    procedure CNAB240Remessa;
    procedure CNAB240Retorno;
    procedure CadastroDeCarteiras(TipoCarteira, ItemCarteira: Integer);
    procedure CadastroDeContas();
    procedure CadastroDeContasNiv();
    procedure CadastroEntidades;

    procedure ImprimeChequeAtual(ChAtu, ChMax, ChConfCab, CPI, TopoIni, Altura,
              MEsq: Integer; Dia, Mes, Ano, Vcto, Valor, Favorecido, Extenso1,
              Extenso2, Obs, Cidade: String);
    procedure SelecionaImagemdefundo;
    procedure MostraBackup1;
    procedure AcoesExtrasDeCadastroDeEntidades(Grade: TStringGrid; Codigo: Integer;
              Aba: Boolean = False);
    procedure AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
              Codigo: Integer; Grade: TStringGrid);
    procedure CriaFormLotes(FormShow, Tipo, Codigo, Controle: Integer);
    procedure CadastroDeOpcoesCreditor;
    procedure AcoesIniciaisDoAplicativo;
    procedure MinhaGrade1DrawCell(Sender: TObject; ACol, ARow: Integer;
              Rect: TRect; State: TGridDrawState);
    procedure AtualizaCreditado(Banco, Agencia, Conta, CPF, Nome:
              String; Cliente, LastPaymt: Integer);
    procedure AtualizaSacado(CNPJ, Nome, Rua: String; Numero: Integer;
              Compl, Bairro, Cidade, UF, CEP, Tel1, IE, Email: String; Risco: Double);
    procedure SalvaInfoRegEdit(Form: TForm; Dest: Integer;
              MinhaGrade: TStringGrid);
    procedure AplicaInfoRegEdit(Form: TForm; Forca: Boolean);
    procedure ResetaDadosComponentes(Form: TForm; MinhaGrade: TStringGrid);
    function  CartaoDeFatura: Integer;
    function  CompensacaoDeFatura: String;
    procedure MostraCarteiras(Carteira: Integer);
    procedure CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
    //
    procedure TextosCartasGetValue(const ParName: String;
              var ParValue: Variant);
    procedure ImprimeChequeSimples(Valor, Creditado, Cidade: String;
              Data: TDateTime; Nominal: Boolean);
    procedure DepositoDeCheques(Config: Integer);
    procedure AdicionaBorderoANF(NF, Bordero: Integer);
    procedure AtualizaSB2(Panel: Integer);
    function  RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
    function  VerificaDiasCompensacao(Praca, CBE: Integer; Valor: Double;
              DtEmiss, DtVence: TDateTime; SBC: Integer): Integer;
    procedure CadastroGruSacEmi(Grupo: Integer);
    procedure AtualizaLastEditLote(Data: String);
    procedure CriaCMC7s;
    procedure LeCMC7_Criados;
    procedure ProcuraBACs;
    function  LocalizaEmitente(Linha: Integer; MudaNome: Boolean): Boolean;
    function  CriaBAC_Pesq(Banco, Agencia, Conta: String; DVCC: Integer): String;
    procedure SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
              TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
    function  AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
              Data: TDateTime; Arquivo: String): Boolean;
    procedure AtualizaClientesLotesIts;
    procedure RetornoCNAB;
    procedure VerificaBDsTerceiros();
    procedure DefineVarsCliInt(CliInt: Integer);
    function  AcaoEspecificaDeApp(Servico: String): Boolean;
    procedure CadastroOcorBank(Codigo: Integer);
    procedure ReCaptionComponentesDeForm(Form: TForm(*; Memo: TMemo*));
    //
    procedure MostraDuplicatas2(Controle, Cliente: Integer);
    procedure MostraRemessaCNABNovo(Codigo, Controle: Integer);
    procedure MostraOpcoesCreditoX();
    //
    procedure MostraDiarioAdd(Assunto, Contato, Depto: Integer; Texto: String);
    procedure MostraDiarioGer(Assunto, Contato, Depto: Integer; Texto: String);
    procedure MostraDiarioAss();
    procedure MostraTaxas(Codigo: Integer);
    procedure MostraEntiJur1(Entidade: Integer);
    procedure MostraCartaG(Codigo: Integer);
    //
    function  FixBugAtualizacoesApp(FixBug: Integer; FixBugStr: String): Boolean;
  end;
var
  FmPrincipal: TFmPrincipal;
  WindowList1: TList;

const
  SExtraBDsCaminho = 'Software\Creditor\Empresas';
  FVersaoSuitCash = '07.02.04.1226';
  FDermatekCamCli = 'C:\Dermatek\Clientes\';
//const FTPLotes
  FFTPCodeAtivo  = 226;
  FMaxTime = 30000;
  FTicTime =  1000;

implementation

uses Module, Entidade2, VerifiDB, MyGlyfs, MyDBCheck, Creditor_MLA,
  Opcoes, MyListas, Sacados, Taxas, VerifiDBy, Bancos, Alineas,
  OcorBank, Feriados, Senha, Lotes1, OpcoesCreditor, CreditorConsts,
  SomaDiasCred, BancosDados, Emitentes, SociosImp, UnFinanceiro, OperaImp,
  ContratoImp, Cartas, CartaG, Decomp, Composto, CompJuros, OcorreuC,
  CalcPercent, Contas, Deposimp, ServicoManager, RiscoCli, UnDmkWeb,
  PertoChek, (*CarteirasImp,*) NFs, EntiRapido1, EntiJur1, RepasCH,
  RepasDU, MaloteConf, RiscoAll, NovaVersao, UCreate, MyInis, OperaCol,
  Servidor, Inadimps, RepasImp, APCD, InfoChanges, HistCliEmi, SimulaParcela,
  Result1, GruSacEmi, Evolucapi, LocPag, Carteiras, ExpContab, ExpContabExp,
  CobrancaBB, ConfigBB, MortoLotes, LotesMorto, Modulc, Deposito, ChequesExtras,
  RetornoBB, CNAB240Rem, CNAB240Ret, CNAB240Dir, VerifiDBi, wclients, (*WSincro,*)
  Evolucap2, Duplicatas2, ChequesGer, LSincro, EmitSacExport, EmitSacImport,
  LotesDep, PesqDep, SocketU, SPC_Pesq, SPC_Config, SPC_Modali, SelfGer,
  ModuleFin, RiscoAll_1, ContasNiv, ModuleGeral, CNAB_Cfg, CNAB_Ret2, CNAB_Lot,
  LeDataArq, EmiteCheque_0, Duplicata1, Promissoria, OperaAll, SubGrupos,
  Grupos, Conjuntos, Plano, PlaCtas, DiarioAdd, DiarioGer, DiarioAss,
  VerifiDBTerceiros, AlineasDup, Matriz, ParamsEmp, PreEmail, NFSe_PF_0000,
  NFSe_PF_0201, UnEntities, UnFixBugs, About;

const
  FAltLin = CO_POLEGADA / 2.16;

{$R *.DFM}

procedure TBandaMagnetica.DestrincharBanda(Banda: String);
begin
  if FBandaMagnetica <> Banda then FBandaMagnetica := Banda;
  FDv3     := Copy(FBandaMagnetica,30,1)[1];
  FConta   := Copy(FBandaMagnetica,20,10);
  FDv1     := Copy(FBandaMagnetica,19,1)[1];
  FTipo    := Copy(FBandaMagnetica,18,1)[1];
  FNumero  := Copy(FBandaMagnetica,12,6);
  FCompe   := Copy(FBandaMagnetica,9,3);
  FDv2     := Copy(FBandaMagnetica,8,1)[1];
  FAgencia := Copy(FBandaMagnetica,4,4);
  FBanco   := Copy(FBandaMagnetica,1,3);
end;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

procedure TFmPrincipal.Sair1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmPrincipal.Logoffde1Click(Sender: TObject);
begin
  ShowMessage('Em constru��o');
end;

procedure TFmPrincipal.LotesdeRPS1Click(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeLRpsC_0201(0);
end;

procedure TFmPrincipal.FormActivate(Sender: TObject);
begin
  APP_LIBERADO := True;
  MyObjects.CorIniComponente;
  VAR_ATUALIZANDO := False;
  VAR_APPNAME := Application.Title;
  if Geral.VersaoTxt2006(CO_VERSAO) <>
    Geral.FileVerInfo(Application.ExeName, 3 (*Versao*)) then
    Geral.MB_Aviso('Vers�o difere do arquivo!');
  SB2.Panels[7].Text :=
    Geral.FF0(dmkPF.GetAllFiles(False, FDirR+'*.scx', ListBox1, True));
  if not FALiberar then Timer1.Enabled := True;
end;

procedure TFmPrincipal.FormCreate(Sender: TObject);
var
  Imagem, ExtraTxt: String;
  //VerAtual: Integer;
begin
  VAR_TYPE_LOG       := ttlCliIntUni;
  FCr3               := '/z8niKfqbXvTw9xe';
  FDirR              := 'C:\Dermatek\FTP_scx\';
  VAR_USA_TAG_BITBTN := True;
  // Servidor ou cliente (pen drive - rede)
  VAR_SERVIDOR3 := 3;
  //App version
  FWinsc := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  try
    FOldVersion := FWinsc.ReadInteger('wsc', 'vernfo', 1);
  finally
    FWinsc.Free;
  end;
  //
  ExtraTxt := '';
  //VerAtual := Geral.VersaoInt2006(CO_VERSAO);
  //
  Pagina.ActivePageIndex       := 0;
  PageControl2.ActivePageIndex := 0;
  //
  TabSheet3.TabVisible  := False;
  //TabSheet7.TabVisible  := False;
  TabSheet9.TabVisible  := False;
  TabSheet12.TabVisible := False;
  TabSheet8.TabVisible  := False;
  //
  VAR_IP_LICENCA     := Geral.ReadAppKeyLM('IPClient', Application.Title, ktString, '');
  FTipoNovoEnti      := 1000;
  FMyDBs             := TMeusDBs.Create;
  FMyDBS.RegistryKey := HKEY_LOCAL_MACHINE;
  FMyDBS.StringGrid  := Grade;
  FMyDBS.KeyPath     := SExtraBDsCaminho;
  FMyDBs.Reload;
  //
  VAR_BDsEXTRAS            := ' :: ' + Geral.FF0(FMyDBs.MaxDBs);
  VAR_REPRES_ENTIJUR1      := 'Fornece5';
  VAR_ENTICREDS            := True;
  VAR_STLOGIN              := StatusBar.Panels[01];
  StatusBar.Panels[3].Text := Geral.VersaoTxt2006(CO_VERSAO);
  VAR_STTERMINAL           := StatusBar.Panels[05];
  VAR_STDATALICENCA        := StatusBar.Panels[07];
  VAR_SKINUSANDO           := StatusBar.Panels[09];
  VAR_STDATABASES          := StatusBar.Panels[11];
  VAR_STIMPCHEQUE          := StatusBar.Panels[13];
  VAR_TIPOSPRODM_TXT       := '0,1,2,3,4,5,6,7,8,9,10,11,12,13';
  //
  VAR_APP       := ExtractFilePath(Application.ExeName);
  VAR_VENDEOQUE := 1;
  //
  Application.OnException := MyObjects.MostraErro;
  Application.OnMessage   := MyObjects.FormMsg;
  Application.OnHint      := ShowHint;
  //2017-07-19 => N�o ativar por causa da c�mera Application.OnIdle := AppIdle;
  TimerIdle.Interval := VAR_TIMERIDLEITERVAL;
  TimerIdle.Enabled  := True;

  //Application.OnActionExecute := AppActionExecute;
  //
  Imagem := Geral.ReadAppKeyLM('ImagemFundo', Application.Title, ktString, VAR_APP+'Fundo.jpg');
  //
  if FileExists(Imagem) then
    ImgPrincipal.Picture.LoadFromFile(Imagem);
  //
  VAR_CAD_POPUP := PMGeral;
  //
  CRD_Consts.SetaVariaveisIniciais;
  Geral.DefineFormatacoes;
  //
  //VAR_TIMERIDLE := TimerIdle; 2017-05-24 => Movido para o evento OnIdle
  TPCheque.Date := Date;
  //
  // Local
  VAR_IMPCHEQUE       := Geral.ReadAppKeyCU('ImpCheque', 'Dermatek', ktInteger, 0);
  VAR_IMPCHEQUE_PORTA := Geral.ReadAppKeyCU('ImpCheque_Porta', 'Dermatek', ktString, 'COM2');
  // Servidor
  VAR_IMPCH_IP        := Geral.ReadAppKeyCU('ImpreCh_IP', 'Dermatek', ktString, '127.0.0.1');
  VAR_IMPCHPORTA      := Geral.ReadAppKeyCU('ImpreChPorta', 'Dermatek', ktInteger, 9520);
  //
  {
  ArqSCX.ConfiguraGrades(GradeC, nil, nil);
  GradeC.ColCount := GradeC.ColCount + 1;
  GradeC.Cells[GradeC.ColCount-1, 0] := 'CMC7';
  GradeC.ColWidths[GradeC.ColCount-1] := 200;
  }
  //M�dulo WEB desativado quando reativar usar o do Credito2
  Web1.Enabled        := False;
  BtDownLotes.Enabled := False;
end;

procedure TFmPrincipal.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  if VAR_IMPRECHEQUE = 1 then
    PertoChekP.DesabilitaPertoCheck;

    (*DmDados.TbMaster.Open;
    DmDados.TbMaster.Edit;
    if Trunc(Date) > DmDados.TbMasterLA.Value then
      DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value +
      (Trunc(Date) - Trunc(DmDados.TbMasterLA.Value));
    if Trunc(Date) < DmDados.TbMasterLA.Value then
      DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value + 3;
    if (Trunc(Date) = DmDados.TbMasterLA.Value) and
       (Time < DmDados.TbMasterLH.Value)  then
      DmDados.TbMasterLD.Value := DmDados.TbMasterLD.Value + 1;
    DmDados.TbMasterLA.Value := Trunc(Date);
    DmDados.TbMasterLH.Value := Time;
    DmDados.TbMaster.Post;
    DmDados.TbMaster.Close;
    FmSkin.Close;
    FmSkin.Destroy;*)
////////////////////////////////////////////////////////////////////////////////
  FMyDBs.Destroi;
  FMyDBs.Free;
  ///
  try
    APP_LIBERADO := False;
    Application.Terminate;
  except
  //
  end;
  if SockSPC.Active then
    SockSPC.Active := False;
end;

procedure TFmPrincipal.Entidades1Click(Sender: TObject);
begin
  CadastroEntidades;
end;

procedure TFmPrincipal.Premail1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPreEmail, FmPreEmail, afmoNegarComAviso) then
  begin
    FmPreEmail.ShowModal;
    FmPreEmail.Destroy;
  end;
end;

function TFmPrincipal.PreparaMinutosSQL: Boolean;
//var
//  i: Integer;
begin
  Result := True;
  try
    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('DELETE FROM ocupacao');
    Dmod.QrUpdL.ExecSQL;
    ///
(*    Dmod.QrUpdL.SQL.Clear;
    Dmod.QrUpdL.SQL.Add('INSERT INTO ocupacao SET Min=:P0');
    for i := 1 to 1440 do
    begin
      Dmod.QrUpdL.Params[0].AsInteger := I;
      Dmod.QrUpdL.ExecSQL;
    end;*)
  except
    Result := False
  end;
end;

function TFmPrincipal.AdicionaMinutosSQL(HI, HF: TTime): Boolean;
var
  Hour, Min, Sec, MSec: Word;
  i, Ini, Fim: Integer;
begin
  Result := True;
  if (HI=0) and (HF=0) then Exit;
  DecodeTime(HI, Hour, Min, Sec, MSec);
  Ini := (Hour * 60) + Min;
  DecodeTime(HF, Hour, Min, Sec, MSec);
  Fim := (Hour * 60) + Min - 1;
  if Fim < Ini then Fim := Fim + 1440;
  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('INSERT INTO ocupacao SET Qtd=1, Min=:P0');
  for i := Ini to Fim do
  begin
    Dmod.QrUpdL.Params[0].AsInteger := I;
    Dmod.QrUpdL.ExecSQL;
  end;
end;

procedure TFmPrincipal.SBCadEntidadesClick(Sender: TObject);
begin
  CadastroEntidades;
end;

procedure TFmPrincipal.CadastroEntidades;
begin
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoNegarComAviso) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
  end;
end;

procedure TFmPrincipal.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FALiberar := True;
  FmCreditor_MLA.Show;
  Enabled := False;
  FmCreditor_MLA.Refresh;
  FmCreditor_MLA.EdSenha.Text := FmCreditor_MLA.EdSenha.Text+'*';
  FmCreditor_MLA.EdSenha.Refresh;
  FmCreditor_MLA.Refresh;
  try
    Application.CreateForm(TDmod, Dmod);
  except
    Application.MessageBox(PChar('Imposs�vel criar Modulo de dados'), 'Erro', MB_OK+MB_ICONERROR);
    Application.Terminate;
    Exit;
  end;
  FmCreditor_MLA.EdSenha.Text := FmCreditor_MLA.EdSenha.Text+'*';
  FmCreditor_MLA.EdSenha.Refresh;
  FmCreditor_MLA.ReloadSkin;
  FmCreditor_MLA.EdLogin.Text := '';
  FmCreditor_MLA.EdSenha.Text := '';
  FmCreditor_MLA.EdSenha.Refresh;
  FmCreditor_MLA.EdLogin.ReadOnly := False;
  FmCreditor_MLA.EdSenha.ReadOnly := False;
  FmCreditor_MLA.EdLogin.SetFocus;
  //FmCreditor_MLA.ReloadSkin;
  FmCreditor_MLA.Refresh;
end;

procedure TFmPrincipal.FormShow(Sender: TObject);
begin
  //MeuVCLSkin.VCLSkinDefineUso(FmPrincipal.sd1, FmPrincipal.skinstore1);
end;

procedure TFmPrincipal.Backup1Click(Sender: TObject);
begin
  MostraBackup1;
end;

{
procedure TFmPrincipal.MostraBackup;
begin
  VAR_MYSQLUSER := 'root';
  //
  VAR_CURSOR := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  VAR_IPBACKUP := '127.0.0.1';
  try
    Application.CreateForm(TFmDumptab, FmDumptab);
  finally
    Screen.Cursor := VAR_CURSOR;
  end;
  FmDumptab.ShowModal;
  FmDumptab.Destroy;
end;
}

procedure TFmPrincipal.Diversos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmOpcoes, FmOpcoes, afmoNegarComAviso) then
  begin
    FmOpcoes.ShowModal;
    FmOpcoes.Destroy;
  end;
end;

procedure TFmPrincipal.BtEntidades2Click(Sender: TObject);
begin
  CadastroEntidades;
end;

procedure TFmPrincipal.BtProtecaoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDumptab, BtProtecao);
end;

procedure TFmPrincipal.CadastroDeCarteiras(TipoCarteira, ItemCarteira: Integer);
begin
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
  end;
end;

procedure TFmPrincipal.ImprimeChequeAtual(ChAtu, ChMax, ChConfCab, CPI, TopoIni,
  Altura, MEsq: Integer; Dia, Mes, Ano, Vcto, Valor, Favorecido, Extenso1,
  Extenso2, Obs, Cidade: String);
var
  LinhaAtual: Integer;
  MyCPI, Texto, Espaco, Linha, FormataA, FormataI, FormataNI, FormataNF: String;
begin
  Screen.Cursor := crHourGlass;
  try
    LinhaAtual := 0;
    if ChAtu = 1 then
    begin
      AssignFile(FArqPrn, 'LPT1:');
      ReWrite(FArqPrn);
      Write(FArqPrn, #13);
      if CPI = 12 then MyCPI := 'M' else MyCPI := 'P';
      Write(FArqPrn, #27+MyCPI);
      FormataA  := #15;
      FormataI  := #15;
      FormataNI := #15;
      FormataNF := #15;
      //if (ChAtu < ChMax) then Substitui := 1 else Substitui := 0;
      if TopoIni < 0 then LinhaAtual := LinhaAtual - TopoIni else
        LinhaAtual := AvancaCarro(LinhaAtual, TopoIni);
    end;
    //////////////////////////////////////////////////////////////////////////
    QrTopos1.Close;
    QrTopos1.Params[0].AsInteger := ChConfCab;
    QrTopos1.Open;
    QrTopos1.First;
    while not QrTopos1.Eof do
    begin
      LinhaAtual := AvancaCarro(LinhaAtual, QrTopos1TopR.Value);
      if not FPrintedLine then
      begin
        QrView1.Close;
        QrView1.Params[0].AsInteger := ChConfCab;
        QrView1.Params[1].AsInteger := QrTopos1TopR.Value;
        QrView1.Open;
        QrView1.First;
        Linha := '';
        FTamLinha  := -MEsq;
        FFoiExpand := 0;
        while not QrView1.Eof do
          begin
          case QrView1FTam.Value of
            0: FormataA := #15;
            1: FormataA := #18;
            2: FormataA := #18#14;
          end;
          Espaco := EspacoEntreTextos(QrView1, CPI);
          //if QrView1Nulo.Value = 1 then Nulo := True else Nulo := False;
          //
          case QrView1Campo.Value of
            01: Texto := Valor;
            02: Texto := Extenso1;
            03: Texto := Extenso2;
            04: Texto := Favorecido;
            05: Texto := Dia;
            06: Texto := Mes;
            07: Texto := Ano;
            08: Texto := Vcto;
            09: Texto := Obs;
            10: Texto := Cidade;
            else Texto := '';
          end;
          Texto := FormataTexto(Texto, QrView1Pref.Value, QrView1Sufi.Value,
          '', QrView1PrEs.Value, 0, QrView1AliH.Value, QrView1Larg.Value,
          QrView1FTam.Value, 0(*Texto*) , CPI, True(*Nulo*));
          Linha := Linha + Espaco + FormataA + Texto;
          // J� usou expandido na mesma linha
          if QrView1FTam.Value = 2 then FFoiExpand := 100;
          QrView1.Next;
        end;
        Write(FarqPrn, #27+'3'+#0);
        //Write(FarqPrn, #27+'F');
        WriteLn(FArqPrn, Linha);
        FPrintedLine := True;
      end;
      QrTopos1.Next;
    end;
    //LinhaAtual :=
    AvancaCarro(LinhaAtual, Altura);
    //Write(FArqPrn, '_____________-------------------_________________---------------____________');
    if ChAtu = ChMax then
    begin
      Write(FArqPrn, #13);
      WriteLn(FArqPrn, #27+'0');
      //if CkEjeta.Checked then
      //Write(FArqPrn, #12);
      CloseFile(FArqPrn);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmPrincipal.AvancaCarro(PosicaoAtual, NovaPosicao: Integer): Integer;
var
  LinAtu: Integer;
begin
  if PosicaoAtual < NovaPosicao then
  begin
    LinAtu := Trunc((NovaPosicao - PosicaoAtual) / FAltLin);
    while LinAtu > 0 do
    begin
      if LinAtu > 216 then
      begin
        Write(FArqPrn, #27+'3'+#216);
        LinAtu := LinAtu - 216;
        WriteLn(FArqPrn,' ');
      end else begin
        Write(FArqPrn, MLAGeral.DefAlturaDotImp(3, LinAtu, ''));
        WriteLn(FArqPrn,' ');
        LinAtu := 0;
      end;
    end;
  end;
  Result := NovaPosicao;
  FPrintedLine := False;
end;

function TFmPrincipal.FormataTexto(Texto, Prefixo, Sufixo, Substituicao: String;
  EspacosPrefixo, Substitui, Alinhamento, Comprimento, Fonte, Formatacao, CPI:
  Integer; Nulo: Boolean): String;
var
  i, Tam, Letras, MyCPI, Vazios: Integer;
  Esq: Boolean;
  Txt, Vaz: String;
  Increm: Double;
begin
  if Substitui = 1 then Txt := Substituicao else
  begin
    if Texto = '' then Txt := ''
    else Txt := Formata(Texto, Prefixo, Sufixo, EspacosPrefixo, Formatacao, Nulo);
  end;
  if Txt = '' then
  Txt := ' '; // evitar erro de Margem Esquerda ao imprimir ??
  if CPI = 0 then MyCPI := 10 else MyCPI := CPI;
  Tam := Fonte + (MyCPI*10);
  Letras := 0;
  case Tam of
    100: Letras := 17;
    101: Letras := 10;
    102: Letras := 05;
    120: Letras := 20;
    121: Letras := 12;
    122: Letras := 06;
    else Geral.MB_Erro('CPI indefinido!');
  end;
  Increm := (CO_POLEGADA * 100) / Letras;
  Letras := Trunc(((Comprimento / 100) / CO_POLEGADA) * Letras);
  FTamLinha := FTamLinha + Trunc(Increm * Letras);
  Vazios := Letras - Length(Txt);
  Esq := True;
  // [0: Esq] [1: Centro] [2: Direita]
  if Alinhamento = 1 then
  begin
    for i := 1 to Vazios do
    begin
      if Esq then
      begin
        Txt := Txt + ' ';
        Esq := False;
      end else begin
        Txt := ' ' + Txt;
        Esq := True;
      end;
    end;
  end else begin
    for i := 1 to Vazios do Vaz := Vaz + ' ';
    if Alinhamento = 0 then Txt := Txt + Vaz else Txt := Vaz + Txt;
  end;
  Result := Txt;
end;

function TFmPrincipal.EspacoEntreTextos(QrView: TmySQLQuery; CPI: Integer): String;
var
  Texto: String;
  i, MyCPI, Letras: Integer;
  Soma: Double;
begin
  Texto := '';
  if FTamLinha > QrView.FieldByName('MEsq').AsInteger then
  begin
    // Erro !!!
  end;
  if FTamLinha < QrView.FieldByName('MEsq').AsInteger then
  begin
    Texto := #15;
    if CPI = 0 then MyCPI := 10 else MyCPI := CPI;
    MyCPI := MyCPI + FFoiExpand;
    case MyCPI of
     010: MyCPI := 17;
     012: MyCPI := 20;
     110: MyCPI := 10;
     112: MyCPI := 10;
    end;
    Soma := (CO_POLEGADA * 100)/ MyCPI;
    Letras := Trunc((QrView.FieldByName('MEsq').AsInteger - FTamLinha) /
      (CO_POLEGADA * 100) * MyCPI);
    for i := 1 to Letras do Texto := Texto + ' ';
    FTamLinha := FTamLinha + Trunc(Soma * Letras);
  end;
  Result := Texto;
end;

function TFmPrincipal.Formata(Texto, Prefixo, Sufixo: String; EspacosPrefixo,
 Formato: Integer; Nulo: Boolean): String;
var
  MeuTexto, MeuPrefixo: String;
  i: Integer;
begin
  if Trim(Texto) = '' then MeuTexto := Texto else
  begin
    case Formato of
      1: MeuTexto := Geral.TFT(Texto, 0, siNegativo);
      2: MeuTexto := Geral.TFT(Texto, 1, siNegativo);
      3: MeuTexto := Geral.TFT(Texto, 2, siNegativo);
      4: MeuTexto := Geral.TFT(Texto, 3, siNegativo);
      5: MeuTexto := Geral.TFT(Texto, 4, siNegativo);
      6: MeuTexto := FormatDateTime(VAR_FORMATDATE3, StrToDate(Texto));
      7: MeuTexto := FormatDateTime(VAR_FORMATDATE2, StrToDate(Texto));
      8: MeuTexto := FormatDateTime(VAR_FORMATTIME2, StrToTime(Texto));
      9: MeuTexto := FormatDateTime(VAR_FORMATTIME , StrToTime(Texto));
      else MeuTexto := Texto;
    end;
  end;
  if (Formato in ([1,2,3,4,5])) and Nulo then
  begin
    if Geral.DMV(MeuTexto) < 0.0001 then MeuTexto := '';
  end;
  if MeuTexto = '' then Result := '' else
  begin
    MeuPrefixo := '';
    if Length(Prefixo) > 0 then
      for i := 0 to EspacosPrefixo-1 do MeuPrefixo := MeuPrefixo + ' ';
    Result := Prefixo+MeuPrefixo+MeuTexto+Sufixo;
  end;
end;

procedure TFmPrincipal.Entidades2Click(Sender: TObject);
begin
  CadastroEntidades;
end;

procedure TFmPrincipal.BancodoBrasil240posies1Click(Sender: TObject);
begin
  Application.CreateForm(TFmConfigBB, FmConfigBB);
  FmConfigBB.ShowModal;
  FmConfigBB.Destroy;
end;

procedure TFmPrincipal.Bancos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmBancos, FmBancos, afmoNegarComAviso) then
  begin
    FmBancos.ShowModal;
    FmBancos.Destroy;
  end;
end;

procedure TFmPrincipal.Alneasdedevoluo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmAlineas, FmAlineas, afmoNegarComAviso) then
  begin
    FmAlineas.ShowModal;
    FmAlineas.Destroy;
  end;
end;

procedure TFmPrincipal.Ocorrnciasbancrias1Click(Sender: TObject);
begin
  CadastroOcorBank(0);
end;

procedure TFmPrincipal.odosNveis1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPlaCtas, FmPlaCtas, afmoNegarComAviso) then
  begin
    FmPlaCtas.ShowModal;
    FmPlaCtas.Destroy;
  end;
end;

procedure TFmPrincipal.FaturamentoMensal1Click(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeFatCab(True, nil, nil);
end;

procedure TFmPrincipal.Feriados1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFeriados, FmFeriados, afmoNegarComAviso) then
  begin
    FmFeriados.ShowModal;
    FmFeriados.Destroy;
  end;
end;

procedure TFmPrincipal.Filiais1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmParamsEmp, FmParamsEmp, afmoSoBoss) then
  begin
    FmParamsEmp.ShowModal;
    FmParamsEmp.Destroy;
  end;
end;

function TFmPrincipal.FixBugAtualizacoesApp(FixBug: Integer;
  FixBugStr: String): Boolean;
begin
  Result := True;
  (* Marcar quando for criar uma fun��o de atualiza��o
  try
    if FixBug = 0 then
      Atualiza��o1()
    else if FixBug = 1 then
      Atualiza��o2()
    else if FixBug = 2 then
      Atualiza��o3()
    else
      Result := False;
  except
    Result := False;
  end;
  *)
end;

procedure TFmPrincipal.BtBorderosClick(Sender: TObject);
begin
  //MostraImprimeCh(True, False);
  //CriaFormLotes(0, 0, 0);
  //MyObjects.MostraPopUpDeBotao(PMBorderos, BtBorderos);
  CriaFormLotes(1, 0, 0, 0);
end;

procedure TFmPrincipal.CriaFormLotes(FormShow, Tipo, Codigo, Controle: Integer);
begin
  Screen.Cursor  := crHourGlass;
  FFormLotesShow := FormShow;
  case FormShow of
    {
    0:
    begin
      try
        Application.CreateForm(TFmLotes0, FmLotes0);
        FmLotes0.FTipoLote := Tipo;
        if Codigo > 0 then
        begin
          FmLotes0.LocCod(Codigo, Codigo);
          if Controle > 0 then FmLotes0.QrLotesIts.Locate('Controle', Controle, []);
        end;
        FmLotes0.ShowModal;
        FmLotes0.Destroy;
        Screen.Cursor := crDefault;
      except
        Screen.Cursor := crDefault;
        raise;
      end;
    end;
    }
    1:
    begin
      try
        Application.CreateForm(TFmLotes1, FmLotes1);
        FmLotes1.FTipoLote := Tipo;
        if Codigo > 0 then
        begin
          FmLotes1.LocCod(Codigo, Codigo);
          if Controle > 0 then FmLotes1.QrLotesIts.Locate('Controle', Controle, []);
        end;
        FmLotes1.ShowModal;
        FmLotes1.Destroy;
        Screen.Cursor := crDefault;
      except
        Screen.Cursor := crDefault;
        raise;
      end;
    end;
    else Geral.MB_Erro('Janela de gerenciamento de border�s n�o definida! (01)');
  end;
end;

procedure TFmPrincipal.Especficos1Click(Sender: TObject);
begin
  CadastroDeOpcoesCreditor;
end;

procedure TFmPrincipal.CadastroDeOpcoesCreditor;
begin
  if DBCheck.CriaFm(TFmOpcoesCreditor, FmOpcoesCreditor, afmoNegarComAviso) then
  begin
    FmOpcoesCreditor.ShowModal;
    FmOpcoesCreditor.Destroy;
    if FReiniciarAppDB then
    begin
      Geral.MB_Aviso('O aplicativo ser� finalizado para que ' +
        'altera��es criticas sejam executadas!');
      Close;
    end;
  end;
end;

procedure TFmPrincipal.AcoesIniciaisDoAplicativo();
begin
  try
    if ZZTerminate then
      Exit;
    Application.ProcessMessages;
    Screen.Cursor := crHourGlass;
    CRD_Consts.AvisaSetarOpcoes;
    AtualizaSB2(0);
    //PertoChekP.VerificaPertochek;
    UMyMod.VerificaFeriadosFuturos(TFmFeriados, FmFeriados);
    if DModG <> nil then
    begin
      //DModG.ConectaBDLocalViaServidor();
      DmodG.AtualizaEntiConEnt();
      //
      DModG.MyPID_DB_Cria();
      //
      UFixBugs.MostraFixBugs(['']);
      (*
      if DModG.QrCtrlGeralAtualizouEntidades.AsInteger = 0 then
        Entities.AtualizaEntidadesParaEntidade2(ProgressBar2, Dmod.MyDB, DModG.AllID_DB);
      *)
      //
      DmkWeb.ConfiguraAlertaWOrdSerApp(TmSuporte, TySuporte, BalloonHint1);
      //
      TmVersao.Enabled := True;
      //
      DmkWeb.HistoricoDeAlteracoes(CO_DMKID_APP, CO_VERSAO, dtMostra);
      //
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.BtCalendarioClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSomaDiasCred, FmSomaDiasCred, afmoNegarComAviso) then
  begin
    FmSomaDiasCred.ShowModal;
    FmSomaDiasCred.Destroy;
  end;
end;

procedure TFmPrincipal.VerificaPdx1Click(Sender: TObject);
begin
  Application.CreateForm(TFmBancoDados, FmBancoDados);
  FmBancoDados.ShowModal;
  FmBancoDados.Destroy;
  //
  FMyDBs.Reload;
end;

procedure TFmPrincipal.AcoesIniciaisDeCadastroDeEntidades(Reference: TComponent;
Codigo: Integer; Grade: TStringGrid);
var
  i: Integer;
begin
  FMinhaGrade1 := Grade;
  Grade.Options := Grade.Options + [goEditing];
  Grade.OnDrawCell := MinhaGrade1DrawCell;
  Grade.RowCount := 2;
  Grade.ColCount := 4;
  Grade.Cells[00,00] := 'Coligada';
  Grade.Cells[01,00] := '% Compra M�n';
  Grade.Cells[02,00] := '% Compra M�x';
  if Dmod.QrControleTipoAdValorem.Value = 0 then
    Grade.Cells[03,00] := '% Ad Valorem'
  else Grade.Cells[03,00] := '$ Ad Valorem';
  //
  Grade.ColWidths[00] := 240;
  Grade.ColWidths[01] := 080;
  Grade.ColWidths[02] := 080;
  Grade.ColWidths[03] := 080;
  //
  for i := 0 to FMyDBs.MaxDBs-1 do
  begin
    if i > 0 then Grade.RowCount := i + 2;
    Grade.Cells[0,Grade.RowCount-1] := FMyDBs.IDName[i];
    if FMyDBs.Connected[i] = '1' then
    begin
      Dmod.FArrayMySQLEE[i].Close;
      Dmod.FArrayMySQLEE[i].Params[0].AsInteger := Codigo;
      Dmod.FArrayMySQLEE[i].Open;
      Grade.Cells[1, Grade.RowCount-1] := Geral.FFT(
        Dmod.FArrayMySQLEE[i].FieldByName('Menor').AsFloat, 6, siPositivo);
      Grade.Cells[2, Grade.RowCount-1] := Geral.FFT(
        Dmod.FArrayMySQLEE[i].FieldByName('Maior').AsFloat, 6, siPositivo);
      Grade.Cells[3, Grade.RowCount-1] := Geral.FFT(
        Dmod.FArrayMySQLEE[i].FieldByName('AdVal').AsFloat, 6, siPositivo);
    end;
  end;
end;

function TFmPrincipal.AcaoEspecificaDeApp(Servico: String): Boolean;
begin
  if Servico = '?' then
  begin
    Result := True;
    //
  end
  else Result := False;
end;

procedure TFmPrincipal.AcoesExtrasDeCadastroDeEntidades(Grade: TStringGrid;
  Codigo: Integer; Aba: Boolean = False);
var
  i: Integer;
begin
  if Grade <> nil then
  begin
    FMinhaGrade1 := nil;
    Dmod.QrUpd_.SQL.Clear;
    for i := 0 to FMyDBs.MaxDBs -1 do
    begin
      if FMyDBs.Connected[i] = '1' then
      begin
       Dmod.QrUpd_.Database := Dmod.FArrayMySQLBD[i];
       Dmod.QrUpd_.SQL.Clear;
       Dmod.QrUpd_.SQL.Add('DELETE FROM exenti WHERE Codigo=:P0');
       Dmod.QrUpd_.Params[0].AsInteger := Codigo;
       Dmod.QrUpd_.ExecSQL;
       //
       Dmod.QrUpd_.SQL.Clear;
       Dmod.QrUpd_.SQL.Add('INSERT INTO exenti SET Menor=:P0, ');
       Dmod.QrUpd_.SQL.Add('Maior=:P1, AdVal=:P2, Codigo=:Pa');
       //
       Dmod.QrUpd_.Params[0].AsFloat   := Geral.DMV(Grade.Cells[1, i+1]);
       Dmod.QrUpd_.Params[1].AsFloat   := Geral.DMV(Grade.Cells[2, i+1]);
       Dmod.QrUpd_.Params[2].AsFloat   := Geral.DMV(Grade.Cells[3, i+1]);
       //
       Dmod.QrUpd_.Params[3].AsInteger := Codigo;
       Dmod.QrUpd_.ExecSQL;
       //
      end;
    end;
    Dmod.QrControle.Close;
    Dmod.QrControle.Open;
  end;
end;

procedure TFmPrincipal.MinhaGrade1DrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
var
  OldAlign, Color: Integer;
begin
  if FMinhaGrade1 = nil then Exit;
  Color := clBlack;
  SetTextColor(FMinhaGrade1.Canvas.Handle, Color);
  if ARow = 0 then
  begin
    //
  end else if ACol = 0 then begin
    if ARow <> 0 then begin
      OldAlign := SetTextAlign(FMinhaGrade1.Canvas.Handle, TA_LEFT);
      FMinhaGrade1.Canvas.TextRect(Rect, Rect.Left+2, Rect.Top + 2,
        FMinhaGrade1.Cells[Acol, ARow]);
      SetTextAlign(FMinhaGrade1.Canvas.Handle, OldAlign);
    end;
  end else if ACol = 01 then begin
      OldAlign := SetTextAlign(FMinhaGrade1.Canvas.Handle, TA_RIGHT);
      FMinhaGrade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Geral.TFT(FMinhaGrade1.Cells[Acol, ARow], 6, siPositivo));
      SetTextAlign(FMinhaGrade1.Canvas.Handle, OldAlign);
  end else if ACol = 02 then begin
      OldAlign := SetTextAlign(FMinhaGrade1.Canvas.Handle, TA_RIGHT);
      FMinhaGrade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Geral.TFT(FMinhaGrade1.Cells[Acol, ARow], 6, siPositivo));
      SetTextAlign(FMinhaGrade1.Canvas.Handle, OldAlign);
  end else if ACol = 03 then begin
      OldAlign := SetTextAlign(FMinhaGrade1.Canvas.Handle, TA_RIGHT);
      FMinhaGrade1.Canvas.TextRect(Rect, Rect.Right - 2, Rect.Top + 2,
        Geral.TFT(FMinhaGrade1.Cells[Acol, ARow], 6, siPositivo));
      SetTextAlign(FMinhaGrade1.Canvas.Handle, OldAlign);
  end (*else if ACol = 03 then begin
      OldAlign := SetTextAlign(FMinhaGrade1.Canvas.Handle, TA_CENTER);
      FMinhaGrade1.Canvas.TextRect(Rect, (Rect.Left+Rect.Right) div 2, Rect.Top + 2,
        FMinhaGrade1.Cells[Acol, ARow]);
      SetTextAlign(FMinhaGrade1.Canvas.Handle, OldAlign);
  end else if ACol = 04 then begin*)
end;

procedure TFmPrincipal.AtualizaCreditado(Banco, Agencia, Conta, CPF, Nome:
  String; Cliente, LastPaymt: Integer);
var
  Registros: Integer;
begin
  case FmPrincipal.FFormLotesShow of
    //0: FmLotes0.ReopenPagtos;
    1: FmLotes1.ReopenPagtos;
    else Geral.MB_Erro('Janela de gerenciamento de border�s n�o definida! (17)');
  end;
  //
  if (Trim(Banco) = '')
  or (Trim(Agencia) = '')
  or (Trim(Conta) = '') then Exit;
  //
  QrCreditado.Close;
  QrCreditado.Params[0].AsString := Banco+Agencia+Conta;
  QrCreditado.Open;
  //
  CPF := Geral.SoNumero_TT(CPF);
  if QrCreditado.RecordCount > 1 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM creditados WHERE BAC=:P0');
    Dmod.QrUpd.Params[0].AsString := Banco+Agencia+Conta;
    Dmod.QrUpd.ExecSQL;
    //
    Registros := 0;
  end else begin
    Registros := QrCreditado.RecordCount;
  end;
  Dmod.QrUpd.SQL.Clear;
  if Registros = 0 then
    Dmod.QrUpd.SQL.Add('INSERT INTO creditados SET ')
  else
    Dmod.QrUpd.SQL.Add('UPDATE creditados SET ');
  Dmod.QrUpd.SQL.Add('CPF=:P0, Nome=:P1, Banco=:P2, Agencia=:P3, ');
  Dmod.QrUpd.SQL.Add('Conta=:P4');
  if Registros = 0 then
    Dmod.QrUpd.SQL.Add(', BAC=:Pa')
  else
    Dmod.QrUpd.SQL.Add('WHERE BAC=:Pa');
  Dmod.QrUpd.Params[0].AsString  := CPF;
  Dmod.QrUpd.Params[1].AsString  := Nome;
  Dmod.QrUpd.Params[2].AsInteger := Geral.IMV(Banco);
  Dmod.QrUpd.Params[3].AsInteger := Geral.IMV(Agencia);
  Dmod.QrUpd.Params[4].AsString  := Conta;
  //
  Dmod.QrUpd.Params[5].AsString  := Banco+Agencia+Conta;
  Dmod.QrUpd.ExecSQL;

  //

  if LastPaymt > 0 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO creditadosits SET ');
    Dmod.QrUpd.SQL.Add('BAC=:P0, Cliente=:P1, LastPaymt=:P2');
    Dmod.QrUpd.Params[0].AsString  := Banco+Agencia+Conta;
    Dmod.QrUpd.Params[1].AsInteger := Cliente;
    Dmod.QrUpd.Params[2].AsInteger := LastPaymt;
    Dmod.QrUpd.ExecSQL;
  end;
end;

procedure TFmPrincipal.AtualizaSacado(CNPJ, Nome, Rua: String; Numero: Integer;
Compl, Bairro, Cidade, UF, CEP, Tel1, IE, Email: String; Risco: Double);
var
  Registros: Integer;
begin
  CNPJ := Geral.SoNumero_TT(CNPJ);
  QrSacado.Close;
  QrSacado.Params[0].AsString := CNPJ;
  QrSacado.Open;
  //
  if Risco = -1 then Risco := QrSacadoRisco.Value;
  if QrSacado.RecordCount > 1 then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM sacados WHERE CNPJ=:P0');
    Dmod.QrUpd.Params[0].AsString := CNPJ;
    Dmod.QrUpd.ExecSQL;
    //
    Registros := 0;
  end else begin
    Registros := QrSacado.RecordCount;
  end;
  if (Risco < 0.01) and (Registros = 0 ) then
    Risco := Dmod.QrControleDURisco.Value;
  //
  Dmod.QrUpd.SQL.Clear;
  if Registros = 0 then
    Dmod.QrUpd.SQL.Add('INSERT INTO sacados SET ')
  else
    Dmod.QrUpd.SQL.Add('UPDATE sacados SET ');
  Dmod.QrUpd.SQL.Add('Nome=:P0, Rua=:P1, Numero=:P2, Compl=:P3, Bairro=:P4, ');
  Dmod.QrUpd.SQL.Add('Cidade=:P5, UF=:P6, CEP=:P7, Tel1=:P8, Risco=:P9, ');
  Dmod.QrUpd.SQL.Add('IE=:P10, Email=:P11 ');
  if Registros = 0 then
    Dmod.QrUpd.SQL.Add(', CNPJ=:Pa')
  else
    Dmod.QrUpd.SQL.Add('WHERE CNPJ=:Pa');
  //
  Dmod.QrUpd.Params[00].AsString  := Nome;
  Dmod.QrUpd.Params[01].AsString  := Rua;
  Dmod.QrUpd.Params[02].AsInteger := Numero;
  Dmod.QrUpd.Params[03].AsString  := Compl;
  Dmod.QrUpd.Params[04].AsString  := Bairro;
  Dmod.QrUpd.Params[05].AsString  := Cidade;
  Dmod.QrUpd.Params[06].AsString  := UF;
  Dmod.QrUpd.Params[07].AsString  := CEP;
  Dmod.QrUpd.Params[08].AsString  := Tel1;
  Dmod.QrUpd.Params[09].AsFloat   := Risco;
  Dmod.QrUpd.Params[10].AsString  := IE;
  Dmod.QrUpd.Params[11].AsString  := Email;
  //
  Dmod.QrUpd.Params[12].AsString  := CNPJ;
  Dmod.QrUpd.ExecSQL;
  //

  //  ATUALIZAR ITENS SENDO IMPORTADOS
  UCriar.RecriaTabelaLocal('ImportLote', 1);

  Dmod.QrUpdL.SQL.Clear;
  Dmod.QrUpdL.SQL.Add('UPDATE importlote SET');
  Dmod.QrUpdL.SQL.Add('Nome_2=:P0, Rua_2=:P1, Numero_2=:P2, Compl_2=:P3, ');
  Dmod.QrUpdL.SQL.Add('Bairro_2=:P4, Cidade_2=:P5, UF_2=:P6, CEP_2=:P7, ');
  Dmod.QrUpdL.SQL.Add('Tel1_2=:P8, RiscoSA=:P9, IE_2=:P10, Email_2=:P11, ');
  Dmod.QrUpdL.SQL.Add('CPF_2=:Pa WHERE CPF=:Pb');
  //
  Dmod.QrUpdL.Params[00].AsString  := Nome;
  Dmod.QrUpdL.Params[01].AsString  := Rua;
  Dmod.QrUpdL.Params[02].AsInteger := Numero;
  Dmod.QrUpdL.Params[03].AsString  := Compl;
  Dmod.QrUpdL.Params[04].AsString  := Bairro;
  Dmod.QrUpdL.Params[05].AsString  := Cidade;
  Dmod.QrUpdL.Params[06].AsString  := UF;
  Dmod.QrUpdL.Params[07].AsString  := CEP;
  Dmod.QrUpdL.Params[08].AsString  := Tel1;
  Dmod.QrUpdL.Params[09].AsFloat   := Risco;
  Dmod.QrUpdL.Params[10].AsString  := IE;
  Dmod.QrUpdL.Params[11].AsString  := Email;
  //
  Dmod.QrUpdL.Params[12].AsString  := CNPJ;
  Dmod.QrUpdL.Params[13].AsString  := CNPJ;
  Dmod.QrUpdL.ExecSQL;
end;

procedure TFmPrincipal.SalvaInfoRegEdit(Form: TForm; Dest: Integer; MinhaGrade: TStringGrid);
var
  Cam, N: String;
  i, j, L, ARow: Integer;
  Painel: TPanel;
  Grade: TDBGrid;
begin
  ARow := 0;
  Cam := Application.Title+'\InitialConfig\'+Form.Name;
  if Dest = 1 then
  begin
    //if not Aplicar1.Checked then Exit;
    Geral.WriteAppKeyLM2('WindowState', Cam, Form.WindowState, ktInteger);
    Geral.WriteAppKeyLM2('WindowWidth', Cam, Form.Width, ktInteger);
    Geral.WriteAppKeyLM2('WindowHeigh', Cam, Form.Height, ktInteger);
    //Geral.WriteAppKeyLM2('WindowAplic', Cam, Aplicar1.Checked, ktBoolean);
  end else
  if (Dest = 0) and (MinhaGrade <> nil) then
  begin
    MinhaGrade.ColWidths[1] := 128;
    MinhaGrade.ColWidths[3] := 128;
    MinhaGrade.ColWidths[4] := 128;
    MinhaGrade.ColWidths[5] := 128;
    MinhaGrade.Cells[0, 0] := 'Componente';
    MinhaGrade.Cells[1, 0] := 'Nome do componente';
    MinhaGrade.Cells[2, 0] := 'Comprimento';
    MinhaGrade.Cells[3, 0] := 'Coluna';
    MinhaGrade.Cells[4, 0] := 'Campo';
    MinhaGrade.Cells[5, 0] := 'T�tulo';
  end;
  for i := 0 to Form.ComponentCount -1 do
  begin
    if Form.Components[i] is TPanel then
    begin
      Painel := TPanel(Form.Components[i]);
      N := Painel.Name;
      if Painel.Align <> alClient then
      begin
        L := Painel.Width;
        if Dest = 1 then
          Geral.WriteAppKeyLM2(N, Cam, L, ktInteger)
        else
        if (Dest = 0) and (MinhaGrade<>nil) then
        begin
          ARow := ARow + 1;
          MinhaGrade.RowCount := ARow + 1;
          MinhaGrade.Cells[0, ARow] := 'TPanel';
          MinhaGrade.Cells[1, ARow] := Painel.Name;
          MinhaGrade.Cells[2, ARow] := Geral.FF0(Painel.Width);
        end;
      end;
    end;
    if Form.Components[i] is TDBGrid then
    begin
      Grade := TDBGrid(Form.Components[i]);
      for j := 0 to Grade.Columns.Count-1 do
      begin
        if Dest = 1 then
        begin
          Geral.WriteAppKeyLM2(Geral.FF0(j), Cam + '\Grades\' + Grade.Name + '\Pos', Grade.Columns[j].FieldName, ktString);
          Geral.WriteAppKeyLM2(Geral.FF0(j), Cam + '\Grades\' + Grade.Name + '\Title', Grade.Columns[j].Title.Caption, ktString);
          Geral.WriteAppKeyLM2(Geral.FF0(j), Cam + '\Grades\' + Grade.Name + '\Width', Grade.Columns[j].Width, ktInteger);
        end else
        if (Dest = 0) and (MinhaGrade<>nil) then
        begin
          ARow := ARow + 1;
          MinhaGrade.RowCount := ARow + 1;
          MinhaGrade.Cells[0, ARow] := 'TDBGrid';
          MinhaGrade.Cells[1, ARow] := Grade.Name;
          MinhaGrade.Cells[2, ARow] := Geral.FF0(Grade.Columns[j].Width);
          MinhaGrade.Cells[3, ARow] := Geral.FF0(j);
          MinhaGrade.Cells[4, ARow] := Grade.Columns[j].FieldName;
          MinhaGrade.Cells[5, ARow] := Grade.Columns[j].Title.Caption;
        end;
      end;
    end;
  end;
end;

procedure TFmPrincipal.AplicaInfoRegEdit(Form: TForm; Forca: Boolean);
var
  Cam, N: String;
  //W, H,
  i, L, j: Integer;
  Painel: TPanel;
  Grade: TDBGrid;
  Query: TmySQLQuery;
begin
  if Forca then FAplicaInfoRegEdit := False;
  if FAplicaInfoRegEdit then Exit;
  FAplicaInfoRegEdit := True;
  Cam := Application.Title+'\InitialConfig\'+Form.Name;
  if Forca or Geral.ReadAppKeyLM('WindowAplic', Cam, ktBoolean, False) then
  begin
    //Aplicar1.Checked := True;
    Form.WindowState := Geral.ReadAppKeyLM('WindowState', Cam, ktInteger, 0);
    for i := 0 to Form.ComponentCount -1 do
    begin
      ////////////////////////////////////////////////////////////////////////
      if Form.Components[i] is TmySQLQuery then
      begin
        Query := TmySQLQuery(Form.Components[i]);
        for j := 0 to Query.FieldCount-1 do
        begin
          if Query.Fields[j] is TDateField then
          TDateField(Query.Fields[j]).DisplayFormat := VAR_FORMATDATEi;
        end;
      end;
      ////////////////////////////////////////////////////////////////////////
      if Form.Components[i] is TPanel then
      begin
        Painel := TPanel(Form.Components[i]);
        N := Painel.Name;
        if Painel.Align <> alClient then
        begin
          L := Geral.ReadAppKeyLM(N, Cam, ktInteger, 0);
          if L > 0 then
          Painel.Width := L;
        end;
      end;
      ////////////////////////////////////////////////////////////////////////
      if Form.Components[i] is TDBGrid then
      begin
        Grade := TDBGrid(Form.Components[i]);
        for j := 0 to Grade.Columns.Count-1 do
        begin
          N := Geral.ReadAppKeyLM(Geral.FF0(j), Cam + '\Grades\' + Grade.Name + '\Pos', ktString, '');
          //
          if N <> '' then
            Grade.Columns[j].FieldName := N;
          //
          N := Geral.ReadAppKeyLM(Geral.FF0(j), Cam + '\Grades\' + Grade.Name + '\Title', ktString, '');
          //
          if N <> '' then
            Grade.Columns[j].Title.Caption := N;
          //
          L := Geral.ReadAppKeyLM(Geral.FF0(j), Cam + '\Grades\' + Grade.Name + '\Width', ktInteger, 0);
          //
          if L > 0 then
            Grade.Columns[j].Width := L;
        end;
      end;
    end;
  end;
end;

procedure TFmPrincipal.ResetaDadosComponentes(Form: TForm; MinhaGrade: TStringGrid);
var
  i, c: Integer;
  Painel: TPanel;
  Grade: TDBGrid;
begin
  for i := 1 to MinhaGrade.RowCount-1 do
  begin
    if MinhaGrade.Cells[0, i] = 'TDBGrid' then
    begin
      c := StrToInt(MinhaGrade.Cells[3, i]);
      Grade := TDBGrid(Form.FindComponent(MinhaGrade.Cells[1, i]));
      Grade.Columns[c].Width := StrToInt(MinhaGrade.Cells[2, i]);
      Grade.Columns[c].FieldName := MinhaGrade.Cells[4, i];
      Grade.Columns[c].Title.Caption := MinhaGrade.Cells[5, i];
    end;
    if MinhaGrade.Cells[0, i] = 'TPanel' then
    begin
      if TPanel(FindComponent(MinhaGrade.Cells[1, i])).Name = '' then
        Geral.MB_Erro('ERRO! Painel sem nome!')
      else begin
        Painel := TPanel(Form.FindComponent(MinhaGrade.Cells[1, i]));
        Painel.Width := StrToInt(MinhaGrade.Cells[2, i]);
      end;
    end;
  end;
  MLAGeral.ResetaConfiguiracoesIniciais(True, True);
  SalvaInfoRegEdit(Form, 1, nil);
end;

procedure TFmPrincipal.Taxas1Click(Sender: TObject);
begin
  MostraTaxas(0);
end;

procedure TFmPrincipal.BtChequeClick(Sender: TObject);
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Application.CreateForm(TFmChequesGer, FmChequesGer);
    FmChequesGer.ShowModal;
    FmChequesGer.Destroy;
    Screen.Cursor := MyCursor
  except
    Screen.Cursor := MyCursor;
    raise;
  end;
  //Abre o novo o antigo n�o usa mais
  //MyObjects.MostraPopUpDeBotao(PMCheques, BtCheque);
end;

procedure TFmPrincipal.Scios1Click(Sender: TObject);
begin
  Application.CreateForm(TFmSociosImp, FmSociosImp);
  FmSociosImp.ShowModal;
  FmSociosImp.Destroy;
end;

procedure TFmPrincipal.Operaes1Click(Sender: TObject);
begin
  Application.CreateForm(TFmOperaImp, FmOperaImp);
  FmOperaImp.ShowModal;
  FmOperaImp.Destroy;
end;

procedure TFmPrincipal.ContratodeFomentoMercantil1Click(Sender: TObject);
begin
  Application.CreateForm(TFmContratoImp, FmContratoImp);
  FmContratoImp.ShowModal;
  FmContratoImp.Destroy;
end;

procedure TFmPrincipal.Textos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCartas, FmCartas, afmoNegarComAviso) then
  begin
    FmCartas.FTipo := 4;
    FmCartas.ShowModal;
    FmCartas.Destroy;
    //
    Dmod.QrControle.Close;
    Dmod.QrControle.Open;
  end;
end;

procedure TFmPrincipal.Taxadejurocomposto1Click(Sender: TObject);
begin
  Application.CreateForm(TFmDecomp, FmDecomp);
  FmDecomp.ShowModal;
  FmDecomp.Destroy;
end;

procedure TFmPrincipal.Jurocomposto1Click(Sender: TObject);
begin
  Application.CreateForm(TFmComposto, FmComposto);
  FmComposto.ShowModal;
  FmComposto.Destroy;
end;

procedure TFmPrincipal.Juros1Click(Sender: TObject);
begin
  Application.CreateForm(TFmCompJuros, FmCompJuros);
  FmCompJuros.ShowModal;
  FmCompJuros.Destroy;
end;

procedure TFmPrincipal.BtEntidadesExclamacaoClick(Sender: TObject);
begin
  Application.CreateForm(TFmOcorreuC, FmOcorreuC);
  FmOcorreuC.ShowModal;
  FmOcorreuC.Destroy;
end;

procedure TFmPrincipal.Contratos1Click(Sender: TObject);
begin
  MostraCartaG(0);
end;

procedure TFmPrincipal.Corrigecompensaodelanamentos1Click(Sender: TObject);
var
  Controle, Posi, Tot: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  QrLoc2.Close;
  QrLoc2.SQL.Clear;
  QrLoc2.SQL.Add('SELECT * FROM lanctos');
  QrLoc2.Open;
  //
  QrLoc2.First;
  if QrLoc2.RecordCount > 0 then
  begin
    Posi := 0;
    Tot  := QrLoc2.RecordCount;
    //
    ProgressBar2.Visible  := True;
    ProgressBar2.Position := 0;
    ProgressBar2.Max      := Tot;
    //
    while not QrLoc2.Eof do
    begin
      Posi     := Posi + 1;
      Controle := QrLoc2.FieldByName('Controle').AsInteger;
      //
      UFinanceiro.AtualizaEmissaoMasterExtra_Novo(Controle, QrLoc2, nil, nil, nil, nil, '', '');
      //
      ProgressBar2.Position := Posi;
      ProgressBar2.Update;
      Application.ProcessMessages;
      //
      QrLoc2.Next;
    end;
    ProgressBar2.Visible   := False;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.CorrigeEmpresa1Click(Sender: TObject);
begin
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE controle SET Dono=-1');
  DMod.QrUpd.ExecSQL;
  //
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE entidades SET CliInt=-11, Filial=-11 WHERE Codigo=-11');
  DMod.QrUpd.ExecSQL;
  //
  DMod.QrUpd.SQL.Clear;
  DMod.QrUpd.SQL.Add('UPDATE entidades SET CliInt=-1, Filial=-1 WHERE Codigo=-1');
  DMod.QrUpd.ExecSQL;
end;

procedure TFmPrincipal.Corrigestatusdeocorrncias1Click(Sender: TObject);
var
  Codigo: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  try
    Screen.Cursor := crHourGlass;
    //
    QrLoc2.Close;
    QrLoc2.SQL.Clear;
    QrLoc2.SQL.Add('SELECT *');
    QrLoc2.SQL.Add('FROM ocorreu');
    QrLoc2.SQL.Add('WHERE Valor + TaxaV - Pago = 0');
    QrLoc2.SQL.Add('AND Status < 2');
    QrLoc2.Open;
    if QrLoc2.RecordCount > 0 then
    begin
      QrLoc2.First;
      while not QrLoc2.Eof do
      begin
        Codigo := QrLoc2.FieldByName('Codigo').AsInteger;
        if Codigo > 0 then
        begin
          UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ocorreu', False, ['status'],
            ['Codigo'], [2], [Codigo], True);
        end;
        //
        QrLoc2.Next;
      end;
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

function TFmPrincipal.CartaoDeFatura: Integer;
begin
  ShowMessage('Faturando n�o implementado');
  Result := 0;
end;

function TFmPrincipal.CompensacaoDeFatura: String;
begin
  ShowMessage('Faturando n�o implementado');
  Result := '';
end;

procedure TFmPrincipal.MostraCartaG(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmCartaG, FmCartaG, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmCartaG.LocCod(Codigo, Codigo);
    FmCartaG.FTipo := 4;
    FmCartaG.ShowModal;
    FmCartaG.Destroy;
  end;
end;

procedure TFmPrincipal.MostraCarteiras(Carteira: Integer);
begin
  // n�o � necess�rio
end;

procedure TFmPrincipal.MostraDiarioAdd(Assunto, Contato, Depto: Integer;
  Texto: String);
var
  CliInt: Integer;
begin
  CliInt := Dmod.QrControleDono.Value;
  //
  if DBCheck.CriaFm(TFmDiarioAdd, FmDiarioAdd, afmoNegarComAviso) then
  begin
    FmDiarioAdd.PreencheDados(False, 0, 0, Assunto, CliInt, 0, Contato, 0, Texto);
    //
    FmDiarioAdd.FDepto          := Depto;
    FmDiarioAdd.FChamou         := -1;
    FmDiarioAdd.ImgTipo.SQLType := stIns;
    FmDiarioAdd.ShowModal;
    FmDiarioAdd.Destroy;
  end;
end;

procedure TFmPrincipal.MostraDiarioGer(Assunto, Contato, Depto: Integer;
  Texto: String);
var
  CliInt: Integer;
begin
  CliInt := Dmod.QrControleDono.Value;
  //
  if DBCheck.CriaFm(TFmDiarioGer, FmDiarioGer, afmoNegarComAviso) then
  begin
    if Assunto <> 0 then
    begin
      FmDiarioGer.EdDiarioAss.ValueVariant := Assunto;
      FmDiarioGer.CBDiarioAss.KeyValue     := Assunto;
    end;
    if CliInt <> 0 then
    begin
      FmDiarioGer.EdCliInt.ValueVariant    := CliInt;
      FmDiarioGer.CBCliInt.KeyValue        := CliInt;
    end;
    if Contato <> 0 then
    begin
      FmDiarioGer.EdEntidade.ValueVariant  := Contato;
      FmDiarioGer.CBEntidade.KeyValue      := Contato;
    end;
    if Depto <> 0 then
      FmDiarioGer.EdLctCtrl.Text := Geral.FF0(Depto);
    if Length(Texto) > 0 then
      FmDiarioGer.EdNome.Text := Texto;
    //
    FmDiarioGer.ShowModal;
    FmDiarioGer.Destroy;
  end;
end;

procedure TFmPrincipal.MostraDiarioAss();
begin
  if DBCheck.CriaFm(TFmDiarioAss, FmDiarioAss, afmoNegarComAviso) then
  begin
    FmDiarioAss.ShowModal;
    FmDiarioAss.Destroy;
  end;
end;

procedure TFmPrincipal.MostraDuplicatas2(Controle, Cliente: Integer);
begin
  Application.CreateForm(TFmDuplicatas2, FmDuplicatas2);
  if Controle > 0 then
    FmDuplicatas2.EdControle.Texto := FormatFloat('0', Controle);
  if Cliente <> 0 then
  begin
    FmDuplicatas2.EdCliente.ValueVariant := Cliente;
    FmDuplicatas2.CBCliente.KeyValue     := Cliente;
  end;
  FmDuplicatas2.ShowModal;
  FmDuplicatas2.Destroy;
end;

procedure TFmPrincipal.MostraEntiJur1(Entidade: Integer);
begin
  if DBCheck.CriaFm(TFmEntiJur1, FmEntiJur1, afmoNegarComAviso) then
  begin
    if Entidade <> 0 then
      FmEntiJur1.LocCod(Entidade, Entidade);
    FmEntiJur1.ShowModal;
    FmEntiJur1.Destroy;
  end;
end;

procedure TFmPrincipal.MostraOpcoesCreditoX;
begin
  if DBCheck.CriaFm(TFmOpcoesCreditor, FmOpcoesCreditor, afmoNegarComAviso) then
  begin
    FmOpcoesCreditor.ShowModal;
    FmOpcoesCreditor.Destroy;
  end;
end;

procedure TFmPrincipal.MostraRemessaCNABNovo(Codigo, Controle: Integer);
begin
  Application.CreateForm(TFmCNAB_Lot, FmCNAB_Lot);
  if (Codigo > 0) and (Controle > 0) then
  begin
    FmCNAB_Lot.LocCod(Codigo, Codigo);
    FmCNAB_Lot.ReopenCNAB_LotIts(Controle);
  end;
  FmCNAB_Lot.ShowModal;
  FmCNAB_Lot.Destroy;
end;

procedure TFmPrincipal.MostraTaxas(Codigo: Integer);
begin
  Application.CreateForm(TFmTaxas, FmTaxas);
  if Codigo <> 0 then
    FmTaxas.LocCod(Codigo, Codigo);
  FmTaxas.ShowModal;
  FmTaxas.Destroy;
end;

procedure TFmPrincipal.CriaCalcPercent(Valor, Porcentagem: String; Calc: TcpCalc );
begin
  Application.CreateForm(TFmCalcPercent, FmCalcPercent);
  with FmCalcPercent do
  begin
    if Calc = cpMulta    then LaPorcent.Caption := '% Multa';
    if Calc = cpJurosMes then LaPorcent.Caption := '% Juros/m�s';
    EdValor.Text := Valor;
    EdPercent.Text := Porcentagem;
    ShowModal;
    Destroy;
  end;
end;

procedure TFmPrincipal.Contas1Click(Sender: TObject);
begin
  CadastroDeContas();
end;

procedure TFmPrincipal.CadastroDeContas();
begin
  if DBCheck.CriaFm(TFmContas, FmContas, afmoNegarComAviso) then
  begin
    FmContas.ShowModal;
    FmContas.Destroy;
  end;
end;

procedure TFmPrincipal.CadastroDeContasNiv;
begin
  if DBCheck.CriaFm(TFmContasNiv, FmContasNiv, afmoNegarComAviso) then
  begin
    FmContasNiv.ShowModal;
    FmContasNiv.Destroy;
  end;
end;

procedure TFmPrincipal.TextosCartasGetValue(const ParName: String;
var ParValue: Variant);
begin
       if ParName = 'LIMITE  ' then ParValue := 0
  else if ParName = 'NUM_CONT' then ParValue := 0
  else if ParName = 'ADITIVO ' then ParValue := 0
  else if ParName = 'DATA_CON' then ParValue := 0
  else if ParName = 'PLU1_TIT' then ParValue := '(S)'
  else if ParName = 'PLU2_TIT' then ParValue := 'i(ram)'
  else if ParName = 'PLU3_TIT' then ParValue := '�(�o)'
  else if ParName = 'MEMPRESA' then ParValue := Dmod.QrDonoNomeDono.Value
  else if ParName = 'NOME_CLI' then ParValue := ''
  else if ParName = 'DUPLISAC' then ParValue := ''
  else if ParName = 'PLU5_TIT' then ParValue := 'l(is)'
  else if ParName = 'PLU6_TIT' then ParValue := '�o(�es)'
  else if ParName = 'PLU7_TIT' then ParValue := '(es)'
  else if ParName = 'CHEQEMIT' then ParValue := ''
  else if ParName = 'NOME_EMIT' then ParValue := ''
  // cancelamento de protesto
  else if ParName = 'NOME_SAC' then ParValue := ''
  else if ParName = 'TIPD_SAC' then ParValue := ''
  else if ParName = 'DOCU_SAC' then ParValue := ''
  else if ParName = 'ENDE_SAC' then ParValue := ''
end;

procedure TFmPrincipal.Depsitos1Click(Sender: TObject);
begin
  DepositoDeCheques(0);
end;

procedure TFmPrincipal.DepositoDeCheques(Config: Integer);
begin
  Application.CreateForm(TFmDeposimp, FmDeposimp);
  case Config of
    1:
    begin
      FmDeposimp.TPIni.Date := Date;
      FmDeposimp.TPFim.Date := Date;
      FmDeposimp.RGAgrupa.ItemIndex := 0;
      //FmDeposimp.CkQuitado.Checked := True;
      FmDeposimp.RGRepasse.ItemIndex := 0;
      FmDeposimp.RGOrdem1.ItemIndex := 0;
      FmDeposimp.FImprime := True;
    end;
  end;
  FmDeposimp.ShowModal;
  FmDeposimp.Destroy;
end;

procedure TFmPrincipal.ImprimeChequeSimples(Valor, Creditado, Cidade: String;
Data: TDateTime; Nominal: Boolean);
var
  Dia, Mes, Ano: Word;
begin
  DecodeDate(Data, Ano, Mes, Dia);
  FImpCHSimVal := Valor;
  FImpCHSimExt := dmkPF.ExtensoMoney(Valor);
  if Nominal then FImpCHSimEnt := Creditado else FImpCHSimEnt := '';
  FImpCHSimCid := Cidade;
  FImpCHSimDia := Geral.FF0(Dia);
  FImpCHSimMes := dmkPF.VerificaMes(Mes, True);
  FImpCHSimAno := Geral.FF0(Ano);
  //
  MyObjects.frxMostra(frxChequeSimples, 'Cheque simples');
end;

procedure TFmPrincipal.AlneasdeDuplicatas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmAlineasDup, FmAlineasDup, afmoNegarComAviso) then
  begin
    FmAlineasDup.ShowModal;
    FmAlineasDup.Destroy;
  end;
end;

procedure TFmPrincipal.BtDuplicataClick(Sender: TObject);
begin
 // MyObjects.MostraPopUpDeBotao(PMDuplicatas, BtDuplicata);
  MostraDuplicatas2(0, 0);
end;

procedure TFmPrincipal.BtChequeDepositoClick(Sender: TObject);
begin
  DepositoDeCheques(1);
end;

procedure TFmPrincipal.Serviosdowindows1Click(Sender: TObject);
begin
  Application.CreateForm(TFmServicoManager, FmServicoManager);
  FmServicoManager.ShowModal;
  FmServicoManager.Destroy;
end;

procedure TFmPrincipal.Cheque1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmEmiteCheque_0, FmEmiteCheque_0, afmoNegarComAviso) then
  begin
    FmEmiteCheque_0.ShowModal;
    FmEmiteCheque_0.Destroy;
  end;
end;

procedure TFmPrincipal.BtComandoClick(Sender: TObject);
var
  Retorno: String;
begin
  EdResposta.Text := '';
  MeResposta.Text := '';
  EdResposta.Text := PertoChekP.EnviaPertoChek2(EdComando.Text, Retorno);
  MeResposta.Text := Retorno;
end;

procedure TFmPrincipal.BtRefrPrtoClick(Sender: TObject);
var
  Retorno: String;
begin
  PertoChekP.DesabilitaPertoCheck;
  PertoChekP.HabilitaPertoChek(Retorno);
  if Retorno <> '' then
    Geral.MB_Aviso(Retorno);
end;

procedure TFmPrincipal.Emitentes1Click(Sender: TObject);
begin
  Application.CreateForm(TFmEmitentes, FmEmitentes);
  FmEmitentes.ShowModal;
  FmEmitentes.Destroy;
end;

procedure TFmPrincipal.Sacados1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSacados, FmSacados, afmoNegarComAviso) then
  begin
    FmSacados.ShowModal;
    FmSacados.Destroy;
  end;
end;

procedure TFmPrincipal.Movimento1Click(Sender: TObject);
begin
  {Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmCarteirasImp, FmCarteirasImp);
  finally
    Screen.Cursor := crDefault;
  end;
  FmCarteirasImp.ShowModal;
  FmCarteirasImp.Destroy;
  }
end;

procedure TFmPrincipal.NotasFiscais1Click(Sender: TObject);
begin
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmNFs, FmNFs);
  finally
    Screen.Cursor := crDefault;
  end;
  FmNFs.ShowModal;
  FmNFs.Destroy;
end;

procedure TFmPrincipal.AdicionaBorderoANF(NF, Bordero: Integer);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotes SET AlterWeb=1,   NF=:P0 WHERE Codigo=:P1');
  Dmod.QrUpd.Params[0].AsInteger := NF;
  Dmod.QrUpd.Params[1].AsInteger := Bordero;
  Dmod.QrUpd.ExecSQL;
  //
  Dmoc.AlterarNaWeb_Lote(Bordero);
end;

procedure TFmPrincipal.ClientesRpido1Click(Sender: TObject);
begin
  MostraEntiJur1(0);
end;

procedure TFmPrincipal.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else StatusBar.SimplePanel := False;
end;

procedure TFmPrincipal.BtMaloteClick(Sender: TObject);
begin
  Application.CreateForm(TFmMaloteConf, FmMaloteConf);
  FmMaloteConf.ShowModal;
  FmMaloteConf.Destroy;
end;

procedure TFmPrincipal.AtualizaSB2(Panel: Integer);
begin
  if Panel in ([0,1]) then
  begin
    QrMalotes.Close;
    QrMalotes.Params[0].AsInteger := FConnections;
    QrMalotes.Open;
    SB2.Panels[1].Text := ' '+ FloatToStr(QrMalotes.RecordCount);
  end;
  if Panel in ([0,2]) then
  begin
    QrECartaSac.Close;
    QrECartaSac.Open;
    SB2.Panels[3].Text := ' '+ FloatToStr(QrECartaSac.RecordCount);
  end;
end;

procedure TFmPrincipal.AtualsnoComputadorservidor1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Application.CreateForm(TFmRiscoAll, FmRiscoAll);
  FmRiscoAll.ShowModal;
  FmRiscoAll.Destroy;
  Screen.Cursor := crDefault;
end;

procedure TFmPrincipal.BtRiscoAllClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRiscoAll, BtRiscoAll);
end;

procedure TFmPrincipal.DBGrid1DblClick(Sender: TObject);
begin
  if QrECartaSac.RecordCount > 0 then
  begin
    Application.CreateForm(TFmLotes1, FmLotes1);
    FmLotes1.LocCod(QrECartaSacCodigo.Value, QrECartaSacCodigo.Value);
    FmLotes1.FImprime := 2;
    FmLotes1.ImprimeCartaAoSacadoDados;
    FmLotes1.Destroy;
  end;
end;

procedure TFmPrincipal.DBGrid2DblClick(Sender: TObject);
begin
  Application.CreateForm(TFmMaloteConf, FmMaloteConf);
  if not FmMaloteConf.QrLotes.Locate('Codigo', QrMalotesCodigo.Value, []) then
    Geral.MB_Aviso('Lote n�o localizado na pesquisa atual!');
  FmMaloteConf.ShowModal;
  FmMaloteConf.Destroy;
end;

procedure TFmPrincipal.TimerIdleTimer(Sender: TObject);
(*
var
  Dia: Integer;
*)
begin
  if DModG <> nil then
    DmodG.ExecutaPing(FmCreditor_MLA, [Dmod.MyDB, Dmod.MyDBn, DModG.MyPID_DB, DModG.AllID_DB]);
  (* 2017-05-24 => Movido para o evento OnIdle
  TimerIdle.Enabled := False;
  //
  Dia := Geral.ReadAppKeyLM('VeriNetVersao', Application.Title, ktInteger, 0);
  //
  if (Dia > 2) and (Dia < Int(Date)) then
  begin
    VerificaNovasVersoes;
  end;
  *)
end;

procedure TFmPrincipal.TmSuporteTimer(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.AtualizaSolicitApl2(Dmod.QrUpd, Dmod.MyDB, TmSuporte, TySuporte, nil, nil);
  {$ENDIF}
end;

procedure TFmPrincipal.TmVersaoTimer(Sender: TObject);
begin
  TmVersao.Enabled := False;
  //
  if DmkWeb.RemoteConnection then
  begin
    if VerificaNovasVersoes(True) then
      DmkWeb.MostraBalloonHintMenuTopo(BtVersao, BalloonHint1,
        'H� uma nova vers�o!', 'Clique aqui para atualizar.');
  end;
end;

procedure TFmPrincipal.Verificarnovaverso1Click(Sender: TObject);
begin
  VerificaNovasVersoes();
end;

procedure TFmPrincipal.VerificaTabelasTerceiros1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVerifiDBTerceiros, FmVerifiDBTerceiros, afmoNegarComAviso) then
  begin
    FmVerifiDBTerceiros.ShowModal;
    FmVerifiDBTerceiros.Destroy;
  end;
end;

function TFmPrincipal.VerificaNovasVersoes(ApenasVerifica: Boolean = False): Boolean;
var
  Versao, VersaoCredSc, VersaoCredScan: Integer;
  IP, ID, PW, BD, Arq, ArqNome: String;
begin
  VersaoCredScan := 0;
  //
  Result := DmkWeb.VerificaAtualizacaoVersao2(True, True, 'Creditor',
    'Creditor', Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), CO_VERSAO,
    CO_DMKID_APP, DModG.ObtemAgora(), Memo3, dtExec, Versao, ArqNome, False,
    ApenasVerifica, BalloonHint1);
  //
  if (Dmod.QrControleWeb_MySQL.Value > 0) and (Dmod.MyDBn.Connected) then
  begin
    IP := Dmod.QrControleWeb_Host.Value;
    ID := Dmod.QrControleWeb_User.Value;
    PW := Dmod.QrControleWeb_Pwd.Value;
    BD := Dmod.QrControleWeb_DB.Value;
    if not MLAGeral.FaltaInfo(4,
      [IP,'Servidor',ID,'Usu�rio',PW,'Senha',BD,'Banco de dados'],
      'Dados de conex�o ao MySQL no meu site') then
    begin
      Arq := ExtractFilePath(Application.ExeName)+'CredScan.Exe';
      if FileExists(Arq) then
        VersaoCredScan := Geral.VersaoInt2006(Geral.FileVerInfo(Arq, 3 (*Versao*)));
    end;
    DmkWeb.VerificaAtualizacaoVersao2(True, True, 'CredScan2', 'CredScan2',
      Geral.SoNumero_TT(DModG.QrMasterCNPJ.Value), VersaoCredScan, 25,
      DModG.ObtemAgora(), Memo3, dtExec, VersaoCredSc, ArqNome, False,
      ApenasVerifica, BalloonHint1);
  end;
end;

procedure TFmPrincipal.BtVersaoClick(Sender: TObject);
begin
  VerificaNovasVersoes;
end;

procedure TFmPrincipal.ReCaptionComponentesDeForm(Form: TForm(*; Memo: TMemo*));
begin
  //Apenas compatibilidade usado no Syndi2 
end;

function TFmPrincipal.RecriaTabelaLocal(Tabela: String; Acao: Integer): Boolean;
begin
  Result := Ucriar.RecriaTabelaLocal(Tabela, Acao);
end;

procedure TFmPrincipal.RegrasFiscais1Click(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeSrvCad();
end;

procedure TFmPrincipal.MySQL1Click(Sender: TObject);
begin
  Application.CreateForm(TFmMyInis, FmMyInis);
  FmMyInis.ShowModal;
  FmMyInis.Destroy;
end;

procedure TFmPrincipal.Cheques2Click(Sender: TObject);
begin
  Application.CreateForm(TFmRepasCH, FmRepasCH);
  FmRepasCH.ShowModal;
  FmRepasCH.Destroy;
end;

procedure TFmPrincipal.Duplicatas1Click(Sender: TObject);
begin
  Application.CreateForm(TFmRepasDU, FmRepasDU);
  FmRepasDU.ShowModal;
  FmRepasDU.Destroy;
end;

procedure TFmPrincipal.BtEuroExportClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRepasses, BtEuroExport);
end;

procedure TFmPrincipal.BitBtn1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmDuplicata1, FmDuplicata1, afmoNegarComAviso) then
  begin
    FmDuplicata1.ShowModal;
    FmDuplicata1.Destroy;
  end;
end;

procedure TFmPrincipal.BitBtn2Click(Sender: TObject);
begin
  ProgressBar1.Position := 0;
  ProgressBar1.Visible := True;
  QrLotes.Close;
  QrLotes.Open;
  ProgressBar1.Max := QrLotes.RecordCount;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotes SET AlterWeb=1, Itens=:P0 WHERE Codigo=:P1');
  while not QrLotes.Eof do
  begin
    ProgressBar1.Position := ProgressBar1.Position + 1;
    QrLotesIts.Close;
    QrLotesIts.Params[0].AsInteger := QrLotesCodigo.Value;
    QrLotesIts.Open;
    //
    Dmod.QrUpd.Params[0].AsFloat   := QrLotesItsITENS.Value;
    Dmod.QrUpd.Params[1].AsInteger := QrLotesCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrLotes.Next;
  end;
  ProgressBar1.Visible := False;
end;

procedure TFmPrincipal.BitBtn3Click(Sender: TObject);
begin
 if DBCheck.CriaFm(TFmPromissoria, FmPromissoria, afmoNegarComAviso) then
  begin
    FmPromissoria.ShowModal;
    FmPromissoria.Destroy;
  end;
end;

procedure TFmPrincipal.Inadimplncia1Click(Sender: TObject);
begin
  Application.CreateForm(TFmInadimps, FmInadimps);
  FmInadimps.ShowModal;
  FmInadimps.Destroy;
end;

procedure TFmPrincipal.BtInadimplenciaClick(Sender: TObject);
begin
  Inadimplncia1Click(Self);
end;

procedure TFmPrincipal.Conexo1Click(Sender: TObject);
var
  Servidor: Integer;
begin
  Servidor := Geral.ReadAppKeyLM('Server', Application.Title, ktInteger, 0);
  //
  Application.CreateForm(TFmServidor, FmServidor);
  FmServidor.RGTipo.ItemIndex := Servidor;
  FmServidor.ShowModal;
  FmServidor.Destroy;
  if (Servidor <> VAR_SERVIDOR) or (Servidor = 3) then
  begin
    Geral.MB_Aviso('O aplicativo dever� ser reinicializado!');
    Application.Terminate;
  end;
end;

procedure TFmPrincipal.Configuraesdeconsulta1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPC_Config, FmSPC_Config, afmoNegarComAviso) then
  begin
    FmSPC_Config.ShowModal;
    FmSPC_Config.Destroy;
  end;
end;

procedure TFmPrincipal.ConfiguraoEmissesMensais1Click(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSeMenCab(False, nil, nil);
end;

procedure TFmPrincipal.ConsultaSPC1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPC_Pesq, FmSPC_Pesq, afmoNegarComAviso) then
  begin
    FmSPC_Pesq.ShowModal;
    FmSPC_Pesq.Destroy;
  end;
end;

procedure TFmPrincipal.Relatorios1Click(Sender: TObject);
begin
  Application.CreateForm(TFmRepasImp, FmRepasImp);
  FmRepasImp.ShowModal;
  FmRepasImp.Destroy;
end;

procedure TFmPrincipal.Pagamentodechequesdevolvidos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmAPCD, FmAPCD, afmoNegarComAviso) then
  begin
    FmAPCD.ShowModal;
    FmAPCD.Destroy;
  end;
end;

procedure TFmPrincipal.Planos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPlano, FmPlano, afmoNegarComAviso) then
  begin
    FmPlano.ShowModal;
    FmPlano.Destroy;
  end;
end;

function TFmPrincipal.VerificaDiasCompensacao(Praca, CBE: Integer; Valor: Double;
DtEmiss, DtVence: TDateTime; SBC: Integer):
  Integer;
var
  V, E, S: Integer;
begin
  // TESTE
  // CBE Ser� calculado nos dias em TUModule.CalculaDias(...
  CBE := 0;
  // FIM TESTE
  V := Trunc(Int(DtVence));
  E := Trunc(Int(DtEmiss));
  if (V <> 0) and (E <> 0) then S := E-V else S := 0;
  if ((CBE > 0) and (S < CBE)) then
  begin
    Result := CBE + S;
    if Result < 0 then Result := 0;
  end else begin
    if SBC = 1 then Result := 0 else
    begin
      if Praca = Dmod.QrControleRegiaoCompe.Value then
        Result := Dmod.QrControleMinhaCompe.Value
      else
        Result := Dmod.QrControleOutraCompe.Value;
      if Valor < DMod.QrControleChequeMaior.Value then Result := Result + 1;
    end;
  end;
end;

procedure TFmPrincipal.Histricodealteraes1Click(Sender: TObject);
begin
  Application.CreateForm(TFmInfoChanges, FmInfoChanges);
  FmInfoChanges.ShowModal;
  FmInfoChanges.Destroy;
end;

procedure TFmPrincipal.Histrico2Click(Sender: TObject);
begin
  BtHistoricoClick(Self);
end;

procedure TFmPrincipal.BtHistoricoClick(Sender: TObject);
begin
  Application.CreateForm(TFmHistCliEmi, FmHistCliEmi);
  FmHistCliEmi.ShowModal;
  FmHistCliEmi.Destroy;
end;

procedure TFmPrincipal.Seleciona1Click(Sender: TObject);
begin
  SelecionaImagemdefundo;
end;

procedure TFmPrincipal.SelecionaImagemdefundo;
begin
  if OpenPictureDialog1.Execute then
  begin
    ImgPrincipal.Picture.LoadFromFile(OpenpictureDialog1.FileName);
    //
    Geral.WriteAppKeyLM2('ImagemFundo', Application.Title, OpenPictureDialog1.FileName, ktString);
  end;
end;

procedure TFmPrincipal.Original1Click(Sender: TObject);
begin
  ImgPrincipal.Picture := nil;
  //
  Geral.WriteAppKeyLM2('ImagemFundo', Application.Title, '', ktString);
end;

procedure TFmPrincipal.Bruto1Click(Sender: TObject);
begin
  Bruto2Click(Self);
end;

procedure TFmPrincipal.BtResultadosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMResultados, BtResultados);
end;

procedure TFmPrincipal.Bruto2Click(Sender: TObject);
begin
  Application.CreateForm(TFmOperaCol, FmOperaCol);
  FmOperaCol.ShowModal;
  FmOperaCol.Destroy;
end;

procedure TFmPrincipal.N5Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSimulaParcela, FmSimulaParcela, afmoNegarComAviso) then
  begin
    FmSimulaParcela.ShowModal;
    FmSimulaParcela.Destroy;
  end;
end;

procedure TFmPrincipal.LquidoNovo1Click(Sender: TObject);
begin
  Application.CreateForm(TFmResult1, FmResult1);
  FmResult1.ShowModal;
  FmResult1.Destroy;
end;

procedure TFmPrincipal.GerenciaNFSe1Click(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_NFSePesq(False, nil, nil);
end;

procedure TFmPrincipal.GerenciaResultados1Click(Sender: TObject);
begin
  Application.CreateForm(TFmOperaAll, FmOperaAll);
  FmOperaAll.ShowModal;
  FmOperaAll.Destroy;
end;

procedure TFmPrincipal.GerenciaRPS1Click(Sender: TObject);
begin
  UnNFSe_PF_0000.MostraFormNFSe_RPSPesq(False, nil, nil);
end;

procedure TFmPrincipal.CadastroGruSacEmi(Grupo: Integer);
begin
  VAR_CURSOR := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    //if UMyMod.AcessoNegadoAoForm('Perfis', 'GruSacEmi', 0) then Exit;
    Application.CreateForm(TFmGruSacEmi, FmGruSacEmi);
    FmGruSacEmi.FGrupoIni := Grupo;
    FmGruSacEmi.ShowModal;
    FmGruSacEmi.Destroy;
  finally
    Screen.Cursor := VAR_CURSOR;
  end;
end;

procedure TFmPrincipal.CadastroOcorBank(Codigo: Integer);
begin
  if DBCheck.CriaFm(TFmOcorBank, FmOcorBank, afmoNegarComAviso) then
  begin
    if Codigo <> 0 then
      FmOcorBank.LocCod(Codigo, Codigo);
    FmOcorBank.ShowModal;
    FmOcorBank.Destroy;
  end;
end;

procedure TFmPrincipal.GruposdeSacadosEmitentes1Click(Sender: TObject);
begin
  CadastroGruSacEmi(-1000);
end;

procedure TFmPrincipal.BtNegritoClick(Sender: TObject);
var
  Retorno: String;
begin
  EdResposta.Text := PertoChekP.EnviaPertoChek2(',1000', Retorno);
  MeResposta.Text := Retorno;
end;

procedure TFmPrincipal.AtualizaLastEditLote(Data: String);
begin
// Atualiza LastEditLote em controle para orientar usuario na atualiza��o
// do Evolucapi
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE controle SET LastEditLote=:P0 ');
  Dmod.QrUpd.SQL.Add('WHERE LastEditLote > :P0 ');
  Dmod.QrUpd.Params[0].AsString := Data;
  Dmod.QrUpd.Params[1].AsString := Data;
  Dmod.QrUpd.ExecSQL;
end;

procedure TFmPrincipal.BtEvolucapiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEvolucap, BtEvolucapi);
end;

procedure TFmPrincipal.DeclaraodeServio1Click(Sender: TObject);
const
  DPS           = 0;
  NFSeFatCab    = 0;
  Tomador       = 0;
  Intermediario = 0;
  MeuServico    = 0;
  ItemListSrv   = 0;
  Valor         = 0;

  Discriminacao = '';
  GeraNFSe      = True;
  SQLType       = stIns;
  Servico       = fgnLoteRPS;
var
  Prestador, NumNF: Integer;
  SerieNF: String;
begin
  Prestador := DmodG.QrFiliLogFilial.Value;
  UnNFSe_PF_0201.MostraFormNFSe(SQLType,
    Prestador, Tomador, Intermediario, MeuServico, ItemListSrv,
    Discriminacao, GeraNFSe, NFSeFatCab, DPS, Servico, nil, Valor, SerieNF,
    NumNF, nil);
end;

procedure TFmPrincipal.DefineVarsCliInt(CliInt: Integer);
begin
  DmodG.QrCliIntUni.Close;
  DmodG.QrCliIntUni.Params[0].AsInteger := CliInt;
  DmodG.QrCliIntUni.Open;
  //
  FCliInt_ := CliInt;
  FEntInt := DmodG.QrCliIntUniCodigo.Value;
  VAR_LIB_EMPRESAS := FormatFloat('0', DmodG.QrCliIntUniCodigo.Value);
  VAR_LIB_FILIAIS  := '';
  //
  DmodFin.QrCarts.Close;
  DmodFin.QrLctos.Close;
end;

procedure TFmPrincipal.BtFinancasClick(Sender: TObject);
{
var
  MenuItem: TMenuItem;
}
begin
  if DBCheck.CriaFm(TFmSelfGer, FmSelfGer, afmoNegarComAviso) then
  begin
    DefineVarsCliInt(Dmod.QrControleDono.Value);
    //F_EntInt := Dmod.QrMasterDono.Value;
    //ReabreCarteiras(F_EntInt, 0);
    DmodFin.ReabreCarteiras(0, DmodFin.QrCarts, DmodFin.QrCartSum,
    'TFmPrincipal.BtFinancasClick()');

    {
    FmSelfGer.BtEspecificos.Visible := True;
    FmSelfGer.BtEspecificos.Caption := 'Desig';
    MenuItem := TMenuItem.Create(FmSelfGer);
    MenuItem.Caption := 'Gerencia des�gnios';
    MenuItem.OnClick := CadastroDeDesignioSefGer;
    FmSelfGer.PMEspecificos.Items.Add(MenuItem);
    }

    FmSelfGer.ShowModal;
    FmSelfGer.Destroy;
    Application.OnHint := ShowHint;
    DefineVarsCliInt(Dmod.QrControleDono.Value);
    DmodFin.QrCarts.Close;
  end;
{
begin
  if DBCheck.CriaFm(TFmSelfGer, FmSelfGer, afmoNegarComAviso) then
  begin
    //FCliInt := 0;
    FEntInt := Dmod.QrMasterDono.Value;
    DmodFin.ReabreCarteiras(FEntInt, 0, DmodFin.QrCarts, DmodFin.QrCartSum);
    FmSelfGer.ShowModal;
    FmSelfGer.Destroy;
    Application.OnHint := ShowHint;
  end;
}
end;

procedure TFmPrincipal.LocalizarPagamento1Click(Sender: TObject);
begin
  Application.CreateForm(TFmLocPag, FmLocPag);
  FmLocPag.ShowModal;
  FmLocPag.Destroy;
end;

procedure TFmPrincipal.Excluiritensdelote1Click(Sender: TObject);
var
  Num: String;
  Cod, ITS: Integer;
begin
  Num := InputBox('Exclus�o for�ada de itens de lote.','Digite o n�mero do lote.', '' );
  QrLoc.SQL.Add('AND la.Cliente=en.Codigo');
  if Num = '' then Exit
  else begin
    Cod := Geral.IMV(Num);
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT COUNT(Codigo) ITENS');
    QrLoc.SQL.Add('FROM lotesits');
    QrLoc.SQL.Add('WHERE Codigo=' + Geral.FF0(Cod));
    QrLoc.Open;
    ITS := QrLocITENS.Value;
    //
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT COUNT(Codigo) ITENS');
    QrLoc.SQL.Add('FROM lotes');
    QrLoc.SQL.Add('WHERE Codigo=' + Geral.FF0(Cod));
    QrLoc.Open;
    //
    if Geral.MB_Pergunta('Confirma a exclus�o dos ' +
      Geral.FF0(ITS) + ' itens do lote ' + Geral.FF0(Cod)+'?') = ID_YES
    then begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM lotes WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Cod;
      Dmod.QrUpd.ExecSQL;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM lotesits WHERE Codigo=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Cod;
      Dmod.QrUpd.ExecSQL;
    end;
  end;
end;

procedure TFmPrincipal.Carteiras2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCarteiras, FmCarteiras, afmoNegarComAviso) then
  begin
    FmCarteiras.ShowModal;
    FmCarteiras.Destroy;
  end;
end;

procedure TFmPrincipal.CentraldeSuporte1Click(Sender: TObject);
begin
  {$IFDEF UsaWSuport}
  DmkWeb.MostraWSuporte;
  {$ENDIF}
end;

procedure TFmPrincipal.Exportacontabilidade1Click(Sender: TObject);
begin
  Application.CreateForm(TFmExpContab, FmExpContab);
  FmExpContab.ShowModal;
  FmExpContab.Destroy;
end;

procedure TFmPrincipal.Exportadadoscontabilidade1Click(Sender: TObject);
begin
  Application.CreateForm(TFmExpContabExp, FmExpContabExp);
  FmExpContabExp.ShowModal;
  FmExpContabExp.Destroy;
end;

{  MSN in�cio
procedure TFmPrincipal.Conecta1Click(Sender: TObject);
begin
  if not msn.Logined then
  begin
    tvMSN.Items.Clear;
    msn.MSNID       := Dmod.QrControlemsnID.Value;
    msn.MSNPassword := Dmod.QrControlemsnPW.Value;
    msn.Login;
  end;
  StatusMSN;
end;

function TFmPrincipal.StatusMSN: Boolean;
begin
 Result := msn.Logined;
 if Result then SB2.Panels[5].Text := ' CONECTADO'
 else SB2.Panels[5].Text := ' Desconectado';
end;

procedure TFmPrincipal.Timer2Timer(Sender: TObject);
begin
  if StatusMSN then
    msn.SendKeepAlive;
end;

procedure TFmPrincipal.Desconecta1Click(Sender: TObject);
begin
  msn.Logout;
end;

procedure TFmPrincipal.msnLoginStatus(Sender: TObject;
  const Status: String);
begin
  SB2.Panels[5].Text := Status;
end;

procedure TFmPrincipal.msnLogout(Sender: TObject);
begin
  SB2.Panels[5].Text := 'Desconectado';
  Pagina.ActivePageIndex := 5;
  PB1.Position := 0;
  tvMSN.Items.Clear;
  memLog.Lines.Clear;
  Root := nil;
end;

procedure TFmPrincipal.msnBlockBuddy(Sender: TObject; Buddy: TMSNBuddy);
var
  i: integer;
begin
  for i := 0 to tvMSN.Items.Count - 1 do
    if tvMSN.Items[i].Data = Buddy then
    begin
      tvMSN.Items[i].ImageIndex := 4;
      tvMSN.Items[i].SelectedIndex := 4;
      break;
    end;
end;

procedure TFmPrincipal.msnBuddyAddYouRequest(Sender: TObject;
  Buddy: String);
begin
  if MessageDlg(buddy + ' adicionou voc�. Voc� quer que ' + buddy + ' te veja?',
    mtConfirmation, [mbYes, mbNo], 0) = idYes then
    msn.AddBuddy(Buddy);
end;

procedure TFmPrincipal.msnBuddyStatusChanged(Sender: TObject;
  Buddy: TMSNBuddy);
var
  i: integer;
begin
  for i := 0 to tvMSN.Items.Count - 1 do
    if tvMSN.Items[i].Data = Buddy then
    begin
      //tvMSN.Items[i].ImageIndex := StatusAsImageIndex(Buddy.Status);
      tvMSN.Items[i].SelectedIndex := tvMSN.Items[i].ImageIndex;
      break;
    end;
end;

procedure TFmPrincipal.msnDeleteBuddy(Sender: TObject; Buddy: TMSNBuddy);
var
  i: integer;
begin
  for i := 0 to tvMSN.Items.Count - 1 do
  begin
    if tvMSN.Items[i].Data = Buddy then
    begin
      tvMSN.Items[i].Delete;
      break;
    end;
  end;
end;

procedure TFmPrincipal.msnDeleteGroup(Sender: TObject; Group: TMSNGroup);
var
  i: integer;
begin
  for i := 0 to Root.Count - 1 do
  begin
    if Root.Item[i].Data = Group then
    begin
      Root.Item[i].Delete;
      break;
    end;
  end;
end;

procedure TFmPrincipal.msnError(Sender: TObject; const ErrorCode: Integer);
begin
  SB2.Panels[5].Text := 'Erro: ' + Geral.FF0(ErrorCode);
end;

procedure TFmPrincipal.msnLogin(Sender: TObject);
begin
  AvatarIndex := 1;

  Pagina.ActivePageIndex := 5;
  Root := tvMSN.Items.AddChild(nil, msn.NickName);
  //Root.ImageIndex := StatusAsImageIndex(msn.Status);
  Root.SelectedIndex := Root.ImageIndex;

  Root.Selected := true;
end;

procedure TFmPrincipal.msnLoginProgress(Sender: TObject;
  const Progress: Integer);
begin
  PB1.Position := Progress;
end;

procedure TFmPrincipal.msnNicknameChanged(Sender: TObject);
begin
  Root.Text := Msn.NickName;
end;

procedure TFmPrincipal.msnReceiveBuddy(Sender: TObject; Buddy: TMSNBuddy);
var
  i: integer;
  g: TMSNGroup;
begin
  for i := 0 to Root.Count - 1 do
  begin
    g := TMSNGroup(Root.Item[i].Data);

    if g.ID = Buddy.GroupID then
    begin
      with tvMSN.Items.AddChild(Root.Item[i], Buddy.NickName) do
      begin
        Data := Buddy;
        //ImageIndex := StatusAsImageIndex(Buddy.Status);
        SelectedIndex := ImageIndex;
      end;
      break;
    end;
  end;
end;

procedure TFmPrincipal.msnReceiveGroup(Sender: TObject; Group: TMSNGroup);
var
  n: TTreeNode;
begin
  n := tvMSN.Items.AddChildFirst(Root, Group.GroupName);
  n.Data := Group;
  n.ImageIndex := -1;
  n.SelectedIndex := n.ImageIndex;
end;

procedure TFmPrincipal.msnRenameGroup(Sender: TObject; Group: TMSNGroup);
var
  i: integer;
begin
  for i := 0 to Root.Count - 1 do
  begin
    if Root.Item[i].Data = Group then
    begin
      Root.Item[i].Text := Group.GroupName;
      break;
    end;
  end;
end;

procedure TFmPrincipal.msnShakeEvent(Sender: TObject; Buddy: String);
begin
  memLog.Lines.Add(Buddy + ' enviou um SHAKE.');
end;

procedure TFmPrincipal.msnStatusChanged(Sender: TObject);
begin
  //Root.ImageIndex := StatusAsImageIndex(MSN.Status);
  Root.SelectedIndex := Root.ImageIndex;
end;

procedure TFmPrincipal.msnUnblockBuddy(Sender: TObject; Buddy: TMSNBuddy);
var
  i: integer;
begin
  for i := 0 to tvMSN.Items.Count - 1 do
    if tvMSN.Items[i].Data = Buddy then
    begin
      //tvMSN.Items[i].ImageIndex := StatusAsImageIndex(Buddy.Status);
      tvMSN.Items[i].SelectedIndex := tvMSN.Items[i].ImageIndex;
      break;
    end;
end;

procedure TFmPrincipal.tvMSNMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  p: TPoint;
  o: TObject;
begin
  if Button = mbRight then
  begin
    p.X := X;
    p.Y := Y;
    p := ClientToScreen(p);

    if tvMSN.Selected <> nil then
    begin
      if tvMSN.Selected = Root then
        pmRoot.Popup(p.X, p.Y)
      else
      begin
        o := TObject(tvMSN.Selected.Data);

        if o is TMSNGroup then
          pmGroup.Popup(p.X, p.Y);

        if o is TMSNBuddy then
          pmBuddy.Popup(p.X, p.Y);
      end;
    end;
  end;
end;

procedure TFmPrincipal.ChangeNickname1Click(Sender: TObject);
//var
  //s: string;
begin
  //if InputQuery('Muda apelido', 'Apelido', s) then
    //msn.ChangeNickname(s);
end;

procedure TFmPrincipal.MenuItem2Click(Sender: TObject);
var
  s: string;
  g: TMSNGroup;
begin
  g := TMSNGroup(tvMSN.Selected.Data);
  if InputQuery('Edita nome do Grupo', 'Novo nome do grupo', s) then
    msn.RenameGroup(g.ID, s);
end;

procedure TFmPrincipal.Delete1Click(Sender: TObject);
begin
  msn.DeleteGroup(TMSNGroup(tvMSN.Selected.Data).ID);
end;

procedure TFmPrincipal.Block1Click(Sender: TObject);
begin
  msn.BlockBuddy(TMSNBuddy(tvMSN.Selected.Data).Email);
end;

procedure TFmPrincipal.Unblock1Click(Sender: TObject);
begin
  msn.UnblockBuddy(TMSNBuddy(tvMSN.Selected.Data).Email);
end;

procedure TFmPrincipal.Delete2Click(Sender: TObject);
begin
  msn.DeleteBuddy(TMSNBuddy(tvMSN.Selected.Data).Email);
end;

procedure TFmPrincipal.SendMessage1Click(Sender: TObject);
var
  b: TMSNBuddy;
  s: string;
begin
  if InputQuery('Envio de mensagem instant�nea', 'Menssagem:', s) then
  begin
    b := TMSNBuddy(tvMSN.Selected.Data);
    msn.SendMessage(b.Email, s);
  end;
end;

procedure TFmPrincipal.SendFile1Click(Sender: TObject);
var
  b: TMSNBuddy;
begin
  if odSend.Execute then
  begin
    b := TMSNBuddy(tvMSN.Selected.Data);
    msn.SendFile(b.Email, odSend.FileName);
  end;
end;

procedure TFmPrincipal.SendShake1Click(Sender: TObject);
var
  b: TMSNBuddy;
begin
  if tvMSN.Selected <> nil then
  begin
    b := TMSNBuddy(tvMSN.Selected.Data);
    MSN.ShakeBuddy(b.Email);
  end;
end;

procedure TFmPrincipal.ReopenMSNs;
begin
  QrMSNs.Close;
  QrMSNs.Open;
  //
  Pagina.ActivePageIndex := 6;
end;

procedure TFmPrincipal.BtRefreshClick(Sender: TObject);
begin
  ReopenMSNs;
end;

procedure TFmPrincipal.QrMSNsCalcFields(DataSet: TDataSet);
begin
  QrMSNsENVIADO_TXT.Value := Geral.FDT(QrMSNsRecebido.Value, 9);
  case QrMSNsStatus.Value of
    0: QrMSNsNOMESTATUS.Value := 'N/D';
    1: QrMSNsNOMESTATUS.Value := 'Recebendo';
    2: QrMSNsNOMESTATUS.Value := 'Recebido';
    9: QrMSNsNOMESTATUS.Value := 'Processado';
  end;
  if QrMSNsItens.Value = 0 then QrMSNsPERCREC.Value :=  0
  else QrMSNsPERCREC.Value := (QrMSNsUltimo.Value / QrMSNsItens.Value) * 100;
end;
MSN Final}

procedure TFmPrincipal.ChangeStatus1Click(Sender: TObject);
var
  s: string;
begin
  s := 'NLN';
  if InputQuery('Altera Status', 'NLN: Dispon�vel' + sLineBreak +
    'BSY: Ocupado' + sLineBreak +
    'IDL: Esperando' + sLineBreak +
    'BRB: Volto logo' + sLineBreak +
    'AWY: Fui' + sLineBreak +
    'PHN: Ao telefone' + sLineBreak +
    'LUN: Sa� para almo�ar', s) then
    //msn.ChangeStatus(StringToMSNStatus(s));
end;

procedure TFmPrincipal.CobrancaBB;
begin
  Application.CreateForm(TFmCobrancaBB, FmCobrancaBB);
  FmCobrancaBB.ShowModal;
  FmCobrancaBB.Destroy;
end;

procedure TFmPrincipal.RetornoBB;
begin
  Application.CreateForm(TFmRetornoBB, FmRetornoBB);
  FmRetornoBB.ShowModal;
  FmRetornoBB.Destroy;
end;

procedure TFmPrincipal.BtCNAB240Click(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCNAB240, BtCNAB240);
end;

procedure TFmPrincipal.CobranaBB1Click(Sender: TObject);
begin
  CobrancaBB;
end;

procedure TFmPrincipal.BtDepositoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PmDeposito, BtDeposito);
end;

procedure TFmPrincipal.BtDownLotesClick(Sender: TObject);
begin
  Dmoc.BaixaLotesWeb;
end;

procedure TFmPrincipal.Janeladeimpresso1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxReport1, '');
end;

procedure TFmPrincipal.BtNormalClick(Sender: TObject);
var
  Retorno: String;
begin
  EdResposta.Text := PertoChekP.EnviaPertoChek2(',0000', Retorno);
  MeResposta.Text := Retorno;
end;

procedure TFmPrincipal.Button9Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
  ProgressBar3.Position := 0;
  QrAlinx.Close;
  QrAlinx.Open;
  ProgressBar3.Max := QrAlinx.RecordCount;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE alinits SET AlterWeb=1, Vencto=:P0, DDeposito=:P1, ');
  Dmod.QrUpd.SQL.Add('LoteOrigem=:P2 ');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P3 ');
  while not QrAlinx.Eof do
  begin
    ProgressBar3.Position := ProgressBar3.Position + 1;
    Application.ProcessMessages;
    //
    Dmod.QrUpd.Params[00].AsDate    := QrAlinxVencto.Value;
    Dmod.QrUpd.Params[01].AsDate    := QrAlinxDDeposito.Value;
    Dmod.QrUpd.Params[02].AsInteger := QrAlinxCOD_LOTE.Value;
    Dmod.QrUpd.Params[03].AsInteger := QrAlinxCOD_ALIN.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrAlinx.Next;
  end;
  Screen.Cursor := crDefault;
  ShowMessage('Atualiza��o terminada!');
  ProgressBar3.Position := 0;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmPrincipal.Button10Click(Sender: TObject);
var
  CPFs, BACs: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
  ProgressBar3.Position := 0;
  if CkCPF.Checked then
  begin
    QrEmitx.Close;
    QrEmitx.Open;
    CPFs := QrEmitx.RecordCount;
  end else CPFs := 0;
  if CkBAC.Checked then
  begin
    QrEmity.Close;
    QrEmity.Open;
    BACs := QrEmity.RecordCount;
  end else BACs := 0;
  ProgressBar3.Max := CPFs + BACs;
  if CkCPF.Checked then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO emitcpf SET CPF=:P0, Nome=:P1, Limite=:P2');
    while not QrEmitx.Eof do
    begin
      ProgressBar3.Position := ProgressBar3.Position + 1;
      Application.ProcessMessages;
      //
      Dmod.QrUpd.Params[00].AsString  := QrEmitxCPF.Value;
      Dmod.QrUpd.Params[01].AsString  := QrEmitxNome.Value;
      Dmod.QrUpd.Params[02].AsFloat   := QrEmitxRisco.Value;
      try
        Dmod.QrUpd.ExecSQL;
      except
        ;
      end;
      //
      QrEmitx.Next;
    end;
  end;
  if CkBAC.Checked then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO emitbac SET CPF=:P0, BAC=:P1');
    while not QrEmity.Eof do
    begin
      ProgressBar3.Position := ProgressBar3.Position + 1;
      Application.ProcessMessages;
      //
      Dmod.QrUpd.Params[00].AsString  := QrEmityCPF.Value;
      Dmod.QrUpd.Params[01].AsString  := QrEmityBAC.Value;
      try
        Dmod.QrUpd.ExecSQL;
      except
        ;
      end;
      //
      QrEmity.Next;
    end;
  end;
  Screen.Cursor := crDefault;
  ShowMessage('Atualiza��o terminada!');
  ProgressBar3.Position := 0;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmPrincipal.ArquivoMorto1Click(Sender: TObject);
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Application.CreateForm(TFmLotesMorto, FmLotesMorto);
    FmLotesMorto.ShowModal;
    FmLotesMorto.Destroy;
    Screen.Cursor := MyCursor
  except
    Screen.Cursor := MyCursor;
    raise;
  end;
end;

procedure TFmPrincipal.BtImportaClick(Sender: TObject);
begin
  if OpenDialog3.Execute then
  begin
    Screen.Cursor := crHourGlass;
    Application.ProcessMessages;
    MyObjects.LimpaGrade(GradeC, 1, 1, True);
    Application.ProcessMessages;
    Geral.LeArquivoToMemo(OpenDialog3.FileName, Memo1);
    CriaCMC7s;
    LeCMC7_Criados;
    ProcuraBACs;
    TPCheque.Date := Geral.ValidaDataSimples(GradeC.Cells[07, 1], True) +
      Geral.IMV( GradeC.Cells[08, 1]);
    if TPCheque.Date < 1000 then TPCheque.Date := Date;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrincipal.ProcuraBACs;
var
  i: Integer;
begin
  ProgressBar4.Position := 0;
  ProgressBar4.Max := GradeC.RowCount - 1;
  for i := 1 to GradeC.RowCount - 1 do
  begin
    ProgressBar4.Position := ProgressBar4.Position + 1;
    Application.ProcessMessages;
    LocalizaEmitente(i, True);
  end;
  ProgressBar4.Position := 0;
end;

function TFmPrincipal.CriaBAC_Pesq(Banco, Agencia, Conta: String; DVCC: Integer): String;
var
  RealCC: String;
begin
  RealCC := Copy(Conta, 10-DVCC+1, DVCC);
  Result := Banco+Agencia+RealCC;
end;

function TFmPrincipal.LocalizaEmitente(Linha: Integer; MudaNome: Boolean): Boolean;
var
  B,A,C, CPF: String;
begin
  Result := False;
  if  (Geral.IMV(GradeC.Cells[2,Linha]) > 0)
  and (Geral.IMV(GradeC.Cells[3,Linha]) > 0)
  and (Geral.DMV(GradeC.Cells[4,Linha]) > 0.1) then
  begin
    Screen.Cursor := crHourGlass;
    try
      //
      if MudaNome then
      begin
        B := GradeC.Cells[2,Linha];
        A := GradeC.Cells[3,Linha];
        C := GradeC.Cells[4,Linha];
        CPF := Geral.SoNumero_TT(EdCPF.Text);
        Dmoc.ReopenBanco(B);
        //
        Dmoc.QrLocEmiBAC.Close;
        Dmoc.QrLocEmiBAC.Params[0].AsInteger := Dmoc.QrBancoDVCC.Value;
        Dmoc.QrLocEmiBAC.Params[1].AsString  := CriaBAC_Pesq(B,A,C,Dmoc.QrBancoDVCC.Value);
        Dmoc.QrLocEmiBAC.Open;
        //
        if Dmoc.QrLocEmiBAC.RecordCount = 0 then
        begin
          //EdEmitente.Text := '';
          //EdCPF.Text := '';
        end else begin
          Dmoc.QrLocEmiCPF.Close;
          Dmoc.QrLocEmiCPF.Params[0].AsString := Dmoc.QrLocEmiBACCPF.Value;
          Dmoc.QrLocEmiCPF.Open;
          //
          if Dmoc.QrLocEmiCPF.RecordCount = 0 then
          begin
            //EdEmitente.Text := '';
            //EdCPF.Text := '';
          end else begin
            GradeC.Cells[10, Linha] := Dmoc.QrLocEmiCPFNome.Value;
            GradeC.Cells[09, Linha] := Geral.FormataCNPJ_TT(Dmoc.QrLocEmiCPFCPF.Value);
            //PesquisaRiscoSacado(EdCPF.Text);
          end;
        end;
      end;
      Screen.Cursor := crDefault;
      Result := True;
    except
      Screen.Cursor := crDefault;
      raise;
    end;
  end;
end;

procedure TFmPrincipal.CriaCMC7s;
var
  i: Integer;
  Txt: String;
  Dv3, Conta, Dv1, Tipo, Numero, Compe, Dv2, Agencia, Banco, Raz: String;
begin
  Memo2.Lines.Clear;
  for i := 0 to Memo1.Lines.Count - 1 do
  begin
    Txt := Memo1.Lines[i];
    if Length(Txt) > 30 then
    begin
      Compe   := Copy(Txt,014,03);
      Numero  := Copy(Txt,020,06);
      Tipo    := Copy(Txt,029,01);
      Dv1     := Copy(Txt,033,01);
      Banco   := Copy(Txt,037,03);
      Agencia := Copy(Txt,043,04);
      Dv2     := Copy(Txt,050,01);
      Raz     := Copy(Txt,054,03);
      Conta   := Copy(Txt,060,08);
      Dv3     := Copy(Txt,063,01);
      //
      Conta := Raz + Geral.SoNumero_TT(Conta);
      Memo2.Lines.Add(MLAGeral.RetornaCMC7(Banco, Agencia, Conta, Numero,
        Compe, Tipo));
    end;
  end;
end;

procedure TFmPrincipal.LeCMC7_Criados;
var
  i: Integer;
  Banda: TBandaMagnetica;
  Comp, Banco, Agencia, Conta, Cheque, Valor, Vencto: String;
begin
  FCheque := 0;
  Banda := TBandaMagnetica.Create;
  for i := 0 to Memo2.Lines.Count - 1 do
  begin
    if MLAGeral.CalculaCMC7(Memo2.Lines[i]) = 0 then
    begin
      Banda.BandaMagnetica := Geral.SoNumero_TT(Memo2.Lines[i]);
      Comp    := Banda.Compe;
      Banco   := Banda.Banco;
      Agencia := Banda.Agencia;
      Conta   := Banda.Conta;
      Cheque  := Banda.Numero;
      //
      Valor   := Trim(Copy(Memo1.Lines[i], 072, 15));
      Vencto  :=      Copy(Memo1.Lines[i], 001, 10);
      //
      FCheque := FCheque + 1;
      with FmPrincipal.GradeC do
      begin
        if FCheque > RowCount-1 then RowCount := FCheque + 1;
        Cells[00,FCheque] := Geral.FF0(FCheque);
        Cells[01,FCheque] := Comp;
        Cells[02,FCheque] := Banco;
        Cells[03,FCheque] := Agencia;
        Cells[04,FCheque] := Conta;
        Cells[05,FCheque] := Cheque;
        Cells[06,FCheque] := Valor;
        Cells[07,FCheque] := Vencto;
        Cells[09,FCheque] := '';//CPF;
        Cells[10,FCheque] := '';//Emitente;
        Cells[11,FCheque] := Geral.SoNumero_TT(Memo2.Lines[i]);
      end;
      FChangeData := True;
      ArqSCX.CalculaGradeC(EdQtdeCh, EdValorCH, nil, nil, nil,
        GradeC, nil, TPCheque);
    end;
  end;
end;

procedure TFmPrincipal.Lerarquivodetransfernciadedados1Click(Sender: TObject);
begin
  Application.CreateForm(TFmLeDataArq, FmLeDataArq);
  FmLeDataArq.ShowModal;
  FmLeDataArq.Destroy;
end;

procedure TFmPrincipal.EdBandaChange(Sender: TObject);
var
  CMC7: String;
  i: Integer;
begin
  BtConfAlt.Enabled := False;
  EdCPF.Text := '';
  EdEmitente.Text := '';
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
  begin
    CMC7 := Geral.SoNumero_TT(EdBanda.Text);
    if MLAGeral.CalculaCMC7(CMC7) = 0 then
    begin
      for i := 1 to GradeC.RowCount - 1 do
      begin
        if GradeC.Cells[11, i] = CMC7 then
        begin
          GradeC.Row := i;
          EdCPF.Text := GradeC.Cells[09, i];
          EdEmitente.Text := GradeC.Cells[10, i];
          EdCPF.SetFocus;
          BtConfAlt.Enabled := True;
          Break;
        end;
      end;
      EdCPF.SetFocus;
    end;
  end;
end;

procedure TFmPrincipal.EdCPFEnter(Sender: TObject);
begin
  //AvisaErroCMC7;
end;

procedure TFmPrincipal.EdEmitenteEnter(Sender: TObject);
begin
  if Trim(EdCPF.Text) = '' then EdCPF.SetFocus;
  //AvisaErroCMC7;
end;

procedure TFmPrincipal.BtConfAltClick(Sender: TObject);
var
  i, x: Integer;
  CMC7: String;
begin
  //if AvisaErroCMC7 then Exit;
  CMC7 := Geral.SoNumero_TT(EdBanda.Text);
  x := 0;
  for i := 1 to GradeC.RowCount - 1 do
  begin
    if GradeC.Cells[11, i] = CMC7 then
    begin
      x := i;
      Break;
    end;
  end;
  if GradeC.Cells[11, x] = CMC7 then
  begin
    GradeC.Cells[09, x] := EdCPF.Text;
    GradeC.Cells[10, x] := EdEmitente.Text;
    Dmoc.AtualizaEmitente(GradeC.Cells[2, i], GradeC.Cells[3, i],
      GradeC.Cells[4, i], Geral.SoNumero_TT(EdCPF.Text), EdEmitente.Text, -1);
    //
    EdBanda.Text := '';
    EdCPF.Text := '';
    EdEmitente.Text := '';
  end else
    Geral.MB_Erro('CMC7 n�o localizado!');
  EdBanda.SetFocus;
end;

procedure TFmPrincipal.EdCPFExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  Screen.Cursor := crHourGlass;
  CPF := Geral.SoNumero_TT(EdCPF.Text);
  if CPF <> CO_VAZIO then
  begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if MLAGeral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Geral.MB_Erro('N�mero inv�lido para CNPJ/CPF!');
      EdCPF.SetFocus;
    end else
    begin
      EdCPF.Text := Geral.FormataCNPJ_TT(CPF);
      if EdEmitente.Text = '' then
      begin
        Dmoc.QrLocEmiCPF.Close;
        Dmoc.QrLocEmiCPF.Params[0].AsString := CPF;
        Dmoc.QrLocEmiCPF.Open;
        //
        if Dmoc.QrLocEmiCPF.RecordCount > 0 then
          EdEmitente.Text := Dmoc.QrLocEmiCPFNome.Value;
      end;
      //PesquisaRiscoSacado(EdCPF.Text);
    end;
  end else EdCPF.Text := CO_VAZIO;
  Screen.Cursor := crDefault;
end;

procedure TFmPrincipal.EdCPFChange(Sender: TObject);
begin
  EdEmitente.Text := '';
end;

procedure TFmPrincipal.EdCPFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    EdBanda.Text := '';
    EdBanda.SetFocus;
  end;
end;

procedure TFmPrincipal.Chequesabertos1Click(Sender: TObject);
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Application.CreateForm(TFmDeposito, FmDeposito);
    FmDeposito.ShowModal;
    FmDeposito.Destroy;
    Screen.Cursor := MyCursor
  except
    Screen.Cursor := MyCursor;
    raise;
  end;
end;

procedure TFmPrincipal.SalvaArquivo(EdNomCli, EdCPFCli: TEdit; Grade:
  TStringGrid; Data: TDateTime; FileName: String; ChangeData: Boolean);
begin
  //MyCBase.SalvaArquivo(EdNomCli, EdCPFCli, Grade, Data, VAR_DBPATH+FileName, ChangeData);
end;

function TFmPrincipal.AbreArquivoINI(Grade: TStringGrid; EdNomCli, EdCPFCli: TEdit;
  Data: TDateTime; Arquivo: String): Boolean;
begin
  Result := False;
  //MyCBase.AbreArquivoINI(Grade, EdNomCli, EdCPFCli, Data, Arquivo);
end;

procedure TFmPrincipal.Chequesavulsos1Click(Sender: TObject);
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Application.CreateForm(TFmChequesExtras, FmChequesExtras);
    FmChequesExtras.ShowModal;
    FmChequesExtras.Destroy;
    Screen.Cursor := MyCursor
  except
    Screen.Cursor := MyCursor;
    raise;
  end;
end;

procedure TFmPrincipal.Button11Click(Sender: TObject);
begin
  // j� tem em: Ferramentas -> Atualiza clientes de itens de border�
  //AtualizaClientesLotesIts;
end;

procedure TFmPrincipal.Button13Click(Sender: TObject);
var
  Banda: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC7_1.Text) = 0 then
  begin
    Banda := TBandaMagnetica.Create;
    Banda.BandaMagnetica := EdCMC7_1.Text;
    Ed_Com.Text := Banda.Compe;
    Ed_Bco.Text := Banda.Banco;
    Ed_Age.Text := Banda.Agencia;
    Ed_Cta.Text := Banda.Conta;
    Ed_Chq.Text := Banda.Numero;
  end;
end;

procedure TFmPrincipal.Button14Click(Sender: TObject);
var
  CMC7: String;
begin
  if MLAgeral.GeraCMC7_30(Ed_Com.ValueVariant, Ed_Bco.ValueVariant,
    Ed_Age.ValueVariant, Ed_Cta.ValueVariant, Ed_Chq.ValueVariant,
    Ed_Tip.ValueVariant, CMC7) then
  EdCMC7_2.Text := CMC7;
end;

procedure TFmPrincipal.Button15Click(Sender: TObject);
var
  Tel1, DDD1: String;
begin
  Tel1 := Geral.SoNumero_TT(Edit4.Text);
  //
  if Length(Tel1) > 8 then
  begin
    DDD1 := Copy(Tel1, 1, Length(Tel1) - 8);
    Tel1 := Copy(Tel1, Length(Tel1) - 7, 8);
  end else DDD1 := '0';
  //
  Edit5.Text := DDD1;
  Edit6.Text := Tel1;
end;

procedure TFmPrincipal.Button1Click(Sender: TObject);
var
  Sit: Integer;
  Pago, Juros: Double;
begin
  Dmod.QrUpdM.Close;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('SELECT Data3, Codigo, Valor, TaxaV');
  Dmod.QrUpdM.SQL.Add('FROM ocorreu');
  Dmod.QrUpdM.SQL.Add('WHERE Data3 <> 0');
  Dmod.QrUpdM.SQL.Add('AND Pago <> 0');
  Dmod.QrUpdM.SQL.Add('AND Status = 0');
  Dmod.QrUpdM.SQL.Add('AND TaxaV = 0');
  Dmod.QrUpdM.SQL.Add('ORDER BY Cliente');
  Dmod.QrUpdM.Open;
  //
  ProgressBar3.Visible  := True;
  ProgressBar3.Max      := Dmod.QrUpdM.RecordCount;
  ProgressBar3.Position := 0;
  //
  if Dmod.QrUpdM.RecordCount > 0 then
  begin
    while not Dmod.QrUpdM.Eof do
    begin
      Dmod.QrUpdU.Close;
      Dmod.QrUpdU.SQL.Clear;
      Dmod.QrUpdU.SQL.Add('SELECT SUM(Juros) Juros, SUM(Pago) Pago');
      Dmod.QrUpdU.SQL.Add('FROM ocorrpg');
      Dmod.QrUpdU.SQL.Add('WHERE Ocorreu=:P0');
      Dmod.QrUpdU.Params[0].AsInteger := Dmod.QrUpdM.FieldByName('Codigo').Value;
      Dmod.QrUpdU.Open;
      if Dmod.QrUpdU.FieldByName('Pago').Value <> Null then
        Pago := Dmod.QrUpdU.FieldByName('Pago').Value
      else
        Pago := 0;
      if Dmod.QrUpdU.FieldByName('Juros').Value then
        Juros := Dmod.QrUpdU.FieldByName('Juros').Value
      else
        Juros := 0;
      if Pago < Dmod.QrUpdM.FieldByName('Valor').Value +
        Dmod.QrUpdM.FieldByName('TaxaV').Value -0.009
      then
        Sit := 1
      else
        Sit := 2;
      if Pago <> 0 then
      begin
        if (Pago < (Dmod.QrUpdM.FieldByName('Valor').Value + Dmod.QrUpdM.FieldByName('TaxaV').Value -0.009)) then
          Sit := 1
        else
          Sit := 2;
      end;
      //
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ocorreu SET AlterWeb=1, ');
      Dmod.QrUpd.SQL.Add('TaxaV=:P0, Pago=:P1, Status=:P2, ');
      Dmod.QrUpd.SQL.Add('Data3=:P3 WHERE Codigo=:Pa');
      //
      Dmod.QrUpd.Params[00].AsFloat   := Juros;
      Dmod.QrUpd.Params[01].AsFloat   := Pago;
      Dmod.QrUpd.Params[02].AsInteger := Sit;
      Dmod.QrUpd.Params[03].AsString  := Dmod.QrUpdM.FieldByName('Data3').Value;
      Dmod.QrUpd.Params[04].AsInteger := Dmod.QrUpdM.FieldByName('Codigo').Value;
      Dmod.QrUpd.ExecSQL;
      //
      ProgressBar3.Position := ProgressBar3.Position + 1;
      ProgressBar3.Update;
      Application.ProcessMessages;
      //
      Dmod.QrUpdM.Next;
    end;
  end;
  Dmod.QrUpd.Close;
  Dmod.QrUpdU.Close;
  Dmod.QrUpdM.Close;
end;

procedure TFmPrincipal.AtualizaClientesLotesIts;
begin
  Screen.Cursor := crHourGlass;
  QrUpdCli1.ExecSQL;
  QrUpdCli2.ExecSQL;
  Screen.Cursor := crDefault;
  ShowMessage('Atualiza��o concluida!');
end;

procedure TFmPrincipal.Novorestauraorpida1Click(Sender: TObject);
begin
  MostraBackup1;
end;

procedure TFmPrincipal.NovoRetornoeenvio1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAB_Cfg, FmCNAB_Cfg, afmoNegarComAviso) then
  begin
    FmCNAB_Cfg.ShowModal;
    FmCNAB_Cfg.Destroy;
  end;
end;

procedure TFmPrincipal.Nvel2Subgrupos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSubGrupos, FmSubGrupos, afmoNegarComAviso) then
  begin
    FmSubGrupos.ShowModal;
    FmSubGrupos.Destroy;
  end;
end;

procedure TFmPrincipal.Nvel3Grupos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGrupos, FmGrupos, afmoNegarComAviso) then
  begin
    FmGrupos.ShowModal;
    FmGrupos.Destroy;
  end;
end;

procedure TFmPrincipal.Nvel4Conjuntos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmConjuntos, FmConjuntos, afmoNegarComAviso) then
  begin
    FmConjuntos.ShowModal;
    FmConjuntos.Destroy;
  end;
end;

procedure TFmPrincipal.Modalidadesdeconsultas1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmSPC_Modali, FmSPC_Modali, afmoNegarComAviso) then
  begin
    FmSPC_Modali.ShowModal;
    FmSPC_Modali.Destroy;
  end;
end;

procedure TFmPrincipal.MostraBackup1;
begin
  DModG.MostraBackup3();
  {VAR_CURSOR := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Application.CreateForm(TFmBackup3, FmBackup3);
  finally
    Screen.Cursor := VAR_CURSOR;
  end;
  FmBackup3.ShowModal;
  FmBackup3.Destroy;}
end;

procedure TFmPrincipal.CNAB240Remessa;
begin
  Application.CreateForm(TFmCNAB240Rem, FmCNAB240Rem);
  FmCNAB240Rem.ShowModal;
  FmCNAB240Rem.Destroy;
end;

procedure TFmPrincipal.RemessaCNAB2401Click(Sender: TObject);
begin
  CNAB240Remessa;
end;

procedure TFmPrincipal.CNAB240Retorno;
begin
  Application.CreateForm(TFmCNAB240Ret, FmCNAB240Ret);
  FmCNAB240Ret.ShowModal;
  FmCNAB240Ret.Destroy;
end;

procedure TFmPrincipal.DiretriosCNAB2401Click(Sender: TObject);
begin
  Application.CreateForm(TFmCNAB240Dir, FmCNAB240Dir);
  FmCNAB240Dir.ShowModal;
  FmCNAB240Dir.Destroy;
end;

procedure TFmPrincipal.Diretriosderetorno1Click(Sender: TObject);
begin
  Application.CreateForm(TFmCNAB240Dir, FmCNAB240Dir);
  FmCNAB240Dir.ShowModal;
  FmCNAB240Dir.Destroy;
end;

procedure TFmPrincipal.Manual1Click(Sender: TObject);
begin
  RetornoBB;
end;

procedure TFmPrincipal.Matriz1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMatriz, FmMatriz, afmoSoBoss) then
  begin
    FmMatriz.ShowModal;
    FmMatriz.Destroy;
  end;
end;

procedure TFmPrincipal.MenuItem5Click(Sender: TObject);
begin
  Application.CreateForm(TFmLotesDep, FmLotesDep);
  FmLotesDep.ShowModal;
  FmLotesDep.Destroy;
end;

procedure TFmPrincipal.MenuItem6Click(Sender: TObject);
begin
  Application.CreateForm(TFmPesqDep, FmPesqDep);
  FmPesqDep.ShowModal;
  FmPesqDep.Destroy;
end;

procedure TFmPrincipal.Automtico1Click(Sender: TObject);
begin
  CNAB240Retorno;
end;

procedure TFmPrincipal.RemessaCNAB2402Click(Sender: TObject);
begin
  CNAB240Remessa;
end;

procedure TFmPrincipal.RetornoCNAB2401Click(Sender: TObject);
begin
  CNAB240Retorno;
end;

procedure TFmPrincipal.RetornoCNABNovo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAB_Ret2, FmCNAB_Ret2, afmoNegarComAviso) then
  begin
    FmCNAB_Ret2.ShowModal;
    FmCNAB_Ret2.Destroy;
  end;
end;

procedure TFmPrincipal.RetornoNovo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCNAB_Ret2, FmCNAB_Ret2, afmoNegarComAviso) then
  begin
    FmCNAB_Ret2.ShowModal;
    FmCNAB_Ret2.Destroy;
  end;
end;

procedure TFmPrincipal.Remessa1Click(Sender: TObject);
begin
  CobrancaBB;
end;

procedure TFmPrincipal.Remessa240400Novo1Click(Sender: TObject);
begin
  MostraRemessaCNABNovo(0, 0);
end;

procedure TFmPrincipal.Verificaremoto1Click(Sender: TObject);
begin
  {
  Application.CreateForm(TFmFTPLotes, FmFTPLotes);
  FmFTPLotes.FTipoConexao := 1;
  FmFTPLotes.PnCliente.Visible := False;
  FmFTPLotes.PnPHP.Visible := True;
  FmFTPLotes.PnConfirm.Visible := True;
  FmFTPLotes.BtAtualiza.Enabled := True;
  FmFTPLotes.ShowModal;
  FmFTPLotes.Destroy;
  }
end;

procedure TFmPrincipal.Clientes1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmwclients, Fmwclients, afmoNegarComAviso) then
  begin
    Fmwclients.ShowModal;
    Fmwclients.Destroy;
  end;
end;

procedure TFmPrincipal.Prprio1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmVerifiDB, FmVerifiDB, afmoNegarComAviso) then
  begin
    FmVerifiDB.ShowModal;
    FmVerifiDB.Destroy;
  end;
end;

procedure TFmPrincipal.Terceiros1Click(Sender: TObject);
begin
  VerificaBDsTerceiros();
end;

procedure TFmPrincipal.VCLSkin1Click(Sender: TObject);
begin
  FmLinkRankSkin.Show;
end;

procedure TFmPrincipal.VerificaBDsTerceiros();
var
  i: Integer;
begin
  FConnections := 0;
  for i := 0 to FMyDBs.MaxDBs-1 do
  begin
    //t := Geral.FF0(i+1);
    if (FMyDBs.BDName[i] <> '') and (FMyDBs.FolderExist[i] = '1') then
    begin
      //MyDBx := TmySQLDatabase(FindComponent('MyDB'+t));
      //FArrayMySQLBD[i] := MyDB1;

      Dmod.FArrayMySQLBD[i].DatabaseName := FMyDBs.BDName[i];
      Dmod.FArrayMySQLBD[i].Host         := Dmod.MyDB.Host;
      Dmod.FArrayMySQLBD[i].UserPassword := Dmod.MyDB.UserPassword;
      Dmod.FArrayMySQLBD[i].Connected    := False;
      try
        Dmod.FArrayMySQLBD[i].Connected := True;
        FMyDBs.Connected[i] := '1';
        FConnections := FConnections +1;
      except
        ;
      end;
      if FMyDBs.Connected[i] = '1' then
      begin
        Application.CreateForm(TFmVerifiDBy, FmVerifiDBy);
        with FmVerifiDBy do
        begin
          FMySQLDB := Dmod.FArrayMySQLBD[i];
          BtSair.Enabled := False;
          FVerifi := True;
          ShowModal;
          FVerifi := False;
          Destroy;
        end;
      end;
    end;
  end;
end;

procedure TFmPrincipal.Web2Click(Sender: TObject);
begin
  if Dmod.QrControleWeb_MySQL.Value > 0 then
  begin
    Application.CreateForm(TFmVerifiDBi, FmVerifiDBi);
    FmVerifiDBi.ShowModal;
    FmVerifiDBi.Destroy;
  end;
end;

procedure TFmPrincipal.Verificanovoslotes1Click(Sender: TObject);
begin
  Dmoc.BaixaLotesWeb;
end;

procedure TFmPrincipal.Sincronizar1Click(Sender: TObject);
begin
  (*
  Application.CreateForm(TFmWSincro, FmWSincro);
  FmWSincro.ShowModal;
  FmWSincro.Destroy;
  *)
end;

procedure TFmPrincipal.Sobre1Click(Sender: TObject);
begin
  Application.CreateForm(TFmAbout, FmAbout);
  FmAbout.ShowModal;
  FmAbout.Destroy;
end;

procedure TFmPrincipal.SuporteRemoto1Click(Sender: TObject);
begin
  dmkWeb.AbrirAppAcessoRemoto;
end;

procedure TFmPrincipal.Antigo2Click(Sender: TObject);
begin
  Application.CreateForm(TFmEvolucapi, FmEvolucapi);
  FmEvolucapi.ShowModal;
  FmEvolucapi.Destroy;
end;

procedure TFmPrincipal.Novo1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Application.CreateForm(TFmRiscoAll_1, FmRiscoAll_1);
  FmRiscoAll_1.ShowModal;
  FmRiscoAll_1.Destroy;
  Screen.Cursor := crDefault;
end;

procedure TFmPrincipal.NovoEmconstruo1Click(Sender: TObject);
begin
  Application.CreateForm(TFmEvolucap2, FmEvolucap2);
  FmEvolucap2.ShowModal;
  FmEvolucap2.Destroy;
end;

procedure TFmPrincipal.Importar1Click(Sender: TObject);
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Application.CreateForm(TFmLSincro, FmLSincro);
    FmLSincro.ShowModal;
    FmLSincro.Destroy;
    Screen.Cursor := MyCursor
  except
    Screen.Cursor := MyCursor;
    raise;
  end;
end;

procedure TFmPrincipal.Exportaemitentesesacados1Click(Sender: TObject);
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Application.CreateForm(TFmEmitSacExport, FmEmitSacExport);
    FmEmitSacExport.ShowModal;
    FmEmitSacExport.Destroy;
    Screen.Cursor := MyCursor
  except
    Screen.Cursor := MyCursor;
    raise;
  end;
end;

procedure TFmPrincipal.ImportaEmitentes1Click(Sender: TObject);
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Application.CreateForm(TFmEmitSacImport, FmEmitSacImport);
    FmEmitSacImport.ShowModal;
    FmEmitSacImport.Destroy;
    Screen.Cursor := MyCursor
  except
    Screen.Cursor := MyCursor;
    raise;
  end;
end;

procedure TFmPrincipal.Importaemitentes2Click(Sender: TObject);
begin
  ImportaEmitentes1Click(Self);
end;

procedure TFmPrincipal.Atualizaclientesdeitensdeborder1Click(
  Sender: TObject);
begin
  AtualizaClientesLotesIts;
end;

procedure TFmPrincipal.Timer5Timer(Sender: TObject);
begin
  (*if (Dmod.QrControle.State = dsBrowse) and (Dmod.MyDBn.Connected) then
  begin
    Dmoc.Qrwlc.Close;
    Dmoc.Qrwlc.Open;
    if Dmoc.Qrwlc.RecordCount > 0 then
    case Dmoc.Qrwlc.RecordCount of
      0:
        ; // Nada
      1:
        Geral.MB_Aviso('H� um novo lote para baixar na web!');
      else
        Geral.MB_Aviso('H� ' + Geral.FF0(Dmoc.Qrwlc.RecordCount) +
          ' novos lotes para baixar na web!');
    end;
  end; *)
end;

function GetWindows(Handle: HWND; Info: Pointer): Boolean; stdcall;
begin
  Result := True;
  WindowList1.Add(Pointer(Handle));
end;

procedure TFmPrincipal.Exportaemitentes1Click(Sender: TObject);
begin
  Exportaemitentesesacados1Click(Self);
end;

procedure TFmPrincipal.ltimocontroledeLLoteIts1Click(Sender: TObject);
var
  Controle: Integer;
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT MAX(Controle) Controle FROM lloteits');
  Dmod.QrAux.Open;
  Controle := Dmod.QrAux.FieldByName('Controle').AsInteger;
  //
  Dmoc.QrCtrl.Close;
  Dmoc.QrCtrl.Params[0].AsInteger := Controle;
  Dmoc.QrCtrl.Open;
  if Dmoc.QrCtrl.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta('Foram localizados ' +
      Geral.FF0(Dmoc.QrCtrl.RecordCount) + ' itens de lote na web com o ' +
      'n�mero de controle inv�lido. Deseja atualiz�-los?') = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      Dmod.QrWeb.Close;
      Dmod.QrWeb.SQL.Clear;
      Dmod.QrWeb.SQL.Add('UPDATE wloteits ');
      Dmod.QrWeb.SQL.Add('SET Controle=:P0');
      Dmod.QrWeb.SQL.Add('WHERE Controle=:P1');
      Dmod.QrWeb.SQL.Add('AND Codigo=:P2');
      //
      Dmoc.QrCtrl.First;
      while not Dmoc.QrCtrl.Eof do
      begin
        Controle := Controle + 1;
        Dmod.QrWeb.Params[00].AsInteger := Controle;
        Dmod.QrWeb.Params[01].AsInteger := Dmoc.QrCtrlControle.Value;
        Dmod.QrWeb.Params[02].AsInteger := Dmoc.QrCtrlCodigo.Value;
        Dmod.QrWeb.ExecSQL;
        //
        Dmoc.QrCtrl.Next;
      end;
      Screen.Cursor := crDefault;
      Geral.MB_Aviso('Atualiza��o finalizada!');
    end;
  end;
end;

procedure TFmPrincipal.RetornoCNAB;
begin
  // Compatibilidade
end;

//fazer ocorerncias por cliente no retorno de arquivo!
procedure TFmPrincipal.frxChequeSimplesGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_VAL' then Value := '$$ '+FImpCHSimVal+' $$'
  else if VarName = 'VARF_EXT' then Value := FImpCHSimExt
  else if VarName = 'VARF_ENT' then Value := FImpCHSimEnt
  else if VarName = 'VARF_CID' then Value := FImpCHSimCid
  else if VarName = 'VARF_DIA' then Value := FImpCHSimDia
  else if VarName = 'VARF_MES' then Value := FImpCHSimMes
  else if VarName = 'VARF_ANO' then Value := FImpCHSimAno
end;

procedure TFmPrincipal.Risco1Click(Sender: TObject);
begin
  BtRiscoAllClick(Self);
end;

{ TODO : Trocar todos Application.MessageBox }
{ TODO : ver todos "MLAGeral.TFD2(" }

end.

