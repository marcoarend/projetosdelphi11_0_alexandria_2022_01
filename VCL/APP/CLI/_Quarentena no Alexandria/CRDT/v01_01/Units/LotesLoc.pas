unit LotesLoc;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, Grids, DBGrids,
  ComCtrls, dmkEdit, dmkEditCB, dmkDBLookupComboBox, dmkGeral, UnDmkProcFunc;

type
  TFmLotesLoc = class(TForm)
    PainelDados: TPanel;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label1: TLabel;
    Label2: TLabel;
    CBCliente: TdmkDBLookupComboBox;
    DsClientes: TDataSource;
    EdCliente: TdmkEditCB;
    EdLote: TdmkEdit;
    Label3: TLabel;
    QrClientes: TmySQLQuery;
    QrClientesNOMEENTIDADE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    QrLoc: TmySQLQuery;
    DBGrid1: TDBGrid;
    DsLoc: TDataSource;
    QrLocCodigo: TIntegerField;
    QrLocLote: TSmallintField;
    QrLocData: TDateField;
    QrLocTipo: TSmallintField;
    QrLocNOMETIPO: TWideStringField;
    QrLocTotal: TFloatField;
    QrLocNOMECLIENTE: TWideStringField;
    QrLocCNPJCPF: TWideStringField;
    QrLocSpread: TSmallintField;
    QrLocCliente: TIntegerField;
    QrLocDias: TFloatField;
    QrLocPeCompra: TFloatField;
    QrLocTxCompra: TFloatField;
    QrLocValValorem: TFloatField;
    QrLocAdValorem: TFloatField;
    QrLocIOC: TFloatField;
    QrLocIOC_VAL: TFloatField;
    QrLocTarifas: TFloatField;
    QrLocCPMF: TFloatField;
    QrLocCPMF_VAL: TFloatField;
    QrLocTipoAdV: TIntegerField;
    QrLocIRRF: TFloatField;
    QrLocIRRF_Val: TFloatField;
    QrLocISS: TFloatField;
    QrLocISS_Val: TFloatField;
    QrLocPIS: TFloatField;
    QrLocPIS_Val: TFloatField;
    QrLocPIS_R: TFloatField;
    QrLocPIS_R_Val: TFloatField;
    QrLocCOFINS: TFloatField;
    QrLocCOFINS_Val: TFloatField;
    QrLocCOFINS_R: TFloatField;
    QrLocCOFINS_R_Val: TFloatField;
    QrLocOcorP: TFloatField;
    QrLocMaxVencto: TDateField;
    QrLocCHDevPg: TFloatField;
    QrLocLk: TIntegerField;
    QrLocDataCad: TDateField;
    QrLocDataAlt: TDateField;
    QrLocUserCad: TIntegerField;
    QrLocUserAlt: TIntegerField;
    QrLocMINTC: TFloatField;
    QrLocMINAV: TFloatField;
    QrLocMINTC_AM: TFloatField;
    QrLocPIS_T_Val: TFloatField;
    QrLocCOFINS_T_Val: TFloatField;
    QrLocPgLiq: TFloatField;
    QrLocDUDevPg: TFloatField;
    QrLocNF: TIntegerField;
    TPIni: TDateTimePicker;
    Label34: TLabel;
    TPFim: TDateTimePicker;
    Label4: TLabel;
    QrLocItens: TIntegerField;
    QrLocConferido: TIntegerField;
    QrLocECartaSac: TSmallintField;
    EdControle: TdmkEdit;
    Label5: TLabel;
    Label6: TLabel;
    QrCon: TmySQLQuery;
    QrConCodigo: TIntegerField;
    procedure BtDesisteClick(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdLoteExit(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure QrLocAfterOpen(DataSet: TDataSet);
    procedure BtConfirmaClick(Sender: TObject);
    procedure EdLoteChange(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure QrLocCalcFields(DataSet: TDataSet);
    procedure TPIniClick(Sender: TObject);
    procedure TPFimClick(Sender: TObject);
    procedure EdControleChange(Sender: TObject);
  private
    { Private declarations }
    procedure LocalizaBordero(Lote: Integer);
  public
    { Public declarations }
    FNF, FFormCall: Integer;
    procedure ReopenLoc;
  end;

var
  FmLotesLoc: TFmLotesLoc;

implementation

uses Module, Entidades, Lotes1, Principal, UnMyObjects;

{$R *.DFM}

procedure TFmLotesLoc.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLotesLoc.EdClienteChange(Sender: TObject);
begin
  ReopenLoc;
end;

procedure TFmLotesLoc.ReopenLoc;
var
  Cliente, Controle: Integer;
begin
  Controle := Geral.IMV(EdControle.Text);
  Cliente := Geral.IMV(EdCliente.Text);
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrLoc.SQL.Add('ELSE en.Nome END NOMECLIENTE,');
  QrLoc.SQL.Add('CASE WHEN en.Tipo=0 THEN en.CNPJ');
  QrLoc.SQL.Add('ELSE en.CPF END CNPJCPF, lo.*');
  QrLoc.SQL.Add('FROM lotes lo');
  QrLoc.SQL.Add('LEFT JOIN entidades en ON en.Codigo=lo.Cliente');
  QrLoc.SQL.Add(dmkPF.SQL_Periodo('WHERE lo.Data ',
    TPIni.Date, TPFim.Date, True, True));
  if Cliente <> 0 then
    QrLoc.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
  if Controle <> 0 then
  begin
    QrCon.Close;
    QrCon.Params[0].AsInteger := Controle;
    QrCon.Open;
    //
    if QrConCodigo.Value > 0 then
      QrLoc.SQL.Add('AND lo.Codigo='+IntToStr(QrConCodigo.Value));
  end;
  if FFormCall = 1 then
  begin
    QrLoc.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
    QrLoc.SQL.Add('ORDER BY lo.Lote DESC');
  end else if FFormCall = 2 then
  begin
    QrLoc.SQL.Add('AND lo.TxCompra+lo.ValValorem >= 0.01');
    QrLoc.SQL.Add('AND lo.NF = 0');
    QrLoc.SQL.Add('ORDER BY lo.Data');
  end else if FFormCall = 3 then
  begin
    QrLoc.SQL.Add('AND lo.TxCompra+lo.ValValorem >= 0.01');
    QrLoc.SQL.Add('AND lo.NF <> 0');
    QrLoc.SQL.Add('ORDER BY lo.Data');
  end;
  QrLoc.Open;
  //
  LocalizaBordero(Geral.IMV(EdLote.Text));
end;

procedure TFmLotesLoc.FormCreate(Sender: TObject);
begin
  QrClientes.Open;
  TPIni.Date := Date - 365;
  TPFim.Date := Date;
end;

procedure TFmLotesLoc.EdLoteExit(Sender: TObject);
begin
  LocalizaBordero(Geral.IMV(EdLote.Text));
end;

procedure TFmLotesLoc.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1,PainelTitulo, True,0);
end;

procedure TFmLotesLoc.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmLotesLoc.QrLocAfterOpen(DataSet: TDataSet);
begin
  BtConfirma.Enabled := Geral.IntToBool_0(QrLoc.RecordCount);
end;

procedure TFmLotesLoc.BtConfirmaClick(Sender: TObject);
begin
  case FFormCall of
    1:
    begin
      FmPrincipal.FLoteLoc := QrLocCodigo.Value;
      Close;
    end;
    2:
    begin
      FmPrincipal.AdicionaBorderoANF(FNF, QrLocCodigo.Value);
      ReopenLoc;
    end;
    3:
    begin
      FmPrincipal.FLoteLoc := QrLocNF.Value;
      Close;
    end;
    else FmPrincipal.FLoteLoc := 0;
  end;
end;

procedure TFmLotesLoc.LocalizaBordero(Lote: Integer);
begin
  if QrLoc.State = dsBrowse then
    if QrLoc.RecordCount > 0 then
      if Lote > 0 then QrLoc.Locate('Lote', Lote, []);
end;

procedure TFmLotesLoc.EdLoteChange(Sender: TObject);
begin
  LocalizaBordero(Geral.IMV(EdLote.Text));
  ReopenLoc;
end;

procedure TFmLotesLoc.DBGrid1DblClick(Sender: TObject);
begin
  BtConfirmaClick(Self);
end;

procedure TFmLotesLoc.QrLocCalcFields(DataSet: TDataSet);
begin
  case QrLocTipo.Value of
    0: QrLocNOMETIPO.Value := 'Cheques';
    1: QrLocNOMETIPO.Value := 'Duplicatas';
    else QrLocNOMETIPO.Value := '? ? ?';
  end;
end;

procedure TFmLotesLoc.TPIniClick(Sender: TObject);
begin
  ReopenLoc;
end;

procedure TFmLotesLoc.TPFimClick(Sender: TObject);
begin
  ReopenLoc;
end;

procedure TFmLotesLoc.EdControleChange(Sender: TObject);
begin
  ReopenLoc;
end;

end.

