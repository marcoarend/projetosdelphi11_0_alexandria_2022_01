object FmConfigBB: TFmConfigBB
  Left = 329
  Top = 161
  Caption = 'Configura'#231#227'o de Cobran'#231'a'
  ClientHeight = 592
  ClientWidth = 816
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Label14: TLabel
    Left = 16
    Top = 140
    Width = 33
    Height = 13
    Caption = 'Tam 2:'
  end
  object Label15: TLabel
    Left = 16
    Top = 184
    Width = 33
    Height = 13
    Caption = 'Tam 3:'
  end
  object Label16: TLabel
    Left = 16
    Top = 228
    Width = 33
    Height = 13
    Caption = 'Tam 4:'
  end
  object Label63: TLabel
    Left = 172
    Top = 124
    Width = 88
    Height = 13
    Caption = 'Reservado banco:'
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 816
    Height = 544
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 814
      Height = 464
      Align = alTop
      TabOrder = 0
      object PageControl2: TPageControl
        Left = 1
        Top = 45
        Width = 812
        Height = 418
        ActivePage = TabSheet4
        Align = alClient
        TabOrder = 1
        object TabSheet3: TTabSheet
          Caption = 'Dados 1 '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel8: TPanel
            Left = 0
            Top = 0
            Width = 804
            Height = 390
            Align = alClient
            TabOrder = 0
            object Label19: TLabel
              Left = 4
              Top = 200
              Width = 42
              Height = 13
              Caption = 'Diret'#243'rio:'
              FocusControl = DBEdit6
            end
            object GroupBox1: TGroupBox
              Left = 4
              Top = 4
              Width = 372
              Height = 173
              Caption = ' Sigla da esp'#233'cie: '
              TabOrder = 0
              object DBEdit14: TDBEdit
                Left = 12
                Top = 20
                Width = 77
                Height = 21
                DataField = 'SiglaEspecie'
                DataSource = DsConfigBB
                TabOrder = 0
              end
            end
            object DBRadioGroup1: TDBRadioGroup
              Left = 380
              Top = 4
              Width = 169
              Height = 128
              Caption = ' Moeda: '
              DataField = 'Moeda'
              DataSource = DsConfigBB
              Items.Strings = (
                '02   US$'
                '05   IDTR'
                '07   UFIR'
                '08   FAJTR'
                '09   R$')
              ParentBackground = True
              TabOrder = 1
              Values.Strings = (
                '02'
                '05'
                '07'
                '08'
                '09')
            end
            object DBRadioGroup2: TDBRadioGroup
              Left = 380
              Top = 136
              Width = 169
              Height = 40
              Caption = ' Aceite: '
              Columns = 2
              DataField = 'Aceite'
              DataSource = DsConfigBB
              Items.Strings = (
                'N'#227'o'
                'Sim')
              ParentBackground = True
              TabOrder = 2
              Values.Strings = (
                '0'
                '1')
            end
            object DBCheckBox1: TDBCheckBox
              Left = 4
              Top = 180
              Width = 777
              Height = 17
              Caption = 'Permitir pagar antecipadamente com desconto at'#233' uma data limite.'
              DataField = 'PgAntes'
              DataSource = DsConfigBB
              TabOrder = 3
              ValueChecked = '1'
              ValueUnchecked = '0'
            end
            object DBEdit6: TDBEdit
              Left = 4
              Top = 216
              Width = 789
              Height = 21
              DataField = 'Diretorio'
              DataSource = DsConfigBB
              TabOrder = 4
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Dados 2 '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 804
            Height = 390
            Align = alClient
            TabOrder = 0
            object Label30: TLabel
              Left = 4
              Top = 4
              Width = 52
              Height = 13
              Caption = 'Instru'#231#245'es:'
              Visible = False
            end
            object Label3: TLabel
              Left = 396
              Top = 4
              Width = 48
              Height = 13
              Caption = 'Conv'#234'nio:'
              FocusControl = DBEdit1
            end
            object Label4: TLabel
              Left = 504
              Top = 4
              Width = 39
              Height = 13
              Caption = 'Carteira:'
              FocusControl = DBEdit2
            end
            object Label5: TLabel
              Left = 564
              Top = 4
              Width = 45
              Height = 13
              Caption = 'Varia'#231#227'o:'
              FocusControl = DBEdit3
            end
            object Label51: TLabel
              Left = 616
              Top = 4
              Width = 64
              Height = 13
              Caption = 'C'#243'd. produto:'
            end
            object DBEdit15: TDBEdit
              Left = 4
              Top = 20
              Width = 389
              Height = 21
              DataField = 'MsgLinha1'
              DataSource = DsConfigBB
              TabOrder = 0
              Visible = False
            end
            object DBEdit1: TDBEdit
              Left = 396
              Top = 20
              Width = 105
              Height = 21
              DataField = 'Convenio'
              DataSource = DsConfigBB
              TabOrder = 1
            end
            object DBEdit2: TDBEdit
              Left = 504
              Top = 20
              Width = 56
              Height = 21
              DataField = 'Carteira'
              DataSource = DsConfigBB
              TabOrder = 2
            end
            object DBEdit3: TDBEdit
              Left = 564
              Top = 20
              Width = 48
              Height = 21
              DataField = 'Variacao'
              DataSource = DsConfigBB
              TabOrder = 3
            end
            object GroupBox4: TGroupBox
              Left = 4
              Top = 44
              Width = 333
              Height = 105
              Caption = ' Multa padr'#227'o para clientes sem configura'#231#227'o: '
              TabOrder = 4
              object Label35: TLabel
                Left = 156
                Top = 12
                Width = 46
                Height = 13
                Caption = 'Valor fixo:'
              end
              object Label36: TLabel
                Left = 248
                Top = 12
                Width = 66
                Height = 13
                Caption = 'Porcentagem:'
              end
              object Label37: TLabel
                Left = 156
                Top = 60
                Width = 56
                Height = 13
                Caption = 'Dias ap'#243's..:'
              end
              object DBRadioGroup3: TDBRadioGroup
                Left = 2
                Top = 15
                Width = 147
                Height = 88
                Align = alLeft
                Caption = ' C'#243'digo da Multa: '
                DataField = 'MultaCodi'
                DataSource = DsConfigBB
                Items.Strings = (
                  'Valor calculado pela %'
                  'Valor fixo (Valor fixo)'
                  'Porcentagem')
                ParentBackground = True
                TabOrder = 0
                Values.Strings = (
                  '0'
                  '1'
                  '2'
                  '3')
              end
              object DBEdit16: TDBEdit
                Left = 156
                Top = 28
                Width = 88
                Height = 21
                DataField = 'MultaValr'
                DataSource = DsConfigBB
                TabOrder = 1
              end
              object DBEdit17: TDBEdit
                Left = 248
                Top = 28
                Width = 80
                Height = 21
                DataField = 'MultaPerc'
                DataSource = DsConfigBB
                TabOrder = 2
              end
              object DBEdit18: TDBEdit
                Left = 156
                Top = 76
                Width = 64
                Height = 21
                DataField = 'MultaDias'
                DataSource = DsConfigBB
                TabOrder = 3
              end
              object DBRadioGroup4: TDBRadioGroup
                Left = 224
                Top = 52
                Width = 105
                Height = 51
                Caption = ' ... Qual data?: '
                DataField = 'MultaTiVe'
                DataSource = DsConfigBB
                Items.Strings = (
                  'Vencto doc.'
                  'Dia '#250'til vencto.')
                ParentBackground = True
                TabOrder = 4
                Values.Strings = (
                  '0'
                  '1')
              end
            end
            object DBRadioGroup5: TDBRadioGroup
              Left = 340
              Top = 44
              Width = 200
              Height = 105
              Caption = ' Pendente de impress'#227'o local?: '
              DataField = 'ImpreLoc'
              DataSource = DsConfigBB
              Items.Strings = (
                'A ser impresso pelo BB Cobran'#231'a.'
                'J'#225' impresso.'
                'N'#227'o '#233' carteira 15, 16, 17 ou 18.')
              ParentBackground = True
              TabOrder = 5
              Values.Strings = (
                '0'
                '1'
                '2')
            end
            object DBRGModalidade: TDBRadioGroup
              Left = 4
              Top = 152
              Width = 773
              Height = 140
              Caption = ' Tipo da modalidade do t'#237'tulo: '
              DataField = 'Modalidade'
              DataSource = DsConfigBB
              Items.Strings = (
                
                  '01 para t'#237'tulo simples (sem registro quando for carteira 16 ou 1' +
                  '8 e com registro quando for carteira 11,12 ou 15) '
                '02 para t'#237'tulo vinculado (quando for carteira 31) '
                '04 para t'#237'tulo descontado (quando for carteira 11,17 ou 51) '
                '07 para t'#237'tulo simples (com registro quando for carteira 17) '
                
                  '08 para t'#237'tulo BB Vendor (com registro quando for carteira 11 ou' +
                  ' 17)')
              ParentBackground = True
              TabOrder = 6
              Values.Strings = (
                '1'
                '2'
                '4'
                '7'
                '8')
            end
            object GroupBox6: TGroupBox
              Left = 544
              Top = 44
              Width = 229
              Height = 105
              Caption = ' Convenente l'#237'der do cedente: '
              TabOrder = 7
              object Label38: TLabel
                Left = 16
                Top = 40
                Width = 42
                Height = 13
                Caption = 'Ag'#234'ncia:'
              end
              object Label39: TLabel
                Left = 64
                Top = 40
                Width = 18
                Height = 13
                Caption = 'DV:'
              end
              object Label40: TLabel
                Left = 104
                Top = 40
                Width = 73
                Height = 13
                Caption = 'Conta corrente:'
              end
              object Label41: TLabel
                Left = 180
                Top = 40
                Width = 18
                Height = 13
                Caption = 'DV:'
              end
              object DBEdit4: TDBEdit
                Left = 16
                Top = 56
                Width = 44
                Height = 21
                DataField = 'clcAgencNr'
                DataSource = DsConfigBB
                TabOrder = 0
              end
              object DBEdit5: TDBEdit
                Left = 64
                Top = 56
                Width = 24
                Height = 21
                DataField = 'clcAgencDV'
                DataSource = DsConfigBB
                TabOrder = 1
              end
              object DBEdit19: TDBEdit
                Left = 104
                Top = 56
                Width = 72
                Height = 21
                DataField = 'clcContaNr'
                DataSource = DsConfigBB
                TabOrder = 2
              end
              object DBEdit20: TDBEdit
                Left = 180
                Top = 56
                Width = 24
                Height = 21
                DataField = 'clcContaDV'
                DataSource = DsConfigBB
                TabOrder = 3
              end
            end
            object GroupBox8: TGroupBox
              Left = 548
              Top = 188
              Width = 229
              Height = 104
              Caption = ' Cedente: '
              TabOrder = 8
              object Label46: TLabel
                Left = 16
                Top = 40
                Width = 42
                Height = 13
                Caption = 'Ag'#234'ncia:'
              end
              object Label47: TLabel
                Left = 64
                Top = 40
                Width = 18
                Height = 13
                Caption = 'DV:'
              end
              object Label48: TLabel
                Left = 104
                Top = 40
                Width = 73
                Height = 13
                Caption = 'Conta corrente:'
              end
              object Label49: TLabel
                Left = 180
                Top = 40
                Width = 18
                Height = 13
                Caption = 'DV:'
              end
              object DBEdit21: TDBEdit
                Left = 16
                Top = 56
                Width = 44
                Height = 21
                DataField = 'cedAgencNr'
                DataSource = DsConfigBB
                TabOrder = 0
              end
              object DBEdit22: TDBEdit
                Left = 64
                Top = 56
                Width = 24
                Height = 21
                DataField = 'cedAgencDV'
                DataSource = DsConfigBB
                TabOrder = 1
              end
              object DBEdit23: TDBEdit
                Left = 104
                Top = 56
                Width = 72
                Height = 21
                DataField = 'cedContaNr'
                DataSource = DsConfigBB
                TabOrder = 2
              end
              object DBEdit24: TDBEdit
                Left = 180
                Top = 56
                Width = 24
                Height = 21
                DataField = 'cedContaDV'
                DataSource = DsConfigBB
                TabOrder = 3
              end
            end
            object DBEdit26: TDBEdit
              Left = 616
              Top = 20
              Width = 64
              Height = 21
              DataField = 'Produto'
              DataSource = DsConfigBB
              TabOrder = 9
            end
          end
        end
        object TabSheet6: TTabSheet
          Caption = 'Dados 3 '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBRGEspecie: TDBRadioGroup
            Left = 0
            Top = 0
            Width = 172
            Height = 390
            Align = alLeft
            Caption = ' Esp'#233'cie de t'#237'tulo: '
            DataField = 'Especie'
            DataSource = DsConfigBB
            Items.Strings = (
              '01-Duplicata Mercantil'
              '02-Nota Promiss'#243'ria'
              '03-Nota de Seguro'
              '05-Recibo'
              '08-Letra de C'#226'mbio'
              '09-Warrant'
              '10-Cheque'
              '12-Duplicata de Servi'#231'o'
              '13-Nota de D'#233'bito'
              '15-Ap'#243'lice de Seguro'
              '21-Duplicata Rural'
              '25-D'#237'vida Ativa da Uni'#227'o    '
              '26-D'#237'vida Ativa de Estado   '
              '27-D'#237'vida Ativa de Munic'#237'pio')
            ParentBackground = True
            TabOrder = 0
            Values.Strings = (
              '1'
              '2'
              '3'
              '5'
              '8'
              '9'
              '10'
              '12'
              '13'
              '15'
              '21'
              '25'
              '26'
              '27')
          end
          object DBRGProtestar: TDBRadioGroup
            Left = 172
            Top = 0
            Width = 401
            Height = 390
            Align = alLeft
            Caption = ' Primeira instru'#231#227'o codificada: '
            DataField = 'Protestar'
            DataSource = DsConfigBB
            Items.Strings = (
              '00-Ausencia de instrucoes'
              
                '01-Cobrar juros - Dispensavel se inform. o valor a ser cobrado p' +
                'or dia de atraso.'
              '03-Protestar no terceiro dia util apos vencido'
              '04-Protestar no quarto dia util apos vencido'
              '05-Protestar no quinto dia util apos vencido'
              '06-INDICA PROTESTO EM DIAS CORRIDOS, COM PRAZO DE...'
              '07-Nao protestar'
              '10-Protestar no 10. dia corrido apos vencido'
              '15-Protestar no 15. dia corrido apos vencido'
              '20-Protestar no 20. dia corrido apos vencido'
              '22-Conceder desconto soh ateh a data estipulada'
              '25-Protestar no 25. dia corrido apos vencido'
              '30-Protestar no 30. dia corrido apos vencido'
              '45-Protestar no 45. dia corrido apos vencido')
            ParentBackground = True
            TabOrder = 1
            Values.Strings = (
              '0'
              '1'
              '3'
              '4'
              '5'
              '6'
              '7'
              '10'
              '15'
              '20'
              '22'
              '25'
              '30'
              '45')
          end
          object DBEdit25: TDBEdit
            Left = 520
            Top = 116
            Width = 28
            Height = 21
            DataField = 'Corrido'
            DataSource = DsConfigBB
            TabOrder = 2
          end
        end
        object TabSheet7: TTabSheet
          Caption = 'CNAB240 - Remessa  (1) '
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label54: TLabel
            Left = 248
            Top = 96
            Width = 108
            Height = 13
            Caption = 'Dias ap'#243's vencimento:'
          end
          object Label55: TLabel
            Left = 248
            Top = 208
            Width = 56
            Height = 13
            Caption = 'Dias ap'#243's..:'
          end
          object Label59: TLabel
            Left = 248
            Top = 324
            Width = 140
            Height = 13
            Caption = '$/dia Mora ou % / M'#234's Juros:'
            Visible = False
          end
          object Label61: TLabel
            Left = 420
            Top = 296
            Width = 95
            Height = 13
            Caption = 'Contr. Oper. cr'#233'dito:'
          end
          object DBCheckBox2: TDBCheckBox
            Left = 248
            Top = 3
            Width = 262
            Height = 17
            Caption = 'Informa conv'#234'nio no header do arquivo CNAB240.'
            DataField = 'InfoCovH'
            DataSource = DsConfigBB
            TabOrder = 1
            ValueChecked = '1'
            ValueUnchecked = '0'
          end
          object DBRGCarteira240: TDBRadioGroup
            Left = 420
            Top = 168
            Width = 237
            Height = 125
            Caption = ' Carteira: '
            DataField = 'Carteira240'
            DataSource = DsConfigBB
            Items.Strings = (
              '1 - Cobran'#231'a Simples.                      '
              '2 - Cobran'#231'a Vinculada.                    '
              '3 - Cobran'#231'a Caucionada.                   '
              '4 - Cobran'#231'a Descontada.                   '
              '7 - Cobran'#231'a Direta Especial (carteira 17)')
            ParentBackground = True
            TabOrder = 7
            Values.Strings = (
              '1'
              '2'
              '3'
              '4'
              '7')
          end
          object DBRGCadastramento: TDBRadioGroup
            Left = 660
            Top = 24
            Width = 115
            Height = 65
            Caption = ' Cadastramento: '
            DataField = 'Cadastramento'
            DataSource = DsConfigBB
            Items.Strings = (
              '1 - Com cadastro.'
              '2 - Sem cadastro.')
            ParentBackground = True
            TabOrder = 8
            Values.Strings = (
              '1'
              '2')
          end
          object DBRGTradiEscrit: TDBRadioGroup
            Left = 660
            Top = 92
            Width = 115
            Height = 65
            Caption = ' Tipo de documento: '
            DataField = 'TradiEscrit'
            DataSource = DsConfigBB
            Items.Strings = (
              '1 - Tradicional.'
              '2 - Escritural.')
            ParentBackground = True
            TabOrder = 9
            Values.Strings = (
              '1'
              '2')
          end
          object DBRGDistribuicao: TDBRadioGroup
            Left = 660
            Top = 160
            Width = 115
            Height = 65
            Caption = ' Distribui'#231#227'o: '
            DataField = 'Distribuicao'
            DataSource = DsConfigBB
            Items.Strings = (
              '1 - Banco.'
              '2 - Cliente.')
            ParentBackground = True
            TabOrder = 10
            Values.Strings = (
              '1'
              '2')
          end
          object DBRGAceite240: TDBRadioGroup
            Left = 660
            Top = 228
            Width = 115
            Height = 65
            Caption = ' Aceite: '
            DataField = 'Aceite240'
            DataSource = DsConfigBB
            Items.Strings = (
              'A - Aceite.'
              'N - N'#227'o aceite.')
            ParentBackground = True
            TabOrder = 11
            Values.Strings = (
              'A'
              'N')
          end
          object DBRGProtesto: TDBRadioGroup
            Left = 248
            Top = 20
            Width = 169
            Height = 73
            Caption = ' Protesto: '
            DataField = 'Protesto'
            DataSource = DsConfigBB
            Items.Strings = (
              '1 - Dias corridos.'
              '2 - Dias '#250'teis.'
              '3 - N'#227'o protestar.          ')
            ParentBackground = True
            TabOrder = 2
            Values.Strings = (
              '1'
              '2'
              '3')
          end
          object DBEdit28: TDBEdit
            Left = 248
            Top = 112
            Width = 165
            Height = 21
            DataField = 'Protestodd'
            DataSource = DsConfigBB
            TabOrder = 3
          end
          object DBRGBaixaDevol: TDBRadioGroup
            Left = 248
            Top = 136
            Width = 169
            Height = 69
            Caption = ' Baixa / devolu'#231#227'o: '
            DataField = 'BaixaDevol'
            DataSource = DsConfigBB
            Items.Strings = (
              '1 - Baixar/devolver.'
              '2 - N'#227'o baixar/n'#227'o devolver.')
            ParentBackground = True
            TabOrder = 4
            Values.Strings = (
              '1'
              '2')
          end
          object DBEdit29: TDBEdit
            Left = 248
            Top = 224
            Width = 169
            Height = 21
            DataField = 'BaixaDevoldd'
            DataSource = DsConfigBB
            TabOrder = 5
          end
          object DBRGEmisBloqueto: TDBRadioGroup
            Left = 420
            Top = 24
            Width = 237
            Height = 141
            Caption = ' Emiss'#227'o de bloqueto: '
            DataField = 'EmisBloqueto'
            DataSource = DsConfigBB
            Items.Strings = (
              '1 - Banco emite.                          '
              '2 - Cliente emite.                        '
              '3 - Banco pr'#233'-emite e cliente complementa.'
              '4 - Banco reemite.                        '
              '5 - Banco n'#227'o reemite.                    '
              '6 - Cobran'#231'a sem Papel.                   ')
            ParentBackground = True
            TabOrder = 6
            Values.Strings = (
              '1'
              '2'
              '3'
              '4'
              '5'
              '6')
          end
          object DBRGEspecie240: TDBRadioGroup
            Left = 0
            Top = 0
            Width = 245
            Height = 390
            Align = alLeft
            Caption = ' Esp'#233'cie de t'#237'tulo: '
            DataField = 'Especie240'
            DataSource = DsConfigBB
            Items.Strings = (
              '01 - CH  Cheque.'
              '02 - DM  Duplicata Mercantil.'
              '03 - DMI Duplicata Mercantil p/ Indica'#231#227'o.'
              '04 - DS  Duplicata de Servi'#231'o.'
              '05 - DSI Duplicata de Servi'#231'o p/ Indica'#231#227'o.'
              '06 - DR  Duplicata Rural.'
              '07 - LC  Letra de C'#226'mbio.'
              '08 - NCC Nota de Cr'#233'dito Comercial.'
              '09 - NCE Nota de Cr'#233'dito A Exporta'#231#227'o.'
              '10 - NCI Nota de Cr'#233'dito Industrial.'
              '11 - NCR Nota de Cr'#233'dito Rural.'
              '12 - NP  Nota Promiss'#243'ria.'
              '13 - NPR Nota Promiss'#243'ria Rural.'
              '14 - TM  Triplicata Mercantil.'
              '15 - TS  Triplicata de Servi'#231'o.'
              '16 - NS  Nota de Seguro.'
              '17 - RC  Recibo.'
              '18 - FAT Fatura.'
              '19 - ND  Nota de D'#233'bito.'
              '20 - AP  Ap'#243'lice de Seguro.'
              '21 - ME  Mensalidade Escolar'
              '22 - PC  Parcela de Cons'#243'rcio'
              '99 - Outros')
            ParentBackground = True
            TabOrder = 0
            Values.Strings = (
              '01'
              '02'
              '03'
              '04'
              '05'
              '06'
              '07'
              '08'
              '09'
              '10'
              '11'
              '12'
              '13'
              '14'
              '15'
              '16'
              '17'
              '18'
              '19'
              '20'
              '21'
              '22'
              '99')
          end
          object DBRGJuros240Cod: TDBRadioGroup
            Left = 248
            Top = 248
            Width = 169
            Height = 73
            Caption = ' Juros / Mora:'
            DataField = 'Juros240Cod'
            DataSource = DsConfigBB
            Items.Strings = (
              '1 - Valor por Dia'
              '2 - Taxa Mensal'
              '3 - Isento')
            ParentBackground = True
            TabOrder = 12
            Values.Strings = (
              '1'
              '2'
              '3')
            Visible = False
          end
          object DBEdit30: TDBEdit
            Left = 248
            Top = 340
            Width = 169
            Height = 21
            DataField = 'Juros240Qtd'
            DataSource = DsConfigBB
            TabOrder = 13
            Visible = False
          end
          object DBEdit31: TDBEdit
            Left = 420
            Top = 312
            Width = 97
            Height = 21
            DataField = 'ContrOperCred'
            DataSource = DsConfigBB
            TabOrder = 14
          end
        end
        object TabSheet10: TTabSheet
          Caption = 'CNAB240 - Remessa [2] '
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label74: TLabel
            Left = 4
            Top = 44
            Width = 61
            Height = 13
            Caption = 'TL_124_117'
            FocusControl = DBEdit37
          end
          object Label69: TLabel
            Left = 4
            Top = 4
            Width = 88
            Height = 13
            Caption = 'Reservado banco:'
          end
          object Label70: TLabel
            Left = 168
            Top = 4
            Width = 98
            Height = 13
            Caption = 'Reservado empresa:'
          end
          object Label71: TLabel
            Left = 332
            Top = 4
            Width = 65
            Height = 13
            Caption = 'LH_208_033:'
          end
          object Label72: TLabel
            Left = 600
            Top = 4
            Width = 66
            Height = 13
            Caption = 'SQ_233_008:'
          end
          object Label75: TLabel
            Left = 668
            Top = 4
            Width = 66
            Height = 13
            Caption = 'SR_208_033:'
          end
          object DBEdit32: TDBEdit
            Left = 4
            Top = 20
            Width = 160
            Height = 21
            DataField = 'ReservBanco'
            DataSource = DsConfigBB
            TabOrder = 0
          end
          object DBEdit33: TDBEdit
            Left = 168
            Top = 20
            Width = 160
            Height = 21
            DataField = 'ReservEmprs'
            DataSource = DsConfigBB
            TabOrder = 1
          end
          object DBEdit34: TDBEdit
            Left = 332
            Top = 20
            Width = 264
            Height = 21
            DataField = 'LH_208_33'
            DataSource = DsConfigBB
            TabOrder = 2
          end
          object DBEdit35: TDBEdit
            Left = 600
            Top = 20
            Width = 64
            Height = 21
            DataField = 'SQ_233_008'
            DataSource = DsConfigBB
            TabOrder = 3
          end
          object DBEdit36: TDBEdit
            Left = 668
            Top = 20
            Width = 264
            Height = 21
            DataField = 'SR_208_033'
            DataSource = DsConfigBB
            TabOrder = 4
          end
          object DBEdit37: TDBEdit
            Left = 4
            Top = 60
            Width = 848
            Height = 21
            DataField = 'TL_124_117'
            DataSource = DsConfigBB
            TabOrder = 5
          end
        end
        object TabSheet12: TTabSheet
          Caption = 'CNAB240 - Retorno [1]'
          ImageIndex = 5
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GroupBox11: TGroupBox
            Left = 0
            Top = 0
            Width = 393
            Height = 390
            Align = alLeft
            Caption = ' Status de movimentos de retorno: '
            TabOrder = 0
            Visible = False
            object GroupBox12: TGroupBox
              Left = 2
              Top = 15
              Width = 389
              Height = 373
              Align = alClient
              Caption = ' Liquida'#231#227'o: '
              TabOrder = 0
              object Label84: TLabel
                Left = 8
                Top = 56
                Width = 98
                Height = 13
                Caption = 'Antecipado no valor:'
              end
              object Label85: TLabel
                Left = 8
                Top = 16
                Width = 98
                Height = 13
                Caption = 'Antecipado a menor:'
              end
              object Label86: TLabel
                Left = 8
                Top = 96
                Width = 94
                Height = 13
                Caption = 'Antecipado a maior:'
              end
              object Label87: TLabel
                Left = 8
                Top = 136
                Width = 116
                Height = 13
                Caption = 'No vencimento a menor:'
              end
              object Label88: TLabel
                Left = 8
                Top = 176
                Width = 116
                Height = 13
                Caption = 'No vencimento no valor:'
              end
              object Label89: TLabel
                Left = 8
                Top = 216
                Width = 112
                Height = 13
                Caption = 'No vencimento a maior:'
              end
              object Label90: TLabel
                Left = 8
                Top = 256
                Width = 83
                Height = 13
                Caption = 'Vencido a menor:'
              end
              object Label91: TLabel
                Left = 8
                Top = 296
                Width = 83
                Height = 13
                Caption = 'Vencido no valor:'
              end
              object Label92: TLabel
                Left = 8
                Top = 336
                Width = 79
                Height = 13
                Caption = 'Vencido a maior:'
              end
              object DBEdit38: TDBEdit
                Left = 8
                Top = 32
                Width = 372
                Height = 21
                DataField = 'NOMELAN'
                DataSource = DsConfigBB
                TabOrder = 0
              end
              object DBEdit39: TDBEdit
                Left = 8
                Top = 72
                Width = 372
                Height = 21
                DataField = 'NOMELAL'
                DataSource = DsConfigBB
                TabOrder = 1
              end
              object DBEdit40: TDBEdit
                Left = 8
                Top = 112
                Width = 372
                Height = 21
                DataField = 'NOMELAI'
                DataSource = DsConfigBB
                TabOrder = 2
              end
              object DBEdit41: TDBEdit
                Left = 8
                Top = 152
                Width = 372
                Height = 21
                DataField = 'NOMELVN'
                DataSource = DsConfigBB
                TabOrder = 3
              end
              object DBEdit42: TDBEdit
                Left = 8
                Top = 192
                Width = 372
                Height = 21
                DataField = 'NOMELVL'
                DataSource = DsConfigBB
                TabOrder = 4
              end
              object DBEdit43: TDBEdit
                Left = 8
                Top = 232
                Width = 372
                Height = 21
                DataField = 'NOMELVI'
                DataSource = DsConfigBB
                TabOrder = 5
              end
              object DBEdit44: TDBEdit
                Left = 8
                Top = 272
                Width = 372
                Height = 21
                DataField = 'NOMELDN'
                DataSource = DsConfigBB
                TabOrder = 6
              end
              object DBEdit45: TDBEdit
                Left = 8
                Top = 312
                Width = 372
                Height = 21
                DataField = 'NOMELDL'
                DataSource = DsConfigBB
                TabOrder = 7
              end
              object DBEdit46: TDBEdit
                Left = 8
                Top = 352
                Width = 372
                Height = 21
                DataField = 'NOMELDI'
                DataSource = DsConfigBB
                TabOrder = 8
              end
            end
          end
        end
        object TabSheet13: TTabSheet
          Caption = 'CNAB240 - Retorno [2]'
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
        end
      end
      object Panel7: TPanel
        Left = 1
        Top = 1
        Width = 812
        Height = 44
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label1: TLabel
          Left = 4
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 48
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object Label52: TLabel
          Left = 404
          Top = 4
          Width = 34
          Height = 13
          Caption = 'Banco:'
          FocusControl = DBEdit27
        end
        object DBEdCodigo: TDBEdit
          Left = 4
          Top = 20
          Width = 41
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsConfigBB
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdNome: TDBEdit
          Left = 48
          Top = 20
          Width = 353
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsConfigBB
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object DBEdit27: TDBEdit
          Left = 404
          Top = 20
          Width = 36
          Height = 21
          DataField = 'Banco'
          DataSource = DsConfigBB
          TabOrder = 2
        end
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 495
      Width = 814
      Height = 48
      Align = alBottom
      TabOrder = 1
      object LaRegistro: TLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 344
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtIncluiClick
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 816
    Height = 544
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 814
      Height = 472
      Align = alTop
      TabOrder = 0
      object PageControl1: TPageControl
        Left = 1
        Top = 45
        Width = 812
        Height = 426
        ActivePage = TabSheet8
        Align = alClient
        TabOrder = 1
        object TabSheet1: TTabSheet
          Caption = 'Dados 1'
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel2: TPanel
            Left = 0
            Top = 0
            Width = 804
            Height = 398
            Align = alClient
            TabOrder = 0
            object Label18: TLabel
              Left = 8
              Top = 204
              Width = 207
              Height = 13
              Caption = 'Diret'#243'rio de arquivos cobran'#231'a - envio  [F4]:'
            end
            object CkPgAntes: TCheckBox
              Left = 4
              Top = 180
              Width = 777
              Height = 17
              Caption = 'Permitir pagar antecipadamente com desconto at'#233' uma data limite.'
              Enabled = False
              TabOrder = 4
            end
            object RGSiglaEspecie: TRadioGroup
              Left = 4
              Top = 4
              Width = 372
              Height = 172
              Caption = ' Sigla da esp'#233'cie: '
              Columns = 2
              Items.Strings = (
                'Ap'#243'lice de Seguro (AP)'
                'D'#237'vida Ativa da Uni'#227'o (DAU)'
                'D'#237'vida Ativa de Estado (DAE)'
                'D'#237'vida Ativa de Munic'#237'pio (DAM)'
                'Duplicata de Servi'#231'o (DS)'
                'Duplicata Mercantil (DM)'
                'Duplicata Rural (DR)'
                'Letra de C'#226'mbio (LC)'
                'Nota de D'#233'bito (ND)'
                'Nota de Seguro (SG)'
                'Nota Promiss'#243'ria (NP)'
                'Nota Promiss'#243'ria Rural (NPR)'
                'Recibo (RC)'
                'Outra Sigla:')
              TabOrder = 0
            end
            object RGMoeda: TRadioGroup
              Left = 380
              Top = 4
              Width = 169
              Height = 128
              Caption = ' Moeda: '
              Enabled = False
              ItemIndex = 4
              Items.Strings = (
                '02   US$'
                '05   IDTR'
                '07   UFIR'
                '08   FAJTR'
                '09   R$')
              TabOrder = 2
            end
            object RGAceite: TRadioGroup
              Left = 380
              Top = 136
              Width = 169
              Height = 40
              Caption = ' Aceite: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'N'#227'o'
                'Sim')
              TabOrder = 3
            end
            object EdOutraSigla: TdmkEdit
              Left = 282
              Top = 148
              Width = 52
              Height = 21
              MaxLength = 255
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdDiretorio: TdmkEdit
              Left = 8
              Top = 220
              Width = 785
              Height = 21
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              OnKeyDown = EdDiretorioKeyDown
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Dados 2'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 804
            Height = 398
            Align = alClient
            TabOrder = 0
            object Label31: TLabel
              Left = 4
              Top = 4
              Width = 104
              Height = 13
              Caption = 'Mensagem da linha 1:'
              Visible = False
            end
            object Label8: TLabel
              Left = 396
              Top = 4
              Width = 48
              Height = 13
              Caption = 'Conv'#234'nio:'
            end
            object Label11: TLabel
              Left = 504
              Top = 4
              Width = 39
              Height = 13
              Caption = 'Carteira:'
            end
            object Label12: TLabel
              Left = 564
              Top = 4
              Width = 45
              Height = 13
              Caption = 'Varia'#231#227'o:'
            end
            object Label50: TLabel
              Left = 616
              Top = 4
              Width = 64
              Height = 13
              Caption = 'C'#243'd. produto:'
            end
            object EdMsgLinha1: TdmkEdit
              Left = 4
              Top = 20
              Width = 389
              Height = 21
              MaxLength = 40
              TabOrder = 0
              Visible = False
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdConvenio: TdmkEdit
              Left = 396
              Top = 20
              Width = 105
              Height = 21
              Alignment = taRightJustify
              MaxLength = 255
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              OnExit = EdConvenioExit
            end
            object EdCarteira: TdmkEdit
              Left = 504
              Top = 20
              Width = 56
              Height = 21
              MaxLength = 255
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object EdVariacao: TdmkEdit
              Left = 564
              Top = 20
              Width = 48
              Height = 21
              MaxLength = 255
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
            object GroupBox3: TGroupBox
              Left = 4
              Top = 44
              Width = 333
              Height = 105
              Caption = ' Multa padr'#227'o para clientes sem configura'#231#227'o: '
              TabOrder = 5
              object Label32: TLabel
                Left = 156
                Top = 12
                Width = 46
                Height = 13
                Caption = 'Valor fixo:'
              end
              object Label33: TLabel
                Left = 248
                Top = 12
                Width = 66
                Height = 13
                Caption = 'Porcentagem:'
              end
              object Label34: TLabel
                Left = 156
                Top = 60
                Width = 56
                Height = 13
                Caption = 'Dias ap'#243's..:'
              end
              object RGMultaCodi: TRadioGroup
                Left = 2
                Top = 15
                Width = 147
                Height = 88
                Align = alLeft
                Caption = ' C'#243'digo da multa: '
                ItemIndex = 2
                Items.Strings = (
                  'Valor calculado pela %'
                  'Valor fixo (Valor fixo)'
                  'Porcentagem')
                TabOrder = 0
              end
              object EdMultaValr: TdmkEdit
                Left = 156
                Top = 28
                Width = 88
                Height = 21
                Alignment = taRightJustify
                MaxLength = 255
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdMultaPerc: TdmkEdit
                Left = 248
                Top = 28
                Width = 80
                Height = 21
                Alignment = taRightJustify
                MaxLength = 255
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdMultaDias: TdmkEdit
                Left = 156
                Top = 76
                Width = 64
                Height = 21
                Alignment = taRightJustify
                MaxLength = 255
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
              end
              object RGMultaTiVe: TRadioGroup
                Left = 224
                Top = 52
                Width = 105
                Height = 51
                Caption = ' ... qual data?: '
                ItemIndex = 1
                Items.Strings = (
                  'Vencto doc.'
                  'Dia '#250'til vencto.')
                TabOrder = 4
              end
            end
            object RGImpreLoc: TRadioGroup
              Left = 340
              Top = 44
              Width = 200
              Height = 105
              Caption = ' Pendente de impress'#227'o local?: '
              ItemIndex = 0
              Items.Strings = (
                'A ser impresso pelo BB Cobran'#231'a.'
                'J'#225' impresso.'
                'N'#227'o '#233' carteira 15, 16, 17 ou 18.')
              TabOrder = 6
            end
            object RGModalidade: TRadioGroup
              Left = 4
              Top = 152
              Width = 769
              Height = 140
              Caption = ' Tipo da modalidade do t'#237'tulo: '
              Items.Strings = (
                
                  '01 para t'#237'tulo simples (sem registro quando for carteira 16 ou 1' +
                  '8 e com registro quando for carteira 11,12 ou 15) '
                '02 para t'#237'tulo vinculado (quando for carteira 31) '
                '04 para t'#237'tulo descontado (quando for carteira 11,17 ou 51) '
                '07 para t'#237'tulo simples (com registro quando for carteira 17) '
                
                  '08 para t'#237'tulo BB Vendor (com registro quando for carteira 11 ou' +
                  ' 17)')
              TabOrder = 8
            end
            object GroupBox5: TGroupBox
              Left = 544
              Top = 44
              Width = 229
              Height = 105
              Caption = ' Convenente l'#237'der do cedente: '
              TabOrder = 7
              object Label6: TLabel
                Left = 16
                Top = 40
                Width = 42
                Height = 13
                Caption = 'Ag'#234'ncia:'
              end
              object Label7: TLabel
                Left = 64
                Top = 40
                Width = 18
                Height = 13
                Caption = 'DV:'
              end
              object Label13: TLabel
                Left = 104
                Top = 40
                Width = 73
                Height = 13
                Caption = 'Conta corrente:'
              end
              object Label17: TLabel
                Left = 180
                Top = 40
                Width = 18
                Height = 13
                Caption = 'DV:'
              end
              object EdclcAgencNr: TdmkEdit
                Left = 16
                Top = 56
                Width = 44
                Height = 21
                MaxLength = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnExit = EdclcAgencNrExit
              end
              object EdclcAgencDV: TdmkEdit
                Left = 64
                Top = 56
                Width = 24
                Height = 21
                CharCase = ecUpperCase
                MaxLength = 1
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnExit = EdclcAgencDVExit
              end
              object EdclcContaNr: TdmkEdit
                Left = 100
                Top = 56
                Width = 72
                Height = 21
                MaxLength = 8
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnExit = EdclcContaNrExit
              end
              object EdclcContaDV: TdmkEdit
                Left = 180
                Top = 56
                Width = 24
                Height = 21
                CharCase = ecUpperCase
                MaxLength = 1
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnExit = EdclcContaDVExit
              end
            end
            object GroupBox7: TGroupBox
              Left = 544
              Top = 188
              Width = 229
              Height = 104
              Caption = ' Cedente: '
              TabOrder = 9
              object Label42: TLabel
                Left = 16
                Top = 40
                Width = 42
                Height = 13
                Caption = 'Ag'#234'ncia:'
              end
              object Label43: TLabel
                Left = 64
                Top = 40
                Width = 18
                Height = 13
                Caption = 'DV:'
              end
              object Label44: TLabel
                Left = 104
                Top = 40
                Width = 73
                Height = 13
                Caption = 'Conta corrente:'
              end
              object Label45: TLabel
                Left = 180
                Top = 40
                Width = 18
                Height = 13
                Caption = 'DV:'
              end
              object EdcedAgencNr: TdmkEdit
                Left = 16
                Top = 56
                Width = 44
                Height = 21
                MaxLength = 4
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnExit = EdcedAgencNrExit
              end
              object EdcedAgencDV: TdmkEdit
                Left = 64
                Top = 56
                Width = 24
                Height = 21
                CharCase = ecUpperCase
                MaxLength = 1
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnExit = EdcedAgencDVExit
              end
              object EdcedContaNr: TdmkEdit
                Left = 104
                Top = 56
                Width = 72
                Height = 21
                MaxLength = 8
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnExit = EdcedContaNrExit
              end
              object EdcedContaDV: TdmkEdit
                Left = 180
                Top = 56
                Width = 24
                Height = 21
                CharCase = ecUpperCase
                MaxLength = 1
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnExit = EdcedContaDVExit
              end
            end
            object EdProduto: TdmkEdit
              Left = 616
              Top = 20
              Width = 64
              Height = 21
              MaxLength = 255
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
            end
          end
        end
        object TabSheet5: TTabSheet
          Caption = 'Dados 3'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object RGEspecie: TRadioGroup
            Left = 0
            Top = 0
            Width = 172
            Height = 398
            Align = alLeft
            Caption = ' Esp'#233'cie de t'#237'tulo: '
            ItemIndex = 0
            Items.Strings = (
              '01-Duplicata Mercantil'
              '02-Nota Promiss'#243'ria'
              '03-Nota de Seguro'
              '05-Recibo'
              '08-Letra de C'#226'mbio'
              '09-Warrant'
              '10-Cheque'
              '12-Duplicata de Servi'#231'o'
              '13-Nota de D'#233'bito'
              '15-Ap'#243'lice de Seguro'
              '21-Duplicata Rural'
              '25-D'#237'vida Ativa da Uni'#227'o    '
              '26-D'#237'vida Ativa de Estado   '
              '27-D'#237'vida Ativa de Munic'#237'pio')
            TabOrder = 0
          end
          object RGProtestar: TRadioGroup
            Left = 172
            Top = 0
            Width = 405
            Height = 398
            Align = alLeft
            Caption = ' Primeira instru'#231#227'o codificada: '
            ItemIndex = 0
            Items.Strings = (
              '00-Ausencia de instrucoes'
              
                '01-Cobrar juros - Dispensavel se inform. o valor a ser cobrado p' +
                'or dia de atraso.'
              '03-Protestar no terceiro dia util apos vencido'
              '04-Protestar no quarto dia util apos vencido'
              '05-Protestar no quinto dia util apos vencido'
              '06-INDICA PROTESTO EM DIAS CORRIDOS, COM PRAZO DE...'
              '07-Nao protestar'
              '10-Protestar no 10. dia corrido apos vencido'
              '15-Protestar no 15. dia corrido apos vencido'
              '20-Protestar no 20. dia corrido apos vencido'
              '22-Conceder desconto soh ateh a data estipulada'
              '25-Protestar no 25. dia corrido apos vencido'
              '30-Protestar no 30. dia corrido apos vencido'
              '45-Protestar no 45. dia corrido apos vencido')
            TabOrder = 1
          end
          object EdCorrido: TdmkEdit
            Left = 520
            Top = 116
            Width = 28
            Height = 21
            Alignment = taRightJustify
            MaxLength = 255
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnExit = EdCorridoExit
          end
        end
        object TabSheet8: TTabSheet
          Caption = 'CNAB240 - Remessa [1]'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label56: TLabel
            Left = 248
            Top = 208
            Width = 56
            Height = 13
            Caption = 'Dias ap'#243's..:'
          end
          object Label57: TLabel
            Left = 248
            Top = 96
            Width = 56
            Height = 13
            Caption = 'Dias ap'#243's..:'
          end
          object Label58: TLabel
            Left = 248
            Top = 324
            Width = 140
            Height = 13
            Caption = '$/dia Mora ou % / M'#234's Juros:'
            Visible = False
          end
          object Label60: TLabel
            Left = 420
            Top = 296
            Width = 95
            Height = 13
            Caption = 'Contr. Oper. cr'#233'dito:'
          end
          object CkInfoCovH: TCheckBox
            Left = 248
            Top = 3
            Width = 265
            Height = 17
            Caption = 'Informa conv'#234'nio no header do arquivo CNAB240.'
            TabOrder = 0
          end
          object RGCarteira240: TRadioGroup
            Left = 420
            Top = 168
            Width = 237
            Height = 125
            Caption = ' Carteira: '
            Items.Strings = (
              '1 - Cobran'#231'a Simples.                      '
              '2 - Cobran'#231'a Vinculada.                    '
              '3 - Cobran'#231'a Caucionada.                   '
              '4 - Cobran'#231'a Descontada.                   '
              '7 - Cobran'#231'a Direta Especial (carteira 17)')
            TabOrder = 1
          end
          object RGCadastramento: TRadioGroup
            Left = 660
            Top = 24
            Width = 115
            Height = 65
            Caption = ' Cadastramento: '
            Items.Strings = (
              '1 - Com cadastro.'
              '2 - Sem cadastro.')
            TabOrder = 2
          end
          object RGDistribuicao: TRadioGroup
            Left = 660
            Top = 160
            Width = 115
            Height = 65
            Caption = ' Distribui'#231#227'o: '
            Items.Strings = (
              '1 - Banco.'
              '2 - Cliente.')
            TabOrder = 3
          end
          object RGAceite240: TRadioGroup
            Left = 660
            Top = 228
            Width = 115
            Height = 65
            Caption = ' Aceite: '
            Items.Strings = (
              'A - Aceite.'
              'N - N'#227'o aceite.')
            TabOrder = 4
          end
          object RGTradiEscrit: TRadioGroup
            Left = 660
            Top = 92
            Width = 115
            Height = 65
            Caption = ' Tipo de documento: '
            Items.Strings = (
              '1 - Tradicional.'
              '2 - Escritural.')
            TabOrder = 5
          end
          object EdBaixaDevoldd: TdmkEdit
            Left = 248
            Top = 224
            Width = 169
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object RGBaixaDevol: TRadioGroup
            Left = 248
            Top = 136
            Width = 169
            Height = 69
            Caption = ' Baixa / devolu'#231#227'o: '
            Items.Strings = (
              '1 - Baixar/devolver.'
              '2 - N'#227'o baixar/n'#227'o devolver.')
            TabOrder = 7
          end
          object EdProtestodd: TdmkEdit
            Left = 248
            Top = 112
            Width = 169
            Height = 21
            Alignment = taRightJustify
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object RGProtesto: TRadioGroup
            Left = 248
            Top = 20
            Width = 169
            Height = 73
            Caption = ' Protesto: '
            Items.Strings = (
              '1 - Dias corridos.'
              '2 - Dias '#250'teis.'
              '3 - N'#227'o protestar.          ')
            TabOrder = 9
          end
          object RGEmisBloqueto: TRadioGroup
            Left = 420
            Top = 24
            Width = 237
            Height = 141
            Caption = ' Emiss'#227'o de bloqueto: '
            Items.Strings = (
              '1 - Banco emite.                          '
              '2 - Cliente emite.                        '
              '3 - Banco pr'#233'-emite e cliente complementa.'
              '4 - Banco reemite.                        '
              '5 - Banco n'#227'o reemite.                    '
              '6 - Cobran'#231'a sem Papel.                   ')
            TabOrder = 10
          end
          object RGEspecie240: TRadioGroup
            Left = 0
            Top = 0
            Width = 245
            Height = 398
            Align = alLeft
            Caption = ' Esp'#233'cie: '
            Items.Strings = (
              '01 - CH  Cheque.'
              '02 - DM  Duplicata Mercantil.'
              '03 - DMI Duplicata Mercantil p/ Indica'#231#227'o.'
              '04 - DS  Duplicata de Servi'#231'o.'
              '05 - DSI Duplicata de Servi'#231'o p/ Indica'#231#227'o.'
              '06 - DR  Duplicata Rural.'
              '07 - LC  Letra de C'#226'mbio.'
              '08 - NCC Nota de Cr'#233'dito Comercial.'
              '09 - NCE Nota de Cr'#233'dito A Exporta'#231#227'o.'
              '10 - NCI Nota de Cr'#233'dito Industrial.'
              '11 - NCR Nota de Cr'#233'dito Rural.'
              '12 - NP  Nota Promiss'#243'ria.'
              '13 - NPR Nota Promiss'#243'ria Rural.'
              '14 - TM  Triplicata Mercantil.'
              '15 - TS  Triplicata de Servi'#231'o.'
              '16 - NS  Nota de Seguro.'
              '17 - RC  Recibo.'
              '18 - FAT Fatura.'
              '19 - ND  Nota de D'#233'bito.'
              '20 - AP  Ap'#243'lice de Seguro.'
              '21 - ME  Mensalidade Escolar'
              '22 - PC  Parcela de Cons'#243'rcio'
              '99 - Outros')
            TabOrder = 11
          end
          object RGJuros240Cod: TRadioGroup
            Left = 248
            Top = 248
            Width = 169
            Height = 73
            Caption = ' Juros / Mora:'
            Items.Strings = (
              '1 - Valor por Dia'
              '2 - Taxa Mensal'
              '3 - Isento')
            TabOrder = 12
            Visible = False
            OnClick = RGJuros240CodClick
          end
          object EdJuros240Qtd: TdmkEdit
            Left = 248
            Top = 340
            Width = 169
            Height = 21
            Alignment = taRightJustify
            TabOrder = 13
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
          object EdContrOperCred: TdmkEdit
            Left = 420
            Top = 312
            Width = 97
            Height = 21
            Alignment = taRightJustify
            TabOrder = 14
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            OnExit = EdContrOperCredExit
          end
        end
        object TabSheet9: TTabSheet
          Caption = 'CNAB240 - Remessa [2]'
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Label62: TLabel
            Left = 4
            Top = 4
            Width = 88
            Height = 13
            Caption = 'Reservado banco:'
          end
          object Label64: TLabel
            Left = 168
            Top = 4
            Width = 98
            Height = 13
            Caption = 'Reservado empresa:'
          end
          object Label65: TLabel
            Left = 332
            Top = 4
            Width = 65
            Height = 13
            Caption = 'LH_208_033:'
          end
          object Label66: TLabel
            Left = 600
            Top = 4
            Width = 66
            Height = 13
            Caption = 'SQ_233_008:'
          end
          object Label67: TLabel
            Left = 4
            Top = 44
            Width = 65
            Height = 13
            Caption = 'LH_208_033:'
          end
          object Label68: TLabel
            Left = 668
            Top = 4
            Width = 66
            Height = 13
            Caption = 'SR_208_033:'
          end
          object EdReservBanco: TdmkEdit
            Left = 4
            Top = 20
            Width = 160
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdReservEmprs: TdmkEdit
            Left = 168
            Top = 20
            Width = 160
            Height = 21
            Alignment = taRightJustify
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdLH_208_33: TdmkEdit
            Left = 332
            Top = 20
            Width = 264
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdSQ_233_008: TdmkEdit
            Left = 600
            Top = 20
            Width = 64
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdTL_124_117: TdmkEdit
            Left = 4
            Top = 60
            Width = 848
            Height = 21
            Alignment = taRightJustify
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
          object EdSR_208_033: TdmkEdit
            Left = 668
            Top = 20
            Width = 264
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
          end
        end
        object TabSheet11: TTabSheet
          Caption = 'CNAB240 - Retorno [1]'
          ImageIndex = 5
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GroupBox9: TGroupBox
            Left = 0
            Top = 0
            Width = 393
            Height = 398
            Align = alLeft
            Caption = ' Status de movimentos de retorno: '
            TabOrder = 0
            Visible = False
            object GroupBox10: TGroupBox
              Left = 2
              Top = 15
              Width = 389
              Height = 381
              Align = alClient
              Caption = ' Liquida'#231#227'o: '
              TabOrder = 0
              object Label73: TLabel
                Left = 8
                Top = 56
                Width = 98
                Height = 13
                Caption = 'Antecipado no valor:'
              end
              object Label76: TLabel
                Left = 8
                Top = 16
                Width = 98
                Height = 13
                Caption = 'Antecipado a menor:'
              end
              object Label77: TLabel
                Left = 8
                Top = 96
                Width = 94
                Height = 13
                Caption = 'Antecipado a maior:'
              end
              object Label78: TLabel
                Left = 8
                Top = 136
                Width = 116
                Height = 13
                Caption = 'No vencimento a menor:'
              end
              object Label79: TLabel
                Left = 8
                Top = 176
                Width = 116
                Height = 13
                Caption = 'No vencimento no valor:'
              end
              object Label80: TLabel
                Left = 8
                Top = 216
                Width = 112
                Height = 13
                Caption = 'No vencimento a maior:'
              end
              object Label81: TLabel
                Left = 8
                Top = 256
                Width = 83
                Height = 13
                Caption = 'Vencido a menor:'
              end
              object Label82: TLabel
                Left = 8
                Top = 296
                Width = 83
                Height = 13
                Caption = 'Vencido no valor:'
              end
              object Label83: TLabel
                Left = 8
                Top = 336
                Width = 79
                Height = 13
                Caption = 'Vencido a maior:'
              end
              object EdLiqAntMen: TdmkEditCB
                Left = 8
                Top = 32
                Width = 49
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBLiqAntMen
                IgnoraDBLookupComboBox = False
              end
              object CBLiqAntMen: TdmkDBLookupComboBox
                Left = 60
                Top = 32
                Width = 321
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOcorDupl
                TabOrder = 1
                dmkEditCB = EdLiqAntMen
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdLiqAntVal: TdmkEditCB
                Left = 8
                Top = 72
                Width = 49
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBLiqAntVal
                IgnoraDBLookupComboBox = False
              end
              object CBLiqAntVal: TdmkDBLookupComboBox
                Left = 60
                Top = 72
                Width = 321
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOcorDupl
                TabOrder = 3
                dmkEditCB = EdLiqAntVal
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdLiqAntMai: TdmkEditCB
                Left = 8
                Top = 112
                Width = 49
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBLiqAntMai
                IgnoraDBLookupComboBox = False
              end
              object CBLiqAntMai: TdmkDBLookupComboBox
                Left = 60
                Top = 112
                Width = 321
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOcorDupl
                TabOrder = 5
                dmkEditCB = EdLiqAntMai
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdLiqVenMen: TdmkEditCB
                Left = 8
                Top = 152
                Width = 49
                Height = 21
                Alignment = taRightJustify
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBLiqVenMen
                IgnoraDBLookupComboBox = False
              end
              object CBLiqVenMen: TdmkDBLookupComboBox
                Left = 60
                Top = 152
                Width = 321
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOcorDupl
                TabOrder = 7
                dmkEditCB = EdLiqVenMen
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdLiqVenVal: TdmkEditCB
                Left = 8
                Top = 192
                Width = 49
                Height = 21
                Alignment = taRightJustify
                TabOrder = 8
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBLiqVenVal
                IgnoraDBLookupComboBox = False
              end
              object CBLiqVenVal: TdmkDBLookupComboBox
                Left = 60
                Top = 192
                Width = 321
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOcorDupl
                TabOrder = 9
                dmkEditCB = EdLiqVenVal
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdLiqVenMai: TdmkEditCB
                Left = 8
                Top = 232
                Width = 49
                Height = 21
                Alignment = taRightJustify
                TabOrder = 10
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBLiqVenMai
                IgnoraDBLookupComboBox = False
              end
              object CBLiqVenMai: TdmkDBLookupComboBox
                Left = 60
                Top = 232
                Width = 321
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOcorDupl
                TabOrder = 11
                dmkEditCB = EdLiqVenMai
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdLiqVcdMen: TdmkEditCB
                Left = 8
                Top = 272
                Width = 49
                Height = 21
                Alignment = taRightJustify
                TabOrder = 12
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBLiqVcdMen
                IgnoraDBLookupComboBox = False
              end
              object CBLiqVcdMen: TdmkDBLookupComboBox
                Left = 60
                Top = 272
                Width = 321
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOcorDupl
                TabOrder = 13
                dmkEditCB = EdLiqVcdMen
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdLiqVcdVal: TdmkEditCB
                Left = 8
                Top = 312
                Width = 49
                Height = 21
                Alignment = taRightJustify
                TabOrder = 14
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBLiqVcdVal
                IgnoraDBLookupComboBox = False
              end
              object CBLiqVcdVal: TdmkDBLookupComboBox
                Left = 60
                Top = 312
                Width = 321
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOcorDupl
                TabOrder = 15
                dmkEditCB = EdLiqVcdVal
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdLiqVcdMai: TdmkEditCB
                Left = 8
                Top = 352
                Width = 49
                Height = 21
                Alignment = taRightJustify
                TabOrder = 16
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBLiqVcdMai
                IgnoraDBLookupComboBox = False
              end
              object CBLiqVcdMai: TdmkDBLookupComboBox
                Left = 60
                Top = 352
                Width = 321
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOcorDupl
                TabOrder = 17
                dmkEditCB = EdLiqVcdMai
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
            end
          end
        end
        object TabSheet14: TTabSheet
          Caption = 'CNAB240 - Retorno [2]'
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object CheckGroup1: TdmkCheckGroup
            Left = 0
            Top = 0
            Width = 804
            Height = 398
            Align = alClient
            Caption = 'CheckGroup1'
            Items.Strings = (
              '02 - Entrada confirmada.'
              '03 - Entrada rejeitada.'
              '04 - Transfer'#234'ncia de carteira/entrada.'
              '05 - Transfer'#234'ncia de carteira/baixa.'
              '06 - Liquida'#231#227'o.'
              '09 - Baixa.'
              '11 - T'#237'tulos em carteira (em ser).'
              '12 - Confirma'#231#227'o recebimento instru'#231#227'o de abatimento.'
              
                '13 - Confirma'#231#227'o recebimento instru'#231#227'o de cancelamento abatiment' +
                'o.'
              '14 - Confirma'#231#227'o recebimento instru'#231#227'o altera'#231#227'o de vencimento.'
              '15 - Franco de pagamento.'
              '17 - Liquida'#231#227'o apos baixa ou liquida'#231#227'o t'#237'tulo n'#227'o registrado.'
              '19 - Confirma'#231#227'o recebimento instru'#231#227'o de protesto.'
              
                '20 - Confirma'#231#227'o recebimento instru'#231#227'o de susta'#231#227'o/cancelamento ' +
                'de protesto.'
              '23 - Remessa a cart'#243'rio (aponte em cart'#243'rio).'
              '24 - Retirada de cart'#243'rio e manuten'#231#227'o em carteira.'
              '25 - Protestado e baixado (baixa por ter sido protestado).'
              '26 - Instru'#231#227'o rejeitada.'
              '27 - Confirma'#231#227'o do pedido de altera'#231#227'o de outros dados.'
              '28 - D'#233'bito de tarifas/custas.'
              '29 - Ocorr'#234'ncias do sacado.'
              '30 - Altera'#231#227'o de dados rejeitada.')
            TabOrder = 0
            UpdType = utYes
            Value = 0
            OldValor = 0
          end
        end
      end
      object Panel1: TPanel
        Left = 1
        Top = 1
        Width = 812
        Height = 44
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Label9: TLabel
          Left = 4
          Top = 4
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
        end
        object Label10: TLabel
          Left = 48
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
        end
        object Label53: TLabel
          Left = 404
          Top = 4
          Width = 34
          Height = 13
          Caption = 'Banco:'
          FocusControl = DBEdit27
        end
        object EdCodigo: TdmkEdit
          Left = 4
          Top = 20
          Width = 41
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdNome: TdmkEdit
          Left = 48
          Top = 20
          Width = 353
          Height = 21
          MaxLength = 255
          TabOrder = 1
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
        end
        object EdBanco: TdmkEdit
          Left = 404
          Top = 20
          Width = 36
          Height = 21
          MaxLength = 255
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnChange = EdBancoChange
        end
      end
    end
    object PainelConfirma: TPanel
      Left = 1
      Top = 495
      Width = 814
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Default = True
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 688
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtDesisteClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 816
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                            Configura'#231#227'o de Cobran'#231'a'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 733
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 507
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 481
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsConfigBB: TDataSource
    DataSet = QrConfigBB
    Left = 748
    Top = 21
  end
  object QrConfigBB: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrConfigBBBeforeOpen
    AfterOpen = QrConfigBBAfterOpen
    SQL.Strings = (
      'SELECT cbb.*, obk.Nome NOMEOCORBANK,'
      'lan.Nome NOMELAN, lal.Nome NOMELAL, lai.Nome NOMELAI, '
      'lvn.Nome NOMELVN, lvl.Nome NOMELVL, lvi.Nome NOMELVI, '
      'ldn.Nome NOMELDN, ldl.Nome NOMELDL, ldi.Nome NOMELDI'
      ' '
      'FROM configbb cbb'
      ''
      'LEFT JOIN ocorbank obk ON obk.Codigo=cbb.LiqOcoCOc'
      ''
      'LEFT JOIN ocordupl lan ON lan.Codigo=cbb.LiqAntMen'
      'LEFT JOIN ocordupl lal ON lal.Codigo=cbb.LiqAntVal'
      'LEFT JOIN ocordupl lai ON lai.Codigo=cbb.LiqAntMai '
      ''
      'LEFT JOIN ocordupl lvn ON lvn.Codigo=cbb.LiqVenMen'
      'LEFT JOIN ocordupl lvl ON lvl.Codigo=cbb.LiqVenVal'
      'LEFT JOIN ocordupl lvi ON lvi.Codigo=cbb.LiqVenMai'
      ''
      'LEFT JOIN ocordupl ldn ON ldn.Codigo=cbb.LiqVcdMen'
      'LEFT JOIN ocordupl ldl ON ldl.Codigo=cbb.LiqVcdVal'
      'LEFT JOIN ocordupl ldi ON ldi.Codigo=cbb.LiqVcdMai'
      ''
      'WHERE cbb.Codigo > 0')
    Left = 720
    Top = 21
    object QrConfigBBCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConfigBBNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrConfigBBConvenio: TIntegerField
      FieldName = 'Convenio'
    end
    object QrConfigBBCarteira: TWideStringField
      FieldName = 'Carteira'
      Size = 2
    end
    object QrConfigBBVariacao: TWideStringField
      FieldName = 'Variacao'
      Size = 3
    end
    object QrConfigBBSiglaEspecie: TWideStringField
      FieldName = 'SiglaEspecie'
      Size = 5
    end
    object QrConfigBBMoeda: TWideStringField
      FieldName = 'Moeda'
      Size = 5
    end
    object QrConfigBBAceite: TSmallintField
      FieldName = 'Aceite'
    end
    object QrConfigBBPgAntes: TSmallintField
      FieldName = 'PgAntes'
    end
    object QrConfigBBProtestar: TSmallintField
      FieldName = 'Protestar'
    end
    object QrConfigBBMsgLinha1: TWideStringField
      FieldName = 'MsgLinha1'
      Size = 40
    end
    object QrConfigBBMultaCodi: TSmallintField
      FieldName = 'MultaCodi'
    end
    object QrConfigBBMultaDias: TSmallintField
      FieldName = 'MultaDias'
    end
    object QrConfigBBMultaValr: TFloatField
      FieldName = 'MultaValr'
      DisplayFormat = '#,###,##0.00'
    end
    object QrConfigBBMultaPerc: TFloatField
      FieldName = 'MultaPerc'
      DisplayFormat = '#,###,##0.00'
    end
    object QrConfigBBMultaTiVe: TSmallintField
      FieldName = 'MultaTiVe'
    end
    object QrConfigBBImpreLoc: TSmallintField
      FieldName = 'ImpreLoc'
    end
    object QrConfigBBModalidade: TIntegerField
      FieldName = 'Modalidade'
    end
    object QrConfigBBclcAgencNr: TWideStringField
      FieldName = 'clcAgencNr'
      Size = 4
    end
    object QrConfigBBclcAgencDV: TWideStringField
      FieldName = 'clcAgencDV'
      Size = 1
    end
    object QrConfigBBclcContaNr: TWideStringField
      FieldName = 'clcContaNr'
      Size = 8
    end
    object QrConfigBBclcContaDV: TWideStringField
      FieldName = 'clcContaDV'
      Size = 1
    end
    object QrConfigBBcedAgencNr: TWideStringField
      FieldName = 'cedAgencNr'
      Size = 4
    end
    object QrConfigBBcedAgencDV: TWideStringField
      FieldName = 'cedAgencDV'
      Size = 1
    end
    object QrConfigBBcedContaNr: TWideStringField
      FieldName = 'cedContaNr'
      Size = 8
    end
    object QrConfigBBcedContaDV: TWideStringField
      FieldName = 'cedContaDV'
      Size = 1
    end
    object QrConfigBBEspecie: TSmallintField
      FieldName = 'Especie'
    end
    object QrConfigBBCorrido: TSmallintField
      FieldName = 'Corrido'
    end
    object QrConfigBBIDEmpresa: TWideStringField
      FieldName = 'IDEmpresa'
    end
    object QrConfigBBBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrConfigBBProduto: TWideStringField
      FieldName = 'Produto'
      Size = 4
    end
    object QrConfigBBInfoCovH: TSmallintField
      FieldName = 'InfoCovH'
    end
    object QrConfigBBCarteira240: TWideStringField
      FieldName = 'Carteira240'
      Size = 1
    end
    object QrConfigBBCadastramento: TWideStringField
      FieldName = 'Cadastramento'
      Size = 1
    end
    object QrConfigBBTradiEscrit: TWideStringField
      FieldName = 'TradiEscrit'
      Size = 1
    end
    object QrConfigBBDistribuicao: TWideStringField
      FieldName = 'Distribuicao'
      Size = 1
    end
    object QrConfigBBAceite240: TWideStringField
      FieldName = 'Aceite240'
      Size = 1
    end
    object QrConfigBBProtesto: TWideStringField
      FieldName = 'Protesto'
      Size = 1
    end
    object QrConfigBBProtestodd: TIntegerField
      FieldName = 'Protestodd'
    end
    object QrConfigBBBaixaDevol: TWideStringField
      FieldName = 'BaixaDevol'
      Size = 1
    end
    object QrConfigBBBaixaDevoldd: TIntegerField
      FieldName = 'BaixaDevoldd'
    end
    object QrConfigBBEmisBloqueto: TWideStringField
      FieldName = 'EmisBloqueto'
      Size = 1
    end
    object QrConfigBBEspecie240: TWideStringField
      FieldName = 'Especie240'
      Size = 2
    end
    object QrConfigBBJuros240Cod: TWideStringField
      FieldName = 'Juros240Cod'
      Size = 1
    end
    object QrConfigBBJuros240Qtd: TFloatField
      FieldName = 'Juros240Qtd'
      DisplayFormat = '#,###,##0.00'
    end
    object QrConfigBBContrOperCred: TIntegerField
      FieldName = 'ContrOperCred'
      DisplayFormat = '0000000000'
    end
    object QrConfigBBReservBanco: TWideStringField
      FieldName = 'ReservBanco'
    end
    object QrConfigBBReservEmprs: TWideStringField
      FieldName = 'ReservEmprs'
    end
    object QrConfigBBLH_208_33: TWideStringField
      FieldName = 'LH_208_33'
      Size = 33
    end
    object QrConfigBBSQ_233_008: TWideStringField
      FieldName = 'SQ_233_008'
      Size = 8
    end
    object QrConfigBBSR_208_033: TWideStringField
      FieldName = 'SR_208_033'
      Size = 33
    end
    object QrConfigBBTL_124_117: TWideStringField
      FieldName = 'TL_124_117'
      Size = 117
    end
    object QrConfigBBLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrConfigBBDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrConfigBBDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrConfigBBUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrConfigBBUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrConfigBBLiqAntMen: TIntegerField
      FieldName = 'LiqAntMen'
      Required = True
    end
    object QrConfigBBLiqAntVal: TIntegerField
      FieldName = 'LiqAntVal'
      Required = True
    end
    object QrConfigBBLiqAntMai: TIntegerField
      FieldName = 'LiqAntMai'
      Required = True
    end
    object QrConfigBBLiqVenMen: TIntegerField
      FieldName = 'LiqVenMen'
      Required = True
    end
    object QrConfigBBLiqVenVal: TIntegerField
      FieldName = 'LiqVenVal'
      Required = True
    end
    object QrConfigBBLiqVenMai: TIntegerField
      FieldName = 'LiqVenMai'
      Required = True
    end
    object QrConfigBBLiqVcdMen: TIntegerField
      FieldName = 'LiqVcdMen'
      Required = True
    end
    object QrConfigBBLiqVcdVal: TIntegerField
      FieldName = 'LiqVcdVal'
      Required = True
    end
    object QrConfigBBLiqVcdMai: TIntegerField
      FieldName = 'LiqVcdMai'
      Required = True
    end
    object QrConfigBBNOMELAN: TWideStringField
      FieldName = 'NOMELAN'
      Size = 50
    end
    object QrConfigBBNOMELAL: TWideStringField
      FieldName = 'NOMELAL'
      Size = 50
    end
    object QrConfigBBNOMELAI: TWideStringField
      FieldName = 'NOMELAI'
      Size = 50
    end
    object QrConfigBBNOMELVN: TWideStringField
      FieldName = 'NOMELVN'
      Size = 50
    end
    object QrConfigBBNOMELVL: TWideStringField
      FieldName = 'NOMELVL'
      Size = 50
    end
    object QrConfigBBNOMELVI: TWideStringField
      FieldName = 'NOMELVI'
      Size = 50
    end
    object QrConfigBBNOMELDN: TWideStringField
      FieldName = 'NOMELDN'
      Size = 50
    end
    object QrConfigBBNOMELDL: TWideStringField
      FieldName = 'NOMELDL'
      Size = 50
    end
    object QrConfigBBNOMELDI: TWideStringField
      FieldName = 'NOMELDI'
      Size = 50
    end
    object QrConfigBBDiretorio: TWideStringField
      FieldName = 'Diretorio'
      Size = 255
    end
  end
  object DsOcorDupl: TDataSource
    DataSet = QrOcorDupl
    Left = 612
    Top = 57
  end
  object QrOcorDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ocordupl'
      'ORDER BY Nome'
      '')
    Left = 584
    Top = 57
    object QrOcorDuplCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorDuplNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrOcorDuplBase: TFloatField
      FieldName = 'Base'
    end
    object QrOcorDuplLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorDuplDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorDuplDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorDuplUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorDuplUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorDuplDatapbase: TIntegerField
      FieldName = 'Datapbase'
    end
    object QrOcorDuplOcorrbase: TIntegerField
      FieldName = 'Ocorrbase'
    end
  end
  object QrocorBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ocorbank'
      'ORDER BY Nome'
      '')
    Left = 640
    Top = 57
    object QrocorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrocorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrocorBankLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrocorBankDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrocorBankDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrocorBankUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrocorBankUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrocorBankBase: TFloatField
      FieldName = 'Base'
    end
  end
  object DsOcorBank: TDataSource
    DataSet = QrocorBank
    Left = 668
    Top = 57
  end
end
