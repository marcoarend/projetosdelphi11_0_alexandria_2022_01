object FmGruSacEmi: TFmGruSacEmi
  Left = 368
  Top = 194
  Caption = 'Grupos de Sac/Emi'
  ClientHeight = 346
  ClientWidth = 679
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnSacEmi: TPanel
    Left = 0
    Top = 48
    Width = 679
    Height = 298
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object Panel2: TPanel
      Left = 1
      Top = 249
      Width = 677
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfIts: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Default = True
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfItsClick
        NumGlyphs = 2
      end
      object BtDesiIts: TBitBtn
        Tag = 15
        Left = 584
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtDesiItsClick
        NumGlyphs = 2
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 677
      Height = 196
      Align = alTop
      TabOrder = 0
      object Label3: TLabel
        Left = 8
        Top = 8
        Width = 164
        Height = 13
        Caption = 'CPF / CNPJ Emitente [F8 localiza]:'
      end
      object EdCPF1: TdmkEdit
        Left = 8
        Top = 24
        Width = 177
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdCPF1Change
        OnExit = EdCPF1Exit
      end
      object DBEdEmitente: TDBEdit
        Left = 188
        Top = 0
        Width = 489
        Height = 21
        TabStop = False
        DataField = 'Nome'
        DataSource = DsEmiSac
        TabOrder = 1
        Visible = False
        OnChange = DBEdEmitenteChange
      end
      object EdNomeEmiSac: TdmkEdit
        Left = 188
        Top = 24
        Width = 489
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 679
    Height = 298
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object PainelControle: TPanel
      Left = 1
      Top = 249
      Width = 677
      Height = 48
      Align = alBottom
      TabOrder = 1
      object LaRegistro: TLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'ltimo registro'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
          NumGlyphs = 2
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Pr'#243'ximo registro'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
          NumGlyphs = 2
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Registro anterior'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
          NumGlyphs = 2
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Primeiro registro'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
          NumGlyphs = 2
        end
      end
      object Panel3: TPanel
        Left = 207
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
          NumGlyphs = 2
        end
        object BtEmiSac: TBitBtn
          Tag = 110
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Emi/Sac'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtEmiSacClick
          NumGlyphs = 2
        end
        object BtGrupo: TBitBtn
          Tag = 243
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Grupo'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtGrupoClick
          NumGlyphs = 2
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 677
      Height = 52
      Align = alTop
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 120
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsGruSacEmi
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 120
        Top = 24
        Width = 549
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsGruSacEmi
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 90
      Width = 677
      Height = 159
      Align = alBottom
      DataSource = DsGruSacEmiIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Nome'
          Title.Caption = 'Sacado / Emitente'
          Width = 533
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CNPJ_CPF_TXT'
          Title.Caption = 'CNPJ / CPF'
          Width = 113
          Visible = True
        end>
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 679
    Height = 298
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 249
      Width = 677
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Default = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
      object BtDesiste: TBitBtn
        Tag = 15
        Left = 584
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
        Caption = '&Desiste'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtDesisteClick
        NumGlyphs = 2
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 677
      Height = 196
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 120
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNome: TdmkEdit
        Left = 120
        Top = 24
        Width = 300
        Height = 21
        MaxLength = 255
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 679
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Grupos de Sac/Emi'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 596
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 370
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 0
        NumGlyphs = 2
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 1
        NumGlyphs = 2
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 2
        OnClick = SbNumeroClick
        NumGlyphs = 2
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 3
        OnClick = SbNomeClick
        NumGlyphs = 2
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        TabOrder = 4
        OnClick = SbQueryClick
        NumGlyphs = 2
      end
    end
  end
  object DsGruSacEmi: TDataSource
    DataSet = QrGruSacEmi
    Left = 488
    Top = 161
  end
  object QrGruSacEmi: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrGruSacEmiBeforeOpen
    AfterOpen = QrGruSacEmiAfterOpen
    AfterScroll = QrGruSacEmiAfterScroll
    SQL.Strings = (
      'SELECT * FROM grusacemi'
      'WHERE Codigo > 0')
    Left = 460
    Top = 161
    object QrGruSacEmiLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrGruSacEmiDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrGruSacEmiDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrGruSacEmiUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrGruSacEmiUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrGruSacEmiCodigo: TSmallintField
      FieldName = 'Codigo'
    end
    object QrGruSacEmiNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object PMGrupo: TPopupMenu
    Left = 224
    Top = 272
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
    end
  end
  object PMEmiSac: TPopupMenu
    Left = 312
    Top = 272
    object Adiciona1: TMenuItem
      Caption = '&Adiciona'
      OnClick = Adiciona1Click
    end
    object Remove1: TMenuItem
      Caption = '&Remove'
      OnClick = Remove1Click
    end
    object Edita1: TMenuItem
      Caption = '&Edita'
      OnClick = Edita1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Nomeiagrupo1: TMenuItem
      Caption = '&Renomeia grupo'
      OnClick = Nomeiagrupo1Click
    end
  end
  object QrEmiSac: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT Nome, CPF, "Emitente" Tipo FROM emitcpf'
      'WHERE CPF = :P0'
      'UNION'
      'SELECT DISTINCT Nome, CNPJ CPF, "Sacado" Tipo FROM sacados'
      'WHERE CNPJ= :P1'
      ''
      'ORDER BY Nome')
    Left = 136
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEmiSacNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrEmiSacCPF: TWideStringField
      FieldName = 'CPF'
      Required = True
      Size = 15
    end
    object QrEmiSacTipo: TWideStringField
      FieldName = 'Tipo'
      Required = True
      Size = 8
    end
  end
  object DsEmiSac: TDataSource
    DataSet = QrEmiSac
    Left = 164
    Top = 200
  end
  object QrGruSacEmiIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrGruSacEmiItsCalcFields
    SQL.Strings = (
      'SELECT * FROM grusacemiits'
      'WHERE Codigo =:P0'
      'ORDER BY Nome')
    Left = 460
    Top = 189
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGruSacEmiItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGruSacEmiItsCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 15
    end
    object QrGruSacEmiItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrGruSacEmiItsCNPJ_CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_CPF_TXT'
      Size = 40
      Calculated = True
    end
  end
  object DsGruSacEmiIts: TDataSource
    DataSet = QrGruSacEmiIts
    Left = 488
    Top = 189
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CNPJ_CPF'
      'FROM grusacemiits'
      'WHERE CNPJ_CPF=:P0'
      'AND Codigo=:P1')
    Left = 213
    Top = 109
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 15
    end
  end
end
