object FmOperaImp: TFmOperaImp
  Left = 419
  Top = 217
  Caption = 'Impress'#227'o de Operac'#245'es'
  ClientHeight = 373
  ClientWidth = 597
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 597
    Height = 277
    Align = alClient
    TabOrder = 0
    object Panel1: TPanel
      Left = 1
      Top = 68
      Width = 595
      Height = 137
      Align = alTop
      Caption = 'Panel1'
      TabOrder = 0
      object Panel2: TPanel
        Left = 1
        Top = 1
        Width = 432
        Height = 135
        Align = alLeft
        Caption = 'Panel1'
        TabOrder = 0
        object RGPeriodo: TRadioGroup
          Left = 1
          Top = 1
          Width = 430
          Height = 133
          Align = alClient
          Caption = ' Configura'#231#227'o inicial do per'#237'odo: '
          Items.Strings = (
            #218'ltimos sete dias.'
            'Semanal:'
            'Decendial')
          TabOrder = 0
        end
        object CBDiaSemanaI: TComboBox
          Left = 76
          Top = 61
          Width = 173
          Height = 21
          Hint = 'Nome da fonte|Seleciona o nome da fonte'
          Ctl3D = False
          DropDownCount = 10
          ParentCtl3D = False
          TabOrder = 1
        end
        object StaticText1: TStaticText
          Left = 76
          Top = 45
          Width = 52
          Height = 13
          AutoSize = False
          Caption = 'Dia inicial:'
          TabOrder = 2
        end
        object CBDiaSemanaF: TComboBox
          Left = 252
          Top = 61
          Width = 173
          Height = 21
          Hint = 'Nome da fonte|Seleciona o nome da fonte'
          Ctl3D = False
          DropDownCount = 10
          ParentCtl3D = False
          TabOrder = 3
        end
        object StaticText2: TStaticText
          Left = 252
          Top = 45
          Width = 45
          Height = 13
          AutoSize = False
          Caption = 'Dia final:'
          TabOrder = 4
        end
      end
      object Panel3: TPanel
        Left = 433
        Top = 1
        Width = 161
        Height = 135
        Align = alClient
        TabOrder = 1
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 1
      Width = 595
      Height = 67
      Align = alTop
      Caption = 'Panel4'
      TabOrder = 1
      object Label3: TLabel
        Left = 8
        Top = 8
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label34: TLabel
        Left = 384
        Top = 8
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object Label1: TLabel
        Left = 488
        Top = 8
        Width = 48
        Height = 13
        Caption = 'Data final:'
      end
      object EdCliente: TdmkEditCB
        Left = 8
        Top = 24
        Width = 57
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CbCliente
        IgnoraDBLookupComboBox = False
      end
      object CbCliente: TdmkDBLookupComboBox
        Left = 68
        Top = 24
        Width = 313
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECLIENTE'
        ListSource = DsClientes
        TabOrder = 1
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPIni: TDateTimePicker
        Left = 384
        Top = 24
        Width = 101
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 2
      end
      object TPFim: TDateTimePicker
        Left = 488
        Top = 24
        Width = 101
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 3
      end
    end
    object Panel5: TPanel
      Left = 1
      Top = 205
      Width = 595
      Height = 71
      Align = alClient
      TabOrder = 2
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 325
    Width = 597
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtImprime: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Confirma'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtImprimeClick
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 490
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 597
    Height = 48
    Align = alTop
    Caption = 'Impress'#227'o de Operac'#245'es'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 595
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 593
      ExplicitHeight = 44
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo'
      'FROM entidades'
      'WHERE Cliente1='#39'V'#39
      'ORDER BY NOMECLIENTE')
    Left = 288
    Top = 64
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 316
    Top = 64
  end
  object QrOpera: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOperaCalcFields
    SQL.Strings = (
      'SELECT lo.*, IF(en.Tipo=0, en.RazaoSocial, en.Nome)NOMECLIENTE, '
      'IF(en.Tipo=0, en.CNPJ, en.CPF) CNPJCPF'
      'FROM lotes lo'
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente'
      'WHERE lo.data BETWEEN :P0 AND :P1'
      'AND (TxCompra+ValValorem)>=0.01'
      'ORDER BY lo.Data, NOMECLIENTE, lo.Lote')
    Left = 176
    Top = 36
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOperaNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrOperaCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOperaTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrOperaSpread: TSmallintField
      FieldName = 'Spread'
      Required = True
    end
    object QrOperaCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOperaLote: TSmallintField
      FieldName = 'Lote'
      Required = True
    end
    object QrOperaData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrOperaTotal: TFloatField
      FieldName = 'Total'
      Required = True
    end
    object QrOperaDias: TFloatField
      FieldName = 'Dias'
      Required = True
    end
    object QrOperaPeCompra: TFloatField
      FieldName = 'PeCompra'
      Required = True
    end
    object QrOperaTxCompra: TFloatField
      FieldName = 'TxCompra'
      Required = True
    end
    object QrOperaValValorem: TFloatField
      FieldName = 'ValValorem'
      Required = True
    end
    object QrOperaAdValorem: TFloatField
      FieldName = 'AdValorem'
      Required = True
    end
    object QrOperaIOC: TFloatField
      FieldName = 'IOC'
      Required = True
    end
    object QrOperaCPMF: TFloatField
      FieldName = 'CPMF'
      Required = True
    end
    object QrOperaTipoAdV: TIntegerField
      FieldName = 'TipoAdV'
      Required = True
    end
    object QrOperaIRRF: TFloatField
      FieldName = 'IRRF'
      Required = True
    end
    object QrOperaIRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
      Required = True
    end
    object QrOperaISS: TFloatField
      FieldName = 'ISS'
      Required = True
    end
    object QrOperaISS_Val: TFloatField
      FieldName = 'ISS_Val'
      Required = True
    end
    object QrOperaPIS_R: TFloatField
      FieldName = 'PIS_R'
      Required = True
    end
    object QrOperaPIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
      Required = True
    end
    object QrOperaLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOperaDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOperaDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOperaUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOperaUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOperaTarifas: TFloatField
      FieldName = 'Tarifas'
      Required = True
    end
    object QrOperaOcorP: TFloatField
      FieldName = 'OcorP'
      Required = True
    end
    object QrOperaCOFINS_R: TFloatField
      FieldName = 'COFINS_R'
      Required = True
    end
    object QrOperaCOFINS_R_Val: TFloatField
      FieldName = 'COFINS_R_Val'
      Required = True
    end
    object QrOperaPIS: TFloatField
      FieldName = 'PIS'
      Required = True
    end
    object QrOperaPIS_Val: TFloatField
      FieldName = 'PIS_Val'
      Required = True
    end
    object QrOperaCOFINS: TFloatField
      FieldName = 'COFINS'
      Required = True
    end
    object QrOperaCOFINS_Val: TFloatField
      FieldName = 'COFINS_Val'
      Required = True
    end
    object QrOperaPIS_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PIS_TOTAL'
      Calculated = True
    end
    object QrOperaCOFINS_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'COFINS_TOTAL'
      Calculated = True
    end
    object QrOperaIOC_VAL: TFloatField
      FieldName = 'IOC_VAL'
      Required = True
    end
    object QrOperaCPMF_VAL: TFloatField
      FieldName = 'CPMF_VAL'
      Required = True
    end
    object QrOperaMaxVencto: TDateField
      FieldName = 'MaxVencto'
      Required = True
    end
    object QrOperaCHDevPg: TFloatField
      FieldName = 'CHDevPg'
      Required = True
    end
    object QrOperaMINTC: TFloatField
      FieldName = 'MINTC'
      Required = True
    end
    object QrOperaMINAV: TFloatField
      FieldName = 'MINAV'
      Required = True
    end
    object QrOperaMINTC_AM: TFloatField
      FieldName = 'MINTC_AM'
      Required = True
    end
    object QrOperaPIS_T_Val: TFloatField
      FieldName = 'PIS_T_Val'
      Required = True
    end
    object QrOperaCOFINS_T_Val: TFloatField
      FieldName = 'COFINS_T_Val'
      Required = True
    end
    object QrOperaPgLiq: TFloatField
      FieldName = 'PgLiq'
      Required = True
    end
    object QrOperaDUDevPg: TFloatField
      FieldName = 'DUDevPg'
      Required = True
    end
    object QrOperaNF: TIntegerField
      FieldName = 'NF'
      Required = True
    end
    object QrOperaItens: TIntegerField
      FieldName = 'Itens'
      Required = True
    end
    object QrOperaCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrOperaIOFd_VAL: TFloatField
      FieldName = 'IOFd_VAL'
      Required = True
    end
    object QrOperaIOFv_VAL: TFloatField
      FieldName = 'IOFv_VAL'
      Required = True
    end
    object QrOperaTipoIOF: TSmallintField
      FieldName = 'TipoIOF'
      Required = True
    end
    object QrOperaCNPJCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJCPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrOperaIOF_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'IOF_TOTAL'
      Calculated = True
    end
  end
  object frxOperacoes: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39451.850826631900000000
    ReportOptions.LastChange = 39451.850826631900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxOperacoesGetValue
    Left = 384
    Top = 120
    Datasets = <
      item
        DataSet = Dmod.frxDsControle
        DataSetName = 'frxDsControle'
      end
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsOperacoes
        DataSetName = 'frxDsOperacoes'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 56.692950000000000000
        Top = 18.897650000000000000
        Width = 1122.520410000000000000
        object Memo1: TfrxMemoView
          Left = 75.590600000000000000
          Top = 37.795300000000000000
          Width = 982.677800000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 143.622140000000000000
        Top = 98.267780000000000000
        Width = 1122.520410000000000000
        object Memo2: TfrxMemoView
          Left = 75.590600000000000000
          Top = 34.015770000000000000
          Width = 982.677800000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DAS OPERA'#199#213'ES REALIZADAS')
          ParentFont = False
        end
        object Shape1: TfrxShapeView
          Left = 75.590600000000000000
          Top = 56.692950000000000000
          Width = 982.677800000000000000
          Height = 22.677180000000000000
        end
        object Memo3: TfrxMemoView
          Left = 128.504020000000000000
          Top = 56.692950000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Tipo de opera'#231#227'o:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 245.669450000000000000
          Top = 56.692950000000000000
          Width = 136.063080000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Compra de Direitos')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 627.401980000000000000
          Top = 56.692950000000000000
          Width = 132.283550000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Servi'#231'os executados:')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 759.685530000000000000
          Top = 56.692950000000000000
          Width = 234.330860000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'An'#225'lise de Riscos e Liquidez')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 75.590600000000000000
          Top = 83.149660000000000000
          Width = 41.574830000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 117.165430000000000000
          Top = 83.149660000000000000
          Width = 721.890230000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 884.410020000000000000
          Top = 83.149660000000000000
          Width = 173.858380000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 839.055660000000000000
          Top = 83.149660000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haRight
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 75.590600000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data'
            'Opera'#231#227'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 124.724490000000000000
          Top = 109.606370000000000000
          Width = 102.047310000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'CNPJ ou CPF'
            'Contratante')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 226.771800000000000000
          Top = 109.606370000000000000
          Width = 215.433210000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cliente'
            'Contratante')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 442.205010000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186
            'Border'#244)
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 491.338900000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'N'#186
            'N.F.')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 540.472790000000000000
          Top = 109.606370000000000000
          Width = 68.031540000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Valor'
            'Negociado')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 608.504330000000000000
          Top = 109.606370000000000000
          Width = 56.692950000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Fator de'
            'Compra')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 665.197280000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ad'
            'Valorem')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 714.331170000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsControle."Moeda"]'
            'PIS')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 763.465060000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsControle."Moeda"]'
            'ISS')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 812.598950000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsControle."Moeda"]'
            'COFINS')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 861.732840000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsControle."Moeda"]'
            'IOC')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 910.866730000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsControle."Moeda"]'
            'IOF')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 960.000620000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsControle."Moeda"]'
            'IOF adicio.')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 1009.134510000000000000
          Top = 109.606370000000000000
          Width = 49.133890000000000000
          Height = 34.015770000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAL'
            '[frxDsControle."Moeda"] IOF')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 302.362400000000000000
        Width = 1122.520410000000000000
        DataSet = frxDsOperacoes
        DataSetName = 'frxDsOperacoes'
        RowCount = 0
        object Memo14: TfrxMemoView
          Left = 75.590600000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataField = 'Data'
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOperacoes."Data"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 124.724490000000000000
          Width = 102.047310000000000000
          Height = 18.897650000000000000
          DataField = 'CNPJCPF_TXT'
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOperacoes."CNPJCPF_TXT"]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 226.771800000000000000
          Width = 215.433210000000000000
          Height = 18.897650000000000000
          DataField = 'NOMECLIENTE'
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOperacoes."NOMECLIENTE"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 442.205010000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39',<frxDsOperacoes."Lote">)]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 491.338900000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39',<frxDsOperacoes."NF">)]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 540.472790000000000000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DataField = 'Total'
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOperacoes."Total"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 608.504330000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataField = 'MINTC'
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOperacoes."MINTC"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 665.197280000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataField = 'MINAV'
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOperacoes."MINAV"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 714.331170000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataField = 'PIS_T_Val'
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOperacoes."PIS_T_Val"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 763.465060000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataField = 'ISS_Val'
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOperacoes."ISS_Val"]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 812.598950000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataField = 'COFINS_T_Val'
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOperacoes."COFINS_T_Val"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 861.732840000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataField = 'IOC_VAL'
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOperacoes."IOC_VAL"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 910.866730000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataField = 'IOFd_VAL'
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOperacoes."IOFd_VAL"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 960.000620000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataField = 'IOFv_VAL'
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOperacoes."IOFv_VAL"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 1009.134510000000000000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataField = 'IOF_TOTAL'
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOperacoes."IOF_TOTAL"]')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 71.811070000000000000
        Top = 343.937230000000000000
        Width = 1122.520410000000000000
        object Memo41: TfrxMemoView
          Left = 540.472790000000000000
          Top = 3.779530000000020000
          Width = 68.031540000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOperacoes."Total">,MasterData1)]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 608.504330000000000000
          Top = 3.779530000000020000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOperacoes."MINTC">,MasterData1)]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 665.197280000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOperacoes."MINAV">,MasterData1)]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 714.331170000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOperacoes."PIS_T_Val">,MasterData1)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 763.465060000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOperacoes."ISS_Val">,MasterData1)]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 812.598950000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOperacoes."COFINS_T_Val">,MasterData1)]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 861.732840000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOperacoes."IOC_Val">,MasterData1)]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 910.866730000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOperacoes."IOFd_Val">,MasterData1)]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 960.000620000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOperacoes."IOFv_Val">,MasterData1)]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 1009.134510000000000000
          Top = 3.779530000000020000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsOperacoes."IOF_TOTAL">,MasterData1)]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 75.590600000000000000
          Top = 3.779530000000020000
          Width = 464.882190000000000000
          Height = 18.897650000000000000
          DataSet = frxDsOperacoes
          DataSetName = 'frxDsOperacoes'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            ' TOTAL DA PESQUISA')
          ParentFont = False
        end
      end
    end
  end
  object frxDsOperacoes: TfrxDBDataset
    UserName = 'frxDsOperacoes'
    CloseDataSource = False
    DataSet = QrOpera
    BCDToCurrency = False
    Left = 412
    Top = 120
  end
end
