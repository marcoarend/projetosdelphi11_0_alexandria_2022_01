unit Deposito;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, Db, mySQLDbTables,
  ComCtrls, Mask, DBCtrls, Menus, dmkEdit, dmkGeral, UnDmkProcFunc, UnDmkEnums;

type
  TFmDeposito = class(TForm)
    PainelConfirma: TPanel;
    BtDeposito: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrCheques: TmySQLQuery;
    QrChequesCodigo: TIntegerField;
    QrChequesControle: TIntegerField;
    QrChequesComp: TIntegerField;
    QrChequesBanco: TIntegerField;
    QrChequesAgencia: TIntegerField;
    QrChequesConta: TWideStringField;
    QrChequesCheque: TIntegerField;
    QrChequesCPF: TWideStringField;
    QrChequesEmitente: TWideStringField;
    QrChequesBruto: TFloatField;
    QrChequesDesco: TFloatField;
    QrChequesValor: TFloatField;
    QrChequesEmissao: TDateField;
    QrChequesDCompra: TDateField;
    QrChequesDDeposito: TDateField;
    QrChequesVencto: TDateField;
    QrChequesTxaCompra: TFloatField;
    QrChequesTxaJuros: TFloatField;
    QrChequesTxaAdValorem: TFloatField;
    QrChequesVlrCompra: TFloatField;
    QrChequesVlrAdValorem: TFloatField;
    QrChequesDMais: TIntegerField;
    QrChequesDias: TIntegerField;
    QrChequesDuplicata: TWideStringField;
    QrChequesDevolucao: TIntegerField;
    QrChequesQuitado: TIntegerField;
    QrChequesLk: TIntegerField;
    QrChequesDataCad: TDateField;
    QrChequesDataAlt: TDateField;
    QrChequesUserCad: TIntegerField;
    QrChequesUserAlt: TIntegerField;
    QrChequesPraca: TIntegerField;
    QrChequesBcoCobra: TIntegerField;
    QrChequesAgeCobra: TIntegerField;
    QrChequesTotalJr: TFloatField;
    QrChequesTotalDs: TFloatField;
    QrChequesTotalPg: TFloatField;
    QrChequesData3: TDateField;
    QrChequesProrrVz: TIntegerField;
    QrChequesProrrDd: TIntegerField;
    QrChequesRepassado: TSmallintField;
    QrChequesDepositado: TSmallintField;
    QrChequesValQuit: TFloatField;
    QrChequesValDeposito: TFloatField;
    QrChequesTipo: TIntegerField;
    QrChequesAliIts: TIntegerField;
    QrChequesAlinPgs: TIntegerField;
    QrChequesNaoDeposita: TSmallintField;
    QrChequesReforcoCxa: TSmallintField;
    QrChequesCartDep: TIntegerField;
    QrChequesCobranca: TIntegerField;
    QrChequesRepCli: TIntegerField;
    QrChequesNOMECLI: TWideStringField;
    QrChequesLote: TIntegerField;
    DsCheques: TDataSource;
    DBG1: TDBGrid;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel7: TPanel;
    CkDescende1: TCheckBox;
    CkDescende2: TCheckBox;
    CkDescende3: TCheckBox;
    Panel8: TPanel;
    Label24: TLabel;
    EdSoma: TdmkEdit;
    Panel6: TPanel;
    RGOrdem3: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem1: TRadioGroup;
    Panel3: TPanel;
    TPVencto: TDateTimePicker;
    Label1: TLabel;
    BtRefresh: TBitBtn;
    Timer1: TTimer;
    QrSoma: TmySQLQuery;
    DsSoma: TDataSource;
    QrSomaVALOR: TFloatField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    PMDeposito: TPopupMenu;
    Itemselecionado1: TMenuItem;
    Determinarperodo1: TMenuItem;
    QrMin: TmySQLQuery;
    QrMinVencto: TDateField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrChequesAfterOpen(DataSet: TDataSet);
    procedure QrChequesAfterClose(DataSet: TDataSet);
    procedure BtRefreshClick(Sender: TObject);
    procedure TPVenctoChange(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure DBG1CellClick(Column: TColumn);
    procedure DBG1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtDepositoClick(Sender: TObject);
    procedure Itemselecionado1Click(Sender: TObject);
    procedure Determinarperodo1Click(Sender: TObject);
  private
    { Private declarations }
    FAscendente: Boolean;
    procedure ReopenCheques(Controle: Integer);
    procedure FechaQrCheques;
    procedure SomaLinhas(Ascendente: Boolean);
    procedure PreparaSoma(Ascendente: Boolean);
    procedure DepositaSelecionados;
    procedure DepositaAtual;
    //procedure Deposita;
  public
    { Public declarations }
  end;

  var
  FmDeposito: TFmDeposito;

implementation

uses Module, DataDef, UnInternalConsts, UnMyObjects;

{$R *.DFM}

procedure TFmDeposito.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDeposito.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmDeposito.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmDeposito.FormCreate(Sender: TObject);
begin
  TPVencto.Date := Date;
  ReopenCheques(0);
end;

procedure TFmDeposito.QrChequesAfterOpen(DataSet: TDataSet);
begin
  BtRefresh.Enabled := False;
end;

procedure TFmDeposito.QrChequesAfterClose(DataSet: TDataSet);
begin
  BtRefresh.Enabled := True;
end;

procedure TFmDeposito.BtRefreshClick(Sender: TObject);
begin
  if QrCheques.State = dsBrowse then
    ReopenCheques(QrChequesControle.Value)
  else
    ReopenCheques(0);
end;

procedure TFmDeposito.TPVenctoChange(Sender: TObject);
begin
  FechaQrCheques;
end;

procedure TFmDeposito.RGOrdem1Click(Sender: TObject);
begin
  FechaQrCheques;
end;

procedure TFmDeposito.FechaQrCheques;
begin
  QrCheques.Close;
  QrSoma.Close;
end;

procedure TFmDeposito.SomaLinhas(Ascendente: Boolean);
var
  Valor: Double;
  i, k: Integer;
begin
  k := QrChequesControle.Value;
  if DBG1.SelectedRows.Count = 0 then Valor := QrChequesValor.Value else
  begin
    Valor := 0;
    with DBG1.DataSource.DataSet do
    begin
      if Ascendente then
      begin
        for i:= 0 to DBG1.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          Valor := Valor + QrChequesValor.Value;
        end;
      end else begin
        for i:= DBG1.SelectedRows.Count-1 downto 0 do
        begin
          GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          Valor := Valor + QrChequesValor.Value;
        end;
      end;
    end;
    QrCheques.Locate('Controle', k, []);
  end;
  EdSoma.Text := Geral.FFT(Valor, 2, siNegativo);
end;

procedure TFmDeposito.Timer1Timer(Sender: TObject);
begin
  SomaLinhas(FAscendente);
end;

procedure TFmDeposito.DBG1CellClick(Column: TColumn);
begin
  SomaLinhas(True);
end;

procedure TFmDeposito.DBG1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_DOWN) and (Shift=[ssShift]) then PreparaSoma(True)  else
  if (Key=VK_UP)   and (Shift=[ssShift]) then PreparaSoma(False) else
  if (Key=VK_DOWN)                       then PreparaSoma(True)  else
  if (Key=VK_UP)                         then PreparaSoma(False);
end;

procedure TFmDeposito.PreparaSoma(Ascendente: Boolean);
begin
  FAscendente := Ascendente;
  if Timer1.Enabled then Timer1.Enabled := False;
  Timer1.Enabled := True;
end;

procedure TFmDeposito.ReopenCheques(Controle: Integer);
const
  NomeA: array[0..5] of string = ('DDeposito','Valor','Cheque','Banco , Agencia , Conta ','Emitente','NOMECLI');
  NomeD: array[0..5] of string = ('DDeposito DESC','Cheque DESC','Valor DESC','Banco DESC, Agencia DESC, Conta DESC','Emitente','NOMECLI');
var
  Ordem: String;
begin
  Ordem := 'ORDER BY ';
  if CkDescende1.Checked then
    Ordem := Ordem + NomeD[RGOrdem1.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem1.ItemIndex]+', ';
  if CkDescende2.Checked then
    Ordem := Ordem + NomeD[RGOrdem2.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem2.ItemIndex]+', ';
  if CkDescende3.Checked then
    Ordem := Ordem + NomeD[RGOrdem3.ItemIndex]
  else
    Ordem := Ordem + NomeA[RGOrdem3.ItemIndex];
  //
  QrSoma.Close;
  QrCheques.Close;
  QrCheques.SQL.Clear;
  QrCheques.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrCheques.SQL.Add('ELSE ent.Nome END NOMECLI, lot.Lote, loi.*');
  QrCheques.SQL.Add('FROM lotesits loi');
  QrCheques.SQL.Add('LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo');
  QrCheques.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente');
  QrCheques.SQL.Add('WHERE lot.Tipo = 0');
  QrCheques.SQL.Add('AND loi.RepCli = 0');
  QrCheques.SQL.Add('AND loi.Repassado = 0');
  QrCheques.SQL.Add('AND loi.Devolucao = 0');
  QrCheques.SQL.Add('AND loi.Depositado = 0');
  QrCheques.SQL.Add('');
  QrCheques.SQL.Add(dmkPF.SQL_Periodo('AND loi.Vencto ', 0, Date, False, True));
  //
  QrCheques.SQL.Add(Ordem);
  QrCheques.Open;
  //
  //
  QrSoma.SQL.Clear;
  QrSoma.SQL.Add('SELECT SUM(Valor) Valor');
  QrSoma.SQL.Add('FROM lotesits loi');
  QrSoma.SQL.Add('LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo');
  QrSoma.SQL.Add('WHERE lot.Tipo = 0');
  QrSoma.SQL.Add('AND loi.RepCli = 0');
  QrSoma.SQL.Add('AND loi.Repassado = 0');
  QrSoma.SQL.Add('AND loi.Devolucao = 0');
  QrSoma.SQL.Add('AND loi.Depositado = 0');
  QrSoma.SQL.Add('');
  QrSoma.SQL.Add(dmkPF.SQL_Periodo('AND loi.Vencto ', 0, Date, False, True));
  QrSoma.Open;
end;

procedure TFmDeposito.BtDepositoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDeposito, BtDeposito);
end;

procedure TFmDeposito.Itemselecionado1Click(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  if DBG1.SelectedRows.Count = 0 then
    DepositaAtual
  else
    DepositaSelecionados;
  ReopenCheques(0);
  Screen.Cursor := crDefault;
end;

procedure TFmDeposito.DepositaSelecionados;
var
  i : Integer;
begin
  with DBG1.DataSource.DataSet do
  for i:= 0 to DBG1.SelectedRows.Count-1 do
  begin
    GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
    DepositaAtual;
  end;
end;

procedure TFmDeposito.DepositaAtual;
begin
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE lotesits SET AlterWeb=1,   Depositado=1 WHERE Controle=:P0 ');
  Dmod.QrUpdM.Params[0].AsInteger := QrChequesControle.Value;
  //
  Dmod.QrUpdM.ExecSQL;
end;

procedure TFmDeposito.Determinarperodo1Click(Sender: TObject);
begin
  QrMin.Close;
  QrMin.Open;
  MLAGeral.EscolhePeriodo(TFmDataDef, FmDataDef, QrMinVencto.Value, Date-2);
  if VAR_DATA_FIN_ESCOLHIDA > 0 then
  begin
    Dmod.QrUpdM.SQL.Clear;
    Dmod.QrUpdM.SQL.Add('UPDATE lotesits SET AlterWeb=1,  loi, Lotes lot');
    Dmod.QrUpdM.SQL.Add('SET loi.Depositado = 1');
    Dmod.QrUpdM.SQL.Add('WHERE lot.Codigo=loi.Codigo');
    Dmod.QrUpdM.SQL.Add('AND lot.Tipo=0');
    Dmod.QrUpdM.SQL.Add('AND loi.RepCli = 0');
    Dmod.QrUpdM.SQL.Add('AND loi.Repassado = 0');
    Dmod.QrUpdM.SQL.Add('AND loi.Devolucao = 0');
    Dmod.QrUpdM.SQL.Add('AND loi.Depositado = 0');
    Dmod.QrUpdM.SQL.Add(dmkPF.SQL_Periodo('AND loi.Vencto ',
      VAR_DATA_INI_ESCOLHIDA, VAR_DATA_FIN_ESCOLHIDA, True, True));
    Dmod.QrUpdM.ExecSQL;
    VAR_DATA_INI_ESCOLHIDA := 0;
    VAR_DATA_FIN_ESCOLHIDA := 0;
    //
    ReopenCheques(0);
  end;
end;

end.

