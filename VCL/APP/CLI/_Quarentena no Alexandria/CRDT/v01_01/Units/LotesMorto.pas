unit LotesMorto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls, UnMLAGeral, UnInternalConsts, ComCtrls, Db,
  mySQLDbTables, Mask, DBCtrls, Grids, DBGrids, dmkGeral, dmkEdit, UnDmkEnums;

type
  TFmLotesMorto = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    BtSair: TBitBtn;
    BtPesquisa: TBitBtn;
    QrSoma: TmySQLQuery;
    QrSomaValor: TFloatField;
    Panel2: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    Label6: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    TPLimite: TDateTimePicker;
    EdAllLot: TdmkEdit;
    EdAllLoi: TdmkEdit;
    EdAllMer: TdmkEdit;
    EdAllTic: TdmkEdit;
    EdAllPag: TdmkEdit;
    Progress: TProgressBar;
    EdAllMot: TdmkEdit;
    EdAll: TdmkEdit;
    EdAntLot: TdmkEdit;
    EdAntLoi: TdmkEdit;
    EdAntMer: TdmkEdit;
    EdAntTic: TdmkEdit;
    EdAntPag: TdmkEdit;
    EdAntMot: TdmkEdit;
    EdAnt: TdmkEdit;
    EdReducao: TdmkEdit;
    DBGrid1: TDBGrid;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel6: TPanel;
    ST1: TStaticText;
    ST2: TStaticText;
    ST3: TStaticText;
    ST4: TStaticText;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText4: TStaticText;
    StaticText5: TStaticText;
    Qr1: TmySQLQuery;
    Qr3: TmySQLQuery;
    Qr2: TmySQLQuery;
    BtArquivoMorto: TBitBtn;
    QrQtd: TmySQLQuery;
    QrQtdRegistros: TFloatField;
    QrULot: TmySQLQuery;
    QrSumEmi: TmySQLQuery;
    QrSumCli: TmySQLQuery;
    QrSumEmiCPF: TWideStringField;
    QrSumEmiValor: TFloatField;
    QrSumEmiItens: TLargeintField;
    QrSumCliCliente: TIntegerField;
    QrSumCliValor: TFloatField;
    QrSumCliItens: TLargeintField;
    Timer1: TTimer;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtSairClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TPLimiteChange(Sender: TObject);
    procedure TPLimiteClick(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtPararClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtArquivoMortoClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    FExportando: Boolean;
    FConta, FTodos: Integer;
    FTempo: TDateTime;
    procedure CalculaPerformance;
    //
    procedure AtualizaSaldos;
    procedure AtualizaTempos;
  public
    { Public declarations }
  end;

var
  FmLotesMorto: TFmLotesMorto;

const
  AtenCli = 'Status transfer�ncia cliente %s:';

implementation

uses Module, Principal, UMySQLModule, UnMyObjects;

{$R *.DFM}

procedure TFmLotesMorto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmLotesMorto.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 31);
end;

procedure TFmLotesMorto.BtSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLotesMorto.FormCreate(Sender: TObject);
var
  Conta: Integer;
begin
  ST1.Caption := '...';
  ST2.Caption := '00:00:00';
  ST3.Caption := '00:00:00';
  ST4.Caption := '00:00:00';
  (*if Application.MessageBox(PChar('� altamente recomend�vel atualizar os '+
  'saldos dos clientes antes de prosseguir. Deseja faz�-lo agora?'), 'Pergunta',
  MB_YESNO+MB_ICONQUESTION) = ID_YES then AtualizaSaldos;*)
  //////////////////////////////////////////////////////////////////////////////
  TPLimite.Date    := Int(Date) - Dmod.QrControleddArqMorto.Value;
  TPLimite.MaxDate := Int(Date) - Dmod.QrControleddArqMorto.Value;
  Conta := 0;
  //
  QrQtd.Close;
  QrQtd.SQL.Clear;
  QrQtd.SQL.Add('SELECT COUNT(Codigo)+0.000 Registros');
  QrQtd.SQL.Add('FROM lotes');
  QrQtd.Open;
  Conta := Conta + Trunc(QrQtdRegistros.Value);
  EdAllLot.Text := IntToStr(Trunc(QrQtdRegistros.Value));
  //
  QrQtd.Close;
  QrQtd.SQL.Clear;
  QrQtd.SQL.Add('SELECT COUNT(Controle)+0.000 Registros');
  QrQtd.SQL.Add('FROM lotesits');
  QrQtd.Open;
  Conta := Conta + Trunc(QrQtdRegistros.Value);
  EdAllLoi.Text := IntToStr(Trunc(QrQtdRegistros.Value));
  //
  (*QrQtd.Close;
  QrQtd.SQL.Clear;
  QrQtd.SQL.Add('SELECT COUNT(vl.Codigo)+0.000 Registros');
  QrQtd.SQL.Add('FROM vendas ve, VendasMot vl');
  QrQtd.SQL.Add('WHERE vl.Codigo=ve.Codigo');
  QrQtd.Open;
  Conta := Conta + Trunc(QrQtdRegistros.Value);
  EdAllMot.Text := IntToStr(Trunc(QrQtdRegistros.Value));
  //
  QrQtd.Close;
  QrQtd.SQL.Clear;
  QrQtd.SQL.Add('SELECT COUNT(vl.Codigo)+0.000 Registros');
  QrQtd.SQL.Add('FROM vendas ve, VendasMotD vl');
  QrQtd.SQL.Add('WHERE vl.Codigo=ve.Codigo');
  QrQtd.Open;
  Conta := Conta + Trunc(QrQtdRegistros.Value);
  EdAllMot.Text := IntToStr(Trunc(QrQtdRegistros.Value)+Geral.IMV(EdAllMot.Text));
  //
  QrQtd.Close;
  QrQtd.SQL.Clear;
  QrQtd.SQL.Add('SELECT COUNT(vl.Codigo)+0.000 Registros');
  QrQtd.SQL.Add('FROM vendas ve, VendasPro vl');
  QrQtd.SQL.Add('WHERE vl.Codigo=ve.Codigo');
  QrQtd.Open;
  Conta := Conta + Trunc(QrQtdRegistros.Value);
  EdAllMer.Text := IntToStr(Trunc(QrQtdRegistros.Value));
  //
  QrQtd.Close;
  QrQtd.SQL.Clear;
  QrQtd.SQL.Add('SELECT COUNT(vl.Codigo)+0.000 Registros');
  QrQtd.SQL.Add('FROM vendas ve, VendasTic vl');
  QrQtd.SQL.Add('WHERE vl.Codigo=ve.Codigo');
  QrQtd.Open;
  Conta := Conta + Trunc(QrQtdRegistros.Value);
  EdAllTic.Text := IntToStr(Trunc(QrQtdRegistros.Value));
  //
  QrQtd.Close;
  QrQtd.SQL.Clear;
  QrQtd.SQL.Add('SELECT COUNT(la.Controle)+0.000 Registros');
  QrQtd.SQL.Add('FROM ' + VAR_LCT  + ' la, Vendas ve');
  QrQtd.SQL.Add('WHERE la.FatNum=ve.Codigo');
  QrQtd.SQL.Add('AND la.FatID=35');
  QrQtd.Open;
  Conta := Conta + Trunc(QrQtdRegistros.Value);
  EdAllPag.Text := IntToStr(Trunc(QrQtdRegistros.Value));*)

  //

  EdAll.Text := IntToStr(Conta);
  //
  CalculaPerformance;
  //
end;

procedure TFmLotesMorto.CalculaPerformance;
var
  Limite: String;
  Conta, Total: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    BtArquivoMorto.Enabled := True;
    Limite := FormatDateTime(VAR_FORMATDATE, TPLimite.Date);
    Conta := 0;
    //
    QrQtd.Close;
    QrQtd.SQL.Clear;
    QrQtd.SQL.Add('SELECT Count(Codigo)+0.000  Registros');
    QrQtd.SQL.Add('FROM lotes');
    QrQtd.SQL.Add('WHERE Codigo > 0');
    QrQtd.SQL.Add('AND Tipo = 0');
    QrQtd.SQL.Add('AND MaxVencto <=:P0');
    QrQtd.Params[0].AsString := Limite;
    QrQtd.Open;
    EdAntLot.Text := IntToStr(Trunc(QrQtdRegistros.Value));
    Conta := Conta + Trunc(QrQtdRegistros.Value);
    //
    QrQtd.Close;
    QrQtd.SQL.Clear;
    QrQtd.SQL.Add('SELECT COUNT(loi.Controle)+0.000 Registros');
    QrQtd.SQL.Add('FROM lotesits loi');
    QrQtd.SQL.Add('LEFT JOIN lotes lot ON lot.Codigo = loi.Codigo');
    QrQtd.SQL.Add('WHERE lot.Codigo > 0');
    QrQtd.SQL.Add('AND lot.Tipo = 0');
    QrQtd.SQL.Add('AND lot.MaxVencto <=:P0');
    QrQtd.Params[0].AsString := Limite;
    QrQtd.Open;
    EdAntLoi.Text := IntToStr(Trunc(QrQtdRegistros.Value));
    Conta := Conta + Trunc(QrQtdRegistros.Value);
    //
    EdAnt.Text := IntToStr(Conta);
    Total := Geral.IMV(EdAll.Text);
    if Total > 0 then
      EdReducao.Text := Geral.FFT(Conta / Total * 100, 2, siNegativo);
    //
    Progress.Position := 0;
    Update;
    Application.ProcessMessages;
    Screen.Cursor := crDefault;
  except;
    raise;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmLotesMorto.TPLimiteChange(Sender: TObject);
begin
  BtArquivoMorto.Enabled := False;
end;

procedure TFmLotesMorto.TPLimiteClick(Sender: TObject);
begin
  BtArquivoMorto.Enabled := False;
end;

procedure TFmLotesMorto.BtPesquisaClick(Sender: TObject);
var
  Data: String;
begin
  Screen.Cursor := crHourGlass;
  CalculaPerformance;
  Data := FormatDateTime(VAR_FORMATDATE, TPLimite.Date);
  Screen.Cursor := crDefault;
end;

procedure TFmLotesMorto.BtPararClick(Sender: TObject);
begin
  (*FParar := True;
  BtParar.Enabled := False;
  BtParar.Caption := 'Continu&ar';*)
end;

procedure TFmLotesMorto.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  //if BtParar.Enabled then Action := caNone;
end;

procedure TFmLotesMorto.BtArquivoMortoClick(Sender: TObject);
var
  Data: String;
  i: Integer;
begin
  Screen.Cursor := crHourGlass;
  Timer1.Enabled := True;
  Progress.Position := 0;
  FExportando := True;
  Data := Geral.FDT(TPLimite.Date, 1);
  //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  (*ST1.Caption := 'Guardando saldos atuais para futura compara��o...';
  Update;
  Application.ProcessMessages;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE entidades SET AlterWeb=1, TempA=TempD');
  Dmod.QrUpd.ExecSQL;*)
  //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
  ST1.Caption := 'Selecionando Emitentes do per�odo para atualiza��o...';
  Update;
  Application.ProcessMessages;
  Data := Geral.FDT(TPLimite.Date, 1);
  QrSumEmi.Close;
  QrSumEmi.Params[0].AsString := Data;
  QrSumEmi.Open;
  //
  ST1.Caption := 'Selecionando Clientes do per�odo para atualiza��o...';
  Update;
  Application.ProcessMessages;
  Data := Geral.FDT(TPLimite.Date, 1);
  QrSumCli.Close;
  QrSumCli.Params[0].AsString := Data;
  QrSumCli.Open;
  //
  Progress.Max := QrSumEmi.RecordCount + QrSumCli.RecordCount;
  //


  // LotesIts /////////////////////////////////////////////////////////////////
  Qr1.Database := Dmod.MyDB;
  Qr2.Database := Dmod.MyDB;
  Qr3.Database := Dmod.MyDB;
  ST1.Caption := 'LotesIts - Excluindo arquivo tempor�rio...';
  Update;
  Application.ProcessMessages;
  if FileExists('C:\Dermatek\LotesIts.txt') then
  DeleteFile('C:\Dermatek\LotesIts.txt');
  ST1.Caption := 'LotesIts - Exportando para arquivo tempor�rio...';
  Update;
  Application.ProcessMessages;
  Qr1.SQL.Clear;
  Qr1.SQL.Add('SELECT '+UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB,
    'LotezIts', 'loi.'));
  Qr1.SQL.Add('FROM lotesits loi');
  Qr1.SQL.Add('LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo');
  Qr1.SQL.Add('WHERE lot.Codigo>0');
  Qr1.SQL.Add('AND lot.Tipo = 0');
  Qr1.SQL.Add('AND lot.MaxVencto<= :P0');
  Qr1.SQL.Add('INTO OUTFILE "C:/Dermatek/LotesIts.txt"');
  Qr1.Params[0].AsString := Data;
  Qr1.ExecSQL;
  //
  ST1.Caption := 'LotesIts - Importando de arquivo tempor�rio...';
  Update;
  Application.ProcessMessages;
  Qr2.SQL.Clear;
  Qr2.SQL.Add('LOAD DATA LOCAL INFILE "C:/Dermatek/LotesIts.txt"');
  Qr2.SQL.Add('INTO Table LotezIts');
  Qr2.ExecSQL;
  //
  ST1.Caption := 'LotesIts - Excluindo registros exportados...';
  Update;
  Application.ProcessMessages;
  Qr3.SQL.Clear;
  Qr3.SQL.Add('DELETE FROM loi');
  Qr3.SQL.Add('USING LotesIts loi, Lotes lot');
  Qr3.SQL.Add('WHERE lot.Codigo=loi.Codigo');
  Qr3.SQL.Add('AND lot.Codigo > 0');
  Qr3.SQL.Add('AND lot.Tipo = 0');
  Qr3.SQL.Add('AND lot.MaxVencto<= :P0');
  Qr3.Params[0].AsString := Data;
  Qr3.ExecSQL;
  //
  if FileExists('C:\Dermatek\LotesIts.txt') then
  DeleteFile('C:\Dermatek\LotesIts.txt');
  //////////////////////////////////////////////////////////////////////////////

  // EmLotIts /////////////////////////////////////////////////////////////////
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs-1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      Qr1.Database := Dmod.DatabaseColigadas(i);
      Qr2.Database := Dmod.DatabaseColigadas(i);
      Qr3.Database := Dmod.DatabaseColigadas(i);
      ST1.Caption := 'EmLotIts - Excluindo arquivo tempor�rio...';
      Update;
      Application.ProcessMessages;
      if FileExists('C:\Dermatek\EmLotIts.txt') then
      DeleteFile('C:\Dermatek\EmLotIts.txt');
      ST1.Caption := 'EmLotIts - Exportando para arquivo tempor�rio...';
      Update;
      Application.ProcessMessages;
      Qr1.SQL.Clear;
      Qr1.SQL.Add('SELECT '+UMyMod.ObtemCamposDeTabelaIdentica(Dmod.DatabaseColigadas(i),
        'EzLotIts', 'loi.'));
      Qr1.SQL.Add('FROM emlotits loi');
      Qr1.SQL.Add('LEFT JOIN creditor.lotes lot ON lot.Codigo=loi.Codigo');
      Qr1.SQL.Add('WHERE lot.Codigo>0');
      Qr1.SQL.Add('AND lot.MaxVencto<= :P0');
      Qr1.SQL.Add('AND lot.Tipo = 0');
      Qr1.SQL.Add('INTO OUTFILE "C:/Dermatek/EmLotIts.txt"');
      Qr1.Params[0].AsString := Data;
      Qr1.ExecSQL;
      //
      ST1.Caption := 'EmLotIts - Importando de arquivo tempor�rio...';
      Update;
      Application.ProcessMessages;
      Qr2.SQL.Clear;
      Qr2.SQL.Add('LOAD DATA LOCAL INFILE "C:/Dermatek/EmLotIts.txt"');
      Qr2.SQL.Add('INTO Table EzLotIts');
      Qr2.ExecSQL;
      //
      ST1.Caption := 'EmLotIts - Excluindo registros exportados...';
      Update;
      Application.ProcessMessages;
      Qr3.SQL.Clear;
      Qr3.SQL.Add('DELETE FROM loi');
      Qr3.SQL.Add('USING EmLotIts loi, Creditor.Lotes lot');
      Qr3.SQL.Add('WHERE lot.Codigo=loi.Codigo');
      Qr3.SQL.Add('AND lot.Codigo > 0');
      Qr3.SQL.Add('AND lot.Tipo = 0');
      Qr3.SQL.Add('AND lot.MaxVencto<= :P0');
      Qr3.Params[0].AsString := Data;
      Qr3.ExecSQL;
      //
      if FileExists('C:\Dermatek\EmLotIts.txt') then
      DeleteFile('C:\Dermatek\EmLotIts.txt');
    end;
  end;
  //////////////////////////////////////////////////////////////////////////////

  // EmLot /////////////////////////////////////////////////////////////////////
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs-1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      Qr1.Database := Dmod.DatabaseColigadas(i);
      Qr2.Database := Dmod.DatabaseColigadas(i);
      Qr3.Database := Dmod.DatabaseColigadas(i);
      ST1.Caption := 'EmLot - Excluindo arquivo tempor�rio...';
      Update;
      Application.ProcessMessages;
      if FileExists('C:\Dermatek\EmLot.txt') then
      DeleteFile('C:\Dermatek\EmLot.txt');
      ST1.Caption := 'EmLot - Exportando para arquivo tempor�rio...';
      Update;
      Application.ProcessMessages;
      Qr1.SQL.Clear;
      Qr1.SQL.Add('SELECT '+UMyMod.ObtemCamposDeTabelaIdentica(Dmod.DatabaseColigadas(i),
        'EzLot', 'loi.'));
      Qr1.SQL.Add('FROM emlot loi');
      Qr1.SQL.Add('LEFT JOIN creditor.lotes lot ON lot.Codigo=loi.Codigo');
      Qr1.SQL.Add('WHERE lot.Codigo>0');
      Qr1.SQL.Add('AND lot.MaxVencto<= :P0');
      Qr1.SQL.Add('AND lot.Tipo = 0');
      Qr1.SQL.Add('INTO OUTFILE "C:/Dermatek/EmLot.txt"');
      Qr1.Params[0].AsString := Data;
      Qr1.ExecSQL;
      //
      ST1.Caption := 'EmLot - Importando de arquivo tempor�rio...';
      Update;
      Application.ProcessMessages;
      Qr2.SQL.Clear;
      Qr2.SQL.Add('LOAD DATA LOCAL INFILE "C:/Dermatek/EmLot.txt"');
      Qr2.SQL.Add('INTO Table EzLot');
      Qr2.ExecSQL;
      //
      ST1.Caption := 'EmLot - Excluindo registros exportados...';
      Update;
      Application.ProcessMessages;
      Qr3.SQL.Clear;
      Qr3.SQL.Add('DELETE FROM loi');
      Qr3.SQL.Add('USING EmLot loi, Creditor.Lotes lot');
      Qr3.SQL.Add('WHERE lot.Codigo=loi.Codigo');
      Qr3.SQL.Add('AND lot.Codigo > 0');
      Qr3.SQL.Add('AND lot.Tipo = 0');
      Qr3.SQL.Add('AND lot.MaxVencto<= :P0');
      Qr3.Params[0].AsString := Data;
      Qr3.ExecSQL;
      //
      if FileExists('C:\Dermatek\EmLot.txt') then
      DeleteFile('C:\Dermatek\EmLot.txt');
    end;
  end;
  //////////////////////////////////////////////////////////////////////////////







  // Deve ser o �ltimo !!!!!!!!!!!!!!!!

  // Lotes /////////////////////////////////////////////////////////////////
  Qr1.Database := Dmod.MyDB;
  Qr2.Database := Dmod.MyDB;
  Qr3.Database := Dmod.MyDB;
  ST1.Caption := 'Lotes - Excluindo arquivo tempor�rio...';
  Update;
  Application.ProcessMessages;
  if FileExists('C:\Dermatek\Lotes.txt') then
  DeleteFile('C:\Dermatek\Lotes.txt');
  ST1.Caption := 'Lotes - Exportando para arquivo tempor�rio...';
  Update;
  Application.ProcessMessages;
  Qr1.SQL.Clear;
  Qr1.SQL.Add('SELECT '+UMyMod.ObtemCamposDeTabelaIdentica(Dmod.MyDB,
    'Lotez', 'lot.'));
  Qr1.SQL.Add('FROM lotes lot');
  Qr1.SQL.Add('WHERE lot.Codigo>0');
  Qr1.SQL.Add('AND lot.MaxVencto<= :P0');
  Qr1.SQL.Add('AND lot.Tipo = 0');
  Qr1.SQL.Add('INTO OUTFILE "C:/Dermatek/Lotes.txt"');
  Qr1.Params[0].AsString := Data;
  Qr1.ExecSQL;
  //
  ST1.Caption := 'Lotes - Importando de arquivo tempor�rio...';
  Update;
  Application.ProcessMessages;
  Qr2.SQL.Clear;
  Qr2.SQL.Add('LOAD DATA LOCAL INFILE "C:/Dermatek/Lotes.txt"');
  Qr2.SQL.Add('INTO Table Lotez');
  Qr2.ExecSQL;
  //
  ST1.Caption := 'Lotes - Excluindo registros exportados...';
  Update;
  Application.ProcessMessages;
  Qr3.SQL.Clear;
  Qr3.SQL.Add('DELETE FROM lotes');
  Qr3.SQL.Add('WHERE Codigo > 0');
  Qr3.SQL.Add('AND MaxVencto<= :P0');
  Qr3.SQL.Add('AND Tipo = 0');
  Qr3.Params[0].AsString := Data;
  Qr3.ExecSQL;
  //
  if FileExists('C:\Dermatek\Lotes.txt') then
  DeleteFile('C:\Dermatek\Lotes.txt');
  //////////////////////////////////////////////////////////////////////////////






  //////////////////////////////////////////////////////////////////////////////
  ST1.Caption := 'Exporta��o finalizada. Otimizando tabelas...';
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('OPTIMIZE Table Lotes, LotesIts, Lotez, LotezIts');
  Dmod.QrUpd.ExecSQL;
  //////////////////////////////////////////////////////////////////////////////
  ST1.Caption := 'Exporta��o finalizada.';
  Screen.Cursor := crDefault;
  //////////////////////////////////////////////////////////////////////////////
  FTempo := Now;
  FTodos := QrSumCli.RecordCount + QrSumEmi.RecordCount;
  FConta := 0;
  AtualizaSaldos;
end;

procedure TFmLotesMorto.AtualizaSaldos;
begin
  ST1.Caption := 'Atualizando hist�rico de clientes ...';
  Update;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE entidades SET AlterWeb=1, QuantI1=QuantI1+ :P0, ');
  Dmod.QrUpd.SQL.Add('QuantN1=QuantN1+ :P1');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
  Application.ProcessMessages;
  while not QrSumCli.Eof do
  begin
    FConta := FConta + 1;
    Progress.Position := Progress.Position + 1;
    Application.ProcessMessages;
    //
    Dmod.QrUpd.Params[00].AsInteger := QrSumCliItens.Value;
    Dmod.QrUpd.Params[01].AsFloat   := QrSumCliValor.Value;
    Dmod.QrUpd.Params[02].AsInteger := QrSumCliCliente.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrSumCli.Next;
  end;

  //

  ST1.Caption := 'Atualizando hist�rico de emitentes ...';
  Update;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE emitcpf SET AcumCHComQ=AcumCHComQ+ :P0, ');
  Dmod.QrUpd.SQL.Add('AcumCHComV=AcumCHComV+ :P1');
  Dmod.QrUpd.SQL.Add('WHERE CPF=:Pa');
  Application.ProcessMessages;
  while not QrSumEmi.Eof do
  begin
    FConta := FConta + 1;
    Progress.Position := Progress.Position + 1;
    Application.ProcessMessages;
    //
    Dmod.QrUpd.Params[00].AsInteger := QrSumEmiItens.Value;
    Dmod.QrUpd.Params[01].AsFloat   := QrSumEmiValor.Value;
    Dmod.QrUpd.Params[02].AsString := QrSumEmiCPF.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrSumEmi.Next;
  end;
  Timer1.Enabled := False;
  ShowMessage('Atualiza��o finalizada!');
end;

procedure TFmLotesMorto.Timer1Timer(Sender: TObject);
begin
  AtualizaTempos;
end;

procedure TFmLotesMorto.AtualizaTempos;
var
  Falta, Agora, Pasou, Total: TTime;
begin
  Agora := Now;
  Pasou := (Agora - FTempo);
  ST2.Caption := FormatDateTime('hh:nn:ss', Pasou);
  if FConta > 0 then
  begin
    Total := Pasou * (FTodos / FConta);
    Falta := Total - Pasou;
    //
    ST3.Caption := FormatDateTime('hh:nn:ss', Total);
    ST4.Caption := FormatDateTime('hh:nn:ss', Falta);
  end;
end;

end.

