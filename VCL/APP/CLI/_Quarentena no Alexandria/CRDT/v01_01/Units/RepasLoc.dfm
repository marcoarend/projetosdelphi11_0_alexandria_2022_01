object FmRepasLoc: TFmRepasLoc
  Left = 396
  Top = 181
  Caption = 'Lacaliza'#231#227'o de Repasse'
  ClientHeight = 334
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object DBGrid1: TDBGrid
    Left = 0
    Top = 95
    Width = 784
    Height = 191
    Align = alClient
    DataSource = DsLoc
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
    Columns = <
      item
        Expanded = False
        FieldName = 'Codigo'
        Title.Caption = 'Lote'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Data'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Codigo'
        Title.Caption = 'Lote'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Total'
        Title.Caption = 'Valor'
        Width = 77
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMECOLIGADO'
        Title.Caption = 'Coligado'
        Width = 477
        Visible = True
      end>
  end
  object PainelDados: TPanel
    Left = 0
    Top = 40
    Width = 784
    Height = 55
    Align = alTop
    TabOrder = 0
    object Label2: TLabel
      Left = 12
      Top = 4
      Width = 44
      Height = 13
      Caption = 'Coligado:'
    end
    object Label34: TLabel
      Left = 504
      Top = 4
      Width = 55
      Height = 13
      Caption = 'Data inicial:'
    end
    object Label4: TLabel
      Left = 608
      Top = 4
      Width = 48
      Height = 13
      Caption = 'Data final:'
    end
    object CBColigado: TdmkDBLookupComboBox
      Left = 80
      Top = 20
      Width = 421
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMEENTIDADE'
      ListSource = DsClientes
      TabOrder = 1
      dmkEditCB = EdColigado
      UpdType = utYes
    end
    object EdColigado: TdmkEditCB
      Left = 12
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      OnChange = EdColigadoChange
      DBLookupComboBox = CBColigado
    end
    object TPIni: TDateTimePicker
      Left = 504
      Top = 20
      Width = 101
      Height = 21
      Date = 38675.714976851900000000
      Time = 38675.714976851900000000
      TabOrder = 2
      OnChange = TPIniChange
    end
    object TPFim: TDateTimePicker
      Left = 608
      Top = 20
      Width = 101
      Height = 21
      Date = 38675.714976851900000000
      Time = 38675.714976851900000000
      TabOrder = 3
      OnChange = TPFimChange
    end
  end
  object PainelControle: TPanel
    Left = 0
    Top = 286
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtConfirma: TBitBtn
      Tag = 14
      Left = 14
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Localiza'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object BtDesiste: TBitBtn
      Tag = 15
      Left = 684
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Desiste'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtDesisteClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 40
    Align = alTop
    Caption = 'Localiza'#231#227'o de Repasse'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 701
      Top = 1
      Width = 82
      Height = 38
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 36
    end
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 700
      Height = 38
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 706
      ExplicitHeight = 36
    end
  end
  object DsClientes: TDataSource
    DataSet = QrColigados
    Left = 416
    Top = 46
  end
  object QrColigados: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'CASE 1 WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTIDADE, Codigo'
      'FROM entidades'
      'WHERE Fornece2="V" '
      'ORDER BY NOMEENTIDADE')
    Left = 388
    Top = 46
    object QrColigadosNOMEENTIDADE: TWideStringField
      FieldKind = fkInternalCalc
      FieldName = 'NOMEENTIDADE'
      Size = 100
    end
    object QrColigadosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrLocAfterOpen
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECOLIGADO, '
      'CASE WHEN en.Tipo=0 THEN en.CNPJ'
      'ELSE en.CPF END CNPJCPF, re.* '
      'FROM repas re'
      'LEFT JOIN entidades en ON en.Codigo=re.Coligado'
      'WHERE re.Coligado>0'
      'ORDER BY Codigo DESC')
    Left = 40
    Top = 156
    object QrLocNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
    object QrLocCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrLocCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLocColigado: TIntegerField
      FieldName = 'Coligado'
      Required = True
    end
    object QrLocData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrLocTotal: TFloatField
      FieldName = 'Total'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLocJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLocLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsLoc: TDataSource
    DataSet = QrLoc
    Left = 68
    Top = 156
  end
end
