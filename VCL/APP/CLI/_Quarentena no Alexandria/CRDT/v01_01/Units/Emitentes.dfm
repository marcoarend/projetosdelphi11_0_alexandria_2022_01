object FmEmitentes: TFmEmitentes
  Left = 332
  Top = 217
  Caption = 'Emitentes'
  ClientHeight = 308
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 212
    Align = alClient
    TabOrder = 0
    object GrEmitentes: TDBGrid
      Left = 1
      Top = 42
      Width = 782
      Height = 108
      Align = alClient
      DataSource = DsEmitPF
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 242
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CPF_TXT'
          Title.Caption = 'CPF'
          Width = 104
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Risco'
          Visible = True
        end>
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 41
      Align = alTop
      TabOrder = 1
      object RGOrdem1: TRadioGroup
        Left = 1
        Top = 1
        Width = 595
        Height = 39
        Align = alClient
        Caption = ' Ordem 1: '
        Columns = 3
        Items.Strings = (
          'Nome'
          'CPF / CNPJ'
          'Risco')
        TabOrder = 0
        OnClick = RGOrdem1Click
      end
      object RGOrdem2: TRadioGroup
        Left = 596
        Top = 1
        Width = 185
        Height = 39
        Align = alRight
        Caption = ' Ordem 2: '
        Columns = 2
        Items.Strings = (
          'Crescente'
          'Decrescente')
        TabOrder = 1
        OnClick = RGOrdem2Click
      end
    end
    object StringGrid1: TStringGrid
      Left = 1
      Top = 150
      Width = 782
      Height = 61
      Align = alBottom
      ColCount = 6
      DefaultRowHeight = 18
      TabOrder = 2
      Visible = False
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 260
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtSaida: TBitBtn
      Tag = 13
      Left = 682
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 3
      OnClick = BtSaidaClick
      NumGlyphs = 2
    end
    object BtInclui: TBitBtn
      Tag = 10
      Left = 18
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Inclui'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtIncluiClick
      NumGlyphs = 2
    end
    object BtAltera: TBitBtn
      Tag = 11
      Left = 114
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Altera'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtAlteraClick
      NumGlyphs = 2
    end
    object BtExclui: TBitBtn
      Tag = 12
      Left = 210
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Exclui'
      Enabled = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtExcluiClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Emitentes'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object DsEmitPF: TDataSource
    DataSet = QrEmitCPF
    Left = 220
    Top = 84
  end
  object QrEmitCPF: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmitCPFCalcFields
    SQL.Strings = (
      'SELECT * FROM emitcpf')
    Left = 192
    Top = 84
    object QrEmitCPFCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrEmitCPFCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrEmitCPFNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrEmitCPFLimite: TFloatField
      FieldName = 'Limite'
    end
    object QrEmitCPFLastAtz: TDateField
      FieldName = 'LastAtz'
    end
    object QrEmitCPFAcumCHComV: TFloatField
      FieldName = 'AcumCHComV'
    end
    object QrEmitCPFAcumCHComQ: TIntegerField
      FieldName = 'AcumCHComQ'
    end
    object QrEmitCPFAcumCHDevV: TFloatField
      FieldName = 'AcumCHDevV'
    end
    object QrEmitCPFAcumCHDevQ: TIntegerField
      FieldName = 'AcumCHDevQ'
    end
  end
end
