object FmOcorreu0: TFmOcorreu0
  Left = 342
  Top = 167
  Caption = 'Controle de Cheques'
  ClientHeight = 633
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 585
    Align = alClient
    BevelOuter = bvNone
    Color = clAppWorkSpace
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 329
      Width = 1008
      Height = 3
      Cursor = crVSplit
      Align = alTop
      ExplicitWidth = 861
    end
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 329
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Panel000: TPanel
        Left = 0
        Top = 153
        Width = 1008
        Height = 107
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 3
        object GradeItens: TDBGrid
          Left = 0
          Top = 0
          Width = 232
          Height = 107
          Align = alLeft
          DataSource = DsPesq
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = GradeItensDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 274
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNPJ_TXT'
              Title.Caption = 'CNPJ/CPF'
              Width = 121
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DCompra'
              Title.Caption = 'Data C.'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DDeposito'
              Title.Caption = 'Dep'#243'sito'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Banco'
              Title.Caption = 'Bco'
              Width = 25
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Agencia'
              Title.Caption = 'Ag.'
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conta'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cheque'
              Width = 43
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Risco'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ProrrVz'
              Title.Caption = 'Pror. Vz'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ProrrDd'
              Title.Caption = 'Pror. Dd'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ObsGerais'
              Title.Caption = 'Descri'#231#227'o'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Visible = True
            end>
        end
        object PainelMuda: TPanel
          Left = 232
          Top = 0
          Width = 776
          Height = 107
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          Visible = False
          object PainelDevo: TPanel
            Left = 0
            Top = 0
            Width = 776
            Height = 107
            Align = alClient
            TabOrder = 1
            Visible = False
            object Label17: TLabel
              Left = 4
              Top = 4
              Width = 41
              Height = 13
              Caption = 'Alinea 1:'
            end
            object Label18: TLabel
              Left = 388
              Top = 4
              Width = 41
              Height = 13
              Caption = 'Alinea 2:'
            end
            object Label19: TLabel
              Left = 296
              Top = 4
              Width = 26
              Height = 13
              Caption = 'Data:'
            end
            object Label23: TLabel
              Left = 680
              Top = 4
              Width = 26
              Height = 13
              Caption = 'Data:'
            end
            object Label24: TLabel
              Left = 4
              Top = 44
              Width = 38
              Height = 13
              Caption = '$ Multa:'
            end
            object Label25: TLabel
              Left = 196
              Top = 44
              Width = 78
              Height = 13
              Caption = '% Taxa de juros:'
            end
            object Label26: TLabel
              Left = 100
              Top = 44
              Width = 41
              Height = 13
              Caption = '$ Taxas:'
            end
            object EdAlinea1: TdmkEditCB
              Left = 4
              Top = 20
              Width = 45
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBAlinea1
              IgnoraDBLookupComboBox = False
            end
            object EdAlinea2: TdmkEditCB
              Left = 388
              Top = 20
              Width = 45
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              DBLookupComboBox = CBAlinea2
              IgnoraDBLookupComboBox = False
            end
            object CBAlinea2: TdmkDBLookupComboBox
              Left = 436
              Top = 20
              Width = 240
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsAlinea2
              TabOrder = 4
              dmkEditCB = EdAlinea2
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object CBAlinea1: TdmkDBLookupComboBox
              Left = 52
              Top = 20
              Width = 240
              Height = 21
              Color = clWhite
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsAlinea1
              TabOrder = 1
              dmkEditCB = EdAlinea1
              UpdType = utYes
              LocF7SQLMasc = '$#'
            end
            object TPData1_1: TDateTimePicker
              Left = 296
              Top = 20
              Width = 89
              Height = 21
              Date = 38698.785142685200000000
              Time = 38698.785142685200000000
              TabOrder = 2
            end
            object TPData1_2: TDateTimePicker
              Left = 680
              Top = 20
              Width = 89
              Height = 21
              Date = 38698.785142685200000000
              Time = 38698.785142685200000000
              TabOrder = 5
            end
            object EdMulta1: TdmkEdit
              Left = 4
              Top = 60
              Width = 93
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
            object EdJuros1: TdmkEdit
              Left = 196
              Top = 60
              Width = 93
              Height = 21
              Alignment = taRightJustify
              TabOrder = 8
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 6
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              OnExit = EdJuros1Exit
            end
            object EdTaxas1: TdmkEdit
              Left = 100
              Top = 60
              Width = 93
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
            end
          end
          object PainelPgDv: TPanel
            Left = 0
            Top = 0
            Width = 776
            Height = 107
            Align = alClient
            BevelOuter = bvNone
            Color = clAppWorkSpace
            TabOrder = 2
            Visible = False
            object Panel9: TPanel
              Left = -68
              Top = 12
              Width = 800
              Height = 48
              TabOrder = 0
              object Label33: TLabel
                Left = 4
                Top = 4
                Width = 52
                Height = 13
                Caption = 'Data base:'
              end
              object Label34: TLabel
                Left = 92
                Top = 4
                Width = 53
                Height = 13
                Caption = 'Valor base:'
              end
              object Label35: TLabel
                Left = 168
                Top = 4
                Width = 66
                Height = 13
                Caption = '% Tx.jur.base:'
              end
              object Label37: TLabel
                Left = 244
                Top = 4
                Width = 65
                Height = 13
                Caption = '% Jur. per'#237'od:'
              end
              object Label12: TLabel
                Left = 320
                Top = 4
                Width = 26
                Height = 13
                Caption = 'Data:'
              end
              object Label28: TLabel
                Left = 404
                Top = 4
                Width = 59
                Height = 13
                Caption = 'Vencimento:'
              end
              object Label30: TLabel
                Left = 488
                Top = 4
                Width = 37
                Height = 13
                Caption = '$ Juros:'
              end
              object Label32: TLabel
                Left = 564
                Top = 4
                Width = 66
                Height = 13
                Caption = 'Total a pagar:'
              end
              object Label31: TLabel
                Left = 640
                Top = 4
                Width = 69
                Height = 13
                Caption = '$ Val. a pagar:'
              end
              object Label1: TLabel
                Left = 716
                Top = 4
                Width = 58
                Height = 13
                Caption = '$ Desconto:'
              end
              object TPDataBase2: TDateTimePicker
                Left = 4
                Top = 20
                Width = 84
                Height = 21
                Date = 38698.785142685200000000
                Time = 38698.785142685200000000
                Color = clBtnFace
                Enabled = False
                TabOrder = 0
                TabStop = False
              end
              object TPData2: TDateTimePicker
                Left = 320
                Top = 20
                Width = 84
                Height = 21
                Date = 38698.785142685200000000
                Time = 38698.785142685200000000
                TabOrder = 1
                OnChange = TPPagto2Change
              end
              object TPPagto2: TDateTimePicker
                Left = 404
                Top = 20
                Width = 84
                Height = 21
                Date = 38698.785142685200000000
                Time = 38698.785142685200000000
                TabOrder = 2
                OnChange = TPPagto2Change
              end
              object EdValorBase2: TdmkEdit
                Left = 92
                Top = 20
                Width = 72
                Height = 21
                Hint = 'AleluiaIrmao'
                TabStop = False
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdJurosBase2: TdmkEdit
                Left = 168
                Top = 20
                Width = 72
                Height = 21
                Hint = 'AleluiaIrmao'
                Alignment = taRightJustify
                Color = clWhite
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 6
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdJurosPeriodo2: TdmkEdit
                Left = 244
                Top = 20
                Width = 72
                Height = 21
                Hint = 'AleluiaIrmao'
                TabStop = False
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 5
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 6
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdJuros2: TdmkEdit
                Left = 488
                Top = 20
                Width = 72
                Height = 21
                Hint = 'AleluiaIrmao'
                TabStop = False
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 6
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdAPagar: TdmkEdit
                Left = 564
                Top = 20
                Width = 72
                Height = 21
                Hint = 'AleluiaIrmao'
                TabStop = False
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 7
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdPago2: TdmkEdit
                Left = 640
                Top = 20
                Width = 72
                Height = 21
                Hint = 'AleluiaIrmao'
                Alignment = taRightJustify
                TabOrder = 8
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdDesco2: TdmkEdit
                Left = 716
                Top = 20
                Width = 72
                Height = 21
                Hint = 'AleluiaIrmao'
                Alignment = taRightJustify
                TabOrder = 9
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
            end
            object Panel8: TPanel
              Left = 148
              Top = 52
              Width = 1048
              Height = 127
              TabOrder = 1
              TabStop = True
              object Label42: TLabel
                Left = 4
                Top = 4
                Width = 34
                Height = 13
                Caption = 'Al'#237'nea:'
              end
              object Label43: TLabel
                Left = 4
                Top = 44
                Width = 143
                Height = 13
                Caption = 'Leitura pela banda magn'#233'tica:'
              end
              object Label44: TLabel
                Left = 256
                Top = 44
                Width = 27
                Height = 13
                Caption = 'Com.:'
              end
              object Label45: TLabel
                Left = 288
                Top = 44
                Width = 22
                Height = 13
                Caption = 'Bco:'
              end
              object Label46: TLabel
                Left = 320
                Top = 44
                Width = 34
                Height = 13
                Caption = 'Ag'#234'nc:'
              end
              object Label47: TLabel
                Left = 360
                Top = 44
                Width = 55
                Height = 13
                Caption = 'Conta corr.:'
              end
              object Label48: TLabel
                Left = 436
                Top = 44
                Width = 45
                Height = 13
                Caption = 'N'#186' cheq.:'
              end
              object Label49: TLabel
                Left = 4
                Top = 84
                Width = 129
                Height = 13
                Caption = 'CGC / CPF [F4 - Pesquisa]:'
              end
              object Label50: TLabel
                Left = 164
                Top = 84
                Width = 44
                Height = 13
                Caption = 'Emitente:'
              end
              object EdAPCD: TdmkEditCB
                Left = 4
                Top = 20
                Width = 41
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                DBLookupComboBox = CBAPCD
                IgnoraDBLookupComboBox = False
              end
              object CBAPCD: TdmkDBLookupComboBox
                Left = 48
                Top = 20
                Width = 441
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsAPCD
                TabOrder = 1
                dmkEditCB = EdAPCD
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object EdBanda2: TdmkEdit
                Left = 4
                Top = 60
                Width = 249
                Height = 21
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                MaxLength = 34
                ParentFont = False
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnChange = EdBanda2Change
              end
              object EdRegiaoCompe2: TdmkEdit
                Left = 256
                Top = 60
                Width = 29
                Height = 21
                Alignment = taCenter
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdBanco2: TdmkEdit
                Left = 288
                Top = 60
                Width = 29
                Height = 21
                Alignment = taCenter
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnChange = EdBanco2Change
                OnExit = EdBanco2Exit
              end
              object EdAgencia2: TdmkEdit
                Left = 320
                Top = 60
                Width = 37
                Height = 21
                Alignment = taCenter
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnChange = EdAgencia2Change
              end
              object EdConta2: TdmkEdit
                Left = 360
                Top = 60
                Width = 73
                Height = 21
                Alignment = taCenter
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnChange = EdConta2Change
              end
              object EdCheque2: TdmkEdit
                Left = 436
                Top = 60
                Width = 53
                Height = 21
                Alignment = taCenter
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdCPF_2: TdmkEdit
                Left = 4
                Top = 100
                Width = 157
                Height = 21
                Alignment = taCenter
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 8
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnExit = EdCPF_2Exit
                OnKeyDown = EdCPF_2KeyDown
              end
              object EdEmitente2: TdmkEdit
                Left = 164
                Top = 100
                Width = 325
                Height = 21
                CharCase = ecUpperCase
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 9
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnExit = EdEmitente2Exit
              end
              object EdCMC_7_2: TEdit
                Left = 80
                Top = 56
                Width = 121
                Height = 21
                MaxLength = 30
                TabOrder = 11
                Visible = False
                OnChange = EdCMC_7_2Change
              end
              object EdRealCC: TdmkEdit
                Left = 360
                Top = 80
                Width = 73
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 12
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 10
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0000000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
              end
              object GroupBox1: TGroupBox
                Left = 750
                Top = 1
                Width = 297
                Height = 125
                Align = alRight
                TabOrder = 10
                object Label51: TLabel
                  Left = 4
                  Top = 26
                  Width = 45
                  Height = 13
                  Caption = 'Cheques:'
                end
                object Label52: TLabel
                  Left = 4
                  Top = 48
                  Width = 53
                  Height = 13
                  Caption = 'Duplicatas:'
                end
                object Label53: TLabel
                  Left = 60
                  Top = 8
                  Width = 30
                  Height = 13
                  Caption = 'Risco:'
                end
                object Label96: TLabel
                  Left = 137
                  Top = 8
                  Width = 42
                  Height = 13
                  Caption = 'Vencido:'
                end
                object Label98: TLabel
                  Left = 214
                  Top = 8
                  Width = 56
                  Height = 13
                  Caption = 'Sub-total:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label99: TLabel
                  Left = 4
                  Top = 74
                  Width = 56
                  Height = 13
                  Caption = 'Sub-total:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object Label91: TLabel
                  Left = 4
                  Top = 116
                  Width = 50
                  Height = 13
                  Caption = 'Ocorr'#234'nc.:'
                end
                object Label54: TLabel
                  Left = 214
                  Top = 96
                  Width = 45
                  Height = 13
                  Caption = 'TOTAL:'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                end
                object EdDR: TdmkEdit
                  Left = 60
                  Top = 44
                  Width = 76
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 3
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
                object EdDT: TdmkEdit
                  Left = 214
                  Top = 44
                  Width = 76
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 5
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
                object EdRT: TdmkEdit
                  Left = 60
                  Top = 70
                  Width = 76
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 6
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
                object EdVT: TdmkEdit
                  Left = 137
                  Top = 70
                  Width = 76
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 7
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
                object EdST: TdmkEdit
                  Left = 214
                  Top = 70
                  Width = 76
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 8
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
                object EdCT: TdmkEdit
                  Left = 214
                  Top = 22
                  Width = 76
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  TabOrder = 2
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
                object EdDV: TdmkEdit
                  Left = 137
                  Top = 44
                  Width = 76
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 4
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
                object EdCR: TdmkEdit
                  Left = 60
                  Top = 22
                  Width = 76
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 0
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
                object EdCV: TdmkEdit
                  Left = 137
                  Top = 22
                  Width = 76
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 1
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
                object EdOA: TdmkEdit
                  Left = 60
                  Top = 112
                  Width = 76
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 9
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
                object EdTT: TdmkEdit
                  Left = 214
                  Top = 112
                  Width = 76
                  Height = 21
                  TabStop = False
                  Alignment = taRightJustify
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  ParentFont = False
                  TabOrder = 10
                  FormatType = dmktfString
                  MskType = fmtNone
                  DecimalSize = 0
                  LeftZeros = 0
                  NoEnterToTab = False
                  NoForceUppercase = False
                  ForceNextYear = False
                  DataFormat = dmkdfShort
                  HoraFormat = dmkhfShort
                  UpdType = utYes
                  Obrigatorio = False
                  PermiteNulo = False
                  ValueVariant = ''
                end
              end
            end
          end
          object PainelOcor: TPanel
            Left = 0
            Top = 0
            Width = 776
            Height = 107
            Align = alClient
            BevelOuter = bvNone
            Color = clAppWorkSpace
            TabOrder = 0
            Visible = False
            object PainelPror: TPanel
              Left = 0
              Top = 0
              Width = 776
              Height = 49
              Align = alTop
              TabOrder = 0
              Visible = False
              object Label13: TLabel
                Left = 12
                Top = 8
                Width = 72
                Height = 13
                Caption = 'Novo dep'#243'sito:'
              end
              object Label14: TLabel
                Left = 108
                Top = 8
                Width = 78
                Height = 13
                Caption = 'Taxa juros base:'
              end
              object Label29: TLabel
                Left = 192
                Top = 8
                Width = 79
                Height = 13
                Caption = '% Juros per'#237'odo:'
              end
              object Label27: TLabel
                Left = 368
                Top = 8
                Width = 37
                Height = 13
                Caption = '$ Juros:'
              end
              object Label15: TLabel
                Left = 276
                Top = 8
                Width = 62
                Height = 13
                Caption = '$ Valor base:'
              end
              object Label16: TLabel
                Left = 460
                Top = 8
                Width = 72
                Height = 13
                Caption = '1'#186' Vencimento:'
              end
              object TPDataN: TDateTimePicker
                Left = 12
                Top = 24
                Width = 93
                Height = 21
                Date = 38711.818866817100000000
                Time = 38711.818866817100000000
                TabOrder = 0
                OnChange = TPDataNChange
              end
              object TPDataI: TDateTimePicker
                Left = 460
                Top = 24
                Width = 93
                Height = 21
                Date = 38711.818866817100000000
                Time = 38711.818866817100000000
                Enabled = False
                TabOrder = 1
                TabStop = False
                OnChange = TPDataNChange
              end
              object EdTaxaPror: TdmkEdit
                Left = 108
                Top = 24
                Width = 81
                Height = 21
                Alignment = taRightJustify
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 6
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdJurosPeriodoN: TdmkEdit
                Left = 192
                Top = 24
                Width = 81
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 6
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdValorBase: TdmkEdit
                Left = 276
                Top = 24
                Width = 88
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdJurosPr: TdmkEdit
                Left = 368
                Top = 24
                Width = 88
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 5
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
            end
            object Panel5: TPanel
              Left = 20
              Top = 53
              Width = 693
              Height = 52
              TabOrder = 1
              object Label20: TLabel
                Left = 8
                Top = 4
                Width = 55
                Height = 13
                Caption = 'Ocorr'#234'ncia:'
              end
              object Label21: TLabel
                Left = 352
                Top = 4
                Width = 26
                Height = 13
                Caption = 'Data:'
              end
              object Label22: TLabel
                Left = 460
                Top = 4
                Width = 27
                Height = 13
                Caption = 'Valor:'
              end
              object Label56: TLabel
                Left = 548
                Top = 4
                Width = 84
                Height = 13
                Caption = 'Taxa atualiza'#231#227'o:'
              end
              object EdOcorrencia: TdmkEditCB
                Left = 8
                Top = 20
                Width = 65
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                OnExit = EdOcorrenciaExit
                DBLookupComboBox = CBOcorrencia
                IgnoraDBLookupComboBox = False
              end
              object CBOcorrencia: TdmkDBLookupComboBox
                Left = 76
                Top = 20
                Width = 273
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOcorBank
                TabOrder = 1
                dmkEditCB = EdOcorrencia
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object TPDataO: TDateTimePicker
                Left = 352
                Top = 20
                Width = 105
                Height = 21
                Date = 38711.818866817100000000
                Time = 38711.818866817100000000
                TabOrder = 2
              end
              object EdValor: TdmkEdit
                Left = 460
                Top = 20
                Width = 85
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdTaxaP: TdmkEdit
                Left = 548
                Top = 20
                Width = 85
                Height = 21
                Alignment = taRightJustify
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 4
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,0000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                OnExit = EdTaxaPExit
              end
            end
          end
        end
      end
      object PainelPesq: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 153
        Align = alTop
        TabOrder = 0
        object Label38: TLabel
          Left = 296
          Top = 44
          Width = 34
          Height = 13
          Caption = 'Banco:'
        end
        object Label39: TLabel
          Left = 332
          Top = 44
          Width = 42
          Height = 13
          Caption = 'Ag'#234'ncia:'
        end
        object Label40: TLabel
          Left = 376
          Top = 44
          Width = 73
          Height = 13
          Caption = 'Conta corrente:'
        end
        object Label41: TLabel
          Left = 456
          Top = 44
          Width = 54
          Height = 13
          Caption = 'N'#186' cheque:'
        end
        object Label36: TLabel
          Left = 8
          Top = 44
          Width = 143
          Height = 13
          Caption = 'Leitura pela banda magn'#233'tica:'
        end
        object Label75: TLabel
          Left = 8
          Top = 4
          Width = 35
          Height = 13
          Caption = 'Cliente:'
        end
        object Label2: TLabel
          Left = 372
          Top = 4
          Width = 347
          Height = 13
          Caption = 
            'CPF / CNPJ [F8 pesquisa por nome] - [F4 pesquisa CPF / CNPJ digi' +
            'tado]:'
        end
        object Label9: TLabel
          Left = 516
          Top = 44
          Width = 80
          Height = 13
          Caption = 'Nome do Banco:'
        end
        object LaColigado: TLabel
          Left = 92
          Top = 84
          Width = 168
          Height = 13
          Caption = 'Coligado (quando repassado = sim):'
        end
        object Label55: TLabel
          Left = 324
          Top = 84
          Width = 38
          Height = 13
          Caption = 'Meu ID:'
        end
        object SpeedButton1: TSpeedButton
          Left = 804
          Top = 104
          Width = 23
          Height = 22
          OnClick = SpeedButton1Click
        end
        object EdBanco: TdmkEdit
          Left = 296
          Top = 60
          Width = 33
          Height = 21
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnChange = EdBancoChange
          OnExit = EdBancoExit
          OnKeyDown = EdBancoKeyDown
        end
        object EdAgencia: TdmkEdit
          Left = 332
          Top = 60
          Width = 41
          Height = 21
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnExit = EdAgenciaExit
        end
        object EdConta: TdmkEdit
          Left = 376
          Top = 60
          Width = 77
          Height = 21
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnExit = EdContaExit
        end
        object EdCheque: TdmkEdit
          Left = 456
          Top = 60
          Width = 57
          Height = 21
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnExit = EdChequeExit
        end
        object EdBanda: TdmkEdit
          Left = 8
          Top = 60
          Width = 285
          Height = 21
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          MaxLength = 34
          ParentFont = False
          TabOrder = 3
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnChange = EdBandaChange
          OnKeyDown = EdBandaKeyDown
        end
        object EdCliente: TdmkEditCB
          Left = 8
          Top = 20
          Width = 49
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdClienteChange
          DBLookupComboBox = CBCliente
          IgnoraDBLookupComboBox = False
        end
        object CBCliente: TdmkDBLookupComboBox
          Left = 60
          Top = 20
          Width = 309
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMECLIENTE'
          ListSource = DsClientes
          TabOrder = 1
          dmkEditCB = EdCliente
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object DBEdit19: TDBEdit
          Left = 516
          Top = 60
          Width = 269
          Height = 21
          TabStop = False
          DataField = 'Nome'
          DataSource = DsBanco
          TabOrder = 8
        end
        object EdCPF_1: TdmkEdit
          Left = 372
          Top = 20
          Width = 113
          Height = 21
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnExit = EdCPF_1Exit
          OnKeyDown = EdCPF_1KeyDown
        end
        object EdCMC_7_1: TEdit
          Left = 84
          Top = 60
          Width = 121
          Height = 21
          MaxLength = 30
          TabOrder = 9
          Visible = False
          OnChange = EdCMC_7_1Change
        end
        object EdColigado: TdmkEditCB
          Left = 92
          Top = 100
          Width = 49
          Height = 21
          Alignment = taRightJustify
          TabOrder = 11
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnChange = EdColigadoChange
          DBLookupComboBox = CBColigado
          IgnoraDBLookupComboBox = False
        end
        object CBColigado: TdmkDBLookupComboBox
          Left = 144
          Top = 100
          Width = 177
          Height = 21
          KeyField = 'Codigo'
          ListField = 'NOMECOLIGADO'
          ListSource = DsColigado
          TabOrder = 12
          dmkEditCB = EdColigado
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object CGStatusAutoma: TdmkCheckGroup
          Left = 560
          Top = 81
          Width = 229
          Height = 68
          Caption = ' Situa'#231#245'es do(s) documento(s): '
          Columns = 2
          Items.Strings = (
            'Liquidado'
            'Devolvido'
            'Em aberto'
            'Pg atras. parcial'
            'Pg atrasado total'
            'Pg atras. a maior')
          TabOrder = 13
          OnClick = CGStatusAutomaClick
          UpdType = utYes
          Value = 0
          OldValor = 0
        end
        object CkRepassado: TdmkCheckGroup
          Left = 8
          Top = 81
          Width = 81
          Height = 60
          Caption = ' Repassado: '
          Items.Strings = (
            'Sim'
            'N'#227'o')
          TabOrder = 10
          OnClick = CGStatusAutomaClick
          UpdType = utYes
          Value = 0
          OldValor = 0
        end
        object EdControle: TdmkEdit
          Left = 324
          Top = 100
          Width = 57
          Height = 21
          Alignment = taRightJustify
          TabOrder = 14
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdEmitente1: TdmkEdit
          Left = 488
          Top = 20
          Width = 297
          Height = 21
          Enabled = False
          TabOrder = 15
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnExit = EdCPF_1Exit
          OnKeyDown = EdCPF_1KeyDown
        end
        object CkAtualiza: TCheckBox
          Left = 92
          Top = 124
          Width = 97
          Height = 17
          Caption = 'Atualiza saldos.'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 16
        end
        object CkMorto: TCheckBox
          Left = 192
          Top = 124
          Width = 189
          Height = 17
          Caption = 'Inclui arquivo morto na pesquisa.'
          TabOrder = 17
        end
        object CGStatusManual: TdmkCheckGroup
          Left = 384
          Top = 81
          Width = 173
          Height = 68
          Caption = ' Status pr'#233'-definidos: '
          Columns = 2
          Items.Strings = (
            'Autom'#225'tico'
            'Prorrogado'
            'Baixado'
            'Moroso')
          TabOrder = 18
          OnClick = CGStatusAutomaClick
          UpdType = utYes
          Value = 0
          OldValor = 0
        end
      end
      object Panel3: TPanel
        Left = 0
        Top = 260
        Width = 1008
        Height = 44
        Align = alBottom
        Enabled = False
        TabOrder = 1
        object Label3: TLabel
          Left = 92
          Top = 1
          Width = 50
          Height = 13
          Caption = 'Opera'#231#227'o:'
          FocusControl = DBEdit2
        end
        object Label4: TLabel
          Left = 160
          Top = 1
          Width = 27
          Height = 13
          Caption = 'Valor:'
          FocusControl = DBEdit3
        end
        object Label5: TLabel
          Left = 8
          Top = 1
          Width = 80
          Height = 13
          Caption = 'Taxa de compra:'
          FocusControl = DBEdit4
        end
        object Label7: TLabel
          Left = 244
          Top = 1
          Width = 59
          Height = 13
          Caption = 'Vencimento:'
          FocusControl = DBEdit6
        end
        object Label10: TLabel
          Left = 312
          Top = 1
          Width = 33
          Height = 13
          Caption = 'Status:'
          FocusControl = DBEdit1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label11: TLabel
          Left = 508
          Top = 1
          Width = 44
          Height = 13
          Caption = 'Coligado:'
          FocusControl = DBEdit8
        end
        object DBEdit2: TDBEdit
          Left = 92
          Top = 17
          Width = 64
          Height = 21
          DataField = 'DCompra'
          DataSource = DsPesq
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 160
          Top = 17
          Width = 80
          Height = 21
          DataField = 'Valor'
          DataSource = DsPesq
          TabOrder = 1
        end
        object DBEdit4: TDBEdit
          Left = 8
          Top = 17
          Width = 80
          Height = 21
          DataField = 'TAXA_COMPRA'
          DataSource = DsPesq
          TabOrder = 2
        end
        object DBEdit6: TDBEdit
          Left = 244
          Top = 17
          Width = 64
          Height = 21
          DataField = 'Vencto'
          DataSource = DsPesq
          TabOrder = 3
        end
        object DBEdit1: TDBEdit
          Left = 312
          Top = 17
          Width = 193
          Height = 21
          DataField = 'NOMESTATUS'
          DataSource = DsPesq
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 4
        end
        object DBEdit8: TDBEdit
          Left = 508
          Top = 17
          Width = 279
          Height = 21
          DataField = 'NOMECOLIGADO'
          DataSource = DsPesq
          TabOrder = 5
        end
      end
      object Panel4: TPanel
        Left = 0
        Top = 304
        Width = 1008
        Height = 25
        Align = alBottom
        Enabled = False
        TabOrder = 2
        object Label8: TLabel
          Left = 8
          Top = 5
          Width = 35
          Height = 13
          Caption = 'Cliente:'
          FocusControl = DBEdit7
        end
        object Label6: TLabel
          Left = 560
          Top = 5
          Width = 29
          Height = 13
          Caption = 'Itens: '
        end
        object Label57: TLabel
          Left = 644
          Top = 6
          Width = 39
          Height = 13
          Caption = 'Total $: '
        end
        object DBEdit7: TDBEdit
          Left = 48
          Top = 1
          Width = 393
          Height = 21
          DataField = 'NOMECLIENTE'
          DataSource = DsPesq
          TabOrder = 0
        end
        object DBEdit5: TDBEdit
          Left = 440
          Top = 1
          Width = 117
          Height = 21
          DataField = 'TEL1CLI_TXT'
          DataSource = DsPesq
          TabOrder = 1
        end
        object EdSomaI: TdmkEdit
          Left = 588
          Top = 1
          Width = 53
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
        end
        object EdSomaV: TdmkEdit
          Left = 688
          Top = 1
          Width = 99
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
      end
    end
    object PainelData: TPanel
      Left = 0
      Top = 332
      Width = 1008
      Height = 205
      Align = alClient
      BevelOuter = bvLowered
      TabOrder = 1
      Visible = False
      object PageControl1: TPageControl
        Left = 1
        Top = 1
        Width = 1006
        Height = 203
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = 'Ocorr'#234'ncias  '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid3: TDBGrid
            Left = 728
            Top = 0
            Width = 270
            Height = 175
            Align = alRight
            DataSource = DsOcorrPg
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Juros'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pago'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'LotePg'
                Title.Caption = 'Lote pgto'
                Width = 48
                Visible = True
              end>
          end
          object Panel2: TPanel
            Left = 0
            Top = 0
            Width = 728
            Height = 175
            Align = alClient
            BevelOuter = bvNone
            Caption = 'Panel2'
            TabOrder = 1
            object DBGrid1: TDBGrid
              Left = 0
              Top = 0
              Width = 728
              Height = 93
              TabStop = False
              Align = alClient
              DataSource = DsOcorreu
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'Controle'
                  Width = 47
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataO'
                  Title.Caption = 'Data'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEOCORRENCIA'
                  Title.Caption = 'Ocorr'#234'ncia'
                  Width = 166
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Valor'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SALDO'
                  Title.Caption = 'Saldo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ATUALIZADO'
                  Title.Caption = 'Atualizado'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'TaxaP'
                  Title.Caption = 'Taxa Atz'
                  Visible = True
                end>
            end
            object PainelOcorPg: TPanel
              Left = 0
              Top = 93
              Width = 728
              Height = 82
              Align = alBottom
              BevelOuter = bvNone
              TabOrder = 1
              Visible = False
              object Label86: TLabel
                Left = 4
                Top = 4
                Width = 52
                Height = 13
                Caption = 'Data base:'
              end
              object Label87: TLabel
                Left = 88
                Top = 4
                Width = 53
                Height = 13
                Caption = 'Valor base:'
              end
              object Label88: TLabel
                Left = 160
                Top = 4
                Width = 63
                Height = 13
                Caption = '%Tx jur.base:'
              end
              object Label89: TLabel
                Left = 244
                Top = 4
                Width = 68
                Height = 13
                Caption = '% Jur.per'#237'odo:'
              end
              object Label82: TLabel
                Left = 0
                Top = 44
                Width = 26
                Height = 13
                Caption = 'Data:'
              end
              object Label83: TLabel
                Left = 88
                Top = 44
                Width = 37
                Height = 13
                Caption = '$ Juros:'
              end
              object Label85: TLabel
                Left = 160
                Top = 44
                Width = 66
                Height = 13
                Caption = 'Total a pagar:'
              end
              object Label84: TLabel
                Left = 244
                Top = 44
                Width = 75
                Height = 13
                Caption = '$ Valor a pagar:'
              end
              object TPDataBase6: TDateTimePicker
                Left = 4
                Top = 20
                Width = 85
                Height = 21
                Date = 38698.785142685200000000
                Time = 38698.785142685200000000
                Color = clBtnFace
                TabOrder = 0
                TabStop = False
              end
              object TPPagto6: TDateTimePicker
                Left = 0
                Top = 60
                Width = 85
                Height = 21
                Date = 38698.785142685200000000
                Time = 38698.785142685200000000
                TabOrder = 1
                OnChange = TPPagto6Change
              end
              object EdValorBase6: TdmkEdit
                Left = 88
                Top = 20
                Width = 68
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdJurosBase6: TdmkEdit
                Left = 160
                Top = 20
                Width = 80
                Height = 21
                Alignment = taRightJustify
                Color = clWhite
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 6
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdJurosPeriodo6: TdmkEdit
                Left = 244
                Top = 20
                Width = 80
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 6
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdPago6: TdmkEdit
                Left = 244
                Top = 60
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 5
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdAPagar6: TdmkEdit
                Left = 160
                Top = 60
                Width = 80
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 6
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdJuros6: TdmkEdit
                Left = 88
                Top = 60
                Width = 68
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 7
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = 'Prorroga'#231#227'o  / Antecipa'#231#227'o'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid2: TDBGrid
            Left = 0
            Top = 0
            Width = 887
            Height = 175
            TabStop = False
            Align = alClient
            DataSource = DsLotesPrr
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Data1'
                Title.Caption = 'D.Original'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Data2'
                Title.Caption = 'A.Anterior'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Data3'
                Title.Caption = 'N.Vencto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DIAS_2_3'
                Title.Caption = 'Dias'
                Width = 48
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DIAS_1_3'
                Title.Caption = 'Acumul.'
                Width = 48
                Visible = True
              end>
          end
          object Panel7: TPanel
            Left = 887
            Top = 0
            Width = 111
            Height = 175
            Align = alRight
            Caption = 'Panel7'
            TabOrder = 1
            object BtReciboAntecip: TBitBtn
              Left = 11
              Top = 36
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Recibo'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtReciboAntecipClick
            end
          end
        end
        object TabSheet3: TTabSheet
          Caption = 'Devolu'#231#227'o  '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object GradeDev: TDBGrid
            Left = 0
            Top = 0
            Width = 998
            Height = 40
            TabStop = False
            Align = alTop
            DataSource = DsAlinIts
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'DATA1_TXT'
                Title.Caption = '1'#170' Devol.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Alinea1'
                Title.Caption = 'A1'
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DATA2_TXT'
                Title.Caption = '2'#170' Devol.'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Alinea2'
                Title.Caption = 'A2'
                Width = 18
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Taxas'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DATA3_TXT'
                Title.Caption = #218'lt.Pgto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PgDesc'
                Title.Caption = 'Pg. Desc.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ValPago'
                Title.Caption = 'Pago'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'SALDO'
                Title.Caption = 'Saldo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'ATUAL'
                Title.Caption = 'Atual'
                Visible = True
              end>
          end
          object DBGrid4: TDBGrid
            Left = 0
            Top = 40
            Width = 998
            Height = 135
            TabStop = False
            Align = alClient
            DataSource = DsAlinPgs
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Data'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Juros'
                Title.Caption = '$ Juros'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Desco'
                Title.Caption = 'Desconto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Pago'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'LotePg'
                Title.Caption = 'Border'#244
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'Controle'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vencto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'DDeposito'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Emitente'
                Width = 140
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CPF'
                Visible = True
              end>
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Tempos de tabelas'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Memo1: TMemo
            Left = 0
            Top = 0
            Width = 998
            Height = 175
            Align = alClient
            TabOrder = 0
          end
        end
      end
    end
    object PainelBtns: TPanel
      Left = 0
      Top = 537
      Width = 1008
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object PainelConf: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 48
        Align = alClient
        TabOrder = 1
        Visible = False
        object BtConfirma5: TBitBtn
          Tag = 14
          Left = 16
          Top = 5
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirma5Click
        end
        object BtDesiste5: TBitBtn
          Tag = 15
          Left = 684
          Top = 5
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtDesiste5Click
        end
      end
      object PainelOcorConf: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 48
        Align = alClient
        TabOrder = 2
        Visible = False
        object BitBtn3: TBitBtn
          Tag = 14
          Left = 16
          Top = 5
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn3Click
        end
        object BitBtn4: TBitBtn
          Tag = 15
          Left = 684
          Top = 5
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BitBtn4Click
        end
      end
      object PainelCtrl: TPanel
        Left = 0
        Top = 0
        Width = 1008
        Height = 48
        Align = alClient
        Color = clAppWorkSpace
        TabOrder = 0
        object PainelBtsE: TPanel
          Left = -23
          Top = 1
          Width = 725
          Height = 46
          BevelOuter = bvNone
          TabOrder = 0
          object BtChequeOcorr: TBitBtn
            Tag = 138
            Left = 12
            Top = 4
            Width = 108
            Height = 40
            Cursor = crHandPoint
            Caption = '&Ocorr'#234'ncia'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtChequeOcorrClick
          end
          object BtChequeDev: TBitBtn
            Tag = 136
            Left = 312
            Top = 4
            Width = 108
            Height = 40
            Cursor = crHandPoint
            Caption = '&Devolu'#231#227'o'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtChequeDevClick
          end
          object BtStatus: TBitBtn
            Tag = 139
            Left = 124
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Status'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtStatusClick
          end
          object BtRefresh: TBitBtn
            Tag = 18
            Left = 627
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Reabre'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = BtRefreshClick
          end
          object BtQuitacao: TBitBtn
            Tag = 171
            Left = 264
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Quita'#231#227'o'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            Visible = False
            OnClick = BtQuitacaoClick
          end
          object BitBtn1: TBitBtn
            Tag = 116
            Left = 534
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Border'#244
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            OnClick = BitBtn1Click
          end
          object BtDesDep: TBitBtn
            Tag = 304
            Left = 423
            Top = 4
            Width = 108
            Height = 40
            Cursor = crHandPoint
            Caption = 'D&esfazer '#13#10'dep'#243'sito'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 6
            OnClick = BtDesDepClick
          end
        end
        object Panel6: TPanel
          Left = 904
          Top = 1
          Width = 103
          Height = 46
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 7
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Controle de Cheques'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 100
      Top = 1
      Width = 825
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 116
      ExplicitTop = 13
      ExplicitWidth = 871
    end
    object LaTipo: TLabel
      Left = 925
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 777
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Panel10: TPanel
      Left = 1
      Top = 1
      Width = 99
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object BtHistorico: TBitBtn
        Tag = 259
        Left = 3
        Top = 2
        Width = 90
        Height = 40
        Caption = '&Hist'#243'rico'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        TabOrder = 0
        OnClick = BtHistoricoClick
      end
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMECLIENTE')
    Left = 521
    Top = 321
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 549
    Top = 321
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    AfterClose = QrPesqAfterClose
    AfterScroll = QrPesqAfterScroll
    OnCalcFields = QrPesqCalcFields
    SQL.Strings = (
      
        'SELECT lo.Codigo, lo.Cliente, li.Cheque, li.Controle, li.Emissao' +
        ', '
      'li.TxaCompra, li.DCompra, li.Valor, li.Desco, li.Vencto,'
      'li.Quitado,li.Devolucao, ai.Status STATUSDEV, li.ProrrVz, '
      'li.ProrrDd, li.Emitente, li.ValQuit, CASE WHEN co.Tipo=0 THEN '
      'co.RazaoSocial ELSE co.Nome END NOMECOLIGADO,'
      'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome '
      
        'END NOMECLIENTE, li.DDeposito, li.CPF, li.NaoDeposita, li.RepCli' +
        ','
      'li.Banco, li.Agencia, li.Conta, li.Depositado, li.ObsGerais,'
      'CASE WHEN cl.Tipo=0 THEN cl.ETe1 ELSE cl.PTe1 END TEL1CLI'
      'FROM lotesits li'
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits  ri ON ri.Origem=li.Controle'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN entidades co ON co.Codigo=re.Coligado'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle'
      'WHERE lo.Tipo=0'
      'AND lo.TxCompra+lo.ValValorem+ 1 >= 0.01'
      'AND li.CPF="05311748400"'
      'AND ('
      '  (li.Devolucao<>0 AND ai.Status=0)'
      'OR'
      '  (li.Devolucao=0 AND li.Vencto>=NOW())'
      'OR'
      '  (li.Devolucao<>0 AND ai.Status=1)'
      'OR'
      
        '  (li.Devolucao<>0 AND ai.Status=2 AND (ai.ValPago >  (ai.Valor+' +
        'ai.Taxas+ai.Multa+ai.JurosV-ai.Desconto)))'
      ')'
      'ORDER BY li.Vencto, li.DDeposito')
    Left = 32
    Top = 197
    object QrPesqCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 40
      Calculated = True
    end
    object QrPesqNOMESTATUS: TWideStringField
      DisplayWidth = 255
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 255
      Calculated = True
    end
    object QrPesqTAXA_COMPRA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TAXA_COMPRA'
      DisplayFormat = '#,##0.000000'
      Calculated = True
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lotesits.Controle'
      Required = True
    end
    object QrPesqEmissao: TDateField
      FieldName = 'Emissao'
      Origin = 'lotesits.Emissao'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Origin = 'lotesits.TxaCompra'
      Required = True
    end
    object QrPesqDCompra: TDateField
      FieldName = 'DCompra'
      Origin = 'lotesits.DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqValor: TFloatField
      FieldName = 'Valor'
      Origin = 'lotesits.Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesqDesco: TFloatField
      FieldName = 'Desco'
      Origin = 'lotesits.Desco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesqVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'lotesits.Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqCheque: TIntegerField
      FieldName = 'Cheque'
      Origin = 'lotesits.Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrPesqQuitado: TIntegerField
      FieldName = 'Quitado'
      Origin = 'lotesits.Quitado'
      Required = True
    end
    object QrPesqNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Origin = 'NOMECOLIGADO'
      Size = 100
    end
    object QrPesqSTATUSDEV: TSmallintField
      FieldName = 'STATUSDEV'
      Origin = 'alinits.Status'
    end
    object QrPesqDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Origin = 'lotesits.Devolucao'
      Required = True
    end
    object QrPesqCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'lotes.Cliente'
    end
    object QrPesqNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Origin = 'NOMECLIENTE'
      Size = 100
    end
    object QrPesqProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Origin = 'lotesits.ProrrVz'
      Required = True
    end
    object QrPesqProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Origin = 'lotesits.ProrrDd'
      Required = True
    end
    object QrPesqEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lotesits.Emitente'
      Size = 50
    end
    object QrPesqValQuit: TFloatField
      FieldName = 'ValQuit'
      Origin = 'lotesits.ValQuit'
      Required = True
    end
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'lotes.Codigo'
      Required = True
    end
    object QrPesqDDeposito: TDateField
      FieldName = 'DDeposito'
      Origin = 'lotesits.DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqCPF: TWideStringField
      FieldName = 'CPF'
      Origin = 'lotesits.CPF'
      Size = 15
    end
    object QrPesqRisco: TFloatField
      FieldKind = fkLookup
      FieldName = 'Risco'
      LookupDataSet = QrEmitCPF
      LookupKeyFields = 'CPF'
      LookupResultField = 'Limite'
      KeyFields = 'CPF'
      Lookup = True
    end
    object QrPesqNome: TWideStringField
      FieldKind = fkLookup
      FieldName = 'Nome'
      LookupDataSet = QrEmitCPF
      LookupKeyFields = 'CPF'
      LookupResultField = 'Nome'
      KeyFields = 'CPF'
      Size = 50
      Lookup = True
    end
    object QrPesqNaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
      Origin = 'lotesits.NaoDeposita'
      Required = True
    end
    object QrPesqRepCli: TIntegerField
      FieldName = 'RepCli'
      Origin = 'lotesits.RepCli'
      Required = True
    end
    object QrPesqBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lotesits.Banco'
      Required = True
    end
    object QrPesqAgencia: TIntegerField
      FieldName = 'Agencia'
      Origin = 'lotesits.Agencia'
      Required = True
    end
    object QrPesqConta: TWideStringField
      FieldName = 'Conta'
      Origin = 'lotesits.Conta'
    end
    object QrPesqDepositado: TSmallintField
      FieldName = 'Depositado'
      Origin = 'lotesits.Depositado'
      Required = True
    end
    object QrPesqObsGerais: TWideStringField
      FieldName = 'ObsGerais'
      Size = 60
    end
    object QrPesqTEL1CLI: TWideStringField
      FieldName = 'TEL1CLI'
    end
    object QrPesqTEL1CLI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL1CLI_TXT'
      Size = 40
      Calculated = True
    end
  end
  object QrOcorreu: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrOcorreuAfterScroll
    OnCalcFields = QrOcorreuCalcFields
    SQL.Strings = (
      'SELECT lo.Cliente CLIENTELOTE, ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.LotesIts=:P0'
      '')
    Left = 113
    Top = 317
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorreuNOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcorreuCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOcorreuLotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrOcorreuDataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrOcorreuValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreuLoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrOcorreuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorreuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorreuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorreuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorreuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorreuTaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
      DisplayFormat = '#,###,##0.0000'
    end
    object QrOcorreuTaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
    end
    object QrOcorreuPago: TFloatField
      FieldName = 'Pago'
      Required = True
    end
    object QrOcorreuDataP: TDateField
      FieldName = 'DataP'
      Required = True
    end
    object QrOcorreuTaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrOcorreuCLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
    object QrOcorreuData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrOcorreuStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrOcorreuCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOcorreuSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreuATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsOcorreu: TDataSource
    DataSet = QrOcorreu
    Left = 141
    Top = 317
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 60
    Top = 197
  end
  object QrOcorBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ocorbank'
      'ORDER BY Nome')
    Left = 577
    Top = 321
    object QrOcorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrOcorBankLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorBankDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorBankDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorBankUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorBankUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorBankBase: TFloatField
      FieldName = 'Base'
    end
  end
  object DsOcorBank: TDataSource
    DataSet = QrOcorBank
    Left = 605
    Top = 321
  end
  object QrBanco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM bancos'
      'WHERE Codigo=:P0')
    Left = 93
    Top = 197
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBancoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrBancoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBancoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBancoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBancoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBancoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBancoSite: TWideStringField
      FieldName = 'Site'
      Size = 255
    end
  end
  object DsBanco: TDataSource
    DataSet = QrBanco
    Left = 121
    Top = 197
  end
  object PMOcorreu: TPopupMenu
    OnPopup = PMOcorreuPopup
    Left = 32
    Top = 433
    object Incluiocorrncia1: TMenuItem
      Caption = '&Inclui ocorr'#234'ncia'
      OnClick = Incluiocorrncia1Click
    end
    object Alteraocorrncia1: TMenuItem
      Caption = '&Altera ocorr'#234'ncia'
      OnClick = Alteraocorrncia1Click
    end
    object Excluiocorrncia1: TMenuItem
      Caption = '&Exclui ocorr'#234'ncia'
      OnClick = Excluiocorrncia1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object IncluiPagamento2: TMenuItem
      Caption = 'Inclui &Pagamento'
      OnClick = IncluiPagamento2Click
    end
    object Excluipagamento2: TMenuItem
      Caption = 'E&xclui pagamento'
      OnClick = Excluipagamento2Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Recibodopagamento2: TMenuItem
      Caption = '&Recibo do pagamento'
      OnClick = Recibodopagamento2Click
    end
  end
  object QrLocPr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM lotesprr'
      'WHERE Data3=('
      'SELECT Max(Data3) FROM lotesprr'
      'WHERE Codigo=:P0)'
      'ORDER BY Controle DESC')
    Left = 841
    Top = 81
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocPrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocPrData1: TDateField
      FieldName = 'Data1'
    end
    object QrLocPrData2: TDateField
      FieldName = 'Data2'
    end
    object QrLocPrData3: TDateField
      FieldName = 'Data3'
    end
    object QrLocPrOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
    end
    object QrLocPrLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocPrDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocPrDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocPrUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocPrUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object QrLotesPrr: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLotesPrrCalcFields
    SQL.Strings = (
      'SELECT * FROM lotesprr'
      'WHERE Codigo=:P0'
      'ORDER BY Controle')
    Left = 537
    Top = 371
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotesPrrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLotesPrrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLotesPrrData1: TDateField
      FieldName = 'Data1'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotesPrrData2: TDateField
      FieldName = 'Data2'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotesPrrData3: TDateField
      FieldName = 'Data3'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotesPrrOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
    end
    object QrLotesPrrLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLotesPrrDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLotesPrrDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLotesPrrUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLotesPrrUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLotesPrrDIAS_1_3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DIAS_1_3'
      Calculated = True
    end
    object QrLotesPrrDIAS_2_3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DIAS_2_3'
      Calculated = True
    end
  end
  object DsLotesPrr: TDataSource
    DataSet = QrLotesPrr
    Left = 565
    Top = 371
  end
  object PMStatus: TPopupMenu
    OnPopup = PMStatusPopup
    Left = 112
    Top = 429
    object Incluiprorrogao1: TMenuItem
      Caption = '&Inclui prorroga'#231#227'o / Antecipa'#231#227'o'
      OnClick = Incluiprorrogao1Click
    end
    object IncluiQuitao1: TMenuItem
      Caption = 'Inclui &Quita'#231#227'o (Cheque sai do dep'#243'sito)'
      OnClick = IncluiQuitao1Click
    end
    object ExcluiProrrogao1: TMenuItem
      Caption = '&Exclui Prorroga'#231#227'o / Antecipa'#231#227'o / Quita'#231#227'o'
      OnClick = ExcluiProrrogao1Click
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object N0ForastatusAutomtico1: TMenuItem
      Caption = '&0. For'#231'a status Autom'#225'tico'
      OnClick = N0ForastatusAutomtico1Click
    end
    object N1Forastatus1: TMenuItem
      Caption = '&1. For'#231'a status Prorrogado '
      OnClick = N1Forastatus1Click
    end
    object N2ForastatusBaixado1: TMenuItem
      Caption = '&2. For'#231'a status Baixado'
      OnClick = N2ForastatusBaixado1Click
    end
    object N3ForastatusMoroso1: TMenuItem
      Caption = '&3. For'#231'a status Moroso'
      OnClick = N3ForastatusMoroso1Click
    end
  end
  object QrAlinIts: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrAlinItsBeforeClose
    AfterScroll = QrAlinItsAfterScroll
    OnCalcFields = QrAlinItsCalcFields
    SQL.Strings = (
      'SELECT al1.Nome NOMEALINEA1, al2.Nome NOMEALINEA2, ai.*'
      'FROM alinits ai'
      'LEFT JOIN alineas al1 ON al1.Codigo=ai.Alinea1'
      'LEFT JOIN alineas al2 ON al2.Codigo=ai.Alinea2'
      'WHERE ai.ChequeOrigem=:P0')
    Left = 633
    Top = 321
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAlinItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAlinItsAlinea1: TIntegerField
      FieldName = 'Alinea1'
      DisplayFormat = '00;-00; '
    end
    object QrAlinItsAlinea2: TIntegerField
      FieldName = 'Alinea2'
      DisplayFormat = '00;-00; '
    end
    object QrAlinItsData1: TDateField
      FieldName = 'Data1'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrAlinItsData2: TDateField
      FieldName = 'Data2'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrAlinItsData3: TDateField
      FieldName = 'Data3'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrAlinItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrAlinItsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrAlinItsAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrAlinItsConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrAlinItsCheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrAlinItsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrAlinItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinItsTaxas: TFloatField
      FieldName = 'Taxas'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrAlinItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAlinItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAlinItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAlinItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAlinItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrAlinItsCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrAlinItsDATA1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA1_TXT'
      Size = 12
      Calculated = True
    end
    object QrAlinItsDATA2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA2_TXT'
      Size = 12
      Calculated = True
    end
    object QrAlinItsDATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Size = 12
      Calculated = True
    end
    object QrAlinItsChequeOrigem: TIntegerField
      FieldName = 'ChequeOrigem'
      Required = True
    end
    object QrAlinItsStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrAlinItsValPago: TFloatField
      FieldName = 'ValPago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinItsMulta: TFloatField
      FieldName = 'Multa'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinItsJurosP: TFloatField
      FieldName = 'JurosP'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinItsJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinItsDesconto: TFloatField
      FieldName = 'Desconto'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinItsNOMEALINEA1: TWideStringField
      FieldName = 'NOMEALINEA1'
      Size = 250
    end
    object QrAlinItsNOMEALINEA2: TWideStringField
      FieldName = 'NOMEALINEA2'
      Size = 250
    end
    object QrAlinItsSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrAlinItsATUAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrAlinItsPgDesc: TFloatField
      FieldName = 'PgDesc'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsAlinIts: TDataSource
    DataSet = QrAlinIts
    Left = 661
    Top = 321
  end
  object QrAlinPgs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ali.Vencto, ali.DDeposito,'
      'ali.Banco, ali.Agencia, ali.Conta, ali.Cheque, '
      'ali.CPF, ali.Emitente, alp.*'
      'FROM alinpgs alp'
      'LEFT JOIN alinits ali ON ali.Codigo=alp.Codigo'
      'WHERE alp.AlinIts=:P0'
      'ORDER BY alp.Data')
    Left = 689
    Top = 321
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAlinPgsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAlinPgsAlinIts: TIntegerField
      FieldName = 'AlinIts'
    end
    object QrAlinPgsData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrAlinPgsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrAlinPgsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAlinPgsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAlinPgsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAlinPgsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAlinPgsJuros: TFloatField
      FieldName = 'Juros'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinPgsPago: TFloatField
      FieldName = 'Pago'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAlinPgsLotePg: TIntegerField
      FieldName = 'LotePg'
      DisplayFormat = '0;-0; '
    end
    object QrAlinPgsVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrAlinPgsDDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrAlinPgsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrAlinPgsAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrAlinPgsConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrAlinPgsCheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrAlinPgsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrAlinPgsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrAlinPgsAPCD: TIntegerField
      FieldName = 'APCD'
      Required = True
    end
    object QrAlinPgsDataCh: TDateField
      FieldName = 'DataCh'
      Required = True
    end
    object QrAlinPgsDesco: TFloatField
      FieldName = 'Desco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsAlinPgs: TDataSource
    DataSet = QrAlinPgs
    Left = 717
    Top = 321
  end
  object PMDevolucao: TPopupMenu
    OnPopup = PMDevolucaoPopup
    Left = 241
    Top = 440
    object IncluiPagamento1: TMenuItem
      Caption = '&Inclui Pagamento'
      OnClick = IncluiPagamento1Click
    end
    object ExcluiPagamento1: TMenuItem
      Caption = '&Exclui Pagamento'
      OnClick = ExcluiPagamento1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Alteradevoluo1: TMenuItem
      Caption = '&Altera devolu'#231#227'o'
      OnClick = Alteradevoluo1Click
    end
    object Desfazdevoluo1: TMenuItem
      Caption = '&Desfaz devolu'#231#227'o'
      OnClick = Desfazdevoluo1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object RecibodoPagamento1: TMenuItem
      Caption = 'Recibo do Pagamento'
      OnClick = RecibodoPagamento1Click
    end
  end
  object QrAlineas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM alineas'
      'ORDER BY Nome')
    Left = 684
    Top = 80
    object QrAlineasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAlineasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrAlineasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrAlineasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAlineasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAlineasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAlineasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsAlinea1: TDataSource
    DataSet = QrAlineas
    Left = 712
    Top = 80
  end
  object DsAlinea2: TDataSource
    DataSet = QrAlineas
    Left = 740
    Top = 80
  end
  object QrSumPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Juros) Juros, SUM(Pago) Pago, SUM(Desco) Desco'
      'FROM alinpgs'
      'WHERE AlinIts=:P0')
    Left = 869
    Top = 81
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPgJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumPgPago: TFloatField
      FieldName = 'Pago'
    end
    object QrSumPgDesco: TFloatField
      FieldName = 'Desco'
    end
  end
  object QrLocPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM alinpgs'
      'WHERE Data=('
      'SELECT Max(Data) FROM alinpgs'
      'WHERE AlinIts=:P0)'
      'ORDER BY Codigo DESC')
    Left = 897
    Top = 81
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocPgAlinIts: TIntegerField
      FieldName = 'AlinIts'
    end
    object QrLocPgData: TDateField
      FieldName = 'Data'
    end
    object QrLocPgLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocPgDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocPgDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocPgUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocPgUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLocPgJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrLocPgPago: TFloatField
      FieldName = 'Pago'
    end
  end
  object QrLast: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Data) Data FROM alinpgs'
      'WHERE AlinIts=:P0')
    Left = 925
    Top = 81
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLastData: TDateField
      FieldName = 'Data'
    end
  end
  object QrBco1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome FROM bancos'
      'WHERE Codigo=:P0')
    Left = 165
    Top = 193
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBco1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsColigado: TDataSource
    DataSet = QrColigado
    Left = 264
    Top = 141
  end
  object QrColigado: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECOLIGADO'
      'FROM entidades'
      'WHERE Fornece2="V"')
    Left = 236
    Top = 141
    object QrColigadoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrColigadoNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
  end
  object QrOcorrPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ocorrpg'
      'WHERE Ocorreu =:P0')
    Left = 441
    Top = 374
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorrPgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorrPgOcorreu: TIntegerField
      FieldName = 'Ocorreu'
    end
    object QrOcorrPgData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorrPgJuros: TFloatField
      FieldName = 'Juros'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorrPgPago: TFloatField
      FieldName = 'Pago'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorrPgLotePg: TIntegerField
      FieldName = 'LotePg'
      DisplayFormat = '0000'
    end
    object QrOcorrPgLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorrPgDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorrPgDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorrPgUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorrPgUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsOcorrPg: TDataSource
    DataSet = QrOcorrPg
    Left = 469
    Top = 374
  end
  object QrLastOcor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Codigo'
      'FROM ocorrpg'
      'WHERE Ocorreu=:P0'
      'ORDER BY Data Desc, Codigo Desc')
    Left = 397
    Top = 373
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLastOcorData: TDateField
      FieldName = 'Data'
    end
    object QrLastOcorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSumOc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Juros) Juros, SUM(Pago) Pago'
      'FROM ocorrpg'
      'WHERE Ocorreu=:P0')
    Left = 369
    Top = 373
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumOcJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumOcPago: TFloatField
      FieldName = 'Pago'
    end
  end
  object QrLocOc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ocorrpg'
      'WHERE Data=('
      'SELECT Max(Data) FROM ocorrpg'
      'WHERE Ocorreu=:P0)'
      'ORDER BY Codigo DESC')
    Left = 341
    Top = 373
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocOcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocOcOcorreu: TIntegerField
      FieldName = 'Ocorreu'
    end
    object QrLocOcData: TDateField
      FieldName = 'Data'
    end
    object QrLocOcJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrLocOcPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLocOcLotePg: TIntegerField
      FieldName = 'LotePg'
    end
    object QrLocOcLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocOcDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocOcDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocOcUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocOcUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object PMQuitacao: TPopupMenu
    Left = 436
    Top = 451
    object Quitadocumento1: TMenuItem
      Caption = '&Quita documento'
      OnClick = Quitadocumento1Click
    end
    object Imprimerecibodequitao1: TMenuItem
      Caption = '&Imprime recibo de quita'#231#227'o'
      OnClick = Imprimerecibodequitao1Click
    end
  end
  object QrAPCD: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Deposita, ExigeCMC7 '
      'FROM apcd'
      'ORDER BY Nome')
    Left = 964
    Top = 81
    object QrAPCDCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAPCDNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrAPCDDeposita: TSmallintField
      FieldName = 'Deposita'
    end
    object QrAPCDExigeCMC7: TSmallintField
      FieldName = 'ExigeCMC7'
    end
  end
  object DsAPCD: TDataSource
    DataSet = QrAPCD
    Left = 992
    Top = 81
  end
  object QrSCB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SCB'
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 240
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSCBSCB: TIntegerField
      FieldName = 'SCB'
    end
  end
  object QrBanco2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, DVCC '
      'FROM bancos'
      'WHERE Codigo=:P0')
    Left = 229
    Top = 49
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBanco2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrBanco2DVCC: TSmallintField
      FieldName = 'DVCC'
    end
  end
  object DsBanco2: TDataSource
    DataSet = QrBanco2
    Left = 257
    Top = 49
  end
  object QrSacRiscoTC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(li.Valor) Valor'
      'FROM lotesits li '
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Cliente=:P0'
      'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())'
      'AND lo.Tipo = 0')
    Left = 525
    Top = 437
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacRiscoTCValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSacRiscoTC: TDataSource
    DataSet = QrSacRiscoTC
    Left = 553
    Top = 437
  end
  object QrSacRiscoC: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSacRiscoCCalcFields
    SQL.Strings = (
      
        'SELECT lo.Tipo, li.Banco, li.Agencia, li.Conta, li.Cheque, li.Va' +
        'lor, '
      'li.DCompra, li.DDeposito, li.Emitente, li.CPF'
      'FROM lotesits li '
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Cliente=:P0'
      'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())'
      'AND lo.Tipo = 0'
      'ORDER BY DDeposito')
    Left = 525
    Top = 409
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacRiscoCBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrSacRiscoCAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrSacRiscoCConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrSacRiscoCCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrSacRiscoCValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacRiscoCDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacRiscoCDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacRiscoCEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrSacRiscoCCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrSacRiscoCCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsSacRiscoC: TDataSource
    DataSet = QrSacRiscoC
    Left = 553
    Top = 409
  end
  object QrSacDOpen: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSacDOpenAfterOpen
    BeforeClose = QrSacDOpenBeforeClose
    OnCalcFields = QrSacDOpenCalcFields
    SQL.Strings = (
      'SELECT od.Nome STATUS, li.Controle, li.Duplicata, li.Repassado,'
      'li.DCompra, li.Valor, li.DDeposito, li.Emitente, li.CPF, '
      'lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg,'
      'li.Vencto, li.Data3'
      'FROM lotesits li'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'WHERE lo.Tipo=1 '
      'AND li.Quitado <> 2'
      'AND lo.Cliente=:P0'
      'ORDER BY li.Vencto')
    Left = 581
    Top = 409
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacDOpenControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrSacDOpenDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrSacDOpenDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacDOpenValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacDOpenDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacDOpenEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrSacDOpenCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrSacDOpenCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSacDOpenSTATUS: TWideStringField
      FieldName = 'STATUS'
      Size = 50
    end
    object QrSacDOpenQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrSacDOpenTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacDOpenTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacDOpenTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacDOpenSALDO_DESATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_DESATUALIZ'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSacDOpenSALDO_ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSacDOpenNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 30
      Calculated = True
    end
    object QrSacDOpenVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrSacDOpenDDCALCJURO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DDCALCJURO'
      Calculated = True
    end
    object QrSacDOpenData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrSacDOpenRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
  end
  object DsSacDOpen: TDataSource
    DataSet = QrSacDOpen
    Left = 609
    Top = 409
  end
  object QrSacCHDevA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSacCHDevAAfterOpen
    BeforeClose = QrSacCHDevABeforeClose
    OnCalcFields = QrSacCHDevACalcFields
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLIENTE, ai.*'
      'FROM alinits ai'
      'LEFT JOIN entidades en ON en.Codigo=ai.Cliente'
      'WHERE ai.Status<2'
      'AND ai.Cliente=:P0'
      'ORDER BY ai.Data1, ai.Data2')
    Left = 637
    Top = 409
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacCHDevADATA1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA1_TXT'
      Calculated = True
    end
    object QrSacCHDevADATA2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA2_TXT'
      Calculated = True
    end
    object QrSacCHDevADATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Calculated = True
    end
    object QrSacCHDevACPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrSacCHDevANOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrSacCHDevACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSacCHDevAAlinea1: TIntegerField
      FieldName = 'Alinea1'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrSacCHDevAAlinea2: TIntegerField
      FieldName = 'Alinea2'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrSacCHDevAData1: TDateField
      FieldName = 'Data1'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacCHDevAData2: TDateField
      FieldName = 'Data2'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacCHDevAData3: TDateField
      FieldName = 'Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacCHDevACliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrSacCHDevABanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrSacCHDevAAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrSacCHDevAConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrSacCHDevACheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrSacCHDevACPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrSacCHDevAValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevATaxas: TFloatField
      FieldName = 'Taxas'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSacCHDevADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSacCHDevADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSacCHDevAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSacCHDevAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSacCHDevAEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrSacCHDevAChequeOrigem: TIntegerField
      FieldName = 'ChequeOrigem'
      Required = True
    end
    object QrSacCHDevAStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrSacCHDevAValPago: TFloatField
      FieldName = 'ValPago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevAMulta: TFloatField
      FieldName = 'Multa'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevAJurosP: TFloatField
      FieldName = 'JurosP'
      Required = True
      DisplayFormat = '#,###,##0.000000'
    end
    object QrSacCHDevAJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevADesconto: TFloatField
      FieldName = 'Desconto'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevASALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSacCHDevAATUAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSacCHDevAPgDesc: TFloatField
      FieldName = 'PgDesc'
      Required = True
    end
    object QrSacCHDevADDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
    end
    object QrSacCHDevAVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrSacCHDevALoteOrigem: TIntegerField
      FieldName = 'LoteOrigem'
      Required = True
    end
  end
  object DsSacCHDevA: TDataSource
    DataSet = QrSacCHDevA
    Left = 665
    Top = 409
  end
  object QrSacOcorA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSacOcorAAfterOpen
    BeforeClose = QrSacOcorABeforeClose
    OnCalcFields = QrSacOcorACalcFields
    SQL.Strings = (
      'SELECT IF(oc.Cliente > 0,'#39'CL'#39',IF(lo.Tipo=0,"CH","DU"))TIPODOC, '
      'ob.Nome NOMEOCORRENCIA, lo.Tipo, lo.Cliente CLIENTELOTE, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.Status<2'
      'AND (lo.Cliente=:P0 OR oc.Cliente=:P1)')
    Left = 693
    Top = 409
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSacOcorATipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrSacOcorATIPODOC: TWideStringField
      FieldName = 'TIPODOC'
      Size = 2
    end
    object QrSacOcorANOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrSacOcorACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSacOcorALotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrSacOcorADataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacOcorAOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrSacOcorAValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacOcorALoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrSacOcorALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSacOcorADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSacOcorADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSacOcorAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSacOcorAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSacOcorATaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrSacOcorATaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSacOcorAPago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSacOcorADataP: TDateField
      FieldName = 'DataP'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacOcorATaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrSacOcorAData3: TDateField
      FieldName = 'Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacOcorAStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrSacOcorASALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,###,##0.00'
      Calculated = True
    end
    object QrSacOcorAATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,###,##0.00'
      Calculated = True
    end
    object QrSacOcorACliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrSacOcorACLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
  end
  object DsSacOcorA: TDataSource
    DataSet = QrSacOcorA
    Left = 721
    Top = 409
  end
  object QrCli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM lotes'
      'WHERE Codigo=:P0')
    Left = 120
    Top = 249
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrAntePror: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lit.Controle ITEMLOTE, lit.VlrCompra, '
      'prr.Controle, prr.Data1, prr.Data2, prr.Data3, '
      'lit.Cheque, lit.Banco, lit.Agencia, lit.Conta, lit.Emitente,'
      'lit.Valor VALORCHEQUE, oco.Valor VALORRESSARCIDO'
      'FROM lotesprr prr'
      'LEFT JOIN ocorreu oco ON oco.Codigo=prr.Ocorrencia'
      'LEFT JOIN lotesits lit ON lit.Controle=prr.Codigo'
      'WHERE prr.Controle=:P0')
    Left = 52
    Top = 257
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAnteProrData1: TDateField
      FieldName = 'Data1'
      Required = True
    end
    object QrAnteProrData2: TDateField
      FieldName = 'Data2'
      Required = True
    end
    object QrAnteProrData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrAnteProrCheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrAnteProrBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrAnteProrAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrAnteProrConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrAnteProrEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrAnteProrVALORCHEQUE: TFloatField
      FieldName = 'VALORCHEQUE'
    end
    object QrAnteProrVALORRESSARCIDO: TFloatField
      FieldName = 'VALORRESSARCIDO'
    end
    object QrAnteProrControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrAnteProrITEMLOTE: TIntegerField
      FieldName = 'ITEMLOTE'
      Required = True
    end
    object QrAnteProrVlrCompra: TFloatField
      FieldName = 'VlrCompra'
    end
  end
  object PMReciboAntecip: TPopupMenu
    Left = 369
    Top = 433
    object Meuparaocliente1: TMenuItem
      Caption = '&Meu para o cliente'
      OnClick = Meuparaocliente1Click
    end
    object Clienteparamim1: TMenuItem
      Caption = '&Cliente para mim'
      OnClick = Clienteparamim1Click
    end
  end
  object PMHistorico: TPopupMenu
    Left = 113
    Top = 9
    object Clientepesquisado1: TMenuItem
      Caption = '&1. Cliente pesquisado'
      OnClick = Clientepesquisado1Click
    end
    object Emitentesacadopesquisado1: TMenuItem
      Caption = '&2. Emitente/sacado pesquisado'
      OnClick = Emitentesacadopesquisado1Click
    end
    object Ambos2: TMenuItem
      Caption = '&3. Ambos'
      OnClick = Ambos2Click
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object Clienteselecionado1: TMenuItem
      Caption = '&4. Cliente selecionado'
      OnClick = Clienteselecionado1Click
    end
    object Emitentesacadoselecionado1: TMenuItem
      Caption = '&5. Emitente/sacado selecionado'
      OnClick = Emitentesacadoselecionado1Click
    end
    object Ambos1: TMenuItem
      Caption = '&6. Ambos'
      OnClick = Ambos1Click
    end
  end
  object QrEmitCPF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM emitcpf')
    Left = 32
    Top = 225
    object QrEmitCPFCPF: TWideStringField
      FieldName = 'CPF'
      Origin = 'emitcpf.CPF'
      Size = 15
    end
    object QrEmitCPFNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'emitcpf.Nome'
      Size = 50
    end
    object QrEmitCPFLimite: TFloatField
      FieldName = 'Limite'
      Origin = 'emitcpf.Limite'
    end
    object QrEmitCPFLastAtz: TDateField
      FieldName = 'LastAtz'
      Origin = 'emitcpf.LastAtz'
    end
    object QrEmitCPFAcumCHComV: TFloatField
      FieldName = 'AcumCHComV'
      Origin = 'emitcpf.AcumCHComV'
    end
    object QrEmitCPFAcumCHComQ: TIntegerField
      FieldName = 'AcumCHComQ'
      Origin = 'emitcpf.AcumCHComQ'
    end
    object QrEmitCPFAcumCHDevV: TFloatField
      FieldName = 'AcumCHDevV'
      Origin = 'emitcpf.AcumCHDevV'
    end
    object QrEmitCPFAcumCHDevQ: TIntegerField
      FieldName = 'AcumCHDevQ'
      Origin = 'emitcpf.AcumCHDevQ'
    end
  end
  object QrEmitBAC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM emitbac')
    Left = 60
    Top = 225
    object QrEmitBACBAC: TWideStringField
      FieldName = 'BAC'
    end
    object QrEmitBACCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSomaAfterOpen
    SQL.Strings = (
      'SELECT COUNT(Controle) Itens, SUM(Valor) Valor'
      'FROM lotesits')
    Left = 648
    Top = 376
    object QrSomaItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
    object QrSomaValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSoma: TDataSource
    DataSet = QrSoma
    Left = 676
    Top = 376
  end
  object Query1: TmySQLQuery
    Database = Dmod.MyDB
    Left = 828
    Top = 148
    object Query1Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
end
