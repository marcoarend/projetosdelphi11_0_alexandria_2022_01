unit PesqCPFCNPJ;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, Db, mySQLDbTables,
  dmkEdit, dmkGeral;

type
  TFmPesqCPFCNPJ = class(TForm)
    PainelConfirma: TPanel;
    BtPesquisa: TBitBtn;
    BtConfirma: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    RGMascara: TRadioGroup;
    Label1: TLabel;
    EdEmitente: TdmkEdit;
    DBGrid1: TDBGrid;
    QrEmitCPF: TmySQLQuery;
    DsEmitCPF: TDataSource;
    QrEmitCPFNome: TWideStringField;
    QrEmitCPFCPF: TWideStringField;
    QrEmitCPFTipo: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure RGMascaraClick(Sender: TObject);
    procedure EdEmitenteChange(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmPesqCPFCNPJ: TFmPesqCPFCNPJ;

implementation

uses Module, UnInternalConsts, UnMyObjects;

{$R *.DFM}

procedure TFmPesqCPFCNPJ.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqCPFCNPJ.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPesqCPFCNPJ.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmPesqCPFCNPJ.BtPesquisaClick(Sender: TObject);
var
  Emitente: String;
begin
  //if Trim(EdEmitente.Text) <> '' then
  //begin
    Emitente := EdEmitente.Text;
    if RGMascara.ItemIndex in ([0,1]) then Emitente := '%'+Emitente;
    if RGMascara.ItemIndex in ([0,2]) then Emitente := Emitente+'%';
  //end else Emitente := '';
  //////////////////////////////////////////////////////////////////////////////
  QrEmitCPF.Close;
  QrEmitCPF.Params[0].AsString := Emitente;
  QrEmitCPF.Params[1].AsString := Emitente;
  QrEmitCPF.Open;
  //
  BtPesquisa.Enabled := False;
  BtConfirma.Enabled := True;
  //configurar grade
end;

procedure TFmPesqCPFCNPJ.RGMascaraClick(Sender: TObject);
begin
  BtPesquisa.Enabled := True;
  BtConfirma.Enabled := False;
end;

procedure TFmPesqCPFCNPJ.EdEmitenteChange(Sender: TObject);
begin
  BtPesquisa.Enabled := True;
  BtConfirma.Enabled := False;
end;

procedure TFmPesqCPFCNPJ.DBGrid1DblClick(Sender: TObject);
begin
  BtConfirmaClick(Self);
end;

procedure TFmPesqCPFCNPJ.BtConfirmaClick(Sender: TObject);
begin
  VAR_CPF_PESQ := Geral.FormataCNPJ_TT(QrEmitCPFCPF.Value);
  Close;
end;

end.
