unit LotesWeb;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, Db, mySQLDbTables,
  dmkCheckGroup;

type
  TFmLotesWeb = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DBGrid1: TDBGrid;
    QrLLC: TmySQLQuery;
    QrLLCCodigo: TIntegerField;
    QrLLCCliente: TIntegerField;
    QrLLCLote: TIntegerField;
    QrLLCTipo: TSmallintField;
    QrLLCData: TDateField;
    QrLLCTotal: TFloatField;
    QrLLCDias: TFloatField;
    QrLLCItens: TIntegerField;
    QrLLCBaixado: TIntegerField;
    QrLLCCPF: TWideStringField;
    QrLLCNOMECLI: TWideStringField;
    QrLLCNOMETIPO: TWideStringField;
    QrLLI: TmySQLQuery;
    QrLLICodigo: TIntegerField;
    QrLLIControle: TIntegerField;
    QrLLIComp: TIntegerField;
    QrLLIPraca: TIntegerField;
    QrLLIBanco: TIntegerField;
    QrLLIAgencia: TIntegerField;
    QrLLIConta: TWideStringField;
    QrLLICheque: TIntegerField;
    QrLLICPF: TWideStringField;
    QrLLIEmitente: TWideStringField;
    QrLLIBruto: TFloatField;
    QrLLIDesco: TFloatField;
    QrLLIValor: TFloatField;
    QrLLIEmissao: TDateField;
    QrLLIDCompra: TDateField;
    QrLLIDDeposito: TDateField;
    QrLLIVencto: TDateField;
    QrLLIDias: TIntegerField;
    QrLLIDuplicata: TWideStringField;
    QrLLIIE: TWideStringField;
    QrLLIRua: TWideStringField;
    QrLLINumero: TLargeintField;
    QrLLICompl: TWideStringField;
    QrLLIBairro: TWideStringField;
    QrLLICidade: TWideStringField;
    QrLLIUF: TWideStringField;
    QrLLICEP: TIntegerField;
    QrLLITel1: TWideStringField;
    QrLLIUser_ID: TIntegerField;
    QrLLIPasso: TIntegerField;
    BitBtn1: TBitBtn;
    Panel3: TPanel;
    CgStatus: TdmkCheckGroup;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
  private
    { Private declarations }
    procedure DefineLoteWeb(LoteWeb: Integer; CloseForm: Boolean);
  public
    { Public declarations }
  end;

  var
  FmLotesWeb: TFmLotesWeb;

implementation

{$R *.DFM}

uses ModulC, Lotes1, Module, Principal, UnMyObjects;

procedure TFmLotesWeb.DefineLoteWeb(LoteWeb: Integer; CloseForm: Boolean);
begin
  case FmPrincipal.FFormLotesShow of
    //0: FmLotes0.FLoteWeb := LoteWeb;
    1: FmLotes1.FLoteWeb := LoteWeb;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (12)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  if CloseForm then
    FmLotesWeb.Close;
end;

procedure TFmLotesWeb.BtSaidaClick(Sender: TObject);
begin
  DefineLoteWeb(0, True);
end;

procedure TFmLotesWeb.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmLotesWeb.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLotesWeb.BtOKClick(Sender: TObject);
begin
  DefineLoteWeb(Dmoc.QrLLCCodigo.Value, True);
end;

procedure TFmLotesWeb.DBGrid1DblClick(Sender: TObject);
begin
  DefineLoteWeb(Dmoc.QrLLCCodigo.Value, True);
end;

procedure TFmLotesWeb.BitBtn1Click(Sender: TObject);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE LLoteCab SET Baixado=4 WHERE Codigo=:P0');
  Dmod.QrUpd.Params[0].AsInteger := Dmoc.QrLLCCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  //
  Dmoc.ReopenLLoteCab(CgStatus.Value);
end;

end.
