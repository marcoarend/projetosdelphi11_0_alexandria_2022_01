unit GruSacEmi;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus, Grids,
  DBGrids, dmkGeral, dmkEdit, UnDmkProcFunc;

type
  TFmGruSacEmi = class(TForm)
    PainelDados: TPanel;
    DsGruSacEmi: TDataSource;
    QrGruSacEmi: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    QrGruSacEmiLk: TIntegerField;
    QrGruSacEmiDataCad: TDateField;
    QrGruSacEmiDataAlt: TDateField;
    QrGruSacEmiUserCad: TIntegerField;
    QrGruSacEmiUserAlt: TIntegerField;
    QrGruSacEmiCodigo: TSmallintField;
    QrGruSacEmiNome: TWideStringField;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtEmiSac: TBitBtn;
    BtGrupo: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    PMGrupo: TPopupMenu;
    Inclui1: TMenuItem;
    Altera1: TMenuItem;
    Exclui1: TMenuItem;
    PMEmiSac: TPopupMenu;
    Adiciona1: TMenuItem;
    Remove1: TMenuItem;
    N1: TMenuItem;
    Nomeiagrupo1: TMenuItem;
    PnSacEmi: TPanel;
    Panel2: TPanel;
    BtConfIts: TBitBtn;
    BtDesiIts: TBitBtn;
    Panel4: TPanel;
    Label3: TLabel;
    EdCPF1: TdmkEdit;
    DBEdEmitente: TDBEdit;
    QrEmiSac: TmySQLQuery;
    QrEmiSacNome: TWideStringField;
    QrEmiSacCPF: TWideStringField;
    QrEmiSacTipo: TWideStringField;
    DsEmiSac: TDataSource;
    DBGrid1: TDBGrid;
    QrGruSacEmiIts: TmySQLQuery;
    DsGruSacEmiIts: TDataSource;
    QrLoc: TmySQLQuery;
    QrLocCNPJ_CPF: TWideStringField;
    QrGruSacEmiItsCodigo: TIntegerField;
    QrGruSacEmiItsCNPJ_CPF: TWideStringField;
    QrGruSacEmiItsNome: TWideStringField;
    EdNomeEmiSac: TdmkEdit;
    Edita1: TMenuItem;
    QrGruSacEmiItsCNPJ_CPF_TXT: TWideStringField;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrGruSacEmiAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrGruSacEmiBeforeOpen(DataSet: TDataSet);
    procedure BtGrupoClick(Sender: TObject);
    procedure Altera1Click(Sender: TObject);
    procedure Inclui1Click(Sender: TObject);
    procedure BtEmiSacClick(Sender: TObject);
    procedure Adiciona1Click(Sender: TObject);
    procedure BtDesiItsClick(Sender: TObject);
    procedure BtConfItsClick(Sender: TObject);
    procedure EdCPF1Exit(Sender: TObject);
    procedure EdCPF1Change(Sender: TObject);
    procedure QrGruSacEmiAfterScroll(DataSet: TDataSet);
    procedure DBEdEmitenteChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Remove1Click(Sender: TObject);
    procedure Nomeiagrupo1Click(Sender: TObject);
    procedure Edita1Click(Sender: TObject);
    procedure QrGruSacEmiItsCalcFields(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure ReopenEmiSac(CPF_CNPJ: String; Reabre: Boolean);
    procedure ReopenGruSacEmiIts;
  public
    { Public declarations }
    FSeq, FGrupoIni: Integer;
    FLocIni: Boolean;
  end;

var
  FmGruSacEmi: TFmGruSacEmi;
const
  FFormatFloat = '00000';

implementation

uses Module, PesqCPFCNPJ, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmGruSacEmi.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmGruSacEmi.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrGruSacEmiCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmGruSacEmi.DefParams;
begin
  VAR_GOTOTABELA := 'GruSacEmi';
  VAR_GOTOMYSQLTABLE := QrGruSacEmi;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM grusacemi');
  VAR_SQLx.Add('WHERE Codigo > -1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmGruSacEmi.MostraEdicao(Mostra : Integer; Status : String; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible := True;
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
      PnSacEmi.Visible       := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      EdNome.Text := CO_VAZIO;
      EdNome.Visible := True;
      PainelControle.Visible:=False;
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.Text := '';
        EdNome.Text   := '';
      end else begin
        EdCodigo.Text := IntToStr(QrGruSacEmiCodigo.Value);
        EdNome.Text   := QrGruSacEmiNome.Value;
      end;
      EdNome.SetFocus;
    end;
    2:
    begin
      PnSacEmi.Visible := True;
      PainelDados.Visible := False;
      //
      if Status = CO_INCLUSAO then
      begin
        EdCPF1.Text := '';
        EdNomeEmiSac.Text := '';
        EdCPF1.ReadOnly := False;
      end else begin
        EdCPF1.Text := MLAGeral.FormataCNPJ_TFT(QrGruSacEmiItsCNPJ_CPF.Value);
        EdNomeEmiSac.Text := QrGruSacEmiItsNome.Value;
        EdCPF1.ReadOnly := True;
      end;
      EdCPF1.SetFocus;
    end;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmGruSacEmi.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmGruSacEmi.QueryPrincipalAfterOpen;
begin
end;

procedure TFmGruSacEmi.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmGruSacEmi.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmGruSacEmi.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmGruSacEmi.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmGruSacEmi.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmGruSacEmi.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrGruSacEmiCodigo.Value;
  Close;
end;

procedure TFmGruSacEmi.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO grusacemi SET ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'GruSacEmi', 'GruSacEmi', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE grusacemi SET ');
    Codigo := QrGruSacEmiCodigo.Value;
  end;
  Dmod.QrUpdU.SQL.Add('Nome=:P0, ');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  //
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[02].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[03].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GruSacEmi', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
  if FSeq = 1 then Close;
end;

procedure TFmGruSacEmi.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'GruSacEmi', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GruSacEmi', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'GruSacEmi', 'Codigo');
end;

procedure TFmGruSacEmi.FormCreate(Sender: TObject);
begin
  FSeq := 0;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  DBGrid1.Align     := alClient;
  Panel4.Align      := alClient;
  CriaOForm;
end;

procedure TFmGruSacEmi.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrGruSacEmiCodigo.Value,LaRegistro.Caption);
end;

procedure TFmGruSacEmi.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmGruSacEmi.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrGruSacEmiCodigo.Value;
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmGruSacEmi.QrGruSacEmiAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmGruSacEmi.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'GruSacEmi', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, CO_INCLUSAO, 0)
  else if (not FLocIni) and (FGrupoIni <> -1000)  then
  begin
    LocCod(FGrupoIni, FGrupoIni);
    if QrGruSacEmiCodigo.Value <> FGrupoIni then Application.MessageBox(
    'Grupo n�o localizado!', 'Aviso', MB_OK+MB_ICONWARNING);
    FLocIni := True;
  end;
end;

procedure TFmGruSacEmi.SbQueryClick(Sender: TObject);
begin
  LocCod(QrGruSacEmiCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'GruSacEmi', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmGruSacEmi.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmGruSacEmi.QrGruSacEmiBeforeOpen(DataSet: TDataSet);
begin
  QrGruSacEmiCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmGruSacEmi.BtGrupoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMGrupo, BtGrupo);
end;

procedure TFmGruSacEmi.Altera1Click(Sender: TObject);
var
  GruSacEmi : Integer;
begin
  GruSacEmi := QrGruSacEmiCodigo.Value;
  if not UMyMod.SelLockY(GruSacEmi, Dmod.MyDB, 'GruSacEmi', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(GruSacEmi, Dmod.MyDB, 'GruSacEmi', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmGruSacEmi.Inclui1Click(Sender: TObject);
begin
  MostraEdicao(1, CO_INCLUSAO, 0);
end;

procedure TFmGruSacEmi.BtEmiSacClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEmiSac, BtEmiSac);
end;

procedure TFmGruSacEmi.Adiciona1Click(Sender: TObject);
begin
  MostraEdicao(2, CO_INCLUSAO, 0);
end;

procedure TFmGruSacEmi.BtDesiItsClick(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmGruSacEmi.EdCPF1Exit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCPF1.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if MLAGeral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Application.MessageBox(PChar('N�mero inv�lido'), 'Erro', MB_OK+MB_ICONERROR);
      EdCPF1.SetFocus;
    end else EdCPF1.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCPF1.Text := CO_VAZIO;
  ReopenEmiSac(CPF, True);
end;

procedure TFmGruSacEmi.EdCPF1Change(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCPF1.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if CPF = Num then
    begin
      BtConfIts.Enabled := True;
      ReopenEmiSac(CPF, True);
    end else begin
      BtConfIts.Enabled := False;
      ReopenEmiSac(CPF, False);
    end;
  end;
end;

procedure TFmGruSacEmi.ReopenEmiSac(CPF_CNPJ: String; Reabre: Boolean);
begin
  QrEmiSac.Close;
  QrEmiSac.Params[0].AsString  := Geral.SoNumero_TT(EdCPF1.Text);
  QrEmiSac.Params[1].AsString  := Geral.SoNumero_TT(EdCPF1.Text);
  if Reabre then QrEmiSac.Open;
end;

procedure TFmGruSacEmi.QrGruSacEmiAfterScroll(DataSet: TDataSet);
begin
  ReopenGruSacEmiIts;
end;

procedure TFmGruSacEmi.BtConfItsClick(Sender: TObject);
begin
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    QrLoc.Close;
    QrLoc.Params[0].AsString  := Geral.SoNumero_TT(EdCPF1.Text);
    QrLoc.Params[1].AsInteger := QrGruSacEmiCodigo.Value;
    QrLoc.Open;
    //
    if QrLoc.RecordCount > 0 then
    begin
      Application.MessageBox('Este CNPJ / CPF j� est� cadastrado no grupo selecionado',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
  end;
  if EdNomeEmiSac.Text = '' then
  begin
    Application.MessageBox('Informe um nome!', 'Nome?', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Dmod.QrUpd.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpd.SQL.Add('INSERT INTO grusacemiits SET ');
    Dmod.QrUpd.SQL.Add('Nome=:P0, CNPJ_CPF=:P1, Codigo=:P2');
  end else begin
    Dmod.QrUpd.SQL.Add('UPDATE grusacemiits SET ');
    Dmod.QrUpd.SQL.Add('Nome=:P0 WHERE CNPJ_CPF=:P1 AND Codigo=:P2');
  end;
  Dmod.QrUpd.SQL.Add('');
  Dmod.QrUpd.Params[0].AsString  := EdNomeEmiSac.Text;
  Dmod.QrUpd.Params[1].AsString  := Geral.SoNumero_TT(EdCPF1.Text);
  Dmod.QrUpd.Params[2].AsInteger := QrGruSacEmiCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  //
  ReopenGruSacEmiIts;
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmGruSacEmi.ReopenGruSacEmiIts;
begin
  QrGruSacEmiIts.Close;
  QrGruSacEmiIts.Params[0].AsInteger := QrGruSacEmiCodigo.Value;
  QrGruSacEmiIts.Open;
end;

procedure TFmGruSacEmi.DBEdEmitenteChange(Sender: TObject);
begin
  EdNomeEmiSac.Text := DBEdEmitente.Text;
end;

procedure TFmGruSacEmi.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F8 then
    if PnSacEmi.Visible then
    begin
      Application.CreateForm(TFmPesqCPFCNPJ, FmPesqCPFCNPJ);
      FmPesqCPFCNPJ.ShowModal;
      FmPesqCPFCNPJ.Destroy;
      if VAR_CPF_PESQ <> '' then
      begin
        EdCPF1.Text := VAR_CPF_PESQ;
      end;
      ReopenEmiSac(VAR_CPF_PESQ, True);
    end;
end;

procedure TFmGruSacEmi.Remove1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma do sacado / emitente do grupo atual?',
  'Pergunta', MB_YESNO+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM grusacemiits WHERE CNPJ_CPF=:P0');
    Dmod.QrUpd.SQL.Add('AND Codigo=:P1');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[0].AsString  := QrGruSacEmiItsCNPJ_CPF.Value;
    Dmod.QrUpd.Params[1].AsInteger := QrGruSacEmiItsCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenGruSacEmiIts;
  end;
end;

procedure TFmGruSacEmi.Nomeiagrupo1Click(Sender: TObject);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE grusacemi SET Nome=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
  Dmod.QrUpd.Params[0].AsString  := QrGruSacEmiItsNome.Value;
  Dmod.QrUpd.Params[1].AsInteger := QrGruSacEmiCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  //
  LocCod(QrGruSacEmiCodigo.Value, QrGruSacEmiCodigo.Value);
end;

procedure TFmGruSacEmi.Edita1Click(Sender: TObject);
begin
  MostraEdicao(2, CO_ALTERACAO, 0);
end;

procedure TFmGruSacEmi.QrGruSacEmiItsCalcFields(DataSet: TDataSet);
begin
  QrGruSacEmiItsCNPJ_CPF_TXT.Value :=
    Geral.FormataCNPJ_TT(QrGruSacEmiItsCNPJ_CPF.Value);
end;

end.

