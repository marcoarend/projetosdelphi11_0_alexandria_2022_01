unit DuplicCancProt;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DBCtrls, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, frxClass,
  frxDBSet, UnDmkEnums;

type
  TFmDuplicCancProt = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdCarta: TdmkEditCB;
    CBCarta: TdmkDBLookupComboBox;
    Label1: TLabel;
    QrCartas: TmySQLQuery;
    DsCartas: TDataSource;
    QrCartasCodigo: TIntegerField;
    QrCartasTitulo: TWideStringField;
    frxCartaCancProtesto: TfrxReport;
    frxDsSaCart: TfrxDBDataset;
    QrSaCart: TmySQLQuery;
    QrSaCartCNPJ: TWideStringField;
    QrSaCartIE: TWideStringField;
    QrSaCartNome: TWideStringField;
    QrSaCartRua: TWideStringField;
    QrSaCartCompl: TWideStringField;
    QrSaCartBairro: TWideStringField;
    QrSaCartCidade: TWideStringField;
    QrSaCartUF: TWideStringField;
    QrSaCartCEP: TIntegerField;
    QrSaCartTel1: TWideStringField;
    QrSaCartRisco: TFloatField;
    QrSaCartCodigo: TIntegerField;
    QrSaCartNUMERO_TXT: TWideStringField;
    QrSaCartLNR: TWideStringField;
    QrSaCartLN2: TWideStringField;
    QrSaCartCUC: TWideStringField;
    QrSaCartCEP_TXT: TWideStringField;
    QrSaCartNumero: TFloatField;
    frxDsCarta: TfrxDBDataset;
    QrCartasTexto: TWideMemoField;
    QrDupSac: TmySQLQuery;
    QrDupSacDuplicata: TWideStringField;
    QrDupSacEmissao: TDateField;
    QrDupSacValor: TFloatField;
    QrDupSacVencto: TDateField;
    QrDupSacBanco: TIntegerField;
    QrDupSacAgencia: TIntegerField;
    QrDupSacNOMEBANCO: TWideStringField;
    QrDupSacData3: TDateField;
    EdComarca: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdNumDistrib: TdmkEdit;
    Label4: TLabel;
    EdLivro: TdmkEdit;
    Label5: TLabel;
    EdFolhas: TdmkEdit;
    QrLotesIProt: TmySQLQuery;
    QrLotesIProtComarca: TWideStringField;
    QrLotesIProtNumDistrib: TWideStringField;
    QrLotesIProtLivro: TWideStringField;
    QrLotesIProtFolhas: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrCartasAfterOpen(DataSet: TDataSet);
    procedure frxCartaSacadoDadosGetValue(const VarName: string;
      var Value: Variant);
    procedure frxCartaCancProtestoGetValue(const VarName: string;
      var Value: Variant);
    procedure BtOKClick(Sender: TObject);
    procedure QrSaCartCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReopenLotesIProt(Controle: Integer);
  end;

  var
  FmDuplicCancProt: TFmDuplicCancProt;

implementation

uses ModuleGeral, Module, UnInternalConsts, Duplicatas2, Principal,
UMySQLModule, UnMyObjects;

{$R *.DFM}

procedure TFmDuplicCancProt.BtOKClick(Sender: TObject);
var
  Comarca, NumDistrib, Livro, Folhas: String;
  Controle: Integer;
begin
  Comarca    := EdComarca.Text;   
  NumDistrib := EdNumDistrib.Text; 
  Livro      := EdLivro.Text; 
  Folhas     := EdFolhas.Text; 
  Controle   := FmDuplicatas2.QrPesqControle.Value;
  //
  if (Trim(Comarca) <> '') or (Trim(NumDistrib) <> '')
  or (Trim(Livro) <> '') or (Trim(Folhas) <> '') then
  begin
    if UMyMod.SQLReplace(Dmod.QrUpd, 'lotesiprot', [
      'Comarca', 'NumDistrib', 'Livro', 'Folhas'], ['Controle'], [
       Comarca, NumDistrib, Livro, Folhas], [Controle], True) then
    begin
    end;
  end;
  //
  ReopenLotesIProt(Controle);
  //
  QrSaCart.Close;
  QrSaCart.Params[0].AsInteger := Controle;
  QrSaCart.Open;
  //
  QrDupSac.Close;
  QrDupSac.Params[0].AsInteger := Controle;
  QrDupSac.Open;
  //
  MyObjects.frxMostra(frxCartaCancProtesto, 'Carta de cancelamento de protesto');
end;

procedure TFmDuplicCancProt.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDuplicCancProt.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmDuplicCancProt.FormCreate(Sender: TObject);
begin
  QrCartas.Close;
  QrCartas.Open;
end;

procedure TFmDuplicCancProt.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmDuplicCancProt.frxCartaSacadoDadosGetValue(const VarName: string;
  var Value: Variant);
var
  Altura: Integer;
begin
  // EXCLUIR TUDO n�o serve aqui! Parei Aqui!
  if AnsiCompareText(VarName, 'VARF_CSD_AltuHeader') = 0 then
    Value := Int(Dmod.QrControleCSD_TopoCidata.Value / VAR_frCM)
  else if AnsiCompareText(VarName, 'VARF_CSD_AltuCidata') = 0 then
  begin
    Altura := Dmod.QrControleCSD_TopoDestin.Value - Dmod.QrControleCSD_TopoCidata.Value;
    Value := Int(Altura / VAR_frCM)
  end
  else if AnsiCompareText(VarName, 'VARF_CSD_AltuDestin') = 0 then
  begin
    Altura := Dmod.QrControleCSD_TopoDuplic.Value - Dmod.QrControleCSD_TopoDestin.Value;
    Value := Int(Altura / VAR_frCM)
  end else if AnsiCompareText(VarName, 'VARF_CSD_AltuDuplic') = 0 then
  begin
    Altura := Dmod.QrControleCSD_TopoClient.Value - Dmod.QrControleCSD_TopoDuplic.Value;
    Value := Int(Altura / VAR_frCM)
  end else if AnsiCompareText(VarName, 'VARF_CSD_AltuFooter') = 0 then
  begin
    Altura := 29700 - 500 - 4500 - Dmod.QrControleCSD_TopoClient.Value;
    Value := Int(Altura / VAR_frCM)
  //
  end else if AnsiCompareText(VarName, 'VARF_CSD_AltuHeade2') = 0 then
    Value := Int(Dmod.QrControleCSD_TopoDesti2.Value / VAR_frCM)
  else if AnsiCompareText(VarName, 'VARF_CSD_AltuDesti2') = 0 then
  begin
    Altura := 26000 (*- 4500*) - Dmod.QrControleCSD_TopoDesti2.Value;
    Value := Int(Altura / VAR_frCM)
  //
  end else if AnsiCompareText(VarName, 'VARF_CSD_MEsqCidata') = 0 then
    Value := Dmod.QrControleCSD_MEsqCidata.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_CSD_MEsqDestin') = 0 then
    Value := Dmod.QrControleCSD_MEsqDestin.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_CSD_MEsqDuplic') = 0 then
    Value := Dmod.QrControleCSD_MEsqDuplic.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_CSD_MEsqClient') = 0 then
    Value := Dmod.QrControleCSD_MEsqClient.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_CSD_MEsqDesti2') = 0 then
    Value := Dmod.QrControleCSD_MEsqDesti2.Value / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_CIDADE_E_DATA') = 0 then
    Value := Dmod.QrDonoCIDADE.Value + ', '+
    FormatDateTime('dd" de "mmmm" de "yyyy"."', Date)
  else if AnsiCompareText(VarName, 'DUPLISACDADOS') = 0  then
  begin
    {
    QrDupSac.First;
    Value := '';
    //'N� Duplicata  Emitida em   Valor de R$   Vencimento Banco e ag�ncia para pagamento'+Chr(10)+Chr(13)+
    //'------------------------------------------------------------------------------------------------'+Chr(10)+Chr(13);
    while not QrDupSac.Eof do
    begin
      Value := Value +
      Geral.CompletaString(QrDupSacDuplicata.Value,' ', 16, taLeftJustify, True)+
      Geral.CompletaString(FormatDateTime(VAR_FORMATDATE2, QrDupSacEmissao.Value), ' ', 10, taLeftJustify, True)+
      Geral.CompletaString(Geral.FFT(QrDupSacValor.Value, 2, siPositivo), ' ', 14, taRightJustify, True)+ '     '+
      Geral.CompletaString(FormatDateTime(VAR_FORMATDATE2, QrDupSacVencto.Value), ' ', 10, taLeftJustify, True)+ ' '+
      '    ' +
      Geral.CompletaString(MLAGeral.FFD(QrDupSacBanco.Value, 3, siPositivo), ' ', 3, taRightJustify, True)+'/'+
      Geral.CompletaString(MLAGeral.FFD(QrDupSacAgencia.Value, 4, siPositivo), ' ', 4, taRightJustify, True)+ ' ' +
      Geral.CompletaString(QrDupSacNOMEBANCO.Value, ' ', 35, taLeftJustify, True)
      +Chr(10)+Chr(13);
      //
      QrDupSac.Next;
    end;
    }
  end
  {
  else if AnsiCompareText(VarName, 'NOME_CLI') = 0 then Value := QrLotesNOMECLIENTE.Value
  else if AnsiCompareText(VarName, 'VARF_NOTEOF') = 0 then
  begin
    if QrSaCart.RecNo < QrSaCart.RecordCount then Value := True else Value := False;
  end
  }
  else ;
end;

procedure TFmDuplicCancProt.frxCartaCancProtestoGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_HOJE' then Value := FormatDateTime(VAR_FORMATDATE2, Date)
  //else if VarName = 'VARF_CHQS' then Value := QrLotesIts.RecordCount
  else if VarName = 'VARF_MOEDA' then Value := Dmod.QrControleMoeda.Value
  else if VarName = 'Coligada0' then Value := Dmod.QrMasterEm.Value
  else if VarName = 'Coligada1' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 1 then
    begin
      if FmPrincipal.FMyDBs.Connected[0] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[0]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'Coligada2' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 2 then
    begin
      if FmPrincipal.FMyDBs.Connected[1] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[1]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'Coligada3' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 3then
    begin
      if FmPrincipal.FMyDBs.Connected[2] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[2]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'Coligada4' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 4 then
    begin
      if FmPrincipal.FMyDBs.Connected[3] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[3]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'Coligada5' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 5 then
    begin
      if FmPrincipal.FMyDBs.Connected[4] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[4]
      else Value := '---';
    end else Value := '---';
  end
  else if VarName = 'Coligada6' then
  begin
    if FmPrincipal.FMyDBs.MaxDBs >= 6 then
    begin
      if FmPrincipal.FMyDBs.Connected[5] =   '1' then
        Value := FmPrincipal.FMyDBs.IDName[5]
      else Value := '---';
    end else Value := '---';
  end
  (*
  else if VarName = 'TaxaJuros0' then Value := QrLotesItsTxaCompra.Value
  else if VarName = 'TaxaJuros1' then Value := FDadosJurosColigadas[1][0]
  else if VarName = 'TaxaJuros2' then Value := FDadosJurosColigadas[2][0]
  else if VarName = 'TaxaJuros3' then Value := FDadosJurosColigadas[3][0]
  else if VarName = 'TaxaJuros4' then Value := FDadosJurosColigadas[4][0]
  else if VarName = 'TaxaJuros5' then Value := FDadosJurosColigadas[5][0]
  else if VarName = 'TaxaJuros6' then Value := FDadosJurosColigadas[6][0]
  else if VarName = 'TaxaJuros7' then Value := FDadosJurosColigadas[0][0]

  else if VarName = 'JurosPeriodo0' then Value := QrLotesItsTxaJuros.Value
  else if VarName = 'JurosPeriodo1' then Value := FDadosJurosColigadas[1][1]
  else if VarName = 'JurosPeriodo2' then Value := FDadosJurosColigadas[2][1]
  else if VarName = 'JurosPeriodo3' then Value := FDadosJurosColigadas[3][1]
  else if VarName = 'JurosPeriodo4' then Value := FDadosJurosColigadas[4][1]
  else if VarName = 'JurosPeriodo5' then Value := FDadosJurosColigadas[5][1]
  else if VarName = 'JurosPeriodo6' then Value := FDadosJurosColigadas[6][1]
  else if VarName = 'JurosPeriodo7' then Value := FDadosJurosColigadas[0][1]

  else if VarName = 'ValorJuros0' then Value := QrLotesItsVlrCompra.Value
  else if VarName = 'ValorJuros1' then Value := FDadosJurosColigadas[1][2]
  else if VarName = 'ValorJuros2' then Value := FDadosJurosColigadas[2][2]
  else if VarName = 'ValorJuros3' then Value := FDadosJurosColigadas[3][2]
  else if VarName = 'ValorJuros4' then Value := FDadosJurosColigadas[4][2]
  else if VarName = 'ValorJuros5' then Value := FDadosJurosColigadas[5][2]
  else if VarName = 'ValorJuros6' then Value := FDadosJurosColigadas[6][2]
  else if VarName = 'ValorJuros7' then Value := FDadosJurosColigadas[0][2]
  else if VarName = 'VARF_TOTAL_EXTENSO' then Value :=
    dmkPF.ExtensoMoney(Geral.FFT(QrLotesTotal.Value, 2, siPositivo))
  *)
  else
  if VarName = 'VARF_CIDADE_E_DATA' then
    Value := Dmod.QrDonoCIDADE.Value
    + ', '+FormatDateTime('dd" de "mmmm" de "yyyy"."', Date)
(*
  else if VarName = 'VARF_ENDERECO_CLIENTE' then Value :=
    GOTOy.EnderecoDeEntidade(QrLotesCliente.Value, 0)
  else if VarName = 'NUM_CONT' then
    Value := QrContratContrato.Value
  else if VarName = 'DATA_CON' then Value :=
    FormatDateTime(VAR_FORMATDATE2, QrContratDataC.Value)
  else if VarName = 'LIMITE  ' then Value := QrContratLimite.Value
  else if VarName = 'ADITIVO ' then Value := QrLotesLote.Value
*)
  else if VarName = 'PLU1_TIT' then
  begin
    if QrDupSac.RecordCount < 2 then Value := '' else Value := 's'
  end
  else if VarName = 'PLU2_TIT' then
  begin
    if QrDupSac.RecordCount < 2 then Value := 'i' else Value := 'ram'
  end
  else if VarName = 'PLU3_TIT' then
  begin
    if QrDupSac.RecordCount < 2 then Value := '�' else Value := '�o'
  end
  else if VarName = 'PLU4_TIT' then
  begin
    if QrDupSac.RecordCount < 2 then Value := '' else Value := 'em'
  end
  else if VarName = 'DUPLISAC' then
  begin
    QrDupSac.First;
    Value :=
  //'N� Duplicata  Emitida em   Valor de R$   Vencimento Banco e ag�ncia para pagamento'+Chr(10)+Chr(13)+
    'N� Duplicata   Vencimento  Valor de R$  N� Distribui��o       Livro          Folhas             '+Chr(10)+Chr(13)+
    '------------------------------------------------------------------------------------------------'+Chr(10)+Chr(13);
    while not QrDupSac.Eof do
    begin
      Value := Value +
      Geral.CompletaString(QrDupSacDuplicata.Value,' ', 14, taLeftJustify, True) + ' ' +
      //Geral.CompletaString(FormatDateTime(VAR_FORMATDATE2, QrDupSacEmissao.Value), ' ', 11, taLeftJustify, True)+
      Geral.CompletaString(FormatDateTime(VAR_FORMATDATE2, QrDupSacVencto.Value), ' ', 10, taLeftJustify, True)+ ' '+
      Geral.CompletaString(Geral.FFT(QrDupSacValor.Value, 2, siPositivo), ' ', 12, taRightJustify, True)+ '  '+
      //Mudar p/ query
      Geral.CompletaString(QrLotesIProtNumDistrib.Value, ' ', 21, taLeftJustify, True) + ' ' +
      Geral.CompletaString(QrLotesIProtLivro.Value, ' ', 14, taLeftJustify, True) + ' ' +
      Geral.CompletaString(QrLotesIProtFolhas.Value, ' ', 19, taLeftJustify, True)+
      {
      Geral.CompletaString(EdNumDistrib.Text, ' ', 19, taLeftJustify, True) + ' ' +
      Geral.CompletaString(EdLivro.Text, ' ', 13, taRightJustify, True) + ' ' +
      Geral.CompletaString(EdFolhas.Text, ' ', 21, taLeftJustify, True)+
      }
      Chr(10)+Chr(13);
      //
      QrDupSac.Next;
    end;
  end
  //else if VarName = 'NOME_CLI' then Value := QrLotesNOMECLIENTE.Value
  else if AnsiCompareText(VarName, 'VARF_NOTEOF') = 0 then
  begin
    if QrSaCart.RecNo < QrSaCart.RecordCount then Value := True else Value := False;
  end
  else if AnsiCompareText(VarName, 'VARF_CIDADE_E_DATA') = 0 then
    Value := Dmod.QrDonoCIDADE.Value + ', ' +
    FormatDateTime('dd" de "mmmm" de "yyyy"."', QrDupSacData3.Value)

  // Deste form

  else if VarName = 'NOME_SAC' then Value := QrSaCartNome.Value
  else if VarName = 'TIPD_SAC' then Value := Geral.TipoDocCNPJCPF(QrSaCartCNPJ.Value)
  else if VarName = 'DOCU_SAC' then Value := Geral.FormataCNPJ_TT(QrSaCartCNPJ.Value)
  else if VarName = 'ENDE_SAC' then Value := QrSaCartCUC.Value
  else if VarName = 'COMARCAD' then Value := QrLotesIProtComarca.Value

  //

  else FmPrincipal.TextosCartasGetValue(VarName, Value)
end;

procedure TFmDuplicCancProt.QrCartasAfterOpen(DataSet: TDataSet);
begin
  if QrCartas.RecordCount = 1 then
  begin
    EdCarta.ValueVariant := QrCartasCodigo.Value;
    CBCarta.KeyValue     := QrCartasCodigo.Value;
  end;
end;

procedure TFmDuplicCancProt.QrSaCartCalcFields(DataSet: TDataSet);
begin
  QrSaCartNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrSaCartRua.Value, Trunc(QrSaCartNumero.Value), False);
  QrSaCartLNR.Value := '';//QrSaCartNOMELOGRAD.Value;
  if Trim(QrSaCartLNR.Value) <> '' then QrSaCartLNR.Value :=
    QrSaCartLNR.Value + ' ';
  QrSaCartLNR.Value := QrSaCartLNR.Value + QrSaCartRua.Value;
  if Trim(QrSaCartRua.Value) <> '' then QrSaCartLNR.Value :=
    QrSaCartLNR.Value + ', ' + QrSaCartNUMERO_TXT.Value;
  if Trim(QrSaCartCompl.Value) <>  '' then QrSaCartLNR.Value :=
    QrSaCartLNR.Value + ' ' + QrSaCartCompl.Value;
  if Trim(QrSaCartBairro.Value) <>  '' then QrSaCartLNR.Value :=
    QrSaCartLNR.Value + ' - ' + QrSaCartBairro.Value;
  //
  QrSaCartLN2.Value := '';
  if Trim(QrSaCartCidade.Value) <>  '' then QrSaCartLN2.Value :=
    QrSaCartLN2.Value + QrSaCartCIDADE.Value;
  QrSaCartLN2.Value := QrSaCartLN2.Value + ' - '+QrSaCartUF.Value;
  if QrSaCartCEP.Value > 0 then QrSaCartLN2.Value :=
    QrSaCartLN2.Value + ' - CEP ' + Geral.FormataCEP_NT(QrSaCartCEP.Value);
  //
  QrSaCartCUC.Value := QrSaCartLNR.Value+ ' - ' +QrSaCartLN2.Value;
  //
  QrSaCartCEP_TXT.Value := Geral.FormataCEP_NT(QrSaCartCEP.Value);
  //
end;

procedure TFmDuplicCancProt.ReopenLotesIProt(Controle: Integer);
begin
  QrLotesIProt.Close;
  QrLotesIProt.Params[0].AsInteger := Controle;
  QrLotesIProt.Open;
end;

end.
