object FmPesqDep: TFmPesqDep
  Left = 274
  Top = 150
  Caption = 'Gerencia recebimentos'
  ClientHeight = 184
  ClientWidth = 466
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 136
    Width = 466
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 354
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 14
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtPesq: TBitBtn
      Tag = 22
      Left = 6
      Top = 5
      Width = 90
      Height = 40
      Caption = '&Pesquisar'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtPesqClick
    end
    object BtImprimir: TBitBtn
      Tag = 5
      Left = 98
      Top = 5
      Width = 90
      Height = 40
      Caption = '&Imprimir'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtImprimirClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 466
    Height = 48
    Align = alTop
    Caption = 'Dep'#243'sito'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 464
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 258
      ExplicitTop = 113
      ExplicitWidth = 462
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 466
    Height = 88
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 464
      Height = 76
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Valor: TLabel
        Left = 5
        Top = 20
        Width = 24
        Height = 13
        Caption = 'Valor'
      end
    end
    object CkPeriodo: TCheckBox
      Left = 92
      Top = 16
      Width = 177
      Height = 17
      Caption = 'Per'#237'odo depo'#243'sito (in'#237'cio - fim):'
      TabOrder = 1
      OnClick = CkPeriodoClick
    end
    object TPIniDep: TDateTimePicker
      Left = 92
      Top = 38
      Width = 90
      Height = 21
      CalColors.TextColor = clMenuText
      Date = 37636.777157974500000000
      Time = 37636.777157974500000000
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
    end
    object TPFimDep: TDateTimePicker
      Left = 188
      Top = 38
      Width = 90
      Height = 21
      Date = 37636.777203761600000000
      Time = 37636.777203761600000000
      TabOrder = 3
    end
    object dmkEdValor: TdmkEdit
      Left = 6
      Top = 38
      Width = 80
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      QryCampo = 'Credito'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
      OnChange = dmkEdValorChange
    end
  end
  object QrDepIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrDepItsAfterOpen
    BeforeClose = QrDepItsBeforeClose
    OnCalcFields = QrDepItsCalcFields
    SQL.Strings = (
      'SELECT li.Codigo, lo.Cliente, li.Banco, li.Agencia, li.Conta, '
      'li.Cheque, lo.NF, li.Emitente, li.CPF, li.Vencto, '
      'li.DDeposito, li.Valor, li.ObsGerais'
      'FROM lotesits li'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'WHERE li.Depositado = 1')
    Left = 361
    Top = 73
    object QrDepItsCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'lotes.Cliente'
    end
    object QrDepItsBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lotesits.Banco'
      Required = True
    end
    object QrDepItsAgencia: TIntegerField
      FieldName = 'Agencia'
      Origin = 'lotesits.Agencia'
      Required = True
    end
    object QrDepItsConta: TWideStringField
      FieldName = 'Conta'
      Origin = 'lotesits.Conta'
    end
    object QrDepItsCheque: TIntegerField
      FieldName = 'Cheque'
      Origin = 'lotesits.Cheque'
      Required = True
    end
    object QrDepItsNF: TIntegerField
      FieldName = 'NF'
      Origin = 'lotes.NF'
    end
    object QrDepItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lotesits.Emitente'
      Size = 50
    end
    object QrDepItsCPF: TWideStringField
      FieldName = 'CPF'
      Origin = 'lotesits.CPF'
      Size = 15
    end
    object QrDepItsVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'lotesits.Vencto'
      Required = True
    end
    object QrDepItsDDeposito: TDateField
      FieldName = 'DDeposito'
      Origin = 'lotesits.DDeposito'
      Required = True
    end
    object QrDepItsValor: TFloatField
      FieldName = 'Valor'
      Origin = 'lotesits.Valor'
      Required = True
    end
    object QrDepItsObsGerais: TWideStringField
      FieldName = 'ObsGerais'
      Origin = 'lotesits.ObsGerais'
      Size = 60
    end
    object QrDepItsCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Calculated = True
    end
    object QrDepItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'lotesits.Codigo'
      Required = True
    end
  end
  object DsDepIts: TDataSource
    DataSet = QrDepIts
    Left = 389
    Top = 73
  end
  object frxDepIts: TfrxReport
    Version = '4.14'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39141.750700231500000000
    ReportOptions.LastChange = 39766.436549479200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxDepItsGetValue
    Left = 305
    Top = 73
    Datasets = <
      item
        DataSet = frxDsDepIts
        DataSetName = 'frxDsDepIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      object ReportTitle1: TfrxReportTitle
        Height = 22.299212350000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo43: TfrxMemoView
          Left = 10.204731000000000000
          Top = 1.133859000000001000
          Width = 755.906000000000000000
          Height = 18.897635350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Dep'#243'sito de cheques')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object MasterData1: TfrxMasterData
        Height = 34.007936260000010000
        Top = 179.905628000000000000
        Width = 718.110700000000000000
        DataSet = frxDsDepIts
        DataSetName = 'frxDsDepIts'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 48.755902829999990000
          Width = 22.677165350000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsDepIts."Banco">)]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 71.433075500000000000
          Width = 30.236220470000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Agencia'
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDepIts."Agencia"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 101.669295980000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDepIts."Conta"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 162.141736920000000000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsDepIts."Cheque">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 241.511815660000000000
          Width = 207.873991340000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Emitente'
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsDepIts."Emitente"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 449.385807000000000000
          Width = 102.047244090000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'CPF_TXT'
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsDepIts."CPF_TXT"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 645.921240070000000000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Valor'
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsDepIts."Valor"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 551.433051090000000000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'Vencto'
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDepIts."Vencto"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 600.566909360000000000
          Width = 45.354330710000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataField = 'DDeposito'
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDepIts."DDeposito"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 199.937012510000000000
          Width = 41.574803150000000000
          Height = 17.000000000000000000
          ShowHint = False
          DataSet = frxDsDepIts
          DataSetName = 'frxDsDepIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsDepIts."NF">)]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 10.960637000000000000
          Top = 0.000051259999992226
          Width = 37.795273150000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDepIts."Cliente"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 10.960637000000000000
          Top = 17.007885000000020000
          Width = 60.472453150000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Observa'#231#245'es')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 71.433117000000000000
          Top = 17.007885000000020000
          Width = 634.961013150000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsDepIts."ObsGerais"]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        Height = 60.850433000000000000
        Top = 61.228386000000000000
        Width = 718.110700000000000000
        object Memo15: TfrxMemoView
          Left = 48.755902830000000000
          Top = 43.842547999999990000
          Width = 22.677165350000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bco.')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 71.433075500000000000
          Top = 43.842547999999990000
          Width = 30.236220470000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag.')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 101.669295980000000000
          Top = 43.842547999999990000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 162.141736920000000000
          Top = 43.842547999999990000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cheq.')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 241.511815660000000000
          Top = 43.842547999999990000
          Width = 207.873991340000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emitente')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 449.385807000000000000
          Top = 43.842547999999990000
          Width = 102.047244090000000000
          Height = 17.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 645.921240070000000000
          Top = 43.842547999999990000
          Width = 60.472440940000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 551.433051090000000000
          Top = 43.842547999999990000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 600.566909360000000000
          Top = 43.842547999999990000
          Width = 45.354330710000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dep'#243'sito')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 199.937012510000000000
          Top = 43.842547999999990000
          Width = 41.574803150000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 10.960637000000000000
          Top = 43.842599260000000000
          Width = 37.795273150000000000
          Height = 17.000000000000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 11.338590000000000000
          Top = 16.748052000000000000
          Width = 755.906000000000000000
          Height = 24.566930350000000000
          ShowHint = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Per'#237'odo de: [VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        Height = 36.000000000000000000
        Top = 273.637972000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 549.921615000000000000
          Width = 158.299258000000000000
          Height = 18.000000000000000000
          ShowHint = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina[Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsDepIts: TfrxDBDataset
    UserName = 'frxDsDepIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Cliente=Cliente'
      'Banco=Banco'
      'Agencia=Agencia'
      'Conta=Conta'
      'Cheque=Cheque'
      'NF=NF'
      'Emitente=Emitente'
      'CPF=CPF'
      'Vencto=Vencto'
      'DDeposito=DDeposito'
      'Valor=Valor'
      'ObsGerais=ObsGerais'
      'CPF_TXT=CPF_TXT'
      'Codigo=Codigo')
    DataSource = DsDepIts
    BCDToCurrency = False
    Left = 333
    Top = 73
  end
end
