object FmMortoLotes: TFmMortoLotes
  Left = 419
  Top = 217
  Caption = 'Arquivamento de Border'#244's'
  ClientHeight = 306
  ClientWidth = 464
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 258
    Width = 464
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtOK: TBitBtn
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      NumGlyphs = 2
    end
    object BtCancela: TBitBtn
      Tag = 15
      Left = 198
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Cancela'
      Enabled = False
      TabOrder = 1
      OnClick = BtCancelaClick
      NumGlyphs = 2
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 362
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtSaidaClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 464
    Height = 48
    Align = alTop
    Caption = 'Arquivamento de Border'#244's'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 462
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 468
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 464
    Height = 210
    Align = alClient
    TabOrder = 2
    object Label137: TLabel
      Left = 8
      Top = 8
      Width = 95
      Height = 13
      Caption = 'Data limite dep'#243'sito:'
    end
    object TPLimite: TDateTimePicker
      Left = 8
      Top = 24
      Width = 97
      Height = 21
      Date = 36526.863521944400000000
      Time = 36526.863521944400000000
      TabOrder = 0
      Visible = False
    end
  end
  object QrLotes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM lotes'
      'WHERE Codigo > 0'
      'AND MaxVencto < :P0'
      'ORDER BY Codigo')
    Left = 160
    Top = 84
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLotesTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLotesSpread: TSmallintField
      FieldName = 'Spread'
    end
    object QrLotesCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLotesLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrLotesData: TDateField
      FieldName = 'Data'
    end
    object QrLotesTotal: TFloatField
      FieldName = 'Total'
    end
    object QrLotesDias: TFloatField
      FieldName = 'Dias'
    end
    object QrLotesPeCompra: TFloatField
      FieldName = 'PeCompra'
    end
    object QrLotesTxCompra: TFloatField
      FieldName = 'TxCompra'
    end
    object QrLotesValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrLotesAdValorem: TFloatField
      FieldName = 'AdValorem'
    end
    object QrLotesIOC: TFloatField
      FieldName = 'IOC'
    end
    object QrLotesIOC_VAL: TFloatField
      FieldName = 'IOC_VAL'
    end
    object QrLotesTarifas: TFloatField
      FieldName = 'Tarifas'
    end
    object QrLotesCPMF: TFloatField
      FieldName = 'CPMF'
    end
    object QrLotesCPMF_VAL: TFloatField
      FieldName = 'CPMF_VAL'
    end
    object QrLotesTipoAdV: TIntegerField
      FieldName = 'TipoAdV'
    end
    object QrLotesIRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrLotesIRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrLotesISS: TFloatField
      FieldName = 'ISS'
    end
    object QrLotesISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrLotesPIS: TFloatField
      FieldName = 'PIS'
    end
    object QrLotesPIS_Val: TFloatField
      FieldName = 'PIS_Val'
    end
    object QrLotesPIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrLotesPIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrLotesCOFINS: TFloatField
      FieldName = 'COFINS'
    end
    object QrLotesCOFINS_Val: TFloatField
      FieldName = 'COFINS_Val'
    end
    object QrLotesCOFINS_R: TFloatField
      FieldName = 'COFINS_R'
    end
    object QrLotesCOFINS_R_Val: TFloatField
      FieldName = 'COFINS_R_Val'
    end
    object QrLotesOcorP: TFloatField
      FieldName = 'OcorP'
    end
    object QrLotesMaxVencto: TDateField
      FieldName = 'MaxVencto'
    end
    object QrLotesCHDevPg: TFloatField
      FieldName = 'CHDevPg'
    end
    object QrLotesLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLotesDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLotesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLotesUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLotesUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLotesMINTC: TFloatField
      FieldName = 'MINTC'
    end
    object QrLotesMINAV: TFloatField
      FieldName = 'MINAV'
    end
    object QrLotesMINTC_AM: TFloatField
      FieldName = 'MINTC_AM'
    end
    object QrLotesPIS_T_Val: TFloatField
      FieldName = 'PIS_T_Val'
    end
    object QrLotesCOFINS_T_Val: TFloatField
      FieldName = 'COFINS_T_Val'
    end
    object QrLotesPgLiq: TFloatField
      FieldName = 'PgLiq'
    end
    object QrLotesDUDevPg: TFloatField
      FieldName = 'DUDevPg'
    end
    object QrLotesNF: TIntegerField
      FieldName = 'NF'
    end
    object QrLotesItens: TIntegerField
      FieldName = 'Itens'
    end
    object QrLotesConferido: TIntegerField
      FieldName = 'Conferido'
    end
    object QrLotesECartaSac: TSmallintField
      FieldName = 'ECartaSac'
    end
    object QrLotesCBE: TIntegerField
      FieldName = 'CBE'
    end
    object QrLotesSCB: TIntegerField
      FieldName = 'SCB'
    end
  end
  object QrLotesIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT loi.* '
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'WHERE lot.Codigo > 0'
      'AND lot.MaxVencto < :P0'
      'ORDER BY loi.Codigo, loi.Controle')
    Left = 188
    Top = 84
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
end
