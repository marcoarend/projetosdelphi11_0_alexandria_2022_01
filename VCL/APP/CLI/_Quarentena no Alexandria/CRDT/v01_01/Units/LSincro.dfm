object FmLSincro: TFmLSincro
  Left = 339
  Top = 185
  Caption = 'Adi'#231#227'o de Dados Externos'
  ClientHeight = 378
  ClientWidth = 577
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 330
    Width = 577
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Importa'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 465
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 577
    Height = 48
    Align = alTop
    Caption = 'Adi'#231#227'o de Dados Externos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 575
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 581
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 577
    Height = 282
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 248
      Top = 8
      Width = 115
      Height = 13
      Caption = 'Tabela fonte dos dados:'
    end
    object Label2: TLabel
      Left = 284
      Top = 164
      Width = 34
      Height = 13
      Caption = 'Incluiu:'
    end
    object Label3: TLabel
      Left = 344
      Top = 164
      Width = 45
      Height = 13
      Caption = 'N'#227'o incl.:'
    end
    object GroupBox1: TGroupBox
      Left = 1
      Top = 1
      Width = 224
      Height = 188
      Caption = ' Banco de dados externo (Fonte dos dados): '
      TabOrder = 0
      object Label32: TLabel
        Left = 8
        Top = 16
        Width = 42
        Height = 13
        Caption = 'Servidor:'
      end
      object Label33: TLabel
        Left = 8
        Top = 56
        Width = 39
        Height = 13
        Caption = 'Usu'#225'rio:'
      end
      object Label34: TLabel
        Left = 8
        Top = 96
        Width = 34
        Height = 13
        Caption = 'Senha:'
      end
      object Label35: TLabel
        Left = 8
        Top = 136
        Width = 81
        Height = 13
        Caption = 'Banco de dados:'
      end
      object EdWeb_Host: TdmkEdit
        Left = 8
        Top = 32
        Width = 201
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdWeb_User: TdmkEdit
        Left = 8
        Top = 72
        Width = 201
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdWeb_Pwd: TdmkEdit
        Left = 8
        Top = 112
        Width = 201
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdWeb_DB: TdmkEdit
        Left = 8
        Top = 152
        Width = 201
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
    object RGTabela: TRadioGroup
      Left = 248
      Top = 52
      Width = 313
      Height = 105
      Caption = ' Tabela a atualizar: '
      Items.Strings = (
        'CPFs (Emitentes)'
        'BACs (Emitentes)')
      TabOrder = 1
    end
    object EdTabSource: TEdit
      Left = 248
      Top = 24
      Width = 121
      Height = 21
      TabOrder = 2
    end
    object EdA: TdmkEdit
      Left = 284
      Top = 180
      Width = 57
      Height = 21
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdB: TdmkEdit
      Left = 344
      Top = 180
      Width = 57
      Height = 21
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
  end
  object QrF: TmySQLQuery
    Database = Dmod.MyDB
    Left = 304
    Top = 257
  end
  object QrS: TmySQLQuery
    Database = DBx
    Left = 276
    Top = 257
  end
  object DBx: TmySQLDatabase
    ConnectOptions = [coCompress]
    Params.Strings = (
      'Port=3306'
      'TIMEOUT=30')
    Left = 248
    Top = 256
  end
end
