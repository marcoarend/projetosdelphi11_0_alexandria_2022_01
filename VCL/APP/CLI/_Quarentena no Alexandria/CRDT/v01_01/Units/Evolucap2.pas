unit Evolucap2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, ComCtrls, Grids,
  DBGrids, DBCtrls, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  TFmEvolucap2 = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    QrCarteiTudo: TmySQLQuery;
    QrCarteiTudoValor: TFloatField;
    QrEvolucap2: TmySQLQuery;
    DsEvolucap2: TDataSource;
    QrColigado: TmySQLQuery;
    QrColigadoCodigo: TIntegerField;
    QrColigadoNOMECOLIGADO: TWideStringField;
    DsColigado: TDataSource;
    QrRepassOutr: TmySQLQuery;
    QrRepassEspe: TmySQLQuery;
    QrRepassOutrValor: TFloatField;
    QrRepassEspeValor: TFloatField;
    QrCHDevolAbe: TmySQLQuery;
    QrCHDevolAbeValor: TFloatField;
    QrDUDevolAbe: TmySQLQuery;
    QrDUDevolAbeValor: TFloatField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    PnAtualiza: TPanel;
    PnEditSdoCC: TPanel;
    Label3: TLabel;
    BitBtn1: TBitBtn;
    EdSaldoConta: TdmkEdit;
    BitBtn2: TBitBtn;
    GroupBox2: TGroupBox;
    Label4: TLabel;
    Label5: TLabel;
    TPIniB: TDateTimePicker;
    TPFimB: TDateTimePicker;
    QrEvolper: TmySQLQuery;
    QrEvolperDataE: TDateField;
    QrEvolperCarteiTudo: TFloatField;
    QrEvolperRepassEspe: TFloatField;
    QrEvolperRepassOutr: TFloatField;
    QrEvolperCARTEIREAL: TFloatField;
    QrEvolperCHDevolAbe: TFloatField;
    QrEvolperDUDevolAbe: TFloatField;
    QrEvolperVALOR_SUB_T: TFloatField;
    QrEvolperSaldoConta: TFloatField;
    QrEvolperVALOR_TOTAL: TFloatField;
    QrMax: TmySQLQuery;
    QrMaxTOTAL: TFloatField;
    BtGrafico: TBitBtn;
    RGIntervalo: TRadioGroup;
    PnDados: TPanel;
    GroupBox1: TGroupBox;
    Label34: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    TPIniA: TDateTimePicker;
    TPFimA: TDateTimePicker;
    EdColigado: TdmkEditCB;
    CBColigado: TdmkDBLookupComboBox;
    BtOK: TBitBtn;
    ProgressBar1: TProgressBar;
    EdSaldoCaixa: TdmkEdit;
    Label6: TLabel;
    QrEvolperSaldoCaixa: TFloatField;
    TabSheet3: TTabSheet;
    DBGrid2: TDBGrid;
    QrCHNull: TmySQLQuery;
    DsCHNull: TDataSource;
    QrCHNullCliente: TIntegerField;
    QrCHNullEmitente: TWideStringField;
    QrCHNullCPF: TWideStringField;
    QrCHNullBanco: TIntegerField;
    QrCHNullAgencia: TIntegerField;
    QrCHNullConta: TWideStringField;
    QrCHNullCheque: TIntegerField;
    QrCHNullValor: TFloatField;
    QrCHNullTaxas: TFloatField;
    QrCHNullMulta: TFloatField;
    QrCHNullJuros: TFloatField;
    QrCHNullDesconto: TFloatField;
    QrCHNullValPago: TFloatField;
    QrCHNullChequeOrigem: TIntegerField;
    QrCHNullLoteOrigem: TIntegerField;
    Panel2: TPanel;
    BtExclui1: TBitBtn;
    QrCHNullCodigo: TIntegerField;
    TabSheet4: TTabSheet;
    DBGrid3: TDBGrid;
    QrCHPrecoce: TmySQLQuery;
    DsCHPrecoce: TDataSource;
    Panel3: TPanel;
    QrCHPrecoceCodigo: TIntegerField;
    QrCHPrecoceAlinea1: TIntegerField;
    QrCHPrecoceAlinea2: TIntegerField;
    QrCHPrecoceData1: TDateField;
    QrCHPrecoceData2: TDateField;
    QrCHPrecoceData3: TDateField;
    QrCHPrecoceCliente: TIntegerField;
    QrCHPrecoceBanco: TIntegerField;
    QrCHPrecoceAgencia: TIntegerField;
    QrCHPrecoceConta: TWideStringField;
    QrCHPrecoceCheque: TIntegerField;
    QrCHPrecoceCPF: TWideStringField;
    QrCHPrecoceValor: TFloatField;
    QrCHPrecoceTaxas: TFloatField;
    QrCHPrecoceMulta: TFloatField;
    QrCHPrecoceJurosP: TFloatField;
    QrCHPrecoceJurosV: TFloatField;
    QrCHPrecoceDesconto: TFloatField;
    QrCHPrecoceEmitente: TWideStringField;
    QrCHPrecoceChequeOrigem: TIntegerField;
    QrCHPrecoceValPago: TFloatField;
    QrCHPrecoceStatus: TSmallintField;
    QrCHPrecoceLk: TIntegerField;
    QrCHPrecoceDataCad: TDateField;
    QrCHPrecoceDataAlt: TDateField;
    QrCHPrecoceUserCad: TIntegerField;
    QrCHPrecoceUserAlt: TIntegerField;
    QrCHPrecocePgDesc: TFloatField;
    QrCHPrecoceDDeposito: TDateField;
    QrCHPrecoceVencto: TDateField;
    QrCHPrecoceLoteOrigem: TIntegerField;
    QrCHPrecoceAlterWeb: TSmallintField;
    QrCHPrecoceCodigo_1: TIntegerField;
    QrCHPrecoceControle: TIntegerField;
    QrCHPrecoceComp: TIntegerField;
    QrCHPrecoceBanco_1: TIntegerField;
    QrCHPrecoceAgencia_1: TIntegerField;
    QrCHPrecoceConta_1: TWideStringField;
    QrCHPrecoceCheque_1: TIntegerField;
    QrCHPrecoceCPF_1: TWideStringField;
    QrCHPrecoceEmitente_1: TWideStringField;
    QrCHPrecoceBruto: TFloatField;
    QrCHPrecoceDesco: TFloatField;
    QrCHPrecoceValor_1: TFloatField;
    QrCHPrecoceEmissao: TDateField;
    QrCHPrecoceDCompra: TDateField;
    QrCHPrecoceDDeposito_1: TDateField;
    QrCHPrecoceVencto_1: TDateField;
    QrCHPrecoceTxaCompra: TFloatField;
    QrCHPrecoceTxaJuros: TFloatField;
    QrCHPrecoceTxaAdValorem: TFloatField;
    QrCHPrecoceVlrCompra: TFloatField;
    QrCHPrecoceVlrAdValorem: TFloatField;
    QrCHPrecoceDMais: TIntegerField;
    QrCHPrecoceDias: TIntegerField;
    QrCHPrecoceDuplicata: TWideStringField;
    QrCHPrecoceDevolucao: TIntegerField;
    QrCHPrecoceQuitado: TIntegerField;
    QrCHPrecoceLk_1: TIntegerField;
    QrCHPrecoceDataCad_1: TDateField;
    QrCHPrecoceDataAlt_1: TDateField;
    QrCHPrecoceUserCad_1: TIntegerField;
    QrCHPrecoceUserAlt_1: TIntegerField;
    QrCHPrecocePraca: TIntegerField;
    QrCHPrecoceBcoCobra: TIntegerField;
    QrCHPrecoceAgeCobra: TIntegerField;
    QrCHPrecoceTotalJr: TFloatField;
    QrCHPrecoceTotalDs: TFloatField;
    QrCHPrecoceTotalPg: TFloatField;
    QrCHPrecoceData3_1: TDateField;
    QrCHPrecoceProrrVz: TIntegerField;
    QrCHPrecoceProrrDd: TIntegerField;
    QrCHPrecoceRepassado: TSmallintField;
    QrCHPrecoceDepositado: TSmallintField;
    QrCHPrecoceValQuit: TFloatField;
    QrCHPrecoceValDeposito: TFloatField;
    QrCHPrecoceTipo: TIntegerField;
    QrCHPrecoceAliIts: TIntegerField;
    QrCHPrecoceAlinPgs: TIntegerField;
    QrCHPrecoceNaoDeposita: TSmallintField;
    QrCHPrecoceReforcoCxa: TSmallintField;
    QrCHPrecoceCartDep: TIntegerField;
    QrCHPrecoceCobranca: TIntegerField;
    QrCHPrecoceRepCli: TIntegerField;
    QrCHPrecoceMotivo: TWideStringField;
    QrCHPrecoceObservacao: TWideStringField;
    QrCHPrecoceObsGerais: TWideStringField;
    QrCHPrecoceCliente_1: TIntegerField;
    QrCHPrecoceAlterWeb_1: TSmallintField;
    QrCHPrecoceTeste: TIntegerField;
    QrEmcart_: TmySQLQuery;
    QrDUVencido: TmySQLQuery;
    DBGrid1: TDBGrid;
    QrEmcart_SitCol: TLargeintField;
    QrEmcart_Tipo: TSmallintField;
    QrEmcart_Valor: TFloatField;
    CkMoroso: TCheckBox;
    QrDUMoroso: TmySQLQuery;
    QrDUVencPag: TmySQLQuery;
    QrDUVencidoValor: TFloatField;
    QrDUMoroPag: TmySQLQuery;
    QrEmcart: TmySQLQuery;
    QrEmcartColigado: TLargeintField;
    QrEmcartTipo: TSmallintField;
    QrEmcartValor: TFloatField;
    QrDUVencidoColigado: TLargeintField;
    QrDUVencidoTipo: TSmallintField;
    QrDUVencPagColigado: TLargeintField;
    QrDUVencPagTipo: TSmallintField;
    QrDUMoroPagColigado: TLargeintField;
    QrDUMoroPagTipo: TSmallintField;
    QrDUMorosoColigado: TLargeintField;
    QrDUMorosoTipo: TSmallintField;
    QrDUMorosoValor: TFloatField;
    QrEvolucap2DataE: TDateField;
    QrEvolucap2CHAbeA: TFloatField;
    QrEvolucap2CHAbeB: TFloatField;
    QrEvolucap2CHAbeC: TFloatField;
    QrEvolucap2DUAbeA: TFloatField;
    QrEvolucap2DUAbeB: TFloatField;
    QrEvolucap2DUAbeC: TFloatField;
    QrEvolucap2CHDevA: TFloatField;
    QrEvolucap2CHDevB: TFloatField;
    QrEvolucap2CHDevC: TFloatField;
    QrEvolucap2DUDevA: TFloatField;
    QrEvolucap2DUDevB: TFloatField;
    QrEvolucap2DUDevC: TFloatField;
    QrEvolucap2CHPgdA: TFloatField;
    QrEvolucap2CHPgdB: TFloatField;
    QrEvolucap2CHPgdC: TFloatField;
    QrEvolucap2DUPgdA: TFloatField;
    QrEvolucap2DUPgdB: TFloatField;
    QrEvolucap2DUPgdC: TFloatField;
    QrEvolucap2CHMorA: TFloatField;
    QrEvolucap2CHMorB: TFloatField;
    QrEvolucap2CHMorC: TFloatField;
    QrEvolucap2DUMorA: TFloatField;
    QrEvolucap2DUMorB: TFloatField;
    QrEvolucap2DUMorC: TFloatField;
    QrEvolucap2CHPgmA: TFloatField;
    QrEvolucap2CHPgmB: TFloatField;
    QrEvolucap2CHPgmC: TFloatField;
    QrEvolucap2DUPgmA: TFloatField;
    QrEvolucap2DUPgmB: TFloatField;
    QrEvolucap2DUPgmC: TFloatField;
    QrCHVencido: TmySQLQuery;
    QrCHVencidoColigado: TLargeintField;
    QrCHVencidoTipo: TSmallintField;
    QrCHVencidoValor: TFloatField;
    QrCHVencPag: TmySQLQuery;
    QrCHVencPagColigado: TLargeintField;
    QrCHVencPagTipo: TSmallintField;
    QrCHVencPagValor: TFloatField;
    QrCHMoroso: TmySQLQuery;
    LargeintField1: TLargeintField;
    SmallintField1: TSmallintField;
    FloatField1: TFloatField;
    QrCHMoroPag: TmySQLQuery;
    LargeintField2: TLargeintField;
    SmallintField2: TSmallintField;
    FloatField2: TFloatField;
    QrDUVencPagValor: TFloatField;
    QrDUMoroPagValor: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtOKClick(Sender: TObject);
    procedure QrEvolucap2CalcFields(DataSet: TDataSet);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure QrEvolperCalcFields(DataSet: TDataSet);
    procedure BtGraficoClick(Sender: TObject);
    procedure QrCHNullAfterOpen(DataSet: TDataSet);
    procedure QrCHNullBeforeClose(DataSet: TDataSet);
    procedure BtExclui1Click(Sender: TObject);
  private
    { Private declarations }
    FRegistryPath: String;
    procedure EditaSaldoConta;
    procedure ReopenEvolucap2(Data: TDateTime);
    procedure InsereItensAbe(
              var ValChA, ValChB, ValChC, ValDuA, ValDuB, ValDuC: Double;
              Data: String; Status: Integer);
    procedure InsereItensVen(Query: TmySQLQuery; var ValA, ValB, ValC: Double;
              Data: String; Status: Integer);
  public
    { Public declarations }
  end;

  var
  FmEvolucap2: TFmEvolucap2;

implementation

{$R *.DFM}

uses Module, Principal, UnMyObjects;

procedure TFmEvolucap2.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEvolucap2.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  QrCHNull.Close;
  QrCHNull.Open;
  if QrCHNull.RecordCount > 0 then
    PageControl1.ActivePageIndex := 2;
  QrCHPrecoce.Close;
  QrCHPrecoce.Open;
  if QrCHPrecoce.RecordCount > 0 then
    PageControl1.ActivePageIndex := 3;
end;

procedure TFmEvolucap2.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmEvolucap2.FormCreate(Sender: TObject);
var
  Especial: Integer;
begin
  // Atualizar LastEditLote
  PageControl1.ActivePageIndex := 0;
  Dmod.QrControle.Close;
  Dmod.QrControle.Open;
  //
  TabSheet2.TabVisible := False;
  //
  FRegistryPath := Application.Title + '\Evolucap2';
  TPIniA.Date   := Geral.ReadAppKeyLM('DataIniA', FRegistryPath, ktDate, Int(Date));
  TPFimA.Date   := Geral.ReadAppKeyLM('DataFimA', FRegistryPath, ktDate, Int(Date));
  TPIniB.Date   := Geral.ReadAppKeyLM('DataIniB', FRegistryPath, ktDate, Int(Date-30));
  TPFimB.Date   := Geral.ReadAppKeyLM('DataFimB', FRegistryPath, ktDate, Int(Date));
  //
  if Dmod.QrControleLastEditLote.Value > 0 then
    if TPIniA.Date > Dmod.QrControleLastEditLote.Value then
      TPIniA.Date := Dmod.QrControleLastEditLote.Value;
  Especial := Dmod.QrControleColigEsp.Value;
  if Especial <> 0 then
  begin
    EdColigado.Text := IntToStr(Especial);
    CBColigado.KeyValue := Especial;
  end;
  QrColigado.Open;
  ReopenEvolucap2(0);
  //
end;

procedure TFmEvolucap2.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Geral.WriteAppKeyLM2('DataIniA', FRegistryPath, Int(TPIniA.Date), ktDate);
  Geral.WriteAppKeyLM2('DataFimA', FRegistryPath, Int(TPFimA.Date), ktDate);
  Geral.WriteAppKeyLM2('DataIniB', FRegistryPath, Int(TPIniB.Date), ktDate);
  Geral.WriteAppKeyLM2('DataFimB', FRegistryPath, Int(TPFimB.Date), ktDate);
end;

procedure TFmEvolucap2.BtOKClick(Sender: TObject);
var
  i, a, b: Integer;
  d: String;
  CHAbeA, CHAbeB, CHAbeC, DUAbeA, DUAbeB, DUAbeC: Double;
  CHVenA, CHVenB, CHVenC, DUVenA, DUVenB, DUVenC: Double;
  CHPgdA, CHPgdB, CHPgdC, DUPgdA, DUPgdB, DUPgdC: Double;
  CHMorA, CHMorB, CHMorC, DUMorA, DUMorB, DUMorC: Double;
  CHPgmA, CHPgmB, CHPgmC, DUPgmA, DUPgmB, DUPgmC: Double;
begin
  CHAbeA := 0;
  CHAbeB := 0;
  CHAbeC := 0;
  DUAbeA := 0;
  DUAbeB := 0;
  DUAbeC := 0;
  CHVenA := 0;
  CHVenB := 0;
  CHVenC := 0;
  DUVenA := 0;
  DUVenB := 0;
  DUVenC := 0;
  CHPgdA := 0;
  CHPgdB := 0;
  CHPgdC := 0;
  DUPgdA := 0;
  DUPgdB := 0;
  DUPgdC := 0;
  CHMorA := 0;
  CHMorB := 0;
  CHMorC := 0;
  DUMorA := 0;
  DUMorB := 0;
  DUMorC := 0;
  CHPgmA := 0;
  CHPgmB := 0;
  CHPgmC := 0;
  DUPgmA := 0;
  DUPgmB := 0;
  DUPgmC := 0;
  //
  a := Trunc(TPIniA.Date);
  b := Trunc(TPFimA.Date);
  if b - a > 92 then
  begin
    if Application.MessageBox(PChar('Confirma a atualiza��o dos dados? '+
    'Per�odos grandes podem demorar alguns minutos!'), 'Pergunta', MB_YESNOCANCEL+
    MB_ICONQUESTION) <> ID_YES then Exit;
  end;
  Screen.Cursor := crDefault;
  if b - a < 0 then
  begin
    Application.MessageBox('Per�odo inv�lido', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM evolucap2 WHERE DataE BETWEEN :P0 AND :P1');
  Dmod.QrUpd.Params[0].AsString := Geral.FDT(a, 1);
  Dmod.QrUpd.Params[1].AsString := Geral.FDT(b, 1);
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM evolucap2its WHERE DataE BETWEEN :P0 AND :P1');
  Dmod.QrUpd.Params[0].AsString := Geral.FDT(a, 1);
  Dmod.QrUpd.Params[1].AsString := Geral.FDT(b, 1);
  Dmod.QrUpd.ExecSQL;
  //
  ProgressBar1.Position := 0;
  ProgressBar1.Max := b - a + 1;
  ProgressBar1.Visible := True;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO evolucap2 SET DataE=:P0, ');

  Dmod.QrUpd.SQL.Add('CHAbeA=:P01, CHAbeB=:P02, CHAbeC=:P03, DUAbeA=:P04, DUAbeB=:P05, DUAbeC=:P06, ');
  Dmod.QrUpd.SQL.Add('CHDevA=:P07, CHDevB=:P08, CHDevC=:P09, DUDevA=:P10, DUDevB=:P11, DUDevC=:P12, ');
  Dmod.QrUpd.SQL.Add('CHPgdA=:P13, CHPgdB=:P14, CHPgdC=:P15, DUPgdA=:P16, DUPgdB=:P17, DUPgdC=:P18, ');
  Dmod.QrUpd.SQL.Add('CHMorA=:P19, CHMorB=:P20, CHMorC=:P21, DUMorA=:P22, DUMorB=:P23, DUMorC=:P24, ');
  Dmod.QrUpd.SQL.Add('CHPgmA=:P25, CHPgmB=:P26, CHPgmC=:P27, DUPgmA=:P28, DUPgmB=:P29, DUPgmC=:P30  ');


  for i := a to b do
  begin
    ProgressBar1.Position := ProgressBar1.Position + 1;
    //
    d := FormatDateTime('yyyy/mm/dd', i);
    //
    QrEmcart.Close;
    QrEmcart.Params[0].AsString  := d;
    QrEmcart.Params[1].AsString  := d;
    QrEmcart.Open;
    InsereItensAbe(ChAbeA, ChAbeB, ChAbeC, DUAbeA, DUAbeB, DuAbeC, d, 0);
    //
    QrDUVencido.Close;
    QrDUVencido.Params[0].AsString  := d;
    QrDUVencido.Params[1].AsString  := d;
    QrDUVencido.Open;
    InsereItensVen(QrDUVencido, DUVenA, DUVenB, DUVenC, d, 1);
    //
    QrDUVencPag.Close;
    QrDUVencPag.Params[0].AsString  := d;
    QrDUVencPag.Params[1].AsString  := d;
    QrDUVencPag.Params[2].AsString  := d;
    QrDUVencPag.Open;
    InsereItensVen(QrDUVencPag, DUPgdA, DUPgdB, DUPgdC, d, 2);
    //
    //
    QrDUMoroso.Close;
    QrDUMoroso.Params[0].AsString  := d;
    QrDUMoroso.Params[1].AsString  := d;
    QrDUMoroso.Open;
    InsereItensVen(QrDUMoroso, DUMorA, DUMorB, DUMorC, d, 3);
    //
    QrDUMoroPag.Close;
    QrDUMoroPag.Params[0].AsString  := d;
    QrDUMoroPag.Params[1].AsString  := d;
    QrDUMoroPag.Params[2].AsString  := d;
    QrDUMoroPag.Open;
    //
    InsereItensVen(QrDUMoroPag, DUPgmA, DUPgmB, DUPgmC, d, 4);
    //
    QrCHVencido.Close;
    QrCHVencido.Params[0].AsString  := d;
    QrCHVencido.Params[1].AsString  := d;
    QrCHVencido.Params[2].AsString  := d;
    QrCHVencido.Open;
    InsereItensVen(QrCHVencido, CHVenA, CHVenB, CHVenC, d, 5);
    //
    QrCHVencPag.Close;
    QrCHVencPag.Params[0].AsString  := d;
    QrCHVencPag.Params[1].AsString  := d;
    QrCHVencPag.Params[2].AsString  := d;
    QrCHVencPag.Open;
    InsereItensVen(QrCHVencPag, CHPgdA, CHPgdB, CHPgdC, d, 6);
    //
    QrCHMoroso.Close;
    QrCHMoroso.Params[0].AsString  := d;
    QrCHMoroso.Params[1].AsString  := d;
    QrCHMoroso.Params[2].AsString  := d;
    QrCHMoroso.Open;
    InsereItensVen(QrCHMoroso, CHMorA, CHMorB, CHMorC, d, 7);
    //
    QrCHMoroPag.Close;
    QrCHMoroPag.Params[0].AsString  := d;
    QrCHMoroPag.Params[1].AsString  := d;
    QrCHMoroPag.Params[2].AsString  := d;
    QrCHMoroPag.Open;
    InsereItensVen(QrCHMoroPag, CHPgmA, CHPgmB, CHPgmC, d, 8);
    //
    //
    // Inclus�o do registro
    //
    Dmod.QrUpd.Params[00].AsString := d;
    Dmod.QrUpd.Params[01].AsFloat  := CHAbeA;
    Dmod.QrUpd.Params[02].AsFloat  := CHAbeB;
    Dmod.QrUpd.Params[03].AsFloat  := CHAbeC;
    Dmod.QrUpd.Params[04].AsFloat  := DUAbeA;
    Dmod.QrUpd.Params[05].AsFloat  := DUAbeB;
    Dmod.QrUpd.Params[06].AsFloat  := DUAbeC;
    Dmod.QrUpd.Params[07].AsFloat  := CHVenA;
    Dmod.QrUpd.Params[08].AsFloat  := CHVenB;
    Dmod.QrUpd.Params[09].AsFloat  := CHVenC;
    Dmod.QrUpd.Params[10].AsFloat  := DUVenA;
    Dmod.QrUpd.Params[11].AsFloat  := DUVenB;
    Dmod.QrUpd.Params[12].AsFloat  := DUVenC;
    Dmod.QrUpd.Params[13].AsFloat  := CHPgdA;
    Dmod.QrUpd.Params[14].AsFloat  := CHPgdB;
    Dmod.QrUpd.Params[15].AsFloat  := CHPgdC;
    Dmod.QrUpd.Params[16].AsFloat  := DUPgdA;
    Dmod.QrUpd.Params[17].AsFloat  := DUPgdB;
    Dmod.QrUpd.Params[18].AsFloat  := DUPgdC;
    Dmod.QrUpd.Params[19].AsFloat  := CHMorA;
    Dmod.QrUpd.Params[20].AsFloat  := CHMorB;
    Dmod.QrUpd.Params[21].AsFloat  := CHMorC;
    Dmod.QrUpd.Params[22].AsFloat  := DUMorA;
    Dmod.QrUpd.Params[23].AsFloat  := DUMorB;
    Dmod.QrUpd.Params[24].AsFloat  := DUMorC;
    Dmod.QrUpd.Params[25].AsFloat  := CHPgmA;
    Dmod.QrUpd.Params[26].AsFloat  := CHPgmB;
    Dmod.QrUpd.Params[27].AsFloat  := CHPgmC;
    Dmod.QrUpd.Params[28].AsFloat  := DUPgmA;
    Dmod.QrUpd.Params[29].AsFloat  := DUPgmB;
    Dmod.QrUpd.Params[30].AsFloat  := DUPgmC;
    // Parei Aqui continuar

    Dmod.QrUpd.ExecSQL;
  end;
  ReopenEvolucap2(0);
  FmPrincipal.AtualizaLastEditLote(Geral.FDT(b, 1));
  ProgressBar1.Visible := False;
  ProgressBar1.Position := 0;
  Screen.Cursor := crDefault;
end;

procedure TFmEvolucap2.QrEvolucap2CalcFields(DataSet: TDataSet);
begin
  (*QrEvolucap2CARTEIREAL.Value :=
  // Parei aqui
    QrEvolucap2CarteiTudo.Value -
    QrEvolucap2RepassEspe.Value -
    QrEvolucap2RepassOutr.Value;
  QrEvolucap2VALOR_TOTAL.Value :=
    QrEvolucap2CarteiTudo.Value +
    QrEvolucap2CHDevolAbe.Value +
    QrEvolucap2DUDevolAbe.Value +
    QrEvolucap2SaldoCaixa.Value +
    QrEvolucap2SaldoConta.Value;
  QrEvolucap2VALOR_SUB_T.Value :=
    QrEvolucap2VALOR_TOTAL.Value -
    QrEvolucap2RepassOutr.Value; *)
end;

procedure TFmEvolucap2.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then EditaSaldoConta;
end;

procedure TFmEvolucap2.DBGrid1DblClick(Sender: TObject);
begin
  EditaSaldoConta;
end;

procedure TFmEvolucap2.BitBtn1Click(Sender: TObject);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE evolucap2 SET SaldoConta=:P0, SaldoCaixa=:P1 ');
  Dmod.QrUpd.SQL.Add('WHERE DataE=:Pa');
  Dmod.QrUpd.Params[0].AsFloat   := Geral.DMV(EdSaldoConta.Text);
  Dmod.QrUpd.Params[1].AsFloat   := Geral.DMV(EdSaldoCaixa.Text);
  //
  Dmod.QrUpd.Params[2].AsString  := Geral.FDT(QrEvolucap2DataE.Value, 1);
  Dmod.QrUpd.ExecSQL;
  //
  ReopenEvolucap2(QrEvolucap2DataE.Value);
  PnAtualiza.Visible  := True;
  PnDados.Enabled     := True;
  PnEditSdoCC.Visible := False;
  DBGrid1.SetFocus;
end;

procedure TFmEvolucap2.ReopenEvolucap2(Data: TDateTime);
begin
  QrEvolucap2.Close;
  QrEvolucap2.Open;
  //
  if Data > 0 then QrEvolucap2.Locate('DataE', Int(Data), [])
  else QrEvolucap2.Last;
  //
end;

procedure TFmEvolucap2.EditaSaldoConta;
begin
  ///////
  Exit;//
  ///////
  PnEditSdoCC.Visible := True;
  PnAtualiza.Visible  := False;
  PnDados.Enabled     := False;
  //
  (*EdSaldoConta.Text := Geral.FFT(QrEvolucap2SaldoConta.Value, 2, siNegativo);
  EdSaldoCaixa.Text := Geral.FFT(QrEvolucap2SaldoCaixa.Value, 2, siNegativo);
  EdSaldoConta.SetFocus;*)
end;

procedure TFmEvolucap2.BitBtn2Click(Sender: TObject);
begin
  PnAtualiza.Visible  := True;
  PnDados.Enabled     := True;
  PnEditSdoCC.Visible := False;
  //
  DBGrid1.SetFocus;
end;

procedure TFmEvolucap2.QrEvolperCalcFields(DataSet: TDataSet);
begin
  QrEvolperCARTEIREAL.Value :=
    QrEvolperCarteiTudo.Value -
    QrEvolperRepassEspe.Value -
    QrEvolperRepassOutr.Value;
  QrEvolperVALOR_TOTAL.Value :=
    QrEvolperCarteiTudo.Value +
    QrEvolperCHDevolAbe.Value +
    QrEvolperDUDevolAbe.Value +
    QrEvolperSaldoCaixa.Value +
    QrEvolperSaldoConta.Value;
  QrEvolperVALOR_SUB_T.Value :=
    QrEvolperVALOR_TOTAL.Value -
    QrEvolperRepassOutr.Value;
end;

procedure TFmEvolucap2.BtGraficoClick(Sender: TObject);
(*
var
  Fator: Integer;
  Maximo: Double;
*)
begin
(*
  QrEvolper.Close;
  QrEvolper.SQL.Clear;
  case RGIntervalo.ItemIndex of
    0:
    begin
      QrEvolper.SQL.Add('SELECT * FROM evolucap2');
      QrEvolper.SQL.Add('WHERE DataE BETWEEN :P0 AND :P1');
      QrEvolper.SQL.Add('ORDER BY DataE');
    end;
    1:
    begin
      QrEvolper.SQL.Add('SELECT * FROM evolucap2');
      QrEvolper.SQL.Add('WHERE DataE BETWEEN :P0 AND :P1');
      QrEvolper.SQL.Add('AND MONTH(DataE) <> MONTH(DATE_ADD(DataE, INTERVAL 1 DAY))');
    end;
  end;
  QrEvolper.Params[0].AsString := Geral.FDT(TPIniB.Date, 1);
  QrEvolper.Params[1].AsString := Geral.FDT(TPFimB.Date, 1);
  QrEvolper.Open;
  //
  QrMax.Close;
  QrMax.Params[0].AsString := Geral.FDT(TPIniB.Date, 1);
  QrMax.Params[1].AsString := Geral.FDT(TPFimB.Date, 1);
  QrMax.Open;
  Fator := 1;
  Maximo := QrMaxTOTAL.Value;
  while Maximo > 1000 do
  begin
    Fator := Fator * 1000;
    Maximo := Maximo / 1000;
  end;
  //
  Pego1.Subsets := 9; // Carteira, Especial, Outros, CH Dev, DU venc., saldo conta, Total - outros, Total
  Pego1.Points := QrEvolper.RecordCount;
  QrEvolper.First;
  while not QrEvolper.Eof do
  begin
    Pego1.YData[0, QrEvolper.RecNo-1] := QrEvolperCARTEIREAL.Value / Fator;
    Pego1.YData[1, QrEvolper.RecNo-1] := QrEvolperRepassEspe.Value / Fator;
    Pego1.YData[2, QrEvolper.RecNo-1] := QrEvolperRepassOutr.Value / Fator;
    Pego1.YData[3, QrEvolper.RecNo-1] := QrEvolperCHDevolAbe.Value / Fator;
    Pego1.YData[4, QrEvolper.RecNo-1] := QrEvolperDUDevolAbe.Value / Fator;
    Pego1.YData[5, QrEvolper.RecNo-1] := QrEvolperSaldoConta.Value / Fator;
    Pego1.YData[6, QrEvolper.RecNo-1] := QrEvolperSaldoCaixa.Value / Fator;
    Pego1.YData[7, QrEvolper.RecNo-1] := QrEvolperVALOR_SUB_T.Value / Fator;
    Pego1.YData[8, QrEvolper.RecNo-1] := QrEvolperVALOR_TOTAL.Value / Fator;
    //
    Pego1.PointLabels[QrEvolper.RecNo-1] := Geral.FDT(QrEvolperDataE.Value, 2);
    //
    QrEvolper.Next;
  end;
  // Set Various Properties //
  Pego1.DeskColor := RGB(192, 192, 192);
  Pego1.GraphBackColor := 0;
  Pego1.GraphForeColor := RGB(255, 255, 255);

  // Set DataShadows to show shadows//
  Pego1.DataShadows := gWithShadows;
  Pego1.BorderTypes := gInset;
  Pego1.MainTitle := '';
  Pego1.SubTitle := 'Evolu��o do Capital de '+
    Geral.FDT(TPIniB.Date, 2)+' at� ' + Geral.FDT(TPFimB.Date, 2);
  if Fator = 1 then
    Pego1.YAxisLabel := '$ Valor'
  else
    Pego1.YAxisLabel := '$ Valor x '+Geral.FFT(Fator, 0, siNegativo);
  Pego1.XAxisLabel := 'Data';
  Pego1.FocalRect := False;
  Pego1.PlottingMethod := gSpline;
  Pego1.GridLineControl := gNoGrid;
  Pego1.AllowRibbon := True;
  Pego1.AllowZooming := gHorzPlusVertZooming;
  Pego1.ZoomStyle := gRO2NOT;

  // Set SubsetLabels property array for 4 subsets //
  Pego1.SubsetLabels[00] := 'Carteira';
  Pego1.SubsetLabels[01] := 'Especial';
  Pego1.SubsetLabels[02] := 'Outros';
  Pego1.SubsetLabels[03] := 'CH Dev.';
  Pego1.SubsetLabels[04] := 'DU Dev.';
  Pego1.SubsetLabels[05] := 'Sdo caixa';
  Pego1.SubsetLabels[06] := 'Sdo conta';
  Pego1.SubsetLabels[07] := '(-)outros';
  Pego1.SubsetLabels[08] := 'Total';

  // this is how to change subset colors //
  Pego1.SubsetColors[0] := RGB(198, 0, 0);
  Pego1.SubsetColors[1] := RGB(0, 198, 198);
  Pego1.SubsetColors[2] := RGB(198, 198, 0);
  Pego1.SubsetColors[3] := RGB(0, 198, 0);

  // this is how to change line types //
  Pego1.SubsetLineTypes[0] := 0;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[1] := 1;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[2] := 2;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[3] := 3;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[4] := 0;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[5] := 1;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[6] := 2;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[7] := 3;//PELT_MEDIUMSOLID;
  Pego1.SubsetLineTypes[8] := 6;//PELT_MEDIUMSOLID;

  // this is how to change point types //
  Pego1.SubsetPointTypes[0] := 0;//PEPT_DOTSOLID;
  Pego1.SubsetPointTypes[1] := 1;//PEPT_UPTRIANGLESOLID;
  Pego1.SubsetPointTypes[2] := 2;//PEPT_SQUARESOLID;
  Pego1.SubsetPointTypes[3] := 3;//PEPT_DOWNTRIANGLESOLID;
  Pego1.SubsetPointTypes[4] := 0;//PEPT_DOTSOLID;
  Pego1.SubsetPointTypes[5] := 1;//PEPT_UPTRIANGLESOLID;
  Pego1.SubsetPointTypes[6] := 2;//PEPT_SQUARESOLID;
  Pego1.SubsetPointTypes[7] := 0;//PEPT_SQUARESOLID;
  Pego1.SubsetPointTypes[8] := 3;//PEPT_DOWNTRIANGLESOLID;

  // Various other features //
  Pego1.FixedFonts := True;
  Pego1.BitmapGradientMode := True;
  Pego1.QuickStyle := gLightLine;
  Pego1.SimpleLineLegend:=True;
  Pego1.SimplePointLegend:=True;
  Pego1.LegendStyle:=gOneLine;

  Pego1.GradientBars := 8;
  Pego1.MainTitleBold := True;
  Pego1.SubTitleBold := True;
  Pego1.LabelBold := True;
  Pego1.LineShadows := True;
  Pego1.TextShadows := gShadowBoldText;
  Pego1.FontSize := gSmall;

  // Always call PEactions := 0 at end //
  Pego1.Visible := True;
  Pego1.PEactions := epeactions(0);
  PageControl1.ActivePageIndex := 1;
*)
end;

procedure TFmEvolucap2.QrCHNullAfterOpen(DataSet: TDataSet);
begin
  if QrCHNull.RecordCount > 0 then
    BtExclui1.Enabled := True
  else
    BtExclui1.Enabled := False;
end;

procedure TFmEvolucap2.QrCHNullBeforeClose(DataSet: TDataSet);
begin
  BtExclui1.Enabled := False;
end;

procedure TFmEvolucap2.BtExclui1Click(Sender: TObject);
begin
  if Application.MessageBox('Confirma a exclus�o do cheque devolvido orf�o?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM alinits WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrCHNullCodigo.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrCHNull.Close;
    QrCHNull.Open;
  end;
end;

procedure TFmEvolucap2.InsereItensAbe(
 var ValChA, ValChB, ValChC, ValDuA, ValDuB, ValDuC: Double;
 Data: String; Status: Integer);
begin
  ValChA := 0;
  ValChB := 0;
  ValChC := 0;
  ValDuA := 0;
  ValDuB := 0;
  ValDuC := 0;
  QrEmcart.First;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO evolucap2its SET ');
  Dmod.QrUpdM.SQL.Add('DataE=:P0, Coligado=:P1, Tipo=:P2, Status=:P3, ');
  Dmod.QrUpdM.SQL.Add('Valor=:P4 ');
  while not QrEmcart.Eof do
  begin
    if QrEmcartColigado.Value = 0 then
    begin
      case QrEmcartTipo.Value of
        0: ValChA := ValChA + QrEmcartValor.Value;
        1: ValDuA := ValDuA + QrEmcartValor.Value;
      end;
    end else
    if QrEmcartColigado.Value = Dmod.QrControleColigEsp.Value then
    begin
      case QrEmcartTipo.Value of
        0: ValChB := ValChB + QrEmcartValor.Value;
        1: ValDuB := ValDuB + QrEmcartValor.Value;
      end;
    end else
    begin
      case QrEmcartTipo.Value of
        0: ValChC := ValChC + QrEmcartValor.Value;
        1: ValDuC := ValDuC + QrEmcartValor.Value;
      end;
    end;
    // SQL Itens
    Dmod.QrUpdM.Params[00].AsString  := Data;
    Dmod.QrUpdM.Params[01].AsInteger := QrEmcartColigado.Value;
    Dmod.QrUpdM.Params[02].AsInteger := QrEmcartTipo.Value;
    Dmod.QrUpdM.Params[03].AsInteger := Status;
    Dmod.QrUpdM.Params[04].AsFloat   := QrEmcartValor.Value;
    Dmod.QrUpdM.ExecSQL;
    //
    QrEmcart.Next;
  end;
end;

procedure TFmEvolucap2.InsereItensVen(Query: TmySQLQuery; var ValA, ValB, ValC: Double;
              Data: String; Status: Integer);
var
  Valor: Double;
  Coligado, Tipo: Integer;
begin
  ValA := 0;
  ValB := 0;
  ValC := 0;
  Query.First;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO evolucap2its SET ');
  Dmod.QrUpdM.SQL.Add('DataE=:P0, Coligado=:P1, Tipo=:P2, Status=:P3, ');
  Dmod.QrUpdM.SQL.Add('Valor=:P4 ');
  while not Query.Eof do
  begin
    try
      Valor    := Query.FieldByName('Valor').AsFloat;
      Coligado := Query.FieldByName('Coligado').AsInteger;
      Tipo     := Query.FieldByName('Tipo').AsInteger
    except
      Valor := 0;
      Coligado := 0;
      Tipo := 0;
    end;
    if Coligado = 0 then ValA := ValA + Valor
    else
    if Coligado = Dmod.QrControleColigEsp.Value then
      ValB := ValB + Valor
    else
      ValC := ValC + Valor;
    // SQL Itens
    Dmod.QrUpdM.Params[00].AsString  := Data;
    Dmod.QrUpdM.Params[01].AsInteger := Coligado;
    Dmod.QrUpdM.Params[02].AsInteger := Tipo;
    Dmod.QrUpdM.Params[03].AsInteger := Status;
    Dmod.QrUpdM.Params[04].AsFloat   := Valor;
    Dmod.QrUpdM.ExecSQL;
    //
    Query.Next;
  end;
(*
  DUVenA := 0;
  DUVenB := 0;
  DuVenC := 0;
  QrDUVencido.First;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('INSERT INTO evolucap2its SET ');
  Dmod.QrUpdM.SQL.Add('DataE=:P0, Coligado=:P1, Tipo=:P2, Status=:P3, ');
  Dmod.QrUpdM.SQL.Add('Valor=:P4 ');
  while not QrDUVencido.Eof do
  begin
    if QrDUVencidoColigado.Value = 0 then
      DUVenA := DUVenA + QrDUVencidoValor.Value;
    else
    if QrDUVencidoColigado.Value = Dmod.QrControleColigEsp.Value then
      DUVenB := DUVenB + QrDUVencidoValor.Value;
    else
      DUVenC := DUVenC + QrDUVencidoValor.Value;
    // SQL Itens
    Dmod.QrUpdM.Params[00].AsString  := d;
    Dmod.QrUpdM.Params[01].AsInteger := QrDUVencidoColigado.Value;
    Dmod.QrUpdM.Params[02].AsInteger := QrDUVencidoTipo.Value;
    Dmod.QrUpdM.Params[03].AsInteger := Status;
    Dmod.QrUpdM.Params[04].AsFloat   := QrDUVencidoValor.Value;
    Dmod.QrUpdM.ExecSQL;
    //
    QrDUVencido.Next;
  end;
*)
end;

end.

