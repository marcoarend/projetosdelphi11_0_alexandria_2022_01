unit LotesRepCliMul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, Db, mySQLDbTables,
  dmkEdit, Mask, DBCtrls, ComCtrls, Variants, dmkGeral, UnDmkProcFunc, UnDmkEnums;

type
  TFmLotesRepCliMul = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    BtSaida: TBitBtn;
    QrCheque7: TmySQLQuery;
    DsCheque7: TDataSource;
    BitBtn1: TBitBtn;
    QrCheque7Codigo: TIntegerField;
    QrCheque7Controle: TIntegerField;
    QrCheque7Comp: TIntegerField;
    QrCheque7Banco: TIntegerField;
    QrCheque7Agencia: TIntegerField;
    QrCheque7Conta: TWideStringField;
    QrCheque7Cheque: TIntegerField;
    QrCheque7CPF: TWideStringField;
    QrCheque7Emitente: TWideStringField;
    QrCheque7Bruto: TFloatField;
    QrCheque7Desco: TFloatField;
    QrCheque7Valor: TFloatField;
    QrCheque7Emissao: TDateField;
    QrCheque7DCompra: TDateField;
    QrCheque7DDeposito: TDateField;
    QrCheque7Vencto: TDateField;
    QrCheque7TxaCompra: TFloatField;
    QrCheque7TxaJuros: TFloatField;
    QrCheque7TxaAdValorem: TFloatField;
    QrCheque7VlrCompra: TFloatField;
    QrCheque7VlrAdValorem: TFloatField;
    QrCheque7DMais: TIntegerField;
    QrCheque7Dias: TIntegerField;
    QrCheque7Duplicata: TWideStringField;
    QrCheque7Devolucao: TIntegerField;
    QrCheque7Quitado: TIntegerField;
    QrCheque7Lk: TIntegerField;
    QrCheque7DataCad: TDateField;
    QrCheque7DataAlt: TDateField;
    QrCheque7UserCad: TIntegerField;
    QrCheque7UserAlt: TIntegerField;
    QrCheque7Praca: TIntegerField;
    QrCheque7BcoCobra: TIntegerField;
    QrCheque7AgeCobra: TIntegerField;
    QrCheque7TotalJr: TFloatField;
    QrCheque7TotalDs: TFloatField;
    QrCheque7TotalPg: TFloatField;
    QrCheque7Data3: TDateField;
    QrCheque7ProrrVz: TIntegerField;
    QrCheque7ProrrDd: TIntegerField;
    QrCheque7Repassado: TSmallintField;
    QrCheque7Depositado: TSmallintField;
    QrCheque7ValQuit: TFloatField;
    QrCheque7ValDeposito: TFloatField;
    QrCheque7Tipo: TIntegerField;
    QrCheque7AliIts: TIntegerField;
    QrCheque7AlinPgs: TIntegerField;
    QrCheque7NaoDeposita: TSmallintField;
    QrCheque7ReforcoCxa: TSmallintField;
    QrCheque7CartDep: TIntegerField;
    QrCheque7Cobranca: TIntegerField;
    QrCheque7RepCli: TIntegerField;
    QrCheque7NOMECLI: TWideStringField;
    QrCheque7Lote: TIntegerField;
    Timer1: TTimer;
    BitBtn2: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel7: TPanel;
    CkDescende1: TCheckBox;
    CkDescende2: TCheckBox;
    CkDescende3: TCheckBox;
    Panel8: TPanel;
    Label24: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    EdSoma: TdmkEdit;
    EdFalta0: TDBEdit;
    EdSaldo: TdmkEdit;
    Panel6: TPanel;
    RGOrdem3: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem1: TRadioGroup;
    Panel3: TPanel;
    DBG1: TDBGrid;
    Panel9: TPanel;
    Panel10: TPanel;
    Label43: TLabel;
    Label49: TLabel;
    Label50: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label42: TLabel;
    Label3: TLabel;
    EdCMC_7_1: TEdit;
    EdCPF_2: TdmkEdit;
    EdEmitente2: TdmkEdit;
    EdRegiaoCompe: TdmkEdit;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    EdConta: TdmkEdit;
    EdRealCC: TdmkEdit;
    EdCheque: TdmkEdit;
    EdValor: TdmkEdit;
    EdVencto: TdmkEdit;
    EdBanda: TdmkEdit;
    TabSheet2: TTabSheet;
    Panel11: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    EdCPFM: TdmkEdit;
    EdEmitM: TdmkEdit;
    EdBancoM: TdmkEdit;
    EdAgenciaM: TdmkEdit;
    EdContaM: TdmkEdit;
    EdChequeM: TdmkEdit;
    EdValorM: TdmkEdit;
    EdVctoM: TdmkEdit;
    DBG2: TDBGrid;
    QrChequeM: TmySQLQuery;
    DsChequeM: TDataSource;
    QrChequeMNOMECLI: TWideStringField;
    QrChequeMLote: TIntegerField;
    QrChequeMCodigo: TIntegerField;
    QrChequeMControle: TIntegerField;
    QrChequeMComp: TIntegerField;
    QrChequeMBanco: TIntegerField;
    QrChequeMAgencia: TIntegerField;
    QrChequeMConta: TWideStringField;
    QrChequeMCheque: TIntegerField;
    QrChequeMCPF: TWideStringField;
    QrChequeMEmitente: TWideStringField;
    QrChequeMBruto: TFloatField;
    QrChequeMDesco: TFloatField;
    QrChequeMValor: TFloatField;
    QrChequeMEmissao: TDateField;
    QrChequeMDCompra: TDateField;
    QrChequeMDDeposito: TDateField;
    QrChequeMVencto: TDateField;
    QrChequeMTxaCompra: TFloatField;
    QrChequeMTxaJuros: TFloatField;
    QrChequeMTxaAdValorem: TFloatField;
    QrChequeMVlrCompra: TFloatField;
    QrChequeMVlrAdValorem: TFloatField;
    QrChequeMDMais: TIntegerField;
    QrChequeMDias: TIntegerField;
    QrChequeMDuplicata: TWideStringField;
    QrChequeMDevolucao: TIntegerField;
    QrChequeMQuitado: TIntegerField;
    QrChequeMLk: TIntegerField;
    QrChequeMDataCad: TDateField;
    QrChequeMDataAlt: TDateField;
    QrChequeMUserCad: TIntegerField;
    QrChequeMUserAlt: TIntegerField;
    QrChequeMPraca: TIntegerField;
    QrChequeMBcoCobra: TIntegerField;
    QrChequeMAgeCobra: TIntegerField;
    QrChequeMTotalJr: TFloatField;
    QrChequeMTotalDs: TFloatField;
    QrChequeMTotalPg: TFloatField;
    QrChequeMData3: TDateField;
    QrChequeMProrrVz: TIntegerField;
    QrChequeMProrrDd: TIntegerField;
    QrChequeMRepassado: TSmallintField;
    QrChequeMDepositado: TSmallintField;
    QrChequeMValQuit: TFloatField;
    QrChequeMValDeposito: TFloatField;
    QrChequeMTipo: TIntegerField;
    QrChequeMAliIts: TIntegerField;
    QrChequeMAlinPgs: TIntegerField;
    QrChequeMNaoDeposita: TSmallintField;
    QrChequeMReforcoCxa: TSmallintField;
    QrChequeMCartDep: TIntegerField;
    QrChequeMCobranca: TIntegerField;
    QrChequeMRepCli: TIntegerField;
    QrChequeMMotivo: TWideStringField;
    QrChequeMObservacao: TWideStringField;
    QrChequeMObsGerais: TWideStringField;
    CkDepositados: TCheckBox;
    TabSheet3: TTabSheet;
    Panel12: TPanel;
    EdValorV: TdmkEdit;
    Label4: TLabel;
    DBG3: TDBGrid;
    QrChequeV: TmySQLQuery;
    DsChequeV: TDataSource;
    QrChequeVNOMECLI: TWideStringField;
    QrChequeVLote: TIntegerField;
    QrChequeVCodigo: TIntegerField;
    QrChequeVControle: TIntegerField;
    QrChequeVComp: TIntegerField;
    QrChequeVBanco: TIntegerField;
    QrChequeVAgencia: TIntegerField;
    QrChequeVConta: TWideStringField;
    QrChequeVCheque: TIntegerField;
    QrChequeVCPF: TWideStringField;
    QrChequeVEmitente: TWideStringField;
    QrChequeVBruto: TFloatField;
    QrChequeVDesco: TFloatField;
    QrChequeVValor: TFloatField;
    QrChequeVEmissao: TDateField;
    QrChequeVDCompra: TDateField;
    QrChequeVDDeposito: TDateField;
    QrChequeVVencto: TDateField;
    QrChequeVTxaCompra: TFloatField;
    QrChequeVTxaJuros: TFloatField;
    QrChequeVTxaAdValorem: TFloatField;
    QrChequeVVlrCompra: TFloatField;
    QrChequeVVlrAdValorem: TFloatField;
    QrChequeVDMais: TIntegerField;
    QrChequeVDias: TIntegerField;
    QrChequeVDuplicata: TWideStringField;
    QrChequeVDevolucao: TIntegerField;
    QrChequeVQuitado: TIntegerField;
    QrChequeVLk: TIntegerField;
    QrChequeVDataCad: TDateField;
    QrChequeVDataAlt: TDateField;
    QrChequeVUserCad: TIntegerField;
    QrChequeVUserAlt: TIntegerField;
    QrChequeVPraca: TIntegerField;
    QrChequeVBcoCobra: TIntegerField;
    QrChequeVAgeCobra: TIntegerField;
    QrChequeVTotalJr: TFloatField;
    QrChequeVTotalDs: TFloatField;
    QrChequeVTotalPg: TFloatField;
    QrChequeVData3: TDateField;
    QrChequeVProrrVz: TIntegerField;
    QrChequeVProrrDd: TIntegerField;
    QrChequeVRepassado: TSmallintField;
    QrChequeVDepositado: TSmallintField;
    QrChequeVValQuit: TFloatField;
    QrChequeVValDeposito: TFloatField;
    QrChequeVTipo: TIntegerField;
    QrChequeVAliIts: TIntegerField;
    QrChequeVAlinPgs: TIntegerField;
    QrChequeVNaoDeposita: TSmallintField;
    QrChequeVReforcoCxa: TSmallintField;
    QrChequeVCartDep: TIntegerField;
    QrChequeVCobranca: TIntegerField;
    QrChequeVRepCli: TIntegerField;
    QrChequeVMotivo: TWideStringField;
    QrChequeVObservacao: TWideStringField;
    QrChequeVObsGerais: TWideStringField;
    CkAuto: TCheckBox;
    Panel13: TPanel;
    Label7: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    EdSoma2: TdmkEdit;
    EdFalta2: TDBEdit;
    EdSaldo2: TdmkEdit;
    LaAvisos: TLabel;
    QrCaixa: TmySQLQuery;
    QrCaixaTOTAL: TFloatField;
    Label16: TLabel;
    DBEdit2: TDBEdit;
    DsCaixa: TDataSource;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure DBG1CellClick(Column: TColumn);
    procedure DBG1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdSomaChange(Sender: TObject);
    procedure EdFalta0Change(Sender: TObject);
    procedure EdBandaChange(Sender: TObject);
    procedure EdCMC_7_1Change(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure EdBancoMExit(Sender: TObject);
    procedure EdAgenciaMExit(Sender: TObject);
    procedure EdContaMExit(Sender: TObject);
    procedure EdChequeMExit(Sender: TObject);
    procedure EdValorMExit(Sender: TObject);
    procedure EdVctoMExit(Sender: TObject);
    procedure EdCPFMExit(Sender: TObject);
    procedure EdEmitMExit(Sender: TObject);
    procedure CkDepositadosClick(Sender: TObject);
    procedure EdValorVExit(Sender: TObject);
  private
    { Private declarations }
    FAscendente: Boolean;
    procedure ReopenCheque7(Controle: Integer);
    procedure ReopenChequeM;
    procedure ReopenChequeV(Automatico: Boolean);
    procedure ReopenCaixa;
    procedure SomaLinhas(Ascendente: Boolean);
    procedure PreparaSoma(Ascendente: Boolean);
    procedure ExecutaSelecionados(Acao: Integer);
    procedure ExecutaAtual(Acao: Integer);
    procedure Executa(Acao: Integer);
    procedure CalculaSaldo;
  public
    { Public declarations }
    FRepCli: Integer;
  end;

  var
  FmLotesRepCliMul: TFmLotesRepCliMul;

implementation

uses Module, Lotes1, Principal, LotesDep, UnMyObjects;

{$R *.DFM}

procedure TFmLotesRepCliMul.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLotesRepCliMul.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  EdBanda.SetFocus;
end;

procedure TFmLotesRepCliMul.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLotesRepCliMul.RGOrdem1Click(Sender: TObject);
begin
  ReopenCheque7(QrCheque7Controle.Value);
end;

procedure TFmLotesRepCliMul.PreparaSoma(Ascendente: Boolean);
begin
  FAscendente := Ascendente;
  if Timer1.Enabled then Timer1.Enabled := False;
  Timer1.Enabled := True;
end;

procedure TFmLotesRepCliMul.SomaLinhas(Ascendente: Boolean);
var
  Valor: Double;
  i, k: Integer;
begin
  k := QrCheque7Controle.Value;
  if DBG1.SelectedRows.Count = 0 then Valor := QrCheque7Valor.Value else
  begin
    Valor := 0;
    with DBG1.DataSource.DataSet do
    begin
      if Ascendente then
      begin
        for i:= 0 to DBG1.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          Valor := Valor + QrCheque7Valor.Value;
        end;
      end else begin
        for i:= DBG1.SelectedRows.Count-1 downto 0 do
        begin
          GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          Valor := Valor + QrCheque7Valor.Value;
        end;
      end;
    end;
    QrCheque7.Locate('Controle', k, []);
  end;
  EdSoma.Text := Geral.FFT(Valor, 2, siNegativo);
  EdSoma2.Text := EdSoma.Text;
end;

procedure TFmLotesRepCliMul.Timer1Timer(Sender: TObject);
begin
  SomaLinhas(FAscendente);
end;

procedure TFmLotesRepCliMul.DBG1CellClick(Column: TColumn);
begin
  SomaLinhas(True);
end;

procedure TFmLotesRepCliMul.DBG1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_DOWN) and (Shift=[ssShift]) then PreparaSoma(True)  else
  if (Key=VK_UP)   and (Shift=[ssShift]) then PreparaSoma(False) else
  if (Key=VK_DOWN)                       then PreparaSoma(True)  else
  if (Key=VK_UP)                         then PreparaSoma(False);
end;

procedure TFmLotesRepCliMul.FormCreate(Sender: TObject);
begin
  case FmPrincipal.FFormLotesShow of
    {
    0:
    begin
      EdFalta0.DataSource := FmLotes0.DsLotes;
      EdFalta2.DataSource := FmLotes0.DsLotes;
    end;
    }
    1:
    begin
      EdFalta0.DataSource := FmLotes1.DsLotes;
      EdFalta2.DataSource := FmLotes1.DsLotes;
    end;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (14)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  ReopenCheque7(0);
  // Marcelo 070310
  ReopenChequeM;
  ReopenCaixa;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmLotesRepCliMul.ExecutaSelecionados(Acao: Integer);
var
  i : Integer;
begin
  case PageControl1.ActivePageIndex of
    0:
    begin
      with DBG1.DataSource.DataSet do
      for i:= 0 to DBG1.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
        ExecutaAtual(Acao);
      end;
    end;
    1:
    begin
      with DBG2.DataSource.DataSet do
      for i:= 0 to DBG2.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBG2.SelectedRows.Items[i]));
        ExecutaAtual(Acao);
      end;
    end;
    2:
    begin
      with DBG3.DataSource.DataSet do
      for i:= 0 to DBG3.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBG3.SelectedRows.Items[i]));
        ExecutaAtual(Acao);
      end;
    end;
  end;
end;

procedure TFmLotesRepCliMul.ExecutaAtual(Acao: Integer);
begin
  Dmod.QrUpdM.SQL.Clear;
  case acao of
    0:
    begin
      Dmod.QrUpdM.SQL.Add('UPDATE lotesits SET AlterWeb=1,   Depositado=1 WHERE Controle=:P0 ');
      case PageControl1.ActivePageIndex of
        0: Dmod.QrUpdM.Params[0].AsInteger := QrCheque7Controle.Value;
        1: Dmod.QrUpdM.Params[0].AsInteger := QrChequeMControle.Value;
        2: Dmod.QrUpdM.Params[0].AsInteger := QrChequeVControle.Value;
      end;
    end;
    1:
    begin
      Dmod.QrUpdM.SQL.Add('UPDATE lotesits SET AlterWeb=1,   Depositado=0, RepCli=:P0 WHERE Controle=:P1 ');
      Dmod.QrUpdM.Params[0].AsInteger := FRepCli;
      case PageControl1.ActivePageIndex of
        0: Dmod.QrUpdM.Params[1].AsInteger := QrCheque7Controle.Value;
        1: Dmod.QrUpdM.Params[1].AsInteger := QrChequeMControle.Value;
        2: Dmod.QrUpdM.Params[1].AsInteger := QrChequeVControle.Value;
      end;
    end;
  end;
  //
  case PageControl1.ActivePageIndex of
    0: if QrCheque7.RecordCount > 0 then Dmod.QrUpdM.ExecSQL;
    1: if QrChequeM.RecordCount > 0 then Dmod.QrUpdM.ExecSQL;
    2: if QrChequeV.RecordCount > 0 then Dmod.QrUpdM.ExecSQL;
  end;
end;

procedure TFmLotesRepCliMul.Executa(Acao: Integer);
var
  Itens: Integer;
begin
  Itens := 0;
  Screen.Cursor := crHourGlass;
  case PageControl1.ActivePageIndex of
    0: itens := DBG1.SelectedRows.Count;
    1: itens := DBG2.SelectedRows.Count;
    2: itens := DBG3.SelectedRows.Count;
  end;
  if Itens = 0 then
    ExecutaAtual(Acao)
  else
    ExecutaSelecionados(Acao);
  case PageControl1.ActivePageIndex of
    0: ReopenCheque7(0);
    1: ReopenChequeM;
    2:
    begin
      EdValorV.Text := '0,00';
      ReopenChequeV(False);
    end;
  end;
  ReopenCaixa;
  if Acao = 1 then
  begin
    case FmPrincipal.FFormLotesShow of
      {
      0:
      begin
        FmLotes0.CalculaLote(FRepCli, False);
        FmLotes0.FMudouSaldo := True;
        FmLotes0.LocCod(FRepCli, FRepCli);
      end;
      }
      1:
      begin
        FmLotes1.CalculaLote(FRepCli, False);
        FmLotes1.FMudouSaldo := True;
        FmLotes1.LocCod(FRepCli, FRepCli);
      end;
      else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
      'n�o definida! (10)'), 'ERRO', MB_OK+MB_ICONERROR);
    end;
  end;
  EdBanda.Text := '';
  case PageControl1.ActivePageIndex of
    0: EdBanda.SetFocus;
    2: EdValorV.SetFocus;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmLotesRepCliMul.BitBtn1Click(Sender: TObject);
var
  MyCursor: TCursor;
begin
  MyCursor := Screen.Cursor;
  Screen.Cursor := crHourGlass;
  try
    Application.CreateForm(TFmLotesDep, FmLotesDep);
    FmLotesDep.ShowModal;
    FmLotesDep.Destroy;
    Screen.Cursor := MyCursor
  except
    Screen.Cursor := MyCursor;
    raise;
  end;
end;

procedure TFmLotesRepCliMul.BtOKClick(Sender: TObject);
begin
  Executa(1);
end;

procedure TFmLotesRepCliMul.EdSomaChange(Sender: TObject);
begin
  CalculaSaldo;
end;

procedure TFmLotesRepCliMul.EdFalta0Change(Sender: TObject);
begin
  CalculaSaldo;
end;

procedure TFmLotesRepCliMul.CalculaSaldo;
begin
  EdSaldo.Text := Geral.FFT(Geral.DMV(EdSoma.Text) - Geral.DMV(
    EdFalta0.Text), 2, siNegativo);
  EdSaldo2.Text := EdSaldo.Text;
end;

procedure TFmLotesRepCliMul.EdBandaChange(Sender: TObject);
begin
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
    EdCMC_7_1.Text := Geral.SoNumero_TT(EdBanda.Text);
end;

procedure TFmLotesRepCliMul.EdCMC_7_1Change(Sender: TObject);
var
  Banda: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC_7_1.Text) = 0 then
  begin
    Banda := TBandaMagnetica.Create;
    Banda.BandaMagnetica := EdCMC_7_1.Text;
    //EdComp.Text    := Banda.Compe;
    EdBanco.Text   := Banda.Banco;
    EdAgencia.Text := Banda.Agencia;
    EdConta.Text   := Banda.Conta;
    EdCheque.Text  := Banda.Numero;
    //
    if QrCheque7.Locate('Banco;Agencia;Conta;Cheque', VarArrayOf([
    EdBanco.Text, EdAgencia.Text, EdConta.Text, EdCheque.Text]), []) then
    begin
      EdValor.Text := Geral.FFT(QrCheque7Valor.Value, 2, siPositivo);
      EdVencto.Text := Geral.FDT(QrCheque7Vencto.Value, 2);
      EdEmitente2.text := QrCheque7Emitente.Value;
      EdCPF_2.Text := MLAGeral.FormataCNPJ_TFT(QrCheque7CPF.Value);
    end else Application.MessageBox('Cheque n�o localizado!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    //Pesquisa(False, False);
  end;
end;

procedure TFmLotesRepCliMul.BitBtn2Click(Sender: TObject);
var
  Sobra: String;
begin
  Sobra := EdFalta0.Text;
  if InputQuery('Sobra de Border�', 'Informe o valor da sobra:', Sobra) then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lotes SET AlterWeb=1,  SobraNow=:P0 WHERE Codigo=:P1');
    Dmod.QrUpd.Params[00].AsFloat   := Geral.DMV(Sobra);
    Dmod.QrUpd.Params[01].AsInteger := FRepCli;
    Dmod.QrUpd.ExecSQL;
    //
    case FmPrincipal.FFormLotesShow of
      {
      0:
      begin
        FmLotes0.CalculaLote(FRepCli, False);
        FmLotes0.LocCod(FRepCli, FRepCli);
      end;
      }
      1:
      begin
        FmLotes1.CalculaLote(FRepCli, False);
        FmLotes1.LocCod(FRepCli, FRepCli);
      end;
      else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
      'n�o definida! (11)'), 'ERRO', MB_OK+MB_ICONERROR);
    end;
  end;
end;

procedure TFmLotesRepCliMul.ReopenCheque7(Controle: Integer);
const
  NomeA: array[0..5] of string = ('DDeposito','Valor','Cheque','Banco , Agencia , Conta ','Emitente','NOMECLI');
  NomeD: array[0..5] of string = ('DDeposito DESC','Cheque DESC','Valor DESC','Banco DESC, Agencia DESC, Conta DESC','Emitente','NOMECLI');
var
  Ordem: String;
begin
  Ordem := 'ORDER BY ';
  if CkDescende1.Checked then
    Ordem := Ordem + NomeD[RGOrdem1.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem1.ItemIndex]+', ';
  if CkDescende2.Checked then
    Ordem := Ordem + NomeD[RGOrdem2.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem2.ItemIndex]+', ';
  if CkDescende3.Checked then
    Ordem := Ordem + NomeD[RGOrdem3.ItemIndex]
  else
    Ordem := Ordem + NomeA[RGOrdem3.ItemIndex];
  //
  QrCheque7.Close;
  QrCheque7.SQL.Clear;
  QrCheque7.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrCheque7.SQL.Add('ELSE ent.Nome END NOMECLI, lot.Lote, loi.*');
  QrCheque7.SQL.Add('FROM lotesits loi');
  QrCheque7.SQL.Add('LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo');
  QrCheque7.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente');
  QrCheque7.SQL.Add('WHERE lot.Tipo = 0');
  QrCheque7.SQL.Add('AND loi.RepCli = 0');
  QrCheque7.SQL.Add('AND loi.Repassado = 0');
  QrCheque7.SQL.Add('AND loi.Devolucao = 0');
  QrCheque7.SQL.Add('AND loi.Depositado = 0');
  QrCheque7.SQL.Add('');
  QrCheque7.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', 0, Date, False, True));
  //
  QrCheque7.SQL.Add(Ordem);
  QrCheque7.Open;
end;

procedure TFmLotesRepCliMul.ReopenChequeM;
const
  NomeA: array[0..5] of string = ('DDeposito','Valor','Cheque','Banco , Agencia , Conta ','Emitente','NOMECLI');
  NomeD: array[0..5] of string = ('DDeposito DESC','Cheque DESC','Valor DESC','Banco DESC, Agencia DESC, Conta DESC','Emitente','NOMECLI');
var
  Ordem: String;
  Controle, Banco, Agencia, Cheque: Integer;
  Conta, Emit, CPF: String;
  Valor: Double;
  Vcto: TDateTime;
begin
  if QrChequeM.State = dsBrowse then
    Controle := QrChequeMControle.Value
  else
    Controle := 0;
  Ordem := 'ORDER BY ';
  if CkDescende1.Checked then
    Ordem := Ordem + NomeD[RGOrdem1.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem1.ItemIndex]+', ';
  if CkDescende2.Checked then
    Ordem := Ordem + NomeD[RGOrdem2.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem2.ItemIndex]+', ';
  if CkDescende3.Checked then
    Ordem := Ordem + NomeD[RGOrdem3.ItemIndex]
  else
    Ordem := Ordem + NomeA[RGOrdem3.ItemIndex];
  //
  QrChequeM.Close;
  QrChequeM.SQL.Clear;
  QrChequeM.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrChequeM.SQL.Add('ELSE ent.Nome END NOMECLI, lot.Lote, loi.*');
  QrChequeM.SQL.Add('FROM lotesits loi');
  QrChequeM.SQL.Add('LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo');
  QrChequeM.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente');
  QrChequeM.SQL.Add('WHERE lot.Tipo = 0');
  QrChequeM.SQL.Add('AND loi.RepCli = 0');
  QrChequeM.SQL.Add('AND loi.Repassado = 0');
  QrChequeM.SQL.Add('AND loi.Devolucao = 0');
  if not CkDepositados.Checked then
    QrChequeM.SQL.Add('AND loi.Depositado = 0');

  Banco    := Geral.IMV(EdBancoM.Text);
  Agencia  := Geral.IMV(EdAgenciaM.Text);
  Conta    :=              EdContaM.Text;
  Cheque   := Geral.IMV(EdChequeM.Text);
  Valor    := Geral.DMV(EdValorM.Text);
  Vcto     := Geral.ValidaDataSimples(EdVctoM.Text, True);
  Emit     :=              EdEmitM.Text;
  CPF      :=              Geral.SoNumero_TT(EdCPFM.Text);

  if Banco > 0 then
    QrChequeM.SQL.Add('AND loi.Banco='+IntToStr(Banco));

  if Agencia > 0 then
    QrChequeM.SQL.Add('AND loi.Agencia='+IntToStr(Agencia));

  if Conta <> '0000000000' then
    QrChequeM.SQL.Add('AND loi.Conta="'+Conta+'"');

  if Cheque > 0 then
    QrChequeM.SQL.Add('AND loi.Cheque='+IntToStr(Cheque));

  if Valor > 0 then
  begin
    QrChequeM.SQL.Add('AND loi.Valor=:PVal');
    QrChequeM.ParamByName('PVal').AsFloat := Valor;
  end;

  if Vcto > 0 then
  begin
    QrChequeM.SQL.Add('AND loi.Vencto=:PVct');
    QrChequeM.ParamByName('PVct').AsString := Geral.FDT(Vcto, 1);
  end;
  if Emit <> '' then
    QrChequeM.SQL.Add('AND loi.Emitente LIKE "%'+Emit+'%"');

  if CPF <> '' then
    QrChequeM.SQL.Add('AND loi.CPF="'+CPF+'"');

  QrChequeM.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', 0, Date, False, True));
  QrChequeM.SQL.Add('');
  //
  QrChequeM.SQL.Add(Ordem);
  QrChequeM.Open;
  if Controle > 0 then QrChequeM.Locate('Controle', Controle, []);
end;

procedure TFmLotesRepCliMul.ReopenChequeV(Automatico: Boolean);
const
  NomeA: array[0..5] of string = ('Vencto','Valor','Cheque','Banco , Agencia , Conta ','Emitente','NOMECLI');
  NomeD: array[0..5] of string = ('Vencto DESC','Cheque DESC','Valor DESC','Banco DESC, Agencia DESC, Conta DESC','Emitente','NOMECLI');
var
  Ordem: String;
  Controle: Integer;
  Valor: Double;
begin
  if QrChequeV.State = dsBrowse then
    Controle := QrChequeVControle.Value
  else
    Controle := 0;
  Ordem := 'ORDER BY ';
  if CkDescende1.Checked then
    Ordem := Ordem + NomeD[RGOrdem1.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem1.ItemIndex]+', ';
  if CkDescende2.Checked then
    Ordem := Ordem + NomeD[RGOrdem2.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem2.ItemIndex]+', ';
  if CkDescende3.Checked then
    Ordem := Ordem + NomeD[RGOrdem3.ItemIndex]
  else
    Ordem := Ordem + NomeA[RGOrdem3.ItemIndex];
  //
  QrChequeV.Close;
  QrChequeV.SQL.Clear;
  QrChequeV.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrChequeV.SQL.Add('ELSE ent.Nome END NOMECLI, lot.Lote, loi.*');
  QrChequeV.SQL.Add('FROM lotesits loi');
  QrChequeV.SQL.Add('LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo');
  QrChequeV.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente');
  QrChequeV.SQL.Add('WHERE lot.Tipo = 0');
  QrChequeV.SQL.Add('AND loi.RepCli = 0');
  QrChequeV.SQL.Add('AND loi.Repassado = 0');
  QrChequeV.SQL.Add('AND loi.Devolucao = 0');
  QrChequeV.SQL.Add('AND loi.Depositado = 0');
  Valor    := Geral.DMV(EdValorV.Text);
    QrChequeV.SQL.Add('AND loi.Valor=:PVal');
    QrChequeV.ParamByName('PVal').AsFloat := Valor;
  QrChequeV.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', 0, Date, False, True));
  QrChequeV.SQL.Add('');
  //
  QrChequeV.SQL.Add(Ordem);
  QrChequeV.Open;
  if Controle > 0 then QrChequeV.Locate('Controle', Controle, []);
  if (QrChequeV.RecordCount = 1) and CkAuto.Checked and Automatico then
  begin
    Executa(1);
    LaAvisos.Caption := 'R$ '+Geral.FFT(QrChequeVValor.Value, 2, siPositivo) +
      ' localizado e lan�ado';
    LaAvisos.Font.Color := clBlue;
    EdValorV.SetFocus;
  end else begin
    LaAvisos.Caption := 'Encontrado '+IntToStr(QrChequeV.RecordCount) +
      ' registro(s)';
    LaAvisos.Font.Color := clRed;
    Beep;
  end;
end;

procedure TFmLotesRepCliMul.ReopenCaixa;
begin
  QrCaixa.Close;
  QrCaixa.Params[0].AsString := Geral.FDT(Date, 1);
  QrCaixa.Open;
end;

procedure TFmLotesRepCliMul.EdBancoMExit(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLotesRepCliMul.EdAgenciaMExit(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLotesRepCliMul.EdContaMExit(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLotesRepCliMul.EdChequeMExit(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLotesRepCliMul.EdValorMExit(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLotesRepCliMul.EdVctoMExit(Sender: TObject);
var
  Data: TDateTime;
begin
  Data := Geral.ValidaDataSimples(EdVctoM.Text, True);
  if Data > 0 then
    EdVctoM.Text := FormatDateTime('dd/mm/yyyy', Data)
  else
    EdVctoM.Text := '000000';
  ReopenChequeM;
end;

procedure TFmLotesRepCliMul.EdCPFMExit(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLotesRepCliMul.EdEmitMExit(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLotesRepCliMul.CkDepositadosClick(Sender: TObject);
begin
  ReopenChequeM;
end;

procedure TFmLotesRepCliMul.EdValorVExit(Sender: TObject);
begin
  ReopenChequeV(True);
end;

end.

