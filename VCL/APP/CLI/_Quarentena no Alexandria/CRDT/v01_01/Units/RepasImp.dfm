object FmRepasImp: TFmRepasImp
  Left = 419
  Top = 217
  Caption = 'Impress'#227'o de Operac'#245'es'
  ClientHeight = 413
  ClientWidth = 597
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 125
    Width = 597
    Height = 240
    Align = alBottom
    TabOrder = 0
    object Label3: TLabel
      Left = 8
      Top = 8
      Width = 44
      Height = 13
      Caption = 'Coligado:'
    end
    object EdColigado: TdmkEditCB
      Left = 8
      Top = 24
      Width = 57
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBColigado
      IgnoraDBLookupComboBox = False
    end
    object CBColigado: TdmkDBLookupComboBox
      Left = 68
      Top = 24
      Width = 521
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NOMECOLIGADO'
      ListSource = DsColigados
      TabOrder = 1
      dmkEditCB = EdColigado
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object RGOrdem1: TRadioGroup
      Left = 8
      Top = 48
      Width = 143
      Height = 141
      Caption = ' Ordem 1: '
      ItemIndex = 0
      Items.Strings = (
        'Coligado'
        'Dia Dep'#243'sito'
        'M'#234's Dep'#243'sito'
        'Emitente'
        'CPF/CNPJ'
        'Border'#244)
      TabOrder = 2
    end
    object RGOrdem2: TRadioGroup
      Left = 154
      Top = 48
      Width = 143
      Height = 141
      Caption = ' Ordem 2: '
      ItemIndex = 1
      Items.Strings = (
        'Coligado'
        'Dia Dep'#243'sito'
        'M'#234's Dep'#243'sito'
        'Emitente'
        'CPF/CNPJ'
        'Border'#244)
      TabOrder = 3
    end
    object RGOrdem3: TRadioGroup
      Left = 300
      Top = 48
      Width = 143
      Height = 141
      Caption = ' Ordem 3: '
      ItemIndex = 2
      Items.Strings = (
        'Coligado'
        'Dia Dep'#243'sito'
        'M'#234's Dep'#243'sito'
        'Emitente'
        'CPF/CNPJ'
        'Border'#244)
      TabOrder = 4
    end
    object RGOrdem4: TRadioGroup
      Left = 446
      Top = 48
      Width = 143
      Height = 141
      Caption = ' Ordem 4: '
      ItemIndex = 3
      Items.Strings = (
        'Coligado'
        'Dia Dep'#243'sito'
        'M'#234's Dep'#243'sito'
        'Emitente'
        'CPF/CNPJ'
        'Border'#244)
      TabOrder = 5
    end
    object RGAgrupa: TRadioGroup
      Left = 8
      Top = 192
      Width = 121
      Height = 41
      Caption = ' Agrupamentos: '
      Columns = 4
      ItemIndex = 0
      Items.Strings = (
        '0'
        '1'
        '2'
        '3')
      TabOrder = 6
    end
    object RGRepasses: TRadioGroup
      Left = 127
      Top = 192
      Width = 226
      Height = 41
      Caption = ' Repasses: '
      Columns = 3
      ItemIndex = 2
      Items.Strings = (
        'Cheques'
        'Duplicatas'
        'Ambos')
      TabOrder = 7
    end
    object RGTipo: TRadioGroup
      Left = 351
      Top = 192
      Width = 142
      Height = 41
      Caption = ' Tipo de relat'#243'rio: '
      Columns = 2
      ItemIndex = 1
      Items.Strings = (
        'Anal'#237'tico'
        'Sint'#233'tico')
      TabOrder = 8
    end
    object RGMediaDias: TRadioGroup
      Left = 491
      Top = 192
      Width = 98
      Height = 41
      Caption = ' M'#233'dia de dias: '
      Columns = 2
      ItemIndex = 0
      Items.Strings = (
        'N'#227'o'
        'Sim')
      TabOrder = 9
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 365
    Width = 597
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtImprime: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtImprimeClick
    end
    object BtSaida: TBitBtn
      Tag = 15
      Left = 490
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 597
    Height = 48
    Align = alTop
    Caption = 'Impress'#227'o de Operac'#245'es'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 595
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 593
      ExplicitHeight = 44
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 597
    Height = 77
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 3
    OnChange = PageControl1Change
    object TabSheet1: TTabSheet
      Caption = 'Dep'#243'sitos'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label34: TLabel
        Left = 384
        Top = 8
        Width = 98
        Height = 13
        Caption = 'Data dep'#243'sito inicial:'
      end
      object Label1: TLabel
        Left = 488
        Top = 8
        Width = 91
        Height = 13
        Caption = 'Data dep'#243'sito final:'
      end
      object TPIniD: TDateTimePicker
        Left = 384
        Top = 24
        Width = 101
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 0
      end
      object TPFimD: TDateTimePicker
        Left = 488
        Top = 24
        Width = 101
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 1
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Repasses'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Label2: TLabel
        Left = 384
        Top = 8
        Width = 95
        Height = 13
        Caption = 'Data repasse inicial:'
      end
      object Label4: TLabel
        Left = 488
        Top = 8
        Width = 88
        Height = 13
        Caption = 'Data repasse final:'
      end
      object TPIniR: TDateTimePicker
        Left = 384
        Top = 24
        Width = 101
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 0
      end
      object TPFimR: TDateTimePicker
        Left = 488
        Top = 24
        Width = 101
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 1
      end
    end
  end
  object QrColigados: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECOLIGADO, Codigo'
      'FROM entidades'
      'WHERE Fornece2='#39'V'#39
      'ORDER BY NOMECOLIGADO')
    Left = 292
    Top = 64
    object QrColigadosNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
    object QrColigadosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsColigados: TDataSource
    DataSet = QrColigados
    Left = 316
    Top = 64
  end
  object QrRepasIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrRepasItsCalcFields
    SQL.Strings = (
      'SELECT MONTH(re.Data)+YEAR(re.Data)*100+0.00 PERIODOR,'
      'MONTH(li.DDeposito)+YEAR(li.DDeposito)*100+0.00 PERIODOD,'
      'CASE WHEN co.Tipo=0 THEN co.RazaoSocial'
      'ELSE co.Nome END NOMECOLIGADO, re.Coligado, re.Data DATA_REP, '
      'li.Banco, li.Agencia, li.Conta, li.Cheque, li.Duplicata,'
      'li.Emitente, li.CPF, li.DDeposito, li.Vencto,'
      '(ri.Valor - ri.JurosV) LIQUIDO, lo.Tipo, ri.*'
      'FROM repasits ri'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN lotesits  li ON li.Controle=ri.Origem'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN entidades co ON co.Codigo=re.Coligado'
      'ORDER BY PERIODOR, re.Data, Emitente')
    Left = 112
    Top = 64
    object QrRepasItsNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
    object QrRepasItsColigado: TIntegerField
      FieldName = 'Coligado'
    end
    object QrRepasItsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrRepasItsAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrRepasItsConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrRepasItsCheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrRepasItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrRepasItsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrRepasItsDDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrRepasItsVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrRepasItsLIQUIDO: TFloatField
      FieldName = 'LIQUIDO'
      Required = True
    end
    object QrRepasItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRepasItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrRepasItsOrigem: TIntegerField
      FieldName = 'Origem'
      Required = True
    end
    object QrRepasItsDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrRepasItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrRepasItsTaxa: TFloatField
      FieldName = 'Taxa'
      Required = True
    end
    object QrRepasItsJurosP: TFloatField
      FieldName = 'JurosP'
      Required = True
    end
    object QrRepasItsJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
    end
    object QrRepasItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrRepasItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrRepasItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrRepasItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrRepasItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrRepasItsCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrRepasItsDDeposito_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DDeposito_TXT'
      Size = 30
      Calculated = True
    end
    object QrRepasItsValor_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Valor_TXT'
      Size = 30
      Calculated = True
    end
    object QrRepasItsCONTAGEM: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CONTAGEM'
      Calculated = True
    end
    object QrRepasItsMESD_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MESD_TXT'
      Size = 40
      Calculated = True
    end
    object QrRepasItsMESR_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MESR_TXT'
      Size = 40
      Calculated = True
    end
    object QrRepasItsDOCUMENTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOCUMENTO'
      Size = 100
      Calculated = True
    end
    object QrRepasItsTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrRepasItsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrRepasItsDATA_REP: TDateField
      FieldName = 'DATA_REP'
    end
    object QrRepasItsDATA_REP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA_REP_TXT'
      Size = 50
      Calculated = True
    end
    object QrRepasItsPERIODOD: TFloatField
      FieldName = 'PERIODOD'
    end
    object QrRepasItsPERIODOR: TFloatField
      FieldName = 'PERIODOR'
    end
    object QrRepasItsPERIODO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PERIODO'
      Calculated = True
    end
    object QrRepasItsDATADEF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATADEF_TXT'
      Size = 40
      Calculated = True
    end
    object QrRepasItsVALDIAS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALDIAS'
      Calculated = True
    end
    object QrRepasItslote: TIntegerField
      FieldName = 'lote'
    end
    object QrRepasItsNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
  end
  object frxRepasCH: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.003585428200000000
    ReportOptions.LastChange = 39718.003585428200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_ORD1> > 0 then GH1.Visible := True else GH1.Visible :=' +
        ' False;'
      
        '  if <VFR_ORD1> > 0 then GF1.Visible := True else GF1.Visible :=' +
        ' False;'
      '  //'
      
        '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsRepasIts."NOMECO' +
        'LIGADO"'#39';'
      
        '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsRepasIts."DATADE' +
        'F_TXT"'#39';'
      
        '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsRepasIts."PERIOD' +
        'O"'#39';'
      
        '  if <VFR_ORD1> = 4 then GH1.Condition := '#39'frxDsRepasIts."Emiten' +
        'te"'#39';'
      '  if <VFR_ORD1> = 5 then GH1.Condition := '#39'frxDsRepasIts."CPF"'#39';'
      
        '  if <VFR_ORD1> = 6 then GH1.Condition := '#39'frxDsRepasIts."Codigo' +
        '"'#39';'
      ''
      '    '
      
        '  if <VFR_ORD2> > 0 then GH2.Visible := True else GH2.Visible :=' +
        ' False;'
      
        '  if <VFR_ORD2> > 0 then GF2.Visible := True else GF2.Visible :=' +
        ' False;'
      '  //'
      
        '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsRepasIts."NOMECO' +
        'LIGADO"'#39';'
      
        '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsRepasIts."DATADE' +
        'F_TXT"'#39';'
      
        '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsRepasIts."PERIOD' +
        'O"'#39';'
      
        '  if <VFR_ORD2> = 4 then GH2.Condition := '#39'frxDsRepasIts."Emiten' +
        'te"'#39';'
      '  if <VFR_ORD2> = 5 then GH2.Condition := '#39'frxDsRepasIts."CPF"'#39';'
      
        '  if <VFR_ORD2> = 6 then GH2.Condition := '#39'frxDsRepasIts."Codigo' +
        '"'#39';'
      ''
      ''
      
        '  if <VFR_ORD3> > 0 then GH3.Visible := True else GH3.Visible :=' +
        ' False;'
      
        '  if <VFR_ORD3> > 0 then GF3.Visible := True else GF3.Visible :=' +
        ' False;'
      '  //'
      
        '  if <VFR_ORD3> = 1 then GH3.Condition := '#39'frxDsRepasIts."NOMECO' +
        'LIGADO"'#39';'
      
        '  if <VFR_ORD3> = 2 then GH3.Condition := '#39'frxDsRepasIts."DATADE' +
        'F_TXT"'#39';'
      
        '  if <VFR_ORD3> = 3 then GH3.Condition := '#39'frxDsRepasIts."PERIOD' +
        'O"'#39';'
      
        '  if <VFR_ORD3> = 4 then GH3.Condition := '#39'frxDsRepasIts."Emiten' +
        'te"'#39';'
      '  if <VFR_ORD3> = 5 then GH3.Condition := '#39'frxDsRepasIts."CPF"'#39';'
      
        '  if <VFR_ORD3> = 6 then GH3.Condition := '#39'frxDsRepasIts."Codigo' +
        '"'#39';'
      '        '
      'end.')
    OnGetValue = frxRepasCHGetValue
    Left = 224
    Top = 64
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsRepasIts
        DataSetName = 'frxDsRepasIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 737.008350000000000000
        object Memo4: TfrxMemoView
          Left = 2.000000000000000000
          Top = 13.102350000000000000
          Width = 716.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_COLIGADO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 554.000000000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 638.740570000000000000
        Width = 737.008350000000000000
        object Memo31: TfrxMemoView
          Left = 574.440940000000000000
          Top = 3.873529999999960000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 662.440940000000000000
          Top = 3.873529999999960000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 72.732220000000000000
        Top = 98.267780000000000000
        Width = 737.008350000000000000
        object Memo5: TfrxMemoView
          Left = 10.000000000000000000
          Top = 25.732220000000000000
          Width = 448.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_TODOS]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 522.000000000000000000
          Top = 25.732220000000000000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PERIODO_D]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 474.000000000000000000
          Top = 25.732220000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 2.000000000000000000
          Top = 55.732220000000000000
          Width = 24.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bco.')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 26.000000000000000000
          Top = 55.732220000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag.')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 54.000000000000000000
          Top = 55.732220000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Conta')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 114.000000000000000000
          Top = 55.732220000000000000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Cheq.')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 154.000000000000000000
          Top = 55.732220000000000000
          Width = 128.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Emitente')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 282.000000000000000000
          Top = 55.732220000000000000
          Width = 92.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 374.000000000000000000
          Top = 55.732220000000000000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 450.000000000000000000
          Top = 55.732220000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 498.000000000000000000
          Top = 55.732220000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dep'#243'sito')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 574.000000000000000000
          Top = 55.732220000000000000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Juros')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 646.000000000000000000
          Top = 55.732220000000000000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ L'#237'quido')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 546.000000000000000000
          Top = 55.732220000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 546.000000000000000000
          Top = 55.732220000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo60OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Dias')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 10.000000000000000000
          Top = 0.629869999999996800
          Width = 700.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[TITULO]')
          ParentFont = False
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        Height = 17.000000000000000000
        Top = 355.275820000000000000
        Width = 737.008350000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsRepasIts
        DataSetName = 'frxDsRepasIts'
        RowCount = 0
        object Memo2: TfrxMemoView
          Left = 2.000000000000000000
          Width = 24.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsRepasIts."Banco">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'0000'#39', <frxDsRepasIts."Agencia">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 54.000000000000000000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepasIts."Conta"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 114.000000000000000000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000'#39', <frxDsRepasIts."Cheque">)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 154.000000000000000000
          Width = 128.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsRepasIts."Emitente"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 282.000000000000000000
          Width = 92.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsRepasIts."CPF_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 374.000000000000000000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."Valor"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 450.000000000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepasIts."Vencto"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 498.000000000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepasIts."DDeposito"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 574.000000000000000000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."JurosV"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 646.000000000000000000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."LIQUIDO"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 546.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 546.000000000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo61OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."Dias"]')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 230.551330000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsRepasIts."NOMECOLIGADO"'
        object Memo29: TfrxMemoView
          Left = 18.000000000000000000
          Width = 692.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 272.126160000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsRepasIts."PERIODOD"'
        object Memo30: TfrxMemoView
          Left = 38.000000000000000000
          Width = 672.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GH3: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 313.700990000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GH3OnBeforePrint'
        Condition = 'frxDsRepasIts."PERIODOR"'
        object Memo33: TfrxMemoView
          Left = 58.000000000000000000
          Width = 652.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA3NOME]')
          ParentFont = False
        end
      end
      object GF3: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 396.850650000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GF3OnBeforePrint'
        object Memo34: TfrxMemoView
          Left = 2.000000000000000000
          Top = 3.779527559055112000
          Width = 372.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsRepasIts."CONTAGEM">)] cheque(s):')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 374.000000000000000000
          Top = 3.779527559055112000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 574.000000000000000000
          Top = 3.779527559055112000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 646.000000000000000000
          Top = 3.779527559055112000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 450.000000000000000000
          Top = 3.779527559055112000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 534.000000000000000000
          Top = 3.779527559055112000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo49OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsRepasIts."VALDIAS">) / SUM(<frxDsRepasIts."Valor">))' +
              ']')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 445.984540000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo37: TfrxMemoView
          Left = 2.000000000000000000
          Top = 3.779527559055112000
          Width = 372.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsRepasIts."CONTAGEM">)] cheque(s):')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 450.000000000000000000
          Top = 3.779527559055112000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 374.173470000000000000
          Top = 3.779527559055112000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 574.173470000000000000
          Top = 3.779527559055112000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 646.173470000000000000
          Top = 3.779527559055112000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 534.173470000000000000
          Top = 3.779527559055112000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo49OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsRepasIts."VALDIAS">) / SUM(<frxDsRepasIts."Valor">))' +
              ']')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 495.118430000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo44: TfrxMemoView
          Left = 2.000000000000000000
          Top = 3.779527560000000000
          Width = 372.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsRepasIts."CONTAGEM">)] cheque(s):')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 450.000000000000000000
          Top = 3.779527560000000000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 374.173470000000000000
          Top = 3.779527560000000000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 574.173470000000000000
          Top = 3.779527560000000000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 646.173470000000000000
          Top = 3.779527560000000000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 534.173470000000000000
          Top = 3.779527560000000000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo49OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsRepasIts."VALDIAS">) / SUM(<frxDsRepasIts."Valor">))' +
              ']')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 32.000000000000000000
        Top = 582.047620000000000000
        Width = 737.008350000000000000
        object Memo50: TfrxMemoView
          Left = 2.000000000000000000
          Top = 11.338582679999950000
          Width = 372.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total geral de [SUM(<frxDsRepasIts."CONTAGEM">)] cheque(s):')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 450.000000000000000000
          Top = 11.338582677165390000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 2.000000000000000000
          Top = 9.322509999999965000
          Width = 719.779530000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo38: TfrxMemoView
          Left = 374.173470000000000000
          Top = 11.338582677165390000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 574.173470000000000000
          Top = 11.338582677165390000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 646.173470000000000000
          Top = 11.338582677165390000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 534.173470000000000000
          Top = 11.338582677165390000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo49OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsRepasIts."VALDIAS">) / SUM(<frxDsRepasIts."Valor">))' +
              ']')
          ParentFont = False
        end
      end
    end
  end
  object frxDsRepasIts: TfrxDBDataset
    UserName = 'frxDsRepasIts'
    CloseDataSource = False
    DataSet = QrRepasIts
    BCDToCurrency = False
    Left = 252
    Top = 64
  end
  object frxRepasDU: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.003585428200000000
    ReportOptions.LastChange = 39718.003585428200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_ORD1> > 0 then GH1.Visible := True else GH1.Visible :=' +
        ' False;'
      
        '  if <VFR_ORD1> > 0 then GF1.Visible := True else GF1.Visible :=' +
        ' False;'
      '  //'
      
        '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsRepasIts."NOMECO' +
        'LIGADO"'#39';'
      
        '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsRepasIts."DATADE' +
        'F_TXT"'#39';'
      
        '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsRepasIts."PERIOD' +
        'O"'#39';'
      
        '  if <VFR_ORD1> = 4 then GH1.Condition := '#39'frxDsRepasIts."Emiten' +
        'te"'#39';'
      '  if <VFR_ORD1> = 5 then GH1.Condition := '#39'frxDsRepasIts."CPF"'#39';'
      
        '  if <VFR_ORD1> = 6 then GH1.Condition := '#39'frxDsRepasIts."Codigo' +
        '"'#39';'
      ''
      '    '
      
        '  if <VFR_ORD2> > 0 then GH2.Visible := True else GH2.Visible :=' +
        ' False;'
      
        '  if <VFR_ORD2> > 0 then GF2.Visible := True else GF2.Visible :=' +
        ' False;'
      '  //'
      
        '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsRepasIts."NOMECO' +
        'LIGADO"'#39';'
      
        '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsRepasIts."DATADE' +
        'F_TXT"'#39';'
      
        '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsRepasIts."PERIOD' +
        'O"'#39';'
      
        '  if <VFR_ORD2> = 4 then GH2.Condition := '#39'frxDsRepasIts."Emiten' +
        'te"'#39';'
      '  if <VFR_ORD2> = 5 then GH2.Condition := '#39'frxDsRepasIts."CPF"'#39';'
      
        '  if <VFR_ORD2> = 6 then GH2.Condition := '#39'frxDsRepasIts."Codigo' +
        '"'#39';'
      ''
      ''
      
        '  if <VFR_ORD3> > 0 then GH3.Visible := True else GH3.Visible :=' +
        ' False;'
      
        '  if <VFR_ORD3> > 0 then GF3.Visible := True else GF3.Visible :=' +
        ' False;'
      '  //'
      
        '  if <VFR_ORD3> = 1 then GH3.Condition := '#39'frxDsRepasIts."NOMECO' +
        'LIGADO"'#39';'
      
        '  if <VFR_ORD3> = 2 then GH3.Condition := '#39'frxDsRepasIts."DATADE' +
        'F_TXT"'#39';'
      
        '  if <VFR_ORD3> = 3 then GH3.Condition := '#39'frxDsRepasIts."PERIOD' +
        'O"'#39';'
      
        '  if <VFR_ORD3> = 4 then GH3.Condition := '#39'frxDsRepasIts."Emiten' +
        'te"'#39';'
      '  if <VFR_ORD3> = 5 then GH3.Condition := '#39'frxDsRepasIts."CPF"'#39';'
      
        '  if <VFR_ORD3> = 6 then GH3.Condition := '#39'frxDsRepasIts."Codigo' +
        '"'#39';'
      '        '
      'end.')
    OnGetValue = frxRepasCHGetValue
    Left = 196
    Top = 64
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsRepasIts
        DataSetName = 'frxDsRepasIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 737.008350000000000000
        object Memo4: TfrxMemoView
          Left = 2.000000000000000000
          Top = 13.102350000000000000
          Width = 716.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_COLIGADO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 554.000000000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 642.520100000000000000
        Width = 737.008350000000000000
        object Memo31: TfrxMemoView
          Left = 574.440940000000000000
          Top = 3.873529999999960000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 662.440940000000000000
          Top = 3.873529999999960000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 73.692950000000000000
        Top = 98.267780000000000000
        Width = 737.008350000000000000
        object Memo5: TfrxMemoView
          Left = 10.000000000000000000
          Top = 25.732220000000000000
          Width = 448.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_TODOS]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 522.000000000000000000
          Top = 25.732220000000000000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PERIODO_D]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 474.000000000000000000
          Top = 25.732220000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 10.000000000000000000
          Top = 0.629869999999996800
          Width = 700.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[TITULO]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 75.559060000000000000
          Top = 56.692950000000010000
          Width = 24.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bco.')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 99.559060000000000000
          Top = 56.692950000000010000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag.')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 7.559060000000000000
          Top = 56.692950000000010000
          Width = 68.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Duplicata')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 127.559060000000000000
          Top = 56.692950000000010000
          Width = 156.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sacado')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 283.559060000000000000
          Top = 56.692950000000010000
          Width = 92.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 375.559060000000000000
          Top = 56.692950000000010000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 451.559060000000000000
          Top = 56.692950000000010000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 499.559060000000000000
          Top = 56.692950000000010000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dep'#243'sito')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 575.559060000000000000
          Top = 56.692950000000010000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Juros')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 647.559060000000000000
          Top = 56.692950000000010000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ L'#237'quido')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 547.559060000000000000
          Top = 56.692950000000010000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 547.559060000000000000
          Top = 56.692950000000010000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo58OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Dias')
          ParentFont = False
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        Height = 17.000000000000000000
        Top = 359.055350000000000000
        Width = 737.008350000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsRepasIts
        DataSetName = 'frxDsRepasIts'
        RowCount = 0
        object Memo2: TfrxMemoView
          Left = 75.559060000000000000
          Width = 24.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsRepasIts."Banco">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 99.559060000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'0000'#39', <frxDsRepasIts."Agencia">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 7.559060000000000000
          Width = 68.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepasIts."Duplicata"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 127.559060000000000000
          Width = 156.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsRepasIts."Emitente"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 283.559060000000000000
          Width = 92.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsRepasIts."CPF_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 375.559060000000000000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."Valor"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 451.559060000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepasIts."Vencto"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 499.559060000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepasIts."DDeposito"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 575.559060000000000000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."JurosV"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 647.559060000000000000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."LIQUIDO"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 547.559060000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 547.559060000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo59OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."Dias"]')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 234.330860000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsRepasIts."NOMECOLIGADO"'
        object Memo29: TfrxMemoView
          Left = 18.000000000000000000
          Width = 692.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 275.905690000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsRepasIts."PERIODOD"'
        object Memo30: TfrxMemoView
          Left = 38.000000000000000000
          Width = 672.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GH3: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 317.480520000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GH3OnBeforePrint'
        Condition = 'frxDsRepasIts."PERIODOR"'
        object Memo33: TfrxMemoView
          Left = 58.000000000000000000
          Width = 652.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA3NOME]')
          ParentFont = False
        end
      end
      object GF3: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 400.630180000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GF3OnBeforePrint'
        object Memo34: TfrxMemoView
          Left = 2.000000000000000000
          Top = 3.779527559055112000
          Width = 372.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsRepasIts."CONTAGEM">)] duplicata(s):')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 374.000000000000000000
          Top = 3.779527559055112000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 574.000000000000000000
          Top = 3.779527559055112000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 646.000000000000000000
          Top = 3.779527559055112000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 450.000000000000000000
          Top = 3.779527559055112000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 534.000000000000000000
          Top = 3.779527559055112000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo49OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsRepasIts."VALDIAS">) / SUM(<frxDsRepasIts."Valor">))' +
              ']')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 449.764070000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo37: TfrxMemoView
          Left = 2.000000000000000000
          Top = 3.779527559055112000
          Width = 372.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsRepasIts."CONTAGEM">)] duplicata(s):')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 450.000000000000000000
          Top = 3.779527559055112000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 374.173470000000000000
          Top = 3.779527559055112000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 574.173470000000000000
          Top = 3.779527559055112000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 646.173470000000000000
          Top = 3.779527559055112000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 534.173470000000000000
          Top = 3.779527559055112000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo49OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsRepasIts."VALDIAS">) / SUM(<frxDsRepasIts."Valor">))' +
              ']')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 498.897960000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo44: TfrxMemoView
          Left = 2.000000000000000000
          Top = 3.779527559054998000
          Width = 372.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsRepasIts."CONTAGEM">)] duplicata(s):')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 450.000000000000000000
          Top = 3.779527559054998000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 374.173470000000000000
          Top = 3.779527559054998000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 574.173470000000000000
          Top = 3.779527559054998000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 646.173470000000000000
          Top = 3.779527559054998000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 534.173470000000000000
          Top = 3.779527559054998000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo49OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsRepasIts."VALDIAS">) / SUM(<frxDsRepasIts."Valor">))' +
              ']')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 32.000000000000000000
        Top = 585.827150000000000000
        Width = 737.008350000000000000
        object Memo50: TfrxMemoView
          Left = 2.000000000000000000
          Top = 11.338582679999950000
          Width = 372.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total geral de [SUM(<frxDsRepasIts."CONTAGEM">)] duplicata(s):')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 450.000000000000000000
          Top = 11.338582677165390000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 2.000000000000000000
          Top = 9.322509999999965000
          Width = 719.779530000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo38: TfrxMemoView
          Left = 374.173470000000000000
          Top = 11.338582677165390000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 574.173470000000000000
          Top = 11.338582677165390000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 646.173470000000000000
          Top = 11.338582677165390000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 534.173470000000000000
          Top = 11.338582677165390000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo49OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsRepasIts."VALDIAS">) / SUM(<frxDsRepasIts."Valor">))' +
              ']')
          ParentFont = False
        end
      end
    end
  end
  object frxRepasCD_Anal: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.003585428200000000
    ReportOptions.LastChange = 39718.003585428200000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VFR_ORD1> > 0 then GH1.Visible := True else GH1.Visible :=' +
        ' False;'
      
        '  if <VFR_ORD1> > 0 then GF1.Visible := True else GF1.Visible :=' +
        ' False;'
      '  //'
      
        '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsRepasIts."NOMECO' +
        'LIGADO"'#39';'
      
        '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsRepasIts."DATADE' +
        'F_TXT"'#39';'
      
        '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsRepasIts."PERIOD' +
        'O"'#39';'
      
        '  if <VFR_ORD1> = 4 then GH1.Condition := '#39'frxDsRepasIts."Emiten' +
        'te"'#39';'
      '  if <VFR_ORD1> = 5 then GH1.Condition := '#39'frxDsRepasIts."CPF"'#39';'
      
        '  if <VFR_ORD1> = 6 then GH1.Condition := '#39'frxDsRepasIts."Codigo' +
        '"'#39';'
      ''
      '    '
      
        '  if <VFR_ORD2> > 0 then GH2.Visible := True else GH2.Visible :=' +
        ' False;'
      
        '  if <VFR_ORD2> > 0 then GF2.Visible := True else GF2.Visible :=' +
        ' False;'
      '  //'
      
        '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsRepasIts."NOMECO' +
        'LIGADO"'#39';'
      
        '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsRepasIts."DATADE' +
        'F_TXT"'#39';'
      
        '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsRepasIts."PERIOD' +
        'O"'#39';'
      
        '  if <VFR_ORD2> = 4 then GH2.Condition := '#39'frxDsRepasIts."Emiten' +
        'te"'#39';'
      '  if <VFR_ORD2> = 5 then GH2.Condition := '#39'frxDsRepasIts."CPF"'#39';'
      
        '  if <VFR_ORD2> = 6 then GH2.Condition := '#39'frxDsRepasIts."Codigo' +
        '"'#39';'
      ''
      ''
      
        '  if <VFR_ORD3> > 0 then GH3.Visible := True else GH3.Visible :=' +
        ' False;'
      
        '  if <VFR_ORD3> > 0 then GF3.Visible := True else GF3.Visible :=' +
        ' False;'
      '  //'
      
        '  if <VFR_ORD3> = 1 then GH3.Condition := '#39'frxDsRepasIts."NOMECO' +
        'LIGADO"'#39';'
      
        '  if <VFR_ORD3> = 2 then GH3.Condition := '#39'frxDsRepasIts."DATADE' +
        'F_TXT"'#39';'
      
        '  if <VFR_ORD3> = 3 then GH3.Condition := '#39'frxDsRepasIts."PERIOD' +
        'O"'#39';'
      
        '  if <VFR_ORD3> = 4 then GH3.Condition := '#39'frxDsRepasIts."Emiten' +
        'te"'#39';'
      '  if <VFR_ORD3> = 5 then GH3.Condition := '#39'frxDsRepasIts."CPF"'#39';'
      
        '  if <VFR_ORD3> = 6 then GH3.Condition := '#39'frxDsRepasIts."Codigo' +
        '"'#39';'
      '        '
      'end.')
    OnGetValue = frxRepasCHGetValue
    Left = 168
    Top = 64
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsRepasIts
        DataSetName = 'frxDsRepasIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 737.008350000000000000
        object Memo4: TfrxMemoView
          Left = 2.000000000000000000
          Top = 13.102350000000000000
          Width = 716.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_COLIGADO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo32: TfrxMemoView
          Left = 554.000000000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 642.520100000000000000
        Width = 737.008350000000000000
        object Memo31: TfrxMemoView
          Left = 574.440940000000000000
          Top = 3.873529999999960000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 662.440940000000000000
          Top = 3.873529999999960000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 73.692950000000000000
        Top = 98.267780000000000000
        Width = 737.008350000000000000
        object Memo5: TfrxMemoView
          Left = 10.000000000000000000
          Top = 25.732220000000000000
          Width = 448.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_TODOS]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 522.000000000000000000
          Top = 25.732220000000000000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PERIODO_D]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 474.000000000000000000
          Top = 25.732220000000000000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 10.000000000000000000
          Top = 0.629869999999996800
          Width = 700.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[TITULO]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 7.559060000000000000
          Top = 56.692950000000010000
          Width = 152.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Documento')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 159.559060000000000000
          Top = 56.692950000000010000
          Width = 128.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sacado / Emitente')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 287.559060000000000000
          Top = 56.692950000000010000
          Width = 92.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 379.559060000000000000
          Top = 56.692950000000010000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 455.559060000000000000
          Top = 56.692950000000010000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 503.559060000000000000
          Top = 56.692950000000010000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dep'#243'sito')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 551.559060000000000000
          Top = 56.692950000000010000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 579.559060000000000000
          Top = 56.692950000000010000
          Width = 68.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Juros')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 647.559060000000000000
          Top = 56.692950000000010000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ L'#237'quido')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 551.559060000000000000
          Top = 56.692950000000010000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo16OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Dias')
          ParentFont = False
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        Height = 17.000000000000000000
        Top = 359.055350000000000000
        Width = 737.008350000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsRepasIts
        DataSetName = 'frxDsRepasIts'
        RowCount = 0
        object Memo8: TfrxMemoView
          Left = 7.559060000000000000
          Width = 152.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepasIts."DOCUMENTO"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 159.559060000000000000
          Width = 128.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsRepasIts."Emitente"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 287.559060000000000000
          Width = 92.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsRepasIts."CPF_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 379.559060000000000000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."Valor"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 455.559060000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepasIts."Vencto"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 503.559060000000000000
          Width = 48.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepasIts."DDeposito"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 551.559060000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 579.559060000000000000
          Width = 68.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."JurosV"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 647.559060000000000000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."LIQUIDO"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 551.559060000000000000
          Width = 28.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo18OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."Dias"]')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 234.330860000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsRepasIts."NOMECOLIGADO"'
        object Memo29: TfrxMemoView
          Left = 18.000000000000000000
          Width = 692.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 275.905690000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsRepasIts."PERIODOD"'
        object Memo30: TfrxMemoView
          Left = 38.000000000000000000
          Width = 672.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GH3: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 317.480520000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GH3OnBeforePrint'
        Condition = 'frxDsRepasIts."PERIODOR"'
        object Memo33: TfrxMemoView
          Left = 58.000000000000000000
          Width = 652.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA3NOME]')
          ParentFont = False
        end
      end
      object GF3: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 400.630180000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GF3OnBeforePrint'
        object Memo34: TfrxMemoView
          Left = 2.000000000000000000
          Top = 3.779527560000020000
          Width = 372.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsRepasIts."CONTAGEM">)] documento(s):')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 374.000000000000000000
          Top = 3.779527560000020000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 574.000000000000000000
          Top = 3.779527560000020000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 646.000000000000000000
          Top = 3.779527560000020000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 450.000000000000000000
          Top = 3.779527560000020000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 534.000000000000000000
          Top = 3.779527560000020000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo49OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsRepasIts."VALDIAS">) / SUM(<frxDsRepasIts."Valor">))' +
              ']')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 449.764070000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo37: TfrxMemoView
          Left = 2.000000000000000000
          Top = 3.779527560000020000
          Width = 372.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsRepasIts."CONTAGEM">)] documento(s):')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 450.000000000000000000
          Top = 3.779527560000020000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 374.173470000000000000
          Top = 3.779527560000020000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 574.173470000000000000
          Top = 3.779527560000020000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 646.173470000000000000
          Top = 3.779527560000020000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 534.173470000000000000
          Top = 3.779527560000020000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo49OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsRepasIts."VALDIAS">) / SUM(<frxDsRepasIts."Valor">))' +
              ']')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.000000000000000000
        Top = 498.897960000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo44: TfrxMemoView
          Left = 2.000000000000000000
          Top = 3.779527560000020000
          Width = 372.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsRepasIts."CONTAGEM">)] documento(s):')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 450.000000000000000000
          Top = 3.779527560000020000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 374.173470000000000000
          Top = 3.779527560000020000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 574.173470000000000000
          Top = 3.779527560000020000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 646.173470000000000000
          Top = 3.779527560000020000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 534.173470000000000000
          Top = 3.779527560000020000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo49OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsRepasIts."VALDIAS">) / SUM(<frxDsRepasIts."Valor">))' +
              ']')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 32.000000000000000000
        Top = 585.827150000000000000
        Width = 737.008350000000000000
        object Memo50: TfrxMemoView
          Left = 2.000000000000000000
          Top = 11.338582679999950000
          Width = 372.000000000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total geral de [SUM(<frxDsRepasIts."CONTAGEM">)] documento(s):')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 450.000000000000000000
          Top = 11.338582677165390000
          Width = 124.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 2.000000000000000000
          Top = 9.322509999999965000
          Width = 719.779530000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo38: TfrxMemoView
          Left = 374.173470000000000000
          Top = 11.338582677165390000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 574.173470000000000000
          Top = 11.338582677165390000
          Width = 72.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 646.173470000000000000
          Top = 11.338582677165390000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 534.173470000000000000
          Top = 11.338582677165390000
          Width = 40.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo49OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(SUM(<frxDsRepasIts."VALDIAS">) / SUM(<frxDsRepasIts."Valor">))' +
              ']')
          ParentFont = False
        end
      end
    end
  end
  object frxRepasCD_Sint: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.035394050900000000
    ReportOptions.LastChange = 39718.035394050900000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  DadosMestre1.Height := 0;'
      '  Memo2.Height := 0;'
      '  Memo8.Height := 0;'
      '  Memo12.Height := 0;'
      '  Memo54.Height := 0;'
      '  Memo24.Height := 0;'
      '  Memo28.Height := 0;'
      '  //GH1.Height := 0;'
      '  //GH2.Height := 0;'
      '  GH3.Height := 0;'
      '  //Memo29.Height := 0;'
      '  //Memo30.Height := 0;'
      '  Memo33.Height := 0;'
      ''
      ''
      
        '  if <VFR_ORD1> > 0 then GH1.Visible := True else GH1.Visible :=' +
        ' False;'
      
        '  if <VFR_ORD1> > 0 then GF1.Visible := True else GF1.Visible :=' +
        ' False;'
      '  //'
      
        '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsRepasIts."NOMECO' +
        'LIGADO"'#39';'
      
        '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsRepasIts."DATADE' +
        'F_TXT"'#39';'
      
        '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsRepasIts."PERIOD' +
        'O"'#39';'
      
        '  if <VFR_ORD1> = 4 then GH1.Condition := '#39'frxDsRepasIts."Emiten' +
        'te"'#39';'
      '  if <VFR_ORD1> = 5 then GH1.Condition := '#39'frxDsRepasIts."CPF"'#39';'
      
        '  if <VFR_ORD1> = 6 then GH1.Condition := '#39'frxDsRepasIts."Codigo' +
        '"'#39';'
      ''
      '    '
      
        '  if <VFR_ORD2> > 0 then GH2.Visible := True else GH2.Visible :=' +
        ' False;'
      
        '  if <VFR_ORD2> > 0 then GF2.Visible := True else GF2.Visible :=' +
        ' False;'
      '  //'
      
        '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsRepasIts."NOMECO' +
        'LIGADO"'#39';'
      
        '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsRepasIts."DATADE' +
        'F_TXT"'#39';'
      
        '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsRepasIts."PERIOD' +
        'O"'#39';'
      
        '  if <VFR_ORD2> = 4 then GH2.Condition := '#39'frxDsRepasIts."Emiten' +
        'te"'#39';'
      '  if <VFR_ORD2> = 5 then GH2.Condition := '#39'frxDsRepasIts."CPF"'#39';'
      
        '  if <VFR_ORD2> = 6 then GH2.Condition := '#39'frxDsRepasIts."Codigo' +
        '"'#39';'
      ''
      ''
      
        '  if <VFR_ORD3> > 0 then GH3.Visible := True else GH3.Visible :=' +
        ' False;'
      
        '  if <VFR_ORD3> > 0 then GF3.Visible := True else GF3.Visible :=' +
        ' False;'
      '  //'
      
        '  if <VFR_ORD3> = 1 then GH3.Condition := '#39'frxDsRepasIts."NOMECO' +
        'LIGADO"'#39';'
      
        '  if <VFR_ORD3> = 2 then GH3.Condition := '#39'frxDsRepasIts."DATADE' +
        'F_TXT"'#39';'
      
        '  if <VFR_ORD3> = 3 then GH3.Condition := '#39'frxDsRepasIts."PERIOD' +
        'O"'#39';'
      
        '  if <VFR_ORD3> = 4 then GH3.Condition := '#39'frxDsRepasIts."Emiten' +
        'te"'#39';'
      '  if <VFR_ORD3> = 5 then GH3.Condition := '#39'frxDsRepasIts."CPF"'#39';'
      
        '  if <VFR_ORD3> = 6 then GH3.Condition := '#39'frxDsRepasIts."Codigo' +
        '"'#39';'
      '  '
      'end.')
    OnGetValue = frxRepasCHGetValue
    Left = 140
    Top = 64
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsRepasIts
        DataSetName = 'frxDsRepasIts'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 5.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'ReportTitle1OnBeforePrint'
        object Memo32: TfrxMemoView
          Left = 554.000000000000000000
          Top = 0.881880000000002400
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 2.000000000000000000
          Top = 16.881880000000000000
          Width = 716.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_COLIGADO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 619.842920000000000000
        Width = 737.008350000000000000
        object Memo31: TfrxMemoView
          Left = 578.220470000000000000
          Top = 3.873529999999960000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 666.220470000000000000
          Top = 3.873529999999960000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TOTALPAGES]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 80.291280000000000000
        Top = 98.267780000000000000
        Width = 737.008350000000000000
        object Memo19: TfrxMemoView
          Left = 10.000000000000000000
          Top = 1.291280000000000000
          Width = 700.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[TITULO]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 10.000000000000000000
          Top = 33.291279999999990000
          Width = 448.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_TODOS]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 522.000000000000000000
          Top = 33.291279999999990000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PERIODO_D]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 474.000000000000000000
          Top = 33.291279999999990000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 15.118110240000000000
          Top = 63.291279999999990000
          Width = 359.055118110236000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 498.000000000000000000
          Top = 63.291279999999990000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 374.000000000000000000
          Top = 63.291279999999990000
          Width = 60.094488188976400000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo25OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Dias')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 574.000000000000000000
          Top = 63.291279999999990000
          Width = 68.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ Juros')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 642.000000000000000000
          Top = 63.291279999999990000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '$ L'#237'quido')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 434.000000000000000000
          Top = 63.291279999999990000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Qtde docs')
          ParentFont = False
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        Height = 15.000000000000000000
        Top = 362.834880000000000000
        Width = 737.008350000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsRepasIts
        DataSetName = 'frxDsRepasIts'
        RowCount = 0
        object Memo8: TfrxMemoView
          Left = 58.692950000000000000
          Width = 152.000000000000000000
          Height = 15.118110236220500000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsRepasIts."DOCUMENTO"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 498.000000000000000000
          Width = 76.000000000000000000
          Height = 15.118110236220500000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."Valor"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 374.000000000000000000
          Width = 60.094488190000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."Dias"]')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 574.000000000000000000
          Width = 68.000000000000000000
          Height = 15.118110236220500000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."JurosV"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 642.000000000000000000
          Width = 76.000000000000000000
          Height = 15.118110236220500000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."LIQUIDO"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 434.000000000000000000
          Width = 64.000000000000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsRepasIts."CONTAGEM"]')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 17.007874020000000000
        Top = 238.110390000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsRepasIts."NOMECOLIGADO"'
        object Memo29: TfrxMemoView
          Left = 15.118110240000000000
          Width = 702.992125984252000000
          Height = 17.007874015748000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 17.007874020000000000
        Top = 279.685220000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsRepasIts."DATADEF_TXT"'
        object Memo30: TfrxMemoView
          Left = 37.795275590000000000
          Width = 680.314960630000000000
          Height = 17.007874015748000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 15.118110240000000000
          Width = 22.677165350000000000
          Height = 17.007874015748000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
      end
      object GH3: TfrxGroupHeader
        FillType = ftBrush
        Height = 17.007874020000000000
        Top = 321.260050000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GH3OnBeforePrint'
        Condition = 'frxDsRepasIts."PERIODOD"'
        object Memo33: TfrxMemoView
          Left = 56.692913390000000000
          Width = 661.417322830000000000
          Height = 17.007874015748000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VFR_LA3NOME]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 37.795275590000000000
          Width = 18.897637800000000000
          Height = 17.007874015748000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 15.118110240000000000
          Width = 22.677165350000000000
          Height = 17.007874015748000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
      end
      object GF3: TfrxGroupFooter
        FillType = ftBrush
        Height = 17.007874015748000000
        Top = 400.630180000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GF3OnBeforePrint'
        object Memo34: TfrxMemoView
          Left = 56.692913390000000000
          Width = 317.480314960000000000
          Height = 17.007874020000000000
          OnBeforePrint = 'Memo34OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VFR_LA3NOME]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 498.000000000000000000
          Width = 76.000000000000000000
          Height = 17.007874015748000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 574.000000000000000000
          Width = 68.000000000000000000
          Height = 17.007874015748000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 642.000000000000000000
          Width = 76.000000000000000000
          Height = 17.007874015748000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 434.000000000000000000
          Width = 64.000000000000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."CONTAGEM">)]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 37.795275590551200000
          Width = 18.897635350000000000
          Height = 17.007874015748000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 15.118110240000000000
          Width = 22.677165350000000000
          Height = 17.007874015748000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 374.000000000000000000
          Width = 60.094488190000000000
          Height = 17.007874020000000000
          OnBeforePrint = 'Memo22OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(IIF(SUM(<frxDsRepasIts."Valor">)>0,(SUM(<frxDsRepasIts."VALDIA' +
              'S">) / SUM(<frxDsRepasIts."Valor">)),0))]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 17.007874015748000000
        Top = 442.205010000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo37: TfrxMemoView
          Left = 37.795275590000000000
          Width = 336.378101650000000000
          Height = 17.007874015748000000
          OnBeforePrint = 'Memo37OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 15.118110240000000000
          Width = 22.677165354330700000
          Height = 17.007874015748000000
          OnBeforePrint = 'Memo15OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.500000000000000000
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 498.173470000000000000
          Width = 76.000000000000000000
          Height = 17.007874015748000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 574.173470000000000000
          Width = 68.000000000000000000
          Height = 17.007874015748000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 642.173470000000000000
          Width = 76.000000000000000000
          Height = 17.007874015748000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 434.173470000000000000
          Width = 64.000000000000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."CONTAGEM">)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 374.173470000000000000
          Width = 60.094488190000000000
          Height = 17.007874020000000000
          OnBeforePrint = 'Memo22OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(IIF(SUM(<frxDsRepasIts."Valor">)>0,(SUM(<frxDsRepasIts."VALDIA' +
              'S">) / SUM(<frxDsRepasIts."Valor">)),0))]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 17.007874015748000000
        Top = 483.779840000000000000
        Width = 737.008350000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo44: TfrxMemoView
          Left = 15.118110240000000000
          Width = 359.055276770000000000
          Height = 17.007874015748000000
          OnBeforePrint = 'Memo44OnBeforePrint'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 498.173470000000000000
          Width = 76.000000000000000000
          Height = 17.007874015748000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 574.173470000000000000
          Width = 68.000000000000000000
          Height = 17.007874015748000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 642.173470000000000000
          Width = 76.000000000000000000
          Height = 17.007874015748000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 434.173470000000000000
          Width = 64.000000000000000000
          Height = 17.007874020000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."CONTAGEM">)]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 374.173470000000000000
          Width = 60.094488190000000000
          Height = 17.007874020000000000
          OnBeforePrint = 'Memo22OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(IIF(SUM(<frxDsRepasIts."Valor">)>0,(SUM(<frxDsRepasIts."VALDIA' +
              'S">) / SUM(<frxDsRepasIts."Valor">)),0))]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 32.000000000000000000
        Top = 563.149970000000100000
        Width = 737.008350000000000000
        object Memo50: TfrxMemoView
          Left = 2.000000000000000000
          Top = 9.448818899999992000
          Width = 371.527520000000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Total geral:')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Left = 2.000000000000000000
          Top = 5.542980000000057000
          Width = 716.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo9: TfrxMemoView
          Left = 498.173470000000000000
          Top = 9.448818899999992000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."Valor">)]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 574.173470000000000000
          Top = 9.448818899999992000
          Width = 68.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."JurosV">)]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 642.173470000000000000
          Top = 9.448818899999992000
          Width = 76.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."LIQUIDO">)]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 434.173470000000000000
          Top = 9.448818899999992000
          Width = 64.000000000000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsRepasIts."CONTAGEM">)]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 374.173470000000000000
          Top = 9.448818899999992000
          Width = 60.000000000000000000
          Height = 17.000000000000000000
          OnBeforePrint = 'Memo22OnBeforePrint'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            
              '[(IIF(SUM(<frxDsRepasIts."Valor">)>0,(SUM(<frxDsRepasIts."VALDIA' +
              'S">) / SUM(<frxDsRepasIts."Valor">)),0))]')
          ParentFont = False
        end
      end
    end
  end
end
