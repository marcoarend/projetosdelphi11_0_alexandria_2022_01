object FmLotesMorto: TFmLotesMorto
  Left = 306
  Top = 175
  Caption = 'Envio de Border'#244's de Cheques ao Arquivo Morto'
  ClientHeight = 502
  ClientWidth = 801
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 801
    Height = 48
    Align = alTop
    Caption = 'Envio de Border'#244's de Cheques ao Arquivo Morto'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    ExplicitWidth = 784
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 799
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 450
    Width = 801
    Height = 52
    Align = alBottom
    TabOrder = 1
    ExplicitTop = 442
    ExplicitWidth = 784
    object BtSair: TBitBtn
      Left = 670
      Top = 4
      Width = 112
      Height = 44
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sair'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSairClick
    end
    object BtPesquisa: TBitBtn
      Left = 10
      Top = 4
      Width = 112
      Height = 44
      Cursor = crHandPoint
      Hint = 'Pesquisa|Pesquisa atendimentos at'#233' a data definida'
      Caption = '&Pesquisar'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtPesquisaClick
    end
    object BtArquivoMorto: TBitBtn
      Left = 126
      Top = 4
      Width = 111
      Height = 44
      Cursor = crHandPoint
      Hint = 'Arquiva|Arquiva os atendimentos pesquisados'
      Caption = '&Arquivar'
      Enabled = False
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      NumGlyphs = 2
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtArquivoMortoClick
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 801
    Height = 402
    Align = alClient
    TabOrder = 2
    ExplicitWidth = 784
    ExplicitHeight = 394
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 799
      Height = 124
      Align = alTop
      TabOrder = 0
      ExplicitWidth = 782
      object Label1: TLabel
        Left = 8
        Top = 12
        Width = 96
        Height = 13
        Caption = 'Data Limite (Atend.):'
      end
      object Label6: TLabel
        Left = 116
        Top = 12
        Width = 45
        Height = 13
        Caption = 'Border'#244's:'
      end
      object Label2: TLabel
        Left = 212
        Top = 12
        Width = 85
        Height = 13
        Caption = 'Itens de border'#244's:'
      end
      object Label3: TLabel
        Left = 308
        Top = 12
        Width = 6
        Height = 13
        Caption = '?'
        Visible = False
      end
      object Label4: TLabel
        Left = 404
        Top = 12
        Width = 6
        Height = 13
        Caption = '?'
        Visible = False
      end
      object Label5: TLabel
        Left = 500
        Top = 12
        Width = 6
        Height = 13
        Caption = '?'
        Visible = False
      end
      object Label9: TLabel
        Left = 596
        Top = 12
        Width = 6
        Height = 13
        Caption = '?'
        Visible = False
      end
      object Label10: TLabel
        Left = 692
        Top = 12
        Width = 38
        Height = 13
        Caption = 'TOTAL:'
      end
      object Label11: TLabel
        Left = 116
        Top = 56
        Width = 45
        Height = 13
        Caption = 'Border'#244's:'
      end
      object Label12: TLabel
        Left = 212
        Top = 56
        Width = 85
        Height = 13
        Caption = 'Itens de border'#244's:'
      end
      object Label13: TLabel
        Left = 308
        Top = 56
        Width = 6
        Height = 13
        Caption = '?'
        Visible = False
      end
      object Label14: TLabel
        Left = 404
        Top = 56
        Width = 6
        Height = 13
        Caption = '?'
        Visible = False
      end
      object Label15: TLabel
        Left = 500
        Top = 56
        Width = 6
        Height = 13
        Caption = '?'
        Visible = False
      end
      object Label16: TLabel
        Left = 596
        Top = 56
        Width = 6
        Height = 13
        Caption = '?'
        Visible = False
      end
      object Label17: TLabel
        Left = 692
        Top = 56
        Width = 38
        Height = 13
        Caption = 'TOTAL:'
      end
      object Label18: TLabel
        Left = 8
        Top = 56
        Width = 64
        Height = 13
        Caption = 'Redu'#231#227'o (%):'
      end
      object TPLimite: TDateTimePicker
        Left = 8
        Top = 28
        Width = 105
        Height = 21
        Date = 38345.387518506900000000
        Time = 38345.387518506900000000
        TabOrder = 0
        OnClick = TPLimiteClick
        OnChange = TPLimiteChange
      end
      object EdAllLot: TdmkEdit
        Left = 116
        Top = 28
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAllLoi: TdmkEdit
        Left = 212
        Top = 28
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAllMer: TdmkEdit
        Left = 308
        Top = 28
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 3
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAllTic: TdmkEdit
        Left = 404
        Top = 28
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 4
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAllPag: TdmkEdit
        Left = 500
        Top = 28
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 5
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object Progress: TProgressBar
        Left = 8
        Top = 100
        Width = 773
        Height = 16
        TabOrder = 6
      end
      object EdAllMot: TdmkEdit
        Left = 596
        Top = 28
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 7
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAll: TdmkEdit
        Left = 692
        Top = 28
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAntLot: TdmkEdit
        Left = 116
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAntLoi: TdmkEdit
        Left = 212
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 10
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAntMer: TdmkEdit
        Left = 308
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 11
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAntTic: TdmkEdit
        Left = 404
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 12
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAntPag: TdmkEdit
        Left = 500
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 13
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAntMot: TdmkEdit
        Left = 596
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 14
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdAnt: TdmkEdit
        Left = 692
        Top = 72
        Width = 93
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdReducao: TdmkEdit
        Left = 8
        Top = 72
        Width = 105
        Height = 21
        Alignment = taRightJustify
        Color = clBtnFace
        ReadOnly = True
        TabOrder = 16
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 205
      Width = 799
      Height = 196
      Align = alClient
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Cliente'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEENTIDADE'
          Title.Caption = 'Cliente'
          Visible = True
        end>
    end
    object Panel4: TPanel
      Left = 1
      Top = 125
      Width = 799
      Height = 80
      Align = alTop
      TabOrder = 2
      ExplicitWidth = 782
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 120
        Height = 78
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object StaticText1: TStaticText
          Left = 0
          Top = 0
          Width = 45
          Height = 17
          Align = alTop
          Caption = 'Avisos:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object StaticText2: TStaticText
          Left = 0
          Top = 17
          Width = 95
          Height = 17
          Align = alTop
          Caption = 'Tempo previsto:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
        object StaticText4: TStaticText
          Left = 0
          Top = 34
          Width = 117
          Height = 17
          Align = alTop
          Caption = 'Tempo transcorrido:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
        end
        object StaticText5: TStaticText
          Left = 0
          Top = 51
          Width = 96
          Height = 17
          Align = alTop
          Caption = 'Tempo restante:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
        end
      end
      object Panel6: TPanel
        Left = 121
        Top = 1
        Width = 677
        Height = 78
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        ExplicitWidth = 660
        object ST1: TStaticText
          Left = 0
          Top = 0
          Width = 27
          Height = 17
          Align = alTop
          Caption = 'ST1'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object ST2: TStaticText
          Left = 0
          Top = 17
          Width = 27
          Height = 17
          Align = alTop
          Caption = 'ST2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 1
        end
        object ST3: TStaticText
          Left = 0
          Top = 34
          Width = 27
          Height = 17
          Align = alTop
          Caption = 'ST3'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 2
        end
        object ST4: TStaticText
          Left = 0
          Top = 51
          Width = 27
          Height = 17
          Align = alTop
          Caption = 'ST4'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 3
        end
      end
    end
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT SUM(vl.LocDeb) Valor'
      'FROM vendasloc vl, Vendas ve'
      'WHERE vl.Codigo=ve.Codigo')
    Left = 8
    Top = 296
    object QrSomaValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object Qr1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT vm.* '
      'FROM vendasmot vm, Vendas ve'
      'WHERE ve.Codigo=vm.Codigo'
      'AND ve.DataCad<= :P0'
      'INTO OUTFILE '#39'C:/Dermatek/VendasMot.txt'#39)
    Left = 508
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object Qr3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'DELETE FROM vm'
      'USING VendasMot vm, Vendas ve '
      'WHERE ve.Codigo=vm.Codigo'
      'AND ve.DataCad<= :P0')
    Left = 564
    Top = 384
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object Qr2: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'LOAD DATA LOCAL INFILE '#39'C:/Dermatek/VendasMot.txt'#39
      'INTO Table VendasMot')
    Left = 536
    Top = 384
  end
  object QrQtd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(ve.Codigo)+0.000 Registros'
      'FROM vendas ve'
      'WHERE ve.DataCad<=:P0')
    Left = 716
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrQtdRegistros: TFloatField
      FieldName = 'Registros'
      Required = True
    end
  end
  object QrULot: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 676
    Top = 176
  end
  object QrSumEmi: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CPF, SUM(Valor) Valor, COUNT(Controle) Itens'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'WHERE lot.Codigo > 0'
      'AND lot.MaxVencto < :P0'
      'GROUP BY CPF')
    Left = 422
    Top = 190
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumEmiCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrSumEmiValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSumEmiItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object QrSumCli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lot.Cliente, SUM(Valor) Valor, COUNT(Controle) Itens'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'WHERE lot.Codigo > 0'
      'AND lot.MaxVencto < :P0'
      'GROUP BY lot.Cliente')
    Left = 450
    Top = 190
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumCliCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSumCliValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSumCliItens: TLargeintField
      FieldName = 'Itens'
      Required = True
    end
  end
  object Timer1: TTimer
    Enabled = False
    OnTimer = Timer1Timer
    Left = 526
    Top = 190
  end
end
