unit APCD;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkRadioGroup,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmAPCD = class(TForm)
    PainelDados: TPanel;
    DsAPCD: TDataSource;
    QrAPCD: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PnPesq: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TStaticText;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label9: TLabel;
    QrAPCDCodigo: TIntegerField;
    QrAPCDNome: TWideStringField;
    QrAPCDDeposita: TSmallintField;
    QrAPCDExigeCMC7: TSmallintField;
    QrAPCDLk: TIntegerField;
    QrAPCDDataCad: TDateField;
    QrAPCDDataAlt: TDateField;
    QrAPCDUserCad: TIntegerField;
    QrAPCDUserAlt: TIntegerField;
    QrAPCDAlterWeb: TSmallintField;
    QrAPCDAtivo: TSmallintField;
    RGDeposita: TdmkRadioGroup;
    RGExigeCMC7: TdmkRadioGroup;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrAPCDAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrAPCDBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmAPCD: TFmAPCD;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmAPCD.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmAPCD.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrAPCDCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmAPCD.DefParams;
begin
  VAR_GOTOTABELA := 'apcd';
  VAR_GOTOMYSQLTABLE := QrAPCD;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM apcd');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmAPCD.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible := True;
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
    end;
    1:
    begin
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      PainelControle.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant := FormatFloat(FFormatFloat, Codigo);
        EdNome.ValueVariant   := '';
        RGDeposita.ItemIndex  := 0;
        RGExigeCMC7.ItemIndex := 0;
        //
      end else begin
        EdCodigo.ValueVariant := QrAPCDCodigo.Value;
        EdNome.ValueVariant   := QrAPCDNome.Value;
        RGDeposita.ItemIndex  := QrAPCDDeposita.Value;
        RGExigeCMC7.ItemIndex := QrAPCDExigeCMC7.Value;
        //
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.SQLType);
end;

procedure TFmAPCD.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmAPCD.QueryPrincipalAfterOpen;
begin
end;

procedure TFmAPCD.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmAPCD.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmAPCD.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmAPCD.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmAPCD.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmAPCD.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrAPCD, [PainelDados],
  [PainelEdita], EdNome, LaTipo, 'cadsimples');
end;

procedure TFmAPCD.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrAPCDCodigo.Value;
  Close;
end;

procedure TFmAPCD.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('apcd', 'Codigo', LaTipo.SQLType,
    QrAPCDCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmAPCD, PainelEdit,
    'apcd', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmAPCD.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.ValueVariant);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'apcd', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'apcd', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'apcd', 'Codigo');
end;

procedure TFmAPCD.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrAPCD, [PainelDados],
  [PainelEdita], EdNome, LaTipo, 'apcd');
end;

procedure TFmAPCD.FormCreate(Sender: TObject);
begin
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmAPCD.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrAPCDCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAPCD.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmAPCD.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrAPCDCodigo.Value, LaRegistro.Caption);
end;

procedure TFmAPCD.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.SQLType);
end;

procedure TFmAPCD.QrAPCDAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmAPCD.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmAPCD.SbQueryClick(Sender: TObject);
begin
  LocCod(QrAPCDCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'apcd', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmAPCD.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmAPCD.QrAPCDBeforeOpen(DataSet: TDataSet);
begin
  QrAPCDCodigo.DisplayFormat := FFormatFloat;
end;

end.

