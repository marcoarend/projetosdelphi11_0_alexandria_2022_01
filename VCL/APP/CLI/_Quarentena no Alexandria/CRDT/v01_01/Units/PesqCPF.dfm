object FmPesqCPF: TFmPesqCPF
  Left = 419
  Top = 217
  Caption = 'Pesquisa de Emitentes de Cheques'
  ClientHeight = 306
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 258
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtPesquisa: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Pesquisa'
      TabOrder = 0
      OnClick = BtPesquisaClick
      NumGlyphs = 2
    end
    object BtConfirma: TBitBtn
      Tag = 15
      Left = 350
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Confirma'
      Enabled = False
      TabOrder = 1
      OnClick = BtConfirmaClick
      NumGlyphs = 2
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 682
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtSaidaClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Pesquisa de Emitentes de Cheques'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 61
    Align = alTop
    TabOrder = 2
    object Label1: TLabel
      Left = 188
      Top = 12
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object RGMascara: TRadioGroup
      Left = 8
      Top = 8
      Width = 177
      Height = 41
      Caption = ' M'#225'scara: '
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Ambos'
        'Prefixo'
        'Sufixo')
      TabOrder = 0
      OnClick = RGMascaraClick
    end
    object EdEmitente: TdmkEdit
      Left = 188
      Top = 28
      Width = 593
      Height = 21
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnChange = EdEmitenteChange
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 109
    Width = 784
    Height = 149
    Align = alClient
    DataSource = DsEmitCPF
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    OnDblClick = DBGrid1DblClick
  end
  object QrEmitCPF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM emitcpf'
      'WHERE Nome LIKE :P0'
      'ORDER BY Nome')
    Left = 296
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmitCPFCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrEmitCPFNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrEmitCPFLimite: TFloatField
      FieldName = 'Limite'
    end
    object QrEmitCPFLastAtz: TDateField
      FieldName = 'LastAtz'
    end
    object QrEmitCPFAcumCHComV: TFloatField
      FieldName = 'AcumCHComV'
    end
    object QrEmitCPFAcumCHComQ: TIntegerField
      FieldName = 'AcumCHComQ'
    end
    object QrEmitCPFAcumCHDevV: TFloatField
      FieldName = 'AcumCHDevV'
    end
    object QrEmitCPFAcumCHDevQ: TIntegerField
      FieldName = 'AcumCHDevQ'
    end
  end
  object DsEmitCPF: TDataSource
    DataSet = QrEmitCPF
    Left = 324
    Top = 164
  end
end
