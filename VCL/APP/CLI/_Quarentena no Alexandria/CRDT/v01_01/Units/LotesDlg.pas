unit LotesDlg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, CheckLst, dmkGeral;

type
  TFmLotesDlg = class(TForm)
    PainelConfirma: TPanel;
    BtImprime: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    CLImpressoes: TCheckListBox;
    Panel1: TPanel;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    BtPreview: TBitBtn;
    BtSalvar: TBitBtn;
    procedure BtImprimeClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPreviewClick(Sender: TObject);
    procedure CLImpressoesClick(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtSalvarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure SetaRelatorios;
    procedure ClicaTodos(Checked: Boolean);
  public
    { Public declarations }
  end;

  var
  FmLotesDlg: TFmLotesDlg;

implementation

uses Lotes1, Principal, UnMyObjects;

{$R *.DFM}

procedure TFmLotesDlg.BtImprimeClick(Sender: TObject);
begin
  case FmPrincipal.FFormLotesShow of
    //0: FmLotes0.FImprime := 1;
    1: FmLotes1.FImprime := 1;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (02)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  Close;
end;

procedure TFmLotesDlg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLotesDlg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmLotesDlg.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLotesDlg.BtPreviewClick(Sender: TObject);
begin
  case FmPrincipal.FFormLotesShow of
    //0: FmLotes0.FImprime := 2;
    1: FmLotes1.FImprime := 2;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (02)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  Close;
end;

procedure TFmLotesDlg.CLImpressoesClick(Sender: TObject);
begin
  SetaRelatorios;
end;

procedure TFmLotesDlg.SetaRelatorios;
begin
  case FmPrincipal.FFormLotesShow of
    {
    0:
    begin
      FmLotes0.FRelatorios := MLAGeral.ValorDeChekListBox(CLImpressoes);
      BtImprime.Enabled := FmLotes0.FRelatorios > 0;
      BtPreview.Enabled := FmLotes0.FRelatorios > 0;
    end;
    }
    1:
    begin
      FmLotes1.FRelatorios := MLAGeral.ValorDeChekListBox(CLImpressoes);
      BtImprime.Enabled := FmLotes1.FRelatorios > 0;
      BtPreview.Enabled := FmLotes1.FRelatorios > 0;
    end;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (03)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  //
end;

procedure TFmLotesDlg.BtTodosClick(Sender: TObject);
begin
  ClicaTodos(True);
end;

procedure TFmLotesDlg.BtNenhumClick(Sender: TObject);
begin
  ClicaTodos(False);
end;

procedure TFmLotesDlg.ClicaTodos(Checked: Boolean);
var
  i: Integer;
begin
  for i := 0 to CLImpressoes.Items.Count -1 do
    CLImpressoes.Checked[i] := Checked;
end;

procedure TFmLotesDlg.BtSalvarClick(Sender: TObject);
var
  Tipo, Rel: Integer;
begin
  Tipo := 0;
  Rel  := 0;
  //
  case FmPrincipal.FFormLotesShow of
    {
    0:
    begin
      Tipo := FmLotes0.QrLotesTipo.Value;
      Rel  := FmLotes0.FRelatorios;
    end;
    }
    1:
    begin
      Tipo := FmLotes1.QrLotesTipo.Value;
      Rel  := FmLotes1.FRelatorios;
    end;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (05)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  Geral.WriteAppKeyLM2('LotesRelats' + Geral.FF0(Tipo), Application.Title, Rel, ktInteger);
end;

procedure TFmLotesDlg.FormCreate(Sender: TObject);
var
  Tipo, Relatorios, i, n: Integer;
begin
  Tipo := 0;
  //
  case FmPrincipal.FFormLotesShow of
    //0: FmLotes0.QrLotesTipo.Value;
    1: FmLotes1.QrLotesTipo.Value;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (06)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  Relatorios := Geral.ReadAppKeyLM('LotesRelats' + Geral.FF0(Tipo), Application.Title, ktInteger, 0);
  n := 2;
  for i := 0 to CLImpressoes.Items.Count -1 do
  begin
    CLImpressoes.Checked[i] := MLAGeral.IntInConjunto2(n, Relatorios);
    n := n * 2;
  end;
  SetaRelatorios;
end;

end.
