object FmCompJuros: TFmCompJuros
  Left = 405
  Top = 175
  Caption = 'C'#225'lculos de juros'
  ClientHeight = 406
  ClientWidth = 573
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 358
    Width = 573
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtSaida: TBitBtn
      Tag = 13
      Left = 470
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 0
      OnClick = BtSaidaClick
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 573
    Height = 358
    Align = alClient
    TabOrder = 1
    object PainelDecomp: TPanel
      Left = 1
      Top = 225
      Width = 571
      Height = 132
      Align = alBottom
      TabOrder = 1
      object Label11: TLabel
        Left = 12
        Top = 8
        Width = 48
        Height = 13
        Caption = 'Juro Final:'
      end
      object Label1: TLabel
        Left = 116
        Top = 8
        Width = 54
        Height = 13
        Caption = 'Dias Prazo:'
      end
      object Label2: TLabel
        Left = 188
        Top = 8
        Width = 32
        Height = 13
        Caption = 'Casas:'
      end
      object Label3: TLabel
        Left = 228
        Top = 8
        Width = 49
        Height = 13
        Caption = 'Taxa m'#234's:'
      end
      object EdJuros: TdmkEdit
        Left = 12
        Top = 24
        Width = 101
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdJurosChange
      end
      object EdPrazD: TdmkEdit
        Left = 116
        Top = 24
        Width = 69
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdPrazDChange
      end
      object EdCasas: TdmkEdit
        Left = 188
        Top = 24
        Width = 37
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '0'
        ValMax = '6'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdCasasChange
      end
      object EdTaxa: TdmkEdit
        Left = 228
        Top = 24
        Width = 101
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
    end
    object PainelDados: TPanel
      Left = 1
      Top = 49
      Width = 571
      Height = 128
      Align = alClient
      TabOrder = 0
      object Label4: TLabel
        Left = 12
        Top = 8
        Width = 27
        Height = 13
        Caption = 'Valor:'
      end
      object Label5: TLabel
        Left = 116
        Top = 8
        Width = 54
        Height = 13
        Caption = 'Dias Prazo:'
      end
      object Label6: TLabel
        Left = 364
        Top = 8
        Width = 37
        Height = 13
        Caption = '$ Juros:'
      end
      object Label7: TLabel
        Left = 260
        Top = 8
        Width = 39
        Height = 13
        Caption = '% Juros:'
      end
      object Label8: TLabel
        Left = 468
        Top = 8
        Width = 39
        Height = 13
        Caption = 'L'#237'quido:'
      end
      object Label9: TLabel
        Left = 188
        Top = 8
        Width = 63
        Height = 13
        Caption = 'Taxa mensal:'
      end
      object EdBaseT: TdmkEdit
        Left = 12
        Top = 24
        Width = 101
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdBaseTChange
      end
      object EdPrazC: TdmkEdit
        Left = 116
        Top = 24
        Width = 69
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdPrazCChange
      end
      object EdJuroV: TdmkEdit
        Left = 364
        Top = 24
        Width = 101
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdJuroP: TdmkEdit
        Left = 260
        Top = 24
        Width = 101
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdLiqui: TdmkEdit
        Left = 468
        Top = 24
        Width = 101
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdTaxaM: TdmkEdit
        Left = 188
        Top = 24
        Width = 69
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        OnChange = EdTaxaMChange
      end
    end
    object Painel2: TPanel
      Left = 1
      Top = 1
      Width = 571
      Height = 48
      Align = alTop
      Caption = 'Comp'#245'e Juros'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -32
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 2
      object Image2: TImage
        Left = 1
        Top = 1
        Width = 569
        Height = 46
        Align = alClient
        Transparent = True
        ExplicitLeft = 2
        ExplicitTop = 2
        ExplicitWidth = 575
        ExplicitHeight = 44
      end
    end
    object Painel1: TPanel
      Left = 1
      Top = 177
      Width = 571
      Height = 48
      Align = alBottom
      Caption = 'Decomp'#245'e Juros'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -32
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 3
      object Image1: TImage
        Left = 1
        Top = 1
        Width = 569
        Height = 46
        Align = alClient
        Transparent = True
        ExplicitLeft = 2
        ExplicitTop = 2
        ExplicitWidth = 575
        ExplicitHeight = 44
      end
    end
  end
end
