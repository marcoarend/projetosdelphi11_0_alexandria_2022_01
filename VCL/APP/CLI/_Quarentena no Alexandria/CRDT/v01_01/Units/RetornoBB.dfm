object FmRetornoBB: TFmRetornoBB
  Left = 252
  Top = 153
  Caption = 'Retorno BB'
  ClientHeight = 553
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 48
    Align = alTop
    Caption = 'Retorno BB'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1014
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 1012
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1016
    Height = 505
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1014
      Height = 48
      Align = alTop
      TabOrder = 0
      object Label31: TLabel
        Left = 8
        Top = 4
        Width = 129
        Height = 13
        Caption = 'Arquivo cobran'#231'a - retorno:'
      end
      object BtAbre: TSpeedButton
        Left = 756
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = BtAbreClick
      end
      object SpeedButton5: TSpeedButton
        Left = 780
        Top = 20
        Width = 23
        Height = 22
        Visible = False
        OnClick = SpeedButton5Click
      end
      object EdPathBBRet: TdmkEdit
        Left = 8
        Top = 20
        Width = 749
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object PageControl1: TPageControl
      Left = 1
      Top = 49
      Width = 1014
      Height = 455
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Lotes de arquivos retorno lidos'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel12: TPanel
          Left = 0
          Top = 0
          Width = 1006
          Height = 49
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Label119: TLabel
            Left = 8
            Top = 4
            Width = 60
            Height = 13
            Caption = 'Lote arq. ret:'
          end
          object Label120: TLabel
            Left = 76
            Top = 4
            Width = 34
            Height = 13
            Caption = 'Banco:'
            FocusControl = DBEdit20
          end
          object Label121: TLabel
            Left = 376
            Top = 4
            Width = 200
            Height = 13
            Caption = 'Configura'#231#227'o de exporta'#231#227'o / importa'#231#227'o:'
            FocusControl = DBEdit22
          end
          object Label122: TLabel
            Left = 264
            Top = 4
            Width = 30
            Height = 13
            Caption = 'Envio:'
            FocusControl = DBEdit24
          end
          object DBEdit19: TDBEdit
            Left = 8
            Top = 20
            Width = 64
            Height = 21
            DataField = 'CNAB240L'
            DataSource = DsCNAB240
            TabOrder = 0
          end
          object DBEdit20: TDBEdit
            Left = 76
            Top = 20
            Width = 33
            Height = 21
            DataField = 'Banco'
            DataSource = DsCNAB240
            TabOrder = 1
          end
          object DBEdit21: TDBEdit
            Left = 108
            Top = 20
            Width = 153
            Height = 21
            DataField = 'NOMEBANCO'
            DataSource = DsCNAB240
            TabOrder = 2
          end
          object DBEdit22: TDBEdit
            Left = 376
            Top = 20
            Width = 45
            Height = 21
            DataField = 'ConfigBB'
            DataSource = DsCNAB240
            TabOrder = 3
          end
          object DBEdit23: TDBEdit
            Left = 420
            Top = 20
            Width = 169
            Height = 21
            DataSource = DsCNAB240
            TabOrder = 4
          end
          object DBEdit24: TDBEdit
            Left = 264
            Top = 20
            Width = 29
            Height = 21
            DataField = 'Envio'
            DataSource = DsCNAB240
            TabOrder = 5
          end
          object DBEdit25: TDBEdit
            Left = 292
            Top = 20
            Width = 81
            Height = 21
            DataField = 'NOMEENVIO'
            DataSource = DsCNAB240
            TabOrder = 6
          end
        end
        object Panel14: TPanel
          Left = 0
          Top = 379
          Width = 1006
          Height = 48
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 1
          object Panel15: TPanel
            Left = 884
            Top = 0
            Width = 122
            Height = 48
            Align = alRight
            ParentColor = True
            TabOrder = 0
            object BitBtn1: TBitBtn
              Tag = 13
              Left = 16
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
          object Panel16: TPanel
            Left = 0
            Top = 0
            Width = 884
            Height = 48
            Align = alClient
            TabOrder = 1
            object BitBtn4: TBitBtn
              Tag = 294
              Left = 292
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Executa'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BitBtn4Click
            end
            object BitBtn2: TBitBtn
              Tag = 150
              Left = 660
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Ger'#234'ncia'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BitBtn2Click
            end
            object BitBtn5: TBitBtn
              Tag = 1
              Left = 8
              Top = 4
              Width = 40
              Height = 40
              Cursor = crHandPoint
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BitBtn5Click
            end
            object BitBtn6: TBitBtn
              Tag = 2
              Left = 48
              Top = 4
              Width = 40
              Height = 40
              Cursor = crHandPoint
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 3
              OnClick = BitBtn6Click
            end
            object BitBtn7: TBitBtn
              Tag = 3
              Left = 88
              Top = 4
              Width = 40
              Height = 40
              Cursor = crHandPoint
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 4
              OnClick = BitBtn7Click
            end
            object BitBtn8: TBitBtn
              Tag = 4
              Left = 128
              Top = 4
              Width = 40
              Height = 40
              Cursor = crHandPoint
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 5
              OnClick = BitBtn8Click
            end
          end
        end
        object Panel13: TPanel
          Left = 0
          Top = 253
          Width = 1006
          Height = 126
          Align = alBottom
          BevelOuter = bvNone
          TabOrder = 2
          object Label123: TLabel
            Left = 8
            Top = 4
            Width = 38
            Height = 13
            Caption = 'Meu ID:'
          end
          object Label124: TLabel
            Left = 8
            Top = 44
            Width = 48
            Height = 13
            Caption = 'Duplicata:'
            FocusControl = DBEdit26
          end
          object Label125: TLabel
            Left = 84
            Top = 4
            Width = 35
            Height = 13
            Caption = 'Cliente:'
            FocusControl = DBEdit27
          end
          object Label126: TLabel
            Left = 388
            Top = 4
            Width = 24
            Height = 13
            Caption = 'Lote:'
            FocusControl = DBEdit28
          end
          object Label127: TLabel
            Left = 456
            Top = 4
            Width = 40
            Height = 13
            Caption = 'Border'#244':'
            FocusControl = DBEdit29
          end
          object Label128: TLabel
            Left = 524
            Top = 4
            Width = 24
            Height = 13
            Caption = 'Tipo:'
            FocusControl = DBEdit30
          end
          object Label129: TLabel
            Left = 592
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Emitente:'
            FocusControl = DBEdit31
          end
          object Label130: TLabel
            Left = 884
            Top = 4
            Width = 99
            Height = 13
            Caption = 'CPF / CNPJ sacado:'
            FocusControl = DBEdit32
          end
          object Label131: TLabel
            Left = 88
            Top = 44
            Width = 51
            Height = 13
            Caption = 'Valor face:'
            FocusControl = DBEdit33
          end
          object Label132: TLabel
            Left = 164
            Top = 44
            Width = 49
            Height = 13
            Caption = 'Desconto:'
            FocusControl = DBEdit34
          end
          object Label133: TLabel
            Left = 240
            Top = 44
            Width = 66
            Height = 13
            Caption = 'Valor receber:'
            FocusControl = DBEdit35
          end
          object Label134: TLabel
            Left = 316
            Top = 44
            Width = 42
            Height = 13
            Caption = 'Emiss'#227'o:'
            FocusControl = DBEdit36
          end
          object Label135: TLabel
            Left = 376
            Top = 44
            Width = 50
            Height = 13
            Caption = 'D.Compra:'
            FocusControl = DBEdit37
          end
          object Label136: TLabel
            Left = 436
            Top = 44
            Width = 37
            Height = 13
            Caption = 'Vencto:'
            FocusControl = DBEdit38
          end
          object Label137: TLabel
            Left = 496
            Top = 44
            Width = 43
            Height = 13
            Caption = 'Resgate:'
            FocusControl = DBEdit39
          end
          object Label138: TLabel
            Left = 596
            Top = 44
            Width = 45
            Height = 13
            Caption = 'D. Pagto:'
            FocusControl = DBEdit40
          end
          object Label139: TLabel
            Left = 556
            Top = 44
            Width = 21
            Height = 13
            Caption = 'Dias'
            FocusControl = DBEdit41
          end
          object Label140: TLabel
            Left = 656
            Top = 44
            Width = 49
            Height = 13
            Caption = 'Cobran'#231'a:'
            FocusControl = DBEdit42
          end
          object Label141: TLabel
            Left = 716
            Top = 44
            Width = 33
            Height = 13
            Caption = 'Status:'
            FocusControl = DBEdit43
          end
          object Label143: TLabel
            Left = 932
            Top = 84
            Width = 35
            Height = 13
            Caption = 'Custas:'
            FocusControl = DBEdit45
          end
          object Label144: TLabel
            Left = 508
            Top = 84
            Width = 40
            Height = 13
            Caption = 'Sacado:'
            FocusControl = DBEdit46
          end
          object Label145: TLabel
            Left = 816
            Top = 84
            Width = 61
            Height = 13
            Caption = 'CPF / CNPJ:'
            FocusControl = DBEdit47
          end
          object Label147: TLabel
            Left = 8
            Top = 84
            Width = 35
            Height = 13
            Caption = 'Cliente:'
            FocusControl = DBEdit49
          end
          object DBEdit26: TDBEdit
            Left = 8
            Top = 60
            Width = 76
            Height = 21
            DataField = 'Duplicata'
            DataSource = DsCNAB240
            TabOrder = 0
          end
          object DBEdit27: TDBEdit
            Left = 84
            Top = 20
            Width = 300
            Height = 21
            DataField = 'NOMECLI'
            DataSource = DsCNAB240
            TabOrder = 1
          end
          object DBEdit28: TDBEdit
            Left = 388
            Top = 20
            Width = 64
            Height = 21
            DataField = 'MeuLote'
            DataSource = DsCNAB240
            TabOrder = 2
          end
          object DBEdit29: TDBEdit
            Left = 456
            Top = 20
            Width = 64
            Height = 21
            DataField = 'Bordero'
            DataSource = DsCNAB240
            TabOrder = 3
          end
          object DBEdit30: TDBEdit
            Left = 524
            Top = 20
            Width = 64
            Height = 21
            DataField = 'NOMETIPOLOTE'
            DataSource = DsCNAB240
            TabOrder = 4
          end
          object DBEdit31: TDBEdit
            Left = 592
            Top = 20
            Width = 289
            Height = 21
            DataField = 'Emitente'
            DataSource = DsCNAB240
            TabOrder = 5
          end
          object DBEdit32: TDBEdit
            Left = 884
            Top = 20
            Width = 113
            Height = 21
            DataField = 'CPF_TXT'
            DataSource = DsCNAB240
            TabOrder = 6
          end
          object DBEdit33: TDBEdit
            Left = 88
            Top = 60
            Width = 72
            Height = 21
            DataField = 'Bruto'
            DataSource = DsCNAB240
            TabOrder = 7
          end
          object DBEdit34: TDBEdit
            Left = 164
            Top = 60
            Width = 72
            Height = 21
            DataField = 'Desco'
            DataSource = DsCNAB240
            TabOrder = 8
          end
          object DBEdit35: TDBEdit
            Left = 240
            Top = 60
            Width = 72
            Height = 21
            DataField = 'Valor'
            DataSource = DsCNAB240
            TabOrder = 9
          end
          object DBEdit36: TDBEdit
            Left = 316
            Top = 60
            Width = 56
            Height = 21
            DataField = 'Emissao'
            DataSource = DsCNAB240
            TabOrder = 10
          end
          object DBEdit37: TDBEdit
            Left = 376
            Top = 60
            Width = 56
            Height = 21
            DataField = 'DCompra'
            DataSource = DsCNAB240
            TabOrder = 11
          end
          object DBEdit38: TDBEdit
            Left = 436
            Top = 60
            Width = 56
            Height = 21
            DataField = 'Vencto'
            DataSource = DsCNAB240
            TabOrder = 12
          end
          object DBEdit39: TDBEdit
            Left = 496
            Top = 60
            Width = 56
            Height = 21
            DataField = 'DDeposito'
            DataSource = DsCNAB240
            TabOrder = 13
          end
          object DBEdit40: TDBEdit
            Left = 596
            Top = 60
            Width = 56
            Height = 21
            DataField = 'DATA3_TXT'
            DataSource = DsCNAB240
            TabOrder = 14
          end
          object DBEdit41: TDBEdit
            Left = 556
            Top = 60
            Width = 36
            Height = 21
            DataField = 'Dias'
            DataSource = DsCNAB240
            TabOrder = 15
          end
          object DBEdit42: TDBEdit
            Left = 656
            Top = 60
            Width = 60
            Height = 21
            DataField = 'Cobranca'
            DataSource = DsCNAB240
            TabOrder = 16
          end
          object DBEdit43: TDBEdit
            Left = 716
            Top = 60
            Width = 281
            Height = 21
            DataField = 'NOMESTATUS'
            DataSource = DsCNAB240
            TabOrder = 17
          end
          object DBEdit44: TDBEdit
            Left = 8
            Top = 20
            Width = 72
            Height = 21
            DataField = 'Controle'
            DataSource = DsCNAB240
            TabOrder = 18
          end
          object DBEdit45: TDBEdit
            Left = 932
            Top = 100
            Width = 64
            Height = 21
            DataField = 'Custas'
            DataSource = DsCNAB240
            TabOrder = 19
          end
          object DBEdit46: TDBEdit
            Left = 508
            Top = 100
            Width = 304
            Height = 21
            DataField = 'Emitente'
            DataSource = DsCNAB240
            TabOrder = 20
          end
          object DBEdit47: TDBEdit
            Left = 816
            Top = 100
            Width = 113
            Height = 21
            DataField = 'CPF_TXT'
            DataSource = DsCNAB240
            TabOrder = 21
          end
          object DBEdit48: TDBEdit
            Left = 72
            Top = 100
            Width = 433
            Height = 21
            DataField = 'NOMECLI'
            DataSource = DsCNAB240
            TabOrder = 22
          end
          object DBEdit49: TDBEdit
            Left = 8
            Top = 100
            Width = 64
            Height = 21
            DataField = 'Cliente'
            DataSource = DsCNAB240
            TabOrder = 23
          end
        end
        object DBGrid1: TDBGrid
          Left = 0
          Top = 49
          Width = 1006
          Height = 204
          Align = alClient
          DataSource = DsCNAB240
          TabOrder = 3
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Meu ID'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLI'
              Title.Caption = 'Cliente'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Title.Caption = 'Sacado'
              Width = 120
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'CNAB240I'
              Title.Caption = 'Ctrl.ret.'
              Width = 37
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Lote'
              Width = 32
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Item'
              Width = 31
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataOcor'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Movimento'
              Width = 25
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEMOVIMENTO'
              Title.Caption = 'Descri'#231#227'o do movimento'
              Width = 200
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IRTCLB_TXT'
              Title.Caption = 'Informa'#231#227'o complementar'
              Width = 160
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValPago'
              Title.Caption = 'Val.Pago'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValCred'
              Title.Caption = 'Val.Cred'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEACAO'
              Title.Caption = 'OK?'
              Width = 24
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Cabe'#231'alho do arquivo lido'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 1006
          Height = 129
          Align = alTop
          TabOrder = 0
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 34
            Height = 13
            Caption = 'Banco:'
          end
          object Label2: TLabel
            Left = 196
            Top = 4
            Width = 77
            Height = 13
            Caption = 'Tipo de arquivo:'
          end
          object Label4: TLabel
            Left = 500
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object Label3: TLabel
            Left = 384
            Top = 4
            Width = 61
            Height = 13
            Caption = 'CPF / CNPJ:'
          end
          object Label5: TLabel
            Left = 668
            Top = 4
            Width = 48
            Height = 13
            Caption = 'Conv'#234'nio:'
          end
          object Label6: TLabel
            Left = 784
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Ag'#234'ncia:'
          end
          object Label7: TLabel
            Left = 864
            Top = 4
            Width = 73
            Height = 13
            Caption = 'Conta corrente:'
          end
          object Label8: TLabel
            Left = 980
            Top = 4
            Width = 18
            Height = 13
            Caption = 'V**:'
          end
          object Label9: TLabel
            Left = 8
            Top = 44
            Width = 68
            Height = 13
            Caption = 'Data gera'#231#227'o:'
          end
          object Label10: TLabel
            Left = 100
            Top = 44
            Width = 68
            Height = 13
            Caption = 'Hora gera'#231#227'o:'
          end
          object Label11: TLabel
            Left = 172
            Top = 44
            Width = 94
            Height = 13
            Caption = 'N'#250'mero sequencial:'
          end
          object Label12: TLabel
            Left = 276
            Top = 44
            Width = 67
            Height = 13
            Caption = 'Vers'#227'o layout:'
          end
          object Label13: TLabel
            Left = 348
            Top = 44
            Width = 102
            Height = 13
            Caption = 'Densidade grava'#231#227'o:'
          end
          object Label14: TLabel
            Left = 456
            Top = 44
            Width = 103
            Height = 13
            Caption = 'Reservado ao banco:'
          end
          object Label15: TLabel
            Left = 620
            Top = 44
            Width = 107
            Height = 13
            Caption = 'Reservado a empresa:'
          end
          object Label16: TLabel
            Left = 8
            Top = 84
            Width = 28
            Height = 13
            Caption = '*CSP:'
          end
          object Label19: TLabel
            Left = 44
            Top = 84
            Width = 11
            Height = 13
            Caption = '**:'
          end
          object Label20: TLabel
            Left = 80
            Top = 84
            Width = 76
            Height = 13
            Caption = 'Tipo de servi'#231'o:'
          end
          object Label21: TLabel
            Left = 388
            Top = 84
            Width = 114
            Height = 13
            Caption = 'C'#243'digo das ocorr'#234'ncias:'
          end
          object Label17: TLabel
            Left = 860
            Top = 48
            Width = 134
            Height = 13
            Caption = '*CSP - Cobran'#231'a sem papel.'
          end
          object Label18: TLabel
            Left = 860
            Top = 64
            Width = 126
            Height = 13
            Caption = '** Uso exclusivo das vans.'
          end
          object Label116: TLabel
            Left = 512
            Top = 84
            Width = 98
            Height = 13
            Caption = 'Qtd lotes no arquivo:'
          end
          object Label117: TLabel
            Left = 616
            Top = 84
            Width = 98
            Height = 13
            Caption = 'Qtd registros no arq.:'
          end
          object Label118: TLabel
            Left = 720
            Top = 84
            Width = 130
            Height = 13
            Caption = 'Qtd contas p/ conc. - lotes:'
          end
          object EdBcoCodH: TdmkEdit
            Left = 8
            Top = 20
            Width = 24
            Height = 21
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdBcoCodHChange
          end
          object EdBcoNomH: TdmkEdit
            Left = 32
            Top = 20
            Width = 160
            Height = 21
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdTipoArq: TdmkEdit
            Left = 196
            Top = 20
            Width = 105
            Height = 21
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdEmpresaCNPJH: TdmkEdit
            Left = 384
            Top = 20
            Width = 113
            Height = 21
            ReadOnly = True
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdEmpresaNomeH: TdmkEdit
            Left = 500
            Top = 20
            Width = 165
            Height = 21
            ReadOnly = True
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdConvenioH: TdmkEdit
            Left = 668
            Top = 20
            Width = 113
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdAgenciaNUH: TdmkEdit
            Left = 784
            Top = 20
            Width = 53
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdAgenciaDVH: TdmkEdit
            Left = 836
            Top = 20
            Width = 25
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdContaNUH: TdmkEdit
            Left = 864
            Top = 20
            Width = 89
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdContaDVH: TdmkEdit
            Left = 952
            Top = 20
            Width = 25
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdAgenCcDVH: TdmkEdit
            Left = 980
            Top = 20
            Width = 20
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 10
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdTipoRem: TdmkEdit
            Left = 300
            Top = 20
            Width = 81
            Height = 21
            ReadOnly = True
            TabOrder = 11
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdHoraGeraH: TdmkEdit
            Left = 100
            Top = 60
            Width = 69
            Height = 21
            Alignment = taCenter
            ReadOnly = True
            TabOrder = 12
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdDataGeraH: TdmkEdit
            Left = 8
            Top = 60
            Width = 89
            Height = 21
            Alignment = taCenter
            ReadOnly = True
            TabOrder = 13
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdSeqArq: TdmkEdit
            Left = 172
            Top = 60
            Width = 101
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 14
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLayoutArq: TdmkEdit
            Left = 276
            Top = 60
            Width = 69
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 15
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdDensiGrava: TdmkEdit
            Left = 348
            Top = 60
            Width = 105
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 16
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdReservaBanco: TdmkEdit
            Left = 456
            Top = 60
            Width = 160
            Height = 21
            ReadOnly = True
            TabOrder = 17
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdReservaEmpresa: TdmkEdit
            Left = 620
            Top = 60
            Width = 157
            Height = 21
            ReadOnly = True
            TabOrder = 18
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCSP: TdmkEdit
            Left = 8
            Top = 100
            Width = 33
            Height = 21
            Alignment = taCenter
            ReadOnly = True
            TabOrder = 19
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdVans: TdmkEdit
            Left = 44
            Top = 100
            Width = 33
            Height = 21
            Alignment = taCenter
            ReadOnly = True
            TabOrder = 20
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdTipoServico: TdmkEdit
            Left = 80
            Top = 100
            Width = 24
            Height = 21
            Alignment = taCenter
            ReadOnly = True
            TabOrder = 21
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdTipoServicoChange
          end
          object EdCodigoOcorr: TdmkEdit
            Left = 388
            Top = 100
            Width = 117
            Height = 21
            Alignment = taCenter
            ReadOnly = True
            TabOrder = 22
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdNomeServico: TdmkEdit
            Left = 104
            Top = 100
            Width = 280
            Height = 21
            ReadOnly = True
            TabOrder = 23
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdArqQtdLotes: TdmkEdit
            Left = 512
            Top = 100
            Width = 101
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 24
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdArqQtdRegistros: TdmkEdit
            Left = 616
            Top = 100
            Width = 101
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 25
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdArqQtdContas: TdmkEdit
            Left = 720
            Top = 100
            Width = 129
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 26
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object Memo1: TMemo
          Left = 0
          Top = 129
          Width = 1006
          Height = 279
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -9
          Font.Name = 'Lucida Console'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ScrollBars = ssBoth
          TabOrder = 1
          WantReturns = False
          OnChange = Memo1Change
          OnClick = Memo1Click
          OnEnter = Memo1Enter
          OnKeyDown = Memo1KeyDown
          OnKeyUp = Memo1KeyUp
        end
        object StatusBar: TStatusBar
          Left = 0
          Top = 408
          Width = 1006
          Height = 19
          Panels = <
            item
              Text = ' Coluna inicial:'
              Width = 72
            end
            item
              Alignment = taCenter
              Text = '0'
              Width = 50
            end
            item
              Text = ' Linha inicial:'
              Width = 68
            end
            item
              Alignment = taCenter
              Text = '0'
              Width = 50
            end
            item
              Text = ' Coluna final:'
              Width = 72
            end
            item
              Alignment = taCenter
              Text = '0'
              Width = 50
            end
            item
              Text = ' Linha final:'
              Width = 64
            end
            item
              Alignment = taCenter
              Text = '0'
              Width = 50
            end
            item
              Text = ' Tamanho:'
              Width = 60
            end
            item
              Alignment = taCenter
              Text = '0'
              Width = 50
            end
            item
              Width = 50
            end>
        end
      end
      object TabSheet8: TTabSheet
        Caption = 'Lotes do arquivo rec'#233'm lido'
        ImageIndex = 4
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object Label45: TLabel
          Left = 0
          Top = 366
          Width = 1006
          Height = 13
          Align = alBottom
          Caption = 
            'VLL*: N'#186' da vers'#227'o do layout do lote   V**: Verifica'#231#227'o de ag'#234'nc' +
            'ia e conta.  **FEBRABAN  CNAB *IRTCLB: Identifica'#231#227'o de rejei'#231#245'e' +
            's, tarifas, custas, liquid. baixas.'
          ExplicitWidth = 771
        end
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 1006
          Height = 125
          Align = alTop
          TabOrder = 0
          object Label22: TLabel
            Left = 8
            Top = 4
            Width = 34
            Height = 13
            Caption = 'Banco:'
          end
          object Label24: TLabel
            Left = 208
            Top = 4
            Width = 61
            Height = 13
            Caption = 'CPF / CNPJ:'
          end
          object Label25: TLabel
            Left = 324
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Empresa:'
          end
          object Label30: TLabel
            Left = 436
            Top = 44
            Width = 55
            Height = 13
            Caption = 'Mensagem:'
          end
          object Label40: TLabel
            Left = 964
            Top = 4
            Width = 26
            Height = 13
            Caption = 'VLL*:'
          end
          object Label34: TLabel
            Left = 8
            Top = 44
            Width = 95
            Height = 13
            Caption = 'N'#186' remessa/retorno:'
          end
          object Label26: TLabel
            Left = 560
            Top = 4
            Width = 48
            Height = 13
            Caption = 'Conv'#234'nio:'
          end
          object Label27: TLabel
            Left = 708
            Top = 4
            Width = 42
            Height = 13
            Caption = 'Ag'#234'ncia:'
          end
          object Label28: TLabel
            Left = 788
            Top = 4
            Width = 73
            Height = 13
            Caption = 'Conta corrente:'
          end
          object Label29: TLabel
            Left = 904
            Top = 4
            Width = 18
            Height = 13
            Caption = 'V**:'
          end
          object Label35: TLabel
            Left = 804
            Top = 82
            Width = 149
            Height = 13
            Caption = 'Reservado FEBRABAN/CNAB:'
          end
          object Label33: TLabel
            Left = 196
            Top = 44
            Width = 76
            Height = 13
            Caption = 'Data do cr'#233'dito:'
          end
          object Label32: TLabel
            Left = 108
            Top = 44
            Width = 69
            Height = 13
            Caption = 'Data rem./ret.:'
          end
          object Label41: TLabel
            Left = 928
            Top = 4
            Width = 11
            Height = 13
            Caption = '**:'
          end
          object Label23: TLabel
            Left = 8
            Top = 82
            Width = 87
            Height = 13
            Caption = 'Tipo de opera'#231#227'o:'
          end
          object Label38: TLabel
            Left = 292
            Top = 82
            Width = 76
            Height = 13
            Caption = 'Tipo de servi'#231'o:'
          end
          object Label39: TLabel
            Left = 580
            Top = 82
            Width = 105
            Height = 13
            Caption = 'Forma de lan'#231'amento:'
          end
          object EdBcoCodL: TdmkEdit
            Left = 8
            Top = 20
            Width = 24
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdBcoCodLChange
          end
          object EdBcoNomL: TdmkEdit
            Left = 32
            Top = 20
            Width = 160
            Height = 21
            ReadOnly = True
            TabOrder = 1
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdEmpresaCNPJL: TdmkEdit
            Left = 208
            Top = 20
            Width = 113
            Height = 21
            ReadOnly = True
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdMensagem1: TdmkEdit
            Left = 436
            Top = 60
            Width = 280
            Height = 21
            ReadOnly = True
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdMensagem2: TdmkEdit
            Left = 716
            Top = 60
            Width = 282
            Height = 21
            ReadOnly = True
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdRemRet: TdmkEdit
            Left = 8
            Top = 60
            Width = 97
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdConvenioL: TdmkEdit
            Left = 560
            Top = 20
            Width = 144
            Height = 21
            ReadOnly = True
            TabOrder = 6
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdEmpresaNomeL: TdmkEdit
            Left = 324
            Top = 20
            Width = 232
            Height = 21
            ReadOnly = True
            TabOrder = 7
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdAgenciaNUL: TdmkEdit
            Left = 708
            Top = 20
            Width = 53
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 8
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdAgenciaDVL: TdmkEdit
            Left = 760
            Top = 20
            Width = 25
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 9
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdContaNUL: TdmkEdit
            Left = 788
            Top = 20
            Width = 89
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 10
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdContaDVL: TdmkEdit
            Left = 876
            Top = 20
            Width = 25
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 11
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdAgenCcDVL: TdmkEdit
            Left = 904
            Top = 20
            Width = 20
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 12
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCNABLot2: TEdit
            Left = 804
            Top = 98
            Width = 193
            Height = 21
            ReadOnly = True
            TabOrder = 13
          end
          object EdHoraGeraL: TdmkEdit
            Left = 196
            Top = 60
            Width = 85
            Height = 21
            Alignment = taCenter
            ReadOnly = True
            TabOrder = 14
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdDataGeraL: TdmkEdit
            Left = 108
            Top = 60
            Width = 85
            Height = 21
            Alignment = taCenter
            ReadOnly = True
            TabOrder = 15
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdLayoutLot: TdmkEdit
            Left = 964
            Top = 20
            Width = 33
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 16
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdCNABLot1: TdmkEdit
            Left = 928
            Top = 20
            Width = 33
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 17
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdOperacao: TdmkEdit
            Left = 8
            Top = 98
            Width = 24
            Height = 21
            Alignment = taCenter
            ReadOnly = True
            TabOrder = 18
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdOperacaoChange
          end
          object EdOperacaoTxt: TdmkEdit
            Left = 32
            Top = 98
            Width = 257
            Height = 21
            ReadOnly = True
            TabOrder = 19
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdServico: TdmkEdit
            Left = 292
            Top = 98
            Width = 24
            Height = 21
            Alignment = taCenter
            ReadOnly = True
            TabOrder = 20
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdServicoChange
          end
          object EdServicoTxt: TdmkEdit
            Left = 316
            Top = 98
            Width = 261
            Height = 21
            ReadOnly = True
            TabOrder = 21
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
          object EdFormLanc: TdmkEdit
            Left = 580
            Top = 98
            Width = 24
            Height = 21
            Alignment = taCenter
            ReadOnly = True
            TabOrder = 22
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdFormLancChange
          end
          object EdFormLancTxt: TdmkEdit
            Left = 604
            Top = 98
            Width = 197
            Height = 21
            ReadOnly = True
            TabOrder = 23
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
          end
        end
        object PainelControle: TPanel
          Left = 0
          Top = 379
          Width = 1006
          Height = 48
          Align = alBottom
          TabOrder = 1
          object LaRegistroB: TLabel
            Left = 351
            Top = 1
            Width = 220
            Height = 42
            Align = alLeft
            Caption = '00000/00000'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -37
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object LaRegistroA: TLabel
            Left = 173
            Top = 1
            Width = 178
            Height = 42
            Align = alLeft
            Caption = '0000/0000'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -37
            Font.Name = 'Arial'
            Font.Style = []
            ParentFont = False
          end
          object Panel5: TPanel
            Left = 1
            Top = 1
            Width = 172
            Height = 46
            Align = alLeft
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 0
            object SpeedButton4: TBitBtn
              Tag = 4
              Left = 128
              Top = 4
              Width = 40
              Height = 40
              Cursor = crHandPoint
              Hint = 'ltimo registro'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = SpeedButton4Click
            end
            object SpeedButton3: TBitBtn
              Tag = 3
              Left = 88
              Top = 4
              Width = 40
              Height = 40
              Cursor = crHandPoint
              Hint = 'Pr'#243'ximo registro'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = SpeedButton3Click
            end
            object SpeedButton2: TBitBtn
              Tag = 2
              Left = 48
              Top = 4
              Width = 40
              Height = 40
              Cursor = crHandPoint
              Hint = 'Registro anterior'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = SpeedButton2Click
            end
            object SpeedButton1: TBitBtn
              Tag = 1
              Left = 8
              Top = 4
              Width = 40
              Height = 40
              Cursor = crHandPoint
              Hint = 'Primeiro registro'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 3
              OnClick = SpeedButton1Click
            end
          end
          object Panel2: TPanel
            Left = 571
            Top = 1
            Width = 434
            Height = 46
            Align = alClient
            BevelOuter = bvNone
            ParentColor = True
            TabOrder = 1
            object Panel6: TPanel
              Left = 325
              Top = 0
              Width = 109
              Height = 46
              Align = alRight
              TabOrder = 0
              object BtSaida: TBitBtn
                Tag = 13
                Left = 8
                Top = 3
                Width = 90
                Height = 40
                Cursor = crHandPoint
                Hint = 'Sai da janela atual'
                Caption = '&Sa'#237'da'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtSaidaClick
              end
            end
            object BitBtn9: TBitBtn
              Tag = 1
              Left = 8
              Top = 4
              Width = 40
              Height = 40
              Cursor = crHandPoint
              Hint = 'Primeiro registro'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 1
              OnClick = BitBtn9Click
            end
            object BitBtn10: TBitBtn
              Tag = 2
              Left = 48
              Top = 4
              Width = 40
              Height = 40
              Cursor = crHandPoint
              Hint = 'Registro anterior'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 2
              OnClick = BitBtn10Click
            end
            object BitBtn11: TBitBtn
              Tag = 3
              Left = 88
              Top = 4
              Width = 40
              Height = 40
              Cursor = crHandPoint
              Hint = 'Pr'#243'ximo registro'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 3
              OnClick = BitBtn11Click
            end
            object BitBtn12: TBitBtn
              Tag = 4
              Left = 128
              Top = 4
              Width = 40
              Height = 40
              Cursor = crHandPoint
              Hint = 'ltimo registro'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 4
              OnClick = BitBtn12Click
            end
            object BtGrava: TBitBtn
              Tag = 11
              Left = 172
              Top = 4
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Caption = '&Grava'
              Enabled = False
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 5
              OnClick = BtGravaClick
            end
          end
        end
        object PageControl2: TPageControl
          Left = 0
          Top = 125
          Width = 1006
          Height = 153
          ActivePage = TabSheet5
          Align = alTop
          TabOrder = 2
          object TabSheet5: TTabSheet
            Caption = 'Segmento P'
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel9: TPanel
              Left = 0
              Top = 0
              Width = 998
              Height = 125
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label146: TLabel
                Left = 4
                Top = 4
                Width = 34
                Height = 13
                Caption = 'Banco:'
              end
              object Label148: TLabel
                Left = 160
                Top = 4
                Width = 24
                Height = 13
                Caption = 'Lote:'
              end
              object Label149: TLabel
                Left = 196
                Top = 4
                Width = 45
                Height = 13
                Caption = 'Reg.Seq.'
              end
              object Label150: TLabel
                Left = 244
                Top = 4
                Width = 11
                Height = 13
                Caption = '**:'
              end
              object Label151: TLabel
                Left = 268
                Top = 4
                Width = 99
                Height = 13
                Caption = 'Movimento de envio:'
              end
              object Label152: TLabel
                Left = 628
                Top = 4
                Width = 42
                Height = 13
                Caption = 'Ag'#234'ncia:'
              end
              object Label153: TLabel
                Left = 708
                Top = 4
                Width = 73
                Height = 13
                Caption = 'Conta corrente:'
              end
              object Label154: TLabel
                Left = 824
                Top = 4
                Width = 18
                Height = 13
                Caption = 'V**:'
              end
              object Label155: TLabel
                Left = 848
                Top = 4
                Width = 125
                Height = 13
                Caption = 'Identif. do t'#237'tulo no banco:'
              end
              object Label158: TLabel
                Left = 700
                Top = 44
                Width = 42
                Height = 13
                Caption = 'Ag'#234'ncia:'
              end
              object Label161: TLabel
                Left = 816
                Top = 84
                Width = 135
                Height = 13
                Caption = 'Identif. do t'#237'tulo na empresa:'
              end
              object Label164: TLabel
                Left = 408
                Top = 44
                Width = 85
                Height = 13
                Caption = 'Valor nomin.t'#237'tulo:'
              end
              object Label167: TLabel
                Left = 332
                Top = 44
                Width = 69
                Height = 13
                Caption = 'Data venc.t'#237't.:'
              end
              object Label169: TLabel
                Left = 220
                Top = 44
                Width = 89
                Height = 13
                Caption = 'Doc. de cobran'#231'a:'
              end
              object Label170: TLabel
                Left = 4
                Top = 44
                Width = 39
                Height = 13
                Caption = 'Carteira:'
              end
              object EdBcoCodDetP: TdmkEdit
                Left = 4
                Top = 20
                Width = 24
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdBcoCodDetPChange
              end
              object EdBcoNomDetP: TdmkEdit
                Left = 28
                Top = 20
                Width = 128
                Height = 21
                ReadOnly = True
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdLotDetP: TdmkEdit
                Left = 160
                Top = 20
                Width = 33
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdSeqDetP: TdmkEdit
                Left = 196
                Top = 20
                Width = 45
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCNABLot009: TdmkEdit
                Left = 244
                Top = 20
                Width = 21
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCodMovDetP: TdmkEdit
                Left = 268
                Top = 20
                Width = 25
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdCodMovDetPChange
              end
              object EdTexMovDetP: TdmkEdit
                Left = 292
                Top = 20
                Width = 333
                Height = 21
                ReadOnly = True
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdAgenciaNUP: TdmkEdit
                Left = 628
                Top = 20
                Width = 53
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdAgenciaDVP: TdmkEdit
                Left = 680
                Top = 20
                Width = 25
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 8
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdContaNUP: TdmkEdit
                Left = 708
                Top = 20
                Width = 89
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 9
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdContaDVP: TdmkEdit
                Left = 796
                Top = 20
                Width = 25
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 10
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdAgenCcDVP: TdmkEdit
                Left = 824
                Top = 20
                Width = 20
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 11
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdIdeTitDetP: TdmkEdit
                Left = 848
                Top = 20
                Width = 144
                Height = 21
                ReadOnly = True
                TabOrder = 12
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdAgenciaDVP2: TdmkEdit
                Left = 752
                Top = 60
                Width = 25
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 19
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdAgenciaNUP2: TdmkEdit
                Left = 700
                Top = 60
                Width = 53
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 18
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdIDTiEDetP: TdmkEdit
                Left = 816
                Top = 100
                Width = 176
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 20
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdIDTiEDetPChange
              end
              object EdValNomDetP: TdmkEdit
                Left = 408
                Top = 60
                Width = 89
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 17
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdVenTitDetP: TdmkEdit
                Left = 332
                Top = 60
                Width = 72
                Height = 21
                Alignment = taCenter
                ReadOnly = True
                TabOrder = 16
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDocCobDetP: TdmkEdit
                Left = 220
                Top = 60
                Width = 108
                Height = 21
                ReadOnly = True
                TabOrder = 15
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdTexCarDetP: TdmkEdit
                Left = 24
                Top = 60
                Width = 193
                Height = 21
                ReadOnly = True
                TabOrder = 14
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCodCarDetP: TdmkEdit
                Left = 4
                Top = 60
                Width = 20
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 13
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdCodCarDetPChange
              end
            end
          end
          object TabSheet6: TTabSheet
            Caption = 'Segmento Q'
            ImageIndex = 1
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Label156: TLabel
              Left = 4
              Top = 4
              Width = 34
              Height = 13
              Caption = 'Banco:'
            end
            object Label157: TLabel
              Left = 160
              Top = 4
              Width = 24
              Height = 13
              Caption = 'Lote:'
            end
            object Label159: TLabel
              Left = 196
              Top = 4
              Width = 45
              Height = 13
              Caption = 'Reg.Seq.'
            end
            object Label160: TLabel
              Left = 244
              Top = 4
              Width = 11
              Height = 13
              Caption = '**:'
            end
            object Label162: TLabel
              Left = 268
              Top = 4
              Width = 99
              Height = 13
              Caption = 'Movimento de envio:'
            end
            object Label163: TLabel
              Left = 628
              Top = 4
              Width = 61
              Height = 13
              Caption = 'CPF / CNPJ:'
            end
            object Label165: TLabel
              Left = 4
              Top = 44
              Width = 80
              Height = 13
              Caption = 'Nome do cliente:'
            end
            object Label166: TLabel
              Left = 296
              Top = 44
              Width = 49
              Height = 13
              Caption = 'Endere'#231'o:'
            end
            object Label168: TLabel
              Left = 588
              Top = 44
              Width = 30
              Height = 13
              Caption = 'Bairro:'
            end
            object Label171: TLabel
              Left = 700
              Top = 44
              Width = 24
              Height = 13
              Caption = 'CEP:'
            end
            object Label191: TLabel
              Left = 772
              Top = 44
              Width = 36
              Height = 13
              Caption = 'Cidade:'
            end
            object Label192: TLabel
              Left = 884
              Top = 44
              Width = 17
              Height = 13
              Caption = 'UF:'
            end
            object Label193: TLabel
              Left = 4
              Top = 84
              Width = 134
              Height = 13
              Caption = 'Nome do sacador / avalista:'
            end
            object Label194: TLabel
              Left = 296
              Top = 84
              Width = 61
              Height = 13
              Caption = 'CPF / CNPJ:'
            end
            object Label195: TLabel
              Left = 412
              Top = 84
              Width = 196
              Height = 13
              Caption = 'Banco correspondente na compensa'#231#227'o:'
            end
            object Label196: TLabel
              Left = 612
              Top = 84
              Width = 133
              Height = 13
              Caption = 'Nosso n'#250'mero bco corresp.:'
            end
            object Label198: TLabel
              Left = 760
              Top = 84
              Width = 11
              Height = 13
              Caption = '**:'
            end
            object EdBcoCodDetQ: TdmkEdit
              Left = 4
              Top = 20
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdBcoCodDetQChange
            end
            object EdBcoNomDetQ: TdmkEdit
              Left = 28
              Top = 20
              Width = 128
              Height = 21
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLotDetQ: TdmkEdit
              Left = 160
              Top = 20
              Width = 33
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdSeqDetQ: TdmkEdit
              Left = 196
              Top = 20
              Width = 45
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCNABLot010: TdmkEdit
              Left = 244
              Top = 20
              Width = 21
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCodMovDetQ: TdmkEdit
              Left = 268
              Top = 20
              Width = 25
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCodMovDetQChange
            end
            object EdTexMovDetQ: TdmkEdit
              Left = 292
              Top = 20
              Width = 333
              Height = 21
              ReadOnly = True
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCPFCNPDetQ: TdmkEdit
              Left = 628
              Top = 20
              Width = 113
              Height = 21
              ReadOnly = True
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEmiCliDetQ: TdmkEdit
              Left = 4
              Top = 60
              Width = 288
              Height = 21
              ReadOnly = True
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEmiEndDetQ: TdmkEdit
              Left = 296
              Top = 60
              Width = 288
              Height = 21
              ReadOnly = True
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEmiBaiDetQ: TdmkEdit
              Left = 588
              Top = 60
              Width = 108
              Height = 21
              ReadOnly = True
              TabOrder = 10
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEmiCEPDetQ: TdmkEdit
              Left = 700
              Top = 60
              Width = 40
              Height = 21
              ReadOnly = True
              TabOrder = 11
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEmiSufDetQ: TdmkEdit
              Left = 740
              Top = 60
              Width = 28
              Height = 21
              ReadOnly = True
              TabOrder = 12
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEmiCidDetQ: TdmkEdit
              Left = 772
              Top = 60
              Width = 108
              Height = 21
              ReadOnly = True
              TabOrder = 13
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEmi_UFDetQ: TdmkEdit
              Left = 884
              Top = 60
              Width = 24
              Height = 21
              ReadOnly = True
              TabOrder = 14
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEmiSacDetQ: TdmkEdit
              Left = 4
              Top = 100
              Width = 288
              Height = 21
              ReadOnly = True
              TabOrder = 15
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEmiCPFDetQ: TdmkEdit
              Left = 296
              Top = 100
              Width = 113
              Height = 21
              ReadOnly = True
              TabOrder = 16
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEmiBCCDetQ: TdmkEdit
              Left = 412
              Top = 100
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 17
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdEmiBCCDetQChange
            end
            object EdEmiNBCDetQ: TdmkEdit
              Left = 436
              Top = 100
              Width = 173
              Height = 21
              ReadOnly = True
              TabOrder = 18
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEmiNNBDetQ: TdmkEdit
              Left = 612
              Top = 100
              Width = 144
              Height = 21
              ReadOnly = True
              TabOrder = 19
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCNABLot011: TdmkEdit
              Left = 760
              Top = 100
              Width = 60
              Height = 21
              ReadOnly = True
              TabOrder = 20
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object TabSheet7: TTabSheet
            Caption = 'Segmento R'
            ImageIndex = 2
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Label197: TLabel
              Left = 4
              Top = 4
              Width = 34
              Height = 13
              Caption = 'Banco:'
            end
            object Label199: TLabel
              Left = 160
              Top = 4
              Width = 24
              Height = 13
              Caption = 'Lote:'
            end
            object Label200: TLabel
              Left = 196
              Top = 4
              Width = 45
              Height = 13
              Caption = 'Reg.Seq.'
            end
            object Label201: TLabel
              Left = 244
              Top = 4
              Width = 11
              Height = 13
              Caption = '**:'
            end
            object Label202: TLabel
              Left = 268
              Top = 4
              Width = 99
              Height = 13
              Caption = 'Movimento de envio:'
            end
            object Label203: TLabel
              Left = 4
              Top = 44
              Width = 107
              Height = 13
              Caption = 'C'#243'digo de desconto 2:'
            end
            object Label204: TLabel
              Left = 160
              Top = 44
              Width = 64
              Height = 13
              Caption = 'Data desc. 2:'
            end
            object Label205: TLabel
              Left = 236
              Top = 44
              Width = 73
              Height = 13
              Caption = 'Val/% conced.:'
            end
            object Label206: TLabel
              Left = 4
              Top = 84
              Width = 107
              Height = 13
              Caption = 'C'#243'digo de desconto 3:'
            end
            object Label207: TLabel
              Left = 160
              Top = 84
              Width = 64
              Height = 13
              Caption = 'Data desc. 3:'
            end
            object Label208: TLabel
              Left = 236
              Top = 84
              Width = 73
              Height = 13
              Caption = 'Val/% conced.:'
            end
            object Label209: TLabel
              Left = 628
              Top = 4
              Width = 79
              Height = 13
              Caption = 'C'#243'digo da multa:'
            end
            object Label210: TLabel
              Left = 784
              Top = 4
              Width = 54
              Height = 13
              Caption = 'Data multa:'
            end
            object Label211: TLabel
              Left = 860
              Top = 4
              Width = 59
              Height = 13
              Caption = 'Val/% multa:'
            end
            object Label212: TLabel
              Left = 316
              Top = 44
              Width = 80
              Height = 13
              Caption = 'Info bco ao sac.:'
            end
            object Label213: TLabel
              Left = 396
              Top = 44
              Width = 64
              Height = 13
              Caption = 'Mensagem 3:'
            end
            object Label214: TLabel
              Left = 692
              Top = 44
              Width = 64
              Height = 13
              Caption = 'Mensagem 4:'
            end
            object Label215: TLabel
              Left = 316
              Top = 84
              Width = 34
              Height = 13
              Caption = 'Banco:'
            end
            object Label216: TLabel
              Left = 472
              Top = 84
              Width = 42
              Height = 13
              Caption = 'Ag'#234'ncia:'
            end
            object Label217: TLabel
              Left = 520
              Top = 84
              Width = 73
              Height = 13
              Caption = 'Conta corrente:'
            end
            object Label218: TLabel
              Left = 624
              Top = 84
              Width = 67
              Height = 13
              Caption = 'Ocor. sacado:'
            end
            object Label219: TLabel
              Left = 696
              Top = 84
              Width = 11
              Height = 13
              Caption = '**:'
            end
            object EdBcoCodDetR: TdmkEdit
              Left = 4
              Top = 20
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdBcoCodDetRChange
            end
            object EdBcoNomDetR: TdmkEdit
              Left = 28
              Top = 20
              Width = 128
              Height = 21
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLotDetR: TdmkEdit
              Left = 160
              Top = 20
              Width = 33
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdSeqDetR: TdmkEdit
              Left = 196
              Top = 20
              Width = 45
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCNABLot012: TdmkEdit
              Left = 244
              Top = 20
              Width = 21
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCodMovDetR: TdmkEdit
              Left = 268
              Top = 20
              Width = 25
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCodMovDetRChange
            end
            object EdTexMovDetR: TdmkEdit
              Left = 292
              Top = 20
              Width = 333
              Height = 21
              ReadOnly = True
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCodDesco2R: TdmkEdit
              Left = 4
              Top = 60
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCodDesco2RChange
            end
            object EdNomDesco2R: TdmkEdit
              Left = 28
              Top = 60
              Width = 128
              Height = 21
              ReadOnly = True
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdDatDesco2R: TdmkEdit
              Left = 160
              Top = 60
              Width = 72
              Height = 21
              Alignment = taCenter
              ReadOnly = True
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdValPerDe2R: TdmkEdit
              Left = 236
              Top = 60
              Width = 77
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 10
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCodDesco3R: TdmkEdit
              Left = 4
              Top = 100
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 11
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCodDesco3RChange
            end
            object EdNomDesco3R: TdmkEdit
              Left = 28
              Top = 100
              Width = 128
              Height = 21
              ReadOnly = True
              TabOrder = 12
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdDatDesco3R: TdmkEdit
              Left = 160
              Top = 100
              Width = 72
              Height = 21
              Alignment = taCenter
              ReadOnly = True
              TabOrder = 13
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdValPerDe3R: TdmkEdit
              Left = 236
              Top = 100
              Width = 77
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 14
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCodiMultaR: TdmkEdit
              Left = 628
              Top = 20
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 15
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCodiMultaRChange
            end
            object EdNomeMultaR: TdmkEdit
              Left = 652
              Top = 20
              Width = 128
              Height = 21
              ReadOnly = True
              TabOrder = 16
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdDataMultaR: TdmkEdit
              Left = 784
              Top = 20
              Width = 72
              Height = 21
              Alignment = taCenter
              ReadOnly = True
              TabOrder = 17
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdVaPeMultaR: TdmkEdit
              Left = 860
              Top = 20
              Width = 77
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 18
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdInfBcoSacR: TEdit
              Left = 316
              Top = 60
              Width = 76
              Height = 21
              ReadOnly = True
              TabOrder = 19
            end
            object EdMensagem3R: TEdit
              Left = 396
              Top = 60
              Width = 292
              Height = 21
              ReadOnly = True
              TabOrder = 20
            end
            object EdMensagem4R: TEdit
              Left = 692
              Top = 60
              Width = 292
              Height = 21
              ReadOnly = True
              TabOrder = 21
            end
            object EdConDebBcoR: TdmkEdit
              Left = 316
              Top = 100
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 22
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdConDebBcoRChange
            end
            object EdConDebBNoR: TdmkEdit
              Left = 340
              Top = 100
              Width = 130
              Height = 21
              ReadOnly = True
              TabOrder = 23
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdConDebAgeR: TdmkEdit
              Left = 472
              Top = 100
              Width = 46
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 24
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdBcoCodDetRChange
            end
            object EdConDebCCoR: TdmkEdit
              Left = 520
              Top = 100
              Width = 100
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 25
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCodOcoSacR: TdmkEdit
              Left = 624
              Top = 100
              Width = 68
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 26
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCNABLot013: TdmkEdit
              Left = 696
              Top = 100
              Width = 244
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 27
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'Segmento S'
            ImageIndex = 3
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
          end
          object TabSheet4: TTabSheet
            Caption = 'Segmrnto T'
            ImageIndex = 4
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Panel8: TPanel
              Left = 0
              Top = 0
              Width = 998
              Height = 125
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              object Label36: TLabel
                Left = 4
                Top = 4
                Width = 34
                Height = 13
                Caption = 'Banco:'
              end
              object Label37: TLabel
                Left = 160
                Top = 4
                Width = 24
                Height = 13
                Caption = 'Lote:'
              end
              object Label42: TLabel
                Left = 196
                Top = 4
                Width = 45
                Height = 13
                Caption = 'Reg.Seq.'
              end
              object Label43: TLabel
                Left = 244
                Top = 4
                Width = 11
                Height = 13
                Caption = '**:'
              end
              object Label44: TLabel
                Left = 268
                Top = 4
                Width = 106
                Height = 13
                Caption = 'Movimento de retorno:'
              end
              object Label46: TLabel
                Left = 628
                Top = 4
                Width = 42
                Height = 13
                Caption = 'Ag'#234'ncia:'
              end
              object Label47: TLabel
                Left = 708
                Top = 4
                Width = 73
                Height = 13
                Caption = 'Conta corrente:'
              end
              object Label48: TLabel
                Left = 824
                Top = 4
                Width = 18
                Height = 13
                Caption = 'V**:'
              end
              object Label49: TLabel
                Left = 848
                Top = 4
                Width = 125
                Height = 13
                Caption = 'Identif. do t'#237'tulo no banco:'
              end
              object Label58: TLabel
                Left = 848
                Top = 44
                Width = 46
                Height = 13
                Caption = 'Inscri'#231#227'o:'
              end
              object Label57: TLabel
                Left = 780
                Top = 44
                Width = 36
                Height = 13
                Caption = 'Moeda:'
              end
              object Label55: TLabel
                Left = 700
                Top = 44
                Width = 42
                Height = 13
                Caption = 'Ag'#234'ncia:'
              end
              object Label142: TLabel
                Left = 532
                Top = 84
                Width = 220
                Height = 13
                Caption = 'Texto do *IRTCLB (informa'#231#227'o complementar):'
              end
              object Label63: TLabel
                Left = 752
                Top = 84
                Width = 11
                Height = 13
                Caption = '**:'
              end
              object Label56: TLabel
                Left = 816
                Top = 84
                Width = 135
                Height = 13
                Caption = 'Identif. do t'#237'tulo na empresa:'
              end
              object Label86: TLabel
                Left = 896
                Top = 44
                Width = 32
                Height = 13
                Caption = 'Isento.'
              end
              object Label54: TLabel
                Left = 500
                Top = 44
                Width = 138
                Height = 13
                Caption = 'Banco cobrador / recebedor:'
              end
              object Label53: TLabel
                Left = 408
                Top = 44
                Width = 85
                Height = 13
                Caption = 'Valor nomin.t'#237'tulo:'
              end
              object Label62: TLabel
                Left = 456
                Top = 84
                Width = 45
                Height = 13
                Caption = '*IRTCLB:'
              end
              object Label61: TLabel
                Left = 364
                Top = 84
                Width = 89
                Height = 13
                Caption = 'Valor tarifa/custas:'
              end
              object Label52: TLabel
                Left = 332
                Top = 44
                Width = 69
                Height = 13
                Caption = 'Data venc.t'#237't.:'
              end
              object Label60: TLabel
                Left = 288
                Top = 84
                Width = 70
                Height = 13
                Caption = 'Contr. Op. Cr'#233':'
              end
              object Label51: TLabel
                Left = 220
                Top = 44
                Width = 89
                Height = 13
                Caption = 'Doc. de cobran'#231'a:'
              end
              object Label50: TLabel
                Left = 4
                Top = 44
                Width = 39
                Height = 13
                Caption = 'Carteira:'
              end
              object Label59: TLabel
                Left = 4
                Top = 84
                Width = 31
                Height = 13
                Caption = 'Nome:'
              end
              object EdBcoCodDetT: TdmkEdit
                Left = 4
                Top = 20
                Width = 24
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdBcoCodDetTChange
              end
              object EdBcoNomDetT: TdmkEdit
                Left = 28
                Top = 20
                Width = 128
                Height = 21
                ReadOnly = True
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdLotDetT: TdmkEdit
                Left = 160
                Top = 20
                Width = 33
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdSeqDetT: TdmkEdit
                Left = 196
                Top = 20
                Width = 45
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCNABLot3: TdmkEdit
                Left = 244
                Top = 20
                Width = 21
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 4
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCodMovDetT: TdmkEdit
                Left = 268
                Top = 20
                Width = 25
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 5
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdCodMovDetTChange
              end
              object EdTexMovDetT: TdmkEdit
                Left = 292
                Top = 20
                Width = 333
                Height = 21
                ReadOnly = True
                TabOrder = 6
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdAgenciaNUT: TdmkEdit
                Left = 628
                Top = 20
                Width = 53
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 7
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdAgenciaDVT: TdmkEdit
                Left = 680
                Top = 20
                Width = 25
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 8
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdContaNUT: TdmkEdit
                Left = 708
                Top = 20
                Width = 89
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 9
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdContaDVT: TdmkEdit
                Left = 796
                Top = 20
                Width = 25
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 10
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdAgenCcDVT: TdmkEdit
                Left = 824
                Top = 20
                Width = 20
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 11
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdIdeTitDetT: TdmkEdit
                Left = 848
                Top = 20
                Width = 144
                Height = 21
                ReadOnly = True
                TabOrder = 12
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdNumInsDetT: TdmkEdit
                Left = 864
                Top = 60
                Width = 128
                Height = 21
                ReadOnly = True
                TabOrder = 13
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdTipInsDetT: TdmkEdit
                Left = 848
                Top = 60
                Width = 17
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 14
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdTipInsDetTChange
              end
              object EdTxtMoeDetT: TdmkEdit
                Left = 800
                Top = 60
                Width = 45
                Height = 21
                ReadOnly = True
                TabOrder = 15
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdCodMoeDetTChange
              end
              object EdCodMoeDetT: TdmkEdit
                Left = 780
                Top = 60
                Width = 21
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 16
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdCodMoeDetTChange
              end
              object EdAgenciaDVT2: TdmkEdit
                Left = 752
                Top = 60
                Width = 25
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 17
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdAgenciaNUT2: TdmkEdit
                Left = 700
                Top = 60
                Width = 53
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 18
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdBcoNomDetT2: TdmkEdit
                Left = 536
                Top = 60
                Width = 161
                Height = 21
                ReadOnly = True
                TabOrder = 19
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdIRTCLB_TXT: TdmkEdit
                Left = 532
                Top = 100
                Width = 217
                Height = 21
                ReadOnly = True
                TabOrder = 20
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCNABLot4: TdmkEdit
                Left = 752
                Top = 100
                Width = 61
                Height = 21
                ReadOnly = True
                TabOrder = 21
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdIDTiEDetT: TdmkEdit
                Left = 816
                Top = 100
                Width = 176
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 22
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdIDTiEDetTChange
              end
              object EdBcoCodDetT2: TdmkEdit
                Left = 500
                Top = 60
                Width = 33
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 23
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdBcoCodDetT2Change
              end
              object EdValNomDetT: TdmkEdit
                Left = 408
                Top = 60
                Width = 89
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 24
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdIdRTCLDetT: TdmkEdit
                Left = 456
                Top = 100
                Width = 72
                Height = 21
                ReadOnly = True
                TabOrder = 25
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdIdRTCLDetTChange
              end
              object EdTarCusDetT: TdmkEdit
                Left = 364
                Top = 100
                Width = 89
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 26
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdVenTitDetT: TdmkEdit
                Left = 332
                Top = 60
                Width = 72
                Height = 21
                Alignment = taCenter
                ReadOnly = True
                TabOrder = 27
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdNConOCDetT: TdmkEdit
                Left = 288
                Top = 100
                Width = 72
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 28
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdBcoCodDetTChange
              end
              object EdNomeDetT: TdmkEdit
                Left = 4
                Top = 100
                Width = 280
                Height = 21
                ReadOnly = True
                TabOrder = 29
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdDocCobDetT: TdmkEdit
                Left = 220
                Top = 60
                Width = 108
                Height = 21
                ReadOnly = True
                TabOrder = 30
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdTexCarDetT: TdmkEdit
                Left = 24
                Top = 60
                Width = 193
                Height = 21
                ReadOnly = True
                TabOrder = 31
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
              end
              object EdCodCarDetT: TdmkEdit
                Left = 4
                Top = 60
                Width = 20
                Height = 21
                Alignment = taRightJustify
                ReadOnly = True
                TabOrder = 32
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                ValWarn = False
                OnChange = EdCodCarDetTChange
              end
            end
          end
          object TabSheet10: TTabSheet
            Caption = 'Segmento U'
            ImageIndex = 5
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Label64: TLabel
              Left = 4
              Top = 4
              Width = 34
              Height = 13
              Caption = 'Banco:'
            end
            object Label69: TLabel
              Left = 4
              Top = 44
              Width = 76
              Height = 13
              Caption = 'Jur./multa/enc.:'
            end
            object Label77: TLabel
              Left = 4
              Top = 84
              Width = 74
              Height = 13
              Caption = 'Data ocorr'#234'nc.:'
            end
            object Label79: TLabel
              Left = 80
              Top = 84
              Width = 108
              Height = 13
              Caption = 'Ocorr'#234'ncia do sacado:'
            end
            object Label70: TLabel
              Left = 88
              Top = 44
              Width = 91
              Height = 13
              Caption = 'Desconto conced.:'
            end
            object Label65: TLabel
              Left = 160
              Top = 4
              Width = 24
              Height = 13
              Caption = 'Lote:'
            end
            object Label66: TLabel
              Left = 196
              Top = 4
              Width = 45
              Height = 13
              Caption = 'Reg.Seq.'
            end
            object Label67: TLabel
              Left = 244
              Top = 4
              Width = 11
              Height = 13
              Caption = '**:'
            end
            object Label68: TLabel
              Left = 268
              Top = 4
              Width = 106
              Height = 13
              Caption = 'Movimento de retorno:'
            end
            object Label71: TLabel
              Left = 180
              Top = 44
              Width = 77
              Height = 13
              Caption = 'Abatim.conced.:'
            end
            object Label72: TLabel
              Left = 260
              Top = 44
              Width = 66
              Height = 13
              Caption = 'IOF recolhido:'
            end
            object Label73: TLabel
              Left = 332
              Top = 44
              Width = 83
              Height = 13
              Caption = 'Val.pago sacado:'
            end
            object Label74: TLabel
              Left = 424
              Top = 44
              Width = 78
              Height = 13
              Caption = 'Val.liq.creditado:'
            end
            object Label75: TLabel
              Left = 516
              Top = 44
              Width = 90
              Height = 13
              Caption = 'Val.outras despes.:'
            end
            object Label82: TLabel
              Left = 540
              Top = 84
              Width = 188
              Height = 13
              Caption = 'Complemento da ocorr'#234'ncia do sacado:'
            end
            object Label76: TLabel
              Left = 608
              Top = 44
              Width = 90
              Height = 13
              Caption = 'Val.outras receitas:'
            end
            object Label78: TLabel
              Left = 700
              Top = 44
              Width = 73
              Height = 13
              Caption = 'Dt. efetiv.cred.:'
            end
            object Label80: TLabel
              Left = 776
              Top = 84
              Width = 73
              Height = 13
              Caption = 'Data ocor.sac.:'
            end
            object Label81: TLabel
              Left = 852
              Top = 84
              Width = 89
              Height = 13
              Caption = 'Valor ocor.sacado:'
            end
            object Label84: TLabel
              Left = 836
              Top = 44
              Width = 145
              Height = 13
              Caption = 'Nosso n'#250'mero banco corresp.:'
            end
            object Label85: TLabel
              Left = 780
              Top = 4
              Width = 11
              Height = 13
              Caption = '**:'
            end
            object Label83: TLabel
              Left = 836
              Top = 4
              Width = 142
              Height = 13
              Caption = 'Banco correspond. compens.:'
            end
            object EdBcoCodDetU: TdmkEdit
              Left = 4
              Top = 20
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdBcoCodDetUChange
            end
            object EdBcoNomDetU: TdmkEdit
              Left = 28
              Top = 20
              Width = 128
              Height = 21
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdValJMEDetU: TdmkEdit
              Left = 4
              Top = 60
              Width = 81
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdDatOcoDetU: TdmkEdit
              Left = 4
              Top = 100
              Width = 72
              Height = 21
              Alignment = taCenter
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCodOcoDetU: TdmkEdit
              Left = 80
              Top = 100
              Width = 33
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCodOcoDetUChange
            end
            object EdTexOcoDetU: TdmkEdit
              Left = 112
              Top = 100
              Width = 425
              Height = 21
              ReadOnly = True
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdValDesDetU: TdmkEdit
              Left = 88
              Top = 60
              Width = 89
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLotDetU: TdmkEdit
              Left = 160
              Top = 20
              Width = 33
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdBcoCodLChange
            end
            object EdSeqDetU: TdmkEdit
              Left = 196
              Top = 20
              Width = 45
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdBcoCodLChange
            end
            object EdCNABLot5: TdmkEdit
              Left = 244
              Top = 20
              Width = 21
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdBcoCodLChange
            end
            object EdCodMovDetU: TdmkEdit
              Left = 268
              Top = 20
              Width = 25
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 10
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCodMovDetUChange
            end
            object EdTexMovDetU: TdmkEdit
              Left = 292
              Top = 20
              Width = 384
              Height = 21
              ReadOnly = True
              TabOrder = 11
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdValAbaDetU: TdmkEdit
              Left = 180
              Top = 60
              Width = 77
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 12
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdValIOFDetU: TdmkEdit
              Left = 260
              Top = 60
              Width = 69
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 13
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdValPagDetU: TdmkEdit
              Left = 332
              Top = 60
              Width = 89
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 14
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdValLiqDetU: TdmkEdit
              Left = 424
              Top = 60
              Width = 89
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 15
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdValOuDDetU: TdmkEdit
              Left = 516
              Top = 60
              Width = 89
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 16
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdComOcSDetU: TdmkEdit
              Left = 540
              Top = 100
              Width = 233
              Height = 21
              ReadOnly = True
              TabOrder = 17
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdValOuCDetU: TdmkEdit
              Left = 608
              Top = 60
              Width = 89
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 18
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdDatCreDetU: TdmkEdit
              Left = 700
              Top = 60
              Width = 72
              Height = 21
              Alignment = taCenter
              ReadOnly = True
              TabOrder = 19
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdDatOcSDetU: TdmkEdit
              Left = 776
              Top = 100
              Width = 72
              Height = 21
              Alignment = taCenter
              ReadOnly = True
              TabOrder = 20
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdValOcSDetU: TdmkEdit
              Left = 852
              Top = 100
              Width = 89
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 21
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdNNBCoCDetU: TdmkEdit
              Left = 836
              Top = 60
              Width = 157
              Height = 21
              ReadOnly = True
              TabOrder = 22
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCBcCoCDetU: TdmkEdit
              Left = 836
              Top = 20
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 23
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCBcCoCDetUChange
            end
            object EdCNABLot6: TdmkEdit
              Left = 780
              Top = 20
              Width = 52
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 24
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdNBcCoCDetU: TdmkEdit
              Left = 860
              Top = 20
              Width = 132
              Height = 21
              ReadOnly = True
              TabOrder = 25
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object TabSheet11: TTabSheet
            Caption = 'Trailer do lote'
            ImageIndex = 6
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Label106: TLabel
              Left = 4
              Top = 4
              Width = 34
              Height = 13
              Caption = 'Banco:'
            end
            object Label107: TLabel
              Left = 160
              Top = 4
              Width = 24
              Height = 13
              Caption = 'Lote:'
            end
            object Label108: TLabel
              Left = 196
              Top = 4
              Width = 11
              Height = 13
              Caption = '**:'
            end
            object Label115: TLabel
              Left = 4
              Top = 80
              Width = 156
              Height = 13
              Caption = 'N'#250'mero do aviso de lan'#231'amento:'
            end
            object Label109: TLabel
              Left = 4
              Top = 52
              Width = 188
              Height = 13
              Caption = 'Quantidade de registros do lan'#231'amento:'
            end
            object Label114: TLabel
              Left = 4
              Top = 104
              Width = 11
              Height = 13
              Caption = '**:'
            end
            object Label113: TLabel
              Left = 392
              Top = 80
              Width = 343
              Height = 13
              Caption = 
                'Quantidade dos t'#237'tulos em carteira e valor total dos t'#237'tulos em ' +
                'cobran'#231'a: '
            end
            object Label112: TLabel
              Left = 392
              Top = 56
              Width = 343
              Height = 13
              Caption = 
                'Quantidade dos t'#237'tulos em carteira e valor total dos t'#237'tulos em ' +
                'cobran'#231'a: '
            end
            object Label111: TLabel
              Left = 392
              Top = 32
              Width = 343
              Height = 13
              Caption = 
                'Quantidade dos t'#237'tulos em carteira e valor total dos t'#237'tulos em ' +
                'cobran'#231'a: '
            end
            object Label110: TLabel
              Left = 392
              Top = 8
              Width = 343
              Height = 13
              Caption = 
                'Quantidade dos t'#237'tulos em carteira e valor total dos t'#237'tulos em ' +
                'cobran'#231'a: '
            end
            object EdBcoCodTra5: TdmkEdit
              Left = 4
              Top = 20
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdBcoCodTra5Change
            end
            object EdBcoNomTra5: TdmkEdit
              Left = 28
              Top = 20
              Width = 128
              Height = 21
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdLotTra5: TdmkEdit
              Left = 160
              Top = 20
              Width = 33
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCNABLot7: TdmkEdit
              Left = 196
              Top = 20
              Width = 68
              Height = 21
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdRegistTra5: TdmkEdit
              Left = 220
              Top = 48
              Width = 44
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdNuAvLaTra5: TdmkEdit
              Left = 204
              Top = 76
              Width = 60
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCNABLot8: TdmkEdit
              Left = 20
              Top = 100
              Width = 844
              Height = 21
              ReadOnly = True
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdQtTit1Tra5: TdmkEdit
              Left = 740
              Top = 4
              Width = 44
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdVaTit1Tra5: TdmkEdit
              Left = 788
              Top = 4
              Width = 76
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdVaTit2Tra5: TdmkEdit
              Left = 788
              Top = 28
              Width = 76
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdQtTit2Tra5: TdmkEdit
              Left = 740
              Top = 28
              Width = 44
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 10
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdQtTit3Tra5: TdmkEdit
              Left = 740
              Top = 52
              Width = 44
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 11
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdVaTit3Tra5: TdmkEdit
              Left = 788
              Top = 52
              Width = 76
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 12
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdVaTit4Tra5: TdmkEdit
              Left = 788
              Top = 76
              Width = 76
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 13
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdQtTit4Tra5: TdmkEdit
              Left = 740
              Top = 76
              Width = 44
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 14
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
          object TabSheet12: TTabSheet
            Caption = 'Os que Faltam P'
            ImageIndex = 7
            ExplicitLeft = 0
            ExplicitTop = 0
            ExplicitWidth = 0
            ExplicitHeight = 0
            object Label172: TLabel
              Left = 4
              Top = 4
              Width = 150
              Height = 13
              Caption = 'Forma de cad. do t'#237't. no banco:'
            end
            object Label173: TLabel
              Left = 160
              Top = 4
              Width = 95
              Height = 13
              Caption = 'Tipo de documento:'
            end
            object Label174: TLabel
              Left = 260
              Top = 4
              Width = 179
              Height = 13
              Caption = 'Identifica'#231#227'o de emiss'#227'o de bloqueto:'
            end
            object Label175: TLabel
              Left = 484
              Top = 4
              Width = 150
              Height = 13
              Caption = 'Forma de cad. do t'#237't. no banco:'
            end
            object Label176: TLabel
              Left = 640
              Top = 4
              Width = 83
              Height = 13
              Caption = 'Esp'#233'cie do t'#237'tulo:'
            end
            object Label177: TLabel
              Left = 796
              Top = 4
              Width = 102
              Height = 13
              Caption = 'ID T'#237't. aceito ou n'#227'o:'
            end
            object Label178: TLabel
              Left = 4
              Top = 44
              Width = 71
              Height = 13
              Caption = 'Data emiss.t'#237't.:'
            end
            object Label179: TLabel
              Left = 80
              Top = 44
              Width = 112
              Height = 13
              Caption = 'C'#243'digo de juro de mora:'
            end
            object Label180: TLabel
              Left = 236
              Top = 44
              Width = 72
              Height = 13
              Caption = 'Data juro mora:'
            end
            object Label181: TLabel
              Left = 312
              Top = 44
              Width = 74
              Height = 13
              Caption = '% juro mora dia:'
            end
            object Label182: TLabel
              Left = 392
              Top = 44
              Width = 107
              Height = 13
              Caption = 'C'#243'digo de desconto 1:'
            end
            object Label183: TLabel
              Left = 548
              Top = 44
              Width = 64
              Height = 13
              Caption = 'Data desc. 1:'
            end
            object Label184: TLabel
              Left = 624
              Top = 44
              Width = 73
              Height = 13
              Caption = 'Val/% conced.:'
            end
            object Label185: TLabel
              Left = 704
              Top = 44
              Width = 47
              Height = 13
              Caption = 'Valor IOF:'
            end
            object Label186: TLabel
              Left = 784
              Top = 44
              Width = 73
              Height = 13
              Caption = 'Val.abatimento:'
            end
            object Label187: TLabel
              Left = 4
              Top = 84
              Width = 101
              Height = 13
              Caption = 'C'#243'digo para protesto:'
            end
            object Label188: TLabel
              Left = 132
              Top = 84
              Width = 24
              Height = 13
              Caption = 'Dias:'
            end
            object Label189: TLabel
              Left = 164
              Top = 84
              Width = 149
              Height = 13
              Caption = 'C'#243'digo para baixa / devolu'#231#227'o:'
            end
            object Label190: TLabel
              Left = 328
              Top = 84
              Width = 24
              Height = 13
              Caption = 'Dias:'
            end
            object EdForCadTitP: TdmkEdit
              Left = 4
              Top = 20
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 0
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdForCadTitPChange
            end
            object EdNomCadTitP: TdmkEdit
              Left = 28
              Top = 20
              Width = 128
              Height = 21
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTipoDocumP: TdmkEdit
              Left = 160
              Top = 20
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 2
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdTipoDocumPChange
            end
            object EdNomeDocumP: TdmkEdit
              Left = 184
              Top = 20
              Width = 73
              Height = 21
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdIDEmitBloP: TdmkEdit
              Left = 260
              Top = 20
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdIDEmitBloPChange
            end
            object EdNoEmitBloP: TdmkEdit
              Left = 284
              Top = 20
              Width = 197
              Height = 21
              ReadOnly = True
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdIDDistribP: TdmkEdit
              Left = 484
              Top = 20
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdIDDistribPChange
            end
            object EdNoDistribP: TdmkEdit
              Left = 508
              Top = 20
              Width = 128
              Height = 21
              ReadOnly = True
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdEspeciTitP: TdmkEdit
              Left = 640
              Top = 20
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdEspeciTitPChange
            end
            object EdNomEspTitP: TdmkEdit
              Left = 664
              Top = 20
              Width = 128
              Height = 21
              ReadOnly = True
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdIDTitAceiP: TdmkEdit
              Left = 796
              Top = 20
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 10
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdIDTitAceiPChange
            end
            object EdNoTitAceiP: TdmkEdit
              Left = 820
              Top = 20
              Width = 128
              Height = 21
              ReadOnly = True
              TabOrder = 11
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdDatEmiTitP: TdmkEdit
              Left = 4
              Top = 60
              Width = 72
              Height = 21
              Alignment = taCenter
              ReadOnly = True
              TabOrder = 12
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCodJurMorP: TdmkEdit
              Left = 80
              Top = 60
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 13
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCodJurMorPChange
            end
            object EdNomJurMorP: TdmkEdit
              Left = 104
              Top = 60
              Width = 128
              Height = 21
              ReadOnly = True
              TabOrder = 14
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdDatJurMorP: TdmkEdit
              Left = 236
              Top = 60
              Width = 72
              Height = 21
              Alignment = taCenter
              ReadOnly = True
              TabOrder = 15
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdJurMorDiaP: TdmkEdit
              Left = 312
              Top = 60
              Width = 77
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 16
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCodDesco1P: TdmkEdit
              Left = 392
              Top = 60
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 17
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCodDesco1PChange
            end
            object EdNomDesco1P: TdmkEdit
              Left = 416
              Top = 60
              Width = 128
              Height = 21
              ReadOnly = True
              TabOrder = 18
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdDatDesco1P: TdmkEdit
              Left = 548
              Top = 60
              Width = 72
              Height = 21
              Alignment = taCenter
              ReadOnly = True
              TabOrder = 19
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdValPerDe1P: TdmkEdit
              Left = 624
              Top = 60
              Width = 77
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 20
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdValIOFDe1P: TdmkEdit
              Left = 704
              Top = 60
              Width = 77
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 21
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdValDesco1P: TdmkEdit
              Left = 784
              Top = 60
              Width = 77
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 22
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCodParProP: TdmkEdit
              Left = 4
              Top = 100
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 23
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCodParProPChange
            end
            object EdNomParProP: TdmkEdit
              Left = 28
              Top = 100
              Width = 104
              Height = 21
              ReadOnly = True
              TabOrder = 24
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdNddParProP: TdmkEdit
              Left = 132
              Top = 100
              Width = 28
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 25
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCodParProPChange
            end
            object EdNomParBaiP: TdmkEdit
              Left = 188
              Top = 100
              Width = 140
              Height = 21
              ReadOnly = True
              TabOrder = 27
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdCodParBaiP: TdmkEdit
              Left = 164
              Top = 100
              Width = 24
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 26
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnChange = EdCodParBaiPChange
            end
            object EdNddParBaiP: TdmkEdit
              Left = 328
              Top = 100
              Width = 28
              Height = 21
              Alignment = taRightJustify
              ReadOnly = True
              TabOrder = 28
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
          end
        end
        object Panel11: TPanel
          Left = 0
          Top = 278
          Width = 1006
          Height = 88
          Align = alClient
          TabOrder = 3
          object Label87: TLabel
            Left = 8
            Top = 4
            Width = 38
            Height = 13
            Caption = 'Meu ID:'
          end
          object Label88: TLabel
            Left = 8
            Top = 44
            Width = 48
            Height = 13
            Caption = 'Duplicata:'
            FocusControl = DBEdit1
          end
          object Label89: TLabel
            Left = 84
            Top = 4
            Width = 35
            Height = 13
            Caption = 'Cliente:'
            FocusControl = DBEdit2
          end
          object Label90: TLabel
            Left = 388
            Top = 4
            Width = 24
            Height = 13
            Caption = 'Lote:'
            FocusControl = DBEdit3
          end
          object Label91: TLabel
            Left = 456
            Top = 4
            Width = 40
            Height = 13
            Caption = 'Border'#244':'
            FocusControl = DBEdit4
          end
          object Label92: TLabel
            Left = 524
            Top = 4
            Width = 24
            Height = 13
            Caption = 'Tipo:'
            FocusControl = DBEdit5
          end
          object Label93: TLabel
            Left = 592
            Top = 4
            Width = 44
            Height = 13
            Caption = 'Emitente:'
            FocusControl = DBEdit6
          end
          object Label94: TLabel
            Left = 884
            Top = 4
            Width = 104
            Height = 13
            Caption = 'CPF / CNPJ emitente:'
            FocusControl = DBEdit7
          end
          object Label95: TLabel
            Left = 88
            Top = 44
            Width = 51
            Height = 13
            Caption = 'Valor face:'
            FocusControl = DBEdit8
          end
          object Label96: TLabel
            Left = 164
            Top = 44
            Width = 49
            Height = 13
            Caption = 'Desconto:'
            FocusControl = DBEdit9
          end
          object Label97: TLabel
            Left = 240
            Top = 44
            Width = 66
            Height = 13
            Caption = 'Valor receber:'
            FocusControl = DBEdit10
          end
          object Label98: TLabel
            Left = 316
            Top = 44
            Width = 42
            Height = 13
            Caption = 'Emiss'#227'o:'
            FocusControl = DBEdit11
          end
          object Label99: TLabel
            Left = 376
            Top = 44
            Width = 50
            Height = 13
            Caption = 'D.Compra:'
            FocusControl = DBEdit12
          end
          object Label100: TLabel
            Left = 436
            Top = 44
            Width = 37
            Height = 13
            Caption = 'Vencto:'
            FocusControl = DBEdit13
          end
          object Label101: TLabel
            Left = 496
            Top = 44
            Width = 43
            Height = 13
            Caption = 'Resgate:'
            FocusControl = DBEdit14
          end
          object Label102: TLabel
            Left = 596
            Top = 44
            Width = 45
            Height = 13
            Caption = 'D. Pagto:'
            FocusControl = DBEdit15
          end
          object Label103: TLabel
            Left = 556
            Top = 44
            Width = 21
            Height = 13
            Caption = 'Dias'
            FocusControl = DBEdit16
          end
          object Label104: TLabel
            Left = 656
            Top = 44
            Width = 49
            Height = 13
            Caption = 'Cobran'#231'a:'
            FocusControl = DBEdit17
          end
          object Label105: TLabel
            Left = 716
            Top = 44
            Width = 33
            Height = 13
            Caption = 'Status:'
            FocusControl = DBEdit18
          end
          object EdLoiControle: TdmkEdit
            Left = 8
            Top = 20
            Width = 72
            Height = 21
            Alignment = taRightJustify
            ReadOnly = True
            TabOrder = 0
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdIDTiEDetTChange
          end
          object DBEdit1: TDBEdit
            Left = 8
            Top = 60
            Width = 76
            Height = 21
            DataField = 'Duplicata'
            DataSource = DsLotesIts
            TabOrder = 1
          end
          object DBEdit2: TDBEdit
            Left = 84
            Top = 20
            Width = 300
            Height = 21
            DataField = 'NOMECLI'
            DataSource = DsLotesIts
            TabOrder = 2
          end
          object DBEdit3: TDBEdit
            Left = 388
            Top = 20
            Width = 64
            Height = 21
            DataField = 'Codigo'
            DataSource = DsLotesIts
            TabOrder = 3
          end
          object DBEdit4: TDBEdit
            Left = 456
            Top = 20
            Width = 64
            Height = 21
            DataField = 'Lote'
            DataSource = DsLotesIts
            TabOrder = 4
          end
          object DBEdit5: TDBEdit
            Left = 524
            Top = 20
            Width = 64
            Height = 21
            DataField = 'NOMETIPOLOTE'
            DataSource = DsLotesIts
            TabOrder = 5
          end
          object DBEdit6: TDBEdit
            Left = 592
            Top = 20
            Width = 289
            Height = 21
            DataField = 'Emitente'
            DataSource = DsLotesIts
            TabOrder = 6
          end
          object DBEdit7: TDBEdit
            Left = 884
            Top = 20
            Width = 113
            Height = 21
            DataField = 'CPF_TXT'
            DataSource = DsLotesIts
            TabOrder = 7
          end
          object DBEdit8: TDBEdit
            Left = 88
            Top = 60
            Width = 72
            Height = 21
            DataField = 'Bruto'
            DataSource = DsLotesIts
            TabOrder = 8
          end
          object DBEdit9: TDBEdit
            Left = 164
            Top = 60
            Width = 72
            Height = 21
            DataField = 'Desco'
            DataSource = DsLotesIts
            TabOrder = 9
          end
          object DBEdit10: TDBEdit
            Left = 240
            Top = 60
            Width = 72
            Height = 21
            DataField = 'Valor'
            DataSource = DsLotesIts
            TabOrder = 10
          end
          object DBEdit11: TDBEdit
            Left = 316
            Top = 60
            Width = 56
            Height = 21
            DataField = 'Emissao'
            DataSource = DsLotesIts
            TabOrder = 11
          end
          object DBEdit12: TDBEdit
            Left = 376
            Top = 60
            Width = 56
            Height = 21
            DataField = 'DCompra'
            DataSource = DsLotesIts
            TabOrder = 12
          end
          object DBEdit13: TDBEdit
            Left = 436
            Top = 60
            Width = 56
            Height = 21
            DataField = 'Vencto'
            DataSource = DsLotesIts
            TabOrder = 13
          end
          object DBEdit14: TDBEdit
            Left = 496
            Top = 60
            Width = 56
            Height = 21
            DataField = 'DDeposito'
            DataSource = DsLotesIts
            TabOrder = 14
          end
          object DBEdit15: TDBEdit
            Left = 596
            Top = 60
            Width = 56
            Height = 21
            DataField = 'DATA3_TXT'
            DataSource = DsLotesIts
            TabOrder = 15
          end
          object DBEdit16: TDBEdit
            Left = 556
            Top = 60
            Width = 36
            Height = 21
            DataField = 'Dias'
            DataSource = DsLotesIts
            TabOrder = 16
          end
          object DBEdit17: TDBEdit
            Left = 656
            Top = 60
            Width = 60
            Height = 21
            DataField = 'Cobranca'
            DataSource = DsLotesIts
            TabOrder = 17
          end
          object DBEdit18: TDBEdit
            Left = 716
            Top = 60
            Width = 281
            Height = 21
            DataField = 'NOMESTATUS'
            DataSource = DsLotesIts
            TabOrder = 18
          end
        end
      end
      object TabSheet9: TTabSheet
        Caption = 'Extras '
        ImageIndex = 5
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object GradeA: TStringGrid
          Left = 0
          Top = 0
          Width = 73
          Height = 427
          Align = alLeft
          ColCount = 2
          DefaultColWidth = 24
          DefaultRowHeight = 18
          RowCount = 2
          TabOrder = 0
        end
        object Memo2: TMemo
          Left = 325
          Top = 0
          Width = 681
          Height = 427
          Align = alClient
          Lines.Strings = (
            'Memo2')
          TabOrder = 1
        end
        object GradeB: TStringGrid
          Left = 73
          Top = 0
          Width = 100
          Height = 427
          Align = alLeft
          ColCount = 3
          DefaultColWidth = 24
          DefaultRowHeight = 18
          RowCount = 2
          TabOrder = 2
          OnClick = GradeBClick
        end
        object Memo3: TMemo
          Left = 173
          Top = 0
          Width = 152
          Height = 427
          Align = alLeft
          TabOrder = 3
        end
      end
    end
  end
  object OpenDialog1: TOpenDialog
    Options = [ofEnableSizing]
    Title = 'Arquivo Retorno'
    Left = 8
    Top = 12
  end
  object QrBanco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM bancos')
    Left = 37
    Top = 13
    object QrBancoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBancoNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrBancoSite: TWideStringField
      FieldName = 'Site'
      Size = 255
    end
    object QrBancoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrBancoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrBancoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrBancoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrBancoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrBancoDVCC: TSmallintField
      FieldName = 'DVCC'
    end
  end
  object QrLotesIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLotesItsCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial'
      'ELSE cli.Nome END NOMECLI, loi.Codigo, loi.Duplicata,'
      
        'loi.CPF, loi.Emitente, loi.Bruto, loi.Desco, loi.Quitado, loi.Va' +
        'lor, '
      'loi.Emissao, loi.DCompra, loi.Vencto, loi.DDeposito, loi.Dias, '
      
        'loi.Data3, loi.Cobranca, loi.Repassado, lot.Lote, lot.Tipo TIPOL' +
        'OTE'
      'FROM lotesits loi'
      'LEFT JOIN lotes     lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=loi.Cliente'
      'WHERE loi.Controle=:P0')
    Left = 65
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotesItsNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrLotesItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrLotesItsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrLotesItsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLotesItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLotesItsBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotesItsDesco: TFloatField
      FieldName = 'Desco'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotesItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrLotesItsEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotesItsDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotesItsVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotesItsDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotesItsDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrLotesItsData3: TDateField
      FieldName = 'Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotesItsCobranca: TIntegerField
      FieldName = 'Cobranca'
      Required = True
    end
    object QrLotesItsLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrLotesItsTIPOLOTE: TSmallintField
      FieldName = 'TIPOLOTE'
    end
    object QrLotesItsNOMETIPOLOTE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPOLOTE'
      Size = 10
      Calculated = True
    end
    object QrLotesItsDATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Size = 25
      Calculated = True
    end
    object QrLotesItsCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 25
      Calculated = True
    end
    object QrLotesItsNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 100
      Calculated = True
    end
    object QrLotesItsQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrLotesItsRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
  end
  object DsLotesIts: TDataSource
    DataSet = QrLotesIts
    Left = 93
    Top = 13
  end
  object QrCNAB240: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCNAB240CalcFields
    SQL.Strings = (
      'SELECT CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial'
      'ELSE cli.Nome END NOMECLI, loi.Codigo MeuLote, loi.Duplicata,'
      
        'loi.CPF, loi.Emitente, loi.Bruto, loi.Desco, loi.Quitado, loi.Va' +
        'lor, '
      'loi.Emissao, loi.DCompra, loi.Vencto, loi.DDeposito, loi.Dias, '
      'loi.Data3, loi.Cobranca, loi.Repassado, loi.Cliente,'
      'lot.Lote Bordero,  lot.Tipo TIPOLOTE, ban.Nome NOMEBANCO, cna.* '
      'FROM cnab240 cna'
      'LEFT JOIN bancos ban ON ban.Codigo=cna.Banco'
      'LEFT JOIN lotesits loi ON loi.Controle=cna.Controle'
      'LEFT JOIN lotes     lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=loi.Cliente'
      'WHERE cna.CNAB240L=:P0')
    Left = 149
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB240CNAB240L: TIntegerField
      FieldName = 'CNAB240L'
    end
    object QrCNAB240CNAB240I: TIntegerField
      FieldName = 'CNAB240I'
    end
    object QrCNAB240Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCNAB240Banco: TIntegerField
      FieldName = 'Banco'
    end
    object QrCNAB240ConfigBB: TIntegerField
      FieldName = 'ConfigBB'
    end
    object QrCNAB240Lote: TIntegerField
      FieldName = 'Lote'
    end
    object QrCNAB240Item: TIntegerField
      FieldName = 'Item'
    end
    object QrCNAB240DataOcor: TDateField
      FieldName = 'DataOcor'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB240Envio: TSmallintField
      FieldName = 'Envio'
    end
    object QrCNAB240Movimento: TSmallintField
      FieldName = 'Movimento'
    end
    object QrCNAB240Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCNAB240DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCNAB240DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCNAB240UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCNAB240UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCNAB240NOMEENVIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEENVIO'
      Size = 10
      Calculated = True
    end
    object QrCNAB240NOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrCNAB240NOMEMOVIMENTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMOVIMENTO'
      Size = 100
      Calculated = True
    end
    object QrCNAB240NOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCNAB240MeuLote: TIntegerField
      FieldName = 'MeuLote'
    end
    object QrCNAB240Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrCNAB240CPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrCNAB240Emitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrCNAB240Bruto: TFloatField
      FieldName = 'Bruto'
    end
    object QrCNAB240Desco: TFloatField
      FieldName = 'Desco'
    end
    object QrCNAB240Quitado: TIntegerField
      FieldName = 'Quitado'
    end
    object QrCNAB240Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrCNAB240Emissao: TDateField
      FieldName = 'Emissao'
    end
    object QrCNAB240DCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrCNAB240Vencto: TDateField
      FieldName = 'Vencto'
    end
    object QrCNAB240DDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrCNAB240Dias: TIntegerField
      FieldName = 'Dias'
    end
    object QrCNAB240Data3: TDateField
      FieldName = 'Data3'
    end
    object QrCNAB240Cobranca: TIntegerField
      FieldName = 'Cobranca'
    end
    object QrCNAB240Repassado: TSmallintField
      FieldName = 'Repassado'
    end
    object QrCNAB240Bordero: TIntegerField
      FieldName = 'Bordero'
    end
    object QrCNAB240TIPOLOTE: TSmallintField
      FieldName = 'TIPOLOTE'
    end
    object QrCNAB240DATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Size = 25
      Calculated = True
    end
    object QrCNAB240NOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 100
      Calculated = True
    end
    object QrCNAB240NOMETIPOLOTE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMETIPOLOTE'
      Size = 10
      Calculated = True
    end
    object QrCNAB240CPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrCNAB240Custas: TFloatField
      FieldName = 'Custas'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCNAB240IRTCLB_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IRTCLB_TXT'
      Size = 100
      Calculated = True
    end
    object QrCNAB240ValPago: TFloatField
      FieldName = 'ValPago'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCNAB240ValCred: TFloatField
      FieldName = 'ValCred'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCNAB240Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrCNAB240Acao: TIntegerField
      FieldName = 'Acao'
      Required = True
    end
    object QrCNAB240NOMEACAO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEACAO'
      Size = 3
      Calculated = True
    end
    object QrCNAB240Sequencia: TIntegerField
      FieldName = 'Sequencia'
      Required = True
    end
    object QrCNAB240Convenio: TWideStringField
      FieldName = 'Convenio'
    end
    object QrCNAB240IRTCLB: TWideStringField
      FieldName = 'IRTCLB'
      Size = 10
    end
  end
  object DsCNAB240: TDataSource
    DataSet = QrCNAB240
    Left = 177
    Top = 13
  end
  object QrDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM cnab240'
      'WHERE Controle=:P0'
      'AND Lote=:P1'
      'AND Item=:P2'
      'AND DataOcor=:P3'
      'AND Envio=:P4'
      'AND Movimento=:P5'
      '')
    Left = 205
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end>
  end
  object QrSumPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ad.Juros) Juros, SUM(ad.Desco) Desco, '
      'SUM(ad.Pago) Pago, li.Valor, MAX(ad.Data) MaxData'
      'FROM aduppgs ad'
      'LEFT JOIN lotesits li ON li.Controle=ad.LotesIts'
      'WHERE ad.LotesIts=:P0'
      'GROUP BY ad.LotesIts')
    Left = 233
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPgJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumPgPago: TFloatField
      FieldName = 'Pago'
    end
    object QrSumPgDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrSumPgValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSumPgMaxData: TDateField
      FieldName = 'MaxData'
    end
  end
  object QrNext: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MIN(CNAB240L) CNAB240L'
      'FROM cnab240')
    Left = 261
    Top = 13
    object QrNextCNAB240L: TIntegerField
      FieldName = 'CNAB240L'
    end
  end
  object QrDup1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CNAB240L '
      'FROM cnab240 '
      'WHERE Sequencia=:P0'
      'AND Convenio=:P1')
    Left = 289
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDup1CNAB240L: TIntegerField
      FieldName = 'CNAB240L'
    end
  end
end
