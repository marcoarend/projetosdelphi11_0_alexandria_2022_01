unit LotesDep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, Db, mySQLDbTables,
  ComCtrls, Mask, DBCtrls, Menus, dmkEdit, dmkGeral, UnDmkProcFunc, UnDmkEnums;

type
  TFmLotesDep = class(TForm)
    PainelConfirma: TPanel;
    BtDeposito: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrCheques: TmySQLQuery;
    QrChequesCodigo: TIntegerField;
    QrChequesControle: TIntegerField;
    QrChequesComp: TIntegerField;
    QrChequesBanco: TIntegerField;
    QrChequesAgencia: TIntegerField;
    QrChequesConta: TWideStringField;
    QrChequesCheque: TIntegerField;
    QrChequesCPF: TWideStringField;
    QrChequesEmitente: TWideStringField;
    QrChequesBruto: TFloatField;
    QrChequesDesco: TFloatField;
    QrChequesValor: TFloatField;
    QrChequesEmissao: TDateField;
    QrChequesDCompra: TDateField;
    QrChequesDDeposito: TDateField;
    QrChequesVencto: TDateField;
    QrChequesTxaCompra: TFloatField;
    QrChequesTxaJuros: TFloatField;
    QrChequesTxaAdValorem: TFloatField;
    QrChequesVlrCompra: TFloatField;
    QrChequesVlrAdValorem: TFloatField;
    QrChequesDMais: TIntegerField;
    QrChequesDias: TIntegerField;
    QrChequesDuplicata: TWideStringField;
    QrChequesDevolucao: TIntegerField;
    QrChequesQuitado: TIntegerField;
    QrChequesLk: TIntegerField;
    QrChequesDataCad: TDateField;
    QrChequesDataAlt: TDateField;
    QrChequesUserCad: TIntegerField;
    QrChequesUserAlt: TIntegerField;
    QrChequesPraca: TIntegerField;
    QrChequesBcoCobra: TIntegerField;
    QrChequesAgeCobra: TIntegerField;
    QrChequesTotalJr: TFloatField;
    QrChequesTotalDs: TFloatField;
    QrChequesTotalPg: TFloatField;
    QrChequesData3: TDateField;
    QrChequesProrrVz: TIntegerField;
    QrChequesProrrDd: TIntegerField;
    QrChequesRepassado: TSmallintField;
    QrChequesDepositado: TSmallintField;
    QrChequesValQuit: TFloatField;
    QrChequesValDeposito: TFloatField;
    QrChequesTipo: TIntegerField;
    QrChequesAliIts: TIntegerField;
    QrChequesAlinPgs: TIntegerField;
    QrChequesNaoDeposita: TSmallintField;
    QrChequesReforcoCxa: TSmallintField;
    QrChequesCartDep: TIntegerField;
    QrChequesCobranca: TIntegerField;
    QrChequesRepCli: TIntegerField;
    QrChequesNOMECLI: TWideStringField;
    QrChequesLote: TIntegerField;
    DsCheques: TDataSource;
    DBG1: TDBGrid;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel7: TPanel;
    CkDescende1: TCheckBox;
    CkDescende2: TCheckBox;
    CkDescende3: TCheckBox;
    Panel8: TPanel;
    Label24: TLabel;
    EdSoma: TdmkEdit;
    Panel6: TPanel;
    RGOrdem3: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem1: TRadioGroup;
    Panel3: TPanel;
    BtRefresh: TBitBtn;
    Timer1: TTimer;
    QrSoma: TmySQLQuery;
    DsSoma: TDataSource;
    QrSomaVALOR: TFloatField;
    Label2: TLabel;
    DBEdit1: TDBEdit;
    QrMin: TmySQLQuery;
    QrMinVencto: TDateField;
    QrChequesObsGerais: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrChequesAfterOpen(DataSet: TDataSet);
    procedure QrChequesAfterClose(DataSet: TDataSet);
    procedure BtRefreshClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure DBG1CellClick(Column: TColumn);
    procedure DBG1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtDepositoClick(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
  private
    { Private declarations }
    FAscendente: Boolean;
    procedure ReopenCheques(Controle: Integer);
    procedure FechaQrCheques;
    procedure SomaLinhas(Ascendente: Boolean);
    procedure PreparaSoma(Ascendente: Boolean);
    procedure DepositaSelecionados;
    procedure DepositaAtual;
    //procedure Deposita;
  public
    { Public declarations }
  end;

  var
  FmLotesDep: TFmLotesDep;
  Desc: String;

implementation

uses Module, UnInternalConsts, CartDep, UnMyObjects;

{$R *.DFM}

procedure TFmLotesDep.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLotesDep.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmLotesDep.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLotesDep.FormCreate(Sender: TObject);
begin
  ReopenCheques(0);
end;

procedure TFmLotesDep.QrChequesAfterOpen(DataSet: TDataSet);
begin
  BtRefresh.Enabled := False;
end;

procedure TFmLotesDep.QrChequesAfterClose(DataSet: TDataSet);
begin
  BtRefresh.Enabled := True;
end;

procedure TFmLotesDep.BtRefreshClick(Sender: TObject);
begin
  if QrCheques.State = dsBrowse then
    ReopenCheques(QrChequesControle.Value)
  else
    ReopenCheques(0);
end;

procedure TFmLotesDep.FechaQrCheques;
begin
  QrCheques.Close;
  QrSoma.Close;
end;

procedure TFmLotesDep.SomaLinhas(Ascendente: Boolean);
var
  Valor: Double;
  i, k: Integer;
begin
  k := QrChequesControle.Value;
  if DBG1.SelectedRows.Count = 0 then Valor := QrChequesValor.Value else
  begin
    Valor := 0;
    with DBG1.DataSource.DataSet do
    begin
      if Ascendente then
      begin
        for i:= 0 to DBG1.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          Valor := Valor + QrChequesValor.Value;
        end;
      end else begin
        for i:= DBG1.SelectedRows.Count-1 downto 0 do
        begin
          GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          Valor := Valor + QrChequesValor.Value;
        end;
      end;
    end;
    QrCheques.Locate('Controle', k, []);
  end;
  EdSoma.Text := Geral.FFT(Valor, 2, siNegativo);
end;

procedure TFmLotesDep.Timer1Timer(Sender: TObject);
begin
  SomaLinhas(FAscendente);
end;

procedure TFmLotesDep.DBG1CellClick(Column: TColumn);
begin
  SomaLinhas(True);
end;

procedure TFmLotesDep.DBG1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_DOWN) and (Shift=[ssShift]) then PreparaSoma(True)  else
  if (Key=VK_UP)   and (Shift=[ssShift]) then PreparaSoma(False) else
  if (Key=VK_DOWN)                       then PreparaSoma(True)  else
  if (Key=VK_UP)                         then PreparaSoma(False);
end;

procedure TFmLotesDep.PreparaSoma(Ascendente: Boolean);
begin
  FAscendente := Ascendente;
  if Timer1.Enabled then Timer1.Enabled := False;
  Timer1.Enabled := True;
end;

procedure TFmLotesDep.ReopenCheques(Controle: Integer);
const
  NomeA: array[0..5] of string = ('DDeposito','Valor','Cheque','Banco , Agencia , Conta ','Emitente','NOMECLI');
  NomeD: array[0..5] of string = ('DDeposito DESC','Cheque DESC','Valor DESC','Banco DESC, Agencia DESC, Conta DESC','Emitente','NOMECLI');
var
  Ordem: String;
begin
  Ordem := 'ORDER BY ';
  if CkDescende1.Checked then
    Ordem := Ordem + NomeD[RGOrdem1.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem1.ItemIndex]+', ';
  if CkDescende2.Checked then
    Ordem := Ordem + NomeD[RGOrdem2.ItemIndex]+', '
  else
    Ordem := Ordem + NomeA[RGOrdem2.ItemIndex]+', ';
  if CkDescende3.Checked then
    Ordem := Ordem + NomeD[RGOrdem3.ItemIndex]
  else
    Ordem := Ordem + NomeA[RGOrdem3.ItemIndex];
  //
  QrSoma.Close;
  QrCheques.Close;
  QrCheques.SQL.Clear;
  QrCheques.SQL.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrCheques.SQL.Add('ELSE ent.Nome END NOMECLI, lot.Lote, loi.*');
  QrCheques.SQL.Add('FROM lotesits loi');
  QrCheques.SQL.Add('LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo');
  QrCheques.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente');
  QrCheques.SQL.Add('WHERE lot.Tipo = 0');
  QrCheques.SQL.Add('AND loi.RepCli = 0');
  QrCheques.SQL.Add('AND loi.Repassado = 0');
  QrCheques.SQL.Add('AND loi.Devolucao = 0');
  QrCheques.SQL.Add('AND loi.Depositado = 0');
  QrCheques.SQL.Add('');
  QrCheques.SQL.Add(dmkPF.SQL_Periodo('AND loi.Vencto ', 0, Date, False, True));
  //
  QrCheques.SQL.Add(Ordem);
  QrCheques.Open;
  //
  //
  QrSoma.SQL.Clear;
  QrSoma.SQL.Add('SELECT SUM(Valor) Valor');
  QrSoma.SQL.Add('FROM lotesits loi');
  QrSoma.SQL.Add('LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo');
  QrSoma.SQL.Add('WHERE lot.Tipo = 0');
  QrSoma.SQL.Add('AND loi.RepCli = 0');
  QrSoma.SQL.Add('AND loi.Repassado = 0');
  QrSoma.SQL.Add('AND loi.Devolucao = 0');
  QrSoma.SQL.Add('AND loi.Depositado = 0');
  QrSoma.SQL.Add('');
  QrSoma.SQL.Add(dmkPF.SQL_Periodo('AND loi.Vencto ', 0, Date, False, True));
  QrSoma.Open;
end;

procedure TFmLotesDep.BtDepositoClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Desc := QrChequesObsGerais.Value;
  if InputQuery('Descri��o.', 'Informe alguma descri��o (n�o � necess�rio.)', Desc) then
  if DBG1.SelectedRows.Count = 0 then
    DepositaAtual
  else
    DepositaSelecionados;
  ReopenCheques(0);
  Screen.Cursor := crDefault;
end;

procedure TFmLotesDep.DepositaSelecionados;
var
  i : Integer;
begin
  with DBG1.DataSource.DataSet do
  for i:= 0 to DBG1.SelectedRows.Count-1 do
  begin
    GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
    DepositaAtual;
  end;
end;

procedure TFmLotesDep.DepositaAtual;
begin
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE lotesits SET ObsGerais=:P0, Depositado=1');
  Dmod.QrUpdM.SQL.Add('WHERE Controle=:P1 ');
  Dmod.QrUpdM.Params[00].AsString  := Desc;
  Dmod.QrUpdM.Params[01].AsInteger := QrChequesControle.Value;
  //
  Dmod.QrUpdM.ExecSQL;
end;

procedure TFmLotesDep.RGOrdem1Click(Sender: TObject);
begin
  FechaQrCheques;
end;

end.

