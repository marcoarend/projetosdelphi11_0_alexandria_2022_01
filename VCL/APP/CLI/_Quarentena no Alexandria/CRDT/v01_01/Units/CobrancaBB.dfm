object FmCobrancaBB: TFmCobrancaBB
  Left = 330
  Top = 184
  Caption = 'Cobran'#231'a BB'
  ClientHeight = 496
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 448
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 399
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel6: TPanel
        Left = 680
        Top = 1
        Width = 109
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        Caption = 'Panel6'
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 8
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 196
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label3: TLabel
        Left = 120
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object Label7: TLabel
        Left = 212
        Top = 8
        Width = 129
        Height = 13
        Caption = 'Configura'#231#227'o de cobran'#231'a:'
      end
      object EdCodigo: TdmkEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object TPDataG: TDateTimePicker
        Left = 120
        Top = 24
        Width = 90
        Height = 21
        Date = 39067.403862476900000000
        Time = 39067.403862476900000000
        TabOrder = 1
      end
      object EdConfigBB: TdmkEditCB
        Left = 212
        Top = 24
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBConfigBB
        IgnoraDBLookupComboBox = False
      end
      object CBConfigBB: TdmkDBLookupComboBox
        Left = 280
        Top = 24
        Width = 497
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsConfigs
        TabOrder = 3
        dmkEditCB = EdConfigBB
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 448
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 1
      Top = 293
      Width = 790
      Height = 3
      Cursor = crVSplit
      Align = alBottom
    end
    object PainelControle: TPanel
      Left = 1
      Top = 399
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 1
      object LaRegistro: TLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'ltimo registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Pr'#243'ximo registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Registro anterior'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Primeiro registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 320
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
        object BtGera: TBitBtn
          Tag = 266
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Gera '
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtGeraClick
        end
        object BtTitulos: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&T'#237'tulos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtTitulosClick
        end
        object BtLotes: TBitBtn
          Tag = 265
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Lote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtLotesClick
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 52
      Align = alTop
      Enabled = False
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 120
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Data:'
        FocusControl = DBEdit1
      end
      object Label8: TLabel
        Left = 220
        Top = 8
        Width = 129
        Height = 13
        Caption = 'Configura'#231#227'o de cobran'#231'a:'
        FocusControl = DBEdit3
      end
      object Label10: TLabel
        Left = 624
        Top = 8
        Width = 74
        Height = 13
        Caption = 'Total de t'#237'tulos:'
      end
      object Label11: TLabel
        Left = 703
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Valor Total'
      end
      object DBEdCodigo: TDBEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCobrancaBB
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit1: TDBEdit
        Left = 120
        Top = 24
        Width = 97
        Height = 21
        DataField = 'MyDATAG'
        DataSource = DsCobrancaBB
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 220
        Top = 24
        Width = 400
        Height = 21
        DataField = 'NOMECONFIG'
        DataSource = DsCobrancaBB
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 624
        Top = 24
        Width = 75
        Height = 21
        DataField = 'TOTTIT'
        DataSource = DsTotal
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 703
        Top = 24
        Width = 75
        Height = 21
        DataField = 'VALTOT'
        DataSource = DsTotal
        TabOrder = 4
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 53
      Width = 790
      Height = 120
      Align = alTop
      DataSource = DsCobrancaBBIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CPF'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bruto'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Desco'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DCompra'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DDeposito'
          Width = 56
          Visible = True
        end>
    end
    object Memo1: TMemo
      Left = 1
      Top = 296
      Width = 790
      Height = 103
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      ReadOnly = True
      TabOrder = 3
      WordWrap = False
    end
  end
  object PnTitulos: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 448
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object Panel2: TPanel
      Left = 1
      Top = 399
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 0
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Adiciona'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BitBtn1Click
      end
      object Panel4: TPanel
        Left = 680
        Top = 1
        Width = 109
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        Caption = 'Panel4'
        TabOrder = 1
        object BitBtn2: TBitBtn
          Tag = 13
          Left = 8
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sair'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn2Click
        end
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 52
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label4: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit01
      end
      object Label5: TLabel
        Left = 120
        Top = 8
        Width = 26
        Height = 13
        Caption = 'Data:'
        FocusControl = DBEdit2
      end
      object Label6: TLabel
        Left = 224
        Top = 28
        Width = 229
        Height = 13
        Caption = 'Escolha um ou mais t'#237'tulos e clique em adiciona.'
      end
      object DBEdit01: TDBEdit
        Left = 16
        Top = 24
        Width = 100
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCobrancaBB
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 120
        Top = 24
        Width = 97
        Height = 21
        DataField = 'MyDATAG'
        DataSource = DsCobrancaBB
        TabOrder = 1
      end
    end
    object DBGrid2: TDBGrid
      Left = 1
      Top = 53
      Width = 790
      Height = 120
      Align = alTop
      DataSource = DsTitulos
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CPF'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bruto'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Desco'
          Title.Caption = 'Desconto'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DCompra'
          Title.Caption = 'D.Compra'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DDeposito'
          Title.Caption = 'D.Pagto'
          Width = 56
          Visible = True
        end>
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Cobran'#231'a BB'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 709
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 483
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 481
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsCobrancaBB: TDataSource
    DataSet = QrCobrancaBB
    Left = 448
    Top = 9
  end
  object QrCobrancaBB: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCobrancaBBBeforeOpen
    AfterOpen = QrCobrancaBBAfterOpen
    AfterScroll = QrCobrancaBBAfterScroll
    OnCalcFields = QrCobrancaBBCalcFields
    SQL.Strings = (
      'SELECT con.Nome NOMECONFIG, con.Diretorio, cob.* '
      'FROM cobrancabb cob'
      'LEFT JOIN configbb con ON con.Codigo=cob.ConfigBB'
      'WHERE cob.Codigo > 0')
    Left = 420
    Top = 9
    object QrCobrancaBBCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCobrancaBBDataG: TDateField
      FieldName = 'DataG'
    end
    object QrCobrancaBBLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCobrancaBBDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCobrancaBBDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCobrancaBBUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCobrancaBBUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCobrancaBBMyDATAG: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MyDATAG'
      Size = 10
      Calculated = True
    end
    object QrCobrancaBBConfigBB: TIntegerField
      FieldName = 'ConfigBB'
    end
    object QrCobrancaBBNOMECONFIG: TWideStringField
      FieldName = 'NOMECONFIG'
      Size = 50
    end
    object QrCobrancaBBDiretorio: TWideStringField
      FieldName = 'Diretorio'
      Size = 255
    end
  end
  object QrCobrancaBBIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lot.Cliente, lot.Lote, ent.MultaCodi, ent.MultaDias, '
      'ent.MultaValr, ent.MultaPerc, ent.MultaTiVe, ent.Protestar, '
      
        'ent.JuroSacado, ent.Tipo TipoCLI, PUF, EUF, uf0.Nome UFE, uf1.No' +
        'me UFP,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOME' +
        'CLI, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.CNPJ    ELSE ent.CPF     END CNPJC' +
        'LI, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ERua    ELSE ent.PRua    END RuaCL' +
        'I, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ENumero+0.000 ELSE ent.PNumero+0.0' +
        '00 END NumCLI, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECompl  ELSE ent.PCompl  END CplCL' +
        'I, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.EBairro ELSE ent.PBairro END BrrCL' +
        'I, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECEP    ELSE ent.PCEP    END CEPCL' +
        'I, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECidade ELSE ent.PCidade END CidCL' +
        'I, '
      'ent.Corrido, sac.Numero+0.000 Numero, sac.*, loi.*'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'LEFT JOIN sacados   sac ON sac.CNPJ=loi.CPF'
      'LEFT JOIN ufs       uf0 ON uf0.Codigo=ent.EUF'
      'LEFT JOIN ufs       uf1 ON uf1.Codigo=ent.PUF'
      'WHERE loi.Cobranca=:P0')
    Left = 480
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCobrancaBBItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrCobrancaBBItsLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrCobrancaBBItsNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCobrancaBBItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCobrancaBBItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCobrancaBBItsComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrCobrancaBBItsBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrCobrancaBBItsAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrCobrancaBBItsConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrCobrancaBBItsCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
    end
    object QrCobrancaBBItsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrCobrancaBBItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrCobrancaBBItsBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCobrancaBBItsDesco: TFloatField
      FieldName = 'Desco'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCobrancaBBItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCobrancaBBItsEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCobrancaBBItsDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCobrancaBBItsDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCobrancaBBItsVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCobrancaBBItsTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrCobrancaBBItsTxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrCobrancaBBItsTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrCobrancaBBItsVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrCobrancaBBItsVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrCobrancaBBItsDMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrCobrancaBBItsDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrCobrancaBBItsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrCobrancaBBItsDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrCobrancaBBItsQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrCobrancaBBItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCobrancaBBItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCobrancaBBItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCobrancaBBItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCobrancaBBItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCobrancaBBItsPraca: TIntegerField
      FieldName = 'Praca'
      Required = True
    end
    object QrCobrancaBBItsBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
      Required = True
    end
    object QrCobrancaBBItsAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
      Required = True
    end
    object QrCobrancaBBItsTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrCobrancaBBItsTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrCobrancaBBItsTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrCobrancaBBItsData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrCobrancaBBItsProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrCobrancaBBItsProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrCobrancaBBItsRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrCobrancaBBItsDepositado: TSmallintField
      FieldName = 'Depositado'
      Required = True
    end
    object QrCobrancaBBItsValQuit: TFloatField
      FieldName = 'ValQuit'
      Required = True
    end
    object QrCobrancaBBItsValDeposito: TFloatField
      FieldName = 'ValDeposito'
      Required = True
    end
    object QrCobrancaBBItsTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCobrancaBBItsAliIts: TIntegerField
      FieldName = 'AliIts'
      Required = True
    end
    object QrCobrancaBBItsAlinPgs: TIntegerField
      FieldName = 'AlinPgs'
      Required = True
    end
    object QrCobrancaBBItsNaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
      Required = True
    end
    object QrCobrancaBBItsReforcoCxa: TSmallintField
      FieldName = 'ReforcoCxa'
      Required = True
    end
    object QrCobrancaBBItsCartDep: TIntegerField
      FieldName = 'CartDep'
      Required = True
    end
    object QrCobrancaBBItsCobranca: TIntegerField
      FieldName = 'Cobranca'
      Required = True
    end
    object QrCobrancaBBItsMultaCodi: TSmallintField
      FieldName = 'MultaCodi'
    end
    object QrCobrancaBBItsMultaDias: TSmallintField
      FieldName = 'MultaDias'
    end
    object QrCobrancaBBItsMultaValr: TFloatField
      FieldName = 'MultaValr'
    end
    object QrCobrancaBBItsMultaPerc: TFloatField
      FieldName = 'MultaPerc'
    end
    object QrCobrancaBBItsCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 15
    end
    object QrCobrancaBBItsIE: TWideStringField
      FieldName = 'IE'
      Size = 25
    end
    object QrCobrancaBBItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCobrancaBBItsRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrCobrancaBBItsCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrCobrancaBBItsBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrCobrancaBBItsCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrCobrancaBBItsUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrCobrancaBBItsCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrCobrancaBBItsTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrCobrancaBBItsRisco: TFloatField
      FieldName = 'Risco'
    end
    object QrCobrancaBBItsMultaTiVe: TSmallintField
      FieldName = 'MultaTiVe'
    end
    object QrCobrancaBBItsProtestar: TSmallintField
      FieldName = 'Protestar'
    end
    object QrCobrancaBBItsJuroSacado: TFloatField
      FieldName = 'JuroSacado'
    end
    object QrCobrancaBBItsPUF: TSmallintField
      FieldName = 'PUF'
    end
    object QrCobrancaBBItsEUF: TSmallintField
      FieldName = 'EUF'
    end
    object QrCobrancaBBItsUFE: TWideStringField
      FieldName = 'UFE'
      Required = True
      Size = 2
    end
    object QrCobrancaBBItsUFP: TWideStringField
      FieldName = 'UFP'
      Required = True
      Size = 2
    end
    object QrCobrancaBBItsCNPJCLI: TWideStringField
      FieldName = 'CNPJCLI'
      Size = 18
    end
    object QrCobrancaBBItsRuaCLI: TWideStringField
      FieldName = 'RuaCLI'
      Size = 30
    end
    object QrCobrancaBBItsCplCLI: TWideStringField
      FieldName = 'CplCLI'
      Size = 30
    end
    object QrCobrancaBBItsBrrCLI: TWideStringField
      FieldName = 'BrrCLI'
      Size = 30
    end
    object QrCobrancaBBItsCEPCLI: TLargeintField
      FieldName = 'CEPCLI'
    end
    object QrCobrancaBBItsCidCLI: TWideStringField
      FieldName = 'CidCLI'
      Size = 25
    end
    object QrCobrancaBBItsTipoCLI: TSmallintField
      FieldName = 'TipoCLI'
    end
    object QrCobrancaBBItsCorrido: TIntegerField
      FieldName = 'Corrido'
    end
    object QrCobrancaBBItsNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrCobrancaBBItsNumCLI: TFloatField
      FieldName = 'NumCLI'
    end
    object QrCobrancaBBItsDescAte: TDateField
      FieldName = 'DescAte'
      Required = True
    end
  end
  object DsCobrancaBBIts: TDataSource
    DataSet = QrCobrancaBBIts
    Left = 508
    Top = 9
  end
  object PMLotes: TPopupMenu
    OnPopup = PMLotesPopup
    Left = 224
    Top = 268
    object Crianovolote1: TMenuItem
      Caption = '&Cria novo lote'
      OnClick = Crianovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      OnClick = Excluiloteatual1Click
    end
  end
  object PMTitulos: TPopupMenu
    Left = 316
    Top = 268
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Retira1: TMenuItem
      Caption = '&Retira'
      OnClick = Retira1Click
    end
  end
  object QrTitulos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT lot.Cliente, lot.Lote, CASE WHEN ent.Tipo=0 THEN ent.Raza' +
        'oSocial'
      'ELSE ent.Nome END NOMECLI, loi.*'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE lot.Tipo=1'
      'AND loi.Cobranca=0')
    Left = 540
    Top = 9
    object QrTitulosCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrTitulosLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrTitulosNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrTitulosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTitulosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrTitulosComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrTitulosBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrTitulosAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrTitulosConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrTitulosCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
    end
    object QrTitulosCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrTitulosEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrTitulosBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTitulosDesco: TFloatField
      FieldName = 'Desco'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTitulosValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTitulosEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrTitulosTxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrTitulosTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrTitulosVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrTitulosVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrTitulosDMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrTitulosDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrTitulosDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrTitulosDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrTitulosQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrTitulosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTitulosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTitulosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTitulosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTitulosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTitulosPraca: TIntegerField
      FieldName = 'Praca'
      Required = True
    end
    object QrTitulosBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
      Required = True
    end
    object QrTitulosAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
      Required = True
    end
    object QrTitulosTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrTitulosTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrTitulosTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrTitulosData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrTitulosProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrTitulosProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrTitulosRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrTitulosDepositado: TSmallintField
      FieldName = 'Depositado'
      Required = True
    end
    object QrTitulosValQuit: TFloatField
      FieldName = 'ValQuit'
      Required = True
    end
    object QrTitulosValDeposito: TFloatField
      FieldName = 'ValDeposito'
      Required = True
    end
    object QrTitulosTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrTitulosAliIts: TIntegerField
      FieldName = 'AliIts'
      Required = True
    end
    object QrTitulosAlinPgs: TIntegerField
      FieldName = 'AlinPgs'
      Required = True
    end
    object QrTitulosNaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
      Required = True
    end
    object QrTitulosReforcoCxa: TSmallintField
      FieldName = 'ReforcoCxa'
      Required = True
    end
    object QrTitulosCartDep: TIntegerField
      FieldName = 'CartDep'
      Required = True
    end
    object QrTitulosCobranca: TIntegerField
      FieldName = 'Cobranca'
      Required = True
    end
  end
  object DsTitulos: TDataSource
    DataSet = QrTitulos
    Left = 568
    Top = 9
  end
  object QrConfigBB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM configbb'
      'WHERE Codigo=:P0')
    Left = 601
    Top = 13
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConfigBBCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConfigBBNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrConfigBBConvenio: TIntegerField
      FieldName = 'Convenio'
    end
    object QrConfigBBCarteira: TWideStringField
      FieldName = 'Carteira'
      Size = 2
    end
    object QrConfigBBVariacao: TWideStringField
      FieldName = 'Variacao'
      Size = 3
    end
    object QrConfigBBSiglaEspecie: TWideStringField
      FieldName = 'SiglaEspecie'
      Size = 5
    end
    object QrConfigBBMoeda: TWideStringField
      FieldName = 'Moeda'
      Size = 5
    end
    object QrConfigBBAceite: TSmallintField
      FieldName = 'Aceite'
    end
    object QrConfigBBProtestar: TSmallintField
      FieldName = 'Protestar'
    end
    object QrConfigBBMsgLinha1: TWideStringField
      FieldName = 'MsgLinha1'
      Size = 40
    end
    object QrConfigBBPgAntes: TSmallintField
      FieldName = 'PgAntes'
    end
    object QrConfigBBMultaCodi: TSmallintField
      FieldName = 'MultaCodi'
    end
    object QrConfigBBMultaDias: TSmallintField
      FieldName = 'MultaDias'
    end
    object QrConfigBBMultaValr: TFloatField
      FieldName = 'MultaValr'
    end
    object QrConfigBBMultaPerc: TFloatField
      FieldName = 'MultaPerc'
    end
    object QrConfigBBMultaTiVe: TSmallintField
      FieldName = 'MultaTiVe'
    end
    object QrConfigBBImpreLoc: TSmallintField
      FieldName = 'ImpreLoc'
    end
    object QrConfigBBModalidade: TIntegerField
      FieldName = 'Modalidade'
    end
    object QrConfigBBclcAgencNr: TWideStringField
      FieldName = 'clcAgencNr'
      Size = 4
    end
    object QrConfigBBclcAgencDV: TWideStringField
      FieldName = 'clcAgencDV'
      Size = 1
    end
    object QrConfigBBclcContaNr: TWideStringField
      FieldName = 'clcContaNr'
      Size = 8
    end
    object QrConfigBBclcContaDV: TWideStringField
      FieldName = 'clcContaDV'
      Size = 1
    end
    object QrConfigBBcedAgencNr: TWideStringField
      FieldName = 'cedAgencNr'
      Size = 4
    end
    object QrConfigBBcedAgencDV: TWideStringField
      FieldName = 'cedAgencDV'
      Size = 1
    end
    object QrConfigBBcedContaNr: TWideStringField
      FieldName = 'cedContaNr'
      Size = 8
    end
    object QrConfigBBcedContaDV: TWideStringField
      FieldName = 'cedContaDV'
      Size = 1
    end
    object QrConfigBBEspecie: TSmallintField
      FieldName = 'Especie'
    end
    object QrConfigBBCorrido: TSmallintField
      FieldName = 'Corrido'
    end
    object QrConfigBBDiretorio: TWideStringField
      FieldName = 'Diretorio'
      Size = 255
    end
  end
  object QrConfigs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM configbb')
    Left = 389
    Top = 121
    object QrConfigsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConfigsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsConfigs: TDataSource
    DataSet = QrConfigs
    Left = 417
    Top = 121
  end
  object QrTotal: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(Emitente) TOTTIT, SUM(Valor) VALTOT'
      'FROM lotesits '
      'WHERE Cobranca=:P0')
    Left = 488
    Top = 264
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotalTOTTIT: TLargeintField
      FieldName = 'TOTTIT'
      Required = True
    end
    object QrTotalVALTOT: TFloatField
      FieldName = 'VALTOT'
      DisplayFormat = '0,00.00'
    end
  end
  object DsTotal: TDataSource
    DataSet = QrTotal
    Left = 517
    Top = 264
  end
end
