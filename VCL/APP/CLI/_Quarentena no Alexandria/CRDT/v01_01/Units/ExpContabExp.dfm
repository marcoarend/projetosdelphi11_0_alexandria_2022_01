object FmExpContabExp: TFmExpContabExp
  Left = 321
  Top = 181
  Caption = 'Exporta Dados Contabilidade'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 106
    Width = 784
    Height = 340
    Align = alClient
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 291
      Width = 782
      Height = 48
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object Progress: TProgressBar
        Left = 0
        Top = 31
        Width = 782
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 782
      Height = 290
      Align = alClient
      ParentColor = True
      TabOrder = 1
      object PageControl1: TPageControl
        Left = 1
        Top = 1
        Width = 780
        Height = 288
        ActivePage = TabSheet1
        Align = alClient
        TabOrder = 0
        object TabSheet1: TTabSheet
          Caption = ' Arquivo gerado '
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel6: TPanel
            Left = 0
            Top = 0
            Width = 772
            Height = 260
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object LbSeq: TListBox
              Left = 603
              Top = 0
              Width = 169
              Height = 260
              Align = alRight
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ItemHeight = 14
              ParentFont = False
              Sorted = True
              TabOrder = 0
              Visible = False
            end
            object Memo1: TMemo
              Left = 0
              Top = 0
              Width = 603
              Height = 260
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              ReadOnly = True
              ScrollBars = ssBoth
              TabOrder = 1
              WantReturns = False
              WordWrap = False
            end
          end
        end
        object TabSheet2: TTabSheet
          Caption = ' Cadastros sem identificador de contabilidade '
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object Panel7: TPanel
            Left = 0
            Top = 0
            Width = 772
            Height = 260
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            object DBGrid1: TDBGrid
              Left = 0
              Top = 0
              Width = 772
              Height = 260
              Align = alClient
              DataSource = Source
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Tabela'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'C'#243'digo'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 483
                  Visible = True
                end>
            end
          end
        end
        object TabSheet4: TTabSheet
          Caption = ' Dados afetados pela falta de identificador de contabilidade '
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object PageControl2: TPageControl
            Left = 0
            Top = 0
            Width = 772
            Height = 260
            ActivePage = TabSheet7
            Align = alClient
            TabOrder = 0
            object TabSheet5: TTabSheet
              Caption = ' Pagamentos '
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel10: TPanel
                Left = 0
                Top = 0
                Width = 764
                Height = 232
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                ExplicitLeft = 180
                ExplicitTop = 60
                ExplicitWidth = 185
                ExplicitHeight = 41
                object DBGrid4: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 764
                  Height = 232
                  Align = alClient
                  DataSource = DsPagErr
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Codigo'
                      Title.Caption = 'Lote'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Data'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Cliente'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NOMEENT'
                      Title.Caption = 'Nome cliente'
                      Width = 180
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Debito'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Documento'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NF'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'DEVEDORA'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CREDORA'
                      Visible = True
                    end>
                end
              end
            end
            object TabSheet6: TTabSheet
              Caption = ' Dep'#243'sitos '
              ImageIndex = 1
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel8: TPanel
                Left = 0
                Top = 0
                Width = 764
                Height = 232
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                ExplicitLeft = 188
                ExplicitTop = 136
                ExplicitWidth = 185
                ExplicitHeight = 41
                object DBGrid2: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 764
                  Height = 232
                  Align = alClient
                  DataSource = DsDepErr
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Codigo'
                      Title.Caption = 'Lote'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Title.Caption = 'Item lote'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Cliente'
                      Width = 48
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NOMECLI'
                      Title.Caption = 'Nome cliente'
                      Width = 175
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CONTAB_ENTI'
                      Title.Caption = 'Ident. cliente'
                      Width = 80
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CartDep'
                      Title.Caption = 'Cart.dep'#243's.'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NOMRCART'
                      Title.Caption = 'Nome carteira'
                      Width = 144
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'CONTAB_CART'
                      Title.Caption = 'Ident. cart. dep.'
                      Width = 80
                      Visible = True
                    end>
                end
              end
            end
            object TabSheet7: TTabSheet
              Caption = ' Impostos '
              ImageIndex = 2
              ExplicitLeft = 0
              ExplicitTop = 0
              ExplicitWidth = 0
              ExplicitHeight = 0
              object Panel9: TPanel
                Left = 0
                Top = 0
                Width = 764
                Height = 232
                Align = alClient
                BevelOuter = bvNone
                ParentBackground = False
                TabOrder = 0
                ExplicitLeft = 180
                ExplicitTop = 60
                ExplicitWidth = 185
                ExplicitHeight = 41
                object DBGrid3: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 764
                  Height = 232
                  Align = alClient
                  DataSource = DsTaxErr
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -11
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Codigo'
                      Title.Caption = 'Lote'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Cliente'
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NOMECLI'
                      Title.Caption = 'Nome do cliente'
                      Width = 594
                      Visible = True
                    end>
                end
              end
            end
          end
        end
      end
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtGera: TBitBtn
      Tag = 163
      Left = 12
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Gera'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtGeraClick
    end
    object BtSalva: TBitBtn
      Tag = 24
      Left = 184
      Top = 4
      Width = 90
      Height = 40
      Caption = 'Sal&va'
      Enabled = False
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtSalvaClick
    end
    object Panel2: TPanel
      Left = 674
      Top = 1
      Width = 109
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      Caption = 'Panel2'
      TabOrder = 2
      object BtSaida: TBitBtn
        Tag = 13
        Left = 6
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 58
    Align = alTop
    TabOrder = 2
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 468
      Height = 56
      Align = alLeft
      BevelInner = bvLowered
      TabOrder = 0
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 55
        Height = 13
        Caption = 'Data inicial:'
      end
      object Label2: TLabel
        Left = 104
        Top = 8
        Width = 48
        Height = 13
        Caption = 'Data final:'
      end
      object TPIni: TDateTimePicker
        Left = 8
        Top = 24
        Width = 90
        Height = 21
        Date = 39032.541427766200000000
        Time = 39032.541427766200000000
        TabOrder = 0
        OnChange = TPIniChange
      end
      object TPFim: TDateTimePicker
        Left = 104
        Top = 24
        Width = 90
        Height = 21
        Date = 39032.541427766200000000
        Time = 39032.541427766200000000
        TabOrder = 1
        OnChange = TPFimChange
      end
      object CkPag: TCheckBox
        Left = 200
        Top = 4
        Width = 90
        Height = 17
        Caption = 'Pagamentos.'
        Checked = True
        State = cbChecked
        TabOrder = 2
      end
      object CkDep: TCheckBox
        Left = 200
        Top = 20
        Width = 90
        Height = 17
        Caption = 'Recebimentos.'
        Checked = True
        State = cbChecked
        TabOrder = 3
      end
      object CkImp: TCheckBox
        Left = 200
        Top = 36
        Width = 90
        Height = 17
        Caption = 'Impostos.'
        Checked = True
        State = cbChecked
        TabOrder = 4
        OnClick = CkImpClick
      end
      object CkAdV: TCheckBox
        Left = 292
        Top = 36
        Width = 97
        Height = 17
        Caption = 'Ad Valorem.'
        Checked = True
        State = cbChecked
        TabOrder = 5
      end
      object CkFaC: TCheckBox
        Left = 292
        Top = 20
        Width = 97
        Height = 17
        Caption = 'Fator de compra.'
        Checked = True
        State = cbChecked
        TabOrder = 6
      end
      object CkIOF: TCheckBox
        Left = 292
        Top = 4
        Width = 97
        Height = 17
        Caption = 'IOF.'
        Checked = True
        State = cbChecked
        TabOrder = 7
      end
      object CkChe: TCheckBox
        Left = 396
        Top = 12
        Width = 68
        Height = 17
        Caption = 'Cheque.'
        Checked = True
        State = cbChecked
        TabOrder = 8
      end
      object CkDup: TCheckBox
        Left = 396
        Top = 28
        Width = 68
        Height = 17
        Caption = 'Duplicata.'
        Checked = True
        State = cbChecked
        TabOrder = 9
      end
    end
    object Panel4: TPanel
      Left = 469
      Top = 1
      Width = 314
      Height = 56
      Align = alClient
      BevelInner = bvLowered
      TabOrder = 1
      object Label3: TLabel
        Left = 2
        Top = 23
        Width = 307
        Height = 26
        Align = alClient
        Caption = 
          'Arquivos gerados como teste n'#227'o podem ser enviados '#224' contabilida' +
          'de!'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
        WordWrap = True
      end
      object Panel5: TPanel
        Left = 2
        Top = 2
        Width = 310
        Height = 21
        Align = alTop
        BevelInner = bvLowered
        TabOrder = 0
        object CkTeste: TCheckBox
          Left = 5
          Top = 2
          Width = 184
          Height = 17
          Caption = 'Teste (informa'#231#227'o de lotes e itens).'
          TabOrder = 0
          OnClick = CkTesteClick
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Exporta Dados Contabilidade'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 3
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object QrPagtos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.Debito, la.Documento, lo.NF, lo.Data, lo.Tipo,'
      'ca.Contab CREDORA, fo.Contab DEVEDORA, '
      'DAY(lo.Data) DIA, MONTH(lo.Data) MES, la.FatNum, la.Controle'
      'FROM lanctos la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'LEFT JOIN lotes     lo ON lo.Codigo=la.FatNum'
      'LEFT JOIN entidades fo ON fo.Codigo=lo.Cliente'
      'WHERE FatID=300'
      'AND lo.Data BETWEEN :P0 AND :P1'
      'AND (lo.TxCompra+lo.ValValorem)>=0.01'
      'AND lo.Tipo in (:P2, :P3)'
      'ORDER BY la.FatParcela, la.Vencimento')
    Left = 60
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrPagtosDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPagtosNF: TIntegerField
      FieldName = 'NF'
    end
    object QrPagtosData: TDateField
      FieldName = 'Data'
    end
    object QrPagtosTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrPagtosCREDORA: TWideStringField
      FieldName = 'CREDORA'
    end
    object QrPagtosDEVEDORA: TWideStringField
      FieldName = 'DEVEDORA'
    end
    object QrPagtosDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrPagtosMES: TLargeintField
      FieldName = 'MES'
    end
    object QrPagtosDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrPagtosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagtosFatNum: TFloatField
      FieldName = 'FatNum'
    end
  end
  object QrConfig: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT exc.*, car.Contab '
      'FROM expcontab exc'
      'LEFT JOIN carteiras car ON car.Codigo=exc.CartPadr')
    Left = 4
    Top = 8
    object QrConfigDiaI: TIntegerField
      FieldName = 'DiaI'
    end
    object QrConfigDiaF: TIntegerField
      FieldName = 'DiaF'
    end
    object QrConfigDiaP: TWideStringField
      FieldName = 'DiaP'
      Size = 1
    end
    object QrConfigMesI: TIntegerField
      FieldName = 'MesI'
    end
    object QrConfigMesF: TIntegerField
      FieldName = 'MesF'
    end
    object QrConfigMesP: TWideStringField
      FieldName = 'MesP'
      Size = 1
    end
    object QrConfigDevI: TIntegerField
      FieldName = 'DevI'
    end
    object QrConfigDevF: TIntegerField
      FieldName = 'DevF'
    end
    object QrConfigDevP: TWideStringField
      FieldName = 'DevP'
      Size = 1
    end
    object QrConfigCreI: TIntegerField
      FieldName = 'CreI'
    end
    object QrConfigCreF: TIntegerField
      FieldName = 'CreF'
    end
    object QrConfigCreP: TWideStringField
      FieldName = 'CreP'
      Size = 1
    end
    object QrConfigComI: TIntegerField
      FieldName = 'ComI'
    end
    object QrConfigComF: TIntegerField
      FieldName = 'ComF'
    end
    object QrConfigComP: TWideStringField
      FieldName = 'ComP'
      Size = 1
    end
    object QrConfigValI: TIntegerField
      FieldName = 'ValI'
    end
    object QrConfigValF: TIntegerField
      FieldName = 'ValF'
    end
    object QrConfigValP: TWideStringField
      FieldName = 'ValP'
      Size = 1
    end
    object QrConfigDocI: TIntegerField
      FieldName = 'DocI'
    end
    object QrConfigDocF: TIntegerField
      FieldName = 'DocF'
    end
    object QrConfigDocP: TWideStringField
      FieldName = 'DocP'
      Size = 1
    end
    object QrConfigCartPadr: TIntegerField
      FieldName = 'CartPadr'
    end
    object QrConfigContab: TWideStringField
      FieldName = 'Contab'
    end
  end
  object QrDeposi: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DAY(li.DDeposito) DIA0, MONTH(li.DDeposito) MES0,'
      'DAY(li.Data3) DIA1, MONTH(li.Data3) MES1,'
      'li.Cheque, li.Valor, li.ValDeposito, lo.NF, cl.Contab CREDORA,'
      'lo.Tipo, ca.Contab DEVEDORA, li.Codigo, li.Controle'
      'FROM lotesits li'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN carteiras ca ON ca.Codigo=li.CartDep'
      'WHERE lo.Codigo > 0'
      'AND (lo.TxCompra+lo.ValValorem)>=0.01'
      'AND ((lo.Tipo=0 AND li.DDeposito BETWEEN :P0 AND :P1)'
      'OR (lo.Tipo=1 AND (li.Data3 BETWEEN :P2 AND :P3)))'
      'AND lo.Tipo in (:P4, :P5)'
      'ORDER BY lo.Data, lo.NF')
    Left = 88
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end>
    object QrDeposiCREDORA: TWideStringField
      FieldName = 'CREDORA'
    end
    object QrDeposiDEVEDORA: TWideStringField
      FieldName = 'DEVEDORA'
    end
    object QrDeposiCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
    end
    object QrDeposiNF: TIntegerField
      FieldName = 'NF'
    end
    object QrDeposiValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrDeposiValDeposito: TFloatField
      FieldName = 'ValDeposito'
      Required = True
    end
    object QrDeposiTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDeposiDIA0: TLargeintField
      FieldName = 'DIA0'
    end
    object QrDeposiMES0: TLargeintField
      FieldName = 'MES0'
    end
    object QrDeposiDIA1: TLargeintField
      FieldName = 'DIA1'
    end
    object QrDeposiMES1: TLargeintField
      FieldName = 'MES1'
    end
    object QrDeposiCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDeposiControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object QrImpost: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrImpostCalcFields
    SQL.Strings = (
      'SELECT DAY(lo.Data) DIA, MONTH(lo.Data) MES, lo.NF, '
      'lo.MINTC, lo.MINAV, en.Contab DEVEDORA, lo.Codigo,'
      'lo.IOC_VAL, lo.IOFd_VAL, lo.IOFv_VAL'
      'FROM lotes lo'
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente'
      'WHERE lo.data BETWEEN :P0 AND :P1'
      'AND (TxCompra+ValValorem)>=0.01'
      'AND lo.Tipo in (:P2, :P3)'
      'ORDER BY lo.Data, lo.NF')
    Left = 116
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrImpostDIA: TLargeintField
      FieldName = 'DIA'
    end
    object QrImpostMES: TLargeintField
      FieldName = 'MES'
    end
    object QrImpostNF: TIntegerField
      FieldName = 'NF'
      Required = True
    end
    object QrImpostIOC_VAL: TFloatField
      FieldName = 'IOC_VAL'
      Required = True
    end
    object QrImpostMINTC: TFloatField
      FieldName = 'MINTC'
      Required = True
    end
    object QrImpostMINAV: TFloatField
      FieldName = 'MINAV'
      Required = True
    end
    object QrImpostDEVEDORA: TWideStringField
      FieldName = 'DEVEDORA'
    end
    object QrImpostCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrImpostIOFd_VAL: TFloatField
      FieldName = 'IOFd_VAL'
      Required = True
    end
    object QrImpostIOFv_VAL: TFloatField
      FieldName = 'IOFv_VAL'
      Required = True
    end
    object QrImpostIOF_TOT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'IOF_TOT'
      Calculated = True
    end
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = 'txt'
    Left = 32
    Top = 8
  end
  object QrPesq1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ca.Codigo CODIGO, ca.Nome NOME'
      'FROM lanctos la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'LEFT JOIN lotes     lo ON lo.Codigo=la.FatNum'
      'WHERE FatID=300'
      'AND lo.Data BETWEEN :P0 AND :P1'
      'AND (lo.TxCompra+lo.ValValorem)>=0.01'
      'AND lo.Tipo in (:P2, :P3)'
      'AND ca.Contab = '#39#39)
    Left = 696
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrPesq1CODIGO: TIntegerField
      FieldName = 'CODIGO'
    end
    object QrPesq1NOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
  end
  object Query: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    Left = 724
    Top = 8
    object QueryCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QueryNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QueryTabela: TWideStringField
      FieldName = 'Tabela'
      Size = 30
    end
  end
  object Source: TDataSource
    DataSet = Query
    Left = 752
    Top = 8
  end
  object QrDepErr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT li.Codigo, li.Controle, lo.Cliente, '
      'li.CartDep, ca.Nome NOMRCART,'
      'cl.Contab CONTAB_ENTI,'#13
      'ca.Contab CONTAB_CART, '
      'IF(cl.Tipo=0, cl.RazaoSocial, cl.Nome) NOMECLI'
      'FROM lotesits li'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN carteiras ca ON ca.Codigo=li.CartDep'
      'WHERE lo.Codigo > 0'
      'AND (lo.TxCompra+lo.ValValorem)>=0.01'
      'AND ((lo.Tipo=0 AND li.DDeposito BETWEEN :P0 AND :P1)'
      'OR (lo.Tipo=1 AND (li.Data3 BETWEEN :P2 AND :P3)))'
      'AND lo.Tipo in (:P4, :P5)'
      'AND (cl.Contab="" OR ca.Contab="")'
      'ORDER BY li.Codigo, li.Controle')
    Left = 288
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P5'
        ParamType = ptUnknown
      end>
    object QrDepErrCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDepErrControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDepErrCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDepErrCartDep: TIntegerField
      FieldName = 'CartDep'
      Required = True
    end
    object QrDepErrNOMRCART: TWideStringField
      FieldName = 'NOMRCART'
      Size = 100
    end
    object QrDepErrCONTAB_ENTI: TWideStringField
      FieldName = 'CONTAB_ENTI'
    end
    object QrDepErrCONTAB_CART: TWideStringField
      FieldName = 'CONTAB_CART'
    end
    object QrDepErrNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
  end
  object DsDepErr: TDataSource
    DataSet = QrDepErr
    Left = 316
    Top = 216
  end
  object QrTaxErr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lo.Codigo, lo.Cliente, '#13
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMECLI,'#10
      ''
      ''
      'lo.MINTC, lo.MINAV, en.Contab DEVEDORA, '
      'lo.IOC_VAL, lo.IOFd_VAL, lo.IOFv_VAL'
      'FROM lotes lo'
      'LEFT JOIN entidades en ON en.Codigo=lo.Cliente'
      'WHERE lo.data BETWEEN :P0 AND :P1'
      'AND (TxCompra+ValValorem)>=0.01'
      'AND lo.Tipo in (:P2, :P3)'#13
      'AND (lo.TxCompra+lo.ValValorem)>=0.01'
      'AND TRIM(en.Contab) = ""'
      'ORDER BY lo.Data, lo.NF')
    Left = 344
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
  end
  object DsTaxErr: TDataSource
    DataSet = QrTaxErr
    Left = 372
    Top = 216
  end
  object QrPagErr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lo.Codigo, la.Debito, la.Documento, '#13
      'lo.NF, lo.Data, lo.Cliente, la.Controle, '
      'ca.Contab CREDORA, fo.Contab DEVEDORA, '#13
      'IF(fo.Tipo=0, fo.RazaoSocial, fo.Nome) NOMEENT'
      'FROM lanctos la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'LEFT JOIN lotes     lo ON lo.Codigo=la.FatNum'
      'LEFT JOIN entidades fo ON fo.Codigo=lo.Cliente'
      'WHERE FatID=300'
      'AND lo.Data BETWEEN :P0 AND :P1'
      'AND (lo.TxCompra+lo.ValValorem)>=0.01'
      'AND lo.Tipo in (:P2, :P3)'
      'AND (TRIM(ca.Contab) = "" OR TRIM(fo.Contab) = "")'
      'ORDER BY la.FatParcela, la.Vencimento')
    Left = 232
    Top = 216
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrPagErrCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
      DisplayFormat = '00000'
    end
    object QrPagErrDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPagErrDocumento: TFloatField
      FieldName = 'Documento'
      DisplayFormat = '00000'
    end
    object QrPagErrNF: TIntegerField
      FieldName = 'NF'
      DisplayFormat = '00000'
    end
    object QrPagErrData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagErrCliente: TIntegerField
      FieldName = 'Cliente'
      DisplayFormat = '00000'
    end
    object QrPagErrControle: TIntegerField
      FieldName = 'Controle'
      Required = True
      DisplayFormat = '00000'
    end
    object QrPagErrCREDORA: TWideStringField
      FieldName = 'CREDORA'
    end
    object QrPagErrDEVEDORA: TWideStringField
      FieldName = 'DEVEDORA'
    end
    object QrPagErrNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object DsPagErr: TDataSource
    DataSet = QrPagErr
    Left = 260
    Top = 216
  end
end
