unit Taxas;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkRadioGroup, dmkCheckBox,
  dmkCheckGroup, UnDmkProcFunc, UnDmkEnums;

type
  TFmTaxas = class(TForm)
    PainelDados: TPanel;
    DsTaxas: TDataSource;
    QrTaxas: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PnPesq: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TStaticText;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label9: TLabel;
    QrTaxasCodigo: TIntegerField;
    QrTaxasNome: TWideStringField;
    EdValor: TdmkEdit;
    Label3: TLabel;
    RGForma: TdmkRadioGroup;
    RGBase: TdmkRadioGroup;
    CGAutoma: TdmkCheckGroup;
    CGMostra: TdmkCheckGroup;
    CGGenero: TdmkCheckGroup;
    dmkDBEdit1: TdmkDBEdit;
    QrTaxasGenero: TSmallintField;
    QrTaxasForma: TSmallintField;
    QrTaxasMostra: TSmallintField;
    QrTaxasBase: TSmallintField;
    QrTaxasAutomatico: TSmallintField;
    QrTaxasValor: TFloatField;
    QrTaxasLk: TIntegerField;
    QrTaxasDataCad: TDateField;
    QrTaxasDataAlt: TDateField;
    QrTaxasUserCad: TIntegerField;
    QrTaxasUserAlt: TIntegerField;
    QrTaxasAlterWeb: TSmallintField;
    QrTaxasAtivo: TSmallintField;
    QrTaxasFORMA_TXT: TWideStringField;
    DBText1: TDBText;
    dmkDBCheckGroup1: TdmkDBCheckGroup;
    dmkDBCheckGroup2: TdmkDBCheckGroup;
    dmkDBCheckGroup3: TdmkDBCheckGroup;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrTaxasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrTaxasBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure QrTaxasCalcFields(DataSet: TDataSet);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure LocCod(Atual, Codigo: Integer);
  end;

var
  FmTaxas: TFmTaxas;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmTaxas.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmTaxas.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrTaxasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmTaxas.DefParams;
begin
  VAR_GOTOTABELA := 'taxas';
  VAR_GOTOMYSQLTABLE := QrTaxas;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM taxas');
  VAR_SQLx.Add('WHERE Codigo > 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  //VAR_SQL2.Add('AND CodUsu=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmTaxas.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible := True;
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
    end;
    1:
    begin
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      PainelControle.Visible := False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant := FormatFloat(FFormatFloat, Codigo);
        EdNome.ValueVariant   := '';
        //
      end else begin
        EdCodigo.ValueVariant := QrTaxasCodigo.Value;
        EdNome.ValueVariant   := QrTaxasNome.Value;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.SQLType);
end;

procedure TFmTaxas.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmTaxas.QueryPrincipalAfterOpen;
begin
end;

procedure TFmTaxas.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmTaxas.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmTaxas.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmTaxas.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmTaxas.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmTaxas.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrTaxas, [PainelDados],
  [PainelEdita], EdNome, LaTipo, 'taxas');
end;

procedure TFmTaxas.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrTaxasCodigo.Value;
  Close;
end;

procedure TFmTaxas.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome := EdNome.ValueVariant;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('taxas', 'Codigo', LaTipo.SQLType,
    QrTaxasCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmTaxas, PainelEdit,
    'taxas', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmTaxas.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.ValueVariant);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'taxas', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'taxas', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'taxas', 'Codigo');
end;

procedure TFmTaxas.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrTaxas, [PainelDados],
  [PainelEdita], EdNome, LaTipo, 'taxas');
end;

procedure TFmTaxas.FormCreate(Sender: TObject);
begin
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmTaxas.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrTaxasCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTaxas.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmTaxas.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrTaxasCodigo.Value, LaRegistro.Caption);
end;

procedure TFmTaxas.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.SQLType);
end;

procedure TFmTaxas.QrTaxasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmTaxas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmTaxas.SbQueryClick(Sender: TObject);
begin
  LocCod(QrTaxasCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'taxas', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmTaxas.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmTaxas.QrTaxasBeforeOpen(DataSet: TDataSet);
begin
  QrTaxasCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmTaxas.QrTaxasCalcFields(DataSet: TDataSet);
begin
  QrTaxasFORMA_TXT.Value := MLAGeral.FormataTaxa(
    QrTaxasForma.Value, Dmod.QrControleMoeda.Value)+':';
end;

end.

