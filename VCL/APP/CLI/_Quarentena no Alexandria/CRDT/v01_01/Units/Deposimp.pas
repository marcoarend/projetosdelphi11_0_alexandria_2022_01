unit Deposimp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, ComCtrls,
  Grids, DBGrids, Mask, Menus, frxClass, frxDBSet, Variants, dmkGeral, dmkEdit,
  dmkEditCB, dmkDBLookupComboBox, UnDmkProcFunc, UnDmkEnums;

type
  TFmDeposimp = class(TForm)
    PainelConfirma: TPanel;
    BtPesquisa: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrClientes: TmySQLQuery;
    DsClientes: TDataSource;
    QrLotesIts: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrLotesItsPERIODO: TFloatField;
    QrLotesItsBanco: TIntegerField;
    QrLotesItsAgencia: TIntegerField;
    QrLotesItsConta: TWideStringField;
    QrLotesItsCheque: TIntegerField;
    QrLotesItsEmitente: TWideStringField;
    QrLotesItsCPF: TWideStringField;
    QrLotesItsDDeposito: TDateField;
    QrLotesItsVencto: TDateField;
    QrLotesItsValor: TFloatField;
    QrClientesNOMECLIENTE: TWideStringField;
    QrLotesItsDDeposito_TXT: TWideStringField;
    QrLotesItsMES_TXT: TWideStringField;
    QrLotesItsCPF_TXT: TWideStringField;
    QrLotesItsValor_TXT: TWideStringField;
    QrLotesItsCONTAGEM: TIntegerField;
    QrLotesItsNOMECLIENTE: TWideStringField;
    QrLotesItsNOMESTATUS: TWideStringField;
    Timer1: TTimer;
    QrLotesItsNF: TIntegerField;
    QrColigados: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsColigados: TDataSource;
    QrLotesItsNOMECOLIGADO: TWideStringField;
    BtImprime: TBitBtn;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    RGAgrupa: TRadioGroup;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    BtLimpar: TBitBtn;
    Panel2: TPanel;
    RGOrdem1: TRadioGroup;
    RGOrdem2: TRadioGroup;
    RGOrdem3: TRadioGroup;
    RGOrdem4: TRadioGroup;
    PainelDados: TPanel;
    Label3: TLabel;
    Label34: TLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    LaColigado: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    TPIni: TDateTimePicker;
    TPFim: TDateTimePicker;
    EdEmitente: TdmkEdit;
    EdCPF: TdmkEdit;
    RGMascara: TRadioGroup;
    GroupBox1: TGroupBox;
    CkP0: TCheckBox;
    EdColigado: TdmkEditCB;
    CBColigado: TdmkDBLookupComboBox;
    Panel3: TPanel;
    Label36: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label8: TLabel;
    Label32: TLabel;
    Label10: TLabel;
    EdBanda: TdmkEdit;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    EdConta: TdmkEdit;
    EdCheque: TdmkEdit;
    DBEdItens: TDBEdit;
    DBEdValor: TDBEdit;
    DBG1: TDBGrid;
    RGOQue: TRadioGroup;
    DsSumD: TDataSource;
    QrLotesItsCodigo: TIntegerField;
    QrLotesItsControle: TIntegerField;
    EdValor: TdmkEdit;
    Label11: TLabel;
    QrSumT: TmySQLQuery;
    QrSumTValor: TFloatField;
    QrSumD: TmySQLQuery;
    QrSumDValor: TFloatField;
    QrSumDQTDE: TLargeintField;
    QrSumTQTDE: TLargeintField;
    DsLotesIts: TDataSource;
    QrLotesItsDepositado: TSmallintField;
    DsSumT: TDataSource;
    Label9: TLabel;
    DBEdit2: TDBEdit;
    DBEdit1: TDBEdit;
    CkP1: TCheckBox;
    CkP2: TCheckBox;
    QrLotesItsValDeposito: TFloatField;
    QrLotesItsCONF_V: TIntegerField;
    Timer2: TTimer;
    QrLotesItsCONF_I: TIntegerField;
    QrLotesItsNF_TXT: TWideStringField;
    CkP3: TCheckBox;
    QrLotesItsReforcoCxa: TSmallintField;
    RGRepasse: TRadioGroup;
    RGReforco: TRadioGroup;
    BtConfigura: TBitBtn;
    PMDeposito: TPopupMenu;
    Selecionados1: TMenuItem;
    Tudo1: TMenuItem;
    QrLotesItsNOMECART: TWideStringField;
    Timer3: TTimer;
    BtDeposito: TBitBtn;
    Label7: TLabel;
    EdSoma: TdmkEdit;
    RGTipo: TRadioGroup;
    SpeedButton1: TSpeedButton;
    Query1: TmySQLQuery;
    Query1Controle: TIntegerField;
    RGAlinIts: TRadioGroup;
    frxDsLotesIts: TfrxDBDataset;
    frxDeposIts: TfrxReport;
    QrLotesItsCliente: TIntegerField;
    Label12: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure QrLotesItsCalcFields(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdCPFExit(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure EdColigadoChange(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure RGOrdem1Click(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
    procedure EdEmitenteChange(Sender: TObject);
    procedure RGMascaraClick(Sender: TObject);
    procedure EdCPFChange(Sender: TObject);
    procedure CkQuitadoClick(Sender: TObject);
    procedure EdBandaChange(Sender: TObject);
    procedure DBEdItensChange(Sender: TObject);
    procedure DBEdValorChange(Sender: TObject);
    procedure BtLimparClick(Sender: TObject);
    procedure DBG1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBG1CellClick(Column: TColumn);
    procedure QrLotesItsAfterOpen(DataSet: TDataSet);
    procedure CkP0Click(Sender: TObject);
    procedure CkP1Click(Sender: TObject);
    procedure CkP2Click(Sender: TObject);
    procedure CkR0Click(Sender: TObject);
    procedure QrLotesItsAfterClose(DataSet: TDataSet);
    procedure EdValorExit(Sender: TObject);
    procedure Timer2Timer(Sender: TObject);
    procedure RGRepasseClick(Sender: TObject);
    procedure RGReforcoClick(Sender: TObject);
    procedure BtConfiguraClick(Sender: TObject);
    procedure Selecionados1Click(Sender: TObject);
    procedure Tudo1Click(Sender: TObject);
    procedure DBG1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Timer3Timer(Sender: TObject);
    procedure BtDepositoClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure frxDeposIts_GetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
    FEmitente, FCPF: String;
    FLotesIts: Integer;
    FAscendente: Boolean;
    procedure ConfiguracaoInicial;
    function LocalizaItem: Boolean;
    procedure ConfereCheque(Status: Integer);
    procedure ReopenLotesIts;
    procedure InsereValDeposito(Valor, Real: Double; Controle: Integer;
              Avisa: Boolean);
    procedure VoltaABanda;
    procedure ReforcoCxa(Status: Integer);
    procedure ContaDeposito(Tipo: Integer);
    procedure DepositaSelecionados;
    procedure DepositaAtual;
    procedure SomaLinhas(Ascendente: Boolean);
    procedure PreparaSoma(Ascendente: Boolean);
  public
    { Public declarations }
    FImprime: Boolean;
  end;

  var
  FmDeposimp: TFmDeposimp;

implementation

{$R *.DFM}

uses Module, UnInternalConsts, Principal, MyVCLSkin, UMySQLModule, CartDep,
UnMyObjects;

procedure TFmDeposimp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDeposimp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if FImprime then Timer1.Enabled := True;
end;

procedure TFmDeposimp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmDeposimp.EdClienteChange(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.FormCreate(Sender: TObject);
begin
  ConfiguracaoInicial;
  //
  QrClientes.Open;
  QrColigados.Open;
  TPIni.Date := Date;
  TPFim.Date := Date + 365;
  //
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmDeposimp.ConfiguracaoInicial;
var
  Cam: String;
begin
  Cam := Application.Title + '\XYConfig\DeposImp';
  RGOrdem1.ItemIndex := Geral.ReadAppKeyLM('Ordem1', Cam, ktInteger, 0);
  RGOrdem2.ItemIndex := Geral.ReadAppKeyLM('Ordem2', Cam, ktInteger, 2);
  RGOrdem3.ItemIndex := Geral.ReadAppKeyLM('Ordem3', Cam, ktInteger, 1);
  RGOrdem4.ItemIndex := Geral.ReadAppKeyLM('Ordem4', Cam, ktInteger, 3);
  RGAgrupa.ItemIndex := Geral.ReadAppKeyLM('Agrupa', Cam, ktInteger, 0);
  //CkQuitado.Checked := Geral.ReadAppKeyLM('Sit000', Cam, ktBoolean, True);
  CkP0.Checked := Geral.ReadAppKeyLM('P0', Cam, ktBoolean, False);
  CkP1.Checked := Geral.ReadAppKeyLM('P1', Cam, ktBoolean, False);
  CkP2.Checked := Geral.ReadAppKeyLM('P2', Cam, ktBoolean, False);
  RGRepasse.ItemIndex := Geral.ReadAppKeyLM('Repasse', Cam, ktInteger, 0);
  RGReforco.ItemIndex := Geral.ReadAppKeyLM('Reforco', Cam, ktInteger, 0);
end;

procedure TFmDeposimp.BtPesquisaClick(Sender: TObject);
begin
  ReopenLotesIts;
end;

procedure TFmDeposimp.ReopenLotesIts;
const
  ItensOrdem: array[0..4] of String = ('NOMECLIENTE', 'DDeposito', 'PERIODO',
  'Emitente', 'CPF');
var
  Cliente, Coligado: Integer;
  P0, P1, P2, P3, R0, R1, D0, D1: Integer;
  PT0, PT1, PT2, RT0, RT1, DT0, DT1: String;
begin
  Screen.Cursor := crHourGlass;
  if Trim(EdEmitente.Text) <> '' then
  begin
    FEmitente := EdEmitente.Text;
    if RGMascara.ItemIndex in ([0,1]) then FEmitente := '%'+FEmitente;
    if RGMascara.ItemIndex in ([0,2]) then FEmitente := FEmitente+'%';
  end else FEmitente := '';
  if Trim(EdCPF.Text) <> '' then FCPF := Geral.SoNumero_TT(EdCPF.Text)
  else FCPF := '';
  P0 := MLAGeral.BoolToIntDef(CkP0.Checked, 0, -1000);
  P1 := MLAGeral.BoolToIntDef(CkP1.Checked, 1, -1000);
  P2 := MLAGeral.BoolToIntDef(CkP2.Checked, 2, -1000);
  P3 := MLAGeral.BoolToIntDef(CkP3.Checked, 4, -1000);
  if RGRepasse.ItemIndex in ([0,2]) then R0 := 0 else R0 := -1000;
  //R0 := MLAGeral.BoolToIntDef(CkR0.Checked, 0, -1000);
  if RGRepasse.ItemIndex in ([1,2]) then R1 := 1 else R1 := -1000;
  //R1 := MLAGeral.BoolToIntDef(CkR1.Checked, 1, -1000);
  if RGReforco.ItemIndex in ([0,2]) then D0 := 0 else D0 := -1000;
  if RGReforco.ItemIndex in ([1,2]) then D1 := 1 else D1 := -1000;
  //
  PT0 := IntToStr(P0);
  PT1 := IntToStr(P1);
  PT2 := IntToStr(P2);
  //PT3 := IntToStr(P3);
  RT0 := IntToStr(R0);
  RT1 := IntToStr(R1);
  DT0 := IntToStr(D0);
  DT1 := IntToStr(D1);
  if (P0+P1+P2+P3=-4000) then
  begin
    Application.MessageBox('Informe pelo menos um status de pagamento!', 'Erro',
      MB_OK+MB_ICONERROR);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (R0+R1=-2000) then
  begin
    Application.MessageBox('Informe pelo menos um status de repasse!', 'Erro',
      MB_OK+MB_ICONERROR);
    Screen.Cursor := crDefault;
    Exit;
  end;
  Cliente := Geral.IMV(EdCliente.Text);
  Coligado := Geral.IMV(EdColigado.Text);
  //
  QrLotesIts.Close;
  QrLotesIts.SQL.Clear;
  QrLotesIts.SQL.Add('SELECT MONTH(li.DDeposito)+YEAR(li.DDeposito)*100+0.00 PERIODO,');
  QrLotesIts.SQL.Add('CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial ELSE cl.Nome END NOMECLIENTE,');
  QrLotesIts.SQL.Add('CASE WHEN co.Tipo=0 THEN co.RazaoSocial ELSE co.Nome END NOMECOLIGADO,');
  QrLotesIts.SQL.Add('li.Banco, li.Agencia, li.Conta, li.Cheque, li.Codigo, ');
  QrLotesIts.SQL.Add('li.Controle, li.Depositado, li.Emitente, li.CPF, ');
  QrLotesIts.SQL.Add('li.DDeposito, li.Vencto, li.Valor, li.ValDeposito, ');
  QrLotesIts.SQL.Add('li.ReforcoCxa, lo.NF, ca.Nome NOMECART, lo.Cliente');
  QrLotesIts.SQL.Add('FROM lotesits li');
  QrLotesIts.SQL.Add('LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo');
  QrLotesIts.SQL.Add('LEFT JOIN repasits  ri ON ri.Origem=li.Controle');
  QrLotesIts.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  QrLotesIts.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  QrLotesIts.SQL.Add('LEFT JOIN entidades co ON co.Codigo=re.Coligado');
  QrLotesIts.SQL.Add('LEFT JOIN carteiras ca ON ca.Codigo=li.CartDep');
  if RGAlinIts.ItemIndex in ([0,1]) then
    QrLotesIts.SQL.Add('LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle');

  QrLotesIts.SQL.Add('WHERE lo.Tipo in (0, 2)');
  QrLotesIts.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  QrLotesIts.SQL.Add('AND li.DDeposito BETWEEN :P0 AND :P1');
  QrLotesIts.SQL.Add('AND li.Quitado in (-1,'+PT0+', '+PT1+', '+PT2+')');
  if Coligado <> 0 then
  begin
    QrLotesIts.SQL.Add('AND ((li.Repassado = '+RT1+' AND re.Coligado='+
    IntToStr(Coligado)+') OR li.Repassado ='+RT0+')');
  end else
    QrLotesIts.SQL.Add('AND li.Repassado in ('+RT0+', '+RT1+')');
  QrLotesIts.SQL.Add('AND li.ReforcoCxa in ('+DT0+', '+DT1+')');
  case RGAlinIts.ItemIndex of
    0: QrLotesIts.SQL.Add('AND ai.Status IS NULL');
    1: QrLotesIts.SQL.Add('AND ai.Status > -1000');
    2: QrLotesIts.SQL.Add('');// Ambos
  end;
////////////////////////////////////////////////////////////////////////////////
  QrSumT.Close;
  QrSumT.SQL.Clear;
  QrSumT.SQL.Add('SELECT SUM(li.Valor) Valor, COUNT(li.Controle) QTDE');
  QrSumT.SQL.Add('FROM lotesits li');
  QrSumT.SQL.Add('LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo');
  QrSumT.SQL.Add('LEFT JOIN repasits  ri ON ri.Origem=li.Controle');
  QrSumT.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  QrSumT.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  QrSumT.SQL.Add('LEFT JOIN entidades co ON co.Codigo=re.Coligado');
  if RGAlinIts.ItemIndex in ([0,1]) then
    QrSumT.SQL.Add('LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle');
  QrSumT.SQL.Add('WHERE lo.Tipo=0');
  QrSumT.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  QrSumT.SQL.Add('AND li.DDeposito BETWEEN :P0 AND :P1');
  QrSumT.SQL.Add('AND li.Quitado in (-1,'+PT0+', '+PT1+', '+PT2+')');
  if Coligado <> 0 then
  begin
    QrSumT.SQL.Add('AND ((li.Repassado = '+RT1+' AND re.Coligado='+
    IntToStr(Coligado)+') OR li.Repassado ='+RT0+')');
  end else
    QrSumT.SQL.Add('AND li.Repassado in ('+RT0+', '+RT1+')');
  QrSumT.SQL.Add('AND li.ReforcoCxa in ('+DT0+', '+DT1+')');
  case RGAlinIts.ItemIndex of
    0: QrSumT.SQL.Add('AND ai.Status IS NULL');
    1: QrSumT.SQL.Add('AND ai.Status > -1000');
    2: QrSumT.SQL.Add('');// Ambos
  end;
////////////////////////////////////////////////////////////////////////////////
  QrSumD.Close;
  QrSumD.SQL.Clear;
  QrSumD.SQL.Add('SELECT SUM(li.ValDeposito) Valor, COUNT(li.Controle) QTDE');
  QrSumD.SQL.Add('FROM lotesits li');
  QrSumD.SQL.Add('LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo');
  QrSumD.SQL.Add('LEFT JOIN repasits  ri ON ri.Origem=li.Controle');
  QrSumD.SQL.Add('LEFT JOIN repas     re ON re.Codigo=ri.Codigo');
  QrSumD.SQL.Add('LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente');
  QrSumD.SQL.Add('LEFT JOIN entidades co ON co.Codigo=re.Coligado');
  if RGAlinIts.ItemIndex in ([0,1]) then
    QrSumD.SQL.Add('LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle');
  QrSumD.SQL.Add('WHERE lo.Tipo=0 AND Depositado=1');
  QrSumD.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  QrSumD.SQL.Add('AND li.DDeposito BETWEEN :P0 AND :P1');
  QrSumD.SQL.Add('AND li.Quitado in (-1,'+PT0+', '+PT1+', '+PT2+')');
  if Coligado <> 0 then
  begin
    QrSumD.SQL.Add('AND ((li.Repassado = '+RT1+' AND re.Coligado='+
    IntToStr(Coligado)+') OR li.Repassado ='+RT0+')');
  end else
    QrSumD.SQL.Add('AND li.Repassado in ('+RT0+', '+RT1+')');
  QrSumD.SQL.Add('AND li.ReforcoCxa in ('+DT0+', '+DT1+')');
  case RGAlinIts.ItemIndex of
    0: QrSumD.SQL.Add('AND ai.Status IS NULL');
    1: QrSumD.SQL.Add('AND ai.Status > -1000');
    2: QrSumD.SQL.Add('');// Ambos
  end;
////////////////////////////////////////////////////////////////////////////////
  if Cliente <> 0 then
  begin
    QrLotesIts.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
    QrSumT.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
    QrSumD.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
  end;
  if Trim(FEmitente) <> '' then
  begin
    QrLotesIts.SQL.Add('AND li.Emitente LIKE "'+FEmitente+'"');
    QrSumT.SQL.Add('AND li.Emitente LIKE "'+FEmitente+'"');
    QrSumD.SQL.Add('AND li.Emitente LIKE "'+FEmitente+'"');
  end;
  if Trim(FCPF) <> '' then
  begin
    QrLotesIts.SQL.Add('AND li.CPF = "'+FCPF+'"');
    QrSumT.SQL.Add('AND li.CPF = "'+FCPF+'"');
    QrSumD.SQL.Add('AND li.CPF = "'+FCPF+'"');
  end;
  if CkP3.Checked = False then
  begin
    QrLotesIts.SQL.Add('AND li.NaoDeposita=0');
    QrSumT.SQL.Add('AND li.NaoDeposita=0');
    QrSumD.SQL.Add('AND li.NaoDeposita=0');
  end;
  QrLotesIts.SQL.Add('ORDER BY '+
    ItensOrdem[RGOrdem1.ItemIndex]+', '+
    ItensOrdem[RGOrdem2.ItemIndex]+', '+
    ItensOrdem[RGOrdem3.ItemIndex]+', '+
    ItensOrdem[RGOrdem4.ItemIndex]);
////////////////////////////////////////////////////////////////////////////////
  QrLotesIts.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrLotesIts.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  QrLotesIts.Open;
////////////////////////////////////////////////////////////////////////////////
  QrSumT.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrSumT.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  QrSumT.Open;
////////////////////////////////////////////////////////////////////////////////
  QrSumD.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPIni.Date);
  QrSumD.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPFim.Date);
  QrSumD.Open;
////////////////////////////////////////////////////////////////////////////////
  BtImprime.Enabled := True;
  Screen.Cursor := crDefault;
end;

procedure TFmDeposimp.QrLotesItsCalcFields(DataSet: TDataSet);
var
  Ano, Mes: Integer;
begin
  if QrLotesItsValDeposito.Value = QrLotesItsValor.Value then
    QrLotesItsCONF_V.Value := 1 else QrLotesItsCONF_V.Value := 0;
  //if QrLotesItsValDeposito.Value > 0 then
    //QrLotesItsCONF_I.Value := 1 else QrLotesItsCONF_I.Value := 0;
  QrLotesItsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrLotesItsCPF.Value);
  //
  QrLotesItsDDeposito_TXT.Value :=
    FormatDateTime(VAR_FORMATDATE3, QrLotesItsDDeposito.Value);
  QrLotesItsValor_TXT.Value :=
    FormatFloat('#,###,##0.00', QrLotesItsValor.Value);
  //
  QrLotesItsCONTAGEM.Value := 1;
  //
  Ano := Trunc(QrLotesItsPERIODO.Value) div 100;
  Mes := Trunc(QrLotesItsPERIODO.Value) mod 100;
  QrLotesItsMES_TXT.Value := FormatDateTime(VAR_FORMATDATE7,
    EncodeDate(Ano, Mes, 1));
  if QrLotesItsCodigo.Value = -1 then QrLotesItsNF_TXT.Value := 'Pg ch dv'
  else QrLotesItsNF_TXT.Value := FormatFloat('000000', QrLotesItsNF.Value);
end;

procedure TFmDeposimp.FormClose(Sender: TObject; var Action: TCloseAction);
var
  Cam: String;
begin
  Cam := Application.Title + '\XYConfig\DeposImp';
  Geral.WriteAppKeyLM2('Ordem1', Cam, RGOrdem1.ItemIndex, ktInteger);
  Geral.WriteAppKeyLM2('Ordem2', Cam, RGOrdem2.ItemIndex, ktInteger);
  Geral.WriteAppKeyLM2('Ordem3', Cam, RGOrdem3.ItemIndex, ktInteger);
  Geral.WriteAppKeyLM2('Ordem4', Cam, RGOrdem4.ItemIndex, ktInteger);
  Geral.WriteAppKeyLM2('Agrupa', Cam, RGAgrupa.ItemIndex, ktInteger);
  //Geral.WriteAppKeyLM2('Sit000', Cam, CkQuitado.Checked, ktBoolean);
  Geral.WriteAppKeyLM2('P0', Cam, CkP0.Checked, ktBoolean);
  Geral.WriteAppKeyLM2('P1', Cam, CkP1.Checked, ktBoolean);
  Geral.WriteAppKeyLM2('P2', Cam, CkP2.Checked, ktBoolean);
  Geral.WriteAppKeyLM2('Repasse', Cam, RGRepasse.ItemIndex, ktInteger);
  Geral.WriteAppKeyLM2('Reforco', Cam, RGReforco.ItemIndex, ktInteger);
end;

procedure TFmDeposimp.EdCPFExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCPF.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if MLAGeral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Application.MessageBox(PChar('N�mero inv�lido para CPF/CNPJ'), 'Erro',
        MB_OK+MB_ICONERROR);
      EdCPF.SetFocus;
    end else EdCPF.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCPF.Text := CO_VAZIO;
end;

procedure TFmDeposimp.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  FImprime := False;
  BtPesquisaClick(Self);
  BtImprimeClick(Self);
  ConfiguracaoInicial;
  BtPesquisaClick(Self);
end;

procedure TFmDeposimp.EdColigadoChange(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxDeposIts, 'Dep�sito de cheques');
end;

procedure TFmDeposimp.RGOrdem1Click(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.TPIniChange(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.TPFimChange(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.EdEmitenteChange(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.RGMascaraClick(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.EdCPFChange(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.CkQuitadoClick(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.EdBandaChange(Sender: TObject);
var
  CMC7: String;
  Banda: TBandaMagnetica;
  //Ban: Integer;
begin
  CMC7 := Geral.SoNumero_TT(EdBanda.Text);
  //Ban := Length(EdBanda.Text);
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
  begin
    if MLAGeral.CalculaCMC7(CMC7) = 0 then
    begin
      Banda := TBandaMagnetica.Create;
      Banda.BandaMagnetica := CMC7;
      EdBanco.Text   := Banda.Banco;
      EdAgencia.Text := Banda.Agencia;
      EdConta.Text   := Banda.Conta;
      EdCheque.Text  := Banda.Numero;
      //
      if LocalizaItem then
      begin
        ConfereCheque(1);
        Timer2.Enabled := True;
      end else begin
        Beep;
        Application.MessageBox('Cheque n�o localizado!', 'Erro',
          MB_OK+MB_ICONERROR);
      end;
    end;
  end;
end;

procedure TFmDeposimp.DBEdItensChange(Sender: TObject);
begin
  DBEdItens.Font.Color := MLAGeral.BoolToInt2(QrSumTQTDE.Value =
    QrSumDQTDE.Value, clGreen, clRed);
end;

procedure TFmDeposimp.DBEdValorChange(Sender: TObject);
begin
  DBEdValor.Font.Color := MLAGeral.BoolToInt2(QrSumTValor.Value =
    QrSumDValor.Value, clGreen, clRed);
end;

function TFmDeposimp.LocalizaItem: Boolean;
begin
  Result := QrLotesIts.Locate('Banco;Agencia;Conta;Cheque', VarArrayOf([
  Geral.IMV(EdBanco.Text), Geral.IMV(EdAgencia.Text), EdConta.Text,
  Geral.IMV(EdCheque.Text)]), []);
  if Result then FLotesIts := QrLotesItsControle.Value;
end;

procedure TFmDeposimp.ConfereCheque(Status: Integer);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   Depositado=:P0 WHERE Controle=:P1');
  Dmod.QrUpd.Params[0].AsInteger := Status;
  Dmod.QrUpd.Params[1].AsInteger := QrLotesItsControle.Value;
  Dmod.QrUpd.ExecSQL;
  //
  ReopenLotesIts;
end;

procedure TFmDeposimp.BtLimparClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   Depositado=0 WHERE Controle=:P0');
  //
  QrLotesIts.First;
  while not QrLotesIts.Eof do
  begin
    Dmod.QrUpd.Params[0].AsInteger := QrLotesItsControle.Value;
    Dmod.QrUpd.ExecSQL;
    QrLotesIts.Next;
  end;
  //
  ReopenLotesIts;
  Screen.Cursor := crDefault;
end;

procedure TFmDeposimp.DBG1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if Column.FieldName = 'Depositado' then
    MeuVCLSkin.DrawGrid(DBG1, Rect, 1, QrLotesItsDepositado.Value);
  if Column.FieldName = 'CONF_V' then
    MeuVCLSkin.DrawGrid(DBG1, Rect, 1, QrLotesItsCONF_V.Value);
  //if Column.FieldName = 'CONF_I' then
    //MeuVCLSkin.DrawGrid(DBG1, Rect, 1, QrLotesItsCONF_I.Value);
  if Column.FieldName = 'ReforcoCxa' then
    MeuVCLSkin.DrawGrid(DBG1, Rect, 1, QrLotesItsReforcoCxa.Value);
end;

procedure TFmDeposimp.DBG1CellClick(Column: TColumn);
var
  Valor: Double;
begin
  FLotesIts := QrLotesItsControle.Value;
  if (Column.Field.FieldName = 'Depositado') then
    ConfereCheque(MLAGeral.IntBool_Inverte(QrLotesItsDepositado.Value));
  if (Column.Field.FieldName = 'CONF_V') then
  //or (Column.Field.FieldName = 'CONF_I') then
  begin
    if QrLotesItsCONF_V.Value = 1 then Valor := 0 else
      Valor := QrLotesItsValor.Value;
    InsereValDeposito(Valor, QrLotesItsValor.Value, QrLotesItsControle.Value,
      False);
  end;
  if (Column.Field.FieldName = 'ReforcoCxa') then
    ReforcoCxa(MLAGeral.IntBool_Inverte(QrLotesItsReforcoCxa.Value));
  SomaLinhas(True);
end;

procedure TFmDeposimp.QrLotesItsAfterOpen(DataSet: TDataSet);
begin
  QrLotesIts.Locate('Controle', FLotesIts, []);
  EdBanda.Enabled := True;
end;

procedure TFmDeposimp.CkP0Click(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.CkP1Click(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.CkP2Click(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.CkR0Click(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.QrLotesItsAfterClose(DataSet: TDataSet);
begin
  EdBanda.Enabled := False;
end;

procedure TFmDeposimp.EdValorExit(Sender: TObject);
begin
  case RGOQue.ItemIndex of
    0:
    begin
      if LocalizaItem then
      //if QrLoc.RecordCount > 0 then
      begin
        InsereValDeposito(Geral.DMV(EdValor.Text), QrLotesItsValor.Value,
          QrLotesItsControle.Value, True);
        VoltaABanda;
      end;
    end;
    1: EdBanda.SetFocus;
  end;
end;

procedure TFmDeposimp.InsereValDeposito(Valor, Real: Double; Controle: Integer;
  Avisa: Boolean);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   ValDeposito=:P0 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  Dmod.QrUpd.Params[0].AsFloat   := Valor;
  Dmod.QrUpd.Params[1].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  ReopenLotesIts;
  //
  if (Int(Valor*100) <> (Real*100)) and Avisa  then
  begin
    Beep;
    Application.MessageBox('Valor n�o confere!', 'Erro', MB_OK+MB_ICONERROR);
  end;
end;

procedure TFmDeposimp.VoltaABanda;
begin
  EdBanda.Text := '';
  EdBanda.SetFocus;
end;

procedure TFmDeposimp.Timer2Timer(Sender: TObject);
begin
  Timer2.Enabled := False;
  case RGOque.ItemIndex of
   0: EdValor.SetFocus;
   1: VoltaABanda;
   2: EdValor.SetFocus;
  end;
end;

procedure TFmDeposimp.ReforcoCxa(Status: Integer);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1, ReforcoCxa=:P0 WHERE Controle=:P1');
  Dmod.QrUpd.Params[0].AsInteger := Status;
  Dmod.QrUpd.Params[1].AsInteger := QrLotesItsControle.Value;
  Dmod.QrUpd.ExecSQL;
  //
  ReopenLotesIts;
end;

procedure TFmDeposimp.RGRepasseClick(Sender: TObject);
begin
  LaColigado.Enabled := RGRepasse.ItemIndex > 0;
  EdColigado.Enabled := RGRepasse.ItemIndex > 0;
  CBColigado.Enabled := RGRepasse.ItemIndex > 0;
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.RGReforcoClick(Sender: TObject);
begin
  BtImprime.Enabled := False;
end;

procedure TFmDeposimp.BtConfiguraClick(Sender: TObject);
begin
  UMyMod.ConfigJanela20(Name, EdCliente, CBCliente, TPIni, TPFim,
    RGOrdem1, RGOrdem2, RGOrdem3, RGOrdem4, EdColigado, CBColigado,
    RGMascara, CkP0, CkP1, CkP2, CkP3, RGRepasse, RGReforco, RGAgrupa,
    nil, nil);
end;

procedure TFmDeposimp.Selecionados1Click(Sender: TObject);
begin
  ContaDeposito(1);
end;

procedure TFmDeposimp.Tudo1Click(Sender: TObject);
begin
  ContaDeposito(2);
end;

procedure TFmDeposimp.ContaDeposito(Tipo: Integer);
begin
  FLotesIts := QrLotesItsControle.Value;
  VAR_CARTDEP := -2;
  Application.CreateForm(TFmCartDep, FmCartDep);
  FmCartDep.ShowModal;
  FmCartDep.Destroy;
  Screen.Cursor := crHourGlass;
  if VAR_CARTDEP <> -2 then
  begin
    case Tipo of
      1:
      begin
        if DBG1.SelectedRows.Count = 0 then DepositaAtual
        else DepositaSelecionados;
      end;
      2:
      begin
        QrLotesIts.DisableControls;
        QrLotesIts.First;
        while not QrLotesIts.Eof do
        begin
          DepositaAtual;
          QrLotesIts.Next;
        end;
        QrLotesIts.EnableControls;
      end;
      else Application.MessageBox('Op��o de conta n�o implementada!', 'Aviso',
        MB_OK+MB_ICONINFORMATION);
    end;
    ReopenLotesIts;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmDeposimp.DepositaSelecionados;
var
  i : Integer;
begin
  with DBG1.DataSource.DataSet do
  for i:= 0 to DBG1.SelectedRows.Count-1 do
  begin
    GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
    DepositaAtual;
  end;
end;

procedure TFmDeposimp.DepositaAtual;
begin
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE lotesits SET AlterWeb=1, CartDep=:P0 WHERE Controle=:P1 ');
  //
  Dmod.QrUpdM.Params[0].AsInteger := VAR_CARTDEP;
  Dmod.QrUpdM.Params[1].AsInteger := QrLotesItsControle.Value;
  //
  Dmod.QrUpdM.ExecSQL;
end;

procedure TFmDeposimp.DBG1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key=VK_DOWN) and (Shift=[ssShift]) then PreparaSoma(True)  else
  if (Key=VK_UP)   and (Shift=[ssShift]) then PreparaSoma(False) else
  if (Key=VK_DOWN)                       then PreparaSoma(True)  else
  if (Key=VK_UP)                         then PreparaSoma(False); 
end;

procedure TFmDeposimp.PreparaSoma(Ascendente: Boolean);
begin
  FAscendente := Ascendente;
  if Timer3.Enabled then Timer3.Enabled := False;
  Timer3.Enabled := True;
end;

procedure TFmDeposimp.SomaLinhas(Ascendente: Boolean);
var
  Valor: Double;
  i, k: Integer;
begin
  k := QrLotesItsControle.Value;
  if DBG1.SelectedRows.Count = 0 then Valor := QrLotesItsValor.Value else
  begin
    Valor := 0;
    with DBG1.DataSource.DataSet do
    begin
      if Ascendente then
      begin
        for i:= 0 to DBG1.SelectedRows.Count-1 do
        begin
          GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          Valor := Valor + QrLotesItsValor.Value;
        end;
      end else begin
        for i:= DBG1.SelectedRows.Count-1 downto 0 do
        begin
          GotoBookmark(pointer(DBG1.SelectedRows.Items[i]));
          Valor := Valor + QrLotesItsValor.Value;
        end;
      end;
    end;
    QrLotesIts.Locate('Controle', k, []);
  end;
  EdSoma.Text := Geral.FFT(Valor, 2, siNegativo);
end;

procedure TFmDeposimp.Timer3Timer(Sender: TObject);
begin
  SomaLinhas(FAscendente);
end;

procedure TFmDeposimp.BtDepositoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDeposito, BtDeposito);
end;

procedure TFmDeposimp.SpeedButton1Click(Sender: TObject);
var
  i: Integer;
begin
  Screen.Cursor := crHourGlass;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET Teste=0');
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET Teste=Teste+2 WHERE Controle=:P0');
  Query1.Close;
  Query1.SQL := QrLotesIts.SQL;

  Query1.SQL := QrLotesIts.SQL;
  for i := 0 to QrLotesIts.ParamCount -1 do
  begin
    Query1.Params[i] := QrLotesIts.Params[i];
  end;  
  Query1.Open;
  Query1.First;
  while not Query1.Eof do
  begin
    Dmod.QrUpd.Params[0].AsInteger := Query1Controle.Value;
    Dmod.QrUpd.ExecSQL;
    Query1.Next;
  end;
  Query1.EnableControls;
  Query1.First;
  Screen.Cursor := crDefault;
end;

procedure TFmDeposimp.frxDeposIts_GetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_CLIENTE' then
  begin
    if CBCliente.KeyValue = NULL then Value := ' ' else
    Value := CBCliente.Text;
  end else if VarName = 'VARF_TODOS' then
  begin
    if CBCliente.KeyValue = NULL then Value := 'TODOS CLIENTES' else
    Value := ' ';
  end else if VarName = 'VARF_REFORCO' then
  begin
    case RGReforco.ItemIndex of
      0: Value := 'N�o';
      1: Value := 'Sim';
      2: Value := 'N+S';
      else Value := '???';
    end;
  end else if VarName = 'VARF_REPASSE' then
  begin
    case RGRepasse.ItemIndex of
      0: Value := 'N�o';
      1: Value := 'Sim';
      2: Value := 'N+S';
      else Value := '???';
    end;
  end else if VarName = 'VARF_PERIODO' then Value :=
    FormatDateTime(VAR_FORMATDATE3, TPIni.Date) + CO_ATE +
    FormatDateTime(VAR_FORMATDATE3, TPFim.Date)
  else if VarName = 'VARF_QTD_CHEQUES' then Value := QrLotesIts.RecordCount
  else if VarName = 'VFR_LA1NOME' then
  begin
    case RGOrdem1.ItemIndex of
      0: Value := 'Cliente: '+QrLotesItsNOMECLIENTE.Value;
      1: Value := 'Dep�sito no dia  '+QrLotesItsDDeposito_TXT.Value;
      2: Value := 'Dep�sito no m�s de '+QrLotesItsMES_TXT.Value;
      3: Value := 'Emitido por: '+QrLotesItsEmitente.Value;
      4: Value := 'CPF/CNPJ: '+QrLotesItsCPF_TXT.Value;
    end;
  end
  else if VarName = 'VFR_LA2NOME' then
  begin
    case RGOrdem2.ItemIndex of
      0: Value := 'Cliente: '+QrLotesItsNOMECLIENTE.Value;
      1: Value := 'Dep�sito no dia  '+QrLotesItsDDeposito_TXT.Value;
      2: Value := 'Dep�sito no m�s de '+QrLotesItsMES_TXT.Value;
      3: Value := 'Emitido por: '+QrLotesItsEmitente.Value;
      4: Value := 'CPF/CNPJ: '+QrLotesItsCPF_TXT.Value;
    end;
  end
  else if VarName = 'VFR_LA3NOME' then
  begin
    case RGOrdem3.ItemIndex of
      0: Value := 'Cliente: '+QrLotesItsNOMECLIENTE.Value;
      1: Value := 'Dep�sito no dia  '+QrLotesItsDDeposito_TXT.Value;
      2: Value := 'Dep�sito no m�s de '+QrLotesItsMES_TXT.Value;
      3: Value := 'Emitido por: '+QrLotesItsEmitente.Value;
      4: Value := 'CPF/CNPJ: '+QrLotesItsCPF_TXT.Value;
    end;
  end
  else if VarName = 'VARF_SITUACOES' then
  begin
    Value := ' ';
    //if CkQuitado.Checked then Value := Value + CkQuitado.Caption;
    //if CkRepassado.Checked then Value := Value + '(incluindo repassados)';
    //if Trim(Value) = '' then Value := '(??????)';
  end
  else if VarName = 'VARF_FILTROS' then
  begin
    Value := '';
    if Geral.IMV(EdColigado.Text) <> 0 then Value := Value +
      'Coligado: '+CBColigado.Text+ '  ';
    if FEmitente <> '' then Value := Value + '  {Emitente: '+ FEmitente+'}';
    if FCPF <> '' then Value := Value + '  {CPF/CNPJ: '+
    Geral.FormataCNPJ_TT(FCPF)+'}';
    if Value <> '' then Value := 'FILTROS : '+Value;
  end;


  if VarName = 'VFR_ORD1' then
  begin
    if RGAgrupa.ItemIndex < 1 then Value := 0 else
    Value := RGOrdem1.ItemIndex + 1;
  end else
  if VarName = 'VFR_ORD2' then
  begin
    if RGAgrupa.ItemIndex < 2 then Value := 0 else
    Value := RGOrdem2.ItemIndex + 1;
  end else
  if VarName = 'VFR_ORD3' then
  begin
    if RGAgrupa.ItemIndex < 3 then Value := 0 else
    Value := RGOrdem3.ItemIndex + 1;
  end else
  if VarName = 'VARF_ANALITICO' then
  begin
    if RGTipo.ItemIndex = 1 then Value := 0 else Value := 17;
  end
end;

end.

