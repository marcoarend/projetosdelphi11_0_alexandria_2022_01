unit ModuleLotes;

interface

uses
  SysUtils, Classes, DB, mySQLDbTables, dmkGeral, UnDmkEnums;

type
  TDmLotes = class(TDataModule)
    QrSPC_Cli: TmySQLQuery;
    QrSPC_CliSPC_ValMin: TFloatField;
    QrSPC_CliSPC_ValMax: TFloatField;
    QrSPC_CliServidor: TWideStringField;
    QrSPC_CliSPC_Porta: TSmallintField;
    QrSPC_CliPedinte: TWideStringField;
    QrSPC_CliCodigSocio: TIntegerField;
    QrSPC_CliModalidade: TIntegerField;
    QrSPC_CliInfoExtra: TIntegerField;
    QrSPC_CliBAC_CMC7: TSmallintField;
    QrSPC_CliTipoCred: TSmallintField;
    QrSPC_CliSenhaSocio: TWideStringField;
    QrSPC_CliCodigo: TIntegerField;
    QrSPC_CliValAviso: TFloatField;
    QrSPC_CliVALCONSULTA: TFloatField;
    QrSum0: TmySQLQuery;
    QrSum0Valor: TFloatField;
    QrSum0Dias: TFloatField;
    QrSum0VlrCompra: TFloatField;
    QrSum0PRAZO_MEDIO: TFloatField;
    QrSum0ITENS: TLargeintField;
    QrSum0Vencto: TDateField;
    QrSum1: TmySQLQuery;
    QrSum1TaxaVal: TFloatField;
    QrSum2: TmySQLQuery;
    QrSum2TaxaVal: TFloatField;
    QrSum3: TmySQLQuery;
    QrSum3TaxaVal: TFloatField;
    QrSum4: TmySQLQuery;
    QrSum4TaxaVal: TFloatField;
    QrSum5: TmySQLQuery;
    QrSum5TaxaVal: TFloatField;
    QrSum6: TmySQLQuery;
    QrSum6TaxaVal: TFloatField;
    QrEmLot6: TmySQLQuery;
    QrEmLot6IRRF: TFloatField;
    QrEmLot6IRRF_Val: TFloatField;
    QrEmLot6ISS: TFloatField;
    QrEmLot6ISS_Val: TFloatField;
    QrEmLot6PIS_R: TFloatField;
    QrEmLot6PIS_R_Val: TFloatField;
    QrEmLot6TaxaVal: TFloatField;
    QrEmLot6ValValorem: TFloatField;
    QrEmLot6Codigo: TIntegerField;
    QrEmLot6AdValorem: TFloatField;
    QrEmLot5: TmySQLQuery;
    QrEmLot5IRRF: TFloatField;
    QrEmLot5IRRF_Val: TFloatField;
    QrEmLot5ISS: TFloatField;
    QrEmLot5ISS_Val: TFloatField;
    QrEmLot5PIS_R: TFloatField;
    QrEmLot5PIS_R_Val: TFloatField;
    QrEmLot5TaxaVal: TFloatField;
    QrEmLot5ValValorem: TFloatField;
    QrEmLot5Codigo: TIntegerField;
    QrEmLot5AdValorem: TFloatField;
    QrEmLot4: TmySQLQuery;
    QrEmLot4IRRF: TFloatField;
    QrEmLot4IRRF_Val: TFloatField;
    QrEmLot4ISS: TFloatField;
    QrEmLot4ISS_Val: TFloatField;
    QrEmLot4PIS_R: TFloatField;
    QrEmLot4PIS_R_Val: TFloatField;
    QrEmLot4TaxaVal: TFloatField;
    QrEmLot4ValValorem: TFloatField;
    QrEmLot4Codigo: TIntegerField;
    QrEmLot4AdValorem: TFloatField;
    QrEmLot3: TmySQLQuery;
    QrEmLot3IRRF: TFloatField;
    QrEmLot3IRRF_Val: TFloatField;
    QrEmLot3ISS: TFloatField;
    QrEmLot3ISS_Val: TFloatField;
    QrEmLot3PIS_R: TFloatField;
    QrEmLot3PIS_R_Val: TFloatField;
    QrEmLot3TaxaVal: TFloatField;
    QrEmLot3ValValorem: TFloatField;
    QrEmLot3Codigo: TIntegerField;
    QrEmLot3AdValorem: TFloatField;
    QrEmLot1: TmySQLQuery;
    QrEmLot1IRRF: TFloatField;
    QrEmLot1IRRF_Val: TFloatField;
    QrEmLot1ISS: TFloatField;
    QrEmLot1ISS_Val: TFloatField;
    QrEmLot1PIS_R: TFloatField;
    QrEmLot1PIS_R_Val: TFloatField;
    QrEmLot1TaxaVal: TFloatField;
    QrEmLot1ValValorem: TFloatField;
    QrEmLot1Codigo: TIntegerField;
    QrEmLot1AdValorem: TFloatField;
    QrExEnti1: TmySQLQuery;
    QrExEnti1Codigo: TIntegerField;
    QrExEnti1Maior: TFloatField;
    QrExEnti1Menor: TFloatField;
    QrExEnti1AdVal: TFloatField;
    QrExEnti2: TmySQLQuery;
    QrExEnti2Codigo: TIntegerField;
    QrExEnti2Maior: TFloatField;
    QrExEnti2Menor: TFloatField;
    QrExEnti2AdVal: TFloatField;
    QrEmLot2: TmySQLQuery;
    QrEmLot2IRRF: TFloatField;
    QrEmLot2IRRF_Val: TFloatField;
    QrEmLot2ISS: TFloatField;
    QrEmLot2ISS_Val: TFloatField;
    QrEmLot2PIS_R: TFloatField;
    QrEmLot2PIS_R_Val: TFloatField;
    QrEmLot2TaxaVal: TFloatField;
    QrEmLot2ValValorem: TFloatField;
    QrEmLot2Codigo: TIntegerField;
    QrEmLot2AdValorem: TFloatField;
    QrExEnti3: TmySQLQuery;
    IntegerField1: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    QrExEnti3AdVal: TFloatField;
    QrExEnti4: TmySQLQuery;
    IntegerField2: TIntegerField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    QrExEnti4AdVal: TFloatField;
    QrExEnti5: TmySQLQuery;
    IntegerField3: TIntegerField;
    FloatField5: TFloatField;
    FloatField6: TFloatField;
    QrExEnti5AdVal: TFloatField;
    QrExEnti6: TmySQLQuery;
    IntegerField4: TIntegerField;
    FloatField7: TFloatField;
    FloatField8: TFloatField;
    QrExEnti6AdVal: TFloatField;
    QrLI: TmySQLQuery;
    QrLICodigo: TIntegerField;
    QrLIControle: TIntegerField;
    QrLIComp: TIntegerField;
    QrLIBanco: TIntegerField;
    QrLIAgencia: TIntegerField;
    QrLIConta: TWideStringField;
    QrLICheque: TIntegerField;
    QrLICPF: TWideStringField;
    QrLIEmitente: TWideStringField;
    QrLIBruto: TFloatField;
    QrLIDesco: TFloatField;
    QrLIValor: TFloatField;
    QrLIEmissao: TDateField;
    QrLIDCompra: TDateField;
    QrLIDDeposito: TDateField;
    QrLIVencto: TDateField;
    QrLITxaCompra: TFloatField;
    QrLITxaJuros: TFloatField;
    QrLITxaAdValorem: TFloatField;
    QrLIVlrCompra: TFloatField;
    QrLIVlrAdValorem: TFloatField;
    QrLIDMais: TIntegerField;
    QrLIDias: TIntegerField;
    QrLIDuplicata: TWideStringField;
    QrLIDevolucao: TIntegerField;
    QrLIQuitado: TIntegerField;
    QrLILk: TIntegerField;
    QrLIDataCad: TDateField;
    QrLIDataAlt: TDateField;
    QrLIUserCad: TIntegerField;
    QrLIUserAlt: TIntegerField;
    QrLIPraca: TIntegerField;
    QrLITipo: TSmallintField;
    QrLIBcoCobra: TIntegerField;
    QrLIAgeCobra: TIntegerField;
    QrLICBE: TIntegerField;
    QrLISCB: TIntegerField;
    QrLICliente: TIntegerField;
    QrLICartDep: TIntegerField;
    QrLIDescAte: TDateField;
    QrLITipific: TSmallintField;
    QrLIStatusSPC: TSmallintField;
    QrLocDU: TmySQLQuery;
    QrLocDUDuplicatas: TLargeintField;
    QrLocCH: TmySQLQuery;
    QrLocCHCheques: TLargeintField;
    QrSumTxs: TmySQLQuery;
    QrSumTxsTaxaVal: TFloatField;
    QrLCalc: TmySQLQuery;
    QrLCalcAdValorem: TFloatField;
    QrLCalcIOC: TFloatField;
    QrLCalcCPMF: TFloatField;
    QrLCalcIRRF: TFloatField;
    QrLCalcPIS: TFloatField;
    QrLCalcPIS_R: TFloatField;
    QrLCalcCOFINS: TFloatField;
    QrLCalcCOFINS_R: TFloatField;
    QrLCalcISS: TFloatField;
    QrLCalcCliente: TIntegerField;
    QrLCalcData: TDateField;
    QrLCalcVAL_LIQUIDO: TFloatField;
    QrLCalcValValorem: TFloatField;
    QrLCalcTxCompra: TFloatField;
    QrLCalcISS_Val: TFloatField;
    QrLCalcIRRF_Val: TFloatField;
    QrLCalcPIS_R_Val: TFloatField;
    QrLCalcTotal: TFloatField;
    QrLCalcIOC_Val: TFloatField;
    QrLCalcCPMF_Val: TFloatField;
    QrLCalcTarifas: TFloatField;
    QrLCalcOcorP: TFloatField;
    QrLCalcCHDevPg: TFloatField;
    QrLCalcSobraNow: TFloatField;
    QrLCalcIOFv: TFloatField;
    QrLCalcIOFd: TFloatField;
    QrLCalcTipo: TSmallintField;
    QrLCalcSimples: TSmallintField;
    QrTxs: TmySQLQuery;
    QrTxsCodigo: TIntegerField;
    QrTxsControle: TIntegerField;
    QrTxsTaxaCod: TIntegerField;
    QrTxsTaxaTxa: TFloatField;
    QrTxsTaxaVal: TFloatField;
    QrTxsLk: TIntegerField;
    QrTxsDataCad: TDateField;
    QrTxsDataAlt: TDateField;
    QrTxsUserCad: TIntegerField;
    QrTxsUserAlt: TIntegerField;
    QrTxsForma: TSmallintField;
    QrTxsTaxaQtd: TFloatField;
    QrTxsBase: TSmallintField;
    QrTxsSysAQtd: TFloatField;
    QrDupNeg: TmySQLQuery;
    QrDupNegLote: TSmallintField;
    QrDupNegCodigo: TIntegerField;
    QrDupNegControle: TIntegerField;
    QrDupNegComp: TIntegerField;
    QrDupNegBanco: TIntegerField;
    QrDupNegAgencia: TIntegerField;
    QrDupNegConta: TWideStringField;
    QrDupNegCheque: TIntegerField;
    QrDupNegCPF: TWideStringField;
    QrDupNegEmitente: TWideStringField;
    QrDupNegValor: TFloatField;
    QrDupNegEmissao: TDateField;
    QrDupNegDCompra: TDateField;
    QrDupNegDDeposito: TDateField;
    QrDupNegVencto: TDateField;
    QrDupNegTxaCompra: TFloatField;
    QrDupNegTxaJuros: TFloatField;
    QrDupNegTxaAdValorem: TFloatField;
    QrDupNegVlrCompra: TFloatField;
    QrDupNegVlrAdValorem: TFloatField;
    QrDupNegDMais: TIntegerField;
    QrDupNegDias: TIntegerField;
    QrDupNegDuplicata: TWideStringField;
    QrDupNegLk: TIntegerField;
    QrDupNegDataCad: TDateField;
    QrDupNegDataAlt: TDateField;
    QrDupNegUserCad: TIntegerField;
    QrDupNegUserAlt: TIntegerField;
    QrDupNegDevolucao: TIntegerField;
    QrDupNegDesco: TFloatField;
    QrDupNegQuitado: TIntegerField;
    QrDupNegBruto: TFloatField;
    QrTaxasCli: TmySQLQuery;
    QrTaxasCliGenero: TSmallintField;
    QrTaxasCliForma: TSmallintField;
    QrTaxasCliBase: TSmallintField;
    QrTaxasCliNOMETAXA: TWideStringField;
    QrTaxasCliControle: TIntegerField;
    QrTaxasCliCliente: TIntegerField;
    QrTaxasCliLk: TIntegerField;
    QrTaxasCliDataCad: TDateField;
    QrTaxasCliDataAlt: TDateField;
    QrTaxasCliUserCad: TIntegerField;
    QrTaxasCliUserAlt: TIntegerField;
    QrTaxasCliTaxa: TIntegerField;
    QrTaxasCliValor: TFloatField;
    QrLocs: TmySQLQuery;
    QrLocsCodigo: TIntegerField;
    QrLocsTipo: TSmallintField;
    QrDupSac: TmySQLQuery;
    QrDupSacDuplicata: TWideStringField;
    QrDupSacEmissao: TDateField;
    QrDupSacValor: TFloatField;
    QrDupSacVencto: TDateField;
    QrDupSacBanco: TIntegerField;
    QrDupSacAgencia: TIntegerField;
    QrDupSacNOMEBANCO: TWideStringField;
    QrSumOPAntigo: TmySQLQuery;
    QrSumP: TmySQLQuery;
    QrSumPDebito: TFloatField;
    QrVerNF: TmySQLQuery;
    QrVerNFCodigo: TIntegerField;
    QrLocNF: TmySQLQuery;
    QrLocNFCodigo: TIntegerField;
    QrSOA: TmySQLQuery;
    QrSOACodigo: TIntegerField;
    QrSOALotesIts: TIntegerField;
    QrSOADataO: TDateField;
    QrSOAOcorrencia: TIntegerField;
    QrSOAValor: TFloatField;
    QrSOALoteQuit: TIntegerField;
    QrSOATaxaB: TFloatField;
    QrSOATaxaP: TFloatField;
    QrSOATaxaV: TFloatField;
    QrSOAPago: TFloatField;
    QrSOADataP: TDateField;
    QrSOALk: TIntegerField;
    QrSOADataCad: TDateField;
    QrSOADataAlt: TDateField;
    QrSOAUserCad: TIntegerField;
    QrSOAUserAlt: TIntegerField;
    QrSOAData3: TDateField;
    QrSOAStatus: TSmallintField;
    QrSOACliente: TIntegerField;
    QrSOACLIENTELOTE: TIntegerField;
    QrSDO: TmySQLQuery;
    QrSDOCliente: TIntegerField;
    QrSDODCompra: TDateField;
    QrSDOValor: TFloatField;
    QrSDODDeposito: TDateField;
    QrSDOQuitado: TIntegerField;
    QrSDOTotalJr: TFloatField;
    QrSDOTotalDs: TFloatField;
    QrSDOTotalPg: TFloatField;
    QrSDOVencto: TDateField;
    QrSDOData3: TDateField;
    QrTCD: TmySQLQuery;
    QrTCDValor: TFloatField;
    QrTCDCliente: TIntegerField;
    QrTCDStatus: TSmallintField;
    QrTCDData1: TDateField;
    QrTCDData3: TDateField;
    QrTCDJurosV: TFloatField;
    QrTCDDesconto: TFloatField;
    QrTCDValPago: TFloatField;
    QrTCDJurosP: TFloatField;
    QrSRS: TmySQLQuery;
    QrSRSValor: TFloatField;
    QrCart: TmySQLQuery;
    QrCartCodigo: TIntegerField;
    QrCartTipo: TIntegerField;
    QrCHsDev: TmySQLQuery;
    QrCHsDevCodigo: TIntegerField;
    QrEmLotIts: TmySQLQuery;
    QrEmLotItsCodigo: TIntegerField;
    QrEmLotItsControle: TIntegerField;
    QrEmLotItsTaxaPer: TFloatField;
    QrEmLotItsTaxaVal: TFloatField;
    QrEmLotItsJuroPer: TFloatField;
    QrSumOco: TmySQLQuery;
    QrSumOcoPAGO: TFloatField;
    QrNovoTxs: TmySQLQuery;
    QrNovoTxsCodigo: TIntegerField;
    QrNovoTxsNome: TWideStringField;
    QrNovoTxsGenero: TSmallintField;
    QrNovoTxsForma: TSmallintField;
    QrNovoTxsAutomatico: TSmallintField;
    QrNovoTxsValor: TFloatField;
    QrNovoTxsMostra: TSmallintField;
    QrNovoTxsLk: TIntegerField;
    QrNovoTxsDataCad: TDateField;
    QrNovoTxsDataAlt: TDateField;
    QrNovoTxsUserCad: TIntegerField;
    QrNovoTxsUserAlt: TIntegerField;
    QrLocOc: TmySQLQuery;
    QrLocOcCodigo: TIntegerField;
    QrLocOcOcorreu: TIntegerField;
    QrLocOcData: TDateField;
    QrLocOcJuros: TFloatField;
    QrLocOcPago: TFloatField;
    QrLocOcLotePg: TIntegerField;
    QrLocOcLk: TIntegerField;
    QrLocOcDataCad: TDateField;
    QrLocOcDataAlt: TDateField;
    QrLocOcUserCad: TIntegerField;
    QrLocOcUserAlt: TIntegerField;
    QrSumOc: TmySQLQuery;
    QrSumOcJuros: TFloatField;
    QrSumOcPago: TFloatField;
    QrLastOcor: TmySQLQuery;
    QrLastOcorData: TDateField;
    QrLastOcorCodigo: TIntegerField;
    QrLotExist: TmySQLQuery;
    QrLocSaca: TmySQLQuery;
    QrLocSacaNome: TWideStringField;
    QrLocSacaCNPJ: TWideStringField;
    QrLocSacaRua: TWideStringField;
    QrLocSacaNumero: TFloatField;
    QrLocSacaCompl: TWideStringField;
    QrLocSacaBairro: TWideStringField;
    QrLocSacaCidade: TWideStringField;
    QrLocSacaCEP: TIntegerField;
    QrLocSacaTel1: TWideStringField;
    QrLocSacaUF: TWideStringField;
    QrSumCHP: TmySQLQuery;
    QrSumCHPPago: TFloatField;
    QrLastCHDV: TmySQLQuery;
    QrLastCHDVData: TDateField;
    QrSumPg: TmySQLQuery;
    QrSumPgJuros: TFloatField;
    QrSumPgPago: TFloatField;
    QrSumPgDesco: TFloatField;
    QrLocPg: TmySQLQuery;
    QrLocPgCodigo: TIntegerField;
    QrLocPgAlinIts: TIntegerField;
    QrLocPgData: TDateField;
    QrLocPgLk: TIntegerField;
    QrLocPgDataCad: TDateField;
    QrLocPgDataAlt: TDateField;
    QrLocPgUserCad: TIntegerField;
    QrLocPgUserAlt: TIntegerField;
    QrLocPgJuros: TFloatField;
    QrLocPgPago: TFloatField;
    QrLocLote: TmySQLQuery;
    QrLocLoteLote: TSmallintField;
    QrSDUOpen: TmySQLQuery;
    QrSDUOpenValor: TFloatField;
    QrSDUOpenDUPLICATAS: TLargeintField;
    QrSumDUP: TmySQLQuery;
    QrSumDUPPago: TFloatField;
    QrLastDPg: TmySQLQuery;
    QrLastDPgData: TDateField;
    QrLastDPgControle: TIntegerField;
    QrPgD: TmySQLQuery;
    QrPgDJuros: TFloatField;
    QrPgDDesco: TFloatField;
    QrPgDPago: TFloatField;
    QrPgDValor: TFloatField;
    QrPgDMaxData: TDateField;
    QrSumRepCli: TmySQLQuery;
    QrSumRepCliValor: TFloatField;
    QrSAlin: TmySQLQuery;
    QrSAlinValor: TFloatField;
    QrSAlinAberto: TFloatField;
    QrSAlinCheques: TLargeintField;
    QrOcorA: TmySQLQuery;
    QrOcorATipo: TSmallintField;
    QrOcorATIPODOC: TWideStringField;
    QrOcorANOMEOCORRENCIA: TWideStringField;
    QrOcorACodigo: TIntegerField;
    QrOcorALotesIts: TIntegerField;
    QrOcorADataO: TDateField;
    QrOcorAOcorrencia: TIntegerField;
    QrOcorAValor: TFloatField;
    QrOcorALoteQuit: TIntegerField;
    QrOcorALk: TIntegerField;
    QrOcorADataCad: TDateField;
    QrOcorADataAlt: TDateField;
    QrOcorAUserCad: TIntegerField;
    QrOcorAUserAlt: TIntegerField;
    QrOcorATaxaP: TFloatField;
    QrOcorATaxaV: TFloatField;
    QrOcorAPago: TFloatField;
    QrOcorADataP: TDateField;
    QrOcorATaxaB: TFloatField;
    QrOcorAData3: TDateField;
    QrOcorAStatus: TSmallintField;
    QrOcorASALDO: TFloatField;
    QrOcorAATUALIZADO: TFloatField;
    QrOcorADOCUM_TXT: TWideStringField;
    QrOcorABanco: TIntegerField;
    QrOcorAAgencia: TIntegerField;
    QrOcorACheque: TIntegerField;
    QrOcorADuplicata: TWideStringField;
    QrOcorAConta: TWideStringField;
    QrOcorAEmissao: TDateField;
    QrOcorADCompra: TDateField;
    QrOcorAVencto: TDateField;
    QrOcorADDeposito: TDateField;
    QrOcorAEmitente: TWideStringField;
    QrOcorACPF: TWideStringField;
    QrOcorACLIENTELOTE: TIntegerField;
    QrOcorACliente: TIntegerField;
    QrOcorADescri: TWideStringField;
    DsOcorA: TDataSource;
    DsOcorTf: TDataSource;
    QrOcorTf: TmySQLQuery;
    QrOcorTfTOTAL_A: TFloatField;
    QrLocCliImp: TmySQLQuery;
    QrLocCliImpCodigo: TIntegerField;
    QrLocCliImpNOMECLI: TWideStringField;
    QrLocCliImpLimiCred: TFloatField;
    DsLocCliImp: TDataSource;
    DsRepCli: TDataSource;
    QrRepCli: TmySQLQuery;
    QrRepCliNOMECLI: TWideStringField;
    QrRepCliLote: TIntegerField;
    QrRepCliCodigo: TIntegerField;
    QrRepCliControle: TIntegerField;
    QrRepCliComp: TIntegerField;
    QrRepCliBanco: TIntegerField;
    QrRepCliAgencia: TIntegerField;
    QrRepCliConta: TWideStringField;
    QrRepCliCheque: TIntegerField;
    QrRepCliCPF: TWideStringField;
    QrRepCliEmitente: TWideStringField;
    QrRepCliBruto: TFloatField;
    QrRepCliDesco: TFloatField;
    QrRepCliValor: TFloatField;
    QrRepCliEmissao: TDateField;
    QrRepCliDCompra: TDateField;
    QrRepCliDDeposito: TDateField;
    QrRepCliVencto: TDateField;
    QrRepCliTxaCompra: TFloatField;
    QrRepCliTxaJuros: TFloatField;
    QrRepCliTxaAdValorem: TFloatField;
    QrRepCliVlrCompra: TFloatField;
    QrRepCliVlrAdValorem: TFloatField;
    QrRepCliDMais: TIntegerField;
    QrRepCliDias: TIntegerField;
    QrRepCliDuplicata: TWideStringField;
    QrRepCliDevolucao: TIntegerField;
    QrRepCliQuitado: TIntegerField;
    QrRepCliLk: TIntegerField;
    QrRepCliDataCad: TDateField;
    QrRepCliDataAlt: TDateField;
    QrRepCliUserCad: TIntegerField;
    QrRepCliUserAlt: TIntegerField;
    QrRepCliPraca: TIntegerField;
    QrRepCliBcoCobra: TIntegerField;
    QrRepCliAgeCobra: TIntegerField;
    QrRepCliTotalJr: TFloatField;
    QrRepCliTotalDs: TFloatField;
    QrRepCliTotalPg: TFloatField;
    QrRepCliData3: TDateField;
    QrRepCliProrrVz: TIntegerField;
    QrRepCliProrrDd: TIntegerField;
    QrRepCliRepassado: TSmallintField;
    QrRepCliDepositado: TSmallintField;
    QrRepCliValQuit: TFloatField;
    QrRepCliValDeposito: TFloatField;
    QrRepCliTipo: TIntegerField;
    QrRepCliAliIts: TIntegerField;
    QrRepCliAlinPgs: TIntegerField;
    QrRepCliNaoDeposita: TSmallintField;
    QrRepCliReforcoCxa: TSmallintField;
    QrRepCliCartDep: TIntegerField;
    QrRepCliCobranca: TIntegerField;
    QrRepCliRepCli: TIntegerField;
    QrRepCliSEQ: TIntegerField;
    QrDUVenc: TmySQLQuery;
    StringField9: TWideStringField;
    DateField6: TDateField;
    FloatField18: TFloatField;
    DateField7: TDateField;
    DsDUVenc: TDataSource;
    DsDOpen: TDataSource;
    QrDOpen: TmySQLQuery;
    QrDOpenControle: TIntegerField;
    QrDOpenDuplicata: TWideStringField;
    QrDOpenDCompra: TDateField;
    QrDOpenValor: TFloatField;
    QrDOpenDDeposito: TDateField;
    QrDOpenEmitente: TWideStringField;
    QrDOpenCPF: TWideStringField;
    QrDOpenCliente: TIntegerField;
    QrDOpenSTATUS: TWideStringField;
    QrDOpenQuitado: TIntegerField;
    QrDOpenTotalJr: TFloatField;
    QrDOpenTotalDs: TFloatField;
    QrDOpenTotalPg: TFloatField;
    QrDOpenSALDO_DESATUALIZ: TFloatField;
    QrDOpenSALDO_ATUALIZADO: TFloatField;
    QrDOpenNOMESTATUS: TWideStringField;
    QrDOpenVencto: TDateField;
    QrDOpenDDCALCJURO: TIntegerField;
    QrDOpenData3: TDateField;
    QrDOpenRepassado: TSmallintField;
    QrDUOpen: TmySQLQuery;
    QrDUOpenDuplicata: TWideStringField;
    QrDUOpenDCompra: TDateField;
    QrDUOpenValor: TFloatField;
    QrDUOpenDDeposito: TDateField;
    DsDUOpen: TDataSource;
    DsCHOpen: TDataSource;
    QrCHOpen: TmySQLQuery;
    QrCHOpenBanco: TIntegerField;
    QrCHOpenAgencia: TIntegerField;
    QrCHOpenConta: TWideStringField;
    QrCHOpenCheque: TIntegerField;
    QrCHOpenValor: TFloatField;
    QrCHOpenDCompra: TDateField;
    QrCHOpenDDeposito: TDateField;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesAdValorem: TFloatField;
    QrClientesDMaisC: TIntegerField;
    QrClientesFatorCompra: TFloatField;
    QrClientesMAIOR_T: TFloatField;
    QrClientesADVAL_T: TFloatField;
    QrClientesDMaisD: TIntegerField;
    QrClientesCBE: TIntegerField;
    QrClientesSCB: TIntegerField;
    QrClientesCPMF: TFloatField;
    QrClientesTipo: TSmallintField;
    QrClientesSimples: TSmallintField;
    DsClientes: TDataSource;
    DsCHDevT: TDataSource;
    QrCHDevT: TmySQLQuery;
    QrCHDevTABERTO: TFloatField;
    QrCHDevA: TmySQLQuery;
    QrCHDevADATA1_TXT: TWideStringField;
    QrCHDevADATA2_TXT: TWideStringField;
    QrCHDevADATA3_TXT: TWideStringField;
    QrCHDevACPF_TXT: TWideStringField;
    QrCHDevANOMECLIENTE: TWideStringField;
    QrCHDevACodigo: TIntegerField;
    QrCHDevAAlinea1: TIntegerField;
    QrCHDevAAlinea2: TIntegerField;
    QrCHDevAData1: TDateField;
    QrCHDevAData2: TDateField;
    QrCHDevAData3: TDateField;
    QrCHDevACliente: TIntegerField;
    QrCHDevABanco: TIntegerField;
    QrCHDevAAgencia: TIntegerField;
    QrCHDevAConta: TWideStringField;
    QrCHDevACheque: TIntegerField;
    QrCHDevACPF: TWideStringField;
    QrCHDevAValor: TFloatField;
    QrCHDevATaxas: TFloatField;
    QrCHDevALk: TIntegerField;
    QrCHDevADataCad: TDateField;
    QrCHDevADataAlt: TDateField;
    QrCHDevAUserCad: TIntegerField;
    QrCHDevAUserAlt: TIntegerField;
    QrCHDevAEmitente: TWideStringField;
    QrCHDevAChequeOrigem: TIntegerField;
    QrCHDevAStatus: TSmallintField;
    QrCHDevAValPago: TFloatField;
    QrCHDevAMulta: TFloatField;
    QrCHDevAJurosP: TFloatField;
    QrCHDevAJurosV: TFloatField;
    QrCHDevADesconto: TFloatField;
    QrCHDevASALDO: TFloatField;
    QrCHDevAATUAL: TFloatField;
    QrCHDevAPgDesc: TFloatField;
    DsCHDevA: TDataSource;
    DsOcorNovo: TDataSource;
    QrOcorNovo: TmySQLQuery;
    QrAlinIts: TmySQLQuery;
    QrAlinItsCodigo: TIntegerField;
    QrAlinItsAlinea1: TIntegerField;
    QrAlinItsAlinea2: TIntegerField;
    QrAlinItsData1: TDateField;
    QrAlinItsData2: TDateField;
    QrAlinItsData3: TDateField;
    QrAlinItsCliente: TIntegerField;
    QrAlinItsBanco: TIntegerField;
    QrAlinItsAgencia: TIntegerField;
    QrAlinItsConta: TWideStringField;
    QrAlinItsCheque: TIntegerField;
    QrAlinItsCPF: TWideStringField;
    QrAlinItsValor: TFloatField;
    QrAlinItsTaxas: TFloatField;
    QrAlinItsLk: TIntegerField;
    QrAlinItsDataCad: TDateField;
    QrAlinItsDataAlt: TDateField;
    QrAlinItsUserCad: TIntegerField;
    QrAlinItsUserAlt: TIntegerField;
    QrAlinItsEmitente: TWideStringField;
    QrAlinItsChequeOrigem: TIntegerField;
    QrAlinItsStatus: TSmallintField;
    QrAlinItsValPago: TFloatField;
    QrAlinItsDATA1_TXT: TWideStringField;
    QrAlinItsDATA2_TXT: TWideStringField;
    QrAlinItsDATA3_TXT: TWideStringField;
    QrAlinItsNOMECLIENTE: TWideStringField;
    QrAlinItsMulta: TFloatField;
    QrAlinItsJurosP: TFloatField;
    QrAlinItsJurosV: TFloatField;
    QrAlinItsDesconto: TFloatField;
    QrAlinItsCPF_TXT: TWideStringField;
    QrTaxas: TmySQLQuery;
    QrTaxasCodigo: TIntegerField;
    QrTaxasNome: TWideStringField;
    QrTaxasGenero: TSmallintField;
    QrTaxasForma: TSmallintField;
    QrTaxasAutomatico: TSmallintField;
    QrTaxasValor: TFloatField;
    QrTaxasMostra: TSmallintField;
    QrTaxasLk: TIntegerField;
    QrTaxasDataCad: TDateField;
    QrTaxasDataAlt: TDateField;
    QrTaxasUserCad: TIntegerField;
    QrTaxasUserAlt: TIntegerField;
    DsTaxas: TDataSource;
    DsAlinIts: TDataSource;
    QrSacRiscoTC: TmySQLQuery;
    QrSacRiscoTCValor: TFloatField;
    DsSacRiscoTC: TDataSource;
    DsCarteiras4: TDataSource;
    QrCarteiras4: TmySQLQuery;
    QrCarteiras4Codigo: TIntegerField;
    QrCarteiras4Nome: TWideStringField;
    QrCarteiras4Banco1: TIntegerField;
    QrCarteiras4Agencia1: TIntegerField;
    QrCarteiras4Conta1: TWideStringField;
    QrSacOcorA: TmySQLQuery;
    QrSacOcorATipo: TSmallintField;
    QrSacOcorATIPODOC: TWideStringField;
    QrSacOcorANOMEOCORRENCIA: TWideStringField;
    QrSacOcorACodigo: TIntegerField;
    QrSacOcorALotesIts: TIntegerField;
    QrSacOcorADataO: TDateField;
    QrSacOcorAOcorrencia: TIntegerField;
    QrSacOcorAValor: TFloatField;
    QrSacOcorALoteQuit: TIntegerField;
    QrSacOcorALk: TIntegerField;
    QrSacOcorADataCad: TDateField;
    QrSacOcorADataAlt: TDateField;
    QrSacOcorAUserCad: TIntegerField;
    QrSacOcorAUserAlt: TIntegerField;
    QrSacOcorATaxaP: TFloatField;
    QrSacOcorATaxaV: TFloatField;
    QrSacOcorAPago: TFloatField;
    QrSacOcorADataP: TDateField;
    QrSacOcorATaxaB: TFloatField;
    QrSacOcorAData3: TDateField;
    QrSacOcorAStatus: TSmallintField;
    QrSacOcorASALDO: TFloatField;
    QrSacOcorAATUALIZADO: TFloatField;
    QrSacOcorACliente: TIntegerField;
    QrSacOcorACLIENTELOTE: TIntegerField;
    DsSacOcorA: TDataSource;
    DsSacCHDevA: TDataSource;
    QrSacCHDevA: TmySQLQuery;
    QrSacCHDevADATA1_TXT: TWideStringField;
    QrSacCHDevADATA2_TXT: TWideStringField;
    QrSacCHDevADATA3_TXT: TWideStringField;
    QrSacCHDevACPF_TXT: TWideStringField;
    QrSacCHDevANOMECLIENTE: TWideStringField;
    QrSacCHDevACodigo: TIntegerField;
    QrSacCHDevAAlinea1: TIntegerField;
    QrSacCHDevAAlinea2: TIntegerField;
    QrSacCHDevAData1: TDateField;
    QrSacCHDevAData2: TDateField;
    QrSacCHDevAData3: TDateField;
    QrSacCHDevACliente: TIntegerField;
    QrSacCHDevABanco: TIntegerField;
    QrSacCHDevAAgencia: TIntegerField;
    QrSacCHDevAConta: TWideStringField;
    QrSacCHDevACheque: TIntegerField;
    QrSacCHDevACPF: TWideStringField;
    QrSacCHDevAValor: TFloatField;
    QrSacCHDevATaxas: TFloatField;
    QrSacCHDevALk: TIntegerField;
    QrSacCHDevADataCad: TDateField;
    QrSacCHDevADataAlt: TDateField;
    QrSacCHDevAUserCad: TIntegerField;
    QrSacCHDevAUserAlt: TIntegerField;
    QrSacCHDevAEmitente: TWideStringField;
    QrSacCHDevAChequeOrigem: TIntegerField;
    QrSacCHDevAStatus: TSmallintField;
    QrSacCHDevAValPago: TFloatField;
    QrSacCHDevAMulta: TFloatField;
    QrSacCHDevAJurosP: TFloatField;
    QrSacCHDevAJurosV: TFloatField;
    QrSacCHDevADesconto: TFloatField;
    QrSacCHDevASALDO: TFloatField;
    QrSacCHDevAATUAL: TFloatField;
    QrSacCHDevAPgDesc: TFloatField;
    QrBanco1: TmySQLQuery;
    QrBanco1Nome: TWideStringField;
    DsBanco1: TDataSource;
    DsBanco0: TDataSource;
    QrBanco0: TmySQLQuery;
    QrBanco0Nome: TWideStringField;
    QrRiscoTC: TmySQLQuery;
    QrRiscoTCValor: TFloatField;
    DsRiscoTC: TDataSource;
    DsRiscoC: TDataSource;
    QrRiscoC: TmySQLQuery;
    QrRiscoCBanco: TIntegerField;
    QrRiscoCAgencia: TIntegerField;
    QrRiscoCConta: TWideStringField;
    QrRiscoCCheque: TIntegerField;
    QrRiscoCValor: TFloatField;
    QrRiscoCDCompra: TDateField;
    QrRiscoCDDeposito: TDateField;
    QrRiscoCEmitente: TWideStringField;
    QrRiscoCCPF: TWideStringField;
    QrRiscoCCPF_TXT: TWideStringField;
    QrSacRiscoC: TmySQLQuery;
    QrSacRiscoCBanco: TIntegerField;
    QrSacRiscoCAgencia: TIntegerField;
    QrSacRiscoCConta: TWideStringField;
    QrSacRiscoCCheque: TIntegerField;
    QrSacRiscoCValor: TFloatField;
    QrSacRiscoCDCompra: TDateField;
    QrSacRiscoCDDeposito: TDateField;
    QrSacRiscoCEmitente: TWideStringField;
    QrSacRiscoCCPF: TWideStringField;
    QrSacRiscoCCPF_TXT: TWideStringField;
    DsSacRiscoC: TDataSource;
    DsSacDOpen: TDataSource;
    QrSacDOpen: TmySQLQuery;
    QrSacDOpenControle: TIntegerField;
    QrSacDOpenDuplicata: TWideStringField;
    QrSacDOpenDCompra: TDateField;
    QrSacDOpenValor: TFloatField;
    QrSacDOpenDDeposito: TDateField;
    QrSacDOpenEmitente: TWideStringField;
    QrSacDOpenCPF: TWideStringField;
    QrSacDOpenCliente: TIntegerField;
    QrSacDOpenSTATUS: TWideStringField;
    QrSacDOpenQuitado: TIntegerField;
    QrSacDOpenTotalJr: TFloatField;
    QrSacDOpenTotalDs: TFloatField;
    QrSacDOpenTotalPg: TFloatField;
    QrSacDOpenSALDO_DESATUALIZ: TFloatField;
    QrSacDOpenSALDO_ATUALIZADO: TFloatField;
    QrSacDOpenNOMESTATUS: TWideStringField;
    QrSacDOpenVencto: TDateField;
    QrSacDOpenDDCALCJURO: TIntegerField;
    QrSacDOpenData3: TDateField;
    QrSacDOpenRepassado: TSmallintField;
    QrBanco4: TmySQLQuery;
    QrBanco4Nome: TWideStringField;
    DsBanco4: TDataSource;
    QrSPC_Result: TmySQLQuery;
    DsSPC_Result: TDataSource;
    QrSPC_ResultLinha: TIntegerField;
    QrSPC_ResultTexto: TWideStringField;
    QrSPC_Cfg: TmySQLQuery;
    QrSPC_CfgSPC_ValMin: TFloatField;
    QrSPC_CfgSPC_ValMax: TFloatField;
    QrSPC_CfgServidor: TWideStringField;
    QrSPC_CfgSPC_Porta: TSmallintField;
    QrSPC_CfgPedinte: TWideStringField;
    QrSPC_CfgCodigSocio: TIntegerField;
    QrSPC_CfgModalidade: TIntegerField;
    QrSPC_CfgInfoExtra: TIntegerField;
    QrSPC_CfgBAC_CMC7: TSmallintField;
    QrSPC_CfgTipoCred: TSmallintField;
    QrSPC_CfgSenhaSocio: TWideStringField;
    QrSPC_CfgCodigo: TIntegerField;
    QrSPC_CfgValAviso: TFloatField;
    QrSPC_CfgVALCONSULTA: TFloatField;
    QrILi: TmySQLQuery;
    QrILiCPF: TWideStringField;
    QrDupSacConta: TWideStringField;
    QrDupSacCheque: TIntegerField;
    QrDupSacDCompra: TDateField;
    QrLocSacaEmail: TWideStringField;
    QrDupsAtencao: TmySQLQuery;
    QrLocSacaIE: TWideStringField;
    procedure QrLCalcCalcFields(DataSet: TDataSet);
    procedure QrClientesCalcFields(DataSet: TDataSet);
    procedure QrAlinItsCalcFields(DataSet: TDataSet);
    procedure QrClientesAfterScroll(DataSet: TDataSet);
    procedure QrCHDevACalcFields(DataSet: TDataSet);
    procedure QrOcorACalcFields(DataSet: TDataSet);
    procedure QrCHDevAAfterScroll(DataSet: TDataSet);
    procedure QrDOpenCalcFields(DataSet: TDataSet);
    procedure QrDOpenAfterOpen(DataSet: TDataSet);
    procedure QrDOpenBeforeClose(DataSet: TDataSet);
    procedure QrRiscoCCalcFields(DataSet: TDataSet);
    procedure QrCHDevAAfterOpen(DataSet: TDataSet);
    procedure QrCHDevABeforeClose(DataSet: TDataSet);
    procedure QrOcorAAfterOpen(DataSet: TDataSet);
    procedure QrOcorABeforeClose(DataSet: TDataSet);
    procedure QrSacRiscoCCalcFields(DataSet: TDataSet);
    procedure QrSacDOpenAfterOpen(DataSet: TDataSet);
    procedure QrSacDOpenBeforeClose(DataSet: TDataSet);
    procedure QrSacDOpenCalcFields(DataSet: TDataSet);
    procedure QrSacCHDevAAfterOpen(DataSet: TDataSet);
    procedure QrSacCHDevABeforeClose(DataSet: TDataSet);
    procedure QrSacCHDevACalcFields(DataSet: TDataSet);
    procedure QrSacOcorAAfterOpen(DataSet: TDataSet);
    procedure QrSacOcorABeforeClose(DataSet: TDataSet);
    procedure QrSacOcorACalcFields(DataSet: TDataSet);
    procedure QrRepCliCalcFields(DataSet: TDataSet);
    procedure QrRepCliBeforeClose(DataSet: TDataSet);
    procedure QrRepCliAfterOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ReopenSPC_Cli(Entidade: Integer);
    procedure ReopenSPC_Result(CPF: String; Linha: Integer);
  end;

var
  DmLotes: TDmLotes;

implementation

uses Module, Principal, Lotes1, UnMLAGeral, UnMyObjects;

{$R *.dfm}

procedure TDmLotes.QrAlinItsCalcFields(DataSet: TDataSet);
begin
  QrAlinItsDATA1_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrAlinItsData1.Value);
  QrAlinItsDATA2_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrAlinItsData2.Value);
  QrAlinItsDATA3_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrAlinItsData3.Value);
  QrAlinItsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrAlinItsCPF.Value);
end;

procedure TDmLotes.QrCHDevAAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  FmLotes1.FCHDevOpen_Total := 0;
  while not DmLotes.QrCHDevA.Eof do
  begin
    //if Int(Date) > DmLotes.QrCHDevAData1.Value then
      FmLotes1.FCHDevOpen_Total := FmLotes1.FCHDevOpen_Total + DmLotes.QrCHDevAATUAL.Value;
    DmLotes.QrCHDevA.Next;
  end;
  Enab  := (FmLotes1.QrLotes.State <> dsInactive) and (FmLotes1.QrLotes.RecordCount > 0);
  Enab2 := (QrCHDevA.State <> dsInactive) and (QrCHDevA.RecordCount > 0);  
  //
  FmLotes1.BtPagamento7.Enabled := Enab and Enab2;
end;

procedure TDmLotes.QrCHDevAAfterScroll(DataSet: TDataSet);
begin
  if DmLotes.QrCHDevASALDO.Value > 0 then FmLotes1.BtPagamento7.Enabled := True
  else FmLotes1.BtPagamento7.Enabled := False;
end;

procedure TDmLotes.QrCHDevABeforeClose(DataSet: TDataSet);
begin
  FmLotes1.FCHDevOpen_Total := 0;
  //
  FmLotes1.BtPagamento7.Enabled := False;
end;

procedure TDmLotes.QrCHDevACalcFields(DataSet: TDataSet);
begin
  DmLotes.QrCHDevADATA1_TXT.Value := MLAGeral.DataNula('dd/mm/yy', DmLotes.QrCHDevAData1.Value);
  DmLotes.QrCHDevADATA2_TXT.Value := MLAGeral.DataNula('dd/mm/yy', DmLotes.QrCHDevAData2.Value);
  DmLotes.QrCHDevADATA3_TXT.Value := MLAGeral.DataNula('dd/mm/yy', DmLotes.QrCHDevAData3.Value);
  DmLotes.QrCHDevACPF_TXT.Value := Geral.FormataCNPJ_TT(DmLotes.QrCHDevACPF.Value);
  DmLotes.QrCHDevASALDO.Value := DmLotes.QrCHDevAValor.Value +DmLotes.QrCHDevATaxas.Value +
    DmLotes.QrCHDevAMulta.Value + DmLotes.QrCHDevAJurosV.Value - DmLotes.QrCHDevAValPago.Value -
    DmLotes.QrCHDevADesconto.Value - DmLotes.QrCHDevAPgDesc.Value;
  //
  DmLotes.QrCHDevAATUAL.Value := DMod.ObtemValorAtualizado(
    DmLotes.QrCHDevACliente.Value, DmLotes.QrCHDevAStatus.Value, DmLotes.QrCHDevAData1.Value, Date,
    DmLotes.QrCHDevAData3.Value, DmLotes.QrCHDevAValor.Value, DmLotes.QrCHDevAJurosV.Value,
    DmLotes.QrCHDevADesconto.Value, DmLotes.QrCHDevAValPago.Value + DmLotes.QrCHDevAPgDesc.Value,
    DmLotes.QrCHDevAJurosP.Value, False);
end;

procedure TDmLotes.QrClientesAfterScroll(DataSet: TDataSet);
var
  i, Lins: Integer;
begin
  if FmLotes1.PainelItens.Visible and FmLotes1.PainelEdita.Visible then
  begin
    FmLotes1.EdTxaCompraX.Text := Geral.FFT(DmLotes.QrClientesFatorCompra.Value, 4, siPositivo);
    if FmLotes1.Pagina.ActivePageIndex = 0 then
    begin
      FmLotes1.EdDMaisX.Text := Geral.FFT(DmLotes.QrClientesDMaisC.Value, 0, siNegativo)
    end else begin
      FmLotes1.EdDMaisX.Text := Geral.FFT(DmLotes.QrClientesDMaisD.Value, 0, siNegativo);
    end;
    ////////////////////////////////////////////////////////////////////////
    FmLotes1.GradeX1.RowCount := 2;
    MyObjects.LimpaGrade(FmLotes1.GradeX1, 1, 1, True);
    Lins := FmPrincipal.FMyDBs.MaxDBs +1;
    if Lins < 2 then Lins := 2;
    FmLotes1.GradeX1.RowCount := Lins;
    for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
    begin
      if FmPrincipal.FMyDBs.Connected[i] = '1' then
      begin
        FmLotes1.GradeX1.Cells[00, i+1] := FmPrincipal.FMyDBs.IDName[i];
        FmLotes1.FArrayMySQLEE[i].Close;
        FmLotes1.FArrayMySQLEE[i].Params[0].AsInteger := DmLotes.QrClientesCodigo.Value;
        FmLotes1.FArrayMySQLEE[i].Open;
        if FmLotes1.QrLotesSpread.Value = 0 then
          FmLotes1.GradeX1.Cells[01, i+1] := Geral.FFT(
            FmLotes1.FArrayMySQLEE[i].FieldByName('Maior').AsFloat, 6, siPositivo)
        else
          FmLotes1.GradeX1.Cells[01, i+1] := Geral.FFT(
            FmLotes1.FArrayMySQLEE[i].FieldByName('Menor').AsFloat, 6, siPositivo);
      end;
    end;
  end;
end;

procedure TDmLotes.QrClientesCalcFields(DataSet: TDataSet);
var
  AdValT, MaiorT: Double;
  i: Integer;
begin
  MaiorT := DmLotes.QrClientesFatorCompra.Value;
  AdValT := DmLotes.QrClientesFatorCompra.Value;
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs-1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      FmLotes1.FArrayMySQLEE[i].Close;
      FmLotes1.FArrayMySQLEE[i].Params[0].AsInteger := DmLotes.QrClientesCodigo.Value;
      FmLotes1.FArrayMySQLEE[i].Open;
      MaiorT := MaiorT + FmLotes1.FArrayMySQLEE[i].FieldByName('Maior').AsFloat;
      AdValT := AdValT + FmLotes1.FArrayMySQLEE[i].FieldByName('AdVal').AsFloat;
    end;
  end;
  DmLotes.QrClientesMAIOR_T.Value := MaiorT;
  DmLotes.QrClientesADVAL_T.Value := AdValT;
  //
end;

procedure TDmLotes.QrDOpenAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
  DR, DV, DT, CR, CV, CT, RT, VT, ST, OA, TT: Double;
begin
  DR := 0; DV := 0;
  while not QrDOpen.Eof do
  begin
    if Int(Date) > QrDOpenVencto.Value then
      DV := DV + QrDOpenSALDO_ATUALIZADO.Value
    else
      DR := DR + QrDOpenSALDO_ATUALIZADO.Value;
    QrDOpen.Next;
  end;
  DT := DR+DV;
  QrDOpen.First;
  CR := QrRiscoTCValor.Value;
  CV := FmLotes1.FCHDevOpen_Total;
  CT := CR+CV;
  RT := DR+CR;
  VT := DV+CV;
  ST := RT+VT;
  OA := FmLotes1.FOcorA_Total;
  TT := ST + OA;
  FmLotes1.EdDV.Text := Geral.FFT(DV, 2, siPositivo);
  FmLotes1.EdDR.Text := Geral.FFT(DR, 2, siPositivo);
  FmLotes1.EdDT.Text := Geral.FFT(DT, 2, siPositivo);
  FmLotes1.EdCV.Text := Geral.FFT(CV, 2, siPositivo);
  FmLotes1.EdCR.Text := Geral.FFT(CR, 2, siPositivo);
  FmLotes1.EdCT.Text := Geral.FFT(CT, 2, siPositivo);
  FmLotes1.EdRT.Text := Geral.FFT(RT, 2, siPositivo);
  FmLotes1.EdVT.Text := Geral.FFT(VT, 2, siPositivo);
  FmLotes1.EdST.Text := Geral.FFT(ST, 2, siPositivo);
  FmLotes1.EdOA.Text := Geral.FFT(OA, 2, siPositivo);
  FmLotes1.EdTT.Text := Geral.FFT(TT, 2, siPositivo);
  //
  FmLotes1.EdDV_C.Text := Geral.FFT(DV, 2, siPositivo);
  FmLotes1.EdDR_C.Text := Geral.FFT(DR, 2, siPositivo);
  FmLotes1.EdDT_C.Text := Geral.FFT(DT, 2, siPositivo);
  FmLotes1.EdCV_C.Text := Geral.FFT(CV, 2, siPositivo);
  FmLotes1.EdCR_C.Text := Geral.FFT(CR, 2, siPositivo);
  FmLotes1.EdCT_C.Text := Geral.FFT(CT, 2, siPositivo);
  FmLotes1.EdRT_C.Text := Geral.FFT(RT, 2, siPositivo);
  FmLotes1.EdVT_C.Text := Geral.FFT(VT, 2, siPositivo);
  FmLotes1.EdST_C.Text := Geral.FFT(ST, 2, siPositivo);
  FmLotes1.EdOA_C.Text := Geral.FFT(OA, 2, siPositivo);
  FmLotes1.EdTT_C.Text := Geral.FFT(TT, 2, siPositivo);
  //
  FmLotes1.EdDV_D.Text := Geral.FFT(DV, 2, siPositivo);
  FmLotes1.EdDR_D.Text := Geral.FFT(DR, 2, siPositivo);
  FmLotes1.EdDT_D.Text := Geral.FFT(DT, 2, siPositivo);
  FmLotes1.EdCV_D.Text := Geral.FFT(CV, 2, siPositivo);
  FmLotes1.EdCR_D.Text := Geral.FFT(CR, 2, siPositivo);
  FmLotes1.EdCT_D.Text := Geral.FFT(CT, 2, siPositivo);
  FmLotes1.EdRT_D.Text := Geral.FFT(RT, 2, siPositivo);
  FmLotes1.EdVT_D.Text := Geral.FFT(VT, 2, siPositivo);
  FmLotes1.EdST_D.Text := Geral.FFT(ST, 2, siPositivo);
  FmLotes1.EdOA_D.Text := Geral.FFT(OA, 2, siPositivo);
  FmLotes1.EdTT_D.Text := Geral.FFT(TT, 2, siPositivo);
  //
  Enab := (FmLotes1.QrLotes.State <> dsInactive) and (FmLotes1.QrLotes.RecordCount > 0);
  Enab2 := (QrDOpen.State <> dsInactive) and (QrDOpen.RecordCount > 0);
  //
  FmLotes1.BtPagtosInclui9.Enabled := Enab and Enab2;
end;

procedure TDmLotes.QrDOpenBeforeClose(DataSet: TDataSet);
begin
  FmLotes1.EdDV.Text := '';
  FmLotes1.EdDR.Text := '';
  FmLotes1.EdDT.Text := '';
  FmLotes1.EdCV.Text := '';
  FmLotes1.EdCR.Text := '';
  FmLotes1.EdCT.Text := '';
  FmLotes1.EdRT.Text := '';
  FmLotes1.EdVT.Text := '';
  FmLotes1.EdST.Text := '';
  FmLotes1.EdOA.Text := '';
  FmLotes1.EdTT.Text := '';
  //
  FmLotes1.BtPagtosInclui9.Enabled := False;
end;

procedure TDmLotes.QrDOpenCalcFields(DataSet: TDataSet);
var
  DtUltimoPg: TDateTime;
begin
  QrDOpenSALDO_DESATUALIZ.Value := QrDOpenValor.Value + QrDOpenTotalJr.Value -
  QrDOpenTotalDs.Value - QrDOpenTotalPg.Value;
  //
  QrDOpenNomeStatus.Value := MLAGeral.NomeStatusPgto2(QrDOpenQuitado.Value,
    QrDOpenDDeposito.Value, Date, QrDOpenData3.Value, QrDOpenRepassado.Value);
  //
  {
  QrDOpenSALDO_ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrDOpenCliente.Value, QrDOpenQuitado.Value, QrDOpenVencto.Value, Date,
    QrDOpenData3.Value, QrDOpenValor.Value, QrDOpenTotalJr.Value, QrDOpenTotalDs.Value,
    QrDOpenTotalPg.Value, 0, True);
  }
  Dmod.ReopenDupLocPg(QrDOpenControle.Value);
  if Dmod.QrLocPg.RecordCount > 0 then
    DtUltimoPg := Dmod.QrLocPgData.Value
  else
    DtUltimoPg := QrDOpenDDeposito.Value;
  //
  QrDOpenSALDO_ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrDOpenCliente.Value, QrDOpenQuitado.Value, QrDOpenVencto.Value, Date,
    DtUltimoPg, QrDOpenValor.Value, QrDOpenTotalJr.Value, QrDOpenTotalDs.Value,
    QrDOpenTotalPg.Value, 0, True);
end;

procedure TDmLotes.QrLCalcCalcFields(DataSet: TDataSet);
var
 VaValT, AdValT, VaTaxT, TotISS, TotIRR, TotPIS: Double;
 i: Integer;
begin
  AdValT := DmLotes.QrLCalcAdValorem.Value ;
  VaValT := DmLotes.QrLCalcValValorem.Value;
  VaTaxT := DmLotes.QrLCalcTxCompra.Value;
  TotISS := DmLotes.QrLCalcISS_Val.Value;
  TotIRR := DmLotes.QrLCalcIRRF_Val.Value;
  TotPIS := DmLotes.QrLCalcPIS_R_Val.Value;
  for i := 0 to FmPrincipal.FMyDBs.MaxDBs -1 do
  begin
    if FmPrincipal.FMyDBs.Connected[i] = '1' then
    begin
      AdValT := AdValT + FmLotes1.FArrayMySQLLO[i].FieldByName('AdValorem').AsFloat;
      VaValT := VaValT + FmLotes1.FArrayMySQLLO[i].FieldByName('ValValorem').AsFloat;
      VaTaxT := VaTaxT + FmLotes1.FArrayMySQLLO[i].FieldByName('TaxaVal').AsFloat;
      TotISS := TotISS + FmLotes1.FArrayMySQLLO[i].FieldByName('ISS_Val').AsFloat;
      TotIRR := TotIRR + FmLotes1.FArrayMySQLLO[i].FieldByName('IRRF_Val').AsFloat;
      TotPIS := TotPIS + FmLotes1.FArrayMySQLLO[i].FieldByName('PIS_R_Val').AsFloat;
    end;
  end;
  //////////////////////////////////////////////////////////////////////////////
  DmLotes.QrLCalcVAL_LIQUIDO.Value := DmLotes.QrLCalcTotal.Value - VaValT
    - VaTaxT - DmLotes.QrLCalcIRRF_VAL.Value - DmLotes.QrLCalcIOC_VAL.Value
    - DmLotes.QrLCalcIOFd.Value -DmLotes.QrLCalcIOFv.Value -
    - DmLotes.QrLCalcCPMF_VAL.Value - DmLotes.QrLCalcTarifas.Value - DmLotes.QrLCalcOcorP.Value -
    DmLotes.QrLCalcCHDevPG.Value;
  //
end;

procedure TDmLotes.QrOcorAAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  FmLotes1.FOcorA_Total := 0;
  while not DmLotes.QrOcorA.Eof do
  begin
    //if Int(Date) > DmLotes.QrOcorADataO.Value then
    FmLotes1.FOcorA_Total := FmLotes1.FOcorA_Total + DmLotes.QrOcorAATUALIZADO.Value;
    DmLotes.QrOcorA.Next;
  end;
  Enab  := (FmLotes1.QrLotes.State <> dsInactive) and (FmLotes1.QrLotes.RecordCount > 0);
  Enab2 := (QrOcorA.State <> dsInactive) and (QrOcorA.RecordCount > 0);
  //
  FmLotes1.BtIncluiocor.Enabled := Enab;
  FmLotes1.BtPagamento6.Enabled := Enab and Enab2;
end;

procedure TDmLotes.QrOcorABeforeClose(DataSet: TDataSet);
begin
  FmLotes1.FOcorA_Total := 0;
  //
  FmLotes1.BtIncluiocor.Enabled := False;
  FmLotes1.BtPagamento6.Enabled := False;
end;

procedure TDmLotes.QrOcorACalcFields(DataSet: TDataSet);
var
  Cliente: Integer;
begin
  if DmLotes.QrOcorACLIENTELOTE.Value > 0 then
    Cliente := DmLotes.QrOcorACLIENTELOTE.Value else
    Cliente := DmLotes.QrOcorACliente.Value;
  //
  DmLotes.QrOcorASALDO.Value := DmLotes.QrOcorAValor.Value + DmLotes.QrOcorATaxaV.Value - DmLotes.QrOcorAPago.Value;
  //
  DmLotes.QrOcorAATUALIZADO.Value := DMod.ObtemValorAtualizado(
    Cliente, 1, DmLotes.QrOcorADataO.Value, Date, DmLotes.QrOcorAData3.Value,
    DmLotes.QrOcorAValor.Value, DmLotes.QrOcorATaxaV.Value, 0 (*Desco*),
    DmLotes.QrOcorAPago.Value, DmLotes.QrOcorATaxaP.Value, False);
  //
  if DmLotes.QrOcorADescri.Value <> '' then
    DmLotes.QrOcorADOCUM_TXT.Value := DmLotes.QrOcorADescri.Value
  else if DmLotes.QrOcorATIPODOC.Value = 'CH' then
    DmLotes.QrOcorADOCUM_TXT.Value := FormatFloat('000', DmLotes.QrOcorABanco.Value)+'/'+
    FormatFloat('0000', DmLotes.QrOcorAAgencia.Value)+'/'+ DmLotes.QrOcorAConta.Value+'-'+
    FormatFloat('000000', DmLotes.QrOcorACheque.Value)
  else if DmLotes.QrOcorATIPODOC.Value = 'DU' then
    DmLotes.QrOcorADOCUM_TXT.Value := DmLotes.QrOcorADuplicata.Value
  else DmLotes.QrOcorADOCUM_TXT.Value := '';
end;

procedure TDmLotes.QrRepCliAfterOpen(DataSet: TDataSet);
var
  Enab, Enab2: Boolean;
begin
  Enab :=  (FmLotes1.QrLotes.State <> dsInactive) and (FmLotes1.QrLotes.RecordCount > 0);
  Enab2 := (QrRepCli.State <> dsInactive) and (QrRepCli.RecordCount > 0);
  //
  FmLotes1.BtPagTerc.Enabled := Enab;
  FmLotes1.BtExclui8.Enabled := Enab and Enab2;
  FmLotes1.BtSobra.Enabled   := Enab;
end;

procedure TDmLotes.QrRepCliBeforeClose(DataSet: TDataSet);
begin
  FmLotes1.BtPagTerc.Enabled := False;
  FmLotes1.BtExclui8.Enabled := False;
  FmLotes1.BtSobra.Enabled   := False;
end;

procedure TDmLotes.QrRepCliCalcFields(DataSet: TDataSet);
begin
  DmLotes.QrRepCliSEQ.Value := DmLotes.QrRepCli.RecNo;
end;

procedure TDmLotes.QrRiscoCCalcFields(DataSet: TDataSet);
begin
  QrRiscoCCPF_TXT.Value := Geral.FormataCNPJ_TT(QrRiscoCCPF.Value);
end;

procedure TDmLotes.QrSacCHDevAAfterOpen(DataSet: TDataSet);
begin
  FmLotes1.FCHDevOpen_Total := 0;
  while not QrSacCHDevA.Eof do
  begin
    //if Int(Date) > QrSacCHDevAData1.Value then
      FmLotes1.FCHDevOpen_Total := FmLotes1.FCHDevOpen_Total + QrSacCHDevAATUAL.Value;
    QrSacCHDevA.Next;
  end;
end;

procedure TDmLotes.QrSacCHDevABeforeClose(DataSet: TDataSet);
begin
  FmLotes1.FCHDevOpen_Total := 0;
end;

procedure TDmLotes.QrSacCHDevACalcFields(DataSet: TDataSet);
begin
  QrSacCHDevADATA1_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrSacCHDevAData1.Value);
  QrSacCHDevADATA2_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrSacCHDevAData2.Value);
  QrSacCHDevADATA3_TXT.Value := MLAGeral.DataNula('dd/mm/yy', QrSacCHDevAData3.Value);
  QrSacCHDevACPF_TXT.Value := Geral.FormataCNPJ_TT(QrSacCHDevACPF.Value);
  QrSacCHDevASALDO.Value := QrSacCHDevAValor.Value +QrSacCHDevATaxas.Value +
    QrSacCHDevAMulta.Value + QrSacCHDevAJurosV.Value - QrSacCHDevAValPago.Value -
    QrSacCHDevADesconto.Value - QrSacCHDevAPgDesc.Value;
  //
  QrSacCHDevAATUAL.Value := DMod.ObtemValorAtualizado(
    QrSacCHDevACliente.Value, QrSacCHDevAStatus.Value, QrSacCHDevAData1.Value, Date,
    QrSacCHDevAData3.Value, QrSacCHDevAValor.Value, QrSacCHDevAJurosV.Value,
    QrSacCHDevADesconto.Value, QrSacCHDevAValPago.Value +
    QrSacCHDevAPgDesc.Value, QrSacCHDevAJurosP.Value, False);
end;

procedure TDmLotes.QrSacDOpenAfterOpen(DataSet: TDataSet);
var
  DR, DV, DT, CR, CV, CT, RT, VT, ST, OA, TT: Double;
begin
  DR := 0; DV := 0;
  while not QrSacDOpen.Eof do
  begin
    if Int(Date) > QrSacDOpenVencto.Value then
      DV := DV + QrSacDOpenSALDO_ATUALIZADO.Value
    else
      DR := DR + QrSacDOpenSALDO_ATUALIZADO.Value;
    QrSacDOpen.Next;
  end;
  DT := DR+DV;
  QrSacDOpen.First;
  CR := QrSacRiscoTCValor.Value;
  CV := FmLotes1.FCHDevOpen_Total;
  CT := CR+CV;
  RT := DR+CR;
  VT := DV+CV;
  ST := RT+VT;
  OA := FmLotes1.FOcorA_Total;
  TT := ST + OA;
  //
  if FmLotes1.QrLotesTipo.Value = 1 then
  begin
    FmLotes1.EdDV_S.Text := Geral.FFT(DV, 2, siPositivo);
    FmLotes1.EdDR_S.Text := Geral.FFT(DR, 2, siPositivo);
    FmLotes1.EdDT_S.Text := Geral.FFT(DT, 2, siPositivo);
    FmLotes1.EdCV_S.Text := Geral.FFT(CV, 2, siPositivo);
    FmLotes1.EdCR_S.Text := Geral.FFT(CR, 2, siPositivo);
    FmLotes1.EdCT_S.Text := Geral.FFT(CT, 2, siPositivo);
    FmLotes1.EdRT_S.Text := Geral.FFT(RT, 2, siPositivo);
    FmLotes1.EdVT_S.Text := Geral.FFT(VT, 2, siPositivo);
    FmLotes1.EdST_S.Text := Geral.FFT(ST, 2, siPositivo);
    FmLotes1.EdOA_S.Text := Geral.FFT(OA, 2, siPositivo);
    FmLotes1.EdTT_S.Text := Geral.FFT(TT, 2, siPositivo);
  end else begin
    FmLotes1.EdDV_E.Text := Geral.FFT(DV, 2, siPositivo);
    FmLotes1.EdDR_E.Text := Geral.FFT(DR, 2, siPositivo);
    FmLotes1.EdDT_E.Text := Geral.FFT(DT, 2, siPositivo);
    FmLotes1.EdCV_E.Text := Geral.FFT(CV, 2, siPositivo);
    FmLotes1.EdCR_E.Text := Geral.FFT(CR, 2, siPositivo);
    FmLotes1.EdCT_E.Text := Geral.FFT(CT, 2, siPositivo);
    FmLotes1.EdRT_E.Text := Geral.FFT(RT, 2, siPositivo);
    FmLotes1.EdVT_E.Text := Geral.FFT(VT, 2, siPositivo);
    FmLotes1.EdST_E.Text := Geral.FFT(ST, 2, siPositivo);
    FmLotes1.EdOA_E.Text := Geral.FFT(OA, 2, siPositivo);
    FmLotes1.EdTT_E.Text := Geral.FFT(TT, 2, siPositivo);
  end;
end;

procedure TDmLotes.QrSacDOpenBeforeClose(DataSet: TDataSet);
begin
  FmLotes1.EdDV_S.Text := '';
  FmLotes1.EdDR_S.Text := '';
  FmLotes1.EdDT_S.Text := '';
  FmLotes1.EdCV_S.Text := '';
  FmLotes1.EdCR_S.Text := '';
  FmLotes1.EdCT_S.Text := '';
  FmLotes1.EdRT_S.Text := '';
  FmLotes1.EdVT_S.Text := '';
  FmLotes1.EdST_S.Text := '';
  FmLotes1.EdOA_S.Text := '';
  FmLotes1.EdTT_S.Text := '';
  //
  FmLotes1.EdDV_E.Text := '';
  FmLotes1.EdDR_E.Text := '';
  FmLotes1.EdDT_E.Text := '';
  FmLotes1.EdCV_E.Text := '';
  FmLotes1.EdCR_E.Text := '';
  FmLotes1.EdCT_E.Text := '';
  FmLotes1.EdRT_E.Text := '';
  FmLotes1.EdVT_E.Text := '';
  FmLotes1.EdST_E.Text := '';
  FmLotes1.EdOA_E.Text := '';
  FmLotes1.EdTT_E.Text := '';
end;

procedure TDmLotes.QrSacDOpenCalcFields(DataSet: TDataSet);
var
  DtUltimoPg: TDateTime;
  Atualizado: Double;
begin
  QrSacDOpenSALDO_DESATUALIZ.Value := QrSacDOpenValor.Value + QrSacDOpenTotalJr.Value -
  QrSacDOpenTotalDs.Value - QrSacDOpenTotalPg.Value;
  //
  QrSacDOpenNomeStatus.Value := MLAGeral.NomeStatusPgto2(QrSacDOpenQuitado.Value,
    QrSacDOpenDDeposito.Value, Date, QrSacDOpenData3.Value, QrSacDOpenRepassado.Value);
  //
  {
  QrSacDOpenSALDO_ATUALIZADO.Value := DMod.ObtemValorAtualizado(
    QrSacDOpenCliente.Value, QrSacDOpenQuitado.Value, QrSacDOpenVencto.Value, Date,
    QrSacDOpenData3.Value, QrSacDOpenValor.Value, QrSacDOpenTotalJr.Value, QrSacDOpenTotalDs.Value,
    QrSacDOpenTotalPg.Value, 0, True);
  }
  Dmod.ReopenDupLocPg(QrDOpenControle.Value);
  if Dmod.QrLocPg.RecordCount > 0 then
    DtUltimoPg := Dmod.QrLocPgData.Value
  else
    DtUltimoPg := QrSacDOpenControle.Value;
  //
  Atualizado := DMod.ObtemValorAtualizado(
    QrSacDOpenCliente.Value, QrSacDOpenQuitado.Value, QrSacDOpenVencto.Value, Date,
    DtUltimoPg, QrSacDOpenValor.Value, QrSacDOpenTotalJr.Value, QrSacDOpenTotalDs.Value,
    QrSacDOpenTotalPg.Value, 0, True);
  //
  QrSacDOpenSALDO_ATUALIZADO.Value := Atualizado;
end;

procedure TDmLotes.QrSacOcorAAfterOpen(DataSet: TDataSet);
begin
  FmLotes1.FOcorA_Total := 0;
  while not QrSacOcorA.Eof do
  begin
    //if Int(Date) > QrSacOcorADataO.Value then
      FmLotes1.FOcorA_Total := FmLotes1.FOcorA_Total + QrSacOcorAATUALIZADO.Value;
    QrSacOcorA.Next;
  end;
end;

procedure TDmLotes.QrSacOcorABeforeClose(DataSet: TDataSet);
begin
  FmLotes1.FOcorA_Total := 0;
end;

procedure TDmLotes.QrSacOcorACalcFields(DataSet: TDataSet);
var
  Cliente: Integer;
begin
  if QrSacOcorACLIENTELOTE.Value > 0 then
    Cliente := QrSacOcorACLIENTELOTE.Value else
    Cliente := QrSacOcorACliente.Value;
  //
  QrSacOcorASALDO.Value := QrSacOcorAValor.Value + QrSacOcorATaxaV.Value - QrSacOcorAPago.Value;
  //
  QrSacOcorAATUALIZADO.Value := DMod.ObtemValorAtualizado(
    Cliente, 1, QrSacOcorADataO.Value, Date, QrSacOcorAData3.Value,
    QrSacOcorAValor.Value, QrSacOcorATaxaV.Value, 0 (*Desco*),
    QrSacOcorAPago.Value, QrSacOcorATaxaP.Value, False);
end;

procedure TDmLotes.QrSacRiscoCCalcFields(DataSet: TDataSet);
begin
  QrSacRiscoCCPF_TXT.Value := Geral.FormataCNPJ_TT(QrSacRiscoCCPF.Value);
end;

procedure TDmLotes.ReopenSPC_Cli(Entidade: Integer);
begin
  QrSPC_Cli.Close;
  QrSPC_Cli.Params[0].AsInteger := Entidade;
  QrSPC_Cli.Open;
end;

procedure TDmLotes.ReopenSPC_Result(CPF: String; Linha: Integer);
begin
  QrSPC_Result.Close;
  QrSPC_Result.Params[0].AsString := CPF;
  QrSPC_Result.Open;
  //
  if Linha > 0 then
    QrSPC_Result.Locate('Linha', Linha, []);
end;

end.
