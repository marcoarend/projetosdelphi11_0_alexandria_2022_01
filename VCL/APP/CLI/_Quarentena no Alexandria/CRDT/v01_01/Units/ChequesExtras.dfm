object FmChequesExtras: TFmChequesExtras
  Left = 331
  Top = 161
  Caption = 'Cheques Avulsos'
  ClientHeight = 413
  ClientWidth = 790
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 365
    Width = 790
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 678
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtExclui: TBitBtn
      Tag = 12
      Left = 113
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Exclui'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtExcluiClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 790
    Height = 48
    Align = alTop
    Caption = 'Cheques Avulsos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 788
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 794
      ExplicitHeight = 44
    end
  end
  object Panel8: TPanel
    Left = 0
    Top = 48
    Width = 790
    Height = 169
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    TabStop = True
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 493
      Height = 169
      Align = alClient
      TabOrder = 0
      object Label42: TLabel
        Left = 4
        Top = 4
        Width = 123
        Height = 13
        Caption = 'Cliente (n'#227'o '#233' obrigat'#243'rio):'
      end
      object Label43: TLabel
        Left = 4
        Top = 44
        Width = 143
        Height = 13
        Caption = 'Leitura pela banda magn'#233'tica:'
      end
      object Label44: TLabel
        Left = 256
        Top = 44
        Width = 27
        Height = 13
        Caption = 'Com.:'
      end
      object Label45: TLabel
        Left = 288
        Top = 44
        Width = 22
        Height = 13
        Caption = 'Bco:'
      end
      object Label46: TLabel
        Left = 320
        Top = 44
        Width = 34
        Height = 13
        Caption = 'Ag'#234'nc:'
      end
      object Label47: TLabel
        Left = 360
        Top = 44
        Width = 55
        Height = 13
        Caption = 'Conta corr.:'
      end
      object Label48: TLabel
        Left = 436
        Top = 44
        Width = 45
        Height = 13
        Caption = 'N'#186' cheq.:'
      end
      object Label49: TLabel
        Left = 4
        Top = 84
        Width = 129
        Height = 13
        Caption = 'CGC / CPF [F4 - Pesquisa]:'
      end
      object Label50: TLabel
        Left = 164
        Top = 84
        Width = 44
        Height = 13
        Caption = 'Emitente:'
      end
      object Label1: TLabel
        Left = 88
        Top = 124
        Width = 26
        Height = 13
        Caption = 'Data:'
      end
      object Label2: TLabel
        Left = 156
        Top = 124
        Width = 59
        Height = 13
        Caption = 'Vencimento:'
      end
      object Label35: TLabel
        Left = 4
        Top = 124
        Width = 27
        Height = 13
        Caption = 'Valor:'
      end
      object EdCliente: TdmkEditCB
        Left = 4
        Top = 20
        Width = 61
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBCliente
        IgnoraDBLookupComboBox = False
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 68
        Top = 20
        Width = 421
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTI'
        ListSource = DsEntidades
        TabOrder = 1
        dmkEditCB = EdCliente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdBanda2: TdmkEdit
        Left = 4
        Top = 60
        Width = 249
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        MaxLength = 34
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdBanda2Change
      end
      object EdCMC_7_2: TEdit
        Left = 80
        Top = 56
        Width = 121
        Height = 21
        MaxLength = 30
        TabOrder = 3
        Visible = False
        OnChange = EdCMC_7_2Change
      end
      object EdRegiaoCompe2: TdmkEdit
        Left = 256
        Top = 60
        Width = 29
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 3
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdBanco2: TdmkEdit
        Left = 288
        Top = 60
        Width = 29
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 3
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdBanco2Change
        OnExit = EdBanco2Exit
      end
      object EdAgencia2: TdmkEdit
        Left = 320
        Top = 60
        Width = 37
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 6
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 4
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnChange = EdAgencia2Change
      end
      object EdConta2: TdmkEdit
        Left = 360
        Top = 60
        Width = 73
        Height = 21
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdConta2Change
      end
      object EdRealCC: TdmkEdit
        Left = 360
        Top = 80
        Width = 73
        Height = 21
        Alignment = taCenter
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCheque2: TdmkEdit
        Left = 436
        Top = 60
        Width = 53
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdCPF_2: TdmkEdit
        Left = 4
        Top = 100
        Width = 157
        Height = 21
        Alignment = taCenter
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnChange = EdCPF_2Change
        OnExit = EdCPF_2Exit
        OnKeyDown = EdCPF_2KeyDown
      end
      object EdEmitente2: TdmkEdit
        Left = 164
        Top = 100
        Width = 325
        Height = 21
        CharCase = ecUpperCase
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 11
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdValor: TdmkEdit
        Left = 4
        Top = 140
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Color = clWhite
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object EdData4: TdmkEdit
        Left = 88
        Top = 140
        Width = 66
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnExit = EdData4Exit
      end
      object EdVence4: TdmkEdit
        Left = 156
        Top = 140
        Width = 66
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 6
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnExit = EdVence4Exit
      end
    end
    object GroupBox1: TGroupBox
      Left = 493
      Top = 0
      Width = 297
      Height = 169
      Align = alRight
      TabOrder = 1
      object Label51: TLabel
        Left = 4
        Top = 26
        Width = 45
        Height = 13
        Caption = 'Cheques:'
      end
      object Label52: TLabel
        Left = 4
        Top = 48
        Width = 53
        Height = 13
        Caption = 'Duplicatas:'
      end
      object Label53: TLabel
        Left = 60
        Top = 8
        Width = 30
        Height = 13
        Caption = 'Risco:'
      end
      object Label96: TLabel
        Left = 137
        Top = 8
        Width = 42
        Height = 13
        Caption = 'Vencido:'
      end
      object Label98: TLabel
        Left = 214
        Top = 8
        Width = 56
        Height = 13
        Caption = 'Sub-total:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label99: TLabel
        Left = 4
        Top = 74
        Width = 56
        Height = 13
        Caption = 'Sub-total:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label91: TLabel
        Left = 4
        Top = 116
        Width = 50
        Height = 13
        Caption = 'Ocorr'#234'nc.:'
      end
      object Label54: TLabel
        Left = 214
        Top = 96
        Width = 45
        Height = 13
        Caption = 'TOTAL:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object EdDR: TdmkEdit
        Left = 60
        Top = 44
        Width = 76
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdDT: TdmkEdit
        Left = 214
        Top = 44
        Width = 76
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdRT: TdmkEdit
        Left = 60
        Top = 70
        Width = 76
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdVT: TdmkEdit
        Left = 137
        Top = 70
        Width = 76
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdST: TdmkEdit
        Left = 214
        Top = 70
        Width = 76
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCT: TdmkEdit
        Left = 214
        Top = 22
        Width = 76
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdDV: TdmkEdit
        Left = 137
        Top = 44
        Width = 76
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCR: TdmkEdit
        Left = 60
        Top = 22
        Width = 76
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCV: TdmkEdit
        Left = 137
        Top = 22
        Width = 76
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdOA: TdmkEdit
        Left = 60
        Top = 112
        Width = 76
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdTT: TdmkEdit
        Left = 214
        Top = 112
        Width = 76
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 217
    Width = 790
    Height = 148
    Align = alClient
    DataSource = DsAvulsos
    TabOrder = 3
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Banco'
        Width = 36
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Agencia'
        Title.Caption = 'Ag'#234'ncia'
        Width = 45
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Conta'
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Valor'
        Width = 80
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'DCompra'
        Title.Caption = 'Data'
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Vencto'
        Width = 56
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'NOMECLI'
        Title.Caption = 'Cliente'
        Width = 100
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Emitente'
        Width = 140
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'CPF_TXT'
        Title.Caption = 'CNPJ / CPF'
        Width = 113
        Visible = True
      end>
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMEENTI'
      'FROM entidades'
      'ORDER BY NOMEENTI')
    Left = 28
    Top = 240
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 56
    Top = 240
  end
  object QrSCB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SCB'
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 28
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSCBSCB: TIntegerField
      FieldName = 'SCB'
    end
  end
  object QrBanco2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, DVCC '
      'FROM bancos'
      'WHERE Codigo=:P0')
    Left = 56
    Top = 268
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBanco2Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrBanco2DVCC: TSmallintField
      FieldName = 'DVCC'
    end
  end
  object QrSacRiscoC: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSacRiscoCCalcFields
    SQL.Strings = (
      
        'SELECT lo.Tipo, li.Banco, li.Agencia, li.Conta, li.Cheque, li.Va' +
        'lor, '
      'li.DCompra, li.DDeposito, li.Emitente, li.CPF'
      'FROM lotesits li '
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Cliente=:P0'
      'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())'
      'AND lo.Tipo = 0'
      'ORDER BY DDeposito')
    Left = 129
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacRiscoCBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrSacRiscoCAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrSacRiscoCConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrSacRiscoCCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrSacRiscoCValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacRiscoCDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacRiscoCDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacRiscoCEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrSacRiscoCCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrSacRiscoCCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsSacRiscoC: TDataSource
    DataSet = QrSacRiscoC
    Left = 157
    Top = 9
  end
  object QrSacDOpen: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSacDOpenAfterOpen
    BeforeClose = QrSacDOpenBeforeClose
    OnCalcFields = QrSacDOpenCalcFields
    SQL.Strings = (
      'SELECT od.Nome STATUS, li.Controle, li.Duplicata, li.Repassado,'
      'li.DCompra, li.Valor, li.DDeposito, li.Emitente, li.CPF, '
      'lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg,'
      'li.Vencto, li.Data3'
      'FROM lotesits li'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'WHERE lo.Tipo=1 '
      'AND li.Quitado <> 2'
      'AND lo.Cliente=:P0'
      'ORDER BY li.Vencto')
    Left = 185
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacDOpenControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrSacDOpenDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrSacDOpenDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacDOpenValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacDOpenDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacDOpenEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrSacDOpenCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrSacDOpenCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSacDOpenSTATUS: TWideStringField
      FieldName = 'STATUS'
      Size = 50
    end
    object QrSacDOpenQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrSacDOpenTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacDOpenTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacDOpenTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacDOpenSALDO_DESATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_DESATUALIZ'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSacDOpenSALDO_ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSacDOpenNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 30
      Calculated = True
    end
    object QrSacDOpenVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrSacDOpenDDCALCJURO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DDCALCJURO'
      Calculated = True
    end
    object QrSacDOpenData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrSacDOpenRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
  end
  object DsSacDOpen: TDataSource
    DataSet = QrSacDOpen
    Left = 213
    Top = 9
  end
  object QrSacCHDevA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSacCHDevAAfterOpen
    OnCalcFields = QrSacCHDevACalcFields
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLIENTE, ai.*'
      'FROM alinits ai'
      'LEFT JOIN entidades en ON en.Codigo=ai.Cliente'
      'WHERE ai.Status<2'
      'AND ai.Cliente=:P0'
      'ORDER BY ai.Data1, ai.Data2')
    Left = 241
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacCHDevADATA1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA1_TXT'
      Calculated = True
    end
    object QrSacCHDevADATA2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA2_TXT'
      Calculated = True
    end
    object QrSacCHDevADATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Calculated = True
    end
    object QrSacCHDevACPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrSacCHDevANOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrSacCHDevACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSacCHDevAAlinea1: TIntegerField
      FieldName = 'Alinea1'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrSacCHDevAAlinea2: TIntegerField
      FieldName = 'Alinea2'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrSacCHDevAData1: TDateField
      FieldName = 'Data1'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacCHDevAData2: TDateField
      FieldName = 'Data2'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacCHDevAData3: TDateField
      FieldName = 'Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacCHDevACliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrSacCHDevABanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrSacCHDevAAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrSacCHDevAConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrSacCHDevACheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrSacCHDevACPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrSacCHDevAValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevATaxas: TFloatField
      FieldName = 'Taxas'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSacCHDevADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSacCHDevADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSacCHDevAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSacCHDevAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSacCHDevAEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrSacCHDevAChequeOrigem: TIntegerField
      FieldName = 'ChequeOrigem'
      Required = True
    end
    object QrSacCHDevAStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrSacCHDevAValPago: TFloatField
      FieldName = 'ValPago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevAMulta: TFloatField
      FieldName = 'Multa'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevAJurosP: TFloatField
      FieldName = 'JurosP'
      Required = True
      DisplayFormat = '#,###,##0.000000'
    end
    object QrSacCHDevAJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevADesconto: TFloatField
      FieldName = 'Desconto'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevASALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSacCHDevAATUAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSacCHDevAPgDesc: TFloatField
      FieldName = 'PgDesc'
      Required = True
    end
    object QrSacCHDevADDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
    end
    object QrSacCHDevAVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrSacCHDevALoteOrigem: TIntegerField
      FieldName = 'LoteOrigem'
      Required = True
    end
  end
  object DsSacCHDevA: TDataSource
    DataSet = QrSacCHDevA
    Left = 269
    Top = 9
  end
  object QrSacOcorA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSacOcorAAfterOpen
    BeforeClose = QrSacOcorABeforeClose
    OnCalcFields = QrSacOcorACalcFields
    SQL.Strings = (
      'SELECT IF(oc.Cliente > 0,'#39'CL'#39',IF(lo.Tipo=0,"CH","DU"))TIPODOC, '
      'ob.Nome NOMEOCORRENCIA, lo.Tipo, lo.Cliente CLIENTELOTE, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.Status<2'
      'AND (lo.Cliente=:P0 OR oc.Cliente=:P1)')
    Left = 297
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSacOcorATipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrSacOcorATIPODOC: TWideStringField
      FieldName = 'TIPODOC'
      Size = 2
    end
    object QrSacOcorANOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrSacOcorACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSacOcorALotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrSacOcorADataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacOcorAOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrSacOcorAValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacOcorALoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrSacOcorALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSacOcorADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSacOcorADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSacOcorAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSacOcorAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSacOcorATaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrSacOcorATaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSacOcorAPago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSacOcorADataP: TDateField
      FieldName = 'DataP'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacOcorATaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrSacOcorAData3: TDateField
      FieldName = 'Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacOcorAStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrSacOcorASALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,###,##0.00'
      Calculated = True
    end
    object QrSacOcorAATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,###,##0.00'
      Calculated = True
    end
    object QrSacOcorACliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrSacOcorACLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
  end
  object DsSacOcorA: TDataSource
    DataSet = QrSacOcorA
    Left = 325
    Top = 9
  end
  object QrSacRiscoTC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(li.Valor) Valor'
      'FROM lotesits li '
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Cliente=:P0'
      'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())'
      'AND lo.Tipo = 0')
    Left = 129
    Top = 37
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacRiscoTCValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSacRiscoTC: TDataSource
    DataSet = QrSacRiscoTC
    Left = 157
    Top = 37
  end
  object QrAvulsos: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrAvulsosCalcFields
    SQL.Strings = (
      'SELECT loi.Banco, loi.Agencia, loi.Conta, loi.Cheque, loi.Valor,'
      'loi.DCompra, loi.Vencto, loi.Cliente, loi.Emitente, loi.CPF,'
      'loi.Controle, CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial '
      'ELSE cli.Nome END NOMECLI '
      'FROM lotesits loi'
      'LEFT JOIN entidades cli ON cli.Codigo=loi.Cliente'
      'WHERE loi.Codigo=0 AND loi.Tipo=-9'
      'ORDER BY loi.Vencto, loi.DCompra')
    Left = 28
    Top = 296
    object QrAvulsosControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrAvulsosBanco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrAvulsosAgencia: TIntegerField
      FieldName = 'Agencia'
      DisplayFormat = '0000'
    end
    object QrAvulsosConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrAvulsosCheque: TIntegerField
      FieldName = 'Cheque'
      DisplayFormat = '000000'
    end
    object QrAvulsosCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrAvulsosEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrAvulsosValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAvulsosDCompra: TDateField
      FieldName = 'DCompra'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrAvulsosVencto: TDateField
      FieldName = 'Vencto'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrAvulsosCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrAvulsosCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrAvulsosNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
  end
  object DsAvulsos: TDataSource
    DataSet = QrAvulsos
    Left = 56
    Top = 296
  end
end
