unit HistCliEmi;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, Mask, DBCtrls,
  Grids, DBGrids, ComCtrls, frxDBSet, frxChBox, frxClass, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, dmkLabelRotate, dmkDBEdit, dmkGeral, UnDmkProcFunc;

type
  TFmHistCliEmi = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrEmitCPF: TmySQLQuery;
    QrEmitCPFNome: TWideStringField;
    DsEmitCPF: TDataSource;
    Panel1: TPanel;
    QrSomaA: TmySQLQuery;
    DsSomaA: TDataSource;
    QrClientes: TmySQLQuery;
    QrClientesNOMECLIENTE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    DsClientes: TDataSource;
    QrItens: TmySQLQuery;
    DsItens: TDataSource;
    QrItensValor: TFloatField;
    QrItensITENS: TLargeintField;
    QrItensITENS_AVencer: TLargeintField;
    QrItensITENS_Vencidos: TLargeintField;
    QrItensITENS_PgVencto: TLargeintField;
    QrItensITENS_Devolvid: TLargeintField;
    QrItensITENS_DevPgTot: TLargeintField;
    QrItensITENS_DevNaoQt: TLargeintField;
    QrItensPago: TFloatField;
    QrItensEXPIRADO: TFloatField;
    QrItensAEXPIRAR: TFloatField;
    QrItensVENCIDO: TFloatField;
    QrItensPGDDEPOSITO: TFloatField;
    QrItensPGATRAZO: TFloatField;
    QrItensPGABERTO: TFloatField;
    QrItensPERIODO: TFloatField;
    QrItensNOMECLI: TWideStringField;
    QrItensNOMEBANCO: TWideStringField;
    QrItensDDeposito: TDateField;
    QrItensNOMEEMITENTE: TWideStringField;
    QrItensTipo: TWideStringField;
    QrItensNOMESTATUS: TWideStringField;
    QrSomaAValor: TFloatField;
    QrSomaAITENS: TLargeintField;
    QrSomaAITENS_AVencer: TFloatField;
    QrSomaAITENS_Vencidos: TFloatField;
    QrSomaAITENS_PgVencto: TFloatField;
    QrSomaAITENS_Devolvid: TFloatField;
    QrSomaAITENS_DevPgTot: TFloatField;
    QrSomaAITENS_DevNaoQt: TFloatField;
    QrSomaAPago: TFloatField;
    QrSomaAEXPIRADO: TFloatField;
    QrSomaAAEXPIRAR: TFloatField;
    QrSomaAVENCIDO: TFloatField;
    QrSomaAPGDDEPOSITO: TFloatField;
    QrSomaAPGATRAZO: TFloatField;
    QrSomaAPGABERTO: TFloatField;
    Panel2: TPanel;
    Panel3: TPanel;
    Label2: TLabel;
    EdCPF1: TdmkEdit;
    Label75: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    CkPeriodo: TCheckBox;
    TPIni: TDateTimePicker;
    TPFim: TDateTimePicker;
    Label1: TLabel;
    DBEdit1: TDBEdit;
    Label3: TLabel;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    DBEdit3: TDBEdit;
    Label5: TLabel;
    DBEdit4: TDBEdit;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    Label7: TLabel;
    DBEdit6: TDBEdit;
    Label8: TLabel;
    DBEdit7: TDBEdit;
    Bevel2: TBevel;
    Bevel3: TBevel;
    Bevel4: TBevel;
    Bevel5: TBevel;
    Bevel6: TBevel;
    Bevel7: TBevel;
    Bevel8: TBevel;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    QrSomaAPERC_AVencer: TFloatField;
    QrSomaAPERC_Vencidos: TFloatField;
    QrSomaAPERC_PgVencto: TFloatField;
    QrSomaAPERC_Devolvid: TFloatField;
    QrSomaAPERC_DevPgTot: TFloatField;
    QrSomaAPERC_DevNaoQt: TFloatField;
    QrSomaAPERC_ITENS: TFloatField;
    QrTudo: TmySQLQuery;
    QrTudoITENS: TLargeintField;
    Bevel9: TBevel;
    Bevel10: TBevel;
    Bevel11: TBevel;
    Bevel12: TBevel;
    Bevel13: TBevel;
    Bevel14: TBevel;
    Bevel15: TBevel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Bevel16: TBevel;
    Bevel17: TBevel;
    Bevel18: TBevel;
    Bevel19: TBevel;
    Bevel20: TBevel;
    Bevel21: TBevel;
    Bevel22: TBevel;
    DBEdit15: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    DBEdit25: TDBEdit;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    DBEdit28: TDBEdit;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Bevel23: TBevel;
    Bevel24: TBevel;
    Label28: TLabel;
    Label29: TLabel;
    Bevel25: TBevel;
    Bevel26: TBevel;
    Bevel27: TBevel;
    Bevel28: TBevel;
    Bevel29: TBevel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    LaValores: TdmkLabelRotate;
    LaItens: TdmkLabelRotate;
    QrSomaAPERC_Expirado: TFloatField;
    QrSomaAPERC_AExpirar: TFloatField;
    QrSomaAPERC_Vencido: TFloatField;
    QrSomaAPERC_PgDDeposito: TFloatField;
    QrSomaAPERC_Atrazo: TFloatField;
    QrSomaAPERC_Aberto: TFloatField;
    QrTudoVALOR: TFloatField;
    QrSomaAPERC_VALOR: TFloatField;
    QrSomaB: TmySQLQuery;
    QrSomaBValor: TFloatField;
    QrSomaBITENS: TLargeintField;
    QrSomaBITENS_AVencer: TFloatField;
    QrSomaBITENS_Vencidos: TFloatField;
    QrSomaBITENS_PgVencto: TFloatField;
    QrSomaBITENS_Devolvid: TFloatField;
    QrSomaBITENS_DevPgTot: TFloatField;
    QrSomaBITENS_DevNaoQt: TFloatField;
    QrSomaBPago: TFloatField;
    QrSomaBEXPIRADO: TFloatField;
    QrSomaBAEXPIRAR: TFloatField;
    QrSomaBVENCIDO: TFloatField;
    QrSomaBPGDDEPOSITO: TFloatField;
    QrSomaBPGATRAZO: TFloatField;
    QrSomaBPGABERTO: TFloatField;
    QrSomaASOMA_I_AVencer: TFloatField;
    QrSomaASOMA_I_Vencidos: TFloatField;
    QrSomaASOMA_I_PgVencto: TFloatField;
    QrSomaASOMA_I_ITENS: TFloatField;
    QrSomaASOMA_I_Devolvid: TFloatField;
    QrSomaASOMA_I_DevPgTot: TFloatField;
    QrSomaASOMA_I_DevNaoQt: TFloatField;
    QrSomaASOMA_V_AEXPIRAR: TFloatField;
    QrSomaASOMA_V_EXPIRADO: TFloatField;
    QrSomaASOMA_V_VENCIDO: TFloatField;
    QrSomaASOMA_V_PGDDEPOSITO: TFloatField;
    QrSomaASOMA_V_PGATRAZO: TFloatField;
    QrSomaASOMA_V_PGABERTO: TFloatField;
    QrSomaASOMA_V_VALOR: TFloatField;
    Label30: TLabel;
    BtImprime: TBitBtn;
    RGTipo: TRadioGroup;
    CkLotesNeg: TCheckBox;
    QrItensControle: TIntegerField;
    QrItensBanco: TIntegerField;
    QrItensAgencia: TIntegerField;
    QrItensConta: TWideStringField;
    QrItensCheque: TIntegerField;
    QrItensDuplicata: TWideStringField;
    QrItensDOCUMENTO: TWideStringField;
    CkGrupos: TCheckBox;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGrid1: TDBGrid;
    QrGruSacEmiIts: TmySQLQuery;
    QrGruSacEmiItsCodigo: TIntegerField;
    QrGruSacEmiItsCNPJ_CPF: TWideStringField;
    QrGruSacEmiItsNome: TWideStringField;
    DsGruSacEmiIts: TDataSource;
    QrPesqGru: TmySQLQuery;
    DBGrid2: TDBGrid;
    QrPesqGruCodigo: TIntegerField;
    BtGrupo: TBitBtn;
    QrGruSacEmiItsCNPJ_CPF_TXT: TWideStringField;
    QrGruSacEmiItsNOMEGRUPO: TWideStringField;
    QrItensITENS_XXX: TLargeintField;
    GroupBox1: TGroupBox;
    CkA: TCheckBox;
    CkQ: TCheckBox;
    CkR: TCheckBox;
    CkP: TCheckBox;
    Label31: TLabel;
    Bevel1: TBevel;
    DBEdit29: TDBEdit;
    DBEdit30: TDBEdit;
    Label32: TLabel;
    Bevel30: TBevel;
    Label33: TLabel;
    Bevel31: TBevel;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    Bevel32: TBevel;
    Label34: TLabel;
    QrRevenda: TmySQLQuery;
    QrRevendaPago: TFloatField;
    QrRevendaItens: TFloatField;
    DsRevenda: TDataSource;
    QrRevendaPago_Perc: TFloatField;
    QrRevendaItens_Perc: TFloatField;
    frxHistorico: TfrxReport;
    frxCheckBoxObject1: TfrxCheckBoxObject;
    frxDsItens: TfrxDBDataset;
    frxDsSomaA: TfrxDBDataset;
    QrMediaDias: TmySQLQuery;
    DsMediaDias: TDataSource;
    QrMediaDiasPRAZO_MEDIO: TFloatField;
    CkMediaDias: TCheckBox;
    dmkEdMediaDias: TdmkDBEdit;
    frxDsMediaDias: TfrxDBDataset;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCPF1Exit(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure CkPeriodoClick(Sender: TObject);
    procedure QrItensCalcFields(DataSet: TDataSet);
    procedure EdCPF1Change(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure TPIniClick(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
    procedure TPFimClick(Sender: TObject);
    procedure CkCrescenteClick(Sender: TObject);
    procedure QrSomaACalcFields(DataSet: TDataSet);
    procedure BtImprimeClick(Sender: TObject);
    procedure RGTipoClick(Sender: TObject);
    procedure DBGrid1TitleClick(Column: TColumn);
    procedure BtGrupoClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure QrGruSacEmiItsCalcFields(DataSet: TDataSet);
    procedure CkLotesNegClick(Sender: TObject);
    procedure CkGruposClick(Sender: TObject);
    procedure QrGruSacEmiItsBeforeClose(DataSet: TDataSet);
    procedure QrGruSacEmiItsAfterOpen(DataSet: TDataSet);
    procedure QrItensBeforeClose(DataSet: TDataSet);
    procedure QrItensAfterOpen(DataSet: TDataSet);
    procedure CkAClick(Sender: TObject);
    procedure CkQClick(Sender: TObject);
    procedure CkRClick(Sender: TObject);
    procedure CkPClick(Sender: TObject);
    procedure QrRevendaCalcFields(DataSet: TDataSet);
    procedure frxHistoricoGetValue(const VarName: String;
      var Value: Variant);
    procedure CkMediaDiasClick(Sender: TObject);
  private
    { Private declarations }
    FOrdC1, FOrdC2, FOrdC3, FOrdS1, FOrdS2, FOrdS3, FGrupos: String;
    procedure FechaPesquisa;
    procedure MostraCkGrupos;
    procedure DesativaItens;
  public
    { Public declarations }
    procedure Pesquisa;
    procedure ReopenEmiSac(CPF: String);
  end;

  var
  FmHistCliEmi: TFmHistCliEmi;

implementation

{$R *.DFM}

uses Module, PesqCPFCNPJ, UnInternalConsts, Principal, UnMyObjects;

procedure TFmHistCliEmi.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmHistCliEmi.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmHistCliEmi.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmHistCliEmi.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F8 then
  begin
    Application.CreateForm(TFmPesqCPFCNPJ, FmPesqCPFCNPJ);
    FmPesqCPFCNPJ.ShowModal;
    FmPesqCPFCNPJ.Destroy;
    if VAR_CPF_PESQ <> '' then
    begin
      EdCPF1.Text := VAR_CPF_PESQ;
    end;
    ReopenEmiSac(VAR_CPF_PESQ);
  end;
end;

procedure TFmHistCliEmi.ReopenEmiSac(CPF: String);
var
  Grupos, CPFx: String;
begin
  QrEmitCPF.Close;
  QrGruSacEmiIts.Close;
  CPFx := Geral.SoNumero_TT(CPF);
  if Trim(CPFx) <> '' then
  begin
    QrEmitCPF.Params[0].AsString := CPFx;
    QrEmitCPF.Open;
    //
    QrPesqGru.Close;
    QrPesqGru.Params[0].AsString := CPFx;
    QrPesqGru.Open;
    if QrPesqGru.RecordCount > 0 then
    begin
      Grupos := '';
      while not QrPesqGru.Eof do
      begin
        Grupos := Grupos + ',' + IntToStr(QrPesqGruCodigo.Value);
        QrPesqGru.Next;
      end;
      Grupos[1] := '(';
      QrGruSacEmiIts.Close;
      QrGruSacEmiIts.SQL.Clear;
      QrGruSacEmiIts.SQL.Add('SELECT gse.Nome NOMEGRUPO, gei.*');
      QrGruSacEmiIts.SQL.Add('FROM grusacemiits gei');
      QrGruSacEmiIts.SQL.Add('LEFT JOIN grusacemi gse ON gse.Codigo=gei.Codigo');
      QrGruSacEmiIts.SQL.Add('WHERE gse.Codigo in '+Grupos+')');
      QrGruSacEmiIts.SQL.Add('ORDER BY NOMEGRUPO, gei.Nome');
      QrGruSacEmiIts.SQL.Add('');
      QrGruSacEmiIts.Open;
    end;
  end;
end;

procedure TFmHistCliEmi.EdCPF1Exit(Sender: TObject);
begin
  ReopenEmiSac(EdCPF1.Text);
  MostraCkGrupos;
end;

procedure TFmHistCliEmi.EdClienteChange(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  FOrdC1 := 'DDeposito';
  FOrdS1 := '';
  FOrdC2 := '';
  FOrdS2 := '';
  FOrdC3 := '';
  FOrdS3 := '';
  //
  QrClientes.Open;
  TPIni.Date := Date - 180;
  TPFim.Date := Date + 365;
end;

procedure TFmHistCliEmi.BtOKClick(Sender: TObject);
begin
  Pesquisa;
end;

procedure TFmHistCliEmi.Pesquisa;
const
  ItensOrdem: array[0..4] of String = ('NOMECLI', 'NOMEEMITENTE', 'NOMEBANCO',
  'Periodo', 'DDeposito');
var
  A, Q, R, P, T: Integer;
begin
  if Geral.IMV(EdCliente.Text) = 0 then
    if Application.MessageBox('N�o foi definido nenhum cliente. Para pesquisar'+
    #13#10+'todos os clientes ser� nescess�rio v�rios minutos. Deseja continuar'
    +#13#10+'assim mesmo?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
      Exit;

  ReopenEmiSac(EdCPF1.Text);
  MostraCkGrupos;
  A := MLAGeral.BoolToInt(CkA.Checked);
  Q := MLAGeral.BoolToInt(CkQ.Checked);
  R := MLAGeral.BoolToInt(CkR.Checked);
  P := MLAGeral.BoolToInt(CkP.Checked);
  T := A + Q + R + P;
  if A = 0 then A := -1000 else A := 1;
  if Q = 0 then Q := -1000 else Q := 2;
  if R = 0 then R := -1000 else R := 4;
  if P = 0 then P := -1000 else P := 0;

  //

  FGrupos := '';
  if QrGruSacEmiIts.State = dsBrowse then
    if QrGruSacEmiIts.RecordCount > 0 then
      //if CkGrupos.Enabled then
        if CkGrupos.Checked then
  begin
    QrGruSacEmiIts.First;
    while not QrGruSacEmiIts.Eof do
    begin
      FGrupos := FGrupos + ',"' + QrGruSacEmiItsCNPJ_CPF.Value+'"';
      QrGruSacEmiIts.Next;
    end;
    if Length(Trim(FGrupos)) > 0 then FGrupos[1] := '(';
  end;
  if RGTipo.ItemIndex in ([1,2]) then
  begin
    QrSomaB.Close;
    QrSomaB.SQL.Clear;
    QrSomaB.SQL.Add('SELECT SUM(loi.Valor) Valor, COUNT(loi.Controle) ITENS,');
    QrSomaB.SQL.Add('SUM(IF(loi.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer,');
    QrSomaB.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos,');
    QrSomaB.SQL.Add('SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 1, 0)) ITENS_PgVencto,');
    QrSomaB.SQL.Add('SUM(IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0)) +');
    QrSomaB.SQL.Add('SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0)) ITENS_Devolvid,');
    QrSomaB.SQL.Add('SUM(IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0)) ITENS_DevPgTot,');
    QrSomaB.SQL.Add('SUM(IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0)) ITENS_DevNaoQt,');
    QrSomaB.SQL.Add('SUM((loi.TotalPg - loi.TotalJr + loi.TotalDs)) Pago,');
    QrSomaB.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE(), loi.Valor, 0)) EXPIRADO,');
    QrSomaB.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE(), 0, loi.Valor)) AEXPIRAR,');
    QrSomaB.SQL.Add('SUM(IF((loi.DDeposito<SYSDATE()) AND ((loi.Data3<2)');
    QrSomaB.SQL.Add('  OR (loi.DDeposito<loi.Data3)), loi.Valor, 0)) VENCIDO,');
    QrSomaB.SQL.Add('SUM(IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3),');
    QrSomaB.SQL.Add('  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGDDEPOSITO,');
    QrSomaB.SQL.Add('SUM(IF(/*(loi.Quitado> 1)AND*/ (loi.DDeposito< loi.Data3),');
    QrSomaB.SQL.Add('  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0)) PGATRAZO,');
    QrSomaB.SQL.Add('SUM(IF((loi.Quitado< 2)AND (loi.DDeposito< SYSDATE()),');
    QrSomaB.SQL.Add('  loi.Valor - (loi.TotalPg - loi.TotalJr) + loi.TotalDs, 0)) PGABERTO');
    QrSomaB.SQL.Add('FROM lotesits loi');
    QrSomaB.SQL.Add('LEFT JOIN lotes     lot ON lot.Codigo=loi.Codigo');
    QrSomaB.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente');
    QrSomaB.SQL.Add('LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco');
    QrSomaB.SQL.Add('WHERE lot.Tipo=1');
    QrSomaB.SQL.Add('AND lot.TxCompra + lot.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
    if CkLotesNeg.Checked = False then
      QrSomaB.SQL.Add('AND lot.Codigo > 0');
    if CkPeriodo.Checked then
      QrSomaB.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', TPIni.Date, TPFim.Date, True, True));
    //////////////////////////////////////////////////////////////////////////////
    if EdCliente.ValueVariant <> 0 then
      QrSomaB.SQL.Add('AND lot.Cliente='+EdCliente.Text);
    if EdCPF1.Text <> '' then
    begin
      if Trim(FGrupos) <> ''  then
        QrSomaB.SQL.Add('AND loi.CPF in '+FGrupos+')')
      else
        QrSomaB.SQL.Add('AND loi.CPF="'+Geral.SoNumero_TT(EdCPF1.Text)+'"');
    end;
    //////////////////////////////////////////////////////////////////////////////
    QrSomaB.Open;
  end;
  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////

  QrSomaA.Close;
  Screen.Cursor := crHourGlass;
  QrSomaA.SQL.Clear;
  QrSomaA.SQL.Add('SELECT SUM(loi.Valor) Valor, COUNT(loi.Controle) ITENS,');
  QrSomaA.SQL.Add('SUM(IF(loi.DDeposito > SYSDATE(), 1, 0)) ITENS_AVencer,');
  QrSomaA.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) ITENS_Vencidos,');
  QrSomaA.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()), 1, 0)) -');
  QrSomaA.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),');
  QrSomaA.SQL.Add('1, 0)) ITENS_PgVencto,');
  QrSomaA.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),');
  QrSomaA.SQL.Add('1, 0)) ITENS_Devolvid,');
  QrSomaA.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12),');
  QrSomaA.SQL.Add('1, 0)) ITENS_DevPgTot,');
  QrSomaA.SQL.Add('SUM(IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) in (10, 11)),');
  QrSomaA.SQL.Add('1, 0)) ITENS_DevNaoQt,');
  QrSomaA.SQL.Add('SUM((chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto)) Pago,');
  QrSomaA.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE(), loi.Valor, 0)) EXPIRADO,');
  QrSomaA.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE(), 0, loi.Valor)) AEXPIRAR,');
  QrSomaA.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0, ');
  QrSomaA.SQL.Add('loi.Valor - IF(loi.DDeposito < SYSDATE(), 0, loi.Valor), 0)) VENCIDO,');
  QrSomaA.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0, 0,');
  QrSomaA.SQL.Add('loi.Valor - IF(loi.DDeposito < SYSDATE(), 0, loi.Valor))) PGDDEPOSITO,');
  QrSomaA.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,');
  QrSomaA.SQL.Add('chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto,0)) PGATRAZO,');
  QrSomaA.SQL.Add('SUM(IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,');
  QrSomaA.SQL.Add('loi.Valor - (chd.ValPago - chd.Taxas - chd.JurosV) +');
  QrSomaA.SQL.Add('chd.Desconto, 0)) PGABERTO');
  QrSomaA.SQL.Add('FROM lotesits loi');
  QrSomaA.SQL.Add('LEFT JOIN lotes     lot ON lot.Codigo=loi.Codigo');
  QrSomaA.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente');
  QrSomaA.SQL.Add('LEFT JOIN alinits   chd ON chd.ChequeOrigem=loi.Controle');
  QrSomaA.SQL.Add('LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco');
  QrSomaA.SQL.Add('WHERE lot.Tipo <> 1');
  QrSomaA.SQL.Add('AND lot.TxCompra + lot.ValValorem+ '+
    IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  if CkLotesNeg.Checked = False then
    QrSomaA.SQL.Add('AND lot.Codigo > 0');
  if RGTipo.ItemIndex in ([0,2]) then
  begin
    if CkPeriodo.Checked then
      QrSomaA.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', TPIni.Date, TPFim.Date, True, True));
  end else QrSomaA.SQL.Add('AND loi.DDeposito < -100000');
  //////////////////////////////////////////////////////////////////////////////
  if EdCliente.ValueVariant <> 0 then
    QrSomaA.SQL.Add('AND lot.Cliente='+EdCliente.Text);
  if EdCPF1.Text <> '' then
  begin
    if Trim(FGrupos) <> ''  then
      QrSomaA.SQL.Add('AND loi.CPF in '+FGrupos+')')
    else
      QrSomaA.SQL.Add('AND loi.CPF="'+Geral.SoNumero_TT(EdCPF1.Text)+'"');
  end;
  QrSomaA.Open;

  /////////////////////////////// MEDIA DE DIAS
  if CkMediaDias.Checked = True then
  begin
    QrMediaDias.Close;
    QrMediaDias.SQL.Clear;
    QrMediaDias.SQL.Add('SELECT SUM(loi.Valor*loi.Dias)/SUM(loi.Valor) PRAZO_MEDIO ');
    QrMediaDias.SQL.Add('FROM lotesits loi');
    QrMediaDias.SQL.Add('LEFT JOIN lotes lot ON lot.Codigo = loi.Codigo');
    QrMediaDias.SQL.Add('LEFT JOIN alinits   chd ON chd.ChequeOrigem=loi.Controle');
    // Per�odo dep�sito
    QrMediaDias.SQL.Add(dmkPF.SQL_Periodo('WHERE loi.DDeposito ', TPIni.Date, TPFim.Date, CkPeriodo.Checked, CkPeriodo.Checked));
    //
    QrMediaDias.SQL.Add('AND lot.TxCompra + lot.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
    //Lotes negativos
    if CkLotesNeg.Checked = False then
      QrMediaDias.SQL.Add('AND lot.Codigo > 0');
    //Tipo de documento
    if RGTipo.ItemIndex <> 2 then
      QrMediaDias.SQL.Add('AND lot.Tipo ='+ IntToStr(RGTipo.ItemIndex) +'');
    // Emitente
    if EdCliente.ValueVariant <> 0 then
      QrMediaDias.SQL.Add('AND lot.Cliente='+EdCliente.Text);
    //CPF / CNPJ Emitente
    if EdCPF1.Text <> '' then
    begin
      if Trim(FGrupos) <> ''  then
        QrMediaDias.SQL.Add('AND loi.CPF in '+FGrupos+')')
      else
        QrMediaDias.SQL.Add('AND loi.CPF="'+Geral.SoNumero_TT(EdCPF1.Text)+'"');
    end;
    // Configura��o da pesquisa
    if T <> 4 then
    begin
      QrMediaDias.SQL.Add('');
      QrMediaDias.SQL.Add('AND (IF(loi.DDeposito > SYSDATE(), 1, 0) +');
      QrMediaDias.SQL.Add('IF((loi.DDeposito < SYSDATE()), 2, 0) -');
      QrMediaDias.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0), 2, 0) +');
      QrMediaDias.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12), 4, 0))');
      QrMediaDias.SQL.Add(' IN ('+IntToStr(A)+', '+IntToStr(Q)+', '+IntToStr(R)+', '+IntToStr(P)+')');
      QrMediaDias.SQL.Add('');
    end;

    QrMediaDias.Open;
  end;
  /////////////////////////////// FIM MEDIA DE DIAS

  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////

  //////////////////////////////////////////////////////////////////////////////
  QrItens.Close;
  QrItens.SQL.Clear;
  if RGTipo.ItemIndex in ([0,2]) then
  begin
    QrItens.SQL.Add('SELECT loi.Controle, loi.Valor, 1 ITENS,');
    QrItens.SQL.Add('');
    QrItens.SQL.Add('(IF(loi.DDeposito > SYSDATE(), 1, 0) +');
    QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()), 2, 0) -');
    QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0), 2, 0) +');
    QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12), 4, 0))');
    QrItens.SQL.Add('ITENS_XXX,');
    QrItens.SQL.Add('');
    QrItens.SQL.Add('IF(loi.DDeposito > SYSDATE(), 1, 0) ITENS_AVencer,');
    QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()), 1, 0) ITENS_Vencidos,');
    QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()), 1, 0) -');
    QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),');
    QrItens.SQL.Add('1, 0) ITENS_PgVencto,');
    QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0),');
    QrItens.SQL.Add('1, 0) ITENS_Devolvid,');
    QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12),');
    QrItens.SQL.Add('1, 0) ITENS_DevPgTot,');
    QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) in (10, 11)),');
    QrItens.SQL.Add('1, 0) ITENS_DevNaoQt,');
    QrItens.SQL.Add('(chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto) Pago,');
    QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE(), loi.Valor, 0) EXPIRADO,');
    QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE(), 0, loi.Valor) AEXPIRAR,');
    QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,');
    QrItens.SQL.Add('loi.Valor - IF(loi.DDeposito < SYSDATE(), 0, loi.Valor), 0) VENCIDO,');
    QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0, 0,');
    QrItens.SQL.Add('loi.Valor - IF(loi.DDeposito < SYSDATE(), 0, loi.Valor)) PGDDEPOSITO,');
    QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,');
    QrItens.SQL.Add('chd.ValPago - chd.Taxas - chd.JurosV + chd.Desconto,0) PGATRAZO,');
    QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE() AND (chd.Status+10) > 0,');
    QrItens.SQL.Add('loi.Valor - (chd.ValPago - chd.Taxas - chd.JurosV) +');
    QrItens.SQL.Add('chd.Desconto, 0) PGABERTO, "C" Tipo,');
    QrItens.SQL.Add('MONTH(loi.DDeposito)+YEAR(loi.DDeposito)*100+0.00 PERIODO,');
    QrItens.SQL.Add('CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOMECLI,');
    QrItens.SQL.Add('ban.Nome NOMEBANCO, loi.DDeposito, loi.Emitente NOMEEMITENTE, ');
    QrItens.SQL.Add('loi.Banco, loi.Agencia, loi.Conta, loi.Cheque, loi.Duplicata');
    QrItens.SQL.Add('FROM lotesits loi');
    QrItens.SQL.Add('LEFT JOIN lotes     lot ON lot.Codigo=loi.Codigo');
    QrItens.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente');
    QrItens.SQL.Add('LEFT JOIN alinits   chd ON chd.ChequeOrigem=loi.Controle');
    QrItens.SQL.Add('LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco');
    QrItens.SQL.Add('WHERE lot.Tipo <> 1');
    if CkLotesNeg.Checked = False then
      QrItens.SQL.Add('AND lot.Codigo > 0');
    QrItens.SQL.Add('AND lot.TxCompra + lot.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
    if CkPeriodo.Checked then
      QrItens.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', TPIni.Date, TPFim.Date, True, True));
    //////////////////////////////////////////////////////////////////////////////
    if EdCliente.ValueVariant <> 0 then
      QrItens.SQL.Add('AND lot.Cliente='+EdCliente.Text);
    if EdCPF1.Text <> '' then
    begin
    if Trim(FGrupos) <> ''  then
        QrItens.SQL.Add('AND loi.CPF in '+FGrupos+')')
      else
        QrItens.SQL.Add('AND loi.CPF="'+Geral.SoNumero_TT(EdCPF1.Text)+'"');
    end;
  //////////////////////////////////////////////////////////////////////////////
    if T <> 4 then
    begin
      QrItens.SQL.Add('');
      QrItens.SQL.Add('AND (IF(loi.DDeposito > SYSDATE(), 1, 0) +');
      QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()), 2, 0) -');
      QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) > 0), 2, 0) +');
      QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()) AND ((chd.Status+10) = 12), 4, 0))');
      QrItens.SQL.Add(' IN ('+IntToStr(A)+', '+IntToStr(Q)+', '+IntToStr(R)+', '+IntToStr(P)+')');
      QrItens.SQL.Add('');
    end;
  end;
  //////////////////////////////////////////////////////////////////////////////

  if RGTipo.ItemIndex = 2 then QrItens.SQL.Add('UNION');

  //////////////////////////////////////////////////////////////////////////////

  if RGTipo.ItemIndex in ([1,2]) then
  begin
    QrItens.SQL.Add('SELECT loi.Controle, loi.Valor, 1 ITENS,');
    QrItens.SQL.Add('');
    QrItens.SQL.Add('(IF(loi.DDeposito > SYSDATE(), 1, 0) +');
    QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 2, 0) +');
    QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 4, 0) )');
    QrItens.SQL.Add('ITENS_XXX,');
    QrItens.SQL.Add('');
    QrItens.SQL.Add('IF(loi.DDeposito > SYSDATE(), 1, 0) ITENS_AVencer,');
    QrItens.SQL.Add('IF((loi.DDeposito < SYSDATE()), 1, 0) ITENS_Vencidos,');
    QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 1, 0) ITENS_PgVencto,');
    QrItens.SQL.Add('IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0) +');
    QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0) ITENS_Devolvid,');
    QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 1, 0) ITENS_DevPgTot,');
    QrItens.SQL.Add('IF((loi.Quitado<2) AND (loi.DDeposito< SYSDATE()), 1, 0) ITENS_DevNaoQt,');
    QrItens.SQL.Add('(loi.TotalPg - loi.TotalJr + loi.TotalDs) Pago,');
    QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE(), loi.Valor, 0) EXPIRADO,');
    QrItens.SQL.Add('IF(loi.DDeposito < SYSDATE(), 0, loi.Valor) AEXPIRAR,');
    QrItens.SQL.Add('IF((loi.DDeposito<SYSDATE()) AND ((loi.Data3<2)');
    QrItens.SQL.Add('  OR (loi.DDeposito<loi.Data3)), loi.Valor, 0) VENCIDO,');
    QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3),');
    QrItens.SQL.Add('  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0) PGDDEPOSITO,');
    QrItens.SQL.Add('IF(/*(loi.Quitado> 1)AND*/ (loi.DDeposito< loi.Data3),');
    QrItens.SQL.Add('  loi.TotalPg - loi.TotalJr + loi.TotalDs, 0) PGATRAZO,');
    QrItens.SQL.Add('IF((loi.Quitado< 2)AND (loi.DDeposito< SYSDATE()),');
    QrItens.SQL.Add('  loi.Valor - (loi.TotalPg - loi.TotalJr) + loi.TotalDs, 0) PGABERTO, ');
    QrItens.SQL.Add('"D" Tipo, MONTH(loi.DDeposito)+YEAR(loi.DDeposito)*100+0.00 PERIODO,');
    QrItens.SQL.Add('CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial ELSE cli.Nome END NOMECLI,');
    QrItens.SQL.Add('ban.Nome NOMEBANCO, loi.DDeposito, loi.Emitente NOMEEMITENTE,');
    QrItens.SQL.Add('loi.Banco, loi.Agencia, loi.Conta, loi.Cheque, loi.Duplicata');
    QrItens.SQL.Add('FROM lotesits loi');
    QrItens.SQL.Add('LEFT JOIN lotes     lot ON lot.Codigo=loi.Codigo');
    QrItens.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=lot.Cliente');
    QrItens.SQL.Add('LEFT JOIN bancos    ban ON ban.Codigo=loi.Banco');
    QrItens.SQL.Add('WHERE lot.Tipo=1');
    QrItens.SQL.Add('AND lot.TxCompra + lot.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
    if CkLotesNeg.Checked = False then
      QrItens.SQL.Add('AND lot.Codigo > 0');
    if CkPeriodo.Checked then
      QrItens.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', TPIni.Date, TPFim.Date, True, True));
    //////////////////////////////////////////////////////////////////////////////
    if EdCliente.ValueVariant <> 0 then
      QrItens.SQL.Add('AND lot.Cliente='+EdCliente.Text);
    if EdCPF1.Text <> '' then
    begin
    if Trim(FGrupos) <> ''  then
        QrItens.SQL.Add('AND loi.CPF in '+FGrupos+')')
      else
        QrItens.SQL.Add('AND loi.CPF="'+Geral.SoNumero_TT(EdCPF1.Text)+'"');
    end;
    ////////////////////////////////////////////////////////////////////////////
    if T <> 4 then
    begin
      QrItens.SQL.Add('');
      QrItens.SQL.Add('AND (IF(loi.DDeposito > SYSDATE(), 1, 0) +');
      QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 2, 0) +');
      QrItens.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 4, 0) )');
      QrItens.SQL.Add(' IN ('+IntToStr(A)+', '+IntToStr(Q)+', '+IntToStr(R)+', '+IntToStr(P)+')');
      QrItens.SQL.Add('');
    end;
    ////////////////////////////////////////////////////////////////////////////
  end;
  QrItens.SQL.Add(MLAGeral.OrdemSQL3(FOrdC1, FOrdS1, FOrdC2, FOrdS2, FOrdC3, FOrdS3));
  QrItens.Open;
  //
  QrRevenda.Close;
  if RGTipo.ItemIndex in ([1,2]) then
  begin
    QrRevenda.SQL.Clear;
    QrRevenda.SQL.Add('SELECT SUM(adp.Pago-adp.Juros) Pago,');
    QrRevenda.SQL.Add('SUM((adp.Pago-adp.Juros)/ loi.Valor) Itens');
    QrRevenda.SQL.Add('FROM aduppgs adp');
    QrRevenda.SQL.Add('LEFT JOIN lotesits loi ON loi.Controle=adp.LotesIts');
    QrRevenda.SQL.Add('LEFT JOIN lotes    lot ON lot.Codigo=loi.Codigo');
    QrRevenda.SQL.Add('WHERE adp.LotePg>0');
    //
    QrRevenda.SQL.Add('AND lot.TxCompra + lot.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
    if CkLotesNeg.Checked = False then
      QrRevenda.SQL.Add('AND lot.Codigo > 0');
    if CkPeriodo.Checked then
      QrRevenda.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ', TPIni.Date, TPFim.Date, True, True));
    //////////////////////////////////////////////////////////////////////////////
    if EdCliente.ValueVariant <> 0 then
      QrRevenda.SQL.Add('AND lot.Cliente='+EdCliente.Text);
    if EdCPF1.Text <> '' then
    begin
    if Trim(FGrupos) <> ''  then
        QrRevenda.SQL.Add('AND loi.CPF in '+FGrupos+')')
      else
        QrRevenda.SQL.Add('AND loi.CPF="'+Geral.SoNumero_TT(EdCPF1.Text)+'"');
    end;
    ////////////////////////////////////////////////////////////////////////////
    if T <> 4 then
    begin
      QrRevenda.SQL.Add('');
      QrRevenda.SQL.Add('AND (IF(loi.DDeposito > SYSDATE(), 1, 0) +');
      QrRevenda.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito>=loi.Data3), 2, 0) +');
      QrRevenda.SQL.Add('IF((loi.Quitado>1) AND (loi.DDeposito< loi.Data3), 4, 0) )');
      QrRevenda.SQL.Add(' IN ('+IntToStr(A)+', '+IntToStr(Q)+', '+IntToStr(R)+', '+IntToStr(P)+')');
      QrRevenda.SQL.Add('');
    end;
    ////////////////////////////////////////////////////////////////////////////
    QrRevenda.Open;
  end;
  // Para imprimir
  if Length(FGrupos) > 0 then FGrupos[1] := ' ';
  Screen.Cursor := crDefault;
end;

procedure TFmHistCliEmi.CkPeriodoClick(Sender: TObject);
begin
  TPIni.Visible := CkPeriodo.Checked;
  TPFim.Visible := CkPeriodo.Checked;
  FechaPesquisa;
end;

procedure TFmHistCliEmi.QrItensCalcFields(DataSet: TDataSet);
begin
  if QrItensITENS_AVencer.Value = 1 then
    QrItensNOMESTATUS.Value := 'A vencer'
  else if QrItensITENS_PgVencto.Value = 1 then
    QrItensNOMESTATUS.Value := 'Quitado'
  else if QrItensITENS_DevPgTot.Value = 1 then
    QrItensNOMESTATUS.Value := 'Revendido'
  else
    QrItensNOMESTATUS.Value := 'Pendente';

  //////////////////////////////////////////////////////////////////////////////

  if QrItensTipo.Value = 'C' then QrItensDOCUMENTO.Value := 'C '+
    FormatFloat('000', QrItensBanco.Value)+'/'+FormatFloat('0000',
    QrItensAgencia.Value)+'  '+QrItensConta.Value+' - '+
    FormatFloat('000000', QrItensCheque.Value)
  else QrItensDOCUMENTO.Value := 'D ' +
    FormatFloat('000', QrItensBanco.Value)+'/'+FormatFloat('0000',
    QrItensAgencia.Value)+' - '+QrItensDuplicata.Value;
end;

procedure TFmHistCliEmi.EdCPF1Change(Sender: TObject);
begin
  FechaPesquisa;
  MostraCkGrupos;
end;

procedure TFmHistCliEmi.TPIniChange(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.TPIniClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.TPFimChange(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.TPFimClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.CkCrescenteClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.QrSomaACalcFields(DataSet: TDataSet);
var
  i: Double;
begin
  QrSomaASOMA_I_AVencer.Value  := QrSomaAITENS_AVencer.Value + QrSomaBITENS_AVencer.Value;
  QrSomaASOMA_I_Vencidos.Value := QrSomaAITENS_Vencidos.Value + QrSomaBITENS_Vencidos.Value;
  QrSomaASOMA_I_ITENS.Value := QrSomaAITENS.Value + QrSomaBITENS.Value;
  i := QrSomaASOMA_I_ITENS.Value / 100;
  if i > 0 then
  begin
    QrSomaAPERC_AVencer.Value  := QrSomaASOMA_I_AVencer.Value  / i;
    QrSomaAPERC_Vencidos.Value := QrSomaASOMA_I_Vencidos.Value / i;
  end else begin
    QrSomaAPERC_AVencer.Value  := 0;
    QrSomaAPERC_Vencidos.Value := 0;
  end;
  //
  QrSomaASOMA_I_PgVencto.Value := QrSomaAITENS_PgVencto.Value + QrSomaBITENS_PgVencto.Value;
  QrSomaASOMA_I_Devolvid.Value := QrSomaAITENS_Devolvid.Value + QrSomaBITENS_Devolvid.Value;
  i := QrSomaASOMA_I_Vencidos.Value / 100;
  if i > 0 then
  begin
    QrSomaAPERC_PgVencto.Value := (QrSomaASOMA_I_PgVencto.Value) / i;
    QrSomaAPERC_Devolvid.Value := (QrSomaASOMA_I_Devolvid.Value) / i;
  end else begin
    QrSomaAPERC_PgVencto.Value := 0;
    QrSomaAPERC_Devolvid.Value := 0;
  end;
  i := QrSomaASOMA_I_Devolvid.Value / 100;
  QrSomaASOMA_I_DevPgTot.Value := QrSomaAITENS_DevPgTot.Value + QrSomaBITENS_DevPgTot.Value;
  QrSomaASOMA_I_DevNaoQt.Value := QrSomaAITENS_DevNaoQt.Value + QrSomaBITENS_DevNaoQt.Value;
  if i > 0 then
  begin
    QrSomaAPERC_DevPgTot.Value := (QrSomaASOMA_I_DevPgTot.Value) / i;
    QrSomaAPERC_DevNaoQt.Value := (QrSomaASOMA_I_DevNaoQt.Value) / i;
  end else begin
    QrSomaAPERC_DevPgTot.Value := 0;
    QrSomaAPERC_DevNaoQt.Value := 0;
  end;
  QrTudo.Close;
  QrTudo.Open;
  if QrTudoITENS.Value > 0 then QrSomaAPERC_ITENS.Value :=
    QrSomaASOMA_I_ITENS.Value / QrTudoITENS.Value * 100
  else QrSomaAPERC_ITENS.Value := 0;
  //////////////////////////////////////////////////////////////////////////////
  QrSomaASOMA_V_Valor.Value := QrSomaAValor.Value + QrSomaBValor.Value;
  i := QrSomaASOMA_V_Valor.Value / 100;
  QrSomaASOMA_V_AEXPIRAR.Value := QrSomaAAEXPIRAR.Value + QrSomaBAEXPIRAR.Value;
  QrSomaASOMA_V_EXPIRADO.Value := QrSomaAEXPIRADO.Value + QrSomaBEXPIRADO.Value;
  if i > 0 then
  begin
    QrSomaAPERC_AExpirar.Value := QrSomaASOMA_V_AEXPIRAR.Value  / i;
    QrSomaAPERC_Expirado.Value := QrSomaASOMA_V_EXPIRADO.Value / i;
  end else begin
    QrSomaAPERC_AExpirar.Value := 0;
    QrSomaAPERC_Expirado.Value := 0;
  end;
  //
  i := QrSomaASOMA_V_EXPIRADO.Value / 100;
  QrSomaASOMA_V_VENCIDO.Value := QrSomaAVENCIDO.Value + QrSomaBVENCIDO.Value;
  QrSomaASOMA_V_PGDDEPOSITO.Value := QrSomaAPGDDEPOSITO.Value + QrSomaBPGDDEPOSITO.Value;
  if i > 0 then
  begin
    QrSomaAPERC_Vencido.Value     := QrSomaASOMA_V_VENCIDO.Value / i;
    QrSomaAPERC_PgDDeposito.Value := QrSomaASOMA_V_PGDDEPOSITO.Value / i;
  end else begin
    QrSomaAPERC_Vencido.Value     := 0;
    QrSomaAPERC_PgDDeposito.Value := 0;
  end;
  i := QrSomaASOMA_V_VENCIDO.Value / 100;
  QrSomaASOMA_V_PGATRAZO.Value := QrSomaAPGATRAZO.Value + QrSomaBPGATRAZO.Value;
  QrSomaASOMA_V_PGABERTO.Value := QrSomaAPGABERTO.Value + QrSomaBPGABERTO.Value;
  if i > 0 then
  begin
    QrSomaAPERC_Atrazo.Value := QrSomaASOMA_V_PGATRAZO.Value / i;
    QrSomaAPERC_Aberto.Value := QrSomaASOMA_V_PGABERTO.Value / i;
  end else begin
    QrSomaAPERC_Atrazo.Value := 0;
    QrSomaAPERC_Aberto.Value := 0;
  end;
  if QrTudoVALOR.Value > 0 then QrSomaAPERC_VALOR.Value :=
    QrSomaASOMA_V_Valor.Value / QrTudoVALOR.Value * 100
  else QrSomaAPERC_VALOR.Value := 0;
end;

procedure TFmHistCliEmi.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxHistorico, 'Hist�rico');
end;

procedure TFmHistCliEmi.RGTipoClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.FechaPesquisa;
begin
  QrItens.Close;
  QrSomaA.Close;
  QrSomaB.Close;
  // N�o pode!!! QrGruSacEmiIts.Close;
end;

procedure TFmHistCliEmi.DBGrid1TitleClick(Column: TColumn);
var
  Antigo: String;
begin
  Antigo := FOrdC1;
  FOrdC1 := Column.FieldName;
  FOrdC2 := '';
  FOrdS2 := '';
  FOrdC3 := '';
  FOrdS3 := '';
  if FOrdC1 = 'DOCUMENTO' then
  begin
    FOrdC1 := 'Tipo, Banco, Agencia, Conta, Cheque, Duplicata';
  end else if FOrdC1 = 'NOMESTATUS' then
  begin
    FOrdC1 := 'ITENS_AVencer';
    FOrdC2 := 'ITENS_PgVencto';
    FOrdC3 := 'ITENS_DevPgTot';
  end else if FOrdC1 = 'Tipo' then
  begin
    FOrdC2 := 'DDeposito';
    FOrdC3 := 'Valor';
  end;
  if Antigo = FOrdC1 then
  FOrdS1 := MLAGeral.InverteOrdemAsc(FOrdS1);
  if FOrdC1 = 'ITENS_AVencer' then
  begin
    FOrdS2 := FOrdS1;
    FOrdS3 := FOrdS1;
  end;
  Pesquisa;
end;

procedure TFmHistCliEmi.BtGrupoClick(Sender: TObject);
begin
  FmPrincipal.CadastroGruSacEmi(QrGruSacEmiItsCodigo.Value);
  if Length(Trim(EdCPF1.Text)) > 0 then ReopenEmiSac(EdCPF1.Text);
  FechaPesquisa;
  Pesquisa;
end;

procedure TFmHistCliEmi.PageControl1Change(Sender: TObject);
begin
  if PageControl1.ActivePageIndex = 1 then BtGrupo.Visible := True
  else BtGrupo.Visible := False;
end;

procedure TFmHistCliEmi.QrGruSacEmiItsCalcFields(DataSet: TDataSet);
begin
  QrGruSacEmiItsCNPJ_CPF_TXT.Value :=
    Geral.FormataCNPJ_TT(QrGruSacEmiItsCNPJ_CPF.Value);
end;

procedure TFmHistCliEmi.CkLotesNegClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.CkMediaDiasClick(Sender: TObject);
begin
  dmkEdMediaDias.Visible := CkMediaDias.Checked;
  FechaPesquisa;
end;

procedure TFmHistCliEmi.CkGruposClick(Sender: TObject);
begin
  FechaPesquisa;
end;

procedure TFmHistCliEmi.QrGruSacEmiItsBeforeClose(DataSet: TDataSet);
begin
  MostraCkGrupos;
end;

procedure TFmHistCliEmi.QrGruSacEmiItsAfterOpen(DataSet: TDataSet);
begin
  MostraCkGrupos;
end;

procedure TFmHistCliEmi.MostraCkGrupos;
var
  Ver: Boolean;
begin
  Ver := False;
  if QrGruSacEmiIts.State = dsBrowse then
    if QrGruSacEmiIts.RecordCount > 0 then
      if EdCPF1.Text <> '' then Ver := True;
  CkGrupos.Enabled := Ver;
  CkGrupos.Checked := CkGrupos.Enabled;
end;

procedure TFmHistCliEmi.QrItensBeforeClose(DataSet: TDataSet);
begin
  BtImprime.Enabled := False;
end;

procedure TFmHistCliEmi.QrItensAfterOpen(DataSet: TDataSet);
begin
  BtImprime.Enabled := True;
end;

procedure TFmHistCliEmi.CkAClick(Sender: TObject);
begin
  DesativaItens;
end;

procedure TFmHistCliEmi.CkQClick(Sender: TObject);
begin
  DesativaItens;
end;

procedure TFmHistCliEmi.CkRClick(Sender: TObject);
begin
  DesativaItens;
end;

procedure TFmHistCliEmi.CkPClick(Sender: TObject);
begin
  DesativaItens;
end;

procedure TFmHistCliEmi.DesativaItens;
begin
  BtImprime.Enabled := False;
  QrItens.Close;
end;

procedure TFmHistCliEmi.QrRevendaCalcFields(DataSet: TDataSet);
begin
  if QrSomaASOMA_I_DevPgTot.Value = 0 then QrRevendaItens_Perc.Value := 0 else
  QrRevendaItens_Perc.Value := QrRevendaItens.Value /
    QrSomaASOMA_I_DevPgTot.Value * 100;
  //
  if QrSomaASOMA_V_PGATRAZO.Value = 0 then QrRevendaPago_Perc.Value := 0 else
  QrRevendaPago_Perc.Value := QrRevendaPago.Value /
    QrSomaASOMA_V_PGATRAZO.Value * 100;
end;

procedure TFmHistCliEmi.frxHistoricoGetValue(const VarName: String;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_CLIENTE') = 0 then
     Value := MLAGeral.CampoReportTxt(CBCliente.Text, 'TODOS')
  else if AnsiCompareText(VarName, 'VARF_EMISAC') = 0 then
     Value := MLAGeral.CampoReportTxt(DBEdit1.Text, 'TODOS')
  else if AnsiCompareText(VarName, 'VARF_GRUPO') = 0 then
     Value := FGrupos
  else if AnsiCompareText(VarName, 'VAR_AVENCER') = 0 then
     Value := CkA.Checked
  else if AnsiCompareText(VarName, 'VAR_QUITADO') = 0 then
     Value := CkQ.Checked
  else if AnsiCompareText(VarName, 'VAR_REVENDIDO') = 0 then
     Value := CkR.Checked
  else if AnsiCompareText(VarName, 'VAR_PENDENTE') = 0 then
     Value := CkP.Checked
  else if AnsiCompareText(VarName, 'VAR_MEDIADIAS') = 0 then
     Value := CkMediaDias.Checked
end;

end.

