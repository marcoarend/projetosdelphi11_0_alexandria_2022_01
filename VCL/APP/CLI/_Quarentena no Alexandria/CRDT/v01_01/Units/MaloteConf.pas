unit MaloteConf;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, Grids, DBGrids,
  ComCtrls, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnDmkProcFunc,
  UnDmkEnums;

type
  TFmMaloteConf = class(TForm)
    PainelDados: TPanel;
    Painel1: TPanel;
    DsTransportadoras: TDataSource;
    QrClientes: TmySQLQuery;
    QrClientesNOMEENTIDADE: TWideStringField;
    QrClientesCodigo: TIntegerField;
    PainelTitulo: TPanel;
    Image1: TImage;
    PainelConfig: TPanel;
    Label1: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    RGOQue: TRadioGroup;
    PainelConfere: TPanel;
    DBG2: TDBGrid;
    QrLotes: TmySQLQuery;
    DsLotes: TDataSource;
    QrLotesNOMECLI: TWideStringField;
    QrLotesCodigo: TIntegerField;
    QrLotesCliente: TIntegerField;
    QrLotesLote: TSmallintField;
    QrLotesData: TDateField;
    QrLotesTotal: TFloatField;
    RGStatus: TRadioGroup;
    GroupBox1: TGroupBox;
    CkPeriodo: TCheckBox;
    Label34: TLabel;
    TPIni: TDateTimePicker;
    TPFim: TDateTimePicker;
    Label2: TLabel;
    QrLoc: TmySQLQuery;
    QrLocCPF: TWideStringField;
    QrLocEmitente: TWideStringField;
    QrLocValor: TFloatField;
    Panel3: TPanel;
    Label36: TLabel;
    EdBanda: TdmkEdit;
    Label7: TLabel;
    EdEmitente: TdmkEdit;
    Label3: TLabel;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    EdConta: TdmkEdit;
    Label32: TLabel;
    EdCheque: TdmkEdit;
    EdCPF: TdmkEdit;
    Label6: TLabel;
    EdValor: TdmkEdit;
    Label11: TLabel;
    DBG1: TDBGrid;
    QrLotesIts: TmySQLQuery;
    DsLotesIts: TDataSource;
    QrLotesItsControle: TIntegerField;
    QrLotesItsBanco: TIntegerField;
    QrLotesItsAgencia: TIntegerField;
    QrLotesItsConta: TWideStringField;
    QrLotesItsCheque: TIntegerField;
    QrLotesItsCPF: TWideStringField;
    QrLotesItsEmitente: TWideStringField;
    QrLotesItsValor: TFloatField;
    QrLotesItsCONF_V: TIntegerField;
    QrLocCodigo: TIntegerField;
    QrLocControle: TIntegerField;
    PainelControle: TPanel;
    BtConfere: TBitBtn;
    BtSair: TBitBtn;
    PainelConfirma: TPanel;
    BtRatifica: TBitBtn;
    BtDesiste: TBitBtn;
    QrLotesItsDADOS: TWideStringField;
    QrLotesItsVALORMALOTE: TFloatField;
    QrLotesItsLOCCHEQUE: TIntegerField;
    QrLotesItsCodigo: TIntegerField;
    QrLotesItens: TIntegerField;
    Label8: TLabel;
    QrSumM: TmySQLQuery;
    DsSumM: TDataSource;
    QrSumMVALOR: TFloatField;
    QrSumMCHEQUES: TFloatField;
    DBEdItens: TDBEdit;
    DBEdValor: TDBEdit;
    BtDesfaz: TBitBtn;
    BtLimpar: TBitBtn;
    Timer1: TTimer;
    procedure BtSairClick(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure CkPeriodoClick(Sender: TObject);
    procedure TPIniChange(Sender: TObject);
    procedure TPFimChange(Sender: TObject);
    procedure RGStatusClick(Sender: TObject);
    procedure EdValorExit(Sender: TObject);
    procedure EdBandaChange(Sender: TObject);
    procedure RGOQueClick(Sender: TObject);
    procedure QrLotesAfterScroll(DataSet: TDataSet);
    procedure DBG1DrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtConfereClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure DBG1CellClick(Column: TColumn);
    procedure QrLotesItsCalcFields(DataSet: TDataSet);
    procedure BtRatificaClick(Sender: TObject);
    procedure BtDesfazClick(Sender: TObject);
    procedure DBEdItensChange(Sender: TObject);
    procedure DBEdValorChange(Sender: TObject);
    procedure BtLimparClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
  private
    { Private declarations }
    FLotes, FLotesIts: Integer;
    procedure ReopenLotes;
    procedure ReopenLotesIts;
    procedure CliqueItem(Tipo: Integer);
    procedure InsereMaloteConf(Localizou: Integer; Valor, Real: Double;
              Codigo, Controle: Integer; Avisa: Boolean);
    procedure RatificaLote(Status: Integer);
    procedure LocalizaItem;
    procedure VoltaABanda;
  public
    { Public declarations }
  end;

var
  FmMaloteConf: TFmMaloteConf;

implementation

uses Module, Entidades, Principal, MyVCLSkin, UnMyObjects;

{$R *.DFM}

procedure TFmMaloteConf.BtSairClick(Sender: TObject);
begin
  Geral.WriteAppKeyLM2('Malotes\Confere\', Application.Title, RGOQue.ItemIndex, ktInteger);
  Close;
end;

procedure TFmMaloteConf.EdClienteChange(Sender: TObject);
begin
  ReopenLotes;
end;

procedure TFmMaloteConf.FormCreate(Sender: TObject);
begin
  FLotes := 0;
  FLotesIts := 0;
  QrClientes.Open;
  TPIni.Date := Date - 30;
  TPFim.Date := Date;
  RGOQue.ItemIndex := Geral.ReadAppKeyLM('Malotes\Confere\', Application.Title, ktInteger, 0);
  ReopenLotes;
end;

procedure TFmMaloteConf.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1,PainelTitulo, True,0);
end;

procedure TFmMaloteConf.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmMaloteConf.ReopenLotes;
var
  Per: Boolean;
  Cli: Integer;
  Lig: String;
begin
  Cli := Geral.IMV(EdCliente.Text);
  //Lig := 'WHERE ';
  QrLotes.Close;
  QrLotes.SQL.Clear;
  QrLotes.SQL.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  QrLotes.SQL.Add('ELSE en.Nome END NOMECLI, lo.Codigo, lo.Cliente,');
  QrLotes.SQL.Add('lo.Lote, lo.Data, lo.Total, lo.Itens');
  QrLotes.SQL.Add('FROM lotes lo');
  QrLotes.SQL.Add('LEFT JOIN entidades en ON en.Codigo=lo.Cliente');
  QrLotes.SQL.Add('WHERE lo.Tipo=0');
  QrLotes.SQL.Add('AND lo.Codigo>0');
  QrLotes.SQL.Add('AND lo.TxCompra+lo.ValValorem+ '+
      IntToStr(FmPrincipal.FConnections)+' >= 0.01');
  Lig := 'AND ';
  if RGStatus.ItemIndex < 2 then
  begin
    QrLotes.SQL.Add(Lig+'lo.Conferido='+IntToStr(RGStatus.ItemIndex));
    //Lig := 'AND ';
  end;
  if Cli <> 0 then
  begin
    QrLotes.SQL.Add(Lig+'lo.Cliente='+IntToStr(Cli));
    //Lig := 'AND ';
  end;
  Per := CkPeriodo.Checked;
  QrLotes.SQL.Add(dmkPF.SQL_Periodo(Lig+'lo.Data ',
    TPIni.Date, TPFim.Date, Per, Per));
  QrLotes.SQL.Add('ORDER BY NOMECLI, Lote DESC');
  QrLotes.Open;
  QrLotes.Locate('Codigo', FLotes, []);
end;

procedure TFmMaloteConf.CkPeriodoClick(Sender: TObject);
begin
  ReopenLotes;
end;

procedure TFmMaloteConf.TPIniChange(Sender: TObject);
begin
  ReopenLotes;
end;

procedure TFmMaloteConf.TPFimChange(Sender: TObject);
begin
  ReopenLotes;
end;

procedure TFmMaloteConf.RGStatusClick(Sender: TObject);
begin
  ReopenLotes;
end;

procedure TFmMaloteConf.EdValorExit(Sender: TObject);
begin
  case RGOQue.ItemIndex of
    0:
    begin
      LocalizaItem;
      if QrLoc.RecordCount > 0 then
      begin
        InsereMaloteConf(1, Geral.DMV(EdValor.Text), QrLocValor.Value,
          QrLocCodigo.Value, QrLocControle.Value, True);
        VoltaABanda;
      end;
    end;
    1: EdBanda.SetFocus;
    2:
    begin
      InsereMaloteConf(0, Geral.DMV(EdValor.Text), QrLocValor.Value,
        QrLocCodigo.Value, -1, True);
      EdValor.Text := '';
      //EdValor.SetFocus;
    end;
  end;
end;

procedure TFmMaloteConf.EdBandaChange(Sender: TObject);
var
  CMC7: String;
  Banda: TBandaMagnetica;
  //Ban: Integer;
begin
  CMC7 := Geral.SoNumero_TT(EdBanda.Text);
  //Ban := Length(EdBanda.Text);
  if MLAGeral.LeuTodoCMC7(EdBanda.Text) then
  begin
    if MLAGeral.CalculaCMC7(CMC7) = 0 then
    begin
      Banda := TBandaMagnetica.Create;
      Banda.BandaMagnetica := CMC7;
      //EdComp.Text    := Banda.Compe;
      EdBanco.Text   := Banda.Banco;
      EdAgencia.Text := Banda.Agencia;
      EdConta.Text   := Banda.Conta;
      EdCheque.Text  := Banda.Numero;
      //
      LocalizaItem;
      if QrLoc.RecordCount > 0 then
      begin
        if RGOQue.ItemIndex <> 1 then Timer1.Enabled := True
        else begin
          EdCPF.Text := Geral.FormataCNPJ_TT(QrLocCPF.Value);
          EdEmitente.Text := QrLocEmitente.Text;
          begin
            InsereMaloteConf(1, Geral.DMV(EdValor.Text), QrLocValor.Value,
              QrLocCodigo.Value, QrLocControle.Value, False);
            Timer1.Enabled := True;
          end;
        end;
      end else begin
        Beep;
        Application.MessageBox('Cheque n�o localizado!', 'Erro', MB_OK+MB_ICONERROR);  
      end;
    end;
  end;
end;

procedure TFmMaloteConf.RGOQueClick(Sender: TObject);
begin
  case RGOQue.ItemIndex of
    0: EdBanda.Enabled := True;
    1: EdBanda.Enabled := True;
    2: EdBanda.Enabled := False;
  end;
  case RGOQue.ItemIndex of
    0: EdValor.Enabled := True;
    1: EdValor.Enabled := False;
    2: EdValor.Enabled := True;
  end;
end;

procedure TFmMaloteConf.QrLotesAfterScroll(DataSet: TDataSet);
begin
  ReopenLotesIts;
end;

procedure TFmMaloteConf.ReopenLotesIts;
begin
  QrLotesIts.Close;
  QrLotesIts.Params[0].AsInteger := QrLotesCodigo.Value;
  QrLotesIts.Open;
  //
  QrLotesIts.Locate('Controle', FLotesIts, []);
  //
  QrSumM.Close;
  QrSumM.Params[0].AsInteger := QrLotesCodigo.Value;
  QrSumM.Open;
end;

procedure TFmMaloteConf.DBG1DrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn;
  State: TGridDrawState);
begin
  if DBG1.Visible then
  begin
    if Column.FieldName = 'LOCCHEQUE' then
      MeuVCLSkin.DrawGrid(DBG1, Rect, 1, QrLotesItsLOCCHEQUE.Value);
    if Column.FieldName = 'CONF_V' then
      MeuVCLSkin.DrawGrid(DBG1, Rect, 1, QrLotesItsCONF_V.Value);
  end;
  //
end;

procedure TFmMaloteConf.BtConfereClick(Sender: TObject);
begin
  PainelConfere.Visible  := True;
  PainelConfirma.Visible := True;
  DBG2.Enabled           := False;
  DBG2.Align             := alTop;
  PainelConfere.Align    := alClient;
  PainelControle.Visible := False;
  PainelConfig.Enabled   := False;
  //
  if EdBanda.Enabled then EdBanda.SetFocus else
  if EdValor.Enabled then EdValor.SetFocus;
end;

procedure TFmMaloteConf.BtDesisteClick(Sender: TObject);
begin
  PainelControle.Visible := True;
  PainelConfere.Visible  := False;
  DBG2.Enabled           := True;
  PainelConfirma.Visible := False;
  PainelConfere.Align    := alBottom;
  DBG2.Align             := alClient;
  PainelConfig.Enabled   := True;
end;

procedure TFmMaloteConf.DBG1CellClick(Column: TColumn);
begin
  FLotesIts := QrLotesItsControle.Value;
  if (Column.Field.FieldName = 'LOCCHEQUE') then CliqueItem(0) else
  if (Column.Field.FieldName = 'CONF_V') then CliqueItem(1);
end;

procedure TFmMaloteConf.CliqueItem(Tipo: Integer);
var
  Localizou: Integer;
  Valor: Double;
begin
  Localizou := 0;
  Valor     := 0;
  Screen.Cursor := crHourGlass;
  case Tipo of
    0:
    begin
      Localizou := MLAGeral.IntBool_Inverte(QrLotesItsLOCCHEQUE.Value);
      Valor     := QrLotesItsVALORMALOTE.Value;
    end;
    1:
    begin
      Localizou := QrLotesItsLOCCHEQUE.Value;
      if QrLotesItsCONF_V.Value = 1 then Valor := 0 else
        Valor := QrLotesItsValor.Value;
    end;
    else Application.MessageBox(PChar('Situa��o de desconhecida!'),
    'Erro', MB_OK+MB_ICONERROR);
  end;
  InsereMaloteConf(Localizou, Valor, QrLotesItsValor.Value,
    QrLotesItsCodigo.Value, QrLotesItsControle.Value, False);
  Screen.Cursor := crDefault;
end;

procedure TFmMaloteConf.InsereMaloteConf(Localizou: Integer; Valor, Real: Double;
Codigo, Controle: Integer; Avisa: Boolean);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM malotes');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P0 AND Controle=:P1');
  Dmod.QrUpd.Params[0].AsInteger := Codigo;
  Dmod.QrUpd.Params[1].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('INSERT INTO malotes SET Cheque=:P0, ');
  Dmod.QrUpd.SQL.Add('Valor=:P1, Codigo=:P2, Controle=:P3');
  Dmod.QrUpd.Params[0].AsInteger := Localizou;
  Dmod.QrUpd.Params[1].AsFloat   := Valor;
  Dmod.QrUpd.Params[2].AsInteger := Codigo;
  Dmod.QrUpd.Params[3].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  ReopenLotesIts;
  //
  if (int(Valor*100) <> (Real*100)) and Avisa then
  begin
    Beep;
    Application.MessageBox('Valor n�o Confere!', 'Erro', MB_OK+MB_ICONERROR);
  end;
end;

procedure TFmMaloteConf.QrLotesItsCalcFields(DataSet: TDataSet);
begin
  QrLotesItsCONF_V.Value := MLAGeral.BoolToInt(QrLotesItsValor.Value =
    QrLotesItsVALORMALOTE.Value);
  //
  QrLotesItsDADOS.Value := MLAGeral.FFD(QrLotesItsBanco.Value, 3, siPositivo)+
    ' / ' + MLAGeral.FFD(QrLotesItsAgencia.Value, 4, siPositivo) +
    ' / ' + QrLotesItsConta.Value +
    ' - ' + MLAGeral.FFD(QrLotesItsCheque.Value, 6, siPositivo);
end;

procedure TFmMaloteConf.BtRatificaClick(Sender: TObject);
begin
  RatificaLote(1);
  BtDesisteClick(Self);
end;                                    

procedure TFmMaloteConf.RatificaLote(Status: Integer);
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotes SET AlterWeb=1,  Conferido=:P0 WHERE Codigo=:P1');
  Dmod.QrUpd.Params[0].AsInteger := Status;
  Dmod.QrUpd.Params[1].AsInteger := QrLotesCodigo.Value;
  Dmod.QrUpd.ExecSQL;
  //
  FLotes := UMyMod.ProximoRegistro(QrLotes, 'Codigo', QrLotesCodigo.Value);
  ReopenLotes;
  FmPrincipal.AtualizaSB2(1);
end;

procedure TFmMaloteConf.BtDesfazClick(Sender: TObject);
begin
  RatificaLote(0);
end;

procedure TFmMaloteConf.DBEdItensChange(Sender: TObject);
begin
  DBEdItens.Font.Color := MLAGeral.BoolToInt2(QrSumMCHEQUES.Value =
    QrLotesItens.Value, clGreen, clRed);
end;

procedure TFmMaloteConf.DBEdValorChange(Sender: TObject);
begin
  DBEdValor.Font.Color := MLAGeral.BoolToInt2(QrSumMVALOR.Value =
    QrLotesTotal.Value, clGreen, clRed);
end;

procedure TFmMaloteConf.LocalizaItem;
begin
  QrLoc.Close;
  QrLoc.Params[0].AsInteger := QrLotesCodigo.Value;
  QrLoc.Params[1].AsInteger := Geral.IMV(EdBanco.Text);
  QrLoc.Params[2].AsInteger := Geral.IMV(EdAgencia.Text);
  QrLoc.Params[3].AsString  := EdConta.Text;
  QrLoc.Params[4].AsInteger := Geral.IMV(EdCheque.Text);
  QrLoc.Open;
end;

procedure TFmMaloteConf.BtLimparClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('DELETE FROM malotes');
  Dmod.QrUpd.ExecSQL;
  //
  ReopenLotesIts;
  Screen.Cursor := crDefault;
end;

procedure TFmMaloteConf.VoltaABanda;
begin
  EdBanda.Text := '';
  EdBanda.SetFocus;
end;

procedure TFmMaloteConf.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  case RGOque.ItemIndex of
   0: EdValor.SetFocus;
   1: VoltaABanda;
   2: EdValor.SetFocus;
  end;
end;

end.

