object FmDuplicatas2: TFmDuplicatas2
  Left = 258
  Top = 164
  Caption = 'Controle de Duplicatas'
  ClientHeight = 562
  ClientWidth = 1016
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1016
    Height = 48
    Align = alTop
    Caption = 'Controle de Duplicatas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 789
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 1012
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbBordero: TBitBtn
        Tag = 116
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbBorderoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        Visible = False
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        Visible = False
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        Visible = False
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1016
    Height = 514
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 200
      Width = 1016
      Height = 314
      Align = alClient
      TabOrder = 0
      object Splitter1: TSplitter
        Left = 1
        Top = 169
        Width = 1014
        Height = 3
        Cursor = crVSplit
        Align = alTop
      end
      object Panel4: TPanel
        Left = 1
        Top = 265
        Width = 1014
        Height = 48
        Align = alBottom
        TabOrder = 0
        object PainelConf: TPanel
          Left = 1
          Top = 1
          Width = 1012
          Height = 46
          Align = alClient
          TabOrder = 1
          Visible = False
          object Label19: TLabel
            Left = 196
            Top = 16
            Width = 403
            Height = 13
            Caption = 
              'Obs.: Duplo clique na grade de pesquisa abre janela com dados ed' +
              'it'#225'veis do sacado.'
          end
          object BtConfirma: TBitBtn
            Tag = 14
            Left = 24
            Top = 3
            Width = 90
            Height = 40
            Caption = '&Confirma'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtConfirmaClick
          end
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 678
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Desiste'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtDesisteClick
          end
        end
        object PainelCtrl: TPanel
          Left = 1
          Top = 1
          Width = 1012
          Height = 46
          Align = alClient
          TabOrder = 0
          object BtZAZ: TBitBtn
            Tag = 153
            Left = 192
            Top = 3
            Width = 90
            Height = 40
            Caption = 'H&+P'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 2
            OnClick = BtZAZClick
          end
          object BtPagtoDuvida: TBitBtn
            Tag = 152
            Left = 100
            Top = 3
            Width = 90
            Height = 40
            Caption = '&Pagto'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 1
            OnClick = BtPagtoDuvidaClick
          end
          object BtCuidado: TBitBtn
            Tag = 151
            Left = 8
            Top = 3
            Width = 90
            Height = 40
            Caption = '&Hist'#243'rico'
            Enabled = False
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BtCuidadoClick
          end
          object BtDuplicataOcorr: TBitBtn
            Tag = 148
            Left = 378
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Ocorr'#234'ncia'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            Visible = False
            OnClick = BtDuplicataOcorrClick
          end
          object BtStatusManual: TBitBtn
            Tag = 154
            Left = 284
            Top = 3
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Status'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 3
            OnClick = BtStatusManualClick
          end
          object BtReabre: TBitBtn
            Tag = 18
            Left = 660
            Top = 2
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Reabre'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 5
            OnClick = BtReabreClick
          end
          object BtQuitacao: TBitBtn
            Tag = 172
            Left = 472
            Top = 2
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Quita'#231#227'o'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 6
            OnClick = BtQuitacaoClick
          end
          object Panel2: TPanel
            Left = 808
            Top = 1
            Width = 203
            Height = 44
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 7
            object BtSaida: TBitBtn
              Tag = 13
              Left = 106
              Top = 3
              Width = 90
              Height = 40
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
            object BtRefresh: TBitBtn
              Tag = 18
              Left = 11
              Top = 3
              Width = 90
              Height = 40
              Caption = 'Re&fresh'
              Enabled = False
              NumGlyphs = 2
              TabOrder = 1
              OnClick = BtRefreshClick
            end
          end
          object ProgressBar1: TProgressBar
            Left = 752
            Top = 25
            Width = 65
            Height = 17
            TabOrder = 8
            Visible = False
          end
          object BtHistCNAB: TBitBtn
            Left = 566
            Top = 2
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Hist. CNAB'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 9
            Visible = False
            OnClick = BtHistCNABClick
          end
        end
      end
      object Panel8: TPanel
        Left = 1
        Top = 1
        Width = 1014
        Height = 168
        Align = alTop
        TabOrder = 1
        object GradeItens: TDBGrid
          Left = 1
          Top = 1
          Width = 1012
          Height = 46
          Align = alClient
          DataSource = DsPesq
          PopupMenu = PMDiario
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDrawColumnCell = GradeItensDrawColumnCell
          OnDblClick = GradeItensDblClick
          OnMouseDown = GradeItensMouseDown
          OnTitleClick = GradeItensTitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'DATA3_TXT'
              Title.Caption = 'Quitado em'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMELOCAL'
              Title.Caption = 'Local'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Duplicata'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DCompra'
              Title.Caption = 'Compra'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Valor'
              Title.Alignment = taRightJustify
              Width = 68
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DDeposito'
              Title.Caption = 'Resgate'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLIENTE'
              Title.Caption = 'Cliente'
              Width = 104
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Emitente'
              Width = 129
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TOTALATUALIZ'
              Title.Caption = 'Tot.Atz'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO_DESATUALIZ'
              Title.Caption = 'Saldo'
              Width = 68
              Visible = True
            end
            item
              Alignment = taRightJustify
              Expanded = False
              FieldName = 'SALDO_TEXTO'
              Title.Caption = 'Atualizado'
              Width = 72
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESTATUS'
              Title.Caption = 'Status'
              Width = 79
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'STATUS'
              Title.Caption = #218'ltima posi'#231#227'o no hist'#243'rico'
              Width = 196
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotalJr'
              Title.Caption = 'Juros'
              Width = 66
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotalDs'
              Title.Caption = 'Descontos'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'TotalPg'
              Title.Caption = 'Pago'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OCORRENCIAS'
              Title.Caption = 'Ocorr.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'OCORATUALIZ'
              Title.Caption = 'Ocorr.Atz.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NF'
              Title.Caption = 'N.F.'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 56
              Visible = True
            end>
        end
        object PainelMuda: TPanel
          Left = 1
          Top = 47
          Width = 1012
          Height = 120
          Align = alBottom
          TabOrder = 1
          Visible = False
          object PainelOcorPg: TPanel
            Left = 1
            Top = 25
            Width = 1010
            Height = 94
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 2
            Visible = False
            object Label86: TLabel
              Left = 4
              Top = 4
              Width = 52
              Height = 13
              Caption = 'Data base:'
            end
            object Label87: TLabel
              Left = 88
              Top = 4
              Width = 53
              Height = 13
              Caption = 'Valor base:'
            end
            object Label88: TLabel
              Left = 160
              Top = 4
              Width = 63
              Height = 13
              Caption = '%Tx jur.base:'
            end
            object Label89: TLabel
              Left = 244
              Top = 4
              Width = 68
              Height = 13
              Caption = '% Jur.per'#237'odo:'
            end
            object Label82: TLabel
              Left = 0
              Top = 44
              Width = 26
              Height = 13
              Caption = 'Data:'
            end
            object Label83: TLabel
              Left = 88
              Top = 44
              Width = 37
              Height = 13
              Caption = '$ Juros:'
            end
            object Label85: TLabel
              Left = 160
              Top = 44
              Width = 66
              Height = 13
              Caption = 'Total a pagar:'
            end
            object Label84: TLabel
              Left = 244
              Top = 44
              Width = 75
              Height = 13
              Caption = '$ Valor a pagar:'
            end
            object TPDataBase6: TDateTimePicker
              Left = 0
              Top = 20
              Width = 85
              Height = 21
              Date = 38698.785142685200000000
              Time = 38698.785142685200000000
              Color = clBtnFace
              TabOrder = 0
              TabStop = False
            end
            object EdValorBase6: TdmkEdit
              Left = 88
              Top = 20
              Width = 68
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 1
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdValorBase6Change
            end
            object EdJurosBase6: TdmkEdit
              Left = 160
              Top = 20
              Width = 80
              Height = 21
              Alignment = taRightJustify
              Color = clWhite
              TabOrder = 2
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 6
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdJurosBase6Change
            end
            object EdJurosPeriodo6: TdmkEdit
              Left = 244
              Top = 20
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 3
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 6
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,000000'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
            object TPPagto6: TDateTimePicker
              Left = 0
              Top = 60
              Width = 85
              Height = 21
              Date = 38698.785142685200000000
              Time = 38698.785142685200000000
              TabOrder = 4
              OnChange = TPPagto6Change
            end
            object EdJuros6: TdmkEdit
              Left = 88
              Top = 60
              Width = 68
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 5
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdJuros6Change
            end
            object EdAPagar6: TdmkEdit
              Left = 160
              Top = 60
              Width = 80
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              Color = clBtnFace
              ReadOnly = True
              TabOrder = 6
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
              OnChange = EdAPagar6Change
            end
            object EdPago6: TdmkEdit
              Left = 244
              Top = 60
              Width = 80
              Height = 21
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfDouble
              MskType = fmtNone
              DecimalSize = 2
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0,00'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0.000000000000000000
              ValWarn = False
            end
          end
          object PainelOcor: TPanel
            Left = 1
            Top = 25
            Width = 1010
            Height = 94
            Align = alClient
            BevelOuter = bvNone
            Color = clAppWorkSpace
            TabOrder = 1
            Visible = False
            object Panel5: TPanel
              Left = 52
              Top = 49
              Width = 617
              Height = 45
              TabOrder = 1
              object Label20: TLabel
                Left = 8
                Top = 4
                Width = 55
                Height = 13
                Caption = 'Ocorr'#234'ncia:'
              end
              object Label21: TLabel
                Left = 352
                Top = 4
                Width = 29
                Height = 13
                Caption = 'Data: '
              end
              object Label22: TLabel
                Left = 460
                Top = 4
                Width = 27
                Height = 13
                Caption = 'Valor:'
              end
              object EdOcorrencia: TdmkEditCB
                Left = 8
                Top = 20
                Width = 65
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ValMin = '-2147483647'
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
                ValWarn = False
                OnExit = EdOcorrenciaExit
                DBLookupComboBox = CBOcorrencia
                IgnoraDBLookupComboBox = False
              end
              object CBOcorrencia: TdmkDBLookupComboBox
                Left = 76
                Top = 20
                Width = 273
                Height = 21
                KeyField = 'Codigo'
                ListField = 'Nome'
                ListSource = DsOcorBank
                TabOrder = 1
                dmkEditCB = EdOcorrencia
                UpdType = utYes
                LocF7SQLMasc = '$#'
              end
              object TPDataO: TDateTimePicker
                Left = 352
                Top = 20
                Width = 105
                Height = 21
                Date = 38711.818866817100000000
                Time = 38711.818866817100000000
                TabOrder = 2
              end
              object EdValor: TdmkEdit
                Left = 460
                Top = 20
                Width = 85
                Height = 21
                Alignment = taRightJustify
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnExit = EdValorExit
              end
            end
            object PainelPror: TPanel
              Left = 0
              Top = 0
              Width = 1010
              Height = 49
              Align = alTop
              TabOrder = 0
              Visible = False
              object Label13: TLabel
                Left = 12
                Top = 8
                Width = 84
                Height = 13
                Caption = 'Novo resgate em:'
              end
              object Label14: TLabel
                Left = 108
                Top = 8
                Width = 78
                Height = 13
                Caption = 'Taxa juros base:'
              end
              object Label29: TLabel
                Left = 192
                Top = 8
                Width = 79
                Height = 13
                Caption = '% Juros per'#237'odo:'
              end
              object Label27: TLabel
                Left = 368
                Top = 8
                Width = 37
                Height = 13
                Caption = '$ Juros:'
              end
              object Label15: TLabel
                Left = 276
                Top = 8
                Width = 62
                Height = 13
                Caption = '$ Valor base:'
              end
              object Label16: TLabel
                Left = 460
                Top = 8
                Width = 72
                Height = 13
                Caption = '1'#186' Vencimento:'
              end
              object TPDataN: TDateTimePicker
                Left = 12
                Top = 24
                Width = 93
                Height = 21
                Date = 38711.818866817100000000
                Time = 38711.818866817100000000
                TabOrder = 0
                OnChange = TPDataNChange
              end
              object EdTaxaPror: TdmkEdit
                Left = 108
                Top = 24
                Width = 81
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 6
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdTaxaProrChange
              end
              object EdJurosPeriodoN: TdmkEdit
                Left = 192
                Top = 24
                Width = 81
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 2
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 6
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object EdJurosPr: TdmkEdit
                Left = 368
                Top = 24
                Width = 88
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 3
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdJurosPrChange
              end
              object EdValorBase: TdmkEdit
                Left = 276
                Top = 24
                Width = 88
                Height = 21
                TabStop = False
                Alignment = taRightJustify
                Color = clBtnFace
                ReadOnly = True
                TabOrder = 4
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
                OnChange = EdValorBaseChange
              end
              object TPDataI: TDateTimePicker
                Left = 460
                Top = 24
                Width = 93
                Height = 21
                Date = 38711.818866817100000000
                Time = 38711.818866817100000000
                Enabled = False
                TabOrder = 5
                TabStop = False
                OnChange = TPDataIChange
              end
            end
          end
          object PanelFill1: TPanel
            Left = 1
            Top = 1
            Width = 1010
            Height = 24
            Align = alTop
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -32
            Font.Name = 'Times New Roman'
            Font.Style = []
            ParentColor = True
            ParentFont = False
            TabOrder = 0
            TabStop = True
            object LaTipo: TLabel
              Left = 927
              Top = 1
              Width = 82
              Height = 22
              Align = alRight
              Alignment = taCenter
              AutoSize = False
              Caption = 'Travado'
              Font.Charset = ANSI_CHARSET
              Font.Color = 8281908
              Font.Height = -15
              Font.Name = 'Times New Roman'
              Font.Style = [fsBold]
              ParentFont = False
              Transparent = True
              ExplicitLeft = 926
              ExplicitTop = 2
              ExplicitHeight = 20
            end
          end
        end
      end
      object Panel7: TPanel
        Left = 1
        Top = 172
        Width = 1014
        Height = 93
        Align = alClient
        TabOrder = 2
        object PageControl1: TPageControl
          Left = 1
          Top = 1
          Width = 1012
          Height = 91
          ActivePage = TabSheet2
          Align = alClient
          TabOrder = 0
          OnChange = PageControl1Change
          object TabSheet1: TTabSheet
            Caption = 'Hist'#243'rico e pagamentos  '
            object DBGrid4: TDBGrid
              Left = 0
              Top = 0
              Width = 404
              Height = 63
              Align = alLeft
              DataSource = DsADupIts
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'DataA'
                  Title.Caption = 'Data'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMESTATUS'
                  Title.Caption = 'Hist'#243'rico'
                  Visible = True
                end>
            end
            object DBGrid3: TDBGrid
              Left = 404
              Top = 0
              Width = 600
              Height = 63
              Align = alClient
              DataSource = DsADupPgs
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Juros'
                  Title.Caption = '$ Juros'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Desco'
                  Title.Caption = '& Desconto'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pago'
                  Width = 72
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Lote'
                  Title.Caption = 'Border'#244
                  Width = 51
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'LotePg'
                  Title.Caption = 'Lote Pgto'
                  Width = 56
                  Visible = True
                end>
            end
          end
          object TabSheet2: TTabSheet
            Caption = 'Ocorr'#234'ncias  '
            ImageIndex = 1
            object DBGrid5: TDBGrid
              Left = 734
              Top = 0
              Width = 270
              Height = 63
              Align = alRight
              DataSource = DsOcorrPg
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Juros'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Pago'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'LotePg'
                  Title.Caption = 'Lote pgto'
                  Width = 48
                  Visible = True
                end>
            end
            object DBGrid1: TDBGrid
              Left = 0
              Top = 0
              Width = 734
              Height = 63
              TabStop = False
              Align = alClient
              DataSource = DsOcorreu
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'SEQ'
                  Title.Caption = 'Item'
                  Width = 26
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Codigo'
                  Title.Caption = 'Controle'
                  Width = 47
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataO'
                  Title.Caption = 'Data'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEOCORRENCIA'
                  Title.Caption = 'Ocorr'#234'ncia'
                  Width = 166
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Valor'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'SALDO'
                  Title.Caption = 'Saldo'
                  Visible = True
                end
                item
                  Alignment = taRightJustify
                  Expanded = False
                  FieldName = 'ATZ_TEXTO'
                  Title.Caption = 'Atualizado'
                  Width = 76
                  Visible = True
                end>
            end
          end
          object TabSheet3: TTabSheet
            Caption = 'Prorroga'#231#245'es  '
            ImageIndex = 2
            object DBGrid2: TDBGrid
              Left = 0
              Top = 0
              Width = 1004
              Height = 63
              TabStop = False
              Align = alClient
              DataSource = DsLotesPrr
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Data1'
                  Title.Caption = 'D.Original'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data2'
                  Title.Caption = 'A.Anterior'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Data3'
                  Title.Caption = 'N.Vencto'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DIAS_2_3'
                  Title.Caption = 'Dias'
                  Width = 48
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DIAS_1_3'
                  Title.Caption = 'Acumul.'
                  Width = 48
                  Visible = True
                end>
            end
          end
          object TabSheet6: TTabSheet
            Caption = 'Hist'#243'rico banc'#225'rio (CNAB)'
            ImageIndex = 3
            object DBGrid6: TDBGrid
              Left = 0
              Top = 0
              Width = 1004
              Height = 63
              Align = alClient
              DataSource = DsCNAB240
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'CNAB240L'
                  Title.Caption = 'Ret. ID'
                  Width = 40
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'CNAB240L'
                  Title.Caption = 'Ctrl ID'
                  Width = 44
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Banco'
                  Title.Caption = 'Bco'
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Convenio'
                  Title.Caption = 'Conv'#234'nio'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Sequencia'
                  Title.Caption = 'Seq. ID'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Lote'
                  Width = 27
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Item'
                  Width = 25
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'DataOcor'
                  Title.Caption = 'Data Ocor.'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEENVIO'
                  Title.Caption = 'Envio'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Movimento'
                  Title.Caption = 'Mov.'
                  Width = 30
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEMOVIMENTO'
                  Title.Caption = 'Descri'#231#227'o movimento'
                  Width = 193
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'IRTCLB'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Custas'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValPago'
                  Title.Caption = 'Pago'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'ValCred'
                  Title.Caption = 'Cr'#233'dito'
                  Visible = True
                end>
            end
          end
          object TabSheet7: TTabSheet
            Caption = ' [Tempos] '
            ImageIndex = 4
            object Memo1: TMemo
              Left = 0
              Top = 0
              Width = 1004
              Height = 63
              Align = alClient
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'Courier New'
              Font.Style = []
              ParentFont = False
              ScrollBars = ssBoth
              TabOrder = 0
            end
          end
        end
      end
    end
    object PageControl2: TPageControl
      Left = 0
      Top = 0
      Width = 1016
      Height = 200
      ActivePage = TabSheet4
      Align = alTop
      TabOrder = 1
      object TabSheet4: TTabSheet
        Caption = 'Configura'
        object PainelPesq: TPanel
          Left = 0
          Top = 0
          Width = 1008
          Height = 172
          Align = alClient
          ParentBackground = False
          TabOrder = 0
          object Label75: TLabel
            Left = 4
            Top = 4
            Width = 35
            Height = 13
            Caption = 'Cliente:'
          end
          object Label1: TLabel
            Left = 4
            Top = 44
            Width = 45
            Height = 13
            Caption = 'Duplicata'
          end
          object Label2: TLabel
            Left = 236
            Top = 44
            Width = 40
            Height = 13
            Caption = 'Sacado:'
          end
          object Label4: TLabel
            Left = 104
            Top = 44
            Width = 61
            Height = 13
            Caption = 'CPF sacado:'
          end
          object Label9: TLabel
            Left = 160
            Top = 84
            Width = 44
            Height = 13
            Caption = 'Coligado:'
          end
          object Label55: TLabel
            Left = 412
            Top = 4
            Width = 38
            Height = 13
            Caption = 'Meu ID:'
          end
          object Label18: TLabel
            Left = 296
            Top = 4
            Width = 45
            Height = 13
            Caption = 'Telefone:'
            FocusControl = DBEdit5
          end
          object EdCliente: TdmkEditCB
            Left = 4
            Top = 20
            Width = 37
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdClienteChange
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 44
            Top = 20
            Width = 249
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMECLIENTE'
            ListSource = DsClientes
            TabOrder = 1
            dmkEditCB = EdCliente
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdDuplicata: TdmkEdit
            Left = 4
            Top = 60
            Width = 97
            Height = 21
            Alignment = taCenter
            CharCase = ecUpperCase
            TabOrder = 3
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdDuplicataChange
          end
          object EdEmitente: TdmkEdit
            Left = 236
            Top = 60
            Width = 233
            Height = 21
            TabOrder = 5
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnChange = EdEmitenteChange
          end
          object RGMascara: TRadioGroup
            Left = 6
            Top = 84
            Width = 87
            Height = 82
            Caption = ' M'#225'scara: '
            ItemIndex = 0
            Items.Strings = (
              'Pref. e suf.'
              'Prefixo'
              'Sufixo')
            TabOrder = 18
            OnClick = RGMascaraClick
          end
          object EdCPF: TdmkEdit
            Left = 104
            Top = 60
            Width = 129
            Height = 21
            TabOrder = 4
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdCPFExit
          end
          object TPEIni: TDateTimePicker
            Left = 481
            Top = 20
            Width = 88
            Height = 21
            Date = 38675.714976851900000000
            Time = 38675.714976851900000000
            TabOrder = 7
            OnChange = TPEIniChange
          end
          object TPEFim: TDateTimePicker
            Left = 481
            Top = 60
            Width = 88
            Height = 21
            Date = 38675.714976851900000000
            Time = 38675.714976851900000000
            TabOrder = 9
            OnChange = TPEFimChange
          end
          object CkEIni: TCheckBox
            Left = 482
            Top = 3
            Width = 97
            Height = 17
            Caption = 'Emiss'#227'o inicial:'
            TabOrder = 6
            OnClick = CkEIniClick
          end
          object CkEFim: TCheckBox
            Left = 482
            Top = 43
            Width = 97
            Height = 17
            Caption = 'Emiss'#227'o final:'
            TabOrder = 8
            OnClick = CkEFimClick
          end
          object CkVIni: TCheckBox
            Left = 574
            Top = 3
            Width = 97
            Height = 17
            Caption = 'Vencto inicial:'
            TabOrder = 10
            OnClick = CkVIniClick
          end
          object TPVIni: TDateTimePicker
            Left = 573
            Top = 20
            Width = 88
            Height = 21
            Date = 38675.714976851900000000
            Time = 38675.714976851900000000
            TabOrder = 11
            OnChange = TPVIniChange
          end
          object CkVFim: TCheckBox
            Left = 574
            Top = 43
            Width = 83
            Height = 17
            Caption = 'Vencto final:'
            TabOrder = 12
            OnClick = CkVFimClick
          end
          object TPVFim: TDateTimePicker
            Left = 573
            Top = 60
            Width = 88
            Height = 21
            Date = 38675.714976851900000000
            Time = 38675.714976851900000000
            TabOrder = 13
            OnChange = TPVFimChange
          end
          object EdColigado: TdmkEditCB
            Left = 160
            Top = 100
            Width = 49
            Height = 21
            Alignment = taRightJustify
            TabOrder = 20
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdColigadoChange
            DBLookupComboBox = CBColigado
            IgnoraDBLookupComboBox = False
          end
          object CBColigado: TdmkDBLookupComboBox
            Left = 212
            Top = 100
            Width = 233
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMECLIENTE'
            ListSource = DsColigados
            TabOrder = 21
            dmkEditCB = EdColigado
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object GroupBox1: TGroupBox
            Left = 766
            Top = 1
            Width = 249
            Height = 139
            Caption = ' Totais: '
            Enabled = False
            TabOrder = 28
            object Label3: TLabel
              Left = 4
              Top = 16
              Width = 50
              Height = 13
              Caption = 'Valor total:'
              FocusControl = DBEdit1
            end
            object Label5: TLabel
              Left = 84
              Top = 16
              Width = 57
              Height = 13
              Caption = 'Aberto total:'
              FocusControl = DBEdit2
            end
            object Label7: TLabel
              Left = 164
              Top = 16
              Width = 43
              Height = 13
              Caption = 'Principal:'
              FocusControl = DBEdit4
            end
            object Label6: TLabel
              Left = 4
              Top = 56
              Width = 76
              Height = 13
              Caption = 'd. venc (m'#233'dia):'
              FocusControl = DBEdit3
            end
            object Label8: TLabel
              Left = 84
              Top = 56
              Width = 52
              Height = 13
              Caption = 'Atualizado:'
            end
            object Label10: TLabel
              Left = 164
              Top = 56
              Width = 53
              Height = 13
              Caption = 'Duplicatas:'
            end
            object Label11: TLabel
              Left = 4
              Top = 96
              Width = 47
              Height = 13
              Caption = 'Val.Ocorr:'
            end
            object Label12: TLabel
              Left = 84
              Top = 96
              Width = 66
              Height = 13
              Caption = 'Ocorr.Atualiz.:'
            end
            object Label17: TLabel
              Left = 164
              Top = 96
              Width = 78
              Height = 13
              Caption = 'Total atualizado:'
            end
            object DBEdit1: TDBEdit
              Left = 4
              Top = 32
              Width = 78
              Height = 21
              TabStop = False
              DataField = 'Valor'
              DataSource = DsSoma
              TabOrder = 0
            end
            object DBEdit2: TDBEdit
              Left = 84
              Top = 32
              Width = 78
              Height = 21
              TabStop = False
              DataField = 'SALDO_DESATUALIZADO'
              DataSource = DsSoma
              TabOrder = 1
            end
            object DBEdit4: TDBEdit
              Left = 60
              Top = -4
              Width = 78
              Height = 21
              TabStop = False
              DataField = 'SALDO_DESATUALIZADO'
              DataSource = DsVcto
              TabOrder = 2
              Visible = False
            end
            object DBEdit3: TDBEdit
              Left = 4
              Top = 72
              Width = 78
              Height = 21
              TabStop = False
              DataField = 'PRAZO_MEDIO'
              DataSource = DsVcto
              TabOrder = 3
            end
            object EdAtualizado: TdmkEdit
              Left = 84
              Top = 72
              Width = 78
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdITENS: TdmkEdit
              Left = 164
              Top = 72
              Width = 78
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdOcorrencias: TdmkEdit
              Left = 4
              Top = 112
              Width = 78
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdOcorAtualiz: TdmkEdit
              Left = 84
              Top = 112
              Width = 78
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 7
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdTotalAtualiz: TdmkEdit
              Left = 164
              Top = 112
              Width = 78
              Height = 21
              TabStop = False
              Alignment = taRightJustify
              TabOrder = 8
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object DBEdit6: TDBEdit
              Left = 164
              Top = 32
              Width = 78
              Height = 21
              TabStop = False
              DataField = 'Principal'
              DataSource = DsSoma
              TabOrder = 9
            end
          end
          object CkRepassado: TdmkCheckGroup
            Left = 96
            Top = 84
            Width = 61
            Height = 82
            Caption = ' Repas.: '
            ItemIndex = 0
            Items.Strings = (
              'Sim'
              'N'#227'o')
            TabOrder = 19
            UpdType = utYes
            Value = 1
            OldValor = 0
          end
          object CkStatus: TdmkCheckGroup
            Left = 448
            Top = 82
            Width = 169
            Height = 84
            Caption = ' Status: '
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              'Vencido'
              'Aberto'
              'Quitado'
              'Baixado'
              'Moroso')
            TabOrder = 24
            UpdType = utYes
            Value = 2
            OldValor = 0
          end
          object CkAtualiza: TCheckBox
            Left = 620
            Top = 124
            Width = 93
            Height = 17
            Caption = 'Atualiza saldos.'
            TabOrder = 27
          end
          object CkMorto: TCheckBox
            Left = 620
            Top = 106
            Width = 93
            Height = 17
            Caption = 'Arquivo morto.'
            TabOrder = 26
          end
          object EdControle: TdmkEdit
            Left = 412
            Top = 20
            Width = 57
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfString
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = ''
            ValWarn = False
            OnExit = EdControleExit
          end
          object CkQIni: TCheckBox
            Left = 666
            Top = 3
            Width = 97
            Height = 17
            Caption = 'Quita'#231#227'o inicial:'
            TabOrder = 14
            OnClick = CkVIniClick
          end
          object TPQIni: TDateTimePicker
            Left = 665
            Top = 20
            Width = 88
            Height = 21
            Date = 38675.714976851900000000
            Time = 38675.714976851900000000
            TabOrder = 15
            OnChange = TPVIniChange
          end
          object CkQFim: TCheckBox
            Left = 666
            Top = 43
            Width = 91
            Height = 17
            Caption = 'Quita'#231#227'o final:'
            TabOrder = 16
            OnClick = CkVFimClick
          end
          object TPQFim: TDateTimePicker
            Left = 665
            Top = 60
            Width = 88
            Height = 21
            Date = 38675.714976851900000000
            Time = 38675.714976851900000000
            TabOrder = 17
            OnChange = TPVFimChange
          end
          object DBEdit5: TDBEdit
            Left = 296
            Top = 20
            Width = 113
            Height = 21
            DataField = 'Te1_TXT'
            DataSource = DsClientes
            TabOrder = 29
          end
          object CkOcorrencias: TCheckBox
            Left = 620
            Top = 88
            Width = 93
            Height = 17
            Caption = 'Ocorr'#234'ncias.'
            Checked = True
            State = cbChecked
            TabOrder = 25
          end
          object EdHistorico: TdmkEditCB
            Left = 160
            Top = 145
            Width = 49
            Height = 21
            Alignment = taRightJustify
            TabOrder = 22
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            OnChange = EdHistoricoChange
            DBLookupComboBox = CBHistorico
            IgnoraDBLookupComboBox = False
          end
          object CBHistorico: TdmkDBLookupComboBox
            Left = 212
            Top = 145
            Width = 233
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsOcorDupl
            TabOrder = 23
            dmkEditCB = EdHistorico
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object CkHistorico: TdmkCheckBox
            Left = 160
            Top = 125
            Width = 151
            Height = 17
            Caption = 'Hist'#243'rico:'
            TabOrder = 30
            OnClick = CkHistoricoClick
            UpdType = utYes
            ValCheck = #0
            ValUncheck = #0
            OldValor = #0
          end
        end
      end
      object TabSheet5: TTabSheet
        Caption = 'Imprime'
        ImageIndex = 1
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 1008
          Height = 131
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object RGOrdem1: TRadioGroup
            Left = 0
            Top = 0
            Width = 224
            Height = 131
            Align = alLeft
            Caption = ' Ordem 1: '
            Columns = 2
            ItemIndex = 0
            Items.Strings = (
              'Cliente'
              'Dia Resgate'
              'M'#234's Resgate'
              'Emitente'
              'CPF/CNPJ'
              'Dia Quita'#231#227'o'
              'M'#234's Quita'#231#227'o')
            TabOrder = 0
            OnClick = RGOrdem1Click
          end
          object RGOrdem2: TRadioGroup
            Left = 224
            Top = 0
            Width = 224
            Height = 131
            Align = alLeft
            Caption = ' Ordem 2: '
            Columns = 2
            ItemIndex = 1
            Items.Strings = (
              'Cliente'
              'Dia Resgate'
              'M'#234's Resgate'
              'Emitente'
              'CPF/CNPJ'
              'Dia Quita'#231#227'o'
              'M'#234's Quita'#231#227'o')
            TabOrder = 1
            OnClick = RGOrdem2Click
          end
          object RGOrdem3: TRadioGroup
            Left = 448
            Top = 0
            Width = 224
            Height = 131
            Align = alLeft
            Caption = ' Ordem 3: '
            Columns = 2
            ItemIndex = 2
            Items.Strings = (
              'Cliente'
              'Dia Resgate'
              'M'#234's Resgate'
              'Emitente'
              'CPF/CNPJ'
              'Dia Quita'#231#227'o'
              'M'#234's Quita'#231#227'o')
            TabOrder = 2
            OnClick = RGOrdem3Click
          end
          object RGOrdem4: TRadioGroup
            Left = 672
            Top = 0
            Width = 224
            Height = 131
            Align = alLeft
            Caption = ' Ordem 4: '
            Columns = 2
            ItemIndex = 3
            Items.Strings = (
              'Cliente'
              'Dia Resgate'
              'M'#234's Resgate'
              'Emitente'
              'CPF/CNPJ'
              'Dia Quita'#231#227'o'
              'M'#234's Quita'#231#227'o')
            TabOrder = 3
            OnClick = RGOrdem4Click
          end
          object BtImprime: TBitBtn
            Tag = 5
            Left = 908
            Top = 54
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Imprime'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 4
            OnClick = BtImprimeClick
          end
        end
        object RGAgrupa: TRadioGroup
          Left = 0
          Top = 131
          Width = 1008
          Height = 41
          Align = alBottom
          Caption = ' Agrupamentos: '
          Columns = 4
          ItemIndex = 0
          Items.Strings = (
            '0'
            '1'
            '2'
            '3')
          TabOrder = 1
        end
      end
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrClientesCalcFields
    SQL.Strings = (
      'SELECT '
      'CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, '
      'CASE WHEN Tipo=0 THEN ETe1'
      'ELSE PTe1 END Te1, Codigo'
      'FROM entidades'
      'WHERE Cliente1='#39'V'#39
      'ORDER BY NOMECLIENTE')
    Left = 176
    Top = 64
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrClientesTe1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Te1_TXT'
      Size = 40
      Calculated = True
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 204
    Top = 64
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrPesqAfterOpen
    BeforeClose = QrPesqBeforeClose
    AfterClose = QrPesqAfterClose
    AfterScroll = QrPesqAfterScroll
    OnCalcFields = QrPesqCalcFields
    SQL.Strings = (
      'SELECT MONTH(li.DDeposito)+YEAR(li.DDeposito)*100+0.00 PERIODO,'
      'MONTH(li.Data3)+YEAR(li.Data3)*100+0.00 PERIODO3,'
      ' lo.NF, od.Nome STATUS, li.Controle, li.Duplicata, li.Emissao, '
      
        'li.DCompra, li.Valor, li.DDeposito, li.Emitente, li.CPF,  li.Rep' +
        'assado,'
      
        'lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg, li.V' +
        'alQuit,'
      'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial'
      'ELSE cl.Nome END NOMECLIENTE,'
      
        'li.Vencto, li.Data3, li.TxaCompra, li.Devolucao, li.ProrrVz, li.' +
        'ProrrDd,'
      'li.Banco, li.Agencia, lo.Tipo + 10 LOCALT, '
      
        ' (li.Valor + li.TotalJr - li.TotalDs - li.TotalPg) SALDO_DESATUA' +
        'LIZ, lo.Codigo,'
      'sa.Tel1'
      'FROM lotesits li'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'LEFT JOIN repasits  ri ON ri.Origem=li.Controle'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN sacados sa ON sa.CNPJ = li.CPF '
      'WHERE lo.Tipo=1 ')
    Left = 612
    Top = 272
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lotesits.Controle'
      Required = True
    end
    object QrPesqDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Origin = 'lotesits.Duplicata'
      Required = True
      Size = 12
    end
    object QrPesqDCompra: TDateField
      FieldName = 'DCompra'
      Origin = 'lotesits.DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqValor: TFloatField
      FieldName = 'Valor'
      Origin = 'lotesits.Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesqDDeposito: TDateField
      FieldName = 'DDeposito'
      Origin = 'lotesits.DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lotesits.Emitente'
      Size = 50
    end
    object QrPesqCPF: TWideStringField
      FieldName = 'CPF'
      Origin = 'lotesits.CPF'
      Size = 15
    end
    object QrPesqCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'lotes.Cliente'
    end
    object QrPesqNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrPesqSTATUS: TWideStringField
      FieldName = 'STATUS'
      Origin = 'ocordupl.Nome'
      Size = 50
    end
    object QrPesqQuitado: TIntegerField
      FieldName = 'Quitado'
      Origin = 'lotesits.Quitado'
      Required = True
    end
    object QrPesqTotalJr: TFloatField
      FieldName = 'TotalJr'
      Origin = 'lotesits.TotalJr'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqTotalDs: TFloatField
      FieldName = 'TotalDs'
      Origin = 'lotesits.TotalDs'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqTotalPg: TFloatField
      FieldName = 'TotalPg'
      Origin = 'lotesits.TotalPg'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesqSALDO_DESATUALIZ: TFloatField
      FieldName = 'SALDO_DESATUALIZ'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesqSALDO_ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrPesqNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 30
      Calculated = True
    end
    object QrPesqVencto: TDateField
      FieldName = 'Vencto'
      Origin = 'lotesits.Vencto'
      Required = True
    end
    object QrPesqDDCALCJURO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DDCALCJURO'
      Calculated = True
    end
    object QrPesqData3: TDateField
      FieldName = 'Data3'
      Origin = 'lotesits.Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 40
      Calculated = True
    end
    object QrPesqTAXA_COMPRA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TAXA_COMPRA'
      Calculated = True
    end
    object QrPesqTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Origin = 'lotesits.TxaCompra'
      Required = True
    end
    object QrPesqDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Origin = 'lotesits.Devolucao'
      Required = True
    end
    object QrPesqProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Origin = 'lotesits.ProrrVz'
      Required = True
    end
    object QrPesqProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Origin = 'lotesits.ProrrDd'
      Required = True
    end
    object QrPesqEmissao: TDateField
      FieldName = 'Emissao'
      Origin = 'lotesits.Emissao'
      Required = True
    end
    object QrPesqRepassado: TSmallintField
      FieldName = 'Repassado'
      Origin = 'lotesits.Repassado'
      Required = True
    end
    object QrPesqValQuit: TFloatField
      FieldName = 'ValQuit'
      Origin = 'lotesits.ValQuit'
      Required = True
    end
    object QrPesqNF: TIntegerField
      FieldName = 'NF'
      Origin = 'lotes.NF'
      DisplayFormat = '000000;-000000; '
    end
    object QrPesqBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lotesits.Banco'
      Required = True
    end
    object QrPesqAgencia: TIntegerField
      FieldName = 'Agencia'
      Origin = 'lotesits.Agencia'
      Required = True
    end
    object QrPesqPERIODO: TFloatField
      FieldName = 'PERIODO'
    end
    object QrPesqOCORRENCIAS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'OCORRENCIAS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesqOCORATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'OCORATUALIZ'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesqTOTALATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALATUALIZ'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesqSALDO_TEXTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SALDO_TEXTO'
      Size = 30
      Calculated = True
    end
    object QrPesqLOCALT: TLargeintField
      FieldName = 'LOCALT'
    end
    object QrPesqNOMELOCAL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMELOCAL'
      Size = 5
      Calculated = True
    end
    object QrPesqPERIODO3: TFloatField
      FieldName = 'PERIODO3'
    end
    object QrPesqDATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Size = 10
      Calculated = True
    end
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'lotes.Codigo'
    end
    object QrPesqTel1: TWideStringField
      FieldName = 'Tel1'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 640
    Top = 272
  end
  object QrSumPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ad.Juros) Juros, SUM(ad.Desco) Desco, '
      'SUM(ad.Pago) Pago, li.Valor, MAX(ad.Data) MaxData'
      'FROM aduppgs ad'
      'LEFT JOIN lotesits li ON li.Controle=ad.LotesIts'
      'WHERE ad.LotesIts=:P0'
      'GROUP BY ad.LotesIts')
    Left = 552
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPgJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumPgPago: TFloatField
      FieldName = 'Pago'
    end
    object QrSumPgDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrSumPgValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSumPgMaxData: TDateField
      FieldName = 'MaxData'
    end
  end
  object QrLocPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM aduppgs'
      'WHERE Data=('
      'SELECT Max(Data) FROM aduppgs'
      'WHERE LotesIts=:P0)'
      'ORDER BY Controle DESC')
    Left = 584
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPgData: TDateField
      FieldName = 'Data'
    end
    object QrLocPgJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrLocPgPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLocPgLotePg: TIntegerField
      FieldName = 'LotePg'
    end
    object QrLocPgLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocPgDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocPgDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocPgUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocPgUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLocPgLotesIts: TIntegerField
      FieldName = 'LotesIts'
    end
    object QrLocPgControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocPgDesco: TFloatField
      FieldName = 'Desco'
    end
  end
  object QrADupIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT od.Nome NOMESTATUS, ad.* '
      'FROM adupits ad'
      'LEFT JOIN ocordupl od ON od.Codigo=ad.Alinea'
      'WHERE ad.LotesIts=:P0'
      'ORDER BY ad.DataA')
    Left = 612
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrADupItsAlinea: TIntegerField
      FieldName = 'Alinea'
    end
    object QrADupItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrADupItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrADupItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrADupItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrADupItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrADupItsLotesIts: TIntegerField
      FieldName = 'LotesIts'
    end
    object QrADupItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrADupItsDataA: TDateField
      FieldName = 'DataA'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrADupItsNOMESTATUS: TWideStringField
      FieldName = 'NOMESTATUS'
      Size = 50
    end
  end
  object DsADupIts: TDataSource
    DataSet = QrADupIts
    Left = 640
    Top = 300
  end
  object DsADupPgs: TDataSource
    DataSet = QrADupPgs
    Left = 640
    Top = 328
  end
  object QrADupPgs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT adp.*, lot.Lote'
      'FROM aduppgs adp'
      'LEFT JOIN lotes lot ON lot.Codigo=adp.LotePg'
      'WHERE adp.LotesIts=:P0'
      'ORDER BY adp.Data, adp.Controle')
    Left = 612
    Top = 328
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrADupPgsData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrADupPgsJuros: TFloatField
      FieldName = 'Juros'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrADupPgsPago: TFloatField
      FieldName = 'Pago'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrADupPgsLotePg: TIntegerField
      FieldName = 'LotePg'
      DisplayFormat = '0;-0; '
    end
    object QrADupPgsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrADupPgsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrADupPgsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrADupPgsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrADupPgsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrADupPgsLotesIts: TIntegerField
      FieldName = 'LotesIts'
    end
    object QrADupPgsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrADupPgsDesco: TFloatField
      FieldName = 'Desco'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrADupPgsLote: TIntegerField
      FieldName = 'Lote'
    end
  end
  object QrPos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ad.Alinea '
      'FROM adupits ad'
      'WHERE ad.LotesIts=:P0'
      'ORDER BY ad.DataA DESC, Controle DESC')
    Left = 316
    Top = 260
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPosAlinea: TIntegerField
      FieldName = 'Alinea'
      Required = True
    end
  end
  object PMStatus: TPopupMenu
    OnPopup = PMStatusPopup
    Left = 32
    Top = 452
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui1Click
    end
  end
  object PMPagto: TPopupMenu
    OnPopup = PMPagtoPopup
    Left = 112
    Top = 448
    object Inclui2: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui2Click
    end
    object Exclui2: TMenuItem
      Caption = '&Exclui'
      OnClick = Exclui2Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Recibo1: TMenuItem
      Caption = '&Recibo'
      OnClick = Recibo1Click
    end
  end
  object PMStatusManual: TPopupMenu
    OnPopup = PMStatusManualPopup
    Left = 316
    Top = 476
    object Incluiprorrogao1: TMenuItem
      Caption = '&Inclui prorroga'#231#227'o'
      OnClick = Incluiprorrogao1Click
    end
    object ExcluiProrrogao1: TMenuItem
      Caption = '&Exclui Prorroga'#231#227'o'
      OnClick = ExcluiProrrogao1Click
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N0Statusautomtico1: TMenuItem
      Caption = '&0. For'#231'a status Autom'#225'tico'
      OnClick = N0Statusautomtico1Click
    end
    object N1Forastatusprorrogado1: TMenuItem
      Caption = '&1. For'#231'a status Prorrogado'
      OnClick = N1Forastatusprorrogado1Click
    end
    object N2ForastatusBaixado1: TMenuItem
      Caption = '&2. For'#231'a status Baixado'
      OnClick = N2ForastatusBaixado1Click
    end
    object N3ForastatusMoroso1: TMenuItem
      Caption = '&3. For'#231'a status Moroso'
      OnClick = N3ForastatusMoroso1Click
    end
  end
  object PMOcorreu: TPopupMenu
    OnPopup = PMOcorreuPopup
    Left = 420
    Top = 468
    object Incluiocorrncia1: TMenuItem
      Caption = '&Inclui ocorr'#234'ncia'
      OnClick = Incluiocorrncia1Click
    end
    object Alteraocorrncia1: TMenuItem
      Caption = '&Altera ocorr'#234'ncia'
      OnClick = Alteraocorrncia1Click
    end
    object Excluiocorrncia1: TMenuItem
      Caption = '&Exclui ocorr'#234'ncia'
      OnClick = Excluiocorrncia1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object IncluiPagamento1: TMenuItem
      Caption = 'Inclui &Pagamento'
      OnClick = IncluiPagamento1Click
    end
    object Excluipagamento1: TMenuItem
      Caption = 'E&xclui pagamento'
      OnClick = Excluipagamento1Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Recibodepagamento1: TMenuItem
      Caption = '&Recibo de pagamento'
      OnClick = Recibodepagamento1Click
    end
  end
  object QrOcorreu: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrOcorreuBeforeClose
    AfterScroll = QrOcorreuAfterScroll
    OnCalcFields = QrOcorreuCalcFields
    SQL.Strings = (
      'SELECT  lo.Cliente CLIENTELOTE, ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.LotesIts=:P0'
      '')
    Left = 408
    Top = 272
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorreuNOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcorreuCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOcorreuLotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrOcorreuDataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorreuOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrOcorreuValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreuLoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrOcorreuLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorreuDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorreuDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorreuUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorreuUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorreuTaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrOcorreuTaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
    end
    object QrOcorreuPago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorreuDataP: TDateField
      FieldName = 'DataP'
      Required = True
    end
    object QrOcorreuTaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrOcorreuATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreuCLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
    object QrOcorreuData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrOcorreuStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrOcorreuCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOcorreuSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcorreuATZ_TEXTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ATZ_TEXTO'
      Size = 30
      Calculated = True
    end
    object QrOcorreuSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
  end
  object DsOcorreu: TDataSource
    DataSet = QrOcorreu
    Left = 436
    Top = 272
  end
  object QrLotesPrr: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLotesPrrCalcFields
    SQL.Strings = (
      'SELECT * FROM lotesprr'
      'WHERE Codigo=:P0'
      'ORDER BY Controle')
    Left = 612
    Top = 356
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLotesPrrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLotesPrrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLotesPrrData1: TDateField
      FieldName = 'Data1'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotesPrrData2: TDateField
      FieldName = 'Data2'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotesPrrData3: TDateField
      FieldName = 'Data3'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrLotesPrrOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
    end
    object QrLotesPrrLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLotesPrrDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLotesPrrDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLotesPrrUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLotesPrrUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLotesPrrDIAS_1_3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DIAS_1_3'
      Calculated = True
    end
    object QrLotesPrrDIAS_2_3: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DIAS_2_3'
      Calculated = True
    end
  end
  object DsLotesPrr: TDataSource
    DataSet = QrLotesPrr
    Left = 640
    Top = 356
  end
  object QrOcorBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ocorbank'
      'ORDER BY Nome')
    Left = 780
    Top = 280
    object QrOcorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrOcorBankLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorBankDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorBankDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorBankUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorBankUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorBankBase: TFloatField
      FieldName = 'Base'
    end
  end
  object DsOcorBank: TDataSource
    DataSet = QrOcorBank
    Left = 808
    Top = 280
  end
  object QrLocPr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM lotesprr'
      'WHERE Data3=('
      'SELECT Max(Data3) FROM lotesprr'
      'WHERE Codigo=:P0)'
      'ORDER BY Controle DESC')
    Left = 784
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPrCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocPrControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocPrData1: TDateField
      FieldName = 'Data1'
    end
    object QrLocPrData2: TDateField
      FieldName = 'Data2'
    end
    object QrLocPrData3: TDateField
      FieldName = 'Data3'
    end
    object QrLocPrOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
    end
    object QrLocPrLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocPrDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocPrDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocPrUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocPrUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor, SUM(Valor-TotalPg) Principal, '
      'SUM(Valor + TotalJr - TotalDs - TotalPg) SALDO_DESATUALIZADO'
      'FROM lotesits li'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'WHERE lo.Tipo=1 ')
    Left = 752
    Top = 356
    object QrSomaValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSomaSALDO_DESATUALIZADO: TFloatField
      FieldName = 'SALDO_DESATUALIZADO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrSomaPrincipal: TFloatField
      FieldName = 'Principal'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSoma: TDataSource
    DataSet = QrSoma
    Left = 780
    Top = 356
  end
  object QrVcto: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrVctoCalcFields
    SQL.Strings = (
      'SELECT SUM(Valor) SOMA_VALOR, '
      'SUM(Valor + TotalJr - TotalDs - TotalPg) SALDO_DESATUALIZADO,'
      'SUM(CURDATE()-Vencto) SOMA_DIAS, '
      'SUM((CURDATE()-Vencto)*Valor) FATOR_VD'
      'FROM lotesits li'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'WHERE lo.Tipo=1 '
      'AND li.Vencto<CURDATE()')
    Left = 752
    Top = 328
    object QrVctoSOMA_VALOR: TFloatField
      FieldName = 'SOMA_VALOR'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVctoSALDO_DESATUALIZADO: TFloatField
      FieldName = 'SALDO_DESATUALIZADO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVctoSOMA_DIAS: TFloatField
      FieldName = 'SOMA_DIAS'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVctoFATOR_VD: TFloatField
      FieldName = 'FATOR_VD'
      DisplayFormat = '#,###,##0.00'
    end
    object QrVctoPRAZO_MEDIO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRAZO_MEDIO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrVctoJURO_CLI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'JURO_CLI'
      DisplayFormat = '#,###,##0.000000'
      Calculated = True
    end
  end
  object DsVcto: TDataSource
    DataSet = QrVcto
    Left = 780
    Top = 328
  end
  object QrSP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT od.Nome STATUS, li.Controle, li.Duplicata, li.Emissao, '
      'li.DCompra, li.Valor, li.DDeposito, li.Emitente, li.CPF, '
      'lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg,'
      'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial'
      'ELSE cl.Nome END NOMECLIENTE,'
      
        'li.Vencto, li.Data3, li.TxaCompra, li.Devolucao, li.ProrrVz, li.' +
        'ProrrDd'
      'FROM lotesits li'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'WHERE lo.Tipo=1 ')
    Left = 696
    Top = 300
    object QrSPSALDO_DESATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_DESATUALIZ'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSPSALDO_ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSPSTATUS: TWideStringField
      FieldName = 'STATUS'
      Size = 50
    end
    object QrSPControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrSPDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrSPEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrSPDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
    end
    object QrSPValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrSPDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
    end
    object QrSPEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrSPCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrSPCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSPQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrSPTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrSPTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrSPTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrSPNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrSPVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrSPData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrSPTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrSPDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrSPProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrSPProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
  end
  object QrLocOc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ocorrpg'
      'WHERE Data=('
      'SELECT Max(Data) FROM ocorrpg'
      'WHERE Ocorreu=:P0)'
      'ORDER BY Codigo DESC')
    Left = 344
    Top = 372
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocOcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocOcOcorreu: TIntegerField
      FieldName = 'Ocorreu'
    end
    object QrLocOcData: TDateField
      FieldName = 'Data'
    end
    object QrLocOcJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrLocOcPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLocOcLotePg: TIntegerField
      FieldName = 'LotePg'
    end
    object QrLocOcLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocOcDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocOcDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocOcUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocOcUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object QrSumOc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Juros) Juros, SUM(Pago) Pago'
      'FROM ocorrpg'
      'WHERE Ocorreu=:P0')
    Left = 368
    Top = 372
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumOcJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumOcPago: TFloatField
      FieldName = 'Pago'
    end
  end
  object QrLastOcor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Codigo'
      'FROM ocorrpg'
      'WHERE Ocorreu=:P0'
      'ORDER BY Data Desc, Codigo Desc')
    Left = 396
    Top = 372
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLastOcorData: TDateField
      FieldName = 'Data'
    end
    object QrLastOcorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrOcorrPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ocorrpg'
      'WHERE Ocorreu =:P0')
    Left = 440
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcorrPgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorrPgOcorreu: TIntegerField
      FieldName = 'Ocorreu'
    end
    object QrOcorrPgData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorrPgJuros: TFloatField
      FieldName = 'Juros'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorrPgPago: TFloatField
      FieldName = 'Pago'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorrPgLotePg: TIntegerField
      FieldName = 'LotePg'
      DisplayFormat = '0000'
    end
    object QrOcorrPgLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorrPgDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorrPgDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorrPgUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorrPgUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsOcorrPg: TDataSource
    DataSet = QrOcorrPg
    Left = 468
    Top = 376
  end
  object PMQuitacao: TPopupMenu
    OnPopup = PMQuitacaoPopup
    Left = 508
    Top = 464
    object Quitadocumento1: TMenuItem
      Caption = '&Quita documento'
      OnClick = Quitadocumento1Click
    end
    object Imprimerecibodequitao1: TMenuItem
      Caption = '&Imprime recibo de quita'#231#227'o'
      OnClick = Imprimerecibodequitao1Click
    end
    object ImprimecartadeCancelamentodeProtesto1: TMenuItem
      Caption = 'Imprime carta de Cancelamento de &Protesto'
      OnClick = ImprimecartadeCancelamentodeProtesto1Click
    end
  end
  object QrPesq2: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPesq2CalcFields
    SQL.Strings = (
      'SELECT MONTH(li.DDeposito)+YEAR(li.DDeposito)*100+0.00 PERIODO,'
      'MONTH(li.Data3)+YEAR(li.Data3)*100+0.00 PERIODO3,'
      ' lo.NF, od.Nome STATUS, li.Controle, li.Duplicata, li.Emissao, '
      
        'li.DCompra, li.Valor, li.DDeposito, li.Emitente, li.CPF,  li.Rep' +
        'assado,'
      
        'lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg, li.V' +
        'alQuit,'
      'CASE WHEN cl.Tipo=0 THEN cl.RazaoSocial'
      'ELSE cl.Nome END NOMECLIENTE,'
      
        'li.Vencto, li.Data3, li.TxaCompra, li.Devolucao, li.ProrrVz, li.' +
        'ProrrDd,'
      'li.Banco, li.Agencia, sa.Tel1'
      'FROM lotesits li'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN entidades cl ON cl.Codigo=lo.Cliente'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'LEFT JOIN repasits  ri ON ri.Origem=li.Controle'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN sacados sa ON sa.CNPJ = li.CPF '
      'WHERE lo.Tipo=1 ')
    Left = 188
    Top = 8
    object QrPesq2Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPesq2Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrPesq2DCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq2Valor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesq2DDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesq2Emitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrPesq2CPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrPesq2Cliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrPesq2NOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrPesq2STATUS: TWideStringField
      FieldName = 'STATUS'
      Size = 50
    end
    object QrPesq2Quitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrPesq2TotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesq2TotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesq2TotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPesq2SALDO_DESATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_DESATUALIZ'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrPesq2SALDO_ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrPesq2NOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 30
      Calculated = True
    end
    object QrPesq2Vencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrPesq2DDCALCJURO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DDCALCJURO'
      Calculated = True
    end
    object QrPesq2Data3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrPesq2CNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 40
      Calculated = True
    end
    object QrPesq2TAXA_COMPRA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TAXA_COMPRA'
      Calculated = True
    end
    object QrPesq2TxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrPesq2Devolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrPesq2ProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrPesq2ProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrPesq2Emissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrPesq2Repassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrPesq2ValQuit: TFloatField
      FieldName = 'ValQuit'
      Required = True
    end
    object QrPesq2NF: TIntegerField
      FieldName = 'NF'
      DisplayFormat = '000000;-000000; '
    end
    object QrPesq2Banco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrPesq2Agencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrPesq2CONTA: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CONTA'
      Calculated = True
    end
    object QrPesq2PERIODO: TFloatField
      FieldName = 'PERIODO'
    end
    object QrPesq2Valor_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Valor_TXT'
      Calculated = True
    end
    object QrPesq2DDeposito_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DDeposito_TXT'
      Calculated = True
    end
    object QrPesq2MES_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES_TXT'
      Size = 30
      Calculated = True
    end
    object QrPesq2TOTALATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALATUALIZ'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesq2OCORATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'OCORATUALIZ'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesq2OCORRENCIAS: TFloatField
      FieldKind = fkCalculated
      FieldName = 'OCORRENCIAS'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrPesq2ATZ_TEXTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ATZ_TEXTO'
      Size = 30
      Calculated = True
    end
    object QrPesq2DATA3TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3TXT'
      Size = 12
      Calculated = True
    end
    object QrPesq2PERIODO3: TFloatField
      FieldName = 'PERIODO3'
    end
    object QrPesq2MEQ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MEQ_TXT'
      Size = 30
      Calculated = True
    end
    object QrPesq2Tel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrPesq2TEL1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL1_TXT'
      Size = 30
      Calculated = True
    end
  end
  object QrOcu1: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOcu1CalcFields
    SQL.Strings = (
      'SELECT  lo.Cliente CLIENTELOTE, ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.LotesIts=:P0'
      '')
    Left = 724
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcu1NOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcu1Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOcu1LotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrOcu1DataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcu1Ocorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrOcu1Valor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcu1LoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrOcu1Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcu1DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcu1DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcu1UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcu1UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcu1TaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrOcu1TaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
    end
    object QrOcu1Pago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcu1DataP: TDateField
      FieldName = 'DataP'
      Required = True
    end
    object QrOcu1TaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrOcu1ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcu1CLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
    object QrOcu1Data3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrOcu1Status: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrOcu1Cliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOcu1SALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object QrOcu2: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrOcu2CalcFields
    SQL.Strings = (
      'SELECT  lo.Cliente CLIENTELOTE, ob.Nome NOMEOCORRENCIA, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.LotesIts=:P0'
      '')
    Left = 752
    Top = 300
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrOcu2NOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcu2Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOcu2LotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrOcu2DataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcu2Ocorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrOcu2Valor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcu2LoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrOcu2Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcu2DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcu2DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcu2UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcu2UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcu2TaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrOcu2TaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
    end
    object QrOcu2Pago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcu2DataP: TDateField
      FieldName = 'DataP'
      Required = True
    end
    object QrOcu2TaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrOcu2ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrOcu2CLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
    object QrOcu2Data3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrOcu2Status: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrOcu2Cliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOcu2SALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object QrColigados: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo'
      'FROM entidades'
      'WHERE Fornece2='#39'V'#39
      'ORDER BY NOMECLIENTE')
    Left = 176
    Top = 96
    object QrColigadosNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrColigadosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsColigados: TDataSource
    DataSet = QrColigados
    Left = 204
    Top = 96
  end
  object QrCNAB240: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCNAB240AfterOpen
    OnCalcFields = QrCNAB240CalcFields
    SQL.Strings = (
      'SELECT * FROM cnab240'
      'WHERE Controle=:P0'
      'ORDER BY DataOcor, CNAB240I')
    Left = 364
    Top = 440
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB240CNAB240L: TIntegerField
      FieldName = 'CNAB240L'
    end
    object QrCNAB240CNAB240I: TIntegerField
      FieldName = 'CNAB240I'
    end
    object QrCNAB240Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCNAB240Banco: TIntegerField
      FieldName = 'Banco'
      DisplayFormat = '000'
    end
    object QrCNAB240Sequencia: TIntegerField
      FieldName = 'Sequencia'
    end
    object QrCNAB240Convenio: TWideStringField
      FieldName = 'Convenio'
    end
    object QrCNAB240ConfigBB: TIntegerField
      FieldName = 'ConfigBB'
    end
    object QrCNAB240Lote: TIntegerField
      FieldName = 'Lote'
    end
    object QrCNAB240Item: TIntegerField
      FieldName = 'Item'
    end
    object QrCNAB240DataOcor: TDateField
      FieldName = 'DataOcor'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCNAB240Envio: TSmallintField
      FieldName = 'Envio'
    end
    object QrCNAB240Movimento: TSmallintField
      FieldName = 'Movimento'
    end
    object QrCNAB240Custas: TFloatField
      FieldName = 'Custas'
      DisplayFormat = '###,###,##0.00'
    end
    object QrCNAB240ValPago: TFloatField
      FieldName = 'ValPago'
      DisplayFormat = '###,###,##0.00'
    end
    object QrCNAB240ValCred: TFloatField
      FieldName = 'ValCred'
      DisplayFormat = '###,###,##0.00'
    end
    object QrCNAB240Acao: TIntegerField
      FieldName = 'Acao'
    end
    object QrCNAB240Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCNAB240DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCNAB240DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCNAB240UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCNAB240UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCNAB240NOMEENVIO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEENVIO'
      Size = 10
      Calculated = True
    end
    object QrCNAB240NOMEMOVIMENTO: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEMOVIMENTO'
      Size = 100
      Calculated = True
    end
    object QrCNAB240IRTCLB: TWideStringField
      FieldName = 'IRTCLB'
      Size = 10
    end
  end
  object DsCNAB240: TDataSource
    DataSet = QrCNAB240
    Left = 392
    Top = 440
  end
  object QrSacados: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sac.Numero+0.000 Numero, sac.* '
      'FROM sacados sac'
      'WHERE CNPJ=:P0')
    Left = 56
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacadosNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrSacadosCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 15
    end
    object QrSacadosIE: TWideStringField
      FieldName = 'IE'
      Required = True
      Size = 25
    end
    object QrSacadosNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrSacadosRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrSacadosCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrSacadosBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrSacadosCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrSacadosUF: TWideStringField
      FieldName = 'UF'
      Required = True
      Size = 2
    end
    object QrSacadosCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrSacadosTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrSacadosRisco: TFloatField
      FieldName = 'Risco'
      Required = True
    end
    object QrSacadosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrSacadosEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
  end
  object PMHistCNAB: TPopupMenu
    OnPopup = PMHistCNABPopup
    Left = 604
    Top = 468
    object Excluihistriciatual1: TMenuItem
      Caption = 'Exclui item de hist'#243'rico atual'
      OnClick = Excluihistriciatual1Click
    end
    object Excluitodositensdehistricosdestaduplicata1: TMenuItem
      Caption = 'Exclui todo hist'#243'rico desta duplicata'
      OnClick = Excluitodositensdehistricosdestaduplicata1Click
    end
  end
  object frxPesq1: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.452973449100000000
    ReportOptions.LastChange = 39718.452973449100000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <VFR_ORD1> > 0 then'
      '  GH1.Visible := True else GH1.Visible := False;'
      '  if <VFR_ORD1> > 0 then'
      '  GF1.Visible := True else GF1.Visible := False;'
      '  //'
      
        '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsPesq2."NOMECLIEN' +
        'TE"'#39';'
      
        '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsPesq2."DDeposito' +
        '_TXT"'#39';'
      
        '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsPesq2."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD1> = 4 then GH1.Condition := '#39'frxDsPesq2."Emitente"' +
        #39';'
      
        '  if <VFR_ORD1> = 5 then GH1.Condition := '#39'frxDsPesq2."CNPJ_TXT"' +
        #39';'
      '  if <VFR_ORD1> = 6 then GH1.Condition := '#39'frxDsPesq2."Data3"'#39';'
      
        '  if <VFR_ORD1> = 7 then GH1.Condition := '#39'frxDsPesq2."PERIODO3"' +
        #39';'
      ''
      ''
      '  if <VFR_ORD2> > 0 then'
      '  GH2.Visible := True else GH2.Visible := False;'
      '  if <VFR_ORD2> > 0 then'
      '  GF2.Visible := True else GF2.Visible := False;'
      '  //'
      
        '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsPesq2."NOMECLIEN' +
        'TE"'#39';'
      
        '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsPesq2."DDeposito' +
        '_TXT"'#39';'
      
        '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsPesq2."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD2> = 4 then GH2.Condition := '#39'frxDsPesq2."Emitente"' +
        #39';'
      
        '  if <VFR_ORD2> = 5 then GH2.Condition := '#39'frxDsPesq2."CNPJ_TXT"' +
        #39';'
      '  if <VFR_ORD2> = 6 then GH2.Condition := '#39'frxDsPesq2."Data3"'#39';'
      
        '  if <VFR_ORD2> = 7 then GH2.Condition := '#39'frxDsPesq2."PERIODO3"' +
        #39';'
      ''
      ''
      '  if <VFR_ORD3> > 0 then'
      '  GH3.Visible := True else GH3.Visible := False;'
      '  if <VFR_ORD3> > 0 then'
      '  GF3.Visible := True else GF3.Visible := False;'
      '  //'
      
        '  if <VFR_ORD3> = 1 then GH3.Condition := '#39'frxDsPesq2."NOMECLIEN' +
        'TE"'#39';'
      
        '  if <VFR_ORD3> = 2 then GH3.Condition := '#39'frxDsPesq2."DDeposito' +
        '_TXT"'#39';'
      
        '  if <VFR_ORD3> = 3 then GH3.Condition := '#39'frxDsPesq2."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD3> = 4 then GH3.Condition := '#39'frxDsPesq2."Emitente"' +
        #39';'
      
        '  if <VFR_ORD3> = 5 then GH3.Condition := '#39'frxDsPesq2."CNPJ_TXT"' +
        #39';'
      '  if <VFR_ORD3> = 6 then GH3.Condition := '#39'frxDsPesq2."Data3"'#39';'
      
        '  if <VFR_ORD3> = 7 then GH3.Condition := '#39'frxDsPesq2."PERIODO3"' +
        #39';'
      '  '
      'end.')
    OnGetValue = frxPesq1GetValue
    Left = 244
    Top = 8
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPesq2
        DataSetName = 'frxDsPesq2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Memo32: TfrxMemoView
          Left = 554.000000000000000000
          Top = 4.661410000000000000
          Width = 164.000000000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 2.000000000000000000
          Top = 20.661410000000000000
          Width = 716.000000000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 661.417750000000000000
        Width = 718.110700000000000000
        object Memo31: TfrxMemoView
          Left = 582.000000000000000000
          Top = 7.653059999999982000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 670.000000000000000000
          Top = 7.653059999999982000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TotalPages#]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 88.070810000000000000
        Top = 98.267780000000000000
        Width = 718.110700000000000000
        object Memo19: TfrxMemoView
          Left = 2.000000000000000000
          Top = 1.070809999999995000
          Width = 700.000000000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE DUPLICATAS [VARF_SITUACOES]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 2.000000000000000000
          Top = 25.070809999999990000
          Width = 448.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_TODOS]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 514.000000000000000000
          Top = 25.070809999999990000
          Width = 188.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 466.000000000000000000
          Top = 25.070809999999990000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Top = 71.070810000000010000
          Width = 26.456692910000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bco.')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 26.456692913385800000
          Top = 71.070810000000010000
          Width = 30.236220472440900000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag.')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 56.692913385826800000
          Top = 71.070810000000010000
          Width = 56.692913385826800000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Duplicata')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 154.960629920000000000
          Top = 71.070810000000010000
          Width = 185.196850393701000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sacado')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 340.157480310000000000
          Top = 71.070810000000010000
          Width = 98.267716535433100000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 536.692913385827000000
          Top = 71.070810000000010000
          Width = 60.472440944881890000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor Face')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 438.425196850394000000
          Top = 71.070810000000010000
          Width = 49.133858267716500000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 487.559055118110000000
          Top = 71.070810000000010000
          Width = 49.133858267716500000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Quita'#231#227'o')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 2.000000000000000000
          Top = 47.070810000000010000
          Width = 692.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_FILTROS]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 113.385826771654000000
          Top = 71.070810000000010000
          Width = 41.574803149606300000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 597.165354330709000000
          Top = 71.070810000000010000
          Width = 60.472440944881890000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor Pago')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 657.637795275591000000
          Top = 71.070810000000010000
          Width = 60.472440944881890000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Saldo Atz')
          ParentFont = False
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        Height = 15.000000000000000000
        Top = 370.393940000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPesq2
        DataSetName = 'frxDsPesq2'
        RowCount = 0
        object Memo2: TfrxMemoView
          Width = 26.456692910000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsPesq2."Banco">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456692910000000000
          Width = 30.236220470000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'0000'#39', <frxDsPesq2."Agencia">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 56.692913385826800000
          Width = 56.692913385826800000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq2."Duplicata"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 154.960629920000000000
          Width = 185.196850393701000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq2."Emitente"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 340.157480310000000000
          Width = 98.267716535433100000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq2."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 536.692913390000000000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."Valor"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 438.425196850000000000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq2."DDeposito"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 487.559055118110000000
          Width = 49.133858267716500000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq2."DATA3TXT"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 113.385826770000000000
          Width = 41.574803150000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsPesq2."NF">)]'
            '')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 597.165354330709000000
          Width = 60.472440944881890000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."TotalPg"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 657.637795275591000000
          Width = 60.472440944881890000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."SALDO_ATUALIZADO"]')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 245.669450000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsPesq2."NOMECLIENTE"'
        object Memo29: TfrxMemoView
          Left = 18.000000000000000000
          Width = 692.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 287.244280000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsPesq2."DDeposito"'
        object Memo30: TfrxMemoView
          Left = 38.000000000000000000
          Width = 672.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GH3: TfrxGroupHeader
        FillType = ftBrush
        Height = 17.519480000000000000
        Top = 328.819110000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GH3OnBeforePrint'
        Condition = 'frxDsPesq2."PERIODO"'
        object Memo33: TfrxMemoView
          Left = 58.000000000000000000
          Width = 652.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA3NOME]')
          ParentFont = False
        end
      end
      object GF3: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.000000000000000000
        Top = 408.189240000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GF3OnBeforePrint'
        object Memo34: TfrxMemoView
          Top = 4.724179999999990000
          Width = 536.692913390000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 657.637795280000000000
          Top = 4.724179999999990000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 536.692913390000000000
          Top = 4.724179999999990000
          Width = 60.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Valor">)]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 597.165354330000000000
          Top = 4.724179999999990000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.000000000000000000
        Top = 453.543600000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo37: TfrxMemoView
          Top = 3.779530000000022000
          Width = 536.692913390000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 657.637795280000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 536.692913390000000000
          Top = 3.779530000000022000
          Width = 60.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Valor">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 597.165354330000000000
          Top = 3.779530000000022000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 43.440630000000000000
        Top = 498.897960000000000000
        Width = 718.110700000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo42: TfrxMemoView
          Top = 7.559059999999988000
          Width = 536.692913390000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 657.637795280000000000
          Top = 7.559059999999988000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 536.692913390000000000
          Top = 7.559059999999988000
          Width = 60.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Valor">)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 597.165354330000000000
          Top = 7.559059999999988000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 32.000000000000000000
        Top = 604.724800000000000000
        Width = 718.110700000000000000
        object Memo46: TfrxMemoView
          Top = 7.559060000000045000
          Width = 536.692913390000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 657.637795280000000000
          Top = 7.559060000000045000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 536.692913390000000000
          Top = 7.559060000000045000
          Width = 60.000000000000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Valor">)]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 597.165354330000000000
          Top = 7.559060000000045000
          Width = 60.472440940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Top = 3.779530000000022000
          Width = 720.000000000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
      end
    end
  end
  object frxDsPesq2: TfrxDBDataset
    UserName = 'frxDsPesq2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Controle=Controle'
      'Duplicata=Duplicata'
      'DCompra=DCompra'
      'Valor=Valor'
      'DDeposito=DDeposito'
      'Emitente=Emitente'
      'CPF=CPF'
      'Cliente=Cliente'
      'NOMECLIENTE=NOMECLIENTE'
      'STATUS=STATUS'
      'Quitado=Quitado'
      'TotalJr=TotalJr'
      'TotalDs=TotalDs'
      'TotalPg=TotalPg'
      'SALDO_DESATUALIZ=SALDO_DESATUALIZ'
      'SALDO_ATUALIZADO=SALDO_ATUALIZADO'
      'NOMESTATUS=NOMESTATUS'
      'Vencto=Vencto'
      'DDCALCJURO=DDCALCJURO'
      'Data3=Data3'
      'CNPJ_TXT=CNPJ_TXT'
      'TAXA_COMPRA=TAXA_COMPRA'
      'TxaCompra=TxaCompra'
      'Devolucao=Devolucao'
      'ProrrVz=ProrrVz'
      'ProrrDd=ProrrDd'
      'Emissao=Emissao'
      'Repassado=Repassado'
      'ValQuit=ValQuit'
      'NF=NF'
      'Banco=Banco'
      'Agencia=Agencia'
      'CONTA=CONTA'
      'PERIODO=PERIODO'
      'Valor_TXT=Valor_TXT'
      'DDeposito_TXT=DDeposito_TXT'
      'MES_TXT=MES_TXT'
      'TOTALATUALIZ=TOTALATUALIZ'
      'OCORATUALIZ=OCORATUALIZ'
      'OCORRENCIAS=OCORRENCIAS'
      'ATZ_TEXTO=ATZ_TEXTO'
      'DATA3TXT=DATA3TXT'
      'PERIODO3=PERIODO3'
      'MEQ_TXT=MEQ_TXT'
      'Tel1=Tel1'
      'TEL1_TXT=TEL1_TXT')
    DataSet = QrPesq2
    BCDToCurrency = False
    Left = 216
    Top = 8
  end
  object frxPesq2: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39718.478022997700000000
    ReportOptions.LastChange = 42174.387920914350000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <VFR_ORD1> > 0 then'
      '  GH1.Visible := True else GH1.Visible := False;'
      '  if <VFR_ORD1> > 0 then'
      '  GF1.Visible := True else GF1.Visible := False;'
      '  //'
      
        '  if <VFR_ORD1> = 1 then GH1.Condition := '#39'frxDsPesq2."NOMECLIEN' +
        'TE"'#39';'
      
        '  if <VFR_ORD1> = 2 then GH1.Condition := '#39'frxDsPesq2."DDeposito' +
        '_TXT"'#39';'
      
        '  if <VFR_ORD1> = 3 then GH1.Condition := '#39'frxDsPesq2."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD1> = 4 then GH1.Condition := '#39'frxDsPesq2."Emitente"' +
        #39';'
      
        '  if <VFR_ORD1> = 5 then GH1.Condition := '#39'frxDsPesq2."CNPJ_TXT"' +
        #39';'
      '  if <VFR_ORD1> = 6 then GH1.Condition := '#39'frxDsPesq2."Data3"'#39';'
      
        '  if <VFR_ORD1> = 7 then GH1.Condition := '#39'frxDsPesq2."PERIODO3"' +
        #39';'
      ''
      ''
      '  if <VFR_ORD2> > 0 then'
      '  GH2.Visible := True else GH2.Visible := False;'
      '  if <VFR_ORD2> > 0 then'
      '  GF2.Visible := True else GF2.Visible := False;'
      '  //'
      
        '  if <VFR_ORD2> = 1 then GH2.Condition := '#39'frxDsPesq2."NOMECLIEN' +
        'TE"'#39';'
      
        '  if <VFR_ORD2> = 2 then GH2.Condition := '#39'frxDsPesq2."DDeposito' +
        '_TXT"'#39';'
      
        '  if <VFR_ORD2> = 3 then GH2.Condition := '#39'frxDsPesq2."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD2> = 4 then GH2.Condition := '#39'frxDsPesq2."Emitente"' +
        #39';'
      
        '  if <VFR_ORD2> = 5 then GH2.Condition := '#39'frxDsPesq2."CNPJ_TXT"' +
        #39';'
      '  if <VFR_ORD2> = 6 then GH2.Condition := '#39'frxDsPesq2."Data3"'#39';'
      
        '  if <VFR_ORD2> = 7 then GH2.Condition := '#39'frxDsPesq2."PERIODO3"' +
        #39';'
      ''
      ''
      '  if <VFR_ORD3> > 0 then'
      '  GH3.Visible := True else GH3.Visible := False;'
      '  if <VFR_ORD3> > 0 then'
      '  GF3.Visible := True else GF3.Visible := False;'
      '  //'
      
        '  if <VFR_ORD3> = 1 then GH3.Condition := '#39'frxDsPesq2."NOMECLIEN' +
        'TE"'#39';'
      
        '  if <VFR_ORD3> = 2 then GH3.Condition := '#39'frxDsPesq2."DDeposito' +
        '_TXT"'#39';'
      
        '  if <VFR_ORD3> = 3 then GH3.Condition := '#39'frxDsPesq2."PERIODO"'#39 +
        ';'
      
        '  if <VFR_ORD3> = 4 then GH3.Condition := '#39'frxDsPesq2."Emitente"' +
        #39';'
      
        '  if <VFR_ORD3> = 5 then GH3.Condition := '#39'frxDsPesq2."CNPJ_TXT"' +
        #39';'
      '  if <VFR_ORD3> = 6 then GH3.Condition := '#39'frxDsPesq2."Data3"'#39';'
      
        '  if <VFR_ORD3> = 7 then GH3.Condition := '#39'frxDsPesq2."PERIODO3"' +
        #39';'
      '  '
      'end.')
    OnGetValue = frxPesq1GetValue
    Left = 272
    Top = 8
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsPesq2
        DataSetName = 'frxDsPesq2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Orientation = poLandscape
      PaperWidth = 297.000000000000000000
      PaperHeight = 210.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 56.000000000000000000
        Top = 18.897650000000000000
        Width = 1046.929810000000000000
        object Memo32: TfrxMemoView
          Left = 716.519790000000000000
          Top = 0.881879999999998800
          Width = 330.299320000000000000
          Height = 16.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[DATE], [TIME]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Top = 20.661410000000000000
          Width = 1048.598640000000000000
          Height = 26.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_CLIENTE]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 36.000000000000000000
        Top = 638.740570000000000000
        Width = 1046.929810000000000000
        object Memo31: TfrxMemoView
          Left = 914.598640000000000000
          Top = 3.873529999999960000
          Width = 88.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[PAGE#] de ')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 1002.598640000000000000
          Top = 3.873529999999960000
          Width = 44.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[TotalPages#]')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 88.070810000000000000
        Top = 98.267780000000000000
        Width = 1046.929810000000000000
        object Memo19: TfrxMemoView
          Top = 1.070809999999995000
          Width = 1047.716760000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'RELAT'#211'RIO DE DUPLICATAS [VARF_SITUACOES]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = -1.338590000000000000
          Top = 25.070810000000010000
          Width = 448.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_TODOS]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 695.858380000000000000
          Top = 25.070810000000010000
          Width = 350.519790000000000000
          Height = 40.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Width = 2.000000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo7: TfrxMemoView
          Left = 647.858380000000000000
          Top = 25.070810000000010000
          Width = 48.000000000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Per'#237'odo:')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = -0.000007320000000000
          Top = 71.070810000000010000
          Width = 26.456692910000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Bco.')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 26.456685590000000000
          Top = 71.070810000000010000
          Width = 30.236220470000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ag.')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 56.692906060000000000
          Top = 71.070810000000010000
          Width = 49.133858267716530000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Duplicata')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 143.622030160000000000
          Top = 71.070810000000010000
          Width = 226.771653540000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sacado')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 370.393649530000000000
          Top = 71.070810000000010000
          Width = 75.590551180000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'CPF / CNPJ')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 608.503910160000000000
          Top = 71.070810000000010000
          Width = 56.692913385826770000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Val.Face')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 510.236193620000000000
          Top = 71.070810000000010000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencto')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 559.370051890000000000
          Top = 71.070810000000010000
          Width = 49.133858270000000000
          Height = 17.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Quita'#231#227'o')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Top = 47.070810000000010000
          Width = 650.425170000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VARF_FILTROS]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 105.826757010000000000
          Top = 71.070810000000010000
          Width = 37.795275590000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '000000'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'N.F.')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 665.196821100000000000
          Top = 71.070810000000010000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 721.889732050000000000
          Top = 71.070810000000010000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Sal.Atz')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 778.582642990000000000
          Top = 71.070810000000010000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Ocorr'#234'nc.')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 835.275553940000000000
          Top = 71.070810000000010000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Ocorr.Atz.')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 891.968464880000000000
          Top = 71.070810000000010000
          Width = 56.692913390000000000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Total Atz.')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 445.984540000000000000
          Top = 71.031540000000030000
          Width = 64.251961180000000000
          Height = 17.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Telefone')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 948.662030000000000000
          Top = 71.055118109999990000
          Width = 98.267743390000010000
          Height = 17.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Hist'#243'rico')
          ParentFont = False
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        Height = 15.000000000000000000
        Top = 370.393940000000000000
        Width = 1046.929810000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsPesq2
        DataSetName = 'frxDsPesq2'
        RowCount = 0
        object Memo2: TfrxMemoView
          Left = -0.000007320000000810
          Width = 26.456692910000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000'#39', <frxDsPesq2."Banco">)]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 26.456685590551200000
          Width = 30.236220472440900000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'0000'#39', <frxDsPesq2."Agencia">)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 56.692906060000000000
          Width = 49.133850940000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq2."Duplicata"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 143.622030160000000000
          Width = 226.771619370000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq2."Emitente"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 370.393649530000000000
          Width = 75.590534090000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq2."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 608.503910160000000000
          Width = 56.692913385826770000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."Valor"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 510.236193620000000000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          DisplayFormat.FormatStr = 'dd/mm/yy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq2."DDeposito"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 559.370051890000000000
          Width = 49.133858270000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq2."DATA3TXT"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 105.826757010000000000
          Width = 37.795273150000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[FormatFloat('#39'000000;-000000; '#39', <frxDsPesq2."NF">)]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 665.196821100000000000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."TotalPg"]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 721.889732050000000000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."SALDO_ATUALIZADO"]')
          ParentFont = False
        end
        object Memo49: TfrxMemoView
          Left = 778.582642990000000000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."OCORRENCIAS"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 835.275553940000000000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."OCORATUALIZ"]')
          ParentFont = False
        end
        object Memo68: TfrxMemoView
          Left = 891.968464880000000000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00;-#,###,###,##0.00; '
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsPesq2."TOTALATUALIZ"]')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 445.984540000000000000
          Width = 64.251944090000000000
          Height = 15.000000000000000000
          DataField = 'TEL1_TXT'
          DataSet = frxDsPesq2
          DataSetName = 'frxDsPesq2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq2."TEL1_TXT"]')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 948.662030000000000000
          Width = 98.267740940000000000
          Height = 15.000000000000000000
          DataSet = frxDsPesq2
          DataSetName = 'frxDsPesq2'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsPesq2."STATUS"]')
          ParentFont = False
        end
      end
      object GH1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 245.669450000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GH1OnBeforePrint'
        Condition = 'frxDsPesq2."NOMECLIENTE"'
        object Memo29: TfrxMemoView
          Width = 722.236240000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA1NOME]')
          ParentFont = False
        end
      end
      object GH2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 287.244280000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GH2OnBeforePrint'
        Condition = 'frxDsPesq2."Emitente"'
        object Memo30: TfrxMemoView
          Left = 22.881880000000000000
          Width = 698.456710000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA2NOME]')
          ParentFont = False
        end
      end
      object GH3: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.000000000000000000
        Top = 328.819110000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GH3OnBeforePrint'
        Condition = 'frxDsPesq2."DDeposito_TXT"'
        object Memo33: TfrxMemoView
          Left = 46.661409999999990000
          Width = 674.677180000000000000
          Height = 18.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VFR_LA3NOME]')
          ParentFont = False
        end
      end
      object GF3: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.000000000000000000
        Top = 408.189240000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GF3OnBeforePrint'
        object Memo34: TfrxMemoView
          Top = 3.779530000000022000
          Width = 608.503917480000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 608.503910160000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Valor">)]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 665.196821100000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 721.889732050000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 778.582642990000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORRENCIAS">)]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 835.275553940000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORATUALIZ">)]')
          ParentFont = False
        end
        object Memo64: TfrxMemoView
          Left = 891.968464880000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TOTALATUALIZ">)]')
          ParentFont = False
        end
      end
      object GF2: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.000000000000000000
        Top = 453.543600000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GF2OnBeforePrint'
        object Memo27: TfrxMemoView
          Top = 3.779530000000022000
          Width = 608.503917480000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 608.503910160000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Valor">)]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 665.196821100000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 721.889732050000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 778.582642990000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORRENCIAS">)]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 835.275553940000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORATUALIZ">)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 891.968464880000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TOTALATUALIZ">)]')
          ParentFont = False
        end
      end
      object GF1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.000000000000000000
        Top = 498.897960000000000000
        Width = 1046.929810000000000000
        OnBeforePrint = 'GF1OnBeforePrint'
        object Memo28: TfrxMemoView
          Top = 3.779530000000022000
          Width = 608.503917480000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 608.503910160000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Valor">)]')
          ParentFont = False
        end
        object Memo50: TfrxMemoView
          Left = 665.196821100000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 721.889732050000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 778.582642990000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORRENCIAS">)]')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 835.275553940000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORATUALIZ">)]')
          ParentFont = False
        end
        object Memo56: TfrxMemoView
          Left = 891.968464880000000000
          Top = 3.779530000000022000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TOTALATUALIZ">)]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 32.000000000000000000
        Top = 582.047620000000000000
        Width = 1046.929810000000000000
        object Line1: TfrxLineView
          Left = 2.000000000000000000
          Top = 5.542979999999943000
          Width = 1039.559060000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 2.000000000000000000
        end
        object Memo35: TfrxMemoView
          Top = 11.338589999999950000
          Width = 608.503917480000000000
          Height = 15.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            'Sub-total de [SUM(<frxDsPesq2."CONTA">)] duplicatas(s):')
          ParentFont = False
        end
        object Memo59: TfrxMemoView
          Left = 608.503910160000000000
          Top = 11.338589999999950000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."Valor">)]')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 665.196821100000000000
          Top = 11.338589999999950000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TotalPg">)]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 721.889732050000000000
          Top = 11.338589999999950000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."SALDO_ATUALIZADO">)]')
          ParentFont = False
        end
        object Memo65: TfrxMemoView
          Left = 778.582642990000000000
          Top = 11.338589999999950000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORRENCIAS">)]')
          ParentFont = False
        end
        object Memo66: TfrxMemoView
          Left = 835.275553940000000000
          Top = 11.338589999999950000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."OCORATUALIZ">)]')
          ParentFont = False
        end
        object Memo67: TfrxMemoView
          Left = 891.968464880000000000
          Top = 11.338589999999950000
          Width = 56.692910940000000000
          Height = 15.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '#,###,###,##0.00'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsPesq2."TOTALATUALIZ">)]')
          ParentFont = False
        end
      end
    end
  end
  object PMImprime: TPopupMenu
    Left = 20
    Top = 36
    object Extratodeduplicata1: TMenuItem
      Caption = '&Extrato de duplicata'
      OnClick = Extratodeduplicata1Click
    end
    object ImprimecartadeCancelamentodeProtesto2: TMenuItem
      Caption = 'Imprime carta de Cancelamento de &Protesto'
      OnClick = ImprimecartadeCancelamentodeProtesto2Click
    end
  end
  object frxExtratoDuplicata: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39949.647167257000000000
    ReportOptions.LastChange = 39949.647167257000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxExtratoDuplicataGetValue
    Left = 300
    Top = 8
    Datasets = <
      item
        DataSet = frxDsADupIts
        DataSetName = 'frxDsADupIts'
      end
      item
        DataSet = frxDsADupPgs
        DataSetName = 'frxDsADupPgs'
      end
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsLotesPrr
        DataSetName = 'frxDsLotesPrr'
      end
      item
        DataSet = frxDsOcorreu
        DataSetName = 'frxDsOcorreu'
      end
      item
        DataSet = frxDsOcorrPg
        DataSetName = 'frxDsOcorrPg'
      end
      item
        DataSet = frxDsPesq
        DataSetName = 'frxDsPesq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 291.023810000000000000
        Width = 680.315400000000000000
        DataSet = frxDsOcorreu
        DataSetName = 'frxDsOcorreu'
        PrintIfDetailEmpty = True
        RowCount = 0
        object Memo26: TfrxMemoView
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataField = 'SEQ'
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOcorreu."SEQ"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 37.795300000000000000
          Width = 52.913420000000000000
          Height = 15.118110240000000000
          DataField = 'Codigo'
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorreu."Codigo"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 90.708720000000000000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOcorreu."DataO"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 151.181200000000000000
          Width = 302.362400000000000000
          Height = 15.118110240000000000
          DataField = 'NOMEOCORRENCIA'
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsOcorreu."NOMEOCORRENCIA"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 453.543600000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorreu."Valor"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorreu."SALDO"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 604.724800000000000000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          DataField = 'ATZ_TEXTO'
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorreu."ATZ_TEXTO"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 812.598950000000000000
        Width = 680.315400000000000000
        object Memo39: TfrxMemoView
          Width = 680.315400000000000000
          Height = 15.118110240000000000
          DataSet = DmodFin.frxDsExtratos
          DataSetName = 'frxDsExtratos'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -7
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'www.dermatek.com.br - Software Customizado')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 64.252010000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape1: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo13: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'EXTRATO DE DUPLICATA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Top = 45.354360000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsPesq."NOMECLIENTE"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 41.574830000000000000
        Top = 226.771800000000000000
        Width = 680.315400000000000000
        object Memo18: TfrxMemoView
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'OCORR'#202'NCIAS DO T'#205'TULO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Top = 26.456709999999990000
          Width = 37.795300000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Seq.')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 37.795300000000000000
          Top = 26.456709999999990000
          Width = 52.913420000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 90.708720000000000000
          Top = 26.456710000000020000
          Width = 60.472480000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 151.181200000000000000
          Top = 26.456709999999990000
          Width = 302.362400000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o da ocorr'#234'ncia')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 453.543600000000000000
          Top = 26.456709999999990000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 529.134200000000000000
          Top = 26.456709999999990000
          Width = 75.590600000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'SALDO')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 604.724800000000000000
          Top = 26.456709999999990000
          Width = 71.811070000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Atualizado')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Top = 366.614410000000000000
        Width = 680.315400000000000000
      end
      object PageHeader3: TfrxPageHeader
        FillType = ftBrush
        Height = 60.472480000000000000
        Top = 105.826840000000000000
        Width = 680.315400000000000000
        object Shape2: TfrxShapeView
          Top = 3.779529999999994000
          Width = 680.315400000000000000
          Height = 56.692950000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Line2: TfrxLineView
          Top = 22.677179999999990000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo36: TfrxMemoView
          Left = 7.559060000000000000
          Top = 3.779529999999994000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Duplicata:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo1: TfrxMemoView
          Left = 60.472480000000000000
          Top = 3.779529999999994000
          Width = 128.504020000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsPesq."Duplicata"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo34: TfrxMemoView
          Left = 188.976500000000000000
          Top = 3.779529999999994000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Valor original:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo64: TfrxMemoView
          Left = 264.567100000000000000
          Top = 3.779529999999994000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsPesq."Valor"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo65: TfrxMemoView
          Left = 408.189240000000000000
          Top = 3.779529999999994000
          Width = 49.133890000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Emiss'#227'o:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo66: TfrxMemoView
          Left = 457.323130000000000000
          Top = 3.779529999999994000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsPesq."Emissao"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo67: TfrxMemoView
          Left = 532.913730000000000000
          Top = 3.779529999999994000
          Width = 64.252010000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Vencimento:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo68: TfrxMemoView
          Left = 597.165740000000000000
          Top = 3.779529999999994000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsPesq."DDeposito"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 7.559060000000000000
          Top = 22.677179999999990000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Sacado:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 60.472480000000000000
          Top = 22.677179999999990000
          Width = 430.866420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsPesq."Emitente"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 7.559060000000000000
          Top = 41.574829999999990000
          Width = 52.913420000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Baixado:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo40: TfrxMemoView
          Left = 60.472480000000000000
          Top = 41.574829999999990000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsPesq."DATA3_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 491.338900000000000000
          Top = 22.677179999999990000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_CNPJ_CPF]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Memo9: TfrxMemoView
          Left = 529.134200000000000000
          Top = 22.677179999999990000
          Width = 143.622022830000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsPesq."CNPJ_TXT"]')
          ParentFont = False
          WordWrap = False
          VAlign = vaCenter
        end
        object Line3: TfrxLineView
          Top = 41.574829999999990000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        Height = 41.574830000000000000
        Top = 389.291590000000000000
        Width = 680.315400000000000000
        object Memo23: TfrxMemoView
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'HIST'#211'RICO')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Top = 26.456709999999990000
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 60.472480000000000000
          Top = 26.456709999999990000
          Width = 619.842920000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Hist'#243'rico')
          ParentFont = False
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 453.543600000000000000
        Width = 680.315400000000000000
        DataSet = frxDsADupIts
        DataSetName = 'frxDsADupIts'
        RowCount = 0
        object Memo32: TfrxMemoView
          Width = 60.472440940000000000
          Height = 15.118110240000000000
          DataField = 'DataA'
          DataSet = frxDsADupIts
          DataSetName = 'frxDsADupIts'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsADupIts."DataA"]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 60.472480000000000000
          Width = 619.842880940000000000
          Height = 15.118110240000000000
          DataField = 'NOMESTATUS'
          DataSet = frxDsADupIts
          DataSetName = 'frxDsADupIts'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsADupIts."NOMESTATUS"]')
          ParentFont = False
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 491.338900000000000000
        Width = 680.315400000000000000
      end
      object Header3: TfrxHeader
        FillType = ftBrush
        Height = 41.574830000000000000
        Top = 517.795610000000000000
        Width = 680.315400000000000000
        object Memo27: TfrxMemoView
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'PAGAMENTOS')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Top = 26.456710000000040000
          Width = 113.385826771654000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 113.385900000000000000
          Top = 26.456710000000040000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 226.771800000000000000
          Top = 26.456710000000040000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 340.157700000000000000
          Top = 26.456710000000040000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 453.543600000000000000
          Top = 26.456710000000040000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Border'#244)
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 566.929499999999900000
          Top = 26.456710000000040000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Lote do pagto')
          ParentFont = False
        end
      end
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 582.047620000000000000
        Width = 680.315400000000000000
        DataSet = frxDsADupPgs
        DataSetName = 'frxDsADupPgs'
        RowCount = 0
        object Memo31: TfrxMemoView
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'Data'
          DataSet = frxDsADupPgs
          DataSetName = 'frxDsADupPgs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsADupPgs."Data"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 113.385900000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'Juros'
          DataSet = frxDsADupPgs
          DataSetName = 'frxDsADupPgs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsADupPgs."Juros"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 226.771800000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'Desco'
          DataSet = frxDsADupPgs
          DataSetName = 'frxDsADupPgs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsADupPgs."Desco"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 340.157700000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'Pago'
          DataSet = frxDsADupPgs
          DataSetName = 'frxDsADupPgs'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsADupPgs."Pago"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 453.543600000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'Lote'
          DataSet = frxDsADupPgs
          DataSetName = 'frxDsADupPgs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsADupPgs."Lote"]')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385826770000000000
          Height = 15.118110240000000000
          DataField = 'LotePg'
          DataSet = frxDsADupPgs
          DataSetName = 'frxDsADupPgs'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsADupPgs."LotePg"]')
          ParentFont = False
        end
      end
      object Header4: TfrxHeader
        FillType = ftBrush
        Height = 41.574830000000000000
        Top = 646.299630000000000000
        Width = 680.315400000000000000
        object Memo49: TfrxMemoView
          Width = 680.315400000000000000
          Height = 26.456710000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'PRORROGA'#199#213'ES')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Top = 26.456710000000040000
          Width = 128.503937007874000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento original')
          ParentFont = False
        end
        object Memo55: TfrxMemoView
          Left = 385.512060000000000000
          Top = 26.456710000000040000
          Width = 136.063031180000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Dias desta prorroga'#231#227'o')
          ParentFont = False
        end
        object Memo51: TfrxMemoView
          Left = 128.504020000000000000
          Top = 26.456710000000040000
          Width = 128.503937010000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Vencimento anterior')
          ParentFont = False
        end
        object Memo53: TfrxMemoView
          Left = 257.008040000000000000
          Top = 26.456710000000040000
          Width = 128.503937010000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Novo vencimento')
          ParentFont = False
        end
        object Memo57: TfrxMemoView
          Left = 521.575140000000000000
          Top = 26.456710000000040000
          Width = 158.740211180000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Ac'#250'mulo dias prorrogados')
          ParentFont = False
        end
      end
      object MasterData4: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 710.551640000000000000
        Width = 680.315400000000000000
        DataSet = frxDsLotesPrr
        DataSetName = 'frxDsLotesPrr'
        RowCount = 0
        object Memo56: TfrxMemoView
          Width = 128.503937010000000000
          Height = 15.118110240000000000
          DataField = 'Data1'
          DataSet = frxDsLotesPrr
          DataSetName = 'frxDsLotesPrr'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotesPrr."Data1"]')
          ParentFont = False
        end
        object Memo61: TfrxMemoView
          Left = 385.512060000000000000
          Width = 136.063031180000000000
          Height = 15.118110240000000000
          DataField = 'DIAS_2_3'
          DataSet = frxDsLotesPrr
          DataSetName = 'frxDsLotesPrr'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotesPrr."DIAS_2_3"]')
          ParentFont = False
        end
        object Memo52: TfrxMemoView
          Left = 128.504020000000000000
          Width = 128.503937010000000000
          Height = 15.118110240000000000
          DataField = 'Data2'
          DataSet = frxDsLotesPrr
          DataSetName = 'frxDsLotesPrr'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotesPrr."Data2"]')
          ParentFont = False
        end
        object Memo54: TfrxMemoView
          Left = 257.008040000000000000
          Width = 128.503937010000000000
          Height = 15.118110240000000000
          DataField = 'Data3'
          DataSet = frxDsLotesPrr
          DataSetName = 'frxDsLotesPrr'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotesPrr."Data3"]')
          ParentFont = False
        end
        object Memo58: TfrxMemoView
          Left = 521.575140000000000000
          Width = 158.740211180000000000
          Height = 15.118110240000000000
          DataField = 'DIAS_1_3'
          DataSet = frxDsLotesPrr
          DataSetName = 'frxDsLotesPrr'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLotesPrr."DIAS_1_3"]')
          ParentFont = False
        end
      end
      object Footer3: TfrxFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 619.842920000000000000
        Width = 680.315400000000000000
      end
      object Footer4: TfrxFooter
        FillType = ftBrush
        Height = 3.779530000000000000
        Top = 748.346940000000000000
        Width = 680.315400000000000000
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 328.819110000000000000
        Width = 680.315400000000000000
        DataSet = frxDsOcorrPg
        DataSetName = 'frxDsOcorrPg'
        RowCount = 0
        object Memo59: TfrxMemoView
          Left = 151.181200000000000000
          Width = 52.913383390000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo60: TfrxMemoView
          Left = 204.094620000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorrPg
          DataSetName = 'frxDsOcorrPg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsOcorrPg."Data"]')
          ParentFont = False
        end
        object Memo62: TfrxMemoView
          Left = 279.685220000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          Memo.UTF8W = (
            'Juros')
          ParentFont = False
        end
        object Memo63: TfrxMemoView
          Left = 328.819110000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorrPg
          DataSetName = 'frxDsOcorrPg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorrPg."Juros"]')
          ParentFont = False
        end
        object Memo69: TfrxMemoView
          Left = 404.409710000000000000
          Width = 49.133858270000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          Memo.UTF8W = (
            'Pago')
          ParentFont = False
        end
        object Memo70: TfrxMemoView
          Left = 453.543600000000000000
          Width = 75.590551180000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorrPg
          DataSetName = 'frxDsOcorrPg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorrPg."Pago"]')
          ParentFont = False
        end
        object Memo71: TfrxMemoView
          Left = 529.134199999999900000
          Width = 75.590563390000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          Memo.UTF8W = (
            'Lote do pagto')
          ParentFont = False
        end
        object Memo72: TfrxMemoView
          Left = 604.724800000000000000
          Width = 71.811021180000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorrPg
          DataSetName = 'frxDsOcorrPg'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsOcorrPg."LotePg"]')
          ParentFont = False
        end
        object Memo73: TfrxMemoView
          Left = 37.795300000000000000
          Width = 113.385900000000000000
          Height = 15.118110240000000000
          DataSet = frxDsOcorreu
          DataSetName = 'frxDsOcorreu'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Fill.BackColor = cl3DLight
          Memo.UTF8W = (
            'PAGAMENTOS')
          ParentFont = False
        end
      end
    end
  end
  object frxDsOcorreu: TfrxDBDataset
    UserName = 'frxDsOcorreu'
    CloseDataSource = False
    DataSet = QrOcorreu
    BCDToCurrency = False
    Left = 464
    Top = 272
  end
  object frxDsADupIts: TfrxDBDataset
    UserName = 'frxDsADupIts'
    CloseDataSource = False
    DataSet = QrADupIts
    BCDToCurrency = False
    Left = 668
    Top = 300
  end
  object frxDsADupPgs: TfrxDBDataset
    UserName = 'frxDsADupPgs'
    CloseDataSource = False
    DataSet = QrADupPgs
    BCDToCurrency = False
    Left = 668
    Top = 328
  end
  object frxDsLotesPrr: TfrxDBDataset
    UserName = 'frxDsLotesPrr'
    CloseDataSource = False
    DataSet = QrLotesPrr
    BCDToCurrency = False
    Left = 668
    Top = 356
  end
  object frxDsPesq: TfrxDBDataset
    UserName = 'frxDsPesq'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Controle=Controle'
      'Duplicata=Duplicata'
      'DCompra=DCompra'
      'Valor=Valor'
      'DDeposito=DDeposito'
      'Emitente=Emitente'
      'CPF=CPF'
      'Cliente=Cliente'
      'NOMECLIENTE=NOMECLIENTE'
      'STATUS=STATUS'
      'Quitado=Quitado'
      'TotalJr=TotalJr'
      'TotalDs=TotalDs'
      'TotalPg=TotalPg'
      'SALDO_DESATUALIZ=SALDO_DESATUALIZ'
      'SALDO_ATUALIZADO=SALDO_ATUALIZADO'
      'NOMESTATUS=NOMESTATUS'
      'Vencto=Vencto'
      'DDCALCJURO=DDCALCJURO'
      'Data3=Data3'
      'CNPJ_TXT=CNPJ_TXT'
      'TAXA_COMPRA=TAXA_COMPRA'
      'TxaCompra=TxaCompra'
      'Devolucao=Devolucao'
      'ProrrVz=ProrrVz'
      'ProrrDd=ProrrDd'
      'Emissao=Emissao'
      'Repassado=Repassado'
      'ValQuit=ValQuit'
      'NF=NF'
      'Banco=Banco'
      'Agencia=Agencia'
      'PERIODO=PERIODO'
      'OCORRENCIAS=OCORRENCIAS'
      'OCORATUALIZ=OCORATUALIZ'
      'TOTALATUALIZ=TOTALATUALIZ'
      'SALDO_TEXTO=SALDO_TEXTO'
      'LOCALT=LOCALT'
      'NOMELOCAL=NOMELOCAL'
      'PERIODO3=PERIODO3'
      'DATA3_TXT=DATA3_TXT'
      'Codigo=Codigo')
    DataSet = QrPesq
    BCDToCurrency = False
    Left = 668
    Top = 272
  end
  object PMDiario: TPopupMenu
    OnPopup = PMDiarioPopup
    Left = 960
    Top = 368
    object Adicionareventoaodiario1: TMenuItem
      Caption = '&Adicionar evento ao di'#225'rio'
      OnClick = Adicionareventoaodiario1Click
    end
    object Gerenciardirio1: TMenuItem
      Caption = 'Gerenciar di'#225'rio'
      OnClick = Gerenciardirio1Click
    end
  end
  object DsOcorDupl: TDataSource
    DataSet = QrOcorDupl
    Left = 184
    Top = 272
  end
  object QrOcorDupl: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM ocordupl'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 156
    Top = 272
    object QrOcorDuplCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorDuplNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object frxDsOcorrPg: TfrxDBDataset
    UserName = 'frxDsOcorrPg'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Ocorreu=Ocorreu'
      'Data=Data'
      'Juros=Juros'
      'Pago=Pago'
      'LotePg=LotePg'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt')
    DataSet = QrOcorrPg
    BCDToCurrency = False
    Left = 496
    Top = 376
  end
end
