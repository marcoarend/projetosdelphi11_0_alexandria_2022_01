unit LSincro;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls, dmkEdit;

type
  TFmLSincro = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    GroupBox1: TGroupBox;
    Label32: TLabel;
    EdWeb_Host: TdmkEdit;
    Label33: TLabel;
    EdWeb_User: TdmkEdit;
    Label34: TLabel;
    EdWeb_Pwd: TdmkEdit;
    Label35: TLabel;
    EdWeb_DB: TdmkEdit;
    QrF: TmySQLQuery;
    QrS: TmySQLQuery;
    DBx: TmySQLDatabase;
    RGTabela: TRadioGroup;
    EdTabSource: TEdit;
    Label1: TLabel;
    EdA: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdB: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure SQLInsert;
    procedure DadosInsert;
  public
    { Public declarations }
  end;

  var
  FmLSincro: TFmLSincro;

implementation

uses Module, UMySQLModule, UnMyObjects;

{$R *.DFM}

procedure TFmLSincro.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLSincro.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmLSincro.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLSincro.FormCreate(Sender: TObject);
begin
  EdWeb_Host.Text := Dmod.QrControleWeb_Host.Value;
  edWeb_User.Text := Dmod.QrControleWeb_User.Value;
  EdWeb_Pwd.Text  := Dmod.QrControleWeb_Pwd.Value;
  EdWeb_DB.Text   := Dmod.QrControleWeb_DB.Value;
  //
end;

procedure TFmLSincro.SQLInsert;
begin
  Dmod.QrUpd.SQL.Clear;
  case RGTabela.ItemIndex of
    0:
    begin
      Dmod.QrUpd.SQL.Add('INSERT INTO emitcpf SET ');
      Dmod.QrUpd.SQL.Add('CPF=:P0, Nome=:P1, LastAtz=:P2, ');
      Dmod.QrUpd.SQL.Add('Limite=:P3');
      Dmod.QrUpd.SQL.Add('');
    end;
    1:
    begin
      Dmod.QrUpd.SQL.Add('INSERT INTO emitbac SET ');
      Dmod.QrUpd.SQL.Add('CPF=:P0, BAC=:P1');
      Dmod.QrUpd.SQL.Add('');
    end;
    else ShowMessage('Tabela fonte n�o definida');
  end;
end;

procedure TFmLSincro.DadosInsert;
begin
  case RGTabela.ItemIndex of
    0:
    begin
      Dmod.QrUpd.Params[00].AsString := QrS.FieldByName('CPF').AsString;
      Dmod.QrUpd.Params[01].AsString := QrS.FieldByName('Nome').AsString;
      Dmod.QrUpd.Params[02].AsString := QrS.FieldByName('LastAtz').AsString;
      Dmod.QrUpd.Params[03].AsFloat  := Dmod.QrControleCHRisco.Value;
    end;
    1:
    begin
      Dmod.QrUpd.Params[00].AsString := QrS.FieldByName('CPF').AsString;
      Dmod.QrUpd.Params[01].AsString := QrS.FieldByName('BAC').AsString;
    end;
    else ShowMessage('Tabela fonte n�o definida');
  end;
end;

procedure TFmLSincro.BtOKClick(Sender: TObject);
var
  BD, IP, PW, ID: String;
  A, B: Integer;
begin
  A := 0;
  B := 0;
  //
  IP := EdWeb_Host.Text;
  ID := EdWeb_User.Text;
  PW := EdWeb_Pwd.Text;
  BD := EdWeb_DB.Text;
  if MLAGeral.FaltaInfo(4,
  [IP,'Servidor',ID,'Usu�rio',PW,'Senha',BD,'Banco de dados'],
  'Conex�o ao MySQL no meu site') then
  begin
    //Result := False;
    Exit;
  end;
  //
  DBx.Connected    := False;
  DBx.UserPassword := PW;
  DBx.UserName     := ID;
  DBx.Host         := IP;
  DBx.DatabaseName := BD; // J� deve existir (configurado pelo hospedeiro)
  DBx.Connected := True;
  //
  QrS.Close;
  QrS.SQL.Clear;
  QrS.SQL.Add('SELECT * FROM '+edtabsource.text);
  QrS.Open;
  if QrS.RecordCount = 0 then
  begin
    Application.MessageBox(PChar('A tabela ' + EdTabSource.Text + ' n�o possui' +
    ' registros!'), 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  SQLInsert;
  while not QrS.Eof do
  begin
    DadosInsert;
    try
      Dmod.QrUpd.ExecSQL;
      A := A + 1;
      EdA.Text := IntToStr(A);
    except
      B := B + 1;
      EdB.Text := IntToStr(B);
    end;
    //
    QrS.Next;
  end;
end;

end.

