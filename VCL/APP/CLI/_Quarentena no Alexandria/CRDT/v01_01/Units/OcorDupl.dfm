object FmOcorDupl: TFmOcorDupl
  Left = 368
  Top = 194
  Caption = 'Al'#237'neas de Duplicatas'
  ClientHeight = 273
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 225
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 176
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 681
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 144
      Align = alTop
      TabOrder = 0
      object Label7: TLabel
        Left = 4
        Top = 4
        Width = 35
        Height = 13
        Caption = 'ID [F4]:'
        FocusControl = DBEdCodigo
      end
      object Label9: TLabel
        Left = 65
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label3: TLabel
        Left = 507
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label4: TLabel
        Left = 4
        Top = 48
        Width = 125
        Height = 13
        Caption = 'Ocorr'#234'ncia banc'#225'ria base:'
        FocusControl = DBEdNome
      end
      object SpeedButton5: TSpeedButton
        Left = 566
        Top = 64
        Width = 21
        Height = 21
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TdmkEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        OnKeyDown = EdCodigoKeyDown
      end
      object EdNome: TdmkEdit
        Left = 65
        Top = 20
        Width = 436
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdBase: TdmkEdit
        Left = 507
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'Base'
        UpdCampo = 'Base'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
      object RGDataPBase: TdmkRadioGroup
        Left = 593
        Top = 4
        Width = 185
        Height = 81
        Caption = 'Data base'
        ItemIndex = 0
        Items.Strings = (
          'Inclus'#227'o da al'#237'nea'
          'Vencimento da duplicata')
        TabOrder = 3
        QryCampo = 'Datapbase'
        UpdCampo = 'Datapbase'
        UpdType = utYes
        OldValor = 0
      end
      object EdOcorrencia: TdmkEditCB
        Left = 4
        Top = 64
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'OcorrBase'
        UpdCampo = 'OcorrBase'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBOcorrencia
        IgnoraDBLookupComboBox = False
      end
      object CBOcorrencia: TdmkDBLookupComboBox
        Left = 60
        Top = 64
        Width = 504
        Height = 21
        DragCursor = crHandPoint
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOcorBank
        TabOrder = 5
        dmkEditCB = EdOcorrencia
        QryCampo = 'OcorrBase'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGStatusBase: TdmkRadioGroup
        Left = 4
        Top = 91
        Width = 774
        Height = 45
        Caption = 'Data base'
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Nenhum'
          'Prorrogado'
          'Baixado'
          'Moroso')
        TabOrder = 6
        QryCampo = 'StatusBase'
        UpdCampo = 'StatusBase'
        UpdType = utYes
        OldValor = 0
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 225
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 144
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 4
        Top = 4
        Width = 14
        Height = 13
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 64
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label5: TLabel
        Left = 507
        Top = 4
        Width = 53
        Height = 13
        Caption = 'Valor base:'
        FocusControl = dmkDBEdit1
      end
      object Label6: TLabel
        Left = 4
        Top = 48
        Width = 125
        Height = 13
        Caption = 'Ocorr'#234'ncia banc'#225'ria base:'
        FocusControl = dmkDBEdit2
      end
      object DBEdCodigo: TdmkDBEdit
        Left = 4
        Top = 20
        Width = 56
        Height = 21
        Hint = 'N'#186' do banco'
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsOcorDupl
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
        UpdType = utYes
        Alignment = taRightJustify
      end
      object DBEdNome: TdmkDBEdit
        Left = 64
        Top = 20
        Width = 436
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsOcorDupl
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 507
        Top = 20
        Width = 80
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Base'
        DataSource = DsOcorDupl
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 4
        Top = 64
        Width = 583
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'NOMEOCORBANK'
        DataSource = DsOcorDupl
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 593
        Top = 4
        Width = 185
        Height = 81
        Caption = 'Data base'
        DataField = 'Datapbase'
        DataSource = DsOcorDupl
        Items.Strings = (
          'Inclus'#227'o da al'#237'nea'
          'Vencimento da duplicata')
        ParentBackground = True
        TabOrder = 4
        Values.Strings = (
          '0'
          '1')
      end
      object DBRadioGroup2: TDBRadioGroup
        Left = 4
        Top = 91
        Width = 774
        Height = 45
        Caption = 'Status base'
        Columns = 4
        DataField = 'Statusbase'
        DataSource = DsOcorDupl
        Items.Strings = (
          'Nenhum'
          'Prorrogado'
          'Baixado'
          'Moroso')
        ParentBackground = True
        TabOrder = 5
        Values.Strings = (
          '0'
          '1'
          '2'
          '3')
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 176
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TStaticText
        Left = 173
        Top = 1
        Width = 123
        Height = 46
        Align = alClient
        BevelKind = bkTile
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 296
        Top = 1
        Width = 493
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 384
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtImportar: TBitBtn
          Tag = 19
          Left = 280
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = 'I&mporta'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 4
          OnClick = BtImportarClick
        end
      end
    end
    object Progress: TProgressBar
      Left = 1
      Top = 159
      Width = 790
      Height = 17
      Align = alBottom
      TabOrder = 2
      Visible = False
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Cadastro de Duplicatas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 709
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 483
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 232
      ExplicitTop = -23
    end
    object PnPesq: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsOcorDupl: TDataSource
    DataSet = QrOcorDupl
    Left = 560
    Top = 12
  end
  object QrOcorDupl: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrOcorDuplBeforeOpen
    AfterOpen = QrOcorDuplAfterOpen
    SQL.Strings = (
      'SELECT ok.Nome NOMEOCORBANK, od.* '
      'FROM ocordupl od'
      'LEFT JOIN ocorbank ok ON ok.Codigo=od.Ocorrbase')
    Left = 532
    Top = 12
    object QrOcorDuplCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorDuplNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrOcorDuplBase: TFloatField
      FieldName = 'Base'
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorDuplLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorDuplDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorDuplDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorDuplUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorDuplUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorDuplDatapbase: TIntegerField
      FieldName = 'Datapbase'
    end
    object QrOcorDuplOcorrbase: TIntegerField
      FieldName = 'Ocorrbase'
    end
    object QrOcorDuplStatusbase: TIntegerField
      FieldName = 'Statusbase'
    end
    object QrOcorDuplEnvio: TIntegerField
      FieldName = 'Envio'
    end
    object QrOcorDuplMovimento: TIntegerField
      FieldName = 'Movimento'
    end
    object QrOcorDuplAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrOcorDuplAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrOcorDuplNOMEOCORBANK: TWideStringField
      FieldName = 'NOMEOCORBANK'
      Size = 50
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 588
    Top = 12
  end
  object QrOcorBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome FROM ocorbank'
      'ORDER BY Nome')
    Left = 616
    Top = 12
    object QrOcorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsOcorBank: TDataSource
    DataSet = QrOcorBank
    Left = 644
    Top = 12
  end
  object OpenDialog1: TOpenDialog
    DefaultExt = '*.txt'
    Filter = 'Arquivos Texto|*.txt'
    Left = 672
    Top = 12
  end
end
