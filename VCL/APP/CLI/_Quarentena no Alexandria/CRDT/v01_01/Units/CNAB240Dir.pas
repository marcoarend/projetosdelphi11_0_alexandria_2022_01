unit CNAB240Dir;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, DBCtrls, Db, (*DBTables,*) ExtDlgs, ZCF2,
  ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkDBEdit, Mask, dmkDBLookupComboBox,
  dmkEditCB, dmkRadioGroup, Variants, UnDmkProcFunc, UnDmkEnums;

type
  TFmCNAB240Dir = class(TForm)
    PainelDados: TPanel;
    DsCNAB240Dir: TDataSource;
    QrCNAB240Dir: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PnPesq: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TStaticText;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TdmkDBEdit;
    DBEdNome: TdmkDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label9: TLabel;
    QrCNAB240DirCodigo: TIntegerField;
    QrCNAB240DirNome: TWideStringField;
    QrCNAB240DirEnvio: TSmallintField;
    QrCNAB240DirBanco: TIntegerField;
    SpeedButton5: TSpeedButton;
    EdBanco: TdmkEditCB;
    CBBanco: TdmkDBLookupComboBox;
    RGEnvio: TdmkRadioGroup;
    Label3: TLabel;
    QrBancos: TmySQLQuery;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    QrBancosSite: TWideStringField;
    QrBancosLk: TIntegerField;
    QrBancosDataCad: TDateField;
    QrBancosDataAlt: TDateField;
    QrBancosUserCad: TIntegerField;
    QrBancosUserAlt: TIntegerField;
    QrBancosDVCC: TSmallintField;
    DsBancos: TDataSource;
    QrCNAB240DirNOMEBANCO: TWideStringField;
    dmkDBEdit1: TdmkDBEdit;
    Label4: TLabel;
    DBRadioGroup1: TDBRadioGroup;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCNAB240DirAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCNAB240DirBeforeOpen(DataSet: TDataSet);
    procedure BtIncluiClick(Sender: TObject);
    procedure SbNovoClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    //procedure IncluiRegistro;
    //procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
  end;

var
  FmCNAB240Dir: TFmCNAB240Dir;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCNAB240Dir.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCNAB240Dir.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCNAB240DirCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCNAB240Dir.DefParams;
begin
  VAR_GOTOTABELA := 'cnab240dir';
  VAR_GOTOMYSQLTABLE := QrCNAB240Dir;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT ban.Nome NOMEBANCO, dir.*');
  VAR_SQLx.Add('FROM cnab240dir dir');
  VAR_SQLx.Add('LEFT JOIN bancos ban ON ban.Codigo=dir.Banco');
  VAR_SQLx.Add('WHERE dir.Codigo > 0');
  //
  VAR_SQL1.Add('AND dir.Codigo=:P0');
  //
  //VAR_SQL2.Add('AND dir.CodUsu=:P0');
  //
  VAR_SQLa.Add('AND dir.Nome Like :P0');
  //
end;

procedure TFmCNAB240Dir.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.ValueVariant := FormatFloat(FFormatFloat, Codigo);
        EdBanco.ValueVariant  := 0;
        CBBanco.KeyValue      := Null;
        EdNome.ValueVariant   := '';
        RGEnvio.ItemIndex     := -1;
        //
      end else begin
        EdCodigo.ValueVariant := QrCNAB240DirCodigo.Value;
        EdBanco.ValueVariant  := QrCNAB240DirBanco.Value;
        CBBanco.KeyValue      := QrCNAB240DirBanco.Value;
        EdNome.ValueVariant   := QrCNAB240DirNome.Value;
        RGEnvio.ItemIndex     := QrCNAB240DirEnvio.Value;
        //
      end;
      EdNome.SetFocus;
    end;
    else Geral.MensagemBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.SQLType);
end;

procedure TFmCNAB240Dir.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCNAB240Dir.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCNAB240Dir.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCNAB240Dir.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCNAB240Dir.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCNAB240Dir.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCNAB240Dir.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCNAB240Dir.SpeedButton5Click(Sender: TObject);
begin
  MyObjects.DefineDiretorio(Self, EdNome);
  EdNome.SetFocus;
end;

procedure TFmCNAB240Dir.BtAlteraClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrCNAB240Dir, [PainelDados],
  [PainelEdita], EdBanco, LaTipo, 'cnab240dir');
end;

procedure TFmCNAB240Dir.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCNAB240DirCodigo.Value;
  Close;
end;

procedure TFmCNAB240Dir.BtConfirmaClick(Sender: TObject);
var
  Codigo, Banco, Envio: Integer;
  Nome: String;
begin
  Nome  := EdNome.ValueVariant;
  Banco := EdBanco.ValueVariant;
  Envio := RGEnvio.ItemIndex;
  //
  if MyObjects.FIC(Banco = 0, EdBanco, 'Banco n�o definido!') then Exit;
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Diret�rio n�o definido!') then Exit;
  if MyObjects.FIC(Envio < 0, RGEnvio, 'Tipo de envio n�o definido!') then Exit;
  //
  Codigo := UMyMod.BuscaEmLivreY_Def('cnab240dir', 'Codigo', LaTipo.SQLType,
    QrCNAB240DirCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmCNAB240Dir, PainelEdit,
    'cnab240dir', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmCNAB240Dir.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.ValueVariant);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'cnab240dir', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cnab240dir', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cnab240dir', 'Codigo');
end;

procedure TFmCNAB240Dir.BtIncluiClick(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrCNAB240Dir, [PainelDados],
  [PainelEdita], EdBanco, LaTipo, 'cnab240dir');
end;

procedure TFmCNAB240Dir.FormCreate(Sender: TObject);
begin
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
  //
  QrBancos.Open;
end;

procedure TFmCNAB240Dir.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCNAB240DirCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCNAB240Dir.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCNAB240Dir.SbNovoClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.CodUsu(QrCNAB240DirCodigo.Value, LaRegistro.Caption);
end;

procedure TFmCNAB240Dir.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.SQLType);
end;

procedure TFmCNAB240Dir.QrCNAB240DirAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCNAB240Dir.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCNAB240Dir.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCNAB240DirCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cnab240dir', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCNAB240Dir.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmCNAB240Dir.QrCNAB240DirBeforeOpen(DataSet: TDataSet);
begin
  QrCNAB240DirCodigo.DisplayFormat := FFormatFloat;
end;

end.

