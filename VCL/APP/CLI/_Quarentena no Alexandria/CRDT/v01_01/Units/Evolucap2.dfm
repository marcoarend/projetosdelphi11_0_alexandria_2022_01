object FmEvolucap2: TFmEvolucap2
  Left = 335
  Top = 176
  Caption = 'Evolu'#231#227'o do Capital (2)'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 784
    Height = 446
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Caption = 'Dados'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel1: TPanel
        Left = 0
        Top = 342
        Width = 776
        Height = 76
        Align = alBottom
        BevelOuter = bvNone
        TabOrder = 0
        object PnAtualiza: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 76
          Align = alClient
          TabOrder = 0
          object GroupBox2: TGroupBox
            Left = 1
            Top = 1
            Width = 774
            Height = 64
            Align = alTop
            Caption = ' Atualizar o gr'#225'fico: '
            TabOrder = 0
            Visible = False
            object Label4: TLabel
              Left = 8
              Top = 20
              Width = 93
              Height = 13
              Caption = 'Data compra inicial:'
            end
            object Label5: TLabel
              Left = 112
              Top = 20
              Width = 86
              Height = 13
              Caption = 'Data compra final:'
            end
            object TPIniB: TDateTimePicker
              Left = 8
              Top = 36
              Width = 101
              Height = 21
              Date = 38675.714976851900000000
              Time = 38675.714976851900000000
              TabOrder = 0
            end
            object TPFimB: TDateTimePicker
              Left = 112
              Top = 36
              Width = 101
              Height = 21
              Date = 38675.714976851900000000
              Time = 38675.714976851900000000
              TabOrder = 1
            end
            object BtGrafico: TBitBtn
              Tag = 262
              Left = 300
              Top = 16
              Width = 90
              Height = 40
              Caption = 'Atuali&za'
              NumGlyphs = 2
              TabOrder = 2
              OnClick = BtGraficoClick
            end
            object RGIntervalo: TRadioGroup
              Left = 218
              Top = 8
              Width = 75
              Height = 54
              Caption = ' Intervalo: '
              ItemIndex = 0
              Items.Strings = (
                'Di'#225'rio'
                'Mensal')
              TabOrder = 3
            end
          end
        end
        object PnEditSdoCC: TPanel
          Left = 0
          Top = 0
          Width = 776
          Height = 76
          Align = alClient
          TabOrder = 1
          Visible = False
          object Label3: TLabel
            Left = 4
            Top = 6
            Width = 69
            Height = 13
            Caption = '$ Saldo conta:'
          end
          object Label6: TLabel
            Left = 88
            Top = 6
            Width = 67
            Height = 13
            Caption = '$ Saldo caixa:'
          end
          object BitBtn1: TBitBtn
            Tag = 14
            Left = 186
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Confirma'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BitBtn1Click
          end
          object EdSaldoConta: TdmkEdit
            Left = 4
            Top = 23
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object BitBtn2: TBitBtn
            Tag = 15
            Left = 282
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Desiste'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BitBtn2Click
          end
          object EdSaldoCaixa: TdmkEdit
            Left = 88
            Top = 23
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
        end
      end
      object PnDados: TPanel
        Left = 0
        Top = 0
        Width = 776
        Height = 342
        Align = alClient
        TabOrder = 1
        object GroupBox1: TGroupBox
          Left = 1
          Top = 1
          Width = 774
          Height = 64
          Align = alTop
          Caption = ' Atualizar os dados do per'#237'odo: '
          TabOrder = 0
          object Label34: TLabel
            Left = 8
            Top = 20
            Width = 93
            Height = 13
            Caption = 'Data compra inicial:'
          end
          object Label1: TLabel
            Left = 112
            Top = 20
            Width = 86
            Height = 13
            Caption = 'Data compra final:'
          end
          object Label2: TLabel
            Left = 324
            Top = 20
            Width = 86
            Height = 13
            Caption = 'Coligado especial:'
            Visible = False
          end
          object TPIniA: TDateTimePicker
            Left = 8
            Top = 36
            Width = 101
            Height = 21
            Date = 38675.714976851900000000
            Time = 38675.714976851900000000
            TabOrder = 0
          end
          object TPFimA: TDateTimePicker
            Left = 112
            Top = 36
            Width = 101
            Height = 21
            Date = 38675.714976851900000000
            Time = 38675.714976851900000000
            TabOrder = 1
          end
          object EdColigado: TdmkEditCB
            Left = 324
            Top = 36
            Width = 52
            Height = 21
            Alignment = taRightJustify
            TabOrder = 2
            Visible = False
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBColigado
            IgnoraDBLookupComboBox = False
          end
          object CBColigado: TdmkDBLookupComboBox
            Left = 378
            Top = 36
            Width = 327
            Height = 21
            KeyField = 'Codigo'
            ListField = 'NOMECOLIGADO'
            ListSource = DsColigado
            TabOrder = 3
            Visible = False
            dmkEditCB = EdColigado
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object BtOK: TBitBtn
            Tag = 14
            Left = 216
            Top = 16
            Width = 90
            Height = 40
            Caption = '&Atualiza'
            NumGlyphs = 2
            TabOrder = 4
            OnClick = BtOKClick
          end
          object CkMoroso: TCheckBox
            Left = 712
            Top = 36
            Width = 61
            Height = 17
            Caption = 'Moroso'
            TabOrder = 5
            Visible = False
          end
        end
        object ProgressBar1: TProgressBar
          Left = 1
          Top = 324
          Width = 774
          Height = 17
          Align = alBottom
          TabOrder = 1
          Visible = False
        end
        object DBGrid1: TDBGrid
          Left = 1
          Top = 65
          Width = 774
          Height = 259
          Align = alClient
          DataSource = DsEvolucap2
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = DBGrid1DblClick
          OnKeyDown = DBGrid1KeyDown
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Gr'#225'fico'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
    end
    object TabSheet3: TTabSheet
      Caption = 'Cheques devolvidos orf'#227'os'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object DBGrid2: TDBGrid
        Left = 0
        Top = 0
        Width = 776
        Height = 370
        Align = alClient
        DataSource = DsCHNull
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object Panel2: TPanel
        Left = 0
        Top = 370
        Width = 776
        Height = 48
        Align = alBottom
        TabOrder = 1
        object BtExclui1: TBitBtn
          Tag = 12
          Left = 11
          Top = 4
          Width = 96
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui entidade atual'
          Caption = '&Exclui'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtExclui1Click
        end
      end
    end
    object TabSheet4: TTabSheet
      Caption = 'Cheques devolvidos precocemente'
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object DBGrid3: TDBGrid
        Left = 0
        Top = 0
        Width = 776
        Height = 370
        Align = alClient
        DataSource = DsCHPrecoce
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
      end
      object Panel3: TPanel
        Left = 0
        Top = 370
        Width = 776
        Height = 48
        Align = alBottom
        TabOrder = 1
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Evolu'#231#227'o do Capital (2)'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object QrCarteiTudo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(li.Valor) Valor'
      'FROM lotesits li'
      'LEFT JOIN alinits ai ON ai.ChequeOrigem=li.Controle'
      'WHERE li.DCompra <= :P0'
      'AND li.DDeposito >= :P1'
      'AND ai.ChequeOrigem IS NULL')
    Left = 532
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCarteiTudoValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrEvolucap2: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEvolucap2CalcFields
    SQL.Strings = (
      'SELECT * FROM evolucap2'
      'ORDER BY DataE')
    Left = 532
    Top = 32
    object QrEvolucap2DataE: TDateField
      FieldName = 'DataE'
    end
    object QrEvolucap2CHAbeA: TFloatField
      FieldName = 'CHAbeA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHAbeB: TFloatField
      FieldName = 'CHAbeB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHAbeC: TFloatField
      FieldName = 'CHAbeC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUAbeA: TFloatField
      FieldName = 'DUAbeA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUAbeB: TFloatField
      FieldName = 'DUAbeB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUAbeC: TFloatField
      FieldName = 'DUAbeC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHDevA: TFloatField
      FieldName = 'CHDevA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHDevB: TFloatField
      FieldName = 'CHDevB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHDevC: TFloatField
      FieldName = 'CHDevC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUDevA: TFloatField
      FieldName = 'DUDevA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUDevB: TFloatField
      FieldName = 'DUDevB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUDevC: TFloatField
      FieldName = 'DUDevC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHPgdA: TFloatField
      FieldName = 'CHPgdA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHPgdB: TFloatField
      FieldName = 'CHPgdB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHPgdC: TFloatField
      FieldName = 'CHPgdC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUPgdA: TFloatField
      FieldName = 'DUPgdA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUPgdB: TFloatField
      FieldName = 'DUPgdB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUPgdC: TFloatField
      FieldName = 'DUPgdC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHMorA: TFloatField
      FieldName = 'CHMorA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHMorB: TFloatField
      FieldName = 'CHMorB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHMorC: TFloatField
      FieldName = 'CHMorC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUMorA: TFloatField
      FieldName = 'DUMorA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUMorB: TFloatField
      FieldName = 'DUMorB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUMorC: TFloatField
      FieldName = 'DUMorC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHPgmA: TFloatField
      FieldName = 'CHPgmA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHPgmB: TFloatField
      FieldName = 'CHPgmB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2CHPgmC: TFloatField
      FieldName = 'CHPgmC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUPgmA: TFloatField
      FieldName = 'DUPgmA'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUPgmB: TFloatField
      FieldName = 'DUPgmB'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolucap2DUPgmC: TFloatField
      FieldName = 'DUPgmC'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsEvolucap2: TDataSource
    DataSet = QrEvolucap2
    Left = 560
    Top = 32
  end
  object QrColigado: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECOLIGADO'
      'FROM entidades'
      'WHERE Fornece2="V"')
    Left = 724
    Top = 5
    object QrColigadoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrColigadoNOMECOLIGADO: TWideStringField
      FieldName = 'NOMECOLIGADO'
      Size = 100
    end
  end
  object DsColigado: TDataSource
    DataSet = QrColigado
    Left = 752
    Top = 5
  end
  object QrRepassOutr: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lit.Valor) Valor'
      'FROM lotesits lit'
      'LEFT JOIN repasits rit ON rit.Origem=lit.Controle '
      'LEFT JOIN repas    rep ON rep.Codigo=rit.Codigo'
      'WHERE lit.DCompra <= :P0'
      'AND lit.DDeposito >= :P1'
      'AND lit.Repassado=1'
      'AND rep.Data <= :P2'
      'AND rep.Coligado <> :P3')
    Left = 560
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrRepassOutrValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrRepassEspe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lit.Valor) Valor'
      'FROM lotesits lit'
      'LEFT JOIN repasits rit ON rit.Origem=lit.Controle '
      'LEFT JOIN repas    rep ON rep.Codigo=rit.Codigo'
      'WHERE lit.DCompra <= :P0'
      'AND lit.DDeposito >= :P1'
      'AND lit.Repassado=1'
      'AND rep.Data <= :P2'
      'AND rep.Coligado = :P3'
      '')
    Left = 588
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrRepassEspeValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCHDevolAbe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor+JurosV-ValPago) Valor'
      'FROM alinits'
      'WHERE Data1 < :P0'
      'AND ((Data3=0) OR (Data3 >:P1)) ')
    Left = 616
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCHDevolAbeValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrDUDevolAbe: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lit.Valor + lit.TotalJr - lit.TotalDs - '
      'lit.TotalPg) Valor'
      'FROM lotesits lit'
      'LEFT JOIN lotes lot ON lot.Codigo=lit.Codigo'
      'WHERE lot.Tipo=1'
      'AND lit.Vencto < :P0'
      'AND ((lit.Quitado in (0,1)) OR (lit.Data3 > :P1))')
    Left = 644
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDUDevolAbeValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrEvolper: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEvolperCalcFields
    SQL.Strings = (
      'SELECT * FROM evolucap2'
      'WHERE DataE BETWEEN :P0 AND :P1'
      'AND MONTH(DataE) <> MONTH(DATE_ADD(DataE, INTERVAL 1 DAY))'
      'ORDER BY DataE')
    Left = 588
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEvolperDataE: TDateField
      FieldName = 'DataE'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEvolperCarteiTudo: TFloatField
      FieldName = 'CarteiTudo'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolperRepassEspe: TFloatField
      FieldName = 'RepassEspe'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolperRepassOutr: TFloatField
      FieldName = 'RepassOutr'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolperCARTEIREAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CARTEIREAL'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrEvolperCHDevolAbe: TFloatField
      FieldName = 'CHDevolAbe'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolperDUDevolAbe: TFloatField
      FieldName = 'DUDevolAbe'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolperVALOR_SUB_T: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR_SUB_T'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrEvolperSaldoConta: TFloatField
      FieldName = 'SaldoConta'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrEvolperVALOR_TOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VALOR_TOTAL'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
      Calculated = True
    end
    object QrEvolperSaldoCaixa: TFloatField
      FieldName = 'SaldoCaixa'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object QrMax: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(CarteiTudo+CHDevolAbe+DUDevolAbe+SaldoConta) TOTAL'
      'FROM evolucap2'
      'WHERE DataE BETWEEN :P0 AND :P1')
    Left = 616
    Top = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMaxTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
  end
  object QrCHNull: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCHNullAfterOpen
    BeforeClose = QrCHNullBeforeClose
    SQL.Strings = (
      'SELECT ai.Cliente, ai.Emitente, ai.CPF, ai.Banco, ai.Agencia, '
      
        'ai.Conta, ai.Cheque, ai.Valor, ai.Taxas, ai.Multa, ai.JurosV Jur' +
        'os, '
      
        'ai.Desconto, ai.ValPago, ai.ChequeOrigem, ai.LoteOrigem, ai.Codi' +
        'go'
      'FROM alinits ai'
      'LEFT JOIN lotesits li ON li.Controle=ai.ChequeOrigem'
      'WHERE ai.Data3=0'
      'AND li.Quitado IS NULL')
    Left = 140
    Top = 112
    object QrCHNullCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrCHNullEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrCHNullCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrCHNullBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrCHNullAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrCHNullConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrCHNullCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
    end
    object QrCHNullValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrCHNullTaxas: TFloatField
      FieldName = 'Taxas'
      Required = True
    end
    object QrCHNullMulta: TFloatField
      FieldName = 'Multa'
      Required = True
    end
    object QrCHNullJuros: TFloatField
      FieldName = 'Juros'
      Required = True
    end
    object QrCHNullDesconto: TFloatField
      FieldName = 'Desconto'
      Required = True
    end
    object QrCHNullValPago: TFloatField
      FieldName = 'ValPago'
      Required = True
    end
    object QrCHNullChequeOrigem: TIntegerField
      FieldName = 'ChequeOrigem'
      Required = True
    end
    object QrCHNullLoteOrigem: TIntegerField
      FieldName = 'LoteOrigem'
      Required = True
    end
    object QrCHNullCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsCHNull: TDataSource
    DataSet = QrCHNull
    Left = 168
    Top = 112
  end
  object QrCHPrecoce: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM alinits ai'
      'LEFT JOIN lotesits li ON li.Controle=ai.ChequeOrigem'
      'WHERE li.DDeposito >= SYSDATE()'
      'AND ai.Data3=0')
    Left = 140
    Top = 140
    object QrCHPrecoceCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCHPrecoceAlinea1: TIntegerField
      FieldName = 'Alinea1'
      Required = True
    end
    object QrCHPrecoceAlinea2: TIntegerField
      FieldName = 'Alinea2'
      Required = True
    end
    object QrCHPrecoceData1: TDateField
      FieldName = 'Data1'
      Required = True
    end
    object QrCHPrecoceData2: TDateField
      FieldName = 'Data2'
      Required = True
    end
    object QrCHPrecoceData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrCHPrecoceCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrCHPrecoceBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrCHPrecoceAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrCHPrecoceConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrCHPrecoceCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
    end
    object QrCHPrecoceCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrCHPrecoceValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrCHPrecoceTaxas: TFloatField
      FieldName = 'Taxas'
      Required = True
    end
    object QrCHPrecoceMulta: TFloatField
      FieldName = 'Multa'
      Required = True
    end
    object QrCHPrecoceJurosP: TFloatField
      FieldName = 'JurosP'
      Required = True
    end
    object QrCHPrecoceJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
    end
    object QrCHPrecoceDesconto: TFloatField
      FieldName = 'Desconto'
      Required = True
    end
    object QrCHPrecoceEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrCHPrecoceChequeOrigem: TIntegerField
      FieldName = 'ChequeOrigem'
      Required = True
    end
    object QrCHPrecoceValPago: TFloatField
      FieldName = 'ValPago'
      Required = True
    end
    object QrCHPrecoceStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrCHPrecoceLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCHPrecoceDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCHPrecoceDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCHPrecoceUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCHPrecoceUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCHPrecocePgDesc: TFloatField
      FieldName = 'PgDesc'
      Required = True
    end
    object QrCHPrecoceDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
    end
    object QrCHPrecoceVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrCHPrecoceLoteOrigem: TIntegerField
      FieldName = 'LoteOrigem'
      Required = True
    end
    object QrCHPrecoceAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrCHPrecoceCodigo_1: TIntegerField
      FieldName = 'Codigo_1'
    end
    object QrCHPrecoceControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCHPrecoceComp: TIntegerField
      FieldName = 'Comp'
    end
    object QrCHPrecoceBanco_1: TIntegerField
      FieldName = 'Banco_1'
    end
    object QrCHPrecoceAgencia_1: TIntegerField
      FieldName = 'Agencia_1'
    end
    object QrCHPrecoceConta_1: TWideStringField
      FieldName = 'Conta_1'
    end
    object QrCHPrecoceCheque_1: TIntegerField
      FieldName = 'Cheque_1'
    end
    object QrCHPrecoceCPF_1: TWideStringField
      FieldName = 'CPF_1'
      Size = 15
    end
    object QrCHPrecoceEmitente_1: TWideStringField
      FieldName = 'Emitente_1'
      Size = 50
    end
    object QrCHPrecoceBruto: TFloatField
      FieldName = 'Bruto'
    end
    object QrCHPrecoceDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrCHPrecoceValor_1: TFloatField
      FieldName = 'Valor_1'
    end
    object QrCHPrecoceEmissao: TDateField
      FieldName = 'Emissao'
    end
    object QrCHPrecoceDCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrCHPrecoceDDeposito_1: TDateField
      FieldName = 'DDeposito_1'
    end
    object QrCHPrecoceVencto_1: TDateField
      FieldName = 'Vencto_1'
    end
    object QrCHPrecoceTxaCompra: TFloatField
      FieldName = 'TxaCompra'
    end
    object QrCHPrecoceTxaJuros: TFloatField
      FieldName = 'TxaJuros'
    end
    object QrCHPrecoceTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
    end
    object QrCHPrecoceVlrCompra: TFloatField
      FieldName = 'VlrCompra'
    end
    object QrCHPrecoceVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
    end
    object QrCHPrecoceDMais: TIntegerField
      FieldName = 'DMais'
    end
    object QrCHPrecoceDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrCHPrecoceDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrCHPrecoceDevolucao: TIntegerField
      FieldName = 'Devolucao'
    end
    object QrCHPrecoceQuitado: TIntegerField
      FieldName = 'Quitado'
    end
    object QrCHPrecoceLk_1: TIntegerField
      FieldName = 'Lk_1'
    end
    object QrCHPrecoceDataCad_1: TDateField
      FieldName = 'DataCad_1'
    end
    object QrCHPrecoceDataAlt_1: TDateField
      FieldName = 'DataAlt_1'
    end
    object QrCHPrecoceUserCad_1: TIntegerField
      FieldName = 'UserCad_1'
    end
    object QrCHPrecoceUserAlt_1: TIntegerField
      FieldName = 'UserAlt_1'
    end
    object QrCHPrecocePraca: TIntegerField
      FieldName = 'Praca'
    end
    object QrCHPrecoceBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
    end
    object QrCHPrecoceAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
    end
    object QrCHPrecoceTotalJr: TFloatField
      FieldName = 'TotalJr'
    end
    object QrCHPrecoceTotalDs: TFloatField
      FieldName = 'TotalDs'
    end
    object QrCHPrecoceTotalPg: TFloatField
      FieldName = 'TotalPg'
    end
    object QrCHPrecoceData3_1: TDateField
      FieldName = 'Data3_1'
    end
    object QrCHPrecoceProrrVz: TIntegerField
      FieldName = 'ProrrVz'
    end
    object QrCHPrecoceProrrDd: TIntegerField
      FieldName = 'ProrrDd'
    end
    object QrCHPrecoceRepassado: TSmallintField
      FieldName = 'Repassado'
    end
    object QrCHPrecoceDepositado: TSmallintField
      FieldName = 'Depositado'
    end
    object QrCHPrecoceValQuit: TFloatField
      FieldName = 'ValQuit'
    end
    object QrCHPrecoceValDeposito: TFloatField
      FieldName = 'ValDeposito'
    end
    object QrCHPrecoceTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrCHPrecoceAliIts: TIntegerField
      FieldName = 'AliIts'
    end
    object QrCHPrecoceAlinPgs: TIntegerField
      FieldName = 'AlinPgs'
    end
    object QrCHPrecoceNaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
    end
    object QrCHPrecoceReforcoCxa: TSmallintField
      FieldName = 'ReforcoCxa'
    end
    object QrCHPrecoceCartDep: TIntegerField
      FieldName = 'CartDep'
    end
    object QrCHPrecoceCobranca: TIntegerField
      FieldName = 'Cobranca'
    end
    object QrCHPrecoceRepCli: TIntegerField
      FieldName = 'RepCli'
    end
    object QrCHPrecoceMotivo: TWideStringField
      FieldName = 'Motivo'
      Size = 70
    end
    object QrCHPrecoceObservacao: TWideStringField
      FieldName = 'Observacao'
      Size = 170
    end
    object QrCHPrecoceObsGerais: TWideStringField
      FieldName = 'ObsGerais'
      Size = 60
    end
    object QrCHPrecoceCliente_1: TIntegerField
      FieldName = 'Cliente_1'
    end
    object QrCHPrecoceAlterWeb_1: TSmallintField
      FieldName = 'AlterWeb_1'
    end
    object QrCHPrecoceTeste: TIntegerField
      FieldName = 'Teste'
    end
  end
  object DsCHPrecoce: TDataSource
    DataSet = QrCHPrecoce
    Left = 168
    Top = 140
  end
  object QrEmcart_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT IF(li.Repassado = 0, 0, IF(re.Coligado=:P0, 1, 2)) SitCol' +
        ','
      'lo.Tipo, SUM(li.Valor) Valor'
      'FROM lotesits li'
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN alinits ai ON ai.ChequeOrigem=li.Controle'
      'LEFT JOIN repasits ri ON ri.Origem=li.Controle '
      'LEFT JOIN repas    re ON re.Codigo=ri.Codigo'
      'WHERE li.DCompra <= :P1'
      'AND li.DDeposito >= :P2'
      'AND ai.ChequeOrigem IS NULL'
      'GROUP BY lo.Tipo, SitCol')
    Left = 672
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEmcart_SitCol: TLargeintField
      FieldName = 'SitCol'
      Required = True
    end
    object QrEmcart_Tipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmcart_Valor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrDUVencido: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(li.Valor) Valor'
      ''
      'FROM lotesits li'
      'LEFT JOIN lotes    lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits ri ON ri.Origem=li.Controle '
      'LEFT JOIN repas    re ON re.Codigo=ri.Codigo'
      'WHERE lo.Tipo=1'
      'AND li.DDeposito < :P0'
      'AND ((li.Data3 > :P1) OR (li.Data3 = 0))'
      'AND li.Quitado <> -3'
      ''
      'GROUP BY lo.Tipo, Coligado')
    Left = 644
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDUVencidoValor: TFloatField
      FieldName = 'Valor'
    end
    object QrDUVencidoColigado: TLargeintField
      FieldName = 'Coligado'
    end
    object QrDUVencidoTipo: TSmallintField
      FieldName = 'Tipo'
    end
  end
  object QrDUMoroso: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(li.Valor) Valor'
      ''
      'FROM lotesits li'
      'LEFT JOIN lotes    lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits ri ON ri.Origem=li.Controle '
      'LEFT JOIN repas    re ON re.Codigo=ri.Codigo'
      'WHERE lo.Tipo=1'
      'AND li.DDeposito < :P0'
      'AND ((li.Data3 > :P1) OR (li.Data3 = 0))'
      'AND li.Quitado = -3'
      ''
      'GROUP BY lo.Tipo, Coligado')
    Left = 644
    Top = 276
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDUMorosoColigado: TLargeintField
      FieldName = 'Coligado'
    end
    object QrDUMorosoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDUMorosoValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrDUVencPag: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(ad.Pago) Valor'
      'FROM aduppgs ad'
      'LEFT JOIN lotesits  li ON li.Controle=ad.LotesIts'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits ri ON ri.Origem=li.Controle '
      'LEFT JOIN repas    re ON re.Codigo=ri.Codigo'
      'WHERE lo.Tipo=1'
      'AND li.DDeposito < :P0'
      'AND ((li.Data3 > :P1) OR (li.Data3 = 0))'
      'AND li.Quitado <> -3'
      'AND ad.Data <= :P2'
      'GROUP BY lo.Tipo, Coligado')
    Left = 672
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrDUVencPagColigado: TLargeintField
      FieldName = 'Coligado'
    end
    object QrDUVencPagTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDUVencPagValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrDUMoroPag: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(ad.Pago) Valor'
      'FROM aduppgs ad'
      'LEFT JOIN lotesits  li ON li.Controle=ad.LotesIts'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits ri ON ri.Origem=li.Controle '
      'LEFT JOIN repas    re ON re.Codigo=ri.Codigo'
      'WHERE lo.Tipo=1'
      'AND li.DDeposito < :P0'
      'AND ((li.Data3 > :P1) OR (li.Data3 = 0))'
      'AND li.Quitado = -3'
      'AND ad.Data <= :P2'
      'GROUP BY lo.Tipo, Coligado')
    Left = 672
    Top = 276
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrDUMoroPagColigado: TLargeintField
      FieldName = 'Coligado'
    end
    object QrDUMoroPagTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrDUMoroPagValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrEmcart: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(li.Valor) Valor'
      'FROM lotesits li'
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN alinits ai ON ai.ChequeOrigem=li.Controle'
      'LEFT JOIN repasits ri ON ri.Origem=li.Controle '
      'LEFT JOIN repas    re ON re.Codigo=ri.Codigo'
      'WHERE li.DCompra <= :P1'
      'AND li.DDeposito >= :P2'
      'AND ai.ChequeOrigem IS NULL'
      'GROUP BY lo.Tipo, Coligado')
    Left = 644
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrEmcartColigado: TLargeintField
      FieldName = 'Coligado'
    end
    object QrEmcartTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrEmcartValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCHVencido: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(li.Valor) Valor'
      'FROM lotesits li'
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits  ri ON ri.Origem=li.Controle'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle'
      'WHERE lo.Tipo=0'
      'AND li.DCompra < :P0'
      'AND li.DDeposito < :P1'
      'AND ai.ChequeOrigem <> 0'
      'AND ( (ai.Data3 = 0) OR (ai.Data3 > :P2) )'
      'AND li.Quitado<>-3'
      'GROUP BY lo.Tipo, Coligado'
      '')
    Left = 644
    Top = 304
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCHVencidoColigado: TLargeintField
      FieldName = 'Coligado'
    end
    object QrCHVencidoTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCHVencidoValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCHVencPag: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(ap.Pago) Valor'
      'FROM lotesits li'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits  ri ON ri.Origem=li.Controle'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle'
      'LEFT JOIN alinpgs   ap ON ap.AlinIts=ai.Codigo'
      'WHERE lo.Tipo=0'
      'AND li.DCompra < :P0'
      'AND li.DDeposito < :P1'
      'AND ai.ChequeOrigem <> 0'
      'AND ( (ai.Data3 = 0) OR (ai.Data3 > :P2) )'
      'AND ap.Pago <> 0'
      'AND li.Quitado<>-3'
      'GROUP BY lo.Tipo, Coligado')
    Left = 672
    Top = 304
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCHVencPagColigado: TLargeintField
      FieldName = 'Coligado'
    end
    object QrCHVencPagTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCHVencPagValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCHMoroso: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(li.Valor) Valor'
      'FROM lotesits li'
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits  ri ON ri.Origem=li.Controle'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle'
      'WHERE lo.Tipo=0'
      'AND li.DCompra < :P0'
      'AND li.DDeposito < :P1'
      'AND ai.ChequeOrigem <> 0'
      'AND ( (ai.Data3 = 0) OR (ai.Data3 > :P2) )'
      'AND li.Quitado=-3'
      'GROUP BY lo.Tipo, Coligado'
      '')
    Left = 644
    Top = 332
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object LargeintField1: TLargeintField
      FieldName = 'Coligado'
    end
    object SmallintField1: TSmallintField
      FieldName = 'Tipo'
    end
    object FloatField1: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCHMoroPag: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(li.Repassado = 0, 0, re.Coligado) Coligado,'
      'lo.Tipo, SUM(ap.Pago) Valor'
      'FROM lotesits li'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN repasits  ri ON ri.Origem=li.Controle'
      'LEFT JOIN repas     re ON re.Codigo=ri.Codigo'
      'LEFT JOIN alinits   ai ON ai.ChequeOrigem=li.Controle'
      'LEFT JOIN alinpgs   ap ON ap.AlinIts=ai.Codigo'
      'WHERE lo.Tipo=0'
      'AND li.DCompra < :P0'
      'AND li.DDeposito < :P1'
      'AND ai.ChequeOrigem <> 0'
      'AND ( (ai.Data3 = 0) OR (ai.Data3 > :P2) )'
      'AND ap.Pago <> 0'
      'AND li.Quitado=-3'
      'GROUP BY lo.Tipo, Coligado')
    Left = 672
    Top = 332
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object LargeintField2: TLargeintField
      FieldName = 'Coligado'
    end
    object SmallintField2: TSmallintField
      FieldName = 'Tipo'
    end
    object FloatField2: TFloatField
      FieldName = 'Valor'
    end
  end
end
