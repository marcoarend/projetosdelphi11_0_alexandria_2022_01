object FmLotesRepCliMul: TFmLotesRepCliMul
  Left = 360
  Top = 177
  Caption = 'Pagamento com Cheques de Terceiros'
  ClientHeight = 496
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 448
    Width = 792
    Height = 48
    Align = alBottom
    TabOrder = 0
    object Label16: TLabel
      Left = 508
      Top = 4
      Width = 46
      Height = 13
      Caption = 'Em caixa:'
      FocusControl = DBEdit2
    end
    object BtOK: TBitBtn
      Tag = 20
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Pagar'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel1: TPanel
      Left = 676
      Top = 1
      Width = 115
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 6
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BitBtn1: TBitBtn
      Tag = 301
      Left = 204
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Depositar'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BitBtn1Click
    end
    object BitBtn2: TBitBtn
      Tag = 293
      Left = 388
      Top = 4
      Width = 90
      Height = 40
      Caption = 'So&bra'
      NumGlyphs = 2
      TabOrder = 3
      OnClick = BitBtn2Click
    end
    object DBEdit2: TDBEdit
      Left = 508
      Top = 20
      Width = 76
      Height = 21
      DataField = 'TOTAL'
      DataSource = DsCaixa
      TabOrder = 4
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Pagamento com Cheques de Terceiros'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 790
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 792
    Height = 400
    ActivePage = TabSheet3
    Align = alClient
    TabOrder = 2
    object TabSheet1: TTabSheet
      Caption = 'Leitora CMC7'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 372
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 784
          Height = 101
          Align = alTop
          TabOrder = 1
          object Panel5: TPanel
            Left = 571
            Top = 1
            Width = 212
            Height = 99
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 0
            object Panel7: TPanel
              Left = 0
              Top = 0
              Width = 101
              Height = 99
              Align = alLeft
              BevelOuter = bvNone
              TabOrder = 0
              object CkDescende1: TCheckBox
                Left = 4
                Top = 12
                Width = 89
                Height = 17
                Caption = 'Descendente.'
                TabOrder = 0
              end
              object CkDescende2: TCheckBox
                Left = 4
                Top = 44
                Width = 89
                Height = 17
                Caption = 'Descendente.'
                Checked = True
                State = cbChecked
                TabOrder = 1
              end
              object CkDescende3: TCheckBox
                Left = 4
                Top = 76
                Width = 89
                Height = 17
                Caption = 'Descendente.'
                TabOrder = 2
              end
            end
            object Panel8: TPanel
              Left = 101
              Top = 0
              Width = 111
              Height = 99
              Align = alClient
              BevelOuter = bvLowered
              TabOrder = 1
              object Label24: TLabel
                Left = 6
                Top = 4
                Width = 95
                Height = 13
                Caption = 'Soma selecionados:'
              end
              object Label1: TLabel
                Left = 6
                Top = 50
                Width = 9
                Height = 13
                Caption = '(-)'
              end
              object Label2: TLabel
                Left = 6
                Top = 76
                Width = 6
                Height = 13
                Caption = '='
              end
              object EdSoma: TdmkEdit
                Left = 6
                Top = 20
                Width = 97
                Height = 21
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 0
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnChange = EdSomaChange
              end
              object EdFalta0: TDBEdit
                Left = 20
                Top = 46
                Width = 83
                Height = 21
                DataField = 'SALDOAPAGAR'
                DataSource = FmLotes1.DsLotes
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                TabOrder = 1
                OnChange = EdFalta0Change
              end
              object EdSaldo: TdmkEdit
                Left = 20
                Top = 72
                Width = 83
                Height = 21
                Alignment = taRightJustify
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = [fsBold]
                ParentFont = False
                ReadOnly = True
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
            end
          end
          object Panel6: TPanel
            Left = 1
            Top = 1
            Width = 570
            Height = 99
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object RGOrdem3: TRadioGroup
              Left = 0
              Top = 32
              Width = 570
              Height = 32
              Align = alTop
              Caption = ' Ordem 3: '
              Columns = 6
              ItemIndex = 1
              Items.Strings = (
                'Vencimento'
                'Valor'
                'Cheque'
                'BAC'
                'Emitente'
                'Cliente')
              TabOrder = 0
              OnClick = RGOrdem1Click
            end
            object RGOrdem2: TRadioGroup
              Left = 0
              Top = 64
              Width = 570
              Height = 32
              Align = alTop
              Caption = ' Ordem 2: '
              Columns = 6
              ItemIndex = 2
              Items.Strings = (
                'Vencimento'
                'Valor'
                'Cheque'
                'BAC'
                'Emitente'
                'Cliente')
              TabOrder = 1
              OnClick = RGOrdem1Click
            end
            object RGOrdem1: TRadioGroup
              Left = 0
              Top = 0
              Width = 570
              Height = 32
              Align = alTop
              Caption = ' Ordem 1: '
              Columns = 6
              ItemIndex = 0
              Items.Strings = (
                'Vencimento'
                'Valor'
                'Cheque'
                'BAC'
                'Emitente'
                'Cliente')
              TabOrder = 2
              OnClick = RGOrdem1Click
            end
          end
        end
        object Panel3: TPanel
          Left = 0
          Top = 101
          Width = 784
          Height = 271
          Align = alClient
          TabOrder = 0
          object DBG1: TDBGrid
            Left = 1
            Top = 1
            Width = 782
            Height = 180
            Align = alClient
            DataSource = DsCheque7
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnCellClick = DBG1CellClick
            OnKeyDown = DBG1KeyDown
            Columns = <
              item
                Expanded = False
                FieldName = 'DDeposito'
                Title.Caption = 'Vencto'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Cheque'
                Width = 56
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Banco'
                Title.Caption = 'Bco'
                Width = 24
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Agencia'
                Title.Caption = 'Agen.'
                Width = 32
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Conta'
                Width = 100
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Emitente'
                Width = 208
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECLI'
                Title.Caption = 'Cliente'
                Width = 208
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Lote'
                Title.Caption = 'Border'#244
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'Lote'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Controle'
                Visible = True
              end>
          end
          object Panel9: TPanel
            Left = 1
            Top = 181
            Width = 782
            Height = 89
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 0
            TabStop = True
            object Panel10: TPanel
              Left = 0
              Top = 0
              Width = 782
              Height = 89
              Align = alClient
              TabOrder = 0
              object Label43: TLabel
                Left = 4
                Top = 4
                Width = 143
                Height = 13
                Caption = 'Leitura pela banda magn'#233'tica:'
              end
              object Label49: TLabel
                Left = 4
                Top = 44
                Width = 56
                Height = 13
                Caption = 'CGC / CPF:'
              end
              object Label50: TLabel
                Left = 164
                Top = 44
                Width = 44
                Height = 13
                Caption = 'Emitente:'
              end
              object Label44: TLabel
                Left = 256
                Top = 4
                Width = 27
                Height = 13
                Caption = 'Com.:'
              end
              object Label45: TLabel
                Left = 288
                Top = 4
                Width = 22
                Height = 13
                Caption = 'Bco:'
              end
              object Label46: TLabel
                Left = 320
                Top = 4
                Width = 34
                Height = 13
                Caption = 'Ag'#234'nc:'
              end
              object Label47: TLabel
                Left = 360
                Top = 4
                Width = 55
                Height = 13
                Caption = 'Conta corr.:'
              end
              object Label48: TLabel
                Left = 436
                Top = 4
                Width = 45
                Height = 13
                Caption = 'N'#186' cheq.:'
              end
              object Label42: TLabel
                Left = 492
                Top = 4
                Width = 27
                Height = 13
                Caption = 'Valor:'
              end
              object Label3: TLabel
                Left = 572
                Top = 4
                Width = 59
                Height = 13
                Caption = 'Vencimento:'
              end
              object EdCMC_7_1: TEdit
                Left = 132
                Top = 16
                Width = 121
                Height = 21
                MaxLength = 30
                TabOrder = 0
                Visible = False
                OnChange = EdCMC_7_1Change
              end
              object EdCPF_2: TdmkEdit
                Left = 4
                Top = 60
                Width = 157
                Height = 21
                Alignment = taCenter
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 1
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdEmitente2: TdmkEdit
                Left = 164
                Top = 60
                Width = 481
                Height = 21
                CharCase = ecUpperCase
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 2
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
              end
              object EdRegiaoCompe: TdmkEdit
                Left = 256
                Top = 20
                Width = 29
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 3
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 3
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
              end
              object EdBanco: TdmkEdit
                Left = 288
                Top = 20
                Width = 29
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 4
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 3
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
              end
              object EdAgencia: TdmkEdit
                Left = 320
                Top = 20
                Width = 37
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 5
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 4
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
              end
              object EdConta: TdmkEdit
                Left = 360
                Top = 20
                Width = 73
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 6
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 10
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0000000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
              end
              object EdRealCC: TdmkEdit
                Left = 360
                Top = 40
                Width = 73
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 7
                Visible = False
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 10
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0000000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
              end
              object EdCheque: TdmkEdit
                Left = 436
                Top = 20
                Width = 53
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 8
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 6
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
              end
              object EdValor: TdmkEdit
                Left = 492
                Top = 20
                Width = 77
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 9
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
              end
              object EdVencto: TdmkEdit
                Left = 572
                Top = 20
                Width = 73
                Height = 21
                Alignment = taRightJustify
                Enabled = False
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -11
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                ParentFont = False
                TabOrder = 10
                FormatType = dmktfInteger
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 6
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '000000'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0
              end
              object EdBanda: TdmkEdit
                Left = 4
                Top = 20
                Width = 249
                Height = 21
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -13
                Font.Name = 'MS Sans Serif'
                Font.Style = []
                MaxLength = 34
                ParentFont = False
                TabOrder = 11
                FormatType = dmktfString
                MskType = fmtNone
                DecimalSize = 0
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = ''
                OnChange = EdBandaChange
              end
            end
          end
        end
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Pesquisa manual'
      ImageIndex = 1
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel11: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 65
        Align = alTop
        TabOrder = 0
        object Label5: TLabel
          Left = 364
          Top = 4
          Width = 56
          Height = 13
          Caption = 'CGC / CPF:'
        end
        object Label6: TLabel
          Left = 524
          Top = 4
          Width = 44
          Height = 13
          Caption = 'Emitente:'
        end
        object Label8: TLabel
          Left = 4
          Top = 4
          Width = 22
          Height = 13
          Caption = 'Bco:'
        end
        object Label9: TLabel
          Left = 36
          Top = 4
          Width = 34
          Height = 13
          Caption = 'Ag'#234'nc:'
        end
        object Label10: TLabel
          Left = 76
          Top = 4
          Width = 55
          Height = 13
          Caption = 'Conta corr.:'
        end
        object Label11: TLabel
          Left = 152
          Top = 4
          Width = 45
          Height = 13
          Caption = 'N'#186' cheq.:'
        end
        object Label12: TLabel
          Left = 208
          Top = 4
          Width = 27
          Height = 13
          Caption = 'Valor:'
        end
        object Label13: TLabel
          Left = 288
          Top = 4
          Width = 59
          Height = 13
          Caption = 'Vencimento:'
        end
        object EdCPFM: TdmkEdit
          Left = 364
          Top = 20
          Width = 157
          Height = 21
          Alignment = taCenter
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 6
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnExit = EdCPFMExit
        end
        object EdEmitM: TdmkEdit
          Left = 524
          Top = 20
          Width = 253
          Height = 21
          CharCase = ecUpperCase
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 7
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          OnExit = EdEmitMExit
        end
        object EdBancoM: TdmkEdit
          Left = 4
          Top = 20
          Width = 29
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 3
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnExit = EdBancoMExit
        end
        object EdAgenciaM: TdmkEdit
          Left = 36
          Top = 20
          Width = 37
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 4
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnExit = EdAgenciaMExit
        end
        object EdContaM: TdmkEdit
          Left = 76
          Top = 20
          Width = 73
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 10
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0000000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnExit = EdContaMExit
        end
        object EdChequeM: TdmkEdit
          Left = 152
          Top = 20
          Width = 53
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 6
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnExit = EdChequeMExit
        end
        object EdValorM: TdmkEdit
          Left = 208
          Top = 20
          Width = 77
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnExit = EdValorMExit
        end
        object EdVctoM: TdmkEdit
          Left = 288
          Top = 20
          Width = 73
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 5
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 6
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '000000'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          OnExit = EdVctoMExit
        end
        object CkDepositados: TCheckBox
          Left = 4
          Top = 44
          Width = 773
          Height = 17
          Caption = 'Incluir cheques depositados.'
          TabOrder = 8
          OnClick = CkDepositadosClick
        end
      end
      object DBG2: TDBGrid
        Left = 0
        Top = 65
        Width = 784
        Height = 307
        Align = alClient
        DataSource = DsChequeM
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnKeyDown = DBG1KeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'DDeposito'
            Title.Caption = 'Vencto'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cheque'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Banco'
            Title.Caption = 'Bco'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Agencia'
            Title.Caption = 'Agen.'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Conta'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Emitente'
            Width = 208
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECLI'
            Title.Caption = 'Cliente'
            Width = 208
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Lote'
            Title.Caption = 'Border'#244
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Lote'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Visible = True
          end>
      end
    end
    object TabSheet3: TTabSheet
      Caption = 'Pesquisa Valor'
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel12: TPanel
        Left = 0
        Top = 0
        Width = 784
        Height = 101
        Align = alTop
        TabOrder = 0
        object Label4: TLabel
          Left = 4
          Top = 4
          Width = 27
          Height = 13
          Caption = 'Valor:'
        end
        object LaAvisos: TLabel
          Left = 8
          Top = 52
          Width = 74
          Height = 24
          Caption = 'AVISOS'
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -21
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
        end
        object EdValorV: TdmkEdit
          Left = 4
          Top = 20
          Width = 77
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnExit = EdValorVExit
        end
        object CkAuto: TCheckBox
          Left = 88
          Top = 24
          Width = 273
          Height = 17
          Caption = 'Autom'#225'tico (Quando apenas um cheque encontrado).'
          Checked = True
          State = cbChecked
          TabOrder = 1
        end
        object Panel13: TPanel
          Left = 664
          Top = 1
          Width = 119
          Height = 99
          Align = alRight
          BevelOuter = bvLowered
          TabOrder = 2
          object Label7: TLabel
            Left = 6
            Top = 4
            Width = 78
            Height = 13
            Caption = 'Saldo pendente:'
          end
          object Label14: TLabel
            Left = 6
            Top = 50
            Width = 9
            Height = 13
            Caption = '(-)'
          end
          object Label15: TLabel
            Left = 6
            Top = 76
            Width = 6
            Height = 13
            Caption = '='
            Visible = False
          end
          object EdSoma2: TdmkEdit
            Left = 6
            Top = 20
            Width = 97
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            OnChange = EdSomaChange
          end
          object EdFalta2: TDBEdit
            Left = 20
            Top = 46
            Width = 83
            Height = 21
            DataField = 'SALDOAPAGAR'
            DataSource = FmLotes1.DsLotes
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 1
            OnChange = EdFalta0Change
          end
          object EdSaldo2: TdmkEdit
            Left = 20
            Top = 72
            Width = 83
            Height = 21
            Alignment = taRightJustify
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ReadOnly = True
            TabOrder = 2
            Visible = False
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 2
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,00'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
          end
        end
      end
      object DBG3: TDBGrid
        Left = 0
        Top = 101
        Width = 784
        Height = 271
        Align = alClient
        DataSource = DsChequeV
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
        TabOrder = 1
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnKeyDown = DBG1KeyDown
        Columns = <
          item
            Expanded = False
            FieldName = 'DDeposito'
            Title.Caption = 'Vencto'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Valor'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Cheque'
            Width = 56
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Banco'
            Title.Caption = 'Bco'
            Width = 24
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Agencia'
            Title.Caption = 'Agen.'
            Width = 32
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Conta'
            Width = 100
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Emitente'
            Width = 208
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMECLI'
            Title.Caption = 'Cliente'
            Width = 208
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Lote'
            Title.Caption = 'Border'#244
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Codigo'
            Title.Caption = 'Lote'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'Controle'
            Visible = True
          end>
      end
    end
  end
  object QrCheque7: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMECLI, lot.Lote, loi.*'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE lot.Tipo = 0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Depositado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.DDeposito <= :P0')
    Left = 20
    Top = 220
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCheque7Codigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCheque7Controle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCheque7Comp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrCheque7Banco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrCheque7Agencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrCheque7Conta: TWideStringField
      FieldName = 'Conta'
    end
    object QrCheque7Cheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrCheque7CPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrCheque7Emitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrCheque7Bruto: TFloatField
      FieldName = 'Bruto'
      Required = True
    end
    object QrCheque7Desco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrCheque7Valor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCheque7Emissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrCheque7DCompra: TDateField
      FieldName = 'DCompra'
      Required = True
    end
    object QrCheque7DDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCheque7Vencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCheque7TxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrCheque7TxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrCheque7TxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrCheque7VlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrCheque7VlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrCheque7DMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrCheque7Dias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrCheque7Duplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrCheque7Devolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrCheque7Quitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrCheque7Lk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCheque7DataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCheque7DataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCheque7UserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCheque7UserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCheque7Praca: TIntegerField
      FieldName = 'Praca'
      Required = True
    end
    object QrCheque7BcoCobra: TIntegerField
      FieldName = 'BcoCobra'
      Required = True
    end
    object QrCheque7AgeCobra: TIntegerField
      FieldName = 'AgeCobra'
      Required = True
    end
    object QrCheque7TotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrCheque7TotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrCheque7TotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrCheque7Data3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrCheque7ProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrCheque7ProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrCheque7Repassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrCheque7Depositado: TSmallintField
      FieldName = 'Depositado'
      Required = True
    end
    object QrCheque7ValQuit: TFloatField
      FieldName = 'ValQuit'
      Required = True
    end
    object QrCheque7ValDeposito: TFloatField
      FieldName = 'ValDeposito'
      Required = True
    end
    object QrCheque7Tipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCheque7AliIts: TIntegerField
      FieldName = 'AliIts'
      Required = True
    end
    object QrCheque7AlinPgs: TIntegerField
      FieldName = 'AlinPgs'
      Required = True
    end
    object QrCheque7NaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
      Required = True
    end
    object QrCheque7ReforcoCxa: TSmallintField
      FieldName = 'ReforcoCxa'
      Required = True
    end
    object QrCheque7CartDep: TIntegerField
      FieldName = 'CartDep'
      Required = True
    end
    object QrCheque7Cobranca: TIntegerField
      FieldName = 'Cobranca'
      Required = True
    end
    object QrCheque7RepCli: TIntegerField
      FieldName = 'RepCli'
      Required = True
    end
    object QrCheque7NOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCheque7Lote: TIntegerField
      FieldName = 'Lote'
    end
  end
  object DsCheque7: TDataSource
    DataSet = QrCheque7
    Left = 48
    Top = 220
  end
  object Timer1: TTimer
    Interval = 300
    OnTimer = Timer1Timer
    Left = 332
    Top = 205
  end
  object QrChequeM: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMECLI, lot.Lote, loi.*'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE lot.Tipo = 0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Depositado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.DDeposito <= :P0')
    Left = 20
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrChequeMNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrChequeMLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrChequeMCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrChequeMControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrChequeMComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrChequeMBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrChequeMAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrChequeMConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrChequeMCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrChequeMCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrChequeMEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrChequeMBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
    end
    object QrChequeMDesco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrChequeMValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrChequeMEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrChequeMDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
    end
    object QrChequeMDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
    end
    object QrChequeMVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequeMTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrChequeMTxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrChequeMTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrChequeMVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrChequeMVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrChequeMDMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrChequeMDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrChequeMDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrChequeMDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrChequeMQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrChequeMLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrChequeMDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChequeMDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChequeMUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrChequeMUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrChequeMPraca: TIntegerField
      FieldName = 'Praca'
      Required = True
    end
    object QrChequeMBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
      Required = True
    end
    object QrChequeMAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
      Required = True
    end
    object QrChequeMTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrChequeMTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrChequeMTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrChequeMData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrChequeMProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrChequeMProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrChequeMRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrChequeMDepositado: TSmallintField
      FieldName = 'Depositado'
      Required = True
    end
    object QrChequeMValQuit: TFloatField
      FieldName = 'ValQuit'
      Required = True
    end
    object QrChequeMValDeposito: TFloatField
      FieldName = 'ValDeposito'
      Required = True
    end
    object QrChequeMTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrChequeMAliIts: TIntegerField
      FieldName = 'AliIts'
      Required = True
    end
    object QrChequeMAlinPgs: TIntegerField
      FieldName = 'AlinPgs'
      Required = True
    end
    object QrChequeMNaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
      Required = True
    end
    object QrChequeMReforcoCxa: TSmallintField
      FieldName = 'ReforcoCxa'
      Required = True
    end
    object QrChequeMCartDep: TIntegerField
      FieldName = 'CartDep'
      Required = True
    end
    object QrChequeMCobranca: TIntegerField
      FieldName = 'Cobranca'
      Required = True
    end
    object QrChequeMRepCli: TIntegerField
      FieldName = 'RepCli'
      Required = True
    end
    object QrChequeMMotivo: TWideStringField
      FieldName = 'Motivo'
      Size = 70
    end
    object QrChequeMObservacao: TWideStringField
      FieldName = 'Observacao'
      Size = 170
    end
    object QrChequeMObsGerais: TWideStringField
      FieldName = 'ObsGerais'
      Size = 60
    end
  end
  object DsChequeM: TDataSource
    DataSet = QrChequeM
    Left = 48
    Top = 248
  end
  object QrChequeV: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMECLI, lot.Lote, loi.*'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE lot.Tipo = 0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Depositado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.DDeposito <= :P0')
    Left = 20
    Top = 276
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrChequeVNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrChequeVLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrChequeVCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrChequeVControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrChequeVComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrChequeVBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrChequeVAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrChequeVConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrChequeVCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
    end
    object QrChequeVCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrChequeVEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrChequeVBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
    end
    object QrChequeVDesco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrChequeVValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrChequeVEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrChequeVDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
    end
    object QrChequeVDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequeVVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrChequeVTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrChequeVTxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrChequeVTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrChequeVVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrChequeVVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrChequeVDMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrChequeVDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrChequeVDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrChequeVDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrChequeVQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrChequeVLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrChequeVDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrChequeVDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrChequeVUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrChequeVUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrChequeVPraca: TIntegerField
      FieldName = 'Praca'
      Required = True
    end
    object QrChequeVBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
      Required = True
    end
    object QrChequeVAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
      Required = True
    end
    object QrChequeVTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrChequeVTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrChequeVTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrChequeVData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrChequeVProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrChequeVProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrChequeVRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrChequeVDepositado: TSmallintField
      FieldName = 'Depositado'
      Required = True
    end
    object QrChequeVValQuit: TFloatField
      FieldName = 'ValQuit'
      Required = True
    end
    object QrChequeVValDeposito: TFloatField
      FieldName = 'ValDeposito'
      Required = True
    end
    object QrChequeVTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrChequeVAliIts: TIntegerField
      FieldName = 'AliIts'
      Required = True
    end
    object QrChequeVAlinPgs: TIntegerField
      FieldName = 'AlinPgs'
      Required = True
    end
    object QrChequeVNaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
      Required = True
    end
    object QrChequeVReforcoCxa: TSmallintField
      FieldName = 'ReforcoCxa'
      Required = True
    end
    object QrChequeVCartDep: TIntegerField
      FieldName = 'CartDep'
      Required = True
    end
    object QrChequeVCobranca: TIntegerField
      FieldName = 'Cobranca'
      Required = True
    end
    object QrChequeVRepCli: TIntegerField
      FieldName = 'RepCli'
      Required = True
    end
    object QrChequeVMotivo: TWideStringField
      FieldName = 'Motivo'
      Size = 70
    end
    object QrChequeVObservacao: TWideStringField
      FieldName = 'Observacao'
      Size = 170
    end
    object QrChequeVObsGerais: TWideStringField
      FieldName = 'ObsGerais'
      Size = 60
    end
  end
  object DsChequeV: TDataSource
    DataSet = QrChequeV
    Left = 48
    Top = 276
  end
  object QrCaixa: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) TOTAL'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'WHERE lot.Tipo = 0'
      'AND loi.RepCli = 0'
      'AND loi.Repassado = 0'
      'AND loi.Depositado = 0'
      'AND loi.Devolucao = 0'
      'AND loi.DDeposito <= :P0')
    Left = 20
    Top = 304
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCaixaTOTAL: TFloatField
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsCaixa: TDataSource
    DataSet = QrCaixa
    Left = 48
    Top = 304
  end
end
