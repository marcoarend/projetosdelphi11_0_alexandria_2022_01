object FmCNAB240Rem: TFmCNAB240Rem
  Left = 256
  Top = 161
  Caption = 'Remessa CNAB240'
  ClientHeight = 610
  ClientWidth = 1250
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PnTitulos: TPanel
    Left = 0
    Top = 59
    Width = 1250
    Height = 551
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object Panel2: TPanel
      Left = 1
      Top = 491
      Width = 1248
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 10
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Adiciona'
        Default = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BitBtn1Click
      end
      object Panel4: TPanel
        Left = 1113
        Top = 1
        Width = 134
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        Caption = 'Panel4'
        TabOrder = 1
        object BitBtn2: TBitBtn
          Tag = 13
          Left = 10
          Top = 2
          Width = 111
          Height = 50
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sair'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn2Click
        end
      end
    end
    object Panel1: TPanel
      Left = 1
      Top = 1
      Width = 1248
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label4: TLabel
        Left = 20
        Top = 10
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit01
      end
      object Label5: TLabel
        Left = 148
        Top = 10
        Width = 32
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data:'
        FocusControl = DBEdit2
      end
      object Label6: TLabel
        Left = 276
        Top = 34
        Width = 287
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Escolha um ou mais t'#237'tulos e clique em adiciona.'
      end
      object DBEdit01: TDBEdit
        Left = 20
        Top = 30
        Width = 123
        Height = 24
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCobrancaBB
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 148
        Top = 30
        Width = 119
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'MyDATAG'
        DataSource = DsCobrancaBB
        TabOrder = 1
      end
    end
    object DBGrid2: TDBGrid
      Left = 1
      Top = 65
      Width = 1248
      Height = 148
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      DataSource = DsTitulos
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -15
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Duplicata'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 189
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Width = 181
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CPF'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bruto'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Desco'
          Title.Caption = 'Desconto'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DCompra'
          Title.Caption = 'D.Compra'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DDeposito'
          Title.Caption = 'D.Pagto'
          Width = 56
          Visible = True
        end>
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 59
    Width = 1250
    Height = 551
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 491
      Width = 1248
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 10
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel6: TPanel
        Left = 1113
        Top = 1
        Width = 134
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        Caption = 'Panel6'
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 10
          Top = 2
          Width = 111
          Height = 50
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1248
      Height = 241
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 20
        Top = 10
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object Label3: TLabel
        Left = 148
        Top = 10
        Width = 32
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data:'
      end
      object Label7: TLabel
        Left = 350
        Top = 10
        Width = 162
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Configura'#231#227'o de cobran'#231'a:'
      end
      object Label11: TLabel
        Left = 261
        Top = 10
        Width = 33
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Hora:'
      end
      object Label13: TLabel
        Left = 20
        Top = 59
        Width = 81
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Mensagem 1:'
      end
      object Label14: TLabel
        Left = 418
        Top = 59
        Width = 81
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Mensagem 2:'
      end
      object Label15: TLabel
        Left = 25
        Top = 118
        Width = 438
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'As mensagens n'#227'o podem conter caracteres especiais, acentuados, ' +
          'etc. '
      end
      object EdCodigo: TdmkEdit
        Left = 20
        Top = 30
        Width = 123
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object TPDataG: TDateTimePicker
        Left = 148
        Top = 30
        Width = 110
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 39067.403862476900000000
        Time = 39067.403862476900000000
        TabOrder = 1
      end
      object EdConfigBB: TdmkEditCB
        Left = 350
        Top = 30
        Width = 80
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBConfigBB
        IgnoraDBLookupComboBox = False
      end
      object CBConfigBB: TdmkDBLookupComboBox
        Left = 433
        Top = 30
        Width = 385
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsConfigs
        TabOrder = 4
        dmkEditCB = EdConfigBB
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPHoraG: TDateTimePicker
        Left = 261
        Top = 30
        Width = 85
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 39198.000000000000000000
        Time = 39198.000000000000000000
        Kind = dtkTime
        TabOrder = 2
      end
      object EdMensagem1: TEdit
        Left = 20
        Top = 79
        Width = 394
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        CharCase = ecUpperCase
        MaxLength = 40
        TabOrder = 5
      end
      object EdMensagem2: TEdit
        Left = 418
        Top = 79
        Width = 394
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        CharCase = ecUpperCase
        MaxLength = 40
        TabOrder = 6
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 1250
    Height = 551
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Splitter1: TSplitter
      Left = 1
      Top = 361
      Width = 1248
      Height = 3
      Cursor = crVSplit
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
    end
    object PainelControle: TPanel
      Left = 1
      Top = 491
      Width = 1248
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object LaRegistro: TLabel
        Left = 213
        Top = 1
        Width = 457
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = '[N]: 0'
        ExplicitWidth = 31
        ExplicitHeight = 16
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 212
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Hint = 'ltimo registro'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Hint = 'Pr'#243'ximo registro'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Hint = 'Registro anterior'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Hint = 'Primeiro registro'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 670
        Top = 1
        Width = 577
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object Label10: TLabel
          Left = 350
          Top = 5
          Width = 39
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Linhas'
        end
        object BtSaida: TBitBtn
          Tag = 13
          Left = 458
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
        object BtGera: TBitBtn
          Tag = 266
          Left = 231
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Gera '
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtGeraClick
        end
        object BtTitulos: TBitBtn
          Tag = 11
          Left = 118
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&T'#237'tulos'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtTitulosClick
        end
        object BtLotes: TBitBtn
          Tag = 265
          Left = 5
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Lote'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtLotesClick
        end
        object Edit1: TEdit
          Left = 350
          Top = 25
          Width = 84
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabOrder = 4
          Text = '0'
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 1248
      Height = 113
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Panel7: TPanel
        Left = 0
        Top = 0
        Width = 809
        Height = 113
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
        object Label16: TLabel
          Left = 10
          Top = 54
          Width = 81
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Mensagem 1:'
          FocusControl = DBEdit5
        end
        object Label1: TLabel
          Left = 10
          Top = 5
          Width = 47
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label8: TLabel
          Left = 138
          Top = 5
          Width = 162
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Configura'#231#227'o de cobran'#231'a:'
          FocusControl = DBEdit3
        end
        object Label17: TLabel
          Left = 409
          Top = 54
          Width = 81
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Mensagem 2:'
          FocusControl = DBEdit6
        end
        object DBEdit5: TDBEdit
          Left = 10
          Top = 74
          Width = 394
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Mensagem1'
          DataSource = DsCobrancaBB
          TabOrder = 0
        end
        object DBEdCodigo: TDBEdit
          Left = 10
          Top = 25
          Width = 123
          Height = 24
          Hint = 'N'#186' do banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsCobrancaBB
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 138
          Top = 25
          Width = 666
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'NOMECONFIG'
          DataSource = DsCobrancaBB
          TabOrder = 2
        end
        object DBEdit6: TDBEdit
          Left = 409
          Top = 74
          Width = 393
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'Mensagem2'
          DataSource = DsCobrancaBB
          TabOrder = 3
        end
      end
      object GroupBox1: TGroupBox
        Left = 991
        Top = 0
        Width = 182
        Height = 113
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Caption = ' '#218'ltimo envio: '
        TabOrder = 1
        object Label2: TLabel
          Left = 10
          Top = 30
          Width = 32
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Data:'
          FocusControl = DBEdit1
        end
        object Label12: TLabel
          Left = 94
          Top = 30
          Width = 33
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Hora:'
          FocusControl = DBEdit4
        end
        object DBEdit1: TDBEdit
          Left = 10
          Top = 49
          Width = 79
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'MyDATAS'
          DataSource = DsCobrancaBB
          TabOrder = 0
        end
        object DBEdit4: TDBEdit
          Left = 94
          Top = 49
          Width = 78
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'HoraS'
          DataSource = DsCobrancaBB
          TabOrder = 1
        end
      end
      object GroupBox2: TGroupBox
        Left = 809
        Top = 0
        Width = 182
        Height = 113
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Caption = ' Cria'#231#227'o: '
        TabOrder = 2
        object Label18: TLabel
          Left = 10
          Top = 30
          Width = 32
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Data:'
          FocusControl = DBEdit7
        end
        object Label19: TLabel
          Left = 94
          Top = 30
          Width = 33
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Hora:'
          FocusControl = DBEdit8
        end
        object DBEdit7: TDBEdit
          Left = 10
          Top = 49
          Width = 79
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'MyDATAG'
          DataSource = DsCobrancaBB
          TabOrder = 0
        end
        object DBEdit8: TDBEdit
          Left = 94
          Top = 49
          Width = 78
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          DataField = 'HoraG'
          DataSource = DsCobrancaBB
          TabOrder = 1
        end
      end
    end
    object DBGrid1: TDBGrid
      Left = 1
      Top = 114
      Width = 1248
      Height = 148
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      DataSource = DsCobrancaBBIts
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -15
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Lote'
          Width = 48
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Width = 140
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CPF'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bruto'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Desco'
          Width = 62
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Valor'
          Width = 66
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DCompra'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DDeposito'
          Width = 56
          Visible = True
        end>
    end
    object Memo1: TMemo
      Left = 1
      Top = 364
      Width = 1248
      Height = 127
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      WordWrap = False
      OnChange = Memo1Change
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1250
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = '                              Remessa CNAB240'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 1148
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
    end
    object Image1: TImage
      Left = 278
      Top = 1
      Width = 870
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 277
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 4
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsCobrancaBB: TDataSource
    DataSet = QrCobrancaBB
    Left = 444
    Top = 9
  end
  object QrCobrancaBB: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCobrancaBBBeforeOpen
    AfterOpen = QrCobrancaBBAfterOpen
    BeforeClose = QrCobrancaBBBeforeClose
    AfterScroll = QrCobrancaBBAfterScroll
    OnCalcFields = QrCobrancaBBCalcFields
    SQL.Strings = (
      'SELECT con.Nome NOMECONFIG, cob.* '
      'FROM cobrancabb cob'
      'LEFT JOIN configbb con ON con.Codigo=cob.ConfigBB'
      'WHERE cob.Codigo > 0')
    Left = 416
    Top = 9
    object QrCobrancaBBCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCobrancaBBLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCobrancaBBDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCobrancaBBDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCobrancaBBUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCobrancaBBUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCobrancaBBConfigBB: TIntegerField
      FieldName = 'ConfigBB'
    end
    object QrCobrancaBBNOMECONFIG: TWideStringField
      FieldName = 'NOMECONFIG'
      Size = 50
    end
    object QrCobrancaBBMensagem1: TWideStringField
      FieldName = 'Mensagem1'
      Size = 40
    end
    object QrCobrancaBBMensagem2: TWideStringField
      FieldName = 'Mensagem2'
      Size = 40
    end
    object QrCobrancaBBDataG: TDateField
      FieldName = 'DataG'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrCobrancaBBDataS: TDateField
      FieldName = 'DataS'
      Required = True
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrCobrancaBBHoraS: TTimeField
      FieldName = 'HoraS'
      Required = True
      DisplayFormat = '00:00:00'
    end
    object QrCobrancaBBHoraG: TTimeField
      FieldName = 'HoraG'
      Required = True
      DisplayFormat = '00:00:00'
    end
    object QrCobrancaBBMyDATAG: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MyDATAG'
      Size = 10
      Calculated = True
    end
    object QrCobrancaBBMyDATAS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MyDATAS'
      Size = 10
      Calculated = True
    end
  end
  object QrCobrancaBBIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCobrancaBBItsCalcFields
    SQL.Strings = (
      'SELECT lot.Cliente, lot.Lote, ent.MultaCodi, ent.MultaDias, '
      'ent.MultaValr, ent.MultaPerc, ent.MultaTiVe, ent.Protestar, '
      
        'ent.JuroSacado, ent.Tipo TipoCLI, PUF, EUF, uf0.Nome UFE, uf1.No' +
        'me UFP,'
      
        'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOME' +
        'CLI, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.CNPJ    ELSE ent.CPF     END CNPJC' +
        'LI, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ERua    ELSE ent.PRua    END RuaCL' +
        'I, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ENumero+0.000 ELSE ent.PNumero+0.0' +
        '00 END NumCLI, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECompl  ELSE ent.PCompl  END CplCL' +
        'I, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.EBairro ELSE ent.PBairro END BrrCL' +
        'I, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECEP    ELSE ent.PCEP    END + 0.0' +
        '00 CEPCLI, '
      
        'CASE WHEN ent.Tipo=0 THEN ent.ECidade ELSE ent.PCidade END CidCL' +
        'I, '
      
        'ent.Corrido, sac.Numero+0.000 Numero, sac.CEP + 0.000 CEP, sac.*' +
        ', loi.*'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'LEFT JOIN sacados   sac ON sac.CNPJ=loi.CPF'
      'LEFT JOIN ufs       uf0 ON uf0.Codigo=ent.EUF'
      'LEFT JOIN ufs       uf1 ON uf1.Codigo=ent.PUF'
      'WHERE loi.Cobranca=:P0')
    Left = 480
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCobrancaBBItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrCobrancaBBItsLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrCobrancaBBItsNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCobrancaBBItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCobrancaBBItsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCobrancaBBItsComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrCobrancaBBItsBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrCobrancaBBItsAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrCobrancaBBItsConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrCobrancaBBItsCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
    end
    object QrCobrancaBBItsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrCobrancaBBItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrCobrancaBBItsBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCobrancaBBItsDesco: TFloatField
      FieldName = 'Desco'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCobrancaBBItsValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCobrancaBBItsEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCobrancaBBItsDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCobrancaBBItsDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCobrancaBBItsVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCobrancaBBItsTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrCobrancaBBItsTxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrCobrancaBBItsTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrCobrancaBBItsVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrCobrancaBBItsVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrCobrancaBBItsDMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrCobrancaBBItsDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrCobrancaBBItsDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrCobrancaBBItsDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrCobrancaBBItsQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrCobrancaBBItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCobrancaBBItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCobrancaBBItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCobrancaBBItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCobrancaBBItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCobrancaBBItsPraca: TIntegerField
      FieldName = 'Praca'
      Required = True
    end
    object QrCobrancaBBItsBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
      Required = True
    end
    object QrCobrancaBBItsAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
      Required = True
    end
    object QrCobrancaBBItsTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrCobrancaBBItsTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrCobrancaBBItsTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrCobrancaBBItsData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrCobrancaBBItsProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrCobrancaBBItsProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrCobrancaBBItsRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrCobrancaBBItsDepositado: TSmallintField
      FieldName = 'Depositado'
      Required = True
    end
    object QrCobrancaBBItsValQuit: TFloatField
      FieldName = 'ValQuit'
      Required = True
    end
    object QrCobrancaBBItsValDeposito: TFloatField
      FieldName = 'ValDeposito'
      Required = True
    end
    object QrCobrancaBBItsTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrCobrancaBBItsAliIts: TIntegerField
      FieldName = 'AliIts'
      Required = True
    end
    object QrCobrancaBBItsAlinPgs: TIntegerField
      FieldName = 'AlinPgs'
      Required = True
    end
    object QrCobrancaBBItsNaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
      Required = True
    end
    object QrCobrancaBBItsReforcoCxa: TSmallintField
      FieldName = 'ReforcoCxa'
      Required = True
    end
    object QrCobrancaBBItsCartDep: TIntegerField
      FieldName = 'CartDep'
      Required = True
    end
    object QrCobrancaBBItsCobranca: TIntegerField
      FieldName = 'Cobranca'
      Required = True
    end
    object QrCobrancaBBItsMultaCodi: TSmallintField
      FieldName = 'MultaCodi'
    end
    object QrCobrancaBBItsMultaDias: TSmallintField
      FieldName = 'MultaDias'
    end
    object QrCobrancaBBItsMultaValr: TFloatField
      FieldName = 'MultaValr'
    end
    object QrCobrancaBBItsMultaPerc: TFloatField
      FieldName = 'MultaPerc'
    end
    object QrCobrancaBBItsCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Required = True
      Size = 15
    end
    object QrCobrancaBBItsIE: TWideStringField
      FieldName = 'IE'
      Size = 25
    end
    object QrCobrancaBBItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCobrancaBBItsRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrCobrancaBBItsCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrCobrancaBBItsBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrCobrancaBBItsCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrCobrancaBBItsUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrCobrancaBBItsTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrCobrancaBBItsRisco: TFloatField
      FieldName = 'Risco'
    end
    object QrCobrancaBBItsMultaTiVe: TSmallintField
      FieldName = 'MultaTiVe'
    end
    object QrCobrancaBBItsProtestar: TSmallintField
      FieldName = 'Protestar'
    end
    object QrCobrancaBBItsJuroSacado: TFloatField
      FieldName = 'JuroSacado'
    end
    object QrCobrancaBBItsPUF: TSmallintField
      FieldName = 'PUF'
    end
    object QrCobrancaBBItsEUF: TSmallintField
      FieldName = 'EUF'
    end
    object QrCobrancaBBItsUFE: TWideStringField
      FieldName = 'UFE'
      Required = True
      Size = 2
    end
    object QrCobrancaBBItsUFP: TWideStringField
      FieldName = 'UFP'
      Required = True
      Size = 2
    end
    object QrCobrancaBBItsCNPJCLI: TWideStringField
      FieldName = 'CNPJCLI'
      Size = 18
    end
    object QrCobrancaBBItsRuaCLI: TWideStringField
      FieldName = 'RuaCLI'
      Size = 30
    end
    object QrCobrancaBBItsCplCLI: TWideStringField
      FieldName = 'CplCLI'
      Size = 30
    end
    object QrCobrancaBBItsBrrCLI: TWideStringField
      FieldName = 'BrrCLI'
      Size = 30
    end
    object QrCobrancaBBItsCidCLI: TWideStringField
      FieldName = 'CidCLI'
      Size = 25
    end
    object QrCobrancaBBItsTipoCLI: TSmallintField
      FieldName = 'TipoCLI'
    end
    object QrCobrancaBBItsCorrido: TIntegerField
      FieldName = 'Corrido'
    end
    object QrCobrancaBBItsNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrCobrancaBBItsNumCLI: TFloatField
      FieldName = 'NumCLI'
    end
    object QrCobrancaBBItsENDERECO_EMI: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ENDERECO_EMI'
      Size = 255
      Calculated = True
    end
    object QrCobrancaBBItsCEPCLI: TFloatField
      FieldName = 'CEPCLI'
    end
    object QrCobrancaBBItsCEP: TFloatField
      FieldName = 'CEP'
    end
  end
  object DsCobrancaBBIts: TDataSource
    DataSet = QrCobrancaBBIts
    Left = 508
    Top = 9
  end
  object PMLotes: TPopupMenu
    OnPopup = PMLotesPopup
    Left = 576
    Top = 424
    object Crianovolote1: TMenuItem
      Caption = '&Cria novo lote'
      OnClick = Crianovolote1Click
    end
    object Alteraloteatual1: TMenuItem
      Caption = '&Altera lote atual'
      OnClick = Alteraloteatual1Click
    end
    object Excluiloteatual1: TMenuItem
      Caption = '&Exclui lote atual'
      OnClick = Excluiloteatual1Click
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object MoverarquivoparapastaEnviados1: TMenuItem
      Caption = '&Mover arquivo para pasta: Enviados'
      OnClick = MoverarquivoparapastaEnviados1Click
    end
  end
  object PMTitulos: TPopupMenu
    OnPopup = PMTitulosPopup
    Left = 660
    Top = 428
    object Inclui1: TMenuItem
      Caption = '&Entrada no Banco'
      OnClick = Inclui1Click
    end
    object Instruesparabanco1: TMenuItem
      Caption = '&Instru'#231#245'es para banco'
      Enabled = False
      OnClick = Instruesparabanco1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Retira1: TMenuItem
      Caption = '&Remove do lote'
      OnClick = Retira1Click
    end
  end
  object QrTitulos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT lot.Cliente, lot.Lote, CASE WHEN ent.Tipo=0 THEN ent.Raza' +
        'oSocial'
      'ELSE ent.Nome END NOMECLI, loi.*'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE lot.Tipo=1'
      'AND loi.Cobranca=0'
      'AND loi.CNAB_Lot=0')
    Left = 540
    Top = 9
    object QrTitulosCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrTitulosLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrTitulosNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrTitulosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTitulosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrTitulosComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrTitulosBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrTitulosAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrTitulosConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrTitulosCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
    end
    object QrTitulosCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrTitulosEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrTitulosBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTitulosDesco: TFloatField
      FieldName = 'Desco'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTitulosValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTitulosEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTitulosTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrTitulosTxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrTitulosTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrTitulosVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrTitulosVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrTitulosDMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrTitulosDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrTitulosDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrTitulosDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrTitulosQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrTitulosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTitulosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTitulosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTitulosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTitulosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTitulosPraca: TIntegerField
      FieldName = 'Praca'
      Required = True
    end
    object QrTitulosBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
      Required = True
    end
    object QrTitulosAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
      Required = True
    end
    object QrTitulosTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrTitulosTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrTitulosTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrTitulosData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrTitulosProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrTitulosProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrTitulosRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrTitulosDepositado: TSmallintField
      FieldName = 'Depositado'
      Required = True
    end
    object QrTitulosValQuit: TFloatField
      FieldName = 'ValQuit'
      Required = True
    end
    object QrTitulosValDeposito: TFloatField
      FieldName = 'ValDeposito'
      Required = True
    end
    object QrTitulosTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrTitulosAliIts: TIntegerField
      FieldName = 'AliIts'
      Required = True
    end
    object QrTitulosAlinPgs: TIntegerField
      FieldName = 'AlinPgs'
      Required = True
    end
    object QrTitulosNaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
      Required = True
    end
    object QrTitulosReforcoCxa: TSmallintField
      FieldName = 'ReforcoCxa'
      Required = True
    end
    object QrTitulosCartDep: TIntegerField
      FieldName = 'CartDep'
      Required = True
    end
    object QrTitulosCobranca: TIntegerField
      FieldName = 'Cobranca'
      Required = True
    end
  end
  object DsTitulos: TDataSource
    DataSet = QrTitulos
    Left = 568
    Top = 9
  end
  object QrConfigBB: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ban.Nome NOMEBANCO, cbb.*'
      'FROM configbb cbb'
      'LEFT JOIN bancos ban ON ban.Codigo=cbb.Banco '
      'WHERE cbb.Codigo=:P0')
    Left = 596
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrConfigBBCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConfigBBNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrConfigBBConvenio: TIntegerField
      FieldName = 'Convenio'
    end
    object QrConfigBBCarteira: TWideStringField
      FieldName = 'Carteira'
      Size = 2
    end
    object QrConfigBBVariacao: TWideStringField
      FieldName = 'Variacao'
      Size = 3
    end
    object QrConfigBBSiglaEspecie: TWideStringField
      FieldName = 'SiglaEspecie'
      Size = 5
    end
    object QrConfigBBMoeda: TWideStringField
      FieldName = 'Moeda'
      Size = 5
    end
    object QrConfigBBAceite: TSmallintField
      FieldName = 'Aceite'
    end
    object QrConfigBBProtestar: TSmallintField
      FieldName = 'Protestar'
    end
    object QrConfigBBMsgLinha1: TWideStringField
      FieldName = 'MsgLinha1'
      Size = 40
    end
    object QrConfigBBPgAntes: TSmallintField
      FieldName = 'PgAntes'
    end
    object QrConfigBBMultaCodi: TSmallintField
      FieldName = 'MultaCodi'
    end
    object QrConfigBBMultaDias: TSmallintField
      FieldName = 'MultaDias'
    end
    object QrConfigBBMultaValr: TFloatField
      FieldName = 'MultaValr'
    end
    object QrConfigBBMultaPerc: TFloatField
      FieldName = 'MultaPerc'
    end
    object QrConfigBBMultaTiVe: TSmallintField
      FieldName = 'MultaTiVe'
    end
    object QrConfigBBImpreLoc: TSmallintField
      FieldName = 'ImpreLoc'
    end
    object QrConfigBBModalidade: TIntegerField
      FieldName = 'Modalidade'
    end
    object QrConfigBBclcAgencNr: TWideStringField
      FieldName = 'clcAgencNr'
      Size = 4
    end
    object QrConfigBBclcAgencDV: TWideStringField
      FieldName = 'clcAgencDV'
      Size = 1
    end
    object QrConfigBBclcContaNr: TWideStringField
      FieldName = 'clcContaNr'
      Size = 8
    end
    object QrConfigBBclcContaDV: TWideStringField
      FieldName = 'clcContaDV'
      Size = 1
    end
    object QrConfigBBcedAgencNr: TWideStringField
      FieldName = 'cedAgencNr'
      Size = 4
    end
    object QrConfigBBcedAgencDV: TWideStringField
      FieldName = 'cedAgencDV'
      Size = 1
    end
    object QrConfigBBcedContaNr: TWideStringField
      FieldName = 'cedContaNr'
      Size = 8
    end
    object QrConfigBBcedContaDV: TWideStringField
      FieldName = 'cedContaDV'
      Size = 1
    end
    object QrConfigBBEspecie: TSmallintField
      FieldName = 'Especie'
    end
    object QrConfigBBCorrido: TSmallintField
      FieldName = 'Corrido'
    end
    object QrConfigBBBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrConfigBBIDEmpresa: TWideStringField
      FieldName = 'IDEmpresa'
    end
    object QrConfigBBProduto: TWideStringField
      FieldName = 'Produto'
      Size = 4
    end
    object QrConfigBBNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrConfigBBInfoCovH: TSmallintField
      FieldName = 'InfoCovH'
      Required = True
    end
    object QrConfigBBCarteira240: TWideStringField
      FieldName = 'Carteira240'
      Size = 1
    end
    object QrConfigBBCadastramento: TWideStringField
      FieldName = 'Cadastramento'
      Size = 1
    end
    object QrConfigBBTradiEscrit: TWideStringField
      FieldName = 'TradiEscrit'
      Size = 1
    end
    object QrConfigBBDistribuicao: TWideStringField
      FieldName = 'Distribuicao'
      Size = 1
    end
    object QrConfigBBAceite240: TWideStringField
      FieldName = 'Aceite240'
      Size = 1
    end
    object QrConfigBBProtesto: TWideStringField
      FieldName = 'Protesto'
      Size = 1
    end
    object QrConfigBBProtestodd: TIntegerField
      FieldName = 'Protestodd'
      Required = True
    end
    object QrConfigBBBaixaDevol: TWideStringField
      FieldName = 'BaixaDevol'
      Size = 1
    end
    object QrConfigBBBaixaDevoldd: TIntegerField
      FieldName = 'BaixaDevoldd'
      Required = True
    end
    object QrConfigBBLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrConfigBBDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrConfigBBDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrConfigBBUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrConfigBBUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrConfigBBEmisBloqueto: TWideStringField
      FieldName = 'EmisBloqueto'
      Size = 1
    end
    object QrConfigBBEspecie240: TWideStringField
      FieldName = 'Especie240'
      Size = 2
    end
    object QrConfigBBJuros240Cod: TWideStringField
      FieldName = 'Juros240Cod'
      Size = 1
    end
    object QrConfigBBJuros240Qtd: TFloatField
      FieldName = 'Juros240Qtd'
      Required = True
    end
    object QrConfigBBContrOperCred: TIntegerField
      FieldName = 'ContrOperCred'
      Required = True
    end
    object QrConfigBBReservBanco: TWideStringField
      FieldName = 'ReservBanco'
    end
    object QrConfigBBReservEmprs: TWideStringField
      FieldName = 'ReservEmprs'
    end
    object QrConfigBBLH_208_33: TWideStringField
      FieldName = 'LH_208_33'
      Size = 33
    end
    object QrConfigBBSQ_233_008: TWideStringField
      FieldName = 'SQ_233_008'
      Size = 8
    end
    object QrConfigBBTL_124_117: TWideStringField
      FieldName = 'TL_124_117'
      Size = 117
    end
    object QrConfigBBSR_208_033: TWideStringField
      FieldName = 'SR_208_033'
      Size = 33
    end
    object QrConfigBBDiretorio: TWideStringField
      FieldName = 'Diretorio'
      Size = 255
    end
  end
  object QrConfigs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM configbb')
    Left = 389
    Top = 121
    object QrConfigsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConfigsNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsConfigs: TDataSource
    DataSet = QrConfigs
    Left = 417
    Top = 121
  end
end
