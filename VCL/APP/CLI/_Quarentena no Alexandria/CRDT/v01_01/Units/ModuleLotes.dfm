object DmLotes: TDmLotes
  OldCreateOrder = False
  Height = 627
  Width = 1026
  object QrSPC_Cli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sm.Valor VALCONSULTA,sc.Codigo, sp.ValMin SPC_ValMin, '
      'sp.ValMax SPC_ValMax, sc.Servidor, sc.Porta SPC_Porta, '
      'sc.Pedinte, sc.CodigSocio, sc.SenhaSocio, sc.Modalidade, '
      'sc.InfoExtra, sc.BAC_CMC7, sc.TipoCred, ValAviso'
      'FROM spc_config sc '
      'LEFT JOIN spc_entida sp ON sp.SPC_Config=sc.Codigo '
      'LEFT JOIN spc_modali sm ON sm.Codigo=sc.Modalidade '
      'WHERE sp.Entidade=:P0')
    Left = 28
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSPC_CliSPC_ValMin: TFloatField
      FieldName = 'SPC_ValMin'
    end
    object QrSPC_CliSPC_ValMax: TFloatField
      FieldName = 'SPC_ValMax'
    end
    object QrSPC_CliServidor: TWideStringField
      FieldName = 'Servidor'
      Size = 100
    end
    object QrSPC_CliSPC_Porta: TSmallintField
      FieldName = 'SPC_Porta'
      Required = True
    end
    object QrSPC_CliPedinte: TWideStringField
      FieldName = 'Pedinte'
      Size = 15
    end
    object QrSPC_CliCodigSocio: TIntegerField
      FieldName = 'CodigSocio'
      Required = True
    end
    object QrSPC_CliModalidade: TIntegerField
      FieldName = 'Modalidade'
      Required = True
    end
    object QrSPC_CliInfoExtra: TIntegerField
      FieldName = 'InfoExtra'
      Required = True
    end
    object QrSPC_CliBAC_CMC7: TSmallintField
      FieldName = 'BAC_CMC7'
      Required = True
    end
    object QrSPC_CliTipoCred: TSmallintField
      FieldName = 'TipoCred'
      Required = True
    end
    object QrSPC_CliSenhaSocio: TWideStringField
      FieldName = 'SenhaSocio'
      Required = True
      Size = 15
    end
    object QrSPC_CliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSPC_CliValAviso: TFloatField
      FieldName = 'ValAviso'
      Required = True
    end
    object QrSPC_CliVALCONSULTA: TFloatField
      FieldName = 'VALCONSULTA'
    end
  end
  object QrSum0: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor, SUM(VlrCompra) VlrCompra,'
      'SUM(Dias) Dias, SUM(Valor*Dias)/SUM(Valor) PRAZO_MEDIO, '
      'MAX(Vencto) Vencto, COUNT(Codigo) ITENS'
      'FROM lotesits'
      'WHERE Codigo=:P0')
    Left = 28
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSum0Valor: TFloatField
      FieldName = 'Valor'
    end
    object QrSum0Dias: TFloatField
      FieldName = 'Dias'
    end
    object QrSum0VlrCompra: TFloatField
      FieldName = 'VlrCompra'
    end
    object QrSum0PRAZO_MEDIO: TFloatField
      FieldName = 'PRAZO_MEDIO'
    end
    object QrSum0ITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrSum0Vencto: TDateField
      FieldName = 'Vencto'
    end
  end
  object QrSum1: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT SUM(TaxaVal) TaxaVal'
      'FROM emlotits'
      'WHERE Codigo=:P0')
    Left = 112
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSum1TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
  end
  object QrSum2: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT SUM(TaxaVal) TaxaVal'
      'FROM emlotits'
      'WHERE Codigo=:P0')
    Left = 112
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSum2TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
  end
  object QrSum3: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT SUM(TaxaVal) TaxaVal'
      'FROM emlotits'
      'WHERE Codigo=:P0')
    Left = 112
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSum3TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
  end
  object QrSum4: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT SUM(TaxaVal) TaxaVal'
      'FROM emlotits'
      'WHERE Codigo=:P0')
    Left = 112
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSum4TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
  end
  object QrSum5: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT SUM(TaxaVal) TaxaVal'
      'FROM emlotits'
      'WHERE Codigo=:P0')
    Left = 112
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSum5TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
  end
  object QrSum6: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT SUM(TaxaVal) TaxaVal'
      'FROM emlotits'
      'WHERE Codigo=:P0')
    Left = 112
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSum6TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
  end
  object QrEmLot6: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 112
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot6IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot6IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot6ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot6ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot6PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot6PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot6TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot6ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot6Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot6AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrEmLot5: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 112
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot5IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot5IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot5ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot5ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot5PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot5PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot5TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot5ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot5Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot5AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrEmLot4: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 112
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot4IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot4IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot4ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot4ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot4PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot4PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot4TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot4ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot4Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot4AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrEmLot3: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 112
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot3IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot3IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot3ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot3ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot3PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot3PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot3TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot3ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot3Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot3AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrEmLot1: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 112
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot1IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot1IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot1ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot1ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot1PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot1PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot1TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot1ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot1AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrExEnti1: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM exenti'
      'WHERE Codigo=:P0')
    Left = 28
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrExEnti1Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrExEnti1Maior: TFloatField
      FieldName = 'Maior'
    end
    object QrExEnti1Menor: TFloatField
      FieldName = 'Menor'
    end
    object QrExEnti1AdVal: TFloatField
      FieldName = 'AdVal'
    end
  end
  object QrExEnti2: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM exenti'
      'WHERE Codigo=:P0')
    Left = 28
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrExEnti2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrExEnti2Maior: TFloatField
      FieldName = 'Maior'
    end
    object QrExEnti2Menor: TFloatField
      FieldName = 'Menor'
    end
    object QrExEnti2AdVal: TFloatField
      FieldName = 'AdVal'
    end
  end
  object QrEmLot2: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM emlot'
      'WHERE Codigo=:P0')
    Left = 112
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLot2IRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrEmLot2IRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrEmLot2ISS: TFloatField
      FieldName = 'ISS'
    end
    object QrEmLot2ISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrEmLot2PIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrEmLot2PIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrEmLot2TaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLot2ValValorem: TFloatField
      FieldName = 'ValValorem'
    end
    object QrEmLot2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLot2AdValorem: TFloatField
      FieldName = 'AdValorem'
    end
  end
  object QrExEnti3: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM exenti'
      'WHERE Codigo=:P0')
    Left = 28
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
    end
    object FloatField1: TFloatField
      FieldName = 'Maior'
    end
    object FloatField2: TFloatField
      FieldName = 'Menor'
    end
    object QrExEnti3AdVal: TFloatField
      FieldName = 'AdVal'
    end
  end
  object QrExEnti4: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM exenti'
      'WHERE Codigo=:P0')
    Left = 28
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
    end
    object FloatField3: TFloatField
      FieldName = 'Maior'
    end
    object FloatField4: TFloatField
      FieldName = 'Menor'
    end
    object QrExEnti4AdVal: TFloatField
      FieldName = 'AdVal'
    end
  end
  object QrExEnti5: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM exenti'
      'WHERE Codigo=:P0')
    Left = 28
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField3: TIntegerField
      FieldName = 'Codigo'
    end
    object FloatField5: TFloatField
      FieldName = 'Maior'
    end
    object FloatField6: TFloatField
      FieldName = 'Menor'
    end
    object QrExEnti5AdVal: TFloatField
      FieldName = 'AdVal'
    end
  end
  object QrExEnti6: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM exenti'
      'WHERE Codigo=:P0')
    Left = 28
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField4: TIntegerField
      FieldName = 'Codigo'
    end
    object FloatField7: TFloatField
      FieldName = 'Maior'
    end
    object FloatField8: TFloatField
      FieldName = 'Menor'
    end
    object QrExEnti6AdVal: TFloatField
      FieldName = 'AdVal'
    end
  end
  object QrLI: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lo.Tipo, lo.CBE, lo.SCB, li.* '
      'FROM lotesits li'
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Codigo=:P0'
      ''
      '')
    Left = 28
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLICodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLIControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLIComp: TIntegerField
      FieldName = 'Comp'
    end
    object QrLIBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrLIAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrLIConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrLICheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrLICPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrLIEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrLIBruto: TFloatField
      FieldName = 'Bruto'
    end
    object QrLIDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrLIValor: TFloatField
      FieldName = 'Valor'
    end
    object QrLIEmissao: TDateField
      FieldName = 'Emissao'
    end
    object QrLIDCompra: TDateField
      FieldName = 'DCompra'
    end
    object QrLIDDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrLIVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrLITxaCompra: TFloatField
      FieldName = 'TxaCompra'
    end
    object QrLITxaJuros: TFloatField
      FieldName = 'TxaJuros'
    end
    object QrLITxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
    end
    object QrLIVlrCompra: TFloatField
      FieldName = 'VlrCompra'
    end
    object QrLIVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
    end
    object QrLIDMais: TIntegerField
      FieldName = 'DMais'
    end
    object QrLIDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrLIDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrLIDevolucao: TIntegerField
      FieldName = 'Devolucao'
    end
    object QrLIQuitado: TIntegerField
      FieldName = 'Quitado'
    end
    object QrLILk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLIDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLIDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLIUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLIUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLIPraca: TIntegerField
      FieldName = 'Praca'
    end
    object QrLITipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLIBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
      Required = True
    end
    object QrLIAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
      Required = True
    end
    object QrLICBE: TIntegerField
      FieldName = 'CBE'
    end
    object QrLISCB: TIntegerField
      FieldName = 'SCB'
    end
    object QrLICliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrLICartDep: TIntegerField
      FieldName = 'CartDep'
      Required = True
    end
    object QrLIDescAte: TDateField
      FieldName = 'DescAte'
      Required = True
    end
    object QrLITipific: TSmallintField
      FieldName = 'Tipific'
      Required = True
    end
    object QrLIStatusSPC: TSmallintField
      FieldName = 'StatusSPC'
      Required = True
    end
  end
  object QrLocDU: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Count(*) Duplicatas'
      'FROM lotesits'
      'WHERE CPF=:P0'
      'AND Duplicata=:P1')
    Left = 28
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocDUDuplicatas: TLargeintField
      FieldName = 'Duplicatas'
      Required = True
    end
  end
  object QrLocCH: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Count(*) Cheques'
      'FROM lotesits'
      'WHERE Banco=:P0'
      'AND Agencia=:P1'
      'AND Conta=:P2'
      'AND Cheque=:P3')
    Left = 28
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrLocCHCheques: TLargeintField
      FieldName = 'Cheques'
      Required = True
    end
  end
  object QrSumTxs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(TaxaVal) TaxaVal'
      'FROM lotestxs lt'
      'WHERE lt.Codigo=:P0')
    Left = 364
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumTxsTaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
  end
  object QrLCalc: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLCalcCalcFields
    SQL.Strings = (
      'SELECT lo.AdValorem, lo.IOC, lo.CPMF, lo.Cliente, lo.Data, '
      'lo.IRRF, lo.PIS, lo.PIS_R, lo.COFINS, lo.COFINS_R,  lo.ISS,'
      'lo.ValValorem, lo.TxCompra, lo.ISS_Val, lo.IRRF_Val,'
      'lo.PIS_R_Val, lo.Total, lo.IOC_Val, lo.CPMF_Val, lo.Tarifas,'
      'lo.OcorP, lo.CHDevPg, lo.SobraNow, lo.IOFv, lo.IOFd,'
      'ent.Tipo, ent.Simples'
      'FROM lotes lo'
      'LEFT JOIN entidades ent on ent.Codigo=lo.Cliente'
      'WHERE lo.Codigo=:P0')
    Left = 28
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLCalcAdValorem: TFloatField
      FieldName = 'AdValorem'
    end
    object QrLCalcIOC: TFloatField
      FieldName = 'IOC'
    end
    object QrLCalcCPMF: TFloatField
      FieldName = 'CPMF'
    end
    object QrLCalcIRRF: TFloatField
      FieldName = 'IRRF'
    end
    object QrLCalcPIS: TFloatField
      FieldName = 'PIS'
    end
    object QrLCalcPIS_R: TFloatField
      FieldName = 'PIS_R'
    end
    object QrLCalcCOFINS: TFloatField
      FieldName = 'COFINS'
    end
    object QrLCalcCOFINS_R: TFloatField
      FieldName = 'COFINS_R'
    end
    object QrLCalcISS: TFloatField
      FieldName = 'ISS'
    end
    object QrLCalcCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrLCalcData: TDateField
      FieldName = 'Data'
    end
    object QrLCalcVAL_LIQUIDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'VAL_LIQUIDO'
      Calculated = True
    end
    object QrLCalcValValorem: TFloatField
      FieldName = 'ValValorem'
      DisplayFormat = '#,###,##0.00'
    end
    object QrLCalcTxCompra: TFloatField
      FieldName = 'TxCompra'
    end
    object QrLCalcISS_Val: TFloatField
      FieldName = 'ISS_Val'
    end
    object QrLCalcIRRF_Val: TFloatField
      FieldName = 'IRRF_Val'
    end
    object QrLCalcPIS_R_Val: TFloatField
      FieldName = 'PIS_R_Val'
    end
    object QrLCalcTotal: TFloatField
      FieldName = 'Total'
    end
    object QrLCalcIOC_Val: TFloatField
      FieldName = 'IOC_Val'
    end
    object QrLCalcCPMF_Val: TFloatField
      FieldName = 'CPMF_Val'
    end
    object QrLCalcTarifas: TFloatField
      FieldName = 'Tarifas'
    end
    object QrLCalcOcorP: TFloatField
      FieldName = 'OcorP'
    end
    object QrLCalcCHDevPg: TFloatField
      FieldName = 'CHDevPg'
    end
    object QrLCalcSobraNow: TFloatField
      FieldName = 'SobraNow'
    end
    object QrLCalcIOFv: TFloatField
      FieldName = 'IOFv'
    end
    object QrLCalcIOFd: TFloatField
      FieldName = 'IOFd'
    end
    object QrLCalcTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrLCalcSimples: TSmallintField
      FieldName = 'Simples'
    end
  end
  object QrTxs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tx.Base, lt.* '
      'FROM lotestxs lt'
      'LEFT JOIN taxas tx ON tx.Codigo=lt.TaxaCod'
      'WHERE lt.Codigo=:P0')
    Left = 364
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTxsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrTxsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrTxsTaxaCod: TIntegerField
      FieldName = 'TaxaCod'
      Required = True
    end
    object QrTxsTaxaTxa: TFloatField
      FieldName = 'TaxaTxa'
      Required = True
    end
    object QrTxsTaxaVal: TFloatField
      FieldName = 'TaxaVal'
      Required = True
    end
    object QrTxsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTxsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTxsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTxsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTxsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTxsForma: TSmallintField
      FieldName = 'Forma'
      Required = True
    end
    object QrTxsTaxaQtd: TFloatField
      FieldName = 'TaxaQtd'
      Required = True
    end
    object QrTxsBase: TSmallintField
      FieldName = 'Base'
    end
    object QrTxsSysAQtd: TFloatField
      FieldName = 'SysAQtd'
      Required = True
    end
  end
  object QrDupNeg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lo.Lote, li.* '
      'FROM lotesits li'
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Cliente=:P0'
      'AND li.Duplicata = :P1'
      'AND li.Codigo<>:P2')
    Left = 364
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrDupNegLote: TSmallintField
      FieldName = 'Lote'
    end
    object QrDupNegCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrDupNegControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDupNegComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrDupNegBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrDupNegAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrDupNegConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrDupNegCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
    end
    object QrDupNegCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrDupNegEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrDupNegValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrDupNegEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrDupNegDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
    end
    object QrDupNegDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
    end
    object QrDupNegVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrDupNegTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrDupNegTxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrDupNegTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrDupNegVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrDupNegVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrDupNegDMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrDupNegDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrDupNegDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrDupNegLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrDupNegDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDupNegDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDupNegUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDupNegUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrDupNegDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrDupNegDesco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrDupNegQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrDupNegBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
    end
  end
  object QrTaxasCli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT '
      'ta.Genero, ta.Forma, ta.Base, ta.Nome NOMETAXA, tc.* '
      'FROM taxascli tc'
      'LEFT JOIN taxas ta ON ta.Codigo=tc.Taxa'
      'WHERE tc.Cliente=:P0'
      'ORDER BY NOMETAXA')
    Left = 364
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTaxasCliGenero: TSmallintField
      FieldName = 'Genero'
    end
    object QrTaxasCliForma: TSmallintField
      FieldName = 'Forma'
    end
    object QrTaxasCliBase: TSmallintField
      FieldName = 'Base'
    end
    object QrTaxasCliNOMETAXA: TWideStringField
      FieldName = 'NOMETAXA'
      Size = 30
    end
    object QrTaxasCliControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrTaxasCliCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrTaxasCliLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTaxasCliDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTaxasCliDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTaxasCliUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTaxasCliUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrTaxasCliTaxa: TIntegerField
      FieldName = 'Taxa'
      Required = True
    end
    object QrTaxasCliValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
  end
  object QrLocs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo FROM lotes '
      'WHERE Codigo >= :P0'
      'ORDER BY Codigo')
    Left = 364
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocsTipo: TSmallintField
      FieldName = 'Tipo'
    end
  end
  object QrDupSac: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ba.Nome NOMEBANCO, li.Duplicata, li.Emissao, '
      'li.Valor, li.Vencto, li.Banco, li.Agencia, li.Conta, li.Cheque,'
      'li.DCompra'
      'FROM lotesits li'
      'LEFT JOIN bancos ba ON ba.Codigo=li.Banco'
      'WHERE li.Codigo=:P0'
      'AND li.CPF=:P1')
    Left = 364
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDupSacDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrDupSacEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrDupSacValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrDupSacVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrDupSacBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
    end
    object QrDupSacAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
    end
    object QrDupSacNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrDupSacConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrDupSacCheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrDupSacDCompra: TDateField
      FieldName = 'DCompra'
    end
  end
  object QrSumOPAntigo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ob.Nome NOMEOOCORRENCIA,'
      'COUNT(oc.Ocorrencia) Ocorrencias, SUM(op.Pago) Pago'
      'FROM ocorrpg op'
      'LEFT JOIN ocorreu oc ON oc.Codigo=op.Ocorreu'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE op.LotePg=:P0'
      'GROUP BY oc.Ocorrencia')
    Left = 364
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrSumP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Debito) Debito '
      'FROM lanctos'
      'WHERE FatID=300'
      'AND FatNum=:P0'
      '')
    Left = 364
    Top = 148
    ParamData = <
      item
        DataType = ftInteger
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'DBMMONEY.lanctos.Debito'
    end
  end
  object QrVerNF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM nfs'
      'WHERE Codigo=:P0')
    Left = 364
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVerNFCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrLocNF: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Codigo) Codigo'
      'FROM nfs')
    Left = 364
    Top = 52
    object QrLocNFCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrSOA: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lo.Cliente CLIENTELOTE, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes    lo ON lo.Codigo   = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo   = oc.Ocorrencia'
      'WHERE oc.Status<2'
      'AND li.CPF =:P0'
      '')
    Left = 364
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSOACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSOALotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrSOADataO: TDateField
      FieldName = 'DataO'
      Required = True
    end
    object QrSOAOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrSOAValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrSOALoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrSOATaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrSOATaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrSOATaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
    end
    object QrSOAPago: TFloatField
      FieldName = 'Pago'
      Required = True
    end
    object QrSOADataP: TDateField
      FieldName = 'DataP'
      Required = True
    end
    object QrSOALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSOADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSOADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSOAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSOAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSOAData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrSOAStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrSOACliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrSOACLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
  end
  object QrSDO: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lo.Cliente, li.DCompra, li.Valor, li.DDeposito,'
      'li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg,'
      'li.Vencto, li.Data3'
      'FROM lotesits li'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'WHERE lo.Tipo=1'
      'AND li.Quitado <> 2'
      'AND li.CPF = :P0'
      'ORDER BY li.Vencto'
      ''
      ' ')
    Left = 280
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSDOCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSDODCompra: TDateField
      FieldName = 'DCompra'
      Required = True
    end
    object QrSDOValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrSDODDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
    end
    object QrSDOQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrSDOTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrSDOTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrSDOTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrSDOVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrSDOData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
  end
  object QrTCD: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ai.Valor, ai.Cliente, ai.Status, ai.Data1, ai.Data3,'
      'ai.JurosV, ai.Desconto, ai.ValPago, ai.JurosP'
      'FROM alinits ai'
      'WHERE ai.Status<2'
      'AND ai.CPF = :P0'
      '')
    Left = 280
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTCDValor: TFloatField
      FieldName = 'Valor'
      Required = True
    end
    object QrTCDCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrTCDStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrTCDData1: TDateField
      FieldName = 'Data1'
      Required = True
    end
    object QrTCDData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrTCDJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
    end
    object QrTCDDesconto: TFloatField
      FieldName = 'Desconto'
      Required = True
    end
    object QrTCDValPago: TFloatField
      FieldName = 'ValPago'
      Required = True
    end
    object QrTCDJurosP: TFloatField
      FieldName = 'JurosP'
      Required = True
    end
  end
  object QrSRS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(li.Valor) Valor'
      'FROM lotesits li'
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Tipo = 0'
      'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())'
      'AND li.CPF = :P0')
    Left = 280
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSRSValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCart: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Tipo'
      'FROM carteiras'
      'WHERE Codigo=:P0')
    Left = 280
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCartCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrCHsDev: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ai.Codigo'
      'FROM alinits ai'
      'LEFT JOIN lotes lo ON ai.LoteOrigem=lo.Codigo'
      'WHERE ai.CPF=27734959857'
      'AND lo.TxCompra+lo.ValValorem+ 1 >= 0.01')
    Left = 280
    Top = 340
    object QrCHsDevCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object QrEmLotIts: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT * FROM emlotits'
      'WHERE Controle=:P0')
    Left = 280
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmLotItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEmLotItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrEmLotItsTaxaPer: TFloatField
      FieldName = 'TaxaPer'
    end
    object QrEmLotItsTaxaVal: TFloatField
      FieldName = 'TaxaVal'
    end
    object QrEmLotItsJuroPer: TFloatField
      FieldName = 'JuroPer'
    end
  end
  object QrSumOco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(op.Pago) PAGO'
      'FROM ocorrpg op'
      'WHERE op.LotePg=:P0')
    Left = 280
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumOcoPAGO: TFloatField
      FieldName = 'PAGO'
    end
  end
  object QrNovoTxs: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM taxas'
      'ORDER BY Nome')
    Left = 280
    Top = 148
    object QrNovoTxsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrNovoTxsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrNovoTxsGenero: TSmallintField
      FieldName = 'Genero'
    end
    object QrNovoTxsForma: TSmallintField
      FieldName = 'Forma'
    end
    object QrNovoTxsAutomatico: TSmallintField
      FieldName = 'Automatico'
    end
    object QrNovoTxsValor: TFloatField
      FieldName = 'Valor'
    end
    object QrNovoTxsMostra: TSmallintField
      FieldName = 'Mostra'
    end
    object QrNovoTxsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrNovoTxsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrNovoTxsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrNovoTxsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrNovoTxsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object QrLocOc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM ocorrpg'
      'WHERE Data=('
      'SELECT Max(Data) FROM ocorrpg'
      'WHERE Ocorreu=:P0)'
      'ORDER BY Codigo DESC')
    Left = 280
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocOcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocOcOcorreu: TIntegerField
      FieldName = 'Ocorreu'
    end
    object QrLocOcData: TDateField
      FieldName = 'Data'
    end
    object QrLocOcJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrLocOcPago: TFloatField
      FieldName = 'Pago'
    end
    object QrLocOcLotePg: TIntegerField
      FieldName = 'LotePg'
    end
    object QrLocOcLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocOcDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocOcDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocOcUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocOcUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object QrSumOc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Juros) Juros, SUM(Pago) Pago'
      'FROM ocorrpg'
      'WHERE Ocorreu=:P0')
    Left = 280
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumOcJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumOcPago: TFloatField
      FieldName = 'Pago'
    end
  end
  object QrLastOcor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Codigo'
      'FROM ocorrpg'
      'WHERE Ocorreu=:P0'
      'ORDER BY Data Desc, Codigo Desc')
    Left = 280
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLastOcorData: TDateField
      FieldName = 'Data'
    end
    object QrLastOcorCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrLotExist: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Lote FROM lotes'
      'WHERE Cliente=:P0'
      'AND Codigo<>:P1'
      'AND Lote=:P2')
    Left = 196
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
  end
  object QrLocSaca: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome, CNPJ, IE, Rua, Numero+0.000 Numero, Compl, Bairro,'
      'Cidade, CEP, Tel1, UF, Email'
      'FROM sacados'
      'WHERE CNPJ=:P0')
    Left = 280
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocSacaNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrLocSacaCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Size = 15
    end
    object QrLocSacaRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrLocSacaNumero: TFloatField
      FieldName = 'Numero'
    end
    object QrLocSacaCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrLocSacaBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrLocSacaCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrLocSacaCEP: TIntegerField
      FieldName = 'CEP'
    end
    object QrLocSacaTel1: TWideStringField
      FieldName = 'Tel1'
    end
    object QrLocSacaUF: TWideStringField
      FieldName = 'UF'
      Size = 2
    end
    object QrLocSacaEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrLocSacaIE: TWideStringField
      FieldName = 'IE'
      Size = 25
    end
  end
  object QrSumCHP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ap.Pago) Pago'
      'FROM alinpgs ap'
      'WHERE ap.LotePG=:P0')
    Left = 196
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumCHPPago: TFloatField
      FieldName = 'Pago'
    end
  end
  object QrLastCHDV: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Data) Data FROM alinpgs'
      'WHERE AlinIts=:P0')
    Left = 196
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLastCHDVData: TDateField
      FieldName = 'Data'
    end
  end
  object QrSumPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Juros) Juros, SUM(Pago) Pago, SUM(Desco) Desco'
      'FROM alinpgs'
      'WHERE AlinIts=:P0')
    Left = 196
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumPgJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrSumPgPago: TFloatField
      FieldName = 'Pago'
    end
    object QrSumPgDesco: TFloatField
      FieldName = 'Desco'
    end
  end
  object QrLocPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM alinpgs'
      'WHERE Data=('
      'SELECT Max(Data) FROM alinpgs'
      'WHERE AlinIts=:P0)'
      'ORDER BY Codigo DESC')
    Left = 196
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocPgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocPgAlinIts: TIntegerField
      FieldName = 'AlinIts'
    end
    object QrLocPgData: TDateField
      FieldName = 'Data'
    end
    object QrLocPgLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrLocPgDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrLocPgDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrLocPgUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrLocPgUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrLocPgJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrLocPgPago: TFloatField
      FieldName = 'Pago'
    end
  end
  object QrLocLote: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Max(Lote) Lote FROM lotes'
      'WHERE Cliente=:P0'
      'AND Codigo<>:P1')
    Left = 196
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrLocLoteLote: TSmallintField
      FieldName = 'Lote'
    end
  end
  object QrSDUOpen: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(li.Valor) Valor, COUNT(*) DUPLICATAS '
      'FROM lotesits li '
      'WHERE li.CPF =:P0'
      'AND li.Quitado=0'
      'ORDER BY DDeposito')
    Left = 196
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSDUOpenValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSDUOpenDUPLICATAS: TLargeintField
      FieldName = 'DUPLICATAS'
      Required = True
    end
  end
  object QrSumDUP: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Pago) Pago'
      'FROM aduppgs'
      'WHERE LotePg=:P0')
    Left = 196
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumDUPPago: TFloatField
      FieldName = 'Pago'
    end
  end
  object QrLastDPg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Data, Controle '
      'FROM aduppgs'
      'WHERE LotesIts=:P0'
      'ORDER BY Data DESC, Controle DESC')
    Left = 196
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLastDPgData: TDateField
      FieldName = 'Data'
    end
    object QrLastDPgControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrPgD: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ad.Juros) Juros, SUM(ad.Desco) Desco, '
      'SUM(ad.Pago) Pago, li.Valor, MAX(ad.Data) MaxData'
      'FROM aduppgs ad'
      'LEFT JOIN lotesits li ON li.Controle=ad.LotesIts'
      'WHERE ad.LotesIts=:P0'
      'GROUP BY ad.LotesIts')
    Left = 196
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPgDJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrPgDDesco: TFloatField
      FieldName = 'Desco'
    end
    object QrPgDPago: TFloatField
      FieldName = 'Pago'
    end
    object QrPgDValor: TFloatField
      FieldName = 'Valor'
    end
    object QrPgDMaxData: TDateField
      FieldName = 'MaxData'
    end
  end
  object QrSumRepCli: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(loi.Valor) Valor'
      'FROM lotesits loi'
      'WHERE loi.RepCli = :P0')
    Left = 196
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumRepCliValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrSAlin: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor+Taxas+Multa+JurosV) Valor,'
      'SUM(Valor+Taxas+Multa+JurosV-Desconto-ValPago) Aberto,'
      'COUNT(*) Cheques'
      'FROM alinits'
      'WHERE CPF=:P0')
    Left = 196
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSAlinValor: TFloatField
      FieldName = 'Valor'
    end
    object QrSAlinAberto: TFloatField
      FieldName = 'Aberto'
    end
    object QrSAlinCheques: TLargeintField
      FieldName = 'Cheques'
      Required = True
    end
  end
  object QrOcorA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrOcorAAfterOpen
    BeforeClose = QrOcorABeforeClose
    OnCalcFields = QrOcorACalcFields
    SQL.Strings = (
      'SELECT IF(oc.Cliente > 0,'#39'CL'#39',IF(lo.Tipo=0,"CH","DU"))TIPODOC, '
      'ob.Nome NOMEOCORRENCIA, li.Banco, li.Agencia, li.Cheque, '
      
        'li.Duplicata, li.Conta, lo.Tipo, li.Emissao, li.DCompra, li.Venc' +
        'to, '
      'li.DDeposito, li.Emitente, li.CPF, lo.Cliente CLIENTELOTE, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.Status<2'
      'AND (oc.Valor + oc.TaxaV - oc.Pago <> 0)'
      'AND (lo.Cliente=:P0 OR oc.Cliente=:P1)')
    Left = 448
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOcorATipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrOcorATIPODOC: TWideStringField
      FieldName = 'TIPODOC'
      Size = 2
    end
    object QrOcorANOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrOcorACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrOcorALotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrOcorADataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorAOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrOcorAValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrOcorALoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrOcorALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrOcorADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrOcorADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrOcorAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrOcorAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrOcorATaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrOcorATaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOcorAPago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrOcorADataP: TDateField
      FieldName = 'DataP'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorATaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrOcorAData3: TDateField
      FieldName = 'Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorAStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrOcorASALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,###,##0.00'
      Calculated = True
    end
    object QrOcorAATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,###,##0.00'
      Calculated = True
    end
    object QrOcorADOCUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOCUM_TXT'
      Size = 100
      Calculated = True
    end
    object QrOcorABanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrOcorAAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrOcorACheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrOcorADuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 12
    end
    object QrOcorAConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrOcorAEmissao: TDateField
      FieldName = 'Emissao'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorADCompra: TDateField
      FieldName = 'DCompra'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrOcorAVencto: TDateField
      FieldName = 'Vencto'
    end
    object QrOcorADDeposito: TDateField
      FieldName = 'DDeposito'
    end
    object QrOcorAEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrOcorACPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrOcorACLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
    object QrOcorACliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrOcorADescri: TWideStringField
      FieldName = 'Descri'
      Size = 30
    end
  end
  object DsOcorA: TDataSource
    DataSet = QrOcorA
    Left = 532
    Top = 4
  end
  object DsOcorTf: TDataSource
    DataSet = QrOcorTf
    Left = 532
    Top = 52
  end
  object QrOcorTf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(oc.Valor+TaxaV-Pago) TOTAL_A'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'WHERE oc.Status<2'
      'AND (lo.Cliente=:P0 OR oc.Cliente=:P1)')
    Left = 448
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrOcorTfTOTAL_A: TFloatField
      FieldName = 'TOTAL_A'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object QrLocCliImp: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, LimiCred,'
      'CASE WHEN Tipo=0 THEN RazaoSocial '
      'ELSE Nome END NOMECLI'
      'FROM entidades'
      'WHERE Codigo=:P0')
    Left = 448
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCliImpCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocCliImpNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrLocCliImpLimiCred: TFloatField
      FieldName = 'LimiCred'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsLocCliImp: TDataSource
    DataSet = QrLocCliImp
    Left = 532
    Top = 100
  end
  object DsRepCli: TDataSource
    DataSet = QrRepCli
    Left = 532
    Top = 148
  end
  object QrRepCli: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrRepCliAfterOpen
    BeforeClose = QrRepCliBeforeClose
    OnCalcFields = QrRepCliCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMECLI, lot.Lote, loi.*'
      'FROM lotesits loi'
      'LEFT JOIN lotes lot ON lot.Codigo=loi.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=lot.Cliente'
      'WHERE loi.RepCli = :P0'
      'ORDER BY Valor DESC')
    Left = 448
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRepCliNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrRepCliLote: TIntegerField
      FieldName = 'Lote'
    end
    object QrRepCliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRepCliControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrRepCliComp: TIntegerField
      FieldName = 'Comp'
      Required = True
    end
    object QrRepCliBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrRepCliAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrRepCliConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrRepCliCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrRepCliCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrRepCliEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrRepCliBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
    end
    object QrRepCliDesco: TFloatField
      FieldName = 'Desco'
      Required = True
    end
    object QrRepCliValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrRepCliEmissao: TDateField
      FieldName = 'Emissao'
      Required = True
    end
    object QrRepCliDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
    end
    object QrRepCliDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
    end
    object QrRepCliVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRepCliTxaCompra: TFloatField
      FieldName = 'TxaCompra'
      Required = True
    end
    object QrRepCliTxaJuros: TFloatField
      FieldName = 'TxaJuros'
      Required = True
    end
    object QrRepCliTxaAdValorem: TFloatField
      FieldName = 'TxaAdValorem'
      Required = True
    end
    object QrRepCliVlrCompra: TFloatField
      FieldName = 'VlrCompra'
      Required = True
    end
    object QrRepCliVlrAdValorem: TFloatField
      FieldName = 'VlrAdValorem'
      Required = True
    end
    object QrRepCliDMais: TIntegerField
      FieldName = 'DMais'
      Required = True
    end
    object QrRepCliDias: TIntegerField
      FieldName = 'Dias'
      Required = True
    end
    object QrRepCliDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrRepCliDevolucao: TIntegerField
      FieldName = 'Devolucao'
      Required = True
    end
    object QrRepCliQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrRepCliLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrRepCliDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrRepCliDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrRepCliUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrRepCliUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrRepCliPraca: TIntegerField
      FieldName = 'Praca'
      Required = True
    end
    object QrRepCliBcoCobra: TIntegerField
      FieldName = 'BcoCobra'
      Required = True
    end
    object QrRepCliAgeCobra: TIntegerField
      FieldName = 'AgeCobra'
      Required = True
    end
    object QrRepCliTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
    end
    object QrRepCliTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
    end
    object QrRepCliTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
    end
    object QrRepCliData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrRepCliProrrVz: TIntegerField
      FieldName = 'ProrrVz'
      Required = True
    end
    object QrRepCliProrrDd: TIntegerField
      FieldName = 'ProrrDd'
      Required = True
    end
    object QrRepCliRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
    object QrRepCliDepositado: TSmallintField
      FieldName = 'Depositado'
      Required = True
    end
    object QrRepCliValQuit: TFloatField
      FieldName = 'ValQuit'
      Required = True
    end
    object QrRepCliValDeposito: TFloatField
      FieldName = 'ValDeposito'
      Required = True
    end
    object QrRepCliTipo: TIntegerField
      FieldName = 'Tipo'
      Required = True
    end
    object QrRepCliAliIts: TIntegerField
      FieldName = 'AliIts'
      Required = True
    end
    object QrRepCliAlinPgs: TIntegerField
      FieldName = 'AlinPgs'
      Required = True
    end
    object QrRepCliNaoDeposita: TSmallintField
      FieldName = 'NaoDeposita'
      Required = True
    end
    object QrRepCliReforcoCxa: TSmallintField
      FieldName = 'ReforcoCxa'
      Required = True
    end
    object QrRepCliCartDep: TIntegerField
      FieldName = 'CartDep'
      Required = True
    end
    object QrRepCliCobranca: TIntegerField
      FieldName = 'Cobranca'
      Required = True
    end
    object QrRepCliRepCli: TIntegerField
      FieldName = 'RepCli'
      Required = True
    end
    object QrRepCliSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
  end
  object QrDUVenc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT li.Duplicata, li.DCompra, '
      'li.Valor, li.DDeposito '
      'FROM lotesits li '
      'WHERE li.CPF =:P0'
      'AND li.Quitado=0'
      'AND li.DDeposito<CURDATE()'
      'ORDER BY DDeposito')
    Left = 448
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object StringField9: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object DateField6: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object FloatField18: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object DateField7: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsDUVenc: TDataSource
    DataSet = QrDUVenc
    Left = 532
    Top = 196
  end
  object DsDOpen: TDataSource
    DataSet = QrDOpen
    Left = 532
    Top = 244
  end
  object QrDOpen: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrDOpenAfterOpen
    BeforeClose = QrDOpenBeforeClose
    OnCalcFields = QrDOpenCalcFields
    SQL.Strings = (
      'SELECT od.Nome STATUS, li.Controle, li.Duplicata, li.Repassado,'
      'li.DCompra, li.Valor, li.DDeposito, li.Emitente, li.CPF, '
      'lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg,'
      'li.Vencto, li.Data3'
      'FROM lotesits li'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'WHERE lo.Tipo=1 '
      'AND li.Quitado <> 2'
      'AND lo.Cliente=:P0'
      'AND lo.TxCompra+lo.ValValorem+ :P1 >= 0.01'
      'ORDER BY li.Vencto')
    Left = 448
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDOpenControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDOpenDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrDOpenDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDOpenValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrDOpenDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDOpenEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrDOpenCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrDOpenCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDOpenSTATUS: TWideStringField
      FieldName = 'STATUS'
      Size = 50
    end
    object QrDOpenQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrDOpenTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrDOpenTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrDOpenTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrDOpenSALDO_DESATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_DESATUALIZ'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrDOpenSALDO_ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrDOpenNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 30
      Calculated = True
    end
    object QrDOpenVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrDOpenDDCALCJURO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DDCALCJURO'
      Calculated = True
    end
    object QrDOpenData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrDOpenRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
  end
  object QrDUOpen: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT li.Duplicata, li.DCompra, '
      'li.Valor, li.DDeposito '
      'FROM lotesits li '
      'WHERE li.CPF =:P0'
      'AND li.Quitado=0'
      'AND li.DDeposito>=CURDATE()'
      'ORDER BY DDeposito')
    Left = 448
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDUOpenDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrDUOpenDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDUOpenValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrDUOpenDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object DsDUOpen: TDataSource
    DataSet = QrDUOpen
    Left = 532
    Top = 532
  end
  object DsCHOpen: TDataSource
    DataSet = QrCHOpen
    Left = 532
    Top = 484
  end
  object QrCHOpen: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT li.Banco, li.Agencia, li.Conta,'
      'li.Cheque, li.Valor, li.DCompra, li.DDeposito'
      'FROM lotesits li '
      'WHERE li.CPF =:P0'
      'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())'
      'ORDER BY DDeposito')
    Left = 448
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCHOpenBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrCHOpenAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '000'
    end
    object QrCHOpenConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrCHOpenCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrCHOpenValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHOpenDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCHOpenDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrClientesAfterScroll
    OnCalcFields = QrClientesCalcFields
    SQL.Strings = (
      'SELECT Codigo, CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, '
      'AdValorem, DMaisC, DMaisD, FatorCompra, CBE, SCB, CPMF,'
      'Tipo, Simples'
      'FROM entidades'
      'WHERE Cliente1="V"'
      'ORDER BY NOMECLIENTE'
      '')
    Left = 448
    Top = 436
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrClientesAdValorem: TFloatField
      FieldName = 'AdValorem'
    end
    object QrClientesDMaisC: TIntegerField
      FieldName = 'DMaisC'
    end
    object QrClientesFatorCompra: TFloatField
      FieldName = 'FatorCompra'
    end
    object QrClientesMAIOR_T: TFloatField
      FieldKind = fkCalculated
      FieldName = 'MAIOR_T'
      Calculated = True
    end
    object QrClientesADVAL_T: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ADVAL_T'
      Calculated = True
    end
    object QrClientesDMaisD: TIntegerField
      FieldName = 'DMaisD'
    end
    object QrClientesCBE: TIntegerField
      FieldName = 'CBE'
    end
    object QrClientesSCB: TIntegerField
      FieldName = 'SCB'
    end
    object QrClientesCPMF: TFloatField
      FieldName = 'CPMF'
    end
    object QrClientesTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrClientesSimples: TSmallintField
      FieldName = 'Simples'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 532
    Top = 436
  end
  object DsCHDevT: TDataSource
    DataSet = QrCHDevT
    Left = 532
    Top = 388
  end
  object QrCHDevT: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT SUM(ai.Valor+ai.Taxas+ai.Multa+ai.JurosV-ai.Desconto-ai.V' +
        'alPago) ABERTO'
      'FROM alinits ai'
      'LEFT JOIN lotes lo ON ai.LoteOrigem=lo.Codigo'
      'WHERE ai.Status<2'
      'AND ai.Cliente=:P0'
      'AND lo.TxCompra+lo.ValValorem+ :P1 >= 0.01')
    Left = 448
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCHDevTABERTO: TFloatField
      FieldName = 'ABERTO'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object QrCHDevA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCHDevAAfterOpen
    BeforeClose = QrCHDevABeforeClose
    AfterScroll = QrCHDevAAfterScroll
    OnCalcFields = QrCHDevACalcFields
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLIENTE, ai.*'
      'FROM alinits ai'
      'LEFT JOIN entidades en ON en.Codigo=ai.Cliente'
      'LEFT JOIN lotes lo ON ai.LoteOrigem=lo.Codigo'
      'WHERE ai.Status<2'
      'AND ai.Cliente=:P0'
      'AND lo.TxCompra+lo.ValValorem+ :P1 >= 0.01'
      'ORDER BY ai.Data1, ai.Data2')
    Left = 448
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCHDevADATA1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA1_TXT'
      Calculated = True
    end
    object QrCHDevADATA2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA2_TXT'
      Calculated = True
    end
    object QrCHDevADATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Calculated = True
    end
    object QrCHDevACPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrCHDevANOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrCHDevACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCHDevAAlinea1: TIntegerField
      FieldName = 'Alinea1'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrCHDevAAlinea2: TIntegerField
      FieldName = 'Alinea2'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrCHDevAData1: TDateField
      FieldName = 'Data1'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCHDevAData2: TDateField
      FieldName = 'Data2'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCHDevAData3: TDateField
      FieldName = 'Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrCHDevACliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrCHDevABanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrCHDevAAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrCHDevAConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrCHDevACheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrCHDevACPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrCHDevAValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevATaxas: TFloatField
      FieldName = 'Taxas'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCHDevADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCHDevADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCHDevAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCHDevAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCHDevAEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrCHDevAChequeOrigem: TIntegerField
      FieldName = 'ChequeOrigem'
      Required = True
    end
    object QrCHDevAStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrCHDevAValPago: TFloatField
      FieldName = 'ValPago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevAMulta: TFloatField
      FieldName = 'Multa'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevAJurosP: TFloatField
      FieldName = 'JurosP'
      Required = True
      DisplayFormat = '#,###,##0.000000'
    end
    object QrCHDevAJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevADesconto: TFloatField
      FieldName = 'Desconto'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrCHDevASALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCHDevAATUAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrCHDevAPgDesc: TFloatField
      FieldName = 'PgDesc'
      Required = True
    end
  end
  object DsCHDevA: TDataSource
    DataSet = QrCHDevA
    Left = 532
    Top = 340
  end
  object DsOcorNovo: TDataSource
    DataSet = QrOcorNovo
    Left = 532
    Top = 292
  end
  object QrOcorNovo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(oc.Cliente > 0,'#39'CL'#39',IF(lo.Tipo=0,"CH","DU"))TIPODOC, '
      'ob.Nome NOMEOCORRENCIA, li.Banco, li.Agencia, li.Cheque, '
      
        'li.Duplicata, li.Conta, lo.Tipo, li.Emissao, li.DCompra, li.Venc' +
        'to, '
      'li.DDeposito, li.Emitente, li.CPF, lo.Cliente CLIENTELOTE, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.Status<2'
      'AND (lo.Cliente=:P0 OR oc.Cliente=:P1)')
    Left = 448
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
  end
  object QrAlinIts: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrAlinItsCalcFields
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLIENTE, ai.*'
      'FROM alinits ai'
      'LEFT JOIN entidades en ON en.Codigo=ai.Cliente'
      'WHERE ai.CPF=:P0'
      ''
      '')
    Left = 616
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAlinItsCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAlinItsAlinea1: TIntegerField
      FieldName = 'Alinea1'
    end
    object QrAlinItsAlinea2: TIntegerField
      FieldName = 'Alinea2'
    end
    object QrAlinItsData1: TDateField
      FieldName = 'Data1'
    end
    object QrAlinItsData2: TDateField
      FieldName = 'Data2'
    end
    object QrAlinItsData3: TDateField
      FieldName = 'Data3'
    end
    object QrAlinItsCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrAlinItsBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrAlinItsAgencia: TIntegerField
      FieldName = 'Agencia'
    end
    object QrAlinItsConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrAlinItsCheque: TIntegerField
      FieldName = 'Cheque'
    end
    object QrAlinItsCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrAlinItsValor: TFloatField
      FieldName = 'Valor'
    end
    object QrAlinItsTaxas: TFloatField
      FieldName = 'Taxas'
    end
    object QrAlinItsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrAlinItsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAlinItsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAlinItsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAlinItsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAlinItsEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrAlinItsChequeOrigem: TIntegerField
      FieldName = 'ChequeOrigem'
    end
    object QrAlinItsStatus: TSmallintField
      FieldName = 'Status'
    end
    object QrAlinItsValPago: TFloatField
      FieldName = 'ValPago'
    end
    object QrAlinItsDATA1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA1_TXT'
      Calculated = True
    end
    object QrAlinItsDATA2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA2_TXT'
      Calculated = True
    end
    object QrAlinItsDATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Calculated = True
    end
    object QrAlinItsNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrAlinItsMulta: TFloatField
      FieldName = 'Multa'
      Required = True
    end
    object QrAlinItsJurosP: TFloatField
      FieldName = 'JurosP'
      Required = True
    end
    object QrAlinItsJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
    end
    object QrAlinItsDesconto: TFloatField
      FieldName = 'Desconto'
      Required = True
    end
    object QrAlinItsCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
  end
  object QrTaxas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM taxas'
      'ORDER BY Nome')
    Left = 616
    Top = 52
    object QrTaxasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTaxasNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrTaxasGenero: TSmallintField
      FieldName = 'Genero'
    end
    object QrTaxasForma: TSmallintField
      FieldName = 'Forma'
    end
    object QrTaxasAutomatico: TSmallintField
      FieldName = 'Automatico'
    end
    object QrTaxasValor: TFloatField
      FieldName = 'Valor'
    end
    object QrTaxasMostra: TSmallintField
      FieldName = 'Mostra'
    end
    object QrTaxasLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrTaxasDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrTaxasDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrTaxasUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrTaxasUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
  end
  object DsTaxas: TDataSource
    DataSet = QrTaxas
    Left = 700
    Top = 52
  end
  object DsAlinIts: TDataSource
    DataSet = QrAlinIts
    Left = 700
    Top = 4
  end
  object QrSacRiscoTC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(li.Valor) Valor'
      'FROM lotesits li '
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Cliente=:P0'
      'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())'
      'AND lo.Tipo = 0')
    Left = 616
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacRiscoTCValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsSacRiscoTC: TDataSource
    DataSet = QrSacRiscoTC
    Left = 700
    Top = 100
  end
  object DsCarteiras4: TDataSource
    DataSet = QrCarteiras4
    Left = 700
    Top = 148
  end
  object QrCarteiras4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Banco1, Agencia1, Conta1'
      'FROM carteiras'
      'ORDER BY Nome')
    Left = 616
    Top = 148
    object QrCarteiras4Codigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteiras4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteiras4Banco1: TIntegerField
      FieldName = 'Banco1'
      DisplayFormat = '000'
    end
    object QrCarteiras4Agencia1: TIntegerField
      FieldName = 'Agencia1'
      DisplayFormat = '0000'
    end
    object QrCarteiras4Conta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
  end
  object QrSacOcorA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSacOcorAAfterOpen
    BeforeClose = QrSacOcorABeforeClose
    OnCalcFields = QrSacOcorACalcFields
    SQL.Strings = (
      'SELECT IF(oc.Cliente > 0,'#39'CL'#39',IF(lo.Tipo=0,"CH","DU"))TIPODOC, '
      'ob.Nome NOMEOCORRENCIA, lo.Tipo, lo.Cliente CLIENTELOTE, oc.*'
      'FROM ocorreu oc'
      'LEFT JOIN lotesits li ON oc.LotesIts = li.Controle'
      'LEFT JOIN lotes lo ON lo.Codigo = li.Codigo'
      'LEFT JOIN ocorbank ob ON ob.Codigo = oc.Ocorrencia'
      'WHERE oc.Status<2'
      'AND (lo.Cliente=:P0 OR oc.Cliente=:P1)')
    Left = 616
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSacOcorATipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrSacOcorATIPODOC: TWideStringField
      FieldName = 'TIPODOC'
      Size = 2
    end
    object QrSacOcorANOMEOCORRENCIA: TWideStringField
      FieldName = 'NOMEOCORRENCIA'
      Size = 50
    end
    object QrSacOcorACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSacOcorALotesIts: TIntegerField
      FieldName = 'LotesIts'
      Required = True
    end
    object QrSacOcorADataO: TDateField
      FieldName = 'DataO'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacOcorAOcorrencia: TIntegerField
      FieldName = 'Ocorrencia'
      Required = True
    end
    object QrSacOcorAValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacOcorALoteQuit: TIntegerField
      FieldName = 'LoteQuit'
      Required = True
    end
    object QrSacOcorALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSacOcorADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSacOcorADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSacOcorAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSacOcorAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSacOcorATaxaP: TFloatField
      FieldName = 'TaxaP'
      Required = True
    end
    object QrSacOcorATaxaV: TFloatField
      FieldName = 'TaxaV'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSacOcorAPago: TFloatField
      FieldName = 'Pago'
      Required = True
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrSacOcorADataP: TDateField
      FieldName = 'DataP'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacOcorATaxaB: TFloatField
      FieldName = 'TaxaB'
      Required = True
    end
    object QrSacOcorAData3: TDateField
      FieldName = 'Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacOcorAStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrSacOcorASALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,###,##0.00'
      Calculated = True
    end
    object QrSacOcorAATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUALIZADO'
      DisplayFormat = '#,###,###,##0.00'
      Calculated = True
    end
    object QrSacOcorACliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrSacOcorACLIENTELOTE: TIntegerField
      FieldName = 'CLIENTELOTE'
    end
  end
  object DsSacOcorA: TDataSource
    DataSet = QrSacOcorA
    Left = 700
    Top = 196
  end
  object DsSacCHDevA: TDataSource
    DataSet = QrSacCHDevA
    Left = 700
    Top = 244
  end
  object QrSacCHDevA: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSacCHDevAAfterOpen
    BeforeClose = QrSacCHDevABeforeClose
    OnCalcFields = QrSacCHDevACalcFields
    SQL.Strings = (
      'SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMECLIENTE, ai.*'
      'FROM alinits ai'
      'LEFT JOIN entidades en ON en.Codigo=ai.Cliente'
      'WHERE ai.Status<2'
      'AND ai.Cliente=:P0'
      'ORDER BY ai.Data1, ai.Data2')
    Left = 616
    Top = 244
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacCHDevADATA1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA1_TXT'
      Calculated = True
    end
    object QrSacCHDevADATA2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA2_TXT'
      Calculated = True
    end
    object QrSacCHDevADATA3_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA3_TXT'
      Calculated = True
    end
    object QrSacCHDevACPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 40
      Calculated = True
    end
    object QrSacCHDevANOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
    object QrSacCHDevACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSacCHDevAAlinea1: TIntegerField
      FieldName = 'Alinea1'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrSacCHDevAAlinea2: TIntegerField
      FieldName = 'Alinea2'
      Required = True
      DisplayFormat = '0;-0; '
    end
    object QrSacCHDevAData1: TDateField
      FieldName = 'Data1'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacCHDevAData2: TDateField
      FieldName = 'Data2'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacCHDevAData3: TDateField
      FieldName = 'Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacCHDevACliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrSacCHDevABanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrSacCHDevAAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrSacCHDevAConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrSacCHDevACheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrSacCHDevACPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrSacCHDevAValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevATaxas: TFloatField
      FieldName = 'Taxas'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevALk: TIntegerField
      FieldName = 'Lk'
    end
    object QrSacCHDevADataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrSacCHDevADataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrSacCHDevAUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrSacCHDevAUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrSacCHDevAEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrSacCHDevAChequeOrigem: TIntegerField
      FieldName = 'ChequeOrigem'
      Required = True
    end
    object QrSacCHDevAStatus: TSmallintField
      FieldName = 'Status'
      Required = True
    end
    object QrSacCHDevAValPago: TFloatField
      FieldName = 'ValPago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevAMulta: TFloatField
      FieldName = 'Multa'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevAJurosP: TFloatField
      FieldName = 'JurosP'
      Required = True
      DisplayFormat = '#,###,##0.000000'
    end
    object QrSacCHDevAJurosV: TFloatField
      FieldName = 'JurosV'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevADesconto: TFloatField
      FieldName = 'Desconto'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacCHDevASALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSacCHDevAATUAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'ATUAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSacCHDevAPgDesc: TFloatField
      FieldName = 'PgDesc'
      Required = True
    end
  end
  object QrBanco1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome FROM bancos'
      'WHERE Codigo=:P0')
    Left = 616
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBanco1Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsBanco1: TDataSource
    DataSet = QrBanco1
    Left = 700
    Top = 532
  end
  object DsBanco0: TDataSource
    DataSet = QrBanco0
    Left = 700
    Top = 484
  end
  object QrBanco0: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome FROM bancos'
      'WHERE Codigo=:P0')
    Left = 616
    Top = 484
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBanco0Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrRiscoTC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(li.Valor) Valor'
      'FROM lotesits li '
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Cliente=:P0'
      'AND lo.TxCompra+lo.ValValorem+ :P1 >= 0.01'
      'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())'
      'AND lo.Tipo = 0')
    Left = 616
    Top = 436
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrRiscoTCValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsRiscoTC: TDataSource
    DataSet = QrRiscoTC
    Left = 700
    Top = 436
  end
  object DsRiscoC: TDataSource
    DataSet = QrRiscoC
    Left = 700
    Top = 388
  end
  object QrRiscoC: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrRiscoCCalcFields
    SQL.Strings = (
      
        'SELECT lo.Tipo, li.Banco, li.Agencia, li.Conta, li.Cheque, li.Va' +
        'lor, '
      'li.DCompra, li.DDeposito, li.Emitente, li.CPF'
      'FROM lotesits li '
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Cliente=:P0'
      'AND lo.TxCompra+lo.ValValorem+ :P1 >= 0.01'
      'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())'
      'AND lo.Tipo = 0'
      'ORDER BY DDeposito')
    Left = 616
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrRiscoCBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrRiscoCAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrRiscoCConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrRiscoCCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrRiscoCValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrRiscoCDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRiscoCDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrRiscoCEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrRiscoCCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrRiscoCCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
  end
  object QrSacRiscoC: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrSacRiscoCCalcFields
    SQL.Strings = (
      
        'SELECT lo.Tipo, li.Banco, li.Agencia, li.Conta, li.Cheque, li.Va' +
        'lor, '
      'li.DCompra, li.DDeposito, li.Emitente, li.CPF'
      'FROM lotesits li '
      'LEFT JOIN lotes lo ON lo.Codigo=li.Codigo'
      'WHERE lo.Cliente=:P0'
      'AND (li.Devolucao=0) AND(DDeposito>=SYSDATE())'
      'AND lo.Tipo = 0'
      'ORDER BY DDeposito')
    Left = 616
    Top = 340
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacRiscoCBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrSacRiscoCAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrSacRiscoCConta: TWideStringField
      FieldName = 'Conta'
    end
    object QrSacRiscoCCheque: TIntegerField
      FieldName = 'Cheque'
      Required = True
      DisplayFormat = '000000'
    end
    object QrSacRiscoCValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacRiscoCDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacRiscoCDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacRiscoCEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrSacRiscoCCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrSacRiscoCCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsSacRiscoC: TDataSource
    DataSet = QrSacRiscoC
    Left = 700
    Top = 340
  end
  object DsSacDOpen: TDataSource
    DataSet = QrSacDOpen
    Left = 700
    Top = 292
  end
  object QrSacDOpen: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrSacDOpenAfterOpen
    BeforeClose = QrSacDOpenBeforeClose
    OnCalcFields = QrSacDOpenCalcFields
    SQL.Strings = (
      'SELECT od.Nome STATUS, li.Controle, li.Duplicata, li.Repassado,'
      'li.DCompra, li.Valor, li.DDeposito, li.Emitente, li.CPF, '
      'lo.Cliente, li.Quitado, li.TotalJr, li.TotalDs, li.TotalPg,'
      'li.Vencto, li.Data3'
      'FROM lotesits li'
      'LEFT JOIN lotes     lo ON lo.Codigo=li.Codigo'
      'LEFT JOIN ocordupl  od ON od.Codigo=li.Devolucao'
      'WHERE lo.Tipo=1 '
      'AND li.Quitado <> 2'
      'AND lo.Cliente=:P0'
      'ORDER BY li.Vencto')
    Left = 616
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSacDOpenControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrSacDOpenDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrSacDOpenDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacDOpenValor: TFloatField
      FieldName = 'Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacDOpenDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrSacDOpenEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrSacDOpenCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrSacDOpenCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrSacDOpenSTATUS: TWideStringField
      FieldName = 'STATUS'
      Size = 50
    end
    object QrSacDOpenQuitado: TIntegerField
      FieldName = 'Quitado'
      Required = True
    end
    object QrSacDOpenTotalJr: TFloatField
      FieldName = 'TotalJr'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacDOpenTotalDs: TFloatField
      FieldName = 'TotalDs'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacDOpenTotalPg: TFloatField
      FieldName = 'TotalPg'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrSacDOpenSALDO_DESATUALIZ: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_DESATUALIZ'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSacDOpenSALDO_ATUALIZADO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO_ATUALIZADO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrSacDOpenNOMESTATUS: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESTATUS'
      Size = 30
      Calculated = True
    end
    object QrSacDOpenVencto: TDateField
      FieldName = 'Vencto'
      Required = True
    end
    object QrSacDOpenDDCALCJURO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'DDCALCJURO'
      Calculated = True
    end
    object QrSacDOpenData3: TDateField
      FieldName = 'Data3'
      Required = True
    end
    object QrSacDOpenRepassado: TSmallintField
      FieldName = 'Repassado'
      Required = True
    end
  end
  object QrBanco4: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome FROM bancos'
      'WHERE Codigo=:P0')
    Left = 784
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrBanco4Nome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsBanco4: TDataSource
    DataSet = QrBanco4
    Left = 868
    Top = 4
  end
  object QrSPC_Result: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Linha, Texto'
      'FROM spc_result'
      'WHERE CPFCNPJ=:P0'
      'ORDER BY Linha')
    Left = 784
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSPC_ResultLinha: TIntegerField
      FieldName = 'Linha'
      DisplayFormat = '000'
    end
    object QrSPC_ResultTexto: TWideStringField
      FieldName = 'Texto'
      Size = 79
    end
  end
  object DsSPC_Result: TDataSource
    DataSet = QrSPC_Result
    Left = 868
    Top = 52
  end
  object QrSPC_Cfg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sm.Valor VALCONSULTA,sc.Codigo, sp.ValMin SPC_ValMin, '
      'sp.ValMax SPC_ValMax, sc.Servidor, sc.Porta SPC_Porta, '
      'sc.Pedinte, sc.CodigSocio, sc.SenhaSocio, sc.Modalidade, '
      'sc.InfoExtra, sc.BAC_CMC7, sc.TipoCred, ValAviso'
      'FROM spc_config sc '
      'LEFT JOIN spc_entida sp ON sp.SPC_Config=sc.Codigo '
      'LEFT JOIN spc_modali sm ON sm.Codigo=sc.Modalidade '
      'WHERE sp.Entidade=:P0')
    Left = 364
    Top = 532
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSPC_CfgSPC_ValMin: TFloatField
      FieldName = 'SPC_ValMin'
    end
    object QrSPC_CfgSPC_ValMax: TFloatField
      FieldName = 'SPC_ValMax'
    end
    object QrSPC_CfgServidor: TWideStringField
      FieldName = 'Servidor'
      Size = 100
    end
    object QrSPC_CfgSPC_Porta: TSmallintField
      FieldName = 'SPC_Porta'
      Required = True
    end
    object QrSPC_CfgPedinte: TWideStringField
      FieldName = 'Pedinte'
      Size = 15
    end
    object QrSPC_CfgCodigSocio: TIntegerField
      FieldName = 'CodigSocio'
      Required = True
    end
    object QrSPC_CfgModalidade: TIntegerField
      FieldName = 'Modalidade'
      Required = True
    end
    object QrSPC_CfgInfoExtra: TIntegerField
      FieldName = 'InfoExtra'
      Required = True
    end
    object QrSPC_CfgBAC_CMC7: TSmallintField
      FieldName = 'BAC_CMC7'
      Required = True
    end
    object QrSPC_CfgTipoCred: TSmallintField
      FieldName = 'TipoCred'
      Required = True
    end
    object QrSPC_CfgSenhaSocio: TWideStringField
      FieldName = 'SenhaSocio'
      Required = True
      Size = 15
    end
    object QrSPC_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrSPC_CfgValAviso: TFloatField
      FieldName = 'ValAviso'
      Required = True
    end
    object QrSPC_CfgVALCONSULTA: TFloatField
      FieldName = 'VALCONSULTA'
    end
  end
  object QrILi: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT CPF'
      'FROM lotesits'
      'WHERE Codigo=:P0'
      #10)
    Left = 952
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrILiCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
  end
  object QrDupsAtencao: TmySQLQuery
    Database = Dmod.MyDB
    Left = 785
    Top = 100
  end
end
