unit PesqDep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkGeral, DBCtrls, Db, mySQLDbTables,
  Grids, DBGrids, dmkEdit, ComCtrls, Variants, Menus, frxClass, frxDBSet,
  UnDmkProcFunc;

type
  TFmPesqDep = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrDepIts: TmySQLQuery;
    DsDepIts: TDataSource;
    Panel3: TPanel;
    BtPesq: TBitBtn;
    Valor: TLabel;
    frxDepIts: TfrxReport;
    frxDsDepIts: TfrxDBDataset;
    BtImprimir: TBitBtn;
    CkPeriodo: TCheckBox;
    TPIniDep: TDateTimePicker;
    TPFimDep: TDateTimePicker;
    dmkEdValor: TdmkEdit;
    QrDepItsCliente: TIntegerField;
    QrDepItsBanco: TIntegerField;
    QrDepItsAgencia: TIntegerField;
    QrDepItsConta: TWideStringField;
    QrDepItsCheque: TIntegerField;
    QrDepItsNF: TIntegerField;
    QrDepItsEmitente: TWideStringField;
    QrDepItsCPF: TWideStringField;
    QrDepItsVencto: TDateField;
    QrDepItsDDeposito: TDateField;
    QrDepItsValor: TFloatField;
    QrDepItsObsGerais: TWideStringField;
    QrDepItsCPF_TXT: TWideStringField;
    QrDepItsCodigo: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesqClick(Sender: TObject);
    procedure frxDepItsGetValue(const ParName: string; var ParValue: Variant);
    procedure BtImprimirClick(Sender: TObject);
    procedure CkPeriodoClick(Sender: TObject);
    procedure QrDepItsCalcFields(DataSet: TDataSet);
    procedure QrDepItsBeforeClose(DataSet: TDataSet);
    procedure QrDepItsAfterOpen(DataSet: TDataSet);
    procedure dmkEdValorChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmPesqDep: TFmPesqDep;

implementation

uses Module, UnInternalConsts, UMySQLModule, MyDBCheck, UnMyObjects;

{$R *.DFM}

procedure TFmPesqDep.BtImprimirClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxDepIts, 'Dep�sito de cheques');
end;

procedure TFmPesqDep.BtPesqClick(Sender: TObject);
var
  Valor : Double;
  Periodo : Boolean;
begin
  if CkPeriodo.Checked = false then
    if Application.MessageBox('N�o foi definido nenhum per�odo. A pesquisa '+
    #13#10+'poder� demorar. Deseja continuar'
    +#13#10+'assim mesmo?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) <> ID_YES then
      Exit;

  Screen.Cursor := crHourGlass;
  //
  Periodo := CkPeriodo.Checked;
  Valor   := dmkEdValor.ValueVariant;
  //
  QrDepIts.Close;
  QrDepIts.SQL.Clear;
  QrDepIts.SQL.Add('SELECT li.Codigo, lo.Cliente, li.Banco, li.Agencia, li.Conta,');
  QrDepIts.SQL.Add('li.Cheque, lo.NF, li.Emitente, li.CPF, li.Vencto,');
  QrDepIts.SQL.Add('li.DDeposito, li.Valor, li.ObsGerais');
  QrDepIts.SQL.Add('FROM lotesits li');
  QrDepIts.SQL.Add('LEFT JOIN lotes lo ON lo.Codigo = li.Codigo');
  QrDepIts.SQL.Add('WHERE lo.TxCompra+lo.ValValorem+ 1 >= 0.01');
  QrDepIts.SQL.Add('AND li.Depositado = 1');
  //
  if (Periodo = true) then
    QrDepIts.SQL.Add('AND li.DDeposito BETWEEN "' + Geral.FDT(TPIniDep.Date, 1) + '" AND "' + Geral.FDT(TPFimDep.Date, 1) +'"');
  //
  if Valor <> 0 then
    QrDepIts.SQL.Add('AND li.Valor =' + FloatToStr(Valor));
  //  
  QrDepIts.SQL.Add('ORDER BY li.Vencto, li.ObsGerais');
  QrDepIts.Open;
  //
  Screen.Cursor := crDefault;
  //
end;

procedure TFmPesqDep.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqDep.CkPeriodoClick(Sender: TObject);
begin
  TPIniDep.Visible := CkPeriodo.Checked;
  TPFimDep.Visible := CkPeriodo.Checked;
  QrDepIts.Close;
end;

procedure TFmPesqDep.dmkEdValorChange(Sender: TObject);
begin
  QrDepIts.Close;
end;

procedure TFmPesqDep.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPesqDep.FormCreate(Sender: TObject);
begin
  CkPeriodo.Checked := true;
  TPIniDep.Date := Geral.PrimeiroDiaDoMes(Date);
  TPFimDep.Date := Geral.UltimoDiaDoMes(Date);
  dmkEdValor.ValueVariant := 0;
end;

procedure TFmPesqDep.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;


procedure TFmPesqDep.frxDepItsGetValue(const ParName: string;
  var ParValue: Variant);
begin
  if ParName = 'VARF_PERIODO' then ParValue := dmkPF.PeriodoImp(TPIniDep.Date,
  TPFimDep.Date, 0, 0, True, True, False, False, '', '')
end;

procedure TFmPesqDep.QrDepItsAfterOpen(DataSet: TDataSet);
begin
  BtImprimir.Enabled := True;
end;

procedure TFmPesqDep.QrDepItsBeforeClose(DataSet: TDataSet);
begin
  BtImprimir.Enabled := False;
end;

procedure TFmPesqDep.QrDepItsCalcFields(DataSet: TDataSet);
begin
  QrDepItsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrDepItsCPF.Value);
end;

end.

