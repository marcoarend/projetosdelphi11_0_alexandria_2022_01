object FmCNAB240RemSel: TFmCNAB240RemSel
  Left = 339
  Top = 185
  Caption = 'Remessa Banc'#225'ria - Instru'#231#227'o Banc'#225'ria'
  ClientHeight = 494
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 446
    Width = 784
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Adiciona'
      TabOrder = 0
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 672
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtReabre: TBitBtn
      Tag = 18
      Left = 338
      Top = 6
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Caption = '&Reabre'
      ParentShowHint = False
      ShowHint = True
      TabOrder = 2
      OnClick = BtReabreClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    Caption = 'Remessa Banc'#225'ria - Instru'#231#227'o Banc'#225'ria'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 782
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 784
    Height = 398
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 784
      Height = 56
      Align = alTop
      TabOrder = 0
      object Label7: TLabel
        Left = 12
        Top = 8
        Width = 55
        Height = 13
        Caption = 'Movimento:'
      end
      object EdConfigBB: TdmkEditCB
        Left = 12
        Top = 24
        Width = 69
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object CBConfigBB: TdmkDBLookupComboBox
        Left = 80
        Top = 24
        Width = 705
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsOcorBank
        TabOrder = 1
        UpdType = utYes
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 145
      Width = 784
      Height = 253
      Align = alClient
      DataSource = DsPesq
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Duplicata'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'LOTE_COD'
          Title.Caption = 'Lote'
          Width = 52
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MY_ID'
          Title.Caption = 'Meu ID'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CPF_TXT'
          Title.Caption = 'CPF / CNPJ'
          Width = 113
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Emitente'
          Title.Caption = 'Sacado'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Bruto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DCompra'
          Title.Caption = 'D. Compra'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DDeposito'
          Title.Caption = 'D. Deposito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Banco'
          Title.Caption = 'Bco'
          Width = 24
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Agencia'
          Title.Caption = 'Ag'#234'n.'
          Width = 28
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CartDep'
          Title.Caption = 'Cart.Dep.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data3'
          Title.Caption = 'D.Pagto'
          Visible = True
        end>
    end
    object Panel4: TPanel
      Left = 0
      Top = 56
      Width = 784
      Height = 89
      Align = alTop
      TabOrder = 2
      object Label75: TLabel
        Left = 12
        Top = 4
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label1: TLabel
        Left = 12
        Top = 44
        Width = 45
        Height = 13
        Caption = 'Duplicata'
      end
      object Label4: TLabel
        Left = 112
        Top = 44
        Width = 61
        Height = 13
        Caption = 'CPF sacado:'
      end
      object Label2: TLabel
        Left = 244
        Top = 44
        Width = 40
        Height = 13
        Caption = 'Sacado:'
      end
      object Label55: TLabel
        Left = 420
        Top = 4
        Width = 38
        Height = 13
        Caption = 'Meu ID:'
      end
      object EdCliente: TdmkEditCB
        Left = 12
        Top = 20
        Width = 49
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 353
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMECLIENTE'
        ListSource = DsClientes
        TabOrder = 1
        UpdType = utYes
      end
      object EdDuplicata: TdmkEdit
        Left = 12
        Top = 60
        Width = 97
        Height = 21
        Alignment = taCenter
        CharCase = ecUpperCase
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdCPF: TdmkEdit
        Left = 112
        Top = 60
        Width = 129
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        OnExit = EdCPFExit
      end
      object EdEmitente: TdmkEdit
        Left = 244
        Top = 60
        Width = 233
        Height = 21
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdControle: TdmkEdit
        Left = 420
        Top = 20
        Width = 57
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object RGMascara: TRadioGroup
        Left = 482
        Top = 1
        Width = 103
        Height = 83
        Caption = ' M'#225'scara: '
        ItemIndex = 0
        Items.Strings = (
          'Prefixo e sufixo'
          'Prefixo'
          'Sufixo')
        TabOrder = 6
      end
      object CkEIni: TCheckBox
        Left = 590
        Top = 3
        Width = 97
        Height = 17
        Caption = 'Emiss'#227'o inicial:'
        TabOrder = 7
      end
      object TPEIni: TDateTimePicker
        Left = 589
        Top = 20
        Width = 88
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 8
      end
      object CkEFim: TCheckBox
        Left = 590
        Top = 43
        Width = 97
        Height = 17
        Caption = 'Emiss'#227'o final:'
        TabOrder = 9
      end
      object TPEFim: TDateTimePicker
        Left = 589
        Top = 60
        Width = 88
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 10
      end
      object CkVIni: TCheckBox
        Left = 682
        Top = 3
        Width = 97
        Height = 17
        Caption = 'Vencto inicial:'
        TabOrder = 11
      end
      object TPVIni: TDateTimePicker
        Left = 681
        Top = 20
        Width = 88
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 12
      end
      object CkVFim: TCheckBox
        Left = 682
        Top = 43
        Width = 83
        Height = 17
        Caption = 'Vencto final:'
        TabOrder = 13
      end
      object TPVFim: TDateTimePicker
        Left = 681
        Top = 60
        Width = 88
        Height = 21
        Date = 38675.714976851900000000
        Time = 38675.714976851900000000
        TabOrder = 14
      end
    end
  end
  object QrOcorBank: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Movimento, Nome'
      'FROM ocorbank'
      'WHERE Envio=1'
      'ORDER BY Nome')
    Left = 8
    Top = 8
    object QrOcorBankCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrOcorBankMovimento: TIntegerField
      FieldName = 'Movimento'
    end
    object QrOcorBankNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsOcorBank: TDataSource
    DataSet = QrOcorBank
    Left = 36
    Top = 8
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPesqCalcFields
    SQL.Strings = (
      'SELECT cbb.ConfigBB, CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial'
      
        'ELSE cli.Nome END NOMECLI, loi.Codigo LOTE_COD, loi.Controle MY_' +
        'ID,'
      
        'loi.Banco, loi.Agencia, loi.CPF, loi.Emitente, loi.Bruto, loi.DC' +
        'ompra,'
      
        'loi.DDeposito, loi.Vencto, loi.Duplicata, loi.Data3, loi.Cobranc' +
        'a,'
      'loi.CartDep, loi.Cliente'
      'FROM lotesits loi'
      'LEFT JOIN cobrancabb cbb ON cbb.Codigo=loi.Cobranca'
      'LEFT JOIN entidades cli ON cli.Codigo=loi.Cliente'
      'WHERE loi.Quitado < 2'
      'AND loi.Cobranca > 0'
      'AND cbb.ConfigBB = :P0'
      'ORDER BY DDeposito')
    Left = 64
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqConfigBB: TIntegerField
      FieldName = 'ConfigBB'
    end
    object QrPesqNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrPesqLOTE_COD: TIntegerField
      FieldName = 'LOTE_COD'
      Required = True
    end
    object QrPesqMY_ID: TIntegerField
      FieldName = 'MY_ID'
      Required = True
    end
    object QrPesqBanco: TIntegerField
      FieldName = 'Banco'
      Required = True
      DisplayFormat = '000'
    end
    object QrPesqAgencia: TIntegerField
      FieldName = 'Agencia'
      Required = True
      DisplayFormat = '0000'
    end
    object QrPesqCPF: TWideStringField
      FieldName = 'CPF'
      Size = 15
    end
    object QrPesqEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 50
    end
    object QrPesqBruto: TFloatField
      FieldName = 'Bruto'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPesqDCompra: TDateField
      FieldName = 'DCompra'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqDDeposito: TDateField
      FieldName = 'DDeposito'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqVencto: TDateField
      FieldName = 'Vencto'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqData3: TDateField
      FieldName = 'Data3'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPesqDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Required = True
      Size = 12
    end
    object QrPesqCobranca: TIntegerField
      FieldName = 'Cobranca'
      Required = True
    end
    object QrPesqCartDep: TIntegerField
      FieldName = 'CartDep'
      Required = True
    end
    object QrPesqCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrPesqCPF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CPF_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 92
    Top = 8
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN Tipo=0 THEN RazaoSocial'
      'ELSE Nome END NOMECLIENTE, Codigo'
      'FROM entidades'
      'WHERE Cliente1='#39'V'#39
      'ORDER BY NOMECLIENTE')
    Left = 328
    Top = 120
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 356
    Top = 120
  end
end
