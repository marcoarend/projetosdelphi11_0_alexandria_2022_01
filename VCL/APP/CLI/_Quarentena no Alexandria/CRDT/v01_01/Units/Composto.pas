unit Composto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit, dmkGeral, UnDmkEnums;

type
  TFmComposto = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Label11: TLabel;
    EdBaseT: TdmkEdit;
    Label1: TLabel;
    EdPrazo: TdmkEdit;
    EdJuroV: TdmkEdit;
    Label2: TLabel;
    Label3: TLabel;
    EdJuroP: TdmkEdit;
    Label4: TLabel;
    EdLiqui: TdmkEdit;
    EdTaxaM: TdmkEdit;
    Label5: TLabel;
    procedure BtOKClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdBaseTChange(Sender: TObject);
    procedure EdPrazoChange(Sender: TObject);
    procedure EdTaxaMChange(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmComposto: TFmComposto;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmComposto.BtOKClick(Sender: TObject);
var
  BaseT, JuroP, JuroV, Prazo, TaxaM, Liqui: Double;
begin
  BaseT := Geral.DMV(EdBaseT.Text);
  Prazo := Geral.DMV(EdPrazo.Text);
  TaxaM := Geral.DMV(EdTaxaM.Text);
  //
  JuroP := MLAGeral.CalculaJuroComposto(TaxaM, Prazo);
  JuroV := Trunc(JuroP * BaseT) / 100;
  Liqui := BaseT - JuroV;
  //
  EdJuroP.Text := Geral.FFT(JuroP, 6, siNegativo);
  EdJuroV.Text := Geral.FFT(JuroV, 2, siNegativo);
  EdLiqui.Text := Geral.FFT(Liqui, 2, siNegativo);
end;

procedure TFmComposto.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmComposto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmComposto.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmComposto.EdBaseTChange(Sender: TObject);
begin
  BtOKClick(Self);
end;

procedure TFmComposto.EdPrazoChange(Sender: TObject);
begin
  BtOKClick(Self);
end;

procedure TFmComposto.EdTaxaMChange(Sender: TObject);
begin
  BtOKClick(Self);
end;

end.
