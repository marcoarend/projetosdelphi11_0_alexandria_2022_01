object Fmwclients: TFmwclients
  Left = 368
  Top = 194
  Caption = 'Clientes WEB'
  ClientHeight = 347
  ClientWidth = 795
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelUser: TPanel
    Left = 0
    Top = 48
    Width = 795
    Height = 299
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Visible = False
    object Panel8: TPanel
      Left = 1
      Top = 250
      Width = 793
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Default = True
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BitBtn1Click
      end
      object Panel9: TPanel
        Left = 684
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn3: TBitBtn
          Tag = 15
          Left = 8
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object Panel10: TPanel
      Left = 1
      Top = 1
      Width = 793
      Height = 208
      Align = alTop
      TabOrder = 0
      object Label27: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label28: TLabel
        Left = 8
        Top = 44
        Width = 61
        Height = 13
        Caption = 'Observa'#231#227'o:'
        FocusControl = EdUObs
      end
      object Label33: TLabel
        Left = 84
        Top = 4
        Width = 50
        Height = 13
        Caption = 'Login [F4]:'
        FocusControl = DBEdit2
      end
      object Label34: TLabel
        Left = 432
        Top = 4
        Width = 55
        Height = 13
        Caption = 'Senha [F4]:'
        FocusControl = DBEdit6
      end
      object EdCodigo2: TdmkEdit
        Left = 8
        Top = 20
        Width = 73
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdULogin: TdmkEdit
        Left = 84
        Top = 20
        Width = 344
        Height = 21
        MaxLength = 32
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdULoginKeyDown
      end
      object EdUSenha: TdmkEdit
        Left = 432
        Top = 20
        Width = 344
        Height = 21
        MaxLength = 32
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdCNPJExit
        OnKeyDown = EdUSenhaKeyDown
      end
      object EdUObs: TdmkEdit
        Left = 8
        Top = 60
        Width = 769
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CkCriaLote: TCheckBox
        Left = 8
        Top = 88
        Width = 149
        Height = 17
        Caption = 'Permite criar novo lote.'
        TabOrder = 4
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 795
    Height = 299
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 793
      Height = 140
      Align = alTop
      Enabled = False
      TabOrder = 0
      object Label2: TLabel
        Left = 84
        Top = 4
        Width = 110
        Height = 13
        Caption = 'Razao social ou Nome:'
        FocusControl = DBEdit2
      end
      object Label6: TLabel
        Left = 564
        Top = 4
        Width = 68
        Height = 13
        Caption = 'CNPJ ou CPF:'
        FocusControl = DBEdit6
      end
      object Label7: TLabel
        Left = 680
        Top = 4
        Width = 53
        Height = 13
        Caption = 'I.E. ou RG:'
        FocusControl = DBEdit7
      end
      object Label97: TLabel
        Left = 8
        Top = 44
        Width = 57
        Height = 13
        Caption = 'Logradouro:'
        FocusControl = DBEdit106
      end
      object Label1: TLabel
        Left = 372
        Top = 44
        Width = 40
        Height = 13
        Caption = 'N'#250'mero:'
        FocusControl = DBEdit9
      end
      object Label4: TLabel
        Left = 440
        Top = 44
        Width = 67
        Height = 13
        Caption = 'Complemento:'
        FocusControl = DBEdit10
      end
      object Label11: TLabel
        Left = 8
        Top = 84
        Width = 30
        Height = 13
        Caption = 'Bairro:'
        FocusControl = DBEdit11
      end
      object Label12: TLabel
        Left = 220
        Top = 84
        Width = 36
        Height = 13
        Caption = 'Cidade:'
        FocusControl = DBEdit12
      end
      object Label14: TLabel
        Left = 452
        Top = 84
        Width = 17
        Height = 13
        Caption = 'UF:'
        FocusControl = DBEdit14
      end
      object Label104: TLabel
        Left = 492
        Top = 84
        Width = 24
        Height = 13
        Caption = 'CEP:'
        FocusControl = DBEdit14
      end
      object Label17: TLabel
        Left = 580
        Top = 84
        Width = 54
        Height = 13
        Caption = 'Telefone 1:'
        FocusControl = DBEdit17
      end
      object Label80: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit81
      end
      object Label5: TLabel
        Left = 696
        Top = 84
        Width = 68
        Height = 13
        Caption = 'Risco por lote:'
        FocusControl = DBEdit1
      end
      object DBEdit2: TDBEdit
        Left = 84
        Top = 20
        Width = 477
        Height = 21
        DataField = 'Nome'
        DataSource = Dswclients
        TabOrder = 0
      end
      object DBEdit6: TDBEdit
        Left = 564
        Top = 20
        Width = 113
        Height = 21
        DataField = 'CNPJ_TXT'
        DataSource = Dswclients
        TabOrder = 1
      end
      object DBEdit7: TDBEdit
        Left = 680
        Top = 20
        Width = 100
        Height = 21
        DataField = 'IE'
        DataSource = Dswclients
        TabOrder = 2
      end
      object DBEdit106: TDBEdit
        Left = 8
        Top = 60
        Width = 361
        Height = 21
        DataField = 'Rua'
        DataSource = Dswclients
        TabOrder = 3
      end
      object DBEdit9: TDBEdit
        Left = 372
        Top = 60
        Width = 64
        Height = 21
        DataField = 'Numero'
        DataSource = Dswclients
        TabOrder = 4
      end
      object DBEdit10: TDBEdit
        Left = 440
        Top = 60
        Width = 341
        Height = 21
        DataField = 'Compl'
        DataSource = Dswclients
        TabOrder = 5
      end
      object DBEdit11: TDBEdit
        Left = 8
        Top = 100
        Width = 209
        Height = 21
        DataField = 'Bairro'
        DataSource = Dswclients
        TabOrder = 6
      end
      object DBEdit12: TDBEdit
        Left = 220
        Top = 100
        Width = 229
        Height = 21
        DataField = 'Cidade'
        DataSource = Dswclients
        TabOrder = 7
      end
      object DBEdit14: TDBEdit
        Left = 452
        Top = 100
        Width = 37
        Height = 21
        DataField = 'UF'
        DataSource = Dswclients
        TabOrder = 8
      end
      object DBEdit15: TDBEdit
        Left = 492
        Top = 100
        Width = 85
        Height = 21
        DataField = 'CEP_TXT'
        DataSource = Dswclients
        TabOrder = 9
      end
      object DBEdit17: TDBEdit
        Left = 580
        Top = 100
        Width = 113
        Height = 21
        DataField = 'TEL1_TXT'
        DataSource = Dswclients
        TabOrder = 10
      end
      object DBEdit81: TDBEdit
        Left = 8
        Top = 20
        Width = 72
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = Dswclients
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 11
      end
      object DBEdit1: TDBEdit
        Left = 696
        Top = 100
        Width = 85
        Height = 21
        DataField = 'Risco'
        DataSource = Dswclients
        TabOrder = 12
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 250
      Width = 793
      Height = 48
      Align = alBottom
      TabOrder = 1
      object LaRegistro: TLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'ltimo registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Pr'#243'ximo registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Registro anterior'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          Hint = 'Primeiro registro'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 323
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtSaida: TBitBtn
          Tag = 13
          Left = 372
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
        object BtUsuario: TBitBtn
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Usu'#225'rio'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtUsuarioClick
        end
        object BtCliente: TBitBtn
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Cliente'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtClienteClick
        end
      end
    end
    object PageControl1: TPageControl
      Left = 144
      Top = 148
      Width = 597
      Height = 101
      ActivePage = TabSheet1
      TabOrder = 2
      OnChange = PageControl1Change
      object TabSheet1: TTabSheet
        Caption = 'Dados dos Usu'#225'rios'
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGUsuarios: TDBGrid
          Left = 0
          Top = 0
          Width = 589
          Height = 73
          Align = alClient
          DataSource = DsUsers
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'User_ID'
              Title.Caption = 'C'#243'digo'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Username'
              Title.Caption = 'Login'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Password'
              Title.Caption = 'Senha'
              Width = 80
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LoginID'
              Title.Caption = 'Identifica'#231#227'o do '#250'ltimo acesso'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LastAcess'
              Title.Caption = #218'ltimo acesso'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Obs'
              Title.Caption = 'Observa'#231#227'o'
              Width = 204
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECRIALOTE'
              Title.Caption = 'C.lote'
              Width = 32
              Visible = True
            end>
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Acessos do usu'#225'rio selecionado a '#225'rea de clientes'
        ImageIndex = 1
        ExplicitLeft = 0
        ExplicitTop = 0
        ExplicitWidth = 0
        ExplicitHeight = 0
        object DBGrid1: TDBGrid
          Left = 0
          Top = 0
          Width = 589
          Height = 73
          Align = alClient
          DataSource = DsVisitascli
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Seq.geral'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataHora'
              Title.Caption = 'Data e hora'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IP'
              Visible = True
            end>
        end
      end
    end
  end
  object PainelWeb: TPanel
    Left = 0
    Top = 48
    Width = 795
    Height = 299
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object Panel4: TPanel
      Left = 1
      Top = 250
      Width = 793
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConf2: TBitBtn
        Tag = 14
        Left = 4
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Default = True
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConf2Click
      end
      object Panel6: TPanel
        Left = 684
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn2: TBitBtn
          Tag = 15
          Left = 8
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn2Click
        end
      end
    end
    object DBGClientes: TDBGrid
      Left = 1
      Top = 49
      Width = 793
      Height = 156
      Align = alTop
      DataSource = DsClientes
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object Panel7: TPanel
      Left = 1
      Top = 1
      Width = 793
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label3: TLabel
        Left = 8
        Top = 4
        Width = 98
        Height = 13
        Caption = 'Pesquisa pelo nome:'
      end
      object SpeedButton5: TSpeedButton
        Left = 380
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label25: TLabel
        Left = 596
        Top = 4
        Width = 84
        Height = 13
        Caption = 'Senha inicial [F4]:'
      end
      object Label26: TLabel
        Left = 408
        Top = 4
        Width = 79
        Height = 13
        Caption = 'Login inicial [F4]:'
      end
      object EdPesq: TdmkEdit
        Left = 8
        Top = 20
        Width = 369
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdPesqChange
        OnKeyDown = EdPesqKeyDown
      end
      object EdPassword: TdmkEdit
        Left = 596
        Top = 20
        Width = 185
        Height = 21
        MaxLength = 32
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdPasswordKeyDown
      end
      object EdUsername: TdmkEdit
        Left = 408
        Top = 20
        Width = 185
        Height = 21
        MaxLength = 32
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdUsernameKeyDown
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 795
    Height = 299
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 250
      Width = 793
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 684
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 8
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 793
      Height = 208
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label15: TLabel
        Left = 8
        Top = 44
        Width = 84
        Height = 13
        Caption = 'Nome logradouro:'
        FocusControl = EdRua
      end
      object Label16: TLabel
        Left = 372
        Top = 44
        Width = 40
        Height = 13
        Caption = 'N'#250'mero:'
        FocusControl = EdNumero
      end
      object Label18: TLabel
        Left = 440
        Top = 44
        Width = 67
        Height = 13
        Caption = 'Complemento:'
        FocusControl = EdCompl
      end
      object Label19: TLabel
        Left = 8
        Top = 84
        Width = 30
        Height = 13
        Caption = 'Bairro:'
        FocusControl = EdBairro
      end
      object Label20: TLabel
        Left = 220
        Top = 84
        Width = 36
        Height = 13
        Caption = 'Cidade:'
        FocusControl = EdCidade
      end
      object Label8: TLabel
        Left = 84
        Top = 4
        Width = 110
        Height = 13
        Caption = 'Razao social ou Nome:'
        FocusControl = DBEdit2
      end
      object Label10: TLabel
        Left = 564
        Top = 4
        Width = 68
        Height = 13
        Caption = 'CNPJ ou CPF:'
        FocusControl = DBEdit6
      end
      object Label13: TLabel
        Left = 680
        Top = 4
        Width = 53
        Height = 13
        Caption = 'I.E. ou RG:'
        FocusControl = DBEdit7
      end
      object Label21: TLabel
        Left = 452
        Top = 84
        Width = 17
        Height = 13
        Caption = 'UF:'
        FocusControl = DBEdit14
      end
      object Label22: TLabel
        Left = 492
        Top = 84
        Width = 24
        Height = 13
        Caption = 'CEP:'
        FocusControl = DBEdit14
      end
      object Label23: TLabel
        Left = 580
        Top = 84
        Width = 54
        Height = 13
        Caption = 'Telefone 1:'
        FocusControl = DBEdit17
      end
      object Label24: TLabel
        Left = 696
        Top = 84
        Width = 68
        Height = 13
        Caption = 'Risco por lote:'
        FocusControl = DBEdit1
      end
      object Label29: TLabel
        Left = 8
        Top = 124
        Width = 53
        Height = 13
        Caption = 'Login [F4]: '
      end
      object Label30: TLabel
        Left = 196
        Top = 124
        Width = 55
        Height = 13
        Caption = 'Senha [F4]:'
      end
      object EdCodigo: TdmkEdit
        Left = 8
        Top = 20
        Width = 73
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdNome: TdmkEdit
        Left = 84
        Top = 20
        Width = 477
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCNPJ: TdmkEdit
        Left = 564
        Top = 20
        Width = 113
        Height = 21
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdCNPJExit
      end
      object EdIE: TdmkEdit
        Left = 680
        Top = 20
        Width = 100
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdRua: TdmkEdit
        Left = 8
        Top = 60
        Width = 361
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdNumero: TdmkEdit
        Left = 372
        Top = 60
        Width = 64
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCompl: TdmkEdit
        Left = 440
        Top = 60
        Width = 341
        Height = 21
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdBairro: TdmkEdit
        Left = 8
        Top = 100
        Width = 209
        Height = 21
        TabOrder = 7
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCidade: TdmkEdit
        Left = 220
        Top = 100
        Width = 229
        Height = 21
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCEP: TdmkEdit
        Left = 492
        Top = 100
        Width = 85
        Height = 21
        TabOrder = 10
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdCEPExit
      end
      object EdTel1: TdmkEdit
        Left = 580
        Top = 100
        Width = 113
        Height = 21
        TabOrder = 11
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnExit = EdTel1Exit
      end
      object EdUF: TdmkEdit
        Left = 452
        Top = 100
        Width = 37
        Height = 21
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdRisco: TdmkEdit
        Left = 696
        Top = 100
        Width = 85
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdUser2: TdmkEdit
        Left = 8
        Top = 140
        Width = 185
        Height = 21
        MaxLength = 32
        TabOrder = 13
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdUser2KeyDown
      end
      object EdPass2: TdmkEdit
        Left = 196
        Top = 140
        Width = 185
        Height = 21
        MaxLength = 32
        TabOrder = 14
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnKeyDown = EdPass2KeyDown
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 795
    Height = 48
    Align = alTop
    Alignment = taLeftJustify
    Caption = '                              Clientes WEB'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object LaTipo: TLabel
      Left = 712
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 486
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 481
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object Dswclients: TDataSource
    DataSet = Qrwclients
    Left = 572
    Top = 13
  end
  object Qrwclients: TmySQLQuery
    Database = Dmod.MyDBn
    BeforeOpen = QrwclientsBeforeOpen
    AfterOpen = QrwclientsAfterOpen
    BeforeClose = QrwclientsBeforeClose
    AfterScroll = QrwclientsAfterScroll
    OnCalcFields = QrwclientsCalcFields
    SQL.Strings = (
      'SELECT * FROM wclients'
      'WHERE Codigo > 0')
    Left = 544
    Top = 13
    object QrwclientsCNPJ: TWideStringField
      FieldName = 'CNPJ'
      Origin = 'wclients.CNPJ'
      Size = 15
    end
    object QrwclientsIE: TWideStringField
      FieldName = 'IE'
      Origin = 'wclients.IE'
      Size = 25
    end
    object QrwclientsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'wclients.Nome'
      Size = 50
    end
    object QrwclientsRua: TWideStringField
      FieldName = 'Rua'
      Origin = 'wclients.Rua'
      Size = 30
    end
    object QrwclientsNumero: TLargeintField
      FieldName = 'Numero'
      Origin = 'wclients.Numero'
    end
    object QrwclientsCompl: TWideStringField
      FieldName = 'Compl'
      Origin = 'wclients.Compl'
      Size = 30
    end
    object QrwclientsBairro: TWideStringField
      FieldName = 'Bairro'
      Origin = 'wclients.Bairro'
      Size = 30
    end
    object QrwclientsCidade: TWideStringField
      FieldName = 'Cidade'
      Origin = 'wclients.Cidade'
      Size = 25
    end
    object QrwclientsUF: TWideStringField
      FieldName = 'UF'
      Origin = 'wclients.UF'
      Size = 2
    end
    object QrwclientsCEP: TIntegerField
      FieldName = 'CEP'
      Origin = 'wclients.CEP'
    end
    object QrwclientsTel1: TWideStringField
      FieldName = 'Tel1'
      Origin = 'wclients.Tel1'
    end
    object QrwclientsRisco: TFloatField
      FieldName = 'Risco'
      Origin = 'wclients.Risco'
      DisplayFormat = '#,###,##0.00'
    end
    object QrwclientsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'wclients.Codigo'
    end
    object QrwclientsCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Calculated = True
    end
    object QrwclientsCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Size = 10
      Calculated = True
    end
    object QrwclientsTEL1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL1_TXT'
      Size = 40
      Calculated = True
    end
    object QrwclientsPassword: TWideStringField
      FieldName = 'Password'
      Origin = 'wclients.Password'
      Size = 32
    end
    object QrwclientsUsername: TWideStringField
      FieldName = 'Username'
      Origin = 'wclients.Username'
      Size = 32
    end
    object QrwclientsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'wclients.Lk'
    end
    object QrwclientsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'wclients.DataCad'
    end
    object QrwclientsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'wclients.DataAlt'
    end
    object QrwclientsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'wclients.UserCad'
    end
    object QrwclientsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'wclients.UserAlt'
    end
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrClientesAfterOpen
    BeforeClose = QrClientesBeforeClose
    SQL.Strings = (
      'SELECT Codigo, LimiCred,'
      'CASE WHEN Tipo=0 THEN RazaoSocial ELSE Nome END NOMECLI,'
      'CASE WHEN Tipo=0 THEN CNPJ    ELSE CPF     END CNPJCPF,'
      'CASE WHEN Tipo=0 THEN IE      ELSE RG      END IERG,'
      'CASE WHEN Tipo=0 THEN ERua    ELSE PRua    END Rua,'
      'CASE WHEN Tipo=0 THEN ENumero ELSE PNumero END Numero,'
      'CASE WHEN Tipo=0 THEN ECompl  ELSE PCompl  END Compl,'
      'CASE WHEN Tipo=0 THEN EBairro ELSE PBairro END Bairro,'
      'CASE WHEN Tipo=0 THEN ECidade ELSE PCidade END Cidade,'
      'CASE WHEN Tipo=0 THEN ECEP    ELSE PCEP    END CEP,'
      'CASE WHEN Tipo=0 THEN EUF     ELSE PUF     END UF,'
      'CASE WHEN Tipo=0 THEN ETe1    ELSE PTe1    END Tel1'
      'FROM entidades'
      'WHERE Cliente1 = "V"'
      'AND CASE WHEN Tipo=0 THEN RazaoSocial ELSE Nome END LIKE :P0'
      'ORDER BY NOMECLI')
    Left = 604
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrClientesCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 18
    end
    object QrClientesIERG: TWideStringField
      FieldName = 'IERG'
    end
    object QrClientesRua: TWideStringField
      FieldName = 'Rua'
      Size = 30
    end
    object QrClientesNumero: TLargeintField
      FieldName = 'Numero'
    end
    object QrClientesCompl: TWideStringField
      FieldName = 'Compl'
      Size = 30
    end
    object QrClientesBairro: TWideStringField
      FieldName = 'Bairro'
      Size = 30
    end
    object QrClientesCidade: TWideStringField
      FieldName = 'Cidade'
      Size = 25
    end
    object QrClientesCEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrClientesUF: TLargeintField
      FieldName = 'UF'
    end
    object QrClientesLimiCred: TFloatField
      FieldName = 'LimiCred'
    end
    object QrClientesTel1: TWideStringField
      FieldName = 'Tel1'
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 632
    Top = 12
  end
  object QrWPesq: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo'
      'FROM wclients'
      'WHERE Codigo = :P0')
    Left = 473
    Top = 141
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrWPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object PMCliente: TPopupMenu
    Left = 328
    Top = 260
    object Incluinovocliente1: TMenuItem
      Caption = '&Inclui novo cliente'
      OnClick = Incluinovocliente1Click
    end
    object Alteraclienteatual1: TMenuItem
      Caption = '&Altera cliente atual'
      OnClick = Alteraclienteatual1Click
    end
  end
  object PMUsuario: TPopupMenu
    OnPopup = PMUsuarioPopup
    Left = 440
    Top = 264
    object Incluinovousurio1: TMenuItem
      Caption = '&Inclui novo usu'#225'rio'
      OnClick = Incluinovousurio1Click
    end
    object Alterausurioatual1: TMenuItem
      Caption = '&Altera usu'#225'rio atual'
      OnClick = Alterausurioatual1Click
    end
    object Excluiusurioatual1: TMenuItem
      Caption = '&Exclui usu'#225'rio atual'
      Enabled = False
      OnClick = Excluiusurioatual1Click
    end
  end
  object QrUsers: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = QrUsersCalcFields
    SQL.Strings = (
      'SELECT * FROM users'
      'WHERE Codigo=:P0'
      'AND Username<>:P1'
      'AND Password<>:P2')
    Left = 380
    Top = 252
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrUsersUser_ID: TAutoIncField
      FieldName = 'User_ID'
    end
    object QrUsersCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUsersPassword: TWideStringField
      FieldName = 'Password'
      Size = 32
    end
    object QrUsersUsername: TWideStringField
      FieldName = 'Username'
      Size = 32
    end
    object QrUsersLoginID: TWideStringField
      FieldName = 'LoginID'
      Size = 32
    end
    object QrUsersObs: TWideStringField
      FieldName = 'Obs'
      Size = 255
    end
    object QrUsersLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrUsersDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrUsersDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrUsersUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrUsersUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrUsersLastAcess: TDateTimeField
      FieldName = 'LastAcess'
      DisplayFormat = 'dd/mm/yy hh:nn:ss'
    end
    object QrUsersCriaLote: TSmallintField
      FieldName = 'CriaLote'
    end
    object QrUsersNOMECRIALOTE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECRIALOTE'
      Size = 3
      Calculated = True
    end
  end
  object DsUsers: TDataSource
    DataSet = QrUsers
    Left = 408
    Top = 252
  end
  object QrVisitascli: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = QrUsersCalcFields
    SQL.Strings = (
      'SELECT * FROM visitascli'
      'WHERE Usuario=:P0'
      'ORDER BY DataHora DESC')
    Left = 464
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrVisitascliControle: TAutoIncField
      FieldName = 'Controle'
    end
    object QrVisitascliIP: TWideStringField
      FieldName = 'IP'
      Size = 64
    end
    object QrVisitascliDataHora: TDateTimeField
      FieldName = 'DataHora'
    end
    object QrVisitascliUsuario: TIntegerField
      FieldName = 'Usuario'
      DisplayFormat = '0000000'
    end
  end
  object DsVisitascli: TDataSource
    DataSet = QrVisitascli
    Left = 492
    Top = 204
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      '')
    Left = 617
    Top = 197
  end
  object QrWPesqUsers: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT User_ID'
      'FROM users'
      'WHERE Username=:P0'
      'AND Password=:P1'
      'AND Codigo=:P2')
    Left = 501
    Top = 141
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrWPesqUsersUser_ID: TIntegerField
      FieldName = 'User_ID'
    end
  end
end
