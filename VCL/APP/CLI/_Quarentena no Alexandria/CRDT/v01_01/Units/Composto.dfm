object FmComposto: TFmComposto
  Left = 419
  Top = 217
  Caption = 'Juro Composto'
  ClientHeight = 164
  ClientWidth = 576
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 576
    Height = 68
    Align = alClient
    TabOrder = 0
    object Label11: TLabel
      Left = 12
      Top = 8
      Width = 27
      Height = 13
      Caption = 'Valor:'
    end
    object Label1: TLabel
      Left = 116
      Top = 8
      Width = 54
      Height = 13
      Caption = 'Dias Prazo:'
    end
    object Label2: TLabel
      Left = 364
      Top = 8
      Width = 37
      Height = 13
      Caption = '$ Juros:'
    end
    object Label3: TLabel
      Left = 260
      Top = 8
      Width = 39
      Height = 13
      Caption = '% Juros:'
    end
    object Label4: TLabel
      Left = 468
      Top = 8
      Width = 39
      Height = 13
      Caption = 'L'#237'quido:'
    end
    object Label5: TLabel
      Left = 188
      Top = 8
      Width = 63
      Height = 13
      Caption = 'Taxa mensal:'
    end
    object EdBaseT: TdmkEdit
      Left = 12
      Top = 24
      Width = 101
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnChange = EdBaseTChange
    end
    object EdPrazo: TdmkEdit
      Left = 116
      Top = 24
      Width = 69
      Height = 21
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnChange = EdPrazoChange
    end
    object EdJuroV: TdmkEdit
      Left = 364
      Top = 24
      Width = 101
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 2
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdJuroP: TdmkEdit
      Left = 260
      Top = 24
      Width = 101
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdLiqui: TdmkEdit
      Left = 468
      Top = 24
      Width = 101
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 4
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdTaxaM: TdmkEdit
      Left = 188
      Top = 24
      Width = 69
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      OnChange = EdTaxaMChange
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 116
    Width = 576
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object BtSaida: TBitBtn
      Tag = 13
      Left = 470
      Top = 4
      Width = 90
      Height = 40
      Cursor = crHandPoint
      Hint = 'Sai da janela atual'
      Caption = '&Sa'#237'da'
      NumGlyphs = 2
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
      OnClick = BtSaidaClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 576
    Height = 48
    Align = alTop
    Caption = 'Juro Composto'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 574
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 580
      ExplicitHeight = 44
    end
  end
end
