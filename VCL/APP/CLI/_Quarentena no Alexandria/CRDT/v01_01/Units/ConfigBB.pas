unit ConfigBB;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  Variants, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkCheckGroup,
  UnDmkProcFunc, UnDmkEnums;

type
  TFmConfigBB = class(TForm)
    PainelDados: TPanel;
    DsConfigBB: TDataSource;
    QrConfigBB: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    PainelEdit: TPanel;
    PainelData: TPanel;
    QrConfigBBCodigo: TIntegerField;
    QrConfigBBNome: TWideStringField;
    QrConfigBBCarteira: TWideStringField;
    QrConfigBBVariacao: TWideStringField;
    QrConfigBBSiglaEspecie: TWideStringField;
    QrConfigBBMoeda: TWideStringField;
    QrConfigBBAceite: TSmallintField;
    QrConfigBBPgAntes: TSmallintField;
    QrConfigBBProtestar: TSmallintField;
    QrConfigBBMsgLinha1: TWideStringField;
    QrConfigBBConvenio: TIntegerField;
    Panel1: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    EdNome: TdmkEdit;
    Label10: TLabel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Panel2: TPanel;
    CkPgAntes: TCheckBox;
    RGSiglaEspecie: TRadioGroup;
    RGMoeda: TRadioGroup;
    RGAceite: TRadioGroup;
    EdOutraSigla: TdmkEdit;
    TabSheet2: TTabSheet;
    Panel6: TPanel;
    Label31: TLabel;
    EdMsgLinha1: TdmkEdit;
    Label8: TLabel;
    EdConvenio: TdmkEdit;
    EdCarteira: TdmkEdit;
    Label11: TLabel;
    Label12: TLabel;
    EdVariacao: TdmkEdit;
    Panel7: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    PageControl2: TPageControl;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    Panel8: TPanel;
    Panel9: TPanel;
    GroupBox1: TGroupBox;
    DBEdit14: TDBEdit;
    DBRadioGroup1: TDBRadioGroup;
    DBRadioGroup2: TDBRadioGroup;
    DBCheckBox1: TDBCheckBox;
    Label30: TLabel;
    DBEdit15: TDBEdit;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    Label4: TLabel;
    Label5: TLabel;
    DBEdit3: TDBEdit;
    GroupBox3: TGroupBox;
    RGMultaCodi: TRadioGroup;
    Label32: TLabel;
    EdMultaValr: TdmkEdit;
    Label33: TLabel;
    EdMultaPerc: TdmkEdit;
    Label34: TLabel;
    EdMultaDias: TdmkEdit;
    QrConfigBBMultaCodi: TSmallintField;
    QrConfigBBMultaDias: TSmallintField;
    QrConfigBBMultaValr: TFloatField;
    QrConfigBBMultaPerc: TFloatField;
    GroupBox4: TGroupBox;
    DBRadioGroup3: TDBRadioGroup;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    DBEdit16: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit18: TDBEdit;
    RGMultaTiVe: TRadioGroup;
    DBRadioGroup4: TDBRadioGroup;
    RGImpreLoc: TRadioGroup;
    QrConfigBBMultaTiVe: TSmallintField;
    QrConfigBBImpreLoc: TSmallintField;
    DBRadioGroup5: TDBRadioGroup;
    RGModalidade: TRadioGroup;
    QrConfigBBModalidade: TIntegerField;
    DBRGModalidade: TDBRadioGroup;
    GroupBox5: TGroupBox;
    Label6: TLabel;
    EdclcAgencNr: TdmkEdit;
    EdclcAgencDV: TdmkEdit;
    Label7: TLabel;
    Label13: TLabel;
    EdclcContaNr: TdmkEdit;
    EdclcContaDV: TdmkEdit;
    Label17: TLabel;
    QrConfigBBclcAgencNr: TWideStringField;
    QrConfigBBclcAgencDV: TWideStringField;
    QrConfigBBclcContaNr: TWideStringField;
    QrConfigBBclcContaDV: TWideStringField;
    GroupBox6: TGroupBox;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    Label38: TLabel;
    Label39: TLabel;
    Label40: TLabel;
    Label41: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    GroupBox7: TGroupBox;
    Label42: TLabel;
    Label43: TLabel;
    Label44: TLabel;
    Label45: TLabel;
    EdcedAgencNr: TdmkEdit;
    EdcedAgencDV: TdmkEdit;
    EdcedContaNr: TdmkEdit;
    EdcedContaDV: TdmkEdit;
    QrConfigBBcedAgencNr: TWideStringField;
    QrConfigBBcedAgencDV: TWideStringField;
    QrConfigBBcedContaNr: TWideStringField;
    QrConfigBBcedContaDV: TWideStringField;
    GroupBox8: TGroupBox;
    Label46: TLabel;
    Label47: TLabel;
    Label48: TLabel;
    Label49: TLabel;
    DBEdit21: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit23: TDBEdit;
    DBEdit24: TDBEdit;
    TabSheet5: TTabSheet;
    RGEspecie: TRadioGroup;
    QrConfigBBEspecie: TSmallintField;
    TabSheet6: TTabSheet;
    DBRGEspecie: TDBRadioGroup;
    RGProtestar: TRadioGroup;
    EdCorrido: TdmkEdit;
    QrConfigBBCorrido: TSmallintField;
    DBRGProtestar: TDBRadioGroup;
    DBEdit25: TDBEdit;
    EdProduto: TdmkEdit;
    Label51: TLabel;
    DBEdit26: TDBEdit;
    QrConfigBBIDEmpresa: TWideStringField;
    QrConfigBBBanco: TIntegerField;
    QrConfigBBProduto: TWideStringField;
    Label52: TLabel;
    DBEdit27: TDBEdit;
    EdBanco: TdmkEdit;
    Label53: TLabel;
    Label50: TLabel;
    TabSheet7: TTabSheet;
    DBCheckBox2: TDBCheckBox;
    QrConfigBBInfoCovH: TSmallintField;
    TabSheet8: TTabSheet;
    CkInfoCovH: TCheckBox;
    RGCarteira240: TRadioGroup;
    DBRGCarteira240: TDBRadioGroup;
    QrConfigBBCarteira240: TWideStringField;
    DBRGCadastramento: TDBRadioGroup;
    DBRGTradiEscrit: TDBRadioGroup;
    DBRGDistribuicao: TDBRadioGroup;
    DBRGAceite240: TDBRadioGroup;
    QrConfigBBCadastramento: TWideStringField;
    QrConfigBBTradiEscrit: TWideStringField;
    QrConfigBBDistribuicao: TWideStringField;
    QrConfigBBAceite240: TWideStringField;
    QrConfigBBProtesto: TWideStringField;
    QrConfigBBProtestodd: TIntegerField;
    QrConfigBBBaixaDevol: TWideStringField;
    QrConfigBBBaixaDevoldd: TIntegerField;
    DBRGProtesto: TDBRadioGroup;
    Label54: TLabel;
    DBEdit28: TDBEdit;
    DBRGBaixaDevol: TDBRadioGroup;
    Label55: TLabel;
    DBEdit29: TDBEdit;
    RGCadastramento: TRadioGroup;
    RGDistribuicao: TRadioGroup;
    RGAceite240: TRadioGroup;
    RGTradiEscrit: TRadioGroup;
    EdBaixaDevoldd: TdmkEdit;
    Label56: TLabel;
    RGBaixaDevol: TRadioGroup;
    EdProtestodd: TdmkEdit;
    RGProtesto: TRadioGroup;
    Label57: TLabel;
    DBRGEmisBloqueto: TDBRadioGroup;
    QrConfigBBEmisBloqueto: TWideStringField;
    RGEmisBloqueto: TRadioGroup;
    DBRGEspecie240: TDBRadioGroup;
    RGEspecie240: TRadioGroup;
    QrConfigBBEspecie240: TWideStringField;
    TabSheet9: TTabSheet;
    RGJuros240Cod: TRadioGroup;
    Label58: TLabel;
    EdJuros240Qtd: TdmkEdit;
    QrConfigBBJuros240Cod: TWideStringField;
    QrConfigBBJuros240Qtd: TFloatField;
    DBRGJuros240Cod: TDBRadioGroup;
    DBEdit30: TDBEdit;
    Label59: TLabel;
    Label60: TLabel;
    EdContrOperCred: TdmkEdit;
    QrConfigBBContrOperCred: TIntegerField;
    Label61: TLabel;
    DBEdit31: TDBEdit;
    Label62: TLabel;
    EdReservBanco: TdmkEdit;
    Label63: TLabel;
    Label64: TLabel;
    EdReservEmprs: TdmkEdit;
    Label65: TLabel;
    EdLH_208_33: TdmkEdit;
    Label66: TLabel;
    EdSQ_233_008: TdmkEdit;
    Label67: TLabel;
    EdTL_124_117: TdmkEdit;
    Label68: TLabel;
    EdSR_208_033: TdmkEdit;
    TabSheet10: TTabSheet;
    QrConfigBBReservBanco: TWideStringField;
    QrConfigBBReservEmprs: TWideStringField;
    QrConfigBBLH_208_33: TWideStringField;
    QrConfigBBSQ_233_008: TWideStringField;
    QrConfigBBSR_208_033: TWideStringField;
    QrConfigBBTL_124_117: TWideStringField;
    DBEdit32: TDBEdit;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    DBEdit35: TDBEdit;
    DBEdit36: TDBEdit;
    Label74: TLabel;
    DBEdit37: TDBEdit;
    Label69: TLabel;
    Label70: TLabel;
    Label71: TLabel;
    Label72: TLabel;
    Label75: TLabel;
    TabSheet11: TTabSheet;
    GroupBox9: TGroupBox;
    GroupBox10: TGroupBox;
    Label73: TLabel;
    EdLiqAntMen: TdmkEditCB;
    CBLiqAntMen: TdmkDBLookupComboBox;
    Label76: TLabel;
    EdLiqAntVal: TdmkEditCB;
    CBLiqAntVal: TdmkDBLookupComboBox;
    Label77: TLabel;
    EdLiqAntMai: TdmkEditCB;
    CBLiqAntMai: TdmkDBLookupComboBox;
    Label78: TLabel;
    EdLiqVenMen: TdmkEditCB;
    CBLiqVenMen: TdmkDBLookupComboBox;
    Label79: TLabel;
    EdLiqVenVal: TdmkEditCB;
    CBLiqVenVal: TdmkDBLookupComboBox;
    Label80: TLabel;
    EdLiqVenMai: TdmkEditCB;
    CBLiqVenMai: TdmkDBLookupComboBox;
    Label81: TLabel;
    EdLiqVcdMen: TdmkEditCB;
    CBLiqVcdMen: TdmkDBLookupComboBox;
    Label82: TLabel;
    EdLiqVcdVal: TdmkEditCB;
    CBLiqVcdVal: TdmkDBLookupComboBox;
    Label83: TLabel;
    EdLiqVcdMai: TdmkEditCB;
    CBLiqVcdMai: TdmkDBLookupComboBox;
    DsOcorDupl: TDataSource;
    QrOcorDupl: TmySQLQuery;
    TabSheet12: TTabSheet;
    GroupBox11: TGroupBox;
    GroupBox12: TGroupBox;
    Label84: TLabel;
    Label85: TLabel;
    Label86: TLabel;
    Label87: TLabel;
    Label88: TLabel;
    Label89: TLabel;
    Label90: TLabel;
    Label91: TLabel;
    Label92: TLabel;
    QrOcorDuplCodigo: TIntegerField;
    QrOcorDuplNome: TWideStringField;
    QrOcorDuplBase: TFloatField;
    QrOcorDuplLk: TIntegerField;
    QrOcorDuplDataCad: TDateField;
    QrOcorDuplDataAlt: TDateField;
    QrOcorDuplUserCad: TIntegerField;
    QrOcorDuplUserAlt: TIntegerField;
    QrOcorDuplDatapbase: TIntegerField;
    QrOcorDuplOcorrbase: TIntegerField;
    QrConfigBBLk: TIntegerField;
    QrConfigBBDataCad: TDateField;
    QrConfigBBDataAlt: TDateField;
    QrConfigBBUserCad: TIntegerField;
    QrConfigBBUserAlt: TIntegerField;
    QrConfigBBLiqAntMen: TIntegerField;
    QrConfigBBLiqAntVal: TIntegerField;
    QrConfigBBLiqAntMai: TIntegerField;
    QrConfigBBLiqVenMen: TIntegerField;
    QrConfigBBLiqVenVal: TIntegerField;
    QrConfigBBLiqVenMai: TIntegerField;
    QrConfigBBLiqVcdMen: TIntegerField;
    QrConfigBBLiqVcdVal: TIntegerField;
    QrConfigBBLiqVcdMai: TIntegerField;
    QrConfigBBNOMELAN: TWideStringField;
    QrConfigBBNOMELAL: TWideStringField;
    QrConfigBBNOMELAI: TWideStringField;
    QrConfigBBNOMELVN: TWideStringField;
    QrConfigBBNOMELVL: TWideStringField;
    QrConfigBBNOMELVI: TWideStringField;
    QrConfigBBNOMELDN: TWideStringField;
    QrConfigBBNOMELDL: TWideStringField;
    QrConfigBBNOMELDI: TWideStringField;
    DBEdit38: TDBEdit;
    DBEdit39: TDBEdit;
    DBEdit40: TDBEdit;
    DBEdit41: TDBEdit;
    DBEdit42: TDBEdit;
    DBEdit43: TDBEdit;
    DBEdit44: TDBEdit;
    DBEdit45: TDBEdit;
    DBEdit46: TDBEdit;
    QrocorBank: TmySQLQuery;
    DsOcorBank: TDataSource;
    QrocorBankCodigo: TIntegerField;
    QrocorBankNome: TWideStringField;
    QrocorBankLk: TIntegerField;
    QrocorBankDataCad: TDateField;
    QrocorBankDataAlt: TDateField;
    QrocorBankUserCad: TIntegerField;
    QrocorBankUserAlt: TIntegerField;
    QrocorBankBase: TFloatField;
    TabSheet13: TTabSheet;
    TabSheet14: TTabSheet;
    CheckGroup1: TdmkCheckGroup;
    Label18: TLabel;
    EdDiretorio: TdmkEdit;
    QrConfigBBDiretorio: TWideStringField;
    Label19: TLabel;
    DBEdit6: TDBEdit;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrConfigBBAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrConfigBBBeforeOpen(DataSet: TDataSet);
    procedure EdConvenioExit(Sender: TObject);
    procedure EdclcAgencNrExit(Sender: TObject);
    procedure EdclcAgencDVExit(Sender: TObject);
    procedure EdclcContaNrExit(Sender: TObject);
    procedure EdclcContaDVExit(Sender: TObject);
    procedure EdcedAgencNrExit(Sender: TObject);
    procedure EdcedAgencDVExit(Sender: TObject);
    procedure EdcedContaNrExit(Sender: TObject);
    procedure EdcedContaDVExit(Sender: TObject);
    procedure EdCorridoExit(Sender: TObject);
    procedure EdBancoChange(Sender: TObject);
    procedure RGJuros240CodClick(Sender: TObject);
    procedure EdContrOperCredExit(Sender: TObject);
    procedure EdDiretorioKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    FSeq: Integer;
  end;

var
  FmConfigBB: TFmConfigBB;
const
  FFormatFloat = '000';

implementation

uses Module, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmConfigBB.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmConfigBB.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrConfigBBCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmConfigBB.DefParams;
begin
  VAR_GOTOTABELA := 'ConfigBB';
  VAR_GOTOMYSQLTABLE := QrConfigBB;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT cbb.*, ');
  VAR_SQLx.Add('lan.Nome NOMELAN, lal.Nome NOMELAL, lai.Nome NOMELAI,');
  VAR_SQLx.Add('lvn.Nome NOMELVN, lvl.Nome NOMELVL, lvi.Nome NOMELVI,');
  VAR_SQLx.Add('ldn.Nome NOMELDN, ldl.Nome NOMELDL, ldi.Nome NOMELDI');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('FROM configbb cbb');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('LEFT JOIN ocordupl lan ON lan.Codigo=cbb.LiqAntMen');
  VAR_SQLx.Add('LEFT JOIN ocordupl lal ON lal.Codigo=cbb.LiqAntVal');
  VAR_SQLx.Add('LEFT JOIN ocordupl lai ON lai.Codigo=cbb.LiqAntMai');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('LEFT JOIN ocordupl lvn ON lvn.Codigo=cbb.LiqVenMen');
  VAR_SQLx.Add('LEFT JOIN ocordupl lvl ON lvl.Codigo=cbb.LiqVenVal');
  VAR_SQLx.Add('LEFT JOIN ocordupl lvi ON lvi.Codigo=cbb.LiqVenMai');
  VAR_SQLx.Add('');
  VAR_SQLx.Add('LEFT JOIN ocordupl ldn ON ldn.Codigo=cbb.LiqVcdMen');
  VAR_SQLx.Add('LEFT JOIN ocordupl ldl ON ldl.Codigo=cbb.LiqVcdVal');
  VAR_SQLx.Add('LEFT JOIN ocordupl ldi ON ldi.Codigo=cbb.LiqVcdMai');
  VAR_SQLx.Add('');
  //
  VAR_SQL1.Add('WHERE cbb.Codigo=:P0');
  //
  VAR_SQLa.Add('WHERE cbb.Nome LIKE :P0');
  //
end;

procedure TFmConfigBB.MostraEdicao(Mostra : Integer; Status : String; Codigo : Integer);
var
  i: Integer;
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PageControl1.ActivePageIndex := PageControl2.ActivePageIndex;
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      EdNome.Text := CO_VAZIO;
      EdNome.Visible := True;
      PainelControle.Visible:=False;
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.Text    := '';
        EdNome.Text      := '';
        EdConvenio.Text  := '';
        EdCarteira.Text  := '';
        EdVariacao.Text  := '';
        //
        EdMultaDias.Text := '1';
        EdCorrido.Text := '6';
        //EdIDEmpresa.Text := '';
        EdBanco.Text := '';
        EdProduto.Text := '';
        EdMultaValr.Text := '0.00';
        EdMultaPerc.Text := '2.00';
        RGMultaCodi.ItemIndex := 2;
        RGMultaTiVe.ItemIndex := 1;
        RGImpreLoc.ItemIndex := 0;
        RGModalidade.ItemIndex := 0;
        RGCarteira240.ItemIndex := 0;
        RGCadastramento.ItemIndex := 0;
        RGTradiEscrit.ItemIndex := 0;
        RGDistribuicao.ItemIndex := 0;
        RGAceite240.ItemIndex := 0;
        RGProtesto.ItemIndex := 0;
        RGBaixaDevol.ItemIndex := 0;
        RGEmisBloqueto.ItemIndex := 0;
        RGEspecie240.ItemIndex := 1;
        RGJuros240Cod.ItemIndex := 1;
        EdProtestodd.Text := '0';
        EdBaixaDevoldd.Text := '0';
        EdJuros240Qtd.Text := '1,00';
        EdclcAgencNr.Text      := '0000';
        EdclcAgencDV.Text      := '0';
        EdclcContaNr.Text      := '00000000';
        EdclcContaDV.Text      := '0';
        EdcedAgencNr.Text      := '0000';
        EdcedAgencDV.Text      := '0';
        EdcedContaNr.Text      := '00000000';
        EdcedContaDV.Text      := '0';
        EdContrOperCred.Text   := '';
        //
        EdReservBanco.Text     := '';
        EdReservEmprs.Text     := '';
        EdLH_208_33.Text       := '';
        EdSQ_233_008.Text      := '';
        EdSR_208_033.Text      := '';
        EdTL_124_117.Text      := '';
        //
        RGSiglaEspecie.ItemIndex := RGSiglaEspecie.Items.Count -1;
        EdOutraSigla.Text        := '?????';
        RGSiglaEspecie.ItemIndex := 4;
        RGAceite.ItemIndex       := 0;
        CkPgAntes.Checked        := False;
        RGProtestar.ItemIndex    := RGProtestar.Items.Count -1;
        RGEspecie.ItemIndex      := 0;
        EdMsgLinha1.Text         := '';
        CkInfoCovH.Checked       := False;
        //
        EdLiqAntMen.Text         := '';
        EdLiqAntVal.Text         := '';
        EdLiqAntMai.Text         := '';
        EdLiqVenMen.Text         := '';
        EdLiqVenVal.Text         := '';
        EdLiqVenMai.Text         := '';
        EdLiqVcdMen.Text         := '';
        EdLiqVcdVal.Text         := '';
        EdLiqVcdMai.Text         := '';
        EdDiretorio.Text         := '';
        //
        CBLiqAntMen.KeyValue     := NULL;
        CBLiqAntVal.KeyValue     := NULL;
        CBLiqAntMai.KeyValue     := NULL;
        CBLiqVenMen.KeyValue     := NULL;
        CBLiqVenVal.KeyValue     := NULL;
        CBLiqVenMai.KeyValue     := NULL;
        CBLiqVcdMen.KeyValue     := NULL;
        CBLiqVcdVal.KeyValue     := NULL;
        CBLiqVcdMai.KeyValue     := NULL;
        //
      end else begin
        EdOutraSigla.Text := '';
        EdCodigo.Text    := IntToStr(QrConfigBBCodigo.Value);
        EdNome.Text      := QrConfigBBNome.Value;
        EdConvenio.Text  := IntToStr(QrConfigBBConvenio.Value);
        EdCarteira.Text  := QrConfigBBCarteira.Value;
        EdVariacao.Text  := QrConfigBBVariacao.Value;
        //
        EdCorrido.Text := IntToStr(QrConfigBBCorrido.Value);
        //EdIDEmpresa.Text := QrConfigBBIDEmpresa.Value;
        EdBanco.Text := FormatFloat('000', QrConfigBBBanco.Value);
        EdProduto.Text := QrConfigBBProduto.Value;
        EdMultaDias.Text := IntToStr(QrConfigBBMultaDias.Value);
        EdMultaValr.Text := Geral.FFT(QrConfigBBMultaValr.Value, 2, siPositivo);
        EdMultaPerc.Text := Geral.FFT(QrConfigBBMultaPerc.Value, 2, siPositivo);
        RGMultaCodi.ItemIndex := QrConfigBBMultaCodi.Value;
        RGMultaTiVe.ItemIndex := QrConfigBBMultaTiVe.Value;
        RGImpreLoc.ItemIndex := QrConfigBBImpreLoc.Value;
        RGModalidade.ItemIndex := DBRGModalidade.ItemIndex;
        RGCarteira240.ItemIndex := DBRGCarteira240.ItemIndex;
        RGCadastramento.ItemIndex := DBRGCadastramento.ItemIndex;
        RGDistribuicao.ItemIndex := DBRGDistribuicao.ItemIndex;
        RGTradiEscrit.ItemIndex := DBRGTradiEscrit.ItemIndex;
        RGAceite240.ItemIndex := DBRGAceite240.ItemIndex;
        RGProtesto.ItemIndex := DBRGProtesto.ItemIndex;
        RGBaixaDevol.ItemIndex := DBRGBaixaDevol.ItemIndex;
        RGEmisBloqueto.ItemIndex := DBRGEmisBloqueto.ItemIndex;
        RGEspecie240.ItemIndex := DBRGEspecie240.ItemIndex;
        RGJuros240Cod.ItemIndex := DBRGJuros240Cod.ItemIndex;
        EdProtestodd.Text := IntToStr(QrConfigBBProtestodd.Value);
        EdBaixaDevoldd.Text := IntToStr(QrConfigBBBaixaDevoldd.Value);
        EdJuros240Qtd.Text := Geral.FFT(QrConfigBBJuros240Qtd.Value, 2, siPositivo);
        EdContrOperCred.Text := Geral.TFD(IntToStr(QrConfigBBContrOperCred.Value), 10, siPositivo);
        //
        //
        EdclcAgencNr.Text      := QrConfigBBclcAgencNr.Value;
        EdclcAgencDV.Text      := QrConfigBBclcAgencDV.Value;
        EdclcContaNr.Text      := QrConfigBBclcContaNr.Value;
        EdclcContaDV.Text      := QrConfigBBclcContaDV.Value;
        //
        EdcedAgencNr.Text      := QrConfigBBcedAgencNr.Value;
        EdcedAgencDV.Text      := QrConfigBBcedAgencDV.Value;
        EdcedContaNr.Text      := QrConfigBBcedContaNr.Value;
        EdcedContaDV.Text      := QrConfigBBcedContaDV.Value;
        //
        EdReservBanco.Text     := QrConfigBBReservBanco.Value;
        EdReservEmprs.Text     := QrConfigBBReservEmprs.Value;
        EdLH_208_33.Text       := QrConfigBBLH_208_33.Value;
        EdSQ_233_008.Text      := QrConfigBBSQ_233_008.Value;
        EdSR_208_033.Text      := QrConfigBBSR_208_033.Value;
        EdTL_124_117.Text      := QrConfigBBTL_124_117.Value;
        //
        RGSiglaEspecie.ItemIndex := RGSiglaEspecie.Items.Count -1;
        for i := 0 to RGSiglaEspecie.Items.Count -1 do
        begin
          if pos('('+QrConfigBBSiglaEspecie.Value+')', RGSiglaEspecie.Items[i])
          > 0 then RGSiglaEspecie.ItemIndex := i;
        end;
        if RGSiglaEspecie.ItemIndex = RGSiglaEspecie.Items.Count -1 then
        EdOutraSigla.Text := QrConfigBBSiglaEspecie.Value;
        RGMoeda.ItemIndex := 0;
        for i := 0 to RGMoeda.Items.Count -1 do
        begin
          if Geral.CompletaString(QrConfigBBMoeda.Value, ' ', 5,
          taLeftJustify, True) = Copy(RGMoeda.Items[i], 1, 5) then
            RGMoeda.ItemIndex := i;
        end;
        RGAceite.ItemIndex := QrConfigBBAceite.Value;
        CkPgAntes.Checked := Geral.IntToBool_0(QrConfigBBPgAntes.Value);
        RGProtestar.ItemIndex := DBRGProtestar.ItemIndex;
        RGEspecie.ItemIndex := DBRGEspecie.ItemIndex;
        if RGProtestar.ItemIndex = -1 then
          RGProtestar.ItemIndex := RGProtestar.Items.Count -1;
        if RGEspecie.ItemIndex = -1 then
          RGEspecie.ItemIndex := RGEspecie.Items.Count -1;
        EdMsgLinha1.Text         := QrConfigBBMsgLinha1.Value;
        CkInfoCovH.Checked       := Geral.IntToBool_0(QrConfigBBInfoCovH.Value);
        //
        EdLiqAntMen.Text         := IntToStr(QrConfigBBLiqAntMen.Value);
        EdLiqAntVal.Text         := IntToStr(QrConfigBBLiqAntVal.Value);
        EdLiqAntMai.Text         := IntToStr(QrConfigBBLiqAntMai.Value);
        EdLiqVenMen.Text         := IntToStr(QrConfigBBLiqVenMen.Value);
        EdLiqVenVal.Text         := IntToStr(QrConfigBBLiqVenVal.Value);
        EdLiqVenMai.Text         := IntToStr(QrConfigBBLiqVenMai.Value);
        EdLiqVcdMen.Text         := IntToStr(QrConfigBBLiqVcdMen.Value);
        EdLiqVcdVal.Text         := IntToStr(QrConfigBBLiqVcdVal.Value);
        EdLiqVcdMai.Text         := IntToStr(QrConfigBBLiqVcdMai.Value);
        EdDiretorio.Text         := QrConfigBBDiretorio.Text;
        //
        //
        CBLiqAntMen.KeyValue     := QrConfigBBLiqAntMen.Value;
        CBLiqAntVal.KeyValue     := QrConfigBBLiqAntVal.Value;
        CBLiqAntMai.KeyValue     := QrConfigBBLiqAntMai.Value;
        CBLiqVenMen.KeyValue     := QrConfigBBLiqVenMen.Value;
        CBLiqVenVal.KeyValue     := QrConfigBBLiqVenVal.Value;
        CBLiqVenMai.KeyValue     := QrConfigBBLiqVenMai.Value;
        CBLiqVcdMen.KeyValue     := QrConfigBBLiqVcdMen.Value;
        CBLiqVcdVal.KeyValue     := QrConfigBBLiqVcdVal.Value;
        CBLiqVcdMai.KeyValue     := QrConfigBBLiqVcdMai.Value;
        //
      end;
      (*GBControle.Caption := ' Controle (informe as posi��es de 1 a '+
        IntToStr(TMaxArr)+'):';
      DesenhaGrid;*)
      EdNome.SetFocus;
    end;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmConfigBB.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmConfigBB.QueryPrincipalAfterOpen;
begin
end;

procedure TFmConfigBB.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmConfigBB.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmConfigBB.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmConfigBB.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmConfigBB.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmConfigBB.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(1, CO_INCLUSAO, 0);
end;

procedure TFmConfigBB.BtAlteraClick(Sender: TObject);
var
  ConfigBB : Integer;
begin
  ConfigBB := QrConfigBBCodigo.Value;
  if not UMyMod.SelLockY(ConfigBB, Dmod.MyDB, 'ConfigBB', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(ConfigBB, Dmod.MyDB, 'ConfigBB', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmConfigBB.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrConfigBBCodigo.Value;
  Close;
end;

procedure TFmConfigBB.BtConfirmaClick(Sender: TObject);
var
  Codigo, i, j, Protestar, Modalidade, Especie, Protestodd, BaixaDevoldd: Integer;
  Nome, SiglaEspecie, Carteira240, Cadastramento, TradiEscrit, Distribuicao,
  Aceite240, Protesto, BaixaDevol, EmisBloqueto, Especie240, Juros240Cod,
  ReservBanco, ReservEmprs, LH_208_33, SQ_233_008, SR_208_033, TL_124_117: String;
  Juros240Qtd: Double;
begin
  PageControl2.ActivePageIndex := PageControl1.ActivePageIndex;
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO configbb SET ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'ConfigBB', 'ConfigBB', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE configbb SET ');
    Codigo := QrConfigBBCodigo.Value;
  end;
  SiglaEspecie := RGSiglaEspecie.Items[RGSiglaEspecie.ItemIndex];
  i := pos('(', SiglaEspecie);
  j := pos(')', SiglaEspecie);
  if (i > 0) and (j > 0) then
    SiglaEspecie := Copy(SiglaEspecie, i+1, j-i-1)
  else
    SiglaEspecie := EdOutraSigla.Text;

  Protestar     := Geral.IMV(Copy(RGProtestar.Items[RGProtestar.ItemIndex], 1, 2));
  Modalidade    := Geral.IMV(Copy(RGModalidade.Items[RGModalidade.ItemIndex], 1, 2));
  Especie       := Geral.IMV(Copy(RGEspecie.Items[RGEspecie.ItemIndex], 1, 2));
  Carteira240   := Copy(RGCarteira240.Items[RGCarteira240.ItemIndex], 1, 1);
  Cadastramento := Copy(RGCadastramento.Items[RGCadastramento.ItemIndex], 1, 1);
  TradiEscrit   := Copy(RGTradiEscrit.Items[RGTradiEscrit.ItemIndex], 1, 1);
  Distribuicao  := Copy(RGDistribuicao.Items[RGDistribuicao.ItemIndex], 1, 1);
  Aceite240     := Copy(RGAceite240.Items[RGAceite240.ItemIndex], 1, 1);
  Protesto      := Copy(RGProtesto.Items[RGProtesto.ItemIndex], 1, 1);
  BaixaDevol    := Copy(RGBaixaDevol.Items[RGBaixaDevol.ItemIndex], 1, 1);
  EmisBloqueto  := Copy(RGEmisBloqueto.Items[RGEmisBloqueto.ItemIndex], 1, 1);
  Especie240    := Copy(RGEspecie240.Items[RGEspecie240.ItemIndex], 1, 2);
  Juros240Cod   := Copy(RGJuros240Cod.Items[RGJuros240Cod.ItemIndex], 1, 1);
  Protestodd    := Geral.IMV(EdProtestodd.Text);
  BaixaDevoldd  := Geral.IMV(EdBaixaDevoldd.Text);
  Juros240Qtd   := Geral.DMV(EdJuros240Qtd.Text);
  ReservBanco   := EdReservBanco.Text;
  ReservEmprs   := EdReservEmprs.Text;
  LH_208_33     := EdLH_208_33.Text;
  SQ_233_008    := EdSQ_233_008.Text;
  SR_208_033    := EdSR_208_033.Text;
  TL_124_117    := EdTL_124_117.Text;
  //
  Dmod.QrUpdU.SQL.Add('Nome=:P00, Convenio=:P01, Carteira=:P02, ');
  Dmod.QrUpdU.SQL.Add('Variacao=:P03, SiglaEspecie=:P04, ');
  Dmod.QrUpdU.SQL.Add('Moeda=:P05, Aceite=:P06, PgAntes=:P07, ');
  Dmod.QrUpdU.SQL.Add('Protestar=:P08, MsgLinha1=:P09, MultaCodi=:P10, ');
  Dmod.QrUpdU.SQL.Add('MultaDias=:P11, MultaValr=:P12, MultaPerc=:P13, ');
  Dmod.QrUpdU.SQL.Add('MultaTiVe=:P14, ImpreLoc=:P15, Modalidade=:P16, ');
  Dmod.QrUpdU.SQL.Add('clcAgencNr=:P17, clcAgencDV=:P18, clcContaNr=:P19, ');
  Dmod.QrUpdU.SQL.Add('clcContaDV=:P20, cedAgencNr=:P21, cedAgencDV=:P22, ');
  Dmod.QrUpdU.SQL.Add('cedContaNr=:P23, cedContaDV=:P24, Especie=:P25, ');
  Dmod.QrUpdU.SQL.Add('Corrido=:P26, IDEmpresa=:P27, Produto=:P28, ');
  Dmod.QrUpdU.SQL.Add('Banco=:P29, InfoCovH=:P30, Carteira240=:P31, ');
  Dmod.QrUpdU.SQL.Add('Cadastramento=:P32, TradiEscrit=:P33, ');
  Dmod.QrUpdU.SQL.Add('Distribuicao=:P34, Aceite240=:P35, Protesto=:P36, ');
  Dmod.QrUpdU.SQL.Add('Protestodd=:P37, BaixaDevol=:P38, BaixaDevoldd=:P39, ');
  Dmod.QrUpdU.SQL.Add('EmisBloqueto=:P40, Especie240=:P41, Juros240Cod=:P42, ');
  Dmod.QrUpdU.SQL.Add('Juros240Qtd=:P43, ContrOperCred=:P44, ReservBanco=:P45, ');
  Dmod.QrUpdU.SQL.Add('ReservEmprs=:P46, LH_208_33=:P47, SQ_233_008=:P48, ');
  Dmod.QrUpdU.SQL.Add('SR_208_033=:P49, TL_124_117=:P50, ');
  Dmod.QrUpdU.SQL.Add('LiqAntMen=:P51, LiqAntVal=:P52, LiqAntMai=:P53, ');
  Dmod.QrUpdU.SQL.Add('LiqVenMen=:P54, LiqVenVal=:P55, LiqVenMai=:P56, ');
  Dmod.QrUpdU.SQL.Add('LiqVcdMen=:P57, LiqVcdVal=:P58, LiqVcdMai=:P59, ');
  Dmod.QrUpdU.SQL.Add('Diretorio=:P60, ');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsString  := Nome;
  Dmod.QrUpdU.Params[01].AsInteger := Geral.IMV(EdConvenio.Text);
  Dmod.QrUpdU.Params[02].AsString  := EdCarteira.Text;
  Dmod.QrUpdU.Params[03].AsString  := EdVariacao.Text;
  Dmod.QrUpdU.Params[04].AsString  := SiglaEspecie;
  Dmod.QrUpdU.Params[05].AsString  := Copy(RGMoeda.Items[RGMoeda.ItemIndex], 1, 5);
  Dmod.QrUpdU.Params[06].AsInteger := RGAceite.ItemIndex;
  Dmod.QrUpdU.Params[07].AsInteger := MLAGeral.BoolToInt(CkPgAntes.Checked);
  Dmod.QrUpdU.Params[08].AsInteger := Protestar;
  Dmod.QrUpdU.Params[09].AsString  := EdMsgLinha1.Text;
  Dmod.QrUpdU.Params[10].AsInteger := RGMultaCodi.ItemIndex;
  Dmod.QrUpdU.Params[11].AsInteger := Geral.IMV(EdMultaDias.Text);
  Dmod.QrUpdU.Params[12].AsFloat   := Geral.DMV(EdMultaValr.Text);
  Dmod.QrUpdU.Params[13].AsFloat   := Geral.DMV(EdMultaPerc.Text);
  Dmod.QrUpdU.Params[14].AsInteger := RGMultaTiVe.ItemIndex;
  Dmod.QrUpdU.Params[15].AsInteger := RGImpreLoc.ItemIndex;
  Dmod.QrUpdU.Params[16].AsInteger := Modalidade;
  Dmod.QrUpdU.Params[17].AsString  := EdclcAgencNr.Text;
  Dmod.QrUpdU.Params[18].AsString  := EdclcAgencDV.Text;
  Dmod.QrUpdU.Params[19].AsString  := EdclcContaNr.Text;
  Dmod.QrUpdU.Params[20].AsString  := EdclcContaDV.Text;
  Dmod.QrUpdU.Params[21].AsString  := EdcedAgencNr.Text;
  Dmod.QrUpdU.Params[22].AsString  := EdcedAgencDV.Text;
  Dmod.QrUpdU.Params[23].AsString  := EdcedContaNr.Text;
  Dmod.QrUpdU.Params[24].AsString  := EdcedContaDV.Text;
  Dmod.QrUpdU.Params[25].AsInteger := Especie;
  Dmod.QrUpdU.Params[26].AsInteger := Geral.IMV(EdCorrido.Text);
  Dmod.QrUpdU.Params[27].AsString  := '';//EdIDEmpresa.Text;
  Dmod.QrUpdU.Params[28].AsString  := EdProduto.Text;
  Dmod.QrUpdU.Params[29].AsInteger := Geral.IMV(EdBanco.Text);
  Dmod.QrUpdU.Params[30].AsInteger := MLAGeral.BoolToInt(CkInfoCovH.Checked);
  Dmod.QrUpdU.Params[31].AsString  := Carteira240;
  Dmod.QrUpdU.Params[32].AsString  := Cadastramento;
  Dmod.QrUpdU.Params[33].AsString  := TradiEscrit;
  Dmod.QrUpdU.Params[34].AsString  := Distribuicao;
  Dmod.QrUpdU.Params[35].AsString  := Aceite240;
  Dmod.QrUpdU.Params[36].AsString  := Protesto;
  Dmod.QrUpdU.Params[37].AsInteger := Protestodd;
  Dmod.QrUpdU.Params[38].AsString  := BaixaDevol;
  Dmod.QrUpdU.Params[39].AsInteger := BaixaDevoldd;
  Dmod.QrUpdU.Params[40].AsString  := EmisBloqueto;
  Dmod.QrUpdU.Params[41].AsString  := Especie240;
  Dmod.QrUpdU.Params[42].AsString  := Juros240Cod;
  Dmod.QrUpdU.Params[43].AsFloat   := Juros240Qtd;
  Dmod.QrUpdU.Params[44].AsInteger := Geral.IMV(EdContrOperCred.Text);
  Dmod.QrUpdU.Params[45].AsString  := ReservBanco;
  Dmod.QrUpdU.Params[46].AsString  := ReservEmprs;
  Dmod.QrUpdU.Params[47].AsString  := LH_208_33;
  Dmod.QrUpdU.Params[48].AsString  := SQ_233_008;
  Dmod.QrUpdU.Params[49].AsString  := SR_208_033;
  Dmod.QrUpdU.Params[50].AsString  := TL_124_117;
    //
  Dmod.QrUpdU.Params[51].AsInteger := Geral.IMV(EdLiqAntMen.Text);
  Dmod.QrUpdU.Params[52].AsInteger := Geral.IMV(EdLiqAntVal.Text);
  Dmod.QrUpdU.Params[53].AsInteger := Geral.IMV(EdLiqAntMai.Text);
  Dmod.QrUpdU.Params[54].AsInteger := Geral.IMV(EdLiqVenMen.Text);
  Dmod.QrUpdU.Params[55].AsInteger := Geral.IMV(EdLiqVenVal.Text);
  Dmod.QrUpdU.Params[56].AsInteger := Geral.IMV(EdLiqVenMai.Text);
  Dmod.QrUpdU.Params[57].AsInteger := Geral.IMV(EdLiqVcdMen.Text);
  Dmod.QrUpdU.Params[58].AsInteger := Geral.IMV(EdLiqVcdVal.Text);
  Dmod.QrUpdU.Params[59].AsInteger := Geral.IMV(EdLiqVcdMai.Text);
  Dmod.QrUpdU.Params[60].AsString  := EdDiretorio.Text;
    //
  Dmod.QrUpdU.Params[61].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[62].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[63].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ConfigBB', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
  if FSeq = 1 then Close;
end;

procedure TFmConfigBB.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  PageControl2.ActivePageIndex := PageControl1.ActivePageIndex;
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'ConfigBB', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ConfigBB', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'ConfigBB', 'Codigo');
end;

procedure TFmConfigBB.FormCreate(Sender: TObject);
begin
  QrOcorDupl.Open;
  QrOcorBank.Open;
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  FSeq := 0;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PainelData.Align  := alClient;
  CriaOForm;
end;

procedure TFmConfigBB.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrConfigBBCodigo.Value,LaRegistro.Caption);
end;

procedure TFmConfigBB.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmConfigBB.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrConfigBBCodigo.Value;
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmConfigBB.QrConfigBBAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmConfigBB.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if FSeq = 1 then MostraEdicao(1, CO_INCLUSAO, 0);
end;

procedure TFmConfigBB.SbQueryClick(Sender: TObject);
begin
  LocCod(QrConfigBBCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'ConfigBB', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmConfigBB.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmConfigBB.QrConfigBBBeforeOpen(DataSet: TDataSet);
begin
  QrConfigBBCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmConfigBB.EdConvenioExit(Sender: TObject);
begin
  if Geral.IMV(EdConvenio.Text) <= 1000000 then
    Application.MessageBox('N�mero do conv�nio deve ser acima de um milh�o',
    'Erro', MB_OK+MB_ICONERROR);
end;

procedure TFmConfigBB.EdDiretorioKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
    MyObjects.DefineDiretorio(Self, EdDiretorio);
end;

procedure TFmConfigBB.EdclcAgencNrExit(Sender: TObject);
begin
  EdclcAgencNr.Text := Geral.SoNumero_TT(EdclcAgencNr.Text);
end;

procedure TFmConfigBB.EdclcAgencDVExit(Sender: TObject);
begin
  //EdclcAgencDV.Text := Geral.SoNumero_TT(EdclcAgencDV.Text);
end;

procedure TFmConfigBB.EdclcContaNrExit(Sender: TObject);
begin
  EdclcContaNr.Text := Geral.SoNumero_TT(EdclcContaNr.Text);
end;

procedure TFmConfigBB.EdclcContaDVExit(Sender: TObject);
begin
  //EdclcContaDV.Text := Geral.SoNumero_TT(EdclcContaDV.Text);
end;

procedure TFmConfigBB.EdcedAgencNrExit(Sender: TObject);
begin
  EdcedAgencNr.Text := Geral.SoNumero_TT(EdcedAgencNr.Text);
end;

procedure TFmConfigBB.EdcedAgencDVExit(Sender: TObject);
begin
  //EdcedAgencDV.Text := Geral.SoNumero_TT(EdcedAgencDV.Text);
end;

procedure TFmConfigBB.EdcedContaNrExit(Sender: TObject);
begin
  EdcedContaNr.Text := Geral.SoNumero_TT(EdcedContaNr.Text);
end;

procedure TFmConfigBB.EdcedContaDVExit(Sender: TObject);
begin
  //EdcedContaDV.Text := Geral.SoNumero_TT(EdcedContaDV.Text);
end;

procedure TFmConfigBB.EdCorridoExit(Sender: TObject);
var
  dd: Integer;
begin
  dd := Geral.IMV(EdCorrido.Text);
  if not (dd in [6..29, 35, 40]) then
  begin
    EdCorrido.Text := '6';
    Application.MessageBox('Defina um dia de 6 a 29 ou 35 ou 40.', 'Erro',
    MB_OK+MB_ICONERROR);
    EdCorrido.Text;
    Exit;
  end;
end;

procedure TFmConfigBB.EdBancoChange(Sender: TObject);
begin
  EdBanco.Text := MLAGeral.TFD2(EdBanco.Text, 3, False);
end;

procedure TFmConfigBB.RGJuros240CodClick(Sender: TObject);
begin
  case RGJuros240Cod.ItemIndex of
    0: Label58.Caption := '$/dia Mora:';
    1: Label58.Caption := '% / M�s Juros:';
    else Label58.Caption := '$/dia Mora ou % / M�s Juros:';
  end;
end;

procedure TFmConfigBB.EdContrOperCredExit(Sender: TObject);
begin
  EdContrOperCred.Text := Geral.TFD(EdContrOperCred.Text, 10, siPositivo);
end;

end.

