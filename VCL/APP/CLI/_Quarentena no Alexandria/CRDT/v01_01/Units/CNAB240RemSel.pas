unit CNAB240RemSel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, Grids,
  DBGrids, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral,
  UnDmkProcFunc;

type
  TFmCNAB240RemSel = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    Label7: TLabel;
    EdConfigBB: TdmkEditCB;
    CBConfigBB: TdmkDBLookupComboBox;
    QrOcorBank: TmySQLQuery;
    DsOcorBank: TDataSource;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankMovimento: TIntegerField;
    QrOcorBankNome: TWideStringField;
    QrPesq: TmySQLQuery;
    DBGrid1: TDBGrid;
    DsPesq: TDataSource;
    QrPesqConfigBB: TIntegerField;
    QrPesqNOMECLI: TWideStringField;
    QrPesqLOTE_COD: TIntegerField;
    QrPesqMY_ID: TIntegerField;
    QrPesqBanco: TIntegerField;
    QrPesqAgencia: TIntegerField;
    QrPesqCPF: TWideStringField;
    QrPesqEmitente: TWideStringField;
    QrPesqBruto: TFloatField;
    QrPesqDCompra: TDateField;
    QrPesqDDeposito: TDateField;
    QrPesqVencto: TDateField;
    QrPesqDuplicata: TWideStringField;
    QrPesqData3: TDateField;
    QrPesqCobranca: TIntegerField;
    QrPesqCartDep: TIntegerField;
    QrPesqCliente: TIntegerField;
    QrPesqCPF_TXT: TWideStringField;
    Panel4: TPanel;
    Label75: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdDuplicata: TdmkEdit;
    Label4: TLabel;
    EdCPF: TdmkEdit;
    EdEmitente: TdmkEdit;
    Label2: TLabel;
    EdControle: TdmkEdit;
    Label55: TLabel;
    RGMascara: TRadioGroup;
    CkEIni: TCheckBox;
    TPEIni: TDateTimePicker;
    CkEFim: TCheckBox;
    TPEFim: TDateTimePicker;
    CkVIni: TCheckBox;
    TPVIni: TDateTimePicker;
    CkVFim: TCheckBox;
    TPVFim: TDateTimePicker;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLIENTE: TWideStringField;
    DsClientes: TDataSource;
    BtReabre: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrPesqCalcFields(DataSet: TDataSet);
    procedure EdCPFExit(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
  private
    { Private declarations }
    FEmitente, FCPF, FDuplicata: String;
    FCliente, FControle, FPesq: Integer;
    function TextoSQLPesq(): Integer;
  public
    FConfigBB: Integer;
    procedure ReabrirTabelas;
    { Public declarations }
  end;

  var
  FmCNAB240RemSel: TFmCNAB240RemSel;

implementation

uses Module, UnMyObjects;

{$R *.DFM}

procedure TFmCNAB240RemSel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCNAB240RemSel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCNAB240RemSel.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCNAB240RemSel.FormCreate(Sender: TObject);
begin
  QrOcorBank.Open;
  QrClientes.Open;
  TPEIni.Date := Date - 180;
  TPEFim.Date := Date;
  TPVFim.Date := Date + 180;
  TPVIni.Date := Date;
end;

procedure TFmCNAB240RemSel.QrPesqCalcFields(DataSet: TDataSet);
begin
  QrPesqCPF_TXT.Value := Geral.FormataCNPJ_TT(QrPesqCPF.Value);
end;

function TFmCNAB240RemSel.TextoSQLPesq(): Integer;
begin
  Result := 0;
  //
  QrPesq.SQL.Add('SELECT cbb.ConfigBB, CASE WHEN cli.Tipo=0 THEN cli.RazaoSocial');
  QrPesq.SQL.Add('ELSE cli.Nome END NOMECLI, loi.Codigo LOTE_COD, loi.Controle MY_ID,');
  QrPesq.SQL.Add('loi.Banco, loi.Agencia, loi.CPF, loi.Emitente, loi.Bruto, loi.DCompra,');
  QrPesq.SQL.Add('loi.DDeposito, loi.Vencto, loi.Duplicata, loi.Data3, loi.Cobranca,');
  QrPesq.SQL.Add('loi.CartDep, loi.Cliente');
  QrPesq.SQL.Add('FROM lotesits loi');
  QrPesq.SQL.Add('LEFT JOIN cobrancabb cbb ON cbb.Codigo=loi.Cobranca');
  QrPesq.SQL.Add('LEFT JOIN entidades cli ON cli.Codigo=loi.Cliente');
  QrPesq.SQL.Add('WHERE loi.Quitado < 2');
  QrPesq.SQL.Add('AND loi.Cobranca > 0');
  QrPesq.SQL.Add('AND cbb.ConfigBB = '+IntToStr(FConfigBB));
  //
  //////////////////////////////////////////////////////////////////////////////
  if FCliente <> 0 then
    QrPesq.SQL.Add('AND loi.Cliente='+IntToStr(FCliente));
  if FEmitente <> '' then
    QrPesq.SQL.Add('AND loi.Emitente LIKE "'+FEmitente+'"');
  if FCPF <> '' then
    QrPesq.SQL.Add('AND loi.CPF = "'+FCPF+'"');
  if FDuplicata <> '' then
    QrPesq.SQL.Add('AND loi.Duplicata = "'+FDuplicata+'"');
  if FControle <> 0 then
    QrPesq.SQL.Add('AND loi.Controle = '+IntToStr(FControle));
  //////////////////////////////////////////////////////////////////////////////
  QrPesq.SQL.Add(dmkPF.SQL_Periodo('AND loi.Emissao ',
    TPEIni.Date, TPEFim.Date, CkEIni.Checked, CkEFim.Checked));
  QrPesq.SQL.Add(dmkPF.SQL_Periodo('AND loi.DDeposito ',
    TPVIni.Date, TPVFim.Date, CkVIni.Checked, CkVFim.Checked));
  //////////////////////////////////////////////////////////////////////////////
  QrPesq.SQL.Add('ORDER BY DDeposito');
end;

procedure TFmCNAB240RemSel.ReabrirTabelas;
const
  ItensOrdem: array[0..6] of String = ('NOMECLIENTE', 'li.DDeposito', 'PERIODO',
  'Emitente', 'CNPJ_TXT', 'li.Data3', 'PERIODO3');
begin
  Screen.Cursor := crHourGlass;
  try
  Application.ProcessMessages;
  if EdEmitente.ValueVariant <> 0 then
  begin
    FEmitente := EdEmitente.Text;
    if RGMascara.ItemIndex in ([0,1]) then FEmitente := '%'+FEmitente;
    if RGMascara.ItemIndex in ([0,2]) then FEmitente := FEmitente+'%';
  end else FEmitente := '';
  if Trim(EdCPF.Text) <> '' then FCPF := Geral.SoNumero_TT(EdCPF.Text)
  else FCPF := '';
  if Trim(EdDuplicata.Text) <> '' then FDuplicata := EdDuplicata.Text
  else FDuplicata := '';
  FControle := Geral.IMV(EdControle.Text);
  FCliente := Geral.IMV(EdCliente.Text);
  //////////////////////////////////////////////////////////////////////////////
  QrPesq.Close;
  QrPesq.SQL.Clear;

  TextoSQLPesq();

  (*QrPesq.SQL.Add(MLAGeral.OrdemSQL3(FOrdC1, FOrdS1, FOrdC2, FOrdS2, FOrdC3, FOrdS3));
    1: QrPesq.SQL.Add('ORDER BY '+
    ItensOrdem[RGOrdem1.ItemIndex]+', '+
    ItensOrdem[RGOrdem2.ItemIndex]+', '+
    ItensOrdem[RGOrdem3.ItemIndex]+', '+
    ItensOrdem[RGOrdem4.ItemIndex]);
    else QrPesq.SQL.Add('ORDER BY ?');
  end;*)
  //
  QrPesq.Open;
  //////////////////////////////////////////////////////////////////////////////
  if FPesq > 0 then QrPesq.Locate('Controle', FPesq, []);
  //
  Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmCNAB240RemSel.EdCPFExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCPF.Text);
  if CPF <> '' then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if MLAGeral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Application.MessageBox(PChar('N�mero inv�lido para CPF/CNPJ'), 'Erro',
        MB_OK+MB_ICONERROR);
      EdCPF.SetFocus;
    end else EdCPF.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCPF.Text := '';
end;

procedure TFmCNAB240RemSel.BtReabreClick(Sender: TObject);
begin
  ReabrirTabelas;
end;

end.

