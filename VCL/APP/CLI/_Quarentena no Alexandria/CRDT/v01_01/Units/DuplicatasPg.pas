unit DuplicatasPg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ComCtrls, DBCtrls, Db, mySQLDbTables,
  Duplicatas2, dmkGeral, dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnDmkEnums;

type
  TFmDuplicatasPg = class(TForm)
    PainelStatus: TPanel;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    BtDesiste: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Label15: TLabel;
    EdAlinea: TdmkEditCB;
    CBAlinea: TdmkDBLookupComboBox;
    Label17: TLabel;
    TPData: TDateTimePicker;
    QrOcorDupl: TmySQLQuery;
    DsOcorDupl: TDataSource;
    QrOcorDuplCodigo: TIntegerField;
    QrOcorDuplNome: TWideStringField;
    QrOcorDuplBase: TFloatField;
    Panel1: TPanel;
    Label2: TLabel;
    TPVenc: TDateTimePicker;
    EdCliCod: TdmkEdit;
    Label3: TLabel;
    EdCliNome: TdmkEdit;
    EdDuplicata: TdmkEdit;
    EdCPF: TdmkEdit;
    Label4: TLabel;
    Label5: TLabel;
    EdEmitente: TdmkEdit;
    Label6: TLabel;
    EdValor: TdmkEdit;
    Label7: TLabel;
    PainelPagto: TPanel;
    Panel5: TPanel;
    Label23: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label29: TLabel;
    TPDataBase2: TDateTimePicker;
    EdValorBase2: TdmkEdit;
    EdJurosBase2: TdmkEdit;
    EdJurosPeriodo2: TdmkEdit;
    Panel4: TPanel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label30: TLabel;
    TPPagto2: TDateTimePicker;
    EdJuros2: TdmkEdit;
    EdPago2: TdmkEdit;
    EdAPagar: TdmkEdit;
    Label1: TLabel;
    EdDesco2: TdmkEdit;
    QrOcorDuplDatapbase: TIntegerField;
    QrOcorDuplOcorrbase: TIntegerField;
    QrOcorDuplStatusbase: TIntegerField;
    procedure BtOKClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdAlineaChange(Sender: TObject);
    procedure EdValorBase2Change(Sender: TObject);
    procedure EdJurosBase2Change(Sender: TObject);
    procedure TPPagto2Change(Sender: TObject);
    procedure EdJuros2Change(Sender: TObject);
    procedure EdDesco2Change(Sender: TObject);
  private
    { Private declarations }
    FPainelStatusHeight: Integer;
  public
    { Public declarations }
    FDuplicataOrigem, FLotePagto: Integer;
    procedure CalculaAPagar;
    procedure CalculaJuros;
  end;

  var
  FmDuplicatasPg: TFmDuplicatasPg;

implementation

{$R *.DFM}

uses Module, UnMLAGeral, UnInternalConsts, UMySQLModule, UnMyObjects;

procedure TFmDuplicatasPg.BtOKClick(Sender: TObject);
var
  ADupIts, ADupPgs, Controle: Integer;
  Data: String;
begin
  if PainelStatus.Visible then
  begin
    ADupIts := Geral.IMV(EdAlinea.Text);
    if ADupIts = 0 then
    begin
      Application.MessageBox('Defina o status da duplicata!', 'Erro',
      MB_OK+MB_ICONERROR);
      Exit;
    end;
  end else ADupIts := 0;
  if ADupIts > 0 then
  begin
    Data  := FormatDateTime(VAR_FORMATDATE, TPData.Date);
    //
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'ADupIts',
    'ADupIts', 'Controle');
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO ADupIts SET Alinea=:P0, DataA=:P1, ');
    Dmod.QrUpd.SQL.Add('Controle=:Pa, LotesIts=:Pb');
    //
    Dmod.QrUpd.Params[00].AsInteger := ADupIts;
    Dmod.QrUpd.Params[01].AsString  := Data;
    Dmod.QrUpd.Params[02].AsInteger := Controle;
    Dmod.QrUpd.Params[03].AsInteger := FmDuplicatas2.QrPesqControle.Value;
    Dmod.QrUpd.ExecSQL;
    FmDuplicatas2.FAdupIts := Controle;
    FmDuplicatas2.ForcaOcorBank := QrOcorDuplOcorrbase.Value;
    FmDuplicatas2.ForcaOBData   := TPData.Date;
    //
    if QrOcorDuplStatusbase.Value < 0 then
    begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE LotesIts SET Quitado=:P0 WHERE Controle=:P1');
      //
      Dmod.QrUpd.Params[00].AsInteger := QrOcorDuplStatusbase.Value;
      Dmod.QrUpd.Params[01].AsInteger := FmDuplicatas2.QrPesqControle.Value;
      Dmod.QrUpd.ExecSQL;
    end;
  end;
  if PainelPagto.Visible then
  begin
    ADupPgs := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
          'ADupPgs', 'ADupPgs', 'Controle');
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO ADupPgs SET AlterWeb=1, ');
    Dmod.QrUpd.SQL.Add('Data=:P0, Juros=:P1, Desco=:P2, Pago=:P3, LotePg=:P4');
    Dmod.QrUpd.SQL.Add(', LotesIts=:Pa, Controle=:Pb');
    Dmod.QrUpd.Params[00].AsString  := FormatDateTime(VAR_FORMATDATE, TPPagto2.Date);
    Dmod.QrUpd.Params[01].AsFloat   := Geral.DMV(EdJuros2.Text);
    Dmod.QrUpd.Params[02].AsFloat   := Geral.DMV(EdDesco2.Text);
    Dmod.QrUpd.Params[03].AsFloat   := Geral.DMV(EdPago2.Text);
    Dmod.QrUpd.Params[04].AsInteger := FmDuplicatas2.FLotePgOrigem;
    //
    Dmod.QrUpd.Params[05].AsInteger := FmDuplicatas2.QrPesqControle.Value;
    Dmod.QrUpd.Params[06].AsInteger := ADupPgs;
    Dmod.QrUpd.ExecSQL;
    //
    FmDuplicatas2.FAdupPgs := ADupPgs;
    FmDuplicatas2.CalculaPagamento(FmDuplicatas2.QrPesqControle.Value);
  end;
  FmDuplicatas2.FLotesIts := FmDuplicatas2.QrPesqControle.Value;
  Close;
end;

procedure TFmDuplicatasPg.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmDuplicatasPg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if PainelPagto.Visible = False then PainelStatus.Align := alClient else
  begin
    PainelStatus.Align := alTop;
    PainelStatus.Height := FPainelStatusHeight;
  end;
  if PainelPagto.Visible then
  begin
    CalculaJuros;
    CalculaAPagar;
  end;
  if PainelStatus.Visible then EdAlinea.SetFocus
  else if Panel5.Visible then EdJurosBase2.SetFocus;
end;

procedure TFmDuplicatasPg.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmDuplicatasPg.FormCreate(Sender: TObject);
begin
  FPainelStatusHeight := PainelStatus.Height;
  FLotePagto := 0;
  QrOcorDupl.Open;
  TPData.Date := Date;
  TPPagto2.Date := Date;
end;

procedure TFmDuplicatasPg.EdAlineaChange(Sender: TObject);
begin
  case QrOcorDuplDatapbase.Value of
    0:
    begin
      if Date > TPData.MinDate   then TPData.Date   := Date;
      if Date > TPPagto2.MinDate then TPPagto2.Date := Date;
    end;
    1:
    begin
      TPData.Date   := TPVenc.Date;
      TPPagto2.Date := TPVenc.Date;
    end;
  end;
  CalculaJuros;
end;

procedure TFmDuplicatasPg.EdValorBase2Change(Sender: TObject);
begin
  CalculaAPagar;
end;

procedure TFmDuplicatasPg.CalculaAPagar;
var
  Base, Juro, Desc: Double;
begin
  Base := Geral.DMV(EdValorBase2.Text);
  Juro := Geral.DMV(EdJuros2.Text);
  Desc := Geral.DMV(EdDesco2.Text);
  //
  EdAPagar.Text := Geral.FFT(Base+Juro-Desc, 2, siPositivo);
  EdPago2.Text  := EdAPagar.Text;
end;


procedure TFmDuplicatasPg.EdJurosBase2Change(Sender: TObject);
begin
  CalculaJuros;
end;

procedure TFmDuplicatasPg.TPPagto2Change(Sender: TObject);
begin
  CalculaJuros;
end;

procedure TFmDuplicatasPg.EdJuros2Change(Sender: TObject);
begin
  CalculaAPagar;
end;

procedure TFmDuplicatasPg.EdDesco2Change(Sender: TObject);
begin
  CalculaAPagar;
end;

procedure TFmDuplicatasPg.CalculaJuros;
var
  Prazo: Integer;
  Taxa, Juros, Valor: Double;
begin
  Juros := 0;
  Prazo := Trunc(Int(TPPagto2.Date) - Int(TPDataBase2.Date));
  if Prazo > 0 then
  begin
    Taxa  := Geral.DMV(EdJurosBase2.Text);
    Juros := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(Taxa, Prazo);
  end;
  Valor := Geral.DMV(EdValorBase2.Text);
  EdJurosPeriodo2.Text := Geral.FFT(Juros, 6, siPositivo);
  EdJuros2.Text := Geral.FFT(Juros * Valor / 100, 2, siPositivo);
end;

end.
