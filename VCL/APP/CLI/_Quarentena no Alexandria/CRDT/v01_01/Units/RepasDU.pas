unit RepasDU;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  Grids, DBGrids, Menus, frxClass, frxDBSet, Variants, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, UnDmkProcFunc, UnDmkEnums;

type
  TFmRepasDU = class(TForm)
    PainelDados: TPanel;
    DsRepas: TDataSource;
    QrRepas: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    BtImpCalculado: TBitBtn;
    BtImpPesquisa: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TdmkEdit;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Label75: TLabel;
    EdColigado: TdmkEditCB;
    CBColigado: TdmkDBLookupComboBox;
    QrColigado: TmySQLQuery;
    DsColigado: TDataSource;
    TPData: TDateTimePicker;
    Label3: TLabel;
    QrColigadoCodigo: TIntegerField;
    QrColigadoNOMECOLIGADO: TWideStringField;
    QrRepasCodigo: TIntegerField;
    QrRepasData: TDateField;
    QrRepasTotal: TFloatField;
    QrRepasJurosV: TFloatField;
    QrRepasColigado: TIntegerField;
    QrRepasNOMECOLIGADO: TWideStringField;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    DBEdit2: TDBEdit;
    Label6: TLabel;
    DBEdit3: TDBEdit;
    QrRepasSALDO: TFloatField;
    Label7: TLabel;
    DBEdit4: TDBEdit;
    QrRepasIts: TmySQLQuery;
    DsRepasIts: TDataSource;
    QrBanco: TmySQLQuery;
    QrBancoCodigo: TIntegerField;
    QrBancoNome: TWideStringField;
    QrBancoLk: TIntegerField;
    QrBancoDataCad: TDateField;
    QrBancoDataAlt: TDateField;
    QrBancoUserCad: TIntegerField;
    QrBancoUserAlt: TIntegerField;
    QrBancoSite: TWideStringField;
    DsBanco: TDataSource;
    QrCheques: TmySQLQuery;
    DsCheques: TDataSource;
    PMLote: TPopupMenu;
    Incluinovolote1: TMenuItem;
    Alteraloteatual1: TMenuItem;
    Excluiloteatual1: TMenuItem;
    PMCheque: TPopupMenu;
    Adicionachequeaoloteatual1: TMenuItem;
    Alterarepassedochequeatual1: TMenuItem;
    Retirachequedoloteatual1: TMenuItem;
    QrChequesCodigo: TIntegerField;
    QrChequesControle: TIntegerField;
    QrChequesComp: TIntegerField;
    QrChequesBanco: TIntegerField;
    QrChequesAgencia: TIntegerField;
    QrChequesConta: TWideStringField;
    QrChequesCheque: TIntegerField;
    QrChequesCPF: TWideStringField;
    QrChequesEmitente: TWideStringField;
    QrChequesValor: TFloatField;
    QrChequesEmissao: TDateField;
    QrChequesDCompra: TDateField;
    QrChequesDDeposito: TDateField;
    QrChequesVencto: TDateField;
    QrChequesTxaCompra: TFloatField;
    QrChequesTxaJuros: TFloatField;
    QrChequesTxaAdValorem: TFloatField;
    QrChequesVlrCompra: TFloatField;
    QrChequesVlrAdValorem: TFloatField;
    QrChequesDMais: TIntegerField;
    QrChequesDias: TIntegerField;
    QrChequesDuplicata: TWideStringField;
    QrChequesLk: TIntegerField;
    QrChequesDataCad: TDateField;
    QrChequesDataAlt: TDateField;
    QrChequesUserCad: TIntegerField;
    QrChequesUserAlt: TIntegerField;
    QrChequesDevolucao: TIntegerField;
    QrChequesDesco: TFloatField;
    QrChequesQuitado: TIntegerField;
    QrChequesBruto: TFloatField;
    QrClientes: TmySQLQuery;
    QrClientesCodigo: TIntegerField;
    QrClientesNOMECLIENTE: TWideStringField;
    DsClientes: TDataSource;
    Panel1: TPanel;
    PainelItens: TPanel;
    Panel4: TPanel;
    Label11: TLabel;
    Label13: TLabel;
    Label8: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdDuplicata: TdmkEdit;
    EdCNPJ: TdmkEdit;
    GradeCHs: TDBGrid;
    Panel3: TPanel;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel6: TPanel;
    BtSaida: TBitBtn;
    BtCheque: TBitBtn;
    BtReceitas: TBitBtn;
    PainelConfI: TPanel;
    BtConfirma2: TBitBtn;
    BtDesiste2: TBitBtn;
    Label14: TLabel;
    EdTaxa: TdmkEdit;
    CkInap: TCheckBox;
    QrSum: TmySQLQuery;
    QrSumValor: TFloatField;
    QrSumJurosV: TFloatField;
    QrRepasFatorCompra: TFloatField;
    QrRepasItsBanco: TIntegerField;
    QrRepasItsAgencia: TIntegerField;
    QrRepasItsEmitente: TWideStringField;
    QrRepasItsCodigo: TIntegerField;
    QrRepasItsControle: TIntegerField;
    QrRepasItsOrigem: TIntegerField;
    QrRepasItsDias: TIntegerField;
    QrRepasItsValor: TFloatField;
    QrRepasItsTaxa: TFloatField;
    QrRepasItsJurosP: TFloatField;
    QrRepasItsJurosV: TFloatField;
    QrRepasItsLk: TIntegerField;
    QrRepasItsDataCad: TDateField;
    QrRepasItsDataAlt: TDateField;
    QrRepasItsUserCad: TIntegerField;
    QrRepasItsUserAlt: TIntegerField;
    QrRepasItsCPF: TWideStringField;
    QrRepasItsCPF_TXT: TWideStringField;
    QrChequesCPF_TXT: TWideStringField;
    QrRepasItsDDeposito: TDateField;
    QrRepasItsVencto: TDateField;
    QrRepasItsLIQUIDO: TFloatField;
    BtExclui2: TBitBtn;
    BtImportar: TBitBtn;
    QrLI: TmySQLQuery;
    QrLICodigo: TIntegerField;
    QrLIControle: TIntegerField;
    QrLIComp: TIntegerField;
    QrLIBanco: TIntegerField;
    QrLIAgencia: TIntegerField;
    QrLIConta: TWideStringField;
    QrLICheque: TIntegerField;
    QrLICPF: TWideStringField;
    QrLIEmitente: TWideStringField;
    QrLIBruto: TFloatField;
    QrLIDesco: TFloatField;
    QrLIValor: TFloatField;
    QrLIEmissao: TDateField;
    QrLIDCompra: TDateField;
    QrLIDDeposito: TDateField;
    QrLIVencto: TDateField;
    QrLITxaCompra: TFloatField;
    QrLITxaJuros: TFloatField;
    QrLITxaAdValorem: TFloatField;
    QrLIVlrCompra: TFloatField;
    QrLIVlrAdValorem: TFloatField;
    QrLIDMais: TIntegerField;
    QrLIDias: TIntegerField;
    QrLIDuplicata: TWideStringField;
    QrLIDevolucao: TIntegerField;
    QrLIQuitado: TIntegerField;
    QrLILk: TIntegerField;
    QrLIDataCad: TDateField;
    QrLIDataAlt: TDateField;
    QrLIUserCad: TIntegerField;
    QrLIUserAlt: TIntegerField;
    QrLIPraca: TIntegerField;
    QrLITipo: TSmallintField;
    QrLIBcoCobra: TIntegerField;
    QrLIAgeCobra: TIntegerField;
    Label15: TLabel;
    EdBordero: TdmkEdit;
    QrChequesLote: TSmallintField;
    GradeRepas: TDBGrid;
    Memo1: TMemo;
    QrLITotalJr: TFloatField;
    QrLITotalDs: TFloatField;
    QrLITotalPg: TFloatField;
    QrLIData3: TDateField;
    QrLIProrrVz: TIntegerField;
    QrLIProrrDd: TIntegerField;
    QrLIRepassado: TSmallintField;
    QrRepasItsDuplicata: TWideStringField;
    Progress: TProgressBar;
    frxRepasIts: TfrxReport;
    frxDsRepasIts: TfrxDBDataset;
    frxDsRepas: TfrxDBDataset;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtReceitasClick(Sender: TObject);
    procedure BtChequeClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrRepasAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrRepasAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrRepasBeforeOpen(DataSet: TDataSet);
    procedure EdDuplicataExit(Sender: TObject);
    procedure EdCNPJExit(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure BtDesiste2Click(Sender: TObject);
    procedure Incluinovolote1Click(Sender: TObject);
    procedure Alteraloteatual1Click(Sender: TObject);
    procedure Adicionachequeaoloteatual1Click(Sender: TObject);
    procedure BtConfirma2Click(Sender: TObject);
    procedure CkInapClick(Sender: TObject);
    procedure Retirachequedoloteatual1Click(Sender: TObject);
    procedure QrRepasItsCalcFields(DataSet: TDataSet);
    procedure QrChequesCalcFields(DataSet: TDataSet);
    procedure BtImpCalculadoClick(Sender: TObject);
    procedure BtImpPesquisaClick(Sender: TObject);
    procedure EdTaxaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrChequesAfterOpen(DataSet: TDataSet);
    procedure QrChequesAfterClose(DataSet: TDataSet);
    procedure QrRepasItsAfterOpen(DataSet: TDataSet);
    procedure BtExclui2Click(Sender: TObject);
    procedure QrRepasItsAfterClose(DataSet: TDataSet);
    procedure BtImportarClick(Sender: TObject);
    procedure EdBorderoExit(Sender: TObject);
    procedure frxRepasItsGetValue(const VarName: String;
      var Value: Variant);
  private
    FRepasIts: Integer;
    FItensBloqueados, FItensImportados: Integer;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
    procedure MostraEdicao(Mostra : Integer; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenRepasIts;
    procedure ReopenDuplicatas(LocCod: Integer);
    procedure CalculaLote(Lote: Integer);
    procedure ImportaItemDeLote(Taxa: Double);
    procedure RepassaCheque;
    procedure RecalculaCheque;
  public
    { Public declarations }
  end;

var
  FmRepasDU: TFmRepasDU;
const
  FFormatFloat = '00000';

implementation

uses Module, Principal, RepasImp, LotesLoc, GetPercent, RepasLoc, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmRepasDU.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmRepasDU.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrRepasCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmRepasDU.DefParams;
begin
  VAR_GOTOTABELA := 'Repas';
  VAR_GOTOMYSQLTABLE := QrRepas;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;
  VAR_GOTOVAR1 := ' Tipo=1';

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT CASE WHEN en.Tipo=0 THEN en.RazaoSocial');
  VAR_SQLx.Add('ELSE en.Nome END NOMECOLIGADO, en.FatorCompra, ');
  VAR_SQLx.Add('re.*, (re.Total-re.JurosV) SALDO');
  VAR_SQLx.Add('FROM repas re');
  VAR_SQLx.Add('LEFT JOIN entidades en ON en.Codigo=re.Coligado');
  VAR_SQLx.Add('WHERE re.Tipo=1');
  //
  VAR_SQL1.Add('AND re.Codigo=:P0');
  //
  VAR_SQLa.Add('AND (CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END) LIKE :P0');
  //
end;

procedure TFmRepasDU.MostraEdicao(Mostra: Integer; Status : String; Codigo : Integer);
begin
  PainelTitulo.Enabled  := False;
  case Mostra of
    0:
    begin
      PainelControle.Visible := True;
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
      PainelItens.Visible    := False;
      PainelConfI.Visible    := False;
      PainelTitulo.Enabled   := True;
      //
      CalculaLote(Codigo);
    end;
    1:
    begin
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      PainelControle.Visible := False;
      PainelItens.Visible    := False;
      PainelConfI.Visible    := False;
      //
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdColigado.Text     := '';
        CBColigado.KeyValue := NULL;
        TPData.Date         := Date;
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdColigado.Text     := IntToStr(QrRepasColigado.Value);
        CBColigado.KeyValue := QrRepasColigado.Value;
        TPData.Date         := QrRepasData.Value;
      end;
      EdColigado.SetFocus;
    end;
    2:
    begin
      PainelItens.Visible    := True;
      PainelConfI.Visible    := True;
      PainelControle.Visible := True;
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
      PainelControle.Visible := False;
      //
      EdDuplicata.SetFocus;
      EdTaxa.Text  := Geral.FFT(QrRepasFatorCompra.Value+
        Dmod.ObtemExEntiMaior(QrRepasColigado.Value), 6, siPositivo);
    end;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmRepasDU.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmRepasDU.AlteraRegistro;
var
  Repas : Integer;
begin
  Repas := QrRepasCodigo.Value;
  if QrRepasCodigo.Value = 0 then
  begin
    Application.MessageBox('N�o dados selecionados para editar', 'Erro',
      MB_OK+MB_ICONERROR);
    Exit;
  end;
  if not UMyMod.SelLockY(Repas, Dmod.MyDB, 'Repas', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(Repas, Dmod.MyDB, 'Repas', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmRepasDU.IncluiRegistro;
var
  Cursor : TCursor;
  Repas : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    Repas := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'Repas', 'Repas', 'Codigo');
    if Length(FormatFloat(FFormatFloat, Repas))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, CO_INCLUSAO, Repas);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmRepasDU.QueryPrincipalAfterOpen;
begin
end;

procedure TFmRepasDU.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmRepasDU.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmRepasDU.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmRepasDU.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmRepasDU.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmRepasDU.BtReceitasClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMLote, BtReceitas);
end;

procedure TFmRepasDU.BtChequeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCheque, BtCheque);
end;

procedure TFmRepasDU.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrRepasCodigo.Value;
  Close;
end;

procedure TFmRepasDU.BtConfirmaClick(Sender: TObject);
var
  Coligado, Codigo : Integer;
begin
  Coligado := Geral.IMV(EdColigado.Text);
  if Coligado = 0 then
  begin
    Application.MessageBox('Defina o coligado!', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('INSERT INTO repas SET Tipo=1, ')
  else Dmod.QrUpdU.SQL.Add('UPDATE repas SET ');
  Dmod.QrUpdU.SQL.Add('Coligado=:P0, Data=:P1, ');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Px, UserCad=:Py, Codigo=:Pz')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Px, UserAlt=:Py WHERE Codigo=:Pz');
  Dmod.QrUpdU.Params[00].AsInteger := Coligado;
  Dmod.QrUpdU.Params[01].AsString  := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  //
  Dmod.QrUpdU.Params[02].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[03].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[04].AsInteger := Codigo;
  Dmod.QrUpdU.ExecSQL;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Repas', 'Codigo');
  FRepasIts := QrRepasItsControle.Value;
  MostraEdicao(0, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
  //
  Progress.Max := QrRepasIts.RecordCount;
  Progress.Position := 0;
  Progress.Visible := True;
  QrRepasIts.First;
  while not QrRepasIts.Eof do
  begin
    Progress.Position := Progress.Position + 1;
    RecalculaCheque;
    QrRepasIts.Next;
  end;
  CalculaLote(Codigo);
  LocCod(Codigo,Codigo);
  Progress.Position := 0;
  Progress.Visible := False;
end;

procedure TFmRepasDU.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'Repas', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Repas', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Repas', 'Codigo');
end;

procedure TFmRepasDU.FormCreate(Sender: TObject);
begin
  PainelControle.Align := alClient;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  Panel1.Align      := alClient;
  GradeRepas.Align  := alClient;
  GradeCHs.Align     := alClient;
  LaRegistro.Align  := alClient;
  CriaOForm;
  //
  QrColigado.Open;
  QrClientes.Open;
end;

procedure TFmRepasDU.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrRepasCodigo.Value,LaRegistro.Caption);
end;

procedure TFmRepasDU.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmRepasDU.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmRepasDU.QrRepasAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmRepasDU.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'Repas', 'Livres', 99) then
  //BtInclui.Enabled := False;
end;

procedure TFmRepasDU.QrRepasAfterScroll(DataSet: TDataSet);
begin
  ReopenRepasIts;
end;

procedure TFmRepasDU.SbQueryClick(Sender: TObject);
begin
  Application.CreateForm(TFmRepasLoc, FmRepasLoc);
  FmRepasLoc.ShowModal;
  FmRepasLoc.Destroy;
  if FmPrincipal.FLoteLoc <> 0 then
    LocCod(FmPrincipal.FLoteLoc, FmPrincipal.FLoteLoc);
end;

procedure TFmRepasDU.FormResize(Sender: TObject);
begin
  PainelItens.Height := Height -530 + 235;
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmRepasDU.QrRepasBeforeOpen(DataSet: TDataSet);
begin
  QrRepasCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmRepasDU.ReopenRepasIts;
begin
  QrRepasIts.Close;
  QrRepasIts.Params[0].AsInteger := QrRepasCodigo.Value;
  QrRepasIts.Open;
  //
  if FRepasIts <> 0 then QrRepasIts.Locate('Controle', FRepasIts, []);
end;

procedure TFmRepasDU.EdDuplicataExit(Sender: TObject);
begin
  ReopenDuplicatas(0);
end;

procedure TFmRepasDU.EdCNPJExit(Sender: TObject);
var
  Num : String;
  CNPJ : String;
begin
  CNPJ := Geral.SoNumero_TT(EdCNPJ.Text);
  if CNPJ <> '' then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CNPJ);
    if MLAGeral.FormataCNPJ_TFT(CNPJ) <> Num then
    begin
      Application.MessageBox(PChar('N�mero inv�lido!'), 'Erro', MB_OK+MB_ICONERROR);
      EdCNPJ.SetFocus;
    end else EdCNPJ.Text := Geral.FormataCNPJ_TT(CNPJ);
  end else EdCNPJ.Text := '';
  ReopenDuplicatas(0);
end;

procedure TFmRepasDU.EdClienteChange(Sender: TObject);
begin
  if Geral.IMV(EdCliente.Text) <> 0 then
    EdBordero.Enabled := True
  else
    EdBordero.Enabled := False;
  ReopenDuplicatas(QrChequesControle.Value);
end;

procedure TFmRepasDU.ReopenDuplicatas(LocCod: Integer);
var
  Cliente, Bordero: Integer;
begin
  QrCheques.Close;
  QrCheques.SQL.Clear;
  QrCheques.SQL.Add('SELECT lo.Lote, li.*');
  QrCheques.SQL.Add('FROM lotesits li');
  QrCheques.SQL.Add('LEFT JOIN lotes lo ON lo.Codigo=li.Codigo');
  QrCheques.SQL.Add('WHERE lo.Tipo=1');
  QrCheques.SQL.Add('AND li.DDeposito > "'+
    FormatDateTime(VAR_FORMATDATE, QrRepasData.Value)+'"');
  QrCheques.SQL.Add('AND li.Quitado = 0');
  QrCheques.SQL.Add('AND li.Repassado = 0');
  //
  Cliente := Geral.IMV(EdCliente.Text);
  if Cliente > 0 then
    QrCheques.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
  //
  if Trim(EdDuplicata.Text) <> '' then
    QrCheques.SQL.Add('AND li.Duplicata='+EdDuplicata.Text);
  //
  if Trim(EdCNPJ.Text) <> '' then
    QrCheques.SQL.Add('AND li.CPF='+Geral.SoNumero_TT(EdCNPJ.Text));
  //
  Cliente := Geral.IMV(EdCliente.Text);
  if Cliente <> 0 then
    QrCheques.SQL.Add('AND lo.Cliente='+IntToStr(Cliente));
  //
  if EdBordero.Enabled then
  begin
    Bordero := Geral.IMV(EdBordero.Text);
    if Bordero <> 0 then
      QrCheques.SQL.Add('AND lo.Lote='+IntToStr(Bordero));
  end;
  //
  QrCheques.Open;
  if LocCod > 0 then QrCheques.Locate('Codigo', LocCod, []);
end;

procedure TFmRepasDU.BtDesiste2Click(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmRepasDU.Incluinovolote1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmRepasDU.Alteraloteatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmRepasDU.Adicionachequeaoloteatual1Click(Sender: TObject);
begin
  MostraEdicao(2, CO_INCLUSAO, 0);
end;

procedure TFmRepasDU.BtConfirma2Click(Sender: TObject);
var
  n, i: integer;
begin
  n := GradeCHs.SelectedRows.Count;
  if n > 0 then
  begin
    if Application.MessageBox(PChar('Confirma o repasse da(s) '+IntToStr(n)+
    ' duplicata(s) selecionada(s)?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) =
    ID_YES then
    begin
      with GradeCHs.DataSource.DataSet do
      for i:= 0 to GradeCHs.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(GradeCHs.SelectedRows.Items[i]));
        RepassaCheque;
      end;
    end;
  end else begin
    if Application.MessageBox('Confirma o repasse da duplicata selecionada?',
    'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then RepassaCheque;
  end;
  CalculaLote(QrRepasCodigo.Value);
  LocCod(QrRepasCodigo.Value, QrRepasCodigo.Value);
  EdDuplicata.Text;
  EdDuplicata.Text := '';
  ReopenDuplicatas(0);
  if LaTipo.Caption = CO_ALTERACAO then MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmRepasDU.RepassaCheque;
var
  Controle, Codigo, Origem, Dias: Integer;
  Valor, Taxa, JurosP, JurosV: Double;
begin
  Codigo := QrRepasCodigo.Value;
  Origem := QrChequesControle.Value;
  Taxa   := Geral.DMV(EdTaxa.Text);
  Dias   := Trunc(int(QrChequesDDeposito.Value) - int(QrRepasData.Value));
  JurosP := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(Taxa, Dias);
  Valor  := QrChequesValor.Value;
  JurosV := (Trunc(Valor * JurosP))/100;
  //
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO repasits SET ');
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'RepasIts', 'RepasIts', 'Codigo');
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE repasits SET ');
    Controle := QrRepasItsControle.Value;
  end;
  Dmod.QrUpdU.SQL.Add('Origem=:P0, Taxa=:P1, JurosP=:P2, JurosV=:P3, ');
  Dmod.QrUpdU.SQL.Add('Dias=:P4, Valor=:P5, ');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd')
  else Dmod.QrUpdU.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Codigo=:Pc AND Controle=:Pd');
  Dmod.QrUpdU.Params[00].AsInteger := Origem;
  Dmod.QrUpdU.Params[01].AsFloat   := Taxa;
  Dmod.QrUpdU.Params[02].AsFloat   := JurosP;
  Dmod.QrUpdU.Params[03].AsFloat   := JurosV;
  Dmod.QrUpdU.Params[04].AsInteger := Dias;
  Dmod.QrUpdU.Params[05].AsFloat   := Valor;
  //
  Dmod.QrUpdU.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[07].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[08].AsInteger := Codigo;
  Dmod.QrUpdU.Params[09].AsInteger := Controle;
  Dmod.QrUpdU.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   Repassado=1 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
  Dmod.QrUpd.Params[00].AsInteger := Origem;
  Dmod.QrUpd.ExecSQL;
  //
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'Repas', 'Codigo');
  FRepasIts := Controle;
end;

procedure TFmRepasDU.RecalculaCheque;
var
  Controle, Codigo, Origem, Dias: Integer;
  Valor, Taxa, JurosP, JurosV: Double;
begin
  Codigo := QrRepasCodigo.Value;
  Origem := QrRepasItsOrigem.Value;
  Taxa   := QrRepasItsTaxa.Value;
  Dias   := Trunc(int(QrRepasItsDDeposito.Value) - int(QrRepasData.Value));
  JurosP := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(Taxa, Dias);
  Valor  := QrRepasItsValor.Value;
  JurosV := (Trunc(Valor * JurosP))/100;
  //
  Controle := QrRepasItsControle.Value;
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('UPDATE repasits SET ');
  Dmod.QrUpdU.SQL.Add('Origem=:P0, Taxa=:P1, JurosP=:P2, JurosV=:P3, ');
  Dmod.QrUpdU.SQL.Add('Dias=:P4, Valor=:P5, ');
  //
  Dmod.QrUpdU.SQL.Add('DataAlt=:Pa, UserAlt=:Pb WHERE Codigo=:Pc AND Controle=:Pd');
  Dmod.QrUpdU.Params[00].AsInteger := Origem;
  Dmod.QrUpdU.Params[01].AsFloat   := Taxa;
  Dmod.QrUpdU.Params[02].AsFloat   := JurosP;
  Dmod.QrUpdU.Params[03].AsFloat   := JurosV;
  Dmod.QrUpdU.Params[04].AsInteger := Dias;
  Dmod.QrUpdU.Params[05].AsFloat   := Valor;
  //
  Dmod.QrUpdU.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[07].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[08].AsInteger := Codigo;
  Dmod.QrUpdU.Params[09].AsInteger := Controle;
  Dmod.QrUpdU.ExecSQL;
  //
end;

procedure TFmRepasDU.ImportaItemDeLote(Taxa: Double);
var
  Controle, Codigo, Origem, Dias: Integer;
  Valor, JurosP, JurosV: Double;
begin
  if QrLIRepassado.Value > 0 then
  begin
    FItensBloqueados := FItensBloqueados + 1;
    Exit;
  end;
  FItensImportados := FItensImportados + 1;
  Codigo := QrRepasCodigo.Value;
  Origem := QrLIControle.Value;
  Dias   := Trunc(int(QrLIDDeposito.Value) - int(QrRepasData.Value));
  JurosP := Dmod.CalculaJurosDesconto(*MLAGeral.CalculaJuroComposto*)(Taxa, Dias);
  Valor  := QrLIValor.Value;
  JurosV := (Trunc(Valor * JurosP))/100;
  //
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
  'RepasIts', 'RepasIts', 'Codigo');
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO repasits SET ');
  Dmod.QrUpdU.SQL.Add('Origem=:P0, Taxa=:P1, JurosP=:P2, JurosV=:P3, ');
  Dmod.QrUpdU.SQL.Add('Dias=:P4, Valor=:P5, ');
  Dmod.QrUpdU.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd');
  //
  Dmod.QrUpdU.Params[00].AsInteger := Origem;
  Dmod.QrUpdU.Params[01].AsFloat   := Taxa;
  Dmod.QrUpdU.Params[02].AsFloat   := JurosP;
  Dmod.QrUpdU.Params[03].AsFloat   := JurosV;
  Dmod.QrUpdU.Params[04].AsInteger := Dias;
  Dmod.QrUpdU.Params[05].AsFloat   := Valor;
  //
  Dmod.QrUpdU.Params[06].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[07].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[08].AsInteger := Codigo;
  Dmod.QrUpdU.Params[09].AsInteger := Controle;
  Dmod.QrUpdU.ExecSQL;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   Repassado=1');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
  Dmod.QrUpd.Params[00].AsInteger := Origem;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TFmRepasDU.CkInapClick(Sender: TObject);
begin
  ReopenDuplicatas(QrChequesCodigo.Value);
end;

procedure TFmRepasDU.CalculaLote(Lote: Integer);
begin
 QrSum.Close;
 QrSum.Params[0].AsInteger := lote;
 QrSum.Open;
 //
 Dmod.QrUpd.SQL.Clear;
 Dmod.QrUpd.SQL.Add('UPDATE repas SET Total=:P0, JurosV=:P1');
 Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa');
 Dmod.QrUpd.SQL.Add('');
 Dmod.QrUpd.Params[00].AsFloat   := QrSumValor.Value;
 Dmod.QrUpd.Params[01].AsFloat   := QrSumJurosV.Value;
 //
 Dmod.QrUpd.Params[02].AsInteger := Lote;
 Dmod.QrUpd.ExecSQL;
end;

procedure TFmRepasDU.Retirachequedoloteatual1Click(Sender: TObject);
begin
  if Application.MessageBox(PChar('Confirma a retirada da duplicata selecionada '+
  'do lote atual?'), 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE lotesits SET AlterWeb=1,   Repassado=0');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrRepasItsOrigem.Value;
    Dmod.QrUpd.ExecSQL;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM repasits WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrRepasItsControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrRepasIts.Next;
    FRepasIts := QrRepasItsControle.Value;
    CalculaLote(QrRepasCodigo.Value);
    LocCod(QrRepasCodigo.Value, QrRepasCodigo.Value);
  end;
end;

procedure TFmRepasDU.QrRepasItsCalcFields(DataSet: TDataSet);
begin
  QrRepasItsCPF_TXT.Value := Geral.FormataCNPJ_TT(QrRepasItsCPF.Value);
end;

procedure TFmRepasDU.QrChequesCalcFields(DataSet: TDataSet);
begin
  QrChequesCPF_TXT.Value := Geral.FormataCNPJ_TT(QrChequesCPF.Value);
end;

procedure TFmRepasDU.BtImpCalculadoClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxRepasIts, 'Repasse de duplicatas');
end;

procedure TFmRepasDU.BtImpPesquisaClick(Sender: TObject);
begin
  Application.CreateForm(TFmRepasImp, FmRepasImp);
  FmRepasImp.ShowModal;
  FmRepasImp.Destroy;
end;

procedure TFmRepasDU.EdTaxaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F12 then
  begin
    if BtConfirma2.Enabled then BtConfirma2Click(Self) else
      Application.MessageBox('Nenhuma duplicata foi localizada!', 'Aviso',
      MB_OK+MB_ICONWARNING);
      EdDuplicata.Text;
      EdDuplicata.Text := '';
  end;
end;

procedure TFmRepasDU.QrChequesAfterOpen(DataSet: TDataSet);
begin
  if QrCheques.RecordCount > 0 then BtConfirma2.Enabled := True else
  BtConfirma2.Enabled := False;
end;

procedure TFmRepasDU.QrChequesAfterClose(DataSet: TDataSet);
begin
  BtConfirma2.Enabled := False;
end;

procedure TFmRepasDU.QrRepasItsAfterOpen(DataSet: TDataSet);
begin
  if PainelItens.Visible then QrRepasIts.Last;
  if QrRepasIts.RecordCount > 0 then BtExclui2.Enabled := True
  else BtExclui2.Enabled := False;
end;

procedure TFmRepasDU.BtExclui2Click(Sender: TObject);
begin
  Retirachequedoloteatual1Click(Self);
end;

procedure TFmRepasDU.QrRepasItsAfterClose(DataSet: TDataSet);
begin
  BtExclui2.Enabled := False;
end;

procedure TFmRepasDU.BtImportarClick(Sender: TObject);
var
  Taxa: Double;
  Texto1, Texto2: String;
begin
  if Application.MessageBox(PChar('Todas duplicatas do border� a ser selecionado '+
  'ser�o incorporadas ao lote de repasse atual. Para incorporar o border� a ser '+
  'selecionado em um lote novo, desista desta opera��o e crie um lote antes '+
  'da importa��o. Deseja continuar assim mesmo?'), 'Aviso', MB_YESNOCANCEL+MB_ICONWARNING) = ID_YES then
  begin
    FmPrincipal.FLoteLoc := 0;
    Application.CreateForm(TFmLotesLoc, FmLotesLoc);
    FmLotesLoc.FFormCall := 1;
    FmLotesLoc.ShowModal;
    FmLotesLoc.Destroy;
    if FmPrincipal.FLoteLoc <> 0 then
    begin
      QrLI.Close;
      QrLI.Params[0].AsInteger := FmPrincipal.FLoteLoc;
      QrLI.Open;
      //
      if QrLI.RecordCount = 0 then
      begin
        Application.MessageBox('O border� selecionado n�o cont�m nenhuma duplicata!',
        'Erro', MB_OK+MB_ICONERROR);
        Exit;
      end else begin
        FItensBloqueados := 0;
        FItensImportados := 0;
        Taxa := MLAGeral.GetPercent(TfmGetPercent, FmGetPercent, 'Taxa de Repasse',
          'Taxa de repasse:', QrRepasFatorCompra.Value+
          Dmod.ObtemExEntiMaior(QrRepasColigado.Value), 6, siPositivo);
        while not QrLI.Eof do
        begin
          ImportaItemDeLote(Taxa);
          QrLI.Next;
        end;
        CalculaLote(QrRepasCodigo.Value);
        LocCod(QrRepasCodigo.Value, QrRepasCodigo.Value);
        FRepasIts := QrRepasItsControle.Value;
        case FItensImportados of
          0: Texto1 := 'Nenhuma duplicata foi importado!';
          1: Texto1 := 'Uma duplicata foi importada!';
          else Texto1 := IntToStr(FItensImportados)+ ' duplicatas foram importadas!';
        end;
        case FItensBloqueados of
          0: Texto2 := 'Nenhum duplicata foi descartada!';
          1: Texto2 := 'Uma duplicata foi descartada!';
          else Texto2 := IntToStr(FItensBloqueados)+ ' duplicatas foram '+
          'descartadas por j� estarem repassados!';
        end;
        Application.MessageBox(PChar(Texto1+Chr(13)+Chr(10)+Chr(13)+Chr(10)+
          Texto2), 'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
  end;
end;

procedure TFmRepasDU.EdBorderoExit(Sender: TObject);
begin
  ReopenDuplicatas(QrChequesControle.Value);
end;

procedure TFmRepasDU.frxRepasItsGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_QTD_CHEQUES' then Value := QrRepasIts.RecordCount;
end;

end.

