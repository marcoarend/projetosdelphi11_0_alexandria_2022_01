unit PesqCPF;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkEdit, Db,
  mySQLDbTables, dmkGeral;

type
  TFmPesqCPF = class(TForm)
    PainelConfirma: TPanel;
    BtPesquisa: TBitBtn;
    BtConfirma: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    RGMascara: TRadioGroup;
    Label1: TLabel;
    EdEmitente: TdmkEdit;
    DBGrid1: TDBGrid;
    QrEmitCPF: TmySQLQuery;
    DsEmitCPF: TDataSource;
    QrEmitCPFCPF: TWideStringField;
    QrEmitCPFNome: TWideStringField;
    QrEmitCPFLimite: TFloatField;
    QrEmitCPFLastAtz: TDateField;
    QrEmitCPFAcumCHComV: TFloatField;
    QrEmitCPFAcumCHComQ: TIntegerField;
    QrEmitCPFAcumCHDevV: TFloatField;
    QrEmitCPFAcumCHDevQ: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure RGMascaraClick(Sender: TObject);
    procedure EdEmitenteChange(Sender: TObject);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmPesqCPF: TFmPesqCPF;

implementation

uses Module, UnInternalConsts, UnMyObjects;

{$R *.DFM}

procedure TFmPesqCPF.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPesqCPF.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPesqCPF.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmPesqCPF.BtPesquisaClick(Sender: TObject);
var
  Emitente: String;
begin
  //if Trim(EdEmitente.Text) <> '' then
  //begin
    Emitente := EdEmitente.Text;
    if RGMascara.ItemIndex in ([0,1]) then Emitente := '%'+Emitente;
    if RGMascara.ItemIndex in ([0,2]) then Emitente := Emitente+'%';
  //end else Emitente := '';
  //////////////////////////////////////////////////////////////////////////////
  QrEmitCPF.Close;
  QrEmitCPF.Params[0].AsString := Emitente;
  QrEmitCPF.Open;
  //
  BtPesquisa.Enabled := False;
  BtConfirma.Enabled := True;
  //configurar grade
end;

procedure TFmPesqCPF.RGMascaraClick(Sender: TObject);
begin
  BtPesquisa.Enabled := True;
  BtConfirma.Enabled := False;
end;

procedure TFmPesqCPF.EdEmitenteChange(Sender: TObject);
begin
  BtPesquisa.Enabled := True;
  BtConfirma.Enabled := False;
end;

procedure TFmPesqCPF.DBGrid1DblClick(Sender: TObject);
begin
  BtConfirmaClick(Self);
end;

procedure TFmPesqCPF.BtConfirmaClick(Sender: TObject);
begin
  VAR_CPF_PESQ := Geral.FormataCNPJ_TT(QrEmitCPFCPF.Value);
  Close;
end;

end.
