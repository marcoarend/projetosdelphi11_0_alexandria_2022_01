unit EntiJur1Ocor;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkLabel;

type
  TFmEntiJur1Ocor = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrOcorBank: TmySQLQuery;
    DsOcorBank: TDataSource;
    QrOcorBankCodigo: TIntegerField;
    QrOcorBankNome: TWideStringField;
    QrOcorBankBase: TFloatField;
    EdOcor: TdmkEditCB;
    CBOcor: TdmkDBLookupComboBox;
    Label1: TLabel;
    Label4: TLabel;
    RGFormaCNAB: TRadioGroup;
    EdBase: TdmkEdit;
    LaTipo: TdmkLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdOcorChange(Sender: TObject);
    procedure CBOcorClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCliente, FControle: Integer;
    FResult: Boolean;
  end;

  var
  FmEntiJur1Ocor: TFmEntiJur1Ocor;

implementation

uses UnFinanceiro, Module, UMySQLModule, UnMyObjects;

{$R *.DFM}

procedure TFmEntiJur1Ocor.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiJur1Ocor.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmEntiJur1Ocor.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmEntiJur1Ocor.FormCreate(Sender: TObject);
begin
  QrOcorBank.Open;
  FResult := False;
end;

procedure TFmEntiJur1Ocor.EdOcorChange(Sender: TObject);
begin
  if QrOcorBank.Locate('Codigo', EdOcor.ValueVariant, []) then
    EdBase.ValueVariant := QrOcorBankBase.value;
end;

procedure TFmEntiJur1Ocor.BtOKClick(Sender: TObject);
var
  Ocorrencia, FormaCNAB: Integer;
  Base: Double;
begin
  Ocorrencia := EdOcor.ValueVariant;
  Base       := EdBase.ValueVariant;
  FormaCNAB  := RGFormaCNAB.ItemIndex;
  if Ocorrencia = 0 then
  begin
    Application.MessageBox('Defina uma ocorrência.', 'Aviso',
      MB_OK+MB_ICONWARNING);
    Exit;
  end;

  FControle := UMyMod.BuscaEmLivreY_Def('ocorcli', 'Controle',
  LaTipo.SQLType, FControle);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'ocorcli', False, [
    'Cliente', 'Ocorrencia', 'Base', 'FormaCNAB'
  ], ['Controle'], [
    FCliente, Ocorrencia, Base, FormaCNAB],
  [FControle], True) then
  begin
    FResult := True;
    Close;
  end;
end;

procedure TFmEntiJur1Ocor.CBOcorClick(Sender: TObject);
begin
  EdBase.ValueVariant := QrOcorBankBase.value;
end;

end.
