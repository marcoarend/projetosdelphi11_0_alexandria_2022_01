unit PdxOrfao;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, lmdrtfbase, lmdrtfrichedit,
  LMDCustomControl, LMDCustomPanel, LMDCustomBevelPanel,
  LMDCustomParentPanel, LMDCustomPanelFill, LMDPanelFill, Db, (*DBTables,*)
  Grids, DBGrids, ComCtrls, mySQLDbTables;

type
  TFmPdxOrfao = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtPesquisa: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TLMDPanelFill;
    Image1: TImage;
    QrMovto2: TQuery;
    DBGrid1: TDBGrid;
    BtExclui: TBitBtn;
    Progress: TProgressBar;
    QrLotesIts: TmySQLQuery;
    QrLotesItsCodigo: TIntegerField;
    QrLotesItsControle: TIntegerField;
    QrMovto2Codigo: TIntegerField;
    QrMovto2Controle: TIntegerField;
    QrMovto2Existe: TIntegerField;
    QrOrfaos: TQuery;
    DsOrfaos: TDataSource;
    QrOrfaosCodigo: TIntegerField;
    QrOrfaosControle: TIntegerField;
    QrOrfaosNumA: TFloatField;
    QrOrfaosNumAV: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenOrfaos;
  public
    { Public declarations }
  end;

  var
  FmPdxOrfao: TFmPdxOrfao;

implementation

uses Module;

{$R *.DFM}

procedure TFmPdxOrfao.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPdxOrfao.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPdxOrfao.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToLMDPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmPdxOrfao.BtPesquisaClick(Sender: TObject);
begin
  Screen.Cursor := crHourGlass;
  try
    QrLotesIts.Close;
    QrLotesIts.Open;
    //
    QrMovto2.Close;
    QrMovto2.Open;
    //
    Progress.Max := QrMovto2.RecordCount;
    Progress.Position := 0;
    Dmod.QrPdxU.SQL.Clear;
    Dmod.QrPdxU.SQL.Add('UPDATE movto2 SET Orfao=0');
    Dmod.QrPdxU.ExecSQL;
    //
    Dmod.QrPdxU.SQL.Clear;
    Dmod.QrPdxU.SQL.Add('UPDATE movto2 SET Orfao=1 WHERE Controle=:P0');
    while not QrMovto2.Eof do
    begin
      Progress.Position := Progress.Position +1;
      if QrMovto2Existe.Value = 0 then
      begin
        Dmod.QrPdxU.Params[0].AsInteger := QrMovto2Controle.Value;
        Dmod.QrPdxU.ExecSQL;
      end;
      QrMovto2.Next;
    end;
    ReopenOrfaos;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

procedure TFmPdxOrfao.ReopenOrfaos;
begin
  QrOrfaos.Close;
  QrOrfaos.Open;
end;

procedure TFmPdxOrfao.BtExcluiClick(Sender: TObject);
var
  i: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    if DBGrid1.SelectedRows.Count = 0 then
    begin
      Dmod.QrPdxU.SQL.Clear;
      Dmod.QrPdxU.SQL.Add('DELETE FROM movto2 WHERE Controle=:P0');
      Dmod.QrPdxU.SQL.Add('');
      Dmod.QrPdxU.Params[0].AsInteger := QrOrfaosControle.Value;
      Dmod.QrPdxU.ExecSQL;
    end else begin
      with DBGrid1.DataSource.DataSet do
      for i:= 0 to DBGrid1.SelectedRows.Count-1 do
      begin
        GotoBookmark(pointer(DBGrid1.SelectedRows.Items[i]));
        Dmod.QrPdxU.SQL.Clear;
        Dmod.QrPdxU.SQL.Add('DELETE FROM movto2 WHERE Controle=:P0');
        Dmod.QrPdxU.SQL.Add('');
        Dmod.QrPdxU.Params[0].AsInteger := QrOrfaosControle.Value;
        Dmod.QrPdxU.ExecSQL;
        //
      end;
    end;
    ReopenOrfaos;
    Screen.Cursor := crDefault;
  except
    Screen.Cursor := crDefault;
    raise;
  end;
end;

end.

