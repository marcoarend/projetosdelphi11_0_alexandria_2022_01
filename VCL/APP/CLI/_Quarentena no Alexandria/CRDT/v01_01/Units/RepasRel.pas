unit RepasRel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral;

type
  TFmRepasRel = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmRepasRel: TFmRepasRel;

implementation

{$R *.DFM}

uses UnMyObjects;

procedure TFmRepasRel.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRepasRel.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmRepasRel.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
