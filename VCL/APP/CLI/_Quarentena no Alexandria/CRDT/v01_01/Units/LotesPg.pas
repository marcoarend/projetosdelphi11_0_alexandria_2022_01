unit LotesPg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, Db, mySQLDbTables, ComCtrls,
  Menus, Mask, frxClass, frxDBSet, dmkGeral, dmkDBLookupComboBox, dmkEdit,
  dmkEditCB, UnDmkProcFunc, UnDmkEnums;

type
  TFmLotesPG = class(TForm)
    PainelDados: TPanel;
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    BtCheque: TBitBtn;
    BtSaida: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    Panel1: TPanel;
    Label75: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdValor: TdmkEdit;
    TPData: TDateTimePicker;
    Label3: TLabel;
    Panel2: TPanel;
    Label4: TLabel;
    EdTxt: TdmkEdit;
    Label33: TLabel;
    EdBanda1: TdmkEdit;
    Label34: TLabel;
    EdBanco1: TdmkEdit;
    EdAgencia1: TdmkEdit;
    Label35: TLabel;
    Label37: TLabel;
    EdConta1: TdmkEdit;
    Label38: TLabel;
    EdCheque1: TdmkEdit;
    EdCMC_7_1: TEdit;
    QrLocCC: TmySQLQuery;
    QrLocCCCodigo: TIntegerField;
    QrLocCCTipo: TIntegerField;
    QrLocCCNome: TWideStringField;
    QrLocCCInicial: TFloatField;
    QrLocCCBanco: TIntegerField;
    QrLocCCID: TWideStringField;
    QrLocCCFatura: TWideStringField;
    QrLocCCID_Fat: TWideStringField;
    QrLocCCSaldo: TFloatField;
    QrLocCCEmCaixa: TFloatField;
    QrLocCCFechamento: TIntegerField;
    QrLocCCPrazo: TSmallintField;
    QrLocCCPagRec: TIntegerField;
    QrLocCCDiaMesVence: TSmallintField;
    QrLocCCExigeNumCheque: TSmallintField;
    QrLocCCForneceI: TIntegerField;
    QrLocCCLk: TIntegerField;
    QrLocCCDataCad: TDateField;
    QrLocCCDataAlt: TDateField;
    QrLocCCUserCad: TIntegerField;
    QrLocCCUserAlt: TIntegerField;
    QrLocCCNome2: TWideStringField;
    QrLocCCTipoDoc: TSmallintField;
    QrLocCCBanco1: TIntegerField;
    QrLocCCAgencia1: TIntegerField;
    QrLocCCConta1: TWideStringField;
    QrLocCCCheque1: TIntegerField;
    Label2: TLabel;
    EdCreditado: TdmkEdit;
    Label5: TLabel;
    EdBanco2: TdmkEdit;
    EdAgencia2: TdmkEdit;
    Label6: TLabel;
    Label7: TLabel;
    EdConta2: TdmkEdit;
    Label10: TLabel;
    EdCPF2: TdmkEdit;
    CBCreditado: TDBLookupComboBox;
    QrCreditados: TmySQLQuery;
    DsCreditados: TDataSource;
    QrCreditadosLastPaymt: TIntegerField;
    QrCreditadosBAC: TWideStringField;
    QrCreditadosNome: TWideStringField;
    QrCreditadosCPF: TWideStringField;
    QrCreditadosBanco: TIntegerField;
    QrCreditadosAgencia: TIntegerField;
    QrCreditadosConta: TWideStringField;
    QrLocCr: TmySQLQuery;
    QrLocCrLastPaymt: TIntegerField;
    BtEnvelope: TBitBtn;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasInicial: TFloatField;
    QrCarteirasBanco: TIntegerField;
    QrCarteirasID: TWideStringField;
    QrCarteirasFatura: TWideStringField;
    QrCarteirasID_Fat: TWideStringField;
    QrCarteirasSaldo: TFloatField;
    QrCarteirasEmCaixa: TFloatField;
    QrCarteirasFechamento: TIntegerField;
    QrCarteirasPrazo: TSmallintField;
    QrCarteirasPagRec: TIntegerField;
    QrCarteirasDiaMesVence: TSmallintField;
    QrCarteirasExigeNumCheque: TSmallintField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasLk: TIntegerField;
    QrCarteirasDataCad: TDateField;
    QrCarteirasDataAlt: TDateField;
    QrCarteirasUserCad: TIntegerField;
    QrCarteirasUserAlt: TIntegerField;
    QrCarteirasNome2: TWideStringField;
    QrCarteirasTipoDoc: TSmallintField;
    QrCarteirasBanco1: TIntegerField;
    QrCarteirasAgencia1: TIntegerField;
    QrCarteirasConta1: TWideStringField;
    QrCarteirasCheque1: TIntegerField;
    QrCarteirasContato1: TWideStringField;
    QrCarteirasNOMEFORNECEI: TWideStringField;
    QrCarteirasNOME2DOBANCO: TWideStringField;
    QrCarteirasNOMETIPODOC: TWideStringField;
    QrBanco1: TmySQLQuery;
    QrBanco1Nome: TWideStringField;
    BitBtn1: TBitBtn;
    Label12: TLabel;
    PnCreditado: TPanel;
    Label14: TLabel;
    DBEdit5: TDBEdit;
    Label8: TLabel;
    DBEdit1: TDBEdit;
    Label9: TLabel;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Label11: TLabel;
    Label13: TLabel;
    DBEdit4: TDBEdit;
    frxDOC_TED: TfrxReport;
    frxDsCarteiras: TfrxDBDataset;
    PMCheque: TPopupMenu;
    Pertochek1: TMenuItem;
    Texto1: TMenuItem;
    Portador1: TMenuItem;
    Nominal1: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdBanda1Change(Sender: TObject);
    procedure EdCMC_7_1Change(Sender: TObject);
    procedure EdCPF2Exit(Sender: TObject);
    procedure QrCreditadosAfterScroll(DataSet: TDataSet);
    procedure CBCreditadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdCreditadoKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtChequeClick(Sender: TObject);
    procedure Nominal1Click(Sender: TObject);
    procedure Portador1Click(Sender: TObject);
    procedure Pertochek1Click(Sender: TObject);
    procedure PMChequePopup(Sender: TObject);
    procedure BtEnvelopeClick(Sender: TObject);
    procedure QrCarteirasCalcFields(DataSet: TDataSet);
    procedure BitBtn1Click(Sender: TObject);
    procedure frxDOC_TEDGetValue(const VarName: String;
      var Value: Variant);
  private
    { Private declarations }
    procedure ConfiguraCBCreditado;
  public
    { Public declarations }
    FCliente, FFatNum, FFatParcela: Integer;
    FNomeCreditado, FCNPJCreditado: String;
    FValor: Double;
    procedure ReopenCreditados;
  end;

  var
  FmLotesPG: TFmLotesPG;

implementation

{$R *.DFM}

uses Module, UMySQLModule, UnInternalConsts, Principal, Lotes1,
EmiteCheque_0, UnMyObjects;

procedure TFmLotesPG.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmLotesPG.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if EdCreditado.Visible then
    EdCreditado.SetFocus;
end;

procedure TFmLotesPG.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmLotesPG.FormCreate(Sender: TObject);
begin
  EdCreditado.Top := CBCreditado.Top;
  QrCarteiras.Open;
end;

procedure TFmLotesPG.BtOKClick(Sender: TObject);
var
  Carteira, Controle, Banco: Integer;
  Nome, DataStr: String;
  Doc, Debito: Double;
begin
  Carteira := Geral.IMV(EdCarteira.Text);
  if Carteira = 0 then
  begin
    Application.MessageBox('Informe a carteira!', 'Erro', MB_OK+MB_ICONERROR);
    EdCarteira.SetFocus;
    Exit;
  end;
  Debito := Geral.DMV(EdValor.Text);
  if Debito = 0 then
  begin
    Application.MessageBox('Informe o valor!', 'Erro', MB_OK+MB_ICONERROR);
    EdValor.SetFocus;
    Exit;
  end;
  Banco   := Geral.IMV(EdBanco2.Text);
  Doc := Geral.DMV(EdCheque1.Text);
  //
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    VAR_LCT, VAR_LCT, 'Controle');
  DataStr := FormatDateTime(VAR_FORMATDATE, TPData.Date);
  VAR_LANCTO2 := Controle;
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('INSERT INTO ' + VAR_LCT  + ' SET Documento=:P0, Data=:P1, ');
  Dmod.QrUpdU.SQL.Add('Tipo=:P2, Carteira=:P3, Credito=:P4, Debito=:P5, ');
  Dmod.QrUpdU.SQL.Add('Genero=:P6, NotaFiscal=:P7, Vencimento=:P8, Mez=:P9, ');
  Dmod.QrUpdU.SQL.Add('Descricao=:P10, Sit=:P11, Controle=:P12, Cartao=:P13, ');
  Dmod.QrUpdU.SQL.Add('Compensado=:P14, Linha=:P15, Fornecedor=:P16, ');
  Dmod.QrUpdU.SQL.Add('Cliente=:P17, MoraDia=:P18, Multa=:P19,DataCad=:P20, ');
  Dmod.QrUpdU.SQL.Add('UserCad=:P21, DataDoc=:P22, Vendedor=:P23, ');
  Dmod.QrUpdU.SQL.Add('Account=:P24, FatID=:P25, FatNum=:P26, ');
  Dmod.QrUpdU.SQL.Add('FatParcela=:P27, Banco=:P28, Agencia=:P29, ContaCorrente=:P30');

  Dmod.QrUpdU.Params[00].AsFloat   := Doc;
  Dmod.QrUpdU.Params[01].AsString  := DataStr;
  Dmod.QrUpdU.Params[02].AsInteger := QrCarteirasTipo.Value;
  Dmod.QrUpdU.Params[03].AsInteger := Carteira;
  Dmod.QrUpdU.Params[04].AsFloat   := 0;
  Dmod.QrUpdU.Params[05].AsFloat   := Debito;
  Dmod.QrUpdU.Params[06].AsInteger := -399;
  Dmod.QrUpdU.Params[07].AsInteger := 0;
  Dmod.QrUpdU.Params[08].AsString  := DataStr;
  Dmod.QrUpdU.Params[09].AsString  := '';
  Dmod.QrUpdU.Params[10].AsString  := EdTxt.Text;
  Dmod.QrUpdU.Params[11].AsInteger := 2;
  Dmod.QrUpdU.Params[12].AsFloat   := Controle;
  Dmod.QrUpdU.Params[13].AsInteger := 0;
  Dmod.QrUpdU.Params[14].AsString  := '';
  Dmod.QrUpdU.Params[15].AsInteger := 0;
  Dmod.QrUpdU.Params[16].AsInteger := 0;
  Dmod.QrUpdU.Params[17].AsInteger := FCliente;
  Dmod.QrUpdU.Params[18].AsFloat   := 0;
  Dmod.QrUpdU.Params[19].AsFloat   := 0;
  Dmod.QrUpdU.Params[20].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[21].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[22].AsString  := DataStr;
  Dmod.QrUpdU.Params[23].AsInteger := 0;
  Dmod.QrUpdU.Params[24].AsInteger := 0;
  Dmod.QrUpdU.Params[25].AsInteger := 300;
  Dmod.QrUpdU.Params[26].AsInteger := FFatNum;
  Dmod.QrUpdU.Params[27].AsInteger := FFatParcela;
  Dmod.QrUpdU.Params[28].AsInteger := Banco;
  Dmod.QrUpdU.Params[29].AsString  := EdAgencia2.Text;
  Dmod.QrUpdU.Params[30].AsString  := EdConta2.Text;
  Dmod.QrUpdU.ExecSQL;
  //
  if CBCreditado.Visible then Nome := CBCreditado.Text
  else Nome := EdCreditado.Text;
  FmPrincipal.AtualizaCreditado(EdBanco2.Text, EdAgencia2.Text, EdConta2.Text,
    Geral.SoNumero_TT(EdCPF2.Text), Nome, FCliente, Controle);

  if Doc > 0.1 then EdCheque1.Text := Geral.FFT(Doc+1, 0, siPositivo);
  CBCreditado.Visible := True;
  PnCreditado.Visible := True;
  EdCreditado.Visible := False;
  FValor := FValor - Debito;
  if FValor < 0 then FValor := 0;
  EdValor.Text := Geral.FFT(FValor, 2, siPositivo);
  EdValor.SetFocus;
  ReopenCreditados;
  case FmPrincipal.FFormLotesShow of
    {
    0:
    begin
      FmLotes0.CalculaLote(FFatNum, False);
      FmLotes0.LocCod(FFatNum, FFatNum);
      FmLotes0.FMudouSaldo := True;
    end;
    }
    1:
    begin
      FmLotes1.CalculaLote(FFatNum, False);
      FmLotes1.LocCod(FFatNum, FFatNum);
      FmLotes1.FMudouSaldo := True;
    end;
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (15)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
end;

procedure TFmLotesPG.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_ESCAPE then BtSaidaClick(Self);
end;

procedure TFmLotesPG.EdBanda1Change(Sender: TObject);
begin
  if MLAGeral.LeuTodoCMC7(EdBanda1.Text) then
    EdCMC_7_1.Text := Geral.SoNumero_TT(EdBanda1.Text);
end;

procedure TFmLotesPG.EdCMC_7_1Change(Sender: TObject);
var
  Banda1: TBandaMagnetica;
begin
  if MLAGeral.CalculaCMC7(EdCMC_7_1.Text) = 0 then
  begin
    Banda1 := TBandaMagnetica.Create;
    Banda1.BandaMagnetica := EdCMC_7_1.Text;
    //EdComp1.Text    := Banda.Compe;
    EdBanco1.Text   := Banda1.Banco;
    EdAgencia1.Text := Banda1.Agencia;
    EdConta1.Text   := Banda1.Conta;
    EdCheque1.Text  := Banda1.Numero;
    //
    //LocalizaContaCorrente;
    QrLocCC.Close;
    QrLocCC.Params[00].AsString := Banda1.Banco;
    QrLocCC.Params[01].AsString := Banda1.Agencia;
    QrLocCC.Params[02].AsString := Banda1.Conta;
    QrLocCC.Open;
    if QrLocCC.RecordCount > 0 then
    begin
      EdCarteira.Text := IntToStr(QrLocCCCodigo.Value);
      CBCarteira.KeyValue := QrLocCCCodigo.Value;
      CBCreditado.SetFocus;
    end else EdCarteira.SetFocus;
  end;
end;

procedure TFmLotesPG.EdCPF2Exit(Sender: TObject);
var
  Num : String;
  CPF2 : String;
begin
  CPF2 := Geral.SoNumero_TT(EdCPF2.Text);
  if CPF2 <> '' then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF2);
    if MLAGeral.FormataCNPJ_TFT(CPF2) <> Num then
    begin
      Application.MessageBox(PChar('N�mero inv�lido!'), 'Erro', MB_OK+MB_ICONERROR);
      EdCPF2.SetFocus;
    end else EdCPF2.Text := Geral.FormataCNPJ_TT(CPF2);
  end else EdCPF2.Text := '';
end;

procedure TFmLotesPG.QrCreditadosAfterScroll(DataSet: TDataSet);
begin
  if CBCreditado.Visible then ConfiguraCBCreditado;
end;

procedure TFmLotesPG.ConfiguraCBCreditado;
begin
  if CBCreditado.Visible then
  begin
    EdCreditado.Text:= QrCreditadosNome.Value;
    EdCPF2.Text     := Geral.FormataCNPJ_TT(QrCreditadosCPF.Value);
    EdBanco2.Text   := MLAGeral.FFD(QrCreditadosBanco.Value, 3, siPositivo);
    EdAgencia2.Text := MLAGeral.FFD(QrCreditadosAgencia.Value, 4, siPositivo);
    EdConta2.Text   := QrCreditadosConta.Value;
  end;
end;

procedure TFmLotesPG.CBCreditadoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = VK_F4) and (not CBCreditado.ListVisible) then
  begin
    EdCreditado.Visible := True;
    CBCreditado.Visible := False;
    PnCreditado.Visible := False;
    EdCreditado.Text    := FNomeCreditado;
    EdCPF2.Text         := Geral.FormataCNPJ_TT(FCNPJCreditado);
    EdBanco2.Text       := '';
    EdAgencia2.Text     := '';
    EdConta2.Text       := '';
    EdCreditado.SetFocus;
  end;
end;

procedure TFmLotesPG.ReopenCreditados;
begin
  QrCreditados.Close;
  QrCreditados.Params[0].AsInteger := FCliente;
  QrCreditados.Open;
  //
  if QrCreditados.RecordCount > 0 then
  begin
    QrLocCr.Close;
    QrLocCr.Params[0].AsInteger := FCliente;
    //QrLocCr.Params[1].AsInteger := FCliente;
    QrLocCr.Open;
    //
    if QrCreditados.Locate('LastPaymt', QrLocCrLastPaymt.Value, []) then
    CBCreditado.KeyValue := QrLocCrLastPaymt.Value;
  end;
end;

procedure TFmLotesPG.EdCreditadoKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then
  begin
    CBCreditado.Visible := True;
    PnCreditado.Visible := True;
    EdCreditado.Visible := False;
    //
    CBCreditado.SetFocus;
    ConfiguraCBCreditado;
  end;
end;

procedure TFmLotesPG.BtChequeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCheque, BtCheque);
end;

procedure TFmLotesPG.Nominal1Click(Sender: TObject);
var
  Creditado: String;
begin
  if CBCreditado.Visible then Creditado := CBCreditado.Text
  else Creditado := EdCreditado.Text;
  FmPrincipal.ImprimeChequeSimples(EdValor.Text, Creditado,
    Dmod.QrDonoCIDADE.Value, TPData.Date, True);
end;

procedure TFmLotesPG.Portador1Click(Sender: TObject);
var
  Creditado: String;
begin
  if CBCreditado.Visible then Creditado := CBCreditado.Text
  else Creditado := EdCreditado.Text;
  FmPrincipal.ImprimeChequeSimples(EdValor.Text, Creditado,
    Dmod.QrDonoCIDADE.Value, TPData.Date, False);
end;

procedure TFmLotesPG.Pertochek1Click(Sender: TObject);
var
  Valor, Benef, Cidade: String;
begin
  // MUDADO EM 2010-05-05
  // N�o foi testado!!!!
{
  Application.CreateForm(TFmEmiteCheque, FmEmiteCheque);
  FmEmiteCheque.TPData.Date  := TPData.Date;
  FmEmiteCheque.EdValor.Text := EdValor.Text;
  if CBCreditado.Visible then FmEmiteCheque.EdBenef.Text := CBCreditado.Text
  else FmEmiteCheque.EdBenef.Text := EdCreditado.Text;
  FmEmiteCheque.EdCidade.Text := Geral.SemAcento(Geral.Maiusculas(
  Dmod.QrDonoCIDADE.Value, False));
  //
  FmEmiteCheque.ShowModal;
  if FmEmiteCheque.ST_CMC7.Caption <> '' then
    EdBanda1.Text := MLAGeral.SoNumeroECMC7(FmEmiteCheque.ST_CMC7.Caption);
  FmEmiteCheque.Destroy;
}
  {
  if FmPrincipal.QrLctDebito.Value <> 0 then
  begin
    Valor := Geral.FFT(FmPrincipal.QrLctDebito.Value, 2, siPositivo);
    Benef := FmPrincipal.QrLctNOMEFORNECEDOR.Value;
  end else begin
    Valor := Geral.FFT(FmPrincipal.QrLctCredito.Value, 2, siPositivo);
    Benef := FmPrincipal.QrLctNOMECLIENTE.Value;
  end;
  }
  Valor := EdValor.Text;
  Benef := QrCreditadosNome.Value;
  //
  Benef := Geral.SemAcento(Geral.Maiusculas(Benef, False));
  Cidade := Geral.SemAcento(Geral.Maiusculas(Dmod.QrDonoCIDADE.Value, False));
  //
  Application.CreateForm(TFmEmiteCheque_0, FmEmiteCheque_0);
  FmEmiteCheque_0.TPData.Date   := TPData.Date;//FmPrincipal.QrLctData.Value;
  FmEmiteCheque_0.EdValor.Text  := Valor;
  FmEmiteCheque_0.EdBenef.Text  := Benef;
  FmEmiteCheque_0.EdCidade.Text := Cidade;
  FmEmiteCheque_0.EdBanco.Text  := EdBanco1.Text;//FormatFloat('0', FmPrincipal.QrCarteirasBanco1.Value);
  //
  FmEmiteCheque_0.ShowModal;
  FmEmiteCheque_0.Destroy;
end;

procedure TFmLotesPG.PMChequePopup(Sender: TObject);
begin
  if VAR_IMPRECHEQUE = 1 then
    Pertochek1.Enabled := True
  else
    Pertochek1.Enabled := False;
end;

procedure TFmLotesPG.BtEnvelopeClick(Sender: TObject);
begin
 MyObjects.frxMostra(frxDOC_TED, 'Carta a banco (DOC - TED)');
end;

procedure TFmLotesPG.QrCarteirasCalcFields(DataSet: TDataSet);
begin
  case QrCarteirasTipoDoc.Value of
    0: QrCarteirasNOMETIPODOC.Value := 'N/I';
    1: QrCarteirasNOMETIPODOC.Value := 'Cheque';
    2: QrCarteirasNOMETIPODOC.Value := 'DOC';
    3: QrCarteirasNOMETIPODOC.Value := 'TED';
    4: QrCarteirasNOMETIPODOC.Value := 'Esp�cie';
    else QrCarteirasNOMETIPODOC.Value := '???';
  end;
end;

procedure TFmLotesPG.BitBtn1Click(Sender: TObject);
begin
  case FmPrincipal.FFormLotesShow of
    //0: FmLotes0.Rpido1Click(Self);
    1: FmLotes1.Rpido1Click(Self);
    else Application.MessageBox(PChar('Janela de gerenciamento de border�s ' +
    'n�o definida! (16)'), 'ERRO', MB_OK+MB_ICONERROR);
  end;
  Close;
end;

procedure TFmLotesPG.frxDOC_TEDGetValue(const VarName: String;
  var Value: Variant);
begin
  if VarName = 'VARF_VALOR' then Value := EdValor.Text + ' ('+
    dmkPF.ExtensoMoney(EdValor.Text)+')'
  else if VarName = 'VARF_BANCO' then
  begin
    QrBanco1.Close;
    QrBanco1.Params[0].AsString := EdBanco2.Text;
    QrBanco1.Open;
    if QrBanco1.RecordCount > 0 then Value := EdBanco2.Text + ' - ' +
      QrBanco1Nome.Value else Value := EdBanco2.Text;
  end else if VarName = 'VARF_AGENCIA' then Value := EdAgencia2.Text
  else if VarName = 'VARF_CONTA' then Value := EdConta2.Text
  else if VarName = 'VARF_CREDITADO' then
  begin
    if CBCreditado.Visible then Value := CBCreditado.Text
    else Value := EdCreditado.Text;
  end else if VarName = 'VARF_CNPJ' then Value := EdCPF2.Text
  else if VarName = 'VARF_CIDADE_E_DATA' then Value := Dmod.QrDonoCIDADE.Value
    + ', '+FormatDateTime('dd" de "mmmm" de "yyyy"."', TPData.Date)
end;

end.
