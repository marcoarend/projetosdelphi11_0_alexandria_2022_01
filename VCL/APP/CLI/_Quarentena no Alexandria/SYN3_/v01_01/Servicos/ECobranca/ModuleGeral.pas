unit ModuleGeral;

interface

uses
  SysUtils, Classes, DB, mySQLDbTables, dmkGeral;

type
  TDModG = class(TDataModule)
    MyPID_DB: TmySQLDatabase;
    QrUpdPID1: TmySQLQuery;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
{
    function  NomeTab(DBName: String; Tab: TNomeTab; SetaVAR_LCT: Boolean;
              Letra: TTipoTab = ttA; Empresa: Integer = 0): String;
}
  end;

var
  DModG: TDModG;

implementation

{$R *.dfm}

procedure TDModG.DataModuleCreate(Sender: TObject);
begin
  MyPID_DB.LoginPrompt := False;

end;

(*
function TDModG.NomeTab(DBName: String; Tab: TNomeTab; SetaVAR_LCT: Boolean;
  Letra: TTipoTab; Empresa: Integer): String;
var
  N1, N2, N3, Nome: String;
  CliInt: Integer;
begin
  if Empresa = 0 then
    CliInt := EmpresaAtual_ObtemCodigo(tecCliInt)
  else CliInt := Empresa;
  //
  if CliInt = 0 then
  begin
    if VAR_FilialUnica <> 0 then
    begin
      CliInt := VAR_FilialUnica
    end else
    begin
      {
      Geral.MensagemBox('ERRO! Tabela de lan�amentos ficaria indefinida!' +
      #13#10 + 'function TDModG.NomeTab()' + #13#10
      + 'O aplicativo ser� encerrado!', 'ERRO', MB_OK+MB_ICONERROR);
      }
      raise Exception.Create(
        'ERRO! Tabela de lan�amentos ficou indefinida!' +
        #13#10 + 'function TDModG.NomeTab()');
      Halt;
    end;
  end;
  if CliInt < 0 then
    N2 := '_' + FormatFloat('000', - CliInt)
  else
    N2 := FormatFloat('0000', CliInt);
  //

  case Letra of
    tt_: N3 := Lowercase(VAR_LETRA_LCT);
    ttA: N3 := 'a';
    ttB: N3 := 'b';
    ttD: N3 := 'd';
  end;
  //
  if not (N3[1] in (['a','b','c','d','e','f'])) then
  begin
    Geral.MensagemBox('Tipo de tabela de lan�amentos n�o definida!',
    'Erro', MB_OK+MB_ICONERROR);
  end;
  if N3 = 'c' then
    N1 := VAR_MyPID_DB_NOME
  else
    N1 := DBName;
  if N1 <> '' then
  begin
    if N1[Length(N1)] <> '.' then
      N1 := N1 + '.';
  end;
  //
  case Tab of
    ntLct: Nome := 'lct';
    ntAri: Nome := 'ari';
    ntCns: Nome := 'cns';
    ntPri: Nome := 'pri';
    ntPrv: Nome := 'prv';
    else   Nome := 'err';
  end;
  Result := Lowercase(N1 + Nome + N2 + N3);
  //
  if SetaVAR_LCT then
  begin
    {$IFDEF DEFINE_VARLCT}
    if (LowerCase(Application.Title) = 'exul2')
    or (LowerCase(Application.Name) = 'credito2')
    or (LowerCase(Application.Name) = 'sindi2') then
      VAR_LCT := '!ERRO!'
    else
      VAR_LCT := Result;
    {$ENDIF}
  end;
end;
*)

end.
