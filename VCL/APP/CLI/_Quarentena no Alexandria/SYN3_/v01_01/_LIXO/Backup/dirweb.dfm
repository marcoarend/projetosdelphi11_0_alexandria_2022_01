object FmDirWeb: TFmDirWeb
  Left = 343
  Top = 228
  Caption = 'WEB-DIRET-001 :: Diret'#243'rios WEB'
  ClientHeight = 588
  ClientWidth = 725
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 92
    Width = 725
    Height = 496
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object Label9: TLabel
      Left = 16
      Top = 8
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label10: TLabel
      Left = 16
      Top = 52
      Width = 31
      Height = 13
      Caption = 'Nome:'
    end
    object Label149: TLabel
      Left = 15
      Top = 131
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object Label1: TLabel
      Left = 16
      Top = 174
      Width = 427
      Height = 13
      Caption = 
        'Aviso: O campo "PASTA" deve estar sem espa'#231'amento e sem acentua'#231 +
        #227'o.'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 16
      Top = 91
      Width = 30
      Height = 13
      Caption = 'Pasta:'
    end
    object EdCodigo: TdmkEdit
      Left = 16
      Top = 24
      Width = 100
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdDescri: TdmkEdit
      Left = 15
      Top = 147
      Width = 385
      Height = 21
      MaxLength = 30
      TabOrder = 3
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdNome: TdmkEdit
      Left = 15
      Top = 68
      Width = 278
      Height = 21
      MaxLength = 20
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object RGNivel: TRadioGroup
      Left = 299
      Top = 64
      Width = 101
      Height = 75
      Caption = 'N'#237'vel'
      ItemIndex = 0
      Items.Strings = (
        'Ambos'
        'Cond'#244'mino'
        'S'#237'ndico'
        'Nenhum')
      TabOrder = 4
    end
    object EdPasta: TdmkEdit
      Left = 15
      Top = 107
      Width = 278
      Height = 21
      CharCase = ecLowerCase
      MaxLength = 8
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnExit = EdPastaExit
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 426
      Width = 725
      Height = 70
      Align = alBottom
      TabOrder = 5
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 721
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel3: TPanel
          Left = 577
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 6
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
            NumGlyphs = 2
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
          NumGlyphs = 2
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 92
    Width = 725
    Height = 496
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 5
      Top = 33
      Width = 723
      Height = 270
      DataSource = DsDirWeb
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 137
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Pasta'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          Width = 58
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descri'
          Title.Caption = 'Descri'#231#227'o'
          Width = 232
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMENIVEL'
          Title.Caption = 'N'#237'vel'
          Width = 66
          Visible = True
        end>
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 426
      Width = 725
      Height = 70
      Align = alBottom
      TabOrder = 1
      object Panel1: TPanel
        Left = 2
        Top = 15
        Width = 721
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 577
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 6
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
            NumGlyphs = 2
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 5
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
          NumGlyphs = 2
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 129
          Top = 5
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
          NumGlyphs = 2
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 253
          Top = 5
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Caption = '&Exclui'
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtExcluiClick
          NumGlyphs = 2
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 725
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 677
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 629
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 189
        Height = 32
        Caption = 'Diret'#243'rios WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 189
        Height = 32
        Caption = 'Diret'#243'rios WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 189
        Height = 32
        Caption = 'Diret'#243'rios WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 725
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 721
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrDirWeb: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = QrDirWebCalcFields
    SQL.Strings = (
      'SELECT *'
      'FROM dirweb')
    Left = 580
    Top = 8
    object QrDirWebCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrDirWebNome: TWideStringField
      FieldName = 'Nome'
      Size = 32
    end
    object QrDirWebDescri: TWideStringField
      FieldName = 'Descri'
      Size = 32
    end
    object QrDirWebPasta: TWideStringField
      FieldName = 'Pasta'
      Size = 32
    end
    object QrDirWebNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrDirWebLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrDirWebDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDirWebDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDirWebUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDirWebUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrDirWebAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrDirWebAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrDirWebNOMENIVEL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMENIVEL'
      Calculated = True
    end
  end
  object DsDirWeb: TDataSource
    DataSet = QrDirWeb
    Left = 608
    Top = 8
  end
  object IdFTP1: TIdFTP
    AutoLogin = True
    ProxySettings.ProxyType = fpcmNone
    ProxySettings.Port = 0
    Left = 544
    Top = 144
  end
  object QrPesqUploads: TmySQLQuery
    Database = Dmod.MyDBn
    OnCalcFields = QrDirWebCalcFields
    SQL.Strings = (
      'SELECT upl.*, dir.Codigo DIRCODIGO'
      'FROM uploads upl'
      'LEFT JOIN dirweb dir ON dir.Codigo = upl.DirWeb'
      'WHERE dir.Codigo=:P0')
    Left = 516
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqUploadsCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrPesqUploadsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 32
    end
    object QrPesqUploadsArquivo: TWideStringField
      FieldName = 'Arquivo'
      Required = True
      Size = 32
    end
    object QrPesqUploadsCond: TIntegerField
      FieldName = 'Cond'
      Required = True
    end
    object QrPesqUploadsLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPesqUploadsDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPesqUploadsDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPesqUploadsUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPesqUploadsUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPesqUploadsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPesqUploadsAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPesqUploadsDirWeb: TIntegerField
      FieldName = 'DirWeb'
      Required = True
    end
    object QrPesqUploadsDIRCODIGO: TAutoIncField
      FieldName = 'DIRCODIGO'
    end
  end
  object DsPesqUploads: TDataSource
    DataSet = QrPesqUploads
    Left = 544
    Top = 8
  end
end
