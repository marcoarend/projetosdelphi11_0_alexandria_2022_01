// CAD-CONDO-014 :: Bloquetos em Aberto de Grupos de UHs
New(FRJanelas);
FRJanelas.ID        := 'CAD-CONDO-014';
FRJanelas.Nome      := 'FmCondGriOpen';
FRJanelas.Descricao := 'Bloquetos em Aberto de Grupos de UHs';
FLJanelas.Add(FRJanelas);
//

unit CondGriOpen;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, Grids, DBGrids, UnInternalConsts,
  dmkImage, UnDmkEnums;

type
  TFmCondGriOpen = class(TForm)
    Panel1: TPanel;
    QrCondGri: TmySQLQuery;
    Panel3: TPanel;
    EdCondGri: TdmkEditCB;
    CBCondGri: TdmkDBLookupComboBox;
    Label1: TLabel;
    DsCondGri: TDataSource;
    QrCondGriIts: TmySQLQuery;
    DsCondGriIts: TDataSource;
    DBGrid1: TDBGrid;
    DBGrid2: TDBGrid;
    QrCondGriItsCliInt: TIntegerField;
    QrCondGriItsDepto: TIntegerField;
    QrCondGriItsForneceI: TIntegerField;
    QrAbertos: TmySQLQuery;
    DsAbertos: TDataSource;
    QrCondGriCodigo: TIntegerField;
    QrCondGriNome: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdCondGriChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCondGriOpen: TFmCondGriOpen;

implementation

uses Module, UnMyObjects;

{$R *.DFM}

procedure TFmCondGriOpen.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGriOpen.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGriOpen.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGriOpen.EdCondGriChange(Sender: TObject);
begin
  QrAbertos.Close;
  QrCondGriIts.Close;
end;

procedure TFmCondGriOpen.BtOKClick(Sender: TObject);
{
var
  Txt, LigE, LigOu: String;
}
begin
  Geral.MensagemBox('Em desenvolvimento! Solicite a DERMATEK sua implementa��o!',
  'Aviso', MB_OK+MB_ICONINFORMATION);
{
  ver como fazer! pode precisar buscar dados de v�rias tabelas de lct9999a
}
{
  QrAbertos.Close;
  QrCondGriIts.Close;
  QrCondGriIts.Params[0].AsInteger := QrCondGriCodigo.Value;
  QrCondGriIts.Open;
  if QrCondGriIts.RecordCount > 0 then
  begin
    QrAbertos.Close;
    QrAbertos.SQL.Clear;
    QrAbertos.SQL.Add('SELECT imv.Unidade, lan.ForneceI,');
    QrAbertos.SQL.Add('lan.Depto, SUM(lan.Credito) Credito,');
    QrAbertos.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
    QrAbertos.SQL.Add('ELSE ent.Nome END NOMEPROPRIET,');
    QrAbertos.SQL.Add('CASE WHEN enb.Tipo=0 THEN enb.RazaoSocial');
    QrAbertos.SQL.Add('ELSE enb.Nome END NOMECOND');
    QrAbertos.SQL.Add('FROM ' + VAR_LCT + ' lan');
    QrAbertos.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrAbertos.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=lan.ForneceI');
    QrAbertos.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=lan.Depto');
    QrAbertos.SQL.Add('LEFT JOIN cond con ON con.Codigo=imv.Codigo');
    QrAbertos.SQL.Add('LEFT JOIN entidades enb ON enb.Codigo=con.Cliente');
    QrAbertos.SQL.Add('WHERE lan.FatID in (600,601)');
    QrAbertos.SQL.Add('AND lan.Reparcel=0');
    QrAbertos.SQL.Add('AND lan.Tipo=2');
    QrAbertos.SQL.Add('AND lan.FatNum>0');
    QrAbertos.SQL.Add('AND lan.Vencimento >= ' + Geral.FDT(Date, 1));
    QrAbertos.SQL.Add('AND lan.Compensado < 2');
    QrAbertos.SQL.Add('AND (');
    //
    LigOu := '';
    while not QrCondGriIts.Eof do
    begin
      Txt := '';
      LigE := '';
      if QrCondGriItsCliInt.Value <> 0 then
      begin
        Txt := Txt + LigE + 'car.ForneceI="' +
          FormatFloat('0', QrCondGriItsCliInt.Value) + '"';
        LigE := ' AND ';
      end;
      if QrCondGriItsDepto.Value <> 0 then
      begin
        Txt := Txt + LigE + 'lan.Depto="' +
          FormatFloat('0', QrCondGriItsDepto.Value) + '"';
        LigE := ' AND ';
      end;
      if QrCondGriItsForneceI.Value <> 0 then
        Txt := Txt + LigE + 'lan.ForneceI="' +
          FormatFloat('0', QrCondGriItsForneceI.Value) + '"';
      //
      if Txt <> '' then
      begin
        QrAbertos.SQL.Add(LigOu + ' (' + Txt + ') ');
        LigOu := ' OR ';
      end;
      QrCondGriIts.Next;
    end;
    //
    QrAbertos.SQL.Add(')');
    QrAbertos.SQL.Add('GROUP BY imv.Unidade');
    QrAbertos.SQL.Add('ORDER BY imv.Unidade');
    QrAbertos.Open;
  end;
}
end;

procedure TFmCondGriOpen.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stPsq;
  QrCondGri.Open;
end;

end.

