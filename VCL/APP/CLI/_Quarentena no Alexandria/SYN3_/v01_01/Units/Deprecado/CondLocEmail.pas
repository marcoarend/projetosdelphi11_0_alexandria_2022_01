{$A8,B-,C+,D+,E-,F-,G+,H+,I+,J-,K-,L+,M-,N+,O+,P+,Q-,R-,S-,T-,U-,V+,W-,X+,Y+,Z1}
{$MINSTACKSIZE $00004000}
{$MAXSTACKSIZE $00100000}
{$IMAGEBASE $00400000}
{$APPTYPE GUI}
{$WARN SYMBOL_DEPRECATED ON}
{$WARN SYMBOL_LIBRARY ON}
{$WARN SYMBOL_PLATFORM ON}
{$WARN SYMBOL_EXPERIMENTAL ON}
{$WARN UNIT_LIBRARY ON}
{$WARN UNIT_PLATFORM ON}
{$WARN UNIT_DEPRECATED ON}
{$WARN UNIT_EXPERIMENTAL ON}
{$WARN HRESULT_COMPAT ON}
{$WARN HIDING_MEMBER ON}
{$WARN HIDDEN_VIRTUAL ON}
{$WARN GARBAGE ON}
{$WARN BOUNDS_ERROR ON}
{$WARN ZERO_NIL_COMPAT ON}
{$WARN STRING_CONST_TRUNCED ON}
{$WARN FOR_LOOP_VAR_VARPAR ON}
{$WARN TYPED_CONST_VARPAR ON}
{$WARN ASG_TO_TYPED_CONST ON}
{$WARN CASE_LABEL_RANGE ON}
{$WARN FOR_VARIABLE ON}
{$WARN CONSTRUCTING_ABSTRACT ON}
{$WARN COMPARISON_FALSE ON}
{$WARN COMPARISON_TRUE ON}
{$WARN COMPARING_SIGNED_UNSIGNED ON}
{$WARN COMBINING_SIGNED_UNSIGNED ON}
{$WARN UNSUPPORTED_CONSTRUCT ON}
{$WARN FILE_OPEN ON}
{$WARN FILE_OPEN_UNITSRC ON}
{$WARN BAD_GLOBAL_SYMBOL ON}
{$WARN DUPLICATE_CTOR_DTOR ON}
{$WARN INVALID_DIRECTIVE ON}
{$WARN PACKAGE_NO_LINK ON}
{$WARN PACKAGED_THREADVAR ON}
{$WARN IMPLICIT_IMPORT ON}
{$WARN HPPEMIT_IGNORED ON}
{$WARN NO_RETVAL ON}
{$WARN USE_BEFORE_DEF ON}
{$WARN FOR_LOOP_VAR_UNDEF ON}
{$WARN UNIT_NAME_MISMATCH ON}
{$WARN NO_CFG_FILE_FOUND ON}
{$WARN IMPLICIT_VARIANTS ON}
{$WARN UNICODE_TO_LOCALE ON}
{$WARN LOCALE_TO_UNICODE ON}
{$WARN IMAGEBASE_MULTIPLE ON}
{$WARN SUSPICIOUS_TYPECAST ON}
{$WARN PRIVATE_PROPACCESSOR ON}
{$WARN UNSAFE_TYPE OFF}
{$WARN UNSAFE_CODE OFF}
{$WARN UNSAFE_CAST OFF}
{$WARN OPTION_TRUNCATED ON}
{$WARN WIDECHAR_REDUCED ON}
{$WARN DUPLICATES_IGNORED ON}
{$WARN UNIT_INIT_SEQ ON}
{$WARN LOCAL_PINVOKE ON}
{$WARN MESSAGE_DIRECTIVE ON}
{$WARN TYPEINFO_IMPLICITLY_ADDED ON}
{$WARN XML_WHITESPACE_NOT_ALLOWED ON}
{$WARN XML_UNKNOWN_ENTITY ON}
{$WARN XML_INVALID_NAME_START ON}
{$WARN XML_INVALID_NAME ON}
{$WARN XML_EXPECTED_CHARACTER ON}
{$WARN XML_CREF_NO_RESOLVE ON}
{$WARN XML_NO_PARM ON}
{$WARN XML_NO_MATCHING_PARM ON}
unit CondLocEmail;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkDBGrid, DmkDAC_PF, UnDmkEnums;

type
  TFmCondLocEmail = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtLocaliza: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    EdEMail: TdmkEdit;
    Label8: TLabel;
    BtPesquisa: TBitBtn;
    dmkDBGrid1: TdmkDBGrid;
    QrEmail: TmySQLQuery;
    DsEmail: TDataSource;
    QrEmailNOMECOND: TWideStringField;
    QrEmailNOMEBLOCO: TWideStringField;
    QrEmailUH: TWideStringField;
    QrEmailCodigo: TIntegerField;
    QrEmailControle: TIntegerField;
    QrEmailConta: TIntegerField;
    QrEmailItem: TIntegerField;
    QrEmailPronome: TWideStringField;
    QrEmailNome: TWideStringField;
    QrEmailEMeio: TWideStringField;
    QrEmailLk: TIntegerField;
    QrEmailDataCad: TDateField;
    QrEmailDataAlt: TDateField;
    QrEmailUserCad: TIntegerField;
    QrEmailUserAlt: TIntegerField;
    QrEmailAlterWeb: TSmallintField;
    QrEmailAtivo: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtLocalizaClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCondLocEmail: TFmCondLocEmail;

implementation

uses UnMyObjects, Module, Cond;

{$R *.DFM}

procedure TFmCondLocEmail.BtLocalizaClick(Sender: TObject);
var
  Codigo, Controle, Conta, Item: Integer;
begin
  try
    Screen.Cursor := crHourGlass;
    //
    Codigo   := QrEmailCodigo.Value;
    Controle := QrEmailControle.Value;
    Conta    := QrEmailConta.Value;
    Item     := QrEmailItem.Value;
    //
    FmCond.LocCod(Codigo, Codigo);
    FmCond.ReopenCondBloco(Controle);
    FmCond.ReopenCondImov(Conta);
    FmCond.ReopenCondEmeios(Item);
    //
    FmCond.PageControl1.ActivePageIndex := 0;
    FmCond.PageControl7.ActivePageIndex := 1;
    FmCond.PageControl2.ActivePageIndex := 8;
  finally
    Screen.Cursor := crDefault;
  end;
  Close;
end;

procedure TFmCondLocEmail.BtPesquisaClick(Sender: TObject);
var
  Email: String;
begin
  Email := EdEMail.ValueVariant;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmail, Dmod.MyDB, [
  'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECOND, ',
  'blo.Descri NOMEBLOCO, imv.Unidade UH, eme.* ',
  'FROM condemeios eme ',
  'LEFT JOIN cond con ON con.Codigo = eme.Codigo ',
  'LEFT JOIN entidades ent ON ent.Codigo  = con.Cliente ',
  'LEFT JOIN condbloco blo ON blo.Controle = eme.Controle ',
  'LEFT JOIN condimov imv ON imv.Conta = eme.Conta ',
  'WHERE EMeio LIKE "%' + Email + '%" ',
  'ORDER BY NOMECOND, NOMEBLOCO, UH, Nome ',
  '']);
end;

procedure TFmCondLocEmail.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondLocEmail.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmCondLocEmail.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCondLocEmail.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
