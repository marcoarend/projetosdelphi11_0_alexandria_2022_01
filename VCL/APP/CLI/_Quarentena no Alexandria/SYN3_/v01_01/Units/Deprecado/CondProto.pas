unit CondProto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, ComCtrls, Variants,
  dmkGeral, dmkImage, dmkEdit, dmkEditCB, dmkDBLookupComboBox, UnDmkEnums;

type
  TFmCondProto = class(TForm)
    PainelDados: TPanel;
    Label1: TLabel;
    CBTarefa: TdmkDBLookupComboBox;
    DsProtocolos: TDataSource;
    EdTarefa: TdmkEditCB;
    QrProtocolos: TmySQLQuery;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel1: TPanel;
    BtConfirma: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    PB1: TProgressBar;
    SpeedButton15: TSpeedButton;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton15Click(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenProtocolos(Cliente: Integer);
  public
    { Public declarations }
    FCliente: Integer;
    FProtocoField: String;
  end;

var
  FmCondProto: TFmCondProto;

implementation

uses UnMyObjects, Module, Entidades, Cond, Principal;

{$R *.DFM}

procedure TFmCondProto.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondProto.BtConfirmaClick(Sender: TObject);
  procedure AlteraProtocoloAtual(Protocolo, Conta: Integer);
  begin
    Screen.Cursor := crHourGlass;
    try
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'condimov', False,
        [FProtocoField], ['Conta'], [Protocolo], [Conta], True);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
var
  i, Conta, Proto: Integer;
begin
  PB1.Position := 0;
  Conta        := FmCond.QrCondImovConta.Value;
  Proto        := StrToInt(EdTarefa.Text);
  //
  if CBTarefa.KeyValue = NULL then
  begin
    Geral.MB_Aviso('Informe uma tarefa!');
    EdTarefa.SetFocus;
    Exit;
  end;
  if FmCond.DBGUnidades.SelectedRows.Count > 1 then
  begin
    if Geral.MB_Pergunta('Confirma a altera��o dos itens selecionados?') = ID_YES then
    begin
      PB1.Max := FmCond.DBGUnidades.SelectedRows.Count;
      //
      with FmCond.DBGUnidades.DataSource.DataSet do
      begin
        for i := 0 to FmCond.DBGUnidades.SelectedRows.Count-1 do
        begin
          PB1.Position := PB1.Position + 1;
          GotoBookmark(pointer(FmCond.DBGUnidades.SelectedRows.Items[i]));
          AlteraProtocoloAtual(Proto, FmCond.QrCondImovConta.Value);
        end;
      end;
    end;
  end else
    AlteraProtocoloAtual(Proto, FmCond.QrCondImovConta.Value);
  //
  FmCond.ReopenCondImov(Conta);
  Close;
end;

procedure TFmCondProto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondProto.FormShow(Sender: TObject);
begin
  ReopenProtocolos(FCliente);
end;

procedure TFmCondProto.ReopenProtocolos(Cliente: Integer);
begin
  QrProtocolos.Close;
  QrProtocolos.Params[0].AsInteger := Cliente;
  UMyMod.AbreQuery(QrProtocolos, Dmod.MyDB, 'TFmCondProto.ReopenProtocolos');
end;

procedure TFmCondProto.SpeedButton15Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.CadastroDeProtocolos(0, EdTarefa.ValueVariant);
  //
  if VAR_CADASTRO > 0 then
  begin
    QrProtocolos.Close;
    QrProtocolos.Open;
    //
    EdTarefa.ValueVariant := VAR_CADASTRO;
    CBTarefa.KeyValue     := VAR_CADASTRO;
    EdTarefa.SetFocus;
  end;
end;

procedure TFmCondProto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

end.
