object FmCondSel: TFmCondSel
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-000 :: Sele'#231#227'o de Condom'#237'nio'
  ClientHeight = 569
  ClientWidth = 784
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 145
    Width = 784
    Height = 424
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object dmkDBGrid1: TdmkDBGrid
      Left = 0
      Top = 0
      Width = 784
      Height = 256
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Filial'
          Title.Caption = 'Condom'#237'nio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Entidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFILIAL'
          Title.Caption = 'Raz'#227'o Social'
          Width = 615
          Visible = True
        end>
      Color = clWindow
      DataSource = DModG.DsEmpresas
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnDblClick = dmkDBGrid1DblClick
      Columns = <
        item
          Expanded = False
          FieldName = 'Filial'
          Title.Caption = 'Condom'#237'nio'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Entidade'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEFILIAL'
          Title.Caption = 'Raz'#227'o Social'
          Width = 615
          Visible = True
        end>
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 354
      Width = 784
      Height = 70
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 780
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 636
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtSaida: TBitBtn
            Tag = 13
            Left = 2
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Desiste'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtOK: TBitBtn
          Tag = 14
          Left = 20
          Top = 4
          Width = 120
          Height = 40
          Caption = '&OK'
          NumGlyphs = 2
          TabOrder = 0
          OnClick = BtOKClick
        end
      end
    end
    object GBAvisos1: TGroupBox
      Left = 0
      Top = 310
      Width = 784
      Height = 44
      Align = alBottom
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 780
        Height = 27
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 120
          Height = 16
          Caption = '..............................'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
    object RGAtivo: TdmkRadioGroup
      Left = 0
      Top = 256
      Width = 784
      Height = 54
      Align = alBottom
      Caption = 'Mostrar apenas condom'#237'nios'
      Columns = 3
      ItemIndex = 0
      Items.Strings = (
        'Ambos'
        'Ativos'
        'Inativos')
      TabOrder = 1
      OnClick = RGAtivoClick
      UpdType = utYes
      OldValor = 0
    end
  end
  object GroupBox1: TGroupBox
    Left = 0
    Top = 48
    Width = 784
    Height = 49
    Align = alTop
    Caption = ' Cliente (Condom'#237'nio): '
    TabOrder = 0
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 32
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object EdEmpresa: TdmkEditCB
        Left = 8
        Top = 4
        Width = 65
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 76
        Top = 4
        Width = 697
        Height = 21
        KeyField = 'Filial'
        ListField = 'NOMEFILIAL'
        ListSource = DModG.DsEmpresas
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object GroupBox2: TGroupBox
    Left = 0
    Top = 97
    Width = 784
    Height = 48
    Align = alTop
    Caption = ' Pesquisa por descri'#231#227'o parcial: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 780
      Height = 31
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object EdNome: TdmkEdit
        Left = 8
        Top = 4
        Width = 765
        Height = 21
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
        OnChange = EdNomeChange
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 784
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 736
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 688
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 288
        Height = 32
        Caption = 'Sele'#231#227'o de Condom'#237'nio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 288
        Height = 32
        Caption = 'Sele'#231#227'o de Condom'#237'nio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 288
        Height = 32
        Caption = 'Sele'#231#227'o de Condom'#237'nio'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
end
