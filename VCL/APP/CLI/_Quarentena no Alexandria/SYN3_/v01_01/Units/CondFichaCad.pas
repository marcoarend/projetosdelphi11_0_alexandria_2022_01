unit CondFichaCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, frxClass, frxDBSet, dmkGeral, dmkImage, UnDmkEnums;

type
  TFmCondFichaCad = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    Panel4: TPanel;
    RGFormato: TRadioGroup;
    QrCondImov: TmySQLQuery;
    frxModelo1: TfrxReport;
    frxDsCondImov: TfrxDBDataset;
    QrPropriet: TmySQLQuery;
    frxDsPropriet: TfrxDBDataset;
    QrProprietIE_RG: TWideStringField;
    QrProprietCNPJ_CPF: TWideStringField;
    QrCondImovDescri: TWideStringField;
    QrCondImovSomaFracao: TFloatField;
    QrCondImovPrefixoUH: TWideStringField;
    QrCondImovNOMECONJUGE: TWideStringField;
    QrCondImovSTATUS1: TWideStringField;
    QrCondImovNOMEPROP: TWideStringField;
    QrCondImovNOMEUSUARIO: TWideStringField;
    QrCondImovNOMEPROCURADOR: TWideStringField;
    QrCondImovNOMEEMP1: TWideStringField;
    QrCondImovNOMEEMP2: TWideStringField;
    QrCondImovNOMEEMP3: TWideStringField;
    QrCondImovIMOB: TWideStringField;
    QrCondImovPropriet: TIntegerField;
    QrCondImovConjuge: TIntegerField;
    QrCondImovProcurador: TIntegerField;
    QrCondImovUsuario: TIntegerField;
    QrProprietNATAL: TDateField;
    QrProprietENatal: TDateField;
    QrProprietPNatal: TDateField;
    QrProprietTipo: TSmallintField;
    QrCondImovAndar: TIntegerField;
    QrCondImovUnidade: TWideStringField;
    QrProprietProfissao: TWideStringField;
    QrProprietEmpresa: TIntegerField;
    QrEntidade: TmySQLQuery;
    QrEntidadeNOME_ENT: TWideStringField;
    QrProprietTE1: TWideStringField;
    QrProprietTE2: TWideStringField;
    QrProprietCELULAR: TWideStringField;
    QrProprietEMAIL: TWideStringField;
    QrProprietNO_ECIVIL: TWideStringField;
    QrProprietConjugeNome: TWideStringField;
    QrMorador: TmySQLQuery;
    QrMoradorTipo: TSmallintField;
    QrMoradorNATAL: TDateField;
    QrMoradorENatal: TDateField;
    QrMoradorPNatal: TDateField;
    QrMoradorIE_RG: TWideStringField;
    QrMoradorCNPJ_CPF: TWideStringField;
    QrMoradorProfissao: TWideStringField;
    QrMoradorEmpresa: TIntegerField;
    QrMoradorTE1: TWideStringField;
    QrMoradorTE2: TWideStringField;
    QrMoradorCELULAR: TWideStringField;
    QrMoradorEMAIL: TWideStringField;
    QrMoradorNO_ECIVIL: TWideStringField;
    QrMoradorConjugeNome: TWideStringField;
    frxDsMorador: TfrxDBDataset;
    QrOutMorad: TmySQLQuery;
    QrCondImovConta: TIntegerField;
    frxDsOutMorad: TfrxDBDataset;
    QrOutMoradTipo: TSmallintField;
    QrOutMoradConta: TIntegerField;
    QrOutMoradNOUTMOR: TWideStringField;
    QrOutMoradDataNasc: TWideStringField;
    QrOutMoradParentesco: TWideStringField;
    QrCondImovESegunda1: TSmallintField;
    QrCondImovETerca1: TSmallintField;
    QrCondImovEQuarta1: TSmallintField;
    QrCondImovEQuinta1: TSmallintField;
    QrCondImovESexta1: TSmallintField;
    QrCondImovESabado1: TSmallintField;
    QrCondImovEDomingo1: TSmallintField;
    QrCondImovSSINI1_TXT: TWideStringField;
    QrCondImovSSSAI1_TXT: TWideStringField;
    QrCondImovSDINI1_TXT: TWideStringField;
    QrCondImovSDSAI1_TXT: TWideStringField;
    QrCondImovESegunda2: TSmallintField;
    QrCondImovETerca2: TSmallintField;
    QrCondImovEQuarta2: TSmallintField;
    QrCondImovEQuinta2: TSmallintField;
    QrCondImovESexta2: TSmallintField;
    QrCondImovESabado2: TSmallintField;
    QrCondImovEDomingo2: TSmallintField;
    QrCondImovESegunda3: TSmallintField;
    QrCondImovETerca3: TSmallintField;
    QrCondImovEQuarta3: TSmallintField;
    QrCondImovEQuinta3: TSmallintField;
    QrCondImovESexta3: TSmallintField;
    QrCondImovESabado3: TSmallintField;
    QrCondImovEDomingo3: TSmallintField;
    QrCondImovSSINI2_TXT: TWideStringField;
    QrCondImovSSSAI2_TXT: TWideStringField;
    QrCondImovSDINI2_TXT: TWideStringField;
    QrCondImovSDSAI2_TXT: TWideStringField;
    QrCondImovSSINI3_TXT: TWideStringField;
    QrCondImovSSSAI3_TXT: TWideStringField;
    QrCondImovSDINI3_TXT: TWideStringField;
    QrCondImovSDSAI3_TXT: TWideStringField;
    QrCondImovEmNome1: TWideStringField;
    QrCondImovEmTel1: TWideStringField;
    QrCondImovEmNome2: TWideStringField;
    QrCondImovEmTel2: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel5: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel6: TPanel;
    PnSaiDesis: TPanel;
    BtImprime: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure frxModelo1GetValue(const VarName: string; var Value: Variant);
    procedure QrCondImovAfterScroll(DataSet: TDataSet);
    procedure QrProprietCalcFields(DataSet: TDataSet);
    procedure QrMoradorCalcFields(DataSet: TDataSet);
    procedure QrCondImovCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FCondParent: String;
    //
    function BuscaValorData(Query: TmySQLQuery; Campo: String): String;
    function BuscaValorInteiro(Query: TmySQLQuery; Campo: String): Integer;
    function BuscaValorTexto(Query: TmySQLQuery; Campo: String): String;
  public
    { Public declarations }
  end;

  var
  FmCondFichaCad: TFmCondFichaCad;

implementation

uses Module, ModuleGeral, UCreate, UMySQLModule, UnInternalConsts, UnMyObjects;

{$R *.DFM}

procedure TFmCondFichaCad.BtImprimeClick(Sender: TObject);
const
  Tipo = 1;
  DataNasc = '/      /   ';
var
  I: Integer;
begin
  FCondParent := UCriar.RecriaTempTableNovo(ntrttCondParent, DmodG.QrUpdPID1, False);
  for I := 1 to 3 do
    UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'condparent', False, [
    'Tipo', 'DataNasc'], [], [Tipo, DataNasc], [], False);
  //
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + FCondParent);
  DModG.QrUpdPID1.SQL.Add('SELECT 0 Tipo, con.Conta, ');
  DModG.QrUpdPID1.SQL.Add('ent.Nome NOUTMOR, com.DataNasc, Parentesco');
  DModG.QrUpdPID1.SQL.Add('FROM ' + TMeuDB + '.outmor com');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.condimov con ON con.Conta = com.Conta');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=com.Nome');
  DModG.QrUpdPID1.SQL.Add('WHERE con.Codigo=:P0');
  DModG.QrUpdPID1.Params[0].AsInteger := DModG.QrEmpresasFilial.Value;
  DModG.QrUpdPID1.ExecSQL;
  //
  QrCondImov.Close;
  QrCondImov.Params[0].AsInteger := DModG.QrEmpresasFilial.Value;
  QrCondImov.Open;
  //
  MyObjects.frxMostra(frxModelo1, 'Cadastro de Moradores');
end;

procedure TFmCondFichaCad.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmCondFichaCad.BuscaValorData(Query: TmySQLQuery;
  Campo: String): String;
const
  Padrao = '.      /      /';
begin
  Result := Padrao;
  if Query.State <> dsInactive then
  begin
    if Query.FieldByName(Campo).AsDateTime > 2 then
      Result := Geral.FDT(Query.FieldByName(Campo).AsDateTime, 2);
  end;
end;

function TFmCondFichaCad.BuscaValorInteiro(Query: TmySQLQuery;
  Campo: String): Integer;
begin
  Result := 0;
  if Query.State <> dsInactive then
    Result := Query.FieldByName(Campo).AsInteger;
end;

function TFmCondFichaCad.BuscaValorTexto(Query: TmySQLQuery;
  Campo: String): String;
const
  Padrao = ' ';
begin
  Result := Padrao;
  if Query.State <> dsInactive then
  begin
    if Query.FieldByName(Campo).AsString > '' then
      Result := Query.FieldByName(Campo).AsString;
  end;
end;

procedure TFmCondFichaCad.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondFichaCad.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  QrOutMorad.Close;
  QrOutMorad.Database := DModG.MyPID_DB;
end;

procedure TFmCondFichaCad.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondFichaCad.frxModelo1GetValue(const VarName: string;
  var Value: Variant);
var
  Int1: Integer;
begin
  if Copy(VarName, 1, 14) = 'VARF_PROPRIET_' then
  begin
    if VarName = 'VARF_PROPRIET_NATAL' then
      Value := BuscaValorData(QrPropriet, 'NATAL')
    else
    if VarName = 'VARF_PROPRIET_IERG' then
      Value := BuscaValorTexto(QrPropriet, 'IE_RG')
    else
    if VarName = 'VARF_PROPRIET_CNPJ_CPF' then
      Value := Geral.FormataCNPJ_TT(BuscaValorTexto(QrPropriet, 'CNPJ_CPF'))
    else
    if VarName = 'VARF_PROPRIET_PROFISSAO' then
      Value := Geral.FormataCNPJ_TT(BuscaValorTexto(QrPropriet, 'Profissao'))
    else
    if VarName = 'VARF_PROPRIET_TE1' then
      Value := Geral.FormataTelefone_TT(BuscaValorTexto(QrPropriet, 'TE1'))
    else
    if VarName = 'VARF_PROPRIET_TE2' then
      Value := Geral.FormataTelefone_TT(BuscaValorTexto(QrPropriet, 'TE2'))
    else
    if VarName = 'VARF_PROPRIET_CEL' then
      Value := Geral.FormataTelefone_TT(BuscaValorTexto(QrPropriet, 'CELULAR'))
    else
    if VarName = 'VARF_PROPRIET_EMAIL' then
      Value := BuscaValorTexto(QrPropriet, 'EMAIL')
    else
    if VarName = 'VARF_PROPRIET_CONJUGE' then
      Value := BuscaValorTexto(QrPropriet, 'ConjugeNome')
    else
    if VarName = 'VARF_PROPRIET_ECIVIL' then
      Value := BuscaValorTexto(QrPropriet, 'NO_ECIVIL')
    else
    if VarName = 'VARF_PROPRIET_EMPRESA' then
    begin
      Int1 := BuscaValorInteiro(QrPropriet, 'Empresa');
      if Int1 <> 0 then
      begin
        QrEntidade.Close;
        QrEntidade.Params[0].AsInteger := Int1;
        QrEntidade.Open;
        Value := BuscaValorTexto(QrEntidade, 'NOME_ENT')
      end else Value := '';
    end;
  end else
  if Copy(VarName, 1, 13) = 'VARF_USUARIO_' then
  begin
    if VarName = 'VARF_USUARIO_NATAL' then
      Value := BuscaValorData(QrMorador, 'NATAL')
    else
    if VarName = 'VARF_USUARIO_IERG' then
      Value := BuscaValorTexto(QrMorador, 'IE_RG')
    else
    if VarName = 'VARF_USUARIO_CNPJ_CPF' then
      Value := Geral.FormataCNPJ_TT(BuscaValorTexto(QrMorador, 'CNPJ_CPF'))
    else
    if VarName = 'VARF_USUARIO_PROFISSAO' then
      Value := Geral.FormataCNPJ_TT(BuscaValorTexto(QrMorador, 'Profissao'))
    else
    if VarName = 'VARF_USUARIO_TE1' then
      Value := Geral.FormataTelefone_TT(BuscaValorTexto(QrMorador, 'TE1'))
    else
    if VarName = 'VARF_USUARIO_TE2' then
      Value := Geral.FormataTelefone_TT(BuscaValorTexto(QrMorador, 'TE2'))
    else
    if VarName = 'VARF_USUARIO_CEL' then
      Value := Geral.FormataTelefone_TT(BuscaValorTexto(QrMorador, 'CELULAR'))
    else
    if VarName = 'VARF_USUARIO_EMAIL' then
      Value := BuscaValorTexto(QrMorador, 'EMAIL')
    else
    if VarName = 'VARF_USUARIO_CONJUGE' then
      Value := BuscaValorTexto(QrMorador, 'ConjugeNome')
    else
    if VarName = 'VARF_USUARIO_ECIVIL' then
      Value := BuscaValorTexto(QrMorador, 'NO_ECIVIL')
    else
    if VarName = 'VARF_USUARIO_EMPRESA' then
    begin
      Int1 := BuscaValorInteiro(QrMorador, 'Empresa');
      if Int1 <> 0 then
      begin
        QrEntidade.Close;
        QrEntidade.Params[0].AsInteger := Int1;
        QrEntidade.Open;
        Value := BuscaValorTexto(QrEntidade, 'NOME_ENT')
      end else Value := '';
    end;
  end else
  if VarName = 'VARF_MODELO' then
    Value := RGFormato.Items[RGFormato.ItemIndex]
  else
  if VarName = 'VARF_NOME_EMP' then
    Value := CBEmpresa.Text
end;

procedure TFmCondFichaCad.QrCondImovAfterScroll(DataSet: TDataSet);
begin
  QrPropriet.Close;
  if QrCondImovPropriet.Value <> 0 then
  begin
    QrPropriet.Params[0].AsInteger := QrCondImovPropriet.Value;
    QrPropriet.Open;
  end;
  //
  QrMorador.Close;
  if QrCondImovUsuario.Value <> 0 then
  begin
    QrMorador.Params[0].AsInteger := QrCondImovUsuario.Value;
    QrMorador.Open;
  end;
  //
  QrOutMorad.Close;
  QrOutMorad.Params[0].AsInteger := QrCondImovConta.Value;
  QrOutMorad.Open;
end;

procedure TFmCondFichaCad.QrCondImovCalcFields(DataSet: TDataSet);
begin
  (* N�o usa desabilitado por causa do erro do DAC
  QrCondImovSSINI1_TXT.Value := Geral.FDT(QrCondImovEHoraSSIni1.Value, 102, True);
  QrCondImovSSSAI1_TXT.Value := Geral.FDT(QrCondImovEHoraSSSai1.Value, 102, True);
  QrCondImovSDINI1_TXT.Value := Geral.FDT(QrCondImovEHoraSDIni1.Value, 102, True);
  QrCondImovSDSAI1_TXT.Value := Geral.FDT(QrCondImovEHoraSDSai1.Value, 102, True);
  //
  QrCondImovSSINI2_TXT.Value := Geral.FDT(QrCondImovEHoraSSIni2.Value, 102, True);
  QrCondImovSSSAI2_TXT.Value := Geral.FDT(QrCondImovEHoraSSSai2.Value, 102, True);
  QrCondImovSDINI2_TXT.Value := Geral.FDT(QrCondImovEHoraSDIni2.Value, 102, True);
  QrCondImovSDSAI2_TXT.Value := Geral.FDT(QrCondImovEHoraSDSai2.Value, 102, True);
  //
  QrCondImovSSINI3_TXT.Value := Geral.FDT(QrCondImovEHoraSSIni3.Value, 102, True);
  QrCondImovSSSAI3_TXT.Value := Geral.FDT(QrCondImovEHoraSSSai3.Value, 102, True);
  QrCondImovSDINI3_TXT.Value := Geral.FDT(QrCondImovEHoraSDIni3.Value, 102, True);
  QrCondImovSDSAI3_TXT.Value := Geral.FDT(QrCondImovEHoraSDSai3.Value, 102, True);
  *)
end;

procedure TFmCondFichaCad.QrMoradorCalcFields(DataSet: TDataSet);
begin
  if QrMoradorTipo.Value = 0 then
    QrMoradorNATAL.Value := QrMoradorENatal.Value
  else
    QrMoradorNATAL.Value := QrMoradorPNatal.Value;
end;

procedure TFmCondFichaCad.QrProprietCalcFields(DataSet: TDataSet);
begin
  if QrProprietTipo.Value = 0 then
    QrProprietNATAL.Value := QrProprietENatal.Value
  else
    QrProprietNATAL.Value := QrProprietPNatal.Value;
end;

end.
