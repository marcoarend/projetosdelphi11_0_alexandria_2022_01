object FmFlxMensBalViw: TFmFlxMensBalViw
  Left = 339
  Top = 185
  Caption = 'FLX-BALAN-003 :: Panorama do Fluxo dos Balancetes'
  ClientHeight = 494
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 164
      Height = 48
      Align = alLeft
      TabOrder = 1
      object DBText1: TDBText
        Left = 2
        Top = 15
        Width = 160
        Height = 31
        Align = alClient
        Alignment = taCenter
        DataField = 'ANOMES_TXT'
        DataSource = DsFlxMens
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -24
        Font.Name = 'Arial Black'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitWidth = 161
        ExplicitHeight = 28
      end
    end
    object GB_M: TGroupBox
      Left = 164
      Top = 0
      Width = 796
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 429
        Height = 32
        Caption = 'Panorama do Fluxo dos Balancetes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 429
        Height = 32
        Caption = 'Panorama do Fluxo dos Balancetes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 429
        Height = 32
        Caption = 'Panorama do Fluxo dos Balancetes'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 338
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 338
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 338
        Align = alClient
        TabOrder = 0
        object Panel1: TPanel
          Left = 2
          Top = 258
          Width = 1004
          Height = 78
          Align = alBottom
          ParentBackground = False
          TabOrder = 0
          object Label1: TLabel
            Left = 332
            Top = 8
            Width = 63
            Height = 13
            Caption = 'Documentos:'
            FocusControl = DBEdit1
          end
          object Label2: TLabel
            Left = 668
            Top = 8
            Width = 73
            Height = 13
            Caption = 'INSS/Contab..:'
            FocusControl = DBEdit2
          end
          object Label3: TLabel
            Left = 8
            Top = 32
            Width = 37
            Height = 13
            Caption = 'An'#225'lise:'
            FocusControl = DBEdit3
          end
          object Label4: TLabel
            Left = 332
            Top = 32
            Width = 73
            Height = 13
            Caption = 'Encaderna'#231#227'o:'
            FocusControl = DBEdit4
          end
          object Label5: TLabel
            Left = 668
            Top = 32
            Width = 40
            Height = 13
            Caption = 'Entrega:'
            FocusControl = DBEdit5
          end
          object Label11: TLabel
            Left = 8
            Top = 8
            Width = 58
            Height = 13
            Caption = 'Concilia'#231#227'o:'
            FocusControl = DBEdit11
          end
          object Label6: TLabel
            Left = 8
            Top = 55
            Width = 26
            Height = 13
            Caption = 'Web:'
            FocusControl = DBEdit6
          end
          object DBEdit1: TDBEdit
            Left = 408
            Top = 4
            Width = 248
            Height = 21
            DataField = 'DOCUM_DHX'
            DataSource = DsFlxMens
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 744
            Top = 4
            Width = 248
            Height = 21
            DataField = 'CONTB_DHX'
            DataSource = DsFlxMens
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 72
            Top = 28
            Width = 248
            Height = 21
            DataField = 'ANALI_DHX'
            DataSource = DsFlxMens
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 408
            Top = 28
            Width = 248
            Height = 21
            DataField = 'ENCAD_DHX'
            DataSource = DsFlxMens
            TabOrder = 3
          end
          object DBEdit5: TDBEdit
            Left = 744
            Top = 28
            Width = 248
            Height = 21
            DataField = 'ENTRG_DHX'
            DataSource = DsFlxMens
            TabOrder = 4
          end
          object DBEdit11: TDBEdit
            Left = 72
            Top = 4
            Width = 248
            Height = 21
            DataField = 'CONCI_DHX'
            DataSource = DsFlxMens
            TabOrder = 5
          end
          object DBEdit6: TDBEdit
            Left = 72
            Top = 53
            Width = 248
            Height = 21
            DataField = 'WEB01_DHX'
            DataSource = DsFlxMens
            TabOrder = 6
          end
        end
        object DBGFlxMens: TDBGrid
          Left = 2
          Top = 15
          Width = 1004
          Height = 243
          Align = alClient
          DataSource = DsFlxMens
          TabOrder = 1
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGFlxMensCellClick
          OnDrawColumnCell = DBGFlxMensDrawColumnCell
          OnDblClick = DBGFlxMensDblClick
          OnTitleClick = DBGFlxMensTitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conci_Situ'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Docum_Situ'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Contb_Situ'
              Title.Caption = 'INSS/Contabilid.'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Anali_Situ'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Encad_Situ'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Entrg_Situ'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Web01_Situ'
              Width = 44
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Protocolo'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RazaoSocial'
              Width = 260
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Conci_DLim_TXT'
              Title.Caption = 'Conc.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Docum_DLim_TXT'
              Title.Caption = 'Doc.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Contb_DLim_TXT'
              Title.Caption = 'Cont.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Anali_DLim_TXT'
              Title.Caption = 'Anali.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Encad_DLim_TXT'
              Title.Caption = 'Encad.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Entrg_DLim_TXT'
              Title.Caption = 'Entre.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Web01_DLim_TXT'
              Title.Caption = 'Web'
              Width = 36
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 386
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 749
        Height = 16
        Caption = 
          'Para informar o n'#250'mero do protocolo d'#234' um duplo clique na c'#233'lula' +
          ' correspondente ao condom'#237'nio desejado na coluna "Protocolo".'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 749
        Height = 16
        Caption = 
          'Para informar o n'#250'mero do protocolo d'#234' um duplo clique na c'#233'lula' +
          ' correspondente ao condom'#237'nio desejado na coluna "Protocolo".'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBCntrl: TGroupBox
    Left = 0
    Top = 430
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 239
      Height = 47
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 168
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
      object BitBtn5: TBitBtn
        Tag = 310
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BitBtn5Click
      end
    end
    object LaRegistro: TStaticText
      Left = 241
      Top = 15
      Width = 30
      Height = 17
      Align = alClient
      BevelInner = bvLowered
      BevelKind = bkFlat
      Caption = '[N]: 0'
      TabOrder = 2
    end
    object Panel6: TPanel
      Left = 472
      Top = 15
      Width = 534
      Height = 47
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object BtExclui: TBitBtn
        Tag = 12
        Left = 188
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtExcluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 96
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object Panel7: TPanel
        Left = 425
        Top = 0
        Width = 109
        Height = 47
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvNone
        TabOrder = 3
        object BtSaida: TBitBtn
          Tag = 13
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BitBtn1: TBitBtn
        Tag = 10034
        Left = 280
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Avan'#231'a'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BitBtn1Click
      end
      object BtDesfazOrdenacao: TBitBtn
        Tag = 329
        Left = 372
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtDesfazOrdenacaoClick
      end
    end
  end
  object QrFlxMens: TMySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrFlxMensBeforeClose
    AfterScroll = QrFlxMensAfterScroll
    OnCalcFields = QrFlxMensCalcFields
    SQL.Strings = (
      'SELECT'
      'IF(Conci_DLim<2, 1, IF(Conci_Data=0, 0, 2)) Conci_Situ,'
      'IF(Docum_DLim<2, 1, IF(Docum_Data=0, 0, 2)) Docum_Situ,'
      'IF(Contb_DLim<2, 1, IF(Contb_Data=0, 0, 2)) Contb_Situ,'
      'IF(Anali_DLim<2, 1, IF(Anali_Data=0, 0, 2)) Anali_Situ,'
      'IF(Encad_DLim<2, 1, IF(Encad_Data=0, 0, 2)) Encad_Situ,'
      'IF(Entrg_DLim<2, 1, IF(Entrg_Data=0, 0, 2)) Entrg_Situ,'
      'IF(Web01_DLim<2, 1, IF(Web01_Data=0, 0, 2)) Web01_Situ,'
      's01.Login L01, s02.Login L02, s03.Login L03, '
      's04.Login L04, s05.Login L05, s06.Login L06, '
      's07.Login L07, ent.RazaoSocial, flx.*'
      'FROM flxmens flx'
      'LEFT JOIN cond cnd ON cnd.Codigo=flx.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      'LEFT JOIN senhas s01 ON s01.Numero=flx.Conci_User'
      'LEFT JOIN senhas s02 ON s02.Numero=flx.Docum_User'
      'LEFT JOIN senhas s03 ON s03.Numero=flx.Contb_User'
      'LEFT JOIN senhas s04 ON s04.Numero=flx.Anali_User'
      'LEFT JOIN senhas s05 ON s05.Numero=flx.Encad_User'
      'LEFT JOIN senhas s06 ON s06.Numero=flx.Entrg_User'
      'LEFT JOIN senhas s07 ON s07.Numero=flx.Web01_User'
      ''
      ''
      ''
      ''
      'WHERE flx.AnoMes=:P0'
      'ORDER BY Ordem')
    Left = 112
    Top = 176
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFlxMensRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Size = 100
    end
    object QrFlxMensAnoMes: TIntegerField
      FieldName = 'AnoMes'
    end
    object QrFlxMensCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrFlxMensOrdem: TIntegerField
      FieldName = 'Ordem'
    end
    object QrFlxMensProtocolo: TIntegerField
      FieldName = 'Protocolo'
      DisplayFormat = '000000;-000000; '
    end
    object QrFlxMensObserv: TWideStringField
      FieldName = 'Observ'
      Size = 50
    end
    object QrFlxMensAbert_User: TIntegerField
      FieldName = 'Abert_User'
    end
    object QrFlxMensAbert_Data: TDateTimeField
      FieldName = 'Abert_Data'
    end
    object QrFlxMensConci_DLim: TDateField
      FieldName = 'Conci_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxMensConci_User: TIntegerField
      FieldName = 'Conci_User'
    end
    object QrFlxMensConci_Data: TDateTimeField
      FieldName = 'Conci_Data'
    end
    object QrFlxMensDocum_DLim: TDateField
      FieldName = 'Docum_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxMensDocum_User: TIntegerField
      FieldName = 'Docum_User'
    end
    object QrFlxMensDocum_Data: TDateTimeField
      FieldName = 'Docum_Data'
    end
    object QrFlxMensContb_DLim: TDateField
      FieldName = 'Contb_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxMensContb_User: TIntegerField
      FieldName = 'Contb_User'
    end
    object QrFlxMensContb_Data: TDateTimeField
      FieldName = 'Contb_Data'
    end
    object QrFlxMensAnali_DLim: TDateField
      FieldName = 'Anali_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxMensAnali_User: TIntegerField
      FieldName = 'Anali_User'
    end
    object QrFlxMensAnali_Data: TDateTimeField
      FieldName = 'Anali_Data'
    end
    object QrFlxMensEncad_DLim: TDateField
      FieldName = 'Encad_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxMensEncad_User: TIntegerField
      FieldName = 'Encad_User'
    end
    object QrFlxMensEncad_Data: TDateTimeField
      FieldName = 'Encad_Data'
    end
    object QrFlxMensEntrg_DLim: TDateField
      FieldName = 'Entrg_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxMensEntrg_User: TIntegerField
      FieldName = 'Entrg_User'
    end
    object QrFlxMensEntrg_Data: TDateTimeField
      FieldName = 'Entrg_Data'
    end
    object QrFlxMensCONCI_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'CONCI_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxMensDOCUM_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'DOCUM_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxMensCONTB_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'CONTB_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxMensANALI_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'ANALI_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxMensENCAD_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'ENCAD_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxMensENTRG_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'ENTRG_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxMensANOMES_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ANOMES_TXT'
      Size = 7
      Calculated = True
    end
    object QrFlxMensL01: TWideStringField
      FieldName = 'L01'
      Required = True
      Size = 30
    end
    object QrFlxMensL02: TWideStringField
      FieldName = 'L02'
      Required = True
      Size = 30
    end
    object QrFlxMensL03: TWideStringField
      FieldName = 'L03'
      Required = True
      Size = 30
    end
    object QrFlxMensL04: TWideStringField
      FieldName = 'L04'
      Required = True
      Size = 30
    end
    object QrFlxMensL05: TWideStringField
      FieldName = 'L05'
      Required = True
      Size = 30
    end
    object QrFlxMensL06: TWideStringField
      FieldName = 'L06'
      Required = True
      Size = 30
    end
    object QrFlxMensConci_Situ: TLargeintField
      DisplayLabel = 'Concilia'#231#227'o'
      FieldName = 'Conci_Situ'
      Required = True
    end
    object QrFlxMensDocum_Situ: TLargeintField
      DisplayLabel = 'Documentos'
      FieldName = 'Docum_Situ'
      Required = True
    end
    object QrFlxMensContb_Situ: TLargeintField
      DisplayLabel = 'INSS / contabilidade'
      FieldName = 'Contb_Situ'
      Required = True
    end
    object QrFlxMensAnali_Situ: TLargeintField
      DisplayLabel = 'An'#225'lise'
      FieldName = 'Anali_Situ'
      Required = True
    end
    object QrFlxMensEncad_Situ: TLargeintField
      DisplayLabel = 'Encaderna'#231#227'o'
      FieldName = 'Encad_Situ'
      Required = True
    end
    object QrFlxMensEntrg_Situ: TLargeintField
      DisplayLabel = 'Entrega'
      FieldName = 'Entrg_Situ'
      Required = True
    end
    object QrFlxMensConci_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Conci_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxMensDocum_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Docum_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxMensContb_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Contb_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxMensAnali_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Anali_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxMensEncad_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Encad_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxMensEntrg_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Entrg_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxMensWeb01_DLim: TDateField
      FieldName = 'Web01_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxMensWeb01_User: TIntegerField
      FieldName = 'Web01_User'
    end
    object QrFlxMensWeb01_Data: TDateTimeField
      FieldName = 'Web01_Data'
    end
    object QrFlxMensL07: TWideStringField
      FieldName = 'L07'
      Required = True
      Size = 30
    end
    object QrFlxMensWeb01_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Web01_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxMensWeb01_Situ: TLargeintField
      DisplayLabel = 'Web'
      FieldName = 'Web01_Situ'
      Required = True
    end
    object QrFlxMensWEB01_DHX: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'WEB01_DHX'
      Size = 51
      Calculated = True
    end
  end
  object DsFlxMens: TDataSource
    DataSet = QrFlxMens
    Left = 140
    Top = 176
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 168
    Top = 176
  end
end
