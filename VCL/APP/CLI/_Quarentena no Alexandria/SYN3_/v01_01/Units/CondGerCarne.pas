unit CondGerCarne;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkDBGrid,
  dmkDBGridDAC, ComCtrls, frxClass, frxDBSet, UnDmkProcFunc, dmkImage,
  UnDmkEnums;

type
  TFmCondGerCarne = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    SpeedButton1: TSpeedButton;
    QrCondImov0: TmySQLQuery;
    DsCondImov0: TDataSource;
    QrCondImov0Codigo: TIntegerField;
    QrCondImov0Controle: TIntegerField;
    QrCondImov0Conta: TIntegerField;
    QrCondImov0Propriet: TIntegerField;
    QrCondImov0Andar: TIntegerField;
    QrCondImov0Unidade: TWideStringField;
    QrCondImov0Status: TIntegerField;
    QrCondImov0SitImv: TIntegerField;
    QrCondImov0Usuario: TIntegerField;
    QrCondImov0Ativo: TSmallintField;
    QrCondImov0Protocolo: TIntegerField;
    QrCondImov0Moradores: TIntegerField;
    QrCondImov0FracaoIdeal: TFloatField;
    QrCondImov0ModelBloq: TSmallintField;
    QrCondImov0ConfigBol: TIntegerField;
    QrCondImov0BloqEndTip: TSmallintField;
    QrCondImov0BloqEndEnt: TIntegerField;
    QrCondImov0EnderLin1: TWideStringField;
    QrCondImov0EnderLin2: TWideStringField;
    QrCondImov0EnderNome: TWideStringField;
    QrCondImov0Procurador: TIntegerField;
    QrCondImov0NO_PROPRIET: TWideStringField;
    GBIni: TGroupBox;
    Label32: TLabel;
    LaAnoI: TLabel;
    CBMesI: TComboBox;
    CBAnoI: TComboBox;
    GBFim: TGroupBox;
    Label34: TLabel;
    LaAnoF: TLabel;
    CBMesF: TComboBox;
    CBAnoF: TComboBox;
    QrPrev: TmySQLQuery;
    QrBolLei: TmySQLQuery;
    QrBolLeiValor: TFloatField;
    QrBolLeiApto: TIntegerField;
    QrBolLeiBOLAPTO: TWideStringField;
    QrBolLeiBoleto: TFloatField;
    QrBolArr: TmySQLQuery;
    QrBolArrValor: TFloatField;
    QrBolArrApto: TIntegerField;
    QrBolArrBOLAPTO: TWideStringField;
    QrBolArrBoleto: TFloatField;
    QrPPI: TmySQLQuery;
    QrPPIPROTOCOLO: TIntegerField;
    QrPPIDataE: TDateField;
    QrPPIDataD: TDateField;
    QrPPICancelado: TIntegerField;
    QrPPIMotivo: TIntegerField;
    QrPPIID_Cod1: TIntegerField;
    QrPPIID_Cod2: TIntegerField;
    QrPPIID_Cod3: TIntegerField;
    QrPPIID_Cod4: TIntegerField;
    QrPPITAREFA: TWideStringField;
    QrPPIDELIVER: TWideStringField;
    QrPPILOTE: TIntegerField;
    QrPPICliInt: TIntegerField;
    QrPPICliente: TIntegerField;
    QrPPIPeriodo: TIntegerField;
    QrPPIDATAE_TXT: TWideStringField;
    QrPPIDATAD_TXT: TWideStringField;
    QrPPIDef_Sender: TIntegerField;
    QrPPIDocum: TFloatField;
    QrBoletosIts: TmySQLQuery;
    QrBoletosItsTEXTO: TWideStringField;
    QrBoletosItsVALOR: TFloatField;
    QrBoletosItsVencto: TDateField;
    QrBoletosItsTEXTO_IMP: TWideStringField;
    QrBoletosItsMedAnt: TFloatField;
    QrBoletosItsMedAtu: TFloatField;
    QrBoletosItsConsumo: TFloatField;
    QrBoletosItsCasas: TLargeintField;
    QrBoletosItsUnidLei: TWideStringField;
    QrBoletosItsUnidImp: TWideStringField;
    QrBoletosItsUnidFat: TFloatField;
    QrBoletosItsTipo: TLargeintField;
    QrBoletosItsVENCTO_TXT: TWideStringField;
    QrBoletosItsControle: TIntegerField;
    QrBoletosItsLancto: TIntegerField;
    QrBoletosItsGeraTyp: TLargeintField;
    QrBoletosItsGeraFat: TFloatField;
    QrBoletosItsCasRat: TLargeintField;
    QrBoletosItsNaoImpLei: TLargeintField;
    QrBoletos: TmySQLQuery;
    QrBoletosApto: TIntegerField;
    QrBoletosUnidade: TWideStringField;
    QrBoletosSUB_ARR: TFloatField;
    QrBoletosSUB_LEI: TFloatField;
    QrBoletosSUB_TOT: TFloatField;
    QrBoletosVencto: TDateField;
    QrBoletosPropriet: TIntegerField;
    QrBoletosNOMEPROPRIET: TWideStringField;
    QrBoletosBOLAPTO: TWideStringField;
    QrBoletosVENCTO_TXT: TWideStringField;
    QrBoletosUSERNAME: TWideStringField;
    QrBoletosPASSWORD: TWideStringField;
    QrBoletosPWD_WEB: TWideStringField;
    QrBoletosPROTOCOLO: TIntegerField;
    QrBoletosAndar: TIntegerField;
    QrBoletosBoleto: TFloatField;
    QrBoletosBLOQUETO: TFloatField;
    QrBoletosKGT: TLargeintField;
    QrBoletosLOTE_PROTOCO: TIntegerField;
    QrBoletosOrdem: TIntegerField;
    QrBoletosFracaoIdeal: TFloatField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBGridUnid: TdmkDBGridDAC;
    DBGradeS: TdmkDBGrid;
    DsBoletos: TDataSource;
    QrPrevCodigo: TIntegerField;
    QrPrevPeriodo: TIntegerField;
    QrPrevGastos: TFloatField;
    QrPrevLk: TIntegerField;
    QrPrevDataCad: TDateField;
    QrPrevDataAlt: TDateField;
    QrPrevUserCad: TIntegerField;
    QrPrevUserAlt: TIntegerField;
    QrPrevCond: TIntegerField;
    QrPrevAviso01: TWideStringField;
    QrPrevAviso02: TWideStringField;
    QrPrevAviso03: TWideStringField;
    QrPrevAviso04: TWideStringField;
    QrPrevAviso05: TWideStringField;
    QrPrevAviso06: TWideStringField;
    QrPrevAviso07: TWideStringField;
    QrPrevAviso08: TWideStringField;
    QrPrevAviso09: TWideStringField;
    QrPrevAviso10: TWideStringField;
    QrPrevAvisoVerso: TWideStringField;
    QrPrevAlterWeb: TSmallintField;
    QrPrevAtivo: TSmallintField;
    QrPrevAviso11: TWideStringField;
    QrPrevAviso12: TWideStringField;
    QrPrevAviso13: TWideStringField;
    QrPrevAviso14: TWideStringField;
    QrPrevAviso15: TWideStringField;
    QrPrevAviso16: TWideStringField;
    QrPrevAviso17: TWideStringField;
    QrPrevAviso18: TWideStringField;
    QrPrevAviso19: TWideStringField;
    QrPrevAviso20: TWideStringField;
    QrPrevModelBloq: TSmallintField;
    QrPrevConfigBol: TIntegerField;
    QrPrevBalAgrMens: TSmallintField;
    QrPrevCompe: TSmallintField;
    QrPrevEncerrado: TSmallintField;
    QrPrevCondCli: TIntegerField;
    QrPrevCondCod: TIntegerField;
    QrPrevNOMECONFIGBOL: TWideStringField;
    frxDsCarne0: TfrxDBDataset;
    frxCarne_0: TfrxReport;
    CkZerado: TCheckBox;
    QrUsers: TmySQLQuery;
    QrUsersCodigoEnt: TIntegerField;
    QrUsersCodigoEsp: TIntegerField;
    QrUsersUsername: TWideStringField;
    QrUsersPassword: TWideStringField;
    BtTodoAno: TBitBtn;
    QrCarnes0: TmySQLQuery;
    PnAviso: TPanel;
    QrCarnes0Pagina: TIntegerField;
    QrCarnes0Carne: TIntegerField;
    QrCarnes0Folha: TIntegerField;
    QrCarnes0Vencto: TDateField;
    QrCarnes0SUB_TOT: TFloatField;
    QrCarnes0Boleto: TFloatField;
    QrCarnes0Unidade: TWideStringField;
    QrCarnes0E_ALL: TWideStringField;
    QrCarnes0PROPRI_E_MORADOR: TWideStringField;
    QrCarnes0CODIGOBARRAS: TWideStringField;
    QrCarnes0NossoNumero: TWideStringField;
    QrCarnes0LinhaDigitavel: TWideStringField;
    QrCondImov0Ordem: TIntegerField;
    QrBoletosSEQ: TIntegerField;
    QrFPC: TmySQLQuery;
    QrCarnes0Imprimir: TIntegerField;
    QrBol: TmySQLQuery;
    QrBolApto: TIntegerField;
    QrBolUnidade: TWideStringField;
    QrBolVencto: TDateField;
    QrBolPropriet: TIntegerField;
    QrBolNOMEPROPRIET: TWideStringField;
    QrBolBOLAPTO: TWideStringField;
    QrBolVENCTO_TXT: TWideStringField;
    QrBolUSERNAME: TWideStringField;
    QrBolPASSWORD: TWideStringField;
    QrBolPWD_WEB: TWideStringField;
    QrBolPROTOCOLO: TIntegerField;
    QrBolAndar: TIntegerField;
    QrBolBoleto: TFloatField;
    QrBolBLOQUETO: TFloatField;
    QrBolLOTE_PROTOCO: TIntegerField;
    QrBolOrdem: TIntegerField;
    QrBolFracaoIdeal: TFloatField;
    QrBolPrev: TIntegerField;
    QrBolCond: TIntegerField;
    QrLei: TmySQLQuery;
    QrArr: TmySQLQuery;
    QrLeiValor: TFloatField;
    QrArrValor: TFloatField;
    QrBolSUB_TOT: TFloatField;
    QrBolKGT: TIntegerField;
    QrBolPeriodo: TIntegerField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel2: TPanel;
    BtOK: TBitBtn;
    Bt_: TBitBtn;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    Panel5: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    QrCarnes0PWD_WEB: TWideStringField;
    QrBolCNAB_Cfg: TIntegerField;
    QrCNAB_Cfg_B: TmySQLQuery;
    QrCNAB_Cfg_BNOMEBANCO: TWideStringField;
    QrCNAB_Cfg_BLocalPag: TWideStringField;
    QrCNAB_Cfg_BNOMECED: TWideStringField;
    QrCNAB_Cfg_BEspecieDoc: TWideStringField;
    QrCNAB_Cfg_BACEITETIT_TXT: TWideStringField;
    QrCNAB_Cfg_BCART_IMP: TWideStringField;
    QrCNAB_Cfg_BEspecieVal: TWideStringField;
    QrCNAB_Cfg_BCedBanco: TIntegerField;
    QrCNAB_Cfg_BAgContaCed: TWideStringField;
    QrCNAB_Cfg_BCedAgencia: TIntegerField;
    QrCNAB_Cfg_BCedPosto: TIntegerField;
    QrCNAB_Cfg_BCedConta: TWideStringField;
    QrCNAB_Cfg_BCartNum: TWideStringField;
    QrCNAB_Cfg_BIDCobranca: TWideStringField;
    QrCNAB_Cfg_BCodEmprBco: TWideStringField;
    QrCNAB_Cfg_BTipoCobranca: TIntegerField;
    QrCNAB_Cfg_BCNAB: TIntegerField;
    QrCNAB_Cfg_BCtaCooper: TWideStringField;
    QrCNAB_Cfg_BModalCobr: TIntegerField;
    QrCNAB_Cfg_BMultaPerc: TFloatField;
    QrCNAB_Cfg_BJurosPerc: TFloatField;
    QrCNAB_Cfg_BTexto01: TWideStringField;
    QrCNAB_Cfg_BTexto02: TWideStringField;
    QrCNAB_Cfg_BTexto03: TWideStringField;
    QrCNAB_Cfg_BTexto04: TWideStringField;
    QrCNAB_Cfg_BTexto05: TWideStringField;
    QrCNAB_Cfg_BTexto06: TWideStringField;
    QrCNAB_Cfg_BTexto07: TWideStringField;
    QrCNAB_Cfg_BTexto08: TWideStringField;
    QrCNAB_Cfg_BCedDAC_C: TWideStringField;
    QrCNAB_Cfg_BCedDAC_A: TWideStringField;
    QrCNAB_Cfg_BOperCodi: TWideStringField;
    QrCNAB_Cfg_BLayoutRem: TWideStringField;
    QrCNAB_Cfg_BNOMESAC: TWideStringField;
    QrCNAB_Cfg_BCorreio: TWideStringField;
    QrCNAB_Cfg_BCartEmiss: TIntegerField;
    QrCNAB_Cfg_BCedente: TIntegerField;
    QrCNAB_Cfg_BCAR_TIPODOC: TIntegerField;
    QrCNAB_Cfg_BCART_ATIVO: TIntegerField;
    frxDsCNAB_Cfg_B: TfrxDBDataset;
    QrCNAB_Cfg_BDVB: TWideStringField;
    QrCNAB_Cfg_BNosNumFxaU: TFloatField;
    QrCNAB_Cfg_BNosNumFxaI: TFloatField;
    QrCNAB_Cfg_BNosNumFxaF: TFloatField;
    QrCNAB_Cfg_BCodigo: TFloatField;
    QrCNAB_Cfg_BCorresBco: TIntegerField;
    QrCNAB_Cfg_BCorresAge: TIntegerField;
    QrCNAB_Cfg_BCorresCto: TWideStringField;
    QrCNAB_Cfg_BNPrinBc: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdEmpresaEnter(Sender: TObject);
    procedure EdEmpresaExit(Sender: TObject);
    procedure EdEmpresaChange(Sender: TObject);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure Bt_Click(Sender: TObject);
    procedure QrBoletosAfterScroll(DataSet: TDataSet);
    procedure frxCarne_0GetValue(const VarName: string; var Value: Variant);
    procedure QrBoletosBeforeOpen(DataSet: TDataSet);
    procedure QrBoletosCalcFields(DataSet: TDataSet);
    procedure BtTodoAnoClick(Sender: TObject);
    procedure QrCondImov0CalcFields(DataSet: TDataSet);
    procedure BtOKClick(Sender: TObject);
    procedure QrBolAfterScroll(DataSet: TDataSet);
    procedure QrBolBeforeOpen(DataSet: TDataSet);
    procedure QrBolCalcFields(DataSet: TDataSet);
    procedure QrBolBeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    FTabAriA,
    FTabCnsA,
    FTabPriA,
    FTabPrvA,
    //
    FTabLctA,
    FTabLctB,
    FTabLctD,
    //
    FNossoNumero,
    FAptos, FCondImov0, FCarne0: String;
    FSel, FPeriodos: Integer;
    procedure ReopenCondImov0(ReInsere, SelAll: Boolean);
    procedure AtualizaTodos(Status: Integer);
    function ReopenBoletos1(Periodos: Integer): Boolean;
    function ReopenBoletos2(PeriodoI, PeriodoF, Periodos, Cond: Integer): Boolean;
    procedure ReopenPrv(Cond, PeriodoI, PeriodoF: Integer);
    procedure DefineFrx(frx: TfrxReport);
  public
    { Public declarations }
    FCond: Integer;
  end;

  var
  FmCondGerCarne: TFmCondGerCarne;

implementation

uses CondSel, UCreate, ModuleGeral, UnMyObjects, dmkGeral, ModuleCond, UnBancos,
  MyGlyfs, CondGerImpGer, UMySQLModule, Module, ModuleBloq, UnInternalConsts;

{$R *.DFM}

procedure TFmCondGerCarne.AtualizaTodos(Status: Integer);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE ' + FCondImov0 + ' SET Ativo=:P0');
  DModG.QrUpdPID1.Params[00].AsInteger := Status;
  DModG.QrUpdPID1.ExecSQL;
  //
  ReopenCondImov0(False, False);
end;

procedure TFmCondGerCarne.BtTodoAnoClick(Sender: TObject);
var
  Ano: String;
  A, M, D: Word;
begin
  DecodeDate(Date, A, M, D);
  Ano := FormatFloat('0', A);
  if InputQuery('Ano', 'Informe o ano:', Ano) then
  begin
    CBMesI.ItemIndex := 0;
    CBMesF.ItemIndex := 11;
    MyObjects.DefineIndexComboBox(CBAnoI, Ano);
    MyObjects.DefineIndexComboBox(CBAnoF, Ano);
  end;
end;

procedure TFmCondGerCarne.DefineFrx(frx: TfrxReport);
begin
  frxDsCarne0.DataSet       := QrCarnes0;
  DModG.frxDsDono.DataSet   := DModG.QrDono;
  DModG.frxDsMaster.DataSet := DModG.QrMaster;
  DmCond.frxDsCond.DataSet  := DmCond.QrCond;
  frxDsCNAB_Cfg_B.DataSet   := QrCNAB_Cfg_B;
  //
  MyObjects.frxDefineDataSets(frx, [
    frxDsCarne0,
    DModG.frxDsDono,
    DModG.frxDsMaster,
    DmCond.frxDsCond,
    frxDsCNAB_Cfg_B
    ]);
end;

procedure TFmCondGerCarne.BtOKClick(Sender: TObject);
  function PeriodoAnoMes(CBAno, CBMes: TComboBox): Integer;
  var
    Ano, Mes: Word;
  begin
    Mes := CBMes.ItemIndex + 1;
    Ano := Geral.IMV(CBAno.Text);
    Result := ((Ano - 2000) * 12) + Mes;
  end;
  //
  function ObtemPaginaDeItem(Item, I: Integer): Integer;
  begin
    Result := ((Item -1) div 3) + ((Item -1) div FPeriodos) + 1;
  end;
const
  MaxI = High(Integer);
var
  Cond, PeriodoI, PeriodoF: Integer;
begin
  Geral.MB_Aviso('Este recurso foi desativado!' + sLineBreak +
    'Para maiores informa��es entre em contato com a Dermatek!');
  Exit;
  //
  DefineFrx(frxCarne_0);
  //
  Cond     := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Cond=0, EdEmpresa, 'Informe o condom�nio') then Exit;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados 1/2.  ');
  //
  PeriodoI := PeriodoAnoMes(CBAnoI, CBMesI);
  PeriodoF := PeriodoAnoMes(CBAnoF, CBMesF);
  FPeriodos := PeriodoF - PeriodoI + 1;
  //
  ReopenPrv(Cond, PeriodoI, PeriodoF);
  if QrPrev.RecordCount <> FPeriodos then
  begin
    if Geral.MensagemBox('A quantidade de periodos cadastrados (' +
    IntToStr(QrPrev.RecordCount) + ') difere do per�odo selecionado (' +
    IntToStr(FPeriodos) +
    '). Verifique se todos per�odos desejados foram criados!'+ #13#10+#13#10+
    'Deseja continuar assim mesmo?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION)
    <> ID_YES then
    begin
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      Exit;
    end;
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Otimizando impress�o para melhor montagem dos carn�s.  ');
  //
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add(DELETE_FROM + FCarne0);
  DModG.QrUpdPID1.ExecSQL;
  FAptos := '';
  QrCondImov0.DisableControls;
  try
    QrCondImov0.First;
    while not QrCondImov0.Eof do
    begin
      if QrCondImov0Ativo.Value = 1 then
      begin
        FAptos := FAptos + ',' + FormatFloat('0', QrCondImov0Conta.Value);
        //
      end;
      //
      UMyMod.SQLInsUpd(DmodG.QrUpdPID2, stUpd, 'condimov0', False, [
      'Ordem'], ['Conta'], [
      QrCondImov0.RecNo], [QrCondImov0Conta.Value], False);
      //
      QrCondImov0.Next;
    end;
  finally
    QrCondImov0.Close;
    QrCondImov0.Open;
    QrCondImov0.EnableControls;
  end;
  //
  if FAptos = '' then
  begin
    Geral.MensagemBox('Nenhuma unidade foi selecionada!',
      'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    Exit;
  end else FAptos := Copy(FAptos, 2);
  //
  if ReopenBoletos2(PeriodoI, PeriodoF, FPeriodos, Cond) then
  begin
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando relat�rio.');
    //
    MyObjects.frxMostra(frxCarne_0, 'Carn� de bloquetos');
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
  end;
end;

function TFmCondGerCarne.ReopenBoletos2(PeriodoI, PeriodoF, Periodos, Cond: Integer): Boolean;
  function TipoBol: String;
  begin
    Result := '<>';
  end;
  //
  procedure InsereBandasVazias(Banda, Atual, Pagina: Integer);
  var
    I: Integer;
  begin
    I := Atual;
    while I < Pagina do
    begin
      I := I + 1;
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'carne0', False, [
      'Imprimir'], [
      'Pagina', 'Banda'], [
      0], [I, Banda], False);
    end;
  end;
  //
  procedure UpdateFolhas();
  begin
    //se a banda atual tiver quantidade de p�ginas diferente da anterior,
    //complementar as bandas om vazio at� a �ltima p�gina atual
  end;
var
  AvisoB, TabBoletosCarne: String;
  //
  Vencto: TDateTime;
  SUB_TOT, Boleto: Double;
  Unidade, E_ALL, PROPRI_E_MORADOR, CODIGOBARRAS, NossoNumero, LinhaDigitavel: WideString;
  NossoNumero_Rem, PWD_WEB: String;
  Pagina, Carne, Folha: Integer;
  PagB1, PagB2, PagB3, Banda, Apto: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados 2/2.  ');
  //
  TabBoletosCarne := UCriar.RecriaTempTableNovo(ntrttBoletosCarne, DmodG.QrUpdPID1, False);
  //
  DModG.QrUpdPID1.Close;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + TabBoletosCarne + ' ');
  DModG.QrUpdPID1.SQL.Add('SELECT DISTINCT ari.Codigo Prev, prv.Cond,');
  DModG.QrUpdPID1.SQL.Add('cdb.Ordem, cdi.FracaoIdeal, cdi.Andar,');
  DModG.QrUpdPID1.SQL.Add('ari.Boleto, ari.Apto, ');
  DModG.QrUpdPID1.SQL.Add('ari.Propriet, ari.Vencto, cdi.Unidade,');
  DModG.QrUpdPID1.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  DModG.QrUpdPID1.SQL.Add('ELSE ent.Nome END NOMEPROPRIET, prv.Periodo, 0 KGT, ari.CNAB_Cfg ');
  DModG.QrUpdPID1.SQL.Add('FROM ' + FTabAriA + ' ari');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.condimov cdi ON cdi.Conta=ari.Apto');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.condbloco cdb ON cdb.Controle=cdi.Controle');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=ari.Propriet');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + FTabPrvA + ' prv ON prv.Codigo=ari.Codigo');
  DModG.QrUpdPID1.SQL.Add('WHERE prv.Periodo BETWEEN :P0 AND :P1');
  DModG.QrUpdPID1.SQL.Add('AND prv.Cond=:P2');
  DModG.QrUpdPID1.SQL.Add('AND ari.Boleto ' + TipoBol + ' 0');
  DModG.QrUpdPID1.SQL.Add('');
  DModG.QrUpdPID1.SQL.Add('AND cdi.Conta IN (' + FAptos + ')');
  DModG.QrUpdPID1.SQL.Add('');
  DModG.QrUpdPID1.SQL.Add('UNION');
  DModG.QrUpdPID1.SQL.Add('');
  DModG.QrUpdPID1.SQL.Add('SELECT DISTINCT cni.Codigo Prev, cni.Cond,');
  DModG.QrUpdPID1.SQL.Add('cdb.Ordem, cdi.FracaoIdeal, cdi.Andar, cni.Boleto, cni.Apto,');
  DModG.QrUpdPID1.SQL.Add('cni.Propriet, cni.Vencto, cdi.Unidade,');
  DModG.QrUpdPID1.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  DModG.QrUpdPID1.SQL.Add('ELSE ent.Nome END NOMEPROPRIET, cni.Periodo, 0 KGT, cni.CNAB_Cfg ');
  DModG.QrUpdPID1.SQL.Add('FROM ' + FTabCnsA + '  cni');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.condimov  cdi ON cdi.Conta=cni.Apto');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.condbloco cdb ON cdb.Controle=cdi.Controle');
  DModG.QrUpdPID1.SQL.Add('LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=cni.Propriet');
  DModG.QrUpdPID1.SQL.Add('WHERE cni.Cond=:P3');
  DModG.QrUpdPID1.SQL.Add('AND cni.Periodo BETWEEN :P4 AND :P5');
  DModG.QrUpdPID1.SQL.Add('AND cni.Boleto ' + TipoBol + ' 0');
  DModG.QrUpdPID1.SQL.Add('');
  DModG.QrUpdPID1.SQL.Add('AND cdi.Conta IN (' + FAptos + ')');
  DModG.QrUpdPID1.SQL.Add('');
  DModG.QrUpdPID1.SQL.Add('ORDER BY Ordem, Andar, Unidade, Periodo, Boleto');
  //
  DModG.QrUpdPID1.Params[00].AsInteger := PeriodoI;
  DModG.QrUpdPID1.Params[01].AsInteger := PeriodoF;
  DModG.QrUpdPID1.Params[02].AsInteger := Cond;
  DModG.QrUpdPID1.Params[03].AsInteger := Cond;
  DModG.QrUpdPID1.Params[04].AsInteger := PeriodoI;
  DModG.QrUpdPID1.Params[05].AsInteger := PeriodoF;
  DModG.QrUpdPID1.ExecSQL;
  //
  QrBol.Close;
  QrBol.Database := DModG.MyPID_DB;
  QrBol.SQL.Clear;
  QrBol.SQL.Add('SELECT DISTINCT Prev, Cond, Ordem, FracaoIdeal, Andar,');
  QrBol.SQL.Add('Boleto, Apto, Propriet, Vencto, Unidade, NOMEPROPRIET,');
  QrBol.SQL.Add('CONCAT_WS("-", Boleto, Apto) BOLAPTO, Periodo, KGT, CNAB_Cfg ');
  QrBol.SQL.Add('FROM ' + TabBoletosCarne + ' ari');
  QrBol.SQL.Add('GROUP BY Boleto');
  QrBol.SQL.Add('ORDER BY Ordem, Andar, Unidade, Periodo, Boleto');
  QrBol.Open;
  //
  Banda  := 0;
  Carne  := 0;
  Folha  := 0;
  Pagina := 0;
  Apto   := 0;
  PagB1  := 0;
  PagB2  := 0;
  PagB3  := 0;
  QrBol.First;
  while not QrBol.Eof do
  begin
    //
    AvisoB := ' Bloqueto ' + IntToStr(QrBol.RecNo) + ' de ' +
    IntToStr(QrBol.RecordCount) + '.';
    MyObjects.Informa2(LaAviso1, LaAviso2, True, AvisoB);
    //
    UBancos.GeraNossoNumero(
      QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
      QrBolBLOQUETO.Value,
      QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
      QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
      QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString),
      QrBolVencto.Value, QrCNAB_Cfg_B.FieldByName('TipoCobranca').AsInteger,
      QrCNAB_Cfg_B.FieldByName('EspecieDoc').AsString,
      QrCNAB_Cfg_B.FieldByName('CNAB').AsInteger,
      QrCNAB_Cfg_B.FieldByName('CtaCooper').AsString,
      QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString,
      FNossoNumero, NossoNumero_Rem);
    //
    CODIGOBARRAS := UBancos.CodigoDeBarra_BoletoDeCobranca(
                      QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
                      QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
                      QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger,
                      QrCNAB_Cfg_B.FieldByName('CorresAge').AsInteger,
                      QrCNAB_Cfg_B.FieldByName('CedDAC_A').AsString,
                      QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
                      QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
                      QrCNAB_Cfg_B.FieldByName('CedDAC_C').AsString,
                      QrCNAB_Cfg_B.FieldByName('CorresCto').AsString,
                      9, 3, 1, FNossoNumero,
                      QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString,
                      QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
                      QrCNAB_Cfg_B.FieldByName('CART_IMP').AsString,
                      QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
                      QrCNAB_Cfg_B.FieldByName('OperCodi').AsString,
                      QrBolVencto.Value, QrBolSUB_TOT.Value,
                      0, 0, not CkZerado.Checked,
                      QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
                      QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString);
    //
    LinhaDigitavel := UBancos.LinhaDigitavel_BoletoDeCobranca(
                        UBancos.CodigoDeBarra_BoletoDeCobranca(
                          QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
                          QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
                          QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger,
                          QrCNAB_Cfg_B.FieldByName('CorresAge').AsInteger,
                          QrCNAB_Cfg_B.FieldByName('CedDAC_A').AsString,
                          QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
                          QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
                          QrCNAB_Cfg_B.FieldByName('CedDAC_C').AsString,
                          QrCNAB_Cfg_B.FieldByName('CorresCto').AsString,
                          9, 3, 1, FNossoNumero,
                          QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString,
                          QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
                          QrCNAB_Cfg_B.FieldByName('CART_IMP').AsString,
                          QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
                          QrCNAB_Cfg_B.FieldByName('OperCodi').AsString,
                          QrBolVencto.Value, QrBolSUB_TOT.Value,
                          0, 0, not CkZerado.Checked,
                          QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
                          QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString));
    //
    if Apto <> QrBolApto.Value then
    begin
      Apto := QrBolApto.Value;
      Carne := Carne + 1;
      case Banda of
        1: PagB1 := Pagina;
        2: PagB2 := Pagina;
        3: PagB3 := Pagina;
      end;
      Geral.EscolheMenorDe3_Int(PagB1, PagB2, PagB3, Pagina, Banda);
      // Fazer Update de folhas aqui?
      UpdateFolhas();
      //
      Folha := 0;
    end;
    Pagina := Pagina + 1;
    Folha  := Folha + 1;
    Vencto           := QrBolVencto.Value;
    SUB_TOT          := QrBolSUB_TOT.Value;
    Boleto           := QrBolBoleto.Value;
    Unidade          := QrBolUnidade.Value;
    E_ALL            := DmBloq.QrInquilinoE_ALL.Value;
    PROPRI_E_MORADOR := DmBloq.QrInquilinoPROPRI_E_MORADOR.Value;
    NossoNumero      := FNossoNumero;
    PWD_WEB          := QrBolPWD_WEB.Value;
    //
    //
    if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'carne0', False, [
    'Vencto', 'SUB_TOT', 'Boleto',
    'Unidade', 'E_ALL', 'PROPRI_E_MORADOR', 'PWD_WEB',
    'CODIGOBARRAS', 'NossoNumero', 'LinhaDigitavel',
    'Carne', 'Folha', 'Imprimir'], [
    'Pagina', 'Banda'], [
    Vencto, SUB_TOT, Boleto,
    Unidade, E_ALL, PROPRI_E_MORADOR, PWD_WEB,
    CODIGOBARRAS, NossoNumero, LinhaDigitavel,
    Carne, Folha, 1], [
    Pagina, Banda], False) then ;
    //
    QrBol.Next;
  end;
  case Banda of
    1: PagB1 := Pagina;
    2: PagB2 := Pagina;
    3: PagB3 := Pagina;
  end;
  Geral.EscolheMaiorDe3_Int(PagB1, PagB2, PagB3, Pagina, Banda);
  InsereBandasVazias(1, PagB1, Pagina);
  InsereBandasVazias(2, PagB2, Pagina);
  InsereBandasVazias(3, PagB3, Pagina);
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo tabela.');
  //
  QrCarnes0.Close;
  QrCarnes0.Open;
  Result := True;
  Screen.Cursor := crDefault;
end;

procedure TFmCondGerCarne.BtNenhumClick(Sender: TObject);
begin
  AtualizaTodos(0);
end;

procedure TFmCondGerCarne.Bt_Click(Sender: TObject);
  function PeriodoAnoMes(CBAno, CBMes: TComboBox): Integer;
  var
    Ano, Mes: Word;
  begin
    Mes := CBMes.ItemIndex + 1;
    Ano := Geral.IMV(CBAno.Text);
    Result := ((Ano - 2000) * 12) + Mes;
  end;
  //
  function ObtemPaginaDeItem(Item, I: Integer): Integer;
  begin
    Result := ((Item -1) div 3) + ((Item -1) div FPeriodos) + 1;
  end;
const
  MaxI = High(Integer);
var
  I, PeriodoI, PeriodoF, Cond, Pagina, Carne, Folha, Item, Menos,
  Ordem: Integer;
begin
  Menos    := 0;
  Cond     := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Cond=0, EdEmpresa, 'Informe o condom�nio') then Exit;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados 1/2.  ');
  //
  PeriodoI := PeriodoAnoMes(CBAnoI, CBMesI);
  PeriodoF := PeriodoAnoMes(CBAnoF, CBMesF);
  FPeriodos := PeriodoF - PeriodoI + 1;
  //
  ReopenPrv(Cond, PeriodoI, PeriodoF);
  if QrPrev.RecordCount <> FPeriodos then
  begin
    if Geral.MensagemBox('A quantidade de periodos cadastrados (' +
    IntToStr(QrPrev.RecordCount) + ') difere do per�odo selecionado (' +
    IntToStr(FPeriodos) +
    '). Verifique se todos per�odos desejados foram criados!'+ #13#10+#13#10+
    'Deseja continuar assim mesmo?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION)
    <> ID_YES then
    begin
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      Exit;
    end;
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Otimizando impress�o para melhor montagem dos carn�s.  ');
  //
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add(DELETE_FROM + FCarne0);
  DModG.QrUpdPID1.ExecSQL;
  FAptos := '';
  Item   := 0;
  QrCondImov0.DisableControls;
  try
    QrCondImov0.First;
    while not QrCondImov0.Eof do
    begin
      if QrCondImov0Ativo.Value = 1 then
      begin
        FAptos := FAptos + ',' + FormatFloat('0', QrCondImov0Conta.Value);
        //
        Ordem := QrCondImov0.RecNo;
        Carne := Ordem;
        for I := 1 to FPeriodos do
        begin
          Item  := Item + 1;
          //Pagina := ((Item - 1) div 3) + I;
          Pagina := ObtemPaginaDeItem(Item, I);
          Folha  := I;
          UMyMod.SQLInsUpd(DmodG.QrUpdPID2, stIns, 'carne0', False, ['Imprimir'], [
          'Pagina', 'Carne', 'Folha'], [1], [
          Pagina, Carne, Folha], False);
        end;
      end;
      //
      UMyMod.SQLInsUpd(DmodG.QrUpdPID2, stUpd, 'condimov0', False, [
      'Ordem'], ['Conta'], [
      QrCondImov0.RecNo], [QrCondImov0Conta.Value], False);
      //
      QrCondImov0.Next;
    end;
    while Item mod 3 <> 0 do
    begin
      Item  := Item + 1;
      Menos := Menos + 1;
      Carne := MaxI - Menos;
      for I := 1 to FPeriodos do
      begin
        //Pagina := ((Item-1) div 3) + I;
        Pagina := ObtemPaginaDeItem(Item, I);
        Folha  := I;
        UMyMod.SQLInsUpd(DmodG.QrUpdPID2, stIns, 'carne0', False, ['Imprimir'], [
        'Pagina', 'Carne', 'Folha'], [0], [
        Pagina, Carne, Folha], False);
      end;
    end;
  finally
    QrCondImov0.Close;
    QrCondImov0.Open;
    QrCondImov0.EnableControls;
  end;
  //
  if FAptos = '' then
  begin
    Geral.MensagemBox('Nenhuma unidade foi selecionada!',
      'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
    Exit;
  end else FAptos := Copy(FAptos, 2);
  //
  if ReopenBoletos1(FPeriodos) then
  begin
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Gerando relat�rio.');
    //
    MyObjects.frxMostra(frxCarne_0, 'Carn� de bloquetos');
    //
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
    //
  end;
end;

procedure TFmCondGerCarne.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerCarne.BtTodosClick(Sender: TObject);
begin
  AtualizaTodos(1);
end;

procedure TFmCondGerCarne.EdEmpresaChange(Sender: TObject);
var
  CliInt: Integer;
begin
  CliInt := EdEmpresa.ValueVariant;
  if CliInt = 0 then
    Exit;
  //
  FTabAriA := DModG.NomeTab(TMeuDB, ntAri, False, ttA, CliInt);
  FTabCnsA := DModG.NomeTab(TMeuDB, ntCns, False, ttA, CliInt);
  FTabPriA := DModG.NomeTab(TMeuDB, ntPri, False, ttA, CliInt);
  FTabPrvA := DModG.NomeTab(TMeuDB, ntPrv, False, ttA, CliInt);
  //
  FTabLctA := DModG.NomeTab(TMeuDB, ntLct, False, ttA, CliInt);
  FTabLctB := DModG.NomeTab(TMeuDB, ntLct, False, ttB, CliInt);
  FTabLctD := DModG.NomeTab(TMeuDB, ntLct, False, ttD, CliInt);
  //
  if not EdEmpresa.Focused then
  begin
    FSel := EdEmpresa.ValueVariant;
    ReopenCondImov0(True, False);
  end;
end;

procedure TFmCondGerCarne.EdEmpresaEnter(Sender: TObject);
begin
  FSel := EdEmpresa.ValueVariant;
end;

procedure TFmCondGerCarne.EdEmpresaExit(Sender: TObject);
begin
  if EdEmpresa.ValueVariant <> FSel then
  begin
    FSel := EdEmpresa.ValueVariant;
    ReopenCondImov0(True, False);
  end;
end;

procedure TFmCondGerCarne.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if FCond <> 0 then
  begin
    EdEmpresa.ValueVariant := FCond;
    CBEmpresa.KeyValue     := FCond;
    FCond := 0;
    // For�ar reabertura com recria��o
    CBEmpresa.SetFocus;
  end;
end;

procedure TFmCondGerCarne.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  FCond := 0;
  QrCondImov0.Database := DModG.MyPID_DB;
  QrCarnes0.Database   := DModG.MyPID_DB;
  FCondImov0 := UCriar.RecriaTempTable('CondImov0', DmodG.QrUpdPID1, False);
  FCarne0 := UCriar.RecriaTempTable('Carne0', DmodG.QrUpdPID1, False);
  MyObjects.PreencheCBAnoECBMes(CBAnoI, CBMesI, 0);
  MyObjects.PreencheCBAnoECBMes(CBAnoF, CBMesF, 11);
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmCondGerCarne.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerCarne.frxCarne_0GetValue(const VarName: string;
  var Value: Variant);
var
  DVB(*, LocalEData, UH*): String;
  Banco: Integer;
  //ModelBloq: Integer;
begin
  // In�cio da Ficha de compensa��o
  if AnsiCompareText(VarName, 'VAX') = 0 then
  begin
    if (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015) then
    begin
      Banco := QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger;
      //
      DVB := UBancos.DigitoVerificadorCodigoBanco(Banco);
    end else
    begin
      Banco := QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger;
      //
      if QrCNAB_Cfg_B.FieldByName('DVB').AsString <> '?' then
        DVB :=  QrCNAB_Cfg_B.FieldByName('DVB').AsString
      else
        DVB := UBancos.DigitoVerificadorCodigoBanco(Banco);
    end;
    Value := FormatFloat('000', Banco) + '-' + DVB;
  end
  else if AnsiCompareText(VarName, 'VARF_CODIGOBARRAS') = 0 then
    Value := QrCarnes0CODIGOBARRAS.Value
  else if AnsiCompareText(VarName, 'LogoBancoExiste') = 0 then
  begin
    if (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015)
    then
      Value := FmMyGlyfs.LogoBancoExiste(QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger)
    else
      Value := FmMyGlyfs.LogoBancoExiste(QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger);
  end
  else if AnsiCompareText(VarName, 'LogoBancoPath') = 0 then
  begin
    if (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015)
    then
      Value := FmMyGlyfs.CaminhoLogoBanco(QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger)
    else
      Value := FmMyGlyfs.CaminhoLogoBanco(QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger);
  end
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO1') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto01').AsString,
      QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBolSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO2') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto02').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBolSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO3') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto03').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBolSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO4') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto04').AsString,
     QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBolSUB_TOT.Value)
 else if AnsiCompareText(VarName, 'VAR_INSTRUCAO5') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto05').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBolSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO6') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto06').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBolSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO7') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto07').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBolSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO8') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao(QrCNAB_Cfg_B.FieldByName('Texto08').AsString,
    QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat, QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat, QrBolSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VARF_PERIODOS') = 0 then
    Value := FPeriodos
  // Fim Ficha de compensa��o
  else if AnsiCompareText(VarName, 'VARF_URL') = 0 then
    Value := DmodG.QrOpcoesGerl.FieldByName('Web_MyURL').AsString
end;

procedure TFmCondGerCarne.QrBolAfterScroll(DataSet: TDataSet);
begin
  DmBloq.ReopenEnderecoInquilino(QrBolApto.Value);
  DmBloq.ReopenCNAB_Cfg_Cond(QrCNAB_Cfg_B, Dmod.MyDB, EdEmpresa.ValueVariant, QrBolCNAB_Cfg.Value);
end;

procedure TFmCondGerCarne.QrBolBeforeClose(DataSet: TDataSet);
begin
  DmBloq.QrInquilino.Close;
  QrCNAB_Cfg_B.Close;
end;

procedure TFmCondGerCarne.QrBolBeforeOpen(DataSet: TDataSet);
begin
  // Senha de acesso a p�gina web
  QrUsers.Close;
  QrUsers.Params[0].AsInteger := DmCond.QrCondCodigo.Value;
  QrUsers.Open;
end;

procedure TFmCondGerCarne.QrBolCalcFields(DataSet: TDataSet);
begin
  QrArr.Close;
  QrArr.SQL.Clear;
  QrArr.SQL.Add('SELECT SUM(ari.Valor) Valor');
  QrArr.SQL.Add('FROM ' + FTabAriA + ' ari');
  QrArr.SQL.Add('WHERE ari.Codigo=:P0');
  QrArr.SQL.Add('AND ari.Apto=:P1');
  QrArr.SQL.Add('AND ari.Boleto=:P2');
  QrArr.Params[00].AsInteger := QrBolPrev.Value;
  QrArr.Params[01].AsInteger := QrBolApto.Value;
  QrArr.Params[02].AsFloat   := QrBolBoleto.Value;
  QrArr.Open;
  //
  QrLei.Close;
  QrLei.SQL.Clear;
  QrLei.SQL.Add('SELECT SUM(cni.Valor) Valor');
  QrLei.SQL.Add('FROM ' + FTabCnsA + ' cni');
  QrLei.SQL.Add('WHERE cni.Cond=:P0');
  QrLei.SQL.Add('AND cni.Periodo=:P1');
  QrLei.SQL.Add('AND cni.Apto=:P2');
  QrLei.SQL.Add('AND cni.Boleto=:P3');
  QrLei.Params[00].AsInteger := QrPrevCondCod.Value;
  QrLei.Params[01].AsInteger := QrPrevPeriodo.Value;
  QrLei.Params[02].AsInteger := QrBolApto.Value;
  QrLei.Params[03].AsFloat   := QrBolBoleto.Value;
  QrLei.Open;
  //
  QrBolSUB_TOT.Value :=
  QrArrValor.Value +
  QrLeiValor.Value;
  //
  if QrBolBoleto.Value = 0 then
    QrBolBLOQUETO.Value := -1
  else
    QrBolBLOQUETO.Value := QrBolBoleto.Value;
  //
  QrBolVENCTO_TXT.Value := Geral.FDT(QrBolVencto.Value, 3);
  //
  if ( (Trim(QrBolUSERNAME.Value) <> '')
  and (Trim(QrBolPASSWORD.Value) <> '')) then
    QrBolPWD_WEB.Value := 'Login: ' + QrBolUSERNAME.Value +
    '   Senha: ' + QrBolPASSWORD.Value
  else
    QrBolPWD_WEB.Value := '';
end;

procedure TFmCondGerCarne.QrBoletosAfterScroll(DataSet: TDataSet);
begin
  DmBloq.ReopenEnderecoInquilino(QrBoletosApto.Value);
end;

procedure TFmCondGerCarne.QrBoletosBeforeOpen(DataSet: TDataSet);
begin
  // Senha de acesso a p�gina web
  QrUsers.Close;
  QrUsers.Params[0].AsInteger := DmCond.QrCondCodigo.Value;
  QrUsers.Open;
end;

procedure TFmCondGerCarne.QrBoletosCalcFields(DataSet: TDataSet);
begin
  QrBoletosSUB_TOT.Value :=
  QrBoletosSUB_ARR.Value +
  QrBoletosSUB_LEI.Value;
  //
  if QrBoletosBoleto.Value = 0 then
    QrBoletosBLOQUETO.Value := -1
  else
    QrBoletosBLOQUETO.Value := QrBoletosBoleto.Value;
  //
  QrBoletosVENCTO_TXT.Value := Geral.FDT(QrBoletosVencto.Value, 3);
  //
  if ( (Trim(QrBoletosUSERNAME.Value) <> '')
  and (Trim(QrBoletosPASSWORD.Value) <> '')) then
    QrBoletosPWD_WEB.Value := 'Login: ' + QrBoletosUSERNAME.Value +
    '   Senha: ' + QrBoletosPASSWORD.Value
  else QrBoletosPWD_WEB.Value := '';
end;

procedure TFmCondGerCarne.QrCondImov0CalcFields(DataSet: TDataSet);
begin
  //QrCondImov0SEQ.Value := QrCondImov0.RecNo;
end;

procedure TFmCondGerCarne.ReopenCondImov0(ReInsere, SelAll: Boolean);
var
  Ativo: String;
begin
  DmCond.ReopenQrCond(EdEmpresa.ValueVariant);
  //
  QrCondImov0.Close;
  if FSel <> 0 then
  begin
    if ReInsere then
    begin
      if SelAll then Ativo := '1' else Ativo := '0';
      DModG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add('DELETE FROM condimov0');
      DModG.QrUpdPID1.ExecSQL;
      //
      DModG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add('INSERT INTO condimov0');
      DModG.QrUpdPID1.SQL.Add('SELECT Codigo, Controle, Conta, Propriet,');
      DModG.QrUpdPID1.SQL.Add('Andar, Unidade, Status, SitImv, Usuario,');
      DModG.QrUpdPID1.SQL.Add(Ativo + ' Ativo, Protocolo, Moradores, ');
      DModG.QrUpdPID1.SQL.Add('FracaoIdeal, ModelBloq, ConfigBol, BloqEndTip, ');
      DModG.QrUpdPID1.SQL.Add('BloqEndEnt, EnderLin1, EnderLin2, EnderNome, ');
      DModG.QrUpdPID1.SQL.Add('Procurador, 0 Ordem');
      DModG.QrUpdPID1.SQL.Add('FROM syndic.condimov');
      DModG.QrUpdPID1.SQL.Add('WHERE Codigo=:P0');
      DModG.QrUpdPID1.Params[00].AsInteger := FSel;
      DModG.QrUpdPID1.ExecSQL;
    end;
    QrCondImov0.Open;
  end;
  //DBGridUnid.Visible :=
    //(QrCondImov0.State <> dsInactive) and (QrCondImov0.RecordCount > 0);
end;

procedure TFmCondGerCarne.ReopenPrv(Cond, PeriodoI, PeriodoF: Integer);
begin
  QrPrev.Close;
  QrPrev.SQL.Clear;
  QrPrev.SQL.Add('SELECT prv.*, cnd.Cliente CondCli, cnd.Codigo CondCod,');
  QrPrev.SQL.Add('cfb.Nome NOMECONFIGBOL');
  QrPrev.SQL.Add('FROM ' + FTabPrvA + ' prv');
  QrPrev.SQL.Add('LEFT JOIN cond cnd ON cnd.Codigo=prv.Cond');
  QrPrev.SQL.Add('LEFT JOIN configbol cfb ON cfb.Codigo=prv.ConfigBol');
  QrPrev.SQL.Add('WHERE prv.Cond=:P0');
  QrPrev.SQL.Add('AND prv.Periodo BETWEEN :P1 AND :P2');
  QrPrev.SQL.Add('ORDER BY Periodo');
  QrPrev.Params[00].AsInteger := Cond;
  QrPrev.Params[01].AsInteger := PeriodoI;
  QrPrev.Params[02].AsInteger := PeriodoF;
  QrPrev.Open;
end;

procedure TFmCondGerCarne.SpeedButton1Click(Sender: TObject);
var
  Cond: Integer;
begin
  Cond := 0;
  Application.CreateForm(TFmCondSel, FmCondSel);
  case DModG.QrEmpresas.RecordCount of
    0: Geral.MensagemBox('N�o h� condom�nio cadastrado ou liberado!', 'Aviso',
       MB_OK+MB_ICONWARNING);
    1:
    begin
      Cond := DModG.QrEmpresasFilial.Value;//FmCondSel.QrCondCodCliInt.Value;
    end else begin
      Screen.Cursor := crDefault;
      FmCondSel.ShowModal;
      Cond := FmCondSel.FCond;
    end;
  end;
  FmCondSel.Destroy;
  //
  if Cond <> 0 then
  begin
    EdEmpresa.ValueVariant := Cond;
    CBEmpresa.KeyValue     := Cond;
  end;
end;

function TFmCondGerCarne.ReopenBoletos1(Periodos: Integer): Boolean;
  function TipoBol: String;
  begin
    {
    if FmCondGer.PageControl2.ActivePageIndex = 3 then
      Result := '='
    else}
      Result := '<>';
  end;
var
  AvisoP, AvisoB: String;
  //
  Vencto: TDateTime;
  SUB_TOT, Boleto: Double;
  Unidade, E_ALL, PROPRI_E_MORADOR, CODIGOBARRAS, NossoNumero, LinhaDigitavel: WideString;
  NossoNumero_Rem: String;
  Carne, Folha: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Obtendo dados 2/2.  ');
  //
  //
  //SeqBol := 0;
  //
  QrPrev.First;
  while not QrPrev.Eof do
  begin
    //
    AvisoP := 'Gerando periodo ' + IntToStr(QrPrev.RecNo) + ' de ' +
    IntToStr(QrPrev.RecordCount) + '.';
    MyObjects.Informa2(LaAviso1, LaAviso2, True, AvisoP);
    //
    QrBolArr.Close;
    QrBolArr.SQL.Clear;
    QrBolArr.SQL.Add('SELECT ari.Boleto, ari.Apto, SUM(ari.Valor) Valor,');
    QrBolArr.SQL.Add('CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO');
    QrBolArr.SQL.Add('FROM ' + FTabAriA + ' ari');
    QrBolArr.SQL.Add('WHERE ari.Codigo=:P0');
    QrBolArr.SQL.Add('GROUP BY ari.Boleto, ari.Apto');
    QrBolArr.Params[00].AsInteger := QrPrevCodigo.Value;
    QrBolArr.Open;
    //
    QrBolLei.Close;
    QrBolLei.SQL.Clear;
    QrBolLei.SQL.Add('SELECT cni.Boleto, cni.Apto, SUM(cni.Valor) Valor,');
    QrBolLei.SQL.Add('CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO');
    QrBolLei.SQL.Add('FROM ' + FTabCnsA + ' cni');
    QrBolLei.SQL.Add('WHERE cni.Cond=:P0');
    QrBolLei.SQL.Add('AND cni.Periodo=:P1');
    QrBolLei.SQL.Add('GROUP BY cni.Boleto, cni.Apto');
    QrBolLei.Params[00].AsInteger := QrPrevCondCod.Value;
    QrBolLei.Params[01].AsInteger := QrPrevPeriodo.Value;
    QrBolLei.Open;
    //
    QrPPI.Close;
    QrPPI.Params[00].AsInteger := QrPrevCodigo.Value;
    QrPPI.Params[01].AsInteger := QrPrevCondCod.Value;
    QrPPI.Open;
    //
    QrBoletos.Close;
    QrBoletos.SQL.Clear;

    QrBoletos.SQL.Add('SELECT DISTINCT cdb.Ordem, cdi.FracaoIdeal, cdi.Andar, ari.Boleto, ari.Apto,');
    QrBoletos.SQL.Add('ari.Propriet, ari.Vencto, cdi.Unidade,');
    QrBoletos.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
    QrBoletos.SQL.Add('ELSE ent.Nome END NOMEPROPRIET,');
    QrBoletos.SQL.Add('CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO, 0 KGT');
    QrBoletos.SQL.Add('FROM ' + FTabAriA + ' ari');
    QrBoletos.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto');
    QrBoletos.SQL.Add('LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle');
    QrBoletos.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
    QrBoletos.SQL.Add('WHERE ari.Codigo=:P0');
    QrBoletos.SQL.Add('AND ari.Boleto ' + TipoBol + ' 0');
    QrBoletos.SQL.Add('');
    QrBoletos.SQL.Add('AND cdi.Conta IN (' + FAptos + ')');
    QrBoletos.SQL.Add('');
    QrBoletos.SQL.Add('UNION');
    QrBoletos.SQL.Add('');
    QrBoletos.SQL.Add('SELECT DISTINCT cdb.Ordem, cdi.FracaoIdeal, cdi.Andar, cni.Boleto, cni.Apto,');
    QrBoletos.SQL.Add('cni.Propriet, cni.Vencto, cdi.Unidade,');
    QrBoletos.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
    QrBoletos.SQL.Add('ELSE ent.Nome END NOMEPROPRIET,');
    QrBoletos.SQL.Add('CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO, 0 KGT');
    QrBoletos.SQL.Add('FROM ' + FTabCnsA + '  cni');
    QrBoletos.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto');
    QrBoletos.SQL.Add('LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle');
    QrBoletos.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet');
    QrBoletos.SQL.Add('WHERE cni.Cond=:P1');
    QrBoletos.SQL.Add('AND cni.Periodo=:P2');
    QrBoletos.SQL.Add('AND cni.Boleto ' + TipoBol + ' 0');
    QrBoletos.SQL.Add('');
    QrBoletos.SQL.Add('AND cdi.Conta IN (' + FAptos + ')');
    QrBoletos.SQL.Add('');
    QrBoletos.SQL.Add('ORDER BY Ordem, Andar, Unidade, Boleto');
    //
    QrBoletos.Params[00].AsInteger := QrPrevCodigo.Value;
    QrBoletos.Params[01].AsInteger := QrPrevCond.Value;
    QrBoletos.Params[02].AsInteger := QrPrevPeriodo.Value;
    QrBoletos.Open;
    //
    QrBoletos.First;
    while not QrBoletos.Eof do
    begin
      //
      AvisoB := ' Bloqueto ' + IntToStr(QrBoletos.RecNo) + ' de ' +
      IntToStr(QrBoletos.RecordCount) + '.';
      MyObjects.Informa2(LaAviso1, LaAviso2, True, AvisoP + AvisoB);
      //
      UBancos.GeraNossoNumero(
        QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
        QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
        QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
        QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
        QrBoletosBLOQUETO.Value,
        QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
        QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
        QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
        Geral.SoNumero_TT(QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString),
        QrBoletosVencto.Value,
        QrCNAB_Cfg_B.FieldByName('TipoCobranca').AsInteger,
        QrCNAB_Cfg_B.FieldByName('EspecieDoc').AsString,
        QrCNAB_Cfg_B.FieldByName('CNAB').AsInteger,
        QrCNAB_Cfg_B.FieldByName('CtaCooper').AsString,
        QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString,
        FNossoNumero, NossoNumero_Rem);
      //
      CODIGOBARRAS := UBancos.CodigoDeBarra_BoletoDeCobranca(
                        QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
                        QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
                        QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger,
                        QrCNAB_Cfg_B.FieldByName('CorresAge').AsInteger,
                        QrCNAB_Cfg_B.FieldByName('CedDAC_A').AsString,
                        QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
                        QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
                        QrCNAB_Cfg_B.FieldByName('CedDAC_C').AsString,
                        QrCNAB_Cfg_B.FieldByName('CorresCto').AsString,
                        9, 3, 1, FNossoNumero,
                        QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString,
                        QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
                        QrCNAB_Cfg_B.FieldByName('CART_IMP').AsString,
                        QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
                        QrCNAB_Cfg_B.FieldByName('OperCodi').AsString,
                        QrBoletosVencto.Value, QrBoletosSUB_TOT.Value,
                        0, 0, not CkZerado.Checked,
                        QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
                        QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString);
      //
      LinhaDigitavel := UBancos.LinhaDigitavel_BoletoDeCobranca(
                          UBancos.CodigoDeBarra_BoletoDeCobranca(
                            QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
                            QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
                            QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger,
                            QrCNAB_Cfg_B.FieldByName('CorresAge').AsInteger,
                            QrCNAB_Cfg_B.FieldByName('CedDAC_A').AsString,
                            QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
                            QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
                            QrCNAB_Cfg_B.FieldByName('CedDAC_C').AsString,
                            QrCNAB_Cfg_B.FieldByName('CorresCto').AsString,
                            9, 3, 1, FNossoNumero,
                            QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString,
                            QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
                            QrCNAB_Cfg_B.FieldByName('CART_IMP').AsString,
                            QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
                            QrCNAB_Cfg_B.FieldByName('OperCodi').AsString,
                            QrBoletosVencto.Value, QrBoletosSUB_TOT.Value,
                            0, 0, not CkZerado.Checked,
                            QrCNAB_Cfg_B.FieldByName('ModalCobr').AsInteger,
                            QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString));
      //
      Vencto           := QrBoletosVencto.Value;
      SUB_TOT          := QrBoletosSUB_TOT.Value;
      Boleto           := QrBoletosBoleto.Value;
      Unidade          := QrBoletosUnidade.Value;
      E_ALL            := DmBloq.QrInquilinoE_ALL.Value;
      PROPRI_E_MORADOR := DmBloq.QrInquilinoPROPRI_E_MORADOR.Value;
      NossoNumero      := FNossoNumero;
      //
      //SeqBol := SeqBol + 1;
      //Pagina := ((SeqBol + 2) div 3); // 1..n
      Carne  := QrBoletosSEQ.Value; //QrCondImov0Ordem.Value; //  1..n
      Folha  := QrPrev.RecNo;
      if Folha = 0 then Folha := Periodos;
      //
      if UMyMod.SQLInsUpd(DModG.QrUpdPID1, stUpd, 'carne0', False, [
      'Vencto', 'SUB_TOT', 'Boleto',
      'Unidade', 'E_ALL', 'PROPRI_E_MORADOR',
      'CODIGOBARRAS', 'NossoNumero', 'LinhaDigitavel'], [
      'Carne', 'Folha'], [
      Vencto, SUB_TOT, Boleto,
      Unidade, E_ALL, PROPRI_E_MORADOR,
      CODIGOBARRAS, NossoNumero, LinhaDigitavel], [
      Carne, Folha], False) then ;
      //
      QrBoletos.Next;
    end;
    QrPrev.Next;
  end;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Abrindo tabela.');
  //
  QrCarnes0.Close;
  QrCarnes0.Open;
  Result := True;
  Screen.Cursor := crDefault;
end;

{
[frxDsBoletos."Vencto"]')
[frxDsBoletos."SUB_TOT"]')
[frxDsBoletos."Boleto"]')
Unidade: [frxDsBoletos."Unidade"]')

[frxDsInquilino."E_ALL"]')
[frxDsInquilino."PROPRI_E_MORADOR"]')

Expression = '<VARF_CODIGOBARRAS>'
[VARF_NossoNumero]')
[VARF_LINHADIGITAVEL]')

[VARF_AGCodCed]')
[VAR_INSTRUCAO3]')
[VAX]')



[frxDsCond."EspecieDoc"]')
[frxDsCond."Aceite"]')
[frxDsCond."Carteira"]')
[frxDsCond."EspecieVal"]')
[frxDsCond."LocalPag"]')
[frxDsCond."NOMECED_IMP"]')
[frxDsCond."NOMEBANCO"]')
[frxDsCond."NOMESAC_IMP"]')
[frxDsCond."NOMECLI"]  ')
}


{ QrBoletosIts
SELECT ari.Texto TEXTO, ari.Valor VALOR, ari.Vencto, 0 Tipo,
0 MedAnt, 0 MedAtu, 0 Consumo, 0 Casas, '' UnidLei,
'' UnidImp, 1 UnidFat, Controle, Lancto, 0 GeraTyp,
0 GeraFat, 0 CasRat, 0 NaoImpLei
FROM arre its ari
WHERE ari.Boleto=:P0
AND ari.Apto=:P1
AND ari.Codigo=:P2

UNION

SELECT cns.Nome TEXTO, cni.Valor VALOR, cni.Vencto, 1 Tipo,
MedAnt, MedAtu, Consumo, Casas, UnidLei, UnidImp, UnidFat,
Controle, Lancto, cni.GeraTyp, cni.GeraFat, cni.CasRat, cni.NaoImpLei
FROM cons cns
LEFT JOIN cons its  cni ON cni.Codigo=cns.Codigo
WHERE cni.Boleto=:P3
AND cni.Apto=:P4
AND cni.Periodo=:P5

ORDER BY VALOR DESC

}
end.
