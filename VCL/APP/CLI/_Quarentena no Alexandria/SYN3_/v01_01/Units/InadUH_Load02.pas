
// ini Delphi 28 Alexandria
//{$I dmk.inc}
// fim Delphi 28 Alexandria
unit InadUH_Load02;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, DB,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel, dmkImage,
  DBCtrls, ComCtrls, UnInternalConsts, Mask, dmkEdit, mySQLDbTables, UnDmkEnums;

type
  TFmInadUH_Load02 = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    Label27: TLabel;
    SpeedButton8: TSpeedButton;
    LaAviso: TLabel;
    EdArq: TdmkEdit;
    PB1: TProgressBar;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    Grade1: TStringGrid;
    BtAbre: TBitBtn;
    BtCarrega: TBitBtn;
    TabSheet4: TTabSheet;
    GradeOK: TStringGrid;
    EdContaTaxa: TdmkEdit;
    dmkLabel1: TdmkLabel;
    dmkLabel3: TdmkLabel;
    dmkLabel4: TdmkLabel;
    EdCondCod: TdmkEdit;
    EdCartCod: TdmkEdit;
    EdCondNome: TdmkEdit;
    EdCartNome: TdmkEdit;
    QrImovel: TmySQLQuery;
    QrImovelDEPTO: TIntegerField;
    QrImovelPropriet: TIntegerField;
    QrCarteira: TmySQLQuery;
    QrCarteiraTipo: TIntegerField;
    dmkLabel5: TdmkLabel;
    EdLinhaIni: TdmkEdit;
    dmkLabel6: TdmkLabel;
    EdTotl: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtAbreClick(Sender: TObject);
    procedure BtCarregaClick(Sender: TObject);
    procedure EdCartCodChange(Sender: TObject);
    procedure SpeedButton8Click(Sender: TObject);
  private
    { Private declarations }
    //procedure AjustaLargurasColunas(AGrid: TStringGrid);
  public
    { Public declarations }
    FCod_Cond: Integer;
  end;

  var
  FmInadUH_Load02: TFmInadUH_Load02;

implementation

uses UnMyObjects, Module, ModuleGeral, UMySQLModule, DmkDAC_PF, UnFinanceiro,
ModuleCond, UnMLAGeral;

{$R *.DFM}

{
procedure TFmInadUH_Load01.AjustaLargurasColunas(AGrid: TStringGrid);
const
  CharWid = 7.2;
var
  M, I, J, R, L: Integer;
  ColWid: array[0..255] of Integer;
begin
  M := Round(CharWid * 5); // Largura de 5 letras m'edias
  for I := 0 to 255 do ColWid[I] := M;
  for I := 0 to AGrid.RowCount - 1 do
  begin
    for J := 0 to AGrid.ColCount -1 do
    begin
      Application.ProcessMessages;  // lendo linha
      L := Round((Length(AGrid.Cells[J, I]) + 0) * CharWid);
      if L > ColWid[J] then
        ColWid[J] := L;
    end;
  end;
  for J := 0 to AGrid.ColCount -1 do
    AGrid.ColWidths[J] := ColWid[J];
end;
}

procedure TFmInadUH_Load02.BtAbreClick(Sender: TObject);
const
  ColXtra = 3;
  Espacos = '          ';
  //
  TxtConta01 = '1.01.01.001 Quota condominial';
  TxtConta97 = '1.01.01.002 Acordo de taxas condominiais em atraso';

var
  I, J, K, L, M, P, Q, R, Pula: Integer;
  X, Linha, InfoLin, TodaLinha: String;
  Cta_UH, UH, Propriet, CtaPla, Historico, Texto, Msg: String;
  Copia, Num: Boolean;
  //
  ValorDoc, Comptcia, Vencimen, NumerDoc, PagtoDta, PagtoVal: String;
  Qok: Integer;

  //

  Quadra, Lote, Valor, Vencto, Histor: String;
  UHAnt, CUAnt, PPAnt: String;
  Valr, Totl: Double;
begin
  BtCarrega.Enabled := False;
  if MyObjects.FIC((EdContaTaxa.ValueVariant = 0)(*or (EdContaAcordo.ValueVariant = 0)*),
  EdContaTaxa, 'Informe a conta do plano de contas!') then
    Exit;
  //
  MyObjects.LimpaGrade(GradeOK, 1, 1, True);
  MyObjects.Xls_To_StringGrid(Grade1, EdArq.Text, PB1, LaAviso, nil);
//
//
  GradeOK.RowCount := 2;
  GradeOK.ColCount := Grade1.ColCount + ColXtra;
  PB1.Position := 0;
  Cta_UH   := '';
  Propriet := '';
  CtaPla := Geral.FF0(EdContaTaxa.ValueVariant);
  //
  Qok := 1;
  UHAnt := '@#$';
  CUAnt := '';
  PPAnt := '';
  PB1.Position := 0;
  PB1.Max := Grade1.RowCount;
  Totl := 0;
  for I := EdLinhaIni.ValueVariant -1 to Grade1.RowCount - 1 do
  begin
    PB1.Position := PB1.Position +1;
    Quadra       := Grade1.Cells[03, I];
    Lote         := Grade1.Cells[04, I];
    NumerDoc     := Grade1.Cells[02, I];
    Valor        := Grade1.Cells[05, I];
    Vencto       := Grade1.Cells[06, I];
    Histor       := Grade1.Cells[07, I];

    if Valor <> '' then
    begin
      Qok := Qok + 1;
      GradeOK.RowCount := Qok;
      //
      GradeOk.Cells[00, Qok -1] := IntToStr(I + 1);
      Quadra := Geral.FFN(Geral.IMV(Quadra), 2);
      Lote   := Geral.FFN(Geral.IMV(Lote), 2);
      UH := 'Q'+ Quadra[1] + Quadra[2] + '-L' + Lote[1] + Lote[2];

      if UH = UHAnt then
      begin
        Cta_UH   := CUAnt;
        Propriet := PPAnt;
      end else
      begin
        UnDmkDAC_PF.AbreMySQLQuery0(QrImovel, DMod.MyDB, [
        'SELECT Conta DEPTO, Propriet ',
        'FROM condimov ',
        'WHERE Codigo=' + Geral.FF0(EdCondCod.ValueVariant),
        'AND Unidade="' + UH + '" ',
        '']);
        if QrImovel.RecordCount <> 1 then
        begin
          BtCarrega.Enabled := False;
          if QrImovel.RecordCount = 0 then
            Msg := 'N�o foi poss�vel definir a UH pois n�o foi encontrado registro para: "' + UH + '"'
          else
            Msg := 'N�o foi poss�vel definir a UH pois foram encontrados ' +
            Geral.FF0(QrImovel.RecordCount) + ' registros para a UH: "' + UH + '"';
          Geral.MB_Aviso(Msg);
          //
          Screen.Cursor := crDefault;
          Exit;
        end else
        begin
          Cta_UH   := Geral.FF0(QrImovelDEPTO.Value);
          Propriet := Geral.FF0(QrImovelPropriet.Value);
        end;
      end;
      UHAnt := UH;
      CUAnt := Cta_UH;
      PPAnt := Propriet;
      //
      Comptcia := IntToStr(MLAGeral.PeriodoToMez(MLAGeral.DataToPeriodo(
        Geral.Validadatabr(Vencto, False, False))));
      Valr := Geral.DMV(Trim(Valor));
      Totl := Totl + Valr;
      //
      GradeOk.Cells[01, Qok -1] := Cta_UH;
      GradeOk.Cells[02, Qok -1] := UH;
      GradeOk.Cells[03, Qok -1] := CtaPla;
      GradeOk.Cells[04, Qok -1] := Histor;
      GradeOk.Cells[05, Qok -1] := Geral.FFT(Valr, 2, siNegativo);
      GradeOk.Cells[06, Qok -1] := Comptcia;
      GradeOk.Cells[07, Qok -1] := Vencto;
      GradeOk.Cells[08, Qok -1] := NumerDoc;
      GradeOk.Cells[09, Qok -1] := '';
      GradeOk.Cells[10, Qok -1] := '0';
      GradeOk.Cells[11, Qok -1] := Propriet;
    end;
  end;
  PB1.Position := PB1.Max;
  Geral.MB_Info('Total = $ ' + Geral.FFT(Totl, 2, siNegativo));
  EdTotl.ValueVariant := Totl;
  //AjustaLargurasColunas(GradeOK);
  MyObjects.LarguraAutomaticaGrade(GradeOK);
  PB1.Position := 0;
  PageControl1.ActivePageIndex := 1;
  //
  BtCarrega.Enabled := True;
  Geral.MB_Info('Abertura de arquivo finalizada!');
end;

procedure TFmInadUH_Load02.BtCarregaClick(Sender: TObject);
{
const
  SerieNF = '';
  SerieCH = '';
  Fatura  = '';
  Emitente  = '';
  ContaCorrente  = '';
  CNPJCPF  = '';
  Protesto  = '';
  Duplicata  = '';
  Antigo  = '';
  Doc2  = '';
  //
  Autorizacao = 0;
  NotaFiscal = 0;
  FatID_Sub = 0;
  ID_Pgto = 0;
  ID_Quit = 0;
  ID_Sub = 0;
  Banco = 0;
  Agencia = 0;
  Local = 0;
  Cartao = 0;
  Linha = 0;
  OperCount = 0;
  Lancto = 0;
  CtrlIni = 0;
  Nivel = 0;
  Vendedor = 0;
  Account = 0;
  Fornecedor = 0;
  DescoPor = 0;
  DescoControle = 0;
  Unidade = 0;
  ExcelGru = 0;
  CNAB_Sit = 0;
  TipoCH = 0;
  Reparcel = 0;
  Atrelado = 0;
  SubPgto1 = 0;
  MultiPgto = 0;
  Protocolo = 0;
  CtrlQuitPg = 0;
  Endossas = 0;
  Cancelado = 0;
  EventosCad = 0;
  Encerrado = 0;
  ErrCtrl = 0;
  IndiPag = 0;
  FatParcRef = 0;
  FatSit = 0;
  FatSitSub = 0;
  FatGrupo = 0;
  FisicoSrc = 0;
  FisicoCod = 0;
  //
  Qtde = 0;
  Pago = 0;
  MoraVal = 0;
  MultaVal = 0;
  ICMS_P = 0;
  ICMS_V = 0;
  DescoVal = 0;
  NFVal = 0;
  PagMul = 0;
  PagJur = 0;
  Endossan = 0;
  Endossad = 0;
  TaxasVal = 0;
}
var
{
  Data, Descricao, Compensado, Vencimento, DataDoc: String;
  Tipo, Carteira, Controle, Sub, Genero, Mez, Cliente, CliInt, ForneceI, Depto,
  Sit, FatID, FatParcela: Integer;
  Debito, Credito, Documento, MoraDia, Multa, FatNum: Double;
}  //
  //DtPeriodo,
  I, Mes: Integer;
begin
  if Geral.MB_Pergunta('A importa��o ser� feita com base no arquivo a ser importado e pode conter erros!' +
    sLineBreak + 'A Dermatek n�o se responsabiliza pelos dados importados!' + sLineBreak +
    'A importa��o dos dados � de responsabilidade do usu�rio que est� importando o arquivo.' +
    sLineBreak + 'Ap�s a importa��o todos os dados devem ser conferidos! ' +
    'Voc� concorda com os termos acima e deseja realmente realizar a importa��o?') <> ID_YES
  then
    Exit;
  //
  Dmod.ReopenControle;
  //
  PB1.Position := 0;
  PB1.Max := GradeOK.RowCount;
  //
  for I := 1 to GradeOK.RowCount -1 do
  begin
    MyObjects.Informa2EUpdPB(PB1, LaAviso1, LaAviso2, True,
      'Incluindo documento da linha: ' + GradeOK.Cells[00, I]);
    //
    if Trim(GradeOK.Cells[00, I]) <> '' then
    begin

{
        Data           := Geral.FDT(Geral.ValidaDataBR(GradeOK.Cells[07, I], False, False), 1);
        Tipo           := 2;
        Carteira       := QrCarteiratipo.Value;
        Sub            := 0;
        //Autorizacao    := ;
        Genero         := Geral.IMV(Geral.SoNumero_TT(GradeOK.Cells[03, I]));
        //Qtde           := ;
        Descricao      := GradeOK.Cells[04, I];
        //SerieNF        := ;
        //NotaFiscal     := ;
        Debito         := 0;
        Credito        := Geral.DMV(GradeOK.Cells[05, I]);
        Compensado     := '0000-00-00';
        //SerieCH        := ;
        Documento      := Geral.DMV(GradeOK.Cells[08, I]);
        Sit            := 0;
        Vencimento     := Geral.FDT(Geral.ValidaDataBR(GradeOK.Cells[07, I], False, False), 1);
        FatID          := 600;
        //FatID_Sub      := ;
        //FatNum         := ; Definido mais adiante...
        FatParcela     := 1;
        //ID_Pgto        := ;
        //ID_Quit        := ;
        //ID_Sub         := ;
        //Fatura         := ;
        //Emitente       := ;
        //Banco          := ;
        //Agencia        := ;
        //ContaCorrente  := ;
        //CNPJCPF        := ;
        //Local          := ;
        //Cartao         := ;
        //Linha          := ;
        //OperCount      := ;
        //Lancto         := ;
        //Pago           := ;
        //Mez            := ;
        //Fornecedor     := ;
        Cliente        := ;
        CliInt         := ;
        ForneceI       := ;
        MoraDia        := ;
        Multa          := ;
        MoraVal        := ;
        MultaVal       := ;
        Protesto       := ;
        DataDoc        := ;
        CtrlIni        := ;
        Nivel          := ;
        Vendedor       := ;
        Account        := ;
        ICMS_P         := ;
        ICMS_V         := ;
        Duplicata      := ;
        Depto          := ;
        DescoPor       := ;
        DescoVal       := ;
        DescoControle  := ;
        Unidade        := ;
        NFVal          := ;
        Antigo         := ;
        ExcelGru       := ;
        Doc2           := ;
        CNAB_Sit       := ;
        TipoCH         := ;
        Reparcel       := ;
        Atrelado       := ;
        PagMul         := ;
        PagJur         := ;
        SubPgto1       := ;
        MultiPgto      := ;
        Protocolo      := ;
        CtrlQuitPg     := ;
        Endossas       := ;
        Endossan       := ;
        Endossad       := ;
        Cancelado      := ;
        EventosCad     := ;
        Encerrado      := ;
        ErrCtrl        := ;
        IndiPag        := ;
        FatParcRef     := ;
        FatSit         := ;
        FatSitSub      := ;
        FatGrupo       := ;
        TaxasVal       := ;
        FisicoSrc      := ;
        FisicoCod      := ;

        //
        FatNum         := ;
        Controle       := ?;
        ? := UMyMod.BuscaEmLivreY_Def('lct0001a', 'Data', 'Tipo', 'Carteira', 'Controle', 'Sub', ImgTipo.SQLType?, CodAtual?);
        ou > ? := UMyMod.BPGS1I32('lct0001a', 'Data', 'Tipo', 'Carteira', 'Controle', 'Sub', '', '', tsPosNeg?, stInsUpd?, CodAtual?);
        if UMyMod.SQLInsUpd_IGNORE?(Dmod.QrUpd?, ImgTipo.SQLType?, 'lct0001a', auto_increment?[
        'Autorizacao', 'Genero', 'Qtde',
        'Descricao', 'SerieNF', 'NotaFiscal',
        'Debito', 'Credito', 'Compensado',
        'SerieCH', 'Documento', 'Sit',
        'Vencimento', 'FatID', 'FatID_Sub',
        'FatNum', 'FatParcela', 'ID_Pgto',
        'ID_Quit', 'ID_Sub', 'Fatura',
        'Emitente', 'Banco', 'Agencia',
        'ContaCorrente', 'CNPJCPF', 'Local',
        'Cartao', 'Linha', 'OperCount',
        'Lancto', 'Pago', 'Mez',
        'Fornecedor', 'Cliente', 'CliInt',
        'ForneceI', 'MoraDia', 'Multa',
        'MoraVal', 'MultaVal', 'Protesto',
        'DataDoc', 'CtrlIni', 'Nivel',
        'Vendedor', 'Account', 'ICMS_P',
        'ICMS_V', 'Duplicata', 'Depto',
        'DescoPor', 'DescoVal', 'DescoControle',
        'Unidade', 'NFVal', 'Antigo',
        'ExcelGru', 'Doc2', 'CNAB_Sit',
        'TipoCH', 'Reparcel', 'Atrelado',
        'PagMul', 'PagJur', 'SubPgto1',
        'MultiPgto', 'Protocolo', 'CtrlQuitPg',
        'Endossas', 'Endossan', 'Endossad',
        'Cancelado', 'EventosCad', 'Encerrado',
        'ErrCtrl', 'IndiPag', 'FatParcRef',
        'FatSit', 'FatSitSub', 'FatGrupo',
        'TaxasVal', 'FisicoSrc', 'FisicoCod'], [
        'Data', 'Tipo', 'Carteira', 'Controle', 'Sub'], [
        Autorizacao, Genero, Qtde,
        Descricao, SerieNF, NotaFiscal,
        Debito, Credito, Compensado,
        SerieCH, Documento, Sit,
        Vencimento, FatID, FatID_Sub,
        FatNum, FatParcela, ID_Pgto,
        ID_Quit, ID_Sub, Fatura,
        Emitente, Banco, Agencia,
        ContaCorrente, CNPJCPF, Local,
        Cartao, Linha, OperCount,
        Lancto, Pago, Mez,
        Fornecedor, Cliente, CliInt,
        ForneceI, MoraDia, Multa,
        MoraVal, MultaVal, Protesto,
        DataDoc, CtrlIni, Nivel,
        Vendedor, Account, ICMS_P,
        ICMS_V, Duplicata, Depto,
        DescoPor, DescoVal, DescoControle,
        Unidade, NFVal, Antigo,
        ExcelGru, Doc2, CNAB_Sit,
        TipoCH, Reparcel, Atrelado,
        PagMul, PagJur, SubPgto1,
        MultiPgto, Protocolo, CtrlQuitPg,
        Endossas, Endossan, Endossad,
        Cancelado, EventosCad, Encerrado,
        ErrCtrl, IndiPag, FatParcRef,
        FatSit, FatSitSub, FatGrupo,
        TaxasVal, FisicoSrc, FisicoCod], [
        Data, Tipo, Carteira, Controle, Sub], UserDataAlterweb?, IGNORE?
}

(*
      DtPeriodo := MLAGeral.PeriodoToMez(MLAGeral.DataToPeriodo(
        Geral.Validadatabr('1/' + GradeOK.Cells[06, I], False, False)));
*)
      Mes := Geral.IMV(GradeOK.Cells[06, I]) - 1; 
      //
      UFinanceiro.LancamentoDefaultVARS;
      //
      FLAN_Data       := Geral.FDT(Geral.ValidaDataBR(GradeOK.Cells[07, I], False, False), 1);
      FLAN_Tipo       := 2;
      FLAN_Documento  := Geral.DMV(GradeOK.Cells[08, I]);
      FLAN_Credito    := Geral.DMV(GradeOK.Cells[05, I]);
      FLAN_MoraDia    := Dmod.QrControle.FieldByName('MoraDD').AsFloat;
      FLAN_Multa      := Dmod.QrControle.FieldByName('Multa').AsFloat;
      FLAN_Carteira   := EdCartCod.ValueVariant;
      FLAN_Genero     := Geral.IMV(Geral.SoNumero_TT(GradeOK.Cells[03, I]));
      FLAN_Cliente    := Geral.IMV(Geral.SoNumero_TT(GradeOK.Cells[11, I]));
      FLAN_CliInt     := DmCond.QrCondCliente.Value;
      FLAN_Depto      := Geral.IMV(Geral.SoNumero_TT(GradeOK.Cells[01, I]));
      FLAN_ForneceI   := Geral.IMV(Geral.SoNumero_TT(GradeOK.Cells[11, I]));
      FLAN_Vencimento := Geral.FDT(Geral.ValidaDataBR(GradeOK.Cells[07, I], False, False), 1);
      FLAN_Mez        := Mes;
      FLAN_FatID      := 600;
      FLAN_FatNum     := Geral.DMV(GradeOK.Cells[08, I]);
      FLAN_FatParcela := 1;
      FLAN_Descricao  := GradeOK.Cells[04, I];
      FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
        'Controle', DmCond.FTabLctA, LAN_CTOS, 'Controle');
      if UFinanceiro.InsereLancamento(DmCond.FTabLctA) then
      begin
        ////////////////////////////////////////////////////////////////////////
        //   N�o h� upd nos boletos pois n�o existe boleto gerado!            //
        ////////////////////////////////////////////////////////////////////////
      end;
    end;
    //
  end;
  Geral.MB_Info('Documentos inseridos com sucesso!');
  PB1.Position := 0;
  MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmInadUH_Load02.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmInadUH_Load02.EdCartCodChange(Sender: TObject);
begin
  UnDMkDAC_PF.AbreMySQLQuery0(QrCarteira, Dmod.MyDB, [
  'SELECT Tipo ',
  'FROM carteiras ',
  'WHERE Codigo=' + Geral.FF0(EdCartCod.ValueVariant),
  '']);
end;

procedure TFmInadUH_Load02.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmInadUH_Load02.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  PageControl1.ActivePageIndex := 0;
  //
  GradeOK.Cells[00, 00] := 'Linha';
  GradeOK.Cells[01, 00] := 'ID UH';
  GradeOK.Cells[02, 00] := 'Nome UH';
  GradeOK.Cells[03, 00] := 'CtaPla';
  GradeOK.Cells[04, 00] := 'Hist�rico';
  GradeOK.Cells[05, 00] := 'Valor Doc.';
  GradeOK.Cells[06, 00] := 'Compet�ncia';
  GradeOK.Cells[07, 00] := 'Vencimento';
  GradeOK.Cells[08, 00] := 'Num. Doc.';
  GradeOK.Cells[09, 00] := 'Pagto. Data';
  GradeOK.Cells[10, 00] := 'Pagto. Val.';
  GradeOK.Cells[11, 00] := 'Propriet.';
end;

procedure TFmInadUH_Load02.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmInadUH_Load02.SpeedButton8Click(Sender: TObject);
var
  Arq: String;
begin
  if MyObjects.FileOpenDialog(Self, '', '', 'Diret�rio', '', [], Arq) then
    EdArq.Text := Arq;
end;

end.
