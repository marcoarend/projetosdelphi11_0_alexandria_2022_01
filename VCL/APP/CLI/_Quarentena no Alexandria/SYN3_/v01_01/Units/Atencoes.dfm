object FmAtencoes: TFmAtencoes
  Left = 339
  Top = 185
  Caption = 'GER-CONDM-030 :: Informa'#231#245'es que Precisam de Aten'#231#227'o'
  ClientHeight = 481
  ClientWidth = 993
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 47
    Width = 993
    Height = 311
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 0
      Width = 993
      Height = 311
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 0
      object TabSheet1: TTabSheet
        Caption = ' Quita'#231#227'o de Parcelamentos '
        object Splitter1: TSplitter
          Left = 0
          Top = 98
          Width = 985
          Height = 8
          Cursor = crVSplit
          Align = alTop
        end
        object DBGErrRepa: TDBGrid
          Left = 0
          Top = 0
          Width = 985
          Height = 98
          Align = alTop
          DataSource = DmAtencoes.DsErrRepa
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 19
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECOND'
              Title.Caption = 'Condom'#237'nio'
              Width = 144
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPROP'
              Title.Caption = 'Propriet'#225'rio'
              Width = 144
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Data'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Compensado'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Width = 160
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Documento'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Pgto'
              Title.Caption = 'Origem'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MES'
              Title.Caption = 'Compet.'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Reparcel'
              Width = 51
              Visible = True
            end>
        end
        object DBGErrRepaIts: TdmkDBGrid
          Left = 0
          Top = 106
          Width = 985
          Height = 130
          Align = alClient
          Color = clWindow
          DataSource = DmAtencoes.DsErrRepaIts
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
        end
        object Panel5: TPanel
          Left = 0
          Top = 236
          Width = 985
          Height = 47
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 2
          object BtRatifQuit: TBitBtn
            Tag = 10116
            Left = 4
            Top = 4
            Width = 128
            Height = 39
            Caption = 'Ratifica quita'#231#227'o'
            TabOrder = 0
            OnClick = BtRatifQuitClick
          end
          object DBGErrRepaSum: TDBGrid
            Left = 522
            Top = 0
            Width = 463
            Height = 47
            Align = alRight
            DataSource = DmAtencoes.DsErrRepaSum
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Visible = False
          end
          object BtLocPag: TBitBtn
            Tag = 22
            Left = 132
            Top = 4
            Width = 128
            Height = 39
            Caption = 'Localiza parcel.'
            TabOrder = 2
            OnClick = BtLocPagClick
          end
          object BtQuitaOrig: TBitBtn
            Tag = 174
            Left = 260
            Top = 4
            Width = 128
            Height = 39
            Caption = 'Quita originais'
            TabOrder = 3
            Visible = False
            OnClick = BtQuitaOrigClick
          end
          object BtExclLancto: TBitBtn
            Tag = 12
            Left = 388
            Top = 4
            Width = 128
            Height = 39
            Caption = 'Exclui lan'#231'to'
            TabOrder = 4
            Visible = False
            OnClick = BtExclLanctoClick
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Lan'#231'amento sem m'#234's de compet'#234'ncia'
        ImageIndex = 1
        object DBGLct: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 985
          Height = 236
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 41
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatNum'
              Title.Caption = 'Bloqueto'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatID'
              Title.Caption = 'Tipo'
              Width = 23
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Pgto'
              Title.Caption = 'ID Pagto'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Protocolo'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SERIE_CHEQUE'
              Title.Caption = 'S'#233'rie/Docum.'
              Width = 58
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IndiPag'
              Title.Caption = 'iPg'
              Width = 16
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 131
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Cr'#233'dito'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencim.'
              Width = 41
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPENSADO_TXT'
              Title.Caption = 'Compen.'
              Width = 41
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENSAL'
              Title.Caption = 'M'#234's'
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'to'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 19
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaFiscal'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMERELACIONADO'
              Title.Caption = 'Cliente / Fornecedor'
              Width = 160
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UH'
              Width = 29
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFORNECEI'
              Title.Caption = 'Propriet'#225'rio'
              Width = 144
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MultaVal'
              Title.Caption = 'Multa paga'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MoraVal'
              Title.Caption = 'Juros pagos'
              Width = 45
              Visible = True
            end>
          Color = clWindow
          DataSource = DmAtencoes.DsSemMez
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          FieldsCalcToOrder.Strings = (
            'NOMESIT=Sit,Vencimento'
            'SERIE_CHEQUE=SerieCH,Documento'
            'COMPENSADO_TXT=Compensado'
            'MENSAL=Mez')
          Columns = <
            item
              Expanded = False
              FieldName = 'Data'
              Width = 41
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatNum'
              Title.Caption = 'Bloqueto'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'FatID'
              Title.Caption = 'Tipo'
              Width = 23
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ID_Pgto'
              Title.Caption = 'ID Pagto'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Protocolo'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SERIE_CHEQUE'
              Title.Caption = 'S'#233'rie/Docum.'
              Width = 58
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'IndiPag'
              Title.Caption = 'iPg'
              Width = 16
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descri'#231#227'o'
              Width = 131
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Debito'
              Title.Caption = 'D'#233'bito'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Credito'
              Title.Caption = 'Cr'#233'dito'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Vencimento'
              Title.Caption = 'Vencim.'
              Width = 41
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'COMPENSADO_TXT'
              Title.Caption = 'Compen.'
              Width = 41
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MENSAL'
              Title.Caption = 'M'#234's'
              Width = 30
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMESIT'
              Title.Caption = 'Situa'#231#227'o'
              Width = 56
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'Lan'#231'to'
              Width = 42
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Sub'
              Width = 19
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'SALDO'
              Width = 51
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NotaFiscal'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMERELACIONADO'
              Title.Caption = 'Cliente / Fornecedor'
              Width = 160
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Qtde'
              Title.Caption = 'Quantidade'
              Width = 50
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'UH'
              Width = 29
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEFORNECEI'
              Title.Caption = 'Propriet'#225'rio'
              Width = 144
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MultaVal'
              Title.Caption = 'Multa paga'
              Width = 45
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'MoraVal'
              Title.Caption = 'Juros pagos'
              Width = 45
              Visible = True
            end>
        end
        object Panel3: TPanel
          Left = 0
          Top = 236
          Width = 985
          Height = 47
          Align = alBottom
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object BitBtn1: TBitBtn
            Tag = 14
            Left = 327
            Top = 4
            Width = 333
            Height = 39
            Caption = '&Colocar m'#234's de compet'#234'ncia onde n'#227'o tem'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BitBtn1Click
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 358
    Width = 993
    Height = 53
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 1
    object Panel4: TPanel
      Left = 2
      Top = 14
      Width = 988
      Height = 37
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -14
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object PBAtz1: TProgressBar
        Left = 0
        Top = 20
        Width = 989
        Height = 17
        Align = alBottom
        TabOrder = 0
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 411
    Width = 993
    Height = 70
    Align = alBottom
    TabOrder = 2
    object Panel6: TPanel
      Left = 2
      Top = 14
      Width = 988
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 846
        Top = 0
        Width = 143
        Height = 52
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtSaida: TBitBtn
          Tag = 13
          Left = 2
          Top = 3
          Width = 118
          Height = 39
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtSaidaClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 118
        Height = 39
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        Visible = False
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 993
    Height = 47
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_R: TGroupBox
      Left = 946
      Top = 0
      Width = 47
      Height = 47
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 47
      Height = 47
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 47
      Top = 0
      Width = 899
      Height = 47
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 440
        Height = 31
        Caption = 'Informa'#231#245'es que Precisam de Aten'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 440
        Height = 31
        Caption = 'Informa'#231#245'es que Precisam de Aten'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 440
        Height = 31
        Caption = 'Informa'#231#245'es que Precisam de Aten'#231#227'o'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
end
