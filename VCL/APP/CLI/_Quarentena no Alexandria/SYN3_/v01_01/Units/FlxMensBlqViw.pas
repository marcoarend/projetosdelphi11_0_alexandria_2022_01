unit FlxMensBlqViw;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, dmkDBGrid, ADODB, dmkPermissoes, UnDmkProcFunc, UnDmkEnums;

type
  TFmFlxMensBlqViw = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    LaRegistro: TStaticText;
    Panel6: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel7: TPanel;
    BitBtn1: TBitBtn;
    DBGFlxBlq: TDBGrid;
    Panel8: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    BitBtn5: TBitBtn;
    QrFlxBloq: TmySQLQuery;
    QrFlxBloqRazaoSocial: TWideStringField;
    QrFlxBloqAnoMes: TIntegerField;
    QrFlxBloqCodigo: TIntegerField;
    QrFlxBloqOrdem: TIntegerField;
    QrFlxBloqddVence: TSmallintField;
    QrFlxBloqFolha_DLim: TDateField;
    QrFlxBloqFolha_User: TIntegerField;
    QrFlxBloqFolha_Data: TDateTimeField;
    QrFlxBloqLeiAr_DLim: TDateField;
    QrFlxBloqLeiAr_User: TIntegerField;
    QrFlxBloqLeiAr_Data: TDateTimeField;
    QrFlxBloqFecha_DLim: TDateField;
    QrFlxBloqFecha_User: TIntegerField;
    QrFlxBloqFecha_Data: TDateTimeField;
    QrFlxBloqRisco_DLim: TDateField;
    QrFlxBloqRisco_User: TIntegerField;
    QrFlxBloqRisco_Data: TDateTimeField;
    QrFlxBloqPrint_DLim: TDateField;
    QrFlxBloqPrint_User: TIntegerField;
    QrFlxBloqPrint_Data: TDateTimeField;
    QrFlxBloqProto_DLim: TDateField;
    QrFlxBloqProto_User: TIntegerField;
    QrFlxBloqProto_Data: TDateTimeField;
    QrFlxBloqRelat_DLim: TDateField;
    QrFlxBloqRelat_User: TIntegerField;
    QrFlxBloqRelat_Data: TDateTimeField;
    QrFlxBloqEMail_DLim: TDateField;
    QrFlxBloqEMail_User: TIntegerField;
    QrFlxBloqEMail_Data: TDateTimeField;
    QrFlxBloqPorta_DLim: TDateField;
    QrFlxBloqPorta_User: TIntegerField;
    QrFlxBloqPorta_Data: TDateTimeField;
    QrFlxBloqPostl_DLim: TDateField;
    QrFlxBloqPostl_User: TIntegerField;
    QrFlxBloqPostl_Data: TDateTimeField;
    QrFlxBloqObserv: TWideStringField;
    QrFlxBloqAbert_User: TIntegerField;
    QrFlxBloqAbert_Data: TDateTimeField;
    QrFlxBloqANOMES_TXT: TWideStringField;
    QrFlxBloqFolha_Situ: TLargeintField;
    QrFlxBloqLeiAr_Situ: TLargeintField;
    QrFlxBloqFecha_Situ: TLargeintField;
    QrFlxBloqRisco_Situ: TLargeintField;
    QrFlxBloqPrint_Situ: TLargeintField;
    QrFlxBloqProto_Situ: TLargeintField;
    QrFlxBloqRelat_Situ: TLargeintField;
    QrFlxBloqEMail_Situ: TLargeintField;
    QrFlxBloqPorta_Situ: TLargeintField;
    QrFlxBloqPostl_Situ: TLargeintField;
    QrFlxBloqFOLHA_DHX: TWideStringField;
    QrFlxBloqLEIAR_DHX: TWideStringField;
    QrFlxBloqFECHA_DHX: TWideStringField;
    QrFlxBloqRISCO_DHX: TWideStringField;
    QrFlxBloqPRINT_DHX: TWideStringField;
    QrFlxBloqPROTO_DHX: TWideStringField;
    QrFlxBloqRELAT_DHX: TWideStringField;
    QrFlxBloqEMAIL_DHX: TWideStringField;
    QrFlxBloqPORTA_DHX: TWideStringField;
    QrFlxBloqENTRG_DHX: TWideStringField;
    QrFlxBloqPOSTL_DHX: TWideStringField;
    QrFlxBloqL01: TWideStringField;
    QrFlxBloqL02: TWideStringField;
    QrFlxBloqL03: TWideStringField;
    QrFlxBloqL04: TWideStringField;
    QrFlxBloqL05: TWideStringField;
    QrFlxBloqL06: TWideStringField;
    QrFlxBloqL07: TWideStringField;
    QrFlxBloqL08: TWideStringField;
    QrFlxBloqL09: TWideStringField;
    QrFlxBloqL10: TWideStringField;
    QrFlxBloqFolha_DLim_TXT: TWideStringField;
    QrFlxBloqLeiAr_DLim_TXT: TWideStringField;
    QrFlxBloqFecha_DLim_TXT: TWideStringField;
    QrFlxBloqRisco_DLim_TXT: TWideStringField;
    QrFlxBloqPrint_DLim_TXT: TWideStringField;
    QrFlxBloqProto_DLim_TXT: TWideStringField;
    QrFlxBloqRelat_DLim_TXT: TWideStringField;
    QrFlxBloqEMail_DLim_TXT: TWideStringField;
    QrFlxBloqPorta_DLim_TXT: TWideStringField;
    QrFlxBloqPostl_DLim_TXT: TWideStringField;
    DsFlxMens: TDataSource;
    DBText1: TDBText;
    BtDesfazOrdenacao: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure QrFlxBloqCalcFields(DataSet: TDataSet);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure DBGFlxBlqDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGFlxBlqCellClick(Column: TColumn);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure DBGFlxBlqTitleClick(Column: TColumn);
    procedure BtDesfazOrdenacaoClick(Sender: TObject);
  private
    { Private declarations }
    FOrderDesc: Boolean;
  public
    { Public declarations }
    procedure ReopenFlx(AnoMes, Empresa: Integer);
  end;

  var
  FmFlxMensBlqViw: TFmFlxMensBlqViw;

implementation

uses Module, UnMyObjects, ModuleGeral, FlxMensSel, FlxMensBlqPer, MyDBCheck,
  FlxMensBlqNew, FlxMensAti, MyVCLSkin, UnGOTOy;

{$R *.DFM}

procedure TFmFlxMensBlqViw.BitBtn1Click(Sender: TObject);
begin
  Close;
end;

procedure TFmFlxMensBlqViw.BitBtn5Click(Sender: TObject);
var
  Localizar: Boolean;
  AnoMes: Integer;
begin
  Application.CreateForm(TFmFlxMensBlqPer, FmFlxMensBlqPer);
  FmFlxMensBlqPer.FLocalizar := False;
  FmFlxMensBlqPer.ShowModal;
  Localizar := FmFlxMensBlqPer.FLocalizar;
  AnoMes   := FmFlxMensBlqPer.FAnoMes;
  FmFlxMensBlqPer.Destroy;
  Application.ProcessMessages;
  if Localizar then
    ReopenFlx(AnoMes, 0);
end;

procedure TFmFlxMensBlqViw.BtAlteraClick(Sender: TObject);
begin
  if QrFlxBloq.RecordCount > 0 then
  begin
    if DBCheck.CriaFm(TFmFlxMensSel, FmFlxMensSel, afmoNegarComAviso) then
    begin
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      FmFlxMensSel.EdEmpresa.ValueVariant := QrFlxBloqCodigo.Value;
      FmFlxMensSel.CBEmpresa.KeyValue     := QrFlxBloqCodigo.Value;
      FmFlxMensSel.RGFluxo.ItemIndex      := 2;
      FmFlxMensSel.EdAnoMes.Text          := QrFlxBloqANOMES_TXT.Value;
      FmFlxMensSel.ShowModal;
      FmFlxMensSel.Destroy;
      //
      ReopenFlx(QrFlxBloqAnoMes.Value, QrFlxBloqCodigo.Value);
    end;
  end;
end;

procedure TFmFlxMensBlqViw.BtDesfazOrdenacaoClick(Sender: TObject);
begin
  if (QrFlxBloq.State <> dsInactive) and (QrFlxBloq.RecordCount > 0) then
  begin
    QrFlxBloq.SortBy('Ordem, Codigo ASC');
    QrFlxBloq.First;
  end;
end;

procedure TFmFlxMensBlqViw.BtExcluiClick(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss then
    Exit;
  DBCheck.QuaisItens_Exclui(DMod.QrUpd, QrFlxBloq, TDBGrid(DBGFlxBlq),
  'flxbloq', ['codigo', 'anomes'], ['codigo', 'anomes'], istPergunta, '');
end;

procedure TFmFlxMensBlqViw.BtIncluiClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFlxMensBlqNew, FmFlxMensBlqNew, afmoNegarComAviso) then
  begin
    FmFlxMensBlqNew.ShowModal;
    FmFlxMensBlqNew.Destroy;
  end;
end;

procedure TFmFlxMensBlqViw.DBGFlxBlqCellClick(Column: TColumn);
var
  Data: TDateTime;
begin
  if Copy(Column.FieldName, 6) = '_Situ' then
  begin
    if QrFlxBloq.FieldByName(Copy(Column.FieldName, 1, 5) + '_DLim').AsDateTime < 2 then
      Exit;
    Data := QrFlxBloq.FieldByName(Copy(Column.FieldName, 1, 5) + '_Data').AsDateTime;
    if Data > 2 then
      Data := 0
    else
      Data := DModG.ObtemAgora();
    //
    if DBCheck.CriaFm(TFmFlxMensAti, FmFlxMensAti, afmoNegarComAviso) then
    begin
      FmFlxMensAti.FNomeTabela  := 'flxbloq';
      FmFlxMensAti.FCampoData   := Copy(Column.FieldName, 1, 5) + '_Data';
      FmFlxMensAti.FCampoUser   := Copy(Column.FieldName, 1, 5) + '_User';
      FmFlxMensAti.EdPasso.Text := QrFlxBloq.FieldByName(Column.FieldName).DisplayLabel;
      FmFlxMensAti.EdCodigo.ValueVariant := QrFlxBloqCodigo.Value;
      FmFlxMensAti.EdAnoMes.ValueVariant := QrFlxBloqAnoMes.Value;
      FmFlxMensAti.EdAgora.ValueVariant  := Data;
      //
      FmFlxMensAti.ShowModal;
      FmFlxMensAti.Destroy;
      //
      ReopenFlx(QrFlxBloqAnoMes.Value, QrFlxBloqCodigo.Value);
    end;
  end;
end;

procedure TFmFlxMensBlqViw.DBGFlxBlqDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
  Data: TDateTime;
  Dias, Situ: Integer;
begin
  if Copy(Column.FieldName, 6) = '_DHX' then
  begin
  end else
  if Copy(Column.FieldName, 6) = '_Situ' then
  begin
    Data := QrFlxBloq.FieldByName(Copy(Column.FieldName, 1, 5) + '_Data').AsDateTime;
    if Data < 2 then
    begin
      Data := QrFlxBloq.FieldByName(Copy(Column.FieldName, 1, 5) + '_DLim').AsDateTime;
      Dias := Trunc(Data) - Trunc(Date);
      if Data < 2  then Cor := clGray    else
      if Dias <= 0 then Cor := clRed     else
      if Dias = 1  then Cor := $000080FF else // cor de laranja $000080FF
      if Dias = 2  then Cor := clYellow  else
                        Cor := clGreen;
    end else            cor := clWindow;
    MyObjects.DesenhaTextoEmDBGrid(DBGFlxBlq, Rect,
      clBlack, Cor, Column.Alignment, Column.Field.DisplayText);
    Situ := QrFlxBloq.FieldByName(Copy(Column.FieldName, 1, 5) + '_Situ').AsInteger;
    { Alexandria!
    MeuVCLSkin.DrawGridCheck(DBGFlxBlq, Rect, 1, Situ, dmkrc0a2);
    }
  end;
end;

procedure TFmFlxMensBlqViw.DBGFlxBlqTitleClick(Column: TColumn);
var
  Field: string;
begin
  if (QrFlxBloq.State <> dsInactive) and (QrFlxBloq.RecordCount > 0) then
  begin
    Field := LowerCase(Column.Field.FieldName);
    if (Field <> 'folha_dlim_txt') and (Field <> 'leiar_dlim_txt') and
      (Field <> 'fecha_dlim_txt') and (Field <> 'risco_dlim_txt') and
      (Field <> 'print_dlim_txt') and (Field <> 'proto_dlim_txt') and
      (Field <> 'relat_dlim_txt') and (Field <> 'email_dlim_txt') and
      (Field <> 'porta_dlim_txt') and (Field <> 'postl_dlim_txt') then
    begin
      if FOrderDesc = False then
      begin
        QrFlxBloq.SortBy(Column.Field.FieldName + ' DESC');
        FOrderDesc := True;
      end else
      begin
        QrFlxBloq.SortBy(Column.Field.FieldName + ' ASC');
        FOrderDesc := False;
      end;
      QrFlxBloq.First;
    end;
  end;
end;

procedure TFmFlxMensBlqViw.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmFlxMensBlqViw.FormCreate(Sender: TObject);
begin
  FOrderDesc := False;
  //
  GOTOy.VaiInt(vpLast, QrFlxBloq, 'FlxBloq', 'AnoMes', '', False);
end;

procedure TFmFlxMensBlqViw.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFlxMensBlqViw.QrFlxBloqCalcFields(DataSet: TDataSet);
begin
  QrFlxBloqFolha_DHX.Value := Geral.FDT(QrFlxBloqFolha_Data.Value, 106) + ' - ' + QrFlxBloqL01.Value;
  QrFlxBloqLeiAr_DHX.Value := Geral.FDT(QrFlxBloqLeiAr_Data.Value, 106) + ' - ' + QrFlxBloqL02.Value;
  QrFlxBloqFecha_DHX.Value := Geral.FDT(QrFlxBloqFecha_Data.Value, 106) + ' - ' + QrFlxBloqL03.Value;
  QrFlxBloqRisco_DHX.Value := Geral.FDT(QrFlxBloqRisco_Data.Value, 106) + ' - ' + QrFlxBloqL04.Value;
  QrFlxBloqPrint_DHX.Value := Geral.FDT(QrFlxBloqPrint_Data.Value, 106) + ' - ' + QrFlxBloqL05.Value;
  QrFlxBloqProto_DHX.Value := Geral.FDT(QrFlxBloqProto_Data.Value, 106) + ' - ' + QrFlxBloqL06.Value;
  QrFlxBloqRelat_DHX.Value := Geral.FDT(QrFlxBloqRelat_Data.Value, 106) + ' - ' + QrFlxBloqL07.Value;
  QrFlxBloqEMail_DHX.Value := Geral.FDT(QrFlxBloqEMail_Data.Value, 106) + ' - ' + QrFlxBloqL08.Value;
  QrFlxBloqPorta_DHX.Value := Geral.FDT(QrFlxBloqPorta_Data.Value, 106) + ' - ' + QrFlxBloqL09.Value;
  QrFlxBloqPostl_DHX.Value := Geral.FDT(QrFlxBloqPostl_Data.Value, 106) + ' - ' + QrFlxBloqL10.Value;
  //
  QrFlxBloqFolha_Dlim_TXT.Value := Geral.FDT(QrFlxBloqFolha_DLim.Value, 12);
  QrFlxBloqLeiAr_Dlim_TXT.Value := Geral.FDT(QrFlxBloqLeiAr_DLim.Value, 12);
  QrFlxBloqFecha_Dlim_TXT.Value := Geral.FDT(QrFlxBloqFecha_DLim.Value, 12);
  QrFlxBloqRisco_Dlim_TXT.Value := Geral.FDT(QrFlxBloqRisco_DLim.Value, 12);
  QrFlxBloqPrint_Dlim_TXT.Value := Geral.FDT(QrFlxBloqPrint_DLim.Value, 12);
  QrFlxBloqProto_Dlim_TXT.Value := Geral.FDT(QrFlxBloqProto_DLim.Value, 12);
  QrFlxBloqRelat_Dlim_TXT.Value := Geral.FDT(QrFlxBloqRelat_DLim.Value, 12);
  QrFlxBloqEMail_Dlim_TXT.Value := Geral.FDT(QrFlxBloqEMail_DLim.Value, 12);
  QrFlxBloqPorta_Dlim_TXT.Value := Geral.FDT(QrFlxBloqPorta_DLim.Value, 12);
  QrFlxBloqPostl_Dlim_TXT.Value := Geral.FDT(QrFlxBloqPostl_DLim.Value, 12);
  //
  QrFlxBloqANOMES_TXT.Value :=
    FormatFloat('00', QrFlxBloqAnoMes.Value mod 100) + '/' +
    FormatFloat('00', QrFlxBloqAnoMes.Value div 100)
end;

procedure TFmFlxMensBlqViw.ReopenFlx(AnoMes, Empresa: Integer);
begin
  QrFlxBloq.Close;
  QrFlxBloq.Params[00].AsInteger := AnoMes;
  QrFlxBloq.Open;
  //
  QrFlxBloq.Locate('Codigo', Empresa, []);
end;

procedure TFmFlxMensBlqViw.SpeedButton1Click(Sender: TObject);
begin
  GOTOy.VaiInt(vpFirst, QrFlxBloq, 'FlxBloq', 'AnoMes', '', False);
end;

procedure TFmFlxMensBlqViw.SpeedButton2Click(Sender: TObject);
begin
  GOTOy.VaiInt(vpPrior, QrFlxBloq, 'FlxBloq', 'AnoMes', '', False);
end;

procedure TFmFlxMensBlqViw.SpeedButton3Click(Sender: TObject);
begin
  GOTOy.VaiInt(vpNext, QrFlxBloq, 'FlxBloq', 'AnoMes', '', False);
end;

procedure TFmFlxMensBlqViw.SpeedButton4Click(Sender: TObject);
begin
  GOTOy.VaiInt(vpLast, QrFlxBloq, 'FlxBloq', 'AnoMes', '', False);
end;

end.
