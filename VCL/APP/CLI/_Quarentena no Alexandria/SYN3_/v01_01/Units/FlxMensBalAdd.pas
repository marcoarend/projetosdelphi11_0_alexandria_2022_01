unit FlxMensBalAdd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkPermissoes, UnDmkEnums;

type
  TFmFlxMensBalAdd = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Panel5: TPanel;
    GroupBox40: TGroupBox;
    Label217: TLabel;
    Label218: TLabel;
    Label222: TLabel;
    Label221: TLabel;
    Label220: TLabel;
    Label219: TLabel;
    EdFlxM_Conci: TdmkEdit;
    EdFlxM_Docum: TdmkEdit;
    EdFlxM_Entrg: TdmkEdit;
    EdFlxM_Encad: TdmkEdit;
    EdFlxM_Contb: TdmkEdit;
    EdFlxM_Anali: TdmkEdit;
    Panel6: TPanel;
    Label223: TLabel;
    EdFlxM_Ordem: TdmkEdit;
    Panel7: TPanel;
    Label1: TLabel;
    LaMes: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    EdAnoMes: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label2: TLabel;
    EdFlxM_Web: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmFlxMensBalAdd: TFmFlxMensBalAdd;

implementation

uses Module, UnMyObjects, ModuleGeral, UMySQLModule;

{$R *.DFM}

procedure TFmFlxMensBalAdd.BtOKClick(Sender: TObject);
  function CriaData(Ano, Mes, Dia: Integer): TDateTime;
  begin
    if Dia = 0 then
      Result := 0
    else
      Result := EncodeDate(Ano, Mes, 1) + Dia - 1;
  end;
var
  Ordem: Integer;
  Data,
  Conci_DLim,
  Docum_DLim,
  Contb_DLim,
  Anali_DLim,
  Encad_DLim,
  Entrg_DLim,
  Web01_DLim,
  Abert_Data: TDateTime;
  //
  Codigo, AnoMes, Abert_User: Integer;
  //
  Ano, Mes, Dia: Word;
begin
  Codigo := EdEmpresa.ValueVariant;
  if MyObjects.FIC(Codigo=0, EdEmpresa, 'Informe a empresa!') then Exit;
  Data   := EdAnoMes.ValueVariant;
  AnoMes := Geral.DTAM(Data);
  if MyObjects.FIC(Data < 2, EdAnoMes, 'Informe o m�s') then Exit;
  //
  DecodeDate(Data, Ano, Mes, Dia);
  //
  Ordem      := EdFlxM_Ordem.ValueVariant;
  Conci_DLim := CriaData(Ano, Mes, EdFlxM_Conci.ValueVariant);
  Docum_DLim := CriaData(Ano, Mes, EdFlxM_Docum.ValueVariant);
  Contb_DLim := CriaData(Ano, Mes, EdFlxM_Contb.ValueVariant);
  Anali_DLim := CriaData(Ano, Mes, EdFlxM_Anali.ValueVariant);
  Encad_DLim := CriaData(Ano, Mes, EdFlxM_Encad.ValueVariant);
  Entrg_DLim := CriaData(Ano, Mes, EdFlxM_Entrg.ValueVariant);
  Web01_DLim := CriaData(Ano, Mes, EdFlxM_Web.ValueVariant);
  //
  Abert_User := VAR_USUARIO;
  Abert_Data := DModG.ObtemAgora();
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'flxmens', False, [
    'Ordem', 'Abert_User', 'Abert_Data',
    'Conci_DLim',
    'Docum_DLim',
    'Contb_DLim',
    'Anali_DLim',
    'Encad_DLim',
    'Entrg_DLim',
    'Web01_DLim'
  ], [
    'AnoMes', 'Codigo'
  ], [
    Ordem, Abert_User, Abert_Data,
    Conci_DLim,
    Docum_DLim,
    Contb_DLim,
    Anali_DLim,
    Encad_DLim,
    Entrg_DLim,
    Web01_DLim
  ], [
    AnoMes, Codigo], True) then
  Close;
end;

procedure TFmFlxMensBalAdd.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFlxMensBalAdd.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmFlxMensBalAdd.FormCreate(Sender: TObject);
begin
  EdAnoMes.ValueVariant := Date;
  CBEmpresa.ListSource := DModG.DsEmpresas;
end;

procedure TFmFlxMensBalAdd.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
