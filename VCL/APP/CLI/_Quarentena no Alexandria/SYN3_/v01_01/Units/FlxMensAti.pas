unit FlxMensAti;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, dmkPermissoes, UnDmkEnums;

type
  TFmFlxMensAti = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    Label3: TLabel;
    EdCodigo: TdmkEdit;
    EdAnoMes: TdmkEdit;
    Label4: TLabel;
    Label1: TLabel;
    EdPasso: TdmkEdit;
    EdAgora: TdmkEdit;
    Label2: TLabel;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FNomeTabela, FCampoData, FCampoUser: String;
  end;

  var
  FmFlxMensAti: TFmFlxMensAti;

implementation

uses Module, UnMyObjects, UMySQLModule, MyDBCheck;

{$R *.DFM}

procedure TFmFlxMensAti.BtOKClick(Sender: TObject);
var
  Agora: String;
begin
  if EdAgora.ValueVariant < 3 then
    if not DBCheck.LiberaPelaSenhaBoss then Exit;
  //
  Agora := Geral.FDT(EdAgora.ValueVariant, 105);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, ImgTipo.SQLType, FNomeTabela, False, [
  FCampoData, FCampoUser], ['AnoMes', 'Codigo'], [
  Agora, VAR_USUARIO], [EdAnoMes.ValueVariant, EdCodigo.ValueVariant
  ], True) then
  Close;
end;

procedure TFmFlxMensAti.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFlxMensAti.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ImgTipo.SQLType := stUpd;
end;

procedure TFmFlxMensAti.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
