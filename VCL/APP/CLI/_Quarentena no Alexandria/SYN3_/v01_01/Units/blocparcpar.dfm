object Fmbloqparcpar: TFmbloqparcpar
  Left = 339
  Top = 185
  Caption = 'BLQ-REPAR-002 :: Edi'#231#227'o de Parcela de Parcelamento'
  ClientHeight = 214
  ClientWidth = 632
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 632
    Height = 52
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 200
      Top = 8
      Width = 45
      Height = 13
      Caption = 'Bloqueto:'
      FocusControl = DBEdit1
    end
    object Label2: TLabel
      Left = 268
      Top = 8
      Width = 59
      Height = 13
      Caption = 'Vencimento:'
      FocusControl = DBEdit2
    end
    object Label3: TLabel
      Left = 152
      Top = 8
      Width = 39
      Height = 13
      Caption = 'Parcela:'
      FocusControl = DBEdit3
    end
    object Label4: TLabel
      Left = 16
      Top = 8
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdit4
    end
    object Label5: TLabel
      Left = 84
      Top = 8
      Width = 42
      Height = 13
      Caption = 'Controle:'
      FocusControl = DBEdit5
    end
    object Label6: TLabel
      Left = 336
      Top = 8
      Width = 71
      Height = 13
      Caption = 'Valor bloqueto:'
      FocusControl = DBEdit6
    end
    object Label7: TLabel
      Left = 532
      Top = 8
      Width = 87
      Height = 13
      Caption = 'Novo vencimento:'
    end
    object Label8: TLabel
      Left = 420
      Top = 8
      Width = 99
      Height = 13
      Caption = 'Novo valor bloqueto:'
    end
    object DBEdit1: TDBEdit
      Left = 200
      Top = 24
      Width = 64
      Height = 21
      TabStop = False
      DataField = 'FatNum'
      TabOrder = 3
    end
    object DBEdit2: TDBEdit
      Left = 268
      Top = 24
      Width = 64
      Height = 21
      TabStop = False
      DataField = 'Vencimento'
      TabOrder = 4
    end
    object DBEdit3: TDBEdit
      Left = 152
      Top = 24
      Width = 45
      Height = 21
      TabStop = False
      DataField = 'Parcela'
      TabOrder = 2
    end
    object DBEdit4: TDBEdit
      Left = 16
      Top = 24
      Width = 64
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      TabOrder = 0
    end
    object DBEdit5: TDBEdit
      Left = 84
      Top = 24
      Width = 64
      Height = 21
      TabStop = False
      DataField = 'Controle'
      TabOrder = 1
    end
    object DBEdit6: TDBEdit
      Left = 336
      Top = 24
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'ValBol'
      TabOrder = 5
    end
    object dmkEdCredito: TdmkEdit
      Left = 420
      Top = 24
      Width = 109
      Height = 21
      Alignment = taRightJustify
      TabOrder = 6
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 2
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,00'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object TPVencimento: TDateTimePicker
      Left = 532
      Top = 24
      Width = 93
      Height = 21
      Date = 39588.000000000000000000
      Time = 0.859359618101734700
      TabOrder = 7
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 632
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 584
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 536
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 432
        Height = 32
        Caption = 'Edi'#231#227'o de Parcela de Parcelamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 432
        Height = 32
        Caption = 'Edi'#231#227'o de Parcela de Parcelamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 432
        Height = 32
        Caption = 'Edi'#231#227'o de Parcela de Parcelamento'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 100
    Width = 632
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 628
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 144
    Width = 632
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 628
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 484
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 0
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Sai da janela atual'
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
      object BtOK: TBitBtn
        Tag = 14
        Left = 20
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 1
        OnClick = BtOKClick
      end
    end
  end
end
