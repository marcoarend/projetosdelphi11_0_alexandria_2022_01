object FmCertidaoNeg: TFmCertidaoNeg
  Left = 339
  Top = 185
  Caption = 'GER-PROPR-003 :: Impress'#227'o de Certid'#245'es e Cartas'
  ClientHeight = 779
  ClientWidth = 805
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 805
    Height = 580
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 343
      Width = 805
      Height = 4
      Cursor = crVSplit
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      ExplicitTop = 320
    end
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 805
      Height = 225
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 5
        Top = 0
        Width = 75
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Condom'#237'nio:'
      end
      object Label1: TLabel
        Left = 5
        Top = 49
        Width = 180
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Propriet'#225'rio [F4 mostra todos]:'
      end
      object Label3: TLabel
        Left = 478
        Top = 49
        Width = 131
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Unidade habitacional:'
      end
      object Label4: TLabel
        Left = 660
        Top = 49
        Width = 110
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Data da pesquisa:'
      end
      object Label5: TLabel
        Left = 5
        Top = 175
        Width = 99
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Modelo do texto:'
      end
      object EdEmpresa: TdmkEditCB
        Left = 5
        Top = 20
        Width = 54
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdEmpresaChange
        DBLookupComboBox = CBEmpresa
        IgnoraDBLookupComboBox = False
      end
      object EdPropriet: TdmkEditCB
        Left = 5
        Top = 69
        Width = 54
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdProprietChange
        OnKeyDown = EdProprietKeyDown
        DBLookupComboBox = CBPropriet
        IgnoraDBLookupComboBox = False
      end
      object EdCondImov: TdmkEditCB
        Left = 478
        Top = 69
        Width = 54
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCondImovChange
        DBLookupComboBox = CBCondImov
        IgnoraDBLookupComboBox = False
      end
      object CBEmpresa: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 724
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'CodCond'
        ListField = 'NOMECOND'
        ListSource = DsEntiCond
        TabOrder = 1
        dmkEditCB = EdEmpresa
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBPropriet: TdmkDBLookupComboBox
        Left = 64
        Top = 69
        Width = 410
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'NOMEPROP'
        ListSource = DsPropriet
        TabOrder = 3
        OnKeyDown = CBProprietKeyDown
        dmkEditCB = EdPropriet
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBCondImov: TdmkDBLookupComboBox
        Left = 537
        Top = 69
        Width = 117
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Conta'
        ListField = 'Unidade'
        ListSource = DsCondImov
        TabOrder = 5
        OnKeyDown = CBCondImovKeyDown
        dmkEditCB = EdCondImov
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object TPDataPesq: TdmkEditDateTimePicker
        Left = 660
        Top = 69
        Width = 128
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Date = 39815.735205127310000000
        Time = 39815.735205127310000000
        TabOrder = 6
        OnChange = TPDataPesqChange
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
      end
      object EdCarta: TdmkEditCB
        Left = 5
        Top = 194
        Width = 54
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCarta
        IgnoraDBLookupComboBox = False
      end
      object CBCarta: TdmkDBLookupComboBox
        Left = 64
        Top = 194
        Width = 724
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        KeyField = 'Codigo'
        ListField = 'Titulo'
        ListSource = DsCartas
        TabOrder = 9
        dmkEditCB = EdCarta
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object RGModo: TRadioGroup
        Left = 5
        Top = 98
        Width = 781
        Height = 46
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Tipo de pesquisa: '
        Columns = 4
        ItemIndex = 0
        Items.Strings = (
          'Nenhum'
          'Apenas com pend'#234'ncias'
          'Apenas sem pend'#234'ncias'
          'Com e sem pend'#234'ncias')
        TabOrder = 7
        OnClick = RGModoClick
      end
      object CkMultaJur: TCheckBox
        Left = 5
        Top = 151
        Width = 160
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Calcular multa e juros'
        TabOrder = 10
      end
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 0
      Top = 347
      Width = 805
      Height = 182
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'Documento'
          Width = 89
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MES'
          Title.Caption = 'M'#234's'
          Width = 54
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR'
          Width = 81
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Multa'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Juros'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TOTAL'
          Title.Caption = 'Atualizado'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PAGO'
          Title.Caption = 'Pago'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PEND_VAL'
          Title.Caption = 'Pendente'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsInad
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'FatNum'
          Title.Caption = 'Documento'
          Width = 89
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MES'
          Title.Caption = 'M'#234's'
          Width = 54
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR'
          Width = 81
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Multa'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Juros'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TOTAL'
          Title.Caption = 'Atualizado'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PAGO'
          Title.Caption = 'Pago'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PEND_VAL'
          Title.Caption = 'Pendente'
          Visible = True
        end>
    end
    object dmkDBGrid2: TdmkDBGrid
      Left = 0
      Top = 225
      Width = 805
      Height = 118
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Columns = <
        item
          Expanded = False
          FieldName = 'NOME_CON'
          Title.Caption = 'Condom'#237'nio'
          Width = 240
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 73
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME_PRP'
          Title.Caption = 'Propriet'#225'rio'
          Width = 220
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR'
          Title.Caption = 'Valor'
          Width = 72
          Visible = True
        end>
      Color = clWindow
      DataSource = DsUnidades
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 2
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOME_CON'
          Title.Caption = 'Condom'#237'nio'
          Width = 240
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Unidade'
          Width = 73
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOME_PRP'
          Title.Caption = 'Propriet'#225'rio'
          Width = 220
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'VALOR'
          Title.Caption = 'Valor'
          Width = 72
          Visible = True
        end>
    end
    object Panel5: TPanel
      Left = 0
      Top = 529
      Width = 805
      Height = 51
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 3
      object RGPesquisa: TRadioGroup
        Left = 0
        Top = 0
        Width = 401
        Height = 51
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        Caption = ' Pesquisa: '
        Columns = 2
        ItemIndex = 1
        Items.Strings = (
          'Desta Janela'
          'Do Module (Novo!)')
        TabOrder = 0
      end
      object RGImpressao: TRadioGroup
        Left = 401
        Top = 0
        Width = 404
        Height = 51
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = ' Impress'#227'o: '
        Columns = 2
        ItemIndex = 1
        Items.Strings = (
          'Desta Janela'
          'Cobran'#231'a (Novo!)')
        TabOrder = 1
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 805
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 746
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 10
        Top = 14
        Width = 39
        Height = 39
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 59
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 59
      Top = 0
      Width = 687
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 9
        Top = 11
        Width = 470
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Impress'#227'o de Certid'#245'es e Cartas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 11
        Top = 14
        Width = 470
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Impress'#227'o de Certid'#245'es e Cartas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 10
        Top = 12
        Width = 470
        Height = 38
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Impress'#227'o de Certid'#245'es e Cartas'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -33
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 639
    Width = 805
    Height = 54
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 18
      Width = 801
      Height = 34
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 16
        Top = 2
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 15
        Top = 1
        Width = 150
        Height = 19
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -17
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 693
    Width = 805
    Height = 86
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 641
      Top = 18
      Width = 162
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 5
        Top = 4
        Width = 148
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 18
      Width = 639
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label6: TLabel
        Left = 325
        Top = 5
        Width = 134
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Unidades localizadas:'
      end
      object BtPesquisa: TBitBtn
        Tag = 20
        Left = 10
        Top = 4
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Pesquisa'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtPesquisaClick
      end
      object EdUnidades: TdmkEdit
        Left = 325
        Top = 25
        Width = 129
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object BtImprime: TBitBtn
        Tag = 5
        Left = 162
        Top = 4
        Width = 148
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Imprime'
        Enabled = False
        NumGlyphs = 2
        TabOrder = 2
        OnClick = BtImprimeClick
      end
      object CkDesign: TCheckBox
        Left = 469
        Top = 30
        Width = 105
        Height = 20
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Design mode.'
        TabOrder = 3
      end
    end
  end
  object QrInad: TmySQLQuery
    Database = Dmod.MyDB
    Filter = 'VALOR=0'
    Left = 216
    Top = 512
    object QrInadMES: TWideStringField
      FieldName = 'MES'
      Required = True
      Size = 7
    end
    object QrInadData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrInadFatNum: TFloatField
      FieldName = 'FatNum'
      DisplayFormat = '000000'
    end
    object QrInadVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrInadMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrInadDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrInadCREDITO: TFloatField
      FieldName = 'CREDITO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadPAGO: TFloatField
      FieldName = 'PAGO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadJuros: TFloatField
      FieldName = 'Juros'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadMulta: TFloatField
      FieldName = 'Multa'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadTOTAL: TFloatField
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadSALDO: TFloatField
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadPEND_VAL: TFloatField
      FieldName = 'PEND_VAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrInadVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object QrInadATZ_VAL: TFloatField
      FieldName = 'ATZ_VAL'
    end
  end
  object DsInad: TDataSource
    DataSet = QrInad
    Left = 244
    Top = 512
  end
  object QrCondImov: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cim.Codigo, cnd.Cliente, cim.Conta, cim.Unidade, '
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECND,'
      'IF(usu.Tipo=0, usu.RazaoSocial, usu.Nome) NOMEUSU,'
      'IF(usu.Tipo=0, usu.CNPJ, usu.CPF) DOC_USU, '
      'IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOMEPRP,'
      'IF(prp.Tipo=0, prp.CNPJ, prp.CPF) DOC_PRP'
      'FROM condimov cim'
      'LEFT JOIN cond      cnd ON cnd.Codigo=cim.Codigo'
      'LEFT JOIN entidades cli ON cli.Codigo=cnd.Cliente'
      'LEFT JOIN entidades usu ON usu.Codigo=cim.Usuario'
      'LEFT JOIN entidades prp ON prp.Codigo=cim.Propriet'#10
      ''
      ''
      ''
      'WHERE cim.Codigo<>0'
      'AND cim.Propriet<>0'
      'ORDER BY cim.Unidade'
      '')
    Left = 428
    Top = 356
    object QrCondImovConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrCondImovUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
    object QrCondImovNOMEUSU: TWideStringField
      FieldName = 'NOMEUSU'
      Size = 100
    end
    object QrCondImovDOC_USU: TWideStringField
      FieldName = 'DOC_USU'
      Size = 18
    end
    object QrCondImovNOMEPRP: TWideStringField
      FieldName = 'NOMEPRP'
      Size = 100
    end
    object QrCondImovDOC_PRP: TWideStringField
      FieldName = 'DOC_PRP'
      Size = 18
    end
    object QrCondImovCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCondImovCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrCondImovNOMECND: TWideStringField
      FieldName = 'NOMECND'
      Size = 100
    end
  end
  object DsCondImov: TDataSource
    DataSet = QrCondImov
    Left = 456
    Top = 356
  end
  object DsPropriet: TDataSource
    DataSet = QrPropriet
    Left = 400
    Top = 356
  end
  object QrPropriet: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT ent.Codigo,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP '
      'FROM entidades ent'
      'LEFT JOIN condimov cim ON ent.Codigo=cim.Propriet'
      'WHERE ent.Cliente2="V"'
      'AND cim.Codigo<>0'
      'ORDER BY NOMEPROP')
    Left = 372
    Top = 356
    object QrProprietCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrProprietNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Origin = 'NOMEPROP'
      Required = True
      Size = 100
    end
  end
  object DsEntiCond: TDataSource
    DataSet = QrEntiCond
    Left = 344
    Top = 356
  end
  object QrEntiCond: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cnd.Codigo CodCond, ent.Codigo CodEnti,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECOND '
      'FROM entidades ent'
      'LEFT JOIN cond cnd ON ent.Codigo=cnd.Cliente'
      'WHERE ent.Cliente1="V" '
      'AND cnd.Codigo<>0'
      'ORDER BY NOMECOND'
      '')
    Left = 316
    Top = 356
    object QrEntiCondCodCond: TIntegerField
      FieldName = 'CodCond'
      Origin = 'cond.Codigo'
    end
    object QrEntiCondCodEnti: TIntegerField
      FieldName = 'CodEnti'
      Origin = 'entidades.Codigo'
      Required = True
    end
    object QrEntiCondNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Origin = 'NOMECOND'
      Required = True
      Size = 100
    end
  end
  object frxDsCartas: TfrxDBDataset
    UserName = 'frxDsCartas'
    CloseDataSource = False
    DataSet = QrTexto
    BCDToCurrency = False
    Left = 272
    Top = 540
  end
  object frxCNDC: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39815.746004548600000000
    ReportOptions.LastChange = 39815.746004548600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure PageHeader1OnBeforePrint(Sender: TfrxComponent);'
      'begin'
      '  if <MeuLogoExiste> = True then'
      '    Picture1.LoadFromFile(<MeuLogoCaminho>);'
      'end;'
      ''
      'begin'
      'end.')
    OnGetValue = frxCNDCGetValue
    Left = 300
    Top = 484
    Datasets = <
      item
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
      end
      item
        DataSet = Dmod.frxDsControle
        DataSetName = 'frxDsControle'
      end
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsInad
        DataSetName = 'frxDsInad'
      end
      item
        DataSet = frxDsUnidades
        DataSetName = 'frxDsUnidades'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 20.000000000000000000
        Top = 158.740260000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
        RowCount = 0
        Stretched = True
        object Rich1: TfrxRichView
          Left = 2.629870000000000000
          Width = 706.929190000000000000
          Height = 20.000000000000000000
          StretchMode = smMaxHeight
          DataField = 'Texto'
          DataSet = frxDsCartas
          DataSetName = 'frxDsCartas'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C204D532053616E732053657269663B7D7D0D0A7B5C636F6C6F72
            74626C203B5C726564305C677265656E305C626C7565303B7D0D0A7B5C2A5C67
            656E657261746F722052696368656432302031302E302E31353036337D5C7669
            65776B696E64345C756331200D0A5C706172645C6366315C66305C667331365C
            7061720D0A7D0D0A00}
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        Height = 115.118120000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsUnidades
        DataSetName = 'frxDsUnidades'
        RowCount = 0
        StartNewPage = True
        object Memo1: TfrxMemoView
          Width = 196.440630000000000000
          Height = 115.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo154: TfrxMemoView
          Left = 196.535433070000000000
          Width = 521.480210000000000000
          Height = 115.118120000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo155: TfrxMemoView
          Left = 199.606370000000000000
          Top = 1.889763779999999000
          Width = 509.291280000000000000
          Height = 18.897637800000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
        end
        object Memo157: TfrxMemoView
          Left = 241.181200000000000000
          Top = 20.787401570000000000
          Width = 168.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."CNPJ_TXT"]')
          ParentFont = False
        end
        object Memo158: TfrxMemoView
          Left = 199.858380000000000000
          Top = 20.787401570000000000
          Width = 41.322820000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CNPJ:')
          ParentFont = False
        end
        object Memo159: TfrxMemoView
          Left = 541.212740000000000000
          Top = 20.787401570000000000
          Width = 168.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."IE_RG"]')
          ParentFont = False
        end
        object Memo160: TfrxMemoView
          Left = 515.008040000000000000
          Top = 20.787401570000000000
          Width = 26.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'I.E.:')
          ParentFont = False
        end
        object Memo163: TfrxMemoView
          Left = 252.519790000000000000
          Top = 35.905511810000000000
          Width = 456.377860000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."E_LNR"]')
          ParentFont = False
        end
        object Memo164: TfrxMemoView
          Left = 357.149660000000000000
          Top = 51.023622050000000000
          Width = 275.779530000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."Cidade"]')
          ParentFont = False
        end
        object Memo165: TfrxMemoView
          Left = 312.047310000000000000
          Top = 51.023622050000000000
          Width = 45.102350000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Cidade: ')
          ParentFont = False
        end
        object Memo166: TfrxMemoView
          Left = 678.031540000000000000
          Top = 51.023622050000000000
          Width = 32.000000000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."NOMEUF"]')
          ParentFont = False
        end
        object Memo167: TfrxMemoView
          Left = 632.929190000000000000
          Top = 51.023622050000000000
          Width = 45.102350000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Estado: ')
          ParentFont = False
        end
        object Memo168: TfrxMemoView
          Left = 233.622140000000000000
          Top = 51.023622050000000000
          Width = 78.425170000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."ECEP_TXT"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 199.858380000000000000
          Top = 35.905511810000000000
          Width = 52.661410000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Endere'#231'o:')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 199.858380000000000000
          Top = 51.023622050000000000
          Width = 33.763760000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'CEP:')
          ParentFont = False
        end
        object Memo169: TfrxMemoView
          Left = 252.519790000000000000
          Top = 66.141732280000000000
          Width = 237.574830000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."TE1_TXT"]')
          ParentFont = False
        end
        object Memo170: TfrxMemoView
          Left = 199.858380000000000000
          Top = 66.141732280000000000
          Width = 52.661410000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Telefone:')
          ParentFont = False
        end
        object Memo171: TfrxMemoView
          Left = 516.299320000000000000
          Top = 66.141732280000000000
          Width = 193.795300000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."FAX_TXT"]')
          ParentFont = False
        end
        object Memo172: TfrxMemoView
          Left = 490.094620000000000000
          Top = 66.141732280000000000
          Width = 26.204700000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Fax:')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 237.858380000000000000
          Top = 83.149660000000000000
          Width = 471.905690000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[VAR_MYURL]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 200.315090000000000000
          Top = 83.149660000000000000
          Width = 37.543290000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Portal:')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 237.858380000000000000
          Top = 98.267780000000000000
          Width = 471.905690000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsDono."EMail"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 200.315090000000000000
          Top = 98.267780000000000000
          Width = 37.543290000000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            'Emeio:')
          ParentFont = False
        end
        object Picture1: TfrxPictureView
          Left = 0.755905510000000000
          Top = 0.755905510000001700
          Width = 194.645669290000000000
          Height = 113.385826770000000000
          Center = True
          HightQuality = True
          Transparent = False
          TransparentColor = clWhite
        end
      end
    end
  end
  object frxCDCU: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39815.746004548600000000
    ReportOptions.LastChange = 39815.746004548600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxCNDCGetValue
    Left = 300
    Top = 512
    Datasets = <
      item
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
      end
      item
        DataSet = Dmod.frxDsControle
        DataSetName = 'frxDsControle'
      end
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsInad
        DataSetName = 'frxDsInad'
      end
      item
        DataSet = frxDsUnidades
        DataSetName = 'frxDsUnidades'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 9.000000000000000000
      BottomMargin = 10.000000000000000000
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 20.000000000000000000
        Top = 45.354360000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsCartas
        DataSetName = 'frxDsCartas'
        RowCount = 0
        Stretched = True
        object Rich1: TfrxRichView
          Left = 2.629870000000000000
          Width = 706.929190000000000000
          Height = 20.000000000000000000
          StretchMode = smMaxHeight
          DataField = 'Texto'
          DataSet = frxDsCartas
          DataSetName = 'frxDsCartas'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C204D532053616E732053657269663B7D7D0D0A7B5C636F6C6F72
            74626C203B5C726564305C677265656E305C626C7565303B7D0D0A7B5C2A5C67
            656E657261746F722052696368656432302031302E302E31353036337D5C7669
            65776B696E64345C756331200D0A5C706172645C6366315C66305C667331365C
            7061720D0A7D0D0A00}
        end
      end
      object DadosMestre1: TfrxMasterData
        FillType = ftBrush
        Height = 3.779527560000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        Columns = 1
        ColumnWidth = 200.000000000000000000
        ColumnGap = 20.000000000000000000
        DataSet = frxDsUnidades
        DataSetName = 'frxDsUnidades'
        RowCount = 0
        StartNewPage = True
      end
    end
  end
  object QrCartas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Titulo, Texto, Tipo'
      'FROM cartas'
      'WHERE Tipo in (1,2)'
      'ORDER BY Titulo')
    Left = 484
    Top = 356
    object QrCartasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartasTitulo: TWideStringField
      FieldName = 'Titulo'
      Size = 100
    end
    object QrCartasTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCartasTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsCartas: TDataSource
    DataSet = QrCartas
    Left = 512
    Top = 356
  end
  object frxDsInad: TfrxDBDataset
    UserName = 'frxDsInad'
    CloseDataSource = False
    DataSet = QrInad
    BCDToCurrency = False
    Left = 272
    Top = 512
  end
  object QrUnidades: TmySQLQuery
    Database = Dmod.MyDB
    Filter = 'VALOR=0'
    Filtered = True
    AfterOpen = QrUnidadesAfterOpen
    BeforeClose = QrUnidadesBeforeClose
    AfterScroll = QrUnidadesAfterScroll
    Left = 216
    Top = 484
    object QrUnidadesVALOR: TFloatField
      FieldName = 'VALOR'
    end
    object QrUnidadesNOME_CON: TWideStringField
      FieldName = 'NOME_CON'
      Size = 100
    end
    object QrUnidadesNOME_PRP: TWideStringField
      FieldName = 'NOME_PRP'
      Size = 100
    end
    object QrUnidadesUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrUnidadesDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrUnidadesDOC_PRP: TWideStringField
      FieldName = 'DOC_PRP'
      Size = 18
    end
    object QrUnidadesNOMEUSU: TWideStringField
      FieldName = 'NOMEUSU'
      Size = 100
    end
    object QrUnidadesDOC_USU: TWideStringField
      FieldName = 'DOC_USU'
      Size = 18
    end
    object QrUnidadesConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrUnidadesCOD_PRP: TIntegerField
      FieldName = 'COD_PRP'
    end
    object QrUnidadesCOD_CON: TIntegerField
      FieldName = 'COD_CON'
    end
    object QrUnidadesCEP_PRP: TFloatField
      FieldName = 'CEP_PRP'
    end
    object QrUnidadesUF_PRP: TFloatField
      FieldName = 'UF_PRP'
    end
    object QrUnidadesMUNI_PRP: TWideStringField
      FieldName = 'MUNI_PRP'
      Size = 100
    end
    object QrUnidadesCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object DsUnidades: TDataSource
    DataSet = QrUnidades
    Left = 244
    Top = 484
  end
  object frxDsUnidades: TfrxDBDataset
    UserName = 'frxDsUnidades'
    CloseDataSource = False
    FieldAliases.Strings = (
      'VALOR=VALOR'
      'NOME_CON=NOME_CON'
      'NOME_PRP=NOME_PRP'
      'Unidade=Unidade'
      'Depto=Depto'
      'DOC_PRP=DOC_PRP'
      'NOMEUSU=NOMEUSU'
      'DOC_USU=DOC_USU'
      'Conta=Conta'
      'COD_PRP=COD_PRP'
      'COD_CON=COD_CON'
      'CEP_PRP=CEP_PRP'
      'UF_PRP=UF_PRP'
      'MUNI_PRP=MUNI_PRP'
      'CliInt=CliInt')
    DataSet = QrUnidades
    BCDToCurrency = False
    Left = 272
    Top = 484
  end
  object QrTexto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Titulo, Texto, Tipo'
      'FROM cartas'
      'WHERE Codigo=:P0')
    Left = 216
    Top = 540
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTextoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTextoTitulo: TWideStringField
      FieldName = 'Titulo'
      Size = 100
    end
    object QrTextoTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrTextoTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object QrUsers: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT usu.CodigoEnt, usu.CodigoEsp,'
      'usu.Username, usu.Password'
      'FROM condimov imv'
      
        'LEFT JOIN entidades ent ON IF(imv.Usuario>0, imv.Usuario, imv.Pr' +
        'opriet)=ent.Codigo'
      'LEFT JOIN users usu ON usu.CodigoEnt=imv.Propriet '
      '  AND usu.CodigoEsp=imv.Conta '
      '  AND (usu.Tipo=1 OR usu.Tipo IS NULL)'
      'WHERE imv.Conta=:P0')
    Left = 540
    Top = 356
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUsersCodigoEnt: TIntegerField
      FieldName = 'CodigoEnt'
      Origin = 'users.CodigoEnt'
    end
    object QrUsersCodigoEsp: TIntegerField
      FieldName = 'CodigoEsp'
      Origin = 'users.CodigoEsp'
    end
    object QrUsersUsername: TWideStringField
      FieldName = 'Username'
      Origin = 'users.Username'
      Size = 32
    end
    object QrUsersPassword: TWideStringField
      FieldName = 'Password'
      Origin = 'users.Password'
      Size = 32
    end
  end
end
