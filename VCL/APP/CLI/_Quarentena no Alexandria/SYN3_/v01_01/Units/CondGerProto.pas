unit CondGerProto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, Grids, DBGrids, Db,
  mySQLDbTables, Menus, dmkGeral, dmkDBGrid, Variants, DmkDAC_PF, dmkImage,
  UnDmkEnums, UnDmkProcFunc;

type
  TFmCondGerProto = class(TForm)
    Panel1: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    DBG_CD1: TDBGrid;
    QrPPICir1: TmySQLQuery;
    QrPPICir1PROTOCOLO: TIntegerField;
    QrPPICir1DataE: TDateField;
    QrPPICir1DataD: TDateField;
    QrPPICir1Cancelado: TIntegerField;
    QrPPICir1Motivo: TIntegerField;
    QrPPICir1ID_Cod1: TIntegerField;
    QrPPICir1ID_Cod2: TIntegerField;
    QrPPICir1ID_Cod3: TIntegerField;
    QrPPICir1ID_Cod4: TIntegerField;
    PMProtocolo: TPopupMenu;
    Gera1: TMenuItem;
    Todosabertos1: TMenuItem;
    Abertosseleciondos1: TMenuItem;
    Atuaseaberto1: TMenuItem;
    QrPPICir1TAREFA: TWideStringField;
    QrPPICir1DELIVER: TWideStringField;
    QrPPICir1LOTE: TIntegerField;
    DsBolMail: TDataSource;
    QrBolMail: TmySQLQuery;
    QrBolMailApto: TIntegerField;
    QrBolMailPropriet: TIntegerField;
    QrBolMailVencto: TDateField;
    QrBolMailUnidade: TWideStringField;
    QrBolMailNOMEPROPRIET: TWideStringField;
    QrBolMailBOLAPTO: TWideStringField;
    QrPPICir1CliInt: TIntegerField;
    QrPPICir1Cliente: TIntegerField;
    QrPPICir1Periodo: TIntegerField;
    QrPPICir1DATAE_TXT: TWideStringField;
    QrPPICir1DATAD_TXT: TWideStringField;
    PnProtoMail: TPanel;
    Panel4: TPanel;
    Panel6: TPanel;
    BtEmeio: TBitBtn;
    PB1: TProgressBar;
    LaProtoMail: TLabel;
    QrPPICir1Def_Sender: TIntegerField;
    QrProtoMail: TmySQLQuery;
    QrProtoMailDepto_Cod: TIntegerField;
    QrProtoMailDepto_Txt: TWideStringField;
    QrProtoMailEntid_Cod: TIntegerField;
    QrProtoMailEntid_Txt: TWideStringField;
    QrProtoMailTaref_Cod: TIntegerField;
    QrProtoMailTaref_Txt: TWideStringField;
    QrProtoMailDeliv_Cod: TIntegerField;
    QrProtoMailDeliv_Txt: TWideStringField;
    QrProtoMailDataE: TDateField;
    QrProtoMailDataE_Txt: TWideStringField;
    QrProtoMailDataD: TDateField;
    QrProtoMailDataD_Txt: TWideStringField;
    QrProtoMailProtoLote: TIntegerField;
    QrProtoMailNivelEmail: TSmallintField;
    QrProtoMailItem: TIntegerField;
    DsProtoMail: TDataSource;
    QrProtoMailNivelDescr: TWideStringField;
    QrProtoMailProtocolo: TIntegerField;
    DBG_CE1: TdmkDBGrid;
    QrCondEmeios: TmySQLQuery;
    QrCondEmeiosCodigo: TIntegerField;
    QrCondEmeiosControle: TIntegerField;
    QrCondEmeiosConta: TIntegerField;
    QrCondEmeiosItem: TIntegerField;
    QrCondEmeiosPronome: TWideStringField;
    QrCondEmeiosNome: TWideStringField;
    QrCondEmeiosEMeio: TWideStringField;
    QrProtoMailRecipEmeio: TWideStringField;
    QrProtoMailRecipNome: TWideStringField;
    QrProtoMailRecipProno: TWideStringField;
    QrProtoMailRecipItem: TIntegerField;
    QrPPIMail: TmySQLQuery;
    QrPPIMailPROTOCOLO: TIntegerField;
    QrPPIMailLOTE: TIntegerField;
    QrPPIMailDataE: TDateField;
    QrPPIMailDATAE_TXT: TWideStringField;
    QrPPIMailDATAD_TXT: TWideStringField;
    QrPPIMailCancelado: TIntegerField;
    QrPPIMailMotivo: TIntegerField;
    QrPPIMailID_Cod1: TIntegerField;
    QrPPIMailID_Cod2: TIntegerField;
    QrPPIMailID_Cod3: TIntegerField;
    QrPPIMailID_Cod4: TIntegerField;
    QrPPIMailCliInt: TIntegerField;
    QrPPIMailCliente: TIntegerField;
    QrPPIMailPeriodo: TIntegerField;
    QrPPIMailDef_Sender: TIntegerField;
    QrPPIMailDELIVER: TWideStringField;
    TabSheet3: TTabSheet;
    DBG_CR1: TDBGrid;
    QrCD1: TmySQLQuery;
    QrCD1Apto: TIntegerField;
    QrCD1Propriet: TIntegerField;
    QrCD1Vencto: TDateField;
    QrCD1Unidade: TWideStringField;
    QrCD1NOMEPROPRIET: TWideStringField;
    QrCD1BOLAPTO: TWideStringField;
    QrCD1PROTOCOLO: TIntegerField;
    QrCD1TAREFA: TWideStringField;
    QrCD1DELIVER: TWideStringField;
    QrCD1LOTE: TIntegerField;
    DsCD1: TDataSource;
    QrCR1: TmySQLQuery;
    DsCR1: TDataSource;
    QrCR1Apto: TIntegerField;
    QrCR1Propriet: TIntegerField;
    QrCR1Vencto: TDateField;
    QrCR1Unidade: TWideStringField;
    QrCR1NOMEPROPRIET: TWideStringField;
    QrCR1BOLAPTO: TWideStringField;
    QrCR1PROTOCOLO: TIntegerField;
    QrCR1TAREFA: TWideStringField;
    QrCR1DELIVER: TWideStringField;
    QrCR1LOTE: TIntegerField;
    Desfazprotocolo1: TMenuItem;
    N1: TMenuItem;
    Localizaprotocolo1: TMenuItem;
    QrProtos: TmySQLQuery;
    QrProtosLOTE: TIntegerField;
    DsProtos: TDataSource;
    QrBolLei: TmySQLQuery;
    QrBolLeiValor: TFloatField;
    QrBolLeiApto: TIntegerField;
    QrBolLeiBOLAPTO: TWideStringField;
    QrBolArr: TmySQLQuery;
    QrBolArrValor: TFloatField;
    QrBolArrApto: TIntegerField;
    QrBolArrBOLAPTO: TWideStringField;
    QrCD1SUB_ARR: TFloatField;
    QrCD1SUB_LEI: TFloatField;
    QrCD1SUB_TOT: TFloatField;
    QrCR1SUB_ARR: TFloatField;
    QrCR1SUB_LEI: TFloatField;
    QrCR1SUB_TOT: TFloatField;
    QrPPICir1Valor: TFloatField;
    QrPPICir1Vencto: TDateField;
    QrCR1VALOR_PROT: TFloatField;
    QrCR1VENCTO_PROT: TDateField;
    QrCD1VALOR_PROT: TFloatField;
    QrCD1VENCTO_PROT: TDateField;
    QrPPICir1MoraDiaVal: TFloatField;
    QrPPICir1MultaVal: TFloatField;
    QrCR1MORADIAVAL: TFloatField;
    QrCR1MULTAVAL: TFloatField;
    QrCD1MORADIAVAL: TFloatField;
    QrCD1MULTAVAL: TFloatField;
    QrProtoMailBloqueto: TFloatField;
    QrBolMailBoleto: TFloatField;
    QrCD1Boleto: TFloatField;
    QrCR1Boleto: TFloatField;
    QrBolArrBoleto: TFloatField;
    QrBolLeiBoleto: TFloatField;
    QrPPICir1Docum: TFloatField;
    QrPPIMailDocum: TFloatField;
    QrBolMailSUB_TOT: TFloatField;
    QrBolMailSUB_ARR: TFloatField;
    QrBolMailSUB_LEI: TFloatField;
    QrProtoMailVencimento: TDateField;
    QrProtoMailValor: TFloatField;
    QrProtoMailPreEmeio: TIntegerField;
    QrPPIMailPreEmeio: TIntegerField;
    QrCondEmeiosNOMECOND: TWideStringField;
    QrProtoMailCondominio: TWideStringField;
    QrProtoMailIDEmeio: TIntegerField;
    QrPPIMailDataD: TDateTimeField;
    QrPPIMailTAREFA_COD: TIntegerField;
    QrPPIMailTAREFA_NOM: TWideStringField;
    DBGrid1: TDBGrid;
    QrProt1: TmySQLQuery;
    QrProt1Codigo: TIntegerField;
    QrProt1Nome: TWideStringField;
    DsProt1: TDataSource;
    Splitter1: TSplitter;
    QrPPI_: TmySQLQuery;
    QrBolMailPPI_BOLETO: TIntegerField;
    QrPPI_Docum: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel3: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtProtocolo: TBitBtn;
    BtSaida: TBitBtn;
    DBGrid2: TDBGrid;
    Splitter2: TSplitter;
    DBGrid3: TDBGrid;
    Splitter3: TSplitter;
    QrCD1PROTOCOD: TFloatField;
    QrCR1PROTOCOD: TFloatField;
    QrBolMailPROTOCOD: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure Todosabertos1Click(Sender: TObject);
    procedure BtProtocoloClick(Sender: TObject);
    procedure Abertosseleciondos1Click(Sender: TObject);
    procedure Atuaseaberto1Click(Sender: TObject);
    procedure BtEmeioClick(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure Desfazprotocolo1Click(Sender: TObject);
    procedure Localizaprotocolo1Click(Sender: TObject);
    procedure QrCD1CalcFields(DataSet: TDataSet);
    procedure QrCR1CalcFields(DataSet: TDataSet);
    procedure QrBolMailCalcFields(DataSet: TDataSet);
    procedure QrProt1AfterScroll(DataSet: TDataSet);
    procedure QrBolMailBeforeOpen(DataSet: TDataSet);
    procedure PMProtocoloPopup(Sender: TObject);
    procedure DBG_CE1DblClick(Sender: TObject);
    procedure QrProt1BeforeClose(DataSet: TDataSet);
  private
    { Private declarations }
    //function DefineLoteProtocolo(): Integer;
    FVeriEmeio: Boolean;
    procedure AvisaErroGet;
    procedure ReopenBolArrEBolLei();
    procedure ReabreProtPakItsCir1();
    procedure ProtocoloAbertos(Quais: TSelType);
    procedure ReopenBolMail(Protocolo: Integer);//(BOLAPTO: String);
    procedure ReopenProtoMails;
    procedure ReopenProtocolos(Tipo: Integer);
    function ObtemCondominioDepto(Depto: Integer): Integer;
  public
    { Public declarations }
    FID_Cod1, FID_Cod2, FPeriodo, FCliEnti, FCedente: Integer;
    FPercMulta, FPercJuros: Double;
    FTabLctA, FTabAriA, FTabCnsA: String;
    //Tabelas de condom�nios
    {
    FTabAriA, FTabCnsA, FTabPriA, FTabPrvA,
    FTabLctA, FTabLctB, FTabLctD: String;
    FDtEncer, FDtMorto: TDateTime;
    }
    //
    function GetTabela: TmySQLQuery;
    function GetNomeTab: String;
    function GetGrade: TDBGrid;
    function GetTipoProtocolo: Integer;
    function GetTarefaProtocolo: Integer;

    procedure VerificaEntregaPorEmail(Protocolo: Integer);
    procedure ReopenQuery(Query: TmySQLQuery; TipoProtocolo: Integer;
              BOLAPTO: String; Protocolo: Integer);
  end;

  var
  FmCondGerProto: TFmCondGerProto;

implementation

uses Module, UMySQLModule, UnInternalConsts, MyDBCheck, CondGerProtoSel,
Ucreate, EmailBloqueto, UnFinanceiro, Principal, CondGerProtoLoc, ModuleGeral,
UnMyObjects, MyListas, Protocolo;

{$R *.DFM}

procedure TFmCondGerProto.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerProto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerProto.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerProto.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrProtoMail.Close;
  QrProtoMail.Database := DModG.MyPID_DB;
  FVeriEmeio := False;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmCondGerProto.FormShow(Sender: TObject);
begin
  //ReopenBolCir1('');
  PageControl1.ActivePageIndex := 0;
  //
  ReopenProtocolos(1); //Entrega de documentos
end;

procedure TFmCondGerProto.BtProtocoloClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProtocolo, BtProtocolo);
end;

{
function TFmCondGerProto.DefineLoteProtocolo(): Integer;
begin
  QrLote.Close;
  QrLote.Params[00].AsInteger := FID_Cod1;
  QrLote.Params[01].AsInteger := FID_Cod2;
  QrLote.Params[02].AsInteger := FID_Cod3;
  QrLote.Open;
  //
  Result := QrLoteControle.Value;
end;
}

procedure TFmCondGerProto.Todosabertos1Click(Sender: TObject);
begin
  ProtocoloAbertos(istTodos);
end;

procedure TFmCondGerProto.Abertosseleciondos1Click(Sender: TObject);
begin
  ProtocoloAbertos(istSelecionados);
end;

procedure TFmCondGerProto.Atuaseaberto1Click(Sender: TObject);
begin
  ProtocoloAbertos(istAtual);
end;

procedure TFmCondGerProto.ProtocoloAbertos(Quais: TSelType);
var
  Tarefa: Integer;
begin
  Tarefa := GetTarefaProtocolo; 
  //
  if DBCheck.CriaFm(TFmCondGerProtoSel, FmCondGerProtoSel, afmoNegarComAviso) then
  begin
    FmCondGerProtoSel.ReopenProtocolos(GetTipoProtocolo, Tarefa, FCliEnti);
    //
    FmCondGerProtoSel.EdTarefa.ValueVariant := Tarefa;
    FmCondGerProtoSel.CBTarefa.KeyValue     := Tarefa;
    //
    FmCondGerProtoSel.FQuais     := Quais;
    FmCondGerProtoSel.FNomeQrSrc := GetNomeTab;
    FmCondGerProtoSel.FQrSource  := GetTabela;
    FmCondGerProtoSel.FDBGrid    := GetGrade;
    FmCondGerProtoSel.FTabLctA   := FTabLctA;
    //
    FmCondGerProtoSel.ShowModal;
    FmCondGerProtoSel.Destroy;
    //
    case PageControl1.ActivePageIndex of
      0: //UHs cadastrados para entrega do documento
        ReopenProtocolos(1);
      1: //UHs cadastrados para entrega por e-mail
        ReopenProtocolos(2);
      2: //UHs cadastrados para cobran�a registrada
        ReopenProtocolos(4);
    end;
  end;
end;

procedure TFmCondGerProto.ReabreProtPakItsCir1();
begin
  QrPPICir1.Close;
  QrPPICir1.SQL.Clear;
  QrPPICir1.SQL.Add('SELECT ppi.Conta PROTOCOLO, ppi.Controle LOTE, ppi.DataE,');
  QrPPICir1.SQL.Add('IF(ppi.DataE=0, "", DATE_FORMAT(ppi.DataE, "%d/%m/%Y")) DATAE_TXT,');
  QrPPICir1.SQL.Add('IF(ppi.DataD=0, "", DATE_FORMAT(ppi.DataD, "%d/%m/%Y")) DATAD_TXT,');
  QrPPICir1.SQL.Add('ppi.DataD, ppi.Cancelado, ppi.Motivo, ppi.ID_Cod1, ppi.ID_Cod2,');
  QrPPICir1.SQL.Add('ppi.ID_Cod3, ppi.ID_Cod4, ppi.CliInt, ppi.Cliente, ppi.Periodo,');
  QrPPICir1.SQL.Add('ppi.Docum, ptc.Nome TAREFA, ptc.Def_Sender, ');
  QrPPICir1.SQL.Add('IF(snd.Tipo=0, snd.RazaoSocial, snd.Nome) DELIVER, ');
  QrPPICir1.SQL.Add('ppi.Valor, ppi.Vencto, ppi.MoraDiaVal, ppi.MultaVal');
  QrPPICir1.SQL.Add('');
  QrPPICir1.SQL.Add('FROM protpakits ppi');
  QrPPICir1.SQL.Add('LEFT JOIN protocolos ptc ON ptc.Codigo=ppi.Codigo');
  QrPPICir1.SQL.Add('LEFT JOIN entidades  snd ON snd.Codigo=ptc.Def_Sender');
  QrPPICir1.SQL.Add('');
  QrPPICir1.SQL.Add('WHERE ppi.Link_ID=' + Geral.FF0(VAR_TIPO_LINK_ID_01_GENERICO)); // ????
  QrPPICir1.SQL.Add('AND ppi.ID_Cod1=' + IntToStr(FID_Cod1));  // Prev
  // Tem necessidade?
  QrPPICir1.SQL.Add('AND ppi.ID_Cod2=' + IntToStr(FID_Cod2));  // Condominio
  //QrPPICir1.SQL.Add('AND ppi.ID_Cod3=:P2');  // Periodo
  //
  //QrPPICir1.Params[00].AsInteger := FID_Cod1;
  //QrPPICir1.Params[01].AsInteger := FID_Cod2;
  //QrPPICir1.Params[02].AsInteger := FID_Cod3;
  //
  QrPPICir1.Open;
  //
end;

procedure TFmCondGerProto.BtEmeioClick(Sender: TObject);
var
  Ordem: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    //FSdoOldNew :=
    UCriar.RecriaTempTable('UnidCond', DmodG.QrUpdPID1, False);
    //
    QrProtoMail.First;
    while not QrProtoMail.Eof do
    begin
      Ordem := QrProtoMail.RecNo;
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'unidcond', False, [
        'Apto', 'Unidade',
        'Proprie', 'Selecio',
        'Entidad', 'Protoco',
        'Bloquet',
        'Data1',
        'Data2',
        'RecipItem', 'RecipNome',
        'RecipEMeio', 'RecipProno',
        'Vencimento', 'Valor',
        'Condominio', 'PreEmeio',
        'IDEmeio'
      ], ['Ordem'], [
        QrProtoMailDepto_Cod.Value, QrProtoMailDepto_Txt.Value,
        QrProtoMailEntid_Txt.Value, MLAGeral.BoolToInt(QrProtoMailDATAE.Value = 0),
        QrProtoMailEntid_Cod.Value, QrProtoMailProtocolo.Value,
        QrProtoMailBloqueto.Value,
        Geral.FDT(QrProtoMailDataE.Value, 1),
        Geral.FDT(QrProtoMailDataD.Value, 1),
        QrProtoMailRecipItem.Value, QrProtoMailRecipNome.Value,
        QrProtoMailRecipEmeio.Value, QrProtoMailRecipProno.Value,
        Geral.FDT(QrProtoMailVencimento.Value, 1), QrProtoMailValor.Value,
        QrProtoMailCondominio.Value, QrProtoMailPreEmeio.Value,
        QrProtoMailIDEmeio.Value
      ], [Ordem], False);
      QrProtoMail.Next;
    end;
    Application.CreateForm(TFmEmailBloqueto, FmEmailBloqueto);
  finally
    Screen.Cursor := crDefault;
  end;
  FmEmailBloqueto.FPrevCodigo := FID_Cod1;
  FmEmailBloqueto.ShowModal;
  FmEmailBloqueto.Destroy;
end;

procedure TFmCondGerProto.PageControl1Change(Sender: TObject);
begin
  case PageControl1.ActivePageIndex of
    0: //UHs cadastrados para entrega do documento
      ReopenProtocolos(1);
    1: //UHs cadastrados para entrega por e-mail
      ReopenProtocolos(2);
    2: //UHs cadastrados para cobran�a registrada
      ReopenProtocolos(4);
  end;
  if PageControl1.ActivePageIndex = 1 then
    MyObjects.Informa2(LaAviso1, LaAviso2, False,
      'D� um duplo clique na grade para abrir o cadastros do propriet�rio!')
  else
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
end;

procedure TFmCondGerProto.PMProtocoloPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  case PageControl1.ActivePageIndex of
      0: Enab := (QrCD1.State <> dsInactive) and (QrCD1.RecordCount > 0);
      1: Enab := (QrProtoMail.State <> dsInactive) and (QrProtoMail.RecordCount > 0);
      2: Enab := (QrCR1.State <> dsInactive) and (QrCR1.RecordCount > 0);
    else Enab := False;
  end;
  Gera1.Enabled              := Enab;
  Desfazprotocolo1.Enabled   := Enab;
  Localizaprotocolo1.Enabled := Enab;
end;

procedure TFmCondGerProto.VerificaEntregaPorEmail(Protocolo: Integer);
  function InsereItem(
        RecipEMeio, RecipNome, RecipProno: String;
        RecipItem: Integer;
        Boleto: Double;
        Apto: Integer; Unidade: String;
        Propriet: Integer;
        NOMEPROPRIET: String;
        PROTOCOD: Integer;
        TAREFA: String;
        DEF_SENDER: Integer;
        DELIVER: String;
        DATAE: TDateTime; DATAE_TXT: String;
        DATAD: TDateTime; DATAD_TXT: String;
        LOTE, PROTOCOLO, Nivel, Item: Integer;
        Vencimento: TDateTime; Valor: Double;
        Condominio: String; PreEmeio, IDEmeio: Integer): Boolean;
  const
    GetNivel: array [0..4] of String = ('Sem emeio', 'Sem protocolo',
    'N�o enviado', 'Aguardando retorno', 'Retornado');
  begin
    try
      UMyMod.SQLInsUpd(DModG.QrUpdPID1, stIns, 'protomail', False, [
        'RecipEMeio', 'RecipNome', 'RecipProno', 'RecipItem',
        'Bloqueto',
        'Depto_Cod', 'Depto_Txt', 'Entid_Cod', 'Entid_Txt',
        'Taref_Cod', 'Taref_Txt', 'Deliv_Cod', 'Deliv_Txt',
        'DataE', 'DataE_Txt', 'DataD', 'DataD_Txt',
        'ProtoLote', 'Protocolo',
        'NivelEmail', 'NivelDescr',
        'Vencimento', 'Valor',
        'Condominio', 'PreEmeio',
        'IDEmeio'
      ], ['Item'], [
        RecipEMeio, RecipNome, RecipProno, RecipItem,
        Boleto,
        Apto, Unidade, Propriet, NOMEPROPRIET,
        PROTOCOD, TAREFA, DEF_SENDER, DELIVER,
        DATAE, DATAE_TXT, DATAD, DATAD_TXT,
        LOTE,PROTOCOLO, Nivel, GetNivel[Nivel],
        Geral.FDT(Vencimento, 1), Valor,
        Condominio, PreEmeio,
        IDEmeio
      ], [Item], False);
      Result := True;
    except
      //Result := False;
      raise;
    end;
  end;
var
  Nivel, Item: Integer;
begin
  Item := 0;
  Screen.Cursor := crHourGlass;
  FVeriEmeio := True;
  //
  ReopenBolMail(Protocolo);//('');
  if QrBolMail.RecordCount > 0 then
  begin
    PB1.Position := 0;
    PB1.Max := QrBolMail.RecordCount;
    //
    UCriar.RecriaTempTable('ProtoMail', DModG.QrUpdPID1, False);
    QrBolMail.First;
    while not QrBolMail.Eof do
    begin
      PB1.Position := PB1.Position + 1;
      // 2009-07-25 desmarquei pois se o protocolo do boloto ainda n�o foi gerado, n�o mostra nada!
      //if QrBolMailPPI_BOLETO.Value = QrBolMailBoleto.Value then
      //begin
        QrCondEmeios.Close;
        QrCondEmeios.Params[0].AsInteger := QrBolMailApto.Value;
        QrCondEmeios.Open;
        if QrCondEmeios.RecordCount > 0 then
        begin
          QrCondEmeios.First;
          while not QrCondEmeios.Eof do
          begin
            QrPPIMail.Close;
            QrPPIMail.Params[00].AsInteger := FID_Cod1;
            QrPPIMail.Params[01].AsInteger := FID_Cod2;
            QrPPIMail.Params[02].AsInteger := QrCondEmeiosItem.Value;
            QrPPIMail.Open;
            //
            Nivel := MLAGeral.BoolToIntDeVarios(
            [
              QrPPIMailDataD.Value > 0,
              QrPPIMailDataE.Value > 0,
              QrPPIMailPROTOCOLO.Value > 0
            ], [4,3,2], 1);
            //
            Inc(Item, 1);
            InsereItem(
              // EMeio
              QrCondEmeiosEMeio.Value,
              QrCondEmeiosNome.Value,
              QrCondEmeiosPronome.Value,
              // ID do cadastro do emeio
              QrCondEmeiosItem.Value,

              //Bloqueto
              QrBolMailBoleto.Value,
              //Apto
              QrBolMailApto.Value, QrBolMailUnidade.Value,
              //Proprietario
              QrBolMailPropriet.Value, QrBolMailNOMEPROPRIET.Value,
              // Tarefa
              QrPPIMailTAREFA_COD.Value,
              QrPPIMailTAREFA_NOM.Value,
              // Deliver
              QrPPIMailDef_Sender.Value,
              QrPPIMailDELIVER.Value,
              // Data entrega
              QrPPIMailDataE.Value, QrPPIMailDATAE_TXT.Value,
              // Data devolu��o
              QrPPIMailDataD.Value, QrPPIMailDATAD_TXT.Value,
              // Lote e protocolo
              QrPPIMailLOTE.Value, QrPPIMailPROTOCOLO.Value,
              // N�vel, ID
              Nivel, Item,
              // Vencto, valor
              QrBolMailVencto.Value, QrBolMailSUB_TOT.Value,
              // Sauda��o e pr�-emeio
              QrCondEmeiosNOMECOND.Value, QrPPIMailPreEmeio.Value,
              // ID do emeio (c�digo do cadastro > campo Item da tabela CondEmeios)
              QrCondEmeiosItem.Value);
            QrCondEmeios.Next;
          end;
        end else begin
          Inc(Item, 1);
          InsereItem(
              // EMeio
              '', '', '', 0,
              //Bloqueto
              QrBolMailBoleto.Value,
              //Apto
              QrBolMailApto.Value, QrBolMailUnidade.Value,
              //Proprietario
              QrBolMailPropriet.Value, QrBolMailNOMEPROPRIET.Value,
              // Tarefa
              QrPPIMailTAREFA_COD.Value,
              QrPPIMailTAREFA_NOM.Value,
              // Deliver
              QrPPIMailDef_Sender.Value,
              QrPPIMailDELIVER.Value,
              // Data entrega
              QrPPIMailDATAE.Value, QrPPIMailDATAE_TXT.Value,
              // Data devolu��o
              QrPPIMailDATAD.Value, QrPPIMailDATAD_TXT.Value,
              // Lote e protocolo
              QrPPIMailLOTE.Value, QrPPIMailPROTOCOLO.Value,
              // N�vel, ID
              0, Item,
              // Vencto, valor
              QrBolMailVencto.Value, QrBolMailSUB_TOT.Value,
              // Sauda��o e pr�-emeio
              '', 0, 0);
        end;
      //end;
      QrBolMail.Next;
    end;
    LaProtoMail.Caption := '';
  end else
  begin
    UCriar.RecriaTempTable('ProtoMail', DModG.QrUpdPID1, False);
    LaProtoMail.Caption := 'Nenhuma UH est� cadastrada para entrega por emeio!';
  end;
  PnProtoMail.Visible := True;
  PB1.Max             := 0;
  PB1.Position        := 0;
  //
  ReopenProtoMails;
  Screen.Cursor := crDefault;
end;

procedure TFmCondGerProto.ReopenBolMail(Protocolo: Integer);//(BOLAPTO: String);
begin
  QrBolMail.Close;
  QrBolMail.SQL.Clear;
  QrBolMail.SQL.Add('SELECT DISTINCT cdi.Andar, ari.Boleto, ari.Apto, ari.Propriet, ari.Vencto,');
  QrBolMail.SQL.Add('cdi.Unidade, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrBolMail.SQL.Add('ELSE ent.Nome END NOMEPROPRIET, ');
  QrBolMail.SQL.Add('IF(cdi.Protocolo <> 0, cdi.Protocolo, IF(cdi.Protocolo2 <> 0, cdi.Protocolo2, cdi.Protocolo3)) + 0.000 PROTOCOD,');
  QrBolMail.SQL.Add('CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO');
  QrBolMail.SQL.Add('FROM ' + FTabAriA + ' ari');
  QrBolMail.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto');
  QrBolMail.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
  QrBolMail.SQL.Add('LEFT JOIN protocolos ptc ON ptc.Codigo=cdi.Protocolo');
  QrBolMail.SQL.Add('LEFT JOIN protocolos pt2 ON pt2.Codigo=cdi.Protocolo2');
  QrBolMail.SQL.Add('LEFT JOIN protocolos pt3 ON pt3.Codigo=cdi.Protocolo3');
  QrBolMail.SQL.Add('WHERE ari.Boleto > 0');
  QrBolMail.SQL.Add('AND (ptc.Tipo=2 OR pt2.Tipo=2 OR pt3.Tipo=2)');
  QrBolMail.SQL.Add('AND ari.Codigo=:P0');
  QrBolMail.SQL.Add('');
  QrBolMail.SQL.Add('UNION');
  QrBolMail.SQL.Add('');
  QrBolMail.SQL.Add('SELECT DISTINCT cdi.Andar, cni.Boleto, cni.Apto, cni.Propriet, cni.Vencto,');
  QrBolMail.SQL.Add('cdi.Unidade, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrBolMail.SQL.Add('ELSE ent.Nome END NOMEPROPRIET, ');
  QrBolMail.SQL.Add('IF(cdi.Protocolo <> 0, cdi.Protocolo, IF(cdi.Protocolo2 <> 0, cdi.Protocolo2, cdi.Protocolo3)) + 0.000 PROTOCOD,');
  QrBolMail.SQL.Add('CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO');
  QrBolMail.SQL.Add('FROM ' + FTabCnsA + '  cni');
  QrBolMail.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto');
  QrBolMail.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet');
  QrBolMail.SQL.Add('LEFT JOIN protocolos ptc ON ptc.Codigo=cdi.Protocolo');
  QrBolMail.SQL.Add('LEFT JOIN protocolos pt2 ON pt2.Codigo=cdi.Protocolo2');
  QrBolMail.SQL.Add('LEFT JOIN protocolos pt3 ON pt3.Codigo=cdi.Protocolo3');
  QrBolMail.SQL.Add('WHERE cni.Boleto > 0');
  QrBolMail.SQL.Add('AND (ptc.Tipo=2 OR pt2.Tipo=2 OR pt3.Tipo=2)');
  QrBolMail.SQL.Add('AND cni.Cond=:P1');
  QrBolMail.SQL.Add('AND cni.Periodo=:P2');
  QrBolMail.SQL.Add('');
  QrBolMail.SQL.Add('ORDER BY Andar, Unidade, Boleto');
  QrBolMail.SQL.Add('');
  //
  QrBolMail.Params[00].AsInteger := FID_Cod1;
  QrBolMail.Params[01].AsInteger := FID_Cod2;
  QrBolMail.Params[02].AsInteger := FPeriodo;
  QrBolMail.Open;
end;

procedure TFmCondGerProto.ReopenProtocolos(Tipo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrProt1, Dmod.MyDB, [
    'SELECT Codigo, Nome ',
    'FROM protocolos ',
    'WHERE Tipo=' + Geral.FF0(Tipo),
    'AND def_client=' + Geral.FF0(FCliEnti),
    '']);
end;

procedure TFmCondGerProto.ReopenProtoMails;
begin
  QrProtoMail.Close;
  QrProtoMail.Open;
end;

procedure TFmCondGerProto.AvisaErroGet;
begin
  Geral.MensagemBox(
  'Aba do controlador de p�ginas sem implementa��o!' +
  'AVISE A DERMATEK!', 'ERRO', MB_OK+MB_ICONERROR);
end;

function TFmCondGerProto.GetTabela: TmySQLQuery;
begin
  case PageControl1.ActivePageIndex of
    0: Result := QrCD1;
    1: Result := QrProtoMail;
    2: Result := QrCR1;
    else Result := nil;
  end;
  if Result = nil then AvisaErroGet;
end;

function TFmCondGerProto.GetNomeTab: String;
begin
  case PageControl1.ActivePageIndex of
    0: Result := 'QrCD1';
    1: Result := 'QrProtoMail';
    2: Result := 'QrCR1';
    else Result := '';
  end;
  if Result = '' then AvisaErroGet;
end;

function TFmCondGerProto.GetGrade: TDBGrid;
begin
  case PageControl1.ActivePageIndex of
    0: Result := DBG_CD1;
    1: Result := TDBGrid(DBG_CE1);
    2: Result := DBG_CR1;
    else Result := nil;
  end;
  if Result = nil then AvisaErroGet;
end;

function TFmCondGerProto.GetTipoProtocolo: Integer;
begin
  case PageControl1.ActivePageIndex of
    0: Result := 1;
    1: Result := 2;
    2: Result := 4;
    else Result := -1;
  end;
  if Result = -1 then AvisaErroGet;
end;

function TFmCondGerProto.GetTarefaProtocolo: Integer;
begin
  case PageControl1.ActivePageIndex of
    0: Result := Trunc(QrCD1PROTOCOD.Value);
    1: Result := 0;
    2: Result := Trunc(QrCR1PROTOCOD.Value);
    else Result := -1;
  end;
  if Result = -1 then AvisaErroGet;
end;

procedure TFmCondGerProto.DBG_CE1DblClick(Sender: TObject);
var
  Cond, CondBloco, CondImov: Integer;
begin
  if (QrProtoMail.State <> dsInactive) and (QrProtoMail.RecordCount > 0) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
      'SELECT Codigo, Controle ',
      'FROM condimov WHERE Conta = ' + Geral.FF0(QrProtoMailDepto_Cod.Value),
      '']);
    if Dmod.QrAux.RecordCount > 0 then
    begin
      Cond      := Dmod.QrAux.FieldByName('Codigo').AsInteger;
      CondBloco := Dmod.QrAux.FieldByName('Controle').AsInteger;
      CondImov  := QrProtoMailDepto_Cod.Value;
      //
      FmPrincipal.CadastroDeCondominio(Cond, CondBloco, CondImov, 0, 1, 8);
      try
        Screen.Cursor := crHourGlass;
        //
        VerificaEntregaPorEmail(QrProt1Codigo.Value);
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmCondGerProto.Desfazprotocolo1Click(Sender: TObject);
var
  Tabela: TmySQLQuery;
  Grade: TDBGrid;
  //TipoP: Integer;
begin
  Tabela := GetTabela;
  Grade  := GetGrade;
  //TipoP  := GetTipoProtocolo;
  if (Tabela <> nil) and (Grade <> nil) then
  begin
    DBCheck.QuaisItens_Exclui(Dmod.QrUpd, Tabela, Grade, 'protpakits',
    ['Protocolo'], ['Conta'], istPergunta, '');
    //ReopenQuery(Tabela, TipoP, '');
    Tabela.Close;
    Tabela.Open;
    //
    case PageControl1.ActivePageIndex of
      0: //UHs cadastrados para entrega do documento
        ReopenProtocolos(1);
      1: //UHs cadastrados para entrega por e-mail
        ReopenProtocolos(2);
      2: //UHs cadastrados para cobran�a registrada
        ReopenProtocolos(4);
    end;
  end;
end;

procedure TFmCondGerProto.Localizaprotocolo1Click(
  Sender: TObject);
var
  TipoProtocolo, Lote: Integer;
begin
  TipoProtocolo := GetTipoProtocolo;
  if TipoProtocolo > 0 then
  begin
    QrProtos.Close;
    QrProtos.Params[00].AsInteger := FID_Cod1;
    QrProtos.Params[01].AsInteger := FID_Cod2;
    QrProtos.Params[02].AsInteger := TipoProtocolo;
    QrProtos.Open;
    //
    Lote := 0;
    case QrProtos.RecordCount of
      0: ;// nada > Lote := 0;
      1: Lote := QrProtosLOTE.Value
      else
      begin
        if DBCheck.CriaFm(TFmCondGerProtoLoc, FmCondGerProtoLoc, afmoNegarComAviso) then
        begin
          FmCondGerProtoLoc.ShowModal;
          if FmCondGerProtoLoc.FLote <> Null then
            Lote := FmCondGerProtoLoc.FLote;
          FmCondGerProtoLoc.Destroy;
        end;
      end;
    end;
    if Lote = 0 then
      Geral.MensagemBox('N�o h� lote definido nesta aba!', 'Aviso',
      MB_OK+MB_ICONERROR)
    else
      FmPrincipal.CadastroDeProtocolos(Lote);
  end;
end;

function TFmCondGerProto.ObtemCondominioDepto(Depto: Integer): Integer;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Dmod.QrAux, Dmod.MyDB, [
  'SELECT con.Cliente ',
  'FROM condimov imv',
  'LEFT JOIN cond con ON con.Codigo = imv.Codigo',
  'WHERE imv.Conta= ' + Geral.FF0(Depto),
  '']);
  if Dmod.QrAux.RecordCount > 0 then
    Result := Dmod.QrAux.FieldByName('Cliente').AsInteger
  else
    Result := 0;
end;

procedure TFmCondGerProto.ReopenQuery(Query: TmySQLQuery; TipoProtocolo: Integer;
  BOLAPTO: String; Protocolo: Integer);
begin
  Screen.Cursor := crHourGlass;
  try
    ReopenBolArrEBolLei();
    ReabreProtPakItsCir1();
    //
    Query.Close;
    Query.SQL.Clear;
    Query.SQL.Add('SELECT DISTINCT cdi.Andar, ari.Boleto, ari.Apto, ari.Propriet, ari.Vencto,');
    Query.SQL.Add('cdi.Unidade, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
    Query.SQL.Add('ELSE ent.Nome END NOMEPROPRIET, ');
    Query.SQL.Add('IF(cdi.Protocolo <> 0, cdi.Protocolo, IF(cdi.Protocolo2 <> 0, cdi.Protocolo2, cdi.Protocolo3)) + 0.000 PROTOCOD, ');
    Query.SQL.Add('CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO');
    Query.SQL.Add('FROM ' + FTabAriA + ' ari');
    Query.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto');
    Query.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
    Query.SQL.Add('LEFT JOIN protocolos ptc ON ptc.Codigo=cdi.Protocolo');
    Query.SQL.Add('LEFT JOIN protocolos pt2 ON pt2.Codigo=cdi.Protocolo2');
    Query.SQL.Add('LEFT JOIN protocolos pt3 ON pt3.Codigo=cdi.Protocolo3');
    Query.SQL.Add('WHERE ari.Boleto > 0');
    Query.SQL.Add('AND (ptc.Tipo='+ Geral.FF0(TipoProtocolo) +' OR pt2.Tipo='+
                   Geral.FF0(TipoProtocolo) +' OR pt3.Tipo='+
                   Geral.FF0(TipoProtocolo) +')');
    Query.SQL.Add('AND ari.Codigo=:P0');
    if Protocolo > -1 then
    begin
      Query.SQL.Add('AND (cdi.Protocolo=' + Geral.FF0(Protocolo) +
        ' OR cdi.Protocolo2=' + Geral.FF0(Protocolo) +
        ' OR cdi.Protocolo3=' + Geral.FF0(Protocolo) + ')');
    end;
    Query.SQL.Add('');
    Query.SQL.Add('UNION');
    Query.SQL.Add('');
    Query.SQL.Add('SELECT DISTINCT cdi.Andar, cni.Boleto, cni.Apto, cni.Propriet, cni.Vencto,');
    Query.SQL.Add('cdi.Unidade, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
    Query.SQL.Add('ELSE ent.Nome END NOMEPROPRIET, ');
    Query.SQL.Add('IF(cdi.Protocolo <> 0, cdi.Protocolo, IF(cdi.Protocolo2 <> 0, cdi.Protocolo2, cdi.Protocolo3)) + 0.000 PROTOCOD, ');
    Query.SQL.Add('CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO');
    Query.SQL.Add('FROM ' + FTabCnsA + ' cni');
    Query.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto');
    Query.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet');
    Query.SQL.Add('LEFT JOIN protocolos ptc ON ptc.Codigo=cdi.Protocolo');
    Query.SQL.Add('LEFT JOIN protocolos pt2 ON pt2.Codigo=cdi.Protocolo2');
    Query.SQL.Add('LEFT JOIN protocolos pt3 ON pt3.Codigo=cdi.Protocolo3');
    Query.SQL.Add('WHERE cni.Boleto > 0');
    Query.SQL.Add('AND (ptc.Tipo='+ Geral.FF0(TipoProtocolo) +' OR pt2.Tipo='+
                   Geral.FF0(TipoProtocolo) +' OR pt3.Tipo='+
                   Geral.FF0(TipoProtocolo) +')');
    Query.SQL.Add('AND cni.Cond=:P1');
    Query.SQL.Add('AND cni.Periodo=:P2');
    if Protocolo > -1 then
    begin
      Query.SQL.Add('AND (cdi.Protocolo=' + Geral.FF0(Protocolo) +
        ' OR cdi.Protocolo2=' + Geral.FF0(Protocolo) +
        ' OR cdi.Protocolo3=' + Geral.FF0(Protocolo) + ')');
    end;
    Query.SQL.Add('');
    Query.SQL.Add('ORDER BY Andar, Unidade, Boleto');
    Query.SQL.Add('');
    //
    Query.Params[00].AsInteger := FID_Cod1;
    Query.Params[01].AsInteger := FID_Cod2;
    Query.Params[02].AsInteger := FPeriodo;
    Query.Open;
    //
    if BOLAPTO <> '' then
      Query.Locate('BOLAPTO', BOLAPTO, []);
    {
    else if PageControl2.ActivePageIndex = 4 then
      Query.Locate('BOLAPTO', FBolSim, [])
    else
      Query.Locate('BOLAPTO', FBolNao, []);
    //
    }
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGerProto.ReopenBolArrEBolLei();
begin
  QrBolArr.Close;
  QrBolArr.SQL.Clear;
  QrBolArr.SQL.Add('SELECT ari.Boleto, ari.Apto, SUM(ari.Valor) Valor,');
  QrBolArr.SQL.Add('CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO');
  QrBolArr.SQL.Add('FROM ' + FTabAriA + ' ari');
  QrBolArr.SQL.Add('WHERE ari.Codigo=:P0');
  QrBolArr.SQL.Add('GROUP BY ari.Boleto, ari.Apto');
  QrBolArr.Params[00].AsInteger := FID_Cod1;
  QrBolArr.Open;
  //
  QrBolLei.Close;
  QrBolLei.SQL.Clear;
  QrBolLei.SQL.Add('SELECT cni.Boleto, cni.Apto, SUM(cni.Valor) Valor,');
  QrBolLei.SQL.Add('CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO');
  QrBolLei.SQL.Add('FROM ' + FTabCnsA + ' cni');
  QrBolLei.SQL.Add('WHERE cni.Cond=:P0');
  QrBolLei.SQL.Add('AND cni.Periodo=:P1');
  QrBolLei.SQL.Add('GROUP BY cni.Boleto, cni.Apto');
  QrBolLei.Params[00].AsInteger := FID_Cod2;
  QrBolLei.Params[01].AsInteger := FPeriodo;
  QrBolLei.Open;
  //
end;

procedure TFmCondGerProto.QrCD1CalcFields(DataSet: TDataSet);
begin
  QrCD1SUB_TOT.Value := QrCD1SUB_ARR.Value + QrCD1SUB_LEI.Value;
end;

procedure TFmCondGerProto.QrProt1AfterScroll(DataSet: TDataSet);
begin
  case PageControl1.ActivePageIndex of
    0:
    begin
      ReopenQuery(QrCD1, 1, '', QrProt1Codigo.Value);
    end;
    1:
    begin
      if not FVeriEmeio then
        VerificaEntregaPorEmail(QrProt1Codigo.Value);
    end;
    2:
    begin
      ReopenQuery(QrCR1, 4, '', QrProt1Codigo.Value);
    end;
  end;
end;

procedure TFmCondGerProto.QrProt1BeforeClose(DataSet: TDataSet);
begin
  QrCD1.Close;
  QrCR1.Close;
end;

procedure TFmCondGerProto.QrCR1CalcFields(DataSet: TDataSet);
begin
  QrCR1SUB_TOT.Value := QrCR1SUB_ARR.Value + QrCR1SUB_LEI.Value;
end;

procedure TFmCondGerProto.QrBolMailBeforeOpen(DataSet: TDataSet);
begin
  ReopenBolArrEBolLei();
end;

procedure TFmCondGerProto.QrBolMailCalcFields(DataSet: TDataSet);
begin
  QrBolMailSUB_TOT.Value :=
  QrBolMailSUB_ARR.Value +
  QrBolMailSUB_LEI.Value;
  //
end;

end.

