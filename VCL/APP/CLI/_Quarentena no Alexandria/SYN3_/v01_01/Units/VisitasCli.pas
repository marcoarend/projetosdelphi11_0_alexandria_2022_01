unit VisitasCli;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, Grids, DBGrids, ComCtrls,
  dmkLabel, dmkDBGrid, Variants, DBCGrids, UnInternalConsts, dmkGeral, dmkImage,
  UnDmkEnums;

type
  TFmVisitasCli = class(TForm)
    Panel1: TPanel;
    QrVisitasCliSin: TmySQLQuery;
    Panel3: TPanel;
    DsVisitasCliSin: TDataSource;
    QrUHs: TmySQLQuery;
    DsUHs: TDataSource;
    QrPropriet: TmySQLQuery;
    DsPropriet: TDataSource;
    TCCont: TTabControl;
    dmkCkCond: TdmkLabel;
    dmkCBCond: TdmkDBLookupComboBox;
    dmkCkPropriet: TdmkLabel;
    dmkEdCBPropriet: TdmkEditCB;
    dmkCBPropriet: TdmkDBLookupComboBox;
    dmkCKUH: TdmkLabel;
    dmkEdCBUH: TdmkEditCB;
    dmkCBUH: TdmkDBLookupComboBox;
    QrCond: TmySQLQuery;
    DsCond: TDataSource;
    QrCondCodigo: TIntegerField;
    QrCondCOND: TWideStringField;
    QrUHsUnidade: TWideStringField;
    dmkEdCBCond: TdmkEditCB;
    QrUHsConta: TIntegerField;
    a: TWideStringField;
    QrProprietPropriet: TIntegerField;
    QrVisitasCliUsu: TmySQLQuery;
    DsVisitasCliUsu: TDataSource;
    QrVisitasCliDono: TmySQLQuery;
    DsVisitasCliDono: TDataSource;
    QrVisitasCliSinControle: TAutoIncField;
    QrVisitasCliSinIP: TWideStringField;
    QrVisitasCliSinDataHora: TDateTimeField;
    QrVisitasCliSinNOME: TWideStringField;
    QrVisitasCliSinTipo: TIntegerField;
    QrVisitasCliDonoControle: TAutoIncField;
    QrVisitasCliDonoIP: TWideStringField;
    QrVisitasCliDonoDataHora: TDateTimeField;
    QrVisitasCliDonoTipo: TIntegerField;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    dmkDBGrid1: TdmkDBGrid;
    dmkDBGrid2: TdmkDBGrid;
    QrVisitasTotUsu: TmySQLQuery;
    DsVisitasTotUsu: TDataSource;
    QrVisitasTotSin: TmySQLQuery;
    DsVisitasTotSin: TDataSource;
    QrVisitasTotDono: TmySQLQuery;
    DsVisitasTotDono: TDataSource;
    QrVisitasTotUsuControle: TAutoIncField;
    QrVisitasTotUsuIP: TWideStringField;
    QrVisitasTotUsuDataHora: TDateTimeField;
    QrVisitasTotUsuNOME: TWideStringField;
    QrVisitasTotUsuNOME2: TWideStringField;
    QrVisitasTotUsuACESSOS: TLargeintField;
    QrVisitasCliUsuControle: TAutoIncField;
    QrVisitasCliUsuIP: TWideStringField;
    QrVisitasCliUsuDataHora: TDateTimeField;
    QrVisitasCliUsuNOME: TWideStringField;
    QrVisitasCliUsuNOME2: TWideStringField;
    QrVisitasCliUsuTipo: TIntegerField;
    QrVisitasCliUsuUsuario: TIntegerField;
    QrVisitasTotSinControle: TAutoIncField;
    QrVisitasTotSinIP: TWideStringField;
    QrVisitasTotSinDataHora: TDateTimeField;
    QrVisitasTotSinNOME: TWideStringField;
    QrVisitasTotSinACESSOS: TLargeintField;
    QrVisitasTotDonoControle: TAutoIncField;
    QrVisitasTotDonoIP: TWideStringField;
    QrVisitasTotDonoDataHora: TDateTimeField;
    QrVisitasTotDonoNOME: TWideStringField;
    QrVisitasTotDonoACESSOS: TLargeintField;
    QrVisitasCliDonoNOME: TWideStringField;
    QrProprietUser_ID: TIntegerField;
    QrUHsUser_ID: TAutoIncField;
    QrCondUser_ID: TAutoIncField;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtPesq: TBitBtn;
    BtSaida: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    ImgWEB: TdmkImage;
    QrVisitasCliUsuUnidade: TWideStringField;
    QrVisitasTotUsuUnidade: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure TCContChange(Sender: TObject);
    procedure dmkEdCBCondChange(Sender: TObject);
    procedure dmkEdCBProprietChange(Sender: TObject);
    procedure dmkCBUHKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdCBUHKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkCBProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkEdCBProprietKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtPesqClick(Sender: TObject);
    procedure dmkEdCBCondKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure dmkCBCondKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    procedure ReopenUHs;
    procedure ReopenPropriet;
    procedure MostraEdicao(Tipo : Integer);
  public
    { Public declarations }
  end;

  var
  FmVisitasCli: TFmVisitasCli;

implementation

uses Module, ModuleGeral, UnMyObjects, UnDmkWeb;

{$R *.DFM}

procedure TFmVisitasCli.BtPesqClick(Sender: TObject);
var
  Tipo, Usuario, Condominio: Integer;
begin
  Usuario := 0;
  BtPesq.Enabled := False;
  case TCCont.TabIndex of
  0:
    begin
      Tipo    := 1;
      if (dmkEdCBPropriet.ValueVariant = 0) and (dmkEdCBUH.ValueVariant = 0) then
        Usuario := 0
      else if dmkEdCBUH.ValueVariant = 0 then
        Usuario := QrProprietUser_ID.Value
      else if dmkEdCBPropriet.ValueVariant = 0 then
        Usuario := QrUHsUser_ID.Value;
      if dmkEdCBCond.ValueVariant = 0 then
        Condominio := 0
      else
        Condominio := dmkEdCBCond.ValueVariant;
      //
      if PageControl1.TabIndex = 0 then
      begin
        QrVisitasTotUsu.Close;
        QrVisitasTotUsu.SQL.Clear;
        QrVisitasTotUsu.SQL.Add('SELECT cli.Controle, cli.IP, cli.DataHora,');
        QrVisitasTotUsu.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME,');
        QrVisitasTotUsu.SQL.Add('IF(eni.Tipo=0, eni.RazaoSocial, eni.Nome) NOME2,');
        QrVisitasTotUsu.SQL.Add('COUNT(cli.Controle) ACESSOS, imv.Unidade');
        QrVisitasTotUsu.SQL.Add('FROM visitascli cli');
        QrVisitasTotUsu.SQL.Add('LEFT JOIN users usu ON usu.User_ID = cli.Usuario');
        QrVisitasTotUsu.SQL.Add('LEFT JOIN condimov imv ON imv.Conta = usu.CodigoEsp');
        QrVisitasTotUsu.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = usu.CodCliEnt');
        QrVisitasTotUsu.SQL.Add('LEFT JOIN entidades eni ON eni.Codigo = usu.CodigoEnt');
        QrVisitasTotUsu.SQL.Add('WHERE cli.Controle > 0');
        if Tipo <> 0 then
          QrVisitasTotUsu.SQL.Add('AND cli.Tipo=' + IntToStr(Tipo));
        if Condominio <> 0 then
          QrVisitasTotUsu.SQL.Add('AND usu.CodCliEsp=' + IntToStr(Condominio));
        if Usuario <> 0 then
          QrVisitasTotUsu.SQL.Add('AND cli.Usuario=' + IntToStr(Usuario));
        QrVisitasTotUsu.SQL.Add('GROUP BY usu.CodigoEnt, usu.CodCliEnt');
        QrVisitasTotUsu.SQL.Add('ORDER BY NOME, NOME2, DataHora DESC');
        QrVisitasTotUsu.Open;
      end else
      begin
        QrVisitasCliUsu.Close;
        QrVisitasCliUsu.SQL.Clear;
        QrVisitasCliUsu.SQL.Add('SELECT cli.Controle, cli.IP, cli.DataHora,');
        QrVisitasCliUsu.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME,');
        QrVisitasCliUsu.SQL.Add('IF(eni.Tipo=0, eni.RazaoSocial, eni.Nome) NOME2,');
        QrVisitasCliUsu.SQL.Add('cli.Tipo, cli.Usuario, imv.Unidade');
        QrVisitasCliUsu.SQL.Add('FROM visitascli cli');
        QrVisitasCliUsu.SQL.Add('LEFT JOIN users usu ON usu.User_ID = cli.Usuario');
        QrVisitasTotUsu.SQL.Add('LEFT JOIN condimov imv ON imv.Conta = usu.CodigoEsp');
        QrVisitasCliUsu.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = usu.CodCliEnt');
        QrVisitasCliUsu.SQL.Add('LEFT JOIN users usr ON usr.User_ID = cli.Usuario');
        QrVisitasCliUsu.SQL.Add('LEFT JOIN entidades eni ON eni.Codigo = usu.CodigoEnt');
        QrVisitasCliUsu.SQL.Add('WHERE cli.Controle > 0');
        if Tipo <> 0 then
          QrVisitasCliUsu.SQL.Add('AND cli.Tipo=' + IntToStr(Tipo));
        if Condominio <> 0 then
          QrVisitasCliUsu.SQL.Add('AND usu.CodCliEsp=' + IntToStr(Condominio));
        if Usuario <> 0 then
          QrVisitasCliUsu.SQL.Add('AND cli.Usuario=' + IntToStr(Usuario));
        QrVisitasCliUsu.SQL.Add('ORDER BY NOME, NOME2, DataHora DESC');
        QrVisitasCliUsu.Open;
      end;
    end;
  1:
    begin
      Tipo    := 7;
      if dmkEdCBCond.ValueVariant = 0 then
        Usuario := 0
      else
        Usuario := QrCondUser_ID.Value;
      //
      if PageControl1.TabIndex = 0 then
      begin
        QrVisitasTotSin.Close;
        QrVisitasTotSin.SQL.Clear;
        QrVisitasTotSin.SQL.Add('SELECT cli.Controle, cli.IP, cli.DataHora,');
        QrVisitasTotSin.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME,');
        QrVisitasTotSin.SQL.Add('COUNT(cli.Controle) ACESSOS');
        QrVisitasTotSin.SQL.Add('FROM visitascli cli');
        QrVisitasTotSin.SQL.Add('LEFT JOIN wclients wcl ON wcl.User_ID = cli.Usuario');
        QrVisitasTotSin.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = wcl.CodCliEnt');
        QrVisitasTotSin.SQL.Add('WHERE cli.Controle > 0');        
        if Tipo <> 0 then
          QrVisitasTotSin.SQL.Add('AND cli.Tipo=' + IntToStr(Tipo));
        if Usuario <> 0 then
          QrVisitasTotSin.SQL.Add('AND cli.Usuario=' + IntToStr(Usuario));
        QrVisitasTotSin.SQL.Add('GROUP BY wcl.CodCliEnt');
        QrVisitasTotSin.SQL.Add('ORDER BY NOME, DataHora DESC');
        QrVisitasTotSin.Open;
      end else
      begin
        QrVisitasCliSin.Close;
        QrVisitasCliSin.SQL.Clear;
        QrVisitasCliSin.SQL.Add('SELECT cli.Controle, cli.IP, cli.DataHora,');
        QrVisitasCliSin.SQL.Add('IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOME,');
        QrVisitasCliSin.SQL.Add('cli.Tipo');
        QrVisitasCliSin.SQL.Add('FROM visitascli cli');
        QrVisitasCliSin.SQL.Add('LEFT JOIN wclients wcl ON wcl.User_ID = cli.Usuario');
        QrVisitasCliSin.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = wcl.CodCliEnt');
        QrVisitasCliSin.SQL.Add('WHERE cli.Controle > 0');
        if Tipo <> 0 then
          QrVisitasCliSin.SQL.Add('AND cli.Tipo=' + IntToStr(Tipo));
        if Usuario <> 0 then
          QrVisitasCliSin.SQL.Add('AND cli.Usuario=' + IntToStr(Usuario));
        QrVisitasCliSin.SQL.Add('ORDER BY NOME, DataHora DESC');
        QrVisitasCliSin.Open;
      end;
    end;
  2:
    begin
      Tipo := 3;
      if dmkEdCBCond.ValueVariant = 0 then
        Usuario := 0
      else
        Usuario := QrCondUser_ID.Value;
      //
      if PageControl1.TabIndex = 0 then
      begin
        QrVisitasTotDono.Close;
        QrVisitasTotDono.SQL.Clear;
        QrVisitasTotDono.SQL.Add('SELECT cli.Controle, cli.IP,');
        QrVisitasTotDono.SQL.Add('cli.DataHora, gri.Nome NOME, COUNT(cli.Controle) ACESSOS');
        QrVisitasTotDono.SQL.Add('FROM visitascli cli');
        QrVisitasTotDono.SQL.Add('LEFT JOIN condgri gri ON gri.Codigo = cli.Usuario');
        QrVisitasTotDono.SQL.Add('WHERE cli.Controle > 0');
        if Tipo <> 0 then
          QrVisitasTotDono.SQL.Add('AND cli.Tipo=' + IntToStr(Tipo));
        if Usuario <> 0 then
          QrVisitasTotDono.SQL.Add('AND cli.Usuario=' + IntToStr(Usuario));
        QrVisitasTotDono.SQL.Add('GROUP BY gri.Nome');
        QrVisitasTotDono.SQL.Add('ORDER BY gri.Nome');
        QrVisitasTotDono.Open;
      end else
      begin
        QrVisitasCliDono.Close;
        QrVisitasCliDono.SQL.Clear;
        QrVisitasCliDono.SQL.Add('SELECT cli.Controle, cli.IP, cli.DataHora,');
        QrVisitasCliDono.SQL.Add('cli.Tipo, gri.Nome NOME');
        QrVisitasCliDono.SQL.Add('FROM visitascli cli');
        QrVisitasCliDono.SQL.Add('LEFT JOIN condgri gri ON gri.Codigo = cli.Usuario');
        QrVisitasCliDono.SQL.Add('WHERE cli.Controle > 0');
        if Tipo <> 0 then
          QrVisitasCliDono.SQL.Add('AND cli.Tipo=' + IntToStr(Tipo));
        if Usuario <> 0 then
          QrVisitasCliDono.SQL.Add('AND cli.Usuario=' + IntToStr(Usuario));
        QrVisitasCliDono.SQL.Add('ORDER BY NOME, DataHora DESC');
        QrVisitasCliDono.Open;
      end;
    end;
  end;
  BtPesq.Enabled := True;
end;

procedure TFmVisitasCli.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmVisitasCli.dmkCBCondKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if dmkCkCond.Caption <> 'Condom�nio:' then
  begin
    if key = VK_F4 then
    begin
      dmkEdCBCond.ValueVariant := 0;
      dmkCBCond.KeyValue       := NULL;
    end;
  end;
end;

procedure TFmVisitasCli.dmkCBProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenPropriet;
end;

procedure TFmVisitasCli.dmkCBUHKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_DELETE then
  begin
    dmkEdCBUH.ValueVariant := 0;
    dmkCBUH.KeyValue       := NULL;
  end;
end;

procedure TFmVisitasCli.dmkEdCBCondChange(Sender: TObject);
begin
  dmkEdCBPropriet.ValueVariant := 0;
  dmkCBPropriet.KeyValue       := NULL;
  dmkEdCBUH.ValueVariant       := 0;
  dmkCBUH.KeyValue             := Null;
  //
  if dmkEdCBCond.ValueVariant <> 0 then
  begin
    ReopenPropriet;
    ReopenUHs;
  end;
end;

procedure TFmVisitasCli.dmkEdCBCondKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if dmkCkCond.Caption <> 'Condom�nio:' then
  begin
    if key = VK_F4 then
    begin
      dmkEdCBCond.ValueVariant := 0;
      dmkCBCond.KeyValue       := NULL;
    end;
  end;
end;

procedure TFmVisitasCli.dmkEdCBProprietChange(Sender: TObject);
begin
  ReopenUHs;
end;

procedure TFmVisitasCli.dmkEdCBProprietKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then ReopenPropriet;
end;

procedure TFmVisitasCli.dmkEdCBUHKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if key = VK_DELETE then
  begin
    dmkEdCBUH.ValueVariant := 0;
    dmkCBUH.KeyValue       := NULL;
  end;
end;

procedure TFmVisitasCli.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmVisitasCli.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmVisitasCli.MostraEdicao(Tipo: Integer);
begin
  Screen.Cursor := crHourglass;
  //
  case Tipo of
  0:
    begin
      dmkCkCond.Caption            := 'Condom�nio [F4 mostra todos]:';
      dmkEdCBCond.ValueVariant     := 0;
      dmkCBCond.KeyValue           := NULL;
      dmkCkPropriet.Visible        := True;
      dmkEdCBPropriet.Visible      := True;
      dmkCBPropriet.Visible        := True;
      dmkEdCBPropriet.ValueVariant := 0;
      dmkCBPropriet.KeyValue       := NULL;
      dmkCKUH.Visible              := True;
      dmkEdCBUH.Visible            := True;
      dmkCBUH.Visible              := True;
      dmkEdCBUH.ValueVariant       := 0;
      dmkCBUH.KeyValue             := NULL;
      //
      dmkDBGrid1.Columns[2].Title.Caption := DModG.ReCaptionTexto(VAR_C_O_N_D_O_M_I_N_I_O);
      dmkDBGrid1.Columns[2].FieldName     := 'NOME';
      dmkDBGrid1.Columns[2].Visible       := True;
      dmkDBGrid1.Columns[3].Title.Caption := DModG.ReCaptionTexto(VAR_M_O_R_A_D_O_R);
      dmkDBGrid1.Columns[3].FieldName     := 'NOME2';
      dmkDBGrid1.Columns[3].Visible       := True;
      dmkDBGrid1.DataSource               := DsVisitasCliUsu;
      //
      dmkDBGrid2.Columns[3].Title.Caption := DModG.ReCaptionTexto(VAR_C_O_N_D_O_M_I_N_I_O);
      dmkDBGrid2.Columns[3].FieldName     := 'NOME';
      dmkDBGrid2.Columns[3].Visible       := True;
      dmkDBGrid2.Columns[4].Title.Caption := DModG.ReCaptionTexto(VAR_M_O_R_A_D_O_R);
      dmkDBGrid2.Columns[4].FieldName     := 'NOME2';
      dmkDBGrid2.Columns[4].Visible       := True;
      dmkDBGrid2.DataSource               := DsVisitasTotUsu;
      //
      QrUHs.Close;
      QrPropriet.Close;
      QrVisitasCliSin.Close;
      //
      QrCond.Close;
      QrCond.SQL.Clear;
      QrCond.SQL.Add('SELECT con.Codigo, usu.User_ID,');
      QrCond.SQL.Add('IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) COND');
      QrCond.SQL.Add('FROM cond con');
      QrCond.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = con.Cliente');
      QrCond.SQL.Add('LEFT JOIN users usu ON usu.CodCliEsp = con.Codigo');
      QrCond.SQL.Add('WHERE con.Ativo=1');
      QrCond.SQL.Add('AND usu.CodCliEsp = con.Codigo');
      QrCond.SQL.Add('GROUP BY usu.CodCliEsp');
      QrCond.SQL.Add('ORDER BY COND');
      QrCond.Open;
    end;
  1:
    begin
      dmkCkCond.Caption            := 'Condom�nio [F4 mostra todos]:';
      dmkEdCBCond.ValueVariant     := 0;
      dmkCBCond.KeyValue           := NULL;
      dmkCkPropriet.Visible        := False;
      dmkEdCBPropriet.Visible      := False;
      dmkCBPropriet.Visible        := False;
      dmkEdCBPropriet.ValueVariant := 0;
      dmkCBPropriet.KeyValue       := NULL;
      dmkCKUH.Visible              := False;
      dmkEdCBUH.Visible            := False;
      dmkCBUH.Visible              := False;
      dmkEdCBUH.ValueVariant       := 0;
      dmkCBUH.KeyValue             := NULL;
      //
      dmkDBGrid1.Columns[2].Title.Caption := 'Condom�nio';
      dmkDBGrid1.Columns[2].FieldName     := 'NOME';
      dmkDBGrid1.Columns[2].Visible       := True;
      dmkDBGrid1.Columns[3].FieldName     := '';
      dmkDBGrid1.Columns[3].Visible       := False;
      dmkDBGrid1.DataSource               := DsVisitasCliSin;
      //
      dmkDBGrid2.Columns[3].Title.Caption := 'Condom�nio';
      dmkDBGrid2.Columns[3].FieldName     := 'NOME';
      dmkDBGrid2.Columns[3].Visible       := True;
      dmkDBGrid2.Columns[4].FieldName     := '';
      dmkDBGrid2.Columns[4].Visible       := False;
      dmkDBGrid2.DataSource               := DsVisitasTotSin;
      //
      QrUHs.Close;
      QrPropriet.Close;
      QrVisitasCliSin.Close;
      //
      QrCond.Close;
      QrCond.SQL.Clear;
      QrCond.SQL.Add('SELECT con.Codigo, wcl.User_ID,');
      QrCond.SQL.Add('IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) COND');
      QrCond.SQL.Add('FROM cond con');
      QrCond.SQL.Add('LEFT JOIN wclients wcl ON con.Codigo = wcl.CodCliEsp');
      QrCond.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = con.Cliente');
      QrCond.SQL.Add('WHERE con.Ativo=1');
      QrCond.SQL.Add('AND con.Codigo = wcl.CodCliEsp');
      QrCond.SQL.Add('ORDER BY COND');
      QrCond.Open;
    end;
  2:
    begin
      dmkCkCond.Caption            := 'Grupo de UHs / Cond�minos [F4 mostra todos]:';
      dmkEdCBCond.ValueVariant     := 0;
      dmkCBCond.KeyValue           := NULL;
      dmkCkPropriet.Visible        := False;
      dmkEdCBPropriet.Visible      := False;
      dmkCBPropriet.Visible        := False;
      dmkEdCBPropriet.ValueVariant := 0;
      dmkCBPropriet.KeyValue       := NULL;
      dmkCKUH.Visible              := False;
      dmkEdCBUH.Visible            := False;
      dmkCBUH.Visible              := False;
      dmkEdCBUH.ValueVariant       := 0;
      dmkCBUH.KeyValue             := NULL;
      //
      dmkDBGrid1.Columns[2].Title.Caption := 'Grupo';
      dmkDBGrid1.Columns[2].FieldName     := 'NOME';
      dmkDBGrid1.Columns[2].Visible       := True;
      dmkDBGrid1.Columns[3].FieldName     := '';
      dmkDBGrid1.Columns[3].Visible       := False;
      dmkDBGrid1.DataSource               := DsVisitasCliDono;
      //
      dmkDBGrid2.Columns[3].Title.Caption := 'Grupo';
      dmkDBGrid2.Columns[3].FieldName     := 'NOME';
      dmkDBGrid2.Columns[3].Visible       := True;
      dmkDBGrid2.Columns[4].FieldName     := '';
      dmkDBGrid2.Columns[4].Visible       := False;
      dmkDBGrid2.DataSource               := DsVisitasTotDono;
      //
      QrVisitasCliDono.Close;
      //
      QrCond.Close;
      QrCond.SQL.Clear;
      QrCond.SQL.Add('SELECT gri.Codigo, gri.Nome COND, gri.Codigo USER_ID');
      QrCond.SQL.Add('FROM condgri gri');
      QrCond.SQL.Add('ORDER BY gri.Nome');
      QrCond.Open;
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmVisitasCli.ReopenPropriet;
var
  Cond: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  if TCCont.TabIndex = 0 then
  begin
    dmkEdCBPropriet.ValueVariant := 0;
    dmkCBPropriet.KeyValue       := NULL;
    dmkEdCBUH.ValueVariant       := 0;
    dmkCBUH.KeyValue             := NULL;
    //
    Cond := dmkEdCBCond.ValueVariant;
    QrPropriet.Close;
    if Cond <> 0 then
    begin
      QrPropriet.Params[00].AsInteger := Cond;
      QrPropriet.Open;
    end;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmVisitasCli.ReopenUHs;
var
  Cond, Propriet: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  dmkEdCBUH.ValueVariant := 0;
  dmkCBUH.KeyValue       := NULL;
  //
  if TCCont.TabIndex = 0 then
  begin
    Cond     := Geral.IMV(dmkEdCBCond.ValueVariant);
    Propriet := Geral.IMV(dmkEdCBPropriet.ValueVariant);
    //
    QrUHs.Close;
    QrUHs.SQL.Clear;
    QrUHs.SQL.Add('SELECT imv.Conta, imv.Unidade, usu.User_ID');
    QrUHs.SQL.Add('FROM condimov imv');
    QrUHs.SQL.Add('LEFT JOIN users usu ON usu.CodigoEnt=imv.Propriet');
    QrUHs.SQL.Add('AND usu.CodigoEsp=imv.Conta');
    QrUHs.SQL.Add('AND (usu.Tipo=1 OR usu.Tipo IS NULL)');
    QrUHs.SQL.Add('WHERE imv.Codigo<>0');
    QrUHs.SQL.Add('AND imv.Propriet<>0');
    if Cond <> 0 then
      QrUHs.SQL.Add('AND imv.Codigo=' + IntToStr(Cond));
    if Propriet <> 0 then
      QrUHs.SQL.Add('AND imv.Propriet=' + IntToStr(Propriet));
    QrUHs.SQL.Add('ORDER BY imv.Unidade');
    QrUHs.Open;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmVisitasCli.TCContChange(Sender: TObject);
begin
  Screen.Cursor := crHourglass;
  //
  case TCCont.TabIndex of
  0:
    MostraEdicao(0);
  1:
    MostraEdicao(1);
  2:
    MostraEdicao(2);
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmVisitasCli.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  QrCond.Open;
  PageControl1.TabIndex := 0;
  dmkDBGrid1.DataSource := DsVisitasCliUsu;
  dmkDBGrid2.DataSource := DsVisitasTotUsu;
  //
  MostraEdicao(0);
end;

end.

