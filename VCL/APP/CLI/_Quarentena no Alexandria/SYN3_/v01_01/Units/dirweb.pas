unit dirweb;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Grids,
  DBGrids, ComCtrls, dmkGeral, dmkEdit, UnDmkProcFunc, dmkImage, dmkDBGrid,
  UnDmkEnums, IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient,
  IdExplicitTLSClientServerBase, IdFTP;

type
  TFmDirWeb = class(TForm)
    PainelDados: TPanel;
    PainelEdita: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    EdCodigo: TdmkEdit;
    Label149: TLabel;
    EdDescri: TdmkEdit;
    Label1: TLabel;
    QrDirWeb: TmySQLQuery;
    DsDirWeb: TDataSource;
    EdNome: TdmkEdit;
    RGNivel: TRadioGroup;
    Label2: TLabel;
    EdPasta: TdmkEdit;
    QrPesqUploads: TmySQLQuery;
    DsPesqUploads: TDataSource;
    QrPesqUploadsCodigo: TAutoIncField;
    QrPesqUploadsNome: TWideStringField;
    QrPesqUploadsArquivo: TWideStringField;
    QrPesqUploadsCond: TIntegerField;
    QrPesqUploadsLk: TIntegerField;
    QrPesqUploadsDataCad: TDateField;
    QrPesqUploadsDataAlt: TDateField;
    QrPesqUploadsUserCad: TIntegerField;
    QrPesqUploadsUserAlt: TIntegerField;
    QrPesqUploadsAlterWeb: TSmallintField;
    QrPesqUploadsAtivo: TSmallintField;
    QrPesqUploadsDirWeb: TIntegerField;
    QrPesqUploadsDIRCODIGO: TAutoIncField;
    QrDirWebCodigo: TAutoIncField;
    QrDirWebNome: TWideStringField;
    QrDirWebDescri: TWideStringField;
    QrDirWebPasta: TWideStringField;
    QrDirWebNivel: TIntegerField;
    QrDirWebLk: TIntegerField;
    QrDirWebDataCad: TDateField;
    QrDirWebDataAlt: TDateField;
    QrDirWebUserCad: TIntegerField;
    QrDirWebUserAlt: TIntegerField;
    QrDirWebAlterWeb: TSmallintField;
    QrDirWebAtivo: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GroupBox1: TGroupBox;
    Panel2: TPanel;
    Panel3: TPanel;
    BtConfirma: TBitBtn;
    BtDesiste: TBitBtn;
    GBRodaPe: TGroupBox;
    Panel1: TPanel;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    dmkDBGrid1: TdmkDBGrid;
    QrDirWebNOMENIVEL: TWideStringField;
    ImgWEB: TdmkImage;
    IdFTP1: TIdFTP;
    QrFTPConfig: TmySQLQuery;
    procedure BtIncluiClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure IpFtpClient1FtpError(Sender: TObject; ErrorCode: Integer;
      const Error: String);
    //procedure IpFtpClient1FtpStatus(Sender: TObject;
      //StatusCode: TIpFtpStatusCode; const Info: String);
    procedure EdPastaExit(Sender: TObject);
  private
    //FDesconectando: Boolean;
    procedure DefineONomeDoForm;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
    procedure ReopenDirWeb(Codigo: Integer);
  public
    { Public declarations }
  end;

var
  FmDirWeb: TFmDirWeb;
const
  FFormatFloat = '00000';

implementation

uses Module, uploads, UnMyObjects, UnDmkWeb, UnFTP;

{$R *.DFM}

procedure TFmDirWeb.MostraEdicao(Mostra : Boolean; SQLType: TSQLType; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelEdita.Visible := True;
    PainelDados.Visible := False;
    //
    if SQLType = stIns then
    begin
      EdCodigo.ValueVariant := 0;
      EdNome.ValueVariant   := '';
      EdPasta.ValueVariant  := '';
      EdPasta.Enabled       := True;
      EdDescri.ValueVariant := '';
      RGNivel.ItemIndex := 0;
    end else begin
      EdCodigo.ValueVariant := IntToStr(QrDirWebCodigo.Value);
      EdNome.ValueVariant   := QrDirWebNome.Value;
      EdPasta.ValueVariant  := QrDirWebPasta.Value;
      EdPasta.Enabled       := False;
      EdDescri.ValueVariant := QrDirWebDescri.Value;
      //
      case QrDirWebNivel.Value of
        0: RGNivel.ItemIndex := 0;
        1: RGNivel.ItemIndex := 1;
        7: RGNivel.ItemIndex := 2;
       -1: RGNivel.ItemIndex := 3;
      end;
    end;
    EdNome.SetFocus;
  end else begin
    PainelDados.Visible := True;
    PainelEdita.Visible := False;
  end;
  ImgTipo.SQLType := SQLType;
  GOTOy.BotoesSb(ImgTipo.SQLType);
end;

procedure TFmDirWeb.ReopenDirWeb(Codigo: Integer);
begin
  UMyMod.AbreQuery(QrDirWeb, Dmod.MyDBn);
  //
  if Codigo <> 0 then
    QrDirWeb.Locate('Codigo', Codigo, []);
end;

procedure TFmDirWeb.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmDirWeb.BtIncluiClick(Sender: TObject);
begin
  MostraEdicao(True, stIns, 0);
end;

procedure TFmDirWeb.BtSaidaClick(Sender: TObject);
begin
  VAR_SERVICOG := QrDirWebCodigo.Value;
  Close;
end;

procedure TFmDirWeb.BtConfirmaClick(Sender: TObject);
var
  Raiz, Caminho, Nome, Descri, Pasta: String;
  Codigo, Nivel: Integer;
  Continua: Boolean;
begin
  UFTP.ReopenFTPConfig(-1, QrFTPConfig, Dmod.MyDB);
  //
  Continua := False;
  Codigo   := EdCodigo.ValueVariant;
  Nome     := EdNome.ValueVariant;
  Descri   := EdDescri.ValueVariant;
  Pasta    := EdPasta.ValueVariant;
  Nivel    := RGNivel.ItemIndex;
  Raiz     := QrFTPConfig.FieldByName('Web_Raiz').AsString;
  //
  if MyObjects.FIC(Length(Nome)   = 0, EdNome,   'Defina um nome!') then Exit;
  if MyObjects.FIC(Length(Pasta)  = 0, EdPasta,  'Defina um nome para a pasta!') then Exit;
  if MyObjects.FIC(Length(Descri) = 0, EdDescri, 'Defina uma descri��o!') then Exit;
  //
  if Nivel = 0 then // Ambos
    Nivel := 0
  else if Nivel = 1 then // Cod�mino
    Nivel := 1
  else if Nivel = 2 then // S�ndico
    Nivel := 7
  else if Nivel = 3 then // Nenhum
    Nivel := -1;
  //
  if ImgTipo.SQLType = stIns then
  begin
    try
      Screen.Cursor := crHourGlass;
      //
      UFTP.ConectaServidorFTP(QrFTPConfig.FieldByName('Web_FTPh').AsString,
        QrFTPConfig.FieldByName('Web_FTPu').AsString,
        QrFTPConfig.FieldByName('SENHA').AsString,
        Geral.IntToBool(QrFTPConfig.FieldByName('Passivo').AsInteger), IdFTP1);
        //
      if IdFTP1.Connected then
      begin
        Caminho := Raiz + '/_local_/pdf/' + Pasta;
        //
        Continua := UFTP.CriaDiretorio(Caminho, IdFTP1);
        if not Continua then
        begin
          Geral.MB_Aviso('Falha ao criar o diret�rio no servidor FTP!');
          Exit;
        end;
      end;
    finally
      IdFTP1.Disconnect;
      Screen.Cursor := crDefault;
    end;
  end else
    Continua := True;
  //Salva no banco de dados
  if Continua then
  begin
    if ImgTipo.SQLType = stIns then
    begin
      Continua := UMyMod.SQLInsUpd(Dmod.QrUpdN, ImgTipo.SQLType, 'dirweb', True,
        ['Nome', 'Descri', 'Pasta', 'Nivel'], [],
        [Nome, Descri, Pasta, Nivel], [], True);
      //
      if Continua then
        Codigo := Dmod.QrUpdN.LastInsertID;
    end else
    begin
      Continua := UMyMod.SQLInsUpd(Dmod.QrUpdN, ImgTipo.SQLType, 'dirweb', False,
        ['Nome', 'Descri', 'Pasta', 'Nivel'], ['Codigo'],
        [Nome, Descri, Pasta, Nivel], [Codigo], True);
    end;
    ReopenDirWeb(Codigo);
    MostraEdicao(False, stLok, 0);
  end;
end;

procedure TFmDirWeb.BtDesisteClick(Sender: TObject);
begin
  MostraEdicao(False, stLok, 0);
end;

procedure TFmDirWeb.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  ReopenDirWeb(0);
  //
  dmkDBGrid1.Align  := alClient;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  //
  DefineONomeDoForm;
end;

procedure TFmDirWeb.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(ImgTipo.SQLType);
end;

procedure TFmDirWeb.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  DmkWeb.CarregaImagemFormWEB(ImgWEB);
end;

procedure TFmDirWeb.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmDirWeb.BtAlteraClick(Sender: TObject);
begin
  MostraEdicao(True, stUpd, 0);
end;

procedure TFmDirWeb.BtExcluiClick(Sender: TObject);
var
  Codigo: Integer;
  Host, User, Pass, Raiz, Caminho: String;
  Passivo: Boolean;
begin
  if (QrDirWeb.State = dsInactive) or (QrDirWeb.RecordCount = 0) then
    Exit;
  //Verifica se existem arquivos
  QrPesqUploads.Close;
  QrPesqUploads.Params[0].AsInteger := QrDirWebCodigo.Value;
  QrPesqUploads.Open;
  if QrPesqUploads.RecordCount > 0 then
  begin
    Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
      'Motivo: O diret�rio n�o est� vazio.');
    Exit;
  end;
  //
  if Geral.MB_Pergunta('Confirma a exclus�o do diret�rio ' +
    QrDirWebNome.Value + '?' + sLineBreak +
    'ATEN��O: Ao excluir o diret�rio, este tamb�m ser� exclu�do do servidor FTP!') = ID_YES then
  begin
    UFTP.ReopenFTPConfig(-1, QrFTPConfig, Dmod.MyDB);
    //
    Raiz    := QrFTPConfig.FieldByName('Web_Raiz').AsString;
    Caminho := Raiz + '/_local_/pdf/' + QrDirWebPasta.Value;
    Host    := QrFTPConfig.FieldByName('Web_FTPh').AsString;
    User    := QrFTPConfig.FieldByName('Web_FTPu').AsString;
    Pass    := QrFTPConfig.FieldByName('SENHA').AsString;
    Passivo := Geral.IntToBool(QrFTPConfig.FieldByName('Passivo').AsInteger);
    //
    UFTP.ConectaServidorFTP(Host, User, Pass, Passivo, IdFTP1);
    //
    if IdFTP1.Connected then
    begin
      //Verifica se arquivo existe no servidor FTP
      if DmkWeb.VerificaSeArquivoFTPExiteServidor(Caminho, Host, User, Pass, Passivo) then
      begin
        IdFTP1.RemoveDir(Caminho);
        try
          Screen.Cursor := crHourGlass;
          //
          Codigo := QrDirWebCodigo.Value;
          //
          Dmod.QrWeb.SQL.Clear;
          Dmod.QrWeb.SQL.Add('DELETE FROM dirweb WHERE Codigo=:P0');
          Dmod.QrWeb.Params[0].AsInteger := Codigo;
          Dmod.QrWeb.ExecSQL;
          //
          ReopenDirWeb(0);
        finally
          Screen.Cursor := crDefault;
        end;
      end else
        Geral.MB_Aviso('Exclus�o abortada!' + sLineBreak +
          'Motivo: O diret�rio n�o foi localizado.');
      IdFTP1.Disconnect;
    end;
  end;
end;

procedure TFmDirWeb.IpFtpClient1FtpError(Sender: TObject;
  ErrorCode: Integer; const Error: String);
begin
  MessageDlg(Error, mtError, [mbOK], 0);
  Close;
end;

procedure TFmDirWeb.EdPastaExit(Sender: TObject);
var
  Pasta: String;
begin
  Pasta := Geral.SemAcento(EdPasta.ValueVariant);
  Pasta := Geral.Substitui(Pasta, ' ', '_');
  //
  EdPasta.ValueVariant := Pasta;  
end;

end.


