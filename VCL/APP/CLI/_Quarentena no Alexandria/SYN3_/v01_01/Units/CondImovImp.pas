unit CondImovImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, frxClass, frxDBSet, Db, mySQLDbTables, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, Grids, DBGrids, dmkDBGrid, ComCtrls,
  dmkImage, UnDmkEnums;

type
  TFmCondImovImp = class(TForm)
    Panel1: TPanel;
    frx_000A: TfrxReport;
    QrImoveis: TmySQLQuery;
    QrImoveisNOMEPROP: TWideStringField;
    QrImoveisNOMECONJUGE: TWideStringField;
    QrImoveisSTATUS1: TWideStringField;
    QrImoveisNOMEEMP1: TWideStringField;
    QrImoveisNOMEEMP2: TWideStringField;
    QrImoveisNOMEEMP3: TWideStringField;
    QrImoveisIMOB: TWideStringField;
    QrImoveisCodigo: TIntegerField;
    QrImoveisControle: TIntegerField;
    QrImoveisConta: TIntegerField;
    QrImoveisPropriet: TIntegerField;
    QrImoveisConjuge: TIntegerField;
    QrImoveisAndar: TIntegerField;
    QrImoveisUnidade: TWideStringField;
    QrImoveisQtdGaragem: TIntegerField;
    QrImoveisStatus: TIntegerField;
    QrImoveisENome1: TIntegerField;
    QrImoveisEHoraSSIni1: TTimeField;
    QrImoveisEHoraSSSai1: TTimeField;
    QrImoveisEHoraSDIni1: TTimeField;
    QrImoveisEHoraSDSai1: TTimeField;
    QrImoveisESegunda1: TSmallintField;
    QrImoveisETerca1: TSmallintField;
    QrImoveisEQuarta1: TSmallintField;
    QrImoveisEQuinta1: TSmallintField;
    QrImoveisESexta1: TSmallintField;
    QrImoveisESabado1: TSmallintField;
    QrImoveisEDomingo1: TSmallintField;
    QrImoveisEVeic1: TSmallintField;
    QrImoveisEFilho1: TSmallintField;
    QrImoveisENome2: TIntegerField;
    QrImoveisEHoraSSIni2: TTimeField;
    QrImoveisEHoraSSSai2: TTimeField;
    QrImoveisEHoraSDIni2: TTimeField;
    QrImoveisEHoraSDSai2: TTimeField;
    QrImoveisESegunda2: TSmallintField;
    QrImoveisETerca2: TSmallintField;
    QrImoveisEQuarta2: TSmallintField;
    QrImoveisEQuinta2: TSmallintField;
    QrImoveisESexta2: TSmallintField;
    QrImoveisESabado2: TSmallintField;
    QrImoveisEDomingo2: TSmallintField;
    QrImoveisEVeic2: TSmallintField;
    QrImoveisEFilho2: TSmallintField;
    QrImoveisENome3: TIntegerField;
    QrImoveisEHoraSSIni3: TTimeField;
    QrImoveisEHoraSSSai3: TTimeField;
    QrImoveisEHoraSDIni3: TTimeField;
    QrImoveisEHoraSDSai3: TTimeField;
    QrImoveisESegunda3: TSmallintField;
    QrImoveisETerca3: TSmallintField;
    QrImoveisEQuarta3: TSmallintField;
    QrImoveisEQuinta3: TSmallintField;
    QrImoveisESexta3: TSmallintField;
    QrImoveisESabado3: TSmallintField;
    QrImoveisEDomingo3: TSmallintField;
    QrImoveisEVeic3: TSmallintField;
    QrImoveisEFilho3: TSmallintField;
    QrImoveisEmNome1: TWideStringField;
    QrImoveisEmTel1: TWideStringField;
    QrImoveisEmNome2: TWideStringField;
    QrImoveisEmTel2: TWideStringField;
    QrImoveisObserv: TWideMemoField;
    QrImoveisSitImv: TIntegerField;
    QrImoveisLk: TIntegerField;
    QrImoveisDataCad: TDateField;
    QrImoveisDataAlt: TDateField;
    QrImoveisUserCad: TIntegerField;
    QrImoveisUserAlt: TIntegerField;
    QrImoveisImobiliaria: TIntegerField;
    QrImoveisContato: TWideStringField;
    QrImoveisContTel: TWideStringField;
    QrImoveisNOMESITIMV: TWideStringField;
    QrImoveisUsuario: TIntegerField;
    QrImoveisNOMEUSUARIO: TWideStringField;
    QrImoveisProtocolo: TIntegerField;
    QrImoveisNOMEPROTOCOLO: TWideStringField;
    frxDsImoveis: TfrxDBDataset;
    Panel3: TPanel;
    QrImoveisTEL1_PROP: TWideStringField;
    QrImoveisCELU_PROP: TWideStringField;
    QrImoveisEMEIO_PROP: TWideStringField;
    QrImoveisTEL1_USU: TWideStringField;
    QrImoveisCELU_USU: TWideStringField;
    QrImoveisEMEIO_USU: TWideStringField;
    QrImoveisTEL1_PROP_TXT: TWideStringField;
    QrImoveisCELU_PROP_TXT: TWideStringField;
    QrImoveisTEL1_USU_TXT: TWideStringField;
    QrImoveisCELU_USU_TXT: TWideStringField;
    QrImoveisCNPJCPF_PROP: TWideStringField;
    QrImoveisCNPJCPF_USU: TWideStringField;
    QrImoveisCNPJCPF_PROP_TXT: TWideStringField;
    QrImoveisCNPJCPF_USU_TXT: TWideStringField;
    frx_001: TfrxReport;
    QrImoveisNOMEPROC: TWideStringField;
    QrImoveisCNPJCPF_PROC: TWideStringField;
    QrImoveisTEL1_PROC: TWideStringField;
    QrImoveisCELU_PROC: TWideStringField;
    QrImoveisEMEIO_PROC: TWideStringField;
    QrImoveisProcurador: TIntegerField;
    QrImoveisTEL1_PROC_TXT: TWideStringField;
    QrImoveisCELU_PROC_TXT: TWideStringField;
    QrImoveisCNPJCPF_PROC_TXT: TWideStringField;
    frx_002: TfrxReport;
    QrImoveisQUEM_TIPO_A: TWideStringField;
    QrImoveisQUEM_TIPO_B: TWideStringField;
    QrImoveisNOME_TIPO_A: TWideStringField;
    QrImoveisNOME_TIPO_B: TWideStringField;
    QrImoveisCNPJCPF_TIPO_A: TWideStringField;
    QrImoveisCNPJCPF_TIPO_B: TWideStringField;
    frx_003: TfrxReport;
    Panel6: TPanel;
    Panel7: TPanel;
    Label1: TLabel;
    EdEmpresa: TdmkEditCB;
    CBEmpresa: TdmkDBLookupComboBox;
    dmkDBGrid1: TdmkDBGrid;
    Label2: TLabel;
    EdTitulo: TEdit;
    PCTipoRel: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    TabSheet3: TTabSheet;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel8: TPanel;
    Panel9: TPanel;
    Panel10: TPanel;
    frx_004: TfrxReport;
    Panel11: TPanel;
    EdPeriodo: TEdit;
    Label4: TLabel;
    Grade04: TStringGrid;
    EdQtdPagObsGer04: TdmkEdit;
    Label5: TLabel;
    RGImp0: TRadioGroup;
    frx_000B: TfrxReport;
    QrImoveisE_ALL_PRO: TWideStringField;
    QrImoveisE_ALL_MOR: TWideStringField;
    QrImoveisRUA_PROP: TWideStringField;
    QrImoveisCOMPL_PROP: TWideStringField;
    QrImoveisBAIRRO_PROP: TWideStringField;
    QrImoveisCIDADE_PROP: TWideStringField;
    QrImoveisNO_PRO_LOGRAD: TWideStringField;
    QrImoveisNO_PRO_UF: TWideStringField;
    QrImoveisRUA_USU: TWideStringField;
    QrImoveisCOMPL_USU: TWideStringField;
    QrImoveisBAIRRO_USU: TWideStringField;
    QrImoveisCIDADE_USU: TWideStringField;
    QrImoveisNO_MOR_LOGRAD: TWideStringField;
    QrImoveisNO_MOR_UF: TWideStringField;
    QrImoveisWebLogin: TWideStringField;
    QrImoveisWebPwd: TWideStringField;
    QrImoveisWebNivel: TSmallintField;
    QrImoveisWebLogID: TWideStringField;
    QrImoveisWebLastLog: TDateTimeField;
    QrImoveisAlterWeb: TSmallintField;
    QrImoveisAtivo: TSmallintField;
    QrImoveisMoradores: TIntegerField;
    QrImoveisFracaoIdeal: TFloatField;
    QrImoveisModelBloq: TSmallintField;
    QrImoveisConfigBol: TIntegerField;
    QrImoveisBloqEndTip: TSmallintField;
    QrImoveisBloqEndEnt: TIntegerField;
    QrImoveisEnderLin1: TWideStringField;
    QrImoveisEnderLin2: TWideStringField;
    QrImoveisEnderNome: TWideStringField;
    CkInadimplentes: TCheckBox;
    QrImoveisInadimplente: TFloatField;
    QrImoveisINADCHECK: TSmallintField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel2: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel12: TPanel;
    BtOK: TBitBtn;
    BitBtn1: TBitBtn;
    Panel13: TPanel;
    BtSaida: TBitBtn;
    QrImoveisLOGRAD_PROP: TFloatField;
    QrImoveisLOGRAD_USU: TFloatField;
    QrImoveisNUMERO_PROP: TFloatField;
    QrImoveisNUMERO_USU: TFloatField;
    QrImoveisCEP_PROP: TFloatField;
    QrImoveisCEP_USU: TFloatField;
    QrImoveisUF_PROP: TFloatField;
    QrImoveisUF_USU: TFloatField;
    CkContato: TCheckBox;
    QrContatosProp: TmySQLQuery;
    QrContatosPropNome: TWideStringField;
    QrContatosPropTelefone: TWideStringField;
    QrContatosPropTipo: TWideStringField;
    QrContatosPropEMail: TWideStringField;
    frxDsContatosProp: TfrxDBDataset;
    QrContatosPropCodigo: TIntegerField;
    frxDsContatosMor: TfrxDBDataset;
    QrContatosMor: TmySQLQuery;
    QrContatosPropTelefone_TXT: TWideStringField;
    QrContatosMorNome: TWideStringField;
    QrContatosMorTelefone: TWideStringField;
    QrContatosMorTipo: TWideStringField;
    QrContatosMorEMail: TWideStringField;
    QrContatosMorTelefone_TXT: TWideStringField;
    QrContatosMorCodigo: TIntegerField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frx_000AGetValue(const VarName: String;
      var Value: Variant);
    procedure QrImoveisCalcFields(DataSet: TDataSet);
    procedure dmkDBGrid1DblClick(Sender: TObject);
    procedure EdEmpresaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CBEmpresaKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrImoveisAfterScroll(DataSet: TDataSet);
    procedure QrImoveisBeforeClose(DataSet: TDataSet);
    procedure QrContatosPropCalcFields(DataSet: TDataSet);
    procedure QrContatosMorCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FTitulo: String;
    procedure ResizeGrade04();
    procedure ReopenTbImoveis(Cond, TipoRel: Integer; MostraInad: Boolean);
    procedure ReopenContatos(Query: TmySQLQuery; Codigo: Integer);
  public
    { Public declarations }
  end;

  var
  FmCondImovImp: TFmCondImovImp;

implementation

uses dmkGeral, UnInternalConsts, Module, ModuleGeral, ModuleCond, UnMyObjects,
  DmkDAC_PF;

{$R *.DFM}

procedure TFmCondImovImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondImovImp.CBEmpresaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  EdEmpresaKeyDown(Sender, Key, Shift);
end;

procedure TFmCondImovImp.dmkDBGrid1DblClick(Sender: TObject);
begin
  if DModG.QrEmpresas.RecordCount > 0 then
  begin
    EdEmpresa.ValueVariant := DModG.QrEmpresasFilial.Value;
    CBEmpresa.KeyValue     := DModG.QrEmpresasFilial.Value;
    BtOKClick(Self);
  end;
end;

procedure TFmCondImovImp.EdEmpresaKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Cond: Integer;
begin
  if Key = VK_F4 then
    DmCond.DefineCond(Cond, EdEmpresa, nil);
end;

procedure TFmCondImovImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondImovImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
  ResizeGrade04();
end;

procedure TFmCondImovImp.FormCreate(Sender: TObject);
var
  Ano, Mes, Dia: Word;
begin
  ImgTipo.SQLType := stLok;
  //
  CBEmpresa.ListSource := DModG.DsEmpresas;
  Grade04.Cells[00,00] := 'T�tulos';
  Grade04.Cells[00,01] := 'A';
  Grade04.Cells[00,02] := 'B';
  Grade04.Cells[00,03] := 'C';
  Grade04.Cells[01,00] := '01';
  Grade04.Cells[02,00] := '02';
  Grade04.Cells[03,00] := '03';
  Grade04.Cells[04,00] := '04';
  Grade04.Cells[05,00] := '05';
  Grade04.Cells[06,00] := '06';
  Grade04.Cells[07,00] := '07';
  Grade04.Cells[08,00] := '08';
  Grade04.Cells[09,00] := '09';
  Grade04.Cells[10,00] := '10';
  Grade04.Cells[11,00] := '11';
  Grade04.Cells[12,00] := '12';
  //
  Grade04.Cells[01,01] := 'Leitura';
  Grade04.Cells[01,02] := 'de';
  Grade04.Cells[01,03] := '�gua';
  //
  Grade04.Cells[02,01] := 'Leitura';
  Grade04.Cells[02,02] := 'de';
  Grade04.Cells[02,03] := 'G�s';
  //
  Grade04.Cells[03,01] := 'Aluguel';
  Grade04.Cells[03,02] := 'do Sal�o';
  Grade04.Cells[03,03] := 'de Festas';
  //
  Grade04.Cells[04,01] := 'Aluguel';
  Grade04.Cells[04,02] := 'da';
  Grade04.Cells[04,03] := 'Quadra';
  //
  Grade04.Cells[05,01] := '';
  Grade04.Cells[05,02] := 'Outros';
  Grade04.Cells[05,03] := '';
  //
  Grade04.Cells[06,01] := '';
  Grade04.Cells[06,02] := 'Outros';
  Grade04.Cells[06,03] := '';
  //
  {
  Grade04.Cells[0,01] := '';
  Grade04.Cells[0,02] := 'Outros';
  Grade04.Cells[0,03] := '';
  //
  }
  DecodeDate(date(), Ano, Mes, Dia);
  EdPeriodo.Text := CO_JANEIRO_UPPER + '/' + FormatFloat('0000', Ano) + ' A ' +
    CO_DEZEMBRO_UPPER + '/' + FormatFloat('0000', Ano);
  //
  //N�o precisa: DmodG.ReopenDono();
end;

procedure TFmCondImovImp.BtOKClick(Sender: TObject);
var
  Cond: Integer;
begin
  Cond := Geral.IMV(EdEmpresa.Text);
  if Cond = 0 then
  begin
    Geral.MensagemBox('Informe o condom�nio!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    EdEmpresa.SetFocus;
    Exit;
  end;
  {
  QrImoveis.Close;
  QrImoveis.Params[0].AsInteger := Cond;
  QrImoveis.Open;
  }
  ReopenTbImoveis(Cond, PCTipoRel.ActivePageIndex, CkInadimplentes.Checked);
  if Trim(EdTitulo.Text) <> '' then
    FTitulo := EdTitulo.Text
  else
  begin
    //FTitulo := PCTipoRel.Items[PCTipoRel.ItemIndex];
    case PCTipoRel.ActivePageIndex of
      0: FTitulo := TabSheet1.Caption;
      1: FTitulo := TabSheet2.Caption;
      2: FTitulo := TabSheet3.Caption;
      3: FTitulo := TabSheet4.Caption;
      4: FTitulo := TabSheet5.Caption;
      else
      begin
        FTitulo := IntToStr(PCTipoRel.ActivePageIndex);
        Geral.MensagemBox('"FTitulo" n�o implementado: Guia ' +
        FTitulo, 'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
  end;
  case PCTipoRel.ActivePageIndex of
    0:
    begin
      case RGImp0.ItemIndex of
        0:
        begin
          MyObjects.frxDefineDataSets(frx_000A, [
            DmodG.frxDsDono,
            frxDsImoveis,
            frxDsContatosProp,
            frxDsContatosMor
            ]);
          MyObjects.frxMostra(frx_000A, 'Listagem de im�veis sem endere�o');
        end;
        1:
        begin
          MyObjects.frxDefineDataSets(frx_000B, [
            DmodG.frxDsDono,
            frxDsImoveis,
            frxDsContatosProp,
            frxDsContatosMor
            ]);
          MyObjects.frxMostra(frx_000B, 'Listagem de im�veis com endere�o');
        end
        else Geral.MensagemBox('Tipo de relat�rio n�o implementado: "' +
        FTitulo + '"!', 'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
    1: MyObjects.frxMostra(frx_001, FTitulo);
    2: MyObjects.frxMostra(frx_002, FTitulo);
    3: MyObjects.frxMostra(frx_003, FTitulo);
    4: frx_004.DesignReport();
    else Geral.MensagemBox('Tipo de relat�rio n�o implementado: "' +
    FTitulo + '"!', 'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmCondImovImp.frx_000AGetValue(const VarName: String;
  var Value: Variant);
var
  I: Integer;
begin
  if VarName = 'VARF_CLIINT' then
    Value := CBEmpresa.Text
  else
  if VarName = 'VARF_TITULO' then
    Value := FTitulo
  // Exclusivo frx_004?
  else
  if VarName = 'PERIODO' then
    Value := EdPeriodo.Text
  else
  if VarName = 'QtdPagObsGer' then
    Value := EdQtdPagObsGer04.ValueVariant
  {
  else
  if VarName = 'MarcaInad' then
  begin
    if CkInadimplentes.Checked then
    begin
      if QrImoveisInadimplente.Value > 0 then
        Value := True
      else
        Value := False;
    end else
      Value := False;
  end
  }
  else
  begin
    for I := 1 to 12 do
    begin
      if VarName = 'TITULO_'+FormatFloat('00', I) +'_A' then
        Value := Grade04.Cells[I, 01];
      if VarName = 'TITULO_'+FormatFloat('00', I) +'_B' then
        Value := Grade04.Cells[I, 02];
      if VarName = 'TITULO_'+FormatFloat('00', I) +'_C' then
        Value := Grade04.Cells[I, 03];
    end;
  end
end;

procedure TFmCondImovImp.QrContatosMorCalcFields(DataSet: TDataSet);
begin
  QrContatosMorTelefone_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrContatosMorTelefone.Value);
end;

procedure TFmCondImovImp.QrContatosPropCalcFields(DataSet: TDataSet);
begin
  QrContatosPropTelefone_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrContatosPropTelefone.Value);
end;

procedure TFmCondImovImp.QrImoveisAfterScroll(DataSet: TDataSet);
begin
  if CkContato.Checked = True then
  begin
    ReopenContatos(QrContatosProp, QrImoveisPropriet.Value);
    ReopenContatos(QrContatosMor, QrImoveisUsuario.Value);
  end else
  begin
    QrContatosProp.Close;
    QrContatosMor.Close;
  end;
end;

procedure TFmCondImovImp.QrImoveisBeforeClose(DataSet: TDataSet);
begin
  QrContatosProp.Close;
  QrContatosMor.Close;
end;

procedure TFmCondImovImp.QrImoveisCalcFields(DataSet: TDataSet);
var
  NUM_TXT: String;
begin
  QrImoveisTEL1_PROP_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrImoveisTEL1_PROP.Value);
  QrImoveisCELU_PROP_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrImoveisCELU_PROP.Value);
  QrImoveisCNPJCPF_PROP_TXT.Value :=
    Geral.FormataCNPJ_TT(QrImoveisCNPJCPF_PROP.Value);
  //
  QrImoveisTEL1_PROC_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrImoveisTEL1_PROC.Value);
  QrImoveisCELU_PROC_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrImoveisCELU_PROC.Value);
  QrImoveisCNPJCPF_PROC_TXT.Value :=
    Geral.FormataCNPJ_TT(QrImoveisCNPJCPF_PROC.Value);
  //
  QrImoveisTEL1_USU_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrImoveisTEL1_USU.Value);
  QrImoveisCELU_USU_TXT.Value :=
    Geral.FormataTelefone_TT_Curto(QrImoveisCELU_USU.Value);
  QrImoveisCNPJCPF_USU_TXT.Value :=
    Geral.FormataCNPJ_TT(QrImoveisCNPJCPF_USU.Value);
  //
  if QrImoveisUsuario.Value <> 0 then
  begin
    QrImoveisQUEM_TIPO_A.Value := 'M';
    QrImoveisNOME_TIPO_A.Value := QrImoveisNOMEUSUARIO.Value;
    QrImoveisCNPJCPF_TIPO_A.Value := QrImoveisCNPJCPF_USU_TXT.Value;
  end else begin
    QrImoveisQUEM_TIPO_A.Value := 'P';
    QrImoveisNOME_TIPO_A.Value := QrImoveisNOMEPROP.Value;
    QrImoveisCNPJCPF_TIPO_A.Value := QrImoveisCNPJCPF_PROP_TXT.Value;
  end;
  //
  if QrImoveisProcurador.Value <> 0 then
  begin
    QrImoveisQUEM_TIPO_B.Value := 'O';
    QrImoveisNOME_TIPO_B.Value := QrImoveisNOMEPROC.Value;
    QrImoveisCNPJCPF_TIPO_B.Value := QrImoveisCNPJCPF_PROC_TXT.Value;
  end else begin
    QrImoveisQUEM_TIPO_B.Value := 'D';
    QrImoveisNOME_TIPO_B.Value := QrImoveisNOMEPROP.Value;
    QrImoveisCNPJCPF_TIPO_B.Value := QrImoveisCNPJCPF_PROP_TXT.Value;
  end;
  //
  NUM_TXT := Geral.FormataNumeroDeRua(QrImoveisRUA_PROP.Value, Trunc(QrImoveisNUMERO_PROP.Value), False);
  QrImoveisE_ALL_PRO.Value := QrImoveisNO_PRO_LOGRAD.Value;
  if Trim(QrImoveisE_ALL_PRO.Value) <> '' then QrImoveisE_ALL_PRO.Value :=
    QrImoveisE_ALL_PRO.Value + ' ';
  QrImoveisE_ALL_PRO.Value := QrImoveisE_ALL_PRO.Value + QrImoveisRUA_PROP.Value;
  if Trim(QrImoveisRUA_PROP.Value) <> '' then QrImoveisE_ALL_PRO.Value :=
    QrImoveisE_ALL_PRO.Value + ', ' + NUM_TXT;
  if Trim(QrImoveisCOMPL_PROP.Value) <>  '' then QrImoveisE_ALL_PRO.Value :=
    QrImoveisE_ALL_PRO.Value + ' ' + QrImoveisCOMPL_PROP.Value;
  if Trim(QrImoveisBAIRRO_PROP.Value) <>  '' then QrImoveisE_ALL_PRO.Value :=
    QrImoveisE_ALL_PRO.Value + ' - ' + QrImoveisBAIRRO_PROP.Value;
  if QrImoveisCEP_PROP.Value > 0 then QrImoveisE_ALL_PRO.Value :=
    QrImoveisE_ALL_PRO.Value + ' CEP ' + Geral.FormataCEP_NT(QrImoveisCEP_PROP.Value);
  if Trim(QrImoveisCIDADE_PROP.Value) <>  '' then QrImoveisE_ALL_PRO.Value :=
    QrImoveisE_ALL_PRO.Value + ' - ' + QrImoveisCIDADE_PROP.Value;
  if Trim(QrImoveisNO_PRO_UF.Value) <>  '' then QrImoveisE_ALL_PRO.Value :=
    QrImoveisE_ALL_PRO.Value + ' - ' + QrImoveisNO_PRO_UF.Value;
  //
  //
  NUM_TXT := Geral.FormataNumeroDeRua(QrImoveisRUA_USU.Value, Trunc(QrImoveisNUMERO_USU.Value), False);
  QrImoveisE_ALL_MOR.Value := QrImoveisNO_MOR_LOGRAD.Value;
  if Trim(QrImoveisE_ALL_MOR.Value) <> '' then QrImoveisE_ALL_MOR.Value :=
    QrImoveisE_ALL_MOR.Value + ' ';
  QrImoveisE_ALL_MOR.Value := QrImoveisE_ALL_MOR.Value + QrImoveisRUA_USU.Value;
  if Trim(QrImoveisRUA_USU.Value) <> '' then QrImoveisE_ALL_MOR.Value :=
    QrImoveisE_ALL_MOR.Value + ', ' + NUM_TXT;
  if Trim(QrImoveisCOMPL_USU.Value) <>  '' then QrImoveisE_ALL_MOR.Value :=
    QrImoveisE_ALL_MOR.Value + ' ' + QrImoveisCOMPL_USU.Value;
  if Trim(QrImoveisBAIRRO_USU.Value) <>  '' then QrImoveisE_ALL_MOR.Value :=
    QrImoveisE_ALL_MOR.Value + ' - ' + QrImoveisBAIRRO_USU.Value;
  if QrImoveisCEP_USU.Value > 0 then QrImoveisE_ALL_MOR.Value :=
    QrImoveisE_ALL_MOR.Value + ' CEP ' + Geral.FormataCEP_NT(QrImoveisCEP_USU.Value);
  if Trim(QrImoveisCIDADE_USU.Value) <>  '' then QrImoveisE_ALL_MOR.Value :=
    QrImoveisE_ALL_MOR.Value + ' - ' + QrImoveisCIDADE_USU.Value;
  if Trim(QrImoveisNO_MOR_UF.Value) <>  '' then QrImoveisE_ALL_MOR.Value :=
    QrImoveisE_ALL_MOR.Value + ' - ' + QrImoveisNO_MOR_UF.Value;
  //
  if QrImoveisInadimplente.Value > 0 then
    QrImoveisINADCHECK.Value := 1
  else
    QrImoveisINADCHECK.Value := 0;
end;

procedure TFmCondImovImp.ReopenContatos(Query: TmySQLQuery; Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT ete.Codigo, eco.Nome, ete.Telefone, "" Email, ',
    'IF(ete.EntiTipCto=0, "", CONCAT(" - ", etc.Nome)) Tipo ',
    'FROM entitel ete ',
    'LEFT JOIN entitipcto etc ON etc.Codigo = ete.EntiTipCto ',
    'LEFT JOIN enticontat eco ON eco.Controle = ete.Controle ',
    'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
    'WHERE ece.Codigo=' + Geral.FF0(Codigo),
    ' ',
    'UNION ',
    ' ',
    'SELECT ema.Codigo, eco.Nome, "" Telefone, ema.EMail, ',
    'IF(ema.EntiTipCto=0, "", CONCAT(" - ", etc.Nome)) Tipo ',
    'FROM entimail ema ',
    'LEFT JOIN entitipcto etc ON etc.Codigo = ema.EntiTipCto ',
    'LEFT JOIN enticontat eco ON eco.Controle = ema.Controle ',
    'LEFT JOIN enticonent ece ON ece.Controle=eco.Controle ',
    'WHERE ece.Codigo=' + Geral.FF0(Codigo),
    ' ',
    'ORDER BY Nome ',
    '']);
end;

procedure TFmCondImovImp.ReopenTbImoveis(Cond, TipoRel: Integer; MostraInad: Boolean);
var
  TabLctA: String;
begin
  TabLctA  := DModG.NomeTab(TMeuDB, ntLct, False, ttA, Cond);
  //
  if Length(TabLctA) = 0 then
  begin
    Geral.MensagemBox('Tabela de lan�amentos financeiros  n�o definida!', 'Erro',
      MB_OK+MB_ICONERROR);
    Exit;
  end;
  QrImoveis.Close;
  QrImoveis.SQL.Clear;
  QrImoveis.SQL.Add('SELECT con.Nome NOMECONJUGE, sta.Descri STATUS1,');
  if (TipoRel = 3) and (MostraInad) then
  begin
    QrImoveis.SQL.Add('(');
    QrImoveis.SQL.Add('SELECT SUM(lan.Credito)');
    QrImoveis.SQL.Add('FROM '+ TabLctA +' lan');
    QrImoveis.SQL.Add('WHERE lan.Reparcel=0');
    QrImoveis.SQL.Add('AND lan.Sit < 2');
    QrImoveis.SQL.Add('AND lan.Credito > lan.Pago');
    QrImoveis.SQL.Add('AND lan.CliInt=cnd.Cliente');
    QrImoveis.SQL.Add('AND lan.Depto=imc.Conta');
    QrImoveis.SQL.Add('AND lan.Vencimento<SYSDATE()');
    QrImoveis.SQL.Add(') Inadimplente,');
  end else
    QrImoveis.SQL.Add('0.0 Inadimplente,');
  QrImoveis.SQL.Add('  IF(imc.Propriet=0, "",');
  QrImoveis.SQL.Add('IF(pro.Tipo=0, pro.RazaoSocial, pro.Nome)) NOMEPROP,');
  QrImoveis.SQL.Add('IF (pro.Tipo=0, pro.CNPJ, pro.CPF) CNPJCPF_PROP,');
  QrImoveis.SQL.Add('IF (pro.Tipo=0, pro.ETe1, pro.PTe1) TEL1_PROP,');
  QrImoveis.SQL.Add('IF (pro.Tipo=0, pro.ECel, pro.PCel) CELU_PROP,');
  QrImoveis.SQL.Add('IF (pro.Tipo=0, pro.EEmail, pro.PEmail) EMEIO_PROP,');
  QrImoveis.SQL.Add('IF (pro.Tipo=0, pro.ELograd, pro.PLograd) + 0.000 LOGRAD_PROP,');
  QrImoveis.SQL.Add('IF (pro.Tipo=0, pro.ERua, pro.PRua) RUA_PROP,');
  QrImoveis.SQL.Add('IF (pro.Tipo=0, pro.ENumero, pro.PNumero) + 0.000 NUMERO_PROP,');
  QrImoveis.SQL.Add('IF (pro.Tipo=0, pro.ECompl, pro.PCompl) COMPL_PROP,');
  QrImoveis.SQL.Add('IF (pro.Tipo=0, pro.EBairro, pro.PBairro) BAIRRO_PROP,');
  QrImoveis.SQL.Add('IF (pro.Tipo=0, pro.ECidade, pro.PCidade) CIDADE_PROP,');
  QrImoveis.SQL.Add('IF (pro.Tipo=0, pro.EUF, pro.PUF) + 0.000 UF_PROP,');
  QrImoveis.SQL.Add('IF (pro.Tipo=0, pro.ECEP, pro.PCEP) + 0.000 CEP_PROP,');
  QrImoveis.SQL.Add('ll1.Nome NO_PRO_LOGRAD, uf1.Nome NO_PRO_UF,');
  QrImoveis.SQL.Add('  IF(imc.Procurador=0, "",');
  QrImoveis.SQL.Add('IF(pcu.Tipo=0, pcu.RazaoSocial, pcu.Nome)) NOMEPROC,');
  QrImoveis.SQL.Add('IF (pcu.Tipo=0, pcu.CNPJ, pcu.CPF) CNPJCPF_PROC,');
  QrImoveis.SQL.Add('IF (pcu.Tipo=0, pcu.ETe1, pcu.PTe1) TEL1_PROC,');
  QrImoveis.SQL.Add('IF (pcu.Tipo=0, pcu.ECel, pcu.PCel) CELU_PROC,');
  QrImoveis.SQL.Add('IF (pcu.Tipo=0, pcu.EEmail, pcu.PEmail) EMEIO_PROC,');
  QrImoveis.SQL.Add('  IF(imc.Usuario=0, "",');
  QrImoveis.SQL.Add('IF (mor.Tipo=0, mor.RazaoSocial, mor.Nome)) NOMEUSUARIO,');
  QrImoveis.SQL.Add('IF (mor.Tipo=0, mor.CNPJ, mor.CPF) CNPJCPF_USU,');
  QrImoveis.SQL.Add('IF (mor.Tipo=0, mor.ETe1, mor.PTe1) TEL1_USU,');
  QrImoveis.SQL.Add('IF (mor.Tipo=0, mor.ECel, mor.PCel) CELU_USU,');
  QrImoveis.SQL.Add('IF (mor.Tipo=0, mor.EEmail, mor.PEmail) EMEIO_USU,');
  QrImoveis.SQL.Add('IF (mor.Tipo=0, mor.ELograd, mor.PLograd) + 0.000 LOGRAD_USU,');
  QrImoveis.SQL.Add('IF (mor.Tipo=0, mor.ERua, mor.PRua) RUA_USU,');
  QrImoveis.SQL.Add('IF (mor.Tipo=0, mor.ENumero, mor.PNumero) + 0.000 NUMERO_USU,');
  QrImoveis.SQL.Add('IF (mor.Tipo=0, mor.ECompl, mor.PCompl) COMPL_USU,');
  QrImoveis.SQL.Add('IF (mor.Tipo=0, mor.EBairro, mor.PBairro) BAIRRO_USU,');
  QrImoveis.SQL.Add('IF (mor.Tipo=0, mor.ECidade, mor.PCidade) CIDADE_USU,');
  QrImoveis.SQL.Add('IF (mor.Tipo=0, mor.EUF, mor.PUF)  + 0.000 UF_USU,');
  QrImoveis.SQL.Add('IF (mor.Tipo=0, mor.ECEP, mor.PCEP) + 0.000 CEP_USU,');
  QrImoveis.SQL.Add('ll2.Nome NO_MOR_LOGRAD, uf2.Nome NO_MOR_UF,');
  QrImoveis.SQL.Add('IF(imc.ENome1=0, "",');
  QrImoveis.SQL.Add('  IF (ena.Tipo=0, ena.RazaoSocial, ena.Nome)) NOMEEMP1,');
  QrImoveis.SQL.Add('IF(imc.ENome2=0, "",');
  QrImoveis.SQL.Add('  IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome)) NOMEEMP2,');
  QrImoveis.SQL.Add('IF(imc.ENome3=0, "",');
  QrImoveis.SQL.Add('  IF (enc.Tipo=0, enc.RazaoSocial, enc.Nome)) NOMEEMP3,');
  QrImoveis.SQL.Add('end.RazaoSocial IMOB, ptc.Nome NOMEPROTOCOLO, imc.*');
  QrImoveis.SQL.Add('FROM condimov imc');
  if (TipoRel = 3) and (MostraInad) then
    QrImoveis.SQL.Add('LEFT JOIN cond cnd ON cnd.Codigo = imc.Codigo');
  QrImoveis.SQL.Add('LEFT JOIN entidades pro ON pro.Codigo=imc.Propriet');
  QrImoveis.SQL.Add('LEFT JOIN entidades mor ON mor.Codigo=imc.Usuario');
  QrImoveis.SQL.Add('LEFT JOIN entidades pcu ON pcu.Codigo=imc.Procurador');
  QrImoveis.SQL.Add('LEFT JOIN entidades con ON con.Codigo=imc.Conjuge');
  QrImoveis.SQL.Add('LEFT JOIN entidades ena ON ena.Codigo=imc.ENome1');
  QrImoveis.SQL.Add('LEFT JOIN entidades enb ON enb.Codigo=imc.ENome2');
  QrImoveis.SQL.Add('LEFT JOIN entidades enc ON enc.Codigo=imc.ENome3');
  QrImoveis.SQL.Add('LEFT JOIN status sta ON sta.Codigo=imc.Status');
  QrImoveis.SQL.Add('LEFT JOIN entidades end ON end.Codigo=imc.Imobiliaria');
  QrImoveis.SQL.Add('LEFT JOIN protocolos ptc ON ptc.Codigo=imc.Protocolo');
  QrImoveis.SQL.Add('LEFT JOIN listalograd ll1 ON ll1.Codigo=IF(pro.Tipo=0, pro.ELograd, pro.PLograd)');
  QrImoveis.SQL.Add('LEFT JOIN ufs uf1 ON uf1.Codigo=IF(pro.Tipo=0, pro.EUF, pro.PUF)');
  QrImoveis.SQL.Add('LEFT JOIN listalograd ll2 ON ll2.Codigo=IF(mor.Tipo=0, mor.ELograd, mor.PLograd)');
  QrImoveis.SQL.Add('LEFT JOIN ufs uf2 ON uf2.Codigo=IF(mor.Tipo=0, mor.EUF, mor.PUF)');
  QrImoveis.SQL.Add('WHERE imc.Codigo=:P0');
  QrImoveis.SQL.Add('ORDER BY Codigo, Controle, Andar, Unidade');
  QrImoveis.Params[0].AsInteger := Cond;
  QrImoveis.Open;
  if (TipoRel = 3) and (MostraInad) then
  begin

  end;
end;

procedure TFmCondImovImp.ResizeGrade04();
var
  MaxTam: Integer;
begin
  MaxTam := (Grade04.Width - 25(*ScrollBar*) - 24(*Coluna 0*) - 8) div 12(*colunas*);
  Grade04.DefaultColWidth := MaxTam;
  Grade04.ColWidths[00] := 24;
end;

end.
