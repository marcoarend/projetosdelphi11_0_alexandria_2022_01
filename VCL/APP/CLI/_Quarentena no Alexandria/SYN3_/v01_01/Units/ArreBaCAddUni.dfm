object FmArreBaCAddUni: TFmArreBaCAddUni
  Left = 450
  Top = 286
  Caption = 'CAD-ARREC-002 :: Arrecada'#231#227'o - Inclui Unidade'
  ClientHeight = 217
  ClientWidth = 477
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 477
    Height = 55
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Panel1: TPanel
      Left = 0
      Top = 0
      Width = 477
      Height = 55
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Label1: TLabel
        Left = 16
        Top = 4
        Width = 103
        Height = 13
        Caption = 'Unidade habitacional:'
      end
      object dmkLabel1: TdmkLabel
        Left = 316
        Top = 4
        Width = 40
        Height = 13
        Caption = 'Inclus'#227'o'
        Enabled = False
        UpdType = utYes
        SQLType = stIns
      end
      object dmkEdCBApto: TdmkEditCB
        Left = 16
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = dmkCBApto
        IgnoraDBLookupComboBox = False
      end
      object dmkCBApto: TdmkDBLookupComboBox
        Left = 72
        Top = 20
        Width = 241
        Height = 21
        KeyField = 'Conta'
        ListField = 'Unidade'
        ListSource = DsAptos
        TabOrder = 1
        dmkEditCB = dmkEdCBApto
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object dmkEdPercent: TdmkEdit
        Left = 316
        Top = 20
        Width = 97
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        TabOrder = 2
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 477
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object GB_R: TGroupBox
      Left = 429
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 381
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 351
        Height = 32
        Caption = 'Arrecada'#231#227'o - Inclui Unidade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 351
        Height = 32
        Caption = 'Arrecada'#231#227'o - Inclui Unidade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 351
        Height = 32
        Caption = 'Arrecada'#231#227'o - Inclui Unidade'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 103
    Width = 477
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 473
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 147
    Width = 477
    Height = 70
    Align = alBottom
    TabOrder = 3
    object Panel3: TPanel
      Left = 2
      Top = 15
      Width = 473
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object PnSaiDesis: TPanel
        Left = 329
        Top = 0
        Width = 144
        Height = 53
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 14
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
          NumGlyphs = 2
        end
      end
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 14
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
        NumGlyphs = 2
      end
    end
  end
  object DsAptos: TDataSource
    DataSet = QrAptos
    Left = 332
    Top = 2
  end
  object QrAptos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cni.Andar, cni.Conta, cni.Unidade'
      'FROM condimov cni'
      'WHERE Codigo=:P0'
      'AND cni.Conta NOT IN ('
      '  SELECT Apto FROM arrebau'
      '  WHERE Codigo=:P1'
      '  AND Controle=:P2)'
      'ORDER BY Andar, Unidade')
    Left = 264
    Top = 2
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrAptosConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrAptosUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
  end
end
