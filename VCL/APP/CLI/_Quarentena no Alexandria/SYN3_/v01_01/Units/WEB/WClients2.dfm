object FmWClients2: TFmWClients2
  Left = 345
  Top = 165
  Caption = 'WEB-CLIEN-001 :: Clientes WEB'
  ClientHeight = 544
  ClientWidth = 916
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 96
    Width = 916
    Height = 448
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    Visible = False
    object PainelEdit: TPanel
      Left = 0
      Top = 0
      Width = 916
      Height = 325
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label8: TLabel
        Left = 8
        Top = 48
        Width = 249
        Height = 13
        Caption = 'Descri'#231#227'o do condom'#237'nio no site (Meu Condom'#237'nio):'
        FocusControl = DBEdit2
      end
      object Label29: TLabel
        Left = 488
        Top = 48
        Width = 29
        Height = 13
        Caption = 'Login:'
      end
      object Label30: TLabel
        Left = 676
        Top = 48
        Width = 34
        Height = 13
        Caption = 'Senha:'
      end
      object Label10: TLabel
        Left = 76
        Top = 4
        Width = 60
        Height = 13
        Caption = 'Condom'#237'nio:'
        FocusControl = DBEdit1
      end
      object Label12: TLabel
        Left = 144
        Top = 4
        Width = 45
        Height = 13
        Caption = 'Entidade:'
        FocusControl = DBEdit3
      end
      object Label13: TLabel
        Left = 212
        Top = 4
        Width = 90
        Height = 13
        Caption = 'Nome da entidade:'
        FocusControl = DBEdit4
      end
      object Label14: TLabel
        Left = 8
        Top = 4
        Width = 40
        Height = 13
        Caption = 'ID Web:'
        FocusControl = DBEdit5
      end
      object Label7: TLabel
        Left = 276
        Top = 92
        Width = 118
        Height = 13
        Caption = 'Nome do s'#237'ndico no site:'
      end
      object Label4: TLabel
        Left = 276
        Top = 134
        Width = 259
        Height = 13
        Caption = 'E-mail (para que o usu'#225'rio possa recuperar sua senha):'
      end
      object SpeedButton5: TSpeedButton
        Left = 465
        Top = 256
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label11: TLabel
        Left = 8
        Top = 240
        Width = 111
        Height = 13
        Caption = 'Perfil de senha s'#237'ndico:'
      end
      object Label24: TLabel
        Left = 8
        Top = 280
        Width = 128
        Height = 13
        Caption = 'Perfil de senha cond'#244'mino:'
      end
      object SpeedButton6: TSpeedButton
        Left = 465
        Top = 296
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton6Click
      end
      object EdNomeWeb: TdmkEdit
        Left = 8
        Top = 64
        Width = 475
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdUser2: TdmkEdit
        Left = 488
        Top = 64
        Width = 182
        Height = 21
        MaxLength = 32
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdPass2: TdmkEdit
        Left = 676
        Top = 64
        Width = 185
        Height = 21
        MaxLength = 32
        TabOrder = 6
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object DBEdit1: TDBEdit
        Left = 76
        Top = 20
        Width = 64
        Height = 21
        DataField = 'Codigo'
        DataSource = Dswclients
        TabOrder = 1
      end
      object DBEdit3: TDBEdit
        Left = 144
        Top = 20
        Width = 64
        Height = 21
        DataField = 'Cliente'
        DataSource = Dswclients
        TabOrder = 2
      end
      object DBEdit4: TDBEdit
        Left = 212
        Top = 20
        Width = 649
        Height = 21
        DataField = 'NCONDOM'
        DataSource = Dswclients
        TabOrder = 3
      end
      object DBEdit5: TDBEdit
        Left = 8
        Top = 20
        Width = 64
        Height = 21
        DataField = 'User_ID'
        DataSource = Dswclients
        TabOrder = 0
      end
      object GroupBox1: TGroupBox
        Left = 8
        Top = 88
        Width = 265
        Height = 149
        Caption = ' Parcelamento de bloquetos em atrazo: '
        TabOrder = 7
        object Label17: TLabel
          Left = 16
          Top = 24
          Width = 120
          Height = 13
          Caption = 'Valor m'#237'nimo da parcela: '
        end
        object Label18: TLabel
          Left = 16
          Top = 48
          Width = 157
          Height = 13
          Caption = 'Quantidade m'#225'xima de parcelas: '
        end
        object Label21: TLabel
          Left = 16
          Top = 96
          Width = 132
          Height = 13
          Caption = 'Taxa em % de juros ao m'#234's:'
        end
        object Label22: TLabel
          Left = 16
          Top = 72
          Width = 57
          Height = 13
          Caption = 'Multa em %:'
        end
        object dmkEdParc_ValMin: TdmkEdit
          Left = 176
          Top = 20
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object dmkEdParc_QtdMax: TdmkEdit
          Left = 176
          Top = 44
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 1
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object dmkEdMulta: TdmkEdit
          Left = 176
          Top = 68
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
        object dmkEdJuros: TdmkEdit
          Left = 176
          Top = 92
          Width = 80
          Height = 21
          Alignment = taRightJustify
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          ValWarn = False
        end
      end
      object EdNomeSindico: TdmkEdit
        Left = 276
        Top = 108
        Width = 394
        Height = 21
        MaxLength = 32
        TabOrder = 8
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object RGPwdSite: TRadioGroup
        Left = 276
        Top = 175
        Width = 260
        Height = 35
        Caption = 'Mostra senha de acesso restrito ao site no boleto: '
        Columns = 2
        ItemIndex = 0
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 10
      end
      object CkOcultaBloq: TCheckBox
        Left = 276
        Top = 215
        Width = 330
        Height = 17
        Caption = 'Oculta boletos vencidos, avencer e reparcelamentos no site.'
        TabOrder = 11
      end
      object dmkEdit1: TdmkEdit
        Left = 276
        Top = 150
        Width = 394
        Height = 21
        TabOrder = 9
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'EMail'
        UpdCampo = 'EMail'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object CBPerfil: TdmkDBLookupComboBox
        Left = 64
        Top = 256
        Width = 397
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsWPerfil
        TabOrder = 13
        dmkEditCB = EdPerfil
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPerfil: TdmkEditCB
        Left = 8
        Top = 256
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPerfil
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
      object CBPerfilImv: TdmkDBLookupComboBox
        Left = 64
        Top = 296
        Width = 397
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsWPerfilImv
        TabOrder = 14
        dmkEditCB = EdPerfilImv
        QryCampo = 'Cargo'
        UpdType = utYes
        LocF7SQLMasc = '$#'
        LocF7PreDefProc = f7pNone
      end
      object EdPerfilImv: TdmkEditCB
        Left = 8
        Top = 296
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Cargo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPerfilImv
        IgnoraDBLookupComboBox = False
        AutoSetIfOnlyOneReg = setregOnlyManual
      end
    end
    object GroupBox4: TGroupBox
      Left = 0
      Top = 378
      Width = 916
      Height = 70
      Align = alBottom
      TabOrder = 1
      object Panel8: TPanel
        Left = 2
        Top = 15
        Width = 912
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel9: TPanel
          Left = 768
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 8
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 6
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtConfirmaClick
        end
      end
    end
  end
  object PainelUser: TPanel
    Left = 0
    Top = 96
    Width = 916
    Height = 448
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 2
    Visible = False
    object Panel10: TPanel
      Left = 0
      Top = 0
      Width = 916
      Height = 95
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label27: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label33: TLabel
        Left = 84
        Top = 4
        Width = 29
        Height = 13
        Caption = 'Login:'
        FocusControl = DBEdit2
      end
      object Label34: TLabel
        Left = 432
        Top = 4
        Width = 34
        Height = 13
        Caption = 'Senha:'
        FocusControl = DBEdit6
      end
      object Label3: TLabel
        Left = 8
        Top = 48
        Width = 259
        Height = 13
        Caption = 'E-mail (para que o usu'#225'rio possa recuperar sua senha):'
      end
      object Edit001: TdmkEdit
        Left = 8
        Top = 20
        Width = 73
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 2
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdULogin: TdmkEdit
        Left = 84
        Top = 20
        Width = 344
        Height = 21
        MaxLength = 32
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdUSenha: TdmkEdit
        Left = 432
        Top = 20
        Width = 344
        Height = 21
        MaxLength = 32
        TabOrder = 2
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdEMail: TdmkEdit
        Left = 8
        Top = 64
        Width = 420
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = True
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'EMail'
        UpdCampo = 'EMail'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
    object GroupBox3: TGroupBox
      Left = 0
      Top = 378
      Width = 916
      Height = 70
      Align = alBottom
      TabOrder = 1
      object Panel11: TPanel
        Left = 2
        Top = 15
        Width = 912
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel12: TPanel
          Left = 768
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          object BitBtn3: TBitBtn
            Tag = 15
            Left = 8
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn3Click
          end
        end
        object BitBtn1: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn1Click
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 96
    Width = 916
    Height = 448
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    object PainelData: TPanel
      Left = 0
      Top = 0
      Width = 916
      Height = 170
      Align = alTop
      BevelOuter = bvNone
      Enabled = False
      TabOrder = 0
      object Label2: TLabel
        Left = 116
        Top = 4
        Width = 110
        Height = 13
        Caption = 'Razao social ou Nome:'
        FocusControl = DBEdit2
      end
      object Label6: TLabel
        Left = 476
        Top = 4
        Width = 30
        Height = 13
        Caption = 'CNPJ:'
        FocusControl = DBEdit6
      end
      object Label80: TLabel
        Left = 8
        Top = 4
        Width = 106
        Height = 13
        Caption = 'Entidade/condom'#237'nio:'
        FocusControl = DBEdit81
      end
      object Label1: TLabel
        Left = 592
        Top = 4
        Width = 45
        Height = 13
        Caption = 'Telefone:'
        FocusControl = DBEdit002
      end
      object Label9: TLabel
        Left = 8
        Top = 44
        Width = 89
        Height = 13
        Caption = 'Descri'#231#227'o na web:'
        FocusControl = DBEdit7
      end
      object Label15: TLabel
        Left = 476
        Top = 44
        Width = 92
        Height = 13
        Caption = 'Usu'#225'rio do s'#237'ndico:'
        FocusControl = DBEdit8
      end
      object Label16: TLabel
        Left = 592
        Top = 44
        Width = 87
        Height = 13
        Caption = 'Senha do s'#237'ndico:'
        FocusControl = DBEdit9
      end
      object Label23: TLabel
        Left = 8
        Top = 85
        Width = 118
        Height = 13
        Caption = 'Nome do s'#237'ndico no site:'
        FocusControl = DBEdit12
      end
      object Label5: TLabel
        Left = 339
        Top = 85
        Width = 259
        Height = 13
        Caption = 'E-mail (para que o usu'#225'rio possa recuperar sua senha):'
        FocusControl = DBEdit13
      end
      object DBEdit2: TDBEdit
        Left = 116
        Top = 20
        Width = 357
        Height = 21
        DataField = 'NCONDOM'
        DataSource = Dswclients
        TabOrder = 2
      end
      object DBEdit6: TDBEdit
        Left = 476
        Top = 20
        Width = 113
        Height = 21
        DataField = 'CNPJ_TXT'
        DataSource = Dswclients
        TabOrder = 3
      end
      object DBEdit81: TDBEdit
        Left = 8
        Top = 20
        Width = 52
        Height = 21
        TabStop = False
        DataField = 'Cliente'
        DataSource = Dswclients
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
      end
      object DBEdit001: TDBEdit
        Left = 60
        Top = 20
        Width = 52
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = Dswclients
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 1
      end
      object DBEdit002: TDBEdit
        Left = 592
        Top = 20
        Width = 113
        Height = 21
        DataField = 'CONDTEL'
        DataSource = Dswclients
        TabOrder = 4
      end
      object DBEdit7: TDBEdit
        Left = 8
        Top = 60
        Width = 465
        Height = 21
        DataField = 'NomeWeb'
        DataSource = Dswclients
        TabOrder = 5
      end
      object DBEdit8: TDBEdit
        Left = 476
        Top = 60
        Width = 113
        Height = 21
        DataField = 'Username'
        DataSource = Dswclients
        TabOrder = 6
      end
      object DBEdit9: TDBEdit
        Left = 592
        Top = 60
        Width = 113
        Height = 21
        DataField = 'Password'
        DataSource = Dswclients
        PasswordChar = '*'
        TabOrder = 7
      end
      object GroupBox2: TGroupBox
        Left = 712
        Top = 9
        Width = 197
        Height = 73
        Caption = ' Parcelamento de bloquetos em atrazo: '
        TabOrder = 8
        object Label19: TLabel
          Left = 4
          Top = 24
          Width = 92
          Height = 13
          Caption = 'Valor m'#237'n. parcela: '
        end
        object Label20: TLabel
          Left = 4
          Top = 48
          Width = 97
          Height = 13
          Caption = 'Qtde m'#225'x. parcelas: '
        end
        object DBEdit10: TDBEdit
          Left = 104
          Top = 20
          Width = 85
          Height = 21
          DataField = 'Parc_ValMin'
          DataSource = Dswclients
          TabOrder = 0
        end
        object DBEdit11: TDBEdit
          Left = 104
          Top = 44
          Width = 85
          Height = 21
          DataField = 'Parc_QtdMax'
          DataSource = Dswclients
          TabOrder = 1
        end
      end
      object DBRadioGroup1: TDBRadioGroup
        Left = 670
        Top = 88
        Width = 238
        Height = 78
        Caption = 'Mostra senha de acesso ao site no boleto: '
        Columns = 2
        DataField = 'PwdSite'
        DataSource = Dswclients
        Items.Strings = (
          'N'#227'o'
          'Sim')
        TabOrder = 11
        Values.Strings = (
          '0'
          '1')
      end
      object DBCheckBox1: TDBCheckBox
        Left = 8
        Top = 129
        Width = 656
        Height = 17
        Caption = 'Oculta boletos vencidos, avencer e reparcelamentos no site.'
        DataField = 'OcultaBloq'
        DataSource = Dswclients
        TabOrder = 12
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit12: TDBEdit
        Left = 8
        Top = 101
        Width = 325
        Height = 21
        DataField = 'NomeSindico'
        DataSource = Dswclients
        TabOrder = 9
      end
      object DBEdit13: TDBEdit
        Left = 339
        Top = 101
        Width = 325
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Email'
        DataSource = Dswclients
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 10
      end
    end
    object PageControl1: TPageControl
      Left = 43
      Top = 216
      Width = 765
      Height = 168
      ActivePage = TabSheet1
      TabHeight = 25
      TabOrder = 1
      object TabSheet1: TTabSheet
        Caption = 'Dados dos Usu'#225'rios'
        object dmkDBGUsers: TdmkDBGrid
          Left = 0
          Top = 17
          Width = 757
          Height = 116
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'User_ID'
              Title.Caption = 'User ID'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Title.Caption = 'UH'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USUARIO'
              Title.Caption = 'C'#243'digo'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USER_NOME'
              Title.Caption = 'Usu'#225'rio ou dono do im'#243'vel'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Username'
              Title.Caption = 'Login'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Password'
              Title.Caption = 'Senha'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Email'
              Title.Caption = 'E-mail'
              Width = 150
              Visible = True
            end>
          Color = clWindow
          DataSource = DsUsers
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnDblClick = dmkDBGUsersDblClick
          Columns = <
            item
              Expanded = False
              FieldName = 'User_ID'
              Title.Caption = 'User ID'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Title.Caption = 'UH'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USUARIO'
              Title.Caption = 'C'#243'digo'
              Width = 65
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'USER_NOME'
              Title.Caption = 'Usu'#225'rio ou dono do im'#243'vel'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Username'
              Title.Caption = 'Login'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Password'
              Title.Caption = 'Senha'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Email'
              Title.Caption = 'E-mail'
              Width = 150
              Visible = True
            end>
        end
        object PB1: TProgressBar
          Left = 0
          Top = 0
          Width = 757
          Height = 17
          Align = alTop
          TabOrder = 1
        end
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 384
      Width = 916
      Height = 64
      Align = alBottom
      TabOrder = 2
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 172
        Height = 47
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 174
        Top = 15
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 393
        Top = 15
        Width = 521
        Height = 47
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel14: TPanel
          Left = 388
          Top = 0
          Width = 133
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCliente: TBitBtn
          Tag = 10005
          Left = 6
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Cliente'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtClienteClick
        end
        object BtUsuario: TBitBtn
          Tag = 10021
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Usu'#225'rio'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtUsuarioClick
        end
        object BtDono: TBitBtn
          Tag = 280
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Grupo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtDonoClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 916
    Height = 52
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 3
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 216
      Height = 52
      Align = alLeft
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 8
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
    object GB_M: TGroupBox
      Left = 216
      Top = 0
      Width = 652
      Height = 52
      Align = alClient
      TabOrder = 1
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 171
        Height = 32
        Caption = 'Clientes WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 171
        Height = 32
        Caption = 'Clientes WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 171
        Height = 32
        Caption = 'Clientes WEB'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GB_R: TGroupBox
      Left = 868
      Top = 0
      Width = 48
      Height = 52
      Align = alRight
      TabOrder = 2
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 52
    Width = 916
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 4
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 912
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 12
        Height = 16
        Caption = '...'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object Dswclients: TDataSource
    DataSet = QrWClients
    Left = 460
    Top = 17
  end
  object QrWClients: TMySQLQuery
    Database = DModG.RV_CEP_DB
    AfterOpen = QrWClientsAfterOpen
    BeforeClose = QrWClientsBeforeClose
    AfterScroll = QrWClientsAfterScroll
    OnCalcFields = QrWClientsCalcFields
    SQL.Strings = (
      'SELECT con.*, IF(rec.Tipo=0, rec.RazaoSocial, rec.Nome) NCONDOM,'
      'IF(rec.Tipo=0, rec.CNPJ, rec.CPF) CNPJ_CPF,'
      'IF(rec.Tipo=0, rec.ETe1, rec.PTe2) CONDTEL, '
      'wcl.NomeWeb, wcl.Username, wcl.Password, wcl.User_ID,'
      'wcl.Parc_ValMin, wcl.Parc_QtdMax, wcl.NomeSindico,'
      'wcl.Juros, wcl.Multa'
      'FROM cond con'
      'LEFT JOIN entidades rec ON rec.Codigo=con.Cliente'
      'LEFT JOIN wclients  wcl ON wcl.CodCliEsp=con.Codigo'
      'WHERE con.Codigo > 0')
    Left = 432
    Top = 17
    object QrWClientsNCONDOM: TWideStringField
      FieldName = 'NCONDOM'
      Size = 100
    end
    object QrWClientsCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrWClientsCONDTEL: TWideStringField
      FieldName = 'CONDTEL'
    end
    object QrWClientsNomeWeb: TWideStringField
      FieldName = 'NomeWeb'
      Origin = 'wclients.NomeWeb'
      Size = 150
    end
    object QrWClientsUsername: TWideStringField
      FieldName = 'Username'
      Origin = 'wclients.Username'
      Size = 32
    end
    object QrWClientsPassword: TWideStringField
      FieldName = 'Password'
      Origin = 'wclients.Password'
      Size = 32
    end
    object QrWClientsUser_ID: TAutoIncField
      FieldName = 'User_ID'
      Origin = 'wclients.User_ID'
    end
    object QrWClientsParc_ValMin: TFloatField
      FieldName = 'Parc_ValMin'
      DisplayFormat = '#,###,##0.00'
    end
    object QrWClientsParc_QtdMax: TIntegerField
      FieldName = 'Parc_QtdMax'
    end
    object QrWClientsNomeSindico: TWideStringField
      FieldName = 'NomeSindico'
      Size = 100
    end
    object QrWClientsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cond.Codigo'
      Required = True
    end
    object QrWClientsJuros: TFloatField
      FieldName = 'Juros'
    end
    object QrWClientsMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrWClientsPwdSite: TSmallintField
      FieldName = 'PwdSite'
      Origin = 'cond.PwdSite'
      Required = True
    end
    object QrWClientsCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'cond.Cliente'
      Required = True
    end
    object QrWClientsCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 40
      Calculated = True
    end
    object QrWClientsOcultaBloq: TIntegerField
      FieldName = 'OcultaBloq'
    end
    object QrWClientsEmail: TWideStringField
      FieldName = 'Email'
    end
    object QrWClientsPerfil: TIntegerField
      FieldName = 'Perfil'
    end
    object QrWClientsPerfilImv: TIntegerField
      FieldName = 'PerfilImv'
    end
  end
  object PMUsuario: TPopupMenu
    OnPopup = PMUsuarioPopup
    Left = 552
    Top = 464
    object Alterausurioatual1: TMenuItem
      Caption = '&Altera usu'#225'rio atual'
      OnClick = Alterausurioatual1Click
    end
    object RecriaLoginsesenhas1: TMenuItem
      Caption = 'Recria Logins e senhas'
      object Atual1: TMenuItem
        Caption = '&Atual'
        OnClick = Atual1Click
      end
      object Selecionados1: TMenuItem
        Caption = '&Selecionados'
        OnClick = Selecionados1Click
      end
      object Todos1: TMenuItem
        Caption = '&Todos'
        OnClick = Todos1Click
      end
    end
  end
  object QrUsers: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT ent.Codigo,  '
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) USER_NOME, '
      'IF(imv.Usuario>0, imv.Usuario, imv.Propriet) + 0,000 USUARIO,'
      'imv.Propriet, imv.Unidade, imv.Conta APTO, usu.*'
      'FROM condimov imv'
      
        'LEFT JOIN entidades ent ON IF(imv.Usuario>0, imv.Usuario, imv.Pr' +
        'opriet)=ent.Codigo'
      
        'LEFT JOIN users usu ON usu.CodigoEsp=imv.Conta AND (usu.Tipo=1 O' +
        'R usu.Tipo IS NULL)'
      'WHERE imv.Codigo=:P0')
    Left = 492
    Top = 16
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUsersCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrUsersUSER_NOME: TWideStringField
      FieldName = 'USER_NOME'
      Size = 100
    end
    object QrUsersUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
    object QrUsersAPTO: TIntegerField
      FieldName = 'APTO'
      Required = True
    end
    object QrUsersUser_ID: TIntegerField
      FieldName = 'User_ID'
    end
    object QrUsersCodCliEsp: TIntegerField
      FieldName = 'CodCliEsp'
    end
    object QrUsersCodigoEsp: TIntegerField
      FieldName = 'CodigoEsp'
    end
    object QrUsersUsername: TWideStringField
      FieldName = 'Username'
      Size = 32
    end
    object QrUsersPassword: TWideStringField
      FieldName = 'Password'
      Size = 32
    end
    object QrUsersLoginID: TWideStringField
      FieldName = 'LoginID'
      Size = 32
    end
    object QrUsersLastAcess: TDateTimeField
      FieldName = 'LastAcess'
    end
    object QrUsersTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrUsersLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrUsersDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrUsersDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrUsersUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrUsersUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrUsersNIVEL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NIVEL'
      Size = 15
      Calculated = True
    end
    object QrUsersPropriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
    object QrUsersEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrUsersUSUARIO: TFloatField
      FieldName = 'USUARIO'
    end
  end
  object DsUsers: TDataSource
    DataSet = QrUsers
    Left = 520
    Top = 16
  end
  object QrExiste: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Password '
      'FROM users'
      'WHERE Password=:P0')
    Left = 540
    Top = 372
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrExistePassword: TWideStringField
      FieldName = 'Password'
      Size = 32
    end
  end
  object QrWPerfil: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 700
    Top = 336
    object QrWPerfilCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWPerfilNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsWPerfil: TDataSource
    DataSet = QrWPerfil
    Left = 728
    Top = 336
  end
  object QrWPerfilImv: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 708
    Top = 392
    object QrWPerfilImvCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrWPerfilImvNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsWPerfilImv: TDataSource
    DataSet = QrWPerfilImv
    Left = 736
    Top = 392
  end
end
