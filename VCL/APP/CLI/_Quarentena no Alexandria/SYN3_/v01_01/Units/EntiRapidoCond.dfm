object FmEntiRapidoCond: TFmEntiRapidoCond
  Left = 371
  Top = 198
  Caption = 'CAD-CONDO-006 :: Cadastro R'#225'pido'
  ClientHeight = 634
  ClientWidth = 694
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel3: TPanel
    Left = 0
    Top = 373
    Width = 694
    Height = 147
    Align = alBottom
    BevelOuter = bvNone
    TabOrder = 1
    object Label39: TLabel
      Left = 4
      Top = 8
      Width = 24
      Height = 13
      Caption = 'CEP:'
      FocusControl = EdCEP
    end
    object Label97: TLabel
      Left = 110
      Top = 8
      Width = 77
      Height = 13
      Caption = 'Tipo logradouro:'
    end
    object Label31: TLabel
      Left = 311
      Top = 8
      Width = 84
      Height = 13
      Caption = 'Nome logradouro:'
      FocusControl = EdRua
    end
    object Label32: TLabel
      Left = 4
      Top = 51
      Width = 40
      Height = 13
      Caption = 'N'#250'mero:'
      FocusControl = EdNumero
    end
    object LaPrefixo: TLabel
      Left = 73
      Top = 51
      Width = 116
      Height = 13
      Caption = 'Prefixo do complemento:'
      FocusControl = EdCompl_Prefixo
    end
    object Label34: TLabel
      Left = 199
      Top = 51
      Width = 30
      Height = 13
      Caption = 'Bairro:'
      FocusControl = EdBairro
    end
    object Label7: TLabel
      Left = 4
      Top = 96
      Width = 17
      Height = 13
      Caption = 'UF:'
    end
    object Label105: TLabel
      Left = 38
      Top = 96
      Width = 36
      Height = 13
      Caption = 'Cidade:'
    end
    object Label110: TLabel
      Left = 480
      Top = 96
      Width = 25
      Height = 13
      Caption = 'Pa'#237's:'
    end
    object EdCEP: TdmkEdit
      Left = 4
      Top = 24
      Width = 77
      Height = 21
      TabOrder = 0
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnEnter = EdCEPEnter
      OnExit = EdCEPExit
    end
    object BtCEP: TBitBtn
      Left = 84
      Top = 24
      Width = 21
      Height = 21
      Caption = '...'
      TabOrder = 1
      TabStop = False
      OnClick = BtCEPClick
    end
    object EdRua: TdmkEdit
      Left = 311
      Top = 24
      Width = 375
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdNumero: TdmkEdit
      Left = 4
      Top = 67
      Width = 64
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdCompl_Prefixo: TdmkEdit
      Left = 73
      Top = 67
      Width = 121
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdBairro: TdmkEdit
      Left = 199
      Top = 67
      Width = 487
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 7
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdCidade: TdmkEditCB
      Left = 38
      Top = 112
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 9
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCidade
      IgnoraDBLookupComboBox = True
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBCidade: TdmkDBLookupComboBox
      Left = 95
      Top = 112
      Width = 380
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsMunici
      TabOrder = 10
      dmkEditCB = EdCidade
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdPais: TdmkEditCB
      Left = 480
      Top = 112
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBPais
      IgnoraDBLookupComboBox = True
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBPais: TdmkDBLookupComboBox
      Left = 536
      Top = 112
      Width = 150
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsBacen_Pais
      TabOrder = 12
      dmkEditCB = EdPais
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdUF: TdmkEdit
      Left = 4
      Top = 112
      Width = 28
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 8
      FormatType = dmktfString
      MskType = fmtUF
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'EUF'
      UpdCampo = 'EUF'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdUFExit
    end
    object EdLograd: TdmkEditCB
      Left = 110
      Top = 24
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBLograd
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBLograd: TdmkDBLookupComboBox
      Left = 166
      Top = 24
      Width = 138
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsListaLograd
      TabOrder = 3
      dmkEditCB = EdLograd
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 694
    Height = 325
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Bevel2: TBevel
      Left = 3
      Top = 275
      Width = 682
      Height = 50
    end
    object Bevel1: TBevel
      Left = 3
      Top = 227
      Width = 218
      Height = 45
    end
    object Label25: TLabel
      Left = 84
      Top = 4
      Width = 31
      Height = 13
      Caption = 'Nome:'
      FocusControl = EdNome
    end
    object Label29: TLabel
      Left = 236
      Top = 44
      Width = 61
      Height = 13
      Caption = 'CNPJ / CPF:'
      FocusControl = EdCNPJCPF
    end
    object Label30: TLabel
      Left = 352
      Top = 44
      Width = 46
      Height = 13
      Caption = 'I.E. / RG:'
      FocusControl = EdIERG
    end
    object Label2: TLabel
      Left = 352
      Top = 4
      Width = 31
      Height = 13
      Caption = 'E-mail:'
      FocusControl = EdEmeio
    end
    object Label43: TLabel
      Left = 456
      Top = 44
      Width = 45
      Height = 13
      Caption = 'Telefone:'
    end
    object Label36: TLabel
      Left = 572
      Top = 44
      Width = 35
      Height = 13
      Caption = 'Celular:'
      FocusControl = EdCel
    end
    object LaSufixo: TLabel
      Left = 144
      Top = 44
      Width = 81
      Height = 13
      Caption = 'Sufixo do compl.:'
      FocusControl = EdCompl_Sufixo
    end
    object Label4: TLabel
      Left = 648
      Top = 4
      Width = 31
      Height = 13
      Caption = 'Andar:'
    end
    object Label5: TLabel
      Left = 532
      Top = 4
      Width = 30
      Height = 13
      Caption = 'Bloco:'
    end
    object Label6: TLabel
      Left = 83
      Top = 45
      Width = 49
      Height = 13
      Caption = 'Garagens:'
    end
    object Label45: TLabel
      Left = 8
      Top = 228
      Width = 33
      Height = 13
      Caption = 'Status:'
    end
    object SpeedButton7: TSpeedButton
      Left = 190
      Top = 244
      Width = 24
      Height = 21
      Caption = '...'
      OnClick = SpeedButton7Click
    end
    object Bevel4: TBevel
      Left = 3
      Top = 87
      Width = 682
      Height = 130
    end
    object Label193: TLabel
      Left = 8
      Top = 92
      Width = 170
      Height = 13
      Caption = 'Protocolo de entrega de bloqueto 1:'
    end
    object SpeedButton15: TSpeedButton
      Left = 656
      Top = 108
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton15Click
    end
    object Label190: TLabel
      Left = 228
      Top = 228
      Width = 53
      Height = 13
      Caption = 'Moradores:'
    end
    object Label194: TLabel
      Left = 288
      Top = 228
      Width = 61
      Height = 13
      Caption = 'Fra'#231#227'o ideal:'
    end
    object Label8: TLabel
      Left = 8
      Top = 280
      Width = 42
      Height = 13
      Caption = 'Inquilino:'
    end
    object SpeedButton1: TSpeedButton
      Left = 659
      Top = 296
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object Label1: TLabel
      Left = 8
      Top = 132
      Width = 170
      Height = 13
      Caption = 'Protocolo de entrega de bloqueto 2:'
    end
    object SpeedButton2: TSpeedButton
      Left = 656
      Top = 148
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton2Click
    end
    object Label3: TLabel
      Left = 8
      Top = 172
      Width = 170
      Height = 13
      Caption = 'Protocolo de entrega de bloqueto 3:'
    end
    object SpeedButton3: TSpeedButton
      Left = 656
      Top = 188
      Width = 21
      Height = 21
      Caption = '...'
      OnClick = SpeedButton3Click
    end
    object EdNome: TdmkEdit
      Left = 84
      Top = 20
      Width = 265
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdCNPJCPF: TdmkEdit
      Left = 236
      Top = 60
      Width = 113
      Height = 21
      TabOrder = 7
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdCNPJCPFExit
    end
    object EdIERG: TdmkEdit
      Left = 352
      Top = 60
      Width = 100
      Height = 21
      TabOrder = 8
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object RGTipo: TRadioGroup
      Left = 4
      Top = 4
      Width = 77
      Height = 77
      Caption = ' Cadastro: '
      ItemIndex = 0
      Items.Strings = (
        '?'
        'Empresa'
        'Pessoal')
      TabOrder = 0
    end
    object EdEmeio: TdmkEdit
      Left = 349
      Top = 23
      Width = 177
      Height = 21
      CharCase = ecLowerCase
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object EdTe1: TdmkEdit
      Left = 456
      Top = 60
      Width = 113
      Height = 21
      TabOrder = 9
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdTe1Exit
    end
    object EdCel: TdmkEdit
      Left = 572
      Top = 60
      Width = 113
      Height = 21
      TabOrder = 10
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdCelExit
    end
    object EdCompl_Sufixo: TdmkEdit
      Left = 144
      Top = 60
      Width = 89
      Height = 21
      CharCase = ecUpperCase
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
      OnExit = EdCompl_SufixoExit
    end
    object CBCondBloco: TdmkDBLookupComboBox
      Left = 532
      Top = 20
      Width = 113
      Height = 21
      KeyField = 'Controle'
      ListField = 'Descri'
      ListSource = DsCondBloco
      TabOrder = 3
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdStatus: TdmkEditCB
      Left = 8
      Top = 244
      Width = 57
      Height = 21
      Alignment = taRightJustify
      TabOrder = 17
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBStatus
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBStatus: TdmkDBLookupComboBox
      Left = 65
      Top = 244
      Width = 123
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Descri'
      ListSource = DsStatus
      TabOrder = 18
      dmkEditCB = EdStatus
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object CkSitImv: TCheckBox
      Left = 393
      Top = 248
      Width = 184
      Height = 17
      Caption = 'Rateia itens base de arrecada'#231#227'o.'
      TabOrder = 21
    end
    object EdProtocolo: TdmkEditCB
      Left = 7
      Top = 108
      Width = 46
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBProtocolo
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBProtocolo: TdmkDBLookupComboBox
      Left = 53
      Top = 108
      Width = 600
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsProtocolos
      TabOrder = 12
      dmkEditCB = EdProtocolo
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object dmkEdAndar: TdmkEdit
      Left = 648
      Top = 20
      Width = 36
      Height = 21
      TabOrder = 4
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 1
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object dmkEdQtdGaragem: TdmkEdit
      Left = 84
      Top = 60
      Width = 57
      Height = 21
      Alignment = taRightJustify
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 1
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdMoradores: TdmkEdit
      Left = 228
      Top = 244
      Width = 53
      Height = 21
      Alignment = taRightJustify
      TabOrder = 19
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object EdFracaoIdeal: TdmkEdit
      Left = 288
      Top = 244
      Width = 97
      Height = 21
      Alignment = taRightJustify
      TabOrder = 20
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 6
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,000000'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
    object EdUsuario: TdmkEditCB
      Left = 7
      Top = 296
      Width = 46
      Height = 21
      Alignment = taRightJustify
      TabOrder = 22
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBUsuario
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBUsuario: TdmkDBLookupComboBox
      Left = 53
      Top = 296
      Width = 600
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NO_ENTI'
      ListSource = DsMoradores
      TabOrder = 23
      dmkEditCB = EdUsuario
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdProtocolo2: TdmkEditCB
      Left = 7
      Top = 148
      Width = 46
      Height = 21
      Alignment = taRightJustify
      TabOrder = 13
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBProtocolo2
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBProtocolo2: TdmkDBLookupComboBox
      Left = 53
      Top = 148
      Width = 600
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsProtocolos
      TabOrder = 14
      dmkEditCB = EdProtocolo2
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdProtocolo3: TdmkEditCB
      Left = 7
      Top = 188
      Width = 46
      Height = 21
      Alignment = taRightJustify
      TabOrder = 15
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBProtocolo3
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CBProtocolo3: TdmkDBLookupComboBox
      Left = 53
      Top = 188
      Width = 600
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsProtocolos
      TabOrder = 16
      dmkEditCB = EdProtocolo3
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 694
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object GB_R: TGroupBox
      Left = 646
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 598
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 205
        Height = 32
        Caption = 'Cadastro R'#225'pido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 205
        Height = 32
        Caption = 'Cadastro R'#225'pido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 205
        Height = 32
        Caption = 'Cadastro R'#225'pido'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 564
    Width = 694
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 548
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 546
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 520
    Width = 694
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 690
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrDuplic2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo FROM entidades'
      'WHERE CNPJ=:P0'
      'AND Codigo <> :P1')
    Left = 68
    Top = 6
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDuplic2Codigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsListaLograd: TDataSource
    DataSet = QrListaLograd
    Left = 40
    Top = 6
  end
  object QrListaLograd: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome FROM listalograd'
      'ORDER BY Nome')
    Left = 12
    Top = 6
    object QrListaLogradCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrListaLogradNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 10
    end
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo'
      'FROM entidades '
      'WHERE Codigo=:P0')
    Left = 96
    Top = 6
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsCondBloco: TDataSource
    DataSet = QrCondBloco
    Left = 156
    Top = 5
  end
  object QrCondBloco: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM condbloco'
      'WHERE Codigo =:P0'
      'ORDER BY Descri')
    Left = 128
    Top = 5
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondBlocoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCondBlocoDescri: TWideStringField
      FieldName = 'Descri'
      Size = 100
    end
    object QrCondBlocoPrefixoUH: TWideStringField
      FieldName = 'PrefixoUH'
    end
  end
  object DsProtocolos: TDataSource
    DataSet = QrProtocolos
    Left = 546
    Top = 6
  end
  object QrProtocolos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ptc.Codigo, ptc.Nome'
      'FROM protocolos ptc'
      'WHERE ptc.Codigo > 0'
      'AND ptc.Tipo in (1,2)'
      'ORDER BY Nome')
    Left = 518
    Top = 6
    object QrProtocolosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrProtocolosNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsStatus: TDataSource
    DataSet = QrStatus
    Left = 602
    Top = 6
  end
  object QrStatus: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Descri'
      'FROM status'
      'ORDER BY Descri')
    Left = 574
    Top = 6
    object QrStatusCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStatusDescri: TWideStringField
      FieldName = 'Descri'
      Size = 100
    end
  end
  object QrMoradores: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0,RazaoSocial,Nome) NO_ENTI'
      'FROM entidades'
      'WHERE Cliente4="V"'
      'ORDER BY NO_ENTI')
    Left = 420
    Top = 388
    object QrMoradoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMoradoresNO_ENTI: TWideStringField
      FieldName = 'NO_ENTI'
      Required = True
      Size = 100
    end
  end
  object DsMoradores: TDataSource
    DataSet = QrMoradores
    Left = 448
    Top = 388
  end
  object QrMunici: TMySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM dtb_munici'
      'ORDER BY Nome')
    Left = 252
    Top = 144
    object QrMuniciCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMuniciNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsMunici: TDataSource
    DataSet = QrMunici
    Left = 280
    Top = 144
  end
  object QrBacen_Pais: TMySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bacen_pais'
      'ORDER BY Nome'
      '')
    Left = 564
    Top = 144
    object QrBacen_PaisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBacen_PaisNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsBacen_Pais: TDataSource
    DataSet = QrBacen_Pais
    Left = 592
    Top = 144
  end
  object PMCEP: TPopupMenu
    Left = 80
    Top = 344
    object Descobrir1: TMenuItem
      Caption = 'BD - &Descobrir'
      OnClick = Descobrir1Click
    end
    object Verificar1: TMenuItem
      Caption = 'BD - &Verificar'
      OnClick = Verificar1Click
    end
    object Internet1: TMenuItem
      Caption = 'Internet - Verificar'
      OnClick = Internet1Click
    end
  end
  object QrProtocolos2: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ptc.Codigo, ptc.Nome'
      'FROM protocolos ptc'
      'WHERE ptc.Codigo > 0'
      'AND ptc.Tipo in (1,2)'
      'ORDER BY Nome')
    Left = 462
    Top = 6
    object IntegerField1: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField1: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsProtocolos2: TDataSource
    DataSet = QrProtocolos2
    Left = 490
    Top = 6
  end
  object QrProtocolos3: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ptc.Codigo, ptc.Nome'
      'FROM protocolos ptc'
      'WHERE ptc.Codigo > 0'
      'AND ptc.Tipo in (1,2)'
      'ORDER BY Nome')
    Left = 406
    Top = 6
    object IntegerField2: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object StringField2: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsProtocolos3: TDataSource
    DataSet = QrProtocolos3
    Left = 434
    Top = 6
  end
end
