unit FlxMensBalViw;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Mask, dmkDBGrid, dmkPermissoes, UnDmkProcFunc, UnDmkEnums;

type
  TFmFlxMensBalViw = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    BitBtn5: TBitBtn;
    LaRegistro: TStaticText;
    Panel6: TPanel;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    Panel7: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label11: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit11: TDBEdit;
    DBGFlxMens: TDBGrid;
    QrFlxMens: TmySQLQuery;
    QrFlxMensRazaoSocial: TWideStringField;
    QrFlxMensAnoMes: TIntegerField;
    QrFlxMensCodigo: TIntegerField;
    QrFlxMensOrdem: TIntegerField;
    QrFlxMensProtocolo: TIntegerField;
    QrFlxMensObserv: TWideStringField;
    QrFlxMensAbert_User: TIntegerField;
    QrFlxMensAbert_Data: TDateTimeField;
    QrFlxMensConci_DLim: TDateField;
    QrFlxMensConci_User: TIntegerField;
    QrFlxMensConci_Data: TDateTimeField;
    QrFlxMensDocum_DLim: TDateField;
    QrFlxMensDocum_User: TIntegerField;
    QrFlxMensDocum_Data: TDateTimeField;
    QrFlxMensContb_DLim: TDateField;
    QrFlxMensContb_User: TIntegerField;
    QrFlxMensContb_Data: TDateTimeField;
    QrFlxMensAnali_DLim: TDateField;
    QrFlxMensAnali_User: TIntegerField;
    QrFlxMensAnali_Data: TDateTimeField;
    QrFlxMensEncad_DLim: TDateField;
    QrFlxMensEncad_User: TIntegerField;
    QrFlxMensEncad_Data: TDateTimeField;
    QrFlxMensEntrg_DLim: TDateField;
    QrFlxMensEntrg_User: TIntegerField;
    QrFlxMensEntrg_Data: TDateTimeField;
    QrFlxMensCONCI_DHX: TWideStringField;
    QrFlxMensDOCUM_DHX: TWideStringField;
    QrFlxMensCONTB_DHX: TWideStringField;
    QrFlxMensANALI_DHX: TWideStringField;
    QrFlxMensENCAD_DHX: TWideStringField;
    QrFlxMensENTRG_DHX: TWideStringField;
    QrFlxMensANOMES_TXT: TWideStringField;
    QrFlxMensL01: TWideStringField;
    QrFlxMensL02: TWideStringField;
    QrFlxMensL03: TWideStringField;
    QrFlxMensL04: TWideStringField;
    QrFlxMensL05: TWideStringField;
    QrFlxMensL06: TWideStringField;
    QrFlxMensConci_Situ: TLargeintField;
    QrFlxMensDocum_Situ: TLargeintField;
    QrFlxMensContb_Situ: TLargeintField;
    QrFlxMensAnali_Situ: TLargeintField;
    QrFlxMensEncad_Situ: TLargeintField;
    QrFlxMensEntrg_Situ: TLargeintField;
    QrFlxMensConci_DLim_TXT: TWideStringField;
    QrFlxMensDocum_DLim_TXT: TWideStringField;
    QrFlxMensContb_DLim_TXT: TWideStringField;
    QrFlxMensAnali_DLim_TXT: TWideStringField;
    QrFlxMensEncad_DLim_TXT: TWideStringField;
    QrFlxMensEntrg_DLim_TXT: TWideStringField;
    DsFlxMens: TDataSource;
    DBText1: TDBText;
    BitBtn1: TBitBtn;
    BtDesfazOrdenacao: TBitBtn;
    dmkPermissoes1: TdmkPermissoes;
    QrFlxMensWeb01_DLim: TDateField;
    QrFlxMensWeb01_User: TIntegerField;
    QrFlxMensWeb01_Data: TDateTimeField;
    QrFlxMensL07: TWideStringField;
    QrFlxMensWeb01_DLim_TXT: TWideStringField;
    Label6: TLabel;
    DBEdit6: TDBEdit;
    QrFlxMensWeb01_Situ: TLargeintField;
    QrFlxMensWEB01_DHX: TWideStringField;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrFlxMensAfterScroll(DataSet: TDataSet);
    procedure QrFlxMensBeforeClose(DataSet: TDataSet);
    procedure QrFlxMensCalcFields(DataSet: TDataSet);
    procedure DBGFlxMensCellClick(Column: TColumn);
    procedure DBGFlxMensDblClick(Sender: TObject);
    procedure DBGFlxMensDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure DBGFlxMensTitleClick(Column: TColumn);
    procedure BtDesfazOrdenacaoClick(Sender: TObject);
  private
    { Private declarations }
    FOrderDesc: Boolean;
  public
    { Public declarations }
    procedure ReopenFlx(AnoMes, Empresa: Integer);
  end;

  var
  FmFlxMensBalViw: TFmFlxMensBalViw;

implementation

uses Module, UnMyObjects, ModuleGeral, FlxMensSel, FlxMensAti, MyDBCheck,
  FlxMensBalPer, FlxMensBalNew, UMySQLModule, MyVCLSkin, UnGOTOy;

{$R *.DFM}

procedure TFmFlxMensBalViw.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmFlxMensBalViw.BitBtn1Click(Sender: TObject);
begin
  if QrFlxMens.RecordCount > 0 then
  begin
    if DBCheck.CriaFm(TFmFlxMensSel, FmFlxMensSel, afmoNegarComAviso) then
    begin
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      FmFlxMensSel.EdEmpresa.ValueVariant := QrFlxMensCodigo.Value;
      FmFlxMensSel.CBEmpresa.KeyValue     := QrFlxMensCodigo.Value;
      FmFlxMensSel.RGFluxo.ItemIndex      := 1;
      FmFlxMensSel.EdAnoMes.Text          := QrFlxMensANOMES_TXT.Value;
      FmFlxMensSel.ShowModal;
      FmFlxMensSel.Destroy;
      //
      ReopenFlx(QrFlxMensAnoMes.Value, QrFlxMensCodigo.Value);
    end;
  end;
end;

procedure TFmFlxMensBalViw.BitBtn5Click(Sender: TObject);
var
  Localizar: Boolean;
  AnoMes: Integer;
begin
  Application.CreateForm(TFmFlxMensBalPer, FmFlxMensBalPer);
  FmFlxMensBalPer.FLocalizar := False;
  FmFlxMensBalPer.ShowModal;
  Localizar := FmFlxMensBalPer.FLocalizar;
  AnoMes   := FmFlxMensBalPer.FAnoMes;
  FmFlxMensBalPer.Destroy;
  Application.ProcessMessages;
  if Localizar then
    ReopenFlx(AnoMes, 0);
end;

procedure TFmFlxMensBalViw.BtAlteraClick(Sender: TObject);
begin
  if QrFlxMens.RecordCount > 0 then
  begin
    if DBCheck.CriaFm(TFmFlxMensSel, FmFlxMensSel, afmoNegarComAviso) then
    begin
      DModG.ReopenEmpresas(VAR_USUARIO, 0);
      FmFlxMensSel.EdEmpresa.ValueVariant := QrFlxMensCodigo.Value;
      FmFlxMensSel.CBEmpresa.KeyValue     := QrFlxMensCodigo.Value;
      FmFlxMensSel.RGFluxo.ItemIndex      := 1;
      FmFlxMensSel.EdAnoMes.Text          := QrFlxMensANOMES_TXT.Value;
      FmFlxMensSel.ShowModal;
      FmFlxMensSel.Destroy;
      //
      ReopenFlx(QrFlxMensAnoMes.Value, QrFlxMensCodigo.Value);
    end;
  end;
end;

procedure TFmFlxMensBalViw.BtDesfazOrdenacaoClick(Sender: TObject);
begin
  if (QrFlxMens.State <> dsInactive) and (QrFlxMens.RecordCount > 0) then
  begin
    QrFlxMens.SortBy('Ordem ASC');
    QrFlxMens.First;
  end;
end;

procedure TFmFlxMensBalViw.BtExcluiClick(Sender: TObject);
begin
{
  DBCheck.ExcluiRegistro(Dmod.QrUpd, QrFlxMens, 'flxmens',
  ['AnoMes','Codigo'], ['AnoMes','Codigo'], True,
  'Confirma a retirada do condom�nio selecionado deste per�odo?');
}
  if not DBCheck.LiberaPelaSenhaBoss then
    Exit;
  DBCheck.QuaisItens_Exclui(DMod.QrUpd, QrFlxMens, TDBGrid(DBGFlxMens),
  'flxmens', ['codigo', 'anomes'], ['codigo', 'anomes'], istPergunta, '');
end;

procedure TFmFlxMensBalViw.BtIncluiClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFlxMensBalNew, FmFlxMensBalNew, afmoNegarComAviso) then
  begin
    FmFlxMensBalNew.ShowModal;
    FmFlxMensBalNew.Destroy;
  end;
end;

procedure TFmFlxMensBalViw.DBGFlxMensCellClick(Column: TColumn);
var
  Data: TDateTime;
begin
  if Copy(Column.FieldName, 6) = '_Situ' then
  begin
    if QrFlxMens.FieldByName(Copy(Column.FieldName, 1, 5) + '_DLim').AsDateTime < 2 then
      Exit;
    Data := QrFlxMens.FieldByName(Copy(Column.FieldName, 1, 5) + '_Data').AsDateTime;
    if Data > 2 then
      Data := 0
    else
      Data := DModG.ObtemAgora();
    //
    if DBCheck.CriaFm(TFmFlxMensAti, FmFlxMensAti, afmoNegarComAviso) then
    begin
      FmFlxMensAti.FNomeTabela  := 'flxMens';
      FmFlxMensAti.FCampoData   := Copy(Column.FieldName, 1, 5) + '_Data';
      FmFlxMensAti.FCampoUser   := Copy(Column.FieldName, 1, 5) + '_User';
      FmFlxMensAti.EdPasso.Text := QrFlxMens.FieldByName(Column.FieldName).DisplayLabel;
      FmFlxMensAti.EdCodigo.ValueVariant := QrFlxMensCodigo.Value;
      FmFlxMensAti.EdAnoMes.ValueVariant := QrFlxMensAnoMes.Value;
      FmFlxMensAti.EdAgora.ValueVariant  := Data;
      //
      FmFlxMensAti.ShowModal;
      FmFlxMensAti.Destroy;
      //
      ReopenFlx(QrFlxMensAnoMes.Value, QrFlxMensCodigo.Value);
    end;
  end;
end;

procedure TFmFlxMensBalViw.DBGFlxMensDblClick(Sender: TObject);
var
  Protocolo, AnoMes, Codigo: Integer;
  Proto_txt: String;
begin
  if (DBGFlxMens.SelectedField.Name = 'QrFlxMensProtocolo') then
  begin
    Proto_txt := FormatFloat('0', QrFlxMensProtocolo.Value);
    if InputQuery('Protocolo', 'Informe o n�mero:', Proto_txt) then
    begin
      Protocolo := Geral.IMV(Proto_txt);
      AnoMes    := QrFlxMensAnoMes.Value;
      Codigo    := QrFlxMensCodigo.Value;
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'flxmens', False, [
      'Protocolo'], ['AnoMes', 'Codigo'], [
      Protocolo], [AnoMes, Codigo], True) then
        ReopenFlx(AnoMes, Codigo);
    end;
  end;
end;

procedure TFmFlxMensBalViw.DBGFlxMensDrawColumnCell(Sender: TObject;
  const Rect: TRect; DataCol: Integer; Column: TColumn; State: TGridDrawState);
var
  Cor: TColor;
  Data: TDateTime;
  Dias, Situ: Integer;
begin
  if Copy(Column.FieldName, 6) = '_DHX' then
  begin
  end;
  if Copy(Column.FieldName, 6) = '_Situ' then
  begin
    Data := QrFlxMens.FieldByName(Copy(Column.FieldName, 1, 5) + '_Data').AsDateTime;
    if Data < 2 then
    begin
      Data := QrFlxMens.FieldByName(Copy(Column.FieldName, 1, 5) + '_DLim').AsDateTime;
      Dias := Trunc(Data) - Trunc(Date);
      if Data < 2  then Cor := clGray    else
      if Dias <= 0 then Cor := clRed     else
      if Dias = 1  then Cor := $000080FF else // cor de laranja $000080FF
      if Dias = 2  then Cor := clYellow  else
                        Cor := clGreen;
    end else            cor := clWindow;
    MyObjects.DesenhaTextoEmDBGrid(DBGFlxMens, Rect,
      clBlack, Cor, Column.Alignment, Column.Field.DisplayText);
    Situ := QrFlxMens.FieldByName(Copy(Column.FieldName, 1, 5) + '_Situ').AsInteger;
    { Alexandria
    MeuVCLSkin.DrawGridCheck(DBGFlxMens, Rect, 1, Situ, dmkrc0a2);
    }
  end;
end;

procedure TFmFlxMensBalViw.DBGFlxMensTitleClick(Column: TColumn);
var
  Field: string;
begin
  if (QrFlxMens.State <> dsInactive) and (QrFlxMens.RecordCount > 0) then
  begin
    Field := LowerCase(Column.Field.FieldName);
    if (Field <> 'conci_dlim_txt') and (Field <> 'docum_dlim_txt') and
      (Field <> 'contb_dlim_txt') and (Field <> 'anali_dlim_txt') and
      (Field <> 'encad_dlim_txt') and (Field <> 'entrg_dlim_txt') and
      (Field <> 'web01_dlim_txt') then
    begin
      if FOrderDesc = False then
      begin
        QrFlxMens.SortBy(Column.Field.FieldName + ' DESC');
        FOrderDesc := True;
      end else
      begin
        QrFlxMens.SortBy(Column.Field.FieldName + ' ASC');
        FOrderDesc := False;
      end;
      QrFlxMens.First;
    end;
  end;
end;

procedure TFmFlxMensBalViw.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  ImgTipo.SQLType := stPsq;
end;

procedure TFmFlxMensBalViw.FormCreate(Sender: TObject);
begin
  FOrderDesc := False;
  //
  GOTOy.VaiInt(vpLast, QrFlxMens, 'FlxMens', 'AnoMes', '', False);
end;

procedure TFmFlxMensBalViw.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmFlxMensBalViw.QrFlxMensAfterScroll(DataSet: TDataSet);
begin
  BtExclui.Enabled := QrFlxMensConci_Data.Value + QrFlxMensDocum_Data.Value +
  QrFlxMensContb_Data.Value + QrFlxMensAnali_Data.Value +
  QrFlxMensEncad_Data.Value + QrFlxMensEntrg_Data.Value < 12;
end;

procedure TFmFlxMensBalViw.QrFlxMensBeforeClose(DataSet: TDataSet);
begin
  BtExclui.Enabled := False;
end;

procedure TFmFlxMensBalViw.QrFlxMensCalcFields(DataSet: TDataSet);
begin
  QrFlxMensCONCI_DHX.Value := Geral.FDT(QrFlxMensConci_Data.Value, 106) + ' - ' + QrFlxMensL01.Value;
  QrFlxMensDOCUM_DHX.Value := Geral.FDT(QrFlxMensDocum_Data.Value, 106) + ' - ' + QrFlxMensL02.Value;
  QrFlxMensCONTB_DHX.Value := Geral.FDT(QrFlxMensContb_Data.Value, 106) + ' - ' + QrFlxMensL03.Value;
  QrFlxMensANALI_DHX.Value := Geral.FDT(QrFlxMensAnali_Data.Value, 106) + ' - ' + QrFlxMensL04.Value;
  QrFlxMensENCAD_DHX.Value := Geral.FDT(QrFlxMensEncad_Data.Value, 106) + ' - ' + QrFlxMensL05.Value;
  QrFlxMensENTRG_DHX.Value := Geral.FDT(QrFlxMensEntrg_Data.Value, 106) + ' - ' + QrFlxMensL06.Value;
  QrFlxMensWEB01_DHX.Value := Geral.FDT(QrFlxMensWeb01_Data.Value, 106) + ' - ' + QrFlxMensL07.Value;
  //
  QrFlxMensConci_DLim_TXT.Value := Geral.FDT(QrFlxMensConci_Data.Value, 12);
  QrFlxMensDocum_DLim_TXT.Value := Geral.FDT(QrFlxMensDocum_Data.Value, 12);
  QrFlxMensContb_DLim_TXT.Value := Geral.FDT(QrFlxMensContb_Data.Value, 12);
  QrFlxMensAnali_DLim_TXT.Value := Geral.FDT(QrFlxMensAnali_Data.Value, 12);
  QrFlxMensEncad_DLim_TXT.Value := Geral.FDT(QrFlxMensEncad_Data.Value, 12);
  QrFlxMensEntrg_DLim_TXT.Value := Geral.FDT(QrFlxMensEntrg_Data.Value, 12);
  QrFlxMensWeb01_DLim_TXT.Value := Geral.FDT(QrFlxMensWeb01_Data.Value, 12);  
  //
  QrFlxMensANOMES_TXT.Value :=
    FormatFloat('00', QrFlxMensAnoMes.Value mod 100) + '/' +
    FormatFloat('00', QrFlxMensAnoMes.Value div 100);
end;

procedure TFmFlxMensBalViw.ReopenFlx(AnoMes, Empresa: Integer);
begin
  QrFlxMens.Close;
  QrFlxMens.Params[00].AsInteger := AnoMes;
  QrFlxMens.Open;
  //
  QrFlxMens.Locate('Codigo', Empresa, []);
end;

procedure TFmFlxMensBalViw.SpeedButton1Click(Sender: TObject);
begin
  GOTOy.VaiInt(vpFirst, QrFlxMens, 'FlxMens', 'AnoMes', '', False);
end;

procedure TFmFlxMensBalViw.SpeedButton2Click(Sender: TObject);
begin
  GOTOy.VaiInt(vpPrior, QrFlxMens, 'FlxMens', 'AnoMes', '', False);
end;

procedure TFmFlxMensBalViw.SpeedButton3Click(Sender: TObject);
begin
  GOTOy.VaiInt(vpNext, QrFlxMens, 'FlxMens', 'AnoMes', '', False);
end;

procedure TFmFlxMensBalViw.SpeedButton4Click(Sender: TObject);
begin
  GOTOy.VaiInt(vpLast, QrFlxMens, 'FlxMens', 'AnoMes', '', False);
end;

end.
