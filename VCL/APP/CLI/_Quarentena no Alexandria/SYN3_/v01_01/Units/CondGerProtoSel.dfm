object FmCondGerProtoSel: TFmCondGerProtoSel
  Left = 396
  Top = 181
  Caption = 'GER-PROTO-009 :: Sele'#231#227'o de protocolo'
  ClientHeight = 528
  ClientWidth = 479
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 479
    Height = 137
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object Label1: TLabel
      Left = 12
      Top = 4
      Width = 34
      Height = 13
      Caption = 'Tarefa:'
    end
    object CBTarefa: TdmkDBLookupComboBox
      Left = 80
      Top = 20
      Width = 385
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsProtocolos
      TabOrder = 1
      dmkEditCB = EdTarefa
      UpdType = utYes
      LocF7SQLMasc = '$#'
      LocF7PreDefProc = f7pNone
    end
    object EdTarefa: TdmkEditCB
      Left = 12
      Top = 20
      Width = 65
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdTarefaChange
      DBLookupComboBox = CBTarefa
      IgnoraDBLookupComboBox = False
      AutoSetIfOnlyOneReg = setregOnlyManual
    end
    object CkRetorna: TCheckBox
      Left = 12
      Top = 48
      Width = 453
      Height = 17
      Caption = 'Devolve documento / aviso que recebeu.'
      TabOrder = 2
      OnClick = CkRetornaClick
    end
    object GroupBox1: TGroupBox
      Left = 12
      Top = 68
      Width = 453
      Height = 65
      Caption = ' Datas limites do item de protocolo: '
      TabOrder = 3
      object LaLimiteSai: TLabel
        Left = 12
        Top = 16
        Width = 32
        Height = 13
        Caption = 'Sa'#237'da:'
      end
      object LaLimiteRem: TLabel
        Left = 136
        Top = 16
        Width = 98
        Height = 13
        Caption = 'Chegada no destino:'
      end
      object LaLimiteRet: TLabel
        Left = 260
        Top = 16
        Width = 41
        Height = 13
        Caption = 'Retorno:'
        Visible = False
      end
      object TPLimiteSai: TdmkEditDateTimePicker
        Left = 12
        Top = 32
        Width = 109
        Height = 21
        Date = 40238.000000000000000000
        Time = 0.476802430552197600
        TabOrder = 0
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPLimiteRem: TdmkEditDateTimePicker
        Left = 136
        Top = 32
        Width = 109
        Height = 21
        Date = 40238.000000000000000000
        Time = 0.476802430552197600
        TabOrder = 1
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
      object TPLimiteRet: TdmkEditDateTimePicker
        Left = 260
        Top = 32
        Width = 109
        Height = 21
        Date = 40238.000000000000000000
        Time = 0.476802430552197600
        TabOrder = 2
        Visible = False
        ReadOnly = False
        DefaultEditMask = '!99/99/99;1;_'
        AutoApplyEditMask = True
        UpdType = utYes
        DatePurpose = dmkdpNone
      end
    end
  end
  object Panel4: TPanel
    Left = 0
    Top = 229
    Width = 479
    Height = 229
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel6: TPanel
      Left = 0
      Top = 0
      Width = 479
      Height = 24
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object CkAbertos: TCheckBox
        Left = 12
        Top = 4
        Width = 181
        Height = 17
        Caption = 'Pesquisar somente lotes abertos.'
        Checked = True
        State = cbChecked
        TabOrder = 0
        OnClick = CkAbertosClick
      end
    end
    object DBGrid1: TDBGrid
      Left = 0
      Top = 24
      Width = 479
      Height = 205
      Align = alClient
      DataSource = DsProtocoPak
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Controle'
          Title.Caption = 'Lote'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataL'
          Title.Caption = 'Data limite'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataI'
          Title.Caption = 'Criado em'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DATAF_TXT'
          Title.Caption = 'Encerrado em'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'MES'
          Title.Caption = 'Per'#237'odo'
          Visible = True
        end>
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 185
    Width = 479
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 475
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 334
        Height = 16
        Caption = 'Selecione o lote no qual ser'#227'o adicionados os protocolos:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 334
        Height = 16
        Caption = 'Selecione o lote no qual ser'#227'o adicionados os protocolos:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 458
    Width = 479
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 333
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtDesiste: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtDesisteClick
      end
    end
    object Panel2: TPanel
      Left = 2
      Top = 15
      Width = 331
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 178
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Novo Lote'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BitBtn1Click
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 479
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 4
    object GB_R: TGroupBox
      Left = 431
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 383
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 258
        Height = 32
        Caption = 'Sele'#231#227'o de Protocolo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 258
        Height = 32
        Caption = 'Sele'#231#227'o de Protocolo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 258
        Height = 32
        Caption = 'Sele'#231#227'o de Protocolo'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object DsProtocolos: TDataSource
    DataSet = QrProtocolos
    Left = 324
    Top = 46
  end
  object QrProtocolos: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ptc.Codigo, ptc.Nome, ptc.Def_Retorn, '
      'ptc.Tipo, ptc.Def_Client'
      'FROM protocolos ptc'
      'WHERE ptc.Codigo > 0'
      'AND ptc.Tipo=:P0'
      'AND ptc.Codigo=:P1'
      'ORDER BY Nome')
    Left = 296
    Top = 46
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrProtocolosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrProtocolosNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrProtocolosDef_Retorn: TIntegerField
      FieldName = 'Def_Retorn'
      Required = True
    end
    object QrProtocolosTipo: TIntegerField
      FieldName = 'Tipo'
    end
    object QrProtocolosDef_Client: TIntegerField
      FieldName = 'Def_Client'
    end
  end
  object QrProtocoPak: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ptp.*, '
      'CONCAT(RIGHT(Mez, 2), '#39'/'#39',  LEFT(Mez + 200000, 4)) MES,'
      
        'IF(ptp.DataF=0,"", DATE_FORMAT(ptp.DataF, "%d/%m/%y" )) DATAF_TX' +
        'T'
      'FROM protocopak ptp'
      'WHERE ptp.Codigo=:P0'
      'AND ptp.DataF>:P1'
      'ORDER BY DataI DESC')
    Left = 12
    Top = 217
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrProtocoPakCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrProtocoPakControle: TIntegerField
      FieldName = 'Controle'
      Required = True
      DisplayFormat = '000000'
    end
    object QrProtocoPakDataI: TDateField
      FieldName = 'DataI'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrProtocoPakDataL: TDateField
      FieldName = 'DataL'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrProtocoPakDataF: TDateField
      FieldName = 'DataF'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrProtocoPakLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrProtocoPakDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrProtocoPakDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrProtocoPakUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrProtocoPakUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrProtocoPakAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrProtocoPakAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrProtocoPakMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrProtocoPakMES: TWideStringField
      FieldName = 'MES'
      Required = True
      Size = 7
    end
    object QrProtocoPakDATAF_TXT: TWideStringField
      FieldName = 'DATAF_TXT'
      Size = 8
    end
  end
  object DsProtocoPak: TDataSource
    DataSet = QrProtocoPak
    Left = 40
    Top = 217
  end
end
