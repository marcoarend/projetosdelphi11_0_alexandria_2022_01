object FmBloqParcAviso: TFmBloqParcAviso
  Left = 339
  Top = 185
  Caption = 'BLQ-REPAR-005 :: Aviso de Reparcelamentos'
  ClientHeight = 403
  ClientWidth = 630
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 630
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 582
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 534
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 325
        Height = 32
        Caption = 'Aviso de Reparcelamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 325
        Height = 32
        Caption = 'Aviso de Reparcelamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 325
        Height = 32
        Caption = 'Aviso de Reparcelamentos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 333
    Width = 630
    Height = 70
    Align = alBottom
    TabOrder = 1
    object PnSaiDesis: TPanel
      Left = 484
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 482
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtLocalizar: TBitBtn
        Tag = 14
        Left = 12
        Top = 4
        Width = 120
        Height = 40
        Caption = '&Localizar'
        TabOrder = 0
        OnClick = BtLocalizarClick
        NumGlyphs = 2
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 113
    Width = 630
    Height = 220
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 2
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 630
      Height = 220
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 630
        Height = 220
        Align = alClient
        TabOrder = 0
        object DBGErrRepa: TDBGrid
          Left = 2
          Top = 15
          Width = 626
          Height = 203
          Align = alClient
          DataSource = DsParc
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Width = 52
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECOND'
              Title.Caption = 'Condom'#237'nio'
              Width = 180
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Unidade'
              Width = 70
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMEPROP'
              Title.Caption = 'Propriet'#225'rio'
              Width = 180
              Visible = True
            end>
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 48
    Width = 630
    Height = 65
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 626
      Height = 40
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 18
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 18
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -16
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object QrParc: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT blp.Codigo, cim.Unidade, eci.TipoTabLct,'
      'IF(cli.Tipo=0, cli.RazaoSocial, cli.Nome) NOMECOND,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEPROP'
      'FROM bloqparc blp'
      'LEFT JOIN entidades cli ON cli.Codigo=blp.CodCliEnt'
      'LEFT JOIN condimov cim ON cim.Conta=blp.CodigoEsp'
      'LEFT JOIN entidades ent ON ent.Codigo=blp.CodigoEnt'
      'LEFT JOIN enticliint eci ON eci.CodEnti=cli.Codigo'
      'WHERE NOT (blp.Novo IN (9,-9))')
    Left = 248
    Top = 257
    object QrParcCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrParcUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrParcNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Size = 100
    end
    object QrParcNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrParcTipoTabLct: TSmallintField
      FieldName = 'TipoTabLct'
    end
  end
  object DsParc: TDataSource
    DataSet = QrParc
    Left = 277
    Top = 257
  end
end
