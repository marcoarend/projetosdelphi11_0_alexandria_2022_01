object FmFlxMensBlqViw: TFmFlxMensBlqViw
  Left = 339
  Top = 185
  Caption = 'FLX-BLOQU-003 :: Panorama do Fluxo dos Bloquetos'
  ClientHeight = 497
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 960
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 165
      Height = 48
      Align = alLeft
      TabOrder = 1
      object DBText1: TDBText
        Left = 2
        Top = 15
        Width = 161
        Height = 31
        Align = alClient
        Alignment = taCenter
        DataField = 'ANOMES_TXT'
        DataSource = DsFlxMens
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clHighlight
        Font.Height = -24
        Font.Name = 'Arial Black'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitHeight = 28
      end
    end
    object GB_M: TGroupBox
      Left = 165
      Top = 0
      Width = 795
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 401
        Height = 32
        Caption = 'Panorama do Fluxo de Bloquetos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 401
        Height = 32
        Caption = 'Panorama do Fluxo de Bloquetos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 401
        Height = 32
        Caption = 'Panorama do Fluxo de Bloquetos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 341
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 341
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 1008
        Height = 341
        Align = alClient
        TabOrder = 0
        object DBGFlxBlq: TDBGrid
          Left = 2
          Top = 15
          Width = 1004
          Height = 223
          Align = alClient
          DataSource = DsFlxMens
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          OnCellClick = DBGFlxBlqCellClick
          OnDrawColumnCell = DBGFlxBlqDrawColumnCell
          OnTitleClick = DBGFlxBlqTitleClick
          Columns = <
            item
              Expanded = False
              FieldName = 'Ordem'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'C'#243'digo'
              Width = 40
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Observ'
              Title.Caption = 'Observa'#231#227'o'
              Width = 250
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Folha_Situ'
              Title.Caption = 'Folha'
              Width = 21
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LeiAr_Situ'
              Title.Caption = 'Leitura/Arrec.'
              Width = 21
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Fecha_Situ'
              Title.Caption = 'Fechamento'
              Width = 21
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Risco_Situ'
              Title.Caption = 'Risco'
              Width = 21
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Print_Situ'
              Title.Caption = 'Impress'#227'o'
              Width = 21
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Proto_Situ'
              Title.Caption = 'Protocolo'
              Width = 21
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Relat_Situ'
              Title.Caption = 'Relat'#243'rio'
              Width = 21
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EMail_Situ'
              Title.Caption = 'Email'
              Width = 21
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Porta_Situ'
              Title.Caption = 'Portaria'
              Width = 21
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Postl_Situ'
              Title.Caption = 'Postal'
              Width = 21
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'RazaoSocial'
              Title.Caption = 'Raz'#227'o Social'
              Width = 260
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Folha_DLim_TXT'
              Title.Caption = 'Folha'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'LeiAr_DLim_TXT'
              Title.Caption = 'Lei.Arr.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Fecha_DLim_TXT'
              Title.Caption = 'Fecham.'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Risco_DLim_TXT'
              Title.Caption = 'Risco'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Print_DLim_TXT'
              Title.Caption = 'Impress'#227'o'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Proto_DLim_TXT'
              Title.Caption = 'Protocolo'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Relat_DLim_TXT'
              Title.Caption = 'Relat'#243'rio'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'EMail_DLim_TXT'
              Title.Caption = 'Email'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Porta_DLim_TXT'
              Title.Caption = 'Portaria'
              Width = 36
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Postl_DLim_TXT'
              Title.Caption = 'Postal'
              Width = 36
              Visible = True
            end>
        end
        object Panel8: TPanel
          Left = 2
          Top = 238
          Width = 1004
          Height = 101
          Align = alBottom
          TabOrder = 1
          object Label1: TLabel
            Left = 332
            Top = 8
            Width = 29
            Height = 13
            Caption = 'Folha:'
            FocusControl = DBEdit1
          end
          object Label2: TLabel
            Left = 664
            Top = 8
            Width = 67
            Height = 13
            Caption = 'Leitura/arrec.:'
            FocusControl = DBEdit2
          end
          object Label3: TLabel
            Left = 8
            Top = 32
            Width = 62
            Height = 13
            Caption = 'Fechamento:'
            FocusControl = DBEdit3
          end
          object Label4: TLabel
            Left = 332
            Top = 32
            Width = 65
            Height = 13
            Caption = 'Admin. riscos:'
            FocusControl = DBEdit4
          end
          object Label5: TLabel
            Left = 664
            Top = 32
            Width = 51
            Height = 13
            Caption = 'Impress'#227'o:'
            FocusControl = DBEdit5
          end
          object Label6: TLabel
            Left = 8
            Top = 56
            Width = 48
            Height = 13
            Caption = 'Protocolo:'
            FocusControl = DBEdit6
          end
          object Label7: TLabel
            Left = 332
            Top = 56
            Width = 50
            Height = 13
            Caption = 'Relat'#243'rios:'
            FocusControl = DBEdit7
          end
          object Label8: TLabel
            Left = 664
            Top = 56
            Width = 28
            Height = 13
            Caption = 'Email:'
            FocusControl = DBEdit8
          end
          object Label9: TLabel
            Left = 8
            Top = 80
            Width = 39
            Height = 13
            Caption = 'Portaria:'
            FocusControl = DBEdit9
          end
          object Label10: TLabel
            Left = 332
            Top = 80
            Width = 32
            Height = 13
            Caption = 'Postal:'
            FocusControl = DBEdit10
          end
          object Label11: TLabel
            Left = 8
            Top = 8
            Width = 43
            Height = 13
            Caption = 'Abertura:'
            FocusControl = DBEdit11
          end
          object DBEdit1: TDBEdit
            Left = 404
            Top = 4
            Width = 248
            Height = 21
            DataField = 'FOLHA_DHX'
            DataSource = DsFlxMens
            TabOrder = 0
          end
          object DBEdit2: TDBEdit
            Left = 740
            Top = 4
            Width = 248
            Height = 21
            DataField = 'LEIAR_DHX'
            DataSource = DsFlxMens
            TabOrder = 1
          end
          object DBEdit3: TDBEdit
            Left = 72
            Top = 28
            Width = 248
            Height = 21
            DataField = 'FECHA_DHX'
            DataSource = DsFlxMens
            TabOrder = 2
          end
          object DBEdit4: TDBEdit
            Left = 404
            Top = 28
            Width = 248
            Height = 21
            DataField = 'RISCO_DHX'
            DataSource = DsFlxMens
            TabOrder = 3
          end
          object DBEdit5: TDBEdit
            Left = 740
            Top = 28
            Width = 248
            Height = 21
            DataField = 'PRINT_DHX'
            DataSource = DsFlxMens
            TabOrder = 4
          end
          object DBEdit6: TDBEdit
            Left = 72
            Top = 52
            Width = 248
            Height = 21
            DataField = 'PROTO_DHX'
            DataSource = DsFlxMens
            TabOrder = 5
          end
          object DBEdit7: TDBEdit
            Left = 404
            Top = 52
            Width = 248
            Height = 21
            DataField = 'RELAT_DHX'
            DataSource = DsFlxMens
            TabOrder = 6
          end
          object DBEdit8: TDBEdit
            Left = 740
            Top = 52
            Width = 248
            Height = 21
            DataField = 'EMAIL_DHX'
            DataSource = DsFlxMens
            TabOrder = 7
          end
          object DBEdit9: TDBEdit
            Left = 72
            Top = 76
            Width = 248
            Height = 21
            DataField = 'PORTA_DHX'
            DataSource = DsFlxMens
            TabOrder = 8
          end
          object DBEdit10: TDBEdit
            Left = 404
            Top = 76
            Width = 248
            Height = 21
            DataField = 'POSTL_DHX'
            DataSource = DsFlxMens
            TabOrder = 9
          end
          object DBEdit11: TDBEdit
            Left = 72
            Top = 4
            Width = 248
            Height = 21
            DataField = 'Abert_Data'
            DataSource = DsFlxMens
            TabOrder = 10
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 389
    Width = 1008
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 1004
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBCntrl: TGroupBox
    Left = 0
    Top = 433
    Width = 1008
    Height = 64
    Align = alBottom
    TabOrder = 3
    object Panel5: TPanel
      Left = 2
      Top = 15
      Width = 239
      Height = 47
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 168
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
      object BitBtn5: TBitBtn
        Tag = 310
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BitBtn5Click
      end
    end
    object LaRegistro: TStaticText
      Left = 241
      Top = 15
      Width = 30
      Height = 17
      Align = alClient
      BevelInner = bvLowered
      BevelKind = bkFlat
      Caption = '[N]: 0'
      TabOrder = 2
    end
    object Panel6: TPanel
      Left = 485
      Top = 15
      Width = 521
      Height = 47
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object BtExclui: TBitBtn
        Tag = 12
        Left = 188
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Exclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtExcluiClick
      end
      object BtAltera: TBitBtn
        Tag = 11
        Left = 96
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Altera'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtAlteraClick
      end
      object BtInclui: TBitBtn
        Tag = 10
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Inclui'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtIncluiClick
      end
      object Panel7: TPanel
        Left = 412
        Top = 0
        Width = 109
        Height = 47
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvNone
        TabOrder = 3
        object BitBtn1: TBitBtn
          Tag = 13
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Sa'#237'da'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn1Click
        end
      end
      object BtDesfazOrdenacao: TBitBtn
        Tag = 329
        Left = 280
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 4
        OnClick = BtDesfazOrdenacaoClick
      end
    end
  end
  object QrFlxBloq: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFlxBloqCalcFields
    SQL.Strings = (
      'SELECT '
      's01.Login L01, s02.Login L02, s03.Login L03, '
      's04.Login L04, s05.Login L05, s06.Login L06, '
      's07.Login L07, s08.Login L08, s09.Login L09, '
      's10.Login L10, flx.*,'
      'IF(Folha_DLim<2, 1, IF(Folha_Data=0, 0, 2)) Folha_Situ,'
      'IF(LeiAr_DLim<2, 1, IF(LeiAr_Data=0, 0, 2)) LeiAr_Situ,'
      'IF(Fecha_DLim<2, 1, IF(Fecha_Data=0, 0, 2)) Fecha_Situ,'
      'IF(Risco_DLim<2, 1, IF(Risco_Data=0, 0, 2)) Risco_Situ,'
      'IF(Print_DLim<2, 1, IF(Print_Data=0, 0, 2)) Print_Situ,'
      'IF(Proto_DLim<2, 1, IF(Proto_Data=0, 0, 2)) Proto_Situ,'
      'IF(Relat_DLim<2, 1, IF(Relat_Data=0, 0, 2)) Relat_Situ,'
      'IF(EMail_DLim<2, 1, IF(EMail_Data=0, 0, 2)) EMail_Situ,'
      'IF(Porta_DLim<2, 1, IF(Porta_Data=0, 0, 2)) Porta_Situ,'
      'IF(Postl_DLim<2, 1, IF(Postl_Data=0, 0, 2)) Postl_Situ,'
      'ent.RazaoSocial'
      'FROM flxbloq flx'
      'LEFT JOIN cond cnd ON cnd.Codigo=flx.Codigo'
      'LEFT JOIN entidades ent ON ent.Codigo=cnd.Cliente'
      'LEFT JOIN senhas s01 ON s01.Numero=Folha_User'
      'LEFT JOIN senhas s02 ON s02.Numero=LeiAr_User'
      'LEFT JOIN senhas s03 ON s03.Numero=Fecha_User'
      'LEFT JOIN senhas s04 ON s04.Numero=Risco_User'
      'LEFT JOIN senhas s05 ON s05.Numero=Print_User'
      'LEFT JOIN senhas s06 ON s06.Numero=Proto_User'
      'LEFT JOIN senhas s07 ON s07.Numero=Relat_User'
      'LEFT JOIN senhas s08 ON s08.Numero=EMail_User'
      'LEFT JOIN senhas s09 ON s09.Numero=Porta_User'
      'LEFT JOIN senhas s10 ON s10.Numero=Postl_User'
      'WHERE flx.AnoMes=:P0'
      'ORDER BY Ordem, Codigo'
      '')
    Left = 72
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFlxBloqRazaoSocial: TWideStringField
      FieldName = 'RazaoSocial'
      Origin = 'entidades.RazaoSocial'
      Size = 100
    end
    object QrFlxBloqAnoMes: TIntegerField
      FieldName = 'AnoMes'
      Origin = 'flxbloq.AnoMes'
    end
    object QrFlxBloqCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'flxbloq.Codigo'
    end
    object QrFlxBloqOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'flxbloq.Ordem'
    end
    object QrFlxBloqddVence: TSmallintField
      FieldName = 'ddVence'
      Origin = 'flxbloq.ddVence'
    end
    object QrFlxBloqFolha_DLim: TDateField
      FieldName = 'Folha_DLim'
      Origin = 'flxbloq.Folha_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxBloqFolha_User: TIntegerField
      FieldName = 'Folha_User'
      Origin = 'flxbloq.Folha_User'
    end
    object QrFlxBloqFolha_Data: TDateTimeField
      FieldName = 'Folha_Data'
      Origin = 'flxbloq.Folha_Data'
    end
    object QrFlxBloqLeiAr_DLim: TDateField
      FieldName = 'LeiAr_DLim'
      Origin = 'flxbloq.LeiAr_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxBloqLeiAr_User: TIntegerField
      FieldName = 'LeiAr_User'
      Origin = 'flxbloq.LeiAr_User'
    end
    object QrFlxBloqLeiAr_Data: TDateTimeField
      FieldName = 'LeiAr_Data'
      Origin = 'flxbloq.LeiAr_Data'
    end
    object QrFlxBloqFecha_DLim: TDateField
      FieldName = 'Fecha_DLim'
      Origin = 'flxbloq.Fecha_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxBloqFecha_User: TIntegerField
      FieldName = 'Fecha_User'
      Origin = 'flxbloq.Fecha_User'
    end
    object QrFlxBloqFecha_Data: TDateTimeField
      FieldName = 'Fecha_Data'
      Origin = 'flxbloq.Fecha_Data'
    end
    object QrFlxBloqRisco_DLim: TDateField
      FieldName = 'Risco_DLim'
      Origin = 'flxbloq.Risco_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxBloqRisco_User: TIntegerField
      FieldName = 'Risco_User'
      Origin = 'flxbloq.Risco_User'
    end
    object QrFlxBloqRisco_Data: TDateTimeField
      FieldName = 'Risco_Data'
      Origin = 'flxbloq.Risco_Data'
    end
    object QrFlxBloqPrint_DLim: TDateField
      FieldName = 'Print_DLim'
      Origin = 'flxbloq.Print_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxBloqPrint_User: TIntegerField
      FieldName = 'Print_User'
      Origin = 'flxbloq.Print_User'
    end
    object QrFlxBloqPrint_Data: TDateTimeField
      FieldName = 'Print_Data'
      Origin = 'flxbloq.Print_Data'
    end
    object QrFlxBloqProto_DLim: TDateField
      FieldName = 'Proto_DLim'
      Origin = 'flxbloq.Proto_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxBloqProto_User: TIntegerField
      FieldName = 'Proto_User'
      Origin = 'flxbloq.Proto_User'
    end
    object QrFlxBloqProto_Data: TDateTimeField
      FieldName = 'Proto_Data'
      Origin = 'flxbloq.Proto_Data'
    end
    object QrFlxBloqRelat_DLim: TDateField
      FieldName = 'Relat_DLim'
      Origin = 'flxbloq.Relat_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxBloqRelat_User: TIntegerField
      FieldName = 'Relat_User'
      Origin = 'flxbloq.Relat_User'
    end
    object QrFlxBloqRelat_Data: TDateTimeField
      FieldName = 'Relat_Data'
      Origin = 'flxbloq.Relat_Data'
    end
    object QrFlxBloqEMail_DLim: TDateField
      FieldName = 'EMail_DLim'
      Origin = 'flxbloq.EMail_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxBloqEMail_User: TIntegerField
      FieldName = 'EMail_User'
      Origin = 'flxbloq.EMail_User'
    end
    object QrFlxBloqEMail_Data: TDateTimeField
      FieldName = 'EMail_Data'
      Origin = 'flxbloq.EMail_Data'
    end
    object QrFlxBloqPorta_DLim: TDateField
      FieldName = 'Porta_DLim'
      Origin = 'flxbloq.Porta_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxBloqPorta_User: TIntegerField
      FieldName = 'Porta_User'
      Origin = 'flxbloq.Porta_User'
    end
    object QrFlxBloqPorta_Data: TDateTimeField
      FieldName = 'Porta_Data'
      Origin = 'flxbloq.Porta_Data'
    end
    object QrFlxBloqPostl_DLim: TDateField
      FieldName = 'Postl_DLim'
      Origin = 'flxbloq.Postl_DLim'
      DisplayFormat = 'dd/mm'
    end
    object QrFlxBloqPostl_User: TIntegerField
      FieldName = 'Postl_User'
      Origin = 'flxbloq.Postl_User'
    end
    object QrFlxBloqPostl_Data: TDateTimeField
      FieldName = 'Postl_Data'
      Origin = 'flxbloq.Postl_Data'
    end
    object QrFlxBloqObserv: TWideStringField
      FieldName = 'Observ'
      Origin = 'flxbloq.Observ'
      Size = 50
    end
    object QrFlxBloqAbert_User: TIntegerField
      FieldName = 'Abert_User'
      Origin = 'flxbloq.Abert_User'
    end
    object QrFlxBloqAbert_Data: TDateTimeField
      FieldName = 'Abert_Data'
      Origin = 'flxbloq.Abert_Data'
    end
    object QrFlxBloqANOMES_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ANOMES_TXT'
      Size = 7
      Calculated = True
    end
    object QrFlxBloqFolha_Situ: TLargeintField
      DisplayLabel = 'Folha de pagamento'
      FieldName = 'Folha_Situ'
      Required = True
      MaxValue = 1
    end
    object QrFlxBloqLeiAr_Situ: TLargeintField
      DisplayLabel = 'Leitura / arrecada'#231#227'o'
      FieldName = 'LeiAr_Situ'
      Required = True
      MaxValue = 1
    end
    object QrFlxBloqFecha_Situ: TLargeintField
      DisplayLabel = 'Fehamento'
      FieldName = 'Fecha_Situ'
      Required = True
      MaxValue = 1
    end
    object QrFlxBloqRisco_Situ: TLargeintField
      DisplayLabel = 'Administradora de riscos'
      FieldName = 'Risco_Situ'
      Required = True
      MaxValue = 1
    end
    object QrFlxBloqPrint_Situ: TLargeintField
      DisplayLabel = 'Impress'#227'o de bloquetos'
      FieldName = 'Print_Situ'
      Required = True
      MaxValue = 1
    end
    object QrFlxBloqProto_Situ: TLargeintField
      DisplayLabel = 'Protocolos'
      FieldName = 'Proto_Situ'
      Required = True
      MaxValue = 1
    end
    object QrFlxBloqRelat_Situ: TLargeintField
      DisplayLabel = 'Relat'#243'rios'
      FieldName = 'Relat_Situ'
      Required = True
      MaxValue = 1
    end
    object QrFlxBloqEMail_Situ: TLargeintField
      DisplayLabel = 'Envio de emails'
      FieldName = 'EMail_Situ'
      Required = True
      MaxValue = 1
    end
    object QrFlxBloqPorta_Situ: TLargeintField
      DisplayLabel = 'Entrega em portaria'
      FieldName = 'Porta_Situ'
      Required = True
      MaxValue = 1
    end
    object QrFlxBloqPostl_Situ: TLargeintField
      DisplayLabel = 'Entrega postal'
      FieldName = 'Postl_Situ'
      Required = True
      MaxValue = 1
    end
    object QrFlxBloqFOLHA_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'FOLHA_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxBloqLEIAR_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'LEIAR_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxBloqFECHA_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'FECHA_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxBloqRISCO_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'RISCO_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxBloqPRINT_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'PRINT_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxBloqPROTO_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'PROTO_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxBloqRELAT_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'RELAT_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxBloqEMAIL_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'EMAIL_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxBloqPORTA_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'PORTA_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxBloqENTRG_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'ENTRG_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxBloqPOSTL_DHX: TWideStringField
      DisplayWidth = 51
      FieldKind = fkCalculated
      FieldName = 'POSTL_DHX'
      Size = 51
      Calculated = True
    end
    object QrFlxBloqL01: TWideStringField
      FieldName = 'L01'
      Required = True
      Size = 30
    end
    object QrFlxBloqL02: TWideStringField
      FieldName = 'L02'
      Required = True
      Size = 30
    end
    object QrFlxBloqL03: TWideStringField
      FieldName = 'L03'
      Required = True
      Size = 30
    end
    object QrFlxBloqL04: TWideStringField
      FieldName = 'L04'
      Required = True
      Size = 30
    end
    object QrFlxBloqL05: TWideStringField
      FieldName = 'L05'
      Required = True
      Size = 30
    end
    object QrFlxBloqL06: TWideStringField
      FieldName = 'L06'
      Required = True
      Size = 30
    end
    object QrFlxBloqL07: TWideStringField
      FieldName = 'L07'
      Required = True
      Size = 30
    end
    object QrFlxBloqL08: TWideStringField
      FieldName = 'L08'
      Required = True
      Size = 30
    end
    object QrFlxBloqL09: TWideStringField
      FieldName = 'L09'
      Required = True
      Size = 30
    end
    object QrFlxBloqL10: TWideStringField
      FieldName = 'L10'
      Required = True
      Size = 30
    end
    object QrFlxBloqFolha_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Folha_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxBloqLeiAr_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'LeiAr_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxBloqFecha_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Fecha_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxBloqRisco_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Risco_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxBloqPrint_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Print_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxBloqProto_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Proto_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxBloqRelat_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Relat_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxBloqEMail_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EMail_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxBloqPorta_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Porta_DLim_TXT'
      Size = 10
      Calculated = True
    end
    object QrFlxBloqPostl_DLim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Postl_DLim_TXT'
      Size = 10
      Calculated = True
    end
  end
  object DsFlxMens: TDataSource
    DataSet = QrFlxBloq
    Left = 100
    Top = 152
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 128
    Top = 152
  end
end
