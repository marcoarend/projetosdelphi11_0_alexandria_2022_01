unit EntiRapidoCond;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, UnInternalConsts, Buttons, DBCtrls, Db, (*DBTables,*)
  UnMLAGeral, UnGOTOy, Mask, UMySQLModule, mySQLDbTables, ComCtrls, Grids,
  DBGrids, dmkGeral, dmkEdit, Variants, dmkEditCB, dmkDBLookupComboBox,
  dmkImage, Menus, DmkDAC_PF, UnDmkEnums;

type
  TFmEntiRapidoCond = class(TForm)
    QrDuplic2: TmySQLQuery;
    QrDuplic2Codigo: TIntegerField;
    Panel1: TPanel;
    EdNome: TdmkEdit;
    Label25: TLabel;
    EdCNPJCPF: TdmkEdit;
    Label29: TLabel;
    Label30: TLabel;
    EdIERG: TdmkEdit;
    DsListaLograd: TDataSource;
    QrListaLograd: TmySQLQuery;
    QrListaLogradCodigo: TIntegerField;
    QrListaLogradNome: TWideStringField;
    RGTipo: TRadioGroup;
    EdEmeio: TdmkEdit;
    Label2: TLabel;
    Label43: TLabel;
    EdTe1: TdmkEdit;
    EdCel: TdmkEdit;
    Label36: TLabel;
    EdCompl_Sufixo: TdmkEdit;
    LaSufixo: TLabel;
    QrPesq: TmySQLQuery;
    QrPesqCodigo: TIntegerField;
    Label4: TLabel;
    CBCondBloco: TdmkDBLookupComboBox;
    Label5: TLabel;
    DsCondBloco: TDataSource;
    QrCondBloco: TmySQLQuery;
    QrCondBlocoControle: TIntegerField;
    QrCondBlocoDescri: TWideStringField;
    Label6: TLabel;
    Label45: TLabel;
    Bevel1: TBevel;
    EdStatus: TdmkEditCB;
    CBStatus: TdmkDBLookupComboBox;
    SpeedButton7: TSpeedButton;
    CkSitImv: TCheckBox;
    Bevel4: TBevel;
    EdProtocolo: TdmkEditCB;
    CBProtocolo: TdmkDBLookupComboBox;
    Label193: TLabel;
    SpeedButton15: TSpeedButton;
    DsProtocolos: TDataSource;
    QrProtocolos: TmySQLQuery;
    QrProtocolosCodigo: TIntegerField;
    QrProtocolosNome: TWideStringField;
    dmkEdAndar: TdmkEdit;
    dmkEdQtdGaragem: TdmkEdit;
    DsStatus: TDataSource;
    QrStatus: TmySQLQuery;
    QrStatusCodigo: TIntegerField;
    QrStatusDescri: TWideStringField;
    Label190: TLabel;
    EdMoradores: TdmkEdit;
    EdFracaoIdeal: TdmkEdit;
    Label194: TLabel;
    Label8: TLabel;
    EdUsuario: TdmkEditCB;
    CBUsuario: TdmkDBLookupComboBox;
    Bevel2: TBevel;
    SpeedButton1: TSpeedButton;
    QrMoradores: TmySQLQuery;
    DsMoradores: TDataSource;
    QrMoradoresCodigo: TIntegerField;
    QrMoradoresNO_ENTI: TWideStringField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtConfirma: TBitBtn;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    Panel3: TPanel;
    Label39: TLabel;
    EdCEP: TdmkEdit;
    BtCEP: TBitBtn;
    Label97: TLabel;
    Label31: TLabel;
    EdRua: TdmkEdit;
    EdNumero: TdmkEdit;
    Label32: TLabel;
    LaPrefixo: TLabel;
    EdCompl_Prefixo: TdmkEdit;
    Label34: TLabel;
    EdBairro: TdmkEdit;
    Label7: TLabel;
    Label105: TLabel;
    EdCidade: TdmkEditCB;
    CBCidade: TdmkDBLookupComboBox;
    Label110: TLabel;
    EdPais: TdmkEditCB;
    CBPais: TdmkDBLookupComboBox;
    QrMunici: TmySQLQuery;
    QrMuniciCodigo: TIntegerField;
    QrMuniciNome: TWideStringField;
    DsMunici: TDataSource;
    QrBacen_Pais: TmySQLQuery;
    QrBacen_PaisCodigo: TIntegerField;
    QrBacen_PaisNome: TWideStringField;
    DsBacen_Pais: TDataSource;
    EdUF: TdmkEdit;
    PMCEP: TPopupMenu;
    Descobrir1: TMenuItem;
    Verificar1: TMenuItem;
    Internet1: TMenuItem;
    EdLograd: TdmkEditCB;
    CBLograd: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdProtocolo2: TdmkEditCB;
    CBProtocolo2: TdmkDBLookupComboBox;
    SpeedButton2: TSpeedButton;
    Label3: TLabel;
    EdProtocolo3: TdmkEditCB;
    CBProtocolo3: TdmkDBLookupComboBox;
    SpeedButton3: TSpeedButton;
    QrProtocolos2: TmySQLQuery;
    IntegerField1: TIntegerField;
    StringField1: TWideStringField;
    DsProtocolos2: TDataSource;
    QrProtocolos3: TmySQLQuery;
    IntegerField2: TIntegerField;
    StringField2: TWideStringField;
    DsProtocolos3: TDataSource;
    QrCondBlocoPrefixoUH: TWideStringField;
    procedure BtDesisteClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure EdCNPJCPFExit(Sender: TObject);
    procedure EdCEPExit(Sender: TObject);
    procedure EdTe1Exit(Sender: TObject);
    procedure EdCelExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton15Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure EdUFExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure BtCEPClick(Sender: TObject);
    procedure Descobrir1Click(Sender: TObject);
    procedure Verificar1Click(Sender: TObject);
    procedure Internet1Click(Sender: TObject);
    procedure EdCEPEnter(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure EdCompl_SufixoExit(Sender: TObject);
  private
    { Private declarations }
    function  InsereEntidade(): Integer;
    procedure ReopenMunici(Query: TmySQLQuery; UF: String);
    procedure ConfiguraComponentesCEP();
    procedure ObtemEnderecoDoCondominio(Codigo: Integer);
    procedure ReopenProtocolos(Cliente: Integer; Query: TmySQLQuery);
  public
    { Public declarations }
    FCliente: Integer;
    FCEP: String;
  end;

var
  FmEntiRapidoCond: TFmEntiRapidoCond;

implementation

uses UnMyObjects, Module, Entidade2, ResIntStrings, UnFinanceiro, Cond, Principal,
  Status, MyDBCheck, MyListas, ModuleGeral, UnEntities;

{$R *.DFM}

procedure TFmEntiRapidoCond.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmEntiRapidoCond.ConfiguraComponentesCEP;
begin
{ Alexandria
  FmEntiCEP.FEdCEP       :=  EdCEP;
  FmEntiCEP.FEdLograd    :=  EdLograd;
  FmEntiCEP.FCBLograd    :=  CBLograd;
  FmEntiCEP.FEdRua       :=  EdRua;
  FmEntiCEP.FEdBairro    :=  EdBairro;
  FmEntiCEP.FEdCidade    :=  EdCidade;
  FmEntiCEP.FEdUF        :=  EdUF;
  FmEntiCEP.FEdPais      :=  nil;
  FmEntiCEP.FEdCodMunici :=  EdCidade;
  FmEntiCEP.FCBCodMunici :=  CBCidade;
  FmEntiCEP.FEdCodiPais  :=  EdPais;
  FmEntiCEP.FCBCodiPais  :=  CBPais;
}
end;

procedure TFmEntiRapidoCond.Descobrir1Click(Sender: TObject);
begin
{ Alexandria
  VAR_ACTIVEPAGE := 7;
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    FmEntiCEP.DBGrid2.Visible := False;
    FmEntiCEP.Painel2.Visible := False;
    ConfiguraComponentesCEP();
    FmEntiCEP.ShowModal;
    FmEntiCEP.Destroy;
  end;
}
end;

function TFmEntiRapidoCond.InsereEntidade(): Integer;
  procedure ConfirmaQueEntidadeEhPropriet(Entidade: Integer);
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE entidades SET Cliente2="V"');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=' + IntToStr(Entidade));
    Dmod.QrUpd.ExecSQL;
  end;
const
  CliInt = 0;  
var
  PUF, EUF, Veiculo, Codigo, Controle, CodUsu, Motivo, ELograd, PLograd,
  CLograd, LLograd, CasasApliDesco, Tipo, ENumero, PNumero, ECodMunici,
  PCodMunici, ECodiPais, PCodiPais: Integer;

  Fantasia, Respons2, Mae, NIRE, Simples, Atividade, Apelido, CPF_Pai, SSP,
  CidadeNatal, EstCivil, Nacionalid, EPais, Ete2, Respons1, Pai,
  FormaSociet, IEST, CPF_Conjuge, DataRG, UFNatal, Ete3, EFax, EContato,
  PPais, Pte2, PFax, PContato, EEmail, Pte3, PEmail, Responsavel,
  Sexo, Profissao, Cargo, DiaRecibo, AjudaEmpV, AjudaEmpP, Cliente3, Fornece5,
  Cadastro, Observacoes, CRua, CCompl, CCidade, CCEP, CTel, CFax, LRua,
  LCompl, LCidade, LCEP, LTel, LFax, Situacao, Grupo, ConjugeNatal,
  Nome1, Nome2, Nome3, Natal4, CreditosL, CreditosF2, CreditosD, CreditosU,
  RazaoSocial, Nome, CPF, CNPJ, IE, RG, ERua, PRua, EBairro, PBairro, ECompl,
  PCompl, ECidade, PCidade, ECEP, PCEP, ETe1, PTe1, ECel, PCel, ENatal, PNatal,
  Cliente1, Cliente2, Fornece1, Fornece2, Fornece3, Fornece4, Terceiro, Mensal,
  Agenda, SenhaQuer, Recibo, Cliente4, Fornece6, Informacoes, CNumero, CBairro,
  CUF, CPais, CCel, CContato, LNumero, LBairro, LUF, LPais, LCel, LContato,
  Nivel, Account, Natal1, Natal2, CreditosI, CreditosV, QuantI1, QuantI3,
  QuantN1, QuantN3, Senha1, TempA, TempD, Agencia, FatorCompra, AdValorem,
  DMaisC, Empresa, SCB, EAtividad, PCidadeCod, ECidadeCod, ConjugeNome,
  Natal3, QuantI2, QuantI4, QuantN2, Banco, ContaCorrente, DMaisD, CBE,
  PPaisCod, Antigo, Contab, PastaTxtFTP, PastaPwdFTP, Nome4, PAtividad,
  EPaisCod, CUF2, MSN1, Protestar, MultaCodi, MultaDias, MultaValr, MultaPerc,
  MultaTiVe, JuroSacado, CPMF, CPF_Resp1, Corrido, Compl: String;

  AjudaV, AjudaP, LimiCred, Comissao, Desco: Double;
  //
begin
  Result := 0;
  Tipo := RGTipo.ItemIndex -1;
  if Tipo = -1 then
  begin
    Geral.MensagemBox('Defina o tipo de cadastro!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if (Length(Geral.SoNumero1a9_TT(EdCNPJCPF.Text))>0)
  then begin
    QrDuplic2.Close;
    QrDuplic2.SQL.Clear;
    if Tipo = 0 then
    begin
      QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
      QrDuplic2.SQL.Add('WHERE CNPJ="'+Geral.SoNumero_TT(EdCNPJCPF.Text)+'"');
    end;
    if Tipo = 1 then
    begin
      QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
      QrDuplic2.SQL.Add('WHERE CPF="'+Geral.SoNumero_TT(EdCNPJCPF.Text)+'"');
    end;
    QrDuplic2.Open;
    if GOTOy.Registros(QrDuplic2) > 0 then
    begin
      if Geral.MensagemBox('Esta entidade j� foi cadastrada com o c�digo n� '+
      IntToStr(QrDuplic2Codigo.Value)+'. Deseja utilizar este n�mero?',
      'Entidade duplicada', MB_YESNOCANCEL+MB_ICONWARNING) = ID_YES then
      begin
        Result := QrDuplic2Codigo.Value;
        ConfirmaQueEntidadeEhPropriet(Result);
      end;
      QrDuplic2.Close;
      Screen.Cursor := crDefault;
      Exit;
    end;
    QrDuplic2.Close;
  end else begin
    QrDuplic2.Close;
    QrDuplic2.SQL.Clear;
    if Tipo = 0 then
    begin
      QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
      QrDuplic2.SQL.Add('WHERE RazaoSocial="'+EdNome.Text+'"');
    end;
    if Tipo = 1 then
    begin
      QrDuplic2.SQL.Add('SELECT Codigo FROM entidades');
      QrDuplic2.SQL.Add('WHERE Nome="'+EdNome.Text+'"');
    end;
    QrDuplic2.Open;
    if GOTOy.Registros(QrDuplic2) > 0 then
    begin
      if Geral.MensagemBox('J� existe um cadastro para "' +
      EdNome.Text + '" sob o n�mero ' + IntToStr(QrDuplic2Codigo.Value) +
      '. Deseja utilizar este cadastro?', 'Entidade duplicada',
      MB_YESNOCANCEL+MB_ICONWARNING) = ID_YES then
      begin
        Result := QrDuplic2Codigo.Value;
        ConfirmaQueEntidadeEhPropriet(Result);
      end;
      QrDuplic2.Close;
      Screen.Cursor := crDefault;
      Exit;
    end;
    QrDuplic2.Close;
  end;
  Compl := Trim(EdCompl_Prefixo.Text);
  (*
  if Trim(EdCompl_Sufixo.Text) <> '' then
    Compl := Compl + ' ' + Trim(EdCompl_Sufixo.Text);
  *)
  if Tipo = 0 then
  begin
    RazaoSocial := EdNome.Text;
    Nome        := '';
    CNPJ        := MLAGeral.FormataCNPJ_TFT(EdCNPJCPF.Text);
    CPF         := '';
    IE          := EdIERG.Text;
    RG          := '';
    ERua        := EdRua.Text;
    PRua        := '';
    ENumero     := Geral.IMV(EdNumero.Text);
    PNumero     := 0;
    ECompl      := Compl;
    PCompl      := '';
    EBairro     := EdBairro.Text;
    PBairro     := '';
    ECodMunici  := EdCidade.ValueVariant;
    PCodMunici  := 0;
    ECodiPais   := EdPais.ValueVariant;
    PCodiPais   := 0;
    EPais       := EdPais.ValueVariant;
    PPais       := '';
    ECidade     := CBCidade.Text;
    PCidade     := '';
    ECEP        := Geral.SoNumero_TT(EdCEP.Text);
    PCEP        := '';
    ETe1        := MlaGeral.SoNumeroESinal_TT(EdTe1.Text);
    PTe1        := '';
    ECel        := MlaGeral.SoNumeroESinal_TT(EdCel.Text);
    PCel        := '';
    EUF         := MLAGeral.GetCodigoUF_da_SiglaUF(EdUF.ValueVariant);
    PUF         := 0;
    EEmail      := EdEmeio.Text;
    PEmail      := '';
  end else begin
    Nome        := EdNome.Text;
    RazaoSocial := '';
    CPF         := MLAGeral.FormataCNPJ_TFT(EdCNPJCPF.Text);
    CNPJ        := '';
    RG          := EdIERG.Text;
    IE          := '';
    PRua        := EdRua.Text;
    ERua        := '';
    PNumero     := Geral.IMV(EdNumero.Text);
    ENumero     := 0;
    PCompl      := Compl;
    ECompl      := '';
    PBairro     := EdBairro.Text;
    EBairro     := '';
    PCodMunici  := EdCidade.ValueVariant;
    ECodMunici  := 0;
    PCodiPais   := EdPais.ValueVariant;
    ECodiPais   := 0;
    EPais       := '';
    PPais       := EdPais.ValueVariant;
    PCidade     := CBCidade.Text;
    ECidade     := '';
    PCEP        := Geral.SoNumero_TT(EdCEP.Text);
    ECEP        := '';
    PTe1        := MlaGeral.SoNumeroESinal_TT(EdTe1.Text);
    ETe1        := '';
    PCel        := MlaGeral.SoNumeroESinal_TT(EdCel.Text);
    ECel        := '';
    PUF         := MLAGeral.GetCodigoUF_da_SiglaUF(EdUF.ValueVariant);
    EUF         := 0;
    EEmail      := '';
    PEmail      := EdEmeio.Text;
  end;
  Mensal   := 'F';
  Veiculo  := 0;
  LimiCred := 0;
  Comissao := 0;
  Desco    := 0;
  CasasApliDesco := 0;
  Motivo := 0;
  ELograd := 0;
  PLograd := 0;
  CLograd := 0;
  LLograd := 0;
  Cliente1 := 'F';
  Cliente2 := 'V';
  Cliente3 := 'F';
  Cliente4 := 'F';
  Fornece1 := 'F';
  Fornece2 := 'F';
  Fornece3 := 'F';
  Fornece4 := 'F';
  Fornece5 := 'F';
  Fornece6 := 'F';
  Terceiro := 'F';
  AjudaV := 0;
  AjudaP := 0;
  Agenda := 'F';
  SenhaQuer := 'F';
  //
  //////////////////////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////////////////////
  Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle', 'Entidades',
            'Entidades', 'Codigo');
  CodUsu := Codigo;          
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'entidades', False, [
    'RazaoSocial', 'Fantasia', 'Respons1', 'Respons2', 'Pai',
    'Mae', 'CNPJ', 'IE', 'NIRE', 'FormaSociet', 'Simples',
    'IEST', 'Atividade', 'Nome', 'Apelido', 'CPF', 'CPF_Pai',
    'CPF_Conjuge', 'RG', 'SSP', 'DataRG', 'CidadeNatal',
    'EstCivil', 'UFNatal', 'Nacionalid', 'ELograd', 'ERua',
    'ENumero', 'ECompl', 'EBairro', 'ECidade', 'EUF', 'ECEP',
    'EPais', 'ETe1', 'Ete2', 'Ete3', 'ECel', 'EFax', 'EEmail',
    'EContato', 'ENatal', 'PLograd', 'PRua', 'PNumero', 'PCompl',
    'PBairro', 'PCidade', 'PUF', 'PCEP', 'PPais', 'PTe1', 'Pte2',
    'Pte3', 'PCel', 'PFax', 'PEmail', 'PContato', 'PNatal',
    'Sexo', 'Responsavel', 'Profissao', 'Cargo', 'Recibo',
    'DiaRecibo', 'AjudaEmpV', 'AjudaEmpP', 'Cliente1', 'Cliente2',
    'Cliente3', 'Cliente4', 'Fornece1', 'Fornece2', 'Fornece3',
    'Fornece4', 'Fornece5', 'Fornece6', 'Terceiro', 'Cadastro',
    'Informacoes', 'Veiculo', 'Mensal', 'Observacoes',
    'Tipo', 'CLograd', 'CRua', 'CNumero', 'CCompl', 'CBairro',
    'CCidade', 'CUF', 'CCEP', 'CPais', 'CTel', 'CCel', 'CFax',
    'CContato', 'LLograd', 'LRua', 'LNumero', 'LCompl', 'LBairro',
    'LCidade', 'LUF', 'LCEP', 'LPais', 'LTel', 'LCel', 'LFax',
    'LContato', 'Comissao', 'Situacao', 'Nivel', 'Grupo',
    'Account', 'ConjugeNome', 'ConjugeNatal', 'Nome1',
    'Natal1', 'Nome2', 'Natal2', 'Nome3', 'Natal3', 'Nome4',
    'Natal4', 'CreditosI', 'CreditosL', 'CreditosF2',
    'CreditosD', 'CreditosU', 'CreditosV', 'Motivo', 'QuantI1',
    'QuantI2', 'QuantI3', 'QuantI4', 'QuantN1', 'QuantN2',
    'QuantN3', 'Agenda', 'SenhaQuer', 'Senha1', 'LimiCred',
    'Desco', 'CasasApliDesco', 'TempA', 'TempD', 'Banco',
    'Agencia', 'ContaCorrente', 'FatorCompra', 'AdValorem',
    'DMaisC', 'DMaisD', 'Empresa', 'CBE', 'SCB', 'PAtividad',
    'EAtividad', 'PCidadeCod', 'ECidadeCod', 'PPaisCod',
    'EPaisCod', 'Antigo', 'CUF2', 'Contab', 'MSN1', 'PastaTxtFTP',
    'PastaPwdFTP', 'Protestar', 'MultaCodi', 'MultaDias',
    'MultaValr', 'MultaPerc', 'MultaTiVe', 'JuroSacado', 'CPMF',
    'Corrido', 'CPF_Resp1', 'ECodiPais ', 'PCodiPais',
    'ECodMunici', 'PCodMunici ', 'CliInt', 'CodUsu'
  ], ['Codigo'], [
    RazaoSocial, Fantasia, Respons1, Respons2, Pai, Mae, CNPJ,
    IE, NIRE, FormaSociet, Simples, IEST, Atividade, Nome,
    Apelido, CPF, CPF_Pai, CPF_Conjuge, RG, SSP, DataRG,
    CidadeNatal, EstCivil, UFNatal, Nacionalid, ELograd, ERua,
    ENumero, ECompl, EBairro, ECidade, EUF, ECEP, EPais, ETe1,
    Ete2, Ete3, ECel, EFax, EEmail, EContato, ENatal, PLograd,
    PRua, PNumero, PCompl, PBairro, PCidade, PUF, PCEP, PPais,
    PTe1, Pte2, Pte3, PCel, PFax, PEmail, PContato, PNatal,
    Sexo, Responsavel, Profissao, Cargo, Recibo, DiaRecibo,
    AjudaEmpV, AjudaEmpP, Cliente1, Cliente2, Cliente3, Cliente4,
    Fornece1, Fornece2, Fornece3, Fornece4, Fornece5, Fornece6,
    Terceiro, Cadastro, Informacoes, Veiculo, Mensal,
    Observacoes, Tipo, CLograd, CRua, CNumero, CCompl, CBairro,
    CCidade, CUF, CCEP, CPais, CTel, CCel, CFax, CContato,
    LLograd, LRua, LNumero, LCompl, LBairro, LCidade, LUF,
    LCEP, LPais, LTel, LCel, LFax, LContato, Comissao,
    Situacao, Nivel, Grupo, Account, ConjugeNome,
    ConjugeNatal, Nome1, Natal1, Nome2, Natal2, Nome3,
    Natal3, Nome4, Natal4, CreditosI, CreditosL, CreditosF2,
    CreditosD, CreditosU, CreditosV, Motivo, QuantI1, QuantI2,
    QuantI3, QuantI4, QuantN1, QuantN2, QuantN3, Agenda,
    SenhaQuer, Senha1, LimiCred, Desco, CasasApliDesco, TempA,
    TempD, Banco, Agencia, ContaCorrente, FatorCompra, AdValorem,
    DMaisC, DMaisD, Empresa, CBE, SCB, PAtividad, EAtividad,
    PCidadeCod, ECidadeCod, PPaisCod, EPaisCod, Antigo, CUF2,
    Contab, MSN1, PastaTxtFTP, PastaPwdFTP, Protestar, MultaCodi,
    MultaDias, MultaValr, MultaPerc, MultaTiVe, JuroSacado,
    CPMF, Corrido, CPF_Resp1, ECodiPais, PCodiPais,
    ECodMunici, PCodMunici, CliInt, CodUsu
  ], [Codigo], True) then
  begin
    (*
    Controle := Entities.CriaContatoGenerico(Codigo, Dmod.QrUpd);
    if Controle > 0 then
    begin
      if Tipo = 0 then
      begin
        if Length(EEmail) > 0 then
          Entities.IncluiEntiMail(Codigo, Controle, 0, 0, 0, EEmail, Dmod.QrUpd);
      end else
      begin
        if Length(PEmail) > 0 then
          Entities.IncluiEntiMail(Codigo, Controle, 0, 0, 0, PEmail, Dmod.QrUpd)
      end;
    end;
    *)
    QrPesq.Close;
    QrPesq.Params[0].AsInteger := Codigo;
    QrPesq.Open;
    if QrPesqCodigo.Value = Codigo then Result := Codigo;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmEntiRapidoCond.Internet1Click(Sender: TObject);
begin
{ Alexandria
  if RGTipo.ItemIndex in [0, 1] then
    U_CEP.ConsultaCEP2(EdCEP, EdLograd, EdRua, EdNumero, EdBairro,
      nil, EdUF, nil, EdNumero, EdCompl_Prefixo, CBLograd, CBCidade,
      CBPais, EdCidade, EdPais)
  else
    Geral.MensagemBox('Selecione o tipo de cadastro!', 'Aviso',
      MB_OK+MB_ICONWARNING);
}
end;

procedure TFmEntiRapidoCond.ObtemEnderecoDoCondominio(Codigo: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(DModG.QrAux, Dmod.MyDB, [
  'SELECT ',
  'IF (ent.Tipo=0, ent.ECEP, ent.PCEP) CEP, ',
  'IF (ent.Tipo=0, ent.ELograd, ent.PLograd) Lograd, ',
  'IF (ent.Tipo=0, ent.ERua, ent.PRua) Rua, ',
  'IF (ent.Tipo=0, ent.ENumero, ent.PNumero) Numero, ',
  'IF (ent.Tipo=0, ent.ECompl, ent.PCompl) Compl, ',
  'IF (ent.Tipo=0, ent.EBairro, ent.PBairro) Bairro, ',
  'IF (ent.Tipo=0, ent.EUF, ent.PUF) UF, ',
  'IF (ent.Tipo=0, ent.ECodMunici, ent.PCodMunici) CodMunici, ',
  'IF (ent.Tipo=0, ent.ECodiPais, ent.PCodiPais) CodPais ',
  'FROM cond con ',
  'LEFT JOIN entidades ent ON ent.Codigo = con.Cliente ',
  'WHERE con.Codigo=' + Geral.FF0(Codigo),
  '']);
  if DModG.QrAux.RecordCount > 0 then
  begin
    EdCEP.ValueVariant           := DModG.QrAux.FieldByName('CEP').AsInteger;
    EdLograd.ValueVariant        := DModG.QrAux.FieldByName('Lograd').AsInteger;
    CBLograd.KeyValue            := DModG.QrAux.FieldByName('Lograd').AsInteger;
    EdRua.ValueVariant           := DModG.QrAux.FieldByName('Rua').AsString;
    EdNumero.ValueVariant        := DModG.QrAux.FieldByName('Numero').AsInteger;
    EdCompl_Prefixo.ValueVariant := DModG.QrAux.FieldByName('Compl').AsString;
    EdBairro.ValueVariant        := DModG.QrAux.FieldByName('Bairro').AsString;
    EdUF.ValueVariant            := DModG.QrAux.FieldByName('UF').AsInteger;
    EdCidade.ValueVariant        := DModG.QrAux.FieldByName('CodMunici').AsInteger;
    CBCidade.KeyValue            := DModG.QrAux.FieldByName('CodMunici').AsInteger;
    EdPais.ValueVariant          := DModG.QrAux.FieldByName('CodPais').AsInteger;
    CBPais.KeyValue              := DModG.QrAux.FieldByName('CodPais').AsInteger;
  end;
end;

procedure TFmEntiRapidoCond.ReopenMunici(Query: TmySQLQuery; UF: String);
begin
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('SELECT mun.Codigo, mun.Nome');
  Query.SQL.Add('FROM dtb_munici mun');
  if Length(UF) <> 0 then
    Query.SQL.Add('WHERE LEFT(mun.Codigo, 2)="' +
    Geral.FF0(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(UF)) + '"');
  Query.SQL.Add('ORDER BY mun.Nome');
  try
    UMyMod.AbreQuery(Query, DModG.AllID_DB);
  except
    DBCheck.AvisaDBTerceiros();
  end;
end;

procedure TFmEntiRapidoCond.ReopenProtocolos(Cliente: Integer; Query: TmySQLQuery);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(Query, Dmod.MyDB, [
    'SELECT ptc.Codigo, ptc.Nome ',
    'FROM protocolos ptc ',
    'WHERE ptc.Codigo > 0 ',
    'AND ptc.Tipo in (1,2) ',
    'AND ptc.Def_Client = ' + Geral.FF0(Cliente),
    'ORDER BY Nome ',
    '']);
end;

procedure TFmEntiRapidoCond.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmEntiRapidoCond.FormShow(Sender: TObject);
begin
  QrCondBloco.Close;
  QrCondBloco.Params[0].AsInteger := FmCond.QrCondCodigo.Value;
  QrCondBloco.Open;
  //
  UMyMod.AbreQuery(QrBacen_Pais, DModG.AllID_DB);
  UMyMod.AbreQuery(QrListaLograd, Dmod.MyDB);
  UMyMod.AbreQuery(QrStatus, Dmod.MyDB);
  //
  ReopenProtocolos(FCliente, QrProtocolos);
  ReopenProtocolos(FCliente, QrProtocolos2);
  ReopenProtocolos(FCliente, QrProtocolos3);
  //
  ReopenMunici(QrMunici, '');
  //
  ObtemEnderecoDoCondominio(FmCond.QrCondCodigo.Value);
end;

procedure TFmEntiRapidoCond.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
end;

procedure TFmEntiRapidoCond.EdCNPJCPFExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCNPJCPF.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if MLAGeral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Geral.MensagemBox(SMLA_NUMEROINVALIDO2, 'Erro', MB_OK+MB_ICONERROR);
      EdCNPJCPF.SetFocus;
    end else EdCNPJCPF.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCNPJCPF.Text := CO_VAZIO;
end;

procedure TFmEntiRapidoCond.EdCompl_SufixoExit(Sender: TObject);
begin
  EdCompl_Prefixo.ValueVariant := EdCompl_Sufixo.ValueVariant;
end;

procedure TFmEntiRapidoCond.EdCEPEnter(Sender: TObject);
begin
  FCEP := Geral.SoNumero_TT(EdCEP.Text);
end;

procedure TFmEntiRapidoCond.EdCEPExit(Sender: TObject);
var
  CEP: String;
begin
{ Alexandria
  CEP := Geral.SoNumero_TT(EdCEP.Text);
  if CEP <> FCEP then
  begin
    if Trim(CEP) <> '' then
      U_CEP.ConsultaCEP(EdCEP, EdLograd, EdRua, EdNumero, EdBairro,
        nil, EdUF, nil, EdNumero, EdCompl_Prefixo, CBLograd,
        EdCidade, CBCidade, EdPais, CBPais);
  end;
}
end;

procedure TFmEntiRapidoCond.EdTe1Exit(Sender: TObject);
begin
  EdTe1.Text := Geral.FormataTelefone_TT(Geral.SoNumero_TT(EdTe1.Text));
end;

procedure TFmEntiRapidoCond.EdUFExit(Sender: TObject);
begin
  ReopenMunici(QrMunici, EdUF.ValueVariant);
end;

procedure TFmEntiRapidoCond.EdCelExit(Sender: TObject);
begin
  EdCel.Text := Geral.FormataTelefone_TT(Geral.SoNumero_TT(EdCel.Text));
end;

procedure TFmEntiRapidoCond.FormCreate(Sender: TObject);
begin
  if VAR_KIND_DEPTO = kdUH then
  begin
    LaPrefixo.Caption := 'Complemento:';
    LaSufixo.Caption  := 'UH:';
  end else
  begin
    LaPrefixo.Caption := 'Prefixo do complemento:';
    LaSufixo.Caption  := 'Sufixo do compl.:';
  end;
end;

procedure TFmEntiRapidoCond.BtCEPClick(Sender: TObject);
begin
  MyObjects.MostraPMCEP(PMCEP, BtCEP);
  try
    EdNumero.SetFocus;
  except
    ;
  end;
end;

procedure TFmEntiRapidoCond.BtConfirmaClick(Sender: TObject);
var
  Codigo, Conta, Propriet, Controle, Andar, QtdGaragem, Status, SitImv,
  Protocolo, Usuario: Integer;
  Unidade: String;
begin
  if Trim(EdNome.Text) = '' then
  begin
    Geral.MensagemBox('Defina o nome da entidade!',
      'Aviso', MB_OK+MB_ICONWARNING);
    EdNome.SetFocus;
    Exit;
  end;
  //
  if RGTipo.ItemIndex = 0 then
  begin
    Geral.MensagemBox('Defina o tipo de cadastro (Empresa ou pessoal)!',
      'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  if CBCondBloco.KeyValue = Null then
  begin
    Geral.MensagemBox('Defina o bloco!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    CBCondBloco.SetFocus;
    Exit;
  end;
  if Trim(EdCompl_Sufixo.ValueVariant) = '' then
  begin
    Geral.MensagemBox('Defina o Sufixo do complemento!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    EdCompl_Sufixo.SetFocus;
    Exit;
  end;  
  //
  Propriet := InsereEntidade;
  if Propriet <> 0 then
  begin
    Codigo     := FmCond.QrCondCodigo.Value;
    Controle   := CBCondBloco.KeyValue;
    Conta      := UMyMod.BuscaEmLivreY_Def('condimov', 'Conta', stIns, 0);
    Andar      := Geral.IMV(dmkEdAndar.Text);
    Unidade    := EdCompl_Sufixo.Text;
    QtdGaragem := Geral.IMV(dmkEdQtdGaragem.Text);
    Status     := Geral.IMV(EdStatus.Text);
    SitImv     := MLAGeral.BoolToInt(CkSitImv.Checked);
    Protocolo  := Geral.IMV(EdProtocolo.Text);
    Usuario    := Geral.IMV(EdUsuario.Text);
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpdM, stIns, 'condimov', False, [
      'Codigo', 'Controle', 'Propriet', 'Andar', 'Unidade', 'QtdGaragem',
      'Status', 'SitImv', 'Protocolo',
      'Moradores', 'FracaoIdeal', 'Usuario'], ['Conta'],
      [Codigo, Controle, Propriet, Andar, Unidade, QtdGaragem,
      Status, SitImv, Protocolo,
      Geral.IMV(EdMoradores.Text), Geral.DMV(EdFracaoIdeal.Text), Usuario],
      [Conta], True) then
    begin
      FmCond.ReopenCondImov(Conta);
      //
      Geral.MensagemBox('Entidade e unidade cadastrados com sucesso!',
      'Mensagem', MB_OK+MB_ICONINFORMATION);
      //
      EdNome.Text          := '';
      EdEmeio.Text         := '';
      EdCompl_Sufixo.Text  := '';
      EdCNPJCPF.Text       := '';
      EdIERG.Text          := '';
      EdTe1.Text           := '';
      EdCel.Text           := '';
      EdNome.SetFocus;
    end;
  end;
end;

procedure TFmEntiRapidoCond.SpeedButton15Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.CadastroDeProtocolos(0, EdProtocolo.ValueVariant);
  if VAR_CADASTRO > 0 then
  begin
    QrProtocolos.Close;
    QrProtocolos.Open;
    //
    EdProtocolo.ValueVariant := VAR_CADASTRO;
    CBProtocolo.KeyValue     := VAR_CADASTRO;
    EdProtocolo.SetFocus;
  end;
end;

procedure TFmEntiRapidoCond.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  if DBCheck.CriaFm(TFmEntidade2, FmEntidade2, afmoLiberado) then
  begin
    FmEntidade2.ShowModal;
    FmEntidade2.Destroy;
    //
    if VAR_CADASTRO <> 0 then
    begin
      (*
      if Geral.MensagemBox('Deseja cadastrar a entidade "' + VAR_ENTIDADENOME +
      '" como inquilino?', 'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE entidades SET Cliente4="V"');
        Dmod.QrUpd.SQL.Add('WHERE Codigo=' + Geral.FF0(VAR_CADASTRO));
        Dmod.QrUpd.ExecSQL;
      end;
      *)
      QrMoradores.Close;
      QrMoradores.Open;
      if QrMoradores.Locate('Codigo', VAR_CADASTRO, []) then
      begin
        EdUsuario.ValueVariant := VAR_CADASTRO;
        CBUsuario.KeyValue     := VAR_CADASTRO;
        EdUsuario.SetFocus;
      end;
    end;
  end;
end;

procedure TFmEntiRapidoCond.SpeedButton2Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.CadastroDeProtocolos(0, EdProtocolo2.ValueVariant);
  if VAR_CADASTRO > 0 then
  begin
    QrProtocolos2.Close;
    QrProtocolos2.Open;
    //
    EdProtocolo2.ValueVariant := VAR_CADASTRO;
    CBProtocolo2.KeyValue     := VAR_CADASTRO;
    EdProtocolo2.SetFocus;
  end;
end;

procedure TFmEntiRapidoCond.SpeedButton3Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  FmPrincipal.CadastroDeProtocolos(0, EdProtocolo3.ValueVariant);
  if VAR_CADASTRO > 0 then
  begin
    QrProtocolos3.Close;
    QrProtocolos3.Open;
    //
    EdProtocolo3.ValueVariant := VAR_CADASTRO;
    CBProtocolo3.KeyValue     := VAR_CADASTRO;
    EdProtocolo3.SetFocus;
  end;
end;

procedure TFmEntiRapidoCond.SpeedButton7Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  if DBCheck.CriaFm(TFmStatus, FmStatus, afmoNegarComAviso) then
  begin
    FmStatus.ShowModal;
    FmStatus.Destroy;
  end;
  if VAR_CADASTRO > 0 then
  begin
    UnDmkDAC_PF.AbreQuery(QrStatus, Dmod.MyDB);
    //
    UMyMod.SetaCodigoPesquisado(EdStatus, CBStatus, QrStatus, VAR_CADASTRO);
    EdStatus.SetFocus;
  end;
end;

procedure TFmEntiRapidoCond.Verificar1Click(Sender: TObject);
begin
{ Alexandria
  VAR_ACTIVEPAGE := 7;
  if DBCheck.CriaFm(TFmEntiCEP, FmEntiCEP, afmoNegarComAviso) then
  begin
    with FmEntiCEP do
    begin
      DBGrid1.Visible := False;
      Painel1.Visible := False;
      ConfiguraComponentesCEP();
      ShowModal;
      Destroy;
    end;
  end;
}
end;

end.

