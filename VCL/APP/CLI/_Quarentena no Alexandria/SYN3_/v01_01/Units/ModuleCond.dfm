object DmCond: TDmCond
  OnCreate = DataModuleCreate
  Height = 678
  Width = 1015
  PixelsPerInch = 96
  object QrEmails: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT DISTINCT imv.Codigo, imv.Controle, imv.Conta Apto, '
      'imv.Unidade, imv.Propriet, 0 Usuario, '
      'IF(en.Tipo=0, en.EEmail, en.PEmail) EMEIO,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOME'
      'FROM condimov imv'
      'LEFT JOIN entidades en ON imv.Propriet=en.Codigo'
      'WHERE imv.Codigo=:P0'
      'AND IF(en.Tipo=0, en.EEmail, en.PEmail) <> '#39#39
      ''
      'UNION'
      ''
      'SELECT DISTINCT imv.Codigo, imv.Controle, imv.Conta Apto, '
      'imv.Unidade, 0 Propriet, imv.Usuario, '
      'IF(en.Tipo=0, en.EEmail, en.PEmail) EMEIO,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOME'
      'FROM condimov imv'
      'LEFT JOIN entidades en ON imv.Usuario=en.Codigo'
      'WHERE imv.Codigo=:P1'
      'AND IF(en.Tipo=0, en.EEmail, en.PEmail) <> '#39#39
      '')
    Left = 44
    Top = 296
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEmailsApto: TIntegerField
      FieldName = 'Apto'
      Required = True
    end
    object QrEmailsUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
    object QrEmailsPropriet: TLargeintField
      FieldName = 'Propriet'
      Required = True
    end
    object QrEmailsUsuario: TLargeintField
      FieldName = 'Usuario'
      Required = True
    end
    object QrEmailsEMEIO: TWideStringField
      FieldName = 'EMEIO'
      Size = 100
    end
    object QrEmailsNOME: TWideStringField
      FieldName = 'NOME'
      Size = 100
    end
    object QrEmailsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEmailsControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object QrSumBaAUni: TMySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor'
      'FROM arrebaauni'
      'WHERE NaoArreRisco=0'
      'AND Apto=:P0'
      ''
      '')
    Left = 944
    Top = 196
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumBaAUniValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCond: TMySQLQuery
    Database = DModG.RV_CEP_DB
    AfterOpen = QrCondAfterOpen
    BeforeClose = QrCondBeforeClose
    AfterScroll = QrCondAfterScroll
    OnCalcFields = QrCondCalcFields
    SQL.Strings = (
      'SELECT IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLI,'
      'IF(ent.Tipo=0, ent.ECidade, ent.PCidade) CIDADE,'
      
        'IF(ent.Tipo=0, ent.EUF, ent.PUF) + 0.000 UF, ufs.Nome NOMEUF, bc' +
        's.DVB,'
      'car.Ativo CART_ATIVO,'
      'car.TipoDoc CAR_TIPODOC, con.*,'
      'IF(sac.Tipo=0, sac.RazaoSocial, sac.Nome) NOMESAC,'
      'IF(ent.Tipo=0, ent.CNPJ, ent.CPF) DOCNUM'
      'FROM cond con'
      'LEFT JOIN entidades sac ON sac.Codigo=con.SacadAvali'
      'LEFT JOIN entidades ent ON ent.Codigo=con.Cliente'
      'LEFT JOIN bancos   bcs ON bcs.Codigo=con.Banco'
      'LEFT JOIN carteiras car ON car.Codigo=con.CartEmiss'
      
        'LEFT JOIN ufs   ufs ON ufs.Codigo=IF(ent.Tipo=0, ent.EUF, ent.PU' +
        'F)'
      'WHERE con.Codigo=:P0')
    Left = 136
    Top = 292
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrCondCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cond.Codigo'
      Required = True
    end
    object QrCondLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cond.Lk'
    end
    object QrCondDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cond.DataCad'
    end
    object QrCondDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cond.DataAlt'
    end
    object QrCondUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cond.UserCad'
    end
    object QrCondUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cond.UserAlt'
    end
    object QrCondCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'cond.Cliente'
      Required = True
    end
    object QrCondObserv: TWideMemoField
      FieldName = 'Observ'
      Origin = 'cond.Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCondAndares: TIntegerField
      FieldName = 'Andares'
      Origin = 'cond.Andares'
      Required = True
    end
    object QrCondTotApt: TIntegerField
      FieldName = 'TotApt'
      Origin = 'cond.TotApt'
      Required = True
    end
    object QrCondDiaVencto: TIntegerField
      FieldName = 'DiaVencto'
      Origin = 'cond.DiaVencto'
      Required = True
    end
    object QrCondBcoLogoPath: TWideStringField
      FieldName = 'BcoLogoPath'
      Origin = 'cond.BcoLogoPath'
      Size = 255
    end
    object QrCondCliLogoPath: TWideStringField
      FieldName = 'CliLogoPath'
      Origin = 'cond.CliLogoPath'
      Size = 255
    end
    object QrCondCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrCondNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Origin = 'ufs.Nome'
      Required = True
      Size = 2
    end
    object QrCondModelBloq: TSmallintField
      FieldName = 'ModelBloq'
      Origin = 'cond.ModelBloq'
      Required = True
    end
    object QrCondMIB: TIntegerField
      FieldName = 'MIB'
      Origin = 'cond.MIB'
      Required = True
    end
    object QrCondMBB: TSmallintField
      FieldName = 'MBB'
      Origin = 'cond.MBB'
      Required = True
    end
    object QrCondMRB: TSmallintField
      FieldName = 'MRB'
      Origin = 'cond.MRB'
      Required = True
    end
    object QrCondPBB: TSmallintField
      FieldName = 'PBB'
      Origin = 'cond.PBB'
      Required = True
    end
    object QrCondMSB: TSmallintField
      FieldName = 'MSB'
      Origin = 'cond.MSB'
      Required = True
    end
    object QrCondPSB: TSmallintField
      FieldName = 'PSB'
      Origin = 'cond.PSB'
      Required = True
    end
    object QrCondMPB: TSmallintField
      FieldName = 'MPB'
      Origin = 'cond.MPB'
      Required = True
    end
    object QrCondMAB: TSmallintField
      FieldName = 'MAB'
      Origin = 'cond.MAB'
      Required = True
    end
    object QrCondAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'cond.AlterWeb'
      Required = True
    end
    object QrCondPwdSite: TSmallintField
      FieldName = 'PwdSite'
      Origin = 'cond.PwdSite'
      Required = True
    end
    object QrCondProLaINSSr: TFloatField
      FieldName = 'ProLaINSSr'
      Origin = 'cond.ProLaINSSr'
      Required = True
    end
    object QrCondProLaINSSp: TFloatField
      FieldName = 'ProLaINSSp'
      Origin = 'cond.ProLaINSSp'
      Required = True
    end
    object QrCondAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'cond.Ativo'
      Required = True
    end
    object QrCondConfigBol: TIntegerField
      FieldName = 'ConfigBol'
      Origin = 'cond.ConfigBol'
      Required = True
    end
    object QrCondMSP: TSmallintField
      FieldName = 'MSP'
      Origin = 'cond.MSP'
      Required = True
    end
    object QrCondCART_ATIVO: TSmallintField
      FieldName = 'CART_ATIVO'
    end
    object QrCondCAR_TIPODOC: TSmallintField
      FieldName = 'CAR_TIPODOC'
    end
    object QrCondOcultaBloq: TSmallintField
      FieldName = 'OcultaBloq'
      Required = True
    end
    object QrCondSomaFracao: TFloatField
      FieldName = 'SomaFracao'
      Required = True
    end
    object QrCondSigla: TWideStringField
      FieldName = 'Sigla'
      Required = True
      Size = 10
    end
    object QrCondBalAgrMens: TSmallintField
      FieldName = 'BalAgrMens'
      Required = True
    end
    object QrCondCompe: TSmallintField
      FieldName = 'Compe'
      Required = True
    end
    object QrCondDOCNUM: TWideStringField
      FieldName = 'DOCNUM'
      Size = 18
    end
    object QrCondDOCNUM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DOCNUM_TXT'
      Size = 30
      Calculated = True
    end
    object QrCondHideCompe: TSmallintField
      FieldName = 'HideCompe'
      Required = True
    end
    object QrCondUF: TFloatField
      FieldName = 'UF'
    end
  end
  object DsCond: TDataSource
    DataSet = QrCond
    Left = 232
    Top = 292
  end
  object QrArreFutI: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = QrArreFutICalcFields
    Left = 140
    Top = 4
    object QrArreFutIControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'arrefut.Controle'
      Required = True
    end
    object QrArreFutIConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'arrefut.Conta'
      Required = True
    end
    object QrArreFutICond: TIntegerField
      FieldName = 'Cond'
      Origin = 'arrefut.Cond'
      Required = True
    end
    object QrArreFutIPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'arrefut.Periodo'
      Required = True
    end
    object QrArreFutIApto: TIntegerField
      FieldName = 'Apto'
      Origin = 'arrefut.Apto'
      Required = True
    end
    object QrArreFutIPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'arrefut.Propriet'
      Required = True
    end
    object QrArreFutIValor: TFloatField
      FieldName = 'Valor'
      Origin = 'arrefut.Valor'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrArreFutITexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'arrefut.Texto'
      Required = True
      Size = 50
    end
    object QrArreFutILk: TIntegerField
      FieldName = 'Lk'
      Origin = 'arrefut.Lk'
    end
    object QrArreFutIDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'arrefut.DataCad'
    end
    object QrArreFutIDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'arrefut.DataAlt'
    end
    object QrArreFutIUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'arrefut.UserCad'
    end
    object QrArreFutIUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'arrefut.UserAlt'
    end
    object QrArreFutINOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrArreFutIUNIDADE: TWideStringField
      FieldName = 'UNIDADE'
      Size = 10
    end
    object QrArreFutINOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrArreFutIPERIODO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODO_TXT'
      Size = 8
      Calculated = True
    end
    object QrArreFutICNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
  end
  object DsArreFutI: TDataSource
    DataSet = QrArreFutI
    Left = 232
    Top = 4
  end
  object QrContasSdo: TMySQLQuery
    Database = DModG.RV_CEP_DB
    AfterScroll = QrContasSdoAfterScroll
    SQL.Strings = (
      'SELECT con.Nome MOMECTA, sdo.*'
      'FROM contassdo sdo'
      'LEFT JOIN contas con ON con.Codigo=sdo.Codigo'
      'WHERE sdo.Entidade=:P0')
    Left = 140
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasSdoMOMECTA: TWideStringField
      FieldName = 'MOMECTA'
      Size = 50
    end
    object QrContasSdoCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrContasSdoEntidade: TIntegerField
      FieldName = 'Entidade'
      Required = True
    end
    object QrContasSdoSdoIni: TFloatField
      FieldName = 'SdoIni'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrContasSdoSdoAtu: TFloatField
      FieldName = 'SdoAtu'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrContasSdoSdoFut: TFloatField
      FieldName = 'SdoFut'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrContasSdoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasSdoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasSdoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasSdoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrContasSdoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrContasSdoTexto: TWideStringField
      FieldName = 'Texto'
      Size = 50
    end
    object QrContasSdoPerAnt: TFloatField
      FieldName = 'PerAnt'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrContasSdoPerAtu: TFloatField
      FieldName = 'PerAtu'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsContasSdo: TDataSource
    DataSet = QrContasSdo
    Left = 232
    Top = 100
  end
  object QrContasTrf: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT con.Nome MOMECTA, trf.DataT, trf.Controle,'
      'trf.CtaOrig, trf.CtaDest, '
      'IF(trf.CtaDest=:P0, trf.Valor, -trf.Valor) VALOR'
      'FROM contastrf trf'
      'LEFT JOIN contas con ON con.Codigo='
      '  if(trf.CtaOrig=:P1, trf.CtaDest, trf.CtaOrig)'
      'WHERE (trf.CtaOrig=:P2 OR trf.CtaDest=:P3)'
      'AND trf.Entidade=:P4'
      'ORDER BY trf.DataT DESC ')
    Left = 140
    Top = 148
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P4'
        ParamType = ptUnknown
      end>
    object QrContasTrfMOMECTA: TWideStringField
      FieldName = 'MOMECTA'
      Size = 50
    end
    object QrContasTrfDataT: TDateField
      FieldName = 'DataT'
      Required = True
    end
    object QrContasTrfControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrContasTrfCtaOrig: TIntegerField
      FieldName = 'CtaOrig'
      Required = True
    end
    object QrContasTrfCtaDest: TIntegerField
      FieldName = 'CtaDest'
      Required = True
    end
    object QrContasTrfVALOR: TFloatField
      FieldName = 'VALOR'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsContasTrf: TDataSource
    DataSet = QrContasTrf
    Left = 232
    Top = 148
  end
  object QrNIA1: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 424
    Top = 193
    object QrNIA1Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'arrebac.Codigo'
      Required = True
    end
    object QrNIA1Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'arrebac.Nome'
      Required = True
      Size = 40
    end
    object QrNIA1Conta: TIntegerField
      FieldName = 'Conta'
      Origin = 'arrebac.Conta'
      Required = True
    end
    object QrNIA1Valor: TFloatField
      FieldName = 'Valor'
      Origin = 'arrebai.Valor'
    end
    object QrNIA1SitCobr: TIntegerField
      FieldName = 'SitCobr'
      Origin = 'arrebai.SitCobr'
    end
    object QrNIA1Parcelas: TIntegerField
      FieldName = 'Parcelas'
      Origin = 'arrebai.Parcelas'
    end
    object QrNIA1ParcPerI: TIntegerField
      FieldName = 'ParcPerI'
      Origin = 'arrebai.ParcPerI'
    end
    object QrNIA1ParcPerF: TIntegerField
      FieldName = 'ParcPerF'
      Origin = 'arrebai.ParcPerF'
    end
    object QrNIA1InfoParc: TSmallintField
      FieldName = 'InfoParc'
      Origin = 'arrebai.InfoParc'
    end
    object QrNIA1Texto: TWideStringField
      FieldName = 'Texto'
      Origin = 'arrebai.Texto'
      Size = 40
    end
    object QrNIA1Controle: TIntegerField
      FieldName = 'Controle'
      Origin = 'arrebai.Controle'
      Required = True
    end
    object QrNIA1Fator: TSmallintField
      FieldName = 'Fator'
      Origin = 'arrebai.Fator'
    end
    object QrNIA1Percent: TFloatField
      FieldName = 'Percent'
      Origin = 'arrebai.Percent'
    end
    object QrNIA1DeQuem: TSmallintField
      FieldName = 'DeQuem'
      Origin = 'arrebai.DeQuem'
    end
    object QrNIA1DoQue: TSmallintField
      FieldName = 'DoQue'
      Origin = 'arrebai.DoQue'
    end
    object QrNIA1ComoCobra: TSmallintField
      FieldName = 'ComoCobra'
      Origin = 'arrebai.ComoCobra'
    end
    object QrNIA1NaoArreSobre: TSmallintField
      FieldName = 'NaoArreSobre'
    end
    object QrNIA1Calculo: TSmallintField
      FieldName = 'Calculo'
    end
    object QrNIA1Arredonda: TFloatField
      FieldName = 'Arredonda'
    end
    object QrNIA1ListaBaU: TIntegerField
      FieldName = 'ListaBaU'
    end
    object QrNIA1DescriCota: TWideStringField
      FieldName = 'DescriCota'
      Size = 10
    end
    object QrNIA1ValFracDef: TFloatField
      FieldName = 'ValFracDef'
    end
    object QrNIA1ValFracAsk: TSmallintField
      FieldName = 'ValFracAsk'
    end
    object QrNIA1Partilha: TSmallintField
      FieldName = 'Partilha'
    end
    object QrNIA1NaoArreRisco: TSmallintField
      FieldName = 'NaoArreRisco'
    end
    object QrNIA1CNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
  end
  object QrAptos: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      
        'SELECT cdi.Conta, cdi.Unidade, cdi.Propriet, CASE WHEN ent.Tipo=' +
        '0 '
      'THEN ent.RazaoSocial ELSE ent.Nome END NOMEPROPRIET, '
      'FracaoIdeal, Moradores'
      'FROM condimov cdi'
      'LEFT JOIN entidades ent ON ent.Codigo=cdi.Propriet'
      'WHERE cdi.SitImv=1'
      'AND cdi.Codigo=:P0'
      'ORDER BY cdi.Unidade')
    Left = 424
    Top = 146
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAptosConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrAptosUnidade: TWideStringField
      FieldName = 'Unidade'
      Required = True
      Size = 10
    end
    object QrAptosPropriet: TIntegerField
      FieldName = 'Propriet'
      Required = True
    end
    object QrAptosNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
    object QrAptosFracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
      Required = True
    end
    object QrAptosMoradores: TIntegerField
      FieldName = 'Moradores'
      Required = True
    end
  end
  object QrTMoradores: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(Moradores) TOTAL'
      'FROM condimov cdi'
      'WHERE cdi.SitImv=1'
      'AND cdi.Codigo=:P0')
    Left = 332
    Top = 146
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTMoradoresTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
  end
  object QrTFracao: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(FracaoIdeal) TOTAL'
      'FROM condimov cdi'
      'WHERE cdi.SitImv=1'
      'AND cdi.Codigo=:P0')
    Left = 44
    Top = 439
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTFracaoTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
  end
  object QrPercAptAll: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(Percent) TotPerc'
      'FROM arrebau'
      'WHERE Controle=:P0')
    Left = 332
    Top = 100
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPercAptAllTotPerc: TFloatField
      FieldName = 'TotPerc'
    end
  end
  object QrPercAptUni: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Percent'
      'FROM arrebau'
      'WHERE Controle=:P0'
      'AND Apto=:P1')
    Left = 332
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPercAptUniPercent: TFloatField
      FieldName = 'Percent'
    end
  end
  object QrConsApt: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 331
    Top = 2
    object QrConsAptValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrSumArreUH: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 44
    Top = 485
    object QrSumArreUHValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCons: TMySQLQuery
    Database = DModG.RV_CEP_DB
    BeforeClose = QrConsBeforeClose
    AfterScroll = QrConsAfterScroll
    OnCalcFields = QrConsCalcFields
    Left = 137
    Top = 194
    object QrConsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrConsNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 50
    end
    object QrConsPreco: TFloatField
      FieldName = 'Preco'
      Required = True
    end
    object QrConsCasas: TSmallintField
      FieldName = 'Casas'
      Required = True
    end
    object QrConsUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Required = True
      Size = 5
    end
    object QrConsUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Required = True
      Size = 5
    end
    object QrConsUnidFat: TFloatField
      FieldName = 'UnidFat'
      Required = True
    end
    object QrConsTOT_UNID1: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOT_UNID1'
      Calculated = True
    end
    object QrConsTOT_UNID2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOT_UNID2'
      Calculated = True
    end
    object QrConsTOT_VALOR: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOT_VALOR'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrConsTOT_UNID1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TOT_UNID1_TXT'
      Size = 30
      Calculated = True
    end
    object QrConsTOT_UNID2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TOT_UNID2_TXT'
      Size = 30
      Calculated = True
    end
  end
  object DsCons: TDataSource
    DataSet = QrCons
    Left = 233
    Top = 194
  end
  object DsCNS: TDataSource
    DataSet = QrCNS
    Left = 233
    Top = 242
  end
  object QrCNS: TMySQLQuery
    Database = DModG.RV_CEP_DB
    AfterOpen = QrCNSAfterOpen
    OnCalcFields = QrCNSCalcFields
    Left = 137
    Top = 242
    object QrCNSUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrCNSCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNSControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCNSApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrCNSMedAnt: TFloatField
      FieldName = 'MedAnt'
    end
    object QrCNSMedAtu: TFloatField
      FieldName = 'MedAtu'
    end
    object QrCNSLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCNSDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCNSDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCNSUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCNSUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCNSCond: TIntegerField
      FieldName = 'Cond'
    end
    object QrCNSPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrCNSConsumo: TFloatField
      FieldName = 'Consumo'
    end
    object QrCNSValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrCNSPreco: TFloatField
      FieldName = 'Preco'
      DisplayFormat = '#,###,##0.0000;-#,###,##0.0000; '
    end
    object QrCNSCasas: TSmallintField
      FieldName = 'Casas'
    end
    object QrCNSUnidLei: TWideStringField
      FieldName = 'UnidLei'
      Size = 5
    end
    object QrCNSUnidImp: TWideStringField
      FieldName = 'UnidImp'
      Size = 5
    end
    object QrCNSUnidFat: TFloatField
      FieldName = 'UnidFat'
      DisplayFormat = '#,###,##0.000000'
    end
    object QrCNSCONSUMO1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CONSUMO1_TXT'
      Size = 30
      Calculated = True
    end
    object QrCNSCONSUMO2_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CONSUMO2_TXT'
      Size = 30
      Calculated = True
    end
    object QrCNSPropriet: TIntegerField
      FieldName = 'Propriet'
    end
    object QrCNSCONSUMO2: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CONSUMO2'
      Calculated = True
    end
    object QrCNSBoleto: TFloatField
      FieldName = 'Boleto'
    end
    object QrCNSGeraTyp: TSmallintField
      FieldName = 'GeraTyp'
    end
    object QrCNSGeraFat: TFloatField
      FieldName = 'GeraFat'
    end
    object QrCNSCasRat: TSmallintField
      FieldName = 'CasRat'
    end
    object QrCNSDifCaren: TSmallintField
      FieldName = 'DifCaren'
    end
    object QrCNSCarencia: TFloatField
      FieldName = 'Carencia'
    end
    object QrCNSLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrCNSBloco: TWideStringField
      FieldName = 'Bloco'
      Size = 100
    end
    object QrCNSCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
  end
  object QrSumBol: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 45
    Top = 391
    object QrSumBolVALOR: TFloatField
      FieldName = 'VALOR'
    end
  end
  object QrSumCP: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 425
    Top = 243
    object QrSumCPCONSUMO: TFloatField
      FieldName = 'CONSUMO'
    end
    object QrSumCPVALOR: TFloatField
      FieldName = 'VALOR'
    end
  end
  object QrCopiaCH: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = QrCopiaCHCalcFields
    SQL.Strings = (
      'SELECT lan.Debito, LPAD(car.Banco1, 3, "0") Banco1,'
      'LPAD(car.Agencia1, 4, "0") Agencia1, car.Conta1,'
      'lan.SerieCH, IF(lan.Documento=0, "",'
      'LPAD(lan.Documento, 6, "0")) Documento, lan.Data,'
      'lan.Descricao, lan.Fornecedor, IF(lan.Fornecedor=0, "",'
      'CASE WHEN frn.Tipo=0 THEN frn.RazaoSocial'
      'ELSE frn.Nome END) NOMEFORNECE,'
      'ban.Nome NOMEBANCO, LPAD(lan.Controle, 6, "0") Controle,'
      'lan.TipoCH, lan.Protocolo, '
      'IF(lan.Protocolo <> 0, 1, 0) EANTem '
      'FROM lct0001a lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN entidades frn ON frn.Codigo=lan.Fornecedor'
      'LEFT JOIN bancos ban ON ban.Codigo=car.Banco1'
      ''
      'WHERE lan.Controle=:P0')
    Left = 44
    Top = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCopiaCHDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrCopiaCHBanco1: TWideStringField
      FieldName = 'Banco1'
      Size = 8
    end
    object QrCopiaCHAgencia1: TWideStringField
      FieldName = 'Agencia1'
      Size = 8
    end
    object QrCopiaCHConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrCopiaCHSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrCopiaCHDocumento: TWideStringField
      FieldName = 'Documento'
      Size = 53
    end
    object QrCopiaCHData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrCopiaCHDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrCopiaCHFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrCopiaCHNOMEFORNECE: TWideStringField
      FieldName = 'NOMEFORNECE'
      Size = 100
    end
    object QrCopiaCHNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Size = 100
    end
    object QrCopiaCHControle: TWideStringField
      FieldName = 'Controle'
      Required = True
      Size = 11
    end
    object QrCopiaCHCRUZADO_SIM: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'CRUZADO_SIM'
      Calculated = True
    end
    object QrCopiaCHCRUZADO_NAO: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'CRUZADO_NAO'
      Calculated = True
    end
    object QrCopiaCHVISADO_SIM: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'VISADO_SIM'
      Calculated = True
    end
    object QrCopiaCHVISADO_NAO: TBooleanField
      FieldKind = fkCalculated
      FieldName = 'VISADO_NAO'
      Calculated = True
    end
    object QrCopiaCHTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrCopiaCHProtocolo: TIntegerField
      FieldName = 'Protocolo'
    end
    object QrCopiaCHEANTem: TLargeintField
      FieldName = 'EANTem'
      Required = True
    end
    object QrCopiaCHEAN128: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EAN128'
      Calculated = True
    end
  end
  object QrContasEnt: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT * FROM '
      'contasent'
      'WHERE Codigo=:P0'
      'AND Entidade=:P1')
    Left = 44
    Top = 248
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrContasEntCntrDebCta: TWideStringField
      FieldName = 'CntrDebCta'
      Size = 50
    end
  end
  object QrNIO_B: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 512
    Top = 148
    object QrNIO_BCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrNIO_BNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 40
    end
    object QrNIO_BConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrNIO_BValor: TFloatField
      FieldName = 'Valor'
    end
    object QrNIO_BSitCobr: TIntegerField
      FieldName = 'SitCobr'
    end
    object QrNIO_BParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrNIO_BParcPerI: TIntegerField
      FieldName = 'ParcPerI'
    end
    object QrNIO_BParcPerF: TIntegerField
      FieldName = 'ParcPerF'
    end
    object QrNIO_BInfoParc: TSmallintField
      FieldName = 'InfoParc'
    end
    object QrNIO_BTexto: TWideStringField
      FieldName = 'Texto'
      Size = 40
    end
    object QrNIO_BControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrNIO_BNOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
  end
  object QrMU6PM: TMySQLQuery
    Database = DModG.MyPID_DB
    OnCalcFields = QrMU6PMCalcFields
    Left = 936
    Top = 4
    object QrMU6PMMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrMU6PMSUM_DEB: TFloatField
      FieldName = 'SUM_DEB'
    end
    object QrMU6PMMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 8
      Calculated = True
    end
  end
  object QrMU6PT: TMySQLQuery
    Database = DModG.MyPID_DB
    Left = 852
    Top = 4
    object QrMU6PTSUM_DEB: TFloatField
      FieldName = 'SUM_DEB'
    end
  end
  object QrMU6MT: TMySQLQuery
    Database = DModG.MyPID_DB
    Left = 852
    Top = 52
    object QrMU6MTSUM_DEB: TFloatField
      FieldName = 'SUM_DEB'
    end
  end
  object QrNIO_A: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 512
    Top = 196
    object QrNIO_ACodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrNIO_ANome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 40
    end
    object QrNIO_AConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrNIO_AValor: TFloatField
      FieldName = 'Valor'
    end
    object QrNIO_ASitCobr: TIntegerField
      FieldName = 'SitCobr'
    end
    object QrNIO_ATexto: TWideStringField
      FieldName = 'Texto'
      Size = 40
    end
    object QrNIO_AControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrNIO_ANOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
  end
  object QrArreFutA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = QrArreFutACalcFields
    Left = 141
    Top = 52
    object QrArreFutANOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrArreFutAUNIDADE: TWideStringField
      FieldName = 'UNIDADE'
      Origin = 'condimov.Unidade'
      Size = 10
    end
    object QrArreFutANOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrArreFutAControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'arrefut.Controle'
      Required = True
    end
    object QrArreFutAConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'arrefut.Conta'
      Required = True
    end
    object QrArreFutACond: TIntegerField
      FieldName = 'Cond'
      Origin = 'arrefut.Cond'
      Required = True
    end
    object QrArreFutAPeriodo: TIntegerField
      FieldName = 'Periodo'
      Origin = 'arrefut.Periodo'
      Required = True
    end
    object QrArreFutAApto: TIntegerField
      FieldName = 'Apto'
      Origin = 'arrefut.Apto'
      Required = True
    end
    object QrArreFutAPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'arrefut.Propriet'
      Required = True
    end
    object QrArreFutAValor: TFloatField
      FieldName = 'Valor'
      Origin = 'arrefut.Valor'
      Required = True
    end
    object QrArreFutATexto: TWideStringField
      FieldName = 'Texto'
      Origin = 'arrefut.Texto'
      Required = True
      Size = 50
    end
    object QrArreFutALk: TIntegerField
      FieldName = 'Lk'
      Origin = 'arrefut.Lk'
    end
    object QrArreFutADataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'arrefut.DataCad'
    end
    object QrArreFutADataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'arrefut.DataAlt'
    end
    object QrArreFutAUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'arrefut.UserCad'
    end
    object QrArreFutAUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'arrefut.UserAlt'
    end
    object QrArreFutAPERIODO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODO_TXT'
      Size = 8
      Calculated = True
    end
    object QrArreFutAInclui: TIntegerField
      FieldName = 'Inclui'
      Origin = 'arrefut.Inclui'
      Required = True
    end
    object QrArreFutACNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
  end
  object DsArreFutA: TDataSource
    DataSet = QrArreFutA
    Left = 233
    Top = 52
  end
  object DsInadTot: TDataSource
    DataSet = QrInadTot
    Left = 512
    Top = 52
  end
  object QrInadTot: TMySQLQuery
    Database = DModG.RV_CEP_DB
    AfterOpen = QrInadTotAfterOpen
    BeforeClose = QrInadTotBeforeClose
    Left = 424
    Top = 52
    object QrInadTotCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object QrInadUsu: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 424
    Top = 100
    object QrInadUsuUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrInadUsuDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrInadUsuCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrInadUsuNOMEPROPRIET: TWideStringField
      FieldName = 'NOMEPROPRIET'
      Size = 100
    end
  end
  object DsInadUsu: TDataSource
    DataSet = QrInadUsu
    Left = 512
    Top = 100
  end
  object QrDebitos: TMySQLQuery
    Database = DModG.MyPID_DB
    Left = 856
    Top = 100
    object QrDebitosCOMPENSADO_TXT: TWideStringField
      FieldName = 'COMPENSADO_TXT'
      Size = 8
    end
    object QrDebitosDATA: TWideStringField
      FieldName = 'DATA'
      Size = 8
    end
    object QrDebitosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrDebitosDEBITO: TFloatField
      FieldName = 'DEBITO'
    end
    object QrDebitosNOTAFISCAL: TLargeintField
      FieldName = 'NOTAFISCAL'
    end
    object QrDebitosSERIECH: TWideStringField
      FieldName = 'SERIECH'
      Size = 10
    end
    object QrDebitosDOCUMENTO: TFloatField
      FieldName = 'DOCUMENTO'
    end
    object QrDebitosMEZ: TLargeintField
      FieldName = 'MEZ'
      Required = True
    end
    object QrDebitosCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrDebitosNOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
    object QrDebitosNOMESGR: TWideStringField
      FieldName = 'NOMESGR'
      Size = 50
    end
    object QrDebitosNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 50
    end
    object QrDebitosITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
    object QrDebitosMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 7
      Calculated = True
    end
    object QrDebitosSERIE_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'SERIE_DOC'
      Size = 30
      Calculated = True
    end
    object QrDebitosNF_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NF_TXT'
      Size = 30
      Calculated = True
    end
    object QrDebitosMES2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES2'
      Size = 5
      Calculated = True
    end
    object QrDebitosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDebitosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrDebitosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDebitosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrDebitosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrDebitosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrDebitosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrDebitosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrDebitosNO_FORNECE: TWideStringField
      FieldName = 'NO_FORNECE'
      Size = 100
    end
  end
  object DsDebitos: TDataSource
    DataSet = QrDebitos
    Left = 940
    Top = 100
  end
  object QrCreditos: TMySQLQuery
    Database = DModG.MyPID_DB
    Left = 856
    Top = 148
    object QrCreditosMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrCreditosCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrCreditosNOMECON: TWideStringField
      FieldName = 'NOMECON'
      Size = 50
    end
    object QrCreditosNOMESGR: TWideStringField
      FieldName = 'NOMESGR'
      Size = 50
    end
    object QrCreditosNOMEGRU: TWideStringField
      FieldName = 'NOMEGRU'
      Size = 50
    end
    object QrCreditosMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 7
      Calculated = True
    end
    object QrCreditosNOMECON_2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECON_2'
      Size = 255
      Calculated = True
    end
    object QrCreditosSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrCreditosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCreditosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrCreditosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrCreditosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrCreditosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrCreditosCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrCreditosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrCreditosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrCreditosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
  end
  object DsCreditos: TDataSource
    DataSet = QrCreditos
    Left = 940
    Top = 148
  end
  object QrResumo: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 28
    Top = 564
    object QrResumoCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrResumoDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrResumoSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      Calculated = True
    end
    object QrResumoFINAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'FINAL'
      Calculated = True
    end
  end
  object QrSaldoA: TMySQLQuery
    Database = DModG.RV_CEP_DB
    Left = 28
    Top = 608
    object QrSaldoAInicial: TFloatField
      FieldName = 'Inicial'
    end
    object QrSaldoASALDO: TFloatField
      FieldName = 'SALDO'
    end
    object QrSaldoATOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL'
      Calculated = True
    end
  end
  object frxDsCond: TfrxDBDataset
    UserName = 'frxDsCond'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMECLI=NOMECLI'
      'Codigo=Codigo'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'Cliente=Cliente'
      'Observ=Observ'
      'Andares=Andares'
      'TotApt=TotApt'
      'DiaVencto=DiaVencto'
      'BcoLogoPath=BcoLogoPath'
      'CliLogoPath=CliLogoPath'
      'DVB=DVB'
      'CIDADE=CIDADE'
      'NOMEUF=NOMEUF'
      'ModelBloq=ModelBloq'
      'MIB=MIB'
      'MBB=MBB'
      'MRB=MRB'
      'PBB=PBB'
      'MSB=MSB'
      'PSB=PSB'
      'MPB=MPB'
      'MAB=MAB'
      'AlterWeb=AlterWeb'
      'PwdSite=PwdSite'
      'ProLaINSSr=ProLaINSSr'
      'ProLaINSSp=ProLaINSSp'
      'Ativo=Ativo'
      'ConfigBol=ConfigBol'
      'MSP=MSP'
      'CART_ATIVO=CART_ATIVO'
      'CAR_TIPODOC=CAR_TIPODOC'
      'OcultaBloq=OcultaBloq'
      'SomaFracao=SomaFracao'
      'Sigla=Sigla'
      'BalAgrMens=BalAgrMens'
      'Compe=Compe'
      'DOCNUM=DOCNUM'
      'DOCNUM_TXT=DOCNUM_TXT'
      'HideCompe=HideCompe'
      'CNAB_Cfg=CNAB_Cfg'
      'UF=UF')
    DataSet = QrCond
    BCDToCurrency = False
    
    Left = 332
    Top = 292
  end
  object frxDsCons: TfrxDBDataset
    UserName = 'frxDsCons'
    CloseDataSource = False
    DataSet = QrCons
    BCDToCurrency = False
    
    Left = 332
    Top = 196
  end
  object frxDsCNS: TfrxDBDataset
    UserName = 'frxDsCNS'
    CloseDataSource = False
    DataSet = QrCNS
    BCDToCurrency = False
    
    Left = 332
    Top = 244
  end
  object QrAgefet: TMySQLQuery
    Database = DModG.RV_CEP_DB
    OnCalcFields = QrAgefetCalcFields
    Left = 508
    Top = 4
    object QrAgefetPrevBaC: TIntegerField
      FieldName = 'PrevBaC'
    end
    object QrAgefetPrevBaI: TIntegerField
      FieldName = 'PrevBaI'
      Required = True
    end
    object QrAgefetCond: TIntegerField
      FieldName = 'Cond'
    end
    object QrAgefetNOMECOND: TWideStringField
      FieldName = 'NOMECOND'
      Size = 100
    end
    object QrAgefetPERIODO_TXT: TWideStringField
      FieldName = 'PERIODO_TXT'
      Size = 7
    end
    object QrAgefetValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
    object QrAgefetTexto: TWideStringField
      FieldName = 'Texto'
      Size = 40
    end
    object QrAgefetSitCobr: TIntegerField
      FieldName = 'SitCobr'
    end
    object QrAgefetParcelas: TIntegerField
      FieldName = 'Parcelas'
    end
    object QrAgefetPrevCod: TIntegerField
      FieldName = 'PrevCod'
    end
    object QrAgefetPeriodo: TIntegerField
      FieldName = 'Periodo'
    end
    object QrAgefetNOMESITCOBR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESITCOBR'
      Size = 30
      Calculated = True
    end
  end
  object QrLocUHs: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT Conta, Unidade'
      'FROM condimov'
      'WHERE Propriet=:P0')
    Left = 424
    Top = 4
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocUHsConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrLocUHsUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
  end
  object QrMU6MM: TMySQLQuery
    Database = DModG.MyPID_DB
    OnCalcFields = QrMU6MMCalcFields
    Left = 936
    Top = 52
    object QrMU6MMSUM_DEB: TFloatField
      FieldName = 'SUM_DEB'
    end
    object QrMU6MMMES: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MES'
      Size = 8
      Calculated = True
    end
    object QrMU6MMMEZ: TIntegerField
      FieldName = 'MEZ'
    end
  end
  object QrSenha: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT cnd.Assistente, snh.Numero'
      'FROM Cond cnd'
      'LEFT JOIN senhas snh ON snh.Funcionario=cnd.Assistente'
      'WHERE cnd.Codigo=1')
    Left = 44
    Top = 100
    object QrSenhaAssistente: TIntegerField
      FieldName = 'Assistente'
    end
    object QrSenhaNumero: TIntegerField
      FieldName = 'Numero'
    end
  end
  object QrAccMgr: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT eci.CodCliInt, cnd.Codigo, '
      'cnd.Assistente, eci.AccManager'
      'FROM cond cnd'
      'RIGHT JOIN enticliint eci ON eci.CodCliInt=cnd.Codigo'
      'WHERE eci.AccManager <> cnd.Assistente'
      'OR cnd.Codigo IS NULL'
      'OR eci.CodCliInt IS NULL')
    Left = 44
    Top = 52
    object QrAccMgrCodCliInt: TIntegerField
      FieldName = 'CodCliInt'
    end
    object QrAccMgrCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrAccMgrAssistente: TIntegerField
      FieldName = 'Assistente'
    end
    object QrAccMgrAccManager: TIntegerField
      FieldName = 'AccManager'
    end
  end
  object QrSumFracao: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT SUM(FracaoIdeal) TOTAL'
      'FROM condimov'
      'WHERE Controle=:P0')
    Left = 44
    Top = 2
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSumFracaoTOTAL: TFloatField
      FieldName = 'TOTAL'
    end
  end
  object QrCNAB_Cfg_: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT *'
      'FROM cnab_cfg '
      'WHERE Codigo=:P0')
    Left = 44
    Top = 152
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_Cfg_Codigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cnab_cfg.Codigo'
    end
    object QrCNAB_Cfg_Nome: TWideStringField
      FieldName = 'Nome'
      Origin = 'cnab_cfg.Nome'
      Size = 50
    end
    object QrCNAB_Cfg_Cedente: TIntegerField
      FieldName = 'Cedente'
      Origin = 'cnab_cfg.Cedente'
    end
    object QrCNAB_Cfg_SacadAvali: TIntegerField
      FieldName = 'SacadAvali'
      Origin = 'cnab_cfg.SacadAvali'
    end
    object QrCNAB_Cfg_CedBanco: TIntegerField
      FieldName = 'CedBanco'
      Origin = 'cnab_cfg.CedBanco'
    end
    object QrCNAB_Cfg_CedAgencia: TIntegerField
      FieldName = 'CedAgencia'
      Origin = 'cnab_cfg.CedAgencia'
    end
    object QrCNAB_Cfg_CedConta: TWideStringField
      FieldName = 'CedConta'
      Origin = 'cnab_cfg.CedConta'
      Size = 30
    end
    object QrCNAB_Cfg_CedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Origin = 'cnab_cfg.CedDAC_A'
      Size = 1
    end
    object QrCNAB_Cfg_CedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Origin = 'cnab_cfg.CedDAC_C'
      Size = 1
    end
    object QrCNAB_Cfg_CedDAC_AC: TWideStringField
      FieldName = 'CedDAC_AC'
      Origin = 'cnab_cfg.CedDAC_AC'
      Size = 1
    end
    object QrCNAB_Cfg_CedNome: TWideStringField
      FieldName = 'CedNome'
      Origin = 'cnab_cfg.CedNome'
      Size = 100
    end
    object QrCNAB_Cfg_SacAvaNome: TWideStringField
      FieldName = 'SacAvaNome'
      Origin = 'cnab_cfg.SacAvaNome'
      Size = 100
    end
    object QrCNAB_Cfg_TipoCart: TSmallintField
      FieldName = 'TipoCart'
      Origin = 'cnab_cfg.TipoCart'
    end
    object QrCNAB_Cfg_CartNum: TWideStringField
      FieldName = 'CartNum'
      Origin = 'cnab_cfg.CartNum'
      Size = 3
    end
    object QrCNAB_Cfg_CartCod: TWideStringField
      FieldName = 'CartCod'
      Origin = 'cnab_cfg.CartCod'
      Size = 3
    end
    object QrCNAB_Cfg_EspecieTit: TWideStringField
      FieldName = 'EspecieTit'
      Origin = 'cnab_cfg.EspecieTit'
      Size = 6
    end
    object QrCNAB_Cfg_AceiteTit: TSmallintField
      FieldName = 'AceiteTit'
      Origin = 'cnab_cfg.AceiteTit'
    end
    object QrCNAB_Cfg_InstrCobr1: TWideStringField
      FieldName = 'InstrCobr1'
      Origin = 'cnab_cfg.InstrCobr1'
      Size = 2
    end
    object QrCNAB_Cfg_InstrCobr2: TWideStringField
      FieldName = 'InstrCobr2'
      Origin = 'cnab_cfg.InstrCobr2'
      Size = 2
    end
    object QrCNAB_Cfg_CodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
      Origin = 'cnab_cfg.CodEmprBco'
    end
    object QrCNAB_Cfg_JurosTipo: TSmallintField
      FieldName = 'JurosTipo'
      Origin = 'cnab_cfg.JurosTipo'
    end
    object QrCNAB_Cfg_JurosPerc: TFloatField
      FieldName = 'JurosPerc'
      Origin = 'cnab_cfg.JurosPerc'
    end
    object QrCNAB_Cfg_JurosDias: TSmallintField
      FieldName = 'JurosDias'
      Origin = 'cnab_cfg.JurosDias'
    end
    object QrCNAB_Cfg_MultaTipo: TSmallintField
      FieldName = 'MultaTipo'
      Origin = 'cnab_cfg.MultaTipo'
    end
    object QrCNAB_Cfg_MultaPerc: TFloatField
      FieldName = 'MultaPerc'
      Origin = 'cnab_cfg.MultaPerc'
    end
    object QrCNAB_Cfg_Texto01: TWideStringField
      FieldName = 'Texto01'
      Origin = 'cnab_cfg.Texto01'
      Size = 100
    end
    object QrCNAB_Cfg_Texto02: TWideStringField
      FieldName = 'Texto02'
      Origin = 'cnab_cfg.Texto02'
      Size = 100
    end
    object QrCNAB_Cfg_Texto03: TWideStringField
      FieldName = 'Texto03'
      Origin = 'cnab_cfg.Texto03'
      Size = 100
    end
    object QrCNAB_Cfg_Texto04: TWideStringField
      FieldName = 'Texto04'
      Origin = 'cnab_cfg.Texto04'
      Size = 100
    end
    object QrCNAB_Cfg_Texto05: TWideStringField
      FieldName = 'Texto05'
      Origin = 'cnab_cfg.Texto05'
      Size = 100
    end
    object QrCNAB_Cfg_Texto06: TWideStringField
      FieldName = 'Texto06'
      Origin = 'cnab_cfg.Texto06'
      Size = 100
    end
    object QrCNAB_Cfg_Texto07: TWideStringField
      FieldName = 'Texto07'
      Origin = 'cnab_cfg.Texto07'
      Size = 100
    end
    object QrCNAB_Cfg_Texto08: TWideStringField
      FieldName = 'Texto08'
      Origin = 'cnab_cfg.Texto08'
      Size = 100
    end
    object QrCNAB_Cfg_Texto09: TWideStringField
      FieldName = 'Texto09'
      Origin = 'cnab_cfg.Texto09'
      Size = 100
    end
    object QrCNAB_Cfg_Texto10: TWideStringField
      FieldName = 'Texto10'
      Origin = 'cnab_cfg.Texto10'
      Size = 100
    end
    object QrCNAB_Cfg_Lk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cnab_cfg.Lk'
    end
    object QrCNAB_Cfg_DataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cnab_cfg.DataCad'
    end
    object QrCNAB_Cfg_DataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cnab_cfg.DataAlt'
    end
    object QrCNAB_Cfg_UserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cnab_cfg.UserCad'
    end
    object QrCNAB_Cfg_UserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cnab_cfg.UserAlt'
    end
    object QrCNAB_Cfg_AlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'cnab_cfg.AlterWeb'
    end
    object QrCNAB_Cfg_Ativo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'cnab_cfg.Ativo'
    end
    object QrCNAB_Cfg_CNAB: TIntegerField
      FieldName = 'CNAB'
      Origin = 'cnab_cfg.CNAB'
    end
    object QrCNAB_Cfg_EnvEmeio: TSmallintField
      FieldName = 'EnvEmeio'
      Origin = 'cnab_cfg.EnvEmeio'
    end
    object QrCNAB_Cfg_CedPosto: TIntegerField
      FieldName = 'CedPosto'
      Origin = 'cnab_cfg.CedPosto'
    end
    object QrCNAB_Cfg_InstrDias: TSmallintField
      FieldName = 'InstrDias'
      Origin = 'cnab_cfg.InstrDias'
    end
    object QrCNAB_Cfg_Diretorio: TWideStringField
      FieldName = 'Diretorio'
      Origin = 'cnab_cfg.Diretorio'
      Size = 255
    end
    object QrCNAB_Cfg_QuemPrint: TWideStringField
      FieldName = 'QuemPrint'
      Origin = 'cnab_cfg.QuemPrint'
      Size = 1
    end
    object QrCNAB_Cfg__237Mens1: TWideStringField
      FieldName = '_237Mens1'
      Origin = 'cnab_cfg._237Mens1'
      Size = 12
    end
    object QrCNAB_Cfg__237Mens2: TWideStringField
      FieldName = '_237Mens2'
      Origin = 'cnab_cfg._237Mens2'
      Size = 60
    end
    object QrCNAB_Cfg_CodOculto: TWideStringField
      FieldName = 'CodOculto'
      Origin = 'cnab_cfg.CodOculto'
    end
    object QrCNAB_Cfg_SeqArq: TIntegerField
      FieldName = 'SeqArq'
      Origin = 'cnab_cfg.SeqArq'
    end
    object QrCNAB_Cfg_LastNosNum: TLargeintField
      FieldName = 'LastNosNum'
      Origin = 'cnab_cfg.LastNosNum'
    end
    object QrCNAB_Cfg_LocalPag: TWideStringField
      FieldName = 'LocalPag'
      Origin = 'cnab_cfg.LocalPag'
      Size = 127
    end
    object QrCNAB_Cfg_EspecieVal: TWideStringField
      FieldName = 'EspecieVal'
      Origin = 'cnab_cfg.EspecieVal'
      Size = 5
    end
    object QrCNAB_Cfg_OperCodi: TWideStringField
      FieldName = 'OperCodi'
      Origin = 'cnab_cfg.OperCodi'
      Size = 3
    end
    object QrCNAB_Cfg_IDCobranca: TWideStringField
      FieldName = 'IDCobranca'
      Origin = 'cnab_cfg.IDCobranca'
      Size = 2
    end
    object QrCNAB_Cfg_AgContaCed: TWideStringField
      FieldName = 'AgContaCed'
      Origin = 'cnab_cfg.AgContaCed'
      Size = 40
    end
    object QrCNAB_Cfg_DirRetorno: TWideStringField
      FieldName = 'DirRetorno'
      Origin = 'cnab_cfg.DirRetorno'
      Size = 255
    end
    object QrCNAB_Cfg_CartRetorno: TIntegerField
      FieldName = 'CartRetorno'
      Origin = 'cnab_cfg.CartRetorno'
    end
    object QrCNAB_Cfg_PosicoesBB: TSmallintField
      FieldName = 'PosicoesBB'
      Origin = 'cnab_cfg.PosicoesBB'
    end
    object QrCNAB_Cfg_IndicatBB: TWideStringField
      FieldName = 'IndicatBB'
      Origin = 'cnab_cfg.IndicatBB'
      Size = 1
    end
    object QrCNAB_Cfg_TipoCobrBB: TWideStringField
      FieldName = 'TipoCobrBB'
      Origin = 'cnab_cfg.TipoCobrBB'
      Size = 5
    end
    object QrCNAB_Cfg_Comando: TIntegerField
      FieldName = 'Comando'
      Origin = 'cnab_cfg.Comando'
    end
    object QrCNAB_Cfg_DdProtesBB: TIntegerField
      FieldName = 'DdProtesBB'
      Origin = 'cnab_cfg.DdProtesBB'
    end
    object QrCNAB_Cfg_Msg40posBB: TWideStringField
      FieldName = 'Msg40posBB'
      Origin = 'cnab_cfg.Msg40posBB'
      Size = 40
    end
    object QrCNAB_Cfg_QuemDistrb: TWideStringField
      FieldName = 'QuemDistrb'
      Origin = 'cnab_cfg.QuemDistrb'
      Size = 1
    end
    object QrCNAB_Cfg_Desco1Cod: TSmallintField
      FieldName = 'Desco1Cod'
      Origin = 'cnab_cfg.Desco1Cod'
    end
    object QrCNAB_Cfg_Desco1Dds: TIntegerField
      FieldName = 'Desco1Dds'
      Origin = 'cnab_cfg.Desco1Dds'
    end
    object QrCNAB_Cfg_Desco1Fat: TFloatField
      FieldName = 'Desco1Fat'
      Origin = 'cnab_cfg.Desco1Fat'
    end
    object QrCNAB_Cfg_Desco2Cod: TSmallintField
      FieldName = 'Desco2Cod'
      Origin = 'cnab_cfg.Desco2Cod'
    end
    object QrCNAB_Cfg_Desco2Dds: TIntegerField
      FieldName = 'Desco2Dds'
      Origin = 'cnab_cfg.Desco2Dds'
    end
    object QrCNAB_Cfg_Desco2Fat: TFloatField
      FieldName = 'Desco2Fat'
      Origin = 'cnab_cfg.Desco2Fat'
    end
    object QrCNAB_Cfg_Desco3Cod: TSmallintField
      FieldName = 'Desco3Cod'
      Origin = 'cnab_cfg.Desco3Cod'
    end
    object QrCNAB_Cfg_Desco3Dds: TIntegerField
      FieldName = 'Desco3Dds'
      Origin = 'cnab_cfg.Desco3Dds'
    end
    object QrCNAB_Cfg_Desco3Fat: TFloatField
      FieldName = 'Desco3Fat'
      Origin = 'cnab_cfg.Desco3Fat'
    end
    object QrCNAB_Cfg_ProtesCod: TSmallintField
      FieldName = 'ProtesCod'
      Origin = 'cnab_cfg.ProtesCod'
    end
    object QrCNAB_Cfg_ProtesDds: TSmallintField
      FieldName = 'ProtesDds'
      Origin = 'cnab_cfg.ProtesDds'
    end
    object QrCNAB_Cfg_BxaDevCod: TSmallintField
      FieldName = 'BxaDevCod'
      Origin = 'cnab_cfg.BxaDevCod'
    end
    object QrCNAB_Cfg_BxaDevDds: TIntegerField
      FieldName = 'BxaDevDds'
      Origin = 'cnab_cfg.BxaDevDds'
    end
    object QrCNAB_Cfg_MoedaCod: TWideStringField
      FieldName = 'MoedaCod'
      Origin = 'cnab_cfg.MoedaCod'
    end
    object QrCNAB_Cfg_Contrato: TFloatField
      FieldName = 'Contrato'
      Origin = 'cnab_cfg.Contrato'
    end
    object QrCNAB_Cfg_EspecieDoc: TWideStringField
      FieldName = 'EspecieDoc'
      Origin = 'cnab_cfg.EspecieDoc'
    end
    object QrCNAB_Cfg_MultaDias: TSmallintField
      FieldName = 'MultaDias'
      Origin = 'cnab_cfg.MultaDias'
    end
    object QrCNAB_Cfg_Variacao: TIntegerField
      FieldName = 'Variacao'
      Origin = 'cnab_cfg.Variacao'
    end
    object QrCNAB_Cfg_ConvCartCobr: TWideStringField
      FieldName = 'ConvCartCobr'
      Origin = 'cnab_cfg.ConvCartCobr'
      Size = 2
    end
    object QrCNAB_Cfg_ConvVariCart: TWideStringField
      FieldName = 'ConvVariCart'
      Origin = 'cnab_cfg.ConvVariCart'
      Size = 3
    end
    object QrCNAB_Cfg_InfNossoNum: TSmallintField
      FieldName = 'InfNossoNum'
      Origin = 'cnab_cfg.InfNossoNum'
    end
    object QrCNAB_Cfg_CodLidrBco: TWideStringField
      FieldName = 'CodLidrBco'
      Origin = 'cnab_cfg.CodLidrBco'
    end
    object QrCNAB_Cfg_CartEmiss: TIntegerField
      FieldName = 'CartEmiss'
      Origin = 'cnab_cfg.CartEmiss'
    end
    object QrCNAB_Cfg_TipoCobranca: TIntegerField
      FieldName = 'TipoCobranca'
      Origin = 'cnab_cfg.TipoCobranca'
    end
    object QrCNAB_Cfg_EspecieTxt: TWideStringField
      FieldName = 'EspecieTxt'
      Origin = 'cnab_cfg.EspecieTxt'
    end
    object QrCNAB_Cfg_CartTxt: TWideStringField
      FieldName = 'CartTxt'
      Origin = 'cnab_cfg.CartTxt'
      Size = 10
    end
    object QrCNAB_Cfg_LayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Size = 50
    end
    object QrCNAB_Cfg_LayoutRet: TWideStringField
      FieldName = 'LayoutRet'
      Size = 50
    end
    object QrCNAB_Cfg_NaoRecebDd: TSmallintField
      FieldName = 'NaoRecebDd'
    end
    object QrCNAB_Cfg_TipBloqUso: TWideStringField
      FieldName = 'TipBloqUso'
      Size = 1
    end
    object QrCNAB_Cfg_ConcatCod: TWideStringField
      FieldName = 'ConcatCod'
      Size = 50
    end
    object QrCNAB_Cfg_ComplmCod: TWideStringField
      FieldName = 'ComplmCod'
      Size = 10
    end
    object QrCNAB_Cfg_NumVersaoI3: TIntegerField
      FieldName = 'NumVersaoI3'
    end
    object QrCNAB_Cfg_ModalCobr: TSmallintField
      FieldName = 'ModalCobr'
    end
    object QrCNAB_Cfg_CtaCooper: TWideStringField
      FieldName = 'CtaCooper'
      Size = 7
    end
    object QrCNAB_Cfg_NosNumFxaI: TIntegerField
      FieldName = 'NosNumFxaI'
    end
    object QrCNAB_Cfg_NosNumFxaF: TIntegerField
      FieldName = 'NosNumFxaF'
    end
    object QrCNAB_Cfg_NosNumFxaU: TSmallintField
      FieldName = 'NosNumFxaU'
    end
  end
  object QrCount: TMySQLQuery
    Database = DModG.RV_CEP_DB
    SQL.Strings = (
      'SELECT COUNT(*) ITENS'
      'FROM lct0001a'
      'WHERE Depto=0'
      '')
    Left = 44
    Top = 344
    object QrCountITENS: TLargeintField
      FieldName = 'ITENS'
      Required = True
    end
  end
end
