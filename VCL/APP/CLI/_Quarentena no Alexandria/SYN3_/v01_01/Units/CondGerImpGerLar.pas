unit CondGerImpGerLar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit, dmkGeral, dmkImage,
  UnDmkEnums;

type
  TFmCondGerImpGerLar = class(TForm)
    Panel1: TPanel;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    StaticText4: TStaticText;
    STTota: TStaticText;
    STPais: TStaticText;
    STRetr: TStaticText;
    STNome: TStaticText;
    Panel3: TPanel;
    EdLargura: TdmkEdit;
    Label1: TLabel;
    StaticText5: TStaticText;
    STLarg: TStaticText;
    STCodi: TStaticText;
    Bt0680: TBitBtn;
    Bt1009: TBitBtn;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel5: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure EdLarguraChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure Bt0680Click(Sender: TObject);
    procedure Bt1009Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure RecalculaDados();
  end;

  var
  FmCondGerImpGerLar: TFmCondGerImpGerLar;

implementation

uses CondGerImpGer, UnMyObjects;

{$R *.DFM}

procedure TFmCondGerImpGerLar.Bt0680Click(Sender: TObject);
begin
  EdLargura.Text := STRetr.Caption;
end;

procedure TFmCondGerImpGerLar.Bt1009Click(Sender: TObject);
begin
  EdLargura.Text := STPais.Caption;
end;

procedure TFmCondGerImpGerLar.BtOKClick(Sender: TObject);
begin
  FmCondGerImpGer.GradeA.ColWidths[Geral.IMV(STCodi.Caption)] :=
    EdLargura.ValueVariant;
  FmCondGerImpGer.CalculaLarguraFolha();
  Close;
end;

procedure TFmCondGerImpGerLar.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerImpGerLar.EdLarguraChange(Sender: TObject);
begin
  RecalculaDados();
end;

procedure TFmCondGerImpGerLar.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  RecalculaDados;
end;

procedure TFmCondGerImpGerLar.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
end;

procedure TFmCondGerImpGerLar.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmCondGerImpGerLar.RecalculaDados;
var
  NewLarg, OldLarg, TotLarA, TotLarN, DifLarP, DifLarL: Integer;
begin
  NewLarg := EdLargura.ValueVariant;
  OldLarg := Geral.IMV(STLarg.Caption);
  TotLarA := FmCondGerImpGer.EdLarguraFolha.ValueVariant;
  TotLarN := TotLarA + NewLarg - OldLarg;
  DifLarP :=  680 - TotLarN;
  DifLarL := 1009 - TotLarN;
  //
  STTota.Caption := IntToStr(TotLarN);
  STRetr.Caption := IntToStr(DifLarP);
  STPais.Caption := IntToStr(DifLarL);
  //
  if DifLarP < 0 then
    STRetr.Font.Color := clGreen
  else
  if DifLarP = 0 then
    STRetr.Font.Color := clBlue
  else
    STRetr.Font.Color := clRed;
  //
  if DifLarL < 0 then
    STPais.Font.Color := clGreen
  else
  if DifLarL = 0 then
    STPais.Font.Color := clBlue
  else
    STPais.Font.Color := clRed;
  //
  Bt0680.Enabled := DifLarP >= 0;
  Bt1009.Enabled := DifLarL >= 0;
end;

end.
