object FmUploads: TFmUploads
  Left = 321
  Top = 159
  Caption = 'WEB-UPLOA-001 :: Uploads de arquivos'
  ClientHeight = 594
  ClientWidth = 814
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 109
    Width = 814
    Height = 485
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Visible = False
    object Label9: TLabel
      Left = 16
      Top = 8
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
    end
    object Label10: TLabel
      Left = 16
      Top = 52
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
    end
    object LaCond: TLabel
      Left = 16
      Top = 94
      Width = 60
      Height = 13
      Caption = 'Condom'#237'nio:'
    end
    object Label1: TLabel
      Left = 16
      Top = 138
      Width = 70
      Height = 13
      Caption = 'Diret'#243'rio WEB:'
    end
    object Label3: TLabel
      Left = 15
      Top = 182
      Width = 261
      Height = 13
      Caption = 'Caminho do arquivo a ser enviado para o servidor [F4]: '
    end
    object Label4: TLabel
      Left = 14
      Top = 225
      Width = 90
      Height = 13
      Caption = 'Data da expira'#231#227'o:'
    end
    object EdCodigo: TdmkEdit
      Left = 16
      Top = 24
      Width = 100
      Height = 21
      Alignment = taRightJustify
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8281908
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdCond: TdmkEditCB
      Left = 16
      Top = 110
      Width = 57
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBCond
      IgnoraDBLookupComboBox = False
    end
    object CBCond: TdmkDBLookupComboBox
      Left = 75
      Top = 110
      Width = 325
      Height = 21
      KeyField = 'Codigo'
      ListField = 'NCONDOM'
      ListSource = DsNomeCond
      TabOrder = 3
      dmkEditCB = EdCond
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdDirWeb: TdmkEditCB
      Left = 16
      Top = 154
      Width = 57
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBDirWeb
      IgnoraDBLookupComboBox = False
    end
    object CBDirWeb: TdmkDBLookupComboBox
      Left = 76
      Top = 154
      Width = 324
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsNomeDir
      TabOrder = 5
      dmkEditCB = EdDirWeb
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdNome: TdmkEdit
      Left = 15
      Top = 69
      Width = 385
      Height = 21
      MaxLength = 30
      TabOrder = 1
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdArquivo: TdmkEdit
      Left = 15
      Top = 198
      Width = 385
      Height = 21
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      OnKeyDown = EdArquivoKeyDown
    end
    object GroupBox1: TGroupBox
      Left = 0
      Top = 415
      Width = 814
      Height = 70
      Align = alBottom
      TabOrder = 9
      object Panel2: TPanel
        Left = 2
        Top = 15
        Width = 810
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Panel3: TPanel
          Left = 666
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtDesiste: TBitBtn
            Tag = 15
            Left = 15
            Top = 3
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
            Caption = '&Desiste'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            NumGlyphs = 2
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtDesisteClick
          end
        end
        object BtConfirma: TBitBtn
          Tag = 14
          Left = 8
          Top = 3
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
          Caption = '&Confirma'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtConfirmaClick
        end
      end
    end
    object TPDataExp: TdmkEditDateTimePicker
      Left = 14
      Top = 241
      Width = 112
      Height = 21
      Date = 40375.706008634260000000
      Time = 40375.706008634260000000
      TabOrder = 7
      ReadOnly = False
      DefaultEditMask = '!99/99/99;1;_'
      AutoApplyEditMask = True
      UpdType = utYes
    end
    object CkExpirados: TCheckBox
      Left = 134
      Top = 243
      Width = 266
      Height = 17
      Caption = 'Excluir todos os arquivos expirados deste diret'#243'rio'
      TabOrder = 8
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 109
    Width = 814
    Height = 485
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 814
      Height = 48
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 0
      object Label2: TLabel
        Left = 6
        Top = 1
        Width = 60
        Height = 13
        Caption = 'Condom'#237'nio:'
      end
      object EdCondPesq: TdmkEditCB
        Left = 6
        Top = 16
        Width = 57
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBCondPesq
        IgnoraDBLookupComboBox = False
      end
      object CBCondPesq: TdmkDBLookupComboBox
        Left = 66
        Top = 16
        Width = 605
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NCONDOM'
        ListSource = DsNomeCond
        TabOrder = 1
        dmkEditCB = EdCondPesq
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object BitBtn1: TBitBtn
        Tag = 18
        Left = 679
        Top = 4
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Reabre'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BitBtn1Click
      end
    end
    object GBRodaPe: TGroupBox
      Left = 0
      Top = 415
      Width = 814
      Height = 70
      Align = alBottom
      TabOrder = 1
      object Panel5: TPanel
        Left = 2
        Top = 15
        Width = 810
        Height = 53
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnSaiDesis: TPanel
          Left = 666
          Top = 0
          Width = 144
          Height = 53
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 11
            Top = 4
            Width = 120
            Height = 40
            Cursor = crHandPoint
            Hint = 'Sai da janela atual'
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 5
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Inclui novo banco'
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtIncluiClick
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 129
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Altera banco atual'
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtAlteraClick
        end
        object BtExclui: TBitBtn
          Tag = 12
          Left = 253
          Top = 4
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Hint = 'Exclui banco atual'
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtExcluiClick
        end
      end
    end
    object PnDados: TPanel
      Left = 0
      Top = 48
      Width = 814
      Height = 300
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 2
      object Splitter1: TSplitter
        Left = 0
        Top = 150
        Width = 814
        Height = 10
        Cursor = crVSplit
        Align = alTop
        Beveled = True
        MinSize = 90
        ResizeStyle = rsUpdate
        ExplicitTop = 100
      end
      object Splitter2: TSplitter
        Left = 280
        Top = 160
        Width = 10
        Height = 140
        Beveled = True
        ExplicitTop = 110
        ExplicitHeight = 190
      end
      object Panel6: TPanel
        Left = 290
        Top = 160
        Width = 524
        Height = 140
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object DBGArquivos: TdmkDBGrid
          Left = 0
          Top = 27
          Width = 524
          Height = 113
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Arquivo'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataExp_TXT'
              Title.Caption = 'Data da expira'#231#227'o'
              Width = 100
              Visible = True
            end>
          Color = clWindow
          DataSource = DsUploads
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Arquivo'
              Width = 150
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DataExp_TXT'
              Title.Caption = 'Data da expira'#231#227'o'
              Width = 100
              Visible = True
            end>
        end
        object StaticText1: TStaticText
          Left = 0
          Top = 0
          Width = 524
          Height = 27
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'Arquivos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -21
          Font.Name = 'Times New Roman'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel7: TPanel
        Left = 0
        Top = 160
        Width = 280
        Height = 140
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object DBGDiretorios: TdmkDBGrid
          Left = 0
          Top = 27
          Width = 280
          Height = 113
          Align = alLeft
          Columns = <
            item
              Expanded = False
              FieldName = 'DirWeb'
              Title.Caption = 'ID Diret'#243'rio'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DIRNOME'
              Title.Caption = 'Diret'#243'rio'
              Width = 150
              Visible = True
            end>
          Color = clWindow
          DataSource = DsUploadsDir
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'DirWeb'
              Title.Caption = 'ID Diret'#243'rio'
              Width = 75
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'DIRNOME'
              Title.Caption = 'Diret'#243'rio'
              Width = 150
              Visible = True
            end>
        end
        object StaticText2: TStaticText
          Left = 0
          Top = 0
          Width = 280
          Height = 27
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'Diret'#243'rios'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -21
          Font.Name = 'Times New Roman'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
      object Panel8: TPanel
        Left = 0
        Top = 0
        Width = 814
        Height = 150
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 2
        object DBGCondominios: TdmkDBGrid
          Left = 0
          Top = 27
          Width = 814
          Height = 123
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Cond'
              Title.Caption = 'ID Condom'#237'nio'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NCONDOM'
              Title.Caption = 'Condom'#237'nio'
              Width = 450
              Visible = True
            end>
          Color = clWindow
          DataSource = DsUploadsCon
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -11
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Cond'
              Title.Caption = 'ID Condom'#237'nio'
              Width = 100
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NCONDOM'
              Title.Caption = 'Condom'#237'nio'
              Width = 450
              Visible = True
            end>
        end
        object StaticText3: TStaticText
          Left = 0
          Top = 0
          Width = 814
          Height = 27
          Align = alTop
          Alignment = taCenter
          BorderStyle = sbsSunken
          Caption = 'Condom'#237'nios'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -21
          Font.Name = 'Times New Roman'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 814
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 726
      Top = 0
      Width = 88
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 48
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
      object ImgWEB: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 678
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 247
        Height = 32
        Caption = 'Uploads de arquivos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 247
        Height = 32
        Caption = 'Uploads de arquivos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 247
        Height = 32
        Caption = 'Uploads de arquivos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 65
    Width = 814
    Height = 44
    Align = alTop
    Caption = ' Avisos: '
    TabOrder = 3
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 810
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object PB1: TProgressBar
    Left = 0
    Top = 48
    Width = 814
    Height = 17
    Align = alTop
    TabOrder = 4
    Visible = False
  end
  object QrNomeCond: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      
        'SELECT con.Codigo, IF (rec.Tipo=0, rec.RazaoSocial, rec.Nome) NC' +
        'ONDOM'
      'FROM cond con'
      'LEFT JOIN entidades rec ON rec.Codigo=con.Cliente'
      'WHERE con.Codigo > 0'
      'AND rec.Cliente1 = '#39'V'#39
      'ORDER BY NCONDOM')
    Left = 545
    Top = 13
    object QrNomeCondCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrNomeCondNCONDOM: TWideStringField
      FieldName = 'NCONDOM'
      Size = 100
    end
  end
  object DsNomeCond: TDataSource
    DataSet = QrNomeCond
    Left = 574
    Top = 13
  end
  object QrNomeDir: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Codigo, Nome, Pasta'
      'FROM dirweb'
      'WHERE Codigo > 0'
      'ORDER BY Nome')
    Left = 601
    Top = 13
    object QrNomeDirCodigo: TAutoIncField
      FieldName = 'Codigo'
    end
    object QrNomeDirNome: TWideStringField
      FieldName = 'Nome'
      Size = 32
    end
    object QrNomeDirPasta: TWideStringField
      FieldName = 'Pasta'
      Size = 32
    end
  end
  object DsNomeDir: TDataSource
    DataSet = QrNomeDir
    Left = 630
    Top = 13
  end
  object OpenDialog1: TOpenDialog
    Filter = 'FDFs (*.pdf)|*.pdf'
    Left = 156
    Top = 29
  end
  object QrUploadsCon: TmySQLQuery
    Database = Dmod.MyDBn
    BeforeClose = QrUploadsConBeforeClose
    AfterScroll = QrUploadsConAfterScroll
    SQL.Strings = (
      'SELECT upl.Cond, '
      'IF (rec.Tipo=0, rec.RazaoSocial, rec.Nome) NCONDOM'
      'FROM uploads upl'
      'LEFT JOIN cond con ON con.Codigo=upl.Cond'
      'LEFT JOIN entidades rec ON rec.Codigo=con.Cliente'
      'GROUP BY upl.Cond'
      'ORDER BY NCONDOM')
    Left = 376
    Top = 277
    object QrUploadsConCond: TIntegerField
      FieldName = 'Cond'
      Origin = 'uploads.Cond'
    end
    object QrUploadsConNCONDOM: TWideStringField
      FieldName = 'NCONDOM'
      Origin = 'NCONDOM'
      Size = 100
    end
  end
  object DsUploadsCon: TDataSource
    DataSet = QrUploadsCon
    Left = 404
    Top = 277
  end
  object QrUploadsDir: TmySQLQuery
    Database = Dmod.MyDBn
    BeforeClose = QrUploadsDirBeforeClose
    AfterScroll = QrUploadsDirAfterScroll
    SQL.Strings = (
      'SELECT upl.DirWeb, dir.Nome DIRNOME, upl.Cond'
      'FROM uploads upl'
      'LEFT JOIN dirweb dir ON dir.Codigo=upl.dirweb'
      'WHERE upl.Cond=:P0'
      'GROUP BY DIRNOME'
      'ORDER BY DIRNOME')
    Left = 432
    Top = 277
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUploadsDirDirWeb: TIntegerField
      FieldName = 'DirWeb'
      Origin = 'uploads.DirWeb'
      Required = True
    end
    object QrUploadsDirDIRNOME: TWideStringField
      FieldName = 'DIRNOME'
      Origin = 'dirweb.Nome'
      Size = 32
    end
    object QrUploadsDirCond: TIntegerField
      FieldName = 'Cond'
    end
  end
  object DsUploadsDir: TDataSource
    DataSet = QrUploadsDir
    Left = 460
    Top = 277
  end
  object QrUploads: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT upl.*, dir.Pasta NDIRWEB, dir.Nome DIRNOME,'
      'IF (rec.Tipo=0, rec.RazaoSocial, rec.Nome) NCONDOM,'
      
        'IF (upl.DataExp = 0, "", DATE_FORMAT(upl.DataExp, "%d/%m/%Y")) D' +
        'ataExp_TXT '
      'FROM uploads upl'
      'LEFT JOIN dirweb dir ON dir.Codigo=upl.dirweb'
      'LEFT JOIN cond con ON con.Codigo=upl.Cond'
      'LEFT JOIN entidades rec ON rec.Codigo=con.Cliente'
      'WHERE upl.DirWeb=:P0'
      'ORDER BY NCONDOM, upl.DirWeb, upl.Nome')
    Left = 488
    Top = 277
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrUploadsCodigo: TAutoIncField
      FieldName = 'Codigo'
      Origin = 'uploads.Codigo'
    end
    object QrUploadsNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'uploads.Nome'
      Size = 32
    end
    object QrUploadsArquivo: TWideStringField
      FieldName = 'Arquivo'
      Origin = 'uploads.Arquivo'
      Size = 32
    end
    object QrUploadsCond: TIntegerField
      FieldName = 'Cond'
      Origin = 'uploads.Cond'
    end
    object QrUploadsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'uploads.Lk'
    end
    object QrUploadsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'uploads.DataCad'
    end
    object QrUploadsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'uploads.DataAlt'
    end
    object QrUploadsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'uploads.UserCad'
    end
    object QrUploadsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'uploads.UserAlt'
    end
    object QrUploadsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'uploads.AlterWeb'
    end
    object QrUploadsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'uploads.Ativo'
      Required = True
    end
    object QrUploadsDirWeb: TIntegerField
      FieldName = 'DirWeb'
      Origin = 'uploads.DirWeb'
      Required = True
    end
    object QrUploadsNDIRWEB: TWideStringField
      FieldName = 'NDIRWEB'
      Origin = 'dirweb.Pasta'
      Size = 32
    end
    object QrUploadsDIRNOME: TWideStringField
      FieldName = 'DIRNOME'
      Origin = 'dirweb.Nome'
      Size = 32
    end
    object QrUploadsNCONDOM: TWideStringField
      FieldName = 'NCONDOM'
      Origin = 'NCONDOM'
      Size = 100
    end
    object QrUploadsDataExp: TDateField
      FieldName = 'DataExp'
    end
    object QrUploadsDataExp_TXT: TWideStringField
      FieldName = 'DataExp_TXT'
      Size = 10
    end
  end
  object DsUploads: TDataSource
    DataSet = QrUploads
    Left = 516
    Top = 277
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDBn
    Left = 385
    Top = 37
  end
  object PMExclui: TPopupMenu
    OnPopup = PMExcluiPopup
    Left = 296
    Top = 512
    object Excluiselecionados1: TMenuItem
      Caption = '&Exclui selecionado(s)'
      OnClick = Excluiselecionados1Click
    end
    object ExcluiExpirados1: TMenuItem
      Caption = 'Exclui arquivos e&xpirados'
      OnClick = ExcluiExpirados1Click
    end
  end
  object QrFTPConfig: TmySQLQuery
    Database = Dmod.MyDBn
    BeforeClose = QrUploadsConBeforeClose
    AfterScroll = QrUploadsConAfterScroll
    Left = 552
    Top = 157
  end
end
