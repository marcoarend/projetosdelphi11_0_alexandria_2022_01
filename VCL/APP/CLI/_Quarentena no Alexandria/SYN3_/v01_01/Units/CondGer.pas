unit CondGer;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Db, mySQLDbTables, ComCtrls, Grids,
  DBGrids, Menus, DBCtrls, frxClass, frxDBSet, frxGradient, frxDesgn,
  frxBarcode, Mask, frxChBox, frxRich, OleCtrls, SHDocVw, dmkDBGrid, dmkEdit,
  UnFinanceiro, frxExportPDF, Variants, dmkGeral, dmkEditDateTimePicker,
  dmkDBLookupComboBox, dmkEditCB, UnDmkProcFunc, dmkImage, UnDmkEnums,
  frxExportBaseDialog;

type
  TFmCondGer = class(TForm)
    Panel1: TPanel;
    Panel3: TPanel;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel4: TPanel;
    TabSheet3: TTabSheet;
    PnLcto: TPanel;
    Shape1: TShape;
    Label14: TLabel;
    PainelDados2: TPanel;
    Panel7: TPanel;
    Label6: TLabel;
    LaSaldo: TLabel;
    LaDiferenca: TLabel;
    LaCaixa: TLabel;
    EdCodigo: TDBEdit;
    EdNome: TDBEdit;
    EdSaldo: TDBEdit;
    EdDiferenca: TDBEdit;
    EdCaixa: TDBEdit;
    BtPagtoDuvida: TBitBtn;
    EdSdoAqui: TdmkEdit;
    BtRefresh: TBitBtn;
    DBGrid2: TDBGrid;
    Panel8: TPanel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label12: TLabel;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    PMSaldoAqui: TPopupMenu;
    Calcula1: TMenuItem;
    Limpa1: TMenuItem;
    Diferena1: TMenuItem;
    TabSheet4: TTabSheet;
    PageControl2: TPageControl;
    TabSheet5: TTabSheet;
    PainelControle: TPanel;
    LaRegistro: TLabel;
    Panel10: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel11: TPanel;
    BitBtn1: TBitBtn;
    BtBoleto: TBitBtn;
    BtOrcamento: TBitBtn;
    QrPrevBaI: TmySQLQuery;
    DsPrevBaI: TDataSource;
    QrPrevBaICodigo: TIntegerField;
    QrPrevBaIControle: TIntegerField;
    QrPrevBaICond: TIntegerField;
    QrPrevBaIValor: TFloatField;
    QrPrevBaITexto: TWideStringField;
    QrPrevBaISitCobr: TIntegerField;
    QrPrevBaIParcelas: TIntegerField;
    QrPrevBaIParcPerI: TIntegerField;
    QrPrevBaIParcPerF: TIntegerField;
    QrPrevBaILk: TIntegerField;
    QrPrevBaIDataCad: TDateField;
    QrPrevBaIDataAlt: TDateField;
    QrPrevBaIUserCad: TIntegerField;
    QrPrevBaIUserAlt: TIntegerField;
    QrPrevBaINOMESITCOBR: TWideStringField;
    QrPrevBaIINICIO: TWideStringField;
    QrPrevBaIFINAL: TWideStringField;
    QrPrevBaINome: TWideStringField;
    QrPrev: TmySQLQuery;
    QrPrevCodigo: TIntegerField;
    QrPrevPeriodo: TIntegerField;
    QrPrevGastos: TFloatField;
    QrPrevLk: TIntegerField;
    QrPrevDataCad: TDateField;
    QrPrevDataAlt: TDateField;
    QrPrevUserCad: TIntegerField;
    QrPrevUserAlt: TIntegerField;
    QrPrevPERIODO_TXT: TWideStringField;
    DsPrev: TDataSource;
    BitBtn5: TBitBtn;
    DBGPrevIts: TDBGrid;
    PMOrcamento: TPopupMenu;
    Incluinovooramento1: TMenuItem;
    BtProvisao: TBitBtn;
    PMProvisao: TPopupMenu;
    Adicinaitensbase1: TMenuItem;
    Novoitemdeoramento1: TMenuItem;
    Alteraitemselecionado1: TMenuItem;
    Excluiitemdeoramentoselecionado1: TMenuItem;
    N1: TMenuItem;
    QrPRI: TmySQLQuery;
    DsPRI: TDataSource;
    PMBase: TPopupMenu;
    Incluiitens1: TMenuItem;
    Alteraitemselecionado2: TMenuItem;
    N2: TMenuItem;
    QrPRIConta: TIntegerField;
    QrPRITexto: TWideStringField;
    QrPRIValor: TFloatField;
    QrPRISubGrupo: TIntegerField;
    QrPRINOMECONTA: TWideStringField;
    QrPRINOMESUBGRUPO: TWideStringField;
    QrPRICodigo: TIntegerField;
    QrPRIControle: TIntegerField;
    QrPRILk: TIntegerField;
    QrPRIDataCad: TDateField;
    QrPRIDataAlt: TDateField;
    QrPRIUserCad: TIntegerField;
    QrPRIUserAlt: TIntegerField;
    QrPRIPrevBaI: TIntegerField;
    QrSoma: TmySQLQuery;
    QrSomaTotal: TFloatField;
    frxGradientObject1: TfrxGradientObject;
    ProgressBar2: TProgressBar;
    TabSheet7: TTabSheet;
    BtArrecada: TBitBtn;
    PMBoleto: TPopupMenu;
    TabSheet8: TTabSheet;
    Panel12: TPanel;
    BtBase: TBitBtn;
    BitBtn3: TBitBtn;
    BtLeitura: TBitBtn;
    PMLeitura: TPopupMenu;
    Alteraleituraatual1: TMenuItem;
    Excluileituraatual1: TMenuItem;
    Incluileituras1: TMenuItem;
    PMArrecada: TPopupMenu;
    Incluiintensbasedearrecadao1: TMenuItem;
    N4: TMenuItem;
    Novoitemdearrecadao1: TMenuItem;
    TabSheet6: TTabSheet;
    QrArreBaI: TmySQLQuery;
    DsArreBaI: TDataSource;
    QrArreBaINome: TWideStringField;
    QrArreBaICodigo: TIntegerField;
    QrArreBaIControle: TIntegerField;
    QrArreBaICond: TIntegerField;
    QrArreBaIValor: TFloatField;
    QrArreBaITexto: TWideStringField;
    QrArreBaISitCobr: TIntegerField;
    QrArreBaIParcelas: TIntegerField;
    QrArreBaIParcPerI: TIntegerField;
    QrArreBaIParcPerF: TIntegerField;
    QrArreBaIInfoParc: TSmallintField;
    QrArreBaILk: TIntegerField;
    QrArreBaIDataCad: TDateField;
    QrArreBaIDataAlt: TDateField;
    QrArreBaIUserCad: TIntegerField;
    QrArreBaIUserAlt: TIntegerField;
    QrArreBaIFator: TSmallintField;
    QrArreBaIPercent: TFloatField;
    QrArreBaIDeQuem: TSmallintField;
    QrArreBaIFINAL: TWideStringField;
    QrArreBaIINICIO: TWideStringField;
    QrArreBaINOMESITCOBR: TWideStringField;
    QrAri: TmySQLQuery;
    DsAri: TDataSource;
    DBGrid6: TDBGrid;
    QrArre: TmySQLQuery;
    DsArre: TDataSource;
    QrArreApto: TIntegerField;
    QrArreValor: TFloatField;
    QrArreUnidade: TWideStringField;
    QrAriCodigo: TIntegerField;
    QrAriControle: TIntegerField;
    QrAriConta: TIntegerField;
    QrAriValor: TFloatField;
    QrAriArreBaI: TIntegerField;
    QrAriTexto: TWideStringField;
    QrAriLk: TIntegerField;
    QrAriDataCad: TDateField;
    QrAriDataAlt: TDateField;
    QrAriUserCad: TIntegerField;
    QrAriUserAlt: TIntegerField;
    QrAriApto: TIntegerField;
    PMMenu: TPopupMenu;
    Transferir1: TMenuItem;
    Quitar1: TMenuItem;
    Compensar1: TMenuItem;
    Pagar1: TMenuItem;
    Reverter1: TMenuItem;
    Lanamento1: TMenuItem;
    Localizar2: TMenuItem;
    Copiar1: TMenuItem;
    Recibo1: TMenuItem;
    Mudacarteiradelanamentosselecionados1: TMenuItem;
    Gerarabertos1: TMenuItem;
    DBGrid8: TDBGrid;
    QrArreBol: TmySQLQuery;
    DsArreBol: TDataSource;
    Panel13: TPanel;
    DBGrid7: TDBGrid;
    N3: TMenuItem;
    TabSheet9: TTabSheet;
    QrZeroA: TmySQLQuery;
    QrZeroAApto: TIntegerField;
    QrZeroB: TmySQLQuery;
    QrZeroBControle: TIntegerField;
    QrZeroC: TmySQLQuery;
    QrZeroCControle: TIntegerField;
    QrSelBol: TmySQLQuery;
    PageControl3: TPageControl;
    TabSheet10: TTabSheet;
    Panel9: TPanel;
    CkAplicaveis: TCheckBox;
    DBGrid3: TDBGrid;
    TabSheet11: TTabSheet;
    N5: TMenuItem;
    Alteraarrecadao1: TMenuItem;
    Itemselecionado1: TMenuItem;
    Exclusodearrecadao1: TMenuItem;
    TODOSitensdearrecadao1: TMenuItem;
    QrMPA: TmySQLQuery;
    QrMPACodigo: TIntegerField;
    QrMPAPercent: TFloatField;
    QrImpBol: TmySQLQuery;
    QrImpBolApto: TIntegerField;
    QrImpBolUnidade: TWideStringField;
    frxDsImpBol: TfrxDBDataset;
    QrBolL: TmySQLQuery;
    QrBolLCasas: TSmallintField;
    QrBolLUnidLei: TWideStringField;
    QrBolLUnidImp: TWideStringField;
    QrBolLUnidFat: TFloatField;
    QrBolLPreco: TFloatField;
    QrBolLProduto: TIntegerField;
    QrBolLApto: TIntegerField;
    QrBolLValor: TFloatField;
    QrBolLMedAnt: TFloatField;
    QrBolLMedAtu: TFloatField;
    QrBolLControle: TIntegerField;
    QrCarts: TmySQLQuery;
    QrCartsCarteira: TIntegerField;
    QrCartsNOMECART: TWideStringField;
    QrCartsInicial: TFloatField;
    QrCartsIni: TmySQLQuery;
    QrCartsFim: TmySQLQuery;
    QrCartsTrf: TmySQLQuery;
    QrCartsTrfValor: TFloatField;
    QrCartsTrfCarteira: TIntegerField;
    TabSheet12: TTabSheet;
    DsCarts: TDataSource;
    QrCartsANT_CRED: TFloatField;
    QrCartsANT_DEB: TFloatField;
    QrCartsATU_CRED: TFloatField;
    QrCartsATU_DEB: TFloatField;
    QrCartsTRANSF: TFloatField;
    QrCartsIniCredito: TFloatField;
    QrCartsIniDebito: TFloatField;
    QrCartsIniCarteira: TIntegerField;
    QrCartsFimCredito: TFloatField;
    QrCartsFimDebito: TFloatField;
    QrCartsFimCarteira: TIntegerField;
    QrCartsANTERIOR: TFloatField;
    QrCartsRECEITAS: TFloatField;
    QrCartsDESPESAS: TFloatField;
    QrCartsSALDOMES: TFloatField;
    QrCartsFINAL: TFloatField;
    frxDsCarts: TfrxDBDataset;
    frxDsPRI: TfrxDBDataset;
    Alteravencimento1: TMenuItem;
    DoBloquetoatual1: TMenuItem;
    BloquetosSelecionados1: TMenuItem;
    DeTodosBloquetos1: TMenuItem;
    QrPrevCond: TIntegerField;
    QrPrevCondCli: TIntegerField;
    QrPesqMB: TmySQLQuery;
    QrPesqBCB: TmySQLQuery;
    frxBarCodeObject1: TfrxBarCodeObject;
    QrBolacarA: TmySQLQuery;
    QrBolacarAControle: TIntegerField;
    QrBolacarAGenero: TIntegerField;
    QrBolacarAValor: TFloatField;
    QrBolacarATexto: TWideStringField;
    QrBolacarAApto: TIntegerField;
    QrBolacarAUnidade: TWideStringField;
    QrBolacarAVencto: TDateField;
    QrBolacarB: TmySQLQuery;
    QrBolacarBValor: TFloatField;
    QrBolacarBGenero: TIntegerField;
    QrBolacarBNOMECONS: TWideStringField;
    QrBolacarBVencto: TDateField;
    QrBolacarBMedAnt: TFloatField;
    QrBolacarBMedAtu: TFloatField;
    QrBolacarBConsumo: TFloatField;
    QrBolacarBCasas: TSmallintField;
    QrBolacarBUnidLei: TWideStringField;
    QrBolacarBUnidFat: TFloatField;
    QrBolacarBUnidImp: TWideStringField;
    Timer2: TTimer;
    QrArrePropriet: TIntegerField;
    QrArreNOMEPROPRIET: TWideStringField;
    QrAriArreBaC: TIntegerField;
    QrAriVencto: TDateField;
    QrAriPropriet: TIntegerField;
    QrZeroAPropriet: TIntegerField;
    QrImpBolPropriet: TIntegerField;
    QrImpBolNOMEPROPRIET: TWideStringField;
    QrBolLPropriet: TIntegerField;
    QrBolacarAPropriet: TIntegerField;
    QrBolacarBPropriet: TIntegerField;
    TabSheet13: TTabSheet;
    DBGrid11: TDBGrid;
    BtPrebol: TBitBtn;
    PMPrebol0: TPopupMenu;
    Imprimir2: TMenuItem;
    Atual2: TMenuItem;
    Selecionados2: TMenuItem;
    Todos2: TMenuItem;
    AlteraVencimento2: TMenuItem;
    DobloquetoAtual2: TMenuItem;
    DosbloquetosSelecionados1: TMenuItem;
    DeTodosbloquetos2: TMenuItem;
    Panel14: TPanel;
    DBText3: TDBText;
    Label16: TLabel;
    DBText4: TDBText;
    Label17: TLabel;
    Panel15: TPanel;
    DBGCons: TDBGrid;
    Panel16: TPanel;
    DBEdit13: TDBEdit;
    DBEdit14: TDBEdit;
    Label21: TLabel;
    DBEdit15: TDBEdit;
    DBText5: TDBText;
    DBText6: TDBText;
    QrSumCT: TmySQLQuery;
    QrSumCTCONSUMO: TFloatField;
    QrSumCTVALOR: TFloatField;
    DsSumCT: TDataSource;
    QrSumARRE: TmySQLQuery;
    DsSumARRE: TDataSource;
    QrSumARREValor: TFloatField;
    DBText7: TDBText;
    Label18: TLabel;
    QrSumPre: TmySQLQuery;
    QrSumPreVALOR: TFloatField;
    QrPrevTOT_BOL: TFloatField;
    QrPrevTOT_PRE: TFloatField;
    Label19: TLabel;
    DBText8: TDBText;
    Label22: TLabel;
    DBText9: TDBText;
    Bloqueto1: TMenuItem;
    Gerartodosabertos1: TMenuItem;
    GerarAtual1: TMenuItem;
    QrLocBol: TmySQLQuery;
    QrLocBolITENS: TLargeintField;
    QrLocBolVALOR: TFloatField;
    QrLocBolApto: TIntegerField;
    QrLocBolVencto: TDateField;
    RetornoCNAB1: TMenuItem;
    QrBolacarBApto: TIntegerField;
    QrBolacarBControle: TIntegerField;
    PMImprime: TPopupMenu;
    frxDsPrev: TfrxDBDataset;
    Desfazerboletos1: TMenuItem;
    Atual3: TMenuItem;
    Selecionados3: TMenuItem;
    Todos3: TMenuItem;
    Panel17: TPanel;
    DBGradeS: TdmkDBGrid;
    Carteiras1: TMenuItem;
    Data1: TMenuItem;
    Compensao1: TMenuItem;
    SpeedButton6: TSpeedButton;
    Alteraperiodoatual1: TMenuItem;
    N9: TMenuItem;
    QrPrevAviso01: TWideStringField;
    QrPrevAviso02: TWideStringField;
    QrPrevAviso03: TWideStringField;
    QrPrevAviso04: TWideStringField;
    QrPrevAviso05: TWideStringField;
    QrPrevAviso06: TWideStringField;
    QrPrevAviso07: TWideStringField;
    QrPrevAviso08: TWideStringField;
    QrPrevAviso09: TWideStringField;
    QrPrevAviso10: TWideStringField;
    TabSheet14: TTabSheet;
    Panel19: TPanel;
    DBEdit24: TDBEdit;
    Label33: TLabel;
    Label32: TLabel;
    DBEdit23: TDBEdit;
    DBEdit22: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit20: TDBEdit;
    DBEdit19: TDBEdit;
    DBEdit18: TDBEdit;
    DBEdit17: TDBEdit;
    DBEdit16: TDBEdit;
    DBEdit5: TDBEdit;
    Label23: TLabel;
    Label24: TLabel;
    Label26: TLabel;
    Label27: TLabel;
    Label28: TLabel;
    Label29: TLabel;
    Label30: TLabel;
    Label31: TLabel;
    QrPropriet: TmySQLQuery;
    QrProprietCodigo: TIntegerField;
    QrProprietCadastro: TDateField;
    QrProprietENatal: TDateField;
    QrProprietPNatal: TDateField;
    QrProprietTipo: TSmallintField;
    QrProprietRespons1: TWideStringField;
    QrProprietNOMEDONO: TWideStringField;
    QrProprietCNPJ_CPF: TWideStringField;
    QrProprietIE_RG: TWideStringField;
    QrProprietNIRE_: TWideStringField;
    QrProprietRUA: TWideStringField;
    QrProprietCOMPL: TWideStringField;
    QrProprietBAIRRO: TWideStringField;
    QrProprietCIDADE: TWideStringField;
    QrProprietNOMELOGRAD: TWideStringField;
    QrProprietNOMEUF: TWideStringField;
    QrProprietPais: TWideStringField;
    QrProprietTE1: TWideStringField;
    QrProprietFAX: TWideStringField;
    QrProprietNUMERO_TXT: TWideStringField;
    QrProprietLNR: TWideStringField;
    QrProprietLN2: TWideStringField;
    Excluileiturasselecionadas1: TMenuItem;
    Excluitodasleituras1: TMenuItem;
    Excluileituraatual2: TMenuItem;
    Excluir2: TMenuItem;
    ItemAtualdoprbloqueto1: TMenuItem;
    OsitensSelecionadosdoprbloquetoselecionado1: TMenuItem;
    Todositensdoprbloquetoatual1: TMenuItem;
    PageControl4: TPageControl;
    TabSheet15: TTabSheet;
    TabSheet16: TTabSheet;
    DBGradeN: TDBGrid;
    DBGrid13: TDBGrid;
    DBGrid9: TDBGrid;
    TabSheet17: TTabSheet;
    DBGrid12: TDBGrid;
    DBGrid14: TDBGrid;
    DBGrid15: TDBGrid;
    PMPreBol1: TPopupMenu;
    Excluiarrecadaodaunidadeatual1: TMenuItem;
    Excluiarrecadaesdasunidadesselecionadas1: TMenuItem;
    ExcluiestaarrecadaodeTODASunidades1: TMenuItem;
    PMPreBol2: TPopupMenu;
    ExcluileituradaunidadeATUAL1: TMenuItem;
    ExcluileiturasdasUNIDADESselecionadas1: TMenuItem;
    ExcluileiturasdeTODASunidades1: TMenuItem;
    nicoimvel1: TMenuItem;
    Mltiplosimveis1: TMenuItem;
    GroupBox1: TGroupBox;
    Label34: TLabel;
    Label35: TLabel;
    Label36: TLabel;
    Label37: TLabel;
    ItemSelecionado3: TMenuItem;
    ItensSelecionados1: TMenuItem;
    Todositens1: TMenuItem;
    TabSheet18: TTabSheet;
    PnNavega: TPanel;
    WebBrowser1: TWebBrowser;
    Panel20: TPanel;
    EdURL: TEdit;
    Label38: TLabel;
    SpeedButton7: TSpeedButton;
    QrBancos: TmySQLQuery;
    QrBancosCodigo: TIntegerField;
    QrBancosNome: TWideStringField;
    QrBancosXlsLinha: TIntegerField;
    QrBancosXlsData: TWideStringField;
    QrBancosXlsHist: TWideStringField;
    QrBancosXlsDocu: TWideStringField;
    QrBancosXlsHiDo: TWideStringField;
    QrBancosXlsCred: TWideStringField;
    QrBancosXlsDebi: TWideStringField;
    QrBancosXlsCrDb: TWideStringField;
    QrBancosXlsDouC: TWideStringField;
    QrBancosXlsTCDB: TSmallintField;
    QrBancosXlsComp: TWideStringField;
    QrBancosXlsCPMF: TWideStringField;
    QrBancosXlsSldo: TWideStringField;
    DsBancos: TDataSource;
    Label39: TLabel;
    EdBanco: TdmkEditCB;
    CBBanco: TdmkDBLookupComboBox;
    QrBancosSite: TWideStringField;
    Timer3: TTimer;
    N10: TMenuItem;
    N11: TMenuItem;
    PagarAVista1: TMenuItem;
    PMQuita: TPopupMenu;
    Compensar2: TMenuItem;
    Pagar2: TMenuItem;
    Reverter2: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    PagarAVista2: TMenuItem;
    StatusBar: TStatusBar;
    PMCNABs: TPopupMenu;
    Antigo1: TMenuItem;
    Novo1: TMenuItem;
    Panel6: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label5: TLabel;
    DBEdit1: TDBEdit;
    BtMenu: TBitBtn;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    TPDataIni: TdmkEditDateTimePicker;
    TPDataFim: TdmkEditDateTimePicker;
    BtExclui: TBitBtn;
    BtContarDinheiro: TBitBtn;
    BtCopiaCH: TBitBtn;
    BtQuita: TBitBtn;
    QrCedSac: TmySQLQuery;
    QrCedSacNOMECED: TWideStringField;
    QrCedSacNOMESAC: TWideStringField;
    BtFluxoCxa: TBitBtn;
    PMAtzCarts: TPopupMenu;
    Acarteiraselecionada1: TMenuItem;
    Todascarteiras1: TMenuItem;
    Panel21: TPanel;
    DBGrid17: TDBGrid;
    Panel22: TPanel;
    DBGCarteiras: TDBGrid;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    Panel23: TPanel;
    DBGrid16: TDBGrid;
    StaticText3: TStaticText;
    Panel24: TPanel;
    BtAtzCarts: TBitBtn;
    Panel25: TPanel;
    DBGrid18: TDBGrid;
    BtAutom: TBitBtn;
    QrPrevCondCod: TIntegerField;
    QrCartN: TmySQLQuery;
    QrCartNCarteira: TIntegerField;
    QrCartNNOMECART: TWideStringField;
    QrCartNInicial: TFloatField;
    QrCartNANT_CRED: TFloatField;
    QrCartNANT_DEB: TFloatField;
    QrCartNATU_CRED: TFloatField;
    QrCartNATU_DEB: TFloatField;
    QrCartNTRANSF: TFloatField;
    QrCartNANTERIOR: TFloatField;
    QrCartNRECEITAS: TFloatField;
    QrCartNDESPESAS: TFloatField;
    QrCartNSALDOMES: TFloatField;
    QrCartNFINAL: TFloatField;
    PMRefresh: TPopupMenu;
    Atual5: TMenuItem;
    Todas1: TMenuItem;
    TabSheet19: TTabSheet;
    Panel26: TPanel;
    DBGrid19: TDBGrid;
    Label15: TLabel;
    EdMesesArreFut: TdmkEdit;
    PMArreFut: TPopupMenu;
    Incluiarrecadaofutura1: TMenuItem;
    Alteraarrecadaofutura1: TMenuItem;
    Excluiarrecadaofutura1: TMenuItem;
    N14: TMenuItem;
    Incluiitenspragendados1: TMenuItem;
    QrCartsTrfCRED: TFloatField;
    QrCartsTrfDEB: TFloatField;
    QrCartsTRF_CRED: TFloatField;
    QrCartsTRF_DEBI: TFloatField;
    BtPesquisa: TBitBtn;
    Transformaremitemdebloqueto1: TMenuItem;
    QrZeroS: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    Gerarselecionados1: TMenuItem;
    DBGLct: TdmkDBGrid;
    QrPrevAvisoVerso: TWideStringField;
    TabSheet21: TTabSheet;
    N15: TMenuItem;
    Criarbloquetosparaitensquenoso1: TMenuItem;
    Provisrio1: TMenuItem;
    N16: TMenuItem;
    Panel30: TPanel;
    Panel31: TPanel;
    dmkDBGrid2: TdmkDBGrid;
    Panel32: TPanel;
    Label40: TLabel;
    DBEdit25: TDBEdit;
    QrZeroAUnidade: TWideStringField;
    CBUH: TDBLookupComboBox;
    PMCopiaCH: TPopupMenu;
    Cpiadecheque1: TMenuItem;
    Cpiadedbitoemconta1: TMenuItem;
    BitBtn11: TBitBtn;
    Label41: TLabel;
    QrUHs: TmySQLQuery;
    QrUHsUnidade: TWideStringField;
    QrUHsConta: TIntegerField;
    DsUHs: TDataSource;
    Adicionarabloqueto1: TMenuItem;
    Itematual1: TMenuItem;
    Selecionados4: TMenuItem;
    Todositens2: TMenuItem;
    Leituraselecionadalista1: TMenuItem;
    BtEmiteCheque: TBitBtn;
    N18: TMenuItem;
    Exclusoincondicional1: TMenuItem;
    N19: TMenuItem;
    Localizarlanamentoorigem1: TMenuItem;
    dmkEdlocLancto: TdmkEdit;
    Label42: TLabel;
    SpeedButton5: TSpeedButton;
    frxPDFExport1: TfrxPDFExport;
    Panel5: TPanel;
    Panel33: TPanel;
    QrZeroCCond: TIntegerField;
    QrZeroCPeriodo: TIntegerField;
    QrZeroCApto: TIntegerField;
    Imveis1: TMenuItem;
    QrArreBolBoleto: TFloatField;
    QrAriBoleto: TFloatField;
    QrImpBolBoleto: TFloatField;
    QrBolLBoleto: TFloatField;
    QrPesqMBBoleto: TFloatField;
    QrPesqBCBBloqueto: TFloatField;
    QrSelBolBloqueto: TFloatField;
    Rateiraconsumo1: TMenuItem;
    N21: TMenuItem;
    QrBolacarBGeraTyp: TSmallintField;
    QrBolacarBGeraFat: TFloatField;
    QrBolacarBCasRat: TSmallintField;
    QrBolacarBNaoImpLei: TSmallintField;
    Listadecomposiesdearrecadaes1: TMenuItem;
    frxVerso: TfrxReport;
    frxCondA: TfrxReport;
    frxCondB: TfrxReport;
    frxCondC: TfrxReport;
    frxCondE2: TfrxReport;
    frxCondG: TfrxReport;
    frxCondR2: TfrxReport;
    frxCondR3: TfrxReport;
    frxCondR4: TfrxReport;
    Demonstrativodereceitasedespesas1: TMenuItem;
    QrPrevAviso11: TWideStringField;
    QrPrevAviso12: TWideStringField;
    QrPrevAviso13: TWideStringField;
    QrPrevAviso14: TWideStringField;
    QrPrevAviso15: TWideStringField;
    QrPrevAviso16: TWideStringField;
    QrPrevAviso17: TWideStringField;
    QrPrevAviso18: TWideStringField;
    QrPrevAviso19: TWideStringField;
    QrPrevAviso20: TWideStringField;
    Label43: TLabel;
    DBEdit26: TDBEdit;
    DBEdit27: TDBEdit;
    Label44: TLabel;
    Label45: TLabel;
    DBEdit28: TDBEdit;
    DBEdit29: TDBEdit;
    Label46: TLabel;
    Label47: TLabel;
    DBEdit30: TDBEdit;
    Label48: TLabel;
    DBEdit31: TDBEdit;
    DBEdit32: TDBEdit;
    Label49: TLabel;
    Label50: TLabel;
    DBEdit33: TDBEdit;
    DBEdit34: TDBEdit;
    Label51: TLabel;
    Label52: TLabel;
    DBEdit35: TDBEdit;
    Label53: TLabel;
    Label54: TLabel;
    Label55: TLabel;
    Label56: TLabel;
    Label58: TLabel;
    Label59: TLabel;
    Label60: TLabel;
    BtProtoCD: TBitBtn;
    PMProtoCD: TPopupMenu;
    Adicionalanamentos1: TMenuItem;
    Atual4: TMenuItem;
    Selecionados5: TMenuItem;
    BtTrfCta: TBitBtn;
    PMTrfCta: TPopupMenu;
    Novatransferncia1: TMenuItem;
    Alteratransferncia1: TMenuItem;
    Excluitransferncia1: TMenuItem;
    BtDesfazOrdenacao: TBitBtn;
    BtAgeProv: TBitBtn;
    Adicionaitensdeprovisoagendados1: TMenuItem;
    BtAgendaProvShow: TBitBtn;
    TabSheet23: TTabSheet;
    MeAtz: TMemo;
    Balanceteconfigurvel1: TMenuItem;
    Colocarmsdecompetnciaondenotem1: TMenuItem;
    Datalancto1: TMenuItem;
    Vencimento1: TMenuItem;
    ColocarUHondenotem1: TMenuItem;
    MsanterioraoVencimento1: TMenuItem;
    Panel34: TPanel;
    DBGrid10: TDBGrid;
    TabSheet22: TTabSheet;
    PageControl5: TPageControl;
    TabSheet24: TTabSheet;
    Panel36: TPanel;
    Splitter1: TSplitter;
    Panel37: TPanel;
    StaticText5: TStaticText;
    DBGBLE: TDBGrid;
    Panel38: TPanel;
    StaticText6: TStaticText;
    TabSheet25: TTabSheet;
    PageControl6: TPageControl;
    TabSheet26: TTabSheet;
    DBGrid24: TDBGrid;
    DBGrid25: TDBGrid;
    TabSheet27: TTabSheet;
    DBGrid27: TDBGrid;
    TabSheet28: TTabSheet;
    DBGrid28: TDBGrid;
    DBGrid29: TDBGrid;
    DBGrid22: TDBGrid;
    Panel35: TPanel;
    BtReopen2: TBitBtn;
    Relatriodearrecadaesselecionveis1: TMenuItem;
    Panel39: TPanel;
    DBGrid26: TDBGrid;
    Listadearrecadaes1: TMenuItem;
    DBGBLC: TDBGrid;
    Panel40: TPanel;
    BtReverte2: TBitBtn;
    Panel41: TPanel;
    BtDescompensa: TBitBtn;
    BtItemCNAB: TBitBtn;
    BtLeiStep: TBitBtn;
    CkZerado: TCheckBox;
    TPLoc: TdmkEditDateTimePicker;
    Label57: TLabel;
    SbLocUltDta: TSpeedButton;
    Label61: TLabel;
    EdBloqueto: TdmkEdit;
    SbLocBloq: TSpeedButton;
    Acertarcreditopelovalorpago1: TMenuItem;
    frxCondE1: TfrxReport;
    GroupBox2: TGroupBox;
    EdSoma: TdmkEdit;
    Label20: TLabel;
    EdDebi: TdmkEdit;
    Label62: TLabel;
    EdCred: TdmkEdit;
    Label63: TLabel;
    ListadeProvises1: TMenuItem;
    ComvaloresporUH1: TMenuItem;
    Somenteprovises1: TMenuItem;
    Ambos1: TMenuItem;
    N23: TMenuItem;
    CpiadechequeNovo1: TMenuItem;
    Panel18: TPanel;
    QrPrevConfigBol: TIntegerField;
    QrPrevModelBloq: TSmallintField;
    QrPrevNOMECONFIGBOL: TWideStringField;
    QrPrevNOMEMODELBLOQ: TWideStringField;
    QrPrevBalAgrMens: TSmallintField;
    QrPrevCompe: TSmallintField;
    TravadestravaPerodo1: TMenuItem;
    QrPrevEncerrado: TSmallintField;
    Panel42: TPanel;
    DBText12: TDBText;
    DBText13: TDBText;
    QrPrevNOME_ENCERRADO: TWideStringField;
    DBText1: TDBText;
    PMCNAB_Vai: TPopupMenu;
    AdicionaaoarquivoCNAB1: TMenuItem;
    GerenciaoarquivoCNAB1: TMenuItem;
    N6: TMenuItem;
    LimpadadosarquivoCNAB1: TMenuItem;
    ImprimebloquetoNovo1: TMenuItem;
    Selecionado1: TMenuItem;
    Atual6: TMenuItem;
    odos1: TMenuItem;
    PorLotedeProtocolo2: TMenuItem;
    DBGrid21: TDBGrid;
    StaticText7: TStaticText;
    N7: TMenuItem;
    ModelosdeimpressoporUH1: TMenuItem;
    Definemodelo1: TMenuItem;
    Excluiitemns1: TMenuItem;
    CkDesign: TCheckBox;
    PMEmiteCheque: TPopupMenu;
    Servidor2: TMenuItem;
    Local1: TMenuItem;
    frxCondH1: TfrxReport;
    frxCondH2: TfrxReport;
    frxCondH3: TfrxReport;
    frxCondH4: TfrxReport;
    frxCondH5: TfrxReport;
    frxCondH6: TfrxReport;
    Panel43: TPanel;
    Panel44: TPanel;
    Label1: TLabel;
    Label25: TLabel;
    DBText11: TDBText;
    BtDesfazOrdemBloq: TBitBtn;
    Localizalote1: TMenuItem;
    Devolveparaagendados1: TMenuItem;
    Excluipermanentemente1: TMenuItem;
    ItemAtual2: TMenuItem;
    ItensSelecionados2: TMenuItem;
    odosItens1: TMenuItem;
    ImprimeCarndebloquetos1: TMenuItem;
    PMConfContasExe: TPopupMenu;
    Pagamentosexecutados1: TMenuItem;
    Contasmensais1: TMenuItem;
    BtDuplica: TBitBtn;
    Pn_: TPanel;
    UmalinhaporUH1: TMenuItem;
    Umalinhaporbloqueto1: TMenuItem;
    BtInsSal: TBitBtn;
    Cpiaautomtica1: TMenuItem;
    Panel45: TPanel;
    RGAgrupListaA: TRadioGroup;
    RGGerado: TRadioGroup;
    LaTempo: TLabel;
    QrBolA: TmySQLQuery;
    QrBolACodigo: TIntegerField;
    QrBolAControle: TIntegerField;
    QrBolAConta: TIntegerField;
    QrBolAValor: TFloatField;
    QrBolAArreBaI: TIntegerField;
    QrBolATexto: TWideStringField;
    QrBolALk: TIntegerField;
    QrBolADataCad: TDateField;
    QrBolADataAlt: TDateField;
    QrBolAUserCad: TIntegerField;
    QrBolAUserAlt: TIntegerField;
    QrBolAApto: TIntegerField;
    QrBolAArreBaC: TIntegerField;
    QrBolAVencto: TDateField;
    QrBolAPropriet: TIntegerField;
    QrBolABoleto: TFloatField;
    ImgCondGer: TImage;
    N8: TMenuItem;
    Localizarolanamentodeorigem1: TMenuItem;
    RGTipoData: TRadioGroup;
    Entrecarteiras1: TMenuItem;
    Incluitransfernciaentrecarteiras1: TMenuItem;
    Alteratransfernciaentrecarteiras1: TMenuItem;
    Excluitransfernciaentrecarteiras1: TMenuItem;
    N17: TMenuItem;
    Localizatransfernciaentrecarteiras1: TMenuItem;
    Entrecontas1: TMenuItem;
    Incluitransfernciaentrecontas1: TMenuItem;
    Alteratransfernciaentrecontas1: TMenuItem;
    Excluitransfernciaentrecontas1: TMenuItem;
    N20: TMenuItem;
    Agendamentoporvarreduradelanamentos1: TMenuItem;
    ListacomItens1: TMenuItem;
    ProvisoAtual1: TMenuItem;
    ProvisesSelecionadas1: TMenuItem;
    odasprovises1: TMenuItem;
    ProvisescomItens1: TMenuItem;
    QrPesqBolA: TmySQLQuery;
    QrPesqBolAControle: TIntegerField;
    QrPesqBolAGenero: TIntegerField;
    QrPesqBolAValor: TFloatField;
    QrPesqBolATexto: TWideStringField;
    QrPesqBolAApto: TIntegerField;
    QrPesqBolAPropriet: TIntegerField;
    QrPesqBolAVencto: TDateField;
    QrPesqBolAUnidade: TWideStringField;
    QrPesqBolADono: TIntegerField;
    PMConfContasCad: TPopupMenu;
    ContasMensaisCadastrodeEmisses1: TMenuItem;
    ContasMensaisCadastrodePagamentos1: TMenuItem;
    frxCondIB: TfrxReport;
    frxCondIR: TfrxReport;
    frxCondD: TfrxReport;
    UmporDocumento1: TMenuItem;
    UmparacadaLanamento1: TMenuItem;
    QrAriLancto: TIntegerField;
    BtFisico: TBitBtn;
    QrProprietNUMERO: TFloatField;
    QrProprietLograd: TFloatField;
    QrProprietCEP: TFloatField;
    QrPRICODCONTA: TIntegerField;
    QrPRICODSUBGRUPO: TIntegerField;
    Atualescolhermodelo1: TMenuItem;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBCntrl: TGroupBox;
    Panel28: TPanel;
    Panel29: TPanel;
    BtReabre: TBitBtn;
    BtSaida: TBitBtn;
    SbImprime: TBitBtn;
    BitBtn4: TBitBtn;
    BtConcilia: TBitBtn;
    BtCNAB: TBitBtn;
    SbPesq: TBitBtn;
    BtContasHistSdo: TBitBtn;
    BtConfContasCad: TBitBtn;
    BtConfContasExe: TBitBtn;
    BtCadCond: TBitBtn;
    BtProtocolo: TBitBtn;
    BtCNAB_Vai: TBitBtn;
    Panel27: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    DbEdCodCond: TDBEdit;
    ProgressBar3: TProgressBar;
    N22: TMenuItem;
    ImportaodeLantosMod01Boletos1: TMenuItem;
    PMImportSal: TPopupMenu;
    Modelo01RelExactus1: TMenuItem;
    Modelo02Pholha1: TMenuItem;
    ImportaodeLantosMod02inad1: TMenuItem;
    Conta1: TMenuItem;
    QrMPALancto: TIntegerField;
    DBGCNS: TdmkDBGrid;
    Panel2: TPanel;
    BitBtn2: TBitBtn;
    QrAriCNAB_Cfg: TIntegerField;
    QrZeroACodigo: TIntegerField;
    QrZero: TmySQLQuery;
    QrLoc: TmySQLQuery;
    N24: TMenuItem;
    N25: TMenuItem;
    Itematual3: TMenuItem;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure PageControl1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure DBGCarteirasDblClick(Sender: TObject);
    procedure BtPagtoDuvidaClick(Sender: TObject);
    procedure Calcula1Click(Sender: TObject);
    procedure Limpa1Click(Sender: TObject);
    procedure Diferena1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TPDataIniChange(Sender: TObject);
    procedure TPDataFimChange(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure QrPrevBaICalcFields(DataSet: TDataSet);
    procedure CkAplicaveisClick(Sender: TObject);
    procedure QrPrevCalcFields(DataSet: TDataSet);
    procedure QrPrevAfterOpen(DataSet: TDataSet);
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure Incluinovooramento1Click(Sender: TObject);
    procedure BtOrcamentoClick(Sender: TObject);
    procedure Novoitemdeoramento1Click(Sender: TObject);
    procedure QrPrevAfterScroll(DataSet: TDataSet);
    procedure Alteraitemselecionado1Click(Sender: TObject);
    procedure PMProvisaoPopup(Sender: TObject);
    procedure BtProvisaoClick(Sender: TObject);
    procedure Adicinaitensbase1Click(Sender: TObject);
    procedure BtBaseClick(Sender: TObject);
    procedure Incluiitens1Click(Sender: TObject);
    procedure PMBasePopup(Sender: TObject);
    procedure Alteraitemselecionado2Click(Sender: TObject);
    procedure BtBoletoClick(Sender: TObject);
    procedure BtLeituraClick(Sender: TObject);
    procedure Incluileituras1Click(Sender: TObject);
    procedure PageControl2Change(Sender: TObject);
    procedure QrPrevAfterClose(DataSet: TDataSet);
    procedure Alteraleituraatual1Click(Sender: TObject);
    procedure BtArrecadaClick(Sender: TObject);
    procedure QrArreBaICalcFields(DataSet: TDataSet);
    procedure QrArreAfterScroll(DataSet: TDataSet);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtMenuClick(Sender: TObject);
    procedure Compensar1Click(Sender: TObject);
    procedure Pagar1Click(Sender: TObject);
    procedure Reverter1Click(Sender: TObject);
    procedure Localizar2Click(Sender: TObject);
    procedure Copiar1Click(Sender: TObject);
    procedure Recibo1Click(Sender: TObject);
    procedure QrArreBolAfterScroll(DataSet: TDataSet);
    procedure Itemselecionado1Click(Sender: TObject);
    procedure TODOSitensdearrecadao1Click(Sender: TObject);
    procedure QrArreBeforeClose(DataSet: TDataSet);
    procedure QrArreBolBeforeClose(DataSet: TDataSet);
    procedure QrCartsCalcFields(DataSet: TDataSet);
    procedure DoBloquetoatual1Click(Sender: TObject);
    procedure BloquetosSelecionados1Click(Sender: TObject);
    procedure DeTodosBloquetos1Click(Sender: TObject);
    procedure PMMenuPopup(Sender: TObject);
    procedure DBGLctDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure DBGLctKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtConciliaClick(Sender: TObject);
    procedure BtPrebolClick(Sender: TObject);
    procedure PageControl2Changing(Sender: TObject; var AllowChange: Boolean);
    procedure Atual2Click(Sender: TObject);
    procedure Selecionados2Click(Sender: TObject);
    procedure Todos2Click(Sender: TObject);
    procedure PMBoletoPopup(Sender: TObject);
    procedure DobloquetoAtual2Click(Sender: TObject);
    procedure DosbloquetosSelecionados1Click(Sender: TObject);
    procedure DeTodosbloquetos2Click(Sender: TObject);
    procedure BtCNABClick(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    // procedure frxBalancete1GetValue(const VarName: String;
    // var Value: Variant);
    procedure BitBtn4Click(Sender: TObject);
    procedure BtCopiaCHClick(Sender: TObject);
    procedure Atual3Click(Sender: TObject);
    procedure Selecionados3Click(Sender: TObject);
    procedure Todos3Click(Sender: TObject);
    procedure Carteiras1Click(Sender: TObject);
    procedure Data1Click(Sender: TObject);
    procedure Compensao1Click(Sender: TObject);
    procedure PMPrebol0Popup(Sender: TObject);
    procedure QrProprietCalcFields(DataSet: TDataSet);
    procedure Excluileituraatual2Click(Sender: TObject);
    procedure Excluileiturasselecionadas1Click(Sender: TObject);
    procedure Excluitodasleituras1Click(Sender: TObject);
    procedure ItemAtualdoprbloqueto1Click(Sender: TObject);
    procedure OsitensSelecionadosdoprbloquetoselecionado1Click(Sender: TObject);
    procedure Todositensdoprbloquetoatual1Click(Sender: TObject);
    procedure Excluiarrecadaodaunidadeatual1Click(Sender: TObject);
    procedure Excluiarrecadaesdasunidadesselecionadas1Click(Sender: TObject);
    procedure ExcluiestaarrecadaodeTODASunidades1Click(Sender: TObject);
    procedure ExcluileituradaunidadeATUAL1Click(Sender: TObject);
    procedure ExcluileiturasdasUNIDADESselecionadas1Click(Sender: TObject);
    procedure ExcluileiturasdeTODASunidades1Click(Sender: TObject);
    procedure nicoimvel1Click(Sender: TObject);
    procedure Mltiplosimveis1Click(Sender: TObject);
    procedure ItemSelecionado3Click(Sender: TObject);
    procedure Excluiitemselecionado1Click(Sender: TObject);
    procedure ItensSelecionados1Click(Sender: TObject);
    procedure Todositens1Click(Sender: TObject);
    procedure SpeedButton7Click(Sender: TObject);
    procedure QrBancosAfterScroll(DataSet: TDataSet);
    procedure Timer3Timer(Sender: TObject);
    procedure EdURLExit(Sender: TObject);
    procedure Compensar2Click(Sender: TObject);
    procedure Reverter2Click(Sender: TObject);
    procedure Pagar2Click(Sender: TObject);
    procedure PagarAVista2Click(Sender: TObject);
    procedure PagarAVista1Click(Sender: TObject);
    procedure BtQuitaClick(Sender: TObject);
    procedure PMQuitaPopup(Sender: TObject);
    procedure Novo1Click(Sender: TObject);
    procedure BtFluxoCxaClick(Sender: TObject);
    procedure BtAtzCartsClick(Sender: TObject);
    procedure Acarteiraselecionada1Click(Sender: TObject);
    procedure Todascarteiras1Click(Sender: TObject);
    procedure BtAutomClick(Sender: TObject);
    procedure QrCartNCalcFields(DataSet: TDataSet);
    procedure Atual5Click(Sender: TObject);
    procedure Todas1Click(Sender: TObject);
    procedure Incluiarrecadaofutura1Click(Sender: TObject);
    procedure Alteraarrecadaofutura1Click(Sender: TObject);
    procedure Excluiarrecadaofutura1Click(Sender: TObject);
    procedure Incluiitenspragendados1Click(Sender: TObject);
    procedure PMArreFutPopup(Sender: TObject);
    // procedure Sempaginao1Click(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure Transformaremitemdebloqueto1Click(Sender: TObject);
    procedure Criarbloquetosparaitensquenoso1Click(Sender: TObject);
    procedure SbPesqClick(Sender: TObject);
    procedure BtContasHistSdoClick(Sender: TObject);
    procedure Cpiadecheque1Click(Sender: TObject);
    procedure Cpiadedbitoemconta1Click(Sender: TObject);
    procedure BitBtn11Click(Sender: TObject);
    procedure CBUHKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CBUHClick(Sender: TObject);
    procedure Itematual1Click(Sender: TObject);
    procedure Selecionados4Click(Sender: TObject);
    procedure Todositens2Click(Sender: TObject);
    procedure BtEmiteChequeClick(Sender: TObject);
    procedure Exclusoincondicional1Click(Sender: TObject);
    procedure Localizarlanamentoorigem1Click(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure Imveis1Click(Sender: TObject);
    procedure Rateiraconsumo1Click(Sender: TObject);
    procedure Listadecomposiesdearrecadaes1Click(Sender: TObject);
    procedure frxCondAGetValue(const VarName: String; var Value: Variant);
    function frxCondAUserFunction(const MethodName: String;
      var Params: Variant): Variant;
    procedure frxVersoGetValue(const VarName: String; var Value: Variant);
    procedure Demonstrativodereceitasedespesas1Click(Sender: TObject);
    procedure BtReabreClick(Sender: TObject);
    // procedure AntigoSEMfraoideal1Click(Sender: TObject);
    procedure BtProtoCDClick(Sender: TObject);
    procedure Atual4Click(Sender: TObject);
    procedure BtTrfCtaClick(Sender: TObject);
    procedure Novatransferncia1Click(Sender: TObject);
    procedure Alteratransferncia1Click(Sender: TObject);
    procedure Excluitransferncia1Click(Sender: TObject);
    procedure PMTrfCtaPopup(Sender: TObject);
    procedure BtDesfazOrdenacaoClick(Sender: TObject);
    procedure BtAgeProvClick(Sender: TObject);
    procedure Adicionaitensdeprovisoagendados1Click(Sender: TObject);
    procedure BtAgendaProvShowClick(Sender: TObject);
    procedure BtConfContasCadClick(Sender: TObject);
    procedure BtConfContasExeClick(Sender: TObject);
    procedure Balanceteconfigurvel1Click(Sender: TObject);
    procedure Datalancto1Click(Sender: TObject);
    procedure Vencimento1Click(Sender: TObject);
    procedure ColocarUHondenotem1Click(Sender: TObject);
    procedure MsanterioraoVencimento1Click(Sender: TObject);
    procedure BtReopen2Click(Sender: TObject);
    procedure DBGPrevItsDblClick(Sender: TObject);
    procedure Relatriodearrecadaesselecionveis1Click(Sender: TObject);
    procedure PMImprimePopup(Sender: TObject);
    procedure RGAgrupListaAClick(Sender: TObject);
    procedure Leituraselecionadalista1Click(Sender: TObject);
    procedure BtReverte2Click(Sender: TObject);
    procedure BtItemCNABClick(Sender: TObject);
    procedure BtDescompensaClick(Sender: TObject);
    procedure BtLeiStepClick(Sender: TObject);
    procedure BtCadCondClick(Sender: TObject);
    procedure SbLocUltDtaClick(Sender: TObject);
    procedure SbLocBloqClick(Sender: TObject);
    procedure Acertarcreditopelovalorpago1Click(Sender: TObject);
    procedure Incluiintensbasedearrecadao1Click(Sender: TObject);
    procedure ListadeProvises1Click(Sender: TObject);
    procedure Somenteprovises1Click(Sender: TObject);
    procedure ComvaloresporUH1Click(Sender: TObject);
    procedure Ambos1Click(Sender: TObject);
    procedure CpiadechequeNovo1Click(Sender: TObject);
    procedure PMOrcamentoPopup(Sender: TObject);
    procedure TravadestravaPerodo1Click(Sender: TObject);
    procedure BtProtocoloClick(Sender: TObject);
    procedure QrPrevBeforeClose(DataSet: TDataSet);
    procedure BtCNAB_VaiClick(Sender: TObject);
    procedure LimpadadosarquivoCNAB1Click(Sender: TObject);
    procedure GerenciaoarquivoCNAB1Click(Sender: TObject);
    procedure AdicionaaoarquivoCNAB1Click(Sender: TObject);
    procedure Atual6Click(Sender: TObject);
    procedure Selecionado1Click(Sender: TObject);
    procedure odos1Click(Sender: TObject);
    procedure PorLotedeProtocolo2Click(Sender: TObject);
    procedure Definemodelo1Click(Sender: TObject);
    procedure Excluiitemns1Click(Sender: TObject);
    procedure Servidor2Click(Sender: TObject);
    procedure Local1Click(Sender: TObject);
    procedure BtDesfazOrdemBloqClick(Sender: TObject);
    procedure Localizalote1Click(Sender: TObject);
    procedure ItemAtual2Click(Sender: TObject);
    procedure ItensSelecionados2Click(Sender: TObject);
    procedure odosItens1Click(Sender: TObject);
    procedure ImprimeCarndebloquetos1Click(Sender: TObject);
    procedure Pagamentosexecutados1Click(Sender: TObject);
    procedure Contasmensais1Click(Sender: TObject);
    procedure BtDuplicaClick(Sender: TObject);
    procedure UmalinhaporUH1Click(Sender: TObject);
    procedure Umalinhaporbloqueto1Click(Sender: TObject);
    procedure BtInsSalClick(Sender: TObject);
    procedure Cpiaautomtica1Click(Sender: TObject);
    procedure Localizarolanamentodeorigem1Click(Sender: TObject);
    procedure RGTipoDataClick(Sender: TObject);
    procedure TPDataIniClick(Sender: TObject);
    procedure TPDataFimClick(Sender: TObject);
    procedure Incluitransfernciaentrecarteiras1Click(Sender: TObject);
    procedure Alteratransfernciaentrecarteiras1Click(Sender: TObject);
    procedure Excluitransfernciaentrecarteiras1Click(Sender: TObject);
    procedure Localizatransfernciaentrecarteiras1Click(Sender: TObject);
    procedure Incluitransfernciaentrecontas1Click(Sender: TObject);
    procedure Alteratransfernciaentrecontas1Click(Sender: TObject);
    procedure Excluitransfernciaentrecontas1Click(Sender: TObject);
    procedure Agendamentoporvarreduradelanamentos1Click(Sender: TObject);
    procedure ProvisoAtual1Click(Sender: TObject);
    procedure ProvisesSelecionadas1Click(Sender: TObject);
    procedure odasprovises1Click(Sender: TObject);
    procedure ProvisescomItens1Click(Sender: TObject);
    procedure QrPRIBeforeClose(DataSet: TDataSet);
    procedure QrPRIAfterScroll(DataSet: TDataSet);
    procedure ContasMensaisCadastrodePagamentos1Click(Sender: TObject);
    procedure ContasMensaisCadastrodeEmisses1Click(Sender: TObject);
    procedure UmparacadaLanamento1Click(Sender: TObject);
    procedure UmporDocumento1Click(Sender: TObject);
    procedure BtFisicoClick(Sender: TObject);
    procedure Atualescolhermodelo1Click(Sender: TObject);
    procedure DbEdCodCondChange(Sender: TObject);
    procedure Modelo01RelExactus1Click(Sender: TObject);
    procedure Modelo02Pholha1Click(Sender: TObject);
    procedure ImportaodeLantosMod01Boletos1Click(Sender: TObject);
    procedure ImportaodeLantosMod02inad1Click(Sender: TObject);
    procedure Conta1Click(Sender: TObject);
    procedure DBGCNSDrawColumnCell(Sender: TObject; const Rect: TRect;
      DataCol: Integer; Column: TColumn; State: TGridDrawState);
    procedure BitBtn2Click(Sender: TObject);
    procedure Gerarabertos1Click(Sender: TObject);
    procedure Gerartodosabertos1Click(Sender: TObject);
    procedure GerarAtual1Click(Sender: TObject);
    procedure Gerarselecionados1Click(Sender: TObject);
    procedure Alteraperiodoatual1Click(Sender: TObject);
    procedure PMLeituraPopup(Sender: TObject);
    procedure Itematual3Click(Sender: TObject);
    procedure PMArrecadaPopup(Sender: TObject);
    procedure PMPreBol1Popup(Sender: TObject);
    procedure PMPreBol2Popup(Sender: TObject);
  private
    { Private declarations }
    function TodosSelecionadosTemVencimento(Tipo: TSelType): Boolean;
    function TodosSelecionadosEstaoSemRestricaoJuridica(Tipo: TSelType)
      : Boolean;
    function FThisEntidade(): Integer;
    procedure ShowHint(Sender: TObject);
    procedure HabilitaBotoes;
    procedure DesfazerBoletos(Tipo: TSelType);
    procedure ExcluiLeitura(Tipo: TSelType);
    procedure ExcluiItemProvisao(Tipo: TSelType; Reagenda: Boolean);
    procedure AtualizaCarteiraAtual();
    procedure MostraFmCondGerArreFutA();
    procedure VerificaArreFut();
    procedure LocalizarPeriodoAtual;
    procedure InsAlt(Acao: TGerencimantoDeRegistro);
    procedure VeSeReabreLct();
    procedure ImprimeBoleto(Selecao: TSelType; EscolheModelo: Boolean);
    procedure MostraCondGerNew(SQLTipo: TSQLType);
    // Financeiro Novo
    procedure RecalcSaldoCarteira();
    procedure TranferenciaEntreCarteiras(Tipo: Integer);
    procedure TranferenciaEntreContas(Tipo: Integer);
    function ImpedePorAlgumMotivo(Selecao: TSelType): Boolean;
    procedure PagarRolarEmissao();
    procedure ConfiguraPopoupQuitacoes();
    procedure LocalizarOLan�amentoDeOrigem();
    procedure GeraBoeltosAbertos(Selecao: TSelType; Grade: TDBGrid);
    procedure AlteraVencimentoPreBol(Selecao: TSelType; PreBol: Boolean;
      Grade: TDBGrid);
    procedure ExcluiLeituraAtual(Controle, Lancto: Integer; Boleto: Double);
  public
    { Public declarations }
    FArreBaA, FArreBaAUni: String;

    FLoteImp, FPreEmBloq, FPreEmProt, FCompensaAltura: Integer;

    FNossoNumero, FBolSim, FBolNao, FBolItsSim, FBolItsNao: String;

    FResumo_SaldoAnt, FResumo_Receitas, FResumo_Despesas, FResumo_SaldoMes,
      FResumo_SaldoTrf: Double;

    FReabrePrevIts, FAgrupaMensal, FReceiReceb: Boolean;

    // Quita��o de emiss�o
    function ObtemValorQrBoletosIts(Item, Tipo: Integer): String;
    function FormataPeriodo(Periodo, FmtType: Integer): String;
    //
    procedure DefineSaldoResumo;
    procedure DefParams;
    procedure LocPeriodo(Atual, Periodo: Integer);
    function Va(Para: TVaiPara): Boolean;
    //
    procedure ReopenQrPrevBaI(Controle: Integer);
    procedure ReopenQrArreBaI(Controle: Integer);
    procedure ReopenPRI(Controle: Integer);
    procedure ReopenArre(Apto, Propriet: Integer);
    procedure ReopenArreBol(Boleto: Integer);
    procedure ReopenARI(Controle: Integer);
    procedure ReopenResumo();

    procedure MostraFmCondGerArreFut(SQLType: TSQLType);
    procedure MostraFmCondGerArreUni(SQLType: TSQLType);
    procedure MostraFmCondGerArreMul(SQLType: TSQLType);
    procedure CalculaTotalPRIEReabrePrevEPRI;
    procedure CalculaTotalARIEReabreArreEARI(Apto, Propriet, Controle: Integer);
    //
    procedure RecalculaArrecadacoes();
    procedure MudarPage2(Index: Integer);
    procedure ExcluiItemPreBloqueto(Tipo: TSelType);
    procedure ExcluiArrecadacaoPorTipo(Quais: TSelType);
    procedure ExcluiLeituraPorTipo(Quais: TSelType);
    procedure VerificaCarteirasCliente();
    procedure VerificaBotoes();
    procedure QuitaItemBoletoAutomatico(Data: TDateTime; Mul, Jur: Double);
    //
    procedure QuitarVariosItens();
    // Destivado para evitar erros procedure AdicionaItemABloqueto(Tipo: TselType);
    function PeriodoNaoDefinido: Boolean;
    function Define_frxCond(frx: TfrxReport; SetaMasterData: Boolean)
      : TfrxReport;
    procedure ExcluiItesArrecadacao(Controle, Lancto: Integer; Boleto: Double);
  end;

var
  FmCondGer: TFmCondGer;

implementation

uses Module, UnInternalConsts, UMySQLModule, Principal, UnGOTOy,
  CondGerNew, CondGerAvisos, ArreBaA, (*PrevBaA, ABSQuery Alexandria*) PrevBAB,
  CondGerLei1, CondGerLei2, LeiGerEdit, CondGerArreFutA, UnAuxCondGer,
  CondGerArreUni, CondGerArreMul, CondGerDelArre, CondGerArreFut, CondGerImpGer,
  ModuleLct2, CondGerBolSel, (*CondGerImpArre, ABSQuery Alexandria*)
  CondGerImpGer2, CondGerImpGer2a,
  CondGerProtoSel, CondGerProto, PrevBaCLctos, ContasHistSdo3, ContasConfCad,
  ContasConfPgto, ContasConfEmis, UnFinanceiroJan, UCreate, LctMudaCart, MeuFrx,
  GetData, ModuleCond, UnBancos, CondGerModelBloq, LctPgEmCxa, Concilia,
  ModuleGeral, LctPgVarios, MyDBCheck, LctEdit, ModuleFin, MyGlyfs, GetValor,
  MyVCLSkin, AptosModBol, UnMyPrinters, EmiteCheque_0, CondGerLocper,
  ModuleBloq, UnMyObjects, CashBal, CondGerCarne, CopiaDoc,
  FluxoCxa, ImportSal1, GeraCNAB, CondGerStep, PrevVeri, ContasMesGer,
  ModuleBco, CNAB_Cfg, Protocolo, InadUH_Load01, InadUH_Load02, ImportSal2,
  DmkDAC_PF, UnBloquetosCond, UnBloquetos_Jan, UnBloquetos;

{$R *.DFM}

/// //////////////////////////////////////////////////////////////////////////////////
procedure TFmCondGer.LocPeriodo(Atual, Periodo: Integer);
begin
  Screen.Cursor := crHourGlass;
  DefParams;
  GOTOy.LC(Atual, Periodo);
  Screen.Cursor := crDefault;
end;

function TFmCondGer.Va(Para: TVaiPara): Boolean;
var
  Tempo: TTime;
begin
  Tempo := Time;
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrPrevPeriodo.Value,
    LaRegistro.Caption[2]);
  Result := LaRegistro.Caption <> VAR_PERIODO_NAO_LOC;
  Tempo := Time - Tempo;
  LaTempo.Caption := FormatDateTime('ss:zzz', Tempo);
end;

procedure TFmCondGer.DefParams;
begin
  VAR_GOTOTABELA := DmCond.FTabPrvA;
  VAR_GOTOmySQLTABLE := QrPrev;
  VAR_GOTONEG := gotoPiZ;
  VAR_GOTOCAMPO := 'Periodo';
  VAR_GOTONOME := '';
  VAR_GOTOMYSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 1;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT prv.*, cnd.Cliente CondCli, cnd.Codigo CondCod, ');
  VAR_SQLx.Add('cfb.Nome NOMECONFIGBOL');
  VAR_SQLx.Add('FROM ' + DmCond.FTabPrvA + ' prv');
  VAR_SQLx.Add('LEFT JOIN cond cnd ON cnd.Codigo=prv.Cond');
  VAR_SQLx.Add('LEFT JOIN configbol cfb ON cfb.Codigo=prv.ConfigBol');
  VAR_SQLx.Add('WHERE prv.Cond=' + FormatFloat('0', DmCond.QrCondCodigo.Value));
  //
  VAR_SQL1.Add('AND prv.Periodo=:P0');
  //
  VAR_SQLa.Add(''); // AND prv.Nome Like :P0');
  //
  VAR_GOTOVAR1 := 'Cond=' + IntToStr(DmCond.QrCondCodigo.Value);
end;

/// //////////////////////////////////////////////////////////////////////////////////

procedure TFmCondGer.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGer.BtTrfCtaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTrfCta, BtTrfCta);
end;

procedure TFmCondGer.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  QrUHs.Close;
  QrUHs.Params[0].AsInteger := DmCond.QrCondCodigo.Value;
  QrUHs.Open;
end;

procedure TFmCondGer.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], ' ' + Geral.FFN(DmCond.QrCondCodigo.Value, 3) + ' - '
    + DmCond.QrCondNOMECLI.Value, False, taLeftJustify, 2, 10, 20);
  //
  { Alexandria
    if FileExists(FmPrincipal.FImagemDescanso) then
    begin
    MyObjects.CarregaImagemEmTImage(ImgCondGer, FmPrincipal.FImagemDescanso,
    FmPrincipal.FBMPDescanso, FmPrincipal.SD1.Colors[csButtonShadow]);
    end;
  }
end;

procedure TFmCondGer.PageControl1Change(Sender: TObject);
begin
  BtSaida.Enabled := PageControl1.ActivePageIndex = 0;
  case PageControl1.ActivePageIndex of
    1:
      VerificaCarteirasCliente();
    2:
      VerificaBotoes();
    3:
      begin
        if QrPrev.State = dsBrowse then
          LocPeriodo(QrPrevPeriodo.Value, QrPrevPeriodo.Value);
      end;
    6:
      begin
        DmCond.QrInadTot.Close;
        DmCond.QrInadTot.SQL.Clear;
        DmCond.QrInadTot.SQL.Add('SELECT SUM(lan.Credito) Credito');
        DmCond.QrInadTot.SQL.Add('FROM ' + DmCond.FTabLctA + ' lan');
        DmCond.QrInadTot.SQL.Add
          ('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
        DmCond.QrInadTot.SQL.Add('WHERE car.Tipo=2');
        DmCond.QrInadTot.SQL.Add('AND car.ForneceI=:P0');
        DmCond.QrInadTot.SQL.Add('AND lan.Depto>0');
        DmCond.QrInadTot.SQL.Add('AND lan.Sit<2');
        DmCond.QrInadTot.SQL.Add('AND lan.Reparcel=0');
        DmCond.QrInadTot.SQL.Add('AND lan.Vencimento < SYSDATE()');
        DmCond.QrInadTot.Params[0].AsInteger := QrPrevCondCli.Value;
        UMyMod.AbreQuery(DmCond.QrInadTot, Dmod.MyDB);
        //
      end;
  end;
  GBCntrl.Visible := PageControl1.ActivePageIndex in ([0, 1, 2]);
  //
  {
    BtFisico.Enabled := False;
    BtFisico.Enabled :=
    Dmod.ReopenCB4Data4(DmCond.QrCondCodigo.Value, CO_TAB_INDX_CB4_FOTODOCU);
  }
end;

procedure TFmCondGer.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //
  Dmod.ReopenControle;
  //
  MyObjects.frxConfiguraPDF(frxPDFExport1);
  //
  Application.OnHint := ShowHint;
  //
  PageControl1.ActivePageIndex := 0;
  PageControl2.ActivePageIndex := 0;
  PageControl3.ActivePageIndex := 0;
  PageControl4.ActivePageIndex := 0;
  PageControl5.ActivePageIndex := 1;
  PageControl6.ActivePageIndex := 1;
  (*
    BtOrcamento.Enabled          := True;
    BtProvisao.Enabled           := False;
    BtLeitura.Enabled            := False;
  *)
  /// ///////////////////////////////////////////////////////////////////////////
  DBGCarteiras.DataSource := DmLct2.DsCrt;
  EdCodigo.DataSource := DmLct2.DsCrt;
  EdNome.DataSource := DmLct2.DsCrt;
  EdSaldo.DataSource := DmLct2.DsCrt;
  EdDiferenca.DataSource := DmLct2.DsCrt;
  EdCaixa.DataSource := DmLct2.DsCrt;
  //
  DBGLct.DataSource := DmLct2.DsLct;
  DBEdit1.DataSource := DmLct2.DsLct;
  DBEdit2.DataSource := DmLct2.DsLct;
  DBEdit3.DataSource := DmLct2.DsLct;
  DBEdit4.DataSource := DmLct2.DsLct;
  // DBEdit5.DataSource := FmPrincipal.DsLct;
  DBGrid16.DataSource := DmLct2.DsCrt;
  DBGrid21.DataSource := DmBloq.DsPrevModBol;
  /// ///////////////////////////////////////////////////////////////////////////
  TPDataIni.Date := Date - Geral.ReadAppKeyCU('Dias', Application.Title,
    ktInteger, 60);
  TPDataFim.Date := Date;
  /// ///////////////////////////////////////////////////////////////////////////
  FBolSim := '';
  FBolItsSim := '';
  FBolNao := '';
  FBolItsNao := '';
  //
  { Alexandria
    if FileExists(FmPrincipal.FImagemDescanso) then
    begin
    MyObjects.CarregaImagemEmTImage(ImgCondGer, FmPrincipal.FImagemDescanso,
    FmPrincipal.FBMPDescanso, FmPrincipal.SD1.Colors[csButtonShadow]);
    end;
  }
  EdMesesArreFut.Text := IntToStr(Geral.ReadAppKeyCU('MesesAntArreFut',
    Application.Title, ktInteger, 1));
  dmkEdlocLancto.ValueVariant := Geral.ReadAppKeyCU('LocLancto',
    Application.Title, ktInteger, 0);
  Panel37.Height := 141;
  TPLoc.Date := Date;
  //
  RGTipoData.ItemIndex := Geral.ReadAppKeyCU('TipoPeriodo', Application.Title,
    ktInteger, 0);
  //
  frxCondH2.ScriptText := frxCondH1.ScriptText;
  frxCondH3.ScriptText := frxCondH1.ScriptText;
  frxCondH4.ScriptText := frxCondH1.ScriptText;
  frxCondH5.ScriptText := frxCondH1.ScriptText;
  frxCondH6.ScriptText := frxCondH1.ScriptText;
  frxCondR3.ScriptText := frxCondR2.ScriptText;
  frxCondR4.ScriptText := frxCondR2.ScriptText;
  // frxCondR2.ScriptText := frxCondH1.ScriptText;
  frxCondR3.ScriptText := frxCondR2.ScriptText;
  frxCondR4.ScriptText := frxCondR2.ScriptText;
  //
  FReabrePrevIts := False;
  GBCntrl.Visible := PageControl1.ActivePageIndex in ([0, 1, 2]);
end;

procedure TFmCondGer.BtIncluiClick(Sender: TObject);
begin
  VerificaCarteirasCliente();
  VerificaBotoes();
  InsAlt(tgrInclui);
end;

procedure TFmCondGer.BtInsSalClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImportSal, BtInsSal);
end;

procedure TFmCondGer.DBGCarteirasDblClick(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 2;
  PageControl1Change(PageControl1);
end;

procedure TFmCondGer.BtPagtoDuvidaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMSaldoAqui, BtPagtoDuvida);
end;

procedure TFmCondGer.Calcula1Click(Sender: TObject);
begin
  EdSdoAqui.Text := UFinanceiro.SaldoAqui(1, EdSdoAqui.Text, DmLct2.QrLct,
    DmLct2.QrCrt);
end;

procedure TFmCondGer.Leituraselecionadalista1Click(Sender: TObject);
begin
  FmCondGerImpGer.ImprimeLeituraSelecionadaLista();
end;

procedure TFmCondGer.Limpa1Click(Sender: TObject);
begin
  EdSdoAqui.Text := UFinanceiro.SaldoAqui(0, EdSdoAqui.Text, DmLct2.QrLct,
    DmLct2.QrCrt);
end;

procedure TFmCondGer.Diferena1Click(Sender: TObject);
begin
  EdSdoAqui.Text := UFinanceiro.SaldoAqui(2, EdSdoAqui.Text, DmLct2.QrLct,
    DmLct2.QrCrt);
end;

procedure TFmCondGer.DBGCNSDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  if Column.FieldName = 'DifCaren' then
    MeuVCLSkin.DrawGrid(TDBGrid(DBGCNS), Rect, 1, DmCond.QrCNSDifCaren.Value);
end;

procedure TFmCondGer.Servidor2Click(Sender: TObject);
{ ???
  var
  Valor, Benef, Cidade: String;
}
begin
  { ???
    if DmLct2.QrLctDebito.Value <> 0 then
    begin
    Valor := Geral.FFT(DmLct2.QrLctDebito.Value, 2, siPositivo);
    Benef := DmLct2.QrLctNOMEFORNECEDOR.Value;
    end else begin
    Valor := Geral.FFT(DmLct2.QrLctCredito.Value, 2, siPositivo);
    Benef := DmLct2.QrLctNOMECLIENTE.Value;
    end;
    //
    Benef := Geral.SemAcento(Geral.Maiusculas(Benef, False));
    Cidade := Geral.SemAcento(Geral.Maiusculas(Dmod.QrDonoCIDADE.Value, False));
    //
    Application.CreateForm(TFmEmiteCheque_0, FmEmiteCheque_0);
    FmEmiteCheque_0.TPData.Date   := DmLct2.QrLctData.Value;
    FmEmiteCheque_0.EdValor.Text  := Valor;
    FmEmiteCheque_0.EdBenef.Text  := Benef;
    FmEmiteCheque_0.EdCidade.Text := Cidade;
    FmEmiteCheque_0.EdBanco.Text  := FormatFloat('0', DmLct2.QrCrtBanco1.Value);
    //
    FmEmiteCheque_0.ShowModal;
    FmEmiteCheque_0.Destroy;
  }
end;

procedure TFmCondGer.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Geral.WriteAppKeyCU('Dias', Application.Title, Date - TPDataIni.Date,
    ktInteger);
  Geral.WriteAppKeyCU('Condominio2', Application.Title,
    DmCond.QrCondCodigo.Value, ktInteger);
  Geral.WriteAppKeyCU('Periodo', Application.Title, QrPrevPeriodo.Value,
    ktInteger);
  //
  Geral.WriteAppKeyCU('TipoPeriodo', Application.Title, RGTipoData.ItemIndex,
    ktInteger);
  //
  FmPrincipal.AtualizaTextoBtAcessoRapido();
  // 20/06/2009
  FmPrincipal.FJaAbriuFmCondGer := True;
  Action := caNone;
  Hide;
end;

procedure TFmCondGer.TPDataIniChange(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmCondGer.TPDataIniClick(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmCondGer.TPDataFimChange(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmCondGer.TPDataFimClick(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmCondGer.BtAgeProvClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPrevBaCLctos, FmPrevBaCLctos, afmoNegarComAviso) then
  begin
    FmPrevBaCLctos.FForm := 'CondGer';
    FmPrevBaCLctos.ShowModal;
    FmPrevBaCLctos.Destroy;
  end;
end;

procedure TFmCondGer.BtAlteraClick(Sender: TObject);
begin
  InsAlt(tgrAltera);
end;

procedure TFmCondGer.BtRefreshClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRefresh, BtRefresh);
end;

procedure TFmCondGer.QrPrevBaICalcFields(DataSet: TDataSet);
var
  Ini, Fim: String;
begin
  QrPrevBaINOMESITCOBR.Value := FmPrincipal.RgSitCobr.Items
    [QrPrevBaISitCobr.Value];
  case QrPrevBaISitCobr.Value of
    1:
      begin
        Ini := dmkPF.VerificaMes(QrPrevBaIParcPerI.Value, False);
        Fim := dmkPF.VerificaMes(QrPrevBaIParcPerF.Value, False);
      end;
    2:
      begin
        Ini := dmkPF.MesEAnoDoPeriodo(QrPrevBaIParcPerI.Value);
        Fim := dmkPF.MesEAnoDoPeriodo(QrPrevBaIParcPerF.Value);
      end;
  else
    begin
      Ini := '';
      Fim := '';
    end;
  end;
  QrPrevBaIINICIO.Value := Ini;
  QrPrevBaIFINAL.Value := Fim;
end;

procedure TFmCondGer.QrPrevBeforeClose(DataSet: TDataSet);
begin
  QrPRI.Close;
  QrArre.Close;
  DmCond.QrCons.Close;
  DmBloq.QrPrevModBol.Close;
  DmBloq.QrBoletos.Close;
  //
  BtProtocolo.Enabled := False;
end;

procedure TFmCondGer.ReopenQrPrevBaI(Controle: Integer);
begin
  QrPrevBaI.Close;
  QrPrevBaI.Params[0].AsInteger := DmCond.QrCondCodigo.Value;
  QrPrevBaI.Open;
  //
  QrPrevBaI.Locate('Controle', Controle, []);
end;

procedure TFmCondGer.ReopenQrArreBaI(Controle: Integer);
begin
  QrArreBaI.Close;
  QrArreBaI.Params[0].AsInteger := DmCond.QrCondCodigo.Value;
  QrArreBaI.Open;
  //
  QrArreBaI.Locate('Controle', Controle, []);
end;

procedure TFmCondGer.CkAplicaveisClick(Sender: TObject);
begin
  ReopenQrPrevBaI(QrPrevBaIControle.Value);
end;

procedure TFmCondGer.QrPrevCalcFields(DataSet: TDataSet);
var
  Valor: Double;
begin
  if QrPrevEncerrado.Value = 0 then
  begin
    QrPrevNOME_ENCERRADO.Value := 'ABERTO';
    DBText12.Font.Color := clGreen;
  end
  else
  begin
    QrPrevNOME_ENCERRADO.Value := 'ENCERRADO';
    DBText12.Font.Color := clRed;
  end;
  //
  QrPrevNOMEMODELBLOQ.Value := UBloquetos.LetraModelosBloq
    (QrPrevModelBloq.Value);
  //
  QrPrevPERIODO_TXT.Value := dmkPF.MesEAnoDoPeriodoLongo(QrPrevPeriodo.Value);
  // + '   [ $ '+Geral.FFT(QrPrevGastos.Value, 2, siNegativo)+' ]';
  DmCond.QrSumBol.Close;
  DmCond.QrSumBol.SQL.Clear;
  DmCond.QrSumBol.SQL.Add('SELECT SUM(Valor) VALOR');
  DmCond.QrSumBol.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
  DmCond.QrSumBol.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto');
  DmCond.QrSumBol.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
  DmCond.QrSumBol.SQL.Add('WHERE ari.Codigo=:P0');
  DmCond.QrSumBol.SQL.Add('AND ari.Boleto <> 0');
  DmCond.QrSumBol.SQL.Add('');
  DmCond.QrSumBol.SQL.Add('UNION');
  DmCond.QrSumBol.SQL.Add('');
  DmCond.QrSumBol.SQL.Add('SELECT SUM(Valor) VALOR');
  DmCond.QrSumBol.SQL.Add('FROM ' + DmCond.FTabCnsA + ' cni');
  DmCond.QrSumBol.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto');
  DmCond.QrSumBol.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet');
  DmCond.QrSumBol.SQL.Add('WHERE cni.Cond=:P1');
  DmCond.QrSumBol.SQL.Add('AND cni.Periodo=:P2');
  DmCond.QrSumBol.SQL.Add('AND cni.Boleto <> 0');
  DmCond.QrSumBol.Params[00].AsInteger := QrPrevCodigo.Value;
  DmCond.QrSumBol.Params[01].AsInteger := QrPrevCond.Value;
  DmCond.QrSumBol.Params[02].AsInteger := QrPrevPeriodo.Value;
  UMyMod.AbreQuery(DmCond.QrSumBol, Dmod.MyDB);
  Valor := 0;
  while not DmCond.QrSumBol.Eof do
  begin
    Valor := Valor + DmCond.QrSumBolVALOR.Value;
    DmCond.QrSumBol.Next;
  end;
  QrPrevTOT_BOL.Value := Valor;
  DmCond.QrSumBol.Close;
  //
  QrSumPre.Close;
  QrSumPre.SQL.Clear;
  QrSumPre.SQL.Add('SELECT SUM(Valor) VALOR');
  QrSumPre.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
  QrSumPre.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto');
  QrSumPre.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
  QrSumPre.SQL.Add('WHERE ari.Codigo=:P0');
  QrSumPre.SQL.Add('AND ari.Boleto = 0');
  QrSumPre.SQL.Add('');
  QrSumPre.SQL.Add('UNION');
  QrSumPre.SQL.Add('');
  QrSumPre.SQL.Add('SELECT SUM(Valor) VALOR');
  QrSumPre.SQL.Add('FROM ' + DmCond.FTabCnsA + ' cni');
  QrSumPre.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto');
  QrSumPre.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet');
  QrSumPre.SQL.Add('WHERE cni.Cond=:P1');
  QrSumPre.SQL.Add('AND cni.Periodo=:P2');
  QrSumPre.SQL.Add('AND cni.Boleto = 0');
  QrSumPre.Params[00].AsInteger := QrPrevCodigo.Value;
  QrSumPre.Params[01].AsInteger := QrPrevCond.Value;
  QrSumPre.Params[02].AsInteger := QrPrevPeriodo.Value;
  QrSumPre.Open;
  QrPrevTOT_PRE.Value := QrSumPreVALOR.Value;
  Valor := 0;
  while not QrSumPre.Eof do
  begin
    Valor := Valor + QrSumPreVALOR.Value;
    QrSumPre.Next;
  end;
  QrPrevTOT_PRE.Value := Valor;
  QrSumPre.Close;
  //
end;

procedure TFmCondGer.QrPRIAfterScroll(DataSet: TDataSet);
begin
  if FReabrePrevIts then
  begin
    FmCondGerImpGer.QrPrevLcts.Close;
    FmCondGerImpGer.QrPrevLcts.Params[0].AsInteger := QrPRIPrevBaI.Value;
    FmCondGerImpGer.QrPrevLcts.Open;
  end;
end;

procedure TFmCondGer.QrPRIBeforeClose(DataSet: TDataSet);
begin
  if FReabrePrevIts then
    FmCondGerImpGer.QrPrevLcts.Close;
end;

procedure TFmCondGer.HabilitaBotoes;
var
  Aberto: Boolean;
begin
  Aberto := QrPrevEncerrado.Value = 0;
  (*
    BtLeitura.Enabled   := MLAGeral.IntToBool_Query(QrPrev);
    BtArrecada.Enabled  := MLAGeral.IntToBool_Query(QrPrev);
    BtPrebol.Enabled    := MLAGeral.IntToBool_Query(QrPrev);
    BtBoleto.Enabled    := MLAGeral.IntToBool_Query(QrPrev);
    BtProvisao.Enabled  := MLAGeral.IntToBool_Query(QrPrev);
  *)
  //
  BtOrcamento.Enabled := MLAGeral.IntToBool_Query(QrPrev);
  BtLeitura.Enabled := False;
  BtArrecada.Enabled := False;
  BtPrebol.Enabled := False;
  BtBoleto.Enabled := False;
  BtProvisao.Enabled := False;
  case PageControl2.ActivePageIndex of
    0:
      begin
        BtOrcamento.Enabled := True;
        BtProvisao.Enabled := Aberto and MLAGeral.IntToBool_Query(QrPrev);
      end;
    1:
      BtLeitura.Enabled := Aberto and MLAGeral.IntToBool_Query(QrPrev);
    2:
      BtArrecada.Enabled := Aberto and MLAGeral.IntToBool_Query(QrPrev);
    3:
      BtPrebol.Enabled := Aberto and MLAGeral.IntToBool_Query(QrPrev);
    4:
      BtBoleto.Enabled := Aberto and MLAGeral.IntToBool_Query(QrPrev);
    5:
      ;
    6:
      ;
    7:
      BtArrecada.Enabled := True;
  end;
end;

procedure TFmCondGer.QrPrevAfterOpen(DataSet: TDataSet);
begin
  HabilitaBotoes;
end;

procedure TFmCondGer.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCondGer.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCondGer.BitBtn5Click(Sender: TObject);
begin
  LocalizarPeriodoAtual;
end;

procedure TFmCondGer.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCondGer.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCondGer.Incluinovooramento1Click(Sender: TObject);
begin
  MostraCondGerNew(stIns);
end;

procedure TFmCondGer.Incluitransfernciaentrecarteiras1Click(Sender: TObject);
begin
  TranferenciaEntreCarteiras(0);
end;

procedure TFmCondGer.Incluitransfernciaentrecontas1Click(Sender: TObject);
begin
  TranferenciaEntreContas(0);
end;

procedure TFmCondGer.BtReabreClick(Sender: TObject);
var
  Carteira: Integer;
begin
  if DmLct2.QrCrt.State = dsInactive then
    Carteira := 0
  else
    Carteira := DmLct2.QrCrtCodigo.Value;
  //
  DmLct2.ReabreCarteiras(Carteira, DmLct2.QrCrt, DmLct2.QrCrtSum,
    'TFmCondGer.BtReabreClick()');
end;

procedure TFmCondGer.BtOrcamentoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMOrcamento, BtOrcamento);
end;

procedure TFmCondGer.Novoitemdeoramento1Click(Sender: TObject);
begin
  UBloquetos_Jan.MostraFmCondGerPrev(stIns, QrPRI, DmCond.FTabPriA,
    QrPrevPERIODO_TXT.Value, QrPrevCodigo.Value);
end;

procedure TFmCondGer.Alteraitemselecionado1Click(Sender: TObject);
begin
  UBloquetos_Jan.MostraFmCondGerPrev(stUpd, QrPRI, DmCond.FTabPriA,
    QrPrevPERIODO_TXT.Value, QrPrevCodigo.Value);
end;

procedure TFmCondGer.MsanterioraoVencimento1Click(Sender: TObject);
begin
  UFinanceiro.ColocarMesDeCompetenciaOndeNaoTem(dcMesAnteriorAoVencto,
    DmLct2.QrCrt, DmLct2.QrLct, DmLct2.FTabLctA);
end;

procedure TFmCondGer.QrPrevAfterScroll(DataSet: TDataSet);
begin
  ReopenPRI(0);
  ReopenArre(0, 0);
  DmCond.ReopenQrCons(0);
  // antes dos boletos
  DmBloq.ReopenPrevModBol(0);
  DmBloq.ReopenBoletos('');
  // Somente ap�s DmBloq.QrBoletos
  BtProtocolo.Enabled := MLAGeral.IntToBool_Query(DmBloq.QrBoletos);
  ReopenResumo();
  //
  QrSumCT.Close;
  QrSumCT.SQL.Clear;
  QrSumCT.SQL.Add('SELECT SUM(Consumo) CONSUMO, SUM(Valor) VALOR');
  QrSumCT.SQL.Add('FROM ' + DmCond.FTabCnsA + ' cni');
  QrSumCT.SQL.Add('WHERE cni.Cond=:P0');
  QrSumCT.SQL.Add('AND cni.Periodo=:P1');
  QrSumCT.Params[00].AsInteger := QrPrevCond.Value;
  QrSumCT.Params[01].AsInteger := QrPrevPeriodo.Value;
  QrSumCT.Open;
  //
end;

procedure TFmCondGer.ReopenPRI(Controle: Integer);
begin
  QrPRI.Close;
  QrPRI.SQL.Clear;
  QrPRI.SQL.Add('SELECT pit.*, con.SubGrupo,');
  QrPRI.SQL.Add('con.Codigo CODCONTA, sgo.Codigo CODSUBGRUPO,');
  QrPRI.SQL.Add('con.Nome NOMECONTA, sgo.Nome NOMESUBGRUPO');
  QrPRI.SQL.Add('FROM ' + DmCond.FTabPriA + ' pit');
  QrPRI.SQL.Add('LEFT JOIN contas con ON con.Codigo=pit.Conta');
  QrPRI.SQL.Add('LEFT JOIN subgrupos sgo ON sgo.Codigo=con.SubGrupo');
  QrPRI.SQL.Add('WHERE pit.Codigo=:p0');
  QrPRI.SQL.Add('ORDER BY NOMESUBGRUPO, NOMECONTA');
  QrPRI.Params[0].AsInteger := QrPrevCodigo.Value;
  QrPRI.Open;
  //
  QrPRI.Locate('Controle', Controle, []);
end;

procedure TFmCondGer.ReopenArre(Apto, Propriet: Integer);
begin
  QrArre.Close;
  QrArre.SQL.Clear;
  QrArre.SQL.Add
    ('SELECT cdi.Andar, ari.Propriet, ari.Apto, SUM(ari.Valor) Valor,');
  QrArre.SQL.Add('cdi.Unidade, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial ELSE');
  QrArre.SQL.Add('ent.Nome END NOMEPROPRIET');
  QrArre.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
  QrArre.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
  QrArre.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=ari.Apto');
  QrArre.SQL.Add('WHERE ari.Codigo=:P0');
  QrArre.SQL.Add('GROUP BY Propriet, Apto');
  QrArre.SQL.Add('ORDER BY cdi.Andar, Unidade, NOMEPROPRIET');
  QrArre.Params[0].AsInteger := QrPrevCodigo.Value;
  QrArre.Open;
  //
  QrArre.Locate('Apto;Propriet', VarArrayOf([Apto, Propriet]), []);
  //
  QrSumARRE.Close;
  QrSumARRE.SQL.Clear;
  QrSumARRE.SQL.Add('SELECT SUM(ari.Valor) Valor');
  QrSumARRE.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
  QrSumARRE.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
  QrSumARRE.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=ari.Apto');
  QrSumARRE.SQL.Add('WHERE ari.Codigo=:P0');
  QrSumARRE.Params[0].AsInteger := QrPrevCodigo.Value;
  QrSumARRE.Open;
end;

procedure TFmCondGer.ReopenArreBol(Boleto: Integer);
begin
  QrArreBol.Close;
  QrArreBol.SQL.Clear;
  QrArreBol.SQL.Add('SELECT ari.Boleto');
  QrArreBol.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
  QrArreBol.SQL.Add('WHERE ari.Codigo=:P0');
  QrArreBol.SQL.Add('AND ari.Apto=:P1');
  QrArreBol.SQL.Add('AND ari.Propriet=:P2');
  QrArreBol.SQL.Add('GROUP BY ari.Propriet, ari.Apto, ari.Boleto');
  QrArreBol.SQL.Add('ORDER BY ari.Boleto');
  QrArreBol.Params[00].AsInteger := QrPrevCodigo.Value;
  QrArreBol.Params[01].AsInteger := QrArreApto.Value;
  QrArreBol.Params[02].AsInteger := QrArrePropriet.Value;
  QrArreBol.Open;
  //
  QrArreBol.Locate('Boleto', Boleto, []);
end;

procedure TFmCondGer.ReopenARI(Controle: Integer);
begin
  QrAri.Close;
  QrAri.SQL.Clear;
  QrAri.SQL.Add('SELECT ari.*');
  QrAri.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
  QrAri.SQL.Add('WHERE ari.Codigo=:P0');
  QrAri.SQL.Add('AND ari.Apto=:P1');
  QrAri.SQL.Add('AND ari.Propriet=:P2');
  QrAri.SQL.Add('AND ari.Boleto=:P3');
  QrAri.SQL.Add('ORDER BY Valor DESC');
  QrAri.Params[00].AsInteger := QrPrevCodigo.Value;
  QrAri.Params[01].AsInteger := QrArreApto.Value;
  QrAri.Params[02].AsInteger := QrArrePropriet.Value;
  QrAri.Params[03].AsFloat := QrArreBolBoleto.Value;
  QrAri.Open;
  //
  QrAri.Locate('Controle', Controle, []);
end;

procedure TFmCondGer.PMProvisaoPopup(Sender: TObject);
var
  Enab, Enab2, Encerr: Boolean;
begin
  Enab := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0);
  Enab2 := (QrPRI.State <> dsInactive) and (QrPRI.RecordCount > 0);
  //
  if Enab then
    Encerr := QrPrevEncerrado.Value <> 0
  else
    Encerr := True;
  //
  Adicinaitensbase1.Enabled := Enab and (not Encerr);
  Adicionaitensdeprovisoagendados1.Enabled := Enab and (not Encerr);
  Novoitemdeoramento1.Enabled := Enab and (not Encerr);
  Alteraitemselecionado1.Enabled := Enab and (not Encerr) and Enab2;
  Excluiitemdeoramentoselecionado1.Enabled := Enab and (not Encerr) and Enab2;
  Agendamentoporvarreduradelanamentos1.Enabled := Enab and (not Encerr);
end;

procedure TFmCondGer.BtProvisaoClick(Sender: TObject);
begin
  MudarPage2(0);
  MyObjects.MostraPopUpDeBotao(PMProvisao, BtProvisao);
end;

procedure TFmCondGer.BtBaseClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMBase, BtBase);
end;

procedure TFmCondGer.PMBasePopup(Sender: TObject);
begin
  Alteraitemselecionado2.Enabled := MLAGeral.IntToBool_Query(QrPrevBaI);
end;

procedure TFmCondGer.Alteraitemselecionado2Click(Sender: TObject);
{ ???  BtBase > PMBase > Alteraitemselecionado2
  var
  Erro, Mostra: Boolean;
}
begin
  { ???
    Erro := False;
    Mostra := True;
    Application.CreateForm(TFmPrevBaC, FmPrevBaC);
    FmPrevBaC.LocCod(0, QrPrevBaICodigo.Value);
    if FmPrevBaC.QrPrevBaCCodigo.Value = QrPrevBaICodigo.Value then
    begin
    if not FmPrevBaC.QrPrevBaI.Locate('Controle', QrPrevBaIControle.Value, []) then
    Erro := True;
    end else Erro := True;
    if Erro then
    begin
    if Geral.MensagemBox('Erro ao localizar item a editar!'+
    Chr(13)+Chr(10)+'Deseja procurar manualmente?', 'Pergunta',
    MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then Mostra := True;
    end else Mostra := True;
    if Mostra then
    begin
    FmPrevBaC.FMostra := 2;
    FmPrevBaC.ShowModal;
    end;
    FmPrevBaC.Destroy;
    if Mostra then
    ReopenQrPrevBaI(QrPrevBaIControle.Value);
  }
end;

procedure TFmCondGer.Incluiitens1Click(Sender: TObject);
begin
  { ??? BtBase > PMBase > Incluiitens1
    DCond.QrNotBAI.Close;
    DCond.QrNotBAI.Params[0].AsInteger := DmCond.QrCondCodigo.Value;
    DCond.QrNotBAI.Open;
    //
    if DCond.QrNotBAI.RecordCount = 0 then
    begin
    Geral.MensagemBox('Este cliente j� est� cadastrado em todas ' +
    'contas bases existentes!', 'Aviso', MB_OK+MB_ICONEXCLAMATION);
    Exit;
    end;
    Screen.Cursor := crHourGlass;
    try
    UCriar.RecriaTempTable('PrevBAN', DModG.QrUpdPID1, False);
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO prevban SET ');
    DModG.QrUpdPID1.SQL.Add('Codigo=:P0');
    //
    ProgressBar3.Visible := True;
    ProgressBar3.Max := DCond.QrNotBAI.RecordCount;
    while not DCond.QrNotBAI.Eof do
    begin
    ProgressBar3.Position := ProgressBar3.Position + 1;
    Update;
    Application.ProcessMessages;
    //
    DModG.QrUpdPID1.Params[00].AsInteger := DCond.QrNotBAICodigo.Value;
    DModG.QrUpdPID1.ExecSQL;
    //
    DCond.QrNotBAI.Next;
    end;
    ProgressBar3.Visible := False;
    Application.CreateForm(TFmPrevBAN, FmPrevBAN);
    FmPrevBAN.FNomeCliente := DmCond.QrCondNOMECLI.Value;
    //FmPrevBAN.FCodiCliente := DmCond.QrCondCliente.Value; ??
    FmPrevBAN.FCodiCliente := DmCond.QrCondCodigo.Value;
    FmPrevBAN.ShowModal;
    FmPrevBAN.Destroy;
    //
    ReopenQrPrevBaI(QrPrevBaIControle.Value);
    finally
    ProgressBar3.Visible := False;
    Screen.Cursor := crDefault;
    end;
  }
end;

procedure TFmCondGer.Acertarcreditopelovalorpago1Click(Sender: TObject);
var
  i: Integer;
  Valor: Double;
begin
  if Geral.MensagemBox('Aten��o CUIDADO!' + sLineBreak + 'Todos itens ' +
    'selecionados ser�o modificados quando houver valor da multa e/ou juros!',
    'Deseja continuar?', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
  begin
    with DBGLct.DataSource.DataSet do
      for i := 0 to DBGLct.SelectedRows.Count - 1 do
      begin
        // GotoBookmark(pointer(DBGLct.SelectedRows.Items[i]));
        GotoBookmark(DBGLct.SelectedRows.Items[i]);
        if (DmLct2.QrLctSit.Value > 1) then
        begin
          Valor := DmLct2.QrLctPagJur.Value + DmLct2.QrLctPagMul.Value;
          if Valor < 0 then
          begin
            // que � igual a DmLct2.QrLctPago.Value;
            Valor := Valor + DmLct2.QrLctCredito.Value;
            UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False,
              ['Credito', 'PagJur', 'PagMul', 'MoraVal', 'MultaVal'],
              ['Controle', 'Sub'], [Valor, 0, 0, 0, 0],
              [DmLct2.QrLctControle.Value, DmLct2.QrLctSub.Value], True, '',
              DmLct2.FTabLctA);
          end;
        end;
      end;
    //
    RecalcSaldoCarteira();
  end;
end;

procedure TFmCondGer.Adicinaitensbase1Click(Sender: TObject);
var
  Pode, Aded, MesI, MesF, MesT, AnoT, a, b, c: Word;
  Adiciona: Integer;
  Texto, LastM: String;
  Last1, Last6: Double;
begin
  Screen.Cursor := crHourGlass;
  Pode := 0;
  Aded := 0;
  //
  UCriar.RecriaTempTable('PrevBAB', DModG.QrUpdPID1, False);
  { Tirei daqui pois estava dando erro!
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO prevbab SET ');
    DModG.QrUpdPID1.SQL.Add('Conta=:P0, PrevBac=:P1, PrevBaI=:P2, Valor=:P3, ');
    DModG.QrUpdPID1.SQL.Add('Texto=:P4, Adiciona=:P5, Last1=:P6, Last6=:P7, ');
    DModG.QrUpdPID1.SQL.Add('NOMECONTA=:P8, LastM=:P9 ');
  }
  //
  DmCond.QrNIO_B.Close;
  DmCond.QrNIO_B.SQL.Clear;
  DmCond.QrNIO_B.SQL.Add('SELECT pbc.Codigo, pbc.Nome, pbc.Conta,');
  DmCond.QrNIO_B.SQL.Add('pbi.Valor, pbi.SitCobr, pbi.Parcelas,');
  DmCond.QrNIO_B.SQL.Add('pbi.ParcPerI, pbi.ParcPerF, pbi.InfoParc,');
  DmCond.QrNIO_B.SQL.Add('pbi.Texto, pbi.Controle, con.Nome NOMECON');
  DmCond.QrNIO_B.SQL.Add('FROM prevbac pbc');
  DmCond.QrNIO_B.SQL.Add('LEFT JOIN prevbai pbi ON pbi.Codigo=pbc.Codigo');
  DmCond.QrNIO_B.SQL.Add('LEFT JOIN contas con ON con.Codigo=pbc.Conta');
  DmCond.QrNIO_B.SQL.Add('WHERE SitCobr in (1,2)');
  DmCond.QrNIO_B.SQL.Add('AND pbi.Cond=:P0');
  DmCond.QrNIO_B.SQL.Add('AND pbc.Codigo NOT IN');
  DmCond.QrNIO_B.SQL.Add('(');
  DmCond.QrNIO_B.SQL.Add('  SELECT Codigo');
  DmCond.QrNIO_B.SQL.Add('  FROM ' + DmCond.FTabPriA);
  DmCond.QrNIO_B.SQL.Add('  WHERE Codigo=:P1');
  DmCond.QrNIO_B.SQL.Add(')');
  DmCond.QrNIO_B.Params[00].AsInteger := DmCond.QrCondCodigo.Value;
  DmCond.QrNIO_B.Params[01].AsInteger := QrPrevCodigo.Value;
  UMyMod.AbreQuery(DmCond.QrNIO_B, Dmod.MyDB);
  //
  ProgressBar2.Visible := True;
  ProgressBar2.Max := DmCond.QrNIO_B.RecordCount;
  while not DmCond.QrNIO_B.Eof do
  begin
    case DmCond.QrNIO_BSitCobr.Value of
      0:
        ; // nada
      1:
        begin
          MesI := DmCond.QrNIO_BParcPerI.Value;
          MesF := DmCond.QrNIO_BParcPerF.Value;
          dmkPF.PeriodoDecode(QrPrevPeriodo.Value, AnoT, MesT);
          //
          if (MesI > 0) and (MesF > 0) and (MesI < 13) and (MesF < 13) then
          begin
            if (MesF > MesI) then
            begin
              if (MesF >= MesT) and (MesI <= MesT) then
                inc(Pode, 1);
            end
            else if (MesF < MesI) then
            begin
              a := MesI;
              b := MesT;
              if MesF >= MesT then
                b := b + 12;
              c := MesF + 12;
              if (c >= b) and (a <= b) then
                inc(Pode, 1);
            end;
          end;
        end;
      2:
        begin
          MesI := DmCond.QrNIO_BParcPerI.Value;
          MesF := DmCond.QrNIO_BParcPerF.Value;
          MesT := QrPrevPeriodo.Value;
          //
          if MesI > MesF then
          begin
            if MesF > MesT then
              MesT := MesT + 12;
            MesF := MesF + 12;
          end;
          if (MesF >= MesT) and (MesI <= MesT) then
            inc(Pode, 1);
        end;
    end;

    /// /////////////////////////////////////////////////////////////////////////

    if Aded < Pode then
    begin
      inc(Aded, 1);
      Adiciona := 1;
      if Trim(DmCond.QrNIO_BTexto.Value) <> '' then
        Texto := DmCond.QrNIO_BTexto.Value
      else
        Texto := DmCond.QrNIO_BNome.Value;
      if DmCond.QrNIO_BInfoParc.Value = 1 then
      begin
        a := 0;
        b := 0;
        c := 0;
        case DmCond.QrNIO_BSitCobr.Value of
          1:
            begin
              dmkPF.PeriodoDecode(QrPrevPeriodo.Value, AnoT, MesT);
              a := DmCond.QrNIO_BParcPerI.Value;
              c := DmCond.QrNIO_BParcPerF.Value;
              if c < a then
                inc(c, 12);
              b := MesT;
              if b < a then
                inc(b, 12);
            end;
          2:
            begin
              a := DmCond.QrNIO_BParcPerI.Value;
              b := QrPrevPeriodo.Value;
              c := DmCond.QrNIO_BParcPerF.Value;
            end;
        end;
        Texto := FormatFloat('00', b - a + 1) + '/' +
          FormatFloat('00', c - a + 1) + ' - ' + Texto;
      end;
      ProgressBar2.Position := ProgressBar2.Position + 1;
      Update;
      Application.ProcessMessages;
      //
      LastM := '';
      Last1 := 0;
      Last6 := 0;
      DmCond.ReopenMU6PM(DmCond.QrCondCliente.Value, QrPrevPeriodo.Value,
        DmCond.QrNIO_BConta.Value);
      if DmCond.QrMU6PM.RecordCount = 0 then
      begin
        DmCond.ReopenMU6MM(DmCond.QrCondCliente.Value, QrPrevPeriodo.Value,
          DmCond.QrNIO_BConta.Value);
        //
        if DmCond.QrMU6MM.RecordCount = 0 then
        begin
          // nada
        end
        else
        begin
          LastM := DmCond.QrMU6MMMES.Value + '*';
          Last1 := DmCond.QrMU6MMSUM_DEB.Value;
          Last6 := DmCond.QrMU6MTSUM_DEB.Value / DmCond.QrMU6MM.RecordCount;
        end;
      end
      else
      begin
        LastM := DmCond.QrMU6PMMES.Value;
        Last1 := DmCond.QrMU6PMSUM_DEB.Value;
        Last6 := DmCond.QrMU6PTSUM_DEB.Value / DmCond.QrMU6PM.RecordCount;
      end;
      // Coloquei aqui para evitar erro!
      DModG.QrUpdPID1.SQL.Clear;
      DModG.QrUpdPID1.SQL.Add('INSERT INTO prevbab SET ');
      DModG.QrUpdPID1.SQL.Add
        ('Conta=:P0, PrevBac=:P1, PrevBaI=:P2, Valor=:P3, ');
      DModG.QrUpdPID1.SQL.Add
        ('Texto=:P4, Adiciona=:P5, Last1=:P6, Last6=:P7, ');
      DModG.QrUpdPID1.SQL.Add('NOMECONTA=:P8, LastM=:P9 ');
      //
      DModG.QrUpdPID1.Params[00].AsInteger := DmCond.QrNIO_BConta.Value;
      DModG.QrUpdPID1.Params[01].AsInteger := DmCond.QrNIO_BCodigo.Value; // BaC
      DModG.QrUpdPID1.Params[02].AsInteger := DmCond.QrNIO_BControle.Value;
      // BaI
      DModG.QrUpdPID1.Params[03].AsFloat := DmCond.QrNIO_BValor.Value;
      DModG.QrUpdPID1.Params[04].AsString := Texto;
      DModG.QrUpdPID1.Params[05].AsInteger := Adiciona;
      DModG.QrUpdPID1.Params[06].AsFloat := Last1;
      DModG.QrUpdPID1.Params[07].AsFloat := Last6;
      DModG.QrUpdPID1.Params[08].AsString := DmCond.QrNIO_BNOMECON.Value;
      DModG.QrUpdPID1.Params[09].AsString := LastM;
      DModG.QrUpdPID1.ExecSQL;
    end;

    /// /////////////////////////////////////////////////////////////////////////

    DmCond.QrNIO_B.Next;
  end;
  ProgressBar2.Visible := False;
  case Pode of
    0:
      Texto := 'N�o foi localizado nenhum item base de or�amento aplic�vel';
    1:
      Texto := 'Foi localizado 1 (um) item base de or�amento aplic�vel';
  else
    Texto := 'Foram localizados ' + IntToStr(Pode) +
      ' itens base de or�amento aplic�veis';
  end;
  Texto := Texto + ' de ' + IntToStr(DmCond.QrNIO_B.RecordCount);
  case DmCond.QrNIO_B.RecordCount of
    0:
      Texto := 'N�o h� itens base de or�amento cadastrados para este cliente!';
    1:
      Texto := Texto + ' pesquisado!';
  else
    Texto := Texto + ' pesquisados!';
  end;
  Screen.Cursor := crDefault;
  Geral.MensagemBox(Texto, 'Mensagem', MB_OK + MB_ICONINFORMATION);
  if Pode > 0 then
  begin
    if DBCheck.CriaFm(TFmPrevBAB, FmPrevBAB, afmoNegarComAviso) then
    begin
      FmPrevBAB.FNomeCliente := DmCond.QrCondNOMECLI.Value;
      FmPrevBAB.FCodiCliente := DmCond.QrCondCliente.Value;
      FmPrevBAB.ShowModal;
      FmPrevBAB.Destroy;
    end;
    //
    ReopenPRI(QrPRIControle.Value);
  end;
  CalculaTotalPRIEReabrePrevEPRI;
  RecalculaArrecadacoes();
end;

procedure TFmCondGer.CalculaTotalPRIEReabrePrevEPRI;
var
  Codigo, Controle, Periodo: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Codigo := QrPrevCodigo.Value;
    Periodo := QrPrevPeriodo.Value;
    Controle := QrPRIControle.Value;
    //
    QrSoma.Close;
    QrSoma.SQL.Clear;
    QrSoma.SQL.Add('SELECT SUM(Valor) Total');
    QrSoma.SQL.Add('FROM ' + DmCond.FTabPriA);
    QrSoma.SQL.Add('WHERE Codigo=:P0');
    QrSoma.Params[0].AsInteger := QrPrevCodigo.Value;
    QrSoma.Open;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + DmCond.FTabPrvA +
      ' SET Gastos=:P0 WHERE Codigo=:P1');
    Dmod.QrUpd.Params[00].AsFloat := QrSomaTotal.Value;
    Dmod.QrUpd.Params[01].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    LocPeriodo(Periodo, Periodo);
    QrPRI.Locate('Controle', Controle, []);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGer.CalculaTotalARIEReabreArreEARI(Apto, Propriet,
  Controle: Integer);
var
  Codigo, Periodo: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    Codigo := QrPrevCodigo.Value;
    Periodo := QrPrevPeriodo.Value;
    //
    QrSoma.Close;
    QrSoma.SQL.Clear;
    QrSoma.SQL.Add('SELECT SUM(Valor) Total');
    QrSoma.SQL.Add('FROM ' + DmCond.FTabPriA);
    QrSoma.SQL.Add('WHERE Codigo=:P0');
    QrSoma.Params[0].AsInteger := QrPrevCodigo.Value;
    QrSoma.Open;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE arre SET Gastos=:P0 WHERE Codigo=:P1');
    Dmod.QrUpd.Params[00].AsFloat := QrSomaTotal.Value;
    Dmod.QrUpd.Params[01].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    LocPeriodo(Periodo, Periodo);
    ReopenArre(Apto, Propriet);
    ReopenARI(Controle);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGer.BtBoletoClick(Sender: TObject);
begin
  MudarPage2(4);
  MyObjects.MostraPopUpDeBotao(PMBoleto, BtBoleto);
end;

function TFmCondGer.ObtemValorQrBoletosIts(Item, Tipo: Integer): String;
begin
  if DmBloq.QrBoletosIts.RecordCount >= Item then
  begin
    if DmBloq.QrBoletosIts.RecNo < Item then
      while DmBloq.QrBoletosIts.RecNo < Item do
        DmBloq.QrBoletosIts.Next;
    if DmBloq.QrBoletosIts.RecNo > Item then
      while DmBloq.QrBoletosIts.RecNo > Item do
        DmBloq.QrBoletosIts.Prior;
    case Tipo of
      0:
        Result := DmBloq.QrBoletosItsTEXTO_IMP.Value;
      1:
        Result := Geral.FFT(DmBloq.QrBoletosItsVALOR.Value, 2, siPositivo);
    else
      Result := '<?>';
    end;
  end
  else
    Result := ' ';
end;

procedure TFmCondGer.odasprovises1Click(Sender: TObject);
begin
  FmCondGerImpGer.ImprimeProvisoesEItens(istTodos, DmCond.FTabLctA);
end;

procedure TFmCondGer.odos1Click(Sender: TObject);
begin
  ImprimeBoleto(istTodos, False);
end;

procedure TFmCondGer.odosItens1Click(Sender: TObject);
begin
  ExcluiItemProvisao(istTodos, True);
end;

(* procedure TFmCondGer.CompensacaoAlturaPontos;
  // Calcula Altura do frxCond -> Page3 -> Reporttitle
  // para manter c�digo de barras sempre na mesma posi��o (altura)
  const
  tG =  26;//400+300; // 15 + 11
  tS =  18;//240+240; // 09 + 09
  tC =  08;//240;     // 09
  tM = 568;//15000;
  //tM = 605;//16000;
  var
  cG, cS, nG, nS: Integer;
  x: Integer;//Double;
  begin
  ProgressBar2.Position := 0;
  ProgressBar2.Visible := True;
  ProgressBar2.Max := QrMov.RecordCount;
  QrMov.First;
  cG := -10000;
  cS := -10000;
  nG := 0;
  nS := 0;
  while not QrMov.Eof do
  begin
  ProgressBar2.Position := ProgressBar2.Position + 1;
  Update;
  if cG <> QrMovGrupo.Value then
  begin
  inc(nG, 1);
  cG := QrMovGrupo.Value;
  end;
  if cS <> QrMovSubGrupo.Value then
  begin
  inc(nS, 1);
  cS := QrMovSubGrupo.Value;
  end;
  QrMov.Next;
  end;
  x := (nG * tG) + (nS * tS) + (QrMov.RecordCount * tC) ;
  if x > tM then x := 0 else x := tM - x;
  FCompensaAltura := Trunc(x);//Trunc(x / VAR_frCM);
  ProgressBar2.Visible := False;
  end; *)

(* procedure TFmCondGer.CompensacaoAlturaMetrico;
  // Calcula Altura do frxCond -> Page3 -> Reporttitle
  // para manter c�digo de barras sempre na mesma posi��o (altura)
  const
  tG =   700;//400+300; // 15 + 11
  tS =   480;//240+240; // 09 + 09
  tC =   210;//210;     // 09
  tM = 15000;//15000;   // 567
  var
  cG, cS, nG, nS: Integer;
  x: Integer;//Double;
  begin
  ProgressBar2.Position := 0;
  ProgressBar2.Visible := True;
  ProgressBar2.Max := QrMov.RecordCount;
  QrMov.First;
  cG := -10000;
  cS := -10000;
  nG := 0;
  nS := 0;
  while not QrMov.Eof do
  begin
  ProgressBar2.Position := ProgressBar2.Position + 1;
  Update;
  if cG <> QrMovGrupo.Value then
  begin
  inc(nG, 1);
  cG := QrMovGrupo.Value;
  end;
  if cS <> QrMovSubGrupo.Value then
  begin
  inc(nS, 1);
  cS := QrMovSubGrupo.Value;
  end;
  QrMov.Next;
  end;
  x := (nG * tG) + (nS * tS) + (QrMov.RecordCount * tC) ;
  if x > tM then x := 0 else x := tM - x;
  FCompensaAltura := Trunc(x/VAR_frCM);
  ProgressBar2.Visible := False;
  end; *)

procedure TFmCondGer.BtLeituraClick(Sender: TObject);
begin
  MudarPage2(1);
  MyObjects.MostraPopUpDeBotao(PMLeitura, BtLeitura);
end;

procedure TFmCondGer.Incluileituras1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCondGerLei1, FmCondGerLei1, afmoNegarComAviso) then
  begin
    FmCondGerLei1.FPeriodo := QrPrevPeriodo.Value;
    FmCondGerLei1.FCond := QrPrevCond.Value;
    FmCondGerLei1.EdPeriodo.Text := dmkPF.MesEAnoDoPeriodo(QrPrevPeriodo.Value);
    FmCondGerLei1.STCli.Caption := DmCond.QrCondNOMECLI.Value;
    //
    FmCondGerLei1.QrCons.Close;
    FmCondGerLei1.QrCons.Params[0].AsInteger := QrPrevCond.Value;
    FmCondGerLei1.QrCons.Open;
    //
    FmCondGerLei1.ShowModal;
    FmCondGerLei1.Destroy;
    //
    DmCond.ReopenQrCons(DmCond.QrConsCodigo.Value);
  end;
end;

procedure TFmCondGer.PageControl2Change(Sender: TObject);
begin
  HabilitaBotoes;
  DmBloq.ReopenBoletos('');
end;

procedure TFmCondGer.QrPrevAfterClose(DataSet: TDataSet);
begin
  HabilitaBotoes;
end;

procedure TFmCondGer.Alteraleituraatual1Click(Sender: TObject);
var
  Lancto: Integer;
  Msg: String;
begin
  Lancto := DmCond.QrCNSLancto.Value;
  Msg := '';
  //
  if (Lancto <> 0) and
    (UBloquetosCond.VerificaSeLctEstaoNaTabLctA(DmCond.FTabLctA, Lancto, QrLoc,
    Dmod.MyDB, Msg) = False) then
  begin
    Geral.MB_Aviso(Msg);
    Exit;
  end;
  Msg := '';
  //
  if (Lancto <> 0) and (not UBloquetosCond.VerificaSeLancamentoEstaPago(Lancto,
    QrLoc, Dmod.MyDB, DmCond.FTabLctA, Msg)) then
  begin
    Geral.MB_Aviso(Msg);
    Exit;
  end;
  if DBCheck.CriaFm(TFmLeiGerEdit, FmLeiGerEdit, afmoNegarComAviso) then
  begin
    FmLeiGerEdit.FControle := DmCond.QrCNSControle.Value;
    FmLeiGerEdit.FLancto := DmCond.QrCNSLancto.Value;
    FmLeiGerEdit.EdLeiAnt.ValueVariant := Geral.FFT(DmCond.QrCNSMedAnt.Value,
      DmCond.QrCNSCasas.Value, siPositivo);
    FmLeiGerEdit.EdLeiAtu.ValueVariant := Geral.FFT(DmCond.QrCNSMedAtu.Value,
      DmCond.QrCNSCasas.Value, siPositivo);
    FmLeiGerEdit.EdPreco.ValueVariant := DmCond.QrCNSPreco.Value;
    FmLeiGerEdit.Label14.Caption := DmCond.QrCNSUnidLei.Value;
    FmLeiGerEdit.Label15.Caption := DmCond.QrCNSUnidImp.Value;
    FmLeiGerEdit.ShowModal;
    FmLeiGerEdit.Destroy;
  end;
end;

procedure TFmCondGer.Alteraperiodoatual1Click(Sender: TObject);
begin
  MostraCondGerNew(stUpd);
end;

procedure TFmCondGer.Alteratransferncia1Click(Sender: TObject);
begin
  TranferenciaEntreContas(1);
  RecalcSaldoCarteira();
  if QrCarts.State = dsBrowse then
    ReopenResumo();
end;

procedure TFmCondGer.Alteratransfernciaentrecarteiras1Click(Sender: TObject);
begin
  TranferenciaEntreCarteiras(1);
end;

procedure TFmCondGer.Alteratransfernciaentrecontas1Click(Sender: TObject);
begin
  TranferenciaEntreContas(1);
end;

procedure TFmCondGer.BtArrecadaClick(Sender: TObject);
begin
  case PageControl2.ActivePageIndex of
    2:
      begin
        MudarPage2(2);
        MyObjects.MostraPopUpDeBotao(PMArrecada, BtArrecada);
      end;
    7:
      MyObjects.MostraPopUpDeBotao(PMArreFut, BtArrecada);
  else
    Geral.MensagemBox('A��o n�o definida!', 'Aviso', MB_OK + MB_ICONWARNING);
  end;
end;

procedure TFmCondGer.Incluiintensbasedearrecadao1Click(Sender: TObject);
begin
  // Deve ser antes por causa da taxa da administra��o de risco
  VerificaArreFut();
  AuxCondGer.IncluiintensbasedearrecadaoNovo(False);
end;

procedure TFmCondGer.QrArreBaICalcFields(DataSet: TDataSet);
var
  Ini, Fim: String;
begin
  QrArreBaINOMESITCOBR.Value := FmPrincipal.RgSitCobr.Items
    [QrArreBaISitCobr.Value];
  case QrArreBaISitCobr.Value of
    1:
      begin
        Ini := dmkPF.VerificaMes(QrArreBaIParcPerI.Value, False);
        Fim := dmkPF.VerificaMes(QrArreBaIParcPerF.Value, False);
      end;
    2:
      begin
        Ini := dmkPF.MesEAnoDoPeriodo(QrArreBaIParcPerI.Value);
        Fim := dmkPF.MesEAnoDoPeriodo(QrArreBaIParcPerF.Value);
      end;
  else
    begin
      Ini := '';
      Fim := '';
    end;
  end;
  QrArreBaIINICIO.Value := Ini;
  QrArreBaIFINAL.Value := Fim;
end;

procedure TFmCondGer.ExcluiItesArrecadacao(Controle, Lancto: Integer;
  Boleto: Double);
var
  QrLocLct: TmySQLQuery;
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add(DELETE_FROM + DmCond.FTabAriA +
    ' WHERE Controle=:P0 AND Boleto = 0');
  Dmod.QrUpd.Params[0].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  if (Lancto <> 0) and (Boleto <> 0) then
  begin
    QrLocLct := TmySQLQuery.Create(Dmod);
    QrLocLct.Database := Dmod.MyDB;
    QrLocLct.SQL.Clear;
    QrLocLct.SQL.Add('SELECT Data, Tipo, Carteira, Sub ');
    QrLocLct.SQL.Add('FROM ' + DmCond.FTabLctA + ' ');
    QrLocLct.SQL.Add('WHERE Controle=:P0');
    QrLocLct.SQL.Add('AND FatID = 600'); // 600 FATID da Arrecada��o
    QrLocLct.Params[0].AsInteger := Lancto;
    QrLocLct.Open;
    if QrLocLct.RecordCount > 0 then
    begin
      UFinanceiro.ExcluiLct_Unico(DmCond.FTabLctA, Dmod.MyDB,
        QrLocLct.FieldByName('Data').AsDateTime, QrLocLct.FieldByName('Tipo')
        .AsInteger, QrLocLct.FieldByName('Carteira').AsInteger, Lancto,
        QrLocLct.FieldByName('Sub').AsInteger, dmkPF.MotivDel_ValidaCodigo(306),
        False, False);
    end;
  end;
end;

procedure TFmCondGer.QrArreAfterScroll(DataSet: TDataSet);
begin
  ReopenArreBol(0);
end;

procedure TFmCondGer.BtExcluiClick(Sender: TObject);
var
  i, k: Integer;
begin
  if DBGLct.SelectedRows.Count > 1 then
  begin
    if Geral.MensagemBox('Confirma a exclus�o dos itens selecionados?',
      'Pergunta', MB_ICONQUESTION + MB_YESNOCANCEL) = ID_YES then
    begin
      with DBGLct.DataSource.DataSet do
      begin
        for i := 0 to DBGLct.SelectedRows.Count - 1 do
        begin
          // GotoBookmark(pointer(DBGLct.SelectedRows.Items[i]));
          GotoBookmark(DBGLct.SelectedRows.Items[i]);
          //
          UFinanceiro.ExcluiItemCarteira(DmLct2.QrLctControle.Value,
            DmLct2.QrLctData.Value, DmLct2.QrLctCarteira.Value,
            DmLct2.QrLctSub.Value, DmLct2.QrLctGenero.Value,
            DmLct2.QrLctCartao.Value, DmLct2.QrLctSit.Value,
            DmLct2.QrLctTipo.Value, 0, DmLct2.QrLctID_Pgto.Value, DmLct2.QrLct,
            DmLct2.QrCrt, False, DmLct2.QrLctCarteira.Value,
            dmkPF.MotivDel_ValidaCodigo(300), DmLct2.FTabLctA, False);
        end;
      end;
      k := UMyMod.ProximoRegistro(DmLct2.QrLct, 'Controle', 0);
      UFinanceiro.RecalcSaldoCarteira(DmLct2.QrCrtCodigo.Value, DmLct2.QrCrt,
        DmLct2.QrLct, False, True);
      DmLct2.QrLct.Locate('Controle', k, []);
    end;
  end
  else
  begin
    UFinanceiro.ExcluiItemCarteira(DmLct2.QrLctControle.Value,
      DmLct2.QrLctData.Value, DmLct2.QrLctCarteira.Value, DmLct2.QrLctSub.Value,
      DmLct2.QrLctGenero.Value, DmLct2.QrLctCartao.Value, DmLct2.QrLctSit.Value,
      DmLct2.QrLctTipo.Value, 0, DmLct2.QrLctID_Pgto.Value, DmLct2.QrLct,
      DmLct2.QrCrt, True, DmLct2.QrLctCarteira.Value,
      dmkPF.MotivDel_ValidaCodigo(300), DmLct2.FTabLctA, False);
  end;
end;

procedure TFmCondGer.BtMenuClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMMenu, BtMenu);
end;

procedure TFmCondGer.Localizalote1Click(Sender: TObject);
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT Controle');
  Dmod.QrAux.SQL.Add('FROM protpakits');
  Dmod.QrAux.SQL.Add('WHERE Conta=:P0');
  Dmod.QrAux.Params[0].AsInteger := DmLct2.QrLctProtocolo.Value;
  Dmod.QrAux.Open;
  //
  FmPrincipal.CadastroDeProtocolos(Dmod.QrAux.FieldByName('Controle')
    .AsInteger);
end;

procedure TFmCondGer.Compensar1Click(Sender: TObject);
begin
  (*
    if DBGLct.SelectedRows.Count < 2 then
    UFinanceiro.QuitacaoDeDocumentos(TDBGrid(DBGLct), DmLct2.QrCrt, DmLct2.QrLct,
    DmLct2.FTabLctA)
    else
    QuitarVariosItens();
  *)
  UFinanceiro.QuitacaoDeDocumentos(TDBGrid(DBGLct), DmLct2.QrCrt, DmLct2.QrLct,
    DmLct2.FTabLctA);
end;

procedure TFmCondGer.ComvaloresporUH1Click(Sender: TObject);
begin
  FmCondGerImpGer.ImprimeProvUHs();
end;

procedure TFmCondGer.Pagamentosexecutados1Click(Sender: TObject);
begin
  Dmod.ReopenControle;
  //
  if DBCheck.CriaFm(TFmContasConfPgto, FmContasConfPgto, afmoNegarComAviso) then
  begin
    with FmContasConfPgto do
    begin
      FFinalidade := lfCondominio;
      FQrLct := DmLct2.QrLct;
      FQrCrt := DmLct2.QrCrt;
      FPercJuroM := Dmod.QrControle.FieldByName('MoraDD').AsFloat;
      FPercMulta := Dmod.QrControle.FieldByName('Multa').AsFloat;
      FSetaVars := nil;
      FAlteraAtehFatID := True;
      FLockCliInt := True;
      FLockForneceI := False;
      FLockAccount := False;
      FLockVendedor := False;
      FCliente := 0;
      FFornecedor := 0;
      FForneceI := 0;
      FAccount := 0;
      FVendedor := 0;
      FIDFinalidade := 2;
      FTabLctA := DmLct2.FTabLctA;
      //
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmCondGer.Pagar1Click(Sender: TObject);
begin
  PagarRolarEmissao();
end;

procedure TFmCondGer.Reverter1Click(Sender: TObject);
begin
  UFinanceiro.DesfazerCompensacao(TDBGrid(DBGLct), DmLct2.QrCrt, DmLct2.QrLct,
    DmLct2.FTabLctA);
end;

procedure TFmCondGer.RGAgrupListaAClick(Sender: TObject);
begin
  DmBloq.ReopenListaA();
end;

procedure TFmCondGer.RGTipoDataClick(Sender: TObject);
begin
  VeSeReabreLct();
end;

procedure TFmCondGer.Localizar2Click(Sender: TObject);
const
  QuemChamou = 0; // ???
begin
  { Alexandria
    UFinanceiro.LocalizarLancamento(TPDataIni, TPDataFim, RGTipoData.ItemIndex,
    DmLct2.QrCrt, DmLct2.QrLct, True, QuemChamou, DmLct2.FTabLctA, DmLct2);
  }
end;

procedure TFmCondGer.Copiar1Click(Sender: TObject);
begin
  InsAlt(tgrDuplica);
end;

procedure TFmCondGer.Recibo1Click(Sender: TObject);
begin
  { Alexandria
    if DmLct2.QrLctCredito.Value > 0 then
    GOTOy.EmiteRecibo(DmLct2.QrLctCliente.Value,
    DmCond.QrCondCliente.Value,
    DmLct2.QrLctCredito.Value, 0, 0,
    IntToStr(DmLct2.QrLctControle.Value) + '-' +
    IntToStr(DmLct2.QrLctSub.Value),
    DmLct2.QrLctDescricao.Value, '', '',
    DmLct2.QrLctData.Value,
    DmLct2.QrLctSit.Value)
    else
    GOTOy.EmiteRecibo(DmCond.QrCondCliente.Value,
    DmLct2.QrLctFornecedor.Value,
    DmLct2.QrLctDebito.Value, 0, 0,
    IntToStr(DmLct2.QrLctControle.Value) + '-' +
    IntToStr(DmLct2.QrLctSub.Value),
    DmLct2.QrLctDescricao.Value, '', '',
    DmLct2.QrLctData.Value,
    DmLct2.QrLctSit.Value);
  }
end;

procedure TFmCondGer.Relatriodearrecadaesselecionveis1Click(Sender: TObject);
begin
  { Alexandria
    if PeriodoNaoDefinido then Exit;
    if DBCheck.CriaFm(TFmCondGerImpArre, FmCondGerImpArre, afmoNegarComAviso) then
    begin
    FmCondGerImpArre.ShowModal;
    FmCondGerImpArre.Destroy;
    end;
  }
end;

procedure TFmCondGer.QrArreBolAfterScroll(DataSet: TDataSet);
begin
  ReopenARI(0);
end;

procedure TFmCondGer.TODOSitensdearrecadao1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCondGerDelArre, FmCondGerDelArre, afmoNegarComAviso) then
  begin
    FmCondGerDelArre.ShowModal;
    FmCondGerDelArre.Destroy;
  end;
end;

procedure TFmCondGer.RecalcSaldoCarteira();
begin
  UFinanceiro.RecalcSaldoCarteira(DmLct2.QrCrtCodigo.Value, DmLct2.QrCrt,
    DmLct2.QrLct, True, True);
end;

procedure TFmCondGer.RecalculaArrecadacoes();
var
  Valor: Double;
begin
  QrMPA.Close;
  QrMPA.SQL.Clear;
  QrMPA.SQL.Add('SELECT DISTINCT abi.Codigo, abi.Percent, ari.Lancto ');
  QrMPA.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
  QrMPA.SQL.Add('LEFT JOIN arrebai abi ON abi.Codigo=ari.ArreBaC');
  QrMPA.SQL.Add('WHERE abi.Fator=1');
  QrMPA.SQL.Add('AND ari.Codigo=:P0');
  QrMPA.Params[00].AsInteger := QrPrevCodigo.Value;
  QrMPA.Open;
  //
  DmCond.QrAptos.Close;
  DmCond.QrAptos.SQL.Clear;
  DmCond.QrAptos.SQL.Add
    ('SELECT cdi.Conta, cdi.Unidade, cdi.Propriet, CASE WHEN ');
  DmCond.QrAptos.SQL.Add
    ('ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEPROPRIET,');
  DmCond.QrAptos.SQL.Add('FracaoIdeal, Moradores');
  DmCond.QrAptos.SQL.Add('FROM condimov cdi');
  DmCond.QrAptos.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cdi.Propriet');
  DmCond.QrAptos.SQL.Add('WHERE cdi.SitImv=1');
  DmCond.QrAptos.SQL.Add('AND cdi.Codigo=:P0');
  DmCond.QrAptos.Params[0].AsInteger := DmCond.QrCondCodigo.Value;
  UMyMod.AbreQuery(DmCond.QrAptos, Dmod.MyDB);
  //
  if (QrMPA.RecordCount > 0) and (DmCond.QrAptos.RecordCount > 0) then
  begin
    if Geral.MensagemBox('Houve altera��es de provis�es, ap�s ' +
      'adi��o de arrecada��es. Deseja atualizar as arrecada��es que s�o ' +
      'calculadas pelo valor das provis�es?' + sLineBreak +
      'ATEN��O: Este procedimento pode demorar alguns minutos!', 'Pergunta',
      MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      try
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('UPDATE ' + DmCond.FTabAriA + ' SET ');
        Dmod.QrUpd.SQL.Add('Valor=:P0 ');
        Dmod.QrUpd.SQL.Add('WHERE Codigo=:Pa AND ArreBaC=:Pb');
        while not QrMPA.Eof do
        begin
          // if DmCond.QrAptos.RecordCount > 0 then
          Valor := Int((QrPrevGastos.Value * QrMPAPercent.Value /
            DmCond.QrAptos.RecordCount) + 0.005) / 100;
          Dmod.QrUpd.Params[00].AsFloat := Valor;
          Dmod.QrUpd.Params[01].AsInteger := QrPrevCodigo.Value;
          Dmod.QrUpd.Params[02].AsInteger := QrMPACodigo.Value;
          Dmod.QrUpd.ExecSQL;
          //
          UMyMod.SQLInsUpd(Dmod.QrUpdU, stUpd, DmCond.FTabLctA, False,
            ['Credito'], ['Controle'], [Valor], [QrMPALancto.Value], True);
          //
          QrMPA.Next;
        end;
        CalculaTotalARIEReabreArreEARI(QrArreApto.Value, QrArrePropriet.Value,
          QrAriControle.Value);
        Screen.Cursor := crDefault;
      finally
        Screen.Cursor := crDefault;
      end;
    end;
  end;
end;

procedure TFmCondGer.Definemodelo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmAptosModBol, FmAptosModBol, afmoNegarComAviso) then
  begin
    FmAptosModBol.FTabela := 'prevmodbol';
    FmAptosModBol.FCodBol := QrPrevCodigo.Value;
    FmAptosModBol.CriaQuery(DmCond.QrCondCodigo.Value);
    FmAptosModBol.ShowModal;
    FmAptosModBol.Destroy;
    //
    DmBloq.ReopenPrevModBol(0);
  end;
end;

procedure TFmCondGer.DefineSaldoResumo;
begin
  FResumo_SaldoAnt := 0;
  FResumo_Receitas := 0;
  FResumo_Despesas := 0;
  FResumo_SaldoMes := 0;
  FResumo_SaldoTrf := 0;
  //
  ReopenResumo();
  //
  QrCarts.First;
  while not QrCarts.Eof do
  begin
    FResumo_SaldoAnt := FResumo_SaldoAnt + QrCartsANTERIOR.Value;
    FResumo_Receitas := FResumo_Receitas + QrCartsRECEITAS.Value;
    FResumo_Despesas := FResumo_Despesas + QrCartsDESPESAS.Value;
    FResumo_SaldoMes := FResumo_SaldoMes + QrCartsSALDOMES.Value;
    FResumo_SaldoTrf := FResumo_SaldoTrf + QrCartsFINAL.Value;
    //
    QrCarts.Next;
  end;
  //
  QrCartN.First;
  while not QrCartN.Eof do
  begin
    FResumo_SaldoAnt := FResumo_SaldoAnt + QrCartNANTERIOR.Value;
    FResumo_Receitas := FResumo_Receitas + QrCartNRECEITAS.Value;
    FResumo_Despesas := FResumo_Despesas + QrCartNDESPESAS.Value;
    FResumo_SaldoMes := FResumo_SaldoMes + QrCartNSALDOMES.Value;
    FResumo_SaldoTrf := FResumo_SaldoTrf + QrCartNFINAL.Value;
    //
    QrCartN.Next;
  end;
end;

procedure TFmCondGer.QrArreBeforeClose(DataSet: TDataSet);
begin
  QrArreBol.Close;
end;

procedure TFmCondGer.QrArreBolBeforeClose(DataSet: TDataSet);
begin
  QrAri.Close;
end;

procedure TFmCondGer.ReopenResumo();
var
  Ini, Fim: String;
  Entidade, CliInt: Integer;
  DtIni, DtEncer, DtMorto: TDateTime;
  Enti_TXT, FldIni, TabLctA, TabLctB, TabLctD, TabLctX: String;
begin
  Ini := Geral.FDT(dmkPF.PrimeiroDiaDoPeriodo_Date(QrPrevPeriodo.Value - 1 +
    DmCond.QrCondPSB.Value), 1);
  Fim := Geral.FDT(dmkPF.UltimoDiaDoPeriodo_Date(QrPrevPeriodo.Value - 1 +
    DmCond.QrCondPSB.Value), 1);
  //
  Entidade := QrPrevCondCli.Value;
  Enti_TXT := FormatFloat('0', Entidade);
  CliInt := QrPrevCond.Value;
  //
  DtIni := Int(dmkPF.PrimeiroDiaDoPeriodo_Date(QrPrevPeriodo.Value - 1 +
    DmCond.QrCondPSB.Value));
  //
  DModG.Def_EM_ABD(TMeuDB, Entidade, CliInt, DtEncer, DtMorto, TabLctA,
    TabLctB, TabLctD);
  TabLctX := UFinanceiro.DefLctTab(DtIni, DtEncer, DtMorto, TabLctA,
    TabLctB, TabLctD);
  FldIni := UFinanceiro.DefLctFldSdoIni(DtIni, DtEncer, DtMorto);
  //
  //
  QrCartsIni.Close;
  QrCartsIni.SQL.Clear;
  (* SELECT SUM(lan.Credito) Credito, SUM(lan.Debito) Debito,
    lan.Carteira
    FROM lan ctos lan
    WHERE lan.Tipo < 2
    /*AND lan.Genero <> -1*/
    AND lan.Data < :P0
    AND lan.Carteira in (
    SELECT Codigo
    FROM carteiras
    WHERE ForneceI=:P1
    AND Tipo < 2)
    GROUP BY lan.Carteira
  *)
  QrCartsIni.SQL.Add
    ('SELECT SUM(lct.Credito) Credito, SUM(lct.Debito) Debito, ');
  QrCartsIni.SQL.Add('lct.Carteira');
  QrCartsIni.SQL.Add('FROM ' + DmCond.FTabLctA + ' lct');
  QrCartsIni.SQL.Add('WHERE lct.Tipo < 2');
  QrCartsIni.SQL.Add('AND lct.Data < "' + Ini + '"');
  QrCartsIni.SQL.Add('AND lct.Carteira in (');
  QrCartsIni.SQL.Add('  SELECT Codigo');
  QrCartsIni.SQL.Add('  FROM carteiras');
  QrCartsIni.SQL.Add('  WHERE ForneceI=' + Enti_TXT);
  QrCartsIni.SQL.Add('  AND Tipo < 2)');
  QrCartsIni.SQL.Add('GROUP BY lct.Carteira');
  QrCartsIni.Open;
  //
  (*
    SELECT SUM(lan.Credito) Credito, SUM(lan.Debito) Debito,
    lan.Carteira
    FROM lan ctos lan
    WHERE lan.Tipo < 2
    /*AND lan.Genero <> -1*/
    AND lan.Data <= :P0
    AND lan.Carteira in (
    SELECT Codigo
    FROM carteiras
    WHERE ForneceI=:P1
    AND Tipo < 2)
    GROUP BY lan.Carteira
  *)
  QrCartsFim.Close;
  QrCartsFim.SQL.Clear;
  QrCartsFim.SQL.Add
    ('SELECT SUM(lct.Credito) Credito, SUM(lct.Debito) Debito,');
  QrCartsFim.SQL.Add('lct.Carteira');
  QrCartsFim.SQL.Add('FROM ' + DmCond.FTabLctA + ' lct');
  QrCartsFim.SQL.Add('WHERE lct.Tipo < 2');
  QrCartsFim.SQL.Add('AND lct.Data <= "' + Fim + '"');
  QrCartsFim.SQL.Add('AND lct.Carteira in (');
  QrCartsFim.SQL.Add('  SELECT Codigo');
  QrCartsFim.SQL.Add('  FROM carteiras');
  QrCartsFim.SQL.Add('  WHERE ForneceI=' + Enti_TXT);
  QrCartsFim.SQL.Add('  AND Tipo < 2)');
  QrCartsFim.SQL.Add('GROUP BY lct.Carteira');
  QrCartsFim.Open;
  //
  QrCartsTrf.Close;
  (*
    SELECT SUM(lan.Credito) CRED, SUM(lan.Debito) DEB,
    SUM(lan.Credito-lan.Debito) Valor, lan.Carteira
    FROM lan ctos lan
    WHERE lan.Genero = -1
    AND (lan.Data BETWEEN :P0 AND :P1)
    AND lan.Carteira in (
    SELECT Codigo
    FROM carteiras
    WHERE ForneceI=:P2
    AND Tipo < 2)
    GROUP BY lan.Carteira
  *)
  QrCartsTrf.SQL.Clear;
  QrCartsTrf.SQL.Add('SELECT SUM(lct.Credito) CRED, SUM(lct.Debito) DEB,');
  QrCartsTrf.SQL.Add('SUM(lct.Credito-lct.Debito) Valor, lct.Carteira');
  QrCartsTrf.SQL.Add('FROM ' + DmCond.FTabLctA + ' lct');
  QrCartsTrf.SQL.Add('WHERE lct.Genero = -1');
  QrCartsTrf.SQL.Add('AND (lct.Data BETWEEN "' + Ini + '" AND "' + Fim + '")');
  QrCartsTrf.SQL.Add('AND lct.Carteira in (');
  QrCartsTrf.SQL.Add('  SELECT Codigo');
  QrCartsTrf.SQL.Add('  FROM carteiras');
  QrCartsTrf.SQL.Add('  WHERE ForneceI=' + Enti_TXT);
  QrCartsTrf.SQL.Add('  AND Tipo < 2)');
  QrCartsTrf.SQL.Add('GROUP BY lct.Carteira');
  QrCartsTrf.Open;
  //
  // Reabrir somente ap�s Ini e Fim para Lookup
  QrCarts.Close;
  QrCarts.SQL.Clear;
  QrCarts.SQL.Add('SELECT car.Codigo Carteira, car.Nome NOMECART, ' + FldIni +
    ' Inicial');
  QrCarts.SQL.Add('FROM carteiras car');
  QrCarts.SQL.Add('WHERE car.ForneceI=:P0');
  QrCarts.SQL.Add('AND car.ForneceN=0');
  QrCarts.SQL.Add('AND car.Tipo < 2');
  QrCarts.Params[0].AsInteger := QrPrevCondCli.Value;
  QrCarts.Open;
  // Carteiras de terceiros (Cobradores)
  QrCartN.Close;
  QrCartN.Params[0].AsInteger := QrPrevCondCli.Value;
  QrCartN.Open;
  //
end;

procedure TFmCondGer.QrCartsCalcFields(DataSet: TDataSet);
begin
  QrCartsANTERIOR.Value := QrCartsInicial.Value + QrCartsANT_CRED.Value -
    QrCartsANT_DEB.Value;
  QrCartsRECEITAS.Value := QrCartsATU_CRED.Value - QrCartsANT_CRED.Value -
    QrCartsTRF_CRED.Value;
  QrCartsDESPESAS.Value := -(QrCartsATU_DEB.Value - QrCartsANT_DEB.Value -
    QrCartsTRF_DEBI.Value);
  QrCartsSALDOMES.Value := QrCartsRECEITAS.Value + QrCartsDESPESAS.Value;
  QrCartsFINAL.Value := QrCartsInicial.Value + QrCartsATU_CRED.Value -
    QrCartsATU_DEB.Value;
end;

procedure TFmCondGer.DoBloquetoatual1Click(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 4;
  //
  AlteraVencimentoPreBol(istAtual, False, TDBGrid(DBGradeS));
end;

procedure TFmCondGer.BloquetosSelecionados1Click(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 4;
  //
  AlteraVencimentoPreBol(istSelecionados, False, TDBGrid(DBGradeS));
end;

procedure TFmCondGer.DeTodosBloquetos1Click(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 4;
  //
  AlteraVencimentoPreBol(istTodos, False, TDBGrid(DBGradeS));
end;

procedure TFmCondGer.AlteraVencimentoPreBol(Selecao: TSelType; PreBol: Boolean;
  Grade: TDBGrid);
var
  Alterou: Boolean;
begin
  Alterou := False;
  //
  case Selecao of
    istAtual:
      Alterou := UBloquetosCond.AlteraVencimentoPreBol(QrPrevPeriodo.Value,
        DmCond.QrCondDiaVencto.Value, PreBol, istAtual, Grade, DmBloq.QrBoletos,
        DmBloq.QrBoletosIts, Dmod.QrUpd, Dmod.QrAux, Dmod.MyDB, ProgressBar3,
        DmCond.FTabLctA);
    istSelecionados:
      Alterou := UBloquetosCond.AlteraVencimentoPreBol(QrPrevPeriodo.Value,
        DmCond.QrCondDiaVencto.Value, PreBol, istSelecionados, Grade,
        DmBloq.QrBoletos, DmBloq.QrBoletosIts, Dmod.QrUpd, Dmod.QrAux,
        Dmod.MyDB, ProgressBar3, DmCond.FTabLctA);
    istTodos:
      Alterou := UBloquetosCond.AlteraVencimentoPreBol(QrPrevPeriodo.Value,
        DmCond.QrCondDiaVencto.Value, PreBol, istTodos, Grade, DmBloq.QrBoletos,
        DmBloq.QrBoletosIts, Dmod.QrUpd, Dmod.QrAux, Dmod.MyDB, ProgressBar3,
        DmCond.FTabLctA);
  end;
  if Alterou then
  begin
    DmBloq.ReopenBoletos(DmBloq.QrBoletosBOLAPTO.Value);
  end;
end;

procedure TFmCondGer.Ambos1Click(Sender: TObject);
begin
  FmCondGerImpGer.ImprimeProvisoes_e_ValorUHs(2);
end;

procedure TFmCondGer.DesfazerBoletos(Tipo: TSelType);
var
  Desfez: Boolean;
begin
  case Tipo of
    istAtual:
      Desfez := UBloquetosCond.DesfazerBoletos(Tipo, Dmod.QrUpd, Dmod.QrAux,
        DmBloq.QrBoletos, DmBloq.QrBoletosIts, Dmod.MyDB, TDBGrid(DBGradeS),
        DmCond.FTabLctA, DmCond.FTabPrvA, DmCond.FTabAriA, DmCond.FTabCnsA,
        DmCond.QrCondCodigo.Value, QrPrevPeriodo.Value, ProgressBar3);
    istSelecionados:
      Desfez := UBloquetosCond.DesfazerBoletos(Tipo, Dmod.QrUpd, Dmod.QrAux,
        DmBloq.QrBoletos, DmBloq.QrBoletosIts, Dmod.MyDB, TDBGrid(DBGradeS),
        DmCond.FTabLctA, DmCond.FTabPrvA, DmCond.FTabAriA, DmCond.FTabCnsA,
        DmCond.QrCondCodigo.Value, QrPrevPeriodo.Value, ProgressBar3);
    istTodos:
      Desfez := UBloquetosCond.DesfazerBoletos(Tipo, Dmod.QrUpd, Dmod.QrAux,
        DmBloq.QrBoletos, DmBloq.QrBoletosIts, Dmod.MyDB, TDBGrid(DBGradeS),
        DmCond.FTabLctA, DmCond.FTabPrvA, DmCond.FTabAriA, DmCond.FTabCnsA,
        DmCond.QrCondCodigo.Value, QrPrevPeriodo.Value, ProgressBar3);
  end;
  if Desfez then
    DmBloq.ReopenBoletos('');
end;

procedure TFmCondGer.PMMenuPopup(Sender: TObject);
begin
  if DmLct2.QrLctGenero.Value = -1 then
  begin
    Alteratransfernciaentrecarteiras1.Enabled := True;
    Excluitransfernciaentrecarteiras1.Enabled := True;
  end
  else
  begin
    Alteratransfernciaentrecarteiras1.Enabled := False;
    Excluitransfernciaentrecarteiras1.Enabled := False;
  end;
  //
  if DmLct2.QrLctFatID.Value = -1 then
  begin
    Alteratransfernciaentrecontas1.Enabled := True;
    Excluitransfernciaentrecontas1.Enabled := True;
  end
  else
  begin
    Alteratransfernciaentrecontas1.Enabled := False;
    Excluitransfernciaentrecontas1.Enabled := False;
  end;
  //
  if DBGLct.SelectedRows.Count > 1 then
  begin
    Carteiras1.Enabled := True;
    Data1.Enabled := True;
    Compensao1.Enabled := True;
    Transformaremitemdebloqueto1.Enabled := True;
    Acertarcreditopelovalorpago1.Enabled := True;
    PagarAVista1.Enabled := True;
    Localizarlanamentoorigem1.Enabled := False;
  end
  else
  begin
    Carteiras1.Enabled := False;
    Data1.Enabled := False;
    Compensao1.Enabled := False;
    Transformaremitemdebloqueto1.Enabled := False;
    Acertarcreditopelovalorpago1.Enabled := False;
    PagarAVista1.Enabled := False;
    Localizarlanamentoorigem1.Enabled := DmLct2.QrLctID_Pgto.Value > 0;
  end;
  //
  ConfiguraPopoupQuitacoes();
end;

procedure TFmCondGer.PMOrcamentoPopup(Sender: TObject);
var
  Enab, Encerr: Boolean;
begin
  Enab := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0);
  //
  if Enab then
    Encerr := QrPrevEncerrado.Value <> 0
  else
    Encerr := True;
  //
  case QrPrevEncerrado.Value of
    0:
      TravadestravaPerodo1.Caption := 'Trava &Per�odo atual';
    1:
      TravadestravaPerodo1.Caption := 'Destrava &Per�odo atual';
  end;
  //
  Alteraperiodoatual1.Enabled := Enab and (not Encerr);
  TravadestravaPerodo1.Enabled := Enab;
end;

procedure TFmCondGer.DBGLctDrawColumnCell(Sender: TObject; const Rect: TRect;
  DataCol: Integer; Column: TColumn; State: TGridDrawState);
begin
  MyObjects.DefineCorTextoSitLancto(TDBGrid(Sender), Rect, Column.FieldName,
    DmLct2.QrLctNOMESIT.Value);
end;

procedure TFmCondGer.DBGLctKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
    InsAlt(tgrAltera)
  else if (Key = VK_DELETE) and (Shift = [ssCtrl]) then
    UFinanceiro.ExcluiItemCarteira(DmLct2.QrLctControle.Value,
      DmLct2.QrLctData.Value, DmLct2.QrLctCarteira.Value, DmLct2.QrLctSub.Value,
      DmLct2.QrLctGenero.Value, DmLct2.QrLctCartao.Value, DmLct2.QrLctSit.Value,
      DmLct2.QrLctTipo.Value, 0, DmLct2.QrLctID_Pgto.Value, DmLct2.QrLct,
      DmLct2.QrCrt, False, DmLct2.QrLctCarteira.Value,
      dmkPF.MotivDel_ValidaCodigo(300), DmLct2.FTabLctA, False)
  else if (Key = VK_F4) and (Shift = [ssCtrl]) then
    MyObjects.MostraPopUpDeBotaoObject(PMQuita, DBGLct, 0, 0)
  else if (Key = VK_F9) then
    UFinanceiro.SomaLinhas_2(TDBGrid(DBGLct), DmLct2.QrCrt, DmLct2.QrLct,
      EdCred, EdDebi, EdSoma);
end;

procedure TFmCondGer.Somenteprovises1Click(Sender: TObject);
begin
  FmCondGerImpGer.ImprimeProvisoes();
end;

procedure TFmCondGer.BtConciliaClick(Sender: TObject);
var
  CartConcilia, Entidade, Condominio (* , CliInt *) : Integer;
  NomeCI: String;
begin
  Condominio := DmCond.QrCondCodigo.Value;
  CartConcilia := DmLct2.QrCrtCodigo.Value;
  Entidade := DmCond.QrCondCliente.Value;
  // CliInt       := DModG.EmpresaAtual_ObtemCodigo(tecCliInt);
  NomeCI := DmCond.QrCondNOMECLI.Value;;
  //
  UFinanceiro.ConciliacaoBancaria(DmLct2.QrCrt, DmLct2.QrLct, TPDataIni, NomeCI,
    Entidade, Condominio, CartConcilia, DmLct2.FTabLctA, DmLct2);
end;

procedure TFmCondGer.BtPrebolClick(Sender: TObject);
begin
  MudarPage2(3);
  case PageControl4.ActivePageIndex of
    0:
      MyObjects.MostraPopUpDeBotao(PMPrebol0, BtPrebol);
    1:
      MyObjects.MostraPopUpDeBotao(PMPreBol1, BtPrebol);
    2:
      MyObjects.MostraPopUpDeBotao(PMPreBol2, BtPrebol);
  else
    Geral.MensagemBox('Nenhum PopUp foi implementado para ' +
      'a orelha selecionada em Pr�-bloquetos!', 'Aviso',
      MB_OK + MB_ICONWARNING);
  end;
end;

procedure TFmCondGer.PageControl2Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if DmBloq.QrBoletosIts.State = dsBrowse then
  begin
    case PageControl2.ActivePageIndex of
      3:
        FBolItsNao := DmBloq.QrBoletosItsTEXTO.Value;
      4:
        FBolItsSim := DmBloq.QrBoletosItsTEXTO.Value;
    end;
  end;
  if DmBloq.QrBoletos.State = dsBrowse then
  begin
    case PageControl2.ActivePageIndex of
      3:
        FBolNao := DmBloq.QrBoletosBOLAPTO.Value;
      4:
        FBolSim := DmBloq.QrBoletosBOLAPTO.Value;
    end;
    case PageControl2.ActivePageIndex of
      3:
        DmBloq.ReopenBoletos(FBolNao);
      4:
        DmBloq.ReopenBoletos(FBolSim);
    end;
  end;
end;

procedure TFmCondGer.Atual2Click(Sender: TObject);
begin
  ImprimeBoleto(istAtual, False);
end;

procedure TFmCondGer.Selecionado1Click(Sender: TObject);
begin
  ImprimeBoleto(istSelecionados, False);
end;

procedure TFmCondGer.Selecionados2Click(Sender: TObject);
begin
  ImprimeBoleto(istSelecionados, False);
end;

procedure TFmCondGer.Todos2Click(Sender: TObject);
begin
  ImprimeBoleto(istTodos, False);
end;

procedure TFmCondGer.PMBoletoPopup(Sender: TObject);
begin
  Alteravencimento1.Enabled := MLAGeral.IntToBool_Query(DmBloq.QrBoletos);
  Desfazerboletos1.Enabled := MLAGeral.IntToBool_Query(DmBloq.QrBoletos);
end;

procedure TFmCondGer.PMImprimePopup(Sender: TObject);
begin
  ImprimebloquetoNovo1.Enabled := MLAGeral.IntToBool_Query(DmBloq.QrBoletos);
  Relatriodearrecadaesselecionveis1.Enabled :=
    (PageControl5.ActivePageIndex = 1) and (PageControl6.ActivePageIndex = 1);
end;

procedure TFmCondGer.PMLeituraPopup(Sender: TObject);
var
  Enab, Enab2, Encerr: Boolean;
begin
  Enab := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0);
  Enab2 := (DmCond.QrCons.State <> dsInactive) and
    (DmCond.QrCons.RecordCount > 0);
  //
  if Enab then
    Encerr := QrPrevEncerrado.Value <> 0
  else
    Encerr := True;
  //
  Incluileituras1.Enabled := Enab and (not Encerr);
  Rateiraconsumo1.Enabled := Enab and (not Encerr);
  Alteraleituraatual1.Enabled := Enab and (not Encerr) and Enab2;
  Excluileituraatual1.Enabled := Enab and (not Encerr) and Enab2;
end;

procedure TFmCondGer.DobloquetoAtual2Click(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 3;
  PageControl4.ActivePageIndex := 0;
  //
  AlteraVencimentoPreBol(istAtual, True, DBGradeN);
end;

procedure TFmCondGer.DosbloquetosSelecionados1Click(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 3;
  PageControl4.ActivePageIndex := 0;
  //
  AlteraVencimentoPreBol(istSelecionados, True, DBGradeN);
end;

procedure TFmCondGer.DeTodosbloquetos2Click(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 3;
  PageControl4.ActivePageIndex := 0;
  //
  AlteraVencimentoPreBol(istTodos, True, DBGradeN);
end;

function TFmCondGer.TodosSelecionadosEstaoSemRestricaoJuridica
  (Tipo: TSelType): Boolean;
begin
  Result := DmBloq.TodosSelecionadosEstaoSemRestricaoJuridica(Tipo, FLoteImp,
    PageControl2, DBGradeN, TDBGrid(DBGradeS));
end;

function TFmCondGer.TodosSelecionadosTemVencimento(Tipo: TSelType): Boolean;
begin
  Result := DmBloq.TodosSelecionadosTemVencimento(Tipo, FLoteImp, PageControl2,
    DBGradeN, TDBGrid(DBGradeS));
end;

procedure TFmCondGer.GeraBoeltosAbertos(Selecao: TSelType; Grade: TDBGrid);
var
  ConsCod: Integer;
  Bolapto: String;
  Gerou: Boolean;
begin
  Gerou := False;
  //
  case Selecao of
    istAtual:
      Gerou := UBloquetosCond.GeraBoletosAbertos(DmCond.QrCondCodigo.Value,
        DmCond.QrCondCliente.Value, QrPrevPeriodo.Value,
        DmCond.QrCondDiaVencto.Value, DmBloq.QrBoletos, DmBloq.QrBoletosIts,
        QrLoc, Dmod.QrUpd, Dmod.MyDB, Grade, istAtual, ProgressBar3,
        DmCond.FTabLctA, DmCond.FTabAriA, DmCond.FTabCnsA);
    istSelecionados:
      Gerou := UBloquetosCond.GeraBoletosAbertos(DmCond.QrCondCodigo.Value,
        DmCond.QrCondCliente.Value, QrPrevPeriodo.Value,
        DmCond.QrCondDiaVencto.Value, DmBloq.QrBoletos, DmBloq.QrBoletosIts,
        QrLoc, Dmod.QrUpd, Dmod.MyDB, Grade, istSelecionados, ProgressBar3,
        DmCond.FTabLctA, DmCond.FTabAriA, DmCond.FTabCnsA);
    istTodos:
      Gerou := UBloquetosCond.GeraBoletosAbertos(DmCond.QrCondCodigo.Value,
        DmCond.QrCondCliente.Value, QrPrevPeriodo.Value,
        DmCond.QrCondDiaVencto.Value, DmBloq.QrBoletos, DmBloq.QrBoletosIts,
        QrLoc, Dmod.QrUpd, Dmod.MyDB, Grade, istTodos, ProgressBar3,
        DmCond.FTabLctA, DmCond.FTabAriA, DmCond.FTabCnsA);
  end;
  if Gerou then
  begin
    ConsCod := DmCond.QrConsCodigo.Value;
    Bolapto := DmBloq.QrBoletosBOLAPTO.Value;
    LocPeriodo(QrPrevPeriodo.Value, QrPrevPeriodo.Value);
    //
    DmCond.ReopenQrCons(ConsCod);
    DmBloq.ReopenBoletos(Bolapto);
    //
    BtProtocolo.Enabled := (DmBloq.QrBoletos.RecordCount > 0);
  end;
end;

procedure TFmCondGer.Gerarabertos1Click(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 4;
  //
  GeraBoeltosAbertos(istTodos, TDBGrid(DBGradeS));
end;

procedure TFmCondGer.GerarAtual1Click(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 3;
  PageControl4.ActivePageIndex := 0;
  //
  GeraBoeltosAbertos(istAtual, DBGradeN);
end;

procedure TFmCondGer.Gerarselecionados1Click(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 3;
  PageControl4.ActivePageIndex := 0;
  //
  GeraBoeltosAbertos(istSelecionados, DBGradeN);
end;

procedure TFmCondGer.Gerartodosabertos1Click(Sender: TObject);
begin
  PageControl2.ActivePageIndex := 3;
  PageControl4.ActivePageIndex := 0;
  //
  GeraBoeltosAbertos(istTodos, DBGradeN);
end;

procedure TFmCondGer.GerenciaoarquivoCNAB1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGeraCNAB, FmGeraCNAB, afmoNegarComAviso) then
  begin
    with FmGeraCNAB do
    begin
      TPDataV.Date := DmBloq.QrBoletosItsVencto.Value;
      TPDataD.Date := DmBloq.QrBoletosItsVencto.Value;
      TPDataC.Date := DmBloq.QrBoletosItsVencto.Value;
      //
      FTabAriA := DmCond.FTabAriA;
      FTabCnsA := DmCond.FTabCnsA;
    end;
    FmGeraCNAB.ShowModal;
    FmGeraCNAB.Destroy;
  end;
end;

procedure TFmCondGer.BtCNABClick(Sender: TObject);
begin
  // MyObjects.MostraPopUpDeBotao(PMCNABs, BtCNAB);
  FmPrincipal.RetornoCNAB;
end;

procedure TFmCondGer.BtCNAB_VaiClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCNAB_Vai, BtCNAB_Vai);
end;

procedure TFmCondGer.SbImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, SbImprime);
end;

procedure TFmCondGer.BitBtn4Click(Sender: TObject);
begin
  LocalizarPeriodoAtual;
end;

procedure TFmCondGer.LocalizarPeriodoAtual;
var
  Localizar: Boolean;
  Periodo: Integer;
begin
  GBCntrl.Visible := False;
  PageControl1.ActivePageIndex := 3;
  //
  if DBCheck.CriaFm(TFmCondGerLocper, FmCondGerLocper, afmoNegarComAviso) then
  begin
    FmCondGerLocper.FLocalizar := False;
    //
    FmCondGerLocper.QrPrev.Close;
    FmCondGerLocper.QrPrev.SQL.Clear;
    FmCondGerLocper.QrPrev.SQL.Add('SELECT Periodo');
    FmCondGerLocper.QrPrev.SQL.Add('FROM ' + DmCond.FTabPrvA);
    FmCondGerLocper.QrPrev.SQL.Add('WHERE Cond=:P0');
    FmCondGerLocper.QrPrev.SQL.Add('ORDER BY Periodo DESC');
    FmCondGerLocper.QrPrev.Params[0].AsInteger := DmCond.QrCondCodigo.Value;
    FmCondGerLocper.QrPrev.Open;
    //
    FmCondGerLocper.ShowModal;
    //
    Localizar := FmCondGerLocper.FLocalizar;
    Periodo := FmCondGerLocper.FPeriodo;
    //
    FmCondGerLocper.Destroy;
    Application.ProcessMessages;
    //
    if Localizar then
      LocPeriodo(Periodo, Periodo);
  end;
end;

procedure TFmCondGer.Localizatransfernciaentrecarteiras1Click(Sender: TObject);
begin
  TranferenciaEntreCarteiras(3);
end;

procedure TFmCondGer.BtCopiaCHClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCopiaCH, BtCopiaCH);
end;

procedure TFmCondGer.Atual3Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 3;
  PageControl2.ActivePageIndex := 4;
  //
  DesfazerBoletos(istAtual);
end;

procedure TFmCondGer.Atual4Click(Sender: TObject);
begin
  UnProtocolo.ProtocolosCD_Lct(istAtual, TDBGrid(DBGLct), DmLct2,
    DmCond.QrCondCliente.Value, FmCondGer.Name, DmCond.FTabLctA);
end;

procedure TFmCondGer.Selecionados3Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 3;
  PageControl2.ActivePageIndex := 4;
  //
  DesfazerBoletos(istSelecionados);
end;

procedure TFmCondGer.Todos3Click(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 3;
  PageControl2.ActivePageIndex := 4;
  //
  DesfazerBoletos(istTodos);
end;

procedure TFmCondGer.DBGPrevItsDblClick(Sender: TObject);
var
  ValorTxt: String;
  Controle: Integer;
begin
  if (QrPRI.State <> dsInactive) and (QrPRI.RecordCount > 0) then
  begin
    ValorTxt := FormatFloat('0.00', QrPRIValor.Value);
    if InputQuery('Obten��o de valor', 'Informe o novo valor:', ValorTxt) then
    begin
      Controle := QrPRIControle.Value;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE ' + DmCond.FTabPriA +
        ' SET Valor=:P0 WHERE Controle=:P1');
      Dmod.QrUpd.Params[00].AsFloat := Geral.DMV(ValorTxt);
      Dmod.QrUpd.Params[01].AsInteger := Controle;
      Dmod.QrUpd.ExecSQL;
      //
      FmCondGer.CalculaTotalPRIEReabrePrevEPRI;
      ReopenPRI(Controle);
    end;
  end;
end;

procedure TFmCondGer.AdicionaaoarquivoCNAB1Click(Sender: TObject);
var
  i: Integer;
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO cnab_1');
  DModG.QrUpdPID1.SQL.Add('SET Boleto=:P0, ');
  DModG.QrUpdPID1.SQL.Add('Instante=:P1');
  // DModG.QrUpdPID1.SQL.Add('');
  //
  with DBGradeS.DataSource.DataSet do
    for i := 0 to DBGradeS.SelectedRows.Count - 1 do
    begin
      // GotoBookmark(pointer(DBGradeS.SelectedRows.Items[i]));
      GotoBookmark(DBGradeS.SelectedRows.Items[i]);
      //
      DModG.QrUpdPID1.Params[00].AsFloat := DmBloq.QrBoletosBoleto.Value;
      DModG.QrUpdPID1.Params[01].AsFloat := Now();
      DModG.QrUpdPID1.ExecSQL;
    end;
end;

procedure TFmCondGer.LimpadadosarquivoCNAB1Click(Sender: TObject);
begin
  UCriar.RecriaTempTable('CNAB_1', DModG.QrUpdPID1, False);
end;

procedure TFmCondGer.Carteiras1Click(Sender: TObject);
begin
  UFinanceiro.MudaCarteiraLancamentosSelecionados(DmLct2.QrCrt, DmLct2.QrLct,
    TDBGrid(DBGLct), DmLct2.FTabLctA, FThisEntidade);
end;

procedure TFmCondGer.Data1Click(Sender: TObject);
begin
  UFinanceiro.MudaDataLancamentosSelecionados(cdData, DmLct2.QrCrt,
    DmLct2.QrLct, TDBGrid(DBGLct), DmLct2.FTabLctA);
end;

procedure TFmCondGer.Datalancto1Click(Sender: TObject);
begin
  UFinanceiro.ColocarMesDeCompetenciaOndeNaoTem(dcData, DmLct2.QrCrt,
    DmLct2.QrLct, DmLct2.FTabLctA);
end;

procedure TFmCondGer.DbEdCodCondChange(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
    [LaAviso1, LaAviso2], ' ' + Geral.FFN(DmCond.QrCondCodigo.Value, 3) + ' - '
    + DmCond.QrCondNOMECLI.Value, False, taLeftJustify, 2, 10, 20);
end;

procedure TFmCondGer.Compensao1Click(Sender: TObject);
begin
  UFinanceiro.MudaDataLancamentosSelecionados(cdCompensado, DmLct2.QrCrt,
    DmLct2.QrLct, TDBGrid(DBGLct), DmLct2.FTabLctA);
end;

procedure TFmCondGer.MudarPage2(Index: Integer);
begin
end;

procedure TFmCondGer.PMPrebol0Popup(Sender: TObject);
var
  Enab, Enab2, Encerr: Boolean;
begin
  Enab := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0);
  Enab2 := (DmBloq.QrBoletos.State <> dsInactive) and
    (DmBloq.QrBoletos.RecordCount > 0);
  //
  if Enab then
    Encerr := QrPrevEncerrado.Value <> 0
  else
    Encerr := True;
  //
  Imprimir2.Enabled := Enab and (not Encerr) and Enab2;
  Bloqueto1.Enabled := Enab and (not Encerr) and Enab2;
  AlteraVencimento2.Enabled := Enab and (not Encerr) and Enab2;
  Excluir2.Enabled := Enab and (not Encerr) and Enab2;
  Adicionarabloqueto1.Visible := False;
  // Desmarcado para evitar erros entre boletos e financeiro (29/07/2014)
end;

procedure TFmCondGer.PMPreBol1Popup(Sender: TObject);
var
  Enab, Enab2, Encerr: Boolean;
begin
  Enab := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0);
  Enab2 := (DmBloq.QrComposA.State <> dsInactive) and
    (DmBloq.QrComposA.RecordCount > 0);
  //
  if Enab then
    Encerr := QrPrevEncerrado.Value <> 0
  else
    Encerr := True;
  //
  Excluiarrecadaodaunidadeatual1.Enabled := Enab and (not Encerr) and Enab2;
  Excluiarrecadaesdasunidadesselecionadas1.Enabled := Enab and (not Encerr)
    and Enab2;
  ExcluiestaarrecadaodeTODASunidades1.Enabled := Enab and (not Encerr)
    and Enab2;
end;

procedure TFmCondGer.PMPreBol2Popup(Sender: TObject);
var
  Enab, Enab2, Encerr: Boolean;
begin
  Enab := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0);
  Enab2 := (DmBloq.QrComposL.State <> dsInactive) and
    (DmBloq.QrComposL.RecordCount > 0);
  //
  if Enab then
    Encerr := QrPrevEncerrado.Value <> 0
  else
    Encerr := True;
  //
  ExcluileituradaunidadeATUAL1.Enabled := Enab and (not Encerr) and Enab2;
  ExcluileiturasdasUNIDADESselecionadas1.Enabled := Enab and (not Encerr)
    and Enab2;
  ExcluileiturasdeTODASunidades1.Enabled := Enab and (not Encerr) and Enab2;
end;

procedure TFmCondGer.QrProprietCalcFields(DataSet: TDataSet);
begin
  QrProprietNUMERO_TXT.Value := MLAGeral.FormataNumeroDeRua(QrProprietRUA.Value,
    Trunc(QrProprietNUMERO.Value), False);
  QrProprietLNR.Value := QrProprietNOMELOGRAD.Value;
  if Trim(QrProprietLNR.Value) <> '' then
    QrProprietLNR.Value := QrProprietLNR.Value + ' ';
  QrProprietLNR.Value := QrProprietLNR.Value + QrProprietRUA.Value;
  if Trim(QrProprietRUA.Value) <> '' then
    QrProprietLNR.Value := QrProprietLNR.Value + ', ' +
      QrProprietNUMERO_TXT.Value;
  if Trim(QrProprietCOMPL.Value) <> '' then
    QrProprietLNR.Value := QrProprietLNR.Value + ' ' + QrProprietCOMPL.Value;
  if Trim(QrProprietBAIRRO.Value) <> '' then
    QrProprietLNR.Value := QrProprietLNR.Value + ' - ' + QrProprietBAIRRO.Value;
  //
  QrProprietLN2.Value := '';
  if Trim(QrProprietCIDADE.Value) <> '' then
    QrProprietLN2.Value := QrProprietLN2.Value + QrProprietCIDADE.Value;
  QrProprietLN2.Value := QrProprietLN2.Value + ' - ' + QrProprietNOMEUF.Value;
  if QrProprietCEP.Value > 0 then
    QrProprietLN2.Value := QrProprietLN2.Value + '     CEP ' +
      Geral.FormataCEP_NT(QrProprietCEP.Value);
  //
  { QrProprietCUC.Value := QrProprietLNR.Value+ '   ' +QrProprietLN2.Value;
    //
    QrProprietCEP_TXT.Value := Geral.FormataCEP_NT(QrProprietCEP.Value); }
  //
end;

procedure TFmCondGer.Excluileituraatual2Click(Sender: TObject);
begin
  ExcluiLeitura(istAtual);
end;

procedure TFmCondGer.Excluileiturasselecionadas1Click(Sender: TObject);
begin
  ExcluiLeitura(istSelecionados);
end;

procedure TFmCondGer.Excluitodasleituras1Click(Sender: TObject);
begin
  ExcluiLeitura(istTodos);
end;

procedure TFmCondGer.Excluitransferncia1Click(Sender: TObject);
begin
  TranferenciaEntreContas(2);
  RecalcSaldoCarteira();
  if QrCarts.State = dsBrowse then
    ReopenResumo();
end;

procedure TFmCondGer.Excluitransfernciaentrecarteiras1Click(Sender: TObject);
begin
  TranferenciaEntreCarteiras(2);
end;

procedure TFmCondGer.Excluitransfernciaentrecontas1Click(Sender: TObject);
begin
  TranferenciaEntreContas(2);
end;

procedure TFmCondGer.ExcluiLeituraAtual(Controle, Lancto: Integer;
  Boleto: Double);
var
  QrLocLct: TmySQLQuery;
begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add(DELETE_FROM + DmCond.FTabCnsA +
    ' WHERE Controle=:P0 AND Boleto=0');
  Dmod.QrUpd.Params[0].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
  if (Lancto <> 0) and (Boleto = 0) then
  begin
    QrLocLct := TmySQLQuery.Create(Dmod);
    QrLocLct.Database := Dmod.MyDB;
    QrLocLct.SQL.Clear;
    QrLocLct.SQL.Add('SELECT Data, Tipo, Carteira, Sub ');
    QrLocLct.SQL.Add('FROM ' + DmCond.FTabLctA + ' ');
    QrLocLct.SQL.Add('WHERE Controle=:P0');
    QrLocLct.SQL.Add('AND FatID = 601'); // 601 FATID do Consumo
    QrLocLct.Params[0].AsInteger := Lancto;
    QrLocLct.Open;
    if QrLocLct.RecordCount > 0 then
    begin
      UFinanceiro.ExcluiLct_Unico(DmCond.FTabLctA, Dmod.MyDB,
        QrLocLct.FieldByName('Data').AsDateTime, QrLocLct.FieldByName('Tipo')
        .AsInteger, QrLocLct.FieldByName('Carteira').AsInteger,
        DmCond.QrCNSLancto.Value, QrLocLct.FieldByName('Sub').AsInteger,
        dmkPF.MotivDel_ValidaCodigo(307), False, False);
    end;
  end;
end;

procedure TFmCondGer.ExcluiLeitura(Tipo: TSelType);
var
  i, Prox: Integer;
begin
  i := 0;
  if Tipo <> istTodos then
  begin
    case Tipo of
      istAtual:
        i := 1;
      istSelecionados:
        begin
          i := DBGCNS.SelectedRows.Count;
          if i = 0 then
            i := 1;
        end;
    end;
    if i = 1 then
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o da leitura selecionada?' +
        sLineBreak +
        'ATEN��O: Leituras que j� possuam n�mero de boleto n�o ser�o exclu�das!')
        = ID_YES then
      begin
        ExcluiLeituraAtual(DmCond.QrCNSControle.Value, DmCond.QrCNSLancto.Value,
          DmCond.QrCNSBoleto.Value);
      end;
    end
    else
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o das ' +
        Geral.FF0(DBGCNS.SelectedRows.Count) + ' leituras selecionadas?' +
        sLineBreak +
        'ATEN��O: Leituras que j� possuam n�mero de boleto n�o ser�o exclu�das!')
        = ID_YES then
      begin
        with DBGCNS.DataSource.DataSet do
        begin
          for i := 0 to DBGCNS.SelectedRows.Count - 1 do
          begin
            // GotoBookmark(pointer(DBGCNS.SelectedRows.Items[i]));
            GotoBookmark(DBGCNS.SelectedRows.Items[i]);
            //
            ExcluiLeituraAtual(DmCond.QrCNSControle.Value,
              DmCond.QrCNSLancto.Value, DmCond.QrCNSBoleto.Value);
          end;
        end;
      end;
    end;
  end
  else
  begin
    DmCond.QrCNS.First;
    while not DmCond.QrCNS.Eof do
    begin
      ExcluiLeituraAtual(DmCond.QrCNSControle.Value, DmCond.QrCNSLancto.Value,
        DmCond.QrCNSBoleto.Value);
      //
      DmCond.QrCNS.Next;
    end;
  end;
  Prox := UMyMod.ProximoRegistro(DmCond.QrCNS, 'Controle', 0);
  DmCond.ReopenCNS(Prox);
end;

procedure TFmCondGer.ItemAtual2Click(Sender: TObject);
begin
  ExcluiItemProvisao(istAtual, True);
end;

procedure TFmCondGer.Itematual3Click(Sender: TObject);
var
  Prox: Integer;
begin
  if Geral.MB_Pergunta('Confirma a exclus�o do item "' + QrAriTexto.Value + '"?'
    + sLineBreak +
    'ATEN��O: Arrecada��es que j� possuam n�mero de boleto n�o ser�o exclu�das!')
    = ID_YES then
  begin
    ExcluiItesArrecadacao(QrAriControle.Value, QrAriLancto.Value,
      QrAriBoleto.Value);
    //
    Prox := UMyMod.ProximoRegistro(QrAri, 'Controle', QrAriControle.Value);
    //
    CalculaTotalARIEReabreArreEARI(QrAriApto.Value, QrAriPropriet.Value, Prox);
  end;
end;

procedure TFmCondGer.ItemAtualdoprbloqueto1Click(Sender: TObject);
begin
  ExcluiItemPreBloqueto(istAtual);
end;

procedure TFmCondGer.OsitensSelecionadosdoprbloquetoselecionado1Click
  (Sender: TObject);
begin
  ExcluiItemPreBloqueto(istSelecionados);
end;

procedure TFmCondGer.Todositensdoprbloquetoatual1Click(Sender: TObject);
begin
  ExcluiItemPreBloqueto(istTodos);
end;

procedure TFmCondGer.ExcluiItemPreBloqueto(Tipo: TSelType);
  procedure ExcluiLeituraAtual;
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(DELETE_FROM + DmCond.FTabCnsA + ' WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := DmBloq.QrBoletosItsControle.Value;
    Dmod.QrUpd.ExecSQL;
  end;
  procedure ExcluiArrecadacaoAtual;
  begin
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(DELETE_FROM + DmCond.FTabAriA + ' WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := DmBloq.QrBoletosItsControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
  end;

var
  i, Prox: Integer;
  Bolapto: String;
begin
  i := 0;
  if Tipo <> istTodos then
  begin
    case Tipo of
      istAtual:
        i := 1;
      istSelecionados:
        begin
          i := DBGrid13.SelectedRows.Count;
          if i = 0 then
            i := 1;
        end;
    end;
    if i = 1 then
    begin
      if Geral.MensagemBox
        ('Confirma a exclus�o do item de pr�-bloqueto selecionado?', 'Pergunta',
        MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
      begin
        if DmBloq.QrBoletosItsTipo.Value = 0 then
          ExcluiArrecadacaoAtual
        else
          ExcluiLeituraAtual;
      end;
    end
    else
    begin
      if Geral.MensagemBox('Confirma a exclus�o dos ' +
        IntToStr(DBGrid13.SelectedRows.Count) +
        ' itens de pr�-bloquetos selecionados?', 'Pergunta',
        MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
      begin
        with DBGrid13.DataSource.DataSet do
          for i := 0 to DBGrid13.SelectedRows.Count - 1 do
          begin
            // GotoBookmark(pointer(DBGrid13.SelectedRows.Items[i]));
            GotoBookmark(DBGrid13.SelectedRows.Items[i]);
            if DmBloq.QrBoletosItsTipo.Value = 0 then
              ExcluiArrecadacaoAtual
            else
              ExcluiLeituraAtual;
          end;
      end;
    end;
  end
  else
  begin
    DmBloq.QrBoletosIts.First;
    while not DmBloq.QrBoletosIts.Eof do
    begin
      if DmBloq.QrBoletosItsTipo.Value = 0 then
        ExcluiArrecadacaoAtual
      else
        ExcluiLeituraAtual;
      DmBloq.QrBoletosIts.Next;
    end;
  end;
  Bolapto := DmBloq.QrBoletosBOLAPTO.Value;
  Prox := 0;
  CalculaTotalARIEReabreArreEARI(QrArreApto.Value, QrArrePropriet.Value, Prox);
  DmBloq.ReopenBoletos(Bolapto);
end;

procedure TFmCondGer.Excluiarrecadaodaunidadeatual1Click(Sender: TObject);
begin
  ExcluiArrecadacaoPorTipo(istAtual);
end;

procedure TFmCondGer.Excluiarrecadaesdasunidadesselecionadas1Click
  (Sender: TObject);
begin
  ExcluiArrecadacaoPorTipo(istSelecionados);
end;

procedure TFmCondGer.ExcluiestaarrecadaodeTODASunidades1Click(Sender: TObject);
begin
  ExcluiArrecadacaoPorTipo(istTodos);
end;

procedure TFmCondGer.Excluiitemns1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, DmBloq.QrPrevModBol, DBGrid21,
    'prevmodbol', ['Codigo', 'Apto'], ['Codigo', 'Apto'], istPergunta, '');
end;

procedure TFmCondGer.ExcluiArrecadacaoPorTipo(Quais: TSelType);
var
  i, Prox, ComposAConta, ComposAArreBaI: Integer;
  Bolapto: String;
begin
  i := 0;
  if Quais <> istTodos then
  begin
    case Quais of
      istAtual:
        i := 1;
      istSelecionados:
        begin
          i := DBGrid14.SelectedRows.Count;
          if i = 0 then
            i := 1;
        end;
    end;
    if i = 1 then
    begin
      if Geral.MB_Pergunta
        ('Confirma a exclus�o da arrecada��o da unidade selecionada?' +
        sLineBreak +
        'ATEN��O: Arrecada��es que j� possuam n�mero de boleto n�o ser�o exclu�das!')
        = ID_YES then
      begin
        ExcluiItesArrecadacao(DmBloq.QrComposAItsControle.Value,
          DmBloq.QrComposAItsLancto.Value, DmBloq.QrComposAItsBoleto.Value);
      end;
    end
    else
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o dos ' +
        Geral.FF0(DBGrid14.SelectedRows.Count) +
        ' itens de pr�-bloquetos selecionados?' + sLineBreak +
        'ATEN��O: Arrecada��es que j� possuam n�mero de boleto n�o ser�o exclu�das!')
        = ID_YES then
      begin
        with DBGrid14.DataSource.DataSet do
          for i := 0 to DBGrid14.SelectedRows.Count - 1 do
          begin
            // GotoBookmark(pointer(DBGrid14.SelectedRows.Items[i]));
            GotoBookmark(DBGrid14.SelectedRows.Items[i]);
            //
            ExcluiItesArrecadacao(DmBloq.QrComposAItsControle.Value,
              DmBloq.QrComposAItsLancto.Value, DmBloq.QrComposAItsBoleto.Value);
          end;
      end;
    end;
  end
  else
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o de todas ' + 'arrecada��es de "' +
      DmBloq.QrComposATEXTO.Value + '"?' + sLineBreak +
      'ATEN��O: Arrecada��es que j� possuam n�mero de boleto n�o ser�o exclu�das!')
      = ID_YES then
    begin
      DmBloq.QrComposAIts.First;
      while not DmBloq.QrComposAIts.Eof do
      begin
        ExcluiItesArrecadacao(DmBloq.QrComposAItsControle.Value,
          DmBloq.QrComposAItsLancto.Value, DmBloq.QrComposAItsBoleto.Value);
        //
        DmBloq.QrComposAIts.Next;
      end;
    end;
  end;
  ComposAConta := DmBloq.QrComposAConta.Value;
  ComposAArreBaI := DmBloq.QrComposAArreBaI.Value;
  //
  Bolapto := DmBloq.QrBoletosBOLAPTO.Value;
  Prox := UMyMod.ProximoRegistro(DmBloq.QrComposAIts, 'Controle', 0);
  //
  CalculaTotalARIEReabreArreEARI(QrArreApto.Value, QrArrePropriet.Value, Prox);
  //
  ReopenArre(0, 0);
  //
  DmBloq.ReopenBoletos(Bolapto);
  //
  if (DmBloq.QrComposA.State <> dsInactive) and
    (DmBloq.QrComposA.RecordCount > 0) then
    DmBloq.QrComposA.Locate('Conta;ArreBaI',
      VarArrayOf([ComposAConta, ComposAArreBaI]), []);
  if (DmBloq.QrComposAIts.State <> dsInactive) and
    (DmBloq.QrComposAIts.RecordCount > 0) then
    DmBloq.QrComposAIts.Locate('Controle', Prox, []);
end;

procedure TFmCondGer.ExcluileituradaunidadeATUAL1Click(Sender: TObject);
begin
  ExcluiLeituraPorTipo(istAtual);
end;

procedure TFmCondGer.ExcluileiturasdasUNIDADESselecionadas1Click
  (Sender: TObject);
begin
  ExcluiLeituraPorTipo(istSelecionados);
end;

procedure TFmCondGer.ExcluileiturasdeTODASunidades1Click(Sender: TObject);
begin
  ExcluiLeituraPorTipo(istTodos);
end;

procedure TFmCondGer.ExcluiLeituraPorTipo(Quais: TSelType);
var
  i, Prox, ComposLCodigo: Integer;
  Bolapto: String;
begin
  i := 0;
  if Quais <> istTodos then
  begin
    case Quais of
      istAtual:
        i := 1;
      istSelecionados:
        begin
          i := DBGrid15.SelectedRows.Count;
          if i = 0 then
            i := 1;
        end;
    end;
    if i = 1 then
    begin
      if Geral.MB_Pergunta
        ('Confirma a exclus�o da leitura da unidade selecionada?' + sLineBreak +
        'ATEN��O: Leituras que j� possuam n�mero de boleto n�o ser�o exclu�das!')
        = ID_YES then
      begin
        ExcluiLeituraAtual(DmBloq.QrComposLItsControle.Value,
          DmBloq.QrComposLItsLancto.Value, DmBloq.QrComposLItsBoleto.Value);
      end;
    end
    else
    begin
      if Geral.MB_Pergunta('Confirma a exclus�o das ' +
        Geral.FF0(DBGrid15.SelectedRows.Count) +
        ' leituras de unidades selecionadas?' + sLineBreak +
        'ATEN��O: Leituras que j� possuam n�mero de boleto n�o ser�o exclu�das!')
        = ID_YES then
      begin
        with DBGrid15.DataSource.DataSet do
          for i := 0 to DBGrid15.SelectedRows.Count - 1 do
          begin
            // GotoBookmark(pointer(DBGrid15.SelectedRows.Items[i]));
            GotoBookmark(DBGrid15.SelectedRows.Items[i]);
            //
            ExcluiLeituraAtual(DmBloq.QrComposLItsControle.Value,
              DmBloq.QrComposLItsLancto.Value, DmBloq.QrComposLItsBoleto.Value);
          end;
      end;
    end;
  end
  else
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o de todas leituras de "' +
      DmBloq.QrComposLNome.Value + '"?' + sLineBreak +
      'ATEN��O: Leituras que j� possuam n�mero de boleto n�o ser�o exclu�das!')
      = ID_YES then
    begin
      DmBloq.QrComposLIts.First;
      while not DmBloq.QrComposLIts.Eof do
      begin
        ExcluiLeituraAtual(DmBloq.QrComposLItsControle.Value,
          DmBloq.QrComposLItsLancto.Value, DmBloq.QrComposLItsBoleto.Value);
        //
        DmBloq.QrComposLIts.Next;
      end;
    end;
  end;
  ComposLCodigo := DmBloq.QrComposLCodigo.Value;
  //
  Bolapto := DmBloq.QrBoletosBOLAPTO.Value;
  Prox := UMyMod.ProximoRegistro(DmBloq.QrComposLIts, 'Controle', 0);
  //
  DmCond.ReopenQrCons(0);
  DmBloq.ReopenBoletos(Bolapto);
  //
  if (DmBloq.QrComposL.State <> dsInactive) and
    (DmBloq.QrComposL.RecordCount > 0) then
    DmBloq.QrComposL.Locate('Codigo', ComposLCodigo, []);
  if (DmBloq.QrComposLIts.State <> dsInactive) and
    (DmBloq.QrComposLIts.RecordCount > 0) then
    DmBloq.QrComposLIts.Locate('Controle', Prox, []);
end;

procedure TFmCondGer.VerificaCarteirasCliente();
var
  Carteira: Integer;
begin
  if DmLct2.QrCrt.State = dsInactive then
    Carteira := 0
  else
    Carteira := DmLct2.QrCrtCodigo.Value;
  //
  if (DmLct2.QrCrt.State = dsInactive)
  (* or (DmLct2.QrCrt.Params[0].AsInteger <> DmCond.QrCondCliente.Value) *) then
    DmLct2.ReabreCarteiras( (* DmCond.QrCondCliente.Value, *) Carteira,
      DmLct2.QrCrt, DmLct2.QrCrtSum, 'TFmCondGer.VerificaCarteirasCliente()');
end;

procedure TFmCondGer.VeSeReabreLct();
begin
  DmLct2.FTipoData := RGTipoData.ItemIndex;
  //
  DmLct2.VeSeReabreLct(TPDataIni, TPDataFim, CBUH.KeyValue,
    DmLct2.QrLctCarteira.Value, DmLct2.QrLctControle.Value,
    DmLct2.QrLctSub.Value, DmLct2.QrCrt, DmLct2.QrLct);
end;

procedure TFmCondGer.VerificaBotoes();
var
  Ativo: Boolean;
begin
  if DmLct2.QrCrt.State = dsBrowse then
  begin
    if DmLct2.QrCrt.RecordCount > 0 then
      Ativo := True
    else
      Ativo := False;
  end
  else
    Ativo := False;
  BtAltera.Enabled := Ativo;
  BtExclui.Enabled := Ativo;
  BtDuplica.Enabled := Ativo and (DModG.QrCtrlGeralDuplicLct.Value = 1);
  BtPagtoDuvida.Enabled := Ativo;
  BtContarDinheiro.Enabled := Ativo;
  BtRefresh.Enabled := Ativo;
  BtProtoCD.Enabled := Ativo;
  //
end;

procedure TFmCondGer.Itemselecionado1Click(Sender: TObject);
begin
  MostraFmCondGerArreUni(stUpd);
end;

procedure TFmCondGer.nicoimvel1Click(Sender: TObject);
begin
  MostraFmCondGerArreUni(stIns);
end;

procedure TFmCondGer.Mltiplosimveis1Click(Sender: TObject);
begin
  MostraFmCondGerArreMul(stIns);
end;

procedure TFmCondGer.MostraFmCondGerArreUni(SQLType: TSQLType);
begin
  if DBCheck.CriaFm(TFmCondGerArreUni, FmCondGerArreUni, afmoNegarComAviso) then
  begin
    FmCondGerArreUni.ImgTipo.SQLType := SQLType;
    FmCondGerArreUni.FPrevPeriodo := QrPrevPERIODO_TXT.Value;
    FmCondGerArreUni.FCodigo := QrPrevCodigo.Value;
    FmCondGerArreUni.FCond := QrPrevCond.Value;
    FmCondGerArreUni.FCliente := DmCond.QrCondCliente.Value;
    //
    FmCondGerArreUni.QrAptos.Close;
    FmCondGerArreUni.QrAptos.Params[0].AsInteger := QrPrevCond.Value;
    FmCondGerArreUni.QrAptos.Open;
    //
    if SQLType = stUpd then
    begin
      // Criado em 27/07/2013 para evitar erros nos boletos e lan�amentos
      FmCondGerArreUni.FApto := QrAriApto.Value;
      FmCondGerArreUni.FPropriet := QrAriPropriet.Value;
      FmCondGerArreUni.FControle := QrAriControle.Value;
      FmCondGerArreUni.FLancto := QrAriLancto.Value;
      //
      FmCondGerArreUni.EdApto.ValueVariant := QrAriApto.Value;
      FmCondGerArreUni.CBApto.KeyValue := QrAriApto.Value;
      FmCondGerArreUni.EdCNAB_Cfg.ValueVariant := QrAriCNAB_Cfg.Value;
      FmCondGerArreUni.CBCNAB_Cfg.KeyValue := QrAriCNAB_Cfg.Value;
      FmCondGerArreUni.EdConta.ValueVariant := QrAriConta.Value;
      FmCondGerArreUni.CBConta.KeyValue := QrAriConta.Value;
      FmCondGerArreUni.EdValor.ValueVariant := QrAriValor.Value;
      FmCondGerArreUni.EdDescricao.ValueVariant := QrAriTexto.Value;
      //
      FmCondGerArreUni.EdApto.Enabled := False;
      FmCondGerArreUni.CBApto.Enabled := False;
      FmCondGerArreUni.CkContinuar.Visible := False;
      FmCondGerArreUni.CkContinuar.Checked := False;
    end
    else
    begin
      FmCondGerArreUni.FApto := 0;
      FmCondGerArreUni.FPropriet := 0;
      FmCondGerArreUni.FControle := 0;
      FmCondGerArreUni.FLancto := 0;
      //
      FmCondGerArreUni.EdApto.Enabled := True;
      FmCondGerArreUni.CBApto.Enabled := True;
      FmCondGerArreUni.CkContinuar.Visible := True;
      FmCondGerArreUni.CkContinuar.Checked := True;
    end;
    FmCondGerArreUni.ShowModal;
    FmCondGerArreUni.Destroy;
  end;
end;

procedure TFmCondGer.MostraFmCondGerArreMul(SQLType: TSQLType);
begin
  if SQLType <> stIns then
  begin
    Geral.MB_Aviso('A a��o selecionada n�o foi implementada!');
    Exit;
  end;
  try
    Screen.Cursor := crHourGlass;
    //
    // FSdoOldNew :=
    UCriar.RecriaTempTable('UnidCond', DModG.QrUpdPID1, False);
    //
    DmCond.QrAptos.Close;
    DmCond.QrAptos.SQL.Clear;
    // De todos  e selecionados
    DmCond.QrAptos.SQL.Add
      ('SELECT cdi.Conta, cdi.Unidade, cdi.Propriet, CASE WHEN ');
    DmCond.QrAptos.SQL.Add
      ('ent.Tipo=0 THEN ent.RazaoSocial ELSE ent.Nome END NOMEPROPRIET, ');
    DmCond.QrAptos.SQL.Add('FracaoIdeal, Moradores');
    DmCond.QrAptos.SQL.Add('FROM condimov cdi');
    DmCond.QrAptos.SQL.Add
      ('LEFT JOIN entidades ent ON ent.Codigo=cdi.Propriet');
    DmCond.QrAptos.SQL.Add('WHERE cdi.Codigo=:P0');
    DmCond.QrAptos.Params[0].AsInteger := DmCond.QrCondCodigo.Value;

    UMyMod.AbreQuery(DmCond.QrAptos, Dmod.MyDB);
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO unidcond SET ');
    DModG.QrUpdPID1.SQL.Add
      ('Apto=:P0, Unidade=:P1, Proprie=:P2, Selecio=:P3, ');
    DModG.QrUpdPID1.SQL.Add('Entidad=:P4');
    while not DmCond.QrAptos.Eof do
    begin
      DModG.QrUpdPID1.Params[00].AsInteger := DmCond.QrAptosConta.Value;
      DModG.QrUpdPID1.Params[01].AsString := DmCond.QrAptosUnidade.Value;
      DModG.QrUpdPID1.Params[02].AsString := DmCond.QrAptosNOMEPROPRIET.Value;
      DModG.QrUpdPID1.Params[03].AsInteger := 0;
      DModG.QrUpdPID1.Params[04].AsInteger := DmCond.QrAptosPropriet.Value;
      DModG.QrUpdPID1.ExecSQL;
      //
      DmCond.QrAptos.Next;
    end;
  finally
    Screen.Cursor := crDefault;
    //
    if DBCheck.CriaFm(TFmCondGerArreMul, FmCondGerArreMul, afmoNegarComAviso)
    then
    begin
      FmCondGerArreMul.ImgTipo.SQLType := stIns;
      FmCondGerArreMul.FCodigo := QrPrevCodigo.Value;
      FmCondGerArreMul.FApto := QrArreApto.Value;
      FmCondGerArreMul.FPropriet := QrArrePropriet.Value;
      FmCondGerArreMul.FCodiCliente := DmCond.QrCondCliente.Value;
      FmCondGerArreMul.FCond := DmCond.QrCondCodigo.Value;
      FmCondGerArreMul.ShowModal;
      FmCondGerArreMul.Destroy;
    end;
  end;
end;

procedure TFmCondGer.ItemSelecionado3Click(Sender: TObject);
begin
  ExcluiItemProvisao(istAtual, False);
end;

procedure TFmCondGer.ExcluiItemProvisao(Tipo: TSelType; Reagenda: Boolean);
  procedure ExcluiProvisaoAtual(Reagenda: Boolean);
  begin
    if Reagenda then
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'prevbai', False, ['prevcod'],
        ['Controle'], [0], [QrPRIPrevBaI.Value], True);
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add(DELETE_FROM + DmCond.FTabPriA + ' WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrPRIControle.Value;
    Dmod.QrUpd.ExecSQL;
  end;

var
  i, Prox: Integer;
begin
  i := 0;
  if Tipo <> istTodos then
  begin
    case Tipo of
      istAtual:
        i := 1;
      istSelecionados:
        begin
          i := DBGPrevIts.SelectedRows.Count;
          if i = 0 then
            i := 1;
        end;
    end;
    if i = 1 then
    begin
      if Geral.MensagemBox
        ('Confirma a exclus�o do item de provis�o selecionado?', 'Pergunta',
        MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
        ExcluiProvisaoAtual(Reagenda);
    end
    else
    begin
      if Geral.MensagemBox('Confirma a exclus�o dos ' +
        IntToStr(DBGPrevIts.SelectedRows.Count) +
        ' itens de provis�o selecionados?', 'Pergunta',
        MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
      begin
        with DBGPrevIts.DataSource.DataSet do
          for i := 0 to DBGPrevIts.SelectedRows.Count - 1 do
          begin
            // GotoBookmark(pointer(DBGPrevIts.SelectedRows.Items[i]));
            GotoBookmark(DBGPrevIts.SelectedRows.Items[i]);
            ExcluiProvisaoAtual(Reagenda);
          end;
      end;
    end;
  end
  else
  begin
    if Geral.MensagemBox
      ('Confirma a exclus�o de TODOS ITENS de provis�o deste per�odo?',
      'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
    begin
      QrPRI.First;
      while not QrPRI.Eof do
      begin
        ExcluiProvisaoAtual(Reagenda);
        QrPRI.Next;
      end;
    end;
  end;
  Prox := UMyMod.ProximoRegistro(QrPRI, 'Controle', QrPRIControle.Value);
  CalculaTotalPRIEReabrePrevEPRI;
  ReopenPRI(Prox);
  RecalculaArrecadacoes;
end;

procedure TFmCondGer.Excluiitemselecionado1Click(Sender: TObject);
begin
  // BtBase > PMBase > Excluiitemselecionado1
end;

procedure TFmCondGer.ItensSelecionados1Click(Sender: TObject);
begin
  ExcluiItemProvisao(istSelecionados, False);
end;

procedure TFmCondGer.ItensSelecionados2Click(Sender: TObject);
begin
  ExcluiItemProvisao(istSelecionados, True);
end;

procedure TFmCondGer.Todositens1Click(Sender: TObject);
begin
  ExcluiItemProvisao(istTodos, False);
end;

procedure TFmCondGer.SpeedButton7Click(Sender: TObject);
begin
  if Trim(EdURL.Text) <> '' then
    WebBrowser1.Navigate(Trim(EdURL.Text));
end;

procedure TFmCondGer.SbLocUltDtaClick(Sender: TObject);
begin
  BtDesfazOrdenacaoClick(Self);
  DmLct2.LocalizaUltimoLanctoDia(TPLoc.Date, DmLct2.QrCrtCodigo.Value);
end;

procedure TFmCondGer.SbLocBloqClick(Sender: TObject);
begin
  if not DmLct2.QrLct.Locate('FatNum', EdBloqueto.ValueVariant, []) then
    Geral.MB_Aviso('Nenhum lan�amento localizado!' + sLineBreak +
      'Verifique se a carteira selecionada � a correta!');
end;

procedure TFmCondGer.QrBancosAfterScroll(DataSet: TDataSet);
begin
  if PageControl1.ActivePageIndex = 5 then
  begin
    Timer3.Enabled := False;
    Timer3.Enabled := True;
  end;
end;

procedure TFmCondGer.Timer3Timer(Sender: TObject);
begin
  Timer3.Enabled := False;
  EdURL.Text := QrBancosSite.Value;
  SpeedButton7Click(Self);
end;

procedure TFmCondGer.EdURLExit(Sender: TObject);
begin
  SpeedButton7Click(Self);
end;

procedure TFmCondGer.PagarAVista1Click(Sender: TObject);
begin
  UFinanceiro.PagarAVistaEmCaixa(DmLct2.QrCrt, DmLct2.QrLct, TDBGrid(DBGLct),
    DmLct2.FTabLctA);
end;

procedure TFmCondGer.Compensar2Click(Sender: TObject);
begin
  UFinanceiro.QuitacaoDeDocumentos(TDBGrid(DBGLct), DmLct2.QrCrt, DmLct2.QrLct,
    DmLct2.FTabLctA);
end;

procedure TFmCondGer.Reverter2Click(Sender: TObject);
begin
  UFinanceiro.DesfazerCompensacao(TDBGrid(DBGLct), DmLct2.QrCrt, DmLct2.QrLct,
    DmLct2.FTabLctA);
end;

procedure TFmCondGer.Pagar2Click(Sender: TObject);
begin
  PagarRolarEmissao();
end;

procedure TFmCondGer.PagarAVista2Click(Sender: TObject);
begin
  UFinanceiro.PagarAVistaEmCaixa(DmLct2.QrCrt, DmLct2.QrLct, TDBGrid(DBGLct),
    DmLct2.FTabLctA);
end;

procedure TFmCondGer.PagarRolarEmissao;
var
  Lancto: Integer;
begin
  UFinanceiro.PagarRolarEmissao(DmLct2.QrCrt, DmLct2.QrLct, DmLct2.FTabLctA,
    False, Lancto);
  //
  if Lancto <> 0 then
    UFinanceiro.LocalizarlanctoCliInt(Lancto, 0, FThisEntidade, TPDataIni,
      DmLct2.QrLct, DmLct2.QrCrt, DmLct2.QrCrtSum, True, DmCond.FTabLctA,
      DmCond.FTabLctB, DmCond.FTabLctD);
end;

procedure TFmCondGer.BtQuitaClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMQuita, BtQuita);
end;

procedure TFmCondGer.PMQuitaPopup(Sender: TObject);
begin
  ConfiguraPopoupQuitacoes();
end;

procedure TFmCondGer.PMTrfCtaPopup(Sender: TObject);
begin
  Alteratransferncia1.Enabled := DmLct2.QrLctFatID.Value = -1;
  Excluitransferncia1.Enabled := DmLct2.QrLctFatID.Value = -1;
end;

procedure TFmCondGer.QuitarVariosItens();
  procedure InsereEmLctoEdit();
  begin
    DModG.QrUpdPID1.Params[00].AsInteger := DmLct2.QrLctControle.Value;
    DModG.QrUpdPID1.Params[01].AsInteger := DmLct2.QrLctSub.Value;
    DModG.QrUpdPID1.Params[02].AsString := DmLct2.QrLctDescricao.Value;
    DModG.QrUpdPID1.Params[03].AsString := Geral.FDT(DmLct2.QrLctData.Value, 1);
    DModG.QrUpdPID1.Params[04].AsString :=
      Geral.FDT(DmLct2.QrLctVencimento.Value, 1);
    DModG.QrUpdPID1.Params[05].AsFloat := DmLct2.QrLctCredito.Value -
      DmLct2.QrLctDebito.Value;
    //
    DModG.QrUpdPID1.ExecSQL;
  end;

var
  i: Integer;
begin
  UCriar.RecriaTempTable('lctoedit', DModG.QrUpdPID1, False);
  //
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('INSERT INTO lctoedit SET ');
  DModG.QrUpdPID1.SQL.Add('Controle=:P0, Sub=:P1, Descricao=:P2, Data=:P3, ');
  DModG.QrUpdPID1.SQL.Add('Vencimento=:P4, ValorOri=:P5');
  DModG.QrUpdPID1.SQL.Add('');
  if DBGLct.SelectedRows.Count > 1 then
  begin
    with DBGLct.DataSource.DataSet do
      for i := 0 to DBGLct.SelectedRows.Count - 1 do
      begin
        // GotoBookmark(pointer(DBGLct.SelectedRows.Items[i]));
        GotoBookmark(DBGLct.SelectedRows.Items[i]);
        InsereEmLctoEdit();
      end;

  end
  else
    InsereEmLctoEdit();
  if DBCheck.CriaFm(TFmLctPgVarios, FmLctPgVarios, afmoNegarComAviso) then
  begin
    FmLctPgVarios.FTabLctA := DmLct2.FTabLctA;
    FmLctPgVarios.FQrCrt := DmLct2.QrCrt;
    FmLctPgVarios.FQrLct := DmLct2.QrLct;
    //
    FmLctPgVarios.EdMulta.Text := Geral.FFT(DmLct2.QrLctMulta.Value, 6,
      siPositivo);
    FmLctPgVarios.EdTaxaM.Text := Geral.FFT(DmLct2.QrLctMoraDia.Value, 6,
      siPositivo);
    FmLctPgVarios.ShowModal;
    FmLctPgVarios.Destroy;
  end;
end;

procedure TFmCondGer.ShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
  begin
    StatusBar.SimplePanel := True;
    StatusBar.SimpleText := Application.Hint;
  end
  else
    StatusBar.SimplePanel := False;
end;

procedure TFmCondGer.Novatransferncia1Click(Sender: TObject);
begin
  TranferenciaEntreContas(0);
  RecalcSaldoCarteira();
  if QrCarts.State = dsBrowse then
    ReopenResumo();
end;

procedure TFmCondGer.Novo1Click(Sender: TObject);
begin
  FmPrincipal.RetornoCNAB;
end;

procedure TFmCondGer.BtFisicoClick(Sender: TObject);
begin
  FmPrincipal.MostraLctFisico(DmCond.QrCondCliente.Value,
    DmCond.QrCondCodigo.Value, DmLct2.QrCrtCodigo.Value);
end;

procedure TFmCondGer.BtFluxoCxaClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmFluxoCxa, FmFluxoCxa, afmoNegarComAviso) then
  begin
    FmFluxoCxa.FEntidade := DmCond.QrCondCliente.Value;
    FmFluxoCxa.FEntidade_TXT := FormatFloat('0', DmCond.QrCondCliente.Value);
    FmFluxoCxa.FEmpresa := DmCond.QrCondCodigo.Value;
    DModG.Def_EM_ABD(TMeuDB, FmFluxoCxa.FEntidade, FmFluxoCxa.FEmpresa,
      FmFluxoCxa.FDtEncer, FmFluxoCxa.FDtMorto, FmFluxoCxa.FTabLctA,
      FmFluxoCxa.FTabLctB, FmFluxoCxa.FTabLctD);
    FmFluxoCxa.ShowModal;
    FmFluxoCxa.Destroy;
  end;
end;

procedure TFmCondGer.BtAtzCartsClick(Sender: TObject);
begin
  if (DmLct2.QrCrt.State = dsBrowse) or (DmLct2.QrCrt.RecordCount = 0) then
    MyObjects.MostraPopUpDeBotao(PMAtzCarts, BtAtzCarts)
  else
    Geral.MensagemBox('N�o h� carteira selecionada!', 'Aviso',
      MB_OK + MB_ICONWARNING);
  // Precisa?
end;

procedure TFmCondGer.Acarteiraselecionada1Click(Sender: TObject);
begin
  AtualizaCarteiraAtual();
end;

procedure TFmCondGer.Todascarteiras1Click(Sender: TObject);
begin
  UFinanceiro.AtualizaTodasCarteiras(DmLct2.QrCrt, DmLct2.QrLct,
    DmLct2.FTabLctA);
end;

procedure TFmCondGer.BtAutomClick(Sender: TObject);
const
  DefJuros = 0.00;
  DefMulta = 0.00;
var
  i, Controle: Integer;
  Jur, Mul: Double;
  DataDef, DataSel: TDateTime;
begin
  Controle := 0;
  //
  DataDef := Date;
  // MLAGeral.UltimoDiaDoPeriodo_Date(QrPrevPeriodo.Value) + DmCond.QrCondDiaVencto.Value;
  //
  if not DBCheck.ObtemData_Juros_Multa(VAR_DATA_MINIMA, DataDef, DataSel,
    DefJuros, DefMulta, Jur, Mul) then
    Exit;
  //
  if DBGLct.SelectedRows.Count > 1 then
  begin
    if Geral.MensagemBox('Confirma a quita��o autom�tica ' +
      'dos itens selecionados?', 'Pergunta', MB_ICONQUESTION + MB_YESNOCANCEL) = ID_YES
    then
    begin
      with DBGLct.DataSource.DataSet do
        for i := 0 to DBGLct.SelectedRows.Count - 1 do
        begin
          // GotoBookmark(pointer(DBGLct.SelectedRows.Items[i]));
          GotoBookmark(DBGLct.SelectedRows.Items[i]);
          //
          QuitaItemBoletoAutomatico(DataSel, Mul, Jur);
          Controle := DmLct2.QrLctControle.Value;
        end;
    end;
  end
  else
    QuitaItemBoletoAutomatico(DataSel, Mul, Jur);
  //
  UFinanceiro.RecalcSaldoCarteira(DmLct2.QrCrtCodigo.Value, DmLct2.QrCrt,
    DmLct2.QrLct, False, True);
  UFinanceiro.RecalcSaldoCarteira(DmLct2.QrCrtBanco.Value, nil, nil,
    False, False);
  //
  DmLct2.QrLct.Locate('Controle', Controle, []);
end;

procedure TFmCondGer.QuitaItemBoletoAutomatico(Data: TDateTime;
  Mul, Jur: Double);
var
  Controle2: Integer;
  Credito, Debito, Valor, Multa, Juros: Double;
  Msg, Compensado: String;
begin
  if UFinanceiro.VerificaSeLanctoEParcelamento(DmLct2.QrLct, Msg) then
  begin
    Geral.MB_Aviso(Msg);
    Exit;
  end;
  //
  if Data < VAR_DATA_MINIMA then
  begin
    Geral.MensagemBox('O lan�amento n� ' + IntToStr(DmLct2.QrLctControle.Value)
      + ' n�o pode ser quitado pois a data de quita��o pertence a um m�s encerrado!',
      'Aviso', MB_OK + MB_ICONWARNING);
    Exit;
  end;
  //
  Controle2 := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    DmLct2.FTabLctA, LAN_CTOS, 'Controle');
  //
  if DmLct2.QrLctSit.Value > 0 then
  begin
    Geral.MensagemBox('O lan�amento n� ' + IntToStr(DmLct2.QrLctControle.Value)
      + ' j� possui pagamento parcial ou total, portanto n�o ser� quitado!',
      'Aviso', MB_OK + MB_ICONWARNING);
    Exit;
  end;
  Compensado := FormatDateTime(VAR_FORMATDATE, Data);
  //
  if Data - DmLct2.QrLctVencimento.Value > 0 then
  begin
    Juros := Jur / 30 * (Data - DmLct2.QrLctVencimento.Value);
    Valor := DmLct2.QrLctCredito.Value - DmLct2.QrLctDebito.Value;
    if Valor < 0 then
    begin
      Valor := -Valor;
      Multa := Mul * Valor / 100;
      Juros := Juros * Valor / 100;
      Credito := DmLct2.QrLctCredito.Value;
      Debito := DmLct2.QrLctDebito.Value + Multa + Juros;
    end
    else
    begin
      Multa := Mul * Valor / 100;
      Juros := Juros * Valor / 100;
      Credito := DmLct2.QrLctCredito.Value + Multa + Juros;
      Debito := DmLct2.QrLctDebito.Value;
    end;
  end
  else
  begin
    Juros := 0;
    Multa := 0;
    Credito := DmLct2.QrLctCredito.Value;
    Debito := DmLct2.QrLctDebito.Value;
  end;
  UFinanceiro.LancamentoDefaultVARS;
  FLAN_Data := Geral.FDT(Data, 1);
  FLAN_Vencimento := Geral.FDT(DmLct2.QrLctVencimento.Value, 1);
  FLAN_DataCad := Geral.FDT(Date, 1);
  FLAN_Mez := DmLct2.QrLctMez.Value;
  FLAN_Descricao := DmLct2.QrLctDescricao.Value;
  FLAN_Compensado := Geral.FDT(Data, 1);
  FLAN_Duplicata := DmLct2.QrLct FLAN_Duplicata := DmLct2.QrLctDuplicata.Value;
  FLAN_Doc2 := DmLct2.QrLctDoc2.Value;
  FLAN_SerieCH := DmLct2.QrLctSerieCH.Value;

  FLAN_Documento := Trunc(DmLct2.QrLctDocumento.Value);
  FLAN_Tipo := 1;
  FLAN_Carteira := DmLct2.QrCrtBanco.Value;
  FLAN_Credito := Credito;
  FLAN_Debito := Debito;
  FLAN_Genero := DmLct2.QrLctGenero.Value;
  FLAN_NotaFiscal := DmLct2.QrLctNotaFiscal.Value;
  FLAN_Sit := 3;
  FLAN_Cartao := 0;
  FLAN_Linha := 0;
  FLAN_Fornecedor := DmLct2.QrLctFornecedor.Value;
  FLAN_Cliente := DmLct2.QrLctCliente.Value;
  FLAN_MoraDia := Jur;
  FLAN_Multa := Mul;
  // FLAN_UserCad    := VAR_USUARIO;
  // FLAN_DataDoc    := Geral.FDT(Date, 1);
  FLAN_Vendedor := DmLct2.QrLctVendedor.Value;
  FLAN_Account := DmLct2.QrLctAccount.Value;
  FLAN_ICMS_P := DmLct2.QrLctICMS_P.Value;
  FLAN_ICMS_V := DmLct2.QrLctICMS_V.Value;
  FLAN_CliInt := DmLct2.QrLctCliInt.Value;
  FLAN_Depto := DmLct2.QrLctDepto.Value;
  FLAN_DescoPor := DmLct2.QrLctDescoPor.Value;
  FLAN_ForneceI := DmLct2.QrLctForneceI.Value;
  FLAN_DescoVal := DmLct2.QrLctDescoVal.Value;
  FLAN_NFVal := DmLct2.QrLctNFVal.Value;
  FLAN_Unidade := DmLct2.QrLctUnidade.Value;
  FLAN_Qtde := DmLct2.QrLctQtde.Value;
  FLAN_FatID := DmLct2.QrLctFatID.Value;
  FLAN_FatID_Sub := DmLct2.QrLctFatID_Sub.Value;
  FLAN_FatNum := Trunc(DmLct2.QrLctFatNum.Value);
  FLAN_FatParcela := DmLct2.QrLctFatParcela.Value;
  FLAN_MultaVal := Multa;
  FLAN_MoraVal := Juros;
  FLAN_ID_Pgto := DmLct2.QrLctControle.Value;
  FLAN_Controle := Round(Controle2);
  //
  UFinanceiro.InsereLancamento(DmLct2.FTabLctA);
end;

procedure TFmCondGer.QrCartNCalcFields(DataSet: TDataSet);
begin
  QrCartNANTERIOR.Value := QrCartNInicial.Value + QrCartNANT_CRED.Value -
    QrCartNANT_DEB.Value;
  QrCartNRECEITAS.Value := QrCartNATU_CRED.Value - QrCartNANT_CRED.Value;
  QrCartNDESPESAS.Value := -(QrCartNATU_DEB.Value - QrCartNANT_DEB.Value);
  QrCartNSALDOMES.Value := QrCartNRECEITAS.Value + QrCartNDESPESAS.Value;
  QrCartNFINAL.Value := QrCartNInicial.Value + QrCartNATU_CRED.Value -
    QrCartNATU_DEB.Value + QrCartNTRANSF.Value;
end;

procedure TFmCondGer.Atual5Click(Sender: TObject);
begin
  AtualizaCarteiraAtual();
end;

procedure TFmCondGer.Atual6Click(Sender: TObject);
begin
  ImprimeBoleto(istAtual, False);
end;

procedure TFmCondGer.Atualescolhermodelo1Click(Sender: TObject);
begin
  ImprimeBoleto(istAtual, True);
end;

procedure TFmCondGer.AtualizaCarteiraAtual();
begin
  Screen.Cursor := crHourGlass;
  try
    UFinanceiro.AtualizaVencimentos(DmCond.FTabLctA);
    RecalcSaldoCarteira();
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGer.Todas1Click(Sender: TObject);
begin
  UFinanceiro.AtualizaTodasCarteiras(DmLct2.QrCrt, DmLct2.QrLct,
    DmLct2.FTabLctA);
end;

procedure TFmCondGer.ConfiguraPopoupQuitacoes();
const
  k = 3;
var
  i, n: Integer;
  c: array [0 .. k] of Integer;
begin
  for i := 0 to k do
    c[i] := 0;

  if DBGLct.SelectedRows.Count > 1 then
  begin
    with DBGLct.DataSource.DataSet do
      for i := 0 to DBGLct.SelectedRows.Count - 1 do
      begin
        // GotoBookmark(pointer(DBGLct.SelectedRows.Items[i]));
        GotoBookmark(DBGLct.SelectedRows.Items[i]);
        inc(c[DmLct2.QrLctSit.Value], 1);
      end;
    n := DBGLct.SelectedRows.Count;
  end
  else
  begin
    c[DmLct2.QrLctSit.Value] := 1;
    n := 1;
  end;
  Compensar2.Enabled := False;
  Pagar2.Enabled := False;
  Reverter2.Enabled := False;
  PagarAVista2.Enabled := False;
  //
  if DmLct2.QrCrtTipo.Value = 2 then
  begin
    // PMQuita
    Compensar2.Enabled := c[0] = n;
    Pagar2.Enabled := (n = 1) and (c[3] = 0);
    Reverter2.Enabled := c[3] = n;
    PagarAVista2.Enabled := c[0] = n;
    // PMMenu
    Compensar1.Enabled := c[0] = n;
    Pagar1.Enabled := (n = 1) and (c[3] = 0);
    Reverter1.Enabled := c[3] = n;
    PagarAVista1.Enabled := c[0] = n;
  end;
end;

procedure TFmCondGer.Conta1Click(Sender: TObject);
begin
  UFinanceiro.MudaContaLancamentosSelecionados(DmLct2.QrCrt, DmLct2.QrLct,
    TDBGrid(DBGLct), DmLct2.FTabLctA);
end;

procedure TFmCondGer.Contasmensais1Click(Sender: TObject);
begin
  Dmod.ReopenControle;
  //
  if DBCheck.CriaFm(TFmContasConfEmis, FmContasConfEmis, afmoNegarComAviso) then
  begin
    with FmContasConfEmis do
    begin
      FFinalidade := lfCondominio;
      FQrLct := DmLct2.QrLct;
      FQrCrt := DmLct2.QrCrt;
      FPercJuroM := Dmod.QrControle.FieldByName('MoraDD').AsFloat;
      FPercMulta := Dmod.QrControle.FieldByName('Multa').AsFloat;
      FSetaVars := nil;
      FAlteraAtehFatID := True;
      FLockCliInt := True;
      FLockForneceI := False;
      FLockAccount := False;
      FLockVendedor := False;
      FCliente := 0;
      FFornecedor := 0;
      FForneceI := 0;
      FAccount := 0;
      FVendedor := 0;
      FIDFinalidade := 2;
      FTabLctA := DmLct2.FTabLctA;
      //
      ShowModal;
      Destroy;
    end;
  end;
end;

procedure TFmCondGer.ContasMensaisCadastrodeEmisses1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasMesGer, FmContasMesGer, afmoNegarComAviso) then
  begin
    FmContasMesGer.ShowModal;
    FmContasMesGer.Destroy;
  end;
end;

procedure TFmCondGer.ContasMensaisCadastrodePagamentos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasConfCad, FmContasConfCad, afmoNegarComAviso) then
  begin
    FmContasConfCad.ShowModal;
    FmContasConfCad.Destroy;
  end;
end;

procedure TFmCondGer.Incluiarrecadaofutura1Click(Sender: TObject);
begin
  MostraFmCondGerArreFut(stIns);
end;

procedure TFmCondGer.Alteraarrecadaofutura1Click(Sender: TObject);
begin
  MostraFmCondGerArreFut(stUpd);
end;

procedure TFmCondGer.Modelo01RelExactus1Click(Sender: TObject);
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if DBCheck.CriaFm(TFmImportSal1, FmImportSal1, afmoNegarComAviso) then
  begin
    FmImportSal1.FTabLctA := DmLct2.FTabLctA;
    // FmImportSal1.FTabLctB               := DmLct2.FTabLctB;
    // FmImportSal1.FTabLctD               := DmLct2.FTabLctD;
    FmImportSal1.EdEmpresa.ValueVariant := DmCond.QrCondCodigo.Value;
    FmImportSal1.CBEmpresa.KeyValue := DmCond.QrCondCodigo.Value;
    (*
      FmImportSal1.LaEmpresa.Enabled      := False;
      FmImportSal1.EdEmpresa.Enabled      := False;
      FmImportSal1.CBEmpresa.Enabled      := False;
    *)
    //
    FmImportSal1.EdCarteira.ValueVariant := DmLct2.QrCrtCodigo.Value;
    FmImportSal1.CBCarteira.KeyValue := DmLct2.QrCrtCodigo.Value;
    //
    FmImportSal1.ShowModal;
    FmImportSal1.Destroy;
  end;
end;

procedure TFmCondGer.Modelo02Pholha1Click(Sender: TObject);
begin
  DModG.ReopenEmpresas(VAR_USUARIO, 0);
  if DBCheck.CriaFm(TFmImportSal2, FmImportSal2, afmoNegarComAviso) then
  begin
    FmImportSal2.FTabLctA := DmLct2.FTabLctA;
    FmImportSal2.EdEmpresa.ValueVariant := DmCond.QrCondCodigo.Value;
    FmImportSal2.CBEmpresa.KeyValue := DmCond.QrCondCodigo.Value;
    //
    FmImportSal2.EdCarteira.ValueVariant := DmLct2.QrCrtCodigo.Value;
    FmImportSal2.CBCarteira.KeyValue := DmLct2.QrCrtCodigo.Value;
    //
    FmImportSal2.ShowModal;
    FmImportSal2.Destroy;
  end;
end;

procedure TFmCondGer.MostraCondGerNew(SQLTipo: TSQLType);
var
  Periodo: Integer;
begin
  if SQLTipo = stIns then
  begin
    if DBCheck.CriaFm(TFmCondGerNew, FmCondGerNew, afmoNegarComAviso) then
    begin
      FmCondGerNew.ShowModal;
      //
      Periodo := FmCondGerNew.FPeriodo;
      //
      FmCondGerNew.Destroy;
      //
      LocPeriodo(Periodo, Periodo);
    end;
  end
  else
  begin
    if DBCheck.CriaFm(TFmCondGerAvisos, FmCondGerAvisos, afmoNegarComAviso) then
    begin
      FmCondGerAvisos.MeAvisoVerso.Text := QrPrevAvisoVerso.Value;
      FmCondGerAvisos.Edit1.Text := QrPrevAviso01.Value;
      FmCondGerAvisos.Edit2.Text := QrPrevAviso02.Value;
      FmCondGerAvisos.Edit3.Text := QrPrevAviso03.Value;
      FmCondGerAvisos.Edit4.Text := QrPrevAviso04.Value;
      FmCondGerAvisos.Edit5.Text := QrPrevAviso05.Value;
      FmCondGerAvisos.Edit6.Text := QrPrevAviso06.Value;
      FmCondGerAvisos.Edit7.Text := QrPrevAviso07.Value;
      FmCondGerAvisos.Edit8.Text := QrPrevAviso08.Value;
      FmCondGerAvisos.Edit9.Text := QrPrevAviso09.Value;
      FmCondGerAvisos.Edit10.Text := QrPrevAviso10.Value;
      FmCondGerAvisos.Edit11.Text := QrPrevAviso11.Value;
      FmCondGerAvisos.Edit12.Text := QrPrevAviso12.Value;
      FmCondGerAvisos.Edit13.Text := QrPrevAviso13.Value;
      FmCondGerAvisos.Edit14.Text := QrPrevAviso14.Value;
      FmCondGerAvisos.Edit15.Text := QrPrevAviso15.Value;
      FmCondGerAvisos.Edit16.Text := QrPrevAviso16.Value;
      FmCondGerAvisos.Edit17.Text := QrPrevAviso17.Value;
      FmCondGerAvisos.Edit18.Text := QrPrevAviso18.Value;
      FmCondGerAvisos.Edit19.Text := QrPrevAviso19.Value;
      FmCondGerAvisos.Edit20.Text := QrPrevAviso20.Value;
      FmCondGerAvisos.CkBalAgrMens.Checked :=
        Geral.IntToBool(QrPrevBalAgrMens.Value);
      FmCondGerAvisos.RGModelBloq.ItemIndex := QrPrevModelBloq.Value;
      FmCondGerAvisos.RGCompe.ItemIndex := QrPrevCompe.Value;
      FmCondGerAvisos.EdConfigBol.Text := Geral.FF0(QrPrevConfigBol.Value);
      FmCondGerAvisos.CBConfigBol.KeyValue := QrPrevConfigBol.Value;
      FmCondGerAvisos.ShowModal;
      FmCondGerAvisos.Destroy;
      //
      LocPeriodo(QrPrevPeriodo.Value, QrPrevPeriodo.Value);
    end;
  end;
end;

procedure TFmCondGer.MostraFmCondGerArreFut(SQLType: TSQLType);
var
  Ano, Mes, i: Integer;
begin
  Application.CreateForm(TFmCondGerArreFut, FmCondGerArreFut);
  FmCondGerArreFut.FCliInt := DmCond.QrCondCodigo.Value;
  FmCondGerArreFut.ImgTipo.SQLType := SQLType;
  //
  FmCondGerArreFut.QrDeptos.Close;
  FmCondGerArreFut.QrDeptos.Params[0].AsInteger := QrPrevCond.Value;
  FmCondGerArreFut.QrDeptos.Open;
  //
  if SQLType = stUpd then
  begin
    Ano := MLAGeral.AnoDoPeriodo(DmCond.QrArreFutIPeriodo.Value);
    Mes := MLAGeral.MesDoPeriodo(DmCond.QrArreFutIPeriodo.Value);
    with FmCondGerArreFut do
    begin
      EdDepto.ValueVariant := DmCond.QrArreFutIApto.Value;
      CBDepto.KeyValue := DmCond.QrArreFutIApto.Value;
      EdCNAB_Cfg.ValueVariant := DmCond.QrArreFutICNAB_Cfg.Value;
      CBCNAB_Cfg.KeyValue := DmCond.QrArreFutICNAB_Cfg.Value;
      EdConta.ValueVariant := DmCond.QrArreFutIConta.Value;
      CBConta.KeyValue := DmCond.QrArreFutIConta.Value;
      EdValor.ValueVariant := DmCond.QrArreFutIValor.Value;
      EdDescricao.Text := DmCond.QrArreFutITexto.Value;
      //
      FmCondGerArreFut.CkContinuar.Visible := False;
      FmCondGerArreFut.CkContinuar.Checked := False;
      //
      for i := 0 to FmCondGerArreFut.CBAno.Items.Count - 1 do
      begin
        if Geral.IMV(FmCondGerArreFut.CBAno.Items[i]) = Ano then
          FmCondGerArreFut.CBAno.ItemIndex := i;
      end;
      FmCondGerArreFut.CBMes.ItemIndex := Mes - 1;
    end;
  end
  else
  begin
    FmCondGerArreFut.CkContinuar.Visible := True;
    FmCondGerArreFut.CkContinuar.Checked := True;
  end;
  FmCondGerArreFut.ShowModal;
  FmCondGerArreFut.Destroy;
end;

procedure TFmCondGer.Excluiarrecadaofutura1Click(Sender: TObject);
begin
  if UMyMod.SQLDel1(Dmod.QrUpd, DmCond.QrArreFutI, 'ArreFut', 'Controle',
    DmCond.QrArreFutIControle.Value, True,
    'Confirma a exclus�o da arrecada��o futura selecionada?', True) then
    Geral.MB_Aviso
      ('AVISO! A exclus�o de item futuro n�o elimina item j� arrecadado!');
end;

procedure TFmCondGer.MostraFmCondGerArreFutA();
begin
  if DBCheck.CriaFm(TFmCondGerArreFutA, FmCondGerArreFutA, afmoNegarComAviso)
  then
  begin
    FmCondGerArreFutA.ShowModal;
    FmCondGerArreFutA.Destroy;
  end;
end;

procedure TFmCondGer.Vencimento1Click(Sender: TObject);
begin
  UFinanceiro.ColocarMesDeCompetenciaOndeNaoTem(dcVencimento, DmLct2.QrCrt,
    DmLct2.QrLct, DmLct2.FTabLctA);
end;

procedure TFmCondGer.VerificaArreFut();
begin
  DmCond.QrArreFutA.Close;
  DmCond.QrArreFutA.SQL.Clear;
  DmCond.QrArreFutA.SQL.Add
    ('SELECT IF(prp.Tipo=0, prp.RazaoSocial, prp.Nome) NOMEPROP,');
  DmCond.QrArreFutA.SQL.Add('imv.Unidade UNIDADE, con.Nome NOMECONTA, arf.*');
  DmCond.QrArreFutA.SQL.Add('FROM arrefut arf');
  DmCond.QrArreFutA.SQL.Add
    ('LEFT JOIN entidades prp ON prp.Codigo=arf.Propriet');
  DmCond.QrArreFutA.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=arf.Apto');
  DmCond.QrArreFutA.SQL.Add('LEFT JOIN contas    con ON con.Codigo=arf.Conta');
  DmCond.QrArreFutA.SQL.Add('WHERE arf.' + TAB_ARI + '=0');
  DmCond.QrArreFutA.SQL.Add('AND arf.Cond=:P0');
  DmCond.QrArreFutA.SQL.Add('AND arf.Periodo=:P1');
  DmCond.QrArreFutA.SQL.Add('ORDER BY arf.Periodo DESC');
  DmCond.QrArreFutA.Params[00].AsInteger := DmCond.QrCondCodigo.Value;
  DmCond.QrArreFutA.Params[01].AsInteger := QrPrevPeriodo.Value;
  UMyMod.AbreQuery(DmCond.QrArreFutA, Dmod.MyDB);
  //
  if DmCond.QrArreFutA.RecordCount > 0 then
  begin
    if Geral.MB_Pergunta('Foram localizados ' +
      Geral.FF0(DmCond.QrArreFutA.RecordCount) + ' itens de arrecada��o com ' +
      'pr�-agendadamento para este per�odo ou per�odos anteriores que n�o ' +
      'foram inclu�dos ainda. Deseja visualiz�-los agora?') = ID_YES then
      MostraFmCondGerArreFutA;
  end
  else
    Geral.MB_Aviso('N�o foram localizados itens de ' +
      'arrecada��o com pr�-agendadamento para este per�odo ou per�odos anteriores!');
end;

procedure TFmCondGer.Incluiitenspragendados1Click(Sender: TObject);
begin
  VerificaArreFut();
end;

procedure TFmCondGer.PMArrecadaPopup(Sender: TObject);
var
  Enab, Enab2, Encerr: Boolean;
begin
  Enab := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0);
  Enab2 := (QrArre.State <> dsInactive) and (QrArre.RecordCount > 0);
  //
  if Enab then
    Encerr := QrPrevEncerrado.Value <> 0
  else
    Encerr := True;
  //
  Incluiintensbasedearrecadao1.Enabled := Enab and (not Encerr);
  Novoitemdearrecadao1.Enabled := Enab and (not Encerr);
  Alteraarrecadao1.Enabled := Enab and (not Encerr) and Enab2;
  Exclusodearrecadao1.Enabled := Enab and (not Encerr) and Enab2;
  Incluiitenspragendados1.Enabled := Enab and (not Encerr);
end;

procedure TFmCondGer.PMArreFutPopup(Sender: TObject);
var
  Enab, Enab2, Encerr: Boolean;
begin
  Enab := (QrPrev.State <> dsInactive) and (QrPrev.RecordCount > 0);
  Enab2 := (DmCond.QrArreFutI.State <> dsInactive) and
    (DmCond.QrArreFutI.RecordCount > 0);
  //
  if Enab then
    Encerr := QrPrevEncerrado.Value <> 0
  else
    Encerr := True;
  //
  Incluiarrecadaofutura1.Enabled := Enab and (not Encerr);
  Alteraarrecadaofutura1.Enabled := Enab and (not Encerr) and Enab2;
  Excluiarrecadaofutura1.Enabled := Enab and (not Encerr) and Enab2;
end;

procedure TFmCondGer.BtDescompensaClick(Sender: TObject);
  procedure ReverterCompensacaoBLC();
  var
    i, k: Integer;
  begin
    if DBGBLC.SelectedRows.Count > 1 then
    begin
      if Geral.MensagemBox('Confirma a revers�o da compensa��o ' +
        'dos itens selecionados?', 'Pergunta', MB_ICONQUESTION + MB_YESNOCANCEL)
        = ID_YES then
      begin
        with DBGBLC.DataSource.DataSet do
          for i := 0 to DBGBLC.SelectedRows.Count - 1 do
          begin
            // GotoBookmark(pointer(DBGBLC.SelectedRows.Items[i]));
            GotoBookmark(DBGBLC.SelectedRows.Items[i]);
            if DmBloq.QrBLCSit.Value = 3 then
              UFinanceiro.ReverterPagtoEmissao(DmBloq.QrBLC, nil, False, False,
                False, DmLct2.FTabLctA);
            // FmPrincipal.ReverterPagtoEmissao(DmBloq.QrBLC, nil, False);
          end;
        k := DmBloq.QrBLCControle.Value;
        RecalcSaldoCarteira();
        DmBloq.QrBLC.Locate('Controle', k, []);
      end;
    end
    else // FmPrincipal.ReverterPagtoEmissao(DmBloq.QrBLC, nil, True);
      UFinanceiro.ReverterPagtoEmissao(DmBloq.QrBLC, nil, False, False, False,
        DmLct2.FTabLctA);
  end;

begin
  if DmBloq.QrBLC.RecordCount > 0 then
    ReverterCompensacaoBLC();
end;

procedure TFmCondGer.BtLeiStepClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCondGerStep, FmCondGerStep, afmoNegarComAviso) then
  begin
    with FmCondGerStep do
    begin
      FmCondGerStep.QrLei_Step.Params[00].AsFloat :=
        DmBloq.QrBoletosBLOQUETO.Value;
      FmCondGerStep.QrLei_Step.Params[01].AsInteger :=
        DmCond.QrCondCliente.Value;
      FmCondGerStep.QrLei_Step.Open;
      FmCondGerStep.ShowModal;
      FmCondGerStep.Destroy;
    end;
  end;
end;

procedure TFmCondGer.TranferenciaEntreCarteiras(Tipo: Integer);
begin
  UFinanceiro.CriarTransferCart(Tipo, DmLct2.QrLct, DmLct2.QrCrt, True,
    FThisEntidade, 0, 0, 0, DmLct2.FTabLctA);
  //
  RecalcSaldoCarteira();
end;

procedure TFmCondGer.TranferenciaEntreContas(Tipo: Integer);
begin
  UFinanceiro.CriarTransferCtas(Tipo, DmLct2.QrLct, DmLct2.QrCrt, True,
    FThisEntidade, 0, 0, 0, DmLct2.FTabLctA);
  //
  RecalcSaldoCarteira();
end;

procedure TFmCondGer.Balanceteconfigurvel1Click(Sender: TObject);
begin
  (*
    if DBCheck.CriaFm(TFmCondGerImpBal, FmCondGerImpBal, afmoNegarComAviso) then
    begin
    FmCondGerImpBal.FClienteInicial := DmCond.QrCondCliente.Value;
    FmCondGerImpBal.FPeriodoInicial := QrPrevPeriodo.Value;
    FmCondGerImpBal.ShowModal;
    FmCondGerImpBal.Destroy;
    end;
  *)
  if DBCheck.CriaFm(TFmCashBal, FmCashBal, afmoNegarComAviso) then
  begin
    FmCashBal.FClienteInicial := DmCond.QrCondCliente.Value;
    FmCashBal.FPeriodoInicial := QrPrevPeriodo.Value;
    FmCashBal.ShowModal;
    FmCashBal.Destroy;
  end;
end;

procedure TFmCondGer.BtPesquisaClick(Sender: TObject);
const
  LocSohCliInt = True;
  QuemChamou = 0;
begin
  (*
    UFinanceiro.LocalizarLancamento(TPDataIni,
    DmLct2.QrCrt, DmLct2.QrLct, True, 0);
  *)  //
  UFinanceiro.LocalizarLancamento(TPDataIni, TPDataFim, RGTipoData.ItemIndex,
    DmLct2.QrCrt, DmLct2.QrLct, LocSohCliInt, QuemChamou,
    DmLct2.FTabLctA, DmLct2);
end;

procedure TFmCondGer.Transformaremitemdebloqueto1Click(Sender: TObject);
const
  FatID = 600; // Arrecada��es?
begin
  UFinanceiro.TransformarLancamentoEmItemDeBloqueto(FatID, DmLct2.QrCrt,
    DmLct2.QrLct, TDBGrid(DBGLct), DmLct2.FTabLctA);
end;

procedure TFmCondGer.TravadestravaPerodo1Click(Sender: TObject);
var
  Encerrado, Codigo, Periodo: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss then
    Exit;
  //
  Encerrado := QrPrevEncerrado.Value;
  //
  if Encerrado = 1 then
    Encerrado := 0
  else
    Encerrado := 1;
  //
  Codigo := QrPrevCodigo.Value;
  Periodo := QrPrevPeriodo.Value;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, DmCond.FTabPrvA, False, ['Encerrado'],
    ['Codigo'], [Encerrado], [Codigo], True) then
  begin
    LocPeriodo(Periodo, Periodo);
  end;
end;

procedure TFmCondGer.Umalinhaporbloqueto1Click(Sender: TObject);
var
  Janela: Integer;
begin
  if (PageControl1.ActivePageIndex <> 3) or (PageControl2.ActivePageIndex <> 8)
    or (PageControl5.ActivePageIndex <> 1) or (PageControl6.ActivePageIndex <> 1)
  then
  begin
    PageControl1.ActivePageIndex := 3;
    PageControl2.ActivePageIndex := 8;
    PageControl5.ActivePageIndex := 1;
    PageControl6.ActivePageIndex := 1;
    Geral.MensagemBox('Informe o agrupamento desejado!', 'Aviso',
      MB_OK + MB_ICONWARNING);
  end
  else
  begin
    //
    Janela := MyObjects.SelRadioGroup('Vers�o da janela',
      'Vers�o da janela a ser exibida', ['Modelo A', 'Modelo B'], 1);
    case Janela of
      0:
        FmCondGerImpGer2.RelatorioDeArrecadacoes_ListaDeCondominos(True);
      1:
        begin
          FmCondGerImpGer2a.RelatorioDeArrecadacoes_ListaDeCondominos(True);
        end;
    end;
  end;
end;

procedure TFmCondGer.UmalinhaporUH1Click(Sender: TObject);
begin
  if (PageControl1.ActivePageIndex <> 3) or (PageControl2.ActivePageIndex <> 8)
    or (PageControl5.ActivePageIndex <> 1) or (PageControl6.ActivePageIndex <> 1)
  then
  begin
    PageControl1.ActivePageIndex := 3;
    PageControl2.ActivePageIndex := 8;
    PageControl5.ActivePageIndex := 1;
    PageControl6.ActivePageIndex := 1;
    Geral.MensagemBox('Informe o agrupamento desejado!', 'Aviso',
      MB_OK + MB_ICONWARNING);
  end
  else
    FmCondGerImpGer.RelatorioDeArrecadacoes_ListaDeCondominos(True);
end;

procedure TFmCondGer.UmparacadaLanamento1Click(Sender: TObject);
begin
  UnProtocolo.ProtocolosCD_Lct(istSelecionados, TDBGrid(DBGLct), DmLct2,
    DmCond.QrCondCliente.Value, FmCondGer.Name, DmCond.FTabLctA);
end;

procedure TFmCondGer.UmporDocumento1Click(Sender: TObject);
begin
  UnProtocolo.ProtocolosCD_Doc(istSelecionados, TDBGrid(DBGLct), DmLct2,
    DmCond.QrCondCliente.Value, FmCondGer.Name, DmCond.FTabLctA);
end;

procedure TFmCondGer.Criarbloquetosparaitensquenoso1Click(Sender: TObject);
var
  Boleto: String;
begin
  if Geral.MensagemBox('Tem certeza que deseja transformar todos ' +
    'lan�amentos que n�o s�o bloqueto em bloqueto?', 'Pergunta',
    MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    //
    DmLct2.QrLct.First;
    while not DmLct2.QrLct.Eof do
    begin
      if DmLct2.QrLctFatNum.Value = 0 then
      begin
        if (Trim(DmLct2.QrLctUH.Value) <> '') and
          (Trim(DmLct2.QrLctMENSAL.Value) <> '') then
        begin
          Boleto := Geral.SoNumero_TT(DmLct2.QrLctUH.Value) +
            IntToStr(DmLct2.QrLctMez.Value);
          Boleto := Copy(IntToStr(Geral.IMV(Boleto)), 1, 6);
          //
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('UPDATE ' + DmLct2.FTabLctA +
            ' SET AlterWeb=1, Cliente=ForneceI, ');
          Dmod.QrUpd.SQL.Add('FatNum=:P0, Documento=:P1, FatID=600');
          Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa AND Sub=:Pb');
          Dmod.QrUpd.Params[00].AsString := Boleto;
          Dmod.QrUpd.Params[01].AsString := Boleto;
          Dmod.QrUpd.Params[02].AsInteger := DmLct2.QrLctControle.Value;
          Dmod.QrUpd.Params[03].AsInteger := DmLct2.QrLctSub.Value;
          Dmod.QrUpd.ExecSQL;
        end;
      end;
      DmLct2.QrLct.Next;
    end;
    RecalcSaldoCarteira();
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmCondGer.SbPesqClick(Sender: TObject);
begin
  FmPrincipal.InadimplenciaNew(DmCond.QrCondCodigo.Value);
end;

procedure TFmCondGer.BtContasHistSdoClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmContasHistSdo3, FmContasHistSdo3, afmoNegarComAviso) then
  begin
    FmContasHistSdo3.EdCliInt.ValueVariant := DmCond.QrCondCodigo.Value;
    FmContasHistSdo3.CBCliInt.KeyValue := DmCond.QrCondCodigo.Value;
    FmContasHistSdo3.ShowModal;
    FmContasHistSdo3.Destroy;
  end;
end;

procedure TFmCondGer.Cpiaautomtica1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCopiaDoc, FmCopiaDoc, afmoNegarComAviso) then
  begin
    FmCopiaDoc.FDBG := TDBGrid(DBGLct);
    FmCopiaDoc.FQrLct := DmLct2.QrLct;
    FmCopiaDoc.FTabLct := DmLct2.FTabLctA;
    FmCopiaDoc.ShowModal;
    FmCopiaDoc.Destroy;
  end;
end;

procedure TFmCondGer.Cpiadecheque1Click(Sender: TObject);
var
  i: Integer;
begin
  if DBGLct.SelectedRows.Count > 1 then
  begin
    if Geral.MensagemBox('Confirma a impress�o da c�pia de cheque ' +
      'dos itens selecionados?', 'Pergunta', MB_ICONQUESTION + MB_YESNOCANCEL) = ID_YES
    then
    begin
      // PB1.Max := DBGLct.SelectedRows.Count;
      with DBGLct.DataSource.DataSet do
        for i := 0 to DBGLct.SelectedRows.Count - 1 do
        begin
          // PB1.Position := PB1.Position + 1;
          // GotoBookmark(pointer(DBGLct.SelectedRows.Items[i]));
          GotoBookmark(DBGLct.SelectedRows.Items[i]);
          FmCondGerImpGer.ImprimeCopiaCH(DmLct2.QrLctControle.Value,
            DmLct2.QrLctGenero.Value, DmLct2.FTabLctA);
        end;
    end;
  end
  else
    FmCondGerImpGer.ImprimeCopiaCH(DmLct2.QrLctControle.Value,
      DmLct2.QrLctGenero.Value, DmLct2.FTabLctA);
end;

procedure TFmCondGer.CpiadechequeNovo1Click(Sender: TObject);
begin
  FmCondGerImpGer.ImprimeCopiaVC(DmLct2.QrLct, DmLct2.FTabLctA);
end;

procedure TFmCondGer.Cpiadedbitoemconta1Click(Sender: TObject);
begin
  FmCondGerImpGer.ImprimeCopiaDC(DmLct2.QrLctControle.Value,
    DmLct2.QrLctGenero.Value, DmLct2.FTabLctA);
end;

procedure TFmCondGer.BtCadCondClick(Sender: TObject);
begin
  FmPrincipal.CadastroDeCondominio(DmCond.QrCondCodigo.Value);
  //
  DmCond.QrCond.Close;
  DmCond.QrCond.Open;
  //
  DmBloq.QrCNAB_Cfg_B.Close;
  DmBloq.QrCNAB_Cfg_B.Open;
end;

procedure TFmCondGer.BtDesfazOrdemBloqClick(Sender: TObject);
var
  Query: TmySQLQuery;
  Boleto: Integer;
begin
  if DBGradeS.DataSource.DataSet is TmySQLQuery then
    Query := TmySQLQuery(DBGradeS.DataSource.DataSet)
  else
    Query := nil;
  if Query <> nil then
  begin
    Query.SortFieldNames := '';
    if Query.State <> dsInactive then
    begin
      Boleto := Query.FieldByName('Boleto').AsInteger;
      Query.Close;
      Query.Open;
      Query.Locate('Boleto', Boleto, []);
    end;
  end;
end;

procedure TFmCondGer.BtDuplicaClick(Sender: TObject);
begin
  VerificaCarteirasCliente();
  VerificaBotoes();
  InsAlt(tgrDuplica);
end;

procedure TFmCondGer.BitBtn11Click(Sender: TObject);
begin
  { ###
    Application.CreateForm(TFmBloqAvulso, FmBloqAvulso);
    FmBloqAvulso.ShowModal;
    FmBloqAvulso.Destroy;
  }
end;

procedure TFmCondGer.BitBtn2Click(Sender: TObject);
var
  Query: TmySQLQuery;
  Controle: Integer;
begin
  if DBGCNS.DataSource.DataSet is TmySQLQuery then
    Query := TmySQLQuery(DBGCNS.DataSource.DataSet)
  else
    Query := nil;
  if Query <> nil then
  begin
    Query.SortFieldNames := '';
    if Query.State <> dsInactive then
    begin
      Controle := Query.FieldByName('Controle').AsInteger;
      Query.Close;
      Query.Open;
      Query.Locate('Controle', Controle, []);
    end;
  end;
end;

procedure TFmCondGer.CBUHKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_DELETE then
  begin
    CBUH.KeyValue := NULL;
    VeSeReabreLct();
  end;
end;

procedure TFmCondGer.CBUHClick(Sender: TObject);
begin
  VeSeReabreLct();
end;

(*
  Destivado para n�o gerar erros entre os boletos e os lan�amentos financeiros.
  Desativado em 29/07/2014.
  Caso seja necess�rio reativar analizar o c�digo

  procedure TFmCondGer.AdicionaItemABloqueto(Tipo: TselType);
  procedure AdicionaLeituraAtual(Boleto: Double; Controle: Integer; Data: TDateTime);
  begin
  Dmod.QrUpdY.SQL.Clear;
  Dmod.QrUpdY.SQL.Clear;
  Dmod.QrUpdY.SQL.Add('UPDATE ' + DmCond.FTabCnsA + ' SET Boleto=:P0, Vencto=:P1 ');
  Dmod.QrUpdY.SQL.Add('WHERE Boleto=0 AND Controle=:P2');
  //
  Dmod.QrUpdY.Params[00].AsFloat   := Trunc(Boleto);
  Dmod.QrUpdY.Params[01].AsString  := Geral.FDT(Data, 1);
  Dmod.QrUpdY.Params[02].AsInteger := Controle;
  Dmod.QrUpdY.ExecSQL;
  //
  // Cria Lancamentos da leitura selecionada
  DmBloq.QrBolB.Close;
  DmBloq.QrBolB.SQL.Clear;
  DmBloq.QrBolB.SQL.Add('SELECT csn.Genero, csn.Nome NOMECONS, csi.Valor,');
  DmBloq.QrBolB.SQL.Add('csi.Propriet, csi.Apto, csi.Vencto, csi.MedAnt, ');
  DmBloq.QrBolB.SQL.Add('csi.MedAtu, csi.Consumo, csi.Casas, csi.UnidLei, ');
  DmBloq.QrBolB.SQL.Add('csi.UnidFat, csi.UnidImp,csi.Controle, csi.GeraTyp,');
  DmBloq.QrBolB.SQL.Add('csi.GeraFat, csi.CasRat, csi.NaoImpLei');
  DmBloq.QrBolB.SQL.Add('FROM ' + DmCond.FTabCnsA + ' csi');
  DmBloq.QrBolB.SQL.Add('LEFT JOIN cons csn ON csn.Codigo=csi.Codigo');
  DmBloq.QrBolB.SQL.Add('WHERE csi.Controle=:P0');
  DmBloq.QrBolB.Params[0].AsInteger := Controle;
  DmBloq.QrBolB.Open;
  UFinanceiro.LancamentoDefaultVARS;
  //
  FLAN_Data       := Geral.FDT(Date, 1);
  FLAN_Tipo       := 2;
  FLAN_Documento  := Boleto;
  FLAN_Credito    := DmBloq.QrBolBValor.Value;
  FLAN_MoraDia    := DmBloq.QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat;
  FLAN_Multa      := DmBloq.QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat;
  FLAN_Carteira   := DmBloq.QrCNAB_Cfg_B.FieldByName('CartEmiss').AsInteger;
  FLAN_Genero     := DmBloq.QrBolBGenero.Value;
  FLAN_Cliente    := DmBloq.QrBolBPropriet.Value;
  FLAN_CliInt     := DmCond.QrCondCliente.Value;
  FLAN_Depto      := DmBloq.QrBolBApto.Value;
  FLAN_ForneceI   := DmBloq.QrBolBPropriet.Value;
  FLAN_Vencimento := Geral.FDT(DmBloq.QrBolBVencto.Value, 1);
  FLAN_Mez        := MLAGeral.PeriodoToAnoMes(QrPrevPeriodo.Value);
  FLAN_FatID      := 601;
  FLAN_FatNum     := Boleto;
  FLAN_FatParcela := 1;
  FLAN_Descricao  := DmBloq.QrBolBNOMECONS.Value + ' (' +
  DmBloq.TextoExplicativoItemBoleto(1, DmBloq.QrBolBCasas.Value,
  DmBloq.QrBolBMedAnt.Value, DmBloq.QrBolBMedAtu.Value,
  DmBloq.QrBolBConsumo.Value, DmBloq.QrBolBUnidFat.Value,
  DmBloq.QrBolBUnidLei.Value, DmBloq.QrBolBUnidImp.Value,
  DmBloq.QrBolBGeraTyp.Value, DmBloq.QrBolBCasRat.Value,
  DmBloq.QrBolBNaoImpLei.Value, DmBloq.QrBolBGeraFat.Value);

  FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
  'Controle', DmCond.FTabLctA, LAN_CTOS, 'Controle');
  if UFinanceiro.InsereLancamento(DmCond.FTabLctA) then
  begin
  Dmod.QrUpd2.SQL.Clear;
  Dmod.QrUpd2.SQL.Add('UPDATE ' + DmCond.FTabCnsA + ' SET Lancto=:P0 WHERE Controle=:P1');
  Dmod.QrUpd2.Params[00].AsInteger := FLAN_Controle;
  Dmod.QrUpd2.Params[01].AsInteger := DmBloq.QrBolBControle.Value;
  Dmod.QrUpd2.ExecSQL;
  end;
  //
  end;
  procedure AdicionaArrecadacaoAtual(Boleto: Double; Controle: Integer; Data: TDateTime);
  begin
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ' + DmCond.FTabAriA + ' SET Boleto=:P0, Vencto=:P1 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P2');
  //
  Dmod.QrUpd.Params[00].AsFloat   := Trunc(Boleto);
  Dmod.QrUpd.Params[01].AsString  := Geral.FDT(Data, 1);
  Dmod.QrUpd.Params[02].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;

  // Cria Lancamentos da arrecada��es selecionada
  DmBloq.QrBolA.Close;
  DmBloq.QrBolA.SQL.Clear;
  DmBloq.QrBolA.SQL.Add('SELECT ari.Controle, ari.Conta Genero, ari.Valor,');
  DmBloq.QrBolA.SQL.Add('ari.Texto, ari.Apto, ari.Propriet, ari.Vencto,');
  DmBloq.QrBolA.SQL.Add('civ.Unidade');
  DmBloq.QrBolA.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
  DmBloq.QrBolA.SQL.Add('LEFT JOIN condimov civ ON civ.Conta=ari.Apto');
  DmBloq.QrBolA.SQL.Add('WHERE ari.Controle=:P0');
  DmBloq.QrBolA.Params[0].AsInteger := Controle;
  DmBloq.QrBolA.Open;
  UFinanceiro.LancamentoDefaultVARS;
  //
  FLAN_Data       := Geral.FDT(Date, 1);
  FLAN_Tipo       := 2;
  FLAN_Documento  := Boleto;
  FLAN_Credito    := DmBloq.QrBolAValor.Value;
  FLAN_MoraDia    := DmBloq.QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat;
  FLAN_Multa      := DmBloq.QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat;
  FLAN_Carteira   := DmBloq.QrCNAB_Cfg_B.FieldByName('CartEmiss').AsInteger;
  FLAN_Genero     := DmBloq.QrBolAGenero.Value;
  FLAN_Cliente    := DmBloq.QrBolAPropriet.Value;
  FLAN_CliInt     := DmCond.QrCondCliente.Value;
  FLAN_Depto      := DmBloq.QrBolAApto.Value;
  FLAN_ForneceI   := DmBloq.QrBolAPropriet.Value;
  FLAN_Vencimento := Geral.FDT(DmBloq.QrBolAVencto.Value, 1);
  FLAN_Mez        := MLAGeral.PeriodoToAnoMes(QrPrevPeriodo.Value);
  FLAN_FatID      := 600;
  FLAN_FatNum     := Boleto;
  FLAN_FatParcela := 1;
  FLAN_Descricao  := DmBloq.QrBolATexto.Value + ' - ' +
  dmkPF.MesEAnoDoPeriodo(QrPrevPeriodo.Value) +
  ' - ' + DmBloq.QrBolAUnidade.Value;
  FLAN_Controle   := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
  'Controle', DmCond.FTabLctA, LAN_CTOS, 'Controle');
  if UFinanceiro.InsereLancamento(DmCond.FTabLctA) then
  begin
  Dmod.QrUpd2.SQL.Clear;
  Dmod.QrUpd2.SQL.Add('UPDATE ' + DmCond.FTabAriA + ' SET Lancto=:P0 WHERE Controle=:P1');
  Dmod.QrUpd2.Params[00].AsInteger := FLAN_Controle;
  Dmod.QrUpd2.Params[01].AsInteger := DmBloq.QrBolAControle.Value;
  Dmod.QrUpd2.ExecSQL;
  end;
  end;
  var
  Boleto: Double;
  i, Prox, Protocolo: integer;
  Bolapto: String;
  Data: TDateTime;
  begin
  if DmBloq.QrBoletosIts.RecordCount = 0 then
  begin
  Geral.MensagemBox('N�o h� arrecada��o para adicionar a bloqueto!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
  end;
  DmBloq.QrBolSel.Close;
  DmBloq.QrBolSel.SQL.Clear;
  DmBloq.QrBolSel.SQL.Add('SELECT DISTINCT ari.Boleto, ari.Vencto');
  DmBloq.QrBolSel.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
  DmBloq.QrBolSel.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto');
  DmBloq.QrBolSel.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
  DmBloq.QrBolSel.SQL.Add('WHERE ari.Codigo=:P0');
  DmBloq.QrBolSel.SQL.Add('AND ari.Apto = :P1');
  DmBloq.QrBolSel.SQL.Add('AND ari.Boleto <> 0');
  DmBloq.QrBolSel.SQL.Add('');
  DmBloq.QrBolSel.SQL.Add('UNION');
  DmBloq.QrBolSel.SQL.Add('');
  DmBloq.QrBolSel.SQL.Add('SELECT DISTINCT cni.Boleto, cni.Vencto');
  DmBloq.QrBolSel.SQL.Add('FROM ' + DmCond.FTabCnsA + ' cni');
  DmBloq.QrBolSel.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto');
  DmBloq.QrBolSel.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet');
  DmBloq.QrBolSel.SQL.Add('WHERE cni.Cond=:P2');
  DmBloq.QrBolSel.SQL.Add('AND cni.Periodo=:P3');
  DmBloq.QrBolSel.SQL.Add('AND cni.Apto = :P4');
  DmBloq.QrBolSel.SQL.Add('AND cni.Boleto <> 0');
  DmBloq.QrBolSel.Params[00].AsInteger := QrPrevCodigo.Value;
  DmBloq.QrBolSel.Params[01].AsInteger := DmBloq.QrBoletosApto.Value;
  DmBloq.QrBolSel.Params[02].AsInteger := QrPrevCond.Value;
  DmBloq.QrBolSel.Params[03].AsInteger := QrPrevPeriodo.Value;
  DmBloq.QrBolSel.Params[04].AsInteger := DmBloq.QrBoletosApto.Value;
  DmBloq.QrBolSel.Open;
  if DmBloq.QrBolSel.RecordCount = 0 then
  begin
  Geral.MensagemBox('N�o h� bloqueto para adicionar arrecada��es!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
  end;
  Application.CreateForm(TFmCondGerBolSel, FmCondGerBolSel);
  FmCondGerBolSel.ShowModal;
  Protocolo := FmCondGerBolSel.FProtocolo;
  Boleto := FmCondGerBolSel.FBoleto;
  Data := FmCondGerBolSel.FData;
  FmCondGerBolSel.Destroy;
  if (Protocolo <> 0) then
  begin
  Geral.MensagemBox('O bloqueto ' + FormatFloat('000000', Boleto) +
  ' n�o pode receber mais itens pois j� possui protocolo com valor definido!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
  end;
  if (Boleto = 0) then
  begin
  Geral.MensagemBox('Bloqueto n�o definido para adicionar arrecada��es!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
  end;
  if (Data < 2) then
  begin
  Geral.MensagemBox('Vencimento n�o definido para adicionar arrecada��es!',
  'Aviso', MB_OK+MB_ICONWARNING);
  Exit;
  end;
  i := 0;
  //
  // Parei aqui avisar sobre protocolos existentes
  if Tipo <> istTodos then
  begin
  case Tipo of
  istAtual: i := 1;
  istSelecionados:
  begin
  i := DBGrid13.SelectedRows.Count;
  if i = 0 then i := 1;
  end;
  end;
  if i = 1 then
  begin
  if Geral.MensagemBox('Confirma a adi��o da arrecada��o a um bloqueto j� existente?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
  if DmBloq.QrBoletosItsTipo.Value = 0 then
  AdicionaArrecadacaoAtual(Boleto, DmBloq.QrBoletosItsControle.Value, Data)
  else
  AdicionaLeituraAtual(Boleto, DmBloq.QrBoletosItsControle.Value, Data);
  end;
  end else begin
  if Geral.MensagemBox('Confirma a adi��o das ' +
  IntToStr(DBGrid13.SelectedRows.Count) + ' arrecada��es a um bloqueto j� existente?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
  with DBGrid13.DataSource.DataSet do
  for i:= 0 to DBGrid13.SelectedRows.Count-1 do
  begin
  GotoBoo kmark(poin ter(DBGrid13.SelectedRows.Items[i]));
  if DmBloq.QrBoletosItsTipo.Value = 0 then
  AdicionaArrecadacaoAtual(Boleto, DmBloq.QrBoletosItsControle.Value, Data)
  else
  AdicionaLeituraAtual(Boleto, DmBloq.QrBoletosItsControle.Value, Data);
  end;
  end;
  end;
  end else
  begin
  DmBloq.QrBoletosIts.First;
  while not DmBloq.QrBoletosIts.Eof do
  begin
  if DmBloq.QrBoletosItsTipo.Value = 0 then
  AdicionaArrecadacaoAtual(Boleto, DmBloq.QrBoletosItsControle.Value, Data)
  else
  AdicionaLeituraAtual(Boleto, DmBloq.QrBoletosItsControle.Value, Data);
  DmBloq.QrBoletosIts.Next;
  end;
  end;
  Bolapto := DmBloq.QrBoletosBOLAPTO.Value;
  Prox := 0;
  CalculaTotalARIEReabreArreEARI(
  QrArreApto.Value, QrArrePropriet.Value, Prox);
  DmBloq.ReopenBoletos(Bolapto);
  end;
*)

procedure TFmCondGer.Adicionaitensdeprovisoagendados1Click(Sender: TObject);
begin
  {
    SELECT pbc.Codigo, pbc.Nome, pbc.Conta,

    pbi.Valor,
    pbi.SitCobr, pbi.Texto, pbi.Controle,

    con.Nome NOMECON
    FROM prevbac pbc
    LEFT JOIN prevbai pbi ON pbi.Codigo=pbc.Codigo
    LEFT JOIN contas con ON con.Codigo=pbc.Conta
    WHERE pbi.SitCobr = 3
    AND pbi.PrevCod=0
    AND pbi.Cond=:P0
    AND pbc.Codigo NOT IN
    (
    SELECT Codigo
    FROM previts
    WHERE Codigo=:P1
    )
  }

  DmCond.QrNIO_A.Close;
  DmCond.QrNIO_A.SQL.Clear;
  DmCond.QrNIO_A.SQL.Add('SELECT pbc.Codigo, pbc.Nome, pbc.Conta,');
  DmCond.QrNIO_A.SQL.Add('pbi.Valor, pbi.SitCobr, pbi.Texto, ');
  DmCond.QrNIO_A.SQL.Add('pbi.Controle, con.Nome NOMECON');
  DmCond.QrNIO_A.SQL.Add('FROM prevbac pbc');
  DmCond.QrNIO_A.SQL.Add('LEFT JOIN prevbai pbi ON pbi.Codigo=pbc.Codigo');
  DmCond.QrNIO_A.SQL.Add('LEFT JOIN contas con ON con.Codigo=pbc.Conta');
  DmCond.QrNIO_A.SQL.Add('WHERE pbi.SitCobr = 3');
  DmCond.QrNIO_A.SQL.Add('AND pbi.PrevCod=0');
  DmCond.QrNIO_A.SQL.Add('AND pbi.Cond=' + FormatFloat('0',
    DmCond.QrCondCodigo.Value));
  DmCond.QrNIO_A.SQL.Add('AND pbc.Codigo NOT IN');
  DmCond.QrNIO_A.SQL.Add('(');
  DmCond.QrNIO_A.SQL.Add('  SELECT Codigo');
  DmCond.QrNIO_A.SQL.Add('  FROM ' + DmCond.FTabPriA);
  DmCond.QrNIO_A.SQL.Add('  WHERE Codigo=' + FormatFloat('0',
    QrPrevCodigo.Value));
  DmCond.QrNIO_A.SQL.Add(')');
  {
    DmCond.QrNIO_A.Params[00].AsInteger := DmCond.QrCondCodigo.Value;
    DmCond.QrNIO_A.Params[01].AsInteger := QrPrevCodigo.Value;
  }
  UMyMod.AbreQuery(DmCond.QrNIO_A, Dmod.MyDB);
  //
  if DmCond.QrNIO_A.RecordCount = 0 then
  begin
    Geral.MB_Aviso('N�o h� nenhum item agendado para este condom�nio!');
    Exit;
  end
  else
  begin
    if DBCheck.CriaFm(TFmPrevBaA, FmPrevBaA, afmoNegarComAviso) then
    begin
      FmPrevBaA.ShowModal;
      FmPrevBaA.Destroy;
    end;
  end;
end;

procedure TFmCondGer.Agendamentoporvarreduradelanamentos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmPrevVeri, FmPrevVeri, afmoNegarComAviso) then
  begin
    FmPrevVeri.FCodEnti := DmCond.QrCondCliente.Value;
    FmPrevVeri.FCodCliI := DmCond.QrCondCodigo.Value;
    FmPrevVeri.FTabLctA := DmCond.FTabLctA;
    FmPrevVeri.FTabPrvA := DmCond.FTabPrvA;
    FmPrevVeri.ShowModal;
    FmPrevVeri.Destroy;
  end;
end;

procedure TFmCondGer.Itematual1Click(Sender: TObject);
begin
  // AdicionaItemABloqueto(istAtual);
end;

procedure TFmCondGer.Selecionados4Click(Sender: TObject);
begin
  // AdicionaItemABloqueto(istSelecionados);
end;

procedure TFmCondGer.Local1Click(Sender: TObject);
begin
  MyPrinters.EmiteCheque(DmLct2.QrLctControle.Value, DmLct2.QrLctSub.Value,
    DmLct2.QrCrtBanco1.Value, 0, '', 0, '', DmLct2.QrLctSerieCH.Value,
    DmLct2.QrLctDocumento.Value, DmLct2.QrLctData.Value,
    DmLct2.QrLctCarteira.Value, 'NOMEFORNECEDOR', TDBGrid(DBGLct),
    DmLct2.FTabLctA, -1, 0, 0);
end;

procedure TFmCondGer.Todositens2Click(Sender: TObject);
begin
  // AdicionaItemABloqueto(istTodos);
end;

function TFmCondGer.PeriodoNaoDefinido: Boolean;
begin
  if QrPrev.State = dsInactive then
    LocalizarPeriodoAtual;
  Result := QrPrev.State = dsInactive;
end;

procedure TFmCondGer.BtEmiteChequeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEmiteCheque, BtEmiteCheque);
end;

procedure TFmCondGer.BtDesfazOrdenacaoClick(Sender: TObject);
var
  Query: TmySQLQuery;
  Controle: Integer;
begin
  if DBGLct.DataSource.DataSet is TmySQLQuery then
    Query := TmySQLQuery(DBGLct.DataSource.DataSet)
  else
    Query := nil;
  if Query <> nil then
  begin
    Query.SortFieldNames := '';
    if Query.State <> dsInactive then
    begin
      Controle := Query.FieldByName('Controle').AsInteger;
      Query.Close;
      Query.Open;
      Query.Locate('Controle', Controle, []);
    end;
  end;
end;

procedure TFmCondGer.BtAgendaProvShowClick(Sender: TObject);
begin
  DmCond.AgendamentoDeProvisaoEfetivado(DmLct2.QrLctControle.Value,
    DmCond.FTabPrvA, True);
end;

procedure TFmCondGer.BtConfContasCadClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConfContasCad, BtConfContasCad);
end;

procedure TFmCondGer.BtConfContasExeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMConfContasExe, BtConfContasExe);
end;

procedure TFmCondGer.BtReopen2Click(Sender: TObject);
begin
  DmBloq.Reopen_BLC_BLE();
end;

procedure TFmCondGer.BtReverte2Click(Sender: TObject);
  procedure ReverterCompensacaoBLE();
  var
    i, k: Integer;
  begin
    if DBGBLE.SelectedRows.Count > 1 then
    begin
      if Geral.MensagemBox('Confirma a revers�o da compensa��o ' +
        'dos itens selecionados?', 'Pergunta', MB_ICONQUESTION + MB_YESNOCANCEL)
        = ID_YES then
      begin
        with DBGBLE.DataSource.DataSet do
          for i := 0 to DBGBLE.SelectedRows.Count - 1 do
          begin
            // GotoBookmark(pointer(DBGBLE.SelectedRows.Items[i]));
            GotoBookmark(DBGBLE.SelectedRows.Items[i]);
            if DmBloq.QrBLESit.Value = 3 then
              // FmPrincipal.ReverterPagtoEmissao(DmBloq.QrBLE, nil, False);
              UFinanceiro.ReverterPagtoEmissao(DmBloq.QrBLE, nil, False, False,
                False, DmLct2.FTabLctA);
          end;
        k := DmBloq.QrBLEControle.Value;
        RecalcSaldoCarteira();
        DmBloq.QrBLE.Locate('Controle', k, []);
      end;
    end
    else // FmPrincipal.ReverterPagtoEmissao(DmBloq.QrBLE, nil, True);
      UFinanceiro.ReverterPagtoEmissao(DmBloq.QrBLC, nil, False, False, False,
        DmLct2.FTabLctA);
  end;

var
  Comp: Boolean;
  Desc: Integer;
begin
  if DmBloq.QrBLE.RecordCount > 0 then
  begin
    Comp := False;
    Desc := ID_NO;
    DmBloq.QrBLE.First;
    while not DmBloq.QrBLE.Eof do
    begin
      if DmBloq.QrBLECompensado.Value > 0 then
      begin
        Comp := True;
        if Desc = ID_NO then
          Desc := Geral.MensagemBox('Os lan�amentos deste bloqueto ' +
            'n�o podem ser exclu�dos pois j� tem iten(s) quitado(s)!' + #13 +
            #10 + 'Deseja descompens�-los?', 'Pergunta',
            MB_YESNOCANCEL + MB_ICONQUESTION);
        if Desc = ID_YES then
          ReverterCompensacaoBLE()
        else if Desc = ID_CANCEL then
          Exit;
      end;
      DmBloq.QrBLE.Next;
    end;
    if Comp then
      Exit;
    if Geral.MensagemBox('Confirma a exclus�o dos itens duplicados?',
      'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES then
    begin
      {
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add(EXCLUI_DE + DmLct2.FTabLctA + '');
        Dmod.QrUpd.SQL.Add('WHERE FatID>=600');
        Dmod.QrUpd.SQL.Add('AND CliInt=:P0');
        Dmod.QrUpd.SQL.Add('AND Mez=:P1');
        Dmod.QrUpd.SQL.Add('AND ForneceI=:P2');
        Dmod.QrUpd.SQL.Add('AND Depto=:P3');
        Dmod.QrUpd.SQL.Add('AND FatNum <> :P4');
        //
        Dmod.QrUpd.Params := DmBloq.QrBLE.Params;
        Dmod.QrUpd.ExecSQL;
        //
      }
      DmodFin.QrLcts.Close;
      DmodFin.QrLcts.SQL.Clear;
      DmodFin.QrLcts.SQL.Add
        ('SELECT Data, Tipo, Carteira, Controle, Sub, FatID');
      DmodFin.QrLcts.SQL.Add('FROM ' + DmLct2.FTabLctA);
      DmodFin.QrLcts.SQL.Add('WHERE FatID>=600');
      DmodFin.QrLcts.SQL.Add('AND CliInt=:P0');
      DmodFin.QrLcts.SQL.Add('AND Mez=:P1');
      DmodFin.QrLcts.SQL.Add('AND ForneceI=:P2');
      DmodFin.QrLcts.SQL.Add('AND Depto=:P3');
      DmodFin.QrLcts.SQL.Add('AND FatNum <> :P4');
      Dmod.QrUpd.Params := DmBloq.QrBLE.Params;
      DmodFin.QrLcts.Open;
      while not DmodFin.QrLcts.Eof do
      begin
        if DmodFin.QrLctsFatID.Value <> 0 then
        begin
          Geral.MensagemBox('Lan�amento espec�fico. Para excluir ' +
            'este item selecione sua janela correta!', 'Erro',
            MB_OK + MB_ICONERROR);
          Exit;
        end;
        //
        UFinanceiro.ExcluiLct_Unico(DmLct2.FTabLctA, Dmod.MyDB,
          DmodFin.QrLctsData.Value, DmodFin.QrLctsTipo.Value,
          DmodFin.QrLctsCarteira.Value, DmodFin.QrLctsControle.Value,
          DmodFin.QrLctsSub.Value, dmkPF.MotivDel_ValidaCodigo(308), True);
        //
        DmodFin.QrLcts.Next;
      end;
      //
      DmBloq.Reopen_BLC_BLE();
    end;
  end;
end;

procedure TFmCondGer.BtItemCNABClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCondGerStep, FmCondGerStep, afmoNegarComAviso) then
  begin
    FmCondGerStep.QrLei_Step.Close;
    FmCondGerStep.QrLei_Step.Params[00].AsFloat :=
      DmBloq.QrBoletosBLOQUETO.Value;
    FmCondGerStep.QrLei_Step.Params[01].AsInteger := DmCond.QrCondCliente.Value;
    FmCondGerStep.QrLei_Step.Open;
    FmCondGerStep.ShowModal;
    FmCondGerStep.Destroy;
  end;
end;

procedure TFmCondGer.BtProtoCDClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMProtoCD, BtProtoCD);
end;

procedure TFmCondGer.BtProtocoloClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCondGerProto, FmCondGerProto, afmoNegarComAviso) then
  begin
    FmCondGerProto.FID_Cod1 := QrPrevCodigo.Value;
    FmCondGerProto.FID_Cod2 := QrPrevCond.Value;
    FmCondGerProto.FPeriodo := QrPrevPeriodo.Value;
    FmCondGerProto.FCliEnti := DmCond.QrCondCliente.Value;
    FmCondGerProto.FCedente := DmBloq.QrCNAB_Cfg_B.FieldByName('Cedente')
      .AsInteger;
    FmCondGerProto.FPercMulta := DmBloq.QrCNAB_Cfg_B.FieldByName
      ('MultaPerc').AsFloat;
    FmCondGerProto.FPercJuros := DmBloq.QrCNAB_Cfg_B.FieldByName
      ('JurosPerc').AsFloat;
    FmCondGerProto.FTabLctA := DmCond.FTabLctA;
    FmCondGerProto.FTabAriA := DmCond.FTabAriA;
    FmCondGerProto.FTabCnsA := DmCond.FTabCnsA;
    FmCondGerProto.ShowModal;
    FmCondGerProto.Destroy;
    // Mostrar protocolos (se) rec�m criados
    DmBloq.ReopenBoletos(DmBloq.QrBoletosBOLAPTO.Value);
  end;
end;

procedure TFmCondGer.Exclusoincondicional1Click(Sender: TObject);
begin
  if not DBCheck.LiberaPelaSenhaBoss then
    Exit;
  if DmLct2.QrLctData.Value < VAR_DATA_MINIMA then
  begin
    Geral.MensagemBox
      ('O lan�amento selecionado n�o ser� excluido pois pertence a um m�s encerrado!',
      VAR_APPNAME, MB_OK + MB_ICONWARNING);
    Exit;
  end;
  if Geral.MensagemBox('Confirma a exclus�o INCONDICIONAL deste lan�amento?',
    VAR_APPNAME, MB_ICONQUESTION + MB_YESNOCANCEL) = ID_YES then
  begin
    {
      Dmod.QrUpdM.SQL.Clear;
      Dmod.QrUpdM.SQL.Add(EXCLUI_DE + DmCond.FTabLctA + ' WHERE Controle=:P0 AND Sub=:P1 ');
      Dmod.QrUpdM.SQL.Add('AND Carteira=:P2 AND Tipo=:P3 AND Credito=:P4 AND Debito=:P5 ');
      Dmod.QrUpdM.Params[00].AsFloat   := DmLct2.QrLctControle.Value;
      Dmod.QrUpdM.Params[01].AsInteger := DmLct2.QrLctSub.Value;
      Dmod.QrUpdM.Params[02].AsInteger := DmLct2.QrLctCarteira.Value;
      Dmod.QrUpdM.Params[03].AsInteger := DmLct2.QrLctTipo.Value;
      Dmod.QrUpdM.Params[04].AsFloat   := DmLct2.QrLctCredito.Value;
      Dmod.QrUpdM.Params[05].AsFloat   := DmLct2.QrLctDebito.Value;
      Dmod.QrUpdM.ExecSQL;
    }
    UFinanceiro.ExcluiLct_Unico(DmCond.FTabLctA, Dmod.MyDB,
      DmLct2.QrLctData.Value, DmLct2.QrLctTipo.Value,
      DmLct2.QrLctCarteira.Value, DmLct2.QrLctControle.Value,
      DmLct2.QrLctSub.Value, dmkPF.MotivDel_ValidaCodigo(301), True);
    //
    RecalcSaldoCarteira();
  end;
end;

procedure TFmCondGer.Localizarlanamentoorigem1Click(Sender: TObject);
begin
  (*
    UFinanceiro.LocalizarlanctoCliInt(DmLct2.QrLctID_Pgto.Value, 0,
    DmCond.QrCondCliente.Value, TPDataIni, DmLct2.QrLct,
    DmLct2.QrCrt, DmLct2.QrCrtSum, True, DmLct2.FTabLctA);
  *)
  LocalizarOLan�amentoDeOrigem();
end;

procedure TFmCondGer.Localizarolanamentodeorigem1Click(Sender: TObject);
begin
  LocalizarOLan�amentoDeOrigem();
end;

procedure TFmCondGer.LocalizarOLan�amentoDeOrigem;
var
  Lancto: Integer;
begin
  Lancto := DmLct2.QrLctID_Pgto.Value;
  //
  if Lancto = 0 then
    Lancto := DmLct2.QrLctID_Quit.Value;
  if Lancto = 0 then
    Lancto := DmLct2.QrLctCtrlQuitPg.Value;
  //
  UFinanceiro.LocalizarlanctoCliInt(Lancto, 0, FThisEntidade, TPDataIni,
    DmLct2.QrLct, DmLct2.QrCrt, DmLct2.QrCrtSum, True, DmCond.FTabLctA,
    DmCond.FTabLctB, DmCond.FTabLctD);
end;

procedure TFmCondGer.SpeedButton5Click(Sender: TObject);
begin
  if dmkEdlocLancto.ValueVariant = '' then
  begin
    Geral.MB_Aviso('Lan�amento n�o definido!');
    Exit;
  end;
  UFinanceiro.LocalizarlanctoCliInt(dmkEdlocLancto.ValueVariant, -1,
    DmCond.QrCondCliente.Value, TPDataIni, DmLct2.QrLct, DmLct2.QrCrt,
    DmLct2.QrCrtSum, True, DmCond.FTabLctA, DmCond.FTabLctB, DmCond.FTabLctD);
  Geral.WriteAppKeyCU('LocLancto', Application.Title,
    dmkEdlocLancto.ValueVariant, ktInteger);
end;

procedure TFmCondGer.InsAlt(Acao: TGerencimantoDeRegistro);
const
  AlteraAtehFatID = False;
begin
  if UFinanceiro.InclusaoLancamento(TFmLctEdit, FmLctEdit, lfCondominio,
    afmoNegarComAviso, DmLct2.QrLct, DmLct2.QrCrt, Acao,
    DmLct2.QrLctControle.Value, DmLct2.QrLctSub.Value, 0 (* Genero.Value *) ,
    Dmod.QrControle.FieldByName('MoraDD').AsFloat,
    Dmod.QrControle.FieldByName('Multa').AsFloat, nil (* SetaVars *) ,
    0 (* FatID *) , 0 (* FatID_Sub *) , 0 (* FatNum *) , 0 (* Carteira *) ,
    0 (* Credito *) , 0 (* Debito *) , AlteraAtehFatID, 0 (* Cliente *) ,
    0 (* Fornecedor *) , FThisEntidade (* cliInt *) , 0 (* ForneceI *) ,
    0 (* Account *) , 0 (* Vendedor *) , True (* LockCliInt *) ,
    False (* LockForneceI *) , False (* LockAccount *) ,
    False (* LockVendedor *) , 0 (* Data *) , 0 (* Vencto *) , 0 (* DataDoc *) ,
    2 (* IDFinalidade *) , 0 (* Mes: TDateTime *) , DmLct2.FTabLctA,
    0 (* FisicoSrc *) , 0 (* FisicoCod *) ) > 0 then
  begin
    DmLct2.LocCod(DmLct2.QrCrtCodigo.Value, DmLct2.QrCrtCodigo.Value,
      DmLct2.QrCrt, '');
    DmLct2.QrLct.Locate('Controle', FLAN_Controle, []);
  end;
end;

procedure TFmCondGer.ImprimeBoleto(Selecao: TSelType; EscolheModelo: Boolean);
var
  LoteStr: String;
  SdoJaAtz: Boolean;
begin
  case Selecao of
    istAtual:
      begin
        if ImpedePorAlgumMotivo(istAtual) then
          Exit;
        //
        SdoJaAtz := False;
        //
        if DmBloq.QrBoletosVencto.Value > 1 then
        begin
          if EscolheModelo then
            DmBloq.ImprimeBoletos_Novo(istAtual, ficMostra, '', nil,
              0, SdoJaAtz)
          else
            DmBloq.ImprimeBoletos_Novo(istAtual, ficMostra, '', nil,
              QrPrevCodigo.Value, SdoJaAtz)
        end
        else
          Geral.MB_Aviso('Informe o vencimento antes de imprimir!');
      end;
    istMarcados:
      begin
        if ImpedePorAlgumMotivo(istMarcados) then
          Exit;
        //
        SdoJaAtz := False;
        LoteStr := FormatFloat('0', DmBloq.QrBoletosLOTE_PROTOCO.Value);
        //
        if InputQuery('Informe o lote', 'Lote n�mero:', LoteStr) then
        begin
          FLoteImp := Geral.IMV(LoteStr);
          if Geral.MensagemBox('Confirma a impress�o dos bloquetos deste ' +
            'cliente neste per�odo do LOTE "' + IntToStr(FLoteImp) + '"?',
            'Pergunta', MB_ICONQUESTION + MB_YESNOCANCEL) = ID_YES then
          begin
            if TodosSelecionadosTemVencimento(istMarcados) then
            begin
              DmBloq.ImprimeBoletos_Novo(istMarcados, ficMostra, '', nil,
                QrPrevCodigo.Value, SdoJaAtz);
            end;
          end;
        end;
      end;
    istSelecionados:
      begin
        if ImpedePorAlgumMotivo(istSelecionados) then
          Exit;
        //
        SdoJaAtz := False;
        //
        if TodosSelecionadosTemVencimento(istSelecionados) then
        begin
          DmBloq.ImprimeBoletos_Novo(istSelecionados, ficMostra, '', nil,
            QrPrevCodigo.Value, SdoJaAtz);
        end;
      end;
    istTodos:
      begin
        if ImpedePorAlgumMotivo(istTodos) then
          Exit;
        //
        SdoJaAtz := False;
        if TodosSelecionadosTemVencimento(istTodos) then
        begin
          DmBloq.ImprimeBoletos_Novo(istTodos, ficMostra, '', nil,
            QrPrevCodigo.Value, SdoJaAtz);
        end;
      end;
  end;
end;

procedure TFmCondGer.ImprimeCarndebloquetos1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCondGerCarne, FmCondGerCarne, afmoNegarComAviso) then
  begin
    FmCondGerCarne.FCond := DmCond.QrCondCodigo.Value;
    FmCondGerCarne.ShowModal;
    FmCondGerCarne.Destroy;
    // Reabrir tabelas terce�rias para evitar incorre��es
    DmBloq.ReopenBoletos('');
  end;
end;

procedure TFmCondGer.Imveis1Click(Sender: TObject);
begin
  FmPrincipal.FormImprimeImoveis(DmCond.QrCondCodigo.Value);
end;

function TFmCondGer.FormataPeriodo(Periodo, FmtType: Integer): String;
var
  DataI, DataF: TDateTime;
begin
  Result := '';
  DataI := MLAGeral.PrimeiroDiaDoPeriodo_Date(Periodo);
  DataF := MLAGeral.UltimoDiaDoPeriodo_Date(Periodo);
  case FmtType of
    // dd/mm/aa a dd/mm/aa
    0:
      Result := Geral.FDT(DataI, 3) + ' a ' + Geral.FDT(DataF, 3);
    // dd a dd/mm/aa
    1:
      Result := Geral.FDT(DataI, 16) + ' a ' + Geral.FDT(DataF, 3);
    // mmmm/aa
    2:
      Result := Geral.FDT(DataI, 17);
    // mmm/aa
    3:
      Result := Geral.FDT(DataI, 18);
    // mm/aa
    4:
      Result := Geral.FDT(DataI, 19);
  else
    Result := '???'
  end;
end;

procedure TFmCondGer.Rateiraconsumo1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmCondGerLei2, FmCondGerLei2, afmoNegarComAviso) then
  begin
    FmCondGerLei2.FPeriodo := QrPrevPeriodo.Value;
    FmCondGerLei2.FCond := QrPrevCond.Value;
    FmCondGerLei2.F_CliInt := DmCond.QrCondCliente.Value;
    FmCondGerLei2.EdPeriodo.Text := dmkPF.MesEAnoDoPeriodo(QrPrevPeriodo.Value);
    FmCondGerLei2.STCli.Caption := DmCond.QrCondNOMECLI.Value;
    //
    FmCondGerLei2.QrCons.Close;
    FmCondGerLei2.QrCons.Database := Dmod.MyDB;
    FmCondGerLei2.QrCons.Params[0].AsInteger := QrPrevCond.Value;
    FmCondGerLei2.QrCons.Open;
    //
    FmCondGerLei2.ShowModal;
    FmCondGerLei2.Destroy;
    //
    DmCond.ReopenQrCons(DmCond.QrConsCodigo.Value);
  end;
end;

procedure TFmCondGer.Listadecomposiesdearrecadaes1Click(Sender: TObject);
begin
  FmCondGerImpGer.ImprimeComposicoesDeArrecadacoes();
end;

procedure TFmCondGer.ListadeProvises1Click(Sender: TObject);
begin
end;

// melhorar (terminar) frxCondH
procedure TFmCondGer.frxCondAGetValue(const VarName: String;
  var Value: Variant);
var
  DVB, LocalEData, UH, NossoNumero_Rem: String;
  ModelBloq, Banco: Integer;
  Status: Boolean;
begin
  Status := DmBloq.QrCNAB_Cfg_BStatus.Value = 0;
  //
  if AnsiCompareText(VarName, 'PERIODODATE_PBB') = 0 then
    Value := FormatDateTime(VAR_FORMATDATE3,
      MLAGeral.PrimeiroDiaDoPeriodo_Date(FmCondGer.QrPrevPeriodo.Value - 1 +
      DmCond.QrCondPBB.Value)) + ' a ' + FormatDateTime(VAR_FORMATDATE3,
      MLAGeral.UltimoDiaDoPeriodo_Date(FmCondGer.QrPrevPeriodo.Value - 1 +
      DmCond.QrCondPBB.Value))
  else if AnsiCompareText(VarName, 'PERIODODATE_PSB') = 0 then
    Value := FormatDateTime(VAR_FORMATDATE3,
      MLAGeral.PrimeiroDiaDoPeriodo_Date(FmCondGer.QrPrevPeriodo.Value - 1 +
      DmCond.QrCondPSB.Value)) + ' a ' + FormatDateTime(VAR_FORMATDATE3,
      MLAGeral.UltimoDiaDoPeriodo_Date(FmCondGer.QrPrevPeriodo.Value - 1 +
      DmCond.QrCondPSB.Value))
  else if AnsiCompareText(VarName, 'VARF_FIM_PREVISAO') = 0 then
  begin
    Value := FmCondGer.FCompensaAltura;
  end
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_TITULO') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucaoArrecadacao
      (DmBloq.QrConfigBolTitAAptTex.Value,
      DmBloq.QrInquilinoPROPRI_E_MORADOR.Value, DmBloq.QrBoletosUnidade.Value,
      DmCond.QrCondNOMECLI.Value, DmCond.QrCondDOCNUM_TXT.Value,
      DmBloq.QrBoletosFracaoIdeal.Value)
  else if AnsiCompareText(VarName, 'VARF_HideCompe') = 0 then
    Value := DmCond.QrCondHideCompe.Value
  else if AnsiCompareText(VarName, 'VARF_MESANO') = 0 then
    Value := dmkPF.MesEAnoDoPeriodo(FmCondGer.QrPrevPeriodo.Value)
  else if AnsiCompareText(VarName, 'VARF_MESANOA') = 0 then
  begin
    if FmCondGer.QrPrevModelBloq.Value in [8, 11] then
    // Modelos configur�veis seta o per�odo na configura��o
      Value := dmkPF.MesEAnoDoPeriodo(FmCondGer.QrPrevPeriodo.Value +
        DmBloq.QrConfigBolMesCompet.Value - 1)
    else
      Value := dmkPF.MesEAnoDoPeriodo(FmCondGer.QrPrevPeriodo.Value +
        DmCond.QrCondPBB.Value - 1);
  end
  else if AnsiCompareText(VarName, 'VARF_PROXIMOPERIODO') = 0 then
  begin
    if FmCondGer.QrPrevModelBloq.Value in [8, 11] then
    // Modelos configur�veis seta o per�odo na configura��o
      Value := dmkPF.MesEAnoDoPeriodo(FmCondGer.QrPrevPeriodo.Value +
        DmBloq.QrConfigBolMesCompet.Value - 1)
    else
      Value := dmkPF.MesEAnoDoPeriodo(FmCondGer.QrPrevPeriodo.Value +
        DmCond.QrCondPBB.Value - 1);
  end
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_1') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(1, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_2') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(2, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_3') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(3, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_4') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(4, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_5') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(5, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_6') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(6, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_7') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(7, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_8') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(8, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_9') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(9, 0)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_NOM_10') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(10, 0)

  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_1') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(1, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_2') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(2, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_3') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(3, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_4') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(4, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_5') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(5, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_6') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(6, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_7') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(7, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_8') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(8, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_9') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(9, 1)
  else if AnsiCompareText(VarName, 'VARF_ARRECADA_VAL_10') = 0 then
    Value := FmCondGer.ObtemValorQrBoletosIts(10, 1)

  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_1') = 0 then
    Value := FmCondGer.QrPrevAviso01.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_2') = 0 then
    Value := FmCondGer.QrPrevAviso02.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_3') = 0 then
    Value := FmCondGer.QrPrevAviso03.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_4') = 0 then
    Value := FmCondGer.QrPrevAviso04.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_5') = 0 then
    Value := FmCondGer.QrPrevAviso05.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_6') = 0 then
    Value := FmCondGer.QrPrevAviso06.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_7') = 0 then
    Value := FmCondGer.QrPrevAviso07.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_8') = 0 then
    Value := FmCondGer.QrPrevAviso08.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_9') = 0 then
    Value := FmCondGer.QrPrevAviso09.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_10') = 0 then
    Value := FmCondGer.QrPrevAviso10.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_11') = 0 then
    Value := FmCondGer.QrPrevAviso11.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_12') = 0 then
    Value := FmCondGer.QrPrevAviso12.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_13') = 0 then
    Value := FmCondGer.QrPrevAviso13.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_14') = 0 then
    Value := FmCondGer.QrPrevAviso14.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_15') = 0 then
    Value := FmCondGer.QrPrevAviso15.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_16') = 0 then
    Value := FmCondGer.QrPrevAviso16.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_17') = 0 then
    Value := FmCondGer.QrPrevAviso17.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_18') = 0 then
    Value := FmCondGer.QrPrevAviso18.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_19') = 0 then
    Value := FmCondGer.QrPrevAviso19.Value
  else if AnsiCompareText(VarName, 'VARF_MENSAGEM_20') = 0 then
    Value := FmCondGer.QrPrevAviso20.Value

  else if AnsiCompareText(VarName, 'VARF_Resumo_SaldoAnt') = 0 then
    Value := FmCondGer.FResumo_SaldoAnt
  else if AnsiCompareText(VarName, 'VARF_Resumo_Receitas') = 0 then
    Value := FmCondGer.FResumo_Receitas
  else if AnsiCompareText(VarName, 'VARF_Resumo_Despesas') = 0 then
    Value := FmCondGer.FResumo_Despesas
  else if AnsiCompareText(VarName, 'VARF_Resumo_SaldoMes') = 0 then
    Value := FmCondGer.FResumo_SaldoMes
  else if AnsiCompareText(VarName, 'VARF_Resumo_SaldoTrf') = 0 then
    Value := FmCondGer.FResumo_SaldoTrf
  else if AnsiCompareText(VarName, 'VARF_INADIMP_U') = 0 then
  begin
    // 2010-10-26
    DmBloq.ReopenPendU(FmCondGer.QrPrevCondCli.Value,
      DmBloq.QrBoletosApto.Value);
    Value := -DmBloq.QrPendUSALDO.Value;
  end
  else if AnsiCompareText(VarName, 'VARF_INADIMP_T') = 0 then
  begin
    Value := -DmBloq.QrPendTSALDO.Value;
  end
  else if AnsiCompareText(VarName, 'VARF_MBB') = 0 then
    Value := DmCond.QrCondMBB.Value
  else if AnsiCompareText(VarName, 'VARF_MRB') = 0 then
    Value := DmCond.QrCondMRB.Value
  else if AnsiCompareText(VarName, 'VARF_MSB') = 0 then
    Value := DmCond.QrCondMSB.Value
  else if AnsiCompareText(VarName, 'VARF_MSP') = 0 then
    Value := DmCond.QrCondMSP.Value
  else if AnsiCompareText(VarName, 'VARF_MIB') = 0 then
    Value := DmCond.QrCondMIB.Value
  else if AnsiCompareText(VarName, 'VARF_MPB') = 0 then
    Value := DmCond.QrCondMPB.Value
  else if AnsiCompareText(VarName, 'VARF_MAB') = 0 then
    Value := DmCond.QrCondMAB.Value
  else if AnsiCompareText(VarName, 'VARF_URL') = 0 then
    Value := DModG.QrOpcoesGerl.FieldByName('Web_MyURL').AsString
  else if AnsiCompareText(VarName, 'PERIODODATE_PBB_H1') = 0 then
    Value := FmCondGer.FormataPeriodo(FmCondGer.QrPrevPeriodo.Value - 1 +
      DmCond.QrCondPBB.Value, DmBloq.QrConfigBolTitBPerFmt.Value)
  else if AnsiCompareText(VarName, 'PERIODODATE_PBB_H2') = 0 then
    Value := FmCondGer.FormataPeriodo(FmCondGer.QrPrevPeriodo.Value - 1 +
      DmCond.QrCondPBB.Value, DmBloq.QrConfigBolTitRPerFmt.Value)
  else if AnsiCompareText(VarName, 'PERIODODATE_PSB_H1') = 0 then
    Value := FmCondGer.FormataPeriodo(FmCondGer.QrPrevPeriodo.Value - 1 +
      DmCond.QrCondPBB.Value, DmBloq.QrConfigBolTitCPerFmt.Value)
  else if AnsiCompareText(VarName, 'MeuLogoBolExiste') = 0 then
    Value := FileExists(DmBloq.QrConfigBolMeuLogoArq.Value)
  else if AnsiCompareText(VarName, 'MeuLogoImp') = 0 then
    Value := DmBloq.QrConfigBolMeuLogoImp.Value

    // frxCondRx
  else if AnsiCompareText(VarName, 'VAR_ENOMEDONO') = 0 then
    Value := DModG.QrEnderecoNOME_ENT.Value
  else if AnsiCompareText(VarName, 'VAR_ECNPJ') = 0 then
    Value := DModG.QrEnderecoCNPJ_TXT.Value
  else if AnsiCompareText(VarName, 'VAR_ENOMERUA') = 0 then
    Value := DModG.QrEnderecoNOMELOGRAD.Value + ' ' + DModG.QrEnderecoRUA.Value
  else if AnsiCompareText(VarName, 'VAR_ENUMERO') = 0 then
    Value := 'N� ' + DModG.QrEnderecoNUMERO_TXT.Value
  else if AnsiCompareText(VarName, 'VAR_ECOMPL') = 0 then
    Value := 'Compl.: ' + DModG.QrEnderecoCOMPL.Value
  else if AnsiCompareText(VarName, 'VAR_EBAIRRO') = 0 then
    Value := 'Bairro: ' + DModG.QrEnderecoBAIRRO.Value
  else if AnsiCompareText(VarName, 'VAR_ECIDADE') = 0 then
    Value := 'Cidade: ' + DModG.QrEnderecoCIDADE.Value
  else if AnsiCompareText(VarName, 'VAR_EUF') = 0 then
    Value := 'UF: ' + DModG.QrEnderecoNOMEUF.Value
  else if AnsiCompareText(VarName, 'VAR_ECEP') = 0 then
    Value := 'CEP: ' + Geral.FormataCEP_NT(DModG.QrEnderecoCEP.Value)
  else if AnsiCompareText(VarName, 'VAR_EPAIS') = 0 then
    Value := 'Pa�s: ' + DModG.QrEnderecoPais.Value
  else if AnsiCompareText(VarName, 'VAR_ETEL') = 0 then
    Value := 'Tel.: ' + DModG.QrEnderecoTE1_TXT.Value
    //
  else if AnsiCompareText(VarName, 'VAR_EXTENSO') = 0 then
    Value := dmkPF.ExtensoMoney(Geral.FFT(DmBloq.QrBoletosSUB_TOT.Value, 2,
      siPositivo))
  else if AnsiCompareText(VarName, 'VAR_REFERENTE') = 0 then
  begin
    if Trim(DmBloq.QrInquilinoPrefixoUH.Value) <> '' then
      UH := DmBloq.QrInquilinoPrefixoUH.Value;
    if Trim(DmBloq.QrInquilinoUnidade.Value) <> '' then
      UH := UH + ' ' + DmBloq.QrInquilinoUnidade.Value;
    Value := UpperCase('Pagamento da quota condominial - ' +
      FormatDateTime('mmm"/"yy', DmBloq.QrBoletosVencto.Value) + ' - ' + UH +
      ' - Com vencimento em ' + FormatDateTime('dd" de "mmmm" de "yyyy',
      DmBloq.QrBoletosVencto.Value) + '.')
  end
  else if AnsiCompareText(VarName, 'VAR_BNOME') = 0 then
    Value := DModG.QrEndereco2NOME_ENT.Value
  else if AnsiCompareText(VarName, 'VAR_BCNPJ') = 0 then
    Value := DModG.QrEndereco2CNPJ_TXT.Value
  else if AnsiCompareText(VarName, 'VAR_BNOMERUA') = 0 then
    Value := DModG.QrEndereco2NOMELOGRAD.Value + ' ' +
      DModG.QrEndereco2RUA.Value
  else if AnsiCompareText(VarName, 'VAR_BNUMERO') = 0 then
    Value := 'N� ' + DModG.QrEndereco2NUMERO_TXT.Value
  else if AnsiCompareText(VarName, 'VAR_BCOMPL') = 0 then
    Value := 'Compl.: ' + DModG.QrEndereco2COMPL.Value
  else if AnsiCompareText(VarName, 'VAR_BBAIRRO') = 0 then
    Value := 'Bairro: ' + DModG.QrEndereco2BAIRRO.Value
  else if AnsiCompareText(VarName, 'VAR_BCIDADE') = 0 then
    Value := 'Cidade: ' + DModG.QrEndereco2CIDADE.Value
  else if AnsiCompareText(VarName, 'VAR_BUF') = 0 then
    Value := 'UF: ' + DModG.QrEndereco2NOMEUF.Value
  else if AnsiCompareText(VarName, 'VAR_BCEP') = 0 then
    Value := 'CEP: ' + Geral.FormataCEP_NT(DModG.QrEndereco2CEP.Value)
  else if AnsiCompareText(VarName, 'VAR_BPAIS') = 0 then
    Value := 'Pa�s: ' + DModG.QrEndereco2Pais.Value
  else if AnsiCompareText(VarName, 'VAR_BTEL') = 0 then
    Value := 'Tel.: ' + DModG.QrEndereco2TE1_TXT.Value
    //
  else if AnsiCompareText(VarName, 'VAR_LOCAL') = 0 then
    Value := DModG.QrEndereco2CIDADE.Value
  else if AnsiCompareText(VarName, 'VAR_LOCALDATA') = 0 then
  begin
    LocalEData := Geral.Maiusculas
      (FormatDateTime('dddd, dd" de "mmmm" de "yyyy', Now),
      Geral.EhMinusculas(DModG.QrEndereco2CIDADE.Value, False));
    if DModG.QrEndereco2CIDADE.Value <> '' then
      LocalEData := DModG.QrEndereco2CIDADE.Value + ', ' + LocalEData;
    //
    Value := LocalEData;
  end
  else if AnsiCompareText(VarName, 'VARF_COLUNASH') = 0 then
  begin
    // Testar este. Caso n�o funcione usar o abaixo (mais lento?)
    Value := DmBloq.FColunasH;
    (*
      if DCond.QrPrevModBol.Locate('Apto', DmBloq.QrBoletosApto.Value, []) then
      ModelBloq := DCond.QrPrevModBolModelBloq.Value
      else
      ModelBloq := DmCond.QrCondModelBloq.Value;
      //
      DCond.QrCB.Close;
      DCond.QrCB.Params[0].AsInteger := ModelBloq;
      DCond.QrCB.Open;
      //
      Value := DCond.QrCBColunas.Value; // Parei aqui! est� correto?
    *)
  end

  // In�cio da Ficha de compensa��o
  else if AnsiCompareText(VarName, 'VARF_AGCodCed') = 0 then
    Value := DmBloq.QrCNAB_Cfg_B.FieldByName('AgContaCed').AsString
  else if AnsiCompareText(VarName, 'VARF_NossoNumero') = 0 then
  begin
    UBancos.GeraNossoNumero(DmBloq.QrCNAB_Cfg_B.FieldByName('ModalCobr')
      .AsInteger, DmBloq.QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
      DmBloq.QrBoletosBLOQUETO.Value,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(DmBloq.QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString),
      DmBloq.QrBoletosVencto.Value,
      DmBloq.QrCNAB_Cfg_B.FieldByName('TipoCobranca').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('EspecieDoc').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CNAB').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CtaCooper').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString, FNossoNumero,
      NossoNumero_Rem);
    //
    Value := FNossoNumero;
  end
  else if AnsiCompareText(VarName, 'VARF_CODIGOBARRAS') = 0 then
  begin
    UBancos.GeraNossoNumero(DmBloq.QrCNAB_Cfg_B.FieldByName('ModalCobr')
      .AsInteger, DmBloq.QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
      DmBloq.QrBoletosBLOQUETO.Value,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(DmBloq.QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString),
      DmBloq.QrBoletosVencto.Value,
      DmBloq.QrCNAB_Cfg_B.FieldByName('TipoCobranca').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('EspecieDoc').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CNAB').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CtaCooper').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString, FNossoNumero,
      NossoNumero_Rem);
    //
    Value := UBancos.CodigoDeBarra_BoletoDeCobranca
      (DmBloq.QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CorresAge').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedDAC_A').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedDAC_C').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CorresCto').AsString, 9, 3, 1,
      FNossoNumero, DmBloq.QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CART_IMP').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('OperCodi').AsString,
      DmBloq.QrBoletosVencto.Value, DmBloq.QrBoletosSUB_TOT.Value, 0, 0,
      not CkZerado.Checked, DmBloq.QrCNAB_Cfg_B.FieldByName('ModalCobr')
      .AsInteger, DmBloq.QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString)
    //
  end
  else if AnsiCompareText(VarName, 'VAX') = 0 then
  begin
    if (DmBloq.QrCNAB_Cfg_B.FieldByName('LayoutRem')
      .AsString = CO_756_CORRESPONDENTE_BRADESCO_2015) or
      (DmBloq.QrCNAB_Cfg_B.FieldByName('LayoutRem')
      .AsString = CO_756_CORRESPONDENTE_BB_2015) then
    begin
      Banco := DmBloq.QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger;
      //
      DVB := UBancos.DigitoVerificadorCodigoBanco(Banco);
    end
    else
    begin
      Banco := DmBloq.QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger;
      //
      if DmBloq.QrCNAB_Cfg_B.FieldByName('DVB').AsString <> '?' then
        DVB := DmBloq.QrCNAB_Cfg_B.FieldByName('DVB').AsString
      else
        DVB := UBancos.DigitoVerificadorCodigoBanco(Banco);
    end;
    Value := FormatFloat('000', Banco) + '-' + DVB;
  end
  else if AnsiCompareText(VarName, 'VARF_LINHADIGITAVEL') = 0 then
  begin
    UBancos.GeraNossoNumero(DmBloq.QrCNAB_Cfg_B.FieldByName('ModalCobr')
      .AsInteger, DmBloq.QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedAgencia').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
      DmBloq.QrBoletosBLOQUETO.Value,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
      Geral.SoNumero_TT(DmBloq.QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString),
      DmBloq.QrBoletosVencto.Value,
      DmBloq.QrCNAB_Cfg_B.FieldByName('TipoCobranca').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('EspecieDoc').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CNAB').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CtaCooper').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString, FNossoNumero,
      NossoNumero_Rem);
    //
    Value := '';
    Value := UBancos.LinhaDigitavel_BoletoDeCobranca
      (UBancos.CodigoDeBarra_BoletoDeCobranca(DmBloq.QrCNAB_Cfg_B.FieldByName
      ('CedBanco').AsInteger, DmBloq.QrCNAB_Cfg_B.FieldByName('CedAgencia')
      .AsInteger, DmBloq.QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CorresAge').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedDAC_A').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedPosto').AsInteger,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedConta').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CedDAC_C').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CorresCto').AsString, 9, 3, 1,
      FNossoNumero, DmBloq.QrCNAB_Cfg_B.FieldByName('CodEmprBco').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CartNum').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('CART_IMP').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('IDCobranca').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('OperCodi').AsString,
      DmBloq.QrBoletosVencto.Value, DmBloq.QrBoletosSUB_TOT.Value, 0, 0,
      not CkZerado.Checked, DmBloq.QrCNAB_Cfg_B.FieldByName('ModalCobr')
      .AsInteger, DmBloq.QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString))
  end
  else if AnsiCompareText(VarName, 'LogoBancoExiste') = 0 then
  begin
    if (DmBloq.QrCNAB_Cfg_B.FieldByName('LayoutRem')
      .AsString = CO_756_CORRESPONDENTE_BRADESCO_2015) or
      (DmBloq.QrCNAB_Cfg_B.FieldByName('LayoutRem')
      .AsString = CO_756_CORRESPONDENTE_BB_2015) then
      Value := FmMyGlyfs.LogoBancoExiste
        (DmBloq.QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger)
    else
      Value := FmMyGlyfs.LogoBancoExiste
        (DmBloq.QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger);
  end
  else if AnsiCompareText(VarName, 'LogoBancoPath') = 0 then
  begin
    if (DmBloq.QrCNAB_Cfg_B.FieldByName('LayoutRem')
      .AsString = CO_756_CORRESPONDENTE_BRADESCO_2015) or
      (DmBloq.QrCNAB_Cfg_B.FieldByName('LayoutRem')
      .AsString = CO_756_CORRESPONDENTE_BB_2015) then
      Value := FmMyGlyfs.CaminhoLogoBanco
        (DmBloq.QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger)
    else
      Value := FmMyGlyfs.CaminhoLogoBanco
        (DmBloq.QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger);
  end
  else if AnsiCompareText(VarName, 'VAR_VALORMMULTA') = 0 then
  begin
    Value := (DmBloq.QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat *
      DmBloq.QrBoletosSUB_TOT.Value / 100) + DmBloq.QrBoletosSUB_TOT.Value;
    Value := Geral.FFT(Value, 2, siPositivo);
  end
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO1') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao
      (DmBloq.QrCNAB_Cfg_B.FieldByName('Texto01').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat,
      DmBloq.QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat,
      DmBloq.QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO2') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao
      (DmBloq.QrCNAB_Cfg_B.FieldByName('Texto02').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat,
      DmBloq.QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat,
      DmBloq.QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO3') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao
      (DmBloq.QrCNAB_Cfg_B.FieldByName('Texto03').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat,
      DmBloq.QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat,
      DmBloq.QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO4') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao
      (DmBloq.QrCNAB_Cfg_B.FieldByName('Texto04').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat,
      DmBloq.QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat,
      DmBloq.QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO5') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao
      (DmBloq.QrCNAB_Cfg_B.FieldByName('Texto05').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat,
      DmBloq.QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat,
      DmBloq.QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO6') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao
      (DmBloq.QrCNAB_Cfg_B.FieldByName('Texto06').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat,
      DmBloq.QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat,
      DmBloq.QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO7') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao
      (DmBloq.QrCNAB_Cfg_B.FieldByName('Texto07').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat,
      DmBloq.QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat,
      DmBloq.QrBoletosSUB_TOT.Value)
  else if AnsiCompareText(VarName, 'VAR_INSTRUCAO8') = 0 then
    Value := FmCondGerImpGer.TraduzInstrucao
      (DmBloq.QrCNAB_Cfg_B.FieldByName('Texto08').AsString,
      DmBloq.QrCNAB_Cfg_B.FieldByName('MultaPerc').AsFloat,
      DmBloq.QrCNAB_Cfg_B.FieldByName('JurosPerc').AsFloat,
      DmBloq.QrBoletosSUB_TOT.Value)
    // Fim Ficha de compensa��o
  else if AnsiCompareText(VarName, 'VARF_TemModuloWEB') = 0 then
    Value := FmPrincipal.FTemModuloWEB
  else if AnsiCompareText(VarName, 'VARF_STATUS') = 0 then
    Value := Status;
end;

function TFmCondGer.frxCondAUserFunction(const MethodName: String;
  var Params: Variant): Variant;
begin
  if MethodName = 'VARF_MBB' then
    Params := DmCond.QrCondMBB.Value
  else if MethodName = 'VARF_MRB' then
    Params := DmCond.QrCondMRB.Value
  else if MethodName = 'VARF_MSB' then
    Params := DmCond.QrCondMSB.Value
  else if MethodName = 'VARF_MSP' then
    Params := DmCond.QrCondMSP.Value
  else if MethodName = 'VARF_MIB' then
    Params := DmCond.QrCondMIB.Value
  else if MethodName = 'VARF_MPB' then
    Params := DmCond.QrCondMPB.Value
  else if MethodName = 'VARF_MAB' then
    Params := DmCond.QrCondMAB.Value
end;

procedure TFmCondGer.frxVersoGetValue(const VarName: String;
  var Value: Variant);
var
  Calculo: Integer;
begin
  if AnsiCompareText(VarName, 'VARF_BLQ_AltuHeader') = 0 then
    Value := Int(Dmod.QrControle.FieldByName('BLQ_TopoAvisoV').AsInteger
      / VAR_frCM)
  else if AnsiCompareText(VarName, 'VARF_BLQ_AltuRotulo') = 0 then
  begin
    Calculo := 26000 (* - 4500 *) - Dmod.QrControle.FieldByName
      ('BLQ_TopoAvisoV').AsInteger;
    Value := Int(Calculo / VAR_frCM);
  end
  else if AnsiCompareText(VarName, 'VARF_BLQ_AltuDestin') = 0 then
    Value := Dmod.QrControle.FieldByName('BLQ_AltuDestin').AsInteger / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_MEsqDestin') = 0 then
    Value := Dmod.QrControle.FieldByName('BLQ_MEsqDestin').AsInteger / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_LargDestin') = 0 then
    Value := Dmod.QrControle.FieldByName('BLQ_LargDestin').AsInteger / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_TopoDestin') = 0 then
  begin
    Calculo := Dmod.QrControle.FieldByName('BLQ_TopoDestin').AsInteger -
      Dmod.QrControle.FieldByName('BLQ_TopoAvisoV').AsInteger;
    Value := Int(Calculo / VAR_frCM);
  end
  else if AnsiCompareText(VarName, 'VARF_BLQ_AltuAvisoV') = 0 then
    Value := Dmod.QrControle.FieldByName('BLQ_AltuAvisoV').AsInteger / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_MEsqAvisoV') = 0 then
    Value := Dmod.QrControle.FieldByName('BLQ_MEsqAvisoV').AsInteger / VAR_frCM
  else if AnsiCompareText(VarName, 'VARF_BLQ_LargAvisoV') = 0 then
    Value := Dmod.QrControle.FieldByName('BLQ_LargAvisoV').AsInteger / VAR_frCM
    //
  else if AnsiCompareText(VarName, 'VARF_NOMEPROPRIET') = 0 then
    Value := DmBloq.QrBoletosNOMEPROPRIET.Value
  else if AnsiCompareText(VarName, 'VARF_LNR') = 0 then
    Value := DmBloq.QrInquilinoLNR.Value
  else if AnsiCompareText(VarName, 'VARF_LN2') = 0 then
    Value := DmBloq.QrInquilinoLN2.Value
  else if AnsiCompareText(VarName, 'VARF_AVISOVERSO') = 0 then
    Value := FmCondGer.QrPrevAvisoVerso.Value
end;

function TFmCondGer.FThisEntidade(): Integer;
begin
  Result := DmCond.QrCondCliente.Value;
end;

procedure TFmCondGer.Demonstrativodereceitasedespesas1Click(Sender: TObject);
begin
  FinanceiroJan.MostraReceDesp(DmCond.QrCondCodigo.Value);
end;

procedure TFmCondGer.PorLotedeProtocolo2Click(Sender: TObject);
begin
  ImprimeBoleto(istMarcados, False);
end;

procedure TFmCondGer.ProvisescomItens1Click(Sender: TObject);
begin
  FmCondGerImpGer.ImprimeProvisoesEItens(istExtra1, DmCond.FTabLctA);
end;

procedure TFmCondGer.ProvisesSelecionadas1Click(Sender: TObject);
begin
  FmCondGerImpGer.ImprimeProvisoesEItens(istSelecionados, DmCond.FTabLctA);
end;

procedure TFmCondGer.ProvisoAtual1Click(Sender: TObject);
begin
  FmCondGerImpGer.ImprimeProvisoesEItens(istAtual, DmCond.FTabLctA);
end;

procedure TFmCondGer.ColocarUHondenotem1Click(Sender: TObject);
var
  Txt: String;
  Controle: Integer;
begin
  if not DBCheck.LiberaPelaSenhaBoss then
    Exit;
  //
  Txt := '';
  Controle := DmLct2.QrLctControle.Value;
  if Geral.MensagemBox('Somente os lan�amentos presentes na grade ' +
    ' (conforme sele��o de carteira e per�odo) ser�o analisados! Deseja ' +
    'continuar assim mesmo?', 'Pergunta', MB_YESNOCANCEL + MB_ICONQUESTION) = ID_YES
  then
  begin
    Screen.Cursor := crHourGlass;
    DmLct2.QrLct.First;
    while not DmLct2.QrLct.Eof do
    begin
      Application.ProcessMessages;
      if (DmLct2.QrLctCliente.Value > 0) and (DmLct2.QrLctDepto.Value = 0) then
      begin
        DmCond.QrLocUHs.Close;
        DmCond.QrLocUHs.Params[0].AsInteger := DmLct2.QrLctCliente.Value;
        UMyMod.AbreQuery(DmCond.QrLocUHs, Dmod.MyDB);
        //
        case DmCond.QrLocUHs.RecordCount of
          0:
            ; // nada
          1:
            begin
              (*
                Dmod.QrUpd.SQL.Clear;
                Dmod.QrUpd.SQL.Add('UPDATE lan ctos SET AlterWeb=1, Depto=:P0 ');
                Dmod.QrUpd.SQL.Add('WHERE Controle=:P1 ');
                Dmod.QrUpd.SQL.Add('');
                Dmod.QrUpd.Params[00].AsInteger := DmCond.QrLocUHsConta.Value;
                Dmod.QrUpd.Params[01].AsInteger := DmLct2.QrLctControle.Value;
                Dmod.QrUpd.ExecSQL;
              *)
              UFinanceiro.SQLInsUpd_Lct(Dmod.QrUpd, stUpd, False, ['Depto'],
                ['Controle'], [DmCond.QrLocUHsConta.Value],
                [DmLct2.QrLctControle.Value], True, '', DmLct2.FTabLctA);
              Txt := Txt + FormatFloat('0000000000', DmLct2.QrLctControle.Value)
                + ' - ' + Geral.CompletaString(DmCond.QrLocUHsUnidade.Value,
                ' ', 20, taLeftJustify, False) + ' - ' + sLineBreak;
            end;
        else
          Geral.MensagemBox('O lan�amento ' +
            IntToStr(DmLct2.QrLctControle.Value) + ' ficou sem atualiza��o ' +
            'porque o ' + DModG.ReCaptionTexto(VAR_P_R_O_P_R_I_E_T_A_R_I_O) +
            ' tem mais de um ' + DModG.ReCaptionTexto(VAR_U_H) +
            ' no aplicativo!', 'Aviso', MB_OK + MB_ICONWARNING);
        end;
      end;
      DmLct2.QrLct.Next;
    end;
    DmLct2.QrLct.Close;
    DmLct2.QrLct.Open;
    DmLct2.QrLct.Locate('Controle', Controle, []);
    Screen.Cursor := crDefault;
  end;
  if Txt <> '' then
  begin
    Txt := 'LAN�AMENTO - ' + DModG.ReCaptionTexto(VAR_U_H) + sLineBreak + Txt;
    Geral.MensagemBox(Txt, 'Lan�amentos alterados', MB_OK + MB_ICONINFORMATION);
  end;
end;

function TFmCondGer.ImpedePorAlgumMotivo(Selecao: TSelType): Boolean;
begin
  // ver se tem algum bloqueto de condomino com restri��o jur�dica!
  Result := not TodosSelecionadosEstaoSemRestricaoJuridica(Selecao);
end;

procedure TFmCondGer.ImportaodeLantosMod01Boletos1Click(Sender: TObject);
begin
  if DmLct2.QrCrtTipo.Value = 2 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
    Application.CreateForm(TFmInadUH_Load01, FmInadUH_Load01);
    //
    FmInadUH_Load01.EdCondCod.ValueVariant := DmCond.QrCondCodigo.Value;
    FmInadUH_Load01.EdCondNome.Text := DmCond.QrCondNOMECLI.Value;
    //
    FmInadUH_Load01.EdCartCod.ValueVariant := DmLct2.QrCrtCodigo.Value;
    FmInadUH_Load01.EdCartNome.Text := DmLct2.QrCrtNome.Value;
    //
    FmInadUH_Load01.ShowModal;
    FmInadUH_Load01.Destroy;
  end
  else
    Geral.MB_Aviso('Carteira deve ser de emiss�o!');
end;

procedure TFmCondGer.ImportaodeLantosMod02inad1Click(Sender: TObject);
begin
  if DmLct2.QrCrtTipo.Value = 2 then
  begin
    if not DBCheck.LiberaPelaSenhaBoss() then
      Exit;
    Application.CreateForm(TFmInadUH_Load02, FmInadUH_Load02);
    //
    FmInadUH_Load02.EdCondCod.ValueVariant := DmCond.QrCondCodigo.Value;
    FmInadUH_Load02.EdCondNome.Text := DmCond.QrCondNOMECLI.Value;
    //
    FmInadUH_Load02.EdCartCod.ValueVariant := DmLct2.QrCrtCodigo.Value;
    FmInadUH_Load02.EdCartNome.Text := DmLct2.QrCrtNome.Value;
    //
    FmInadUH_Load02.ShowModal;
    FmInadUH_Load02.Destroy;
  end
  else
    Geral.MB_Aviso('Carteira deve ser de emiss�o!');
end;

function TFmCondGer.Define_frxCond(frx: TfrxReport; SetaMasterData: Boolean)
  : TfrxReport;
var
  i: Integer;
  MD_ARR, MD_U_CJT, MD_Cta, MD_C_C, MD_C_C_2, MD_PRV: TfrxMasterData;
begin
  DmBloq.frxDsBoletos.DataSet := DmBloq.QrBoletos;
  DmBloq.frxDsBoletosIts.DataSet := DmBloq.QrBoletosIts;
  DmBloq.frxDsInquilino.DataSet := DmBloq.QrInquilino;
  DmBloq.frxDsConfigBol.DataSet := DmBloq.QrConfigBol;
  DmCond.frxDsCond.DataSet := DmCond.QrCond;
  DmBloq.frxDsCNAB_Cfg_B.DataSet := DmBloq.QrCNAB_Cfg_B;
  DmBloq.frxDsMov.DataSet := DmBloq.QrMov;
  DmBloq.frxDsMov3.DataSet := DmBloq.QrMov3;
  DmCond.frxDsCons.DataSet := DmCond.QrCons;
  DmCond.frxDsCNS.DataSet := DmCond.QrCNS;
  DmBloq.frxDsSdoCjt.DataSet := DmBloq.QrSdoCjt;
  // frxDsCopiaCH.DataSet    := DmCond.QrCopiaCH;

  // N � O   P O D E ! ! ! ! ! ! !
  // frx.Clear;  // Parei aqui
  DmCond.frxDsCons.DataSet := DmCond.QrCons;
  DmCond.frxDsCNS.DataSet := DmCond.QrCNS;
  frxDsPrev.DataSet := QrPrev;
  //
  MyObjects.frxDefineDataSets(frx, [DmBloq.frxDsBoletos, DmBloq.frxDsBoletosIts,
    frxDsCarts, DmCond.frxDsCond, DmBloq.frxDsCNAB_Cfg_B, DmBloq.frxDsConfigBol,
    Dmod.frxDsDono, DModG.frxDsEndereco, frxDsImpBol, DmBloq.frxDsInquilino,
    Dmod.frxDsMaster, DmBloq.frxDsMov, DmBloq.frxDsMov3, frxDsPRI,
    DmBloq.frxDsSdoCjt]);
  //
  for i := 0 to frx.Datasets.Count - 1 do
    frx.Datasets.Items[i].DataSet.Enabled := True;
  if SetaMasterData then
  begin
    MD_ARR := frx.FindObject('MD_ARR') as TfrxMasterData;
    MD_ARR.DataSet := DmBloq.frxDsBoletosIts;
    //
    MD_U_CJT := frx.FindObject('MD_U_CJT') as TfrxMasterData;
    MD_U_CJT.DataSet := DmBloq.frxDsSdoCjt;
    //
    MD_Cta := frx.FindObject('MD_Cta') as TfrxMasterData;
    MD_Cta.DataSet := DmBloq.frxDsMov3;
    //
    MD_C_C := frx.FindObject('MD_C_C') as TfrxMasterData;
    MD_C_C.DataSet := frxDsCarts;
    //
    MD_C_C_2 := frx.FindObject('MD_C_C_2') as TfrxMasterData;
    MD_C_C_2.DataSet := frxDsCarts;
    //
    MD_PRV := frx.FindObject('MD_PRV') as TfrxMasterData;
    MD_PRV.DataSet := frxDsPRI;
  end;
  //
  // FIM 2011-05-19
  //
  Result := frx;
end;

end.
