unit ModuleBloq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DbGrids, mySQLDbTables, ComCtrls, frxClass, frxDBSet, UnMLAGeral,
  dmkGeral, UnMyObjects, UnDmkProcFunc, UnDmkEnums, Vcl.ExtCtrls;

type
  TDmBloq = class(TDataModule)
    QrBolLei: TmySQLQuery;
    QrBolLeiValor: TFloatField;
    QrBolLeiApto: TIntegerField;
    QrBolLeiBOLAPTO: TWideStringField;
    QrBolLeiBoleto: TFloatField;
    QrBolArr: TmySQLQuery;
    QrBolArrValor: TFloatField;
    QrBolArrApto: TIntegerField;
    QrBolArrBOLAPTO: TWideStringField;
    QrBolArrBoleto: TFloatField;
    QrUsers: TmySQLQuery;
    QrUsersCodigoEsp: TIntegerField;
    QrUsersUsername: TWideStringField;
    QrUsersPassword: TWideStringField;
    QrPPI: TmySQLQuery;
    QrPPIPROTOCOLO: TIntegerField;
    QrPPIDataE: TDateField;
    QrPPIDataD: TDateField;
    QrPPICancelado: TIntegerField;
    QrPPIMotivo: TIntegerField;
    QrPPIID_Cod1: TIntegerField;
    QrPPIID_Cod2: TIntegerField;
    QrPPIID_Cod3: TIntegerField;
    QrPPIID_Cod4: TIntegerField;
    QrPPITAREFA: TWideStringField;
    QrPPIDELIVER: TWideStringField;
    QrPPILOTE: TIntegerField;
    QrPPICliInt: TIntegerField;
    QrPPICliente: TIntegerField;
    QrPPIPeriodo: TIntegerField;
    QrPPIDATAE_TXT: TWideStringField;
    QrPPIDATAD_TXT: TWideStringField;
    QrPPIDef_Sender: TIntegerField;
    QrPPIDocum: TFloatField;
    QrBoletos: TmySQLQuery;
    QrBoletosApto: TIntegerField;
    QrBoletosUnidade: TWideStringField;
    QrBoletosSUB_ARR: TFloatField;
    QrBoletosSUB_LEI: TFloatField;
    QrBoletosSUB_TOT: TFloatField;
    QrBoletosVencto: TDateField;
    QrBoletosPropriet: TIntegerField;
    QrBoletosNOMEPROPRIET: TWideStringField;
    QrBoletosBOLAPTO: TWideStringField;
    QrBoletosVENCTO_TXT: TWideStringField;
    QrBoletosUSERNAME: TWideStringField;
    QrBoletosPASSWORD: TWideStringField;
    QrBoletosPWD_WEB: TWideStringField;
    QrBoletosPROTOCOLO: TIntegerField;
    QrBoletosAndar: TIntegerField;
    QrBoletosBoleto: TFloatField;
    QrBoletosBLOQUETO: TFloatField;
    QrBoletosKGT: TLargeintField;
    QrBoletosLOTE_PROTOCO: TIntegerField;
    QrBoletosOrdem: TIntegerField;
    QrBoletosFracaoIdeal: TFloatField;
    DsBoletos: TDataSource;
    QrBoletosIts: TmySQLQuery;
    QrBoletosItsTEXTO: TWideStringField;
    QrBoletosItsVALOR: TFloatField;
    QrBoletosItsVencto: TDateField;
    QrBoletosItsTEXTO_IMP: TWideStringField;
    QrBoletosItsMedAnt: TFloatField;
    QrBoletosItsMedAtu: TFloatField;
    QrBoletosItsConsumo: TFloatField;
    QrBoletosItsCasas: TLargeintField;
    QrBoletosItsUnidLei: TWideStringField;
    QrBoletosItsUnidImp: TWideStringField;
    QrBoletosItsUnidFat: TFloatField;
    QrBoletosItsTipo: TLargeintField;
    QrBoletosItsVENCTO_TXT: TWideStringField;
    QrBoletosItsControle: TIntegerField;
    QrBoletosItsLancto: TIntegerField;
    QrBoletosItsGeraTyp: TLargeintField;
    QrBoletosItsGeraFat: TFloatField;
    QrBoletosItsCasRat: TLargeintField;
    QrBoletosItsNaoImpLei: TLargeintField;
    DsBoletosIts: TDataSource;
    QrBLE: TmySQLQuery;
    QrBLC: TmySQLQuery;
    QrBLCCOMPENSADO_TXT: TWideStringField;
    QrBLCFatNum: TFloatField;
    QrBLCData: TDateField;
    QrBLCTipo: TSmallintField;
    QrBLCCarteira: TIntegerField;
    QrBLCControle: TIntegerField;
    QrBLCSub: TSmallintField;
    QrBLCAutorizacao: TIntegerField;
    QrBLCGenero: TIntegerField;
    QrBLCQtde: TFloatField;
    QrBLCDescricao: TWideStringField;
    QrBLCNotaFiscal: TIntegerField;
    QrBLCDebito: TFloatField;
    QrBLCCredito: TFloatField;
    QrBLCCompensado: TDateField;
    QrBLCDocumento: TFloatField;
    QrBLCSit: TIntegerField;
    QrBLCVencimento: TDateField;
    QrBLCFatID: TIntegerField;
    QrBLCFatID_Sub: TIntegerField;
    QrBLCFatParcela: TIntegerField;
    QrBLCID_Pgto: TIntegerField;
    QrBLCID_Sub: TSmallintField;
    QrBLCFatura: TWideStringField;
    QrBLCEmitente: TWideStringField;
    QrBLCBanco: TIntegerField;
    QrBLCContaCorrente: TWideStringField;
    QrBLCCNPJCPF: TWideStringField;
    QrBLCLocal: TIntegerField;
    QrBLCCartao: TIntegerField;
    QrBLCLinha: TIntegerField;
    QrBLCOperCount: TIntegerField;
    QrBLCLancto: TIntegerField;
    QrBLCPago: TFloatField;
    QrBLCMez: TIntegerField;
    QrBLCFornecedor: TIntegerField;
    QrBLCCliente: TIntegerField;
    QrBLCCliInt: TIntegerField;
    QrBLCForneceI: TIntegerField;
    QrBLCMoraDia: TFloatField;
    QrBLCMulta: TFloatField;
    QrBLCProtesto: TDateField;
    QrBLCDataDoc: TDateField;
    QrBLCCtrlIni: TIntegerField;
    QrBLCNivel: TIntegerField;
    QrBLCVendedor: TIntegerField;
    QrBLCAccount: TIntegerField;
    QrBLCICMS_P: TFloatField;
    QrBLCICMS_V: TFloatField;
    QrBLCDuplicata: TWideStringField;
    QrBLCDepto: TIntegerField;
    QrBLCDescoPor: TIntegerField;
    QrBLCDescoVal: TFloatField;
    QrBLCDescoControle: TIntegerField;
    QrBLCUnidade: TIntegerField;
    QrBLCNFVal: TFloatField;
    QrBLCAntigo: TWideStringField;
    QrBLCExcelGru: TIntegerField;
    QrBLCLk: TIntegerField;
    QrBLCDataCad: TDateField;
    QrBLCDataAlt: TDateField;
    QrBLCUserCad: TIntegerField;
    QrBLCUserAlt: TIntegerField;
    QrBLCSerieCH: TWideStringField;
    QrBLCDoc2: TWideStringField;
    QrBLCMoraVal: TFloatField;
    QrBLCMultaVal: TFloatField;
    QrBLCCNAB_Sit: TSmallintField;
    QrBLCTipoCH: TSmallintField;
    QrBLCAlterWeb: TSmallintField;
    QrBLCReparcel: TIntegerField;
    QrBLCID_Quit: TIntegerField;
    QrBLCAtrelado: TIntegerField;
    QrBLCAtivo: TSmallintField;
    QrBLCPagMul: TFloatField;
    QrBLCPagJur: TFloatField;
    QrBLCSubPgto1: TIntegerField;
    QrBLCMultiPgto: TIntegerField;
    QrBLCProtocolo: TIntegerField;
    QrBLCCtrlQuitPg: TIntegerField;
    DsBLC: TDataSource;
    DsBLE: TDataSource;
    QrComposLIts: TmySQLQuery;
    QrComposLItsApto: TIntegerField;
    QrComposLItsMedAnt: TFloatField;
    QrComposLItsMedAtu: TFloatField;
    QrComposLItsConsumo: TFloatField;
    QrComposLItsNOMEPROPRIET: TWideStringField;
    QrComposLItsUnidade: TWideStringField;
    QrComposLItsValor: TFloatField;
    QrComposLItsControle: TIntegerField;
    QrComposL: TmySQLQuery;
    QrComposLCodigo: TIntegerField;
    QrComposLNome: TWideStringField;
    QrComposLVALOR: TFloatField;
    QrComposLCasas: TSmallintField;
    QrComposAIts: TmySQLQuery;
    QrComposAItsNOMEPROPRIET: TWideStringField;
    QrComposAItsUnidade: TWideStringField;
    QrComposAItsApto: TIntegerField;
    QrComposAItsValor: TFloatField;
    QrComposAItsPropriet: TIntegerField;
    QrComposAItsControle: TIntegerField;
    QrComposA: TmySQLQuery;
    QrComposATEXTO: TWideStringField;
    QrComposAVALOR: TFloatField;
    QrComposAConta: TIntegerField;
    QrComposAArreBaI: TIntegerField;
    DsComposA: TDataSource;
    DsComposAIts: TDataSource;
    DsComposL: TDataSource;
    DsComposLIts: TDataSource;
    QrInquilino: TmySQLQuery;
    QrInquilinoNATAL_TXT: TWideStringField;
    QrInquilinoTE1_TXT: TWideStringField;
    QrInquilinoFAX_TXT: TWideStringField;
    QrInquilinoCNPJ_TXT: TWideStringField;
    QrInquilinoE_ALL: TWideStringField;
    QrInquilinoNUMERO_TXT: TWideStringField;
    QrInquilinoECEP_TXT: TWideStringField;
    QrInquilinoPROPRI_E_MORADOR: TWideStringField;
    QrInquilinoUnidade: TWideStringField;
    QrInquilinoPrefixoUH: TWideStringField;
    QrInquilinoLNR: TWideStringField;
    QrInquilinoLN2: TWideStringField;
    QrInquilinoBloqEndTip: TSmallintField;
    QrInquilinoUsuario: TIntegerField;
    QrInquilinoCliente: TIntegerField;
    QrInquilinoPropriet: TIntegerField;
    QrInquilinoEnderLin1: TWideStringField;
    QrInquilinoEnderLin2: TWideStringField;
    QrInquilinoEnderNome: TWideStringField;
    QrDonoUH: TmySQLQuery;
    QrDonoUHNOMEPROPRIET: TWideStringField;
    QrDonoUHCNPJ_CPF: TWideStringField;
    QrDonoUHCNPJ_CPF_TXT: TWideStringField;
    QrDonoUHNOMETIPO: TWideStringField;
    QrListaA: TmySQLQuery;
    QrListaAConta: TIntegerField;
    QrListaANOME_CONTA: TWideStringField;
    QrListaAArreBaI: TIntegerField;
    QrListaATexto: TWideStringField;
    QrListaACodigo: TIntegerField;
    QrListaANOME_ARREC: TWideStringField;
    QrListaAVALOR: TFloatField;
    QrListaATEXTO_IMP: TWideStringField;
    QrListaASIGLA_CONTA: TWideStringField;
    QrListaASIGLA_IMP: TWideStringField;
    QrListaASIGLA_BAC: TWideStringField;
    QrListaAInfoRelArr: TSmallintField;
    QrListaADescriCota: TWideStringField;
    QrListaATITULO_FATOR: TWideStringField;
    QrListaADeQuem: TSmallintField;
    QrListaAPartilha: TSmallintField;
    QrListaACalculo: TSmallintField;
    QrListaAIts: TmySQLQuery;
    QrListaAItsNOMEPROPRIET: TWideStringField;
    QrListaAItsUnidade: TWideStringField;
    QrListaAItsApto: TIntegerField;
    QrListaAItsValor: TFloatField;
    QrListaAItsPropriet: TIntegerField;
    QrListaAItsControle: TIntegerField;
    QrListaAItsDeQuem: TSmallintField;
    QrListaAItsCalculo: TSmallintField;
    QrListaAItsPartilha: TSmallintField;
    QrListaAItsPercent: TFloatField;
    QrListaAItsMoradores: TIntegerField;
    QrListaAItsFracaoIdeal: TFloatField;
    QrListaAItsArreBaI: TIntegerField;
    QrListaAItsDescriCota: TWideStringField;
    QrListaAItsFATOR_COBRADO: TFloatField;
    QrListaAItsTITULO_FATOR: TWideStringField;
    QrListaAItsBoleto: TFloatField;
    QrListaL: TmySQLQuery;
    QrListaLCodigo: TIntegerField;
    QrListaLNome: TWideStringField;
    QrListaLVALOR: TFloatField;
    QrListaLCasas: TSmallintField;
    QrListaLUnidLei: TWideStringField;
    QrListaLSigla: TWideStringField;
    QrListaLSIGLA_IMP: TWideStringField;
    QrListaLIts: TmySQLQuery;
    QrListaLItsApto: TIntegerField;
    QrListaLItsMedAnt: TFloatField;
    QrListaLItsMedAtu: TFloatField;
    QrListaLItsConsumo: TFloatField;
    QrListaLItsControle: TIntegerField;
    QrListaLItsValor: TFloatField;
    QrListaLItsNOMEPROPRIET: TWideStringField;
    QrListaLItsUnidade: TWideStringField;
    QrListaLItsBoleto: TFloatField;
    DsListaA: TDataSource;
    DsListaAIts: TDataSource;
    DsListaL: TDataSource;
    DsListaLIts: TDataSource;
    QrAptoModBloq: TmySQLQuery;
    QrAptoModBloqModelBloq: TSmallintField;
    QrAptoModBloqConfigBol: TIntegerField;
    QrAptoModBloqBalAgrMens: TSmallintField;
    QrAptoModBloqCompe: TSmallintField;
    QrAptoModBloqColunas: TSmallintField;
    QrConfigBol: TmySQLQuery;
    QrConfigBolCodigo: TIntegerField;
    QrConfigBolNome: TWideStringField;
    QrConfigBolTitBTxtSiz: TSmallintField;
    QrConfigBolTitBTxtFmt: TSmallintField;
    QrConfigBolTitBMrgSup: TIntegerField;
    QrConfigBolTitBLinAlt: TIntegerField;
    QrConfigBolTitBTxtTit: TWideStringField;
    QrConfigBolTitBPerFmt: TSmallintField;
    QrConfigBolNiv1TxtSiz: TSmallintField;
    QrConfigBolNiv1ValSiz: TSmallintField;
    QrConfigBolNiv1TxtFmt: TSmallintField;
    QrConfigBolNiv1ValFmt: TSmallintField;
    QrConfigBolNiv1ValLar: TIntegerField;
    QrConfigBolNiv1LinAlt: TIntegerField;
    QrConfigBolCxaSTxtSiz: TSmallintField;
    QrConfigBolCxaSValSiz: TSmallintField;
    QrConfigBolCxaSTxtFmt: TSmallintField;
    QrConfigBolCxaSValFmt: TSmallintField;
    QrConfigBolCxaSTxtAlt: TIntegerField;
    QrConfigBolCxaSValAlt: TIntegerField;
    QrConfigBolCxaSMrgSup: TIntegerField;
    QrConfigBolCxaSIniTam: TFloatField;
    QrConfigBolCxaSCreTam: TFloatField;
    QrConfigBolCxaSDebTam: TFloatField;
    QrConfigBolCxaSMovTam: TFloatField;
    QrConfigBolCxaSFimTam: TFloatField;
    QrConfigBolCxaSIniTxt: TWideStringField;
    QrConfigBolCxaSCreTxt: TWideStringField;
    QrConfigBolCxaSDebTxt: TWideStringField;
    QrConfigBolCxaSMovTxt: TWideStringField;
    QrConfigBolCxaSFimTxt: TWideStringField;
    QrConfigBolCxaSMrgInf: TIntegerField;
    QrConfigBolTit2TxtSiz: TSmallintField;
    QrConfigBolTit2TxtFmt: TSmallintField;
    QrConfigBolTit2MrgSup: TIntegerField;
    QrConfigBolTit2LinAlt: TIntegerField;
    QrConfigBolSom2TxtSiz: TSmallintField;
    QrConfigBolSom2ValSiz: TSmallintField;
    QrConfigBolSom2TxtFmt: TSmallintField;
    QrConfigBolSom2ValFmt: TSmallintField;
    QrConfigBolSom2ValLar: TIntegerField;
    QrConfigBolSom2LinAlt: TIntegerField;
    QrConfigBolTit3TxtSiz: TSmallintField;
    QrConfigBolTit3TxtFmt: TSmallintField;
    QrConfigBolTit3MrgSup: TIntegerField;
    QrConfigBolTit3LinAlt: TIntegerField;
    QrConfigBolSom3TxtSiz: TSmallintField;
    QrConfigBolSom3ValSiz: TSmallintField;
    QrConfigBolSom3TxtFmt: TSmallintField;
    QrConfigBolSom3ValFmt: TSmallintField;
    QrConfigBolSom3ValLar: TIntegerField;
    QrConfigBolSom3LinAlt: TIntegerField;
    QrConfigBolTit4TxtSiz: TSmallintField;
    QrConfigBolTit4TxtFmt: TSmallintField;
    QrConfigBolTit4MrgSup: TIntegerField;
    QrConfigBolTit4LinAlt: TIntegerField;
    QrConfigBolSom4LinAlt: TIntegerField;
    QrConfigBolTitRTxtSiz: TSmallintField;
    QrConfigBolTitRTxtFmt: TSmallintField;
    QrConfigBolTitRMrgSup: TIntegerField;
    QrConfigBolTitRLinAlt: TIntegerField;
    QrConfigBolTitRTxtTit: TWideStringField;
    QrConfigBolTitRPerFmt: TSmallintField;
    QrConfigBolTitCTxtSiz: TSmallintField;
    QrConfigBolTitCTxtFmt: TSmallintField;
    QrConfigBolTitCMrgSup: TIntegerField;
    QrConfigBolTitCLinAlt: TIntegerField;
    QrConfigBolTitCTxtTit: TWideStringField;
    QrConfigBolTitCPerFmt: TSmallintField;
    QrConfigBolSdoCTxtSiz: TSmallintField;
    QrConfigBolSdoCTxtFmt: TSmallintField;
    QrConfigBolSdoCLinAlt: TIntegerField;
    QrConfigBolTitITxtSiz: TSmallintField;
    QrConfigBolTitITxtFmt: TSmallintField;
    QrConfigBolTitIMrgSup: TIntegerField;
    QrConfigBolTitILinAlt: TIntegerField;
    QrConfigBolTitITxtTit: TWideStringField;
    QrConfigBolSdoITxMSiz: TSmallintField;
    QrConfigBolSdoITxMFmt: TSmallintField;
    QrConfigBolSdoIVaMSiz: TSmallintField;
    QrConfigBolSdoIVaMFmt: TSmallintField;
    QrConfigBolSdoILiMAlt: TIntegerField;
    QrConfigBolSdoITxMTxt: TWideStringField;
    QrConfigBolSdoILaMVal: TIntegerField;
    QrConfigBolSdoITxTSiz: TSmallintField;
    QrConfigBolSdoITxTFmt: TSmallintField;
    QrConfigBolSdoIVaTSiz: TSmallintField;
    QrConfigBolSdoIVaTFmt: TSmallintField;
    QrConfigBolSdoILiTAlt: TIntegerField;
    QrConfigBolSdoITxTTxt: TWideStringField;
    QrConfigBolSdoILaTVal: TIntegerField;
    QrConfigBolCtaPTxtSiz: TSmallintField;
    QrConfigBolCtaPTxtFmt: TSmallintField;
    QrConfigBolCtaPValSiz: TSmallintField;
    QrConfigBolCtaPValFmt: TSmallintField;
    QrConfigBolCtaPLinAlt: TIntegerField;
    QrConfigBolCtaPLarVal: TIntegerField;
    QrConfigBolSgrPTitSiz: TSmallintField;
    QrConfigBolSgrPTitFmt: TSmallintField;
    QrConfigBolSgrPTitAlt: TIntegerField;
    QrConfigBolSgrPSumSiz: TSmallintField;
    QrConfigBolSgrPSumFmt: TSmallintField;
    QrConfigBolSgrPSumAlt: TIntegerField;
    QrConfigBolSgrPLarVal: TIntegerField;
    QrConfigBolPrvPTitSiz: TSmallintField;
    QrConfigBolPrvPTitFmt: TSmallintField;
    QrConfigBolPrvPTitAlt: TIntegerField;
    QrConfigBolPrvPTitStr: TWideStringField;
    QrConfigBolPrvPMrgSup: TIntegerField;
    QrConfigBolSumPTxtSiz: TSmallintField;
    QrConfigBolSumPTxtFmt: TSmallintField;
    QrConfigBolSumPValSiz: TSmallintField;
    QrConfigBolSumPValFmt: TSmallintField;
    QrConfigBolSumPLinAlt: TIntegerField;
    QrConfigBolSumPValLar: TIntegerField;
    QrConfigBolSumPTxtStr: TWideStringField;
    QrConfigBolCtaATxtSiz: TSmallintField;
    QrConfigBolCtaATxtFmt: TSmallintField;
    QrConfigBolCtaAValSiz: TSmallintField;
    QrConfigBolCtaAValFmt: TSmallintField;
    QrConfigBolCtaALinAlt: TIntegerField;
    QrConfigBolCtaALarVal: TIntegerField;
    QrConfigBolTitATitSiz: TSmallintField;
    QrConfigBolTitATitFmt: TSmallintField;
    QrConfigBolTitATitAlt: TIntegerField;
    QrConfigBolTitATitStr: TWideStringField;
    QrConfigBolTitAMrgSup: TIntegerField;
    QrConfigBolTitAAptSiz: TSmallintField;
    QrConfigBolTitAAptFmt: TSmallintField;
    QrConfigBolTitAAptAlt: TIntegerField;
    QrConfigBolTitAAptSup: TIntegerField;
    QrConfigBolTitASumSiz: TSmallintField;
    QrConfigBolTitASumFmt: TSmallintField;
    QrConfigBolTitASumAlt: TIntegerField;
    QrConfigBolLk: TIntegerField;
    QrConfigBolDataCad: TDateField;
    QrConfigBolDataAlt: TDateField;
    QrConfigBolUserCad: TIntegerField;
    QrConfigBolUserAlt: TIntegerField;
    QrConfigBolAlterWeb: TSmallintField;
    QrConfigBolAtivo: TSmallintField;
    QrConfigBolMeuLogoImp: TSmallintField;
    QrConfigBolMeuLogoAlt: TIntegerField;
    QrConfigBolMeuLogoLar: TIntegerField;
    QrConfigBolMeuLogoArq: TWideStringField;
    QrConfigBolColunas: TSmallintField;
    QrConfigBolNiv1Grades: TSmallintField;
    QrConfigBolCxaTrnsTxt: TWideStringField;
    QrConfigBolTit8TxtSiz: TSmallintField;
    QrConfigBolTit8TxtFmt: TSmallintField;
    QrConfigBolTit8MrgSup: TIntegerField;
    QrConfigBolTit8LinAlt: TIntegerField;
    QrConfigBolNiv8TxtSiz: TSmallintField;
    QrConfigBolNiv8ValSiz: TSmallintField;
    QrConfigBolNiv8Grades: TSmallintField;
    QrConfigBolNiv8TxtFmt: TSmallintField;
    QrConfigBolNiv8ValFmt: TSmallintField;
    QrConfigBolNiv8ValLar: TIntegerField;
    QrConfigBolNiv8LinAlt: TIntegerField;
    QrConfigBolSom8TxtSiz: TSmallintField;
    QrConfigBolSom8ValSiz: TSmallintField;
    QrConfigBolSom8TxtFmt: TSmallintField;
    QrConfigBolSom8ValFmt: TSmallintField;
    QrConfigBolSom8ValLar: TIntegerField;
    QrConfigBolSom8LinAlt: TIntegerField;
    frxBloq: TfrxReport;
    QrPrevBloq: TmySQLQuery;
    QrPrevBloqModelBloq: TSmallintField;
    QrPrevBloqConfigBol: TIntegerField;
    QrPrevBloqBalAgrMens: TSmallintField;
    QrPrevBloqCompe: TSmallintField;
    QrPrevBloqColunas: TSmallintField;
    QrCjSdo: TmySQLQuery;
    QrCjSdoCodigo: TIntegerField;
    QrCjSdoSdoAtu: TFloatField;
    QrCJMov: TmySQLQuery;
    QrCJMovConjunto: TIntegerField;
    QrCJMovCredito: TFloatField;
    QrCJMovDebito: TFloatField;
    QrCJMovValor: TFloatField;
    QrMov: TmySQLQuery;
    QrMovNOMEGRUPO: TWideStringField;
    QrMovGrupo: TIntegerField;
    QrMovNOMESUBGRUPO: TWideStringField;
    QrMovSubGrupo: TIntegerField;
    QrMovNOMECONTA: TWideStringField;
    QrMovGenero: TIntegerField;
    QrMovMez: TIntegerField;
    QrMovValor: TFloatField;
    QrMovNOMECONTA_EXT: TWideStringField;
    QrMovNOMEPLANO: TWideStringField;
    QrMovNOMECONJUNTO: TWideStringField;
    QrMovConjunto: TIntegerField;
    QrMovCJ_CRED: TFloatField;
    QrMovCJ_DEBI: TFloatField;
    QrMovCJ_SALF: TFloatField;
    QrMovCJ_SALI: TFloatField;
    QrMovCJ_SALM: TFloatField;
    QrMovSubPgto1: TIntegerField;
    QrMov3: TmySQLQuery;
    QrCtasSdo: TmySQLQuery;
    QrCtasSdoCodigo: TIntegerField;
    QrCtasSdoSdoAnt: TFloatField;
    QrCtasSdoSumCre: TFloatField;
    QrCtasSdoSumDeb: TFloatField;
    QrCtasSdoSdoFim: TFloatField;
    QrCtasSdoValor: TFloatField;
    QrSdoSgr: TmySQLQuery;
    QrSdoSgrCodigo: TIntegerField;
    QrSdoSgrSumCre: TFloatField;
    QrSdoSgrSumDeb: TFloatField;
    QrSdoSgrSdoFim: TFloatField;
    QrSdoSgrValor: TFloatField;
    QrSdoSgrSdoAnt: TFloatField;
    QrSdoGru: TmySQLQuery;
    QrSdoGruCodigo: TIntegerField;
    QrSdoGruSdoAnt: TFloatField;
    QrSdoGruSumCre: TFloatField;
    QrSdoGruSumDeb: TFloatField;
    QrSdoGruSdoFim: TFloatField;
    QrSdoGruValor: TFloatField;
    QrSdoCjt: TmySQLQuery;
    QrSdoCjtCodigo: TIntegerField;
    QrSdoCjtSumCre: TFloatField;
    QrSdoCjtSumDeb: TFloatField;
    QrSdoCjtSdoFim: TFloatField;
    QrSdoCjtValor: TFloatField;
    QrSdoCjtSdoAnt: TFloatField;
    QrSdoCjtNOME_CJT: TWideStringField;
    DsMov3: TDataSource;
    QrSumCtaBol: TmySQLQuery;
    QrSumCtaBolTEXTO: TWideStringField;
    QrSumCtaBolVALOR: TFloatField;
    QrSumCtaBolTipo: TLargeintField;
    QrSumCtaBolConsumo: TFloatField;
    QrSumCtaBolCasas: TLargeintField;
    QrSumCtaBolUnidLei: TWideStringField;
    QrSumCtaBolUnidImp: TWideStringField;
    QrSumCtaBolUnidFat: TFloatField;
    QrSumCtaBolGeraTyp: TLargeintField;
    QrSumCtaBolGeraFat: TFloatField;
    QrSumCtaBolCasRat: TLargeintField;
    QrSumCtaBolNaoImpLei: TLargeintField;
    QrSumCtaBolTEXTO_IMP: TWideStringField;
    QrSumCtaBolKGT: TLargeintField;
    frxDsSumCtaBol: TfrxDBDataset;
    QrPrevModBol: TmySQLQuery;
    QrPrevModBolUnidade: TWideStringField;
    QrPrevModBolNOME_CONFIGBOL: TWideStringField;
    QrPrevModBolApto: TIntegerField;
    QrPrevModBolConfigBol: TIntegerField;
    QrPrevModBolModelBloq: TSmallintField;
    QrPrevModBolBalAgrMens: TSmallintField;
    QrPrevModBolCompe: TSmallintField;
    QrPrevModBolNOME_MODELBLOQ: TWideStringField;
    QrPrevModBolCodigo: TIntegerField;
    DsPrevModBol: TDataSource;
    QrBolB: TmySQLQuery;
    QrBolBGenero: TIntegerField;
    QrBolBNOMECONS: TWideStringField;
    QrBolBValor: TFloatField;
    QrBolBPropriet: TIntegerField;
    QrBolBApto: TIntegerField;
    QrBolBVencto: TDateField;
    QrBolBMedAnt: TFloatField;
    QrBolBMedAtu: TFloatField;
    QrBolBConsumo: TFloatField;
    QrBolBCasas: TSmallintField;
    QrBolBUnidLei: TWideStringField;
    QrBolBUnidFat: TFloatField;
    QrBolBUnidImp: TWideStringField;
    QrBolBControle: TIntegerField;
    QrBolBGeraTyp: TSmallintField;
    QrBolBGeraFat: TFloatField;
    QrBolBCasRat: TSmallintField;
    QrBolBNaoImpLei: TSmallintField;
    QrBolA: TmySQLQuery;
    QrBolAControle: TIntegerField;
    QrBolAGenero: TIntegerField;
    QrBolAValor: TFloatField;
    QrBolATexto: TWideStringField;
    QrBolAApto: TIntegerField;
    QrBolAPropriet: TIntegerField;
    QrBolAVencto: TDateField;
    QrBolAUnidade: TWideStringField;
    QrBolSel: TmySQLQuery;
    QrBolSelVencto: TDateField;
    QrBolSelPROTOCOLO: TIntegerField;
    QrBolSelBoleto: TFloatField;
    DsBolSel: TDataSource;
    frxDsConfigBol: TfrxDBDataset;
    frxDsMov: TfrxDBDataset;
    frxDsMov3: TfrxDBDataset;
    frxDsInquilino: TfrxDBDataset;
    frxDsBoletosIts: TfrxDBDataset;
    frxDsBoletos: TfrxDBDataset;
    frxDsSdoCjt: TfrxDBDataset;
    QrSaldosNiv: TmySQLQuery;
    QrSaldosNivNivel: TIntegerField;
    QrSaldosNivCO_Cta: TIntegerField;
    QrSaldosNivCO_SGr: TIntegerField;
    QrSaldosNivCO_Gru: TIntegerField;
    QrSaldosNivCO_Cjt: TIntegerField;
    QrSaldosNivCO_Pla: TIntegerField;
    QrSaldosNivNO_Cta: TWideStringField;
    QrSaldosNivNO_SGr: TWideStringField;
    QrSaldosNivNO_Gru: TWideStringField;
    QrSaldosNivNO_Cjt: TWideStringField;
    QrSaldosNivNO_Pla: TWideStringField;
    QrSaldosNivOL_Cta: TIntegerField;
    QrSaldosNivOL_SGr: TIntegerField;
    QrSaldosNivOL_Gru: TIntegerField;
    QrSaldosNivOL_Cjt: TIntegerField;
    QrSaldosNivOL_Pla: TIntegerField;
    QrSaldosNivSdoAnt: TFloatField;
    QrSaldosNivCredito: TFloatField;
    QrSaldosNivDebito: TFloatField;
    QrSaldosNivMovim: TFloatField;
    QrSaldosNivEX_Cta: TIntegerField;
    QrSaldosNivEX_SGr: TIntegerField;
    QrSaldosNivEX_Gru: TIntegerField;
    QrSaldosNivEX_Cjt: TIntegerField;
    QrSaldosNivEX_Pla: TIntegerField;
    QrSaldosNivID_SEQ: TIntegerField;
    QrSaldosNivAtivo: TSmallintField;
    QrMov3NOMEPLANO: TWideStringField;
    QrMov3NOMECONJUNTO: TWideStringField;
    QrMov3NOMEGRUPO: TWideStringField;
    QrMov3NOMESUBGRUPO: TWideStringField;
    QrMov3NOMECONTA: TWideStringField;
    QrMov3Mez: TIntegerField;
    QrMov3Valor: TFloatField;
    QrMov3SubPgto1: TIntegerField;
    QrMov3NOMECONTA_EXT: TWideStringField;
    QrMov3Genero: TIntegerField;
    QrMov3SDOANT: TFloatField;
    QrMov3SUMCRE: TFloatField;
    QrMov3SUMDEB: TFloatField;
    QrMov3SDOFIM: TFloatField;
    QrMov3SDOMES: TFloatField;
    QrMov3CJT_COD: TIntegerField;
    QrMov3CJT_ANT: TFloatField;
    QrMov3CJT_CRE: TFloatField;
    QrMov3CJT_DEB: TFloatField;
    QrMov3CJT_FIM: TFloatField;
    QrMov3CJT_MOV: TFloatField;
    QrMov3SGR_COD: TIntegerField;
    QrMov3SGR_ANT: TFloatField;
    QrMov3SGR_CRE: TFloatField;
    QrMov3SGR_DEB: TFloatField;
    QrMov3SGR_FIM: TFloatField;
    QrMov3SGR_MOV: TFloatField;
    QrMov3GRU_COD: TIntegerField;
    QrMov3GRU_ANT: TFloatField;
    QrMov3GRU_CRE: TFloatField;
    QrMov3GRU_DEB: TFloatField;
    QrMov3GRU_MOV: TFloatField;
    QrMov3GRU_FIM: TFloatField;
    QrMov3Conjunto: TIntegerField;
    QrMov3Grupo: TIntegerField;
    QrMov3SubGrupo: TIntegerField;
    QrPendT: TmySQLQuery;
    QrPendTSALDO: TFloatField;
    QrPendU: TmySQLQuery;
    QrPendUSALDO: TFloatField;
    QrBLEData: TDateField;
    QrBLETipo: TSmallintField;
    QrBLECarteira: TIntegerField;
    QrBLEControle: TIntegerField;
    QrBLESub: TSmallintField;
    QrBLEAutorizacao: TIntegerField;
    QrBLEGenero: TIntegerField;
    QrBLEQtde: TFloatField;
    QrBLEDescricao: TWideStringField;
    QrBLENotaFiscal: TIntegerField;
    QrBLEDebito: TFloatField;
    QrBLECredito: TFloatField;
    QrBLECompensado: TDateField;
    QrBLEDocumento: TFloatField;
    QrBLESit: TIntegerField;
    QrBLEVencimento: TDateField;
    QrBLEFatID: TIntegerField;
    QrBLEFatID_Sub: TIntegerField;
    QrBLEFatNum: TFloatField;
    QrBLEFatParcela: TIntegerField;
    QrBLEID_Pgto: TIntegerField;
    QrBLEID_Sub: TSmallintField;
    QrBLEFatura: TWideStringField;
    QrBLEEmitente: TWideStringField;
    QrBLEBanco: TIntegerField;
    QrBLEContaCorrente: TWideStringField;
    QrBLECNPJCPF: TWideStringField;
    QrBLELocal: TIntegerField;
    QrBLECartao: TIntegerField;
    QrBLELinha: TIntegerField;
    QrBLEOperCount: TIntegerField;
    QrBLELancto: TIntegerField;
    QrBLEPago: TFloatField;
    QrBLEMez: TIntegerField;
    QrBLEFornecedor: TIntegerField;
    QrBLECliente: TIntegerField;
    QrBLECliInt: TIntegerField;
    QrBLEForneceI: TIntegerField;
    QrBLEMoraDia: TFloatField;
    QrBLEMulta: TFloatField;
    QrBLEProtesto: TDateField;
    QrBLEDataDoc: TDateField;
    QrBLECtrlIni: TIntegerField;
    QrBLENivel: TIntegerField;
    QrBLEVendedor: TIntegerField;
    QrBLEAccount: TIntegerField;
    QrBLEICMS_P: TFloatField;
    QrBLEICMS_V: TFloatField;
    QrBLEDuplicata: TWideStringField;
    QrBLEDepto: TIntegerField;
    QrBLEDescoPor: TIntegerField;
    QrBLEDescoVal: TFloatField;
    QrBLEDescoControle: TIntegerField;
    QrBLEUnidade: TIntegerField;
    QrBLENFVal: TFloatField;
    QrBLEAntigo: TWideStringField;
    QrBLEExcelGru: TIntegerField;
    QrBLELk: TIntegerField;
    QrBLEDataCad: TDateField;
    QrBLEDataAlt: TDateField;
    QrBLEUserCad: TIntegerField;
    QrBLEUserAlt: TIntegerField;
    QrBLESerieCH: TWideStringField;
    QrBLEDoc2: TWideStringField;
    QrBLEMoraVal: TFloatField;
    QrBLEMultaVal: TFloatField;
    QrBLECNAB_Sit: TSmallintField;
    QrBLETipoCH: TSmallintField;
    QrBLEAlterWeb: TSmallintField;
    QrBLEReparcel: TIntegerField;
    QrBLEID_Quit: TIntegerField;
    QrBLEAtrelado: TIntegerField;
    QrBLEAtivo: TSmallintField;
    QrBLEPagMul: TFloatField;
    QrBLEPagJur: TFloatField;
    QrBLESubPgto1: TIntegerField;
    QrBLEMultiPgto: TIntegerField;
    QrBLEProtocolo: TIntegerField;
    QrBLECtrlQuitPg: TIntegerField;
    QrBLESerieNF: TWideStringField;
    QrBLEEndossas: TSmallintField;
    QrBLEEndossan: TFloatField;
    QrBLEEndossad: TFloatField;
    QrBLECancelado: TSmallintField;
    QrBLEEventosCad: TIntegerField;
    QrBLEEncerrado: TIntegerField;
    QrBLEErrCtrl: TIntegerField;
    QrBLECOMPENSADO_TXT: TWideStringField;
    QrConfigBolTitAAptTex: TWideMemoField;
    QrConfigBolTitWebTxt: TWideStringField;
    QrConfigBolMesCompet: TSmallintField;
    QrBoletosJuridico: TSmallintField;
    QrBoletosJuridico_TXT: TWideStringField;
    QrBoletosJuridico_DESCRI: TWideStringField;
    QrIC: TmySQLQuery;
    QrICCodigo: TIntegerField;
    QrICTipo: TSmallintField;
    QrICNOMEENT: TWideStringField;
    QrICCNPJ_CPF: TWideStringField;
    QrICIE_RG: TWideStringField;
    QrICNIRE_: TWideStringField;
    QrICRUA: TWideStringField;
    QrICCOMPL: TWideStringField;
    QrICBAIRRO: TWideStringField;
    QrICCIDADE: TWideStringField;
    QrICNOMELOGRAD: TWideStringField;
    QrICNOMEUF: TWideStringField;
    QrICPais: TWideStringField;
    QrICTE1: TWideStringField;
    QrICFAX: TWideStringField;
    QrICNOMETIPO: TWideStringField;
    QrICNUMERO: TFloatField;
    QrICLograd: TFloatField;
    QrICCEP: TFloatField;
    QrInquilinoCNPJ_CPF_USUARIO: TWideStringField;
    QrInquilinoNOMETIPO_USUARIO: TWideStringField;
    QrInquilinoNO_USUARIO: TWideStringField;
    QrBLEAgencia: TIntegerField;
    QrBLCAgencia: TIntegerField;
    QrBoletosCNAB_Cfg: TIntegerField;
    QrBoletosItsTabelaOrig: TWideStringField;
    QrBoletosItsGenero: TIntegerField;
    QrComposAItsLancto: TIntegerField;
    QrComposAItsBoleto: TFloatField;
    QrComposLItsBoleto: TFloatField;
    QrComposLItsLancto: TIntegerField;
    frxDsCNAB_Cfg_B: TfrxDBDataset;
    QrCNAB_Cfg_B: TmySQLQuery;
    QrCNAB_Cfg_BNOMEBANCO: TWideStringField;
    QrCNAB_Cfg_BLocalPag: TWideStringField;
    QrCNAB_Cfg_BNOMECED: TWideStringField;
    QrCNAB_Cfg_BEspecieDoc: TWideStringField;
    QrCNAB_Cfg_BACEITETIT_TXT: TWideStringField;
    QrCNAB_Cfg_BCART_IMP: TWideStringField;
    QrCNAB_Cfg_BEspecieVal: TWideStringField;
    QrCNAB_Cfg_BCedBanco: TIntegerField;
    QrCNAB_Cfg_BAgContaCed: TWideStringField;
    QrCNAB_Cfg_BCedAgencia: TIntegerField;
    QrCNAB_Cfg_BCedPosto: TIntegerField;
    QrCNAB_Cfg_BCedConta: TWideStringField;
    QrCNAB_Cfg_BCartNum: TWideStringField;
    QrCNAB_Cfg_BIDCobranca: TWideStringField;
    QrCNAB_Cfg_BCodEmprBco: TWideStringField;
    QrCNAB_Cfg_BTipoCobranca: TIntegerField;
    QrCNAB_Cfg_BCNAB: TIntegerField;
    QrCNAB_Cfg_BCtaCooper: TWideStringField;
    QrCNAB_Cfg_BModalCobr: TIntegerField;
    QrCNAB_Cfg_BMultaPerc: TFloatField;
    QrCNAB_Cfg_BJurosPerc: TFloatField;
    QrCNAB_Cfg_BTexto01: TWideStringField;
    QrCNAB_Cfg_BTexto02: TWideStringField;
    QrCNAB_Cfg_BTexto03: TWideStringField;
    QrCNAB_Cfg_BTexto04: TWideStringField;
    QrCNAB_Cfg_BTexto05: TWideStringField;
    QrCNAB_Cfg_BTexto06: TWideStringField;
    QrCNAB_Cfg_BTexto07: TWideStringField;
    QrCNAB_Cfg_BTexto08: TWideStringField;
    QrCNAB_Cfg_BCedDAC_C: TWideStringField;
    QrCNAB_Cfg_BCedDAC_A: TWideStringField;
    QrCNAB_Cfg_BOperCodi: TWideStringField;
    QrCNAB_Cfg_BLayoutRem: TWideStringField;
    QrCNAB_Cfg_BNOMESAC: TWideStringField;
    QrCNAB_Cfg_BCorreio: TWideStringField;
    QrCNAB_Cfg_BCartEmiss: TIntegerField;
    QrCNAB_Cfg_BCedente: TIntegerField;
    QrCNAB_Cfg_BCAR_TIPODOC: TIntegerField;
    QrCNAB_Cfg_BCART_ATIVO: TIntegerField;
    QrCNAB_Cfg_BDVB: TWideStringField;
    QrCNAB_Cfg_BCodigo: TFloatField;
    QrCNAB_Cfg_BNosNumFxaU: TFloatField;
    QrCNAB_Cfg_BNosNumFxaI: TFloatField;
    QrCNAB_Cfg_BNosNumFxaF: TFloatField;
    QrCNAB_Cfg_BStatus: TFloatField;
    QrCNAB_Cfg_BCorresBco: TIntegerField;
    QrCNAB_Cfg_BCorresAge: TIntegerField;
    QrCNAB_Cfg_BCorresCto: TWideStringField;
    QrCNAB_Cfg_BNPrinBc: TWideStringField;
    procedure QrBoletosAfterScroll(DataSet: TDataSet);
    procedure QrBoletosBeforeClose(DataSet: TDataSet);
    procedure QrBoletosCalcFields(DataSet: TDataSet);
    procedure QrBoletosItsCalcFields(DataSet: TDataSet);
    procedure QrBLECalcFields(DataSet: TDataSet);
    procedure QrBLCCalcFields(DataSet: TDataSet);
    procedure QrComposAAfterScroll(DataSet: TDataSet);
    procedure QrComposABeforeClose(DataSet: TDataSet);
    procedure QrComposLAfterScroll(DataSet: TDataSet);
    procedure QrComposLBeforeClose(DataSet: TDataSet);
    procedure QrInquilinoCalcFields(DataSet: TDataSet);
    procedure QrDonoUHCalcFields(DataSet: TDataSet);
    procedure QrListaAAfterScroll(DataSet: TDataSet);
    procedure QrListaABeforeClose(DataSet: TDataSet);
    procedure QrListaACalcFields(DataSet: TDataSet);
    procedure QrListaAItsCalcFields(DataSet: TDataSet);
    procedure QrListaLAfterScroll(DataSet: TDataSet);
    procedure QrListaLBeforeClose(DataSet: TDataSet);
    procedure QrListaLCalcFields(DataSet: TDataSet);
    procedure QrMovAfterClose(DataSet: TDataSet);
    procedure QrMovCalcFields(DataSet: TDataSet);
    procedure QrMov3CalcFields(DataSet: TDataSet);
    procedure QrSumCtaBolCalcFields(DataSet: TDataSet);
    procedure QrPrevModBolCalcFields(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
    FAgrupaMensal: Boolean;
    {###
    FExtratoCC: String;
    }
    FEstagioA, FEstagioL: String;
  public
    { Public declarations }
    FfrxGer: TfrxReport;
    FColunasH, FConfigGer, FModeloGer, FConfigNew, FConfigOld, FModeloNew: Integer;
    FSaldoDeContasJaAtualizado: Boolean;
    //
    function frxComposite(Prev, Apto: Integer; Titulo: string; var ModelBloqAnt,
             ConfigAnt: Integer; ClearLastReport: Boolean): Boolean;
    function frxPrepara(frx: TfrxReport; Titulo: string;
             ClearLastReport: Boolean): Boolean;
    function frxDefineModelo(ModeloDoApto: Boolean; var ModelBloqAnt,
             ConfigAnt, ModelBloqNew, ConfigNew: Integer): TfrxReport;
    function frxMostra_Novo(PrevCod: Integer; Titulo: string; Tipo: TSelType): Boolean;
    function frxMostra_Antigo(frx: TfrxReport; Titulo: string; Tipo: TSelType): Boolean;
    function frxSalva_Antigo(frx: TfrxReport; Titulo: string; Tipo: TSelType;
             Arquivo: String): Boolean;
    function frxSalva_Novo(PrevCod: Integer; Titulo: string; Tipo: TselType;
             Arquivo: String): Boolean;
    function frxExporta_Antigo(frx: TfrxReport; Titulo: string; Tipo: TSelType;
             Arquivo: String; Filtro: TfrxCustomExportFilter): Boolean;
    function frxExporta_Novo(PrevCod: Integer; Titulo: string; Tipo: TSelType;
             Arquivo: String; Filtro: TfrxCustomExportFilter): Boolean;
    function ReabrirTabelasBloquetoAtual(Modelo, Config: Integer;
             SaldoDeContasJaAtualizado: Boolean): Boolean;
    function ImprimeBoletos_Novo(Quais: TSelType; Como: TfrxImpComo; Arquivo:
             String; Filtro: TfrxCustomExportFilter; PrevCod: Integer;
             var SdoJaAtz: Boolean): TfrxReport;
    //
    function  TodosSelecionadosTemVencimento(Tipo: TSelType; LoteImp: Integer;
              PageControl: TPageControl; DBGradeN, DBGradeS: TDBGrid): Boolean;
    function  TodosSelecionadosEstaoSemRestricaoJuridica(Tipo: TSelType;
              LoteImp: Integer; PageControl: TPageControl;
              DBGradeN, DBGradeS: TDBGrid): Boolean;
    //
    function  BoletoJaExiste(Boleto: Double; Cond: Integer; Desde: TDateTime): Boolean;
    //
    procedure ReopenBoletos(BOLAPTO: String);
    procedure ReopenBoletosIts();
    procedure ReopenComposAIts(Apto: Integer);
    procedure ReopenComposLIts(Apto: Integer);
    procedure ReopenEnderecoInquilino(Apto: Integer = 0);
    procedure ReopenListaA(ComBoleto: Boolean = True);
    procedure ReopenListaAIts(Apto: Integer);
    procedure ReopenListaL(ComBoleto: Boolean = True);
    procedure ReopenListaLIts(Apto: Integer);
    procedure ReopenPrevModBol(Apto: Integer);
    procedure Reopen_BLC_BLE();
    procedure ReopenPendU(Entidade, Apto: Integer);
    function  TextoExplicativoItemBoleto(EhConsumo, Casas: Integer; MedAnt,
              MedAtu, Consumo, UnidFat: Double; UnidLei, UnidImp: String;
              GeraTyp, CasRat, NaoImpLei: Integer; GeraFat: Double): String;
    function  ValidaModeloBoleto(RGModelBloq, RGCompe: TRadioGroup; Config: Integer): Boolean;
    procedure ReopenBoletosQry(QryBol, QryArr, QryLei, QryPPI, QryUsr:
              TmySQLQuery; PrevCodi, PrevCond, Periodo: Integer;
              TipoBol, TabAriA, TabCnsA, Vencto: String; CriaTemp: Boolean);
    procedure ConfiguraCompeConfigModelBol(RGModelBloq: TRadioGroup; var Compe, Model: Boolean);
    procedure ReopenCNAB_Cfg_Cond(Query: TmySQLQuery; Database: TmySQLDatabase; Cond, CNAB_Cfg: Integer);
  end;

var
  DmBloq: TDmBloq;

const
  SNenhumaUH = ' * * *  TODOS N�O ESPEC�FICOS  * * *';

implementation

uses Module, UnInternalConsts, CondGerModelBloq, MyDBCheck,
// CondGer,
UnGOTOy,
CondGerImpGer, ModuleCond, MeuFrx, MyGlyfs, ModuleGeral, UnBancos, UnFinanceiro,
  Principal, UMySQLModule, ModuleFin, UCreate, DmkDAC_PF, UnBloquetos;

{$R *.dfm}

{ TDmBloq }

function TDmBloq.BoletoJaExiste(Boleto: Double; Cond: Integer;
  Desde: TDateTime): Boolean;
begin
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT ari.*');
  Dmod.QrAux.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
  Dmod.QrAux.SQL.Add('LEFT JOIN ' + DmCond.FTabPrvA + ' prv ON prv.Codigo=ari.Codigo');
  Dmod.QrAux.SQL.Add('WHERE ari.Boleto=:P0');
  Dmod.QrAux.SQL.Add('AND ari.Vencto >= :P1');
  Dmod.QrAux.SQL.Add('AND prv.Cond=:P2');
  Dmod.QrAux.Params[00].AsFloat   := Boleto;
  Dmod.QrAux.Params[01].AsString  := Geral.FDT(Desde, 1);
  Dmod.QrAux.Params[02].AsInteger := Cond;
  Dmod.QrAux.Open;
  //
  if Dmod.QrAux.RecordCount > 0 then
  begin
    Result := Geral.MensagemBox('O n�mero de boleto ' +
    FormatFloat('000000', Boleto) + ' j� foi utilizado pare este condom�nio depois de '+
    Geral.FDT(Desde-1, 3) + '!' + Chr(13) + Chr(10) +
    'Deseja continuar assim mesmo?', 'Aviso', MB_YESNO+MB_ICONQUESTION) = ID_NO;
  end else Result := False;
  //
  Dmod.QrAux.Close;
end;

procedure TDmBloq.ConfiguraCompeConfigModelBol(RGModelBloq: TRadioGroup;
  var Compe, Model: Boolean);
var
  Modelo: String;
begin
  Modelo := RGModelBloq.Items[RGModelBloq.ItemIndex][1];
  //
  Model := (Modelo = 'H') or (Modelo = 'R') or (Modelo = 'I');
  Compe := Modelo = 'E';
end;

procedure TDmBloq.DataModuleCreate(Sender: TObject);
var
  //Campos: String;
  Compo: TComponent;
  Query: TmySQLQuery;
  I(*, N*): Integer;
//  Mensagem: WideString;
begin
  if DModG <> nil then
  begin
    // 2012-04-24
    // Evitar desconfigura��o espont�nea da IDE do Delphi
    for I := 0 to DmBloq.ComponentCount - 1 do
    begin
      Compo := DmBloq.Components[I];
      if Compo is TmySQLQuery then
      begin
        Query := TmySQLQuery(Compo);
        if Query.Name = 'QrSaldosNiv' then
          Query.Database := DModG.MyPID_DB
        else
        if Query.Name = 'QrCtasSdo' then
          Query.Database := DModG.MyPID_DB
        else
        if Query.Name = 'QrSdoSgr' then
          Query.Database := DModG.MyPID_DB
        else
        if Query.Name = 'QrSdoGru' then
          Query.Database := DModG.MyPID_DB
        else
        if Query.Name = 'QrSdoCjt' then
          Query.Database := DModG.MyPID_DB
        // Fim site DModG
        //
        else
          Query.Database := Dmod.MyDB;
      end;
    end;
    // FIM 2012-04-24
  end;
  //
  frxDsBoletos.DataSet    := QrBoletos;
  frxDsBoletosIts.DataSet := QrBoletosIts;
  frxDsConfigBol.DataSet  := QrConfigBol;
  frxDsInquilino.DataSet  := QrInquilino;
  frxDsMov.DataSet        := QrMov;
  frxDsMov3.DataSet       := QrMov3;
end;

function TDmBloq.frxComposite(Prev, Apto: Integer; Titulo: string;
  var ModelBloqAnt, ConfigAnt: Integer; ClearLastReport: Boolean): Boolean;
var
  s: TMemoryStream;
  PorApto: Boolean;
  frxAtu: TfrxReport;
  ModelBloqNew, ConfigNew: Integer;
begin
  Result := False;
  //buscar frx por Apto
  QrAptoModBloq.Close;
  QrAptoModBloq.Params[00].AsInteger := Prev;
  QrAptoModBloq.Params[01].AsInteger := Apto;
  QrAptoModBloq.Open;
  //
  FConfigOld := FConfigNew;
  //Definir frx com base no prev e apto
  PorApto := QrAptoModBloq.RecordCount > 0;
  if PorApto or (FfrxGer = nil) then
  begin
    frxAtu     := frxDefineModelo(QrAptoModBloq.RecordCount > 0, ModelBloqAnt,
                    ConfigAnt, ModelBloqNew, ConfigNew);
    FConfigNew := ConfigNew;
    FModeloNew := ModelBloqNew;
    //
    if frxAtu = nil then
      Exit;
  end else begin
    frxAtu     := FfrxGer;
    FConfigNew := FConfigGer;
    FModeloNew := FModeloGer;
  end;
  //
  if (FConfigNew <> FConfigOld) and (FModeloNew in ([8,9,10,11])) then
  begin
    QrConfigBol.Close;
    QrConfigBol.Params[0].AsInteger := FConfigNew;
    QrConfigBol.Open;
    //
    if QrConfigBol.RecordCount = 0 then
    begin
      Result := False;
      Geral.MensagemBox('N�o foi definida nenhuma configura��o ' +
      'de impress�o para o formato selecionado!', 'Aviso',
      MB_OK+MB_ICONWARNING);
    end;
  end;
  //
  try
    if frxAtu <> nil then
    begin
      //if ClearLastReport then
      //begin
      //frxBloq.Clear;
      frxBloq.OnGetValue := frxAtu.OnGetValue;
      frxBloq.OnUserFunction := frxAtu.OnUserFunction;
      frxBloq.ReportOptions.Name := Titulo;
      //  mudado de frxBloq para frxAtu
      frxAtu.Variables['MeuLogoExiste'] := VAR_MEULOGOEXISTE;
      frxAtu.Variables['MeuLogoCaminho'] := QuotedStr(VAR_MEULOGOPATH);
      frxAtu.Variables['LogoNFExiste'] := VAR_LOGONFEXISTE;
      frxAtu.Variables['LogoNFCaminho'] := QuotedStr(VAR_LOGONFPATH);
      //end;
      //
      frxAtu.Variables['LogoBancoExiste'] := True;
      frxAtu.Variables['LogoBancoPath'] := QuotedStr(DmCond.QrCondBcoLogoPath.Value);
      //
      frxAtu.Variables['LogoEmpresaExiste'] := True;
      frxAtu.Variables['LogoEmpresaPath'] := QuotedStr(DmCond.QrCondCliLogoPath.Value);
      //  FIM mudado de frxBloq para frxAtu

      // agregar frx selecionado ao frxBloq (frx virtual que est� agrupando v�rios frx para imprimir tudo junto)
      s := TMemoryStream.Create;
      frxAtu.SaveToStream(s);
      s.Position := 0;
      frxBloq.LoadFromStream(s);
      s.Free;
      //#$%
      frxBloq.PrepareReport(ClearLastReport);
      //
      Result := True;
    end else
      Geral.MensagemBox('ERRO. "frxCond?" n�o definido.', 'ERRO', MB_OK+MB_ICONERROR);
  except
    raise;
  end;
end;

function TDmBloq.frxDefineModelo(ModeloDoApto: Boolean; var ModelBloqAnt,
  ConfigAnt, ModelBloqNew, ConfigNew: Integer): TfrxReport;
var
  FichaCompe: Integer;
  Definido: Boolean;
  frxSel: TfrxReport;
  NomeDest: String;
  //SaldoDeContasJaAtualizado,
  EhGeral: Boolean;
begin
{ Alexandria
  frxSel := nil;
  //SaldoDeContasJaAtualizado := False;
  Definido   := False;
  FColunasH  := 0;
  EhGeral    := False;
  FichaCompe := 0;
  if ModeloDoApto then
  begin
    EhGeral      := False;
    NomeDest     := QrBoletosUnidade.Value + ' - ' + QrBoletosNOMEPROPRIET.Value;
    ModelBloqNew := QrAptoModBloqModelBloq.Value;
    ConfigNew    := QrAptoModBloqConfigBol.Value;
    if ConfigNew = 0 then FColunasH := 0 else
      FColunasH  := QrAptoModBloqColunas.Value;
    FAgrupaMensal := MLAGeral.ITB(QrAptoModBloqBalAgrMens.Value);
    FichaCompe   := QrAptoModBloqCompe.Value;
    //
    Definido := ModelBloqNew > 0;
  end else begin
    if FfrxGer <> nil then
      frxSel := FfrxGer
    else
    begin
      EhGeral      := True;
      NomeDest     := SNenhumaUH;
      ModelBloqNew := QrPrevBloqModelBloq.Value;
      ConfigNew    := QrPrevBloqConfigBol.Value;
      if ConfigNew = 0 then FColunasH := 0 else
        FColunasH  := QrPrevBloqColunas.Value;
      FAgrupaMensal := MLAGeral.ITB(QrPrevBloqBalAgrMens.Value);
      FichaCompe   := QrPrevBloqCompe.Value;
      //
      Definido := ModelBloqNew > 0;
    end;
  end;
  if frxSel = nil then
  begin
    if Definido and (ModelBloqNew in ([8,9,10,11])) then Definido := ConfigNew <> 0;
    if Definido and (ModelBloqNew = 5) then Definido := FichaCompe > 0;
    //
    if not Definido then
    begin
      if DBCheck.CriaFm(TFmCondGerModelBloq, FmCondGerModelBloq, afmoLiberado) then
      begin
        FmCondGerModelBloq.ST_Condominio.Caption := DmCond.QrCondNOMECLI.Value;
        FmCondGerModelBloq.STUH_Propriet.Caption := NomeDest;
        //
        FmCondGerModelBloq.EdConfig.ValueVariant := DmCond.QrCondConfigBol.Value;
        FmCondGerModelBloq.CBConfig.KeyValue     := DmCond.QrCondConfigBol.Value;
        FmCondGerModelBloq.RGModelBloq.ItemIndex := FmCondGer.QrPrevModelBloq.Value;
        FmCondGerModelBloq.ShowModal;
        if FmCondGerModelBloq.FImprime then
        begin
          ModelBloqNew := FmCondGerModelBloq.RGModelBloq.ItemIndex;
          ConfigNew    := Geral.IMV(FmCondGerModelBloq.EdConfig.Text);
          if ConfigNew  = 0 then FColunasH := 0 else
            FColunasH    := FmCondGerModelBloq.QrConfigBolColunas.Value;
          FAgrupaMensal := FmCondGerModelBloq.CkAgrupaMensal.Checked;
          FichaCompe   := FmCondGerModelBloq.RGCompe.ItemIndex;
        end;
        FmCondGerModelBloq.Destroy;
      end;
      FmCondGer.Update;
      Application.ProcessMessages;
    end;
    if (ModelBloqNew <> ModelBloqAnt) or (ConfigNew <> ConfigAnt) then
    begin
      ModelBloqAnt := ModelBloqNew;
      ConfigAnt := ConfigNew;
      ReabrirTabelasBloquetoAtual(ModelBloqAnt, ConfigAnt, FSaldoDeContasJaAtualizado);
      FSaldoDeContasJaAtualizado := True;
    end;
    case ModelBloqNew of
      1: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondA, False);
      2: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondB, False);
      3: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondC, False);
      4: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondD, False);
      5:
      begin
        case FichaCompe of
          1: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondE1, False);
          2: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondE2, False);
        end;
      end;
      6: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondG, False);
      7: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondG, False);
      8:
      begin
        case FColunasH of
          1: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondH1, True);
          2: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondH2, True);
          3: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondH3, True);
          4: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondH4, True);
          5: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondH5, True);
          6: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondH6, True);
        end;
      end;
      9:
      begin
        GOTOy.EnderecoDeEntidade1(DmCond.QrCondCliente.Value, -1);
        GOTOy.EnderecoDeEntidade2(QrICCodigo.Value, -1);
        case FColunasH of
          2: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondR2, False);
          3: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondR3, False);
          4: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondR4, False);
        end;
      end;
      10:
      begin
        GOTOy.EnderecoDeEntidade1(DmCond.QrCondCliente.Value, -1);
        GOTOy.EnderecoDeEntidade2(QrICCodigo.Value, -1);
        case FColunasH of
          1: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondIB, False);
        end;
      end;
      11:
      begin
        GOTOy.EnderecoDeEntidade1(DmCond.QrCondCliente.Value, -1);
        GOTOy.EnderecoDeEntidade2(QrICCodigo.Value, -1);
        case FColunasH of
          1: frxSel := FmCondGer.Define_frxCond(FmCondGer.frxCondIR, False);
        end;
      end;
    end;
  end;
  Result := frxSel;
  if EhGeral and (FfrxGer = nil) then
  begin
    FfrxGer := frxSel;
    FConfigGer := ConfigNew;
    FModeloGer := ModelBloqNew;
  end;
  if Result = nil then
  begin
    if (ModelBloqNew = 8) and (FColunasH = 0) then
      Geral.MB_Aviso('Quantidade de coluna n�o definida para o bloqueto "H!')
    else if (ModelBloqNew = 9) and (not (FColunasH in ([2,3,4]))) then
      Geral.MB_Aviso('Para o modelo de bloqueto "R" s�o permitidas apenas 2, 3 ou 4 colunas!')
    else if (ModelBloqNew in [10,11]) and (not (FColunasH = 1)) then
      Geral.MB_Aviso('Para os modelos de bloquetos "IB" ou "IR" s�o permitidas apenas 1 coluna!')
    else
      Geral.MB_Aviso('Nenhum modelo de bloqueto foi selecionado!');
  end;
}
end;

function TDmBloq.frxExporta_Antigo(frx: TfrxReport; Titulo: string;
  Tipo: TSelType; Arquivo: String; Filtro: TfrxCustomExportFilter): Boolean;
  function frxGravaAtual(frx: TfrxReport; Titulo, Arquivo: String;
  Filtro: TfrxCustomExportFilter): Boolean;
  begin
    Result := frxPrepara(frx, Titulo, True);
    frx.Export(Filtro);
  end;
var
  i: Integer;
  Grade: TDBGrid;
begin
{ Alexandria
  Result := True;
  case Tipo of
    istTodos:
    begin
      QrBoletos.First;
      while not QrBoletos.Eof do
      begin
        if Result then
            Result := frxGravaAtual(frx, Titulo, Arquivo, Filtro);
        QrBoletos.Next;
      end;
    end;
    istSelecionados:
    begin
      case FmCondGer.PageControl2.ActivePageIndex of
        3: Grade := FmCondGer.DBGradeN;
        4: Grade := TDBGrid(FmCondGer.DBGradeS);
        else Grade := nil;
      end;
      if Grade = nil then Geral.MensagemBox('Grade n�o definida!', 'Erro',
      MB_OK+MB_ICONERROR) else
      begin
        with Grade.DataSource.DataSet do
        for i:= 0 to Grade.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
          GotoBookmark(Grade.SelectedRows.Items[i]);
            Result := frxGravaAtual(frx, Titulo, Arquivo, Filtro);
        end;
      end;
    end;
    istAtual: Result := frxGravaAtual(frx, Titulo, Arquivo, Filtro);
  end;
}
end;

function TDmBloq.frxExporta_Novo(PrevCod: Integer; Titulo: string; Tipo: TSelType;
  Arquivo: String; Filtro: TfrxCustomExportFilter): Boolean;

  function frxGravaAtual(frx: TfrxReport; Titulo, Arquivo: String;
  Filtro: TfrxCustomExportFilter): Boolean;
  begin
    //Result := frxPrepara(frx, Titulo, True);
    Result := frx.Export(Filtro);
  end;

var
  i: Integer;
  Grade: TDBGrid;
  ModelBloqAnt, ConfigAnt: Integer;
begin
{ Alexandria
  Result := True;
  ModelBloqAnt := -1000;
  ConfigAnt := -1000;
  case Tipo of
    istTodos:
    begin
      QrBoletos.First;
      while not QrBoletos.Eof do
      begin
        if Result then
        begin
          Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, True);
          if Result then Result := frxGravaAtual(frxBloq, Titulo, Arquivo, Filtro);
        end;
        QrBoletos.Next;
      end;
    end;
    istSelecionados:
    begin
      case FmCondGer.PageControl2.ActivePageIndex of
        3: Grade := FmCondGer.DBGradeN;
        4: Grade := TDBGrid(FmCondGer.DBGradeS);
        else Grade := nil;
      end;
      if Grade = nil then Geral.MensagemBox('Grade n�o definida!', 'Erro',
      MB_OK+MB_ICONERROR) else
      begin
        with Grade.DataSource.DataSet do
        for i:= 0 to Grade.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
          GotoBookmark(Grade.SelectedRows.Items[i]);
          Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, True);
          if Result then Result := frxGravaAtual(frxBloq, Titulo, Arquivo, Filtro);
        end;
      end;
    end;
    istAtual:
    begin
      Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, True);
      if Result then Result := frxGravaAtual(frxBloq, Titulo, Arquivo, Filtro);
    end;
  end;
}
end;

function TDmBloq.frxMostra_Antigo(frx: TfrxReport; Titulo: string;
  Tipo: TSelType): Boolean;
var
  i: Integer;
  Grade: TDBGrid;
  Primeiro: Boolean;
begin
{ Alexandria
  Result := True;
  case Tipo of
    istTodos:
    begin
      QrBoletos.First;
      while not QrBoletos.Eof do
      begin
        if Result then
        begin
          if QrBoletos.RecNo = 1 then
            Result := frxPrepara(frx, Titulo, True)
          else
            Result := frxPrepara(frx, Titulo, False);
        end;
        QrBoletos.Next;
      end;
    end;
    istMarcados:
    begin
      Primeiro := True;
      QrBoletos.First;
      while not QrBoletos.Eof do
      begin
        if Result then
        begin
          if QrBoletosLOTE_PROTOCO.Value = FmCondGer.FLoteImp then
          begin
            //if QrBoletos.RecNo = 1 then
              Result := frxPrepara(frx, Titulo, Primeiro);
              Primeiro := False;
            //else
              //Result := frxPrepara(frx, Titulo, False);
          end;
        end;
        QrBoletos.Next;
      end;
    end;
    istSelecionados:
    begin
      case FmCondGer.PageControl2.ActivePageIndex of
        3: Grade := FmCondGer.DBGradeN;
        4: Grade := TDBGrid(FmCondGer.DBGradeS);
        else Grade := nil;
      end;
      if Grade = nil then Geral.MensagemBox('Grade n�o definida!', 'Erro',
      MB_OK+MB_ICONERROR) else
      begin
        with Grade.DataSource.DataSet do
        for i:= 0 to Grade.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
          GotoBookmark(Grade.SelectedRows.Items[i]);
          if i = 0 then
            Result := frxPrepara(frx, Titulo, True)
          else
            Result := frxPrepara(frx, Titulo, False);
        end;
      end;
    end;
    istAtual:
    begin
      //if CkDesign.Checked then
        //frx.Designreport
      //else
        MyObjects.frxMostra(frx, Titulo);
    end;
  end;
  if (Tipo in ([istTodos, istMarcados, istSelecionados])) and Result then
  begin
    Application.CreateForm(TFmMeuFrx, FmMeuFrx);
    frx.Preview := FmMeuFrx.PvVer;
    //
    FmMeuFrx.PvVer.OutlineWidth := frx.PreviewOptions.OutlineWidth;
    FmMeuFrx.PvVer.Zoom := frx.PreviewOptions.Zoom;
    FmMeuFrx.PvVer.ZoomMode := frx.PreviewOptions.ZoomMode;
    FmMeuFrx.UpdateZoom;
    //
    FmMeuFrx.ShowModal;
    FmMeuFrx.Destroy;
  end;
}
end;

function TDmBloq.frxMostra_Novo(PrevCod: Integer; Titulo: string; Tipo: TSelType): Boolean;
var
  i: Integer;
  Grade: TDBGrid;
  ModelBloqAnt, ConfigAnt: Integer;
  Primeiro: Boolean;
begin
{ Alexandria
  Result := True;
  ModelBloqAnt := -1000;
  ConfigAnt := -1000;
  case Tipo of
    istTodos:
    begin
      QrBoletos.First;
      while not QrBoletos.Eof do
      begin
        if Result then
        begin
          if QrBoletos.RecNo = 1 then
            Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, True)
          else
            Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, False);
        end;
        //#$%
        QrBoletos.Next;
      end;
    end;
    istMarcados:
    begin
      Primeiro := True;
      QrBoletos.First;
      while not QrBoletos.Eof do
      begin
        if Result then
        begin
          if QrBoletosLOTE_PROTOCO.Value = FmCondGer.FLoteImp then
          begin
            //if QrBoletos.RecNo = 1 then
              Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, Primeiro);
            Primeiro := False;
            //else
              //Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, False, Antigo);
          end;
        end;
        QrBoletos.Next;
      end;
    end;
    istSelecionados:
    begin
      case FmCondGer.PageControl2.ActivePageIndex of
        3: Grade := FmCondGer.DBGradeN;
        4: Grade := TDBGrid(FmCondGer.DBGradeS);
        else Grade := nil;
      end;
      if Grade = nil then Geral.MensagemBox('Grade n�o definida!', 'Erro',
      MB_OK+MB_ICONERROR) else
      begin
        with Grade.DataSource.DataSet do
        for i:= 0 to Grade.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
          GotoBookmark(Grade.SelectedRows.Items[i]);
          if i = 0 then
            Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, True)
          else
            Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, False);
        end;
      end;
    end;
    istAtual:
    begin
      //if CkDesign.Checked then
        //frx.Designreport
      //else
        Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, True);
    end;
  end;
  //frxBloq.PrepareReport(True(*ClearLastReport*)); //#$%
  if (Tipo in ([istAtual, istTodos, istMarcados, istSelecionados])) and Result then
  begin
    Application.CreateForm(TFmMeufrx, FmMeufrx);
    if FmCondGer.CkDesign.Checked then
    begin
      frxBloq.DesignReport;
    end else
    begin
      frxBloq.Preview := FmMeufrx.PvVer;
      //
      FmMeufrx.PvVer.OutlineWidth := frxBloq.PreviewOptions.OutlineWidth;
      FmMeufrx.PvVer.Zoom := frxBloq.PreviewOptions.Zoom;
      FmMeufrx.PvVer.ZoomMode := frxBloq.PreviewOptions.ZoomMode;
      FmMeufrx.UpdateZoom;
      //
      FmMeufrx.ShowModal;
      FmMeufrx.Destroy;
    end;
  end;
}
end;

function TDmBloq.frxPrepara(frx: TfrxReport; Titulo: string;
  ClearLastReport: Boolean): Boolean;
begin
  try
    if ClearLastReport then
    begin
      frx.ReportOptions.Name := Titulo;
      frx.Variables['MeuLogoExiste'] := VAR_MEULOGOEXISTE;
      frx.Variables['MeuLogoCaminho'] := QuotedStr(VAR_MEULOGOPATH);
      frx.Variables['LogoNFExiste'] := VAR_LOGONFEXISTE;
      frx.Variables['LogoNFCaminho'] := QuotedStr(VAR_LOGONFPATH);
    end;
    //
    frx.Variables['LogoBancoExiste'] := True;
    frx.Variables['LogoBancoPath'] := QuotedStr(DmCond.QrCondBcoLogoPath.Value);
    //
    frx.Variables['LogoEmpresaExiste'] := True;
    frx.Variables['LogoEmpresaPath'] := QuotedStr(DmCond.QrCondCliLogoPath.Value);
    frx.PrepareReport(ClearLastReport);
    //
    Result := True;
  except
    raise;
  end;
end;

function TDmBloq.frxSalva_Antigo(frx: TfrxReport; Titulo: string;
  Tipo: TSelType; Arquivo: String): Boolean;
  function frxGravaAtual(frx: TfrxReport; Titulo, Arquivo: String): Boolean;
  begin
    Result := frxPrepara(frx, Titulo, True);
    MyObjects.frxSalva(frx, Titulo, Arquivo);
  end;
var
  i: Integer;
  Grade: TDBGrid;
begin
{ Alexandria
  Result := True;
  case Tipo of
    istTodos:
    begin
      QrBoletos.First;
      while not QrBoletos.Eof do
      begin
        if Result then
            Result := frxGravaAtual(frx, Titulo, Arquivo);
        QrBoletos.Next;
      end;
    end;
    istMarcados:
    begin
      QrBoletos.First;
      while not QrBoletos.Eof do
      begin
        if QrBoletosLOTE_PROTOCO.Value = FmCondGer.FLoteImp then
        begin
          if Result then
              Result := frxGravaAtual(frx, Titulo, Arquivo);
          QrBoletos.Next;
        end;
      end;
    end;
    istSelecionados:
    begin
      case FmCondGer.PageControl2.ActivePageIndex of
        3: Grade := FmCondGer.DBGradeN;
        4: Grade := TDBGrid(FmCondGer.DBGradeS);
        else Grade := nil;
      end;
      if Grade = nil then Geral.MensagemBox('Grade n�o definida!', 'Erro',
      MB_OK+MB_ICONERROR) else
      begin
        with Grade.DataSource.DataSet do
        for i:= 0 to Grade.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
          GotoBookmark(Grade.SelectedRows.Items[i]);
            Result := frxGravaAtual(frx, Titulo, Arquivo);
        end;
      end;
    end;
    istAtual: Result := frxGravaAtual(frx, Titulo, Arquivo);

  end;
}
end;

function TDmBloq.frxSalva_Novo(PrevCod: Integer; Titulo: string; Tipo: TselType;
  Arquivo: String): Boolean;

  function frxGravaAtual(frx: TfrxReport; Titulo, Arquivo: String): Boolean;
  begin
    //Result := frxPrepara(frx, Titulo, True);
    Result := MyObjects.frxSalva(frx, Titulo, Arquivo);
  end;

var
  i: Integer;
  Grade: TDBGrid;
  ModelBloqAnt, ConfigAnt: Integer;
begin
  Result := True;
{ Alexandria
  ModelBloqAnt := -1000;
  ConfigAnt := -1000;
  case Tipo of
    istTodos:
    begin
      QrBoletos.First;
      while not QrBoletos.Eof do
      begin
        if Result then
        begin
          //if QrBoletos.RecNo = 1 then
            Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, True);
          //else
            //Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, False, Antigo);
          frxGravaAtual(frxBloq, Titulo, Arquivo);
        end;
        QrBoletos.Next;
      end;
    end;
    istMarcados:
    begin
      QrBoletos.First;
      while not QrBoletos.Eof do
      begin
        if Result then
        begin
          if QrBoletosLOTE_PROTOCO.Value = FmCondGer.FLoteImp then
          begin
            //if QrBoletos.RecNo = 1 then
              Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, True);
            //else
              //Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, False, Antigo);
            frxGravaAtual(frxBloq, Titulo, Arquivo);
          end;
        end;
        QrBoletos.Next;
      end;
    end;
    istSelecionados:
    begin
      case FmCondGer.PageControl2.ActivePageIndex of
        3: Grade := FmCondGer.DBGradeN;
        4: Grade := TDBGrid(FmCondGer.DBGradeS);
        else Grade := nil;
      end;
      if Grade = nil then Geral.MensagemBox('Grade n�o definida!', 'Erro',
      MB_OK+MB_ICONERROR) else
      begin
        with Grade.DataSource.DataSet do
        for i:= 0 to Grade.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
          GotoBookmark(Grade.SelectedRows.Items[i]);
          //if i = 0 then
            Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, True);
          //else
            //Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, False, Antigo);
          frxGravaAtual(frxBloq, Titulo, Arquivo);
        end;
      end;
    end;
    istAtual:
    begin
      Result := frxComposite(PrevCod, QrBoletosApto.Value, Titulo, ModelBloqAnt, ConfigAnt, True);
      frxGravaAtual(frxBloq, Titulo, Arquivo);
    end;
  end;
}
end;

function TDmBloq.ImprimeBoletos_Novo(Quais: TSelType; Como: TfrxImpComo;
  Arquivo: String; Filtro: TfrxCustomExportFilter; PrevCod: Integer;
  var SdoJaAtz: Boolean): TfrxReport;
const
  MaxHear = 4;
var
  //ModelBloq, Config, Colunas: Integer;
  Definido: Boolean;
begin
{ Alexandria
  Result     := nil;
  //
  FfrxGer    := nil;
  FConfigGer := 0;
  FConfigNew := 0;
  FConfigOld := 0;
  FModeloGer := 0;
  FModeloNew := 0;
  FSaldoDeContasJaAtualizado := SdoJaAtz;
  //FModeloQrConfigBol := 0;
  //
  Screen.Cursor := crHourGlass;
  try
    Definido := False;
    (*
    ModelBloq := 0;
    Config    := 0;
    Colunas   := 0;
    *)
    if UBancos.DigitoVerificadorCodigoBanco(QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger) = '?' then
    begin
      if QrCNAB_Cfg_B.FieldByName('DVB').AsString = '?' then
      begin
        Geral.MensagemBox('� necess�rio definir o d�gito ' +
        'verificador (DVB) do banco selecionado em seu cadastro!', 'Aviso',
        MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end;
    //
    // dados para o frx padr�o do cond no m�s
    QrPrevBloq.Close;
    QrPrevBloq.SQL.Clear;
    QrPrevBloq.SQL.Add('SELECT prv.ModelBloq, prv.ConfigBol,');
    QrPrevBloq.SQL.Add('prv.BalAgrMens, prv.Compe, cfb.Colunas');
    QrPrevBloq.SQL.Add('FROM ' + DmCond.FTabPrvA + ' prv');
    QrPrevBloq.SQL.Add('LEFT JOIN configbol');
    QrPrevBloq.SQL.Add('cfb ON cfb.Codigo=prv.ConfigBol');
    QrPrevBloq.SQL.Add('WHERE prv.Codigo=:P0');
    QrPrevBloq.Params[0].AsInteger := PrevCod;
    QrPrevBloq.Open;
    //
    if (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BRADESCO_2015)
      or (QrCNAB_Cfg_B.FieldByName('LayoutRem').AsString = CO_756_CORRESPONDENTE_BB_2015)
    then
      FmMyGlyfs.PreparaLogoBanco(QrCNAB_Cfg_B.FieldByName('CorresBco').AsInteger)
    else
      FmMyGlyfs.PreparaLogoBanco(QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger);
    //
    QrPendT.Close;
    QrPendT.SQL.Clear;
    QrPendT.SQL.Add('SELECT SUM(lan.Pago-lan.Credito) SALDO');
    QrPendT.SQL.Add('FROM ' + DmCond.FTabLctA + ' lan');
    QrPendT.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
    QrPendT.SQL.Add('WHERE car.Tipo=2');
    QrPendT.SQL.Add('AND car.ForneceI=:P0');
    QrPendT.SQL.Add('AND lan.Sit<2');
    QrPendT.SQL.Add('AND lan.Reparcel=0');
    QrPendT.SQL.Add('AND lan.Vencimento < SYSDATE()');
    QrPendT.Params[0].AsInteger := FmCondGer.QrPrevCondCli.Value;
    QrPendT.Open;
    //
    Application.ProcessMessages;
    FmCondGer.DefineSaldoResumo;
    Application.ProcessMessages;
    case Como of
      ficMostra: Definido := frxMostra_Novo(PrevCod, 'Bloquetos condom�nio', Quais);
      ficSalva: Definido := frxSalva_Novo(PrevCod, 'Bloquetos condom�nio', Quais, Arquivo);
      ficExporta: Definido := frxExporta_Novo(PrevCod, 'Bloquetos condom�nio', Quais, Arquivo, Filtro);
      else Geral.MensagemBox(
        'Tipo de sa�da para impress�o de bloqueto n�o definida.' +
        sLineBreak + 'Informe a DERMATEK!',
        'ERRO', MB_OK+MB_ICONINFORMATION);
    end;
    if Definido then Result := frxBloq;
    Application.ProcessMessages;
  finally
    frxBloq.Clear;
    FfrxGer := nil;
    FConfigGer := 0;
    FConfigNew := 0;
    FConfigOld := 0;
    FModeloGer := 0;
    FModeloNew := 0;
    // Deve ser antes
    SdoJaAtz := FSaldoDeContasJaAtualizado;
    FSaldoDeContasJaAtualizado := False;
    Screen.Cursor := crDefault;
  end;
}
end;

procedure TDmBloq.QrBLCCalcFields(DataSet: TDataSet);
begin
  QrBLCCOMPENSADO_TXT.Value := Geral.FDT(QrBLCCompensado.Value, 3);
end;

procedure TDmBloq.QrBLECalcFields(DataSet: TDataSet);
begin
  QrBLECOMPENSADO_TXT.Value := Geral.FDT(QrBLECompensado.Value, 3);
end;

procedure TDmBloq.QrBoletosAfterScroll(DataSet: TDataSet);
begin
{ Alexandria
  ReopenBoletosIts();
  Reopen_BLC_BLE();
  ReopenEnderecoInquilino();
  ReopenCNAB_Cfg_Cond(QrCNAB_Cfg_B, Dmod.MyDB, FmCondGer.QrPrevCond.Value, QrBoletosCNAB_Cfg.Value);
}
end;

procedure TDmBloq.QrBoletosBeforeClose(DataSet: TDataSet);
begin
  QrBoletosIts.Close;
  QrBLE.Close;
  QrBLC.Close;
  QrCNAB_Cfg_B.Close;
end;

procedure TDmBloq.QrBoletosCalcFields(DataSet: TDataSet);
begin
  QrBoletosSUB_TOT.Value :=
  QrBoletosSUB_ARR.Value +
  QrBoletosSUB_LEI.Value;
  //
  if QrBoletosBoleto.Value = 0 then
    QrBoletosBLOQUETO.Value := -1
  else
    QrBoletosBLOQUETO.Value := QrBoletosBoleto.Value;
  //
  QrBoletosVENCTO_TXT.Value := Geral.FDT(QrBoletosVencto.Value, 3);
  //
  if ( (Trim(QrBoletosUSERNAME.Value) <> '')
  and (Trim(QrBoletosPASSWORD.Value) <> '')) then
    QrBoletosPWD_WEB.Value := 'Login: ' + QrBoletosUSERNAME.Value +
    '   Senha: ' + QrBoletosPASSWORD.Value
  else QrBoletosPWD_WEB.Value := '';

  //

  QrBoletosJuridico_TXT.Value := dmkPF.DefineJuricoSigla(QrBoletosJuridico.Value);
  QrBoletosJuridico_DESCRI.Value := dmkPF.DefineJuricoTexto(QrBoletosJuridico.Value);
end;

procedure TDmBloq.QrBoletosItsCalcFields(DataSet: TDataSet);
begin
  QrBoletosItsTEXTO_IMP.Value := QrBoletosItsTEXTO.Value +
    TextoExplicativoItemBoleto(QrBoletosItsTipo.Value, QrBoletosItsCasas.Value,
    QrBoletosItsMedAnt.Value, QrBoletosItsMedAtu.Value,
    QrBoletosItsConsumo.Value, QrBoletosItsUnidFat.Value,
    QrBoletosItsUnidLei.Value, QrBoletosItsUnidImp.Value,
    QrBoletosItsGeraTyp.Value, QrBoletosItsCasRat.Value,
    QrBoletosItsNaoImpLei.Value, QrBoletosItsGeraFat.Value);
  //
  QrBoletosItsVENCTO_TXT.Value := Geral.FDT(QrBoletosItsVencto.Value, 3);
end;

procedure TDmBloq.QrComposAAfterScroll(DataSet: TDataSet);
begin
  ReopenComposAIts(0);
end;

procedure TDmBloq.QrComposABeforeClose(DataSet: TDataSet);
begin
  QrComposAIts.Close;
end;

procedure TDmBloq.QrComposLAfterScroll(DataSet: TDataSet);
begin
  ReopenComposLIts(0);
end;

procedure TDmBloq.QrComposLBeforeClose(DataSet: TDataSet);
begin
  QrComposLIts.Close;
end;

procedure TDmBloq.QrDonoUHCalcFields(DataSet: TDataSet);
begin
  QrDonoUHCNPJ_CPF_TXT.Value := Geral.FormataCNPJ_TT(QrDonoUHCNPJ_CPF.Value);
end;

procedure TDmBloq.QrInquilinoCalcFields(DataSet: TDataSet);
  procedure Agregador(var Endereco: String; const Item, Prefixo, PosFixo: String);
  begin
    if Item <> '' then
    Endereco := Endereco + Prefixo + Item + PosFixo;
    //Result := True;
  end;
var
  LNR, LN2, Compl, Resp, Prop: String;
  TipoEnd: Integer;
begin
  Resp := DModG.ReCaptionTexto('Resp.');
  Prop := DModG.ReCaptionTexto('Propr.');
  //
  QrInquilinoTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrICTe1.Value);
  QrInquilinoFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrICFax.Value);
  QrInquilinoCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrICCNPJ_CPF.Value);

  //
  ///////////////// P R E P A R A � � O   D O   E N D E R E � O ////////////////
  //
  TipoEnd := 0;
  case QrInquilinoBloqEndTip.Value of
    //WHEN 0 THEN (IF(imv.Usuario <> 0, cnd.Cliente, imv.Propriet) )
    0: if QrInquilinoUsuario.Value > 0 then TipoEnd := 1 else TipoEnd := 0;
    1: TipoEnd := 1;
    2: TipoEnd := 0;
    3: TipoEnd := 0; //
    4: TipoEnd := 2;
  end;
  case TipoEnd of
    0: Compl := QrICCOMPL.Value;
    1: Compl := QrInquilinoPrefixoUH.Value + ' ' + QrInquilinoUnidade.Value;
    else Compl := '';
  end;
  //
  if QrICNumero.Value = 0 then
    QrInquilinoNUMERO_TXT.Value :=  'S/N�'
  else
    QrInquilinoNUMERO_TXT.Value := FormatFloat('###,###,##0', QrICNumero.Value);
  //
  LNR := '';
  LN2 := '';
  if TipoEnd < 2 then
  begin
    //
    /////////////////////////// E N D E R E � O //////////////////////////////////
    //
    // Tipo Logradouro
    Agregador(LNR, QrICNOMELOGRAD.Value, '', ' ');
    // Rua
    Agregador(LNR, QrICRUA.Value, '', ', ');
    // N�mero
    Agregador(LNR, QrInquilinoNUMERO_TXT.Value, '', ' ' );
    // Complemento
    Agregador(LNR, Compl, '', ', ');
    // Bairro
    Agregador(LNR, QrICBairro.Value, '', ' - ');
    // Cidade e UF
    Agregador(LN2, QrICCidade.Value, '', ' - ' + QrICNOMEUF.Value);
    // CEP
    Agregador(LN2, Geral.FormataCEP_NT(QrICCEP.Value), '     CEP ', '');
    QrInquilinoLNR.Value := LNR;
    QrInquilinoLN2.Value := LN2;
    QrInquilinoE_ALL.Value := QrInquilinoLNR.Value + ' ' + QrInquilinoLN2.Value;
  end else begin
    QrInquilinoLNR.Value := QrInquilinoEnderLin1.Value;
    QrInquilinoLN2.Value := QrInquilinoEnderLin2.Value;
    QrInquilinoE_ALL.Value :=
      QrInquilinoEnderLin1.Value + sLineBreak + QrInquilinoEnderLin2.Value;
  end;
  ///////////////////// F I M   E N D E R E � O ////////////////////////////////
  //
  QrInquilinoECEP_TXT.Value := Geral.FormataCEP_NT(QrICCEP.Value);
  //
  if Trim(QrDonoUHNOMEPROPRIET.Value) <> '' then
     QrInquilinoPROPRI_E_MORADOR.Value := Prop + ': ' +
     QrDonoUHNOMEPROPRIET.Value + ' - ' + QrDonoUHNOMETIPO.Value + ' ' + QrDonoUHCNPJ_CPF_TXT.Value +
     '  ||  ' + Resp +  ': ' + QrInquilinoNO_USUARIO.Value  + ' - ' +
     QrInquilinoNOMETIPO_USUARIO. Value + ' ' +
     Geral.FormataCNPJ_TT(QrInquilinoCNPJ_CPF_USUARIO.Value)
  else begin
    case TipoEnd of
      0: // entidade pesquisada
      begin
        if QrInquilinoBloqEndTip.Value = 2 then
        begin
          QrInquilinoPROPRI_E_MORADOR.Value := Prop + ': ' +
          QrICNOMEENT.Value  + ' - ' +
          QrICNOMETIPO.Value + ' ' + QrInquilinoCNPJ_TXT.Value;
        end else if QrInquilinoBloqEndTip.Value = 0 then
        begin
          if QrInquilinoUsuario.Value = 0 then
          begin
            QrInquilinoPROPRI_E_MORADOR.Value := Prop + ': ' +
            QrICNOMEENT.Value  + ' - ' +
            QrICNOMETIPO.Value + ' ' + QrInquilinoCNPJ_TXT.Value;
          end else
          begin
            QrInquilinoPROPRI_E_MORADOR.Value := Resp + ': ' +
            QrICNOMEENT.Value  + ' - ' +
            QrICNOMETIPO.Value + ' ' + QrInquilinoCNPJ_TXT.Value;
          end;
        end else
        begin
          QrInquilinoPROPRI_E_MORADOR.Value := Resp + ': ' +
          QrICNOMEENT.Value  + ' - ' +
          QrICNOMETIPO.Value + ' ' + QrInquilinoCNPJ_TXT.Value;
        end;
      end;
      1: // do moraddor
      begin
        QrInquilinoPROPRI_E_MORADOR.Value := Resp + ': ' +
        QrInquilinoNO_USUARIO.Value  + ' - ' +
        QrInquilinoNOMETIPO_USUARIO.Value + ' ' +
        Geral.FormataCNPJ_TT(QrInquilinoCNPJ_CPF_USUARIO.Value);
      end;
      else //2: // ningu�m
      begin
        QrInquilinoPROPRI_E_MORADOR.Value := QrInquilinoEnderNome.Value;
      end;
    end;
  end;
end;
  
procedure TDmBloq.QrListaAAfterScroll(DataSet: TDataSet);
begin
  ReopenListaAIts(0);
end;

procedure TDmBloq.QrListaABeforeClose(DataSet: TDataSet);
begin
  QrListaAIts.Close;
end;

procedure TDmBloq.QrListaACalcFields(DataSet: TDataSet);
var
  Sigla: String;
begin
{ Alexandria
  Sigla := '';
  case FmCondger.RGAgrupListaA.ItemIndex of
    0: QrListaATEXTO_IMP.Value := QrListaATexto.Value;
    1:
    begin
      if QrListaACodigo.Value = 0 then
        QrListaATEXTO_IMP.Value := '*OUTROS*'
      else
        QrListaATEXTO_IMP.Value := QrListaANOME_ARREC.Value;
      if QrListaASIGLA_BAC.Value <> '?' then
        Sigla := QrListaASIGLA_BAC.Value;
    end;
    2:
    begin
      QrListaATEXTO_IMP.Value := QrListaANOME_CONTA.Value;
      if QrListaASIGLA_CONTA.Value <> '?' then
        Sigla := QrListaASIGLA_CONTA.Value;
    end else QrListaATEXTO_IMP.Value := '*** Index desconhecido ***';
  end;
  if Sigla = '' then
    Sigla := QrListaATEXTO_IMP.Value;
  QrListaASIGLA_IMP.Value := Sigla;



  case QrListaADeQuem.Value of
    0:
    begin
      case QrListaAPartilha.Value of
        0: QrListaATITULO_FATOR.Value  := '';
        1: QrListaATITULO_FATOR.Value  := 'Fra��o ideal';
        2: QrListaATITULO_FATOR.Value  := 'Moradores cadastrados';
      end;
    end;
    1:
    begin
      case QrListaACalculo.Value of
        0: QrListaATITULO_FATOR.Value  := '';
        1: QrListaATITULO_FATOR.Value  := 'Percentual';
        2: QrListaATITULO_FATOR.Value  := QrListaADescriCota.Value;
      end;
    end;
  end;
}
end;

procedure TDmBloq.QrListaAItsCalcFields(DataSet: TDataSet);
begin
  case QrListaAItsDeQuem.Value of
    0:
    begin
      case QrListaAItsPartilha.Value of
        0:
        begin
          QrListaAItsFATOR_COBRADO.Value := 0;
          QrListaAItsTITULO_FATOR.Value  := '';
        end;
        1:
        begin
          QrListaAItsFATOR_COBRADO.Value := QrListaAItsFracaoIdeal.Value;
          QrListaAItsTITULO_FATOR.Value  := 'Fra��o ideal';
        end;
        2:
        begin
          QrListaAItsFATOR_COBRADO.Value := QrListaAItsMoradores.Value;
          QrListaAItsTITULO_FATOR.Value  := 'Moradores cadastrados';
        end;
      end;
    end;
    1:
    begin
      case QrListaAItsCalculo.Value of
        0:
        begin
          QrListaAItsFATOR_COBRADO.Value := 0;
          QrListaAItsTITULO_FATOR.Value  := '';
        end;
        1:
        begin
          QrListaAItsFATOR_COBRADO.Value := QrListaAItsPercent.Value;
          QrListaAItsTITULO_FATOR.Value  := 'Percentual';
        end;
        2:
        begin
          QrListaAItsFATOR_COBRADO.Value := QrListaAItsPercent.Value;
          QrListaAItsTITULO_FATOR.Value  := QrListaAItsDescriCota.Value;
        end;
      end;
    end;
  end;
end;

procedure TDmBloq.QrListaLAfterScroll(DataSet: TDataSet);
begin
  ReopenListaLIts(0);
end;

procedure TDmBloq.QrListaLBeforeClose(DataSet: TDataSet);
begin
  QrListaLIts.Close;
end;

procedure TDmBloq.QrListaLCalcFields(DataSet: TDataSet);
begin
  if QrListaLSigla.Value <> '?' then
    QrListaLSIGLA_IMP.Value := QrListaLSigla.Value
  else
    QrListaLSIGLA_IMP.Value := QrListaLNome.Value;
end;

procedure TDmBloq.QrMov3CalcFields(DataSet: TDataSet);
var
  SubPgto1: String;
begin
{ Alexandria
  if QrMov3SubPgto1.Value = 0 then
    SubPgto1 := ''
  else
    SubPgto1 := ' RECEB. ACORDO EXTRAJUDICIAL';

  QrMov3NOMECONTA_EXT.Value := QrMov3NOMECONTA.Value;
  if (QrMov3Mez.Value > 0) and (FmCondGer.FAgrupaMensal) then
    QrMov3NOMECONTA_EXT.Value := QrMov3NOMECONTA_EXT.Value + ' - ' +
    MLAGeral.MezToFDT(QrMov3Mez.Value, 0, 104) + SubPgto1;
  //
}
end;

procedure TDmBloq.QrMovAfterClose(DataSet: TDataSet);
begin
  QrCjSdo.Close;
  QrCjMov.Close;
end;

procedure TDmBloq.QrMovCalcFields(DataSet: TDataSet);
var
  SubPgto1: String;
begin
  if QrMovSubPgto1.Value = 0 then SubPgto1 := ''
    else SubPgto1 := ' RECEB. ACORDO EXTRAJUDICIAL';

  QrMovNOMECONTA_EXT.Value := QrMovNOMECONTA.Value;
  if QrMovMez.Value > 0 then QrMovNOMECONTA_EXT.Value :=
    QrMovNOMECONTA_EXT.Value + ' - ' + MLAGeral.MezToFDT(QrMovMez.Value, 0, 104)
    + SubPgto1;
  //
  QrMovCJ_SALI.Value :=
    QrMovCJ_SALF.Value - QrMovCJ_CRED.Value + QrMovCJ_DEBI.Value;
  QrMovCJ_SALM.Value :=
    QrMovCJ_CRED.Value - QrMovCJ_DEBI.Value;
end;

procedure TDmBloq.QrPrevModBolCalcFields(DataSet: TDataSet);
begin
  QrPrevModBolNOME_MODELBLOQ.Value :=
    UBloquetos.LetraModelosBloq(QrPrevModBolModelBloq.Value);
end;

procedure TDmBloq.QrSumCtaBolCalcFields(DataSet: TDataSet);
begin
  QrSumCtaBolTEXTO_IMP.Value := QrSumCtaBolTEXTO.Value +
    TextoExplicativoItemBoleto(QrSumCtaBolTipo.Value, QrSumCtaBolCasas.Value,
    0, 0,
    QrSumCtaBolConsumo.Value, QrSumCtaBolUnidFat.Value,
    QrSumCtaBolUnidLei.Value, QrSumCtaBolUnidImp.Value,
    QrSumCtaBolGeraTyp.Value, QrSumCtaBolCasRat.Value,
    QrSumCtaBolNaoImpLei.Value, QrSumCtaBolGeraFat.Value);
end;

function TDmBloq.ReabrirTabelasBloquetoAtual(Modelo, Config: Integer;
SaldoDeContasJaAtualizado: Boolean): Boolean;
  procedure GeraParteSQL(TabLct, Empresa, DataI, DataF: String);
  begin
    QrMov.SQL.Add('SELECT pla.Nome NOMEPLANO, cjt.Nome NOMECONJUNTO,');
    QrMov.SQL.Add('gru.Conjunto, gru.Nome NOMEGRUPO, sgr.Grupo,');
    QrMov.SQL.Add('sgr.Nome NOMESUBGRUPO, con.SubGrupo, con.Nome NOMECONTA,');
    QrMov.SQL.Add('lct.Genero, lct.Mez, SUM(lct.Credito - lct.Debito) Valor,');
    QrMov.SQL.Add('lct.SubPgto1, pla.OrdemLista OL_PLA,');
    QrMov.SQL.Add('cjt.OrdemLista OL_CJT, gru.OrdemLista OL_GRU,');
    QrMov.SQL.Add('sgr.OrdemLista OL_SGR, con.OrdemLista OL_CTA');
    QrMov.SQL.Add('');
    QrMov.SQL.Add('FROM ' + TabLct + ' lct');
    QrMov.SQL.Add('LEFT JOIN ' + TMeuDB + '.carteiras car ON car.Codigo=lct.Carteira');
    QrMov.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas    con ON con.Codigo=lct.Genero');
    QrMov.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgr ON sgr.Codigo=con.SubGrupo');
    QrMov.SQL.Add('LEFT JOIN ' + TMeuDB + '.grupos    gru ON gru.Codigo=sgr.Grupo');
    QrMov.SQL.Add('LEFT JOIN ' + TMeuDB + '.conjuntos cjt ON cjt.Codigo=gru.Conjunto');
    QrMov.SQL.Add('LEFT JOIN ' + TMeuDB + '.plano     pla ON pla.Codigo=cjt.Plano');
    QrMov.SQL.Add('');
    QrMov.SQL.Add('WHERE lct.CliInt=' + Empresa);
    QrMov.SQL.Add('AND car.Tipo < 2');
    QrMov.SQL.Add('AND lct.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
    QrMov.SQL.Add('GROUP BY lct.Genero, lct.Mez, lct.SubPgto1');
  end;
var
  DataI, DataF, Empresa: String;
  //Total, Ativos
  Mez: Integer;
begin
 { Alexandria
 Result := UBancos.BancoImplementado(QrCNAB_Cfg_B.FieldByName('CedBanco').AsInteger, 0, ecnabBloquet);
  if Result then
  begin
    Empresa := FormatFloat('0', DmCond.QrCondCliente.Value);
    DataI := Geral.FDT(MLAGeral.PrimeiroDiaDoPeriodo_Date(
      FmCondGer.QrPrevPeriodo.Value - 1 + DmCond.QrCondPBB.Value), 1);
    DataF := Geral.FDT(MLAGeral.UltimoDiaDoPeriodo_Date(
      FmCondGer.QrPrevPeriodo.Value - 1 + DmCond.QrCondPBB.Value), 1);
    //
    if Modelo < 7 then
    begin
      // 2010-10-05 falta testar com modelo de bloqueto diferente de H !!!
      // Desabilitado a muito tempo!
      (*
      DModG.Atualiza Contas Entidade(DmCond.QrCondCliente.Value,
        FmCondGer.QrPrevPeriodo.Value - 1 + DmCond.QrCondPBB.Value,
          FmCondGer.ProgressBar3, FmCondGer.MeAtz);
      *)
      //
      QrCjSdo.Close;
      QrCjSdo.Params[0].AsInteger := DmCond.QrCondCliente.Value;//QrMov.Params[0].AsInteger; // CliInt
      QrCjSdo.Open;
      //
      QrCjMov.Close;
      QrCjMov.SQL.Clear;
      QrCjMov.SQL.Add('SELECT gru.Conjunto, SUM(lan.Credito) Credito,');
      QrCjMov.SQL.Add('SUM(lan.Debito) Debito, SUM(lan.Credito - lan.Debito) Valor');
      QrCjMov.SQL.Add('FROM ' + DmCond.FTabLctA + ' lan');
      QrCjMov.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
      QrCjMov.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lan.Genero');
      QrCjMov.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo');
      QrCjMov.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
      QrCjMov.SQL.Add('');
      QrCjMov.SQL.Add('WHERE lan.CliInt=' + Empresa);
      QrCjMov.SQL.Add('AND car.Tipo < 2');
      QrCjMov.SQL.Add('AND lan.Data BETWEEN "' + DataI + '" AND "' + DataF + '"');
      QrCjMov.SQL.Add('GROUP BY gru.Conjunto');
      QrCjMov.Open;
      //
      QrMov.Close;
      QrMov.SQL.Clear;
(*
      QrMov.Params[00].AsInteger := DmCond.QrCondCliente.Value;
      QrMov.Params[01].AsString  := DataI;
      QrMov.Params[02].AsString  := DataF;
      QrMov.Open;
*)      //
      QrMov.SQL.Add('DROP TABLE IF EXISTS _MOD_COND_RESUMO_1_;');
      QrMov.SQL.Add('CREATE TABLE _MOD_COND_RESUMO_1_');
      QrMov.SQL.Add('');
      GeraParteSQL(DmCond.FTabLctA, Empresa, DataI, DataF);
      QrMov.SQL.Add('UNION');
      GeraParteSQL(DmCond.FTabLctB, Empresa, DataI, DataF);
      QrMov.SQL.Add('UNION');
      GeraParteSQL(DmCond.FTabLctD, Empresa, DataI, DataF);
      QrMov.SQL.Add(';');
      QrMov.SQL.Add('SELECT NOMEPLANO, NOMECONJUNTO, Conjunto, NOMEGRUPO, Grupo,');
      QrMov.SQL.Add('NOMESUBGRUPO, SubGrupo, NOMECONTA, Genero, Mez, Valor, SubPgto1');
      QrMov.SQL.Add('FROM _MOD_COND_RESUMO_1_');
      QrMov.SQL.Add('ORDER BY OL_PLA, NOMEPLANO, OL_CJT, NOMECONJUNTO,');
      QrMov.SQL.Add('OL_GRU, NOMEGRUPO, OL_SGR, NOMESUBGRUPO,');
      QrMov.SQL.Add('OL_CTA, NOMECONTA, Mez');
      QrMov.SQL.Add(';');
      UMyMod.AbreQuery(QrMov, Dmod.MyDB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');
      //
    end else begin
      // 2008-05-04
      // Deve ser aberto antes pois tem lookup
      if not SaldoDeContasJaAtualizado then
      begin
        if DmCond.QrCondPBB.Value = 0 then
          Mez := MLAGeral.PeriodoToMez(FmCondGer.QrPrevPeriodo.Value - 1)
        else
          Mez := MLAGeral.PeriodoToMez(FmCondGer.QrPrevPeriodo.Value);
        DmodFin.AtualizaContasHistSdo3(DmCond.QrCondCliente.Value, nil(*LaSub1*),
          nil, DModG.MezLastEncerr(), DmCond.FTabLctA);
        DmodFin.SaldosDeContasControladas3(DmCond.QrCondCliente.Value, Mez,
          QrSaldosNiv);

        QrCtasSdo.Close;
        QrCtasSdo.Open;

        QrSdoSgr.Close;
        QrSdoSgr.Open;

        QrSdoGru.Close;
        QrSdoGru.Open;

        QrSdoCjt.Close;
        QrSdoCjt.Open;

        //

        QrMov3.Close;
        QrMov3.SQL.Clear;
        QrMov3.SQL.Add('SELECT pla.Nome NOMEPLANO, cjt.Nome NOMECONJUNTO,');
        QrMov3.SQL.Add('gru.Conjunto, gru.Nome NOMEGRUPO,');
        QrMov3.SQL.Add('sgr.Grupo, sgr.Nome NOMESUBGRUPO,');
        QrMov3.SQL.Add('con.SubGrupo, con.Nome NOMECONTA,');
        QrMov3.SQL.Add('lan.Genero, lan.Mez,');
        QrMov3.SQL.Add('SUM(lan.Credito - lan.Debito) Valor, lan.SubPgto1');
        QrMov3.SQL.Add('FROM ' + DmCond.FTabLctA + ' lan');
        QrMov3.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
        QrMov3.SQL.Add('LEFT JOIN contas    con ON con.Codigo=lan.Genero');
        QrMov3.SQL.Add('LEFT JOIN subgrupos sgr ON sgr.Codigo=con.SubGrupo');
        QrMov3.SQL.Add('LEFT JOIN grupos    gru ON gru.Codigo=sgr.Grupo');
        QrMov3.SQL.Add('LEFT JOIN conjuntos cjt ON cjt.Codigo=gru.Conjunto');
        QrMov3.SQL.Add('LEFT JOIN plano     pla ON pla.Codigo=cjt.Plano');
        QrMov3.SQL.Add('');
        QrMov3.SQL.Add('WHERE car.ForneceI=:P0');
        QrMov3.SQL.Add('AND car.Tipo < 2');
        QrMov3.SQL.Add('AND lan.Genero>0');
        QrMov3.SQL.Add('AND lan.Data BETWEEN :P1 AND :P2');
        QrMov3.SQL.Add('GROUP BY lan.Genero');
        if FAgrupaMensal then
          QrMov3.SQL.Add(', lan.Mez /*, lan.Subpgto1*/');
        QrMov3.SQL.Add('ORDER BY pla.OrdemLista, NOMEPLANO,');
        QrMov3.SQL.Add('                    cjt.OrdemLista, NOMECONJUNTO,');
        QrMov3.SQL.Add('                    gru.OrdemLista, NOMEGRUPO,');
        QrMov3.SQL.Add('                    sgr.OrdemLista, NOMESUBGRUPO,');
        QrMov3.SQL.Add('                    con.OrdemLista, NOMECONTA, lan.Mez');
        QrMov3.SQL.Add('');
        QrMov3.Params[00].AsInteger := DmCond.QrCondCliente.Value;
        QrMov3.Params[01].AsString  := DataI;
        QrMov3.Params[02].AsString  := DataF;
        QrMov3.Open;
      end else Result := True;
    end;
    if Modelo in ([8,9,10,11]) then
    begin
      if (QrConfigBol.State = dsInactive)
      //or (FModeloQrConfigBol <> Modelo)
      or (Config <> QrConfigBol.Params[0].AsInteger) then
      begin
        //FModeloQrConfigBol := Modelo;
        QrConfigBol.Close;
        QrConfigBol.Params[0].AsInteger := Config;
        QrConfigBol.Open;
        //
        if QrConfigBol.RecordCount = 0 then
        begin
          Result := False;
          Geral.MensagemBox('N�o foi definida nenhuma configura��o ' +
          'de impress�o para o formato selecionado!', 'Aviso',
          MB_OK+MB_ICONWARNING);
        end;
      end;
    end;
    if Result then FmCondGer.ReopenResumo;
  end;
}
end;

procedure TDmBloq.ReopenBoletos(BOLAPTO: String);
{ Alexandria
  function TipoBol: String;
  begin
    if FmCondGer.PageControl2.ActivePageIndex = 3 then
      Result := '='
    else
      Result := '<>';
  end;
}
begin
  //Para evitar erros na reabertura
  if(DmCond.FTabAriA = '') or (DmCond.FTabCnsA = '') then Exit;
{
  // Arrecada��es
  QrBolArr.Close;
  QrBolArr.SQL.Clear;
  QrBolArr.SQL.Add('SELECT ari.Boleto, ari.Apto, SUM(ari.Valor) Valor,');
  QrBolArr.SQL.Add('CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO');
  QrBolArr.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
  QrBolArr.SQL.Add('WHERE ari.Codigo=:P0');
  QrBolArr.SQL.Add('GROUP BY ari.Boleto, ari.Apto');
  QrBolArr.Params[00].AsInteger := FmCondGer.QrPrevCodigo.Value;
  QrBolArr.Open;
  //
  // Leituras
  QrBolLei.Close;
  QrBolLei.SQL.Clear;
  QrBolLei.SQL.Add('SELECT cni.Boleto, cni.Apto, SUM(cni.Valor) Valor,');
  QrBolLei.SQL.Add('CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO');
  QrBolLei.SQL.Add('FROM ' + DmCond.FTabCnsA + ' cni');
  QrBolLei.SQL.Add('WHERE cni.Cond=:P0');
  QrBolLei.SQL.Add('AND cni.Periodo=:P1');
  QrBolLei.SQL.Add('GROUP BY cni.Boleto, cni.Apto');
  QrBolLei.Params[00].AsInteger := FmCondGer.QrPrevCondCod.Value;
  QrBolLei.Params[01].AsInteger := FmCondGer.QrPrevPeriodo.Value;
  QrBolLei.Open;
  //
  // Protocolo
  QrPPI.Close;
  QrPPI.Params[00].AsInteger := FmCondGer.QrPrevCodigo.Value;
  QrPPI.Params[01].AsInteger := FmCondGer.QrPrevCondCod.Value;
  QrPPI.Open;
  //
  // Senha de acesso a p�gina web
  QrUsers.Close;
  QrUsers.Params[0].AsInteger := FmCondGer.QrPrevCond.Value;
  QrUsers.Open;

  QrBoletos.Close;
  QrBoletos.SQL.Clear;
  QrBoletos.SQL.Add('SELECT DISTINCT cdb.Ordem, cdi.FracaoIdeal, cdi.Andar, ari.Boleto, ari.Apto,');
  QrBoletos.SQL.Add('ari.Propriet, ari.Vencto, cdi.Unidade,');
  QrBoletos.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrBoletos.SQL.Add('ELSE ent.Nome END NOMEPROPRIET,');
  QrBoletos.SQL.Add('CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO, 0 KGT');
  QrBoletos.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
  QrBoletos.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto');
  QrBoletos.SQL.Add('LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle');
  QrBoletos.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
  QrBoletos.SQL.Add('WHERE ari.Codigo=:P0');
  QrBoletos.SQL.Add('AND ari.Boleto ' + TipoBol + ' 0');
  QrBoletos.SQL.Add('');
  QrBoletos.SQL.Add('UNION');
  QrBoletos.SQL.Add('');
  QrBoletos.SQL.Add('SELECT DISTINCT cdb.Ordem, cdi.FracaoIdeal, cdi.Andar, cni.Boleto, cni.Apto,');
  QrBoletos.SQL.Add('cni.Propriet, cni.Vencto, cdi.Unidade,');
  QrBoletos.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  QrBoletos.SQL.Add('ELSE ent.Nome END NOMEPROPRIET,');
  QrBoletos.SQL.Add('CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO, 0 KGT');
  QrBoletos.SQL.Add('FROM ' + DmCond.FTabCnsA + ' cni');
  QrBoletos.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto');
  QrBoletos.SQL.Add('LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle');
  QrBoletos.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet');
  QrBoletos.SQL.Add('WHERE cni.Cond=:P1');
  QrBoletos.SQL.Add('AND cni.Periodo=:P2');
  QrBoletos.SQL.Add('AND cni.Boleto ' + TipoBol + ' 0');
  QrBoletos.SQL.Add('');
  QrBoletos.SQL.Add('ORDER BY Ordem, Andar, Unidade, Boleto');
  //
  QrBoletos.Params[00].AsInteger := FmCondGer.QrPrevCodigo.Value;
  QrBoletos.Params[01].AsInteger := FmCondGer.QrPrevCond.Value;
  QrBoletos.Params[02].AsInteger := FmCondGer.QrPrevPeriodo.Value;
  QrBoletos.Open;
}


{ Alexandria

  ReopenBoletosQry(QrBoletos, QrBolArr, QrBolLei, QrPPI, QrUsers,
    FmCondGer.QrPrevCodigo.Value, FmCondGer.QrPrevCond.Value,
    FmCondGer.QrPrevPeriodo.Value, TipoBol,
    DmCond.FTabAriA, DmCond.FTabCnsA, '', False);
  //
  if BOLAPTO <> '' then
    QrBoletos.Locate('BOLAPTO', BOLAPTO, [])
  else if FmCondGer.PageControl2.ActivePageIndex = 4 then
    QrBoletos.Locate('BOLAPTO', FmCondGer.FBolSim, [])
  else
    QrBoletos.Locate('BOLAPTO', FmCondGer.FBolNao, []);
  //

  //
  QrComposA.Close;
  QrComposA.SQL.Clear;
  QrComposA.SQL.Add('SELECT ari.Conta, ari.ArreBaI, ari.Texto,');
  QrComposA.SQL.Add('SUM(ari.Valor) VALOR');
  QrComposA.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
  QrComposA.SQL.Add('WHERE ari.Codigo=:P0');
  QrComposA.SQL.Add('AND ari.Boleto = 0');
  QrComposA.SQL.Add('GROUP BY ari.Conta, ari.ArreBaI');
  QrComposA.Params[00].AsInteger := FmCondGer.QrPrevCodigo.Value;
  QrComposA.Open;
  //
  QrComposL.Close;
  QrComposL.SQL.Clear;
  QrComposL.SQL.Add('SELECT cns.Codigo, cns.Nome, SUM(cni.Valor) VALOR, cpr.Casas');
  QrComposL.SQL.Add('FROM cons cns');
  QrComposL.SQL.Add('LEFT JOIN ' + DmCond.FTabCnsA + ' cni ON cni.Codigo=cns.Codigo');
  QrComposL.SQL.Add('LEFT JOIN consprc  cpr ON cpr.Codigo=cns.Codigo AND cpr.Cond=:P0');
  QrComposL.SQL.Add('WHERE cni.Cond=:P1');
  QrComposL.SQL.Add('AND cni.Periodo=:P2');
  QrComposL.SQL.Add('AND cni.Boleto =0');
  QrComposL.SQL.Add('GROUP BY cns.Codigo');
  QrComposL.SQL.Add('ORDER BY cns.Nome');
  QrComposL.Params[00].AsInteger := FmCondGer.QrPrevCond.Value;
  QrComposL.Params[01].AsInteger := FmCondGer.QrPrevCond.Value;
  QrComposL.Params[02].AsInteger := FmCondGer.QrPrevPeriodo.Value;
  QrComposL.Open;
  //
  if FmCondGer.PageControl2.ActivePageIndex = 8 then
  begin
    ReopenListaA(FmCondGer.PageControl2.ActivePageIndex <> 3);
    ReopenListaL(FmCondGer.PageControl2.ActivePageIndex <> 3);
  end;
}
end;

procedure TDmBloq.ReopenBoletosQry(
QryBol, QryArr, QryLei, QryPPI,QryUsr: TmySQLQuery;
PrevCodi, PrevCond, Periodo: Integer;
TipoBol, TabAriA, TabCnsA, Vencto: String; CriaTemp: Boolean);
begin
  // Arrecada��es
  if QryArr <> nil then
  begin
    QryArr.Close;
    QryArr.SQL.Clear;
    QryArr.SQL.Add('SELECT ari.Boleto, ari.Apto, SUM(ari.Valor) Valor,');
    QryArr.SQL.Add('CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO');
    QryArr.SQL.Add('FROM ' + TabAriA + ' ari');
    QryArr.SQL.Add('WHERE ari.Codigo=:P0');
    QryArr.SQL.Add('GROUP BY ari.Boleto, ari.Apto');
    QryArr.Params[00].AsInteger := PrevCodi;
    QryArr.Open;
  end;
  //
  // Leituras
  if QryLei <> nil then
  begin
    QryLei.Close;
    QryLei.SQL.Clear;
    QryLei.SQL.Add('SELECT cni.Boleto, cni.Apto, SUM(cni.Valor) Valor,');
    QryLei.SQL.Add('CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO');
    QryLei.SQL.Add('FROM ' + TabCnsA + ' cni');
    QryLei.SQL.Add('WHERE cni.Cond=:P0');
    QryLei.SQL.Add('AND cni.Periodo=:P1');
    QryLei.SQL.Add('GROUP BY cni.Boleto, cni.Apto');
    QryLei.Params[00].AsInteger := PrevCond;
    QryLei.Params[01].AsInteger := Periodo;
    QryLei.Open;
  end;
  //
  // Protocolo
  if QryPPI <> nil then
  begin
    QryPPI.Close;
    QryPPI.Params[00].AsInteger := PrevCodi;
    QryPPI.Params[01].AsInteger := PrevCond;
    QryPPI.Open;
  end;
  //
  // Senha de acesso a p�gina web
  if QryUsr <> nil then
  begin
    QryUsr.Close;
    QryUsr.Params[0].AsInteger := PrevCond;
    QryUsr.Open;
  end;
  // Boletos e seus totais
  if QryBol <> nil then
  begin
    QryBol.Close;
    QryBol.SQL.Clear;
    if CriaTemp then
    begin
      UCriar.RecriaTempTableNovo(ntrttBoletos, DmodG.QrUpdPID1, False);
      QryBol.SQL.Add('INSERT INTO ' + VAR_MyPID_DB_NOME + '._boletos_ ');
    end;
    QryBol.SQL.Add('SELECT cdb.Ordem, cdi.FracaoIdeal, cdi.Andar, ');
    QryBol.SQL.Add('cdi.Juridico, ari.Boleto, ari.Apto, ');
    QryBol.SQL.Add('ari.Propriet, ari.Vencto, cdi.Unidade,');
    QryBol.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
    QryBol.SQL.Add('ELSE ent.Nome END NOMEPROPRIET,');
    QryBol.SQL.Add('CONCAT_WS("-", ari.Boleto, ari.Apto) BOLAPTO, ');
    QryBol.SQL.Add('cdb.IDExporta cdb_IDExpota, cdi.IDExporta cdi_IDExpota,');
    QryBol.SQL.Add('0 KGT, 1 Ativo, ari.CNAB_Cfg ');
    QryBol.SQL.Add('FROM ' + (*DmCond.F*)TabAriA + ' ari');
    QryBol.SQL.Add('LEFT JOIN condimov cdi ON cdi.Conta=ari.Apto');
    QryBol.SQL.Add('LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle');
    QryBol.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=ari.Propriet');
    QryBol.SQL.Add('WHERE ari.Codigo=:P0');
    QryBol.SQL.Add('AND ari.Boleto ' + TipoBol + ' 0');
    if Vencto <> '' then
      QryBol.SQL.Add('AND ari.Vencto = "' + Vencto + '"');
    QryBol.SQL.Add('GROUP BY ari.Apto, ari.CNAB_Cfg, ari.Boleto, ari.Vencto ');
    QryBol.SQL.Add('');
    QryBol.SQL.Add('UNION');
    QryBol.SQL.Add('');
    QryBol.SQL.Add('SELECT cdb.Ordem, cdi.FracaoIdeal, cdi.Andar, ');
    QryBol.SQL.Add('cdi.Juridico, cni.Boleto, cni.Apto,');
    QryBol.SQL.Add('cni.Propriet, cni.Vencto, cdi.Unidade,');
    QryBol.SQL.Add('CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
    QryBol.SQL.Add('ELSE ent.Nome END NOMEPROPRIET,');
    QryBol.SQL.Add('CONCAT_WS("-", cni.Boleto, cni.Apto) BOLAPTO, ');
    QryBol.SQL.Add('cdb.IDExporta cdb_IDExpota, cdi.IDExporta cdi_IDExpota,');
    QryBol.SQL.Add('0 KGT, 1 Ativo, cni.CNAB_Cfg ');
    QryBol.SQL.Add('FROM ' + (*DmCond.F*)TabCnsA + ' cni');
    QryBol.SQL.Add('LEFT JOIN condimov  cdi ON cdi.Conta=cni.Apto');
    QryBol.SQL.Add('LEFT JOIN condbloco cdb ON cdb.Controle=cdi.Controle');
    QryBol.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo=cni.Propriet');
    QryBol.SQL.Add('WHERE cni.Cond=:P1');
    QryBol.SQL.Add('AND cni.Periodo=:P2');
    QryBol.SQL.Add('AND cni.Boleto ' + TipoBol + ' 0');
    if Vencto <> '' then
      QryBol.SQL.Add('AND cni.Vencto = "' + Vencto + '"');
    QryBol.SQL.Add('GROUP BY cni.Apto, cni.CNAB_Cfg, cni.Boleto, cni.Vencto ');
    QryBol.SQL.Add('');
    QryBol.SQL.Add('ORDER BY Ordem, Andar, Unidade, Boleto');
    //
    QryBol.Params[00].AsInteger := PrevCodi;
    QryBol.Params[01].AsInteger := PrevCond;
    QryBol.Params[02].AsInteger := Periodo;
    //
    if CriaTemp then
    begin
      Dmod.QrUpd.SQL.Text := QryBol.SQL.Text;
      Dmod.QrUpd.Params := QryBol.Params;
      Dmod.QrUpd.ExecSQL;
      //
      QryBol.SQL.Clear;
      QryBol.SQL.Add('SELECT * FROM _boletos_')
    end;
    QryBol.Open;
  end;
end;

procedure TDmBloq.ReopenBoletosIts();
begin
{ Alexandria
  QrBoletosIts.Close;
  QrBoletosIts.SQL.Clear;
  QrBoletosIts.SQL.Add('SELECT ari.Texto TEXTO, ari.Valor VALOR, ari.Vencto,');
  QrBoletosIts.SQL.Add('0 Tipo, 0 MedAnt, 0 MedAtu, 0 Consumo, 0 Casas,');
  QrBoletosIts.SQL.Add('"" UnidLei, "" UnidImp, 1 UnidFat, Controle, Lancto,');
  QrBoletosIts.SQL.Add('0 GeraTyp, 0 GeraFat, 0 CasRat, 0 NaoImpLei, ');
  QrBoletosIts.SQL.Add('"'+ DmCond.FTabAriA +'" TabelaOrig, ari.Conta Genero ');
  QrBoletosIts.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
  QrBoletosIts.SQL.Add('WHERE ari.Boleto=:P0');
  QrBoletosIts.SQL.Add('AND ari.Apto=:P1');
  QrBoletosIts.SQL.Add('AND ari.Codigo=:P2');
  QrBoletosIts.SQL.Add('AND ari.CNAB_Cfg=:P3');
  QrBoletosIts.SQL.Add('');
  QrBoletosIts.SQL.Add('UNION');
  QrBoletosIts.SQL.Add('');
  QrBoletosIts.SQL.Add('SELECT cns.Nome TEXTO, cni.Valor VALOR, cni.Vencto,');
  QrBoletosIts.SQL.Add('1 Tipo, MedAnt, MedAtu, Consumo, Casas,');
  QrBoletosIts.SQL.Add('UnidLei, UnidImp, UnidFat, Controle, Lancto,');
  QrBoletosIts.SQL.Add('cni.GeraTyp, cni.GeraFat, cni.CasRat, cni.NaoImpLei, ');
  QrBoletosIts.SQL.Add('"'+ DmCond.FTabCnsA +'" TabelaOrig, cns.Genero ');
  QrBoletosIts.SQL.Add('FROM cons cns');
  QrBoletosIts.SQL.Add('LEFT JOIN ' + DmCond.FTabCnsA + ' cni ON cni.Codigo=cns.Codigo');
  QrBoletosIts.SQL.Add('WHERE cni.Boleto=:P4');
  QrBoletosIts.SQL.Add('AND cni.Apto=:P5');
  QrBoletosIts.SQL.Add('AND cni.Periodo=:P6');
  QrBoletosIts.SQL.Add('AND cni.CNAB_Cfg=:P7');
  QrBoletosIts.SQL.Add('');
  QrBoletosIts.SQL.Add('ORDER BY VALOR DESC');
  QrBoletosIts.Params[00].AsFloat   := QrBoletosBoleto.Value;
  QrBoletosIts.Params[01].AsInteger := QrBoletosApto.Value;
  QrBoletosIts.Params[02].AsInteger := FmCondGer.QrPrevCodigo.Value;
  QrBoletosIts.Params[03].AsInteger := QrBoletosCNAB_Cfg.Value;
  QrBoletosIts.Params[04].AsFloat   := QrBoletosBoleto.Value;
  QrBoletosIts.Params[05].AsInteger := QrBoletosApto.Value;
  QrBoletosIts.Params[06].AsInteger := FmCondGer.QrPrevPeriodo.Value;
  QrBoletosIts.Params[07].AsInteger := QrBoletosCNAB_Cfg.Value;
  QrBoletosIts.Open;
  //
  if FmCondGer.PageControl2.ActivePageIndex = 4 then
  //if PageIndex = 4 then
    QrBoletosIts.Locate('TEXTO', FmCondGer.FBolItsSim, [])
  else
    QrBoletosIts.Locate('TEXTO', FmCondGer.FBolItsNao, []);
}
end;

procedure TDmBloq.ReopenCNAB_Cfg_Cond(Query: TmySQLQuery;
  Database: TmySQLDatabase; Cond, CNAB_Cfg: Integer);
begin
  if (Cond <> 0) and (CNAB_Cfg = 0) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
      'SELECT 0 + 0.000 Codigo, con.Banco CedBanco, con.Agencia CedAgencia, ',
      'con.DVAgencia CedDAC_A, con.Posto CedPosto, ',
      'con.Conta CedConta, con.DVConta CedDAC_C, ',
      'con.CodCedente CodEmprBco, con.AgContaCed, ',
      'con.LocalPag, con.EspecieDoc, ',
      'con.Carteira CartNum, con.EspecieVal, ',
      'con.OperCodi, con.PercMulta MultaPerc, con.PercJuros JurosPerc, ',
      'con.VTCBBNITAR, con.CNAB, ',
      'con.Cedente, con.SacadAvali, ',
      'con.IDCobranca, con.CartEmiss, ',
      'con.TipoCobranca, con.CtaCooper, ',
      'con.Correio, con.ModalCobr, ',
      'con.Instrucao1 Texto01, con.Instrucao2 Texto02, ',
      'con.Instrucao3 Texto03, con.Instrucao4 Texto04, ',
      'con.Instrucao5 Texto05, con.Instrucao6 Texto06, ',
      'con.Instrucao7 Texto07, con.Instrucao8 Texto08, ',
      'IF(con.Cedente=0, IF(ced.Tipo=0, ced.RazaoSocial, ced.Nome), ',
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome)) NOMECED, ',
      'IF(con.SacadAvali=0, "", IF(sac.Tipo=0, sac.RazaoSocial, sac.Nome)) NOMESAC, ',
      'bcs.Nome NOMEBANCO, con.Aceite ACEITETIT_TXT, "" LayoutRem, ',
      'IF(con.CartTxt <> "", con.CartTxt, con.Carteira) CART_IMP,  ',
      'con.CartEmiss, 0 + 0.000 NosNumFxaU, 0 + 0.000 NosNumFxaI, 0 + 0.000 NosNumFxaF, ',
      'car.TipoDoc CAR_TIPODOC, car.Ativo CART_ATIVO, bcs.DVB, 1 + 0.000 Status, ',
      'con.CorresBco, con.CorresAge, con.CorresCto, con.NPrinBc ',
      'FROM cond con ',
      'LEFT JOIN bancos bcs ON bcs.Codigo = con.Banco ',
      'LEFT JOIN entidades ced ON ced.Codigo=con.Cedente ',
      'LEFT JOIN entidades ent ON ent.Codigo=con.Cliente ',
      'LEFT JOIN entidades sac ON sac.Codigo=con.SacadAvali ',
      'LEFT JOIN carteiras car ON car.Codigo=con.CartEmiss ',
      'WHERE con.Codigo=' + Geral.FF0(Cond),
      '']);
  end else
  if (Cond <> 0) and (CNAB_Cfg <> 0) then
  begin
    UnDmkDAC_PF.AbreMySQLQuery0(Query, Database, [
      'SELECT cfg.Codigo + 0.000 Codigo, cfg.CedBanco, cfg.CedAgencia, ',
      'cfg.CedDAC_A, cfg.CedPosto, ',
      'cfg.CedConta, cfg.CedDAC_C, ',
      'cfg.CodEmprBco, cfg.AgContaCed, ',
      'cfg.LocalPag, cfg.EspecieDoc, ',
      'cfg.CartNum, cfg.EspecieTxt EspecieVal, ',
      'cfg.OperCodi, cfg.MultaPerc, cfg.JurosPerc, ',
      'cfg.VTCBBNITAR, cfg.CNAB, ',
      'cfg.Cedente, cfg.SacadAvali, ',
      'cfg.IDCobranca, cfg.CartEmiss, ',
      'cfg.TipoCobranca, cfg.CtaCooper, ',
      'IF(cfg.Correio = 1, "S", "N") Correio, cfg.ModalCobr, ',
      'cfg.Texto01, cfg.Texto02, ',
      'cfg.Texto03, cfg.Texto04, ',
      'cfg.Texto05, cfg.Texto06, ',
      'cfg.Texto07, cfg.Texto08, ',
      'IF(cfg.CedNome <> "", cfg.CedNome, IF(ced.Tipo=0, ced.RazaoSocial, ced.Nome)) NOMECED, ',
      'IF(cfg.SacAvaNome="", IF(cfg.SacadAvali=0, "", IF(sac.Tipo=0, sac.RazaoSocial, sac.Nome)), cfg.SacAvaNome) NOMESAC, ',
      'bcs.Nome NOMEBANCO, IF(cfg.AceiteTit=1, "S", "N") ACEITETIT_TXT, cfg.LayoutRem, ',
      'IF(cfg.CartTxt <> "", cfg.CartTxt, cfg.CartNum) CART_IMP, ',
      'cfg.CartEmiss, cfg.NosNumFxaU + 0.000 NosNumFxaU, cfg.NosNumFxaI + 0.000 NosNumFxaI, cfg.NosNumFxaF + 0.000 NosNumFxaF, ',
      'car.TipoDoc CAR_TIPODOC, car.Ativo CART_ATIVO, bcs.DVB, IF(cfg.TermoAceite <> 0, 1, 0) + 0.000 Status, ',
      'cfg.CorresBco, cfg.CorresAge, cfg.CorresCto, cfg.NPrinBc ',
      'FROM cnab_cfg cfg ',
      'LEFT JOIN bancos bcs ON bcs.Codigo = cfg.CedBanco ',
      'LEFT JOIN entidades ced ON ced.Codigo=cfg.Cedente ',
      'LEFT JOIN entidades sac ON sac.Codigo=cfg.SacadAvali ',
      'LEFT JOIN carteiras car ON car.Codigo=cfg.CartEmiss ',
      'WHERE cfg.Codigo=' + Geral.FF0(CNAB_Cfg),
      '']);
  end else
  begin
    Query.Close;
    //
    Geral.MB_Aviso('Falha ao reabrir query "' + Query.Name + '"!');
  end;
end;

procedure TDmBloq.ReopenComposAIts(Apto: Integer);
begin
{ Alexandria
  QrComposAIts.Close;
  QrComposAIts.SQL.Clear;
  QrComposAIts.SQL.Add('SELECT CASE WHEN prp.Tipo=0 THEN prp.RazaoSocial');
  QrComposAIts.SQL.Add('ELSE prp.Nome END NOMEPROPRIET, imv.Unidade,');
  QrComposAIts.SQL.Add('ari.Apto, ari.Valor, ari.Propriet, ari.Controle, ');
  QrComposAIts.SQL.Add('ari.Lancto, ari.Boleto ');
  QrComposAIts.SQL.Add('FROM ' + DmCond.FTabAriA + ' ari');
  QrComposAIts.SQL.Add('LEFT JOIN entidades prp ON prp.Codigo=ari.Propriet');
  QrComposAIts.SQL.Add('LEFT JOIN condimov imv ON imv.Conta=ari.Apto');
  QrComposAIts.SQL.Add('WHERE ari.Codigo=:P0');
  QrComposAIts.SQL.Add('AND ari.Boleto = 0');
  QrComposAIts.SQL.Add('AND ari.Conta=:P1');
  QrComposAIts.SQL.Add('AND ari.ArreBaI=:P2');
  QrComposAIts.SQL.Add('ORDER BY imv.Unidade');
  QrComposAIts.Params[00].AsInteger := FmCondGer.QrPrevCodigo.Value;
  QrComposAIts.Params[01].AsInteger := QrComposAConta.Value;
  QrComposAIts.Params[02].AsInteger := QrComposAArreBaI.Value;
  QrComposAIts.Open;
  //
}
end;

procedure TDmBloq.ReopenComposLIts(Apto: Integer);
var
  FmtTxT: String;
begin
{ Alexandria
  FmtTxT := MLAGeral.FormataCasas(QrComposLCasas.Value);
  QrComposLItsMedAnt.DisplayFormat := FmtTxt;
  QrComposLItsMedAtu.DisplayFormat := FmtTxt;
  QrComposLItsConsumo.DisplayFormat := FmtTxt;
  //
  QrComposLIts.Close;
  QrComposLIts.SQL.Clear;
  QrComposLIts.SQL.Add('SELECT cni.Apto, cni.MedAnt, cni.MedAtu,');
  QrComposLIts.SQL.Add('cni.Consumo, cni.Controle, cni.Valor,');
  QrComposLIts.SQL.Add('CASE WHEN prp.Tipo=0 THEN prp.RazaoSocial');
  QrComposLIts.SQL.Add('ELSE prp.Nome END NOMEPROPRIET, imv.Unidade, ');
  QrComposLIts.SQL.Add('cni.Lancto, cni.Boleto ');
  QrComposLIts.SQL.Add('FROM ' + DmCond.FTabCnsA + ' cni');
  QrComposLIts.SQL.Add('LEFT JOIN entidades prp ON prp.Codigo=cni.Propriet');
  QrComposLIts.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=cni.Apto');
  QrComposLIts.SQL.Add('WHERE cni.Cond=:P0');
  QrComposLIts.SQL.Add('AND cni.Periodo=:P1');
  QrComposLIts.SQL.Add('AND cni.Boleto =0');
  QrComposLIts.SQL.Add('AND cni.Codigo=:P2');
  QrComposLIts.SQL.Add('ORDER BY imv.Unidade');
  QrComposLIts.Params[00].AsInteger := FmCondGer.QrPrevCond.Value;
  QrComposLIts.Params[01].AsInteger := FmCondGer.QrPrevPeriodo.Value;
  QrComposLIts.Params[02].AsInteger := QrComposLCodigo.Value;
  QrComposLIts.Open;
  //
}
end;

procedure TDmBloq.ReopenEnderecoInquilino(Apto: Integer);
var
  Conta: Integer;
begin
  if Apto = 0 then
    Conta := QrBoletosApto.Value
  else
    Conta := Apto;
  //
  QrDonoUH.Close;
  QrDonoUH.Params[0].AsInteger := Conta;
  UMyMod.AbreQuery(QrDonoUH, Dmod.MyDB);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrIC, Dmod.MyDB, [
    'SELECT en.Codigo, en.Tipo, ',
    'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome    END NOMEENT, ',
    'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF     END CNPJ_CPF, ',
    'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG      END IE_RG, ',
    'CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""         END NIRE_, ',
    'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua    END RUA, ',
    'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero END + 0.000 NUMERO, ',
    'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL, ',
    'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIRRO, ',
    'IF(en.Tipo=0, IF(en.ECodMunici = 0, en.ECidade, emu.Nome), IF(en.PCodMunici = 0, en.PCidade, pmu.Nome)) CIDADE, ',
    'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOMELOGRAD, ',
    'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOMEUF, ',
    'IF(en.Tipo=0, IF(en.ECodiPais = 0, en.EPais, epa.Nome), IF(en.PCodiPais = 0, en.PPais, ppa.Nome)) Pais, ',
    'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END + 0.000 Lograd, ',
    'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END + 0.000 CEP, ',
    'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1, ',
    'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX, ',
    'ELT(en.Tipo+1, "CNPJ", "CPF") NOMETIPO ',
    'FROM entidades en ',
    'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF ',
    'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF ',
    'LEFT JOIN ufs ufc ON ufc.Codigo=en.CUF ',
    'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd ',
    'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici emu ON emu.Codigo=en.ECodMunici ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.dtb_munici pmu ON pmu.Codigo=en.PCodMunici ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais epa ON epa.Codigo=en.ECodiPais ',
    'LEFT JOIN ' + VAR_AllID_DB_NOME + '.bacen_pais ppa ON ppa.Codigo=en.PCodiPais ',
    'WHERE en.Codigo = (SELECT ',
    'CASE imv.BloqEndTip ',
    '  WHEN 0 THEN (IF(imv.Usuario <> 0, cnd.Cliente, imv.Propriet) ) ',
    '  WHEN 1 THEN cnd.Cliente ',
    '  WHEN 2 THEN imv.Propriet ',
    '  WHEN 3 THEN BloqEndEnt ',
    '  WHEN 4 THEN 0 ',
    'END ',
    'FROM condimov imv ',
    'LEFT JOIN cond cnd ON cnd.Codigo=imv.Codigo ',
    'WHERE imv.Conta=' + Geral.FF0(Conta),
    ') ',
    '']);
  //
  QrInquilino.Close;
  QrInquilino.Params[00].AsInteger := Conta;
  UMyMod.AbreQuery(QrInquilino, Dmod.MyDB);
end;

procedure TDmBloq.ReopenListaA(ComBoleto: Boolean);
begin
{ Alexandria
  if ComBoleto then
    FEstagioA := '>'
  else
    FEstagioA := '=';
  QrListaA.Close;
  QrListaA.SQL.Text :=
    'SELECT ari.Conta, con.Nome NOME_CONTA, con.Sigla SIGLA_CONTA,        '+#13+
    'ari.ArreBaI, ari.Texto, bac.Codigo, bac.Nome NOME_ARREC,             '+#13+
    'bac.Sigla SIGLA_BAC, SUM(ari.Valor) VALOR, abi.InfoRelArr,           '+#13+
    'abi.DescriCota, abi.DeQuem, abi.Partilha, abi.Calculo                '+#13+
    'FROM ' + DmCond.FTabAriA + ' ari                                     '+#13+
    'LEFT JOIN arrebai abi ON abi.Controle=ari.ArreBaI                    '+#13+
    'LEFT JOIN arrebac bac ON bac.Codigo=abi.Codigo                       '+#13+
    'LEFT JOIN contas con ON con.Codigo=ari.Conta                         '+#13+
    'WHERE ari.Codigo=:P0                                                 '+#13+
    'AND ari.Boleto ' + FEstagioA + ' 0                                    '+#13;
  case FmCondger.RGAgrupListaA.ItemIndex of
    0: QrListaA.SQL.Add('GROUP BY ari.Conta, ari.ArreBaI');
    1: QrListaA.SQL.Add('GROUP BY bac.Codigo');
    2: QrListaA.SQL.Add('GROUP BY ari.Conta');
  end;                                              ;
  QrListaA.Params[00].AsInteger := FmCondGer.QrPrevCodigo.Value;
  QrListaA.Open;
}
end;

procedure TDmBloq.ReopenListaAIts(Apto: Integer);
begin
{ Alexandria
  QrListaAIts.Close;
  QrListaAIts.SQL.Text :=
    'SELECT bai.DescriCota, bai.DeQuem, bai.Calculo, bai.Partilha,'        +#13+
    'abu.Percent, imv.Moradores, imv.FracaoIdeal, '                        +#13+
    'CASE WHEN prp.Tipo=0 THEN prp.RazaoSocial'                            +#13+
    'ELSE prp.Nome END NOMEPROPRIET, imv.Unidade, '                        +#13+
    'ari.Apto, ari.Valor, ari.Propriet, ari.Controle,'                     +#13+
    'ari.ArreBaI, ari.Boleto'                                              +#13+
    'FROM ' + DmCond.FTabAriA + ' ari'                                     +#13+
    'LEFT JOIN arrebai bai ON bai.Controle=ari.ArreBaI'                    +#13+
    'LEFT JOIN arrebac bac ON bac.Codigo=bai.Codigo'                       +#13+
    'LEFT JOIN entidades prp ON prp.Codigo=ari.Propriet'                   +#13+
    'LEFT JOIN condimov imv ON imv.Conta=ari.Apto'                         +#13+
    'LEFT JOIN arrebau abu '                                               +#13+
    'ON abu.Controle=ari.ArreBaI AND imv.Conta=abu.Apto'                   +#13+
    'WHERE ari.Codigo=' +
      dmkPF.FFP(FmCondGer.QrPrevCodigo.Value, 0)                        +#13+
    'AND ari.Boleto '+ FEstagioA + ' 0'                                    +#13;
    //QrListaAIts.Params[00].AsInteger := FmCondGer.QrPrevCodigo.Value;
  case FmCondger.RGAgrupListaA.ItemIndex of
    0:
    begin
      QrListaAIts.SQL.Add('AND ari.Conta=' +
        dmkPF.FFP(QrListaAConta.Value, 0));
      QrListaAIts.SQL.Add('AND ari.ArreBaI=' +
        dmkPF.FFP(QrListaAArreBaI.Value, 0));
      //QrListaAIts.Params[01].AsInteger := QrListaAConta.Value;
      //QrListaAIts.Params[02].AsInteger := QrListaAArreBaI.Value;
    end;
    1:
    begin
      if QrListaACodigo.Value = 0 then
      begin
        QrListaAIts.SQL.Add('AND bac.Codigo IS NULL OR bac.Codigo=0');
      end else begin
        QrListaAIts.SQL.Add('AND bac.Codigo=' +
          dmkPF.FFP(QrListaACodigo.Value, 0));
        //QrListaAIts.Params[01].AsInteger := QrListaACodigo.Value;
      end;
    end;
    2:
    begin
      QrListaAIts.SQL.Add('AND ari.Conta=' +
        dmkPF.FFP(QrListaAConta.Value, 0));
      //QrListaAIts.Params[01].AsInteger := QrListaAConta.Value;
    end;
  end;
  QrListaAIts.SQL.Add('ORDER BY imv.Unidade');
  QrListaAIts.Open;
  //
}
end;

procedure TDmBloq.ReopenListaL(ComBoleto: Boolean);
begin
{ Alexandria
  if ComBoleto then
    FEstagioL := '>'
  else
    FEstagioL := '=';
  QrListaL.Close;
  QrListaL.SQL.Clear;
  QrListaL.SQL.Add('SELECT cni.UnidLei,  cns.Codigo, cns.Nome, SUM(cni.Valor) VALOR,');
  QrListaL.SQL.Add('cpr.Casas, cns.Sigla');
  QrListaL.SQL.Add('FROM cons cns');
  QrListaL.SQL.Add('LEFT JOIN ' + DmCond.FTabCnsA + ' cni ON cni.Codigo=cns.Codigo');
  QrListaL.SQL.Add('LEFT JOIN consprc  cpr ON cpr.Codigo=cns.Codigo AND cpr.Cond=:P0');
  QrListaL.SQL.Add('WHERE cni.Cond=:P1');
  QrListaL.SQL.Add('AND cni.Periodo=:P2');
  QrListaL.SQL.Add('AND cni.Boleto ' + FEstagioL + '0');
  QrListaL.SQL.Add('GROUP BY cns.Codigo');
  QrListaL.SQL.Add('ORDER BY cns.Nome');
  QrListaL.Params[00].AsInteger := FmCondGer.QrPrevCond.Value;
  QrListaL.Params[01].AsInteger := FmCondGer.QrPrevCond.Value;
  QrListaL.Params[02].AsInteger := FmCondGer.QrPrevPeriodo.Value;
  QrListaL.Open;
  //
}
end;

procedure TDmBloq.ReopenListaLIts(Apto: Integer);
var
  FmtTxT: String;
begin
{ Alexandria
  FmtTxT := MLAGeral.FormataCasas(QrListaLCasas.Value);
  QrListaLItsMedAnt.DisplayFormat := FmtTxt;
  QrListaLItsMedAtu.DisplayFormat := FmtTxt;
  QrListaLItsConsumo.DisplayFormat := FmtTxt;
  //
  QrListaLIts.Close;
  QrListaLIts.SQL.Clear;
  QrListaLIts.SQL.Add('SELECT cni.Apto, cni.MedAnt, cni.MedAtu, cni.Consumo, cni.Controle,');
  QrListaLIts.SQL.Add('cni.Valor, CASE WHEN prp.Tipo=0 THEN prp.RazaoSocial');
  QrListaLIts.SQL.Add('ELSE prp.Nome END NOMEPROPRIET, imv.Unidade, cni.Boleto');
  QrListaLIts.SQL.Add('FROM ' + DmCond.FTabCnsA + ' cni');
  QrListaLIts.SQL.Add('LEFT JOIN entidades prp ON prp.Codigo=cni.Propriet');
  QrListaLIts.SQL.Add('LEFT JOIN condimov  imv ON imv.Conta=cni.Apto');
  QrListaLIts.SQL.Add('WHERE cni.Cond=:P0');
  QrListaLIts.SQL.Add('AND cni.Periodo=:P1');
  QrListaLIts.SQL.Add('AND cni.Boleto ' + FEstagioL + '0');
  QrListaLIts.SQL.Add('AND cni.Codigo=:P2');
  QrListaLIts.SQL.Add('');
  QrListaLIts.SQL.Add('ORDER BY imv.Unidade');
  QrListaLIts.SQL.Add('');
  QrListaLIts.Params[00].AsInteger := FmCondGer.QrPrevCond.Value;
  QrListaLIts.Params[01].AsInteger := FmCondGer.QrPrevPeriodo.Value;
  QrListaLIts.Params[02].AsInteger := QrListaLCodigo.Value;
  QrListaLIts.Open;
  //
}
end;

procedure TDmBloq.ReopenPendU(Entidade, Apto: Integer);
begin
  QrPendU.Close;
  QrPendU.SQL.Clear;
  QrPendU.SQL.Add('SELECT SUM(lan.Pago-lan.Credito) SALDO');
  QrPendU.SQL.Add('FROM ' + DmCond.FTabLctA + ' lan');
  QrPendU.SQL.Add('LEFT JOIN carteiras car ON car.Codigo=lan.Carteira');
  QrPendU.SQL.Add('WHERE car.Tipo=2');
  QrPendU.SQL.Add('AND lan.Reparcel=0');
  QrPendU.SQL.Add('AND car.ForneceI=' + FormatFloat('0', Entidade));
  QrPendU.SQL.Add('AND lan.Depto=' + FormatFloat('0', Apto));
  QrPendU.SQL.Add('AND lan.Sit<2');
  QrPendU.SQL.Add('AND lan.Vencimento < SYSDATE()');
  {
  QrPendU.Params[0].AsInteger := FmCondGer.QrPrevCondCli.Value;
  QrPendU.Params[1].AsInteger := QrBoletosApto.Value;
  }
  QrPendU.Open;
end;

procedure TDmBloq.ReopenPrevModBol(Apto: Integer);
begin
{ Alexandria
  QrPrevModBol.Close;
  QrPrevModBol.Params[0].AsInteger := FmCondGer.QrPrevCodigo.Value;
  QrPrevModBol.Open;
  //
  if Apto > 0 then
    QrPrevModBol.Locate('Apto', Apto, []);
}
end;

procedure TDmBloq.Reopen_BLC_BLE();
  procedure GeraParteSQL(Qry: TmySQLQuery; TabLct, Cliente, Mez, Propriet, Apto, Boleto, BLX: String);
  begin
    Qry.SQL.Add('SELECT lct.*');
    Qry.SQL.Add('FROM ' + TabLct + ' lct');
    Qry.SQL.Add('WHERE lct.FatID>=600');
    Qry.SQL.Add('AND lct.CliInt=' + Cliente);
    Qry.SQL.Add('AND lct.Mez=' + Mez);
    Qry.SQL.Add('AND lct.ForneceI=' + Propriet);
    Qry.SQL.Add('AND lct.Depto=' + Apto);
    Qry.SQL.Add('AND lct.FatNum' + BLX + Boleto);
  end;
var
  Mez, Cliente, Propriet, Apto, Boleto: String;
begin
 { Alexandria
 // Testar no 244
  QrBLE.Close;
  QrBLE.Database := DModG.MyPID_DB;
  QrBLC.Close;
  if FmCondGer.PageControl2.ActivePageIndex = 8 then
  begin
    Mez := FormatFloat('0', MLAGeral.PeriodoToMez(FmCondGer.QrPrevPeriodo.Value));
    Cliente  := FormatFloat('0', DmCond.QrCondCliente.Value);
    Propriet := FormatFloat('0', QrBoletosPropriet.Value);
    Apto     := FormatFloat('0', QrBoletosApto.Value);
    Boleto   := FormatFloat('0',QrBoletosBoleto.Value);
    //
    QrBLE.SQL.Clear;
    //
    QrBLE.SQL.Add('DROP TABLE IF EXISTS _MOD_BLOQ_BLE_1_;');
    QrBLE.SQL.Add('CREATE TABLE _MOD_BLOQ_BLE_1_');
    QrBLE.SQL.Add('');
    GeraParteSQL(QrBLE, DmCond.FTabLctA, Cliente, Mez, Propriet, Apto, Boleto, '<>');
    QrBLE.SQL.Add('UNION');
    GeraParteSQL(QrBLE, DmCond.FTabLctB, Cliente, Mez, Propriet, Apto, Boleto, '<>');
    QrBLE.SQL.Add('UNION');
    GeraParteSQL(QrBLE, DmCond.FTabLctD, Cliente, Mez, Propriet, Apto, Boleto, '<>');
    QrBLE.SQL.Add(';');
    QrBLE.SQL.Add('');
    (* 2011-05-10
    QrBLE.SQL.Add('SELECT SUM(Credito) Credito, -SUM(Debito) Debito ');
    QrBLE.SQL.Add('FROM _MOD_BLOQ_BLE_1_;');
    *)
    QrBLE.SQL.Add('SELECT * FROM _MOD_BLOQ_BLE_1_;');
    // FIM 2011-05-10
    UMyMod.AbreQuery(QrBLE, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');

    //


    //
    QrBLC.SQL.Clear;
    //
    QrBLC.SQL.Add('DROP TABLE IF EXISTS _MOD_BLOQ_BLC_1_;');
    QrBLC.SQL.Add('CREATE TABLE _MOD_BLOQ_BLC_1_');
    QrBLC.SQL.Add('');
    GeraParteSQL(QrBLC, DmCond.FTabLctA, Cliente, Mez, Propriet, Apto, Boleto, '=');
    QrBLC.SQL.Add('UNION');
    GeraParteSQL(QrBLC, DmCond.FTabLctB, Cliente, Mez, Propriet, Apto, Boleto, '=');
    QrBLC.SQL.Add('UNION');
    GeraParteSQL(QrBLC, DmCond.FTabLctD, Cliente, Mez, Propriet, Apto, Boleto, '=');
    QrBLC.SQL.Add(';');
    QrBLC.SQL.Add('');
    (* 2011-05-10
    QrBLC.SQL.Add('SELECT SUM(Credito) Credito, -SUM(Debito) Debito ');
    QrBLC.SQL.Add('FROM _MOD_BLOQ_BLC_1_;');
    *)
    QrBLC.SQL.Add('SELECT * FROM _MOD_BLOQ_BLC_1_;');
    // FIM 2011-05-10
    UMyMod.AbreQuery(QrBLC, DModG.MyPID_DB, 'TFmReceDesp.DemostrativoDeReceitasEDespesas()');

    //
  end;
  //
}
end;

function TDmBloq.TextoExplicativoItemBoleto(EhConsumo, Casas: Integer; MedAnt,
  MedAtu, Consumo, UnidFat: Double; UnidLei, UnidImp: String; GeraTyp, CasRat,
  NaoImpLei: Integer; GeraFat: Double): String;
var
  Leitura: String;
begin
  if EhConsumo = 0 then Result := '' else
  begin
    if GeraTyp = 0 then
      Result := ' (' + Geral.FFT(MedAnt, Casas, siNegativo) + ' ' +
      ' > ' + Geral.FFT(MedAtu, Casas, siNegativo) + ' ' +
      ' = ' + Geral.FFT(Consumo, Casas, siNegativo) + ' ' +UnidLei +
      ' ~ ' + Geral.FFT(Consumo * UnidFat, Casas, siNegativo) +
      ' ' + UnidImp + ')'
    else begin
      if NaoImpLei = 1 then Leitura := '' else Leitura := ' ~ ' +
        Geral.FFT(Consumo * UnidFat, Casas, siNegativo) + ' ' + UnidLei;
      Result := ' (' + UnidImp + ': ' + Geral.FFT(GeraFat, CasRat, siNegativo) +
      Leitura + ')';
    end;
  end;
end;

function TDmBloq.TodosSelecionadosEstaoSemRestricaoJuridica(Tipo: TSelType;
  LoteImp: Integer; PageControl: TPageControl; DBGradeN,
  DBGradeS: TDBGrid): Boolean;
var
  Com, i: Integer;
  Grade: TDBGrid;
  Hoje: TDateTime;
begin
  //Result := False;
  Com := 0;
  Hoje := Int(DmodG.ObtemAgora());
  case Tipo of
    istAtual:
    begin
      if (QrBoletosVencto.Value < Hoje) and
      (QrBoletosJuridico.Value > 0)  then Inc(Com, 1);
    end;
    istTodos:
    begin
      QrBoletos.First;
      QrBoletos.DisableControls;
      while not QrBoletos.Eof do
      begin
        if (QrBoletosVencto.Value < Hoje) and
        (QrBoletosJuridico.Value > 0)  then Inc(Com, 1);
        QrBoletos.Next;
      end;
      QrBoletos.EnableControls;
    end;
    istMarcados:
    begin
      QrBoletos.First;
      QrBoletos.DisableControls;
      while not QrBoletos.Eof do
      begin
        if QrBoletosLOTE_PROTOCO.Value  = LoteImp then
          if (QrBoletosVencto.Value < Hoje) and
          (QrBoletosJuridico.Value > 0)  then
            Inc(Com, 1);
        QrBoletos.Next;
      end;
      QrBoletos.EnableControls;
    end;
    istSelecionados:
    begin
      case PageControl.ActivePageIndex of
        3: Grade := DBGradeN;
        4: Grade := TDBGrid(DBGradeS);
        else Grade := nil;
      end;
      if Grade = nil then Geral.MensagemBox('Grade n�o definida!', 'Erro',
      MB_OK+MB_ICONERROR) else
      begin
        with Grade.DataSource.DataSet do
        for i:= 0 to Grade.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
          GotoBookmark(Grade.SelectedRows.Items[i]);
          if (QrBoletosVencto.Value < Hoje) and
          (QrBoletosJuridico.Value > 0)  then
            Inc(Com, 1);
        end;
      end;
    end;
  end;
  if Com = 0 then Result := True else
  begin
    if Com = 1 then
     Geral.MensagemBox('H� um item com restri��o jur�dica!',
     'Aviso', MB_OK+MB_ICONWARNING)
    else
     Geral.MensagemBox('H� ' + IntToStr(com) + ' itens com ' +
     'restri��o jur�dica!', 'Aviso', MB_OK+MB_ICONWARNING);
    //
    // Pedir senha se deseja assim mesmo!
    Result := DBCheck.LiberaPelaSenhaBoss('SITUA��O JUR�DICA EXIGE SENHA!');
  end;
end;

function TDmBloq.TodosSelecionadosTemVencimento(Tipo: TSelType; LoteImp:
Integer; PageControl: TPageControl; DBGradeN, DBGradeS: TDBGrid): Boolean;
var
  Sem, i: Integer;
  Grade: TDBGrid;
begin
  Result := False;
  Sem := 0;
  case Tipo of
    istAtual:
    begin
      if QrBoletosVencto.Value < 2 then Inc(Sem, 1);
    end;
    istTodos:
    begin
      QrBoletos.First;
      QrBoletos.DisableControls;
      while not QrBoletos.Eof do
      begin
        if QrBoletosVencto.Value < 2 then Inc(Sem, 1);
        QrBoletos.Next;
      end;
      QrBoletos.EnableControls;
    end;
    istMarcados:
    begin
      QrBoletos.First;
      QrBoletos.DisableControls;
      while not QrBoletos.Eof do
      begin
        if QrBoletosLOTE_PROTOCO.Value  = LoteImp then
          if QrBoletosVencto.Value < 2 then
            Inc(Sem, 1);
        QrBoletos.Next;
      end;
      QrBoletos.EnableControls;
    end;
    istSelecionados:
    begin
      case PageControl.ActivePageIndex of
        3: Grade := DBGradeN;
        4: Grade := TDBGrid(DBGradeS);
        else Grade := nil;
      end;
      if Grade = nil then Geral.MensagemBox('Grade n�o definida!', 'Erro',
      MB_OK+MB_ICONERROR) else
      begin
        with Grade.DataSource.DataSet do
        for i:= 0 to Grade.SelectedRows.Count-1 do
        begin
          //GotoBookmark(pointer(Grade.SelectedRows.Items[i]));
          GotoBookmark(Grade.SelectedRows.Items[i]);
          if QrBoletosVencto.Value < 2 then Inc(Sem, 1);
        end;
      end;
    end;
  end;
  if Sem = 0 then Result := True else
  begin
    if Sem = 1 then
     Geral.MensagemBox('H� um item sem vencimento impedindo a impress�o!',
     'Aviso', MB_OK+MB_ICONWARNING)
    else
     Geral.MensagemBox('H� ' + IntToStr(Sem) + ' itens sem ' +
     'vencimento impedindo a impress�o!', 'Aviso', MB_OK+MB_ICONWARNING);
   end;
end;

function TDmBloq.ValidaModeloBoleto(RGModelBloq, RGCompe: TRadioGroup; Config: Integer): Boolean;
begin
  case RGModelBloq.ItemIndex of
        5: Result := RGCompe.ItemIndex > 0;
     8..9: Result := Config <> 0;
     else  Result := RGModelBloq.ItemIndex > 0;
  end;
end;

end.
