object FmFlxMensBalAdd: TFmFlxMensBalAdd
  Left = 339
  Top = 185
  Caption = 'FLX-BALAN-001 :: Abertura de Fluxo de Balancete'
  ClientHeight = 362
  ClientWidth = 564
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 564
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 516
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 468
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 382
        Height = 32
        Caption = 'Abertura de Fluxo de Balancete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 382
        Height = 32
        Caption = 'Abertura de Fluxo de Balancete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 382
        Height = 32
        Caption = 'Abertura de Fluxo de Balancete'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 564
    Height = 200
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 564
      Height = 200
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 564
        Height = 200
        Align = alClient
        TabOrder = 0
        object Panel5: TPanel
          Left = 2
          Top = 64
          Width = 560
          Height = 134
          Align = alClient
          TabOrder = 0
          object GroupBox40: TGroupBox
            Left = 1
            Top = 37
            Width = 558
            Height = 96
            Align = alClient
            Caption = ' Dia (m'#234's) limite* : '
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            ExplicitLeft = -2
            ExplicitTop = 36
            object Label217: TLabel
              Left = 12
              Top = 20
              Width = 63
              Height = 13
              Caption = 'Concilia'#231#245'es:'
              FocusControl = EdFlxM_Conci
            end
            object Label218: TLabel
              Left = 12
              Top = 44
              Width = 66
              Height = 13
              Caption = 'Documentos: '
              FocusControl = EdFlxM_Docum
            end
            object Label222: TLabel
              Left = 196
              Top = 64
              Width = 40
              Height = 13
              Caption = 'Entrega:'
              FocusControl = EdFlxM_Entrg
            end
            object Label221: TLabel
              Left = 196
              Top = 40
              Width = 76
              Height = 13
              Caption = 'Encadernac'#227'o: '
              FocusControl = EdFlxM_Encad
            end
            object Label220: TLabel
              Left = 12
              Top = 68
              Width = 94
              Height = 13
              Caption = 'ISS / contabilidade:'
              FocusControl = EdFlxM_Contb
            end
            object Label219: TLabel
              Left = 196
              Top = 16
              Width = 37
              Height = 13
              Caption = 'An'#225'lise:'
              FocusControl = EdFlxM_Anali
            end
            object Label2: TLabel
              Left = 375
              Top = 16
              Width = 26
              Height = 13
              Caption = 'Web:'
              FocusControl = EdFlxM_Web
            end
            object EdFlxM_Conci: TdmkEdit
              Left = 142
              Top = 16
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '31'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'FlxM_Conci'
              UpdCampo = 'FlxM_Conci'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object EdFlxM_Docum: TdmkEdit
              Left = 142
              Top = 40
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 1
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '31'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'FlxM_Docum'
              UpdCampo = 'FlxM_Docum'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object EdFlxM_Entrg: TdmkEdit
              Left = 326
              Top = 60
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '31'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'FlxM_Entrg'
              UpdCampo = 'FlxM_Entrg'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object EdFlxM_Encad: TdmkEdit
              Left = 326
              Top = 36
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '31'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'FlxM_Ecade'
              UpdCampo = 'FlxM_Ecade'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object EdFlxM_Contb: TdmkEdit
              Left = 142
              Top = 64
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '31'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'FlxM_Contb'
              UpdCampo = 'FlxM_Contb'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object EdFlxM_Anali: TdmkEdit
              Left = 326
              Top = 12
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '31'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'FlxM_Anali'
              UpdCampo = 'FlxM_Anali'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
            object EdFlxM_Web: TdmkEdit
              Left = 505
              Top = 12
              Width = 44
              Height = 21
              Alignment = taRightJustify
              TabOrder = 6
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '0'
              ValMax = '31'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'FlxM_Anali'
              UpdCampo = 'FlxM_Anali'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
          end
          object Panel6: TPanel
            Left = 1
            Top = 1
            Width = 558
            Height = 36
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Label223: TLabel
              Left = 12
              Top = 12
              Width = 149
              Height = 13
              Caption = 'Ordem na lista de condom'#237'nios:'
            end
            object EdFlxM_Ordem: TdmkEdit
              Left = 168
              Top = 8
              Width = 68
              Height = 21
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              QryCampo = 'FlxM_Ordem'
              UpdCampo = 'FlxM_Ordem'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
            end
          end
        end
        object Panel7: TPanel
          Left = 2
          Top = 15
          Width = 560
          Height = 49
          Align = alTop
          Enabled = False
          TabOrder = 1
          object Label1: TLabel
            Left = 8
            Top = 4
            Width = 60
            Height = 13
            Caption = 'Condom'#237'nio:'
          end
          object LaMes: TLabel
            Left = 492
            Top = 4
            Width = 23
            Height = 13
            Caption = 'M'#234's:'
          end
          object EdEmpresa: TdmkEditCB
            Left = 8
            Top = 20
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 0
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            DBLookupComboBox = CBEmpresa
            IgnoraDBLookupComboBox = False
          end
          object CBEmpresa: TdmkDBLookupComboBox
            Left = 64
            Top = 20
            Width = 425
            Height = 21
            KeyField = 'Filial'
            ListField = 'NOMEFILIAL'
            ListSource = DModG.DsEmpresas
            TabOrder = 1
            dmkEditCB = EdEmpresa
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdAnoMes: TdmkEdit
            Left = 492
            Top = 20
            Width = 56
            Height = 21
            Alignment = taCenter
            TabOrder = 2
            FormatType = dmktfMesAno
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfLong
            HoraFormat = dmkhfShort
            QryCampo = 'Mez'
            UpdType = utYes
            Obrigatorio = True
            PermiteNulo = False
            ValueVariant = Null
          end
        end
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 248
    Width = 564
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 560
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 292
    Width = 564
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 418
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 416
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 16
    Top = 8
  end
end
