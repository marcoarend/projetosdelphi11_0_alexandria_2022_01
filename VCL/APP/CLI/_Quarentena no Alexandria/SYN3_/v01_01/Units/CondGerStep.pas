unit CondGerStep;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, Grids, DBGrids,
  dmkDBGrid, dmkGeral, UnMyObjects, dmkImage, UnDmkEnums;

type
  TFmCondGerStep = class(TForm)
    Panel1: TPanel;
    QrLei_Step: TmySQLQuery;
    QrLei_StepStep: TSmallintField;
    QrLei_StepCodigo: TIntegerField;
    QrLei_StepIDNum: TIntegerField;
    QrLei_StepValTitul: TFloatField;
    QrLei_StepValPago: TFloatField;
    QrLei_StepQuitaData: TDateField;
    QrLei_StepArquivo: TWideStringField;
    QrLei_StepCarteira: TIntegerField;
    DsLei_Step: TDataSource;
    DBGrid1: TdmkDBGrid;
    QrLei_StepValJuros: TFloatField;
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    LaTitulo1C: TLabel;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    Panel3: TPanel;
    PnSaiDesis: TPanel;
    BtOK: TBitBtn;
    BitBtn1: TBitBtn;
    BtSaida: TBitBtn;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure BtOKClick(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCondGerStep: TFmCondGerStep;

implementation

uses Module;

{$R *.DFM}

procedure TFmCondGerStep.BitBtn1Click(Sender: TObject);
var
  Valor: String;
  Codigo: Integer;
begin
  Valor := Geral.FFT(QrLei_StepValJuros.Value, 2, siPositivo);
  if InputQuery('Valor do Juroso', 'Informe o novo valor do t�tulo', Valor) then
  begin
    Codigo := QrLei_StepCodigo.Value;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE cnab_Lei SET ValJuros=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
    Dmod.QrUpd.Params[00].AsFloat := Geral.DMV(Valor);
    Dmod.QrUpd.Params[01].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    QrLei_Step.Close;
    QrLei_Step.Open;
    QrLei_Step.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmCondGerStep.BtOKClick(Sender: TObject);
var
  Valor: String;
  Codigo: Integer;
begin
  Valor := Geral.FFT(QrLei_StepValTitul.Value, 2, siPositivo);
  if InputQuery('Valor do Titulo', 'Informe o novo valor do t�tulo', Valor) then
  begin
    Codigo := QrLei_StepCodigo.Value;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE cnab_Lei SET ValTitul=:P0');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
    Dmod.QrUpd.Params[00].AsFloat := Geral.DMV(Valor);
    Dmod.QrUpd.Params[01].AsInteger := Codigo;
    Dmod.QrUpd.ExecSQL;
    //
    QrLei_Step.Close;
    QrLei_Step.Open;
    QrLei_Step.Locate('Codigo', Codigo, []);
  end;
end;

procedure TFmCondGerStep.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCondGerStep.DBGrid1CellClick(Column: TColumn);
var
  Codigo, Status: Integer;
begin
  Screen.Cursor := crHourGlass;
  //
  Codigo := QrLei_StepCodigo.Value;
  if QrLei_StepStep.Value = 0 then
    Status := 1
  else
    Status := 0;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE cnab_lei SET ');
  Dmod.QrUpd.SQL.Add('Step=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Codigo=:P1');
  Dmod.QrUpd.Params[00].AsInteger := Status;
  Dmod.QrUpd.Params[01].AsInteger := Codigo;
  Dmod.QrUpd.ExecSQL;
  //
  QrLei_Step.Close;
  QrLei_Step.Open;
  QrLei_Step.Locate('Codigo', Codigo, []);
  Screen.Cursor := crDefault;
end;

procedure TFmCondGerStep.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCondGerStep.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  //

end;

procedure TFmCondGerStep.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

end.
