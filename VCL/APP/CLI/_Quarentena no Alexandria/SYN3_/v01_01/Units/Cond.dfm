object FmCond: TFmCond
  Left = 368
  Top = 194
  Caption = 'CAD-CONDO-001 :: Cadastro de Condom'#237'nios'
  ClientHeight = 705
  ClientWidth = 993
  Color = clBtnFace
  Constraints.MinHeight = 256
  Constraints.MinWidth = 630
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelEdita: TPanel
    Left = 0
    Top = 51
    Width = 993
    Height = 654
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 1
    Visible = False
    object PainelEdit: TPanel
      Left = -6
      Top = 2
      Width = 999
      Height = 558
      BevelOuter = bvNone
      TabOrder = 0
      object PageControl4: TPageControl
        Left = 0
        Top = 40
        Width = 999
        Height = 518
        ActivePage = TabSheet13
        Align = alClient
        TabHeight = 20
        TabOrder = 1
        object TabSheet14: TTabSheet
          Caption = 'Geral'
          object Panel48: TPanel
            Left = 0
            Top = 0
            Width = 991
            Height = 488
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            ExplicitWidth = 992
            ExplicitHeight = 490
            object LaCond: TLabel
              Left = 8
              Top = 2
              Width = 133
              Height = 13
              Caption = 'Entidade deste condom'#237'nio:'
            end
            object SBCond: TSpeedButton
              Left = 642
              Top = 18
              Width = 20
              Height = 20
              Caption = '...'
              OnClick = SBCondClick
            end
            object Label1: TLabel
              Left = 8
              Top = 42
              Width = 63
              Height = 13
              Caption = 'Tot. andares:'
              FocusControl = EdTAndares
            end
            object Label2: TLabel
              Left = 80
              Top = 42
              Width = 68
              Height = 13
              Caption = 'Tot. unidades:'
              FocusControl = EdTAptos
            end
            object Label224: TLabel
              Left = 227
              Top = 42
              Width = 51
              Height = 13
              Caption = 'Assistente:'
            end
            object SBAssistente: TSpeedButton
              Left = 642
              Top = 57
              Width = 20
              Height = 21
              Caption = '...'
              OnClick = SBAssistenteClick
            end
            object Label199: TLabel
              Left = 150
              Top = 42
              Width = 26
              Height = 13
              Caption = 'Sigla:'
              FocusControl = EdSigla
            end
            object EdEntidade: TdmkEditCB
              Left = 8
              Top = 18
              Width = 56
              Height = 20
              Alignment = taRightJustify
              TabOrder = 0
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBEntidade
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBEntidade: TdmkDBLookupComboBox
              Left = 64
              Top = 18
              Width = 576
              Height = 21
              KeyField = 'Codigo'
              ListField = 'NOME'
              ListSource = DsClientes
              TabOrder = 1
              dmkEditCB = EdEntidade
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object GroupBox37: TGroupBox
              Left = 8
              Top = 84
              Width = 182
              Height = 68
              Caption = ' % INSS s'#237'ndico: '
              TabOrder = 7
              object Label191: TLabel
                Left = 12
                Top = 20
                Width = 51
                Height = 13
                Caption = '% S'#237'ndico:'
              end
              object Label192: TLabel
                Left = 94
                Top = 20
                Width = 71
                Height = 13
                Caption = '% Condom'#237'nio:'
              end
              object dmkEdProLaINSSr: TdmkEdit
                Left = 12
                Top = 35
                Width = 78
                Height = 21
                Alignment = taRightJustify
                TabOrder = 0
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
              object dmkEdProLaINSSp: TdmkEdit
                Left = 94
                Top = 35
                Width = 80
                Height = 21
                Alignment = taRightJustify
                TabOrder = 1
                FormatType = dmktfDouble
                MskType = fmtNone
                DecimalSize = 2
                LeftZeros = 0
                NoEnterToTab = False
                NoForceUppercase = False
                ForceNextYear = False
                DataFormat = dmkdfShort
                HoraFormat = dmkhfShort
                Texto = '0,00'
                UpdType = utYes
                Obrigatorio = False
                PermiteNulo = False
                ValueVariant = 0.000000000000000000
                ValWarn = False
              end
            end
            object EdTAndares: TdmkEdit
              Left = 8
              Top = 57
              Width = 69
              Height = 21
              Alignment = taRightJustify
              MaxLength = 30
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdTAptos: TdmkEdit
              Left = 80
              Top = 57
              Width = 69
              Height = 21
              Alignment = taRightJustify
              MaxLength = 30
              TabOrder = 3
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
            end
            object EdSigla: TdmkEdit
              Left = 150
              Top = 57
              Width = 72
              Height = 21
              MaxLength = 50
              TabOrder = 4
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
            end
            object EdAssistente: TdmkEditCB
              Left = 227
              Top = 57
              Width = 56
              Height = 21
              Alignment = taRightJustify
              TabOrder = 5
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBAssistente
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBAssistente: TdmkDBLookupComboBox
              Left = 285
              Top = 57
              Width = 355
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsAssist
              TabOrder = 6
              dmkEditCB = EdAssistente
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object RGAtivo: TdmkRadioGroup
              Left = 196
              Top = 84
              Width = 444
              Height = 68
              Caption = 'Status do condom'#237'nio'
              Columns = 3
              ItemIndex = 2
              Items.Strings = (
                'Inoperante'
                'Inativo'
                'Ativo')
              TabOrder = 8
              UpdType = utYes
              OldValor = 0
            end
          end
        end
        object TabSheet13: TTabSheet
          Caption = 'Configura'#231#245'es'
          ImageIndex = 5
          object Panel10: TPanel
            Left = 0
            Top = 0
            Width = 991
            Height = 488
            Align = alClient
            BevelOuter = bvNone
            ParentBackground = False
            TabOrder = 0
            ExplicitWidth = 992
            ExplicitHeight = 490
            object LaConfigBol: TLabel
              Left = 8
              Top = 252
              Width = 224
              Height = 13
              Caption = 'Configura'#231#227'o padr'#227'o de impress'#227'o dos boletos:'
            end
            object Label11: TLabel
              Left = 10
              Top = 294
              Width = 273
              Height = 13
              Caption = 'Configura'#231#227'o de corre'#231#227'o monet'#225'ria de boletos vencidos:'
            end
            object SBConfigBol: TSpeedButton
              Left = 742
              Top = 268
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SBConfigBolClick
            end
            object SpeedButton6: TSpeedButton
              Left = 742
              Top = 309
              Width = 21
              Height = 21
              Caption = '...'
              OnClick = SpeedButton6Click
            end
            object Label150: TLabel
              Left = 10
              Top = 366
              Width = 292
              Height = 13
              Caption = 'Logo do cliente (160 largura  X 90 altura no formato bmp): [F4]'
            end
            object RGModelBloq: TRadioGroup
              Left = 8
              Top = 4
              Width = 755
              Height = 197
              Caption = ' Modelo de impress'#227'o do boleto: '
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'FmPrincipal.PreencheModelosBloq')
              TabOrder = 0
              OnClick = RGModelBloqClick
            end
            object EdConfigBol: TdmkEditCB
              Left = 8
              Top = 268
              Width = 55
              Height = 21
              Alignment = taRightJustify
              TabOrder = 2
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBConfigBol
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBConfigBol: TdmkDBLookupComboBox
              Left = 63
              Top = 268
              Width = 675
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsConfigBol
              TabOrder = 3
              dmkEditCB = EdConfigBol
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object RGCompe: TRadioGroup
              Left = 8
              Top = 206
              Width = 755
              Height = 37
              Caption = ' Ficha de compensa'#231#227'o (modelo E): '
              Columns = 3
              ItemIndex = 0
              Items.Strings = (
                '??'
                '1 (uma via)'
                '2 (duas vias)')
              TabOrder = 1
            end
            object CkHideCompe: TCheckBox
              Left = 419
              Top = 340
              Width = 344
              Height = 17
              Caption = 'N'#227'o mostrar a ficha de compensa'#231#227'o no boleto modelo H.'
              Checked = True
              State = cbChecked
              TabOrder = 7
            end
            object CkBalAgrMens: TCheckBox
              Left = 10
              Top = 340
              Width = 399
              Height = 17
              Caption = 
                'Separar as somas de valores no balancete por compet'#234'ncia ao agru' +
                'par valores.'
              Checked = True
              State = cbChecked
              TabOrder = 6
            end
            object EdCfgInfl: TdmkEditCB
              Left = 8
              Top = 309
              Width = 55
              Height = 21
              Alignment = taRightJustify
              TabOrder = 4
              FormatType = dmktfInteger
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ValMin = '-2147483647'
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              Texto = '0'
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = 0
              ValWarn = False
              DBLookupComboBox = CBCfgInfl
              IgnoraDBLookupComboBox = False
              AutoSetIfOnlyOneReg = setregOnlyManual
            end
            object CBCfgInfl: TdmkDBLookupComboBox
              Left = 63
              Top = 309
              Width = 675
              Height = 21
              KeyField = 'Codigo'
              ListField = 'Nome'
              ListSource = DsCfgInfl
              ParentShowHint = False
              ShowHint = False
              TabOrder = 5
              dmkEditCB = EdCfgInfl
              UpdType = utYes
              LocF7SQLMasc = '$#'
              LocF7PreDefProc = f7pNone
            end
            object Panel11: TPanel
              Left = 781
              Top = 0
              Width = 210
              Height = 488
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 8
              object RGMBB: TRadioGroup
                Left = 0
                Top = 0
                Width = 210
                Height = 34
                Align = alTop
                Caption = ' Mostra balancete no boleto: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Sim')
                TabOrder = 0
              end
              object RGMRB: TRadioGroup
                Left = 0
                Top = 34
                Width = 210
                Height = 33
                Align = alTop
                Caption = ' Mostra resumo do balanc. no boleto: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Sim')
                TabOrder = 1
              end
              object RGPBB: TRadioGroup
                Left = 0
                Top = 67
                Width = 210
                Height = 34
                Align = alTop
                Caption = ' Per'#237'odo do balancete no boleto: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'Anterior'
                  'Atual'
                  'Pr'#243'ximo')
                TabOrder = 2
              end
              object RGMSP: TRadioGroup
                Left = 0
                Top = 101
                Width = 210
                Height = 33
                Align = alTop
                Caption = 'Mostra saldos do plano de ctas no bloq.:  '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Sim'
                  'S'#243' cjtos')
                TabOrder = 3
              end
              object RGMSB: TRadioGroup
                Left = 0
                Top = 134
                Width = 210
                Height = 33
                Align = alTop
                Caption = ' Mostra saldos de ctas corr. no boleto: '
                Columns = 3
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Sim'
                  'Fim')
                TabOrder = 4
              end
              object RGPSB: TRadioGroup
                Left = 0
                Top = 167
                Width = 210
                Height = 34
                Align = alTop
                Caption = ' Per'#237'odo dos saldos no boleto: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'Anterior'
                  'Atual')
                TabOrder = 5
              end
              object RGMIB: TRadioGroup
                Left = 0
                Top = 201
                Width = 210
                Height = 50
                Align = alTop
                Caption = ' Mostra inadimpl'#234'ncia no boleto: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Ambos'
                  'Cond'#244'mino'
                  'Condom'#237'nio')
                TabOrder = 6
              end
              object RGMPB: TRadioGroup
                Left = 0
                Top = 251
                Width = 210
                Height = 34
                Align = alTop
                Caption = ' Mostra provis'#245'es para despesas bloq.: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Sim')
                TabOrder = 7
              end
              object RGMAB: TRadioGroup
                Left = 0
                Top = 285
                Width = 210
                Height = 33
                Align = alTop
                Caption = ' Mostra a composi'#231#227'o de arrecad. bloq.: '
                Columns = 2
                ItemIndex = 0
                Items.Strings = (
                  'N'#227'o'
                  'Sim')
                TabOrder = 8
              end
              object RGDiaVencto: TRadioGroup
                Left = 0
                Top = 318
                Width = 210
                Height = 163
                Align = alTop
                Caption = ' Dia do m'#234's do vencto. da quota condom.: '
                Columns = 4
                ItemIndex = 4
                Items.Strings = (
                  '01'
                  '02'
                  '03'
                  '04'
                  '05'
                  '06'
                  '07'
                  '08'
                  '09'
                  '10'
                  '11'
                  '12'
                  '13'
                  '14'
                  '15'
                  '16'
                  '17'
                  '18'
                  '19'
                  '20'
                  '21'
                  '22'
                  '23'
                  '24'
                  '25'
                  '26'
                  '27'
                  '28')
                TabOrder = 9
              end
            end
            object EdCondCliLogo: TdmkEdit
              Left = 10
              Top = 382
              Width = 454
              Height = 20
              TabOrder = 9
              FormatType = dmktfString
              MskType = fmtNone
              DecimalSize = 0
              LeftZeros = 0
              NoEnterToTab = False
              NoForceUppercase = False
              ForceNextYear = False
              DataFormat = dmkdfShort
              HoraFormat = dmkhfShort
              UpdType = utYes
              Obrigatorio = False
              PermiteNulo = False
              ValueVariant = ''
              ValWarn = False
              OnKeyDown = EdCondCliLogoKeyDown
            end
          end
        end
        object TabSheet15: TTabSheet
          Caption = 'Observa'#231#245'es'
          ImageIndex = 1
          object MeObserv: TMemo
            Left = 0
            Top = 0
            Width = 991
            Height = 488
            Align = alClient
            Lines.Strings = (
              'Memo2')
            TabOrder = 0
            ExplicitWidth = 992
            ExplicitHeight = 490
          end
        end
      end
      object Panel16: TPanel
        Left = 0
        Top = 0
        Width = 999
        Height = 40
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitWidth = 998
        object Label9: TLabel
          Left = 8
          Top = 0
          Width = 60
          Height = 13
          Caption = 'Condom'#237'nio:'
        end
        object EdCodigo: TdmkEdit
          Left = 8
          Top = 16
          Width = 48
          Height = 21
          Alignment = taRightJustify
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ReadOnly = True
          TabOrder = 0
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = ''
          ValWarn = False
        end
        object DBEdit10: TDBEdit
          Left = 56
          Top = 16
          Width = 595
          Height = 21
          DataField = 'NCONDOM'
          DataSource = DsCond
          TabOrder = 1
        end
      end
    end
    object GBConfirma: TGroupBox
      Left = 0
      Top = 592
      Width = 993
      Height = 62
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 12
        Top = 17
        Width = 118
        Height = 39
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel9: TPanel
        Left = 855
        Top = 14
        Width = 136
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 118
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 51
    Width = 993
    Height = 654
    Align = alClient
    BevelOuter = bvNone
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentBackground = False
    ParentFont = False
    TabOrder = 0
    object PageControl1: TPageControl
      Left = 0
      Top = 85
      Width = 993
      Height = 461
      ActivePage = TabSheet11
      Align = alTop
      TabHeight = 20
      TabOrder = 0
      OnChange = PageControl1Change
      ExplicitWidth = 1241
      object TabSheet1: TTabSheet
        Caption = ' Estrutura f'#237'sica '
        object PageControl7: TPageControl
          Left = 0
          Top = 0
          Width = 985
          Height = 431
          ActivePage = TabSheet29
          Align = alClient
          TabHeight = 20
          TabOrder = 0
          OnChange = PageControl7Change
          ExplicitWidth = 1233
          object TabSheet29: TTabSheet
            Caption = 'Unidades'
            object Panel41: TPanel
              Left = 0
              Top = 0
              Width = 977
              Height = 401
              Align = alClient
              BevelOuter = bvNone
              TabOrder = 0
              ExplicitWidth = 1225
              object Splitter1: TSplitter
                Left = 338
                Top = 0
                Width = 4
                Height = 401
                ExplicitHeight = 405
              end
              object DBGUnidades: TDBGrid
                Left = 342
                Top = 0
                Width = 635
                Height = 401
                Align = alClient
                DataSource = DsCondImov
                Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
                TabOrder = 0
                TitleFont.Charset = DEFAULT_CHARSET
                TitleFont.Color = clWindowText
                TitleFont.Height = -12
                TitleFont.Name = 'MS Sans Serif'
                TitleFont.Style = []
                OnDblClick = DBGUnidadesDblClick
                Columns = <
                  item
                    Expanded = False
                    FieldName = 'Conta'
                    Title.Caption = 'Controle'
                    Width = 46
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Andar'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    Width = 28
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Unidade'
                    Font.Charset = DEFAULT_CHARSET
                    Font.Color = clWindowText
                    Font.Height = -11
                    Font.Name = 'MS Sans Serif'
                    Font.Style = [fsBold]
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Juridico_TXT'
                    Title.Caption = 'SJ'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEPROP'
                    Title.Caption = 'Propriet'#225'rio'
                    Width = 196
                    Visible = True
                  end
                  item
                    Alignment = taCenter
                    Expanded = False
                    FieldName = 'InadCEMCad'
                    Title.Caption = 'msgs'
                    Width = 24
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'Moradores'
                    Title.Caption = 'Pessoas'
                    Width = 35
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'FracaoIdeal'
                    Title.Caption = 'Fra'#231#227'o ideal'
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'BloqEndTip_TXT'
                    Title.Caption = 'End. entr.blq.'
                    Width = 65
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMESITIMV'
                    Title.Caption = 'Rateia'
                    Width = 29
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEUSUARIO'
                    Title.Caption = 'Inquilino'
                    Width = 158
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'STATUS1'
                    Title.Caption = 'Status'
                    Width = 66
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEPROTOCOLO'
                    Title.Caption = 'Protocolo de entrega de boleto 1'
                    Width = 160
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEPROTOCOLO2'
                    Title.Caption = 'Protocolo de entrega de boleto 2'
                    Width = 160
                    Visible = True
                  end
                  item
                    Expanded = False
                    FieldName = 'NOMEPROTOCOLO3'
                    Title.Caption = 'Protocolo de entrega de boleto 3'
                    Width = 160
                    Visible = True
                  end>
              end
              object Panel42: TPanel
                Left = 0
                Top = 0
                Width = 338
                Height = 401
                Align = alLeft
                BevelOuter = bvNone
                TabOrder = 1
                object DBGrid1: TDBGrid
                  Left = 0
                  Top = 0
                  Width = 338
                  Height = 116
                  Align = alClient
                  DataSource = DsCondBloco
                  TabOrder = 0
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'Controle'
                      Width = 34
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Descri'
                      Title.Caption = 'Bloco'
                      Width = 97
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'PrefixoUH'
                      Title.Caption = 'Prefixo UH'
                      Width = 47
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'SomaFracao'
                      Title.Caption = 'Fra'#231#227'o ideal'
                      Visible = True
                    end>
                end
                object Panel43: TPanel
                  Left = 0
                  Top = 116
                  Width = 338
                  Height = 48
                  Align = alBottom
                  BevelOuter = bvNone
                  ParentBackground = False
                  TabOrder = 1
                  object Label205: TLabel
                    Left = 174
                    Top = 4
                    Width = 72
                    Height = 13
                    Caption = 'Parte do nome:'
                  end
                  object RGTipoEnt: TRadioGroup
                    Left = 1
                    Top = 1
                    Width = 169
                    Height = 39
                    Caption = ' Tipo de entidade: '
                    Columns = 2
                    ItemIndex = 0
                    Items.Strings = (
                      'Propriet'#225'rio'
                      'Morador')
                    TabOrder = 0
                    OnClick = RGTipoEntClick
                  end
                  object EdParcial: TEdit
                    Left = 174
                    Top = 20
                    Width = 157
                    Height = 21
                    TabOrder = 1
                    OnChange = EdParcialChange
                  end
                end
                object GradePesq: TdmkDBGrid
                  Left = 0
                  Top = 164
                  Width = 338
                  Height = 237
                  Align = alBottom
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NO_BLOCO'
                      Title.Caption = 'Bloco'
                      Width = 80
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Unidade'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_ENT'
                      Title.Caption = 'Nome entidade'
                      Width = 563
                      Visible = True
                    end>
                  Color = clWindow
                  DataSource = DsPesq
                  Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
                  TabOrder = 2
                  TitleFont.Charset = DEFAULT_CHARSET
                  TitleFont.Color = clWindowText
                  TitleFont.Height = -12
                  TitleFont.Name = 'MS Sans Serif'
                  TitleFont.Style = []
                  OnDblClick = GradePesqDblClick
                  Columns = <
                    item
                      Expanded = False
                      FieldName = 'NO_BLOCO'
                      Title.Caption = 'Bloco'
                      Width = 80
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'Unidade'
                      Width = 56
                      Visible = True
                    end
                    item
                      Expanded = False
                      FieldName = 'NO_ENT'
                      Title.Caption = 'Nome entidade'
                      Width = 563
                      Visible = True
                    end>
                end
              end
            end
          end
          object TabSheet31: TTabSheet
            Caption = 'Unidade selecionada'
            ImageIndex = 1
            object DBGrid9: TDBGrid
              Left = 0
              Top = 0
              Width = 977
              Height = 40
              Align = alTop
              DataSource = DsCondImov
              Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Controle'
                  Title.Caption = 'Bloco'
                  Width = 31
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Andar'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  Width = 28
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Title.Caption = 'Controle'
                  Width = 46
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Unidade'
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -11
                  Font.Name = 'MS Sans Serif'
                  Font.Style = [fsBold]
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEPROP'
                  Title.Caption = 'Propriet'#225'rio'
                  Width = 196
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'STATUS1'
                  Title.Caption = 'Status'
                  Width = 66
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEPROTOCOLO'
                  Title.Caption = 'Protocolo de entrega de boleto'
                  Width = 158
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMESITIMV'
                  Title.Caption = 'Rateia'
                  Width = 29
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMEUSUARIO'
                  Title.Caption = 'Inquilino'
                  Width = 138
                  Visible = True
                end>
            end
            object PageControl2: TPageControl
              Left = 0
              Top = 40
              Width = 977
              Height = 361
              ActivePage = TabSheet4
              Align = alClient
              TabHeight = 20
              TabOrder = 1
              ExplicitWidth = 1225
              object TabSheet4: TTabSheet
                Caption = 'Geral'
                object Label8: TLabel
                  Left = 8
                  Top = -2
                  Width = 42
                  Height = 13
                  Caption = 'C'#244'njuge:'
                end
                object Label129: TLabel
                  Left = 8
                  Top = 46
                  Width = 49
                  Height = 13
                  Caption = 'Imobili'#225'ria:'
                end
                object Label130: TLabel
                  Left = 253
                  Top = 46
                  Width = 45
                  Height = 13
                  Caption = 'Telefone:'
                end
                object Label131: TLabel
                  Left = 8
                  Top = 85
                  Width = 40
                  Height = 13
                  Caption = 'Contato:'
                end
                object Bevel2: TBevel
                  Left = 4
                  Top = 43
                  Width = 371
                  Height = 80
                end
                object DBEdit014: TDBEdit
                  Left = 8
                  Top = 13
                  Width = 363
                  Height = 21
                  Color = clWhite
                  DataField = 'NOMECONJUGE'
                  DataSource = DsCondImov
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 0
                end
                object DBEdit063: TDBEdit
                  Left = 8
                  Top = 60
                  Width = 241
                  Height = 21
                  Color = clWhite
                  DataField = 'IMOB'
                  DataSource = DsCondImov
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 1
                end
                object DBEdit064: TDBEdit
                  Left = 253
                  Top = 60
                  Width = 118
                  Height = 21
                  Color = clWhite
                  DataField = 'ContTel'
                  DataSource = DsCondImov
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 2
                end
                object DBEdit065: TDBEdit
                  Left = 8
                  Top = 99
                  Width = 363
                  Height = 21
                  Color = clWhite
                  DataField = 'Contato'
                  DataSource = DsCondImov
                  Font.Charset = DEFAULT_CHARSET
                  Font.Color = clWindowText
                  Font.Height = -12
                  Font.Name = 'MS Sans Serif'
                  Font.Style = []
                  ParentFont = False
                  ParentShowHint = False
                  ShowHint = True
                  TabOrder = 3
                end
              end
              object TabSheet19: TTabSheet
                Caption = 'Observa'#231#245'es'
                ImageIndex = 7
                object DBMemo2: TDBMemo
                  Left = 0
                  Top = 0
                  Width = 974
                  Height = 337
                  Align = alClient
                  DataField = 'Observ'
                  DataSource = DsCondImov
                  TabOrder = 0
                end
              end
            end
          end
          object TabSheet34: TTabSheet
            Caption = 'Edit'#225'vel'
            ImageIndex = 2
            object DBGrid2: TDBGrid
              Left = 0
              Top = 0
              Width = 980
              Height = 405
              Align = alClient
              DataSource = DsUnidades
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'NOME_BLOCO'
                  Title.Caption = 'Bloco'
                  Width = 118
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Andar'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Unidade'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_PROPRIET'
                  Title.Caption = 'Propriet'#225'rio'
                  Width = 252
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'FracaoIdeal'
                  Title.Caption = 'Fra'#231#227'o ideal'
                  Width = 92
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Moradores'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'QtdGaragem'
                  Title.Caption = 'Qtd Garagens'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'IDExporta'
                  Title.Caption = 'Nome Exporta'
                  Width = 115
                  Visible = True
                end>
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = ' Geral '
        ImageIndex = 1
        object Panel4: TPanel
          Left = 0
          Top = 0
          Width = 986
          Height = 433
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Label5: TLabel
            Left = 0
            Top = 0
            Width = 66
            Height = 13
            Align = alTop
            Caption = 'Observa'#231#245'es:'
          end
          object DBMemo1: TDBMemo
            Left = 0
            Top = 13
            Width = 986
            Height = 420
            Align = alClient
            DataField = 'Observ'
            DataSource = DsCond
            TabOrder = 0
          end
        end
      end
      object TabSheet3: TTabSheet
        Caption = 'Conselho fiscal'
        ImageIndex = 2
        object PageControl5: TPageControl
          Left = 0
          Top = 0
          Width = 985
          Height = 431
          ActivePage = TabSheet26
          Align = alClient
          TabHeight = 20
          TabOrder = 0
          ExplicitWidth = 1233
          object TabSheet26: TTabSheet
            Caption = 'Integrantes do conselho'
            object DBGEntiRespon: TDBGrid
              Left = 0
              Top = 0
              Width = 980
              Height = 405
              Align = alClient
              DataSource = DsCondOutros
              TabOrder = 0
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -12
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'OrdemLista'
                  Title.Caption = 'Ordem lista'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Nome'
                  Width = 171
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOME_CARGO'
                  Title.Caption = 'Cargo'
                  Width = 90
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NO_ASSINA'
                  Title.Caption = 'Assina'
                  Width = 29
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MandatoIni_TXT'
                  Title.Caption = 'Ini. mandato'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'MandatoFim_TXT'
                  Title.Caption = 'Fim mandato'
                  Width = 45
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Entidade'
                  Width = 59
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Te1_TXT'
                  Title.Caption = 'Telefone'
                  Width = 88
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Cel_TXT'
                  Title.Caption = 'Celular'
                  Width = 88
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Email'
                  Title.Caption = 'E-mail'
                  Width = 160
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Observ'
                  Title.Caption = 'Observa'#231#245'es'
                  Width = 280
                  Visible = True
                end>
            end
          end
        end
      end
      object TabSheet8: TTabSheet
        Caption = ' Modelos e configura'#231#245'es de boletos espec'#237'ficos de UHs '
        ImageIndex = 4
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 46
          Width = 986
          Height = 387
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Unidade'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_MODELBLOQ'
              Title.Caption = 'M'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_CONFIGBOL'
              Title.Caption = 'Configura'#231#227'o'
              Width = 283
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Compe'
              Title.Caption = 'Fichas de compensa'#231#227'o'
              Width = 122
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BalAgrMens'
              Title.Caption = 'Agrupamentos'
              Width = 74
              Visible = True
            end>
          Color = clWindow
          DataSource = DsCondModBol
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 0
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Unidade'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_MODELBLOQ'
              Title.Caption = 'M'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOME_CONFIGBOL'
              Title.Caption = 'Configura'#231#227'o'
              Width = 283
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Compe'
              Title.Caption = 'Fichas de compensa'#231#227'o'
              Width = 122
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'BalAgrMens'
              Title.Caption = 'Agrupamentos'
              Width = 74
              Visible = True
            end>
        end
        object Panel8: TPanel
          Left = 0
          Top = 0
          Width = 986
          Height = 46
          Align = alTop
          ParentBackground = False
          TabOrder = 1
          object BtBloquetos: TBitBtn
            Tag = 492
            Left = 4
            Top = 2
            Width = 118
            Height = 40
            Cursor = crHandPoint
            Caption = 'Blo&queto'
            Enabled = False
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtBloquetosClick
          end
        end
      end
      object TabSheet9: TTabSheet
        Caption = 'Confer'#234'ncia de Provis'#245'es (sobre Contas Mensais)'
        ImageIndex = 5
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 986
          Height = 47
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object BtCtaMesExc: TBitBtn
            Tag = 12
            Left = 248
            Top = 4
            Width = 118
            Height = 39
            Cursor = crHandPoint
            Hint = 'Exclui registro atual'
            Caption = 'E&xclui'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtCtaMesExcClick
          end
          object BtCtaMesAlt: TBitBtn
            Tag = 11
            Left = 126
            Top = 4
            Width = 118
            Height = 39
            Cursor = crHandPoint
            Hint = 'Exclui registro atual'
            Caption = '&Altera'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 1
            OnClick = BtCtaMesAltClick
          end
          object BtCtaMesIns: TBitBtn
            Tag = 10
            Left = 4
            Top = 4
            Width = 118
            Height = 39
            Cursor = crHandPoint
            Hint = 'Inclui novo registro'
            Caption = 'I&nsere'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 2
            OnClick = BtCtaMesInsClick
          end
        end
        object GradeContasMes: TdmkDBGrid
          Left = 0
          Top = 47
          Width = 986
          Height = 386
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Width = 26
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descric'#227'o'
              Width = 480
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PERIODOINI_TXT'
              Title.Caption = 'In'#237'cio'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PERIODOFIM_TXT'
              Title.Caption = 'Final'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorMin'
              Title.Caption = 'Valor m'#237'n.'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorMax'
              Title.Caption = 'Valor m'#225'x.'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeMin'
              Title.Caption = 'Qtde m'#237'n.'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeMax'
              Title.Caption = 'Qtde m'#225'x.'
              Width = 59
              Visible = True
            end>
          Color = clWindow
          DataSource = DsContasMes
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Controle'
              Title.Caption = 'ID'
              Width = 26
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Descricao'
              Title.Caption = 'Descric'#227'o'
              Width = 480
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PERIODOINI_TXT'
              Title.Caption = 'In'#237'cio'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'PERIODOFIM_TXT'
              Title.Caption = 'Final'
              Width = 38
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorMin'
              Title.Caption = 'Valor m'#237'n.'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'ValorMax'
              Title.Caption = 'Valor m'#225'x.'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeMin'
              Title.Caption = 'Qtde m'#237'n.'
              Width = 59
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'QtdeMax'
              Title.Caption = 'Qtde m'#225'x.'
              Width = 59
              Visible = True
            end>
        end
      end
      object TabSheet10: TTabSheet
        Caption = ' Fluxos '
        ImageIndex = 6
        object Panel44: TPanel
          Left = 0
          Top = 0
          Width = 986
          Height = 433
          Align = alClient
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object GroupBox31: TGroupBox
            Left = 0
            Top = 0
            Width = 244
            Height = 403
            Align = alLeft
            Caption = ' Boleto: '
            TabOrder = 0
            object Panel45: TPanel
              Left = 2
              Top = 14
              Width = 240
              Height = 37
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label71: TLabel
                Left = 12
                Top = 12
                Width = 149
                Height = 13
                Caption = 'Ordem na lista de condom'#237'nios:'
                FocusControl = EdFlxB_Ordem
              end
              object EdFlxB_Ordem: TDBEdit
                Left = 166
                Top = 8
                Width = 66
                Height = 21
                DataField = 'FlxB_Ordem'
                DataSource = DsCond
                TabOrder = 0
              end
            end
            object GroupBox39: TGroupBox
              Left = 2
              Top = 51
              Width = 240
              Height = 351
              Align = alClient
              Caption = ' Dia (m'#234's) limite* : '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              object Label211: TLabel
                Left = 12
                Top = 138
                Width = 111
                Height = 13
                Caption = 'Gera'#231#227'o de protocolos:'
                FocusControl = EdFlxB_Proto
              end
              object Label212: TLabel
                Left = 12
                Top = 162
                Width = 50
                Height = 13
                Caption = 'Relat'#243'rios:'
                FocusControl = EdFlxB_Relat
              end
              object Label213: TLabel
                Left = 12
                Top = 185
                Width = 130
                Height = 13
                Caption = 'Envio de boletos por e-mail:'
                FocusControl = EdFlxB_EMail
              end
              object Label214: TLabel
                Left = 12
                Top = 209
                Width = 129
                Height = 13
                Caption = 'Envio de boletos '#224' portaria:'
                FocusControl = EdFlxB_Porta
              end
              object Label215: TLabel
                Left = 12
                Top = 232
                Width = 148
                Height = 13
                Caption = 'Envio de boletos via postagem:'
                FocusControl = EdFlxB_Postl
              end
              object Label210: TLabel
                Left = 12
                Top = 114
                Width = 108
                Height = 13
                Caption = 'Impress'#227'o dos boletos:'
                FocusControl = EdFlxB_Print
              end
              object Label209: TLabel
                Left = 12
                Top = 90
                Width = 117
                Height = 13
                Caption = 'Administradora de riscos:'
                FocusControl = EdFlxB_Risco
              end
              object Label208: TLabel
                Left = 12
                Top = 67
                Width = 62
                Height = 13
                Caption = 'Fechamento:'
              end
              object Label207: TLabel
                Left = 12
                Top = 43
                Width = 116
                Height = 13
                Caption = 'Leituras / arrecada'#231#245'es:'
                FocusControl = EdFlxB_LeiAr
              end
              object Label206: TLabel
                Left = 12
                Top = 20
                Width = 100
                Height = 13
                Caption = 'Folha de pagamento:'
                FocusControl = EdFlxB_Folha
              end
              object EdFlxB_Folha: TDBEdit
                Left = 178
                Top = 16
                Width = 23
                Height = 21
                DataField = 'FlxB_Folha'
                DataSource = DsCond
                TabOrder = 0
              end
              object EdFlxB_Proto: TDBEdit
                Left = 178
                Top = 134
                Width = 23
                Height = 21
                DataField = 'FlxB_Proto'
                DataSource = DsCond
                TabOrder = 1
              end
              object EdFlxB_Relat: TDBEdit
                Left = 178
                Top = 158
                Width = 23
                Height = 21
                DataField = 'FlxB_Relat'
                DataSource = DsCond
                TabOrder = 2
              end
              object EdFlxB_EMail: TDBEdit
                Left = 178
                Top = 181
                Width = 23
                Height = 21
                DataField = 'FlxB_EMail'
                DataSource = DsCond
                TabOrder = 3
              end
              object EdFlxB_Porta: TDBEdit
                Left = 178
                Top = 205
                Width = 23
                Height = 21
                DataField = 'FlxB_Porta'
                DataSource = DsCond
                TabOrder = 4
              end
              object EdFlxB_Postl: TDBEdit
                Left = 178
                Top = 229
                Width = 23
                Height = 21
                DataField = 'FlxB_Postl'
                DataSource = DsCond
                TabOrder = 5
              end
              object EdFlxB_Print: TDBEdit
                Left = 178
                Top = 110
                Width = 23
                Height = 21
                DataField = 'FlxB_Print'
                DataSource = DsCond
                TabOrder = 6
              end
              object EdFlxB_Risco: TDBEdit
                Left = 178
                Top = 86
                Width = 23
                Height = 21
                DataField = 'FlxB_Risco'
                DataSource = DsCond
                TabOrder = 7
              end
              object EdFlxB_LeiAr: TDBEdit
                Left = 178
                Top = 39
                Width = 23
                Height = 21
                DataField = 'FlxB_LeiAr'
                DataSource = DsCond
                TabOrder = 8
              end
              object EdFlxB_Fecha: TDBEdit
                Left = 178
                Top = 63
                Width = 23
                Height = 21
                DataField = 'FlxB_Fecha'
                DataSource = DsCond
                TabOrder = 9
              end
            end
          end
          object GroupBox38: TGroupBox
            Left = 244
            Top = 0
            Width = 742
            Height = 403
            Align = alClient
            Caption = ' Fechamento de per'#237'odo: '
            TabOrder = 1
            object Panel46: TPanel
              Left = 2
              Top = 14
              Width = 739
              Height = 37
              Align = alTop
              BevelOuter = bvNone
              TabOrder = 0
              object Label223: TLabel
                Left = 12
                Top = 12
                Width = 149
                Height = 13
                Caption = 'Ordem na lista de condom'#237'nios:'
              end
              object EdFlxM_Ordem: TDBEdit
                Left = 170
                Top = 8
                Width = 66
                Height = 21
                DataField = 'FlxM_Ordem'
                DataSource = DsCond
                TabOrder = 0
              end
            end
            object GroupBox40: TGroupBox
              Left = 2
              Top = 51
              Width = 739
              Height = 351
              Align = alClient
              Caption = ' Dia (m'#234's) limite* : '
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              TabOrder = 1
              object Label217: TLabel
                Left = 12
                Top = 20
                Width = 63
                Height = 13
                Caption = 'Concilia'#231#245'es:'
                FocusControl = EdFlxM_Conci
              end
              object Label218: TLabel
                Left = 12
                Top = 43
                Width = 66
                Height = 13
                Caption = 'Documentos: '
                FocusControl = EdFlxM_Docum
              end
              object Label222: TLabel
                Left = 12
                Top = 138
                Width = 40
                Height = 13
                Caption = 'Entrega:'
                FocusControl = EdFlxM_Entrg
              end
              object Label221: TLabel
                Left = 12
                Top = 114
                Width = 76
                Height = 13
                Caption = 'Encadernac'#227'o: '
                FocusControl = EdFlxM_Encad
              end
              object Label220: TLabel
                Left = 12
                Top = 67
                Width = 94
                Height = 13
                Caption = 'ISS / contabilidade:'
                FocusControl = EdFlxM_Contb
              end
              object Label219: TLabel
                Left = 12
                Top = 90
                Width = 37
                Height = 13
                Caption = 'An'#225'lise:'
                FocusControl = EdFlxM_Anali
              end
              object EdFlxM_Conci: TDBEdit
                Left = 178
                Top = 16
                Width = 23
                Height = 21
                DataField = 'FlxM_Conci'
                DataSource = DsCond
                TabOrder = 0
              end
              object EdFlxM_Docum: TDBEdit
                Left = 178
                Top = 39
                Width = 23
                Height = 21
                DataField = 'FlxM_Docum'
                DataSource = DsCond
                TabOrder = 1
              end
              object EdFlxM_Entrg: TDBEdit
                Left = 178
                Top = 134
                Width = 23
                Height = 21
                DataField = 'FlxM_Entrg'
                DataSource = DsCond
                TabOrder = 2
              end
              object EdFlxM_Encad: TDBEdit
                Left = 178
                Top = 110
                Width = 23
                Height = 21
                DataField = 'FlxM_Encad'
                DataSource = DsCond
                TabOrder = 3
              end
              object EdFlxM_Contb: TDBEdit
                Left = 178
                Top = 63
                Width = 23
                Height = 21
                DataField = 'FlxM_Contb'
                DataSource = DsCond
                TabOrder = 4
              end
              object EdFlxM_Anali: TDBEdit
                Left = 178
                Top = 86
                Width = 23
                Height = 21
                DataField = 'FlxM_Anali'
                DataSource = DsCond
                TabOrder = 5
              end
            end
          end
          object Panel47: TPanel
            Left = 0
            Top = 403
            Width = 986
            Height = 30
            Align = alBottom
            BevelOuter = bvNone
            TabOrder = 2
            object Label216: TLabel
              Left = 12
              Top = 8
              Width = 253
              Height = 13
              Caption = '*: Deixe zero se n'#227'o deseja controlar o item!'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clRed
              Font.Height = -12
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
            end
          end
        end
      end
      object TabSheet11: TTabSheet
        Caption = 'Configura'#231#245'es de boletos'
        ImageIndex = 6
        object Panel6: TPanel
          Left = 0
          Top = 0
          Width = 986
          Height = 47
          Align = alTop
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object BitBtn3: TBitBtn
            Tag = 492
            Left = 4
            Top = 4
            Width = 148
            Height = 39
            Cursor = crHandPoint
            Hint = 'Inclui novo registro'
            Caption = '&Configura'#231#227'o boleto'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BitBtn3Click
          end
        end
        object dmkDBGrid2: TdmkDBGrid
          Left = 0
          Top = 47
          Width = 986
          Height = 386
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 283
              Visible = True
            end>
          Color = clWindow
          DataSource = DsCNAB_Cfg
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgTitleClick, dgTitleHotTrack]
          TabOrder = 1
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -12
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Title.Caption = 'ID'
              Width = 60
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Nome'
              Title.Caption = 'Descri'#231#227'o'
              Width = 283
              Visible = True
            end>
        end
      end
    end
    object Panel33: TPanel
      Left = 0
      Top = 0
      Width = 993
      Height = 85
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Label19: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label20: TLabel
        Left = 60
        Top = 4
        Width = 60
        Height = 13
        Caption = 'Condom'#237'nio:'
      end
      object Label121: TLabel
        Left = 142
        Top = 43
        Width = 63
        Height = 13
        Caption = 'Tot. andares:'
      end
      object Label125: TLabel
        Left = 214
        Top = 43
        Width = 68
        Height = 13
        Caption = 'Tot. unidades:'
      end
      object Label27: TLabel
        Left = 289
        Top = 43
        Width = 45
        Height = 13
        Caption = 'Telefone:'
      end
      object Label197: TLabel
        Left = 402
        Top = 43
        Width = 61
        Height = 13
        Caption = 'Fra'#231#227'o ideal:'
        FocusControl = DBEdit9
      end
      object Label51: TLabel
        Left = 646
        Top = 3
        Width = 51
        Height = 13
        Caption = 'Assistente:'
      end
      object Label52: TLabel
        Left = 8
        Top = 43
        Width = 26
        Height = 13
        Caption = 'Sigla:'
        FocusControl = DBEdit12
      end
      object Label53: TLabel
        Left = 850
        Top = 43
        Width = 33
        Height = 13
        Caption = 'Status:'
        FocusControl = DBEdAtivo
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 20
        Width = 48
        Height = 21
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsCond
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit2: TDBEdit
        Left = 60
        Top = 20
        Width = 583
        Height = 21
        Color = clWhite
        DataField = 'NCONDOM'
        DataSource = DsCond
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit061: TDBEdit
        Left = 142
        Top = 59
        Width = 70
        Height = 21
        DataField = 'Andares'
        DataSource = DsCond
        TabOrder = 2
      end
      object DBEdit062: TDBEdit
        Left = 214
        Top = 59
        Width = 69
        Height = 21
        DataField = 'TotApt'
        DataSource = DsCond
        TabOrder = 3
      end
      object DBEdTel: TDBEdit
        Left = 289
        Top = 59
        Width = 111
        Height = 21
        DataField = 'TELCON_TXT'
        DataSource = DsCond
        TabOrder = 4
      end
      object DBEdit9: TDBEdit
        Left = 402
        Top = 59
        Width = 79
        Height = 21
        DataField = 'SomaFracao'
        DataSource = DsCond
        TabOrder = 5
      end
      object PB1: TProgressBar
        Left = 488
        Top = 59
        Width = 356
        Height = 21
        TabOrder = 6
      end
      object DBEdit11: TDBEdit
        Left = 646
        Top = 20
        Width = 335
        Height = 21
        DataField = 'NO_ASSISTENTE'
        DataSource = DsCond
        TabOrder = 7
      end
      object DBEdit12: TDBEdit
        Left = 8
        Top = 59
        Width = 132
        Height = 21
        DataField = 'Sigla'
        DataSource = DsCond
        TabOrder = 8
      end
      object DBEdAtivo: TDBEdit
        Left = 850
        Top = 59
        Width = 131
        Height = 21
        DataField = 'ATIVO_TXT'
        DataSource = DsCond
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 9
        OnChange = DBEdAtivoChange
      end
    end
    object PnUmAUm: TPanel
      Left = 0
      Top = 568
      Width = 993
      Height = 22
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      Visible = False
      object CkUmAUm: TCheckBox
        Left = 16
        Top = 2
        Width = 154
        Height = 16
        Caption = 'Aborta altera'#231#227'o "um a um".'
        TabOrder = 0
        Visible = False
      end
    end
    object GBCntrl: TGroupBox
      Left = 0
      Top = 590
      Width = 993
      Height = 64
      Align = alBottom
      TabOrder = 3
      object Panel5: TPanel
        Left = 2
        Top = 14
        Width = 169
        Height = 48
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 126
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 86
          Top = 4
          Width = 40
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 47
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 39
          Height = 39
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object LaRegistro: TStaticText
        Left = 171
        Top = 14
        Width = 30
        Height = 17
        Align = alClient
        BevelInner = bvLowered
        BevelKind = bkFlat
        Caption = '[N]: 0'
        TabOrder = 2
      end
      object Panel3: TPanel
        Left = 334
        Top = 14
        Width = 657
        Height = 48
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object Panel2: TPanel
          Left = 550
          Top = 0
          Width = 108
          Height = 47
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 89
            Height = 39
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCond: TBitBtn
          Tag = 10005
          Left = 4
          Top = 4
          Width = 89
          Height = 39
          Cursor = crHandPoint
          Caption = '&Cond.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCondClick
        end
        object BtBloco: TBitBtn
          Tag = 10003
          Left = 94
          Top = 4
          Width = 89
          Height = 39
          Cursor = crHandPoint
          Caption = '&Bloco'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtBlocoClick
        end
        object BtImovel: TBitBtn
          Tag = 10021
          Left = 185
          Top = 4
          Width = 89
          Height = 39
          Cursor = crHandPoint
          Caption = '&Im'#243'vel'
          Enabled = False
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtImovelClick
        end
      end
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 993
    Height = 51
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object GB_R: TGroupBox
      Left = 946
      Top = 0
      Width = 47
      Height = 51
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 12
        Width = 31
        Height = 31
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 298
      Height = 51
      Align = alLeft
      TabOrder = 1
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 86
        Top = 8
        Width = 40
        Height = 39
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 128
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 170
        Top = 8
        Width = 39
        Height = 39
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
      object BtEntidades: TBitBtn
        Tag = 132
        Left = 212
        Top = 8
        Width = 39
        Height = 39
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 5
        OnClick = BtEntidadesClick
      end
      object BtWClients: TBitBtn
        Tag = 10003
        Left = 254
        Top = 8
        Width = 40
        Height = 39
        Cursor = crHandPoint
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 6
        OnClick = BtWClientsClick
      end
    end
    object GB_M: TGroupBox
      Left = 298
      Top = 0
      Width = 347
      Height = 51
      Align = alLeft
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 300
        Height = 31
        Caption = 'Cadastro de Condom'#237'nios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 300
        Height = 31
        Caption = 'Cadastro de Condom'#237'nios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 300
        Height = 31
        Caption = 'Cadastro de Condom'#237'nios'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -26
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
    object GBAvisos1: TGroupBox
      Left = 645
      Top = 0
      Width = 301
      Height = 51
      Align = alClient
      Caption = ' Avisos: '
      TabOrder = 3
      object Panel1: TPanel
        Left = 2
        Top = 14
        Width = 297
        Height = 36
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object LaAviso1: TLabel
          Left = 13
          Top = 2
          Width = 12
          Height = 16
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clSilver
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
        object LaAviso2: TLabel
          Left = 12
          Top = 1
          Width = 12
          Height = 16
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clRed
          Font.Height = -14
          Font.Name = 'Arial'
          Font.Style = []
          ParentFont = False
          Transparent = True
        end
      end
    end
  end
  object DsCond: TDataSource
    DataSet = QrCond
    Left = 600
    Top = 12
  end
  object QrCond: TMySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCondBeforeOpen
    AfterOpen = QrCondAfterOpen
    BeforeClose = QrCondBeforeClose
    AfterScroll = QrCondAfterScroll
    OnCalcFields = QrCondCalcFields
    SQL.Strings = (
      'SELECT IF(con.Ativo=0, "N'#195'O", "SIM") ATIVO_TXT, con.*,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NCONDOM,'
      'IF (ent.Tipo=0, ent.ETe1, ent.PTe1) TELCOM,'
      'cfg.Nome NO_CNAB_CFG, ass.Nome NO_ASSISTENTE,'
      'cat.Nome NOMECARTCONCIL'
      'FROM cond con'
      'LEFT JOIN entidades ent ON ent.Codigo = con.Cliente'
      'LEFT JOIN entidades ass ON ass.Codigo = con.Assistente'
      'LEFT JOIN carteiras cat ON cat.Codigo = con.CartConcil'
      'LEFT JOIN cnab_cfg  cfg ON cfg.Codigo = con.CNAB_Cfg'
      'WHERE con.Codigo > 0'
      '')
    Left = 572
    Top = 12
    object QrCondNCONDOM: TWideStringField
      FieldName = 'NCONDOM'
      Size = 100
    end
    object QrCondCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'cond.Codigo'
    end
    object QrCondCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'cond.Cliente'
    end
    object QrCondTxtParecer: TWideMemoField
      FieldName = 'TxtParecer'
      Origin = 'cond.TxtParecer'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCondLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'cond.Lk'
    end
    object QrCondDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'cond.DataCad'
    end
    object QrCondDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'cond.DataAlt'
    end
    object QrCondUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'cond.UserCad'
    end
    object QrCondUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'cond.UserAlt'
    end
    object QrCondAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'cond.AlterWeb'
    end
    object QrCondAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'cond.Ativo'
    end
    object QrCondTELCOM: TWideStringField
      FieldName = 'TELCOM'
    end
    object QrCondTELCON_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TELCON_TXT'
      Calculated = True
    end
    object QrCondDiaVencto: TIntegerField
      FieldName = 'DiaVencto'
    end
    object QrCondSigla: TWideStringField
      FieldName = 'Sigla'
      Size = 10
    end
    object QrCondObserv: TWideMemoField
      FieldName = 'Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCondAndares: TIntegerField
      FieldName = 'Andares'
    end
    object QrCondTotApt: TIntegerField
      FieldName = 'TotApt'
    end
    object QrCondCliLogoPath: TWideStringField
      FieldName = 'CliLogoPath'
      Size = 255
    end
    object QrCondModelBloq: TSmallintField
      FieldName = 'ModelBloq'
    end
    object QrCondMIB: TSmallintField
      FieldName = 'MIB'
    end
    object QrCondMBB: TSmallintField
      FieldName = 'MBB'
    end
    object QrCondMRB: TSmallintField
      FieldName = 'MRB'
    end
    object QrCondPBB: TSmallintField
      FieldName = 'PBB'
    end
    object QrCondMSB: TSmallintField
      FieldName = 'MSB'
    end
    object QrCondPSB: TSmallintField
      FieldName = 'PSB'
    end
    object QrCondMPB: TSmallintField
      FieldName = 'MPB'
    end
    object QrCondMAB: TSmallintField
      FieldName = 'MAB'
    end
    object QrCondPwdSite: TSmallintField
      FieldName = 'PwdSite'
    end
    object QrCondProLaINSSr: TFloatField
      FieldName = 'ProLaINSSr'
    end
    object QrCondProLaINSSp: TFloatField
      FieldName = 'ProLaINSSp'
    end
    object QrCondConfigBol: TIntegerField
      FieldName = 'ConfigBol'
    end
    object QrCondOcultaBloq: TSmallintField
      FieldName = 'OcultaBloq'
    end
    object QrCondSomaFracao: TFloatField
      FieldName = 'SomaFracao'
    end
    object QrCondMSP: TSmallintField
      FieldName = 'MSP'
    end
    object QrCondBalAgrMens: TSmallintField
      FieldName = 'BalAgrMens'
    end
    object QrCondCompe: TSmallintField
      FieldName = 'Compe'
    end
    object QrCondHideCompe: TSmallintField
      FieldName = 'HideCompe'
    end
    object QrCondFlxB_Ordem: TIntegerField
      FieldName = 'FlxB_Ordem'
    end
    object QrCondFlxB_Folha: TSmallintField
      FieldName = 'FlxB_Folha'
    end
    object QrCondFlxB_LeiAr: TSmallintField
      FieldName = 'FlxB_LeiAr'
    end
    object QrCondFlxB_Fecha: TSmallintField
      FieldName = 'FlxB_Fecha'
    end
    object QrCondFlxB_Risco: TSmallintField
      FieldName = 'FlxB_Risco'
    end
    object QrCondFlxB_Print: TSmallintField
      FieldName = 'FlxB_Print'
    end
    object QrCondFlxB_Proto: TSmallintField
      FieldName = 'FlxB_Proto'
    end
    object QrCondFlxB_Relat: TSmallintField
      FieldName = 'FlxB_Relat'
    end
    object QrCondFlxB_EMail: TSmallintField
      FieldName = 'FlxB_EMail'
    end
    object QrCondFlxB_Porta: TSmallintField
      FieldName = 'FlxB_Porta'
    end
    object QrCondFlxB_Postl: TSmallintField
      FieldName = 'FlxB_Postl'
    end
    object QrCondFlxM_Ordem: TIntegerField
      FieldName = 'FlxM_Ordem'
    end
    object QrCondFlxM_Conci: TSmallintField
      FieldName = 'FlxM_Conci'
    end
    object QrCondFlxM_Docum: TSmallintField
      FieldName = 'FlxM_Docum'
    end
    object QrCondFlxM_Anali: TSmallintField
      FieldName = 'FlxM_Anali'
    end
    object QrCondFlxM_Contb: TSmallintField
      FieldName = 'FlxM_Contb'
    end
    object QrCondFlxM_Encad: TSmallintField
      FieldName = 'FlxM_Encad'
    end
    object QrCondFlxM_Entrg: TSmallintField
      FieldName = 'FlxM_Entrg'
    end
    object QrCondAssistente: TIntegerField
      FieldName = 'Assistente'
    end
    object QrCondNO_ASSISTENTE: TWideStringField
      FieldName = 'NO_ASSISTENTE'
      Size = 100
    end
    object QrCondATIVO_TXT: TWideStringField
      DisplayWidth = 10
      FieldName = 'ATIVO_TXT'
      Required = True
      Size = 10
    end
    object QrCondFlxM_Web: TSmallintField
      FieldName = 'FlxM_Web'
    end
    object QrCondCfgInfl: TIntegerField
      FieldName = 'CfgInfl'
    end
    object QrCondNO_CfgInfl: TWideStringField
      FieldName = 'NO_CfgInfl'
      Size = 100
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanDel01 = BtBloquetos
    Left = 628
    Top = 12
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 388
    Top = 404
  end
  object QrClientes: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, ent.CodUsu,'
      'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) Nome'
      'FROM entidades ent'
      'WHERE Cliente1 = "V"'
      'ORDER BY Nome')
    Left = 360
    Top = 404
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
    object QrClientesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object PMCond: TPopupMenu
    OnPopup = PMCondPopup
    Left = 276
    Top = 404
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
    object Exclui1: TMenuItem
      Caption = '&Exclui'
      Enabled = False
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Conselho1: TMenuItem
      Caption = '&Conselho'
      object Membrosdoconselho1: TMenuItem
        Caption = '&Membros do conselho'
        OnClick = Membrosdoconselho1Click
      end
      object ParecerdoconselhoTexto1: TMenuItem
        Caption = '&Parecer do conselho (Texto)'
        OnClick = ParecerdoconselhoTexto1Click
      end
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Fluxos1: TMenuItem
      Caption = '&Fluxos'
      OnClick = Fluxos1Click
    end
    object Emails1: TMenuItem
      Caption = 'E-&mails'
      object odos1: TMenuItem
        Caption = '&Todos'
        object Executarprogramapadro1: TMenuItem
          Caption = '&Executar programa padr'#227'o'
          OnClick = Executarprogramapadro1Click
        end
        object Listaremails1: TMenuItem
          Caption = '&Listar e-mails'
          OnClick = Listaremails1Click
        end
      end
      object Conselho2: TMenuItem
        Caption = '&Conselho'
        object Executarprogramapadro2: TMenuItem
          Caption = '&Executar programa padr'#227'o'
          OnClick = Executarprogramapadro2Click
        end
        object Listaremails2: TMenuItem
          Caption = '&Listar e-mails'
          OnClick = Listaremails2Click
        end
      end
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object ImportadeExcel1: TMenuItem
      Caption = 'Importa de Excel'
      OnClick = ImportadeExcel1Click
    end
  end
  object DsCondOutros: TDataSource
    DataSet = QrCondOutros
    Left = 684
    Top = 12
  end
  object DsCondBloco: TDataSource
    DataSet = QrCondBloco
    Left = 740
    Top = 12
  end
  object PMBloco: TPopupMenu
    OnPopup = PMBlocoPopup
    Left = 304
    Top = 404
    object Inclui3: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui3Click
    end
    object Altera3: TMenuItem
      Caption = '&Altera'
      OnClick = Altera3Click
    end
    object Exclui3: TMenuItem
      Caption = '&Exclui'
      Enabled = False
    end
  end
  object DsCondImov: TDataSource
    DataSet = QrCondImov
    Left = 796
    Top = 12
  end
  object PMImovel: TPopupMenu
    OnPopup = PMImovelPopup
    Left = 332
    Top = 404
    object Inclui4: TMenuItem
      Caption = '&Inclui'
      object Multiplos1: TMenuItem
        Caption = '&R'#225'pido'
        OnClick = Multiplos1Click
      end
      object Unico1: TMenuItem
        Caption = '&Detalhado'
        OnClick = Unico1Click
      end
    end
    object Altera4: TMenuItem
      Caption = '&Altera im'#243'vel atual'
      OnClick = Altera4Click
    end
    object Alteraselecionados1: TMenuItem
      Caption = 'Altera &selecionados'
      object FracaoIdeal1: TMenuItem
        Caption = '&Fracao Ideal'
        OnClick = FracaoIdeal1Click
      end
      object Diasparaenviode1: TMenuItem
        Caption = '&Dias para envio de...'
        Enabled = False
        Visible = False
        object SMS1: TMenuItem
          Caption = '&SMS'
          OnClick = SMS1Click
        end
        object Email1: TMenuItem
          Caption = '&E-mail'
          OnClick = Email1Click
        end
        object Carta1: TMenuItem
          Caption = '&Carta'
          OnClick = Carta1Click
        end
      end
      object ConfiguraodeEnviodeMensagens1: TMenuItem
        Caption = 'Configura'#231#227'o de Envio de Mensagens'
        OnClick = ConfiguraodeEnviodeMensagens1Click
      end
    end
    object Alteraumaum1: TMenuItem
      Caption = 'Altera &um a um'
      Visible = False
      OnClick = Alteraumaum1Click
    end
    object Exclui4: TMenuItem
      Caption = '&Exclui im'#243'vel atual'
      Enabled = False
      OnClick = Exclui4Click
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Adicionarzerosesquerda1: TMenuItem
      Caption = 'Adicionar &zeros '#224' esquerda'
      OnClick = Adicionarzerosesquerda1Click
    end
  end
  object QrCondOutros: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCondOutrosCalcFields
    Left = 656
    Top = 12
    object QrCondOutrosCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCondOutrosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrCondOutrosNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 30
    end
    object QrCondOutrosCargo: TIntegerField
      FieldName = 'Cargo'
      Required = True
    end
    object QrCondOutrosAssina: TSmallintField
      FieldName = 'Assina'
      Required = True
    end
    object QrCondOutrosOrdemLista: TIntegerField
      FieldName = 'OrdemLista'
      Required = True
    end
    object QrCondOutrosObserv: TWideStringField
      FieldName = 'Observ'
      Size = 255
    end
    object QrCondOutrosNOME_CARGO: TWideStringField
      FieldName = 'NOME_CARGO'
      Size = 30
    end
    object QrCondOutrosNO_ASSINA: TWideStringField
      FieldName = 'NO_ASSINA'
      Size = 3
    end
    object QrCondOutrosMandatoIni: TDateField
      FieldName = 'MandatoIni'
    end
    object QrCondOutrosMandatoFim: TDateField
      FieldName = 'MandatoFim'
    end
    object QrCondOutrosMandatoIni_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MandatoIni_TXT'
      Size = 10
      Calculated = True
    end
    object QrCondOutrosMandatoFim_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MandatoFim_TXT'
      Size = 10
      Calculated = True
    end
    object QrCondOutrosTe1: TWideStringField
      FieldName = 'Te1'
    end
    object QrCondOutrosCel: TWideStringField
      FieldName = 'Cel'
    end
    object QrCondOutrosEmail: TWideStringField
      FieldName = 'Email'
      Size = 100
    end
    object QrCondOutrosEntidade: TIntegerField
      FieldName = 'Entidade'
    end
    object QrCondOutrosTe1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Te1_TXT'
      Size = 30
      Calculated = True
    end
    object QrCondOutrosCel_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Cel_TXT'
      Size = 30
      Calculated = True
    end
  end
  object QrCondBloco: TMySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrCondBlocoAfterOpen
    BeforeClose = QrCondBlocoBeforeClose
    AfterScroll = QrCondBlocoAfterScroll
    SQL.Strings = (
      'SELECT * FROM condbloco'
      'WHERE Codigo =:P0'
      'ORDER BY Ordem, Descri')
    Left = 712
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondBlocoCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'condbloco.Codigo'
    end
    object QrCondBlocoControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'condbloco.Controle'
    end
    object QrCondBlocoDescri: TWideStringField
      FieldName = 'Descri'
      Origin = 'condbloco.Descri'
      Size = 100
    end
    object QrCondBlocoOrdem: TIntegerField
      FieldName = 'Ordem'
      Origin = 'condbloco.Ordem'
    end
    object QrCondBlocoSomaFracao: TFloatField
      FieldName = 'SomaFracao'
      Origin = 'condbloco.SomaFracao'
      DisplayFormat = '0.000000'
    end
    object QrCondBlocoPrefixoUH: TWideStringField
      FieldName = 'PrefixoUH'
      Origin = 'condbloco.PrefixoUH'
    end
    object QrCondBlocoIDExporta: TWideStringField
      FieldName = 'IDExporta'
      Origin = 'condbloco.IDExporta'
    end
  end
  object QrCondImov: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCondImovCalcFields
    SQL.Strings = (
      'SELECT con.Nome NOMECONJUGE, sta.Descri STATUS1, '
      'IF (pro.Tipo=0, pro.RazaoSocial, pro.Nome) NOMEPROP,'
      'IF (mor.Tipo=0, mor.RazaoSocial, mor.Nome) NOMEUSUARIO,'
      'IF (pcu.Tipo=0, pcu.RazaoSocial, pcu.Nome) NOMEPROCURADOR,'
      'IF (ena.Tipo=0, ena.RazaoSocial, ena.Nome) NOMEEMP1,'
      'IF (enb.Tipo=0, enb.RazaoSocial, enb.Nome) NOMEEMP2,'
      'IF (enc.Tipo=0, enc.RazaoSocial, enc.Nome) NOMEEMP3,'
      'end.RazaoSocial IMOB, ptc.Nome NOMEPROTOCOLO, '
      'pt2.Nome NOMEPROTOCOLO2, pt3.Nome NOMEPROTOCOLO3,'
      'imc.*'
      'FROM condimov imc'
      'LEFT JOIN entidades pro ON pro.Codigo=imc.Propriet'
      'LEFT JOIN entidades mor ON mor.Codigo=imc.Usuario'
      'LEFT JOIN entidades pcu ON pcu.Codigo=imc.Procurador'
      'LEFT JOIN entidades con ON con.Codigo=imc.Conjuge'
      'LEFT JOIN entidades ena ON ena.Codigo=imc.ENome1'
      'LEFT JOIN entidades enb ON enb.Codigo=imc.ENome2'
      'LEFT JOIN entidades enc ON enc.Codigo=imc.ENome3'
      'LEFT JOIN status sta ON sta.Codigo=imc.Status'
      'LEFT JOIN entidades end ON end.Codigo=imc.Imobiliaria'
      'LEFT JOIN protocolos ptc ON ptc.Codigo=imc.Protocolo'
      'LEFT JOIN protocolos pt2 ON pt2.Codigo=imc.Protocolo2'
      'LEFT JOIN protocolos pt3 ON pt3.Codigo=imc.Protocolo3'
      'WHERE imc.Controle =:P0'
      'ORDER BY Andar, Unidade')
    Left = 768
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondImovNOMEPROP: TWideStringField
      FieldName = 'NOMEPROP'
      Size = 100
    end
    object QrCondImovNOMECONJUGE: TWideStringField
      FieldName = 'NOMECONJUGE'
      Origin = 'entidades.Nome'
      Size = 100
    end
    object QrCondImovSTATUS1: TWideStringField
      FieldName = 'STATUS1'
      Origin = 'status.Descri'
      Size = 100
    end
    object QrCondImovNOMEEMP1: TWideStringField
      FieldName = 'NOMEEMP1'
      Size = 100
    end
    object QrCondImovNOMEEMP2: TWideStringField
      FieldName = 'NOMEEMP2'
      Size = 100
    end
    object QrCondImovNOMEEMP3: TWideStringField
      FieldName = 'NOMEEMP3'
      Size = 100
    end
    object QrCondImovIMOB: TWideStringField
      FieldName = 'IMOB'
      Origin = 'entidades.RazaoSocial'
      Size = 100
    end
    object QrCondImovCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'condimov.Codigo'
      Required = True
    end
    object QrCondImovControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'condimov.Controle'
      Required = True
    end
    object QrCondImovConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'condimov.Conta'
      Required = True
    end
    object QrCondImovPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'condimov.Propriet'
      Required = True
    end
    object QrCondImovConjuge: TIntegerField
      FieldName = 'Conjuge'
      Origin = 'condimov.Conjuge'
      Required = True
    end
    object QrCondImovAndar: TIntegerField
      FieldName = 'Andar'
      Origin = 'condimov.Andar'
      Required = True
    end
    object QrCondImovUnidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'condimov.Unidade'
      Required = True
      Size = 10
    end
    object QrCondImovQtdGaragem: TIntegerField
      FieldName = 'QtdGaragem'
      Origin = 'condimov.QtdGaragem'
      Required = True
    end
    object QrCondImovStatus: TIntegerField
      FieldName = 'Status'
      Origin = 'condimov.Status'
      Required = True
    end
    object QrCondImovENome1: TIntegerField
      FieldName = 'ENome1'
      Origin = 'condimov.ENome1'
      Required = True
    end
    object QrCondImovESegunda1: TSmallintField
      FieldName = 'ESegunda1'
      Origin = 'condimov.ESegunda1'
    end
    object QrCondImovETerca1: TSmallintField
      FieldName = 'ETerca1'
      Origin = 'condimov.ETerca1'
    end
    object QrCondImovEQuarta1: TSmallintField
      FieldName = 'EQuarta1'
      Origin = 'condimov.EQuarta1'
    end
    object QrCondImovEQuinta1: TSmallintField
      FieldName = 'EQuinta1'
      Origin = 'condimov.EQuinta1'
    end
    object QrCondImovESexta1: TSmallintField
      FieldName = 'ESexta1'
      Origin = 'condimov.ESexta1'
    end
    object QrCondImovESabado1: TSmallintField
      FieldName = 'ESabado1'
      Origin = 'condimov.ESabado1'
    end
    object QrCondImovEDomingo1: TSmallintField
      FieldName = 'EDomingo1'
      Origin = 'condimov.EDomingo1'
    end
    object QrCondImovEVeic1: TSmallintField
      FieldName = 'EVeic1'
      Origin = 'condimov.EVeic1'
    end
    object QrCondImovEFilho1: TSmallintField
      FieldName = 'EFilho1'
      Origin = 'condimov.EFilho1'
    end
    object QrCondImovENome2: TIntegerField
      FieldName = 'ENome2'
      Origin = 'condimov.ENome2'
      Required = True
    end
    object QrCondImovESegunda2: TSmallintField
      FieldName = 'ESegunda2'
      Origin = 'condimov.ESegunda2'
    end
    object QrCondImovETerca2: TSmallintField
      FieldName = 'ETerca2'
      Origin = 'condimov.ETerca2'
    end
    object QrCondImovEQuarta2: TSmallintField
      FieldName = 'EQuarta2'
      Origin = 'condimov.EQuarta2'
    end
    object QrCondImovEQuinta2: TSmallintField
      FieldName = 'EQuinta2'
      Origin = 'condimov.EQuinta2'
    end
    object QrCondImovESexta2: TSmallintField
      FieldName = 'ESexta2'
      Origin = 'condimov.ESexta2'
    end
    object QrCondImovESabado2: TSmallintField
      FieldName = 'ESabado2'
      Origin = 'condimov.ESabado2'
    end
    object QrCondImovEDomingo2: TSmallintField
      FieldName = 'EDomingo2'
      Origin = 'condimov.EDomingo2'
    end
    object QrCondImovEVeic2: TSmallintField
      FieldName = 'EVeic2'
      Origin = 'condimov.EVeic2'
    end
    object QrCondImovEFilho2: TSmallintField
      FieldName = 'EFilho2'
      Origin = 'condimov.EFilho2'
    end
    object QrCondImovENome3: TIntegerField
      FieldName = 'ENome3'
      Origin = 'condimov.ENome3'
      Required = True
    end
    object QrCondImovESegunda3: TSmallintField
      FieldName = 'ESegunda3'
      Origin = 'condimov.ESegunda3'
    end
    object QrCondImovETerca3: TSmallintField
      FieldName = 'ETerca3'
      Origin = 'condimov.ETerca3'
    end
    object QrCondImovEQuarta3: TSmallintField
      FieldName = 'EQuarta3'
      Origin = 'condimov.EQuarta3'
    end
    object QrCondImovEQuinta3: TSmallintField
      FieldName = 'EQuinta3'
      Origin = 'condimov.EQuinta3'
    end
    object QrCondImovESexta3: TSmallintField
      FieldName = 'ESexta3'
      Origin = 'condimov.ESexta3'
    end
    object QrCondImovESabado3: TSmallintField
      FieldName = 'ESabado3'
      Origin = 'condimov.ESabado3'
    end
    object QrCondImovEDomingo3: TSmallintField
      FieldName = 'EDomingo3'
      Origin = 'condimov.EDomingo3'
    end
    object QrCondImovEVeic3: TSmallintField
      FieldName = 'EVeic3'
      Origin = 'condimov.EVeic3'
    end
    object QrCondImovEFilho3: TSmallintField
      FieldName = 'EFilho3'
      Origin = 'condimov.EFilho3'
    end
    object QrCondImovEmNome1: TWideStringField
      FieldName = 'EmNome1'
      Origin = 'condimov.EmNome1'
      Required = True
      Size = 100
    end
    object QrCondImovEmTel1: TWideStringField
      FieldName = 'EmTel1'
      Origin = 'condimov.EmTel1'
    end
    object QrCondImovEmNome2: TWideStringField
      FieldName = 'EmNome2'
      Origin = 'condimov.EmNome2'
      Required = True
      Size = 100
    end
    object QrCondImovEmTel2: TWideStringField
      FieldName = 'EmTel2'
      Origin = 'condimov.EmTel2'
    end
    object QrCondImovObserv: TWideMemoField
      FieldName = 'Observ'
      Origin = 'condimov.Observ'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCondImovSitImv: TIntegerField
      FieldName = 'SitImv'
      Origin = 'condimov.SitImv'
      Required = True
    end
    object QrCondImovLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'condimov.Lk'
    end
    object QrCondImovDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'condimov.DataCad'
    end
    object QrCondImovDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'condimov.DataAlt'
    end
    object QrCondImovUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'condimov.UserCad'
    end
    object QrCondImovUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'condimov.UserAlt'
    end
    object QrCondImovImobiliaria: TIntegerField
      FieldName = 'Imobiliaria'
      Origin = 'condimov.Imobiliaria'
      Required = True
    end
    object QrCondImovContato: TWideStringField
      FieldName = 'Contato'
      Origin = 'condimov.Contato'
      Required = True
      Size = 60
    end
    object QrCondImovContTel: TWideStringField
      FieldName = 'ContTel'
      Origin = 'condimov.ContTel'
      Required = True
    end
    object QrCondImovNOMESITIMV: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMESITIMV'
      Size = 3
      Calculated = True
    end
    object QrCondImovUsuario: TIntegerField
      FieldName = 'Usuario'
      Origin = 'condimov.Usuario'
      Required = True
    end
    object QrCondImovNOMEUSUARIO: TWideStringField
      FieldName = 'NOMEUSUARIO'
      Size = 100
    end
    object QrCondImovProtocolo: TIntegerField
      FieldName = 'Protocolo'
      Origin = 'condimov.Protocolo'
      Required = True
    end
    object QrCondImovNOMEPROTOCOLO: TWideStringField
      FieldName = 'NOMEPROTOCOLO'
      Origin = 'protocolos.Nome'
      Size = 100
    end
    object QrCondImovMoradores: TIntegerField
      FieldName = 'Moradores'
      Origin = 'condimov.Moradores'
      Required = True
    end
    object QrCondImovFracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
      Origin = 'condimov.FracaoIdeal'
      Required = True
      DisplayFormat = '#,###,##0.000000'
    end
    object QrCondImovAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'condimov.Ativo'
      Required = True
    end
    object QrCondImovBloqEndTip: TSmallintField
      FieldName = 'BloqEndTip'
      Origin = 'condimov.BloqEndTip'
      Required = True
    end
    object QrCondImovBloqEndEnt: TSmallintField
      FieldName = 'BloqEndEnt'
      Origin = 'condimov.BloqEndEnt'
      Required = True
    end
    object QrCondImovBloqEndTip_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'BloqEndTip_TXT'
      Size = 10
      Calculated = True
    end
    object QrCondImovEnderLin1: TWideStringField
      FieldName = 'EnderLin1'
      Origin = 'condimov.EnderLin1'
      Size = 100
    end
    object QrCondImovEnderLin2: TWideStringField
      FieldName = 'EnderLin2'
      Origin = 'condimov.EnderLin2'
      Size = 100
    end
    object QrCondImovEnderNome: TWideStringField
      FieldName = 'EnderNome'
      Origin = 'condimov.EnderNome'
      Size = 100
    end
    object QrCondImovProcurador: TIntegerField
      FieldName = 'Procurador'
      Origin = 'condimov.Procurador'
    end
    object QrCondImovNOMEPROCURADOR: TWideStringField
      FieldName = 'NOMEPROCURADOR'
      Size = 100
    end
    object QrCondImovWebLogin: TWideStringField
      FieldName = 'WebLogin'
      Origin = 'condimov.WebLogin'
      Size = 30
    end
    object QrCondImovWebPwd: TWideStringField
      FieldName = 'WebPwd'
      Origin = 'condimov.WebPwd'
      Size = 30
    end
    object QrCondImovWebNivel: TSmallintField
      FieldName = 'WebNivel'
      Origin = 'condimov.WebNivel'
    end
    object QrCondImovWebLogID: TWideStringField
      FieldName = 'WebLogID'
      Origin = 'condimov.WebLogID'
      Size = 32
    end
    object QrCondImovWebLastLog: TDateTimeField
      FieldName = 'WebLastLog'
      Origin = 'condimov.WebLastLog'
    end
    object QrCondImovAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'condimov.AlterWeb'
    end
    object QrCondImovModelBloq: TSmallintField
      FieldName = 'ModelBloq'
      Origin = 'condimov.ModelBloq'
    end
    object QrCondImovConfigBol: TIntegerField
      FieldName = 'ConfigBol'
      Origin = 'condimov.ConfigBol'
    end
    object QrCondImovddVctEsp: TSmallintField
      FieldName = 'ddVctEsp'
      Origin = 'condimov.ddVctEsp'
    end
    object QrCondImovIDExporta: TWideStringField
      FieldName = 'IDExporta'
      Origin = 'condimov.IDExporta'
    end
    object QrCondImovJuridico: TSmallintField
      FieldName = 'Juridico'
      Origin = 'condimov.Juridico'
    end
    object QrCondImovJuridico_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'Juridico_TXT'
      Size = 2
      Calculated = True
    end
    object QrCondImovInadCEMCad: TIntegerField
      FieldName = 'InadCEMCad'
      Origin = 'condimov.InadCEMCad'
    end
    object QrCondImovSMSCelTipo: TSmallintField
      FieldName = 'SMSCelTipo'
      Origin = 'condimov.SMSCelTipo'
    end
    object QrCondImovSMSCelNumr: TWideStringField
      FieldName = 'SMSCelNumr'
      Origin = 'condimov.SMSCelNumr'
      Size = 18
    end
    object QrCondImovSMSCelNome: TWideStringField
      FieldName = 'SMSCelNome'
      Origin = 'condimov.SMSCelNome'
      Size = 60
    end
    object QrCondImovSMSCelEnti: TIntegerField
      FieldName = 'SMSCelEnti'
      Origin = 'condimov.SMSCelEnti'
    end
    object QrCondImovNOMEPROTOCOLO2: TWideStringField
      FieldName = 'NOMEPROTOCOLO2'
      Size = 100
    end
    object QrCondImovNOMEPROTOCOLO3: TWideStringField
      FieldName = 'NOMEPROTOCOLO3'
      Size = 100
    end
  end
  object QrPesq: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT blc.Controle, blc.Descri NO_BLOCO, imv.Unidade, imv.Conta' +
        ','
      'IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) NO_ENT'
      'FROM condimov imv'
      'LEFT JOIN entidades ent ON ent.Codigo=imv.Propriet'
      'LEFT JOIN condbloco blc ON blc.Controle=imv.Controle'
      'WHERE imv.Codigo=1'
      'AND IF(ent.Tipo=0,ent.RazaoSocial,ent.Nome) LIKE "%jo%"'
      'ORDER BY NO_ENT')
    Left = 704
    Top = 548
    object QrPesqNO_BLOCO: TWideStringField
      FieldName = 'NO_BLOCO'
      Size = 100
    end
    object QrPesqUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrPesqNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Size = 100
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesqConta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 732
    Top = 548
  end
  object TbUnidades: TMySQLTable
    Database = Dmod.MyDB
    Filter = 'Codigo=-1000'
    Filtered = True
    SortFieldNames = 'Unidade'
    TableName = 'condimov'
    Left = 824
    Top = 12
    object TbUnidadesUnidade: TWideStringField
      FieldName = 'Unidade'
      Origin = 'condimov.Unidade'
      Size = 10
    end
    object TbUnidadesFracaoIdeal: TFloatField
      FieldName = 'FracaoIdeal'
      Origin = 'condimov.FracaoIdeal'
      DisplayFormat = '#,###,##0.000000'
    end
    object TbUnidadesControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'condimov.Controle'
    end
    object TbUnidadesConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'condimov.Conta'
    end
    object TbUnidadesPropriet: TIntegerField
      FieldName = 'Propriet'
      Origin = 'condimov.Propriet'
    end
    object TbUnidadesIDExporta: TWideStringField
      FieldName = 'IDExporta'
      Origin = 'condimov.IDExporta'
    end
    object TbUnidadesCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'condimov.Codigo'
    end
    object TbUnidadesNOME_BLOCO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_BLOCO'
      LookupDataSet = QrCB
      LookupKeyFields = 'Controle'
      LookupResultField = 'Descri'
      KeyFields = 'Controle'
      Size = 100
      Lookup = True
    end
    object TbUnidadesNOME_PROPRIET: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOME_PROPRIET'
      LookupDataSet = QrCondImvProp
      LookupKeyFields = 'Codigo'
      LookupResultField = 'NOME_PROPRIET'
      KeyFields = 'Propriet'
      Size = 100
      Lookup = True
    end
    object TbUnidadesMoradores: TIntegerField
      FieldName = 'Moradores'
    end
    object TbUnidadesAndar: TIntegerField
      FieldName = 'Andar'
    end
    object TbUnidadesQtdGaragem: TIntegerField
      FieldName = 'QtdGaragem'
    end
  end
  object DsUnidades: TDataSource
    DataSet = TbUnidades
    Left = 852
    Top = 12
  end
  object QrCondModBol: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCondModBolCalcFields
    SQL.Strings = (
      'SELECT imv.Unidade, cfb.Nome NOME_CONFIGBOL,'
      'cmb.Apto, cmb.ConfigBol, cmb.ModelBloq, '
      'cmb.BalAgrMens, cmb.Compe, cmb.Codigo'
      'FROM condmodbol cmb'
      'LEFT JOIN condimov imv ON imv.Conta=cmb.Apto'
      'LEFT JOIN configbol cfb ON cfb.Codigo=cmb.ConfigBol'
      'WHERE cmb.Codigo=:P0'#13)
    Left = 496
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCondModBolUnidade: TWideStringField
      FieldName = 'Unidade'
      Size = 10
    end
    object QrCondModBolNOME_CONFIGBOL: TWideStringField
      FieldName = 'NOME_CONFIGBOL'
      Size = 50
    end
    object QrCondModBolApto: TIntegerField
      FieldName = 'Apto'
    end
    object QrCondModBolConfigBol: TIntegerField
      FieldName = 'ConfigBol'
    end
    object QrCondModBolModelBloq: TSmallintField
      FieldName = 'ModelBloq'
    end
    object QrCondModBolBalAgrMens: TSmallintField
      FieldName = 'BalAgrMens'
    end
    object QrCondModBolCompe: TSmallintField
      FieldName = 'Compe'
    end
    object QrCondModBolNOME_MODELBLOQ: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_MODELBLOQ'
      Size = 1
      Calculated = True
    end
    object QrCondModBolCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsCondModBol: TDataSource
    DataSet = QrCondModBol
    Left = 524
    Top = 388
  end
  object PMBloquetos: TPopupMenu
    OnPopup = PMBloquetosPopup
    Left = 204
    Top = 284
    object Incluialteramodeloconfigurao1: TMenuItem
      Caption = '&Inclui altera modelo / configura'#231#227'o (UHs)'
      OnClick = Incluialteramodeloconfigurao1Click
    end
    object Excluiitemns1: TMenuItem
      Caption = '&Exclui item(ns) (UHs)'
      OnClick = Excluiitemns1Click
    end
  end
  object QrContasMes: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrContasMesCalcFields
    SQL.Strings = (
      'SELECT IF(en.Tipo=0,RazaoSocial,en.Nome) NO_CliInt, cm.* '
      'FROM contasmes cm'
      'LEFT JOIN entidades en ON en.Codigo=cm.CliInt'
      'WHERE cm.CliInt=:P0')
    Left = 512
    Top = 328
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasMesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasMesControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrContasMesCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrContasMesDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrContasMesPeriodoIni: TIntegerField
      FieldName = 'PeriodoIni'
    end
    object QrContasMesPeriodoFim: TIntegerField
      FieldName = 'PeriodoFim'
    end
    object QrContasMesValorMin: TFloatField
      FieldName = 'ValorMin'
    end
    object QrContasMesValorMax: TFloatField
      FieldName = 'ValorMax'
    end
    object QrContasMesPERIODOINI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODOINI_TXT'
      Size = 10
      Calculated = True
    end
    object QrContasMesPERIODOFIM_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'PERIODOFIM_TXT'
      Size = 10
      Calculated = True
    end
    object QrContasMesNO_CliInt: TWideStringField
      FieldName = 'NO_CliInt'
      Size = 100
    end
    object QrContasMesQtdeMin: TIntegerField
      FieldName = 'QtdeMin'
    end
    object QrContasMesQtdeMax: TIntegerField
      FieldName = 'QtdeMax'
    end
    object QrContasMesLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrContasMesDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrContasMesDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrContasMesUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrContasMesUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrContasMesAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrContasMesAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrContasMesDiaAlert: TSmallintField
      FieldName = 'DiaAlert'
    end
    object QrContasMesDiaVence: TSmallintField
      FieldName = 'DiaVence'
    end
    object QrContasMesTipMesRef: TSmallintField
      FieldName = 'TipMesRef'
    end
  end
  object DsContasMes: TDataSource
    DataSet = QrContasMes
    Left = 540
    Top = 328
  end
  object PMContasMes: TPopupMenu
    Left = 276
    Top = 284
    object Incluimultiplascontas1: TMenuItem
      Caption = 'Inclui m'#250'ltiplas contas'
      OnClick = Incluimultiplascontas1Click
    end
    object Incluinicacontaeconfigura1: TMenuItem
      Caption = 'Inclui '#250'nica conta (e configura)'
      OnClick = Incluinicacontaeconfigura1Click
    end
  end
  object QrAssist: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM entidades'
      'WHERE Fornece5="V"'
      'OR Codigo=0'
      'ORDER BY Nome')
    Left = 520
    Top = 580
  end
  object DsAssist: TDataSource
    DataSet = QrAssist
    Left = 548
    Top = 580
  end
  object OpenPictureDialog1: TOpenPictureDialog
    DefaultExt = 'bmp'
    Filter = 
      'Bitmaps (*.bmp)|*.bmp|Icons (*.ico)|*.ico|Metafiles (*.wmf)|*.wm' +
      'f'
    Left = 636
    Top = 564
  end
  object QrCB: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Descri '
      'FROM condbloco'
      'WHERE Codigo =:P0')
    Left = 664
    Top = 388
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCBControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCBDescri: TWideStringField
      FieldName = 'Descri'
      Size = 100
    end
  end
  object QrCondImvProp: TMySQLQuery
    Database = Dmod.MyDB
    Left = 692
    Top = 388
    object QrCondImvPropNOME_PROPRIET: TWideStringField
      FieldName = 'NOME_PROPRIET'
      Size = 100
    end
    object QrCondImvPropCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object QrCfgInfl: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cfginfl'
      'ORDER BY Nome')
    Left = 328
    Top = 600
    object QrCfgInflCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCfgInflNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCfgInfl: TDataSource
    DataSet = QrCfgInfl
    Left = 356
    Top = 600
  end
  object QrConfigBol: TMySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrContasMesCalcFields
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM configbol'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 368
    Top = 320
    object QrConfigBolCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConfigBolNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsConfigBol: TDataSource
    DataSet = QrConfigBol
    Left = 396
    Top = 320
  end
  object QrCNAB_Cfg: TMySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT abc.Codigo, abc.Sigla, abc.Nome'
      'FROM arrebai abi'
      'LEFT JOIN arrebac abc ON abc.Codigo=abi.Codigo'
      'WHERE Cond=:P0'
      'ORDER BY Nome')
    Left = 144
    Top = 476
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCNAB_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB_CfgNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCNAB_Cfg: TDataSource
    DataSet = QrCNAB_Cfg
    Left = 172
    Top = 476
  end
end
