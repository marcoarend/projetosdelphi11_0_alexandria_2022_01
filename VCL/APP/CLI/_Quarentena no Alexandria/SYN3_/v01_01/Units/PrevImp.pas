unit PrevImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, Grids, DBGrids, dmkGeral, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, ComCtrls, dmkEditDateTimePicker, UnMLAGeral, UnInternalConsts,
  dmkImage, Tabs, frxClass, dmkCheckBox, frxDBSet, UnDmkEnums;

type
  TFmPrevImp = class(TForm)
    PnCabeca: TPanel;
    GB_R: TGroupBox;
    ImgTipo: TdmkImage;
    GB_L: TGroupBox;
    GB_M: TGroupBox;
    LaTitulo1A: TLabel;
    LaTitulo1B: TLabel;
    Panel2: TPanel;
    Panel3: TPanel;
    GroupBox1: TGroupBox;
    GBAvisos1: TGroupBox;
    Panel4: TPanel;
    LaAviso1: TLabel;
    LaAviso2: TLabel;
    GBRodaPe: TGroupBox;
    PnSaiDesis: TPanel;
    BtSaida: TBitBtn;
    Panel1: TPanel;
    BtOK: TBitBtn;
    LaTitulo1C: TLabel;
    GroupBox2: TGroupBox;
    LaAno: TLabel;
    LaMes: TLabel;
    CBMes: TComboBox;
    CBAno: TComboBox;
    DockTabSet1: TTabSet;
    Splitter1: TSplitter;
    BtReexibe: TBitBtn;
    Timer1: TTimer;
    frxProv_e_UHs: TfrxReport;
    PB1: TProgressBar;
    QrPRI: TmySQLQuery;
    QrPRIConta: TIntegerField;
    QrPRITexto: TWideStringField;
    QrPRIValor: TFloatField;
    QrPRISubGrupo: TIntegerField;
    QrPRINOMECONTA: TWideStringField;
    QrPRINOMESUBGRUPO: TWideStringField;
    QrPRICodigo: TIntegerField;
    QrPRIControle: TIntegerField;
    QrPRILk: TIntegerField;
    QrPRIDataCad: TDateField;
    QrPRIDataAlt: TDateField;
    QrPRIUserCad: TIntegerField;
    QrPRIUserAlt: TIntegerField;
    QrPRIPrevBaI: TIntegerField;
    CkMorto: TdmkCheckBox;
    QrEmpresas: TmySQLQuery;
    QrEmpresasCodCliInt: TIntegerField;
    QrEmpresasCodEnti: TIntegerField;
    QrCond: TmySQLQuery;
    QrCondCliInt: TIntegerField;
    QrCondNOMECOND: TWideStringField;
    frxDsCond: TfrxDBDataset;
    QrProv: TmySQLQuery;
    frxDsProv: TfrxDBDataset;
    QrProvCodigo: TIntegerField;
    QrProvControle: TIntegerField;
    QrProvConta: TIntegerField;
    QrProvValor: TFloatField;
    QrProvPrevBaC: TIntegerField;
    QrProvPrevBaI: TIntegerField;
    QrProvTexto: TWideStringField;
    QrProvEmitVal: TFloatField;
    QrProvEmitSit: TIntegerField;
    QrProvNomeConta: TWideStringField;
    QrProvNomeSubGrupo: TWideStringField;
    QrProvPeriodo: TIntegerField;
    QrProvCliInt: TIntegerField;
    frxProvisoes: TfrxReport;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DockTabSet1DockDrop(Sender: TObject; Source: TDragDockObject; X,
      Y: Integer);
    procedure DockTabSet1TabRemoved(Sender: TObject);
    procedure DockTabSet1TabAdded(Sender: TObject);
    procedure BtReexibeClick(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure frxProv_e_UHsGetValue(const VarName: string; var Value: Variant);
  private
    { Private declarations }
    FInCreation: Boolean;
    FPreparou: Boolean;
    procedure RecriaDockForms();
    procedure InserePri(TabPrev: String; CliInt, Periodo: Integer; Morto: Boolean);
    procedure ReopenCond(TabPrev: String);
    procedure ReopenProv(TabPrev: String; CliInt: Integer);
  public
    { Public declarations }
  end;

  var
  FmPrevImp: TFmPrevImp;

implementation

uses UnMyObjects, Module, CreateGeral, UMySQLModule, UCreate, DmkDAC_PF,
//DockForms Ini
_Empresas
//DockForms Fim
, ModuleGeral, UnDmkProcFunc, Periodo, MeuFrx;

{$R *.DFM}

const
  MaxJan = 3;

procedure TFmPrevImp.BtOKClick(Sender: TObject);
var
  Periodo, CliInt: Integer;
  TabPrev: String;
  Prim: Boolean;
  procedure GeraRelatorio;
  var
    s: TMemoryStream;
    frxAtu: TfrxReport;
  begin
    frxAtu := frxProv_e_UHs;
    //
    frxProvisoes.OnGetValue         := frxAtu.OnGetValue;
    frxProvisoes.OnUserFunction     := frxAtu.OnUserFunction;
    frxProvisoes.ReportOptions.Name := 'Provis�es';
    //
    s := TMemoryStream.Create;
    frxAtu.SaveToStream(s);
    s.Position := 0;
    frxProvisoes.LoadFromStream(s);
    //
    if not Prim then
    begin
      frxProvisoes.PrepareReport(True);
      Prim := True;
    end else
      frxProvisoes.PrepareReport(False);
  end;
  procedure ImprimeRelatorio;
  begin
    Application.CreateForm(TFmMeuFrx, FmMeuFrx);
    frxProvisoes.Preview := FmMeufrx.PvVer;
    //
    FmMeufrx.PvVer.OutlineWidth := frxProvisoes.PreviewOptions.OutlineWidth;
    FmMeufrx.PvVer.Zoom         := frxProvisoes.PreviewOptions.Zoom;
    FmMeufrx.PvVer.ZoomMode     := frxProvisoes.PreviewOptions.ZoomMode;
    FmMeufrx.UpdateZoom;
    //
    FmMeufrx.ShowModal;
    FmMeufrx.Destroy;
  end;
begin
  Prim    := False;
  TabPrev := UCriar.RecriaTempTableNovo(ntrttPri, DmodG.QrUpdPID1, False);
  //
  Periodo := MLAGeral.PeriodoEncode(
    Geral.IMV(CBAno.Items[CBAno.ItemIndex]), CBMes.ItemIndex+1);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrEmpresas, DmodG.MyPID_DB, [
  'SELECT CodCliInt, CodEnti ',
  'FROM ' + TAB_TMP_EMPRESAS,
  'WHERE Ativo = 1 ',
  'ORDER BY CodCliInt ',
  '']);
  if QrEmpresas.RecordCount > 0 then
  begin
    PB1.Position := 0;
    PB1.Max      := QrEmpresas.RecordCount;
    PB1.Visible  := True;
    //
    QrEmpresas.First;
    while not QrEmpresas.Eof do
    begin
      CliInt := QrEmpresasCodCliInt.Value;
      InserePri(TabPrev, CliInt, Periodo, CkMorto.Checked);
      //
      PB1.Position := PB1.Position + 1;
      PB1.Update;
      Application.ProcessMessages;
      //
      QrEmpresas.Next;
    end;
    PB1.Visible := False;
    //
    ReopenCond(TabPrev);
    if QrCond.RecordCount > 0 then
    begin
      try
        Screen.Cursor := crHourGlass;
        //
        QrCond.First;
        while not QrCond.Eof do
        begin
          ReopenProv(TabPrev, QrCondCliInt.Value);
          //
          GeraRelatorio;
          //
          QrCond.Next;
        end;
      finally
        Screen.Cursor := crDefault;
      end;
      MyObjects.frxDefineDataSets(frxProv_e_UHs, [frxDsCond, frxDsProv, DModG.frxDsDono]);
      ImprimeRelatorio;
      //MyObjects.frxMostra(frxProv_e_UHs, 'Provis�es');
    end;
  end else
    Geral.MB_Aviso('Voc� deve selecionar no m�nimo um condom�nio!');
end;

procedure TFmPrevImp.BtReexibeClick(Sender: TObject);
begin
  RecriaDockForms();
end;

procedure TFmPrevImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPrevImp.DockTabSet1DockDrop(Sender: TObject;
  Source: TDragDockObject; X, Y: Integer);
begin
  DockTabSet1.Visible := True;
end;

procedure TFmPrevImp.DockTabSet1TabAdded(Sender: TObject);
begin
  DockTabSet1.Visible := DockTabSet1.Tabs.Count > 0;
end;

procedure TFmPrevImp.DockTabSet1TabRemoved(Sender: TObject);
begin
  DockTabSet1.Visible := DockTabSet1.Tabs.Count > 0;
end;

procedure TFmPrevImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente();
  Timer1.Enabled := True;
end;

procedure TFmPrevImp.FormCreate(Sender: TObject);
begin
  ImgTipo.SQLType := stLok;
  FInCreation     := False;
  PB1.Visible     := False;
  //
  MyObjects.PreencheCBAnoECBMes(CBAno, CBMes, -1);
end;

procedure TFmPrevImp.FormResize(Sender: TObject);
begin
  MyObjects.Entitula(GB_M, [LaTitulo1A, LaTitulo1B, LaTitulo1C],
  [LaAviso1, LaAviso2], Caption, True, taCenter, 2, 10, 20);
end;

procedure TFmPrevImp.frxProv_e_UHsGetValue(const VarName: string;
  var Value: Variant);
var
  Periodo: Integer;
begin
  Periodo := MLAGeral.PeriodoEncode(
    Geral.IMV(CBAno.Items[CBAno.ItemIndex]), CBMes.ItemIndex+1);
  //
  if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then Value :=
    dmkPF.MesEAnoDoPeriodoLongo(Periodo)
  else
  if VarName = 'VARF_IMPRIME_PAGE1' then
    Value := True
  else
  if VarName = 'VARF_IMPRIME_PAGE2' then
    Value := False
end;

procedure TFmPrevImp.RecriaDockForms;
var
  i: Integer;
begin
  if FInCreation then
    Exit;
  FInCreation := True;
  //
  MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Carregando tabelas');
  Screen.Cursor := crHourGlass;
  try
    for i := 0 to Screen.FormCount - MaxJan - 1 do
    begin
      if Screen.Forms[i] is TFm_Empresas then
        Screen.Forms[i].Destroy;
    end;
    TFm_Empresas.CreateDockForm().ManualDock(DockTabSet1);
    //
    FInCreation := False;
    MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrevImp.ReopenCond(TabPrev: String);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrCond, DModG.MyPID_DB, [
  'SELECT pri.CliInt, ',
  'IF (ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECOND ',
  'FROM ' + TabPrev + ' pri ',
  'LEFT JOIN ' + TMeuDB + '.enticliint cli ON cli.CodCliInt = pri.CliInt ',
  'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo = cli.CodEnti ',
  'GROUP BY pri.CliInt ',
  'ORDER BY pri.CliInt ',
  '']);
end;

procedure TFmPrevImp.ReopenProv(TabPrev: String; CliInt: Integer);
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrProv, DModG.MyPID_DB, [
  'SELECT pit.* ',
  'FROM '+ TabPrev +' pit ',
  'WHERE CliInt=' + Geral.FF0(CliInt),
  'ORDER BY NomeSubGrupo, NomeConta ',
  '']);
end;

procedure TFmPrevImp.InserePri(TabPrev: String; CliInt, Periodo: Integer;
  Morto: Boolean);
var
  TabPriA, TabPriB, TabPriD, TabPrvA, TabPrvB, TabPrvD: String;
begin
  try
    Screen.Cursor := crHourGlass;
    //
    if CliInt > 0 then
    begin
      TabPriA := DModG.NomeTab(TMeuDB, ntPri, False, ttA, CliInt);
      TabPriB := DModG.NomeTab(TMeuDB, ntPri, False, ttB, CliInt);
      TabPriD := DModG.NomeTab(TMeuDB, ntPri, False, ttD, CliInt);
      //
      TabPrvA := DModG.NomeTab(TMeuDB, ntPrv, False, ttA, CliInt);
      TabPrvB := DModG.NomeTab(TMeuDB, ntPrv, False, ttB, CliInt);
      TabPrvD := DModG.NomeTab(TMeuDB, ntPrv, False, ttD, CliInt);
      //
      QrPRI.Close;
      QrPRI.Database := DModG.MyPID_DB;
      QrPRI.SQL.Clear;
      QrPRI.SQL.Add('INSERT INTO ' + TabPrev + ' ');
      QrPRI.SQL.Add('SELECT pit.Codigo, pit.Controle, pit.Conta, pit.Valor, ');
      QrPRI.SQL.Add('pit.PrevBaC, pit.PrevBaI, pit.Texto, pit.EmitVal, pit.EmitSit, ');
      QrPRI.SQL.Add('con.Nome NOMECONTA, sgo.Nome NOMESUBGRUPO, prv.Periodo, prv.Cond ');
      QrPRI.SQL.Add('FROM ' + TabPriA + ' pit ');
      QrPRI.SQL.Add('LEFT JOIN ' + TabPrvA + ' prv ON prv.Codigo=pit.Codigo ');
      QrPRI.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas con ON con.Codigo=pit.Conta ');
      QrPRI.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgo ON sgo.Codigo=con.SubGrupo ');
      QrPRI.SQL.Add('WHERE prv.Periodo=' + Geral.FF0(Periodo));
      if Morto then
      begin
        QrPRI.SQL.Add('UNION ');
        QrPRI.SQL.Add('SELECT pit.Codigo, pit.Controle, pit.Conta, pit.Valor, ');
        QrPRI.SQL.Add('pit.PrevBaC, pit.PrevBaI, pit.Texto, pit.EmitVal, pit.EmitSit, ');
        QrPRI.SQL.Add('con.Nome NOMECONTA, sgo.Nome NOMESUBGRUPO, prv.Periodo, prv.Cond ');
        QrPRI.SQL.Add('FROM ' + TabPriB + ' pit ');
        QrPRI.SQL.Add('LEFT JOIN ' + TabPrvB + ' prv ON prv.Codigo=pit.Codigo ');
        QrPRI.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas con ON con.Codigo=pit.Conta ');
        QrPRI.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgo ON sgo.Codigo=con.SubGrupo ');
        QrPRI.SQL.Add('WHERE prv.Periodo=' + Geral.FF0(Periodo));
        QrPRI.SQL.Add('UNION ');
        QrPRI.SQL.Add('SELECT pit.Codigo, pit.Controle, pit.Conta, pit.Valor, ');
        QrPRI.SQL.Add('pit.PrevBaC, pit.PrevBaI, pit.Texto, pit.EmitVal, pit.EmitSit, ');
        QrPRI.SQL.Add('con.Nome NOMECONTA, sgo.Nome NOMESUBGRUPO, prv.Periodo, prv.Cond ');
        QrPRI.SQL.Add('FROM ' + TabPriD + ' pit ');
        QrPRI.SQL.Add('LEFT JOIN ' + TabPrvD + ' prv ON prv.Codigo=pit.Codigo ');
        QrPRI.SQL.Add('LEFT JOIN ' + TMeuDB + '.contas con ON con.Codigo=pit.Conta ');
        QrPRI.SQL.Add('LEFT JOIN ' + TMeuDB + '.subgrupos sgo ON sgo.Codigo=con.SubGrupo ');
        QrPRI.SQL.Add('WHERE prv.Periodo=' + Geral.FF0(Periodo));
      end;
      UMyMod.ExecutaQuery(QrPRI);
    end;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPrevImp.Timer1Timer(Sender: TObject);
begin
  Timer1.Enabled := False;
  if FPreparou = False then
  begin
    Screen.Cursor := crHourGlass;
    try
      FPreparou := True;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, True, 'Criando tabelas tempor�rias');
      //
      DModG.Tb_Empresas.Close;
      DModG.Tb_Empresas.Database := DModG.MyPID_DB;
      UnCreateGeral.RecriaTempTableNovo(ntrtt_Empresas, DModG.QrUpdPID1,
        False, 1, TAB_TMP_EMPRESAS);
      UMyMod.ExecutaMySQLQuery1(DModG.QrUpdPID1, [
      'DELETE FROM ' + TAB_TMP_EMPRESAS + ';',
      'INSERT INTO '  + TAB_TMP_EMPRESAS,
      'SELECT eci.CodCliInt, eci.CodEnti, eci.CodFilial, ',
      'IF(ent.Tipo=0, ent.RazaoSocial, Nome) Nome, ',
      '0 Ativo',
      'FROM ' + TMeuDB + '.enticliint eci',
      'LEFT JOIN ' + TMeuDB + '.entidades ent ON ent.Codigo=eci.CodEnti'
      , 'WHERE eci.TipoTabLct=1'
      ]);
      DModG.Tb_Empresas.Open;
      //
      MyObjects.Informa2(LaAviso1, LaAviso2, False, '...');
      //
      RecriaDockForms();
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

end.
