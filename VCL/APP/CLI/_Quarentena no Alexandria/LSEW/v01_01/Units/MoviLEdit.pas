unit MoviLEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, UnInternalConsts, Db, MyDBCheck,
  mySQLDbTables, DBCtrls, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UMySQLModule, dmkLabel, ABSMain, Grids, DBGrids, dmkDBGrid, frxClass,
  frxDBSet, Mask, dmkDBEdit, dmkEditDateTimePicker, jpeg, dmkMemo, dmkGeral,
  Variants, UrlMon, Menus, Shellapi, UnDmkEnums;

type
  TFmMoviLEdit = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    PnDados: TPanel;
    QrCliente: TmySQLQuery;
    QrClienteCodigo: TIntegerField;
    QrClienteNOMEENTI: TWideStringField;
    DsCliente: TDataSource;
    LaTipo: TdmkLabel;
    PnTopo: TPanel;
    Panel8: TPanel;
    Panel3: TPanel;
    Label1: TLabel;
    SBCliente: TSpeedButton;
    Label26: TLabel;
    Label84: TLabel;
    Label85: TLabel;
    Label10: TLabel;
    Label3: TLabel;
    EdCliente: TdmkEditCB;
    CBCliente: TdmkDBLookupComboBox;
    EdListaPre: TdmkEditCB;
    CBListaPre: TdmkDBLookupComboBox;
    PnInfoCli: TPanel;
    Panel14: TPanel;
    Label2: TLabel;
    Panel15: TPanel;
    Panel19: TPanel;
    Label9: TLabel;
    dmkDBEdit5: TdmkDBEdit;
    Label6: TLabel;
    dmkDBEdit4: TdmkDBEdit;
    Label5: TLabel;
    dmkDBEdit3: TdmkDBEdit;
    Label4: TLabel;
    QrGraCusPrc: TmySQLQuery;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    DsGraCusPrc: TDataSource;
    DsHist: TDataSource;
    QrHist: TmySQLQuery;
    QrHistDATAREAL_TXT: TWideStringField;
    QrHistPOSIt: TFloatField;
    QrHistTotal: TFloatField;
    QrHistFrete: TFloatField;
    QrHistDataReal: TDateField;
    QrHistNOMECAD2: TWideStringField;
    QrHistNOMECAD: TWideStringField;
    QrHistUserCad: TIntegerField;
    QrHistCodigo: TIntegerField;
    QrSenhas: TmySQLQuery;
    QrSenhasLogin: TWideStringField;
    QrSenhasNumero: TIntegerField;
    QrSenhasSenha: TWideStringField;
    QrSenhasPerfil: TIntegerField;
    QrSenhasLk: TIntegerField;
    DsSenhas: TDataSource;
    DsGradesCors: TDataSource;
    QrGradesCors: TmySQLQuery;
    QrGradesCorsNOMECOR: TWideStringField;
    QrGradesCorsCodigo: TIntegerField;
    QrGradesCorsControle: TIntegerField;
    QrGradesCorsCor: TIntegerField;
    QrGradesCorsLk: TIntegerField;
    QrGradesCorsDataCad: TDateField;
    QrGradesCorsDataAlt: TDateField;
    QrGradesCorsUserCad: TIntegerField;
    QrGradesCorsUserAlt: TIntegerField;
    QrGradesCorsAlterWeb: TSmallintField;
    QrGradesCorsAtivo: TSmallintField;
    DsGradesTams: TDataSource;
    QrGradesTams: TmySQLQuery;
    QrGradesTamsCodigo: TIntegerField;
    QrGradesTamsControle: TIntegerField;
    QrGradesTamsTam: TIntegerField;
    QrGradesTamsLk: TIntegerField;
    QrGradesTamsDataCad: TDateField;
    QrGradesTamsDataAlt: TDateField;
    QrGradesTamsUserCad: TIntegerField;
    QrGradesTamsUserAlt: TIntegerField;
    QrGradesTamsNOMETAM: TWideStringField;
    DsGrades: TDataSource;
    QrGrades: TmySQLQuery;
    QrGradesCodigo: TIntegerField;
    QrGradesNome: TWideStringField;
    QrGradesCodUsu: TIntegerField;
    QrLoc: TmySQLQuery;
    EdCodigo: TdmkEdit;
    EdDataPed: TdmkEdit;
    EdNomeCad: TdmkEdit;
    EdNomeAlt: TdmkEdit;
    EdDataVenda: TdmkEdit;
    PnBottom: TPanel;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    BtConfPedi: TBitBtn;
    PnVdaBot: TPanel;
    Label21: TLabel;
    Label24: TLabel;
    Label25: TLabel;
    Label22: TLabel;
    Label18: TLabel;
    LaTotal: TLabel;
    Label23: TLabel;
    EdObs: TdmkEdit;
    EdSubTotal: TdmkEdit;
    EdFrete: TdmkEdit;
    EdDesc: TdmkEdit;
    EdPago: TdmkEdit;
    PCProd: TPageControl;
    TabSheet1: TTabSheet;
    Panel11: TPanel;
    Label12: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    Label17: TLabel;
    EdProd: TdmkEditCB;
    CBProd: TdmkDBLookupComboBox;
    EdProdQtd: TdmkEdit;
    EdProdTot: TdmkEdit;
    BtConfItem: TBitBtn;
    EdCor: TdmkEditCB;
    CBCor: TdmkDBLookupComboBox;
    EdTam: TdmkEditCB;
    CBTam: TdmkDBLookupComboBox;
    EdProdVal: TdmkEdit;
    EdEstQ: TdmkEdit;
    EdLeituraCodigoDeBarras: TEdit;
    CkFixo: TCheckBox;
    Panel5: TPanel;
    StaticText1: TStaticText;
    DBGridProd: TdmkDBGrid;
    Panel6: TPanel;
    Panel9: TPanel;
    Image2: TImage;
    DsItem: TDataSource;
    QrItem: TmySQLQuery;
    QrItemCodigo: TIntegerField;
    QrItemCor: TIntegerField;
    QrItemTam: TIntegerField;
    QrItemCodUsu: TIntegerField;
    Label7: TLabel;
    CBGradeD: TdmkDBLookupComboBox;
    EdGradeD: TdmkEditCB;
    QrGradeD: TmySQLQuery;
    QrGradeDCodigo: TIntegerField;
    QrGradeDNome: TWideStringField;
    QrGradeDLk: TIntegerField;
    QrGradeDDataCad: TDateField;
    QrGradeDDataAlt: TDateField;
    QrGradeDUserCad: TIntegerField;
    QrGradeDUserAlt: TIntegerField;
    QrGradeDAlterWeb: TSmallintField;
    QrGradeDAtivo: TSmallintField;
    DsGradeD: TDataSource;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SBClienteClick(Sender: TObject);
    procedure EdClienteChange(Sender: TObject);
    procedure QrHistCalcFields(DataSet: TDataSet);
    procedure CBListaPreExit(Sender: TObject);
    procedure QrGradesAfterScroll(DataSet: TDataSet);
    procedure QrGradesBeforeClose(DataSet: TDataSet);
    procedure EdProdChange(Sender: TObject);
    procedure EdCorChange(Sender: TObject);
    procedure EdTamChange(Sender: TObject);
    procedure EdProdQtdChange(Sender: TObject);
    procedure CBProdExit(Sender: TObject);
    procedure CkFixoClick(Sender: TObject);
    procedure BtConfItemClick(Sender: TObject);
    procedure EdSubTotalChange(Sender: TObject);
    procedure DBGridProdKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtConfPediClick(Sender: TObject);
    procedure EdDescExit(Sender: TObject);
    procedure DBGridProdCellClick(Column: TColumn);
    procedure EdLeituraCodigoDeBarrasChange(Sender: TObject);
    procedure EdLeituraCodigoDeBarrasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure EdGradeDChange(Sender: TObject);
  private
    { Private declarations }
    FTamAll: Boolean;
    FGraGruX, FSequencia: Integer;
    procedure MostraEdicao(Mostra : Integer; Status : String; Codigo : Integer);
    procedure ReopenHist(Cliente: Integer);
    procedure ReopenGrades(Depto: Integer);
    procedure ReopenGradesTams(Codigo: Integer);
    procedure ReopenGradesCors(Codigo: Integer);
    procedure VerificaProduto(Prod, Cor, Tam: Integer);
    procedure AtualizaTotVenda(Codigo, Controle: Integer);
    procedure TotalVenda();
    procedure InsereItem();
    function ReopenItem(): Boolean;
  public
    { Public declarations }
    FCodigo, FControle, FConta, FMoviL, FMovim: Integer;
  end;

  var
  FmMoviLEdit: TFmMoviLEdit;

implementation

{$R *.DFM}

uses Module, MyListas, StatusV, Entidade2, VendaPesq, UnGOTOy, ModuleProd,
  ModuleGeral, UCreate, UnInternalConsts3, MoviL, Principal, UnMyObjects;

const
  FTamMax = 13; // EAN13
  FTamPrd = 12; // tudo � codigo do produto menos o d�gito (n�o tem lote e etc.)
  FTamMin = 1; // Digito verificador

procedure TFmMoviLEdit.AtualizaTotVenda(Codigo, Controle: Integer);
begin
  FmMoviL.AtualizaTotVenda(Codigo, Controle);
  //
  EdSubTotal.ValueVariant := FmMoviL.QrMoviLTotal.Value;
  EdPago.ValueVariant     := FmMoviL.QrMoviLPago.Value;
  LaTotal.Caption         := Geral.TFT(EdSubTotal.ValueVariant + EdFrete.ValueVariant
    - EdDesc.ValueVariant, 2, siNegativo);
end;

procedure TFmMoviLEdit.BtConfItemClick(Sender: TObject);
begin
  InsereItem();
end;

procedure TFmMoviLEdit.BtConfPediClick(Sender: TObject);
var
  GradeD, Codigo, Controle, Cliente, GraCusPrc: Integer;
  DataPed: String;
begin
  Cliente   := EdCliente.ValueVariant;
  GraCusPrc := EdListaPre.ValueVariant;
  DataPed   := Geral.FDT(EdDataPed.ValueVariant, 1);
  GradeD    := Dmod.QrControleLinDepto.Value;
  //
  if GradeD = 0 then
  begin
    Application.MessageBox('Departamento padr�o n�o definido nas op��es do aplicativo!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Cliente = 0 then
  begin
    Application.MessageBox('Cliente n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdCliente.SetFocus;
    Exit;
  end;
  if GraCusPrc = 0 then
  begin
    Application.MessageBox('Lista de pre�o n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdListaPre.SetFocus;
    Exit;
  end;
  if LaTipo.SQLType = stIns then
  begin
    Codigo   := UMyMod.BuscaEmLivreY_Def('movil', 'Codigo', LaTipo.SQLType, FCodigo);
    Controle := Dmod.BuscaProximoMovix;
  end else
  begin
    Codigo   := FmMoviL.QrMoviLCodigo.Value;
    Controle := FmMoviL.QrMoviLControle.Value;
  end;
  //
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'movil', False,
  [
    'Cliente', 'GraCusPrc', 'Frete', 'Descon',
    'Observ', 'DataPedi', 'GradeD', 'Controle'
  ], ['Codigo'],
  [
    Cliente, GraCusPrc, EdFrete.ValueVariant, EdDesc.ValueVariant,
    EdObs.ValueVariant, DataPed, GradeD, Controle
  ], [Codigo]) then
  begin
    FmMoviL.LocCod(Codigo, Codigo);
    if LaTipo.SQLType = stIns then
    begin
      MostraEdicao(0, CO_ALTERACAO, 0);
      LaTipo.SQLType := stUpd;
      EdProd.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmMoviLEdit.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMoviLEdit.CBListaPreExit(Sender: TObject);
var
  ListaPre: Integer;
begin
  ListaPre := EdListaPre.ValueVariant;
  //
  if ListaPre > 0 then
  begin
    EdListaPre.Enabled := False;
    CBListaPre.Enabled := False;
  end else
  begin
    EdListaPre.Enabled := True;
    CBListaPre.Enabled := True;
  end;
end;

procedure TFmMoviLEdit.CBProdExit(Sender: TObject);
begin
  if CBProd.KeyValue < 1 then
    EdObs.SetFocus;
end;

procedure TFmMoviLEdit.CkFixoClick(Sender: TObject);
begin
  if CkFixo.Checked then
  begin
    EdProdQtd.Enabled      := False;
    EdProdQtd.ValueVariant := 1;
  end else
  begin
    EdProdQtd.Enabled      := True;
    EdProdQtd.ValueVariant := 0;
  end;
end;

procedure TFmMoviLEdit.DBGridProdCellClick(Column: TColumn);
var
  Produto: String;
begin
  FmMoviL.QrLocFoto.Close;
  FmMoviL.QrLocFoto.Params[0].AsInteger := FmMoviL.QrMovimGrade.Value;
  FmMoviL.QrLocFoto.Params[1].AsInteger := FmMoviL.QrMovimCor.Value;
  FmMoviL.QrLocFoto.Params[2].AsInteger := FmMoviL.QrMovimTam.Value;
  FmMoviL.QrLocFoto.Open;
  if FmMoviL.QrLocFoto.RecordCount > 0 then
    Produto := FmMoviL.QrLocFotoNOMEFOTO.Value
  else
    Produto := '';
  FmPrincipal.CarregaFoto(Image2, Produto);
end;

procedure TFmMoviLEdit.DBGridProdKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Codigo, Controle, Conta: Integer;
begin
  // Excluir
  if (Key = VK_DELETE) and (ssCtrl in Shift) then
  begin
    if Geral.MB_Pergunta('Confirma a exclus�o do registro?') = ID_YES then
    begin
      Codigo   := FmMoviL.QrMoviLCodigo.Value;
      Controle := FmMoviL.QrMoviLControle.Value;
      Conta    := FmMoviL.QrMovimConta.Value;
      //
      if Conta > 0 then
      begin
        Dmod.QrUpd.SQL.Clear;
        DMod.QrUpd.SQL.Add('DELETE FROM movim WHERE Conta=:P0');
        Dmod.QrUpd.Params[00].AsInteger := Conta;
        Dmod.QrUpd.ExecSQL;
        //
        FmMoviL.ReopenMovim(0);
      end;
      AtualizaTotVenda(Codigo, Controle);
    end;
  end;
end;

procedure TFmMoviLEdit.EdClienteChange(Sender: TObject);
var
  Cliente: Integer;
begin
  Cliente := EdCliente.ValueVariant;
  //
  if Cliente > 0 then
    ReopenHist(Cliente);
end;

procedure TFmMoviLEdit.EdCorChange(Sender: TObject);
var
  Prod, Cor, Tam: Integer;
begin
  if EdCor.ValueVariant > 0 then
  begin
    Prod := QrGradesCodigo.Value;
    Cor  := QrGradesCorsCor.Value;
    Tam  := QrGradesTamsTam.Value;
    //
    VerificaProduto(Prod, Cor, Tam);
  end;
end;

procedure TFmMoviLEdit.EdDescExit(Sender: TObject);
begin
  BtConfPedi.SetFocus;
end;

procedure TFmMoviLEdit.EdGradeDChange(Sender: TObject);
begin
  ReopenGrades(EdGradeD.ValueVariant);
end;

procedure TFmMoviLEdit.EdLeituraCodigoDeBarrasChange(Sender: TObject);
var
  Tam: Integer;
begin
  Tam := Length(EdLeituraCodigoDeBarras.Text);
  if Tam = FTamMax then
  begin
    FGraGruX   := Geral.IMV(Copy(EdLeituraCodigoDeBarras.Text, 07, 6));
    FSequencia := Geral.IMV(Copy(EdLeituraCodigoDeBarras.Text, 13, 8));
    if ReopenItem() then
    begin
      if CkFixo.Checked then
      begin
        FTamAll := True;
        InsereItem();
        EdLeituraCodigoDeBarras.SetFocus;
        EdLeituraCodigoDeBarras.Text := '';
      end else EdProdQtd.SetFocus;
    end;
  end else if Tam = 0 then QrItem.Close;
end;

procedure TFmMoviLEdit.EdLeituraCodigoDeBarrasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Txt: String;
  Tam: Integer;
begin
  if Key = VK_RETURN then
  begin
    if  (Length(EdLeituraCodigoDeBarras.Text) <= FTamPrd)
    and (Length(EdLeituraCodigoDeBarras.Text) >  FTamMin) then
    begin
      Txt := EdLeituraCodigoDeBarras.Text;
      Tam := Length(Txt);
      Txt := Copy(Txt, 1, Tam-1);
      FGraGruX := Geral.IMV(Txt);
      if ReopenItem() then
      begin
        if CkFixo.Checked then
        begin
          FTamAll := True;
          InsereItem();
          EdLeituraCodigoDeBarras.SetFocus;
          EdLeituraCodigoDeBarras.Text := '';
        end else EdProdQtd.SetFocus;
      end;
    end;
  end;  
end;

procedure TFmMoviLEdit.EdProdChange(Sender: TObject);
var
  Qtd: Double;
  Grade, Cor, Tam: Integer;
begin
  Qtd   := EdProdQtd.ValueVariant;
  Grade := EdProd.ValueVariant;
  Cor   := EdCor.ValueVariant;
  Tam   := EdTam.ValueVariant;
  //
  if (Qtd > 0) and (Grade >0) and (Cor > 0) and (Tam > 0) then
    VerificaProduto(QrGradesCodigo.Value, QrGradesCorsCor.Value, QrGradesTamsTam.Value);
end;

procedure TFmMoviLEdit.EdProdQtdChange(Sender: TObject);
var
  Qtd: Double;
  Grade, Cor, Tam: Integer;
begin
  Qtd   := EdProdQtd.ValueVariant;
  Grade := EdProd.ValueVariant;
  Cor   := EdCor.ValueVariant;
  Tam   := EdTam.ValueVariant;
  //
  if (Qtd > 0) and (Grade >0) and (Cor > 0) and (Tam > 0) then
    VerificaProduto(QrGradesCodigo.Value, QrGradesCorsCor.Value, QrGradesTamsTam.Value);
end;

procedure TFmMoviLEdit.EdSubTotalChange(Sender: TObject);
begin
  TotalVenda;
end;

procedure TFmMoviLEdit.EdTamChange(Sender: TObject);
var
  Prod, Cor, Tam: Integer;
begin
  if EdTam.ValueVariant > 0 then
  begin
    Prod := QrGradesCodigo.Value;
    Cor  := QrGradesCorsCor.Value;
    Tam  := QrGradesTamsTam.Value;
    //
    VerificaProduto(Prod, Cor, Tam);
  end;
end;

procedure TFmMoviLEdit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  MostraEdicao(0, LaTipo.Caption, 0);
end;

procedure TFmMoviLEdit.FormCreate(Sender: TObject);
var
  Depto: Integer;
begin
  EdGradeD.ValueVariant := Dmod.QrControleLinDepto.Value;
  CBGradeD.KeyValue     := Dmod.QrControleLinDepto.Value;
  //
  Depto := EdGradeD.ValueVariant;
  //
  QrCliente.Open;
  QrGraCusPrc.Open;
  QrSenhas.Open;
  QrGradeD.Open;
  //
  CkFixo.Checked := True;
  //
  if Depto > 0 then
    ReopenGrades(Depto);
end;

procedure TFmMoviLEdit.InsereItem;
var
  ListaPre, Prod, Cor, Tam, Codigo, Controle, Conta: Integer;
  Total, Qtd: Double;
  Datapedi: String;
begin
  ListaPre := EdListaPre.ValueVariant;
  Prod     := QrGradesCodigo.Value;
  Cor      := EdCor.ValueVariant;
  Tam      := EdTam.ValueVariant;
  Qtd      := EdProdQtd.ValueVariant;
  Datapedi := Geral.FDT(EdDataPed.ValueVariant, 1);
  Total    := EdProdTot.ValueVariant;
  //
  Dmod.QrControle.Close;
  Dmod.QrControle.Open;
  //
  if ListaPre = 0 then
  begin
    Application.MessageBox('Lista de pre�o n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    MostraEdicao(1, CO_INCLUSAO, 0);
    CBListaPre.SetFocus;
    Exit;
  end;
  if Prod = 0 then
  begin
    Application.MessageBox('Produto n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBProd.SetFocus;
    Exit;
  end;
  if Cor = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBCor.SetFocus;
    Exit;
  end;
  if Tam = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBTam.SetFocus;
    Exit;
  end;
  if Qtd = 0 then
  begin
    Application.MessageBox('Quantidade n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdProdQtd.SetFocus;
    Exit;
  end;
  if Total = 0 then
  begin
    Application.MessageBox('O total do produto deve ser superior a 0!!', 'Aviso', MB_OK+MB_ICONWARNING);
    MostraEdicao(1, CO_INCLUSAO, 0);
    Exit;
  end;
  case Dmod.QrControleFatSEtqLin.Value of
    0: //N�o inclui se n�o tiver estoque
    begin
      if EdEstQ.ValueVariant <= 0 then
      begin
        Geral.MensagemBox('Inclus�o abortada!' + sLineBreak +
          'Motivo: Este item n�o possui estoque', 'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
    end;
    2: //Perguntar antes se n�o tiver estoque
    begin
      if Geral.MensagemBox('Este item n�o possui estoque!' + sLineBreak +
        'Deseja inclu�-lo mesmo assim?', 'Pergunta',
        MB_ICONQUESTION+MB_YESNOCANCEL) <> ID_YES
      then
        Exit;
    end;
  end;

  Conta    := UMyMod.BuscaEmLivreY_Def('movim', 'Conta', stIns, FConta);
  Controle := FmMoviL.QrMoviLControle.Value;
  Codigo   := FmMoviL.QrMoviLCodigo.Value;

  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_INCLUSAO, 'movim', False,
  [
    'Motivo', 'Qtd', 'Val', 'Ven', 'Grade',
    'Cor', 'Tam', 'DataPedi', 'Controle'
  ], ['Conta'],
  [
    21, - Qtd, - EdProdVal.ValueVariant * Qtd, EdProdTot.ValueVariant, QrGradesCodigo.Value,
    Cor, Tam, Datapedi, Controle
  ], [Conta]) then
  begin
    AtualizaTotVenda(Codigo, Controle);
    //MostraEdicao(1, CO_INCLUSAO, 0); N�o zerar os campos
    EdProd.SetFocus;
  end;
end;

procedure TFmMoviLEdit.MostraEdicao(Mostra: Integer; Status: String;
  Codigo: Integer);
var
  Total: String;
begin
  case Mostra of
    0: //Cabe�alho da venda
    begin
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.ValueVariant    := 0;
        EdDataPed.ValueVariant   := Date;
        EdDataVenda.ValueVariant := '';
        EdCliente.ValueVariant   := 0;
        CBCliente.KeyValue       := Null;
        EdListaPre.ValueVariant  := Dmod.QrControleLinGraPrc.Value;
        CBListaPre.KeyValue      := Dmod.QrControleLinGraPrc.Value;
        EdSubTotal.ValueVariant  := 0;
        EdFrete.ValueVariant     := 0;
        EdObs.ValueVariant       := '';
        EdDesc.ValueVariant      := 0;        
        EdPago.ValueVariant      := 0;
        LaTotal.Caption          := Geral.TFT('', 2, siNegativo);
        EdNomeCad.ValueVariant   := '';
        EdNomeAlt.ValueVariant   := '';
        //
        EdListaPre.Enabled       := True;
        CBListaPre.Enabled       := True;
        PnVdaBot.Visible         := False;
        PnDados.Visible          := False;
        PnBottom.Align           := alTop;
      end else
      begin
        EdCodigo.ValueVariant    := FmMoviL.QrMoviLCodigo.Value;
        EdDataPed.ValueVariant   := FmMoviL.QrMoviLDataPedi.Value;
        EdDataVenda.ValueVariant := FmMoviL.QrMoviLDATAREAL_TXT.Value;        
        EdCliente.ValueVariant   := FmMoviL.QrMoviLCliente.Value;
        CBCliente.KeyValue       := FmMoviL.QrMoviLCliente.Value;
        EdListaPre.ValueVariant  := FmMoviL.QrMoviLGraCusPrc.Value;
        CBListaPre.KeyValue      := FmMoviL.QrMoviLGraCusPrc.Value;
        EdSubTotal.ValueVariant  := FmMoviL.QrMoviLTotal.Value;
        EdFrete.ValueVariant     := FmMoviL.QrMoviLFrete.Value;
        EdObs.ValueVariant       := FmMoviL.QrMoviLObserv.Value;
        EdDesc.ValueVariant      := FmMoviL.QrMoviLDescon.Value;
        EdPago.ValueVariant      := FmMoviL.QrMoviLPago.Value;
        Total                    := FloatToStr(FmMoviL.QrMoviLTOTALVDA.Value);
        LaTotal.Caption          := Geral.TFT(Total, 2, siNegativo);
        EdNomeCad.ValueVariant   := FmMoviL.QrMoviLNOMECAD2.Value;
        EdNomeAlt.ValueVariant   := FmMoviL.QrMoviLNOMEALT2.Value;
        //
        EdListaPre.Enabled       := False;
        CBListaPre.Enabled       := False;
        PnVdaBot.Visible         := True;
        PnDados.Visible          := True;
        PnBottom.Align           := alBottom;
      end;
      EdCliente.SetFocus;
    end;
    1: //Itens da venda
    begin
      if Status = CO_INCLUSAO then
      begin
        EdProd.ValueVariant          := 0;
        CBProd.KeyValue              := Null;
        EdCor.ValueVariant           := 0;
        CBCor.KeyValue               := Null;
        EdTam.ValueVariant           := 0;
        CBTam.KeyValue               := Null;
        if not CkFixo.Checked then
          EdProdQtd.ValueVariant       := 0;
        EdProdTot.ValueVariant       := 0;
        EdEstQ.ValueVariant          := 0;
        EdProdVal.ValueVariant       := 0;
      end;
      EdProd.SetFocus;
    end;
  end;
end;

procedure TFmMoviLEdit.QrGradesAfterScroll(DataSet: TDataSet);
begin
  ReopenGradesCors(QrGradesCodigo.Value);
  ReopenGradesTams(QrGradesCodigo.Value);
end;

procedure TFmMoviLEdit.QrGradesBeforeClose(DataSet: TDataSet);
begin
  QrGradesTams.Close;
  QrGradesCors.Close;
end;

procedure TFmMoviLEdit.QrHistCalcFields(DataSet: TDataSet);
begin
  QrHistDATAREAL_TXT.Value := Geral.FDT(QrHistDataReal.Value, 2);
  QrHistPOSIt.Value        := QrHistTotal.Value + QrHistFrete.Value;
  case QrHistUserCad.Value of
    -2: QrHistNOMECAD2.Value := 'BOSS [Administrador]';
    -1: QrHistNOMECAD2.Value := 'MASTER [Admin. DERMATEK]';
     0: QrHistNOMECAD2.Value := 'N�o definido';
    else QrHistNOMECAD2.Value := QrHistNOMECAD.Value;
  end;
end;

procedure TFmMoviLEdit.SBClienteClick(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  //
  DModG.CadastroDeEntidade(EdCliente.ValueVariant, fmcadEntidade2, fmcadEntidade2);  
  //
  if VAR_ENTIDADE <> 0 then
  begin
    QrCliente.Close;
    QrCliente.Open;
    //
    EdCliente.ValueVariant := VAR_ENTIDADE;
    CBCliente.KeyValue     := VAR_ENTIDADE;
  end;
end;

procedure TFmMoviLEdit.TotalVenda;
var
  Total, Frete, Desconto, SubTotal: Double;
begin
  Frete    := EdFrete.ValueVariant;
  Desconto := EdDesc.ValueVariant;
  SubTotal := EdSubTotal.ValueVariant;
  Total    := SubTotal + Frete - Desconto;
  //
  LaTotal.Caption := FloatToStr(Total);
  LaTotal.Caption := Geral.TFT(LaTotal.Caption, 2, siNegativo);
end;

procedure TFmMoviLEdit.ReopenHist(Cliente: Integer);
begin
  QrHist.Close;
  QrHist.Params[0].AsInteger := Cliente;
  QrHist.Open;
end;

procedure TFmMoviLEdit.ReopenGrades(Depto: Integer);
begin
  QrGrades.Close;
  QrGrades.Params[0].AsInteger := Depto;
  QrGrades.Open;
end;

procedure TFmMoviLEdit.ReopenGradesCors(Codigo: Integer);
begin
  QrGradesCors.Close;
  QrGradesCors.Params[0].AsInteger := Codigo;
  QrGradesCors.Open;
end;

procedure TFmMoviLEdit.ReopenGradesTams(Codigo: Integer);
begin
  QrGradesTams.Close;
  QrGradesTams.Params[0].AsInteger := Codigo;
  QrGradesTams.Open;
end;

procedure TFmMoviLEdit.VerificaProduto(Prod, Cor, Tam: Integer);
var
  Produto: String;
begin
  if (Prod > 0) and (Cor > 0) and (Tam > 0) then
  begin
    QrLoc.Close;
    QrLoc.Database := Dmod.MyDB;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT Controle, EstqQ, Codigo, Cor, Tam');
    QrLoc.SQL.Add('FROM produtos');
    QrLoc.SQL.Add('WHERE Codigo=:PO');
    QrLoc.SQL.Add('AND Cor=:P1');
    QrLoc.SQL.Add('AND Tam=:P2');
    QrLoc.Params[0].AsInteger := Prod;
    QrLoc.Params[1].AsInteger := Cor;
    QrLoc.Params[2].AsInteger := Tam;
    QrLoc.Open;
    //
    if QrLoc.RecordCount > 0 then
    begin
      EdProdVal.ValueVariant :=  DmProd.ObtemPrecoProduto(EdListaPre.ValueVariant, QrGradesCodigo.Value,
        QrLoc.FieldByName('Controle').Value);
      EdEstQ.ValueVariant    := QrLoc.FieldByName('EstqQ').Value;
      if EdProdVal.ValueVariant > 0 then
        EdProdTot.ValueVariant := EdProdVal.ValueVariant * EdProdQtd.ValueVariant
      else
        EdProdTot.ValueVariant := 0;
        FmMoviL.QrLocFoto.Close;
        FmMoviL.QrLocFoto.Params[0].AsInteger := QrLoc.FieldByName('Codigo').Value;
        FmMoviL.QrLocFoto.Params[1].AsInteger := QrLoc.FieldByName('Cor').Value;
        FmMoviL.QrLocFoto.Params[2].AsInteger := QrLoc.FieldByName('Tam').Value;
        FmMoviL.QrLocFoto.Open;
        if FmMoviL.QrLocFoto.RecordCount > 0 then
          Produto := FmMoviL.QrLocFotoNOMEFOTO.Value
        else
          Produto := '';
        FmPrincipal.CarregaFoto(Image2, Produto);
    end;
  end;
end;

function TFmMoviLEdit.ReopenItem(): Boolean;
begin
  Result := False;
  QrItem.Close;
  if FGraGruX > 0 then
  begin
    QrItem.Params[00].AsInteger := FGraGrux;
    QrItem.Open;
    //
    if QrItem.RecordCount > 0 then
    begin
      EdProd.ValueVariant := QrItemCodUsu.Value;
      CBProd.KeyValue     := QrItemCodUsu.Value;
      EdCor.ValueVariant  := QrItemCor.Value;
      CBCor.KeyValue      := QrItemCor.Value;
      EdTam.ValueVariant  := QrItemTam.Value;
      CBTam.KeyValue      := QrItemTam.Value;
      //
      Result := True;
    end else
      Application.MessageBox('Reduzido n�o localizado!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  end;
end;

end.



