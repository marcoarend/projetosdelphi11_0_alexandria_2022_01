object FmMoviV: TFmMoviV
  Left = 354
  Top = 274
  Caption = 'PRD-VENDA-001 :: Vendas de Mercadorias'
  ClientHeight = 684
  ClientWidth = 944
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelControle: TPanel
    Left = 0
    Top = 636
    Width = 944
    Height = 48
    Align = alBottom
    TabOrder = 0
    object LaRegistro: TdmkLabel
      Left = 173
      Top = 1
      Width = 301
      Height = 46
      Align = alClient
      Caption = '[N]: 0'
      UpdType = utYes
      SQLType = stNil
      ExplicitWidth = 26
      ExplicitHeight = 13
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 172
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'ltimo registro'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Pr'#243'ximo registro'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Registro anterior'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Primeiro registro'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
    end
    object Panel3: TPanel
      Left = 474
      Top = 1
      Width = 469
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 372
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
      object BtPagtos: TBitBtn
        Tag = 187
        Left = 96
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Pagtos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtPagtosClick
      end
      object BtVenda: TBitBtn
        Tag = 303
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Venda'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtVendaClick
      end
      object BtRast: TBitBtn
        Left = 189
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Rast.'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = BtRastClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 944
    Height = 48
    Align = alTop
    Caption = 'Venda de mercadorias'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TLabel
      Left = 865
      Top = 1
      Width = 78
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 641
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 48
      Top = 1
      Width = 817
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 51
      ExplicitTop = -2
    end
    object PainelBotoes: TPanel
      Left = 1
      Top = 1
      Width = 47
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 2
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 944
    Height = 588
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object PnTopo: TPanel
      Left = 1
      Top = 1
      Width = 942
      Height = 110
      Align = alTop
      TabOrder = 0
      object Panel8: TPanel
        Left = 1
        Top = 57
        Width = 940
        Height = 51
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Panel19: TPanel
          Left = 0
          Top = 0
          Width = 325
          Height = 51
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label7: TLabel
            Left = 74
            Top = 27
            Width = 64
            Height = 13
            Caption = 'Pesquisa [F4]'
          end
          object Label9: TLabel
            Left = 4
            Top = 6
            Width = 62
            Height = 21
            Caption = 'Pedido:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Berlin Sans FB'
            Font.Style = []
            ParentFont = False
          end
          object DBEdCodigo: TdmkDBEdit
            Left = 74
            Top = 6
            Width = 100
            Height = 21
            DataField = 'Codigo'
            DataSource = DsMoviV
            TabOrder = 0
            OnDblClick = DBEdCodigoDblClick
            OnKeyDown = DBEdCodigoKeyDown
            UpdType = utYes
            Alignment = taLeftJustify
          end
        end
      end
      object Panel1: TPanel
        Left = 1
        Top = 1
        Width = 940
        Height = 56
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label1: TLabel
          Left = 216
          Top = 6
          Width = 35
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cliente:'
        end
        object Label26: TLabel
          Left = 181
          Top = 31
          Width = 70
          Height = 13
          Caption = 'Lista de pre'#231'o:'
        end
        object Label84: TLabel
          Left = 674
          Top = 6
          Width = 63
          Height = 13
          Caption = 'Cadastro por:'
        end
        object Label85: TLabel
          Left = 677
          Top = 31
          Width = 60
          Height = 13
          Caption = 'Alterado por:'
        end
        object Label10: TLabel
          Left = 9
          Top = 6
          Width = 61
          Height = 13
          Caption = 'Data pedido:'
        end
        object Label3: TLabel
          Left = 9
          Top = 31
          Width = 59
          Height = 13
          Caption = 'Data venda:'
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 743
          Top = 3
          Width = 190
          Height = 21
          DataField = 'NOMECAD2'
          DataSource = DsMoviV
          TabOrder = 2
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit2: TdmkDBEdit
          Left = 743
          Top = 28
          Width = 190
          Height = 21
          DataField = 'NOMEALT2'
          DataSource = DsMoviV
          TabOrder = 5
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object EdDBDataVenda: TdmkDBEdit
          Left = 74
          Top = 29
          Width = 100
          Height = 21
          DataField = 'DATAREAL_TXT'
          DataSource = DsMoviV
          TabOrder = 3
          OnDblClick = EdDBDataVendaDblClick
          UpdType = utYes
          Alignment = taCenter
        end
        object EdDBDataPed: TdmkDBEdit
          Left = 74
          Top = 3
          Width = 100
          Height = 21
          DataField = 'DataPedi'
          DataSource = DsMoviV
          TabOrder = 0
          UpdType = utYes
          Alignment = taCenter
        end
        object dmkDBEdit7: TdmkDBEdit
          Left = 256
          Top = 3
          Width = 412
          Height = 21
          DataField = 'NOMEENTI'
          DataSource = DsMoviV
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit8: TdmkDBEdit
          Left = 256
          Top = 28
          Width = 412
          Height = 21
          DataField = 'NOMGRACUSPRC'
          DataSource = DsMoviV
          TabOrder = 4
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
    end
    object PnInfoCli: TPanel
      Left = 1
      Top = 111
      Width = 942
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel14: TPanel
        Left = 0
        Top = 0
        Width = 655
        Height = 52
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label2: TLabel
          Left = 0
          Top = 0
          Width = 655
          Height = 22
          Align = alTop
          Alignment = taCenter
          Caption = 'Informa'#231#245'es sobre o cliente'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Berlin Sans FB, 14'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          ExplicitWidth = 237
        end
        object Label6: TLabel
          Left = 385
          Top = 29
          Width = 75
          Height = 13
          Caption = 'Cadastrado por:'
        end
        object Label5: TLabel
          Left = 183
          Top = 29
          Width = 95
          Height = 13
          Caption = 'Valor '#250'ltima compra:'
        end
        object Label4: TLabel
          Left = 2
          Top = 29
          Width = 73
          Height = 13
          Caption = #218'ltima compra: '
        end
        object dmkDBEdit5: TdmkDBEdit
          Left = 464
          Top = 26
          Width = 190
          Height = 21
          DataField = 'NOMECAD2'
          DataSource = DsHist
          TabOrder = 2
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit4: TdmkDBEdit
          Left = 282
          Top = 26
          Width = 100
          Height = 21
          DataField = 'POSIt'
          DataSource = DsHist
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit3: TdmkDBEdit
          Left = 77
          Top = 26
          Width = 100
          Height = 21
          DataField = 'DATAREAL_TXT'
          DataSource = DsHist
          TabOrder = 0
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
      object Panel15: TPanel
        Left = 655
        Top = 0
        Width = 287
        Height = 52
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object LaDataExp: TLabel
          Left = 78
          Top = 28
          Width = 75
          Height = 16
          Caption = '24/05/2010'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label12: TLabel
          Left = 8
          Top = 29
          Width = 64
          Height = 16
          Caption = 'Expira'#231#227'o:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object BtUltimo: TBitBtn
          Left = 159
          Top = 6
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&'#218'ltima compra'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtUltimoClick
        end
      end
    end
    object PnBottom: TPanel
      Left = 1
      Top = 531
      Width = 942
      Height = 56
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object Label22: TLabel
        Left = 610
        Top = 8
        Width = 49
        Height = 13
        Caption = 'Desconto:'
      end
      object Label25: TLabel
        Left = 470
        Top = 8
        Width = 27
        Height = 13
        Caption = 'Frete:'
      end
      object Label24: TLabel
        Left = 7
        Top = 32
        Width = 25
        Height = 13
        Caption = 'Obs.:'
      end
      object Label21: TLabel
        Left = 316
        Top = 8
        Width = 42
        Height = 13
        Caption = 'Subtotal:'
      end
      object Label18: TLabel
        Left = 634
        Top = 33
        Width = 28
        Height = 13
        Caption = 'Pago:'
      end
      object LaRastrea: TLabel
        Left = 7
        Top = 8
        Width = 83
        Height = 13
        Cursor = crHandPoint
        Caption = 'Rastreamento:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        OnClick = LaRastreaClick
      end
      object Panel4: TPanel
        Left = 764
        Top = 1
        Width = 177
        Height = 54
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 5
        object DBText1: TDBText
          Left = 71
          Top = 16
          Width = 100
          Height = 22
          Alignment = taRightJustify
          DataField = 'TOTALVDA'
          DataSource = DsMoviV
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label23: TLabel
          Left = 2
          Top = 16
          Width = 62
          Height = 21
          Caption = 'TOTAL:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Berlin Sans FB'
          Font.Style = []
          ParentFont = False
        end
      end
      object dmkDBEdit11: TdmkDBEdit
        Left = 364
        Top = 4
        Width = 100
        Height = 21
        DataField = 'Total'
        DataSource = DsMoviV
        TabOrder = 0
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit6: TdmkDBEdit
        Left = 665
        Top = 29
        Width = 100
        Height = 21
        DataField = 'Pago'
        DataSource = DsMoviV
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit9: TdmkDBEdit
        Left = 501
        Top = 4
        Width = 100
        Height = 21
        DataField = 'Frete'
        DataSource = DsMoviV
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit10: TdmkDBEdit
        Left = 665
        Top = 4
        Width = 100
        Height = 21
        DataField = 'Descon'
        DataSource = DsMoviV
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit12: TdmkDBEdit
        Left = 38
        Top = 29
        Width = 589
        Height = 21
        DataField = 'Observ'
        DataSource = DsMoviV
        TabOrder = 4
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit13: TdmkDBEdit
        Left = 95
        Top = 4
        Width = 215
        Height = 21
        DataField = 'CodRast'
        DataSource = DsMoviV
        TabOrder = 6
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object PCProd: TPageControl
      Left = 1
      Top = 163
      Width = 942
      Height = 368
      ActivePage = TabSheet2
      Align = alClient
      TabOrder = 3
      object TabSheet1: TTabSheet
        Caption = 'Mercadorias'
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 684
          Height = 340
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object DBGridProd: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 684
            Height = 340
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMEGRA'
                Title.Caption = 'Mercadoria'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECOR'
                Title.Caption = 'Cor'
                Width = 130
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETAM'
                Title.Caption = 'Tam'
                Width = 90
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIq'
                Title.Caption = 'Qtde.'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRECO'
                Title.Caption = 'Venda'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIv'
                Title.Caption = 'TOTAL'
                Width = 90
                Visible = True
              end>
            Color = clWindow
            DataSource = DsMovim
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMEGRA'
                Title.Caption = 'Mercadoria'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECOR'
                Title.Caption = 'Cor'
                Width = 130
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETAM'
                Title.Caption = 'Tam'
                Width = 90
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIq'
                Title.Caption = 'Qtde.'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRECO'
                Title.Caption = 'Venda'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIv'
                Title.Caption = 'TOTAL'
                Width = 90
                Visible = True
              end>
          end
        end
        object Panel6: TPanel
          Left = 684
          Top = 0
          Width = 250
          Height = 340
          Align = alRight
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 250
            Height = 182
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Image2: TImage
              Left = 4
              Top = 0
              Width = 240
              Height = 170
              Stretch = True
            end
          end
          object Panel7: TPanel
            Left = 0
            Top = 182
            Width = 250
            Height = 158
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Label17: TLabel
              Left = 0
              Top = 0
              Width = 250
              Height = 15
              Align = alTop
              Alignment = taCenter
              Caption = 'Status do pedido'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Berlin Sans FB'
              Font.Style = []
              ParentFont = False
              ExplicitWidth = 88
            end
            object DBText2: TDBText
              Left = 0
              Top = 15
              Width = 250
              Height = 25
              Align = alTop
              Alignment = taCenter
              DataField = 'NOMESTATAT'
              DataSource = DsMoviV
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -21
              Font.Name = 'Berlin Sans FB'
              Font.Style = []
              ParentFont = False
              ExplicitTop = 13
            end
            object Label19: TLabel
              Left = 0
              Top = 40
              Width = 250
              Height = 13
              Align = alTop
              Alignment = taCenter
              Caption = 'Hist'#243'rico:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ExplicitWidth = 44
            end
            object DBLookupListBox2: TDBLookupListBox
              Left = 0
              Top = 53
              Width = 250
              Height = 95
              Align = alClient
              Enabled = False
              KeyField = 'Controle'
              ListField = 'STATUS_STR'
              ListSource = DsMoviVStat
              TabOrder = 0
            end
          end
        end
      end
      object TabSheet2: TTabSheet
        Caption = 'Kits'
        ImageIndex = 1
        object Panel10: TPanel
          Left = 684
          Top = 0
          Width = 250
          Height = 340
          Align = alRight
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 0
          object Panel11: TPanel
            Left = 0
            Top = 0
            Width = 250
            Height = 182
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Image3: TImage
              Left = 4
              Top = 0
              Width = 240
              Height = 170
              Stretch = True
            end
          end
          object Panel12: TPanel
            Left = 0
            Top = 182
            Width = 250
            Height = 158
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object Label8: TLabel
              Left = 0
              Top = 0
              Width = 250
              Height = 15
              Align = alTop
              Alignment = taCenter
              Caption = 'Status do pedido'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -13
              Font.Name = 'Berlin Sans FB'
              Font.Style = []
              ParentFont = False
              ExplicitWidth = 88
            end
            object DBText3: TDBText
              Left = 0
              Top = 15
              Width = 250
              Height = 25
              Align = alTop
              Alignment = taCenter
              DataField = 'NOMESTATAT'
              DataSource = DsMoviV
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -21
              Font.Name = 'Berlin Sans FB'
              Font.Style = []
              ParentFont = False
              ExplicitTop = 13
            end
            object Label11: TLabel
              Left = 0
              Top = 40
              Width = 250
              Height = 13
              Align = alTop
              Alignment = taCenter
              Caption = 'Hist'#243'rico:'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = []
              ParentFont = False
              ExplicitWidth = 44
            end
            object DBLookupListBox1: TDBLookupListBox
              Left = 0
              Top = 53
              Width = 250
              Height = 95
              Align = alClient
              Enabled = False
              KeyField = 'Controle'
              ListField = 'STATUS_STR'
              ListSource = DsMoviVStat
              TabOrder = 0
            end
          end
        end
        object Panel13: TPanel
          Left = 0
          Top = 0
          Width = 684
          Height = 340
          Align = alClient
          BevelOuter = bvNone
          Caption = 'Panel13'
          TabOrder = 1
          object DBGridKit: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 684
            Height = 323
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Kit'
                Width = 400
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtd'
                Title.Caption = 'Qtde.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FRETE'
                Title.Caption = 'Frete'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Total'
                Title.Caption = 'TOTAL'
                Width = 100
                Visible = True
              end>
            Color = clWindow
            DataSource = DsMoviVK
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            OnKeyDown = DBGridKitKeyDown
            Columns = <
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Kit'
                Width = 400
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Qtd'
                Title.Caption = 'Qtde.'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'FRETE'
                Title.Caption = 'Frete'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Total'
                Title.Caption = 'TOTAL'
                Width = 100
                Visible = True
              end>
          end
          object StaticText2: TStaticText
            Left = 0
            Top = 323
            Width = 684
            Height = 17
            Align = alBottom
            Alignment = taCenter
            BorderStyle = sbsSunken
            Caption = 
              'Selecione o kit na grade e precione a tecla F4 para visualizar o' +
              's itens dos kits'
            TabOrder = 1
          end
        end
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtVenda
    CanUpd01 = BtPagtos
    Left = 20
    Top = 280
  end
  object PMVenda: TPopupMenu
    Left = 152
    Top = 436
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
  end
  object QrMoviV: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrMoviVBeforeClose
    AfterScroll = QrMoviVAfterScroll
    OnCalcFields = QrMoviVCalcFields
    SQL.Strings = (
      'SELECT prc.Nome NOMGRACUSPRC, stv.Nome NOMESTATAT, '
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTI, (moc.Frete * -1)FRETEA, moc.* '
      'FROM moviv moc'
      'LEFT JOIN entidades ent ON ent.Codigo=moc.Cliente'
      'LEFT JOIN statusv stv ON stv.Codigo=moc.StatAtual'
      'LEFT JOIN gracusprc prc ON prc.Codigo = moc.GraCusPrc'
      'WHERE moc.Codigo > 0')
    Left = 48
    Top = 280
    object QrMoviVNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
    object QrMoviVCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'moviv.Codigo'
    end
    object QrMoviVControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'moviv.Controle'
    end
    object QrMoviVDataPedi: TDateField
      FieldName = 'DataPedi'
      Origin = 'moviv.DataPedi'
    end
    object QrMoviVDataReal: TDateField
      FieldName = 'DataReal'
      Origin = 'moviv.DataReal'
    end
    object QrMoviVCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'moviv.Cliente'
      Required = True
    end
    object QrMoviVLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'moviv.Lk'
    end
    object QrMoviVDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'moviv.DataCad'
    end
    object QrMoviVDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'moviv.DataAlt'
    end
    object QrMoviVUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'moviv.UserCad'
    end
    object QrMoviVUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'moviv.UserAlt'
    end
    object QrMoviVDATAREAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATAREAL_TXT'
      Size = 10
      Calculated = True
    end
    object QrMoviVSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMoviVTotal: TFloatField
      FieldName = 'Total'
      Origin = 'moviv.Total'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVPago: TFloatField
      FieldName = 'Pago'
      Origin = 'moviv.Pago'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVPOSIt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIt'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMoviVAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'moviv.AlterWeb'
      Required = True
    end
    object QrMoviVAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'moviv.Ativo'
      Required = True
    end
    object QrMoviVGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
      Origin = 'moviv.GraCusPrc'
      Required = True
    end
    object QrMoviVTransportadora: TIntegerField
      FieldName = 'Transportadora'
      Origin = 'moviv.Transportadora'
    end
    object QrMoviVFrete: TFloatField
      FieldName = 'Frete'
      Origin = 'moviv.Frete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVNOMECAD2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECAD2'
      Size = 100
      Calculated = True
    end
    object QrMoviVNOMEALT2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEALT2'
      Size = 100
      Calculated = True
    end
    object QrMoviVNOMECAD: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMECAD'
      LookupDataSet = QrSenhas
      LookupKeyFields = 'Numero'
      LookupResultField = 'Login'
      KeyFields = 'UserCad'
      Size = 100
      Lookup = True
    end
    object QrMoviVNOMEALT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMEALT'
      LookupDataSet = QrSenhas
      LookupKeyFields = 'Numero'
      LookupResultField = 'Login'
      KeyFields = 'UserAlt'
      Size = 100
      Lookup = True
    end
    object QrMoviVFRETEA: TFloatField
      FieldName = 'FRETEA'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVObserv: TWideStringField
      FieldName = 'Observ'
      Origin = 'moviv.Observ'
      Required = True
      Size = 255
    end
    object QrMoviVDescon: TFloatField
      FieldName = 'Descon'
      Origin = 'moviv.Descon'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVCodRast: TWideStringField
      FieldName = 'CodRast'
      Origin = 'moviv.CodRast'
      Size = 25
    end
    object QrMoviVStatAtual: TIntegerField
      FieldName = 'StatAtual'
      Origin = 'moviv.StatAtual'
    end
    object QrMoviVNOMESTATAT: TWideStringField
      FieldName = 'NOMESTATAT'
      Size = 100
    end
    object QrMoviVNOMGRACUSPRC: TWideStringField
      FieldName = 'NOMGRACUSPRC'
      Size = 30
    end
    object QrMoviVTOTALVDA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALVDA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsMoviV: TDataSource
    DataSet = QrMoviV
    Left = 76
    Top = 280
  end
  object QrMovim: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrMovimAfterScroll
    OnCalcFields = QrMovimCalcFields
    SQL.Strings = (
      'SELECT gra.Nome NOMEGRA, tam.Nome NOMETAM,'
      'cor.Nome NOMECOR, mom.*'
      'FROM movim mom'
      'LEFT JOIN grades gra ON gra.Codigo=mom.Grade'
      'LEFT JOIN tamanhos tam ON tam.Codigo=mom.Tam'
      'LEFT JOIN cores cor ON cor.Codigo=mom.Cor'
      'WHERE mom.Controle= :P0'
      'AND mom.Kit=0')
    Left = 104
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMovimNOMEGRA: TWideStringField
      FieldName = 'NOMEGRA'
      Origin = 'grades.Nome'
      Size = 30
    end
    object QrMovimNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'tamanhos.Nome'
      Size = 5
    end
    object QrMovimNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'cores.Nome'
    end
    object QrMovimControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'movim.Controle'
      Required = True
    end
    object QrMovimConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'movim.Conta'
      Required = True
    end
    object QrMovimGrade: TIntegerField
      FieldName = 'Grade'
      Origin = 'movim.Grade'
      Required = True
    end
    object QrMovimCor: TIntegerField
      FieldName = 'Cor'
      Origin = 'movim.Cor'
      Required = True
    end
    object QrMovimTam: TIntegerField
      FieldName = 'Tam'
      Origin = 'movim.Tam'
      Required = True
    end
    object QrMovimQtd: TFloatField
      FieldName = 'Qtd'
      Origin = 'movim.Qtd'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrMovimVal: TFloatField
      FieldName = 'Val'
      Origin = 'movim.Val'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMovimLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'movim.Lk'
    end
    object QrMovimDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'movim.DataCad'
    end
    object QrMovimDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'movim.DataAlt'
    end
    object QrMovimUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'movim.UserCad'
    end
    object QrMovimUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'movim.UserAlt'
    end
    object QrMovimDataPedi: TDateField
      FieldName = 'DataPedi'
      Origin = 'movim.DataPedi'
      Required = True
    end
    object QrMovimDataReal: TDateField
      FieldName = 'DataReal'
      Origin = 'movim.DataReal'
      Required = True
    end
    object QrMovimPRECO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMovimMotivo: TSmallintField
      FieldName = 'Motivo'
      Origin = 'movim.Motivo'
      Required = True
    end
    object QrMovimVen: TFloatField
      FieldName = 'Ven'
      Origin = 'movim.Ven'
      Required = True
    end
    object QrMovimPOSIq: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIq'
      DisplayFormat = '#,###,##0.000'
      Calculated = True
    end
    object QrMovimPOSIv: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIv'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMovimVAZIO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'VAZIO'
      Calculated = True
    end
  end
  object DsMovim: TDataSource
    DataSet = QrMovim
    Left = 132
    Top = 280
  end
  object QrMoviVK: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrMoviVKBeforeClose
    AfterScroll = QrMoviVKAfterScroll
    SQL.Strings = (
      'SELECT mvk.Codigo, mvk.Controle, mvk.Total, fot.Nome NOMEFOTO, '
      'mvk.GradeK, mvk.Qtd, grk.Nome, (mvk.Qtd *grk.Frete) FRETE'
      'FROM movivk mvk'
      'LEFT JOIN gradek grk ON grk.Codigo=mvk.GradeK'
      'LEFT JOIN fotos fot ON fot.Codigo = grk.Foto'
      'WHERE mvk.Codigo=:P0'
      'ORDER BY grk.Nome')
    Left = 160
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMoviVKCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'movivk.Codigo'
      Required = True
    end
    object QrMoviVKControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'movivk.Controle'
      Required = True
    end
    object QrMoviVKGradeK: TIntegerField
      FieldName = 'GradeK'
      Origin = 'movivk.GradeK'
      Required = True
    end
    object QrMoviVKQtd: TIntegerField
      FieldName = 'Qtd'
      Origin = 'movivk.Qtd'
      Required = True
    end
    object QrMoviVKNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gradek.Nome'
      Size = 100
    end
    object QrMoviVKFRETE: TFloatField
      FieldName = 'FRETE'
      Origin = 'FRETE'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVKTotal: TFloatField
      FieldName = 'Total'
      Origin = 'movivk.Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVKNOMEFOTO: TWideStringField
      FieldName = 'NOMEFOTO'
      Size = 255
    end
  end
  object DsMoviVK: TDataSource
    DataSet = QrMoviVK
    Left = 189
    Top = 280
  end
  object QrSenhas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM senhas')
    Left = 716
    Top = 9
    object QrSenhasLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'senhas.Login'
      Required = True
      Size = 30
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'senhas.Numero'
    end
    object QrSenhasSenha: TWideStringField
      FieldName = 'Senha'
      Origin = 'senhas.Senha'
      Size = 30
    end
    object QrSenhasPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'senhas.Perfil'
    end
    object QrSenhasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'senhas.Lk'
    end
  end
  object DsSenhas: TDataSource
    Left = 744
    Top = 9
  end
  object QrHist: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrHistCalcFields
    SQL.Strings = (
      'SELECT Codigo, DataReal, Total, '
      'Frete, UserCad'
      'FROM moviv'
      'WHERE Cliente=:P0'
      'AND DataReal > 0'
      'ORDER BY DataReal DESC'
      'LIMIT 1')
    Left = 660
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrHistDATAREAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATAREAL_TXT'
      Size = 10
      Calculated = True
    end
    object QrHistPOSIt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIt'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrHistTotal: TFloatField
      FieldName = 'Total'
      Origin = 'moviv.Total'
    end
    object QrHistFrete: TFloatField
      FieldName = 'Frete'
      Origin = 'moviv.Frete'
    end
    object QrHistDataReal: TDateField
      FieldName = 'DataReal'
      Origin = 'moviv.DataReal'
    end
    object QrHistNOMECAD2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECAD2'
      Size = 100
      Calculated = True
    end
    object QrHistNOMECAD: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMECAD'
      LookupDataSet = QrSenhas
      LookupKeyFields = 'Numero'
      LookupResultField = 'Login'
      KeyFields = 'UserCad'
      Size = 100
      Lookup = True
    end
    object QrHistUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'moviv.UserCad'
    end
    object QrHistCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsHist: TDataSource
    DataSet = QrHist
    Left = 688
    Top = 9
  end
  object QrMoviVStat: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMoviVStatCalcFields
    SQL.Strings = (
      'SELECT sta.Codigo, sta.Controle,'
      'sta.StatusV, sta.DiaHora, sta.Cor,'
      'stv.Nome NOMESTATUS'
      'FROM movivstat sta'
      'LEFT JOIN statusv stv ON stv.Codigo = sta.StatusV'
      'WHERE sta.Codigo=:P0'
      'ORDER BY sta.DiaHora DESC')
    Left = 273
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMoviVStatCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'movivstat.Codigo'
      Required = True
    end
    object QrMoviVStatControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'movivstat.Controle'
      Required = True
    end
    object QrMoviVStatStatusV: TIntegerField
      FieldName = 'StatusV'
      Origin = 'movivstat.StatusV'
      Required = True
    end
    object QrMoviVStatDiaHora: TDateTimeField
      FieldName = 'DiaHora'
      Origin = 'movivstat.DiaHora'
    end
    object QrMoviVStatNOMESTATUS: TWideStringField
      FieldName = 'NOMESTATUS'
      Origin = 'statusv.Nome'
      Size = 100
    end
    object QrMoviVStatCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrMoviVStatSTATUS_STR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'STATUS_STR'
      Size = 75
      Calculated = True
    end
  end
  object DsMoviVStat: TDataSource
    DataSet = QrMoviVStat
    Left = 301
    Top = 280
  end
  object PMPagtos: TPopupMenu
    OnPopup = PMPagtosPopup
    Left = 180
    Top = 436
    object IncluiPagtos1: TMenuItem
      Caption = '&Inclui'
      OnClick = IncluiPagtos1Click
    end
    object ExcluiPagtos1: TMenuItem
      Caption = '&Exclui'
      OnClick = ExcluiPagtos1Click
    end
  end
  object QrTotPagtos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) Credito'
      'FROM FTabLctALS'
      'WHERE FatID=510'
      'AND FatNum=:P0')
    Left = 520
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotPagtosCredito: TFloatField
      FieldName = 'Credito'
    end
  end
  object QrTotKit: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Total) Total'
      'FROM movivk'
      'WHERE Codigo=:P0')
    Left = 492
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotKitTotal: TFloatField
      FieldName = 'Total'
    end
  end
  object QrTotProd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Ven) Total'
      'FROM movim'
      'WHERE Controle=:P0'
      'AND Kit=0')
    Left = 464
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotProdTotal: TFloatField
      FieldName = 'Total'
    end
  end
  object QrPagtos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT la.Data, la.Vencimento, la.Credito, la.Banco, '
      'la.Agencia, la.FatID, la.ContaCorrente, la.Documento, '
      'la.Descricao, la.FatParcela, la.FatNum, ca.Nome NOMECARTEIRA, '
      'ca.Nome2 NOMECARTEIRA2, ca.Banco1, ca.Agencia1, ca.Conta1, '
      'ca.TipoDoc, la.Controle, ca.Tipo CARTEIRATIPO, la.Compensado'
      'FROM FTabLctALS la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'WHERE FatID=510'
      'AND FatNum=:P0'
      'ORDER BY la.FatParcela, la.Vencimento')
    Left = 329
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPagtosSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrPagtosData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagtosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagtosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPagtosBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrPagtosFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPagtosContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrPagtosDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPagtosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPagtosFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPagtosFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPagtosNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrPagtosNOMECARTEIRA2: TWideStringField
      FieldName = 'NOMECARTEIRA2'
      Size = 100
    end
    object QrPagtosBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrPagtosAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrPagtosConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrPagtosTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrPagtosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagtosCARTEIRATIPO: TIntegerField
      FieldName = 'CARTEIRATIPO'
      Required = True
    end
    object QrPagtosCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrPagtosAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object DsPagtos: TDataSource
    DataSet = QrPagtos
    Left = 357
    Top = 280
  end
  object QrTotMovim: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(mom.Ven) TOTITENS'
      'FROM moviv mov'
      'LEFT JOIN movim mom ON mom.Controle=mov.Controle'
      'WHERE mom.Controle=:P0'
      'AND mom.Kit=0')
    Left = 632
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotMovimTOTITENS: TFloatField
      FieldName = 'TOTITENS'
    end
  end
  object QrTotMoviVK: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM((mok.Qtd * grk.Frete)) TOTFRETE'
      'FROM moviv mov'
      'LEFT JOIN movivk mok ON mok.Codigo=mov.Codigo'
      'LEFT JOIN gradek grk ON grk.Codigo=mok.GradeK'
      'WHERE mov.Codigo=:P0')
    Left = 604
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotMoviVKTOTFRETE: TFloatField
      FieldName = 'TOTFRETE'
    end
  end
  object QrTotMoviVKIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(mom.Ven) TOTKITITS'
      'FROM moviv mov'
      'LEFT JOIN movim mom ON  mom.Controle = mov.Controle'
      'WHERE mov.Codigo=:P0'
      'AND mom.Kit<>0')
    Left = 576
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotMoviVKItsTOTKITITS: TFloatField
      FieldName = 'TOTKITITS'
    end
  end
  object QrCli: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCliCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, CodUsu, IE, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END NUM' +
        'ERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL, '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CID' +
        'ADE, '
      'CASE WHEN en.Tipo=0 THEN en.EUF     ELSE en.PUF  END UF, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pai' +
        's, '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END Log' +
        'rad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END CEP' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ', '
      
        'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", ' +
        '"RG") CAD_ESTADUAL'
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 548
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrCliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCliCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrCliNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrCliCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrCliIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrCliCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrCliPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrCliFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrCliIE: TWideStringField
      FieldName = 'IE'
    end
    object QrCliCAD_FEDERAL: TWideStringField
      FieldName = 'CAD_FEDERAL'
      Required = True
      Size = 4
    end
    object QrCliCAD_ESTADUAL: TWideStringField
      FieldName = 'CAD_ESTADUAL'
      Required = True
      Size = 4
    end
    object QrCliIE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrCliNUMERO: TLargeintField
      FieldName = 'NUMERO'
    end
    object QrCliUF: TLargeintField
      FieldName = 'UF'
    end
    object QrCliLograd: TLargeintField
      FieldName = 'Lograd'
    end
    object QrCliCEP: TLargeintField
      FieldName = 'CEP'
    end
    object QrCliE_ALL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL2'
      Size = 256
      Calculated = True
    end
  end
  object frxDsMoviV: TfrxDBDataset
    UserName = 'frxDsMoviV'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEENTI=NOMEENTI'
      'Codigo=Codigo'
      'Controle=Controle'
      'DataPedi=DataPedi'
      'DataReal=DataReal'
      'Cliente=Cliente'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'DATAREAL_TXT=DATAREAL_TXT'
      'SALDO=SALDO'
      'Total=Total'
      'Pago=Pago'
      'POSIt=POSIt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'GraCusPrc=GraCusPrc'
      'Transportadora=Transportadora'
      'Frete=Frete'
      'NOMECAD2=NOMECAD2'
      'NOMEALT2=NOMEALT2'
      'NOMECAD=NOMECAD'
      'NOMEALT=NOMEALT'
      'FRETEA=FRETEA'
      'Observ=Observ'
      'Descon=Descon'
      'CodRast=CodRast'
      'StatAtual=StatAtual'
      'NOMESTATAT=NOMESTATAT'
      'NOMGRACUSPRC=NOMGRACUSPRC'
      'TOTALVDA=TOTALVDA')
    DataSet = QrMoviV
    BCDToCurrency = False
    Left = 256
    Top = 368
  end
  object frxDsMovim: TfrxDBDataset
    UserName = 'frxDsMovim'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEGRA=NOMEGRA'
      'NOMETAM=NOMETAM'
      'NOMECOR=NOMECOR'
      'Controle=Controle'
      'Conta=Conta'
      'Grade=Grade'
      'Cor=Cor'
      'Tam=Tam'
      'Qtd=Qtd'
      'Val=Val'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'DataPedi=DataPedi'
      'DataReal=DataReal'
      'PRECO=PRECO'
      'Motivo=Motivo'
      'Ven=Ven'
      'POSIq=POSIq'
      'POSIv=POSIv'
      'VAZIO=VAZIO')
    DataSet = QrMovim
    BCDToCurrency = False
    Left = 284
    Top = 368
  end
  object frxDsMoviVK: TfrxDBDataset
    UserName = 'frxDsMoviVK'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'GradeK=GradeK'
      'Qtd=Qtd'
      'Nome=Nome'
      'FRETE=FRETE'
      'Total=Total')
    DataSet = QrMoviVK
    BCDToCurrency = False
    Left = 312
    Top = 368
  end
  object frxDsMoviVKIts: TfrxDBDataset
    UserName = 'frxDsMoviVKIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grade=Grade'
      'Tam=Tam'
      'Cor=Cor'
      'Qtd=Qtd'
      'Val=Val'
      'NOMEGRADE=NOMEGRADE'
      'NOMECOR=NOMECOR'
      'NOMETAM=NOMETAM'
      'PRECO=PRECO'
      'POSIq=POSIq'
      'POSIv=POSIv'
      'VAZIO=VAZIO')
    DataSet = QrMoviVKIts
    BCDToCurrency = False
    Left = 340
    Top = 368
  end
  object frxDsCli: TfrxDBDataset
    UserName = 'frxDsCli'
    CloseDataSource = False
    FieldAliases.Strings = (
      'E_ALL=E_ALL'
      'CNPJ_TXT=CNPJ_TXT'
      'NOME_TIPO_DOC=NOME_TIPO_DOC'
      'TE1_TXT=TE1_TXT'
      'FAX_TXT=FAX_TXT'
      'NUMERO_TXT=NUMERO_TXT'
      'CEP_TXT=CEP_TXT'
      'Codigo=Codigo'
      'Tipo=Tipo'
      'CodUsu=CodUsu'
      'NOME_ENT=NOME_ENT'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'RUA=RUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMEUF=NOMEUF'
      'Pais=Pais'
      'ENDEREF=ENDEREF'
      'TE1=TE1'
      'FAX=FAX'
      'IE=IE'
      'CAD_FEDERAL=CAD_FEDERAL'
      'CAD_ESTADUAL=CAD_ESTADUAL'
      'IE_TXT=IE_TXT'
      'NUMERO=NUMERO'
      'UF=UF'
      'Lograd=Lograd'
      'CEP=CEP'
      'E_ALL2=E_ALL2')
    DataSet = QrCli
    BCDToCurrency = False
    Left = 368
    Top = 368
  end
  object frxDsMoviVStat: TfrxDBDataset
    UserName = 'frxDsMoviVStat'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'StatusV=StatusV'
      'DiaHora=DiaHora'
      'NOMESTATUS=NOMESTATUS'
      'Cor=Cor'
      'STATUS_STR=STATUS_STR')
    DataSet = QrMoviVStat
    BCDToCurrency = False
    Left = 396
    Top = 368
  end
  object frxPedidosV: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39141.750700231500000000
    ReportOptions.LastChange = 40135.484339085640000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GroupHeader1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure ReportTitle1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'begin'
      
        '  if <VAR_IMGEXISTE> = True then Picture1.LoadFromFile(<VAR_IMG>' +
        ');      '
      'end.')
    OnGetValue = frxPedidosVGetValue
    Left = 228
    Top = 368
    Datasets = <
      item
        DataSet = frxDsCli
        DataSetName = 'frxDsCli'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMovim
        DataSetName = 'frxDsMovim'
      end
      item
        DataSet = frxDsMoviV
        DataSetName = 'frxDsMoviV'
      end
      item
        DataSet = frxDsMoviVK
        DataSetName = 'frxDsMoviVK'
      end
      item
        DataSet = frxDsMoviVKIts
        DataSetName = 'frxDsMoviVKIts'
      end
      item
        DataSet = frxDsMoviVStat
        DataSetName = 'frxDsMoviVStat'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      LargeDesignHeight = True
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Height = 67.700835000000000000
        ParentFont = False
        Top = 200.315090000000000000
        Width = 699.213050000000000000
        DataSet = frxDsMoviV
        DataSetName = 'frxDsMoviV'
        RowCount = 0
        StartNewPage = True
        object Memo6: TfrxMemoView
          Top = 5.267717999999992000
          Width = 699.212598430000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Style = fsDash
          Frame.Typ = [ftTop]
          HAlign = haCenter
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo35: TfrxMemoView
          Top = 48.803185000000000000
          Width = 45.354298980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Pedido:')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 45.354360000000000000
          Top = 48.803185000000000000
          Width = 90.708658980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoviV."Codigo"]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 468.283767000000000000
          Top = 48.803185000000000000
          Width = 95.244070570000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'STATUS DO PEDIDO:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo2: TfrxMemoView
          Left = 304.630118000000000000
          Top = 48.803185000000000000
          Width = 65.763760980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'DATA VENDA:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo3: TfrxMemoView
          Left = 139.086704000000000000
          Top = 48.803185000000000000
          Width = 67.653525980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'DATA PEDIDO:')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo5: TfrxMemoView
          Left = 563.527923000000000000
          Top = 48.803185000000000000
          Width = 132.283464570000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMoviVStat."NOMESTATUS"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo7: TfrxMemoView
          Left = 370.015987000000000000
          Top = 48.803185000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          DataField = 'DATAREAL_TXT'
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoviV."DATAREAL_TXT"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo8: TfrxMemoView
          Left = 206.362338000000000000
          Top = 48.803185000000000000
          Width = 94.488188980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoviV."DataPedi"]')
          ParentFont = False
          VAlign = vaBottom
        end
        object Memo9: TfrxMemoView
          Left = 45.354360000000000000
          Top = 30.236240000000000000
          Width = 652.724831000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMoviV."NOMEENTI"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Top = 30.236240000000000000
          Width = 45.354298980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Cliente:')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 332.598640000000000000
        Width = 699.213050000000000000
        DataSet = frxDsMovim
        DataSetName = 'frxDsMovim'
        RowCount = 0
        object Memo15: TfrxMemoView
          Width = 306.141783540000000000
          Height = 18.897650000000000000
          DataField = 'NOMEGRA'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovim."NOMEGRA"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 306.141783300000000000
          Width = 148.535406950000000000
          Height = 18.897650000000000000
          DataField = 'NOMECOR'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovim."NOMECOR"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 454.677459000000000000
          Width = 96.755845950000000000
          Height = 18.897650000000000000
          DataField = 'NOMETAM'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMovim."NOMETAM"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 551.212957000000000000
          Width = 72.566926690000000000
          Height = 18.897650000000000000
          DataField = 'POSIq'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMovim."POSIq"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 631.181510000000000000
          Width = 30.236222910000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 668.976810000000000000
          Width = 30.236222910000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
      object DetailData2: TfrxDetailData
        FillType = ftBrush
        Height = 18.897637800000000000
        Top = 427.086890000000000000
        Width = 699.213050000000000000
        DataSet = frxDsMoviVK
        DataSetName = 'frxDsMoviVK'
        RowCount = 0
        object Memo26: TfrxMemoView
          Left = 28.346475000000000000
          Width = 454.299139860000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMoviVK."Nome"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 549.165709000000000000
          Width = 74.834645180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviVK."Qtd"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Width = 29.102234540000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'KIT:')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 482.645981000000000000
          Width = 66.519679180000000000
          Height = 18.897637800000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'QUANTIDADE:')
          ParentFont = False
        end
      end
      object SubdetailData1: TfrxSubdetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 510.236550000000000000
        Width = 699.213050000000000000
        DataSet = frxDsMoviVKIts
        DataSetName = 'frxDsMoviVKIts'
        RowCount = 0
        object Memo30: TfrxMemoView
          Width = 302.740206540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviVKIts."NOMEGRADE"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 551.811380000000000000
          Width = 72.188974180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviVKIts."POSIq"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 302.740206300000000000
          Width = 148.535382540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviVKIts."NOMECOR"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 451.275882000000000000
          Width = 100.535351540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviVKIts."NOMETAM"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 631.181510000000000000
          Width = 30.236222910000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 668.976810000000000000
          Width = 30.236222910000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 20.078744060000000000
        Top = 291.023810000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsMoviV."Codigo"'
        object Memo11: TfrxMemoView
          Top = 1.181094059999998000
          Width = 306.141783540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 551.212957000000000000
          Top = 1.181094059999998000
          Width = 72.566926690000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 306.141930000000000000
          Top = 1.181094059999998000
          Width = 148.535406950000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'COR')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 454.677459000000000000
          Top = 1.181094059999998000
          Width = 96.755845950000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'TAMANHO')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 631.181510000000000000
          Top = 1.181090159999998000
          Width = 68.031520470000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CONFER'#202'NCIA')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 31.188992000000000000
        Top = 374.173470000000000000
        Width = 699.213050000000000000
        object Memo39: TfrxMemoView
          Top = 7.000000000000000000
          Width = 623.244106450000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 468.661720000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsMoviVKIts."VAZIO"'
        object Memo20: TfrxMemoView
          Width = 302.740206540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'MERCADORIA')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 550.944884330000000000
          Width = 72.566929133858300000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'QUANTIDADE')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 302.740353000000000000
          Width = 152.314912540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'COR')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 454.677165350000000000
          Width = 96.755821540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'TAMANHO')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 631.181510000000000000
          Width = 68.031520470000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'CONFER'#202'NCIA')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 26.078737720000000000
        Top = 551.811380000000000000
        Width = 699.213050000000000000
        object Memo44: TfrxMemoView
          Top = 6.377953000000000000
          Width = 619.842529450000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          ParentFont = False
        end
      end
      object DetailData3: TfrxDetailData
        FillType = ftBrush
        Height = 98.645733000000000000
        Top = 600.945270000000000000
        Width = 699.213050000000000000
        DataSet = frxDsMoviV
        DataSetName = 'frxDsMoviV'
        RowCount = 0
        object Memo19: TfrxMemoView
          Left = 0.000000240000000000
          Top = 41.952783000000000000
          Width = 620.220482450000000000
          Height = 18.897650000000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DE FRETE')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 620.598826240000000000
          Top = 41.952783000000000000
          Width = 75.212598180000000000
          Height = 18.897650000000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviV."Frete"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Top = 60.850433000000000000
          Width = 620.598435450000000000
          Height = 18.897650000000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL DO PEDIDO')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 620.598826240000000000
          Top = 60.850433000000000000
          Width = 75.212598180000000000
          Height = 18.897650000000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_TOTPED]')
          ParentFont = False
        end
        object Memo47: TfrxMemoView
          Left = 0.000000240000000000
          Top = 79.748083000000000000
          Width = 620.598435450000000000
          Height = 18.897650000000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo48: TfrxMemoView
          Left = 620.598826480000000000
          Top = 79.748083000000000000
          Width = 75.212598180000000000
          Height = 18.897650000000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clNone
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[VAR_TOTVDA]')
          ParentFont = False
        end
        object Rich1: TfrxRichView
          Top = 18.897650000000000000
          Width = 699.213050000000000000
          Height = 18.897637800000000000
          StretchMode = smMaxHeight
          DataField = 'Observ'
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6465
            666C616E67313034367B5C666F6E7474626C7B5C66305C666E696C204D532053
            616E732053657269663B7D7D0D0A7B5C2A5C67656E657261746F72204D736674
            6564697420352E34312E32312E323531303B7D5C766965776B696E64345C7563
            315C706172645C66305C667331365C7061720D0A7D0D0A00}
        end
        object Memo32: TfrxMemoView
          Width = 302.740206540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'OBSERVA'#199#213'ES:')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 120.944960000000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape2: TfrxShapeView
          Width = 699.213050000000000000
          Height = 119.811055350000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo51: TfrxMemoView
          Left = 80.503937010000000000
          Top = 21.433086000000000000
          Width = 467.527581020000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."E_ALL"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 80.637848000000000000
          Top = 0.755906000000000000
          Width = 467.527861000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsMoviV."NOMEENTI"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo52: TfrxMemoView
          Left = 8.826778000000000000
          Top = 0.755906000000000000
          Width = 71.811070000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            'Destinat'#225'rio:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 555.590910000000000000
          Top = 9.653525000000000000
          Width = 124.724409450000000000
          Height = 102.047244090000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo4: TfrxMemoView
          Left = 80.370130000000000000
          Top = 36.669295000000000000
          Width = 467.527581020000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsCli."E_ALL2"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo43: TfrxMemoView
          Left = 80.503937010000000000
          Top = 51.779527560000000000
          Width = 467.527581020000000000
          Height = 15.118110240000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Calibri'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Refer'#234'ncia:  [frxDsCli."ENDEREF"]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
    end
  end
  object QrMoviVKIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mom.Grade, mom.Tam, mom.Cor, mom.Qtd, mom.Val,'
      'gra.Nome NOMEGRADE, cor.Nome NOMECOR, tam.Nome NOMETAM,'
      
        'CASE WHEN mom.Qtd=0 THEN mom.Qtd ELSE mom.Ven / -mom.Qtd END PRE' +
        'CO,'
      '- mom.Qtd POSIq, mom.Ven POSIv'
      'FROM movim mom'
      'LEFT JOIN moviv mov ON mov.Controle = mom.Controle'
      'LEFT JOIN grades gra ON  gra.Codigo = mom.Grade'
      'LEFT JOIN cores cor ON cor.Codigo = mom.Cor'
      'LEFT JOIN tamanhos tam ON tam.Codigo = mom.Tam'
      'WHERE mov.Codigo=:P0'
      'AND mom.Kit=:P1')
    Left = 217
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMoviVKItsGrade: TIntegerField
      FieldName = 'Grade'
      Required = True
    end
    object QrMoviVKItsTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrMoviVKItsCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrMoviVKItsQtd: TFloatField
      FieldName = 'Qtd'
      Required = True
    end
    object QrMoviVKItsVal: TFloatField
      FieldName = 'Val'
      Required = True
    end
    object QrMoviVKItsNOMEGRADE: TWideStringField
      FieldName = 'NOMEGRADE'
      Size = 30
    end
    object QrMoviVKItsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 100
    end
    object QrMoviVKItsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 100
    end
    object QrMoviVKItsPRECO: TFloatField
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVKItsPOSIq: TFloatField
      FieldName = 'POSIq'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrMoviVKItsPOSIv: TFloatField
      FieldName = 'POSIv'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVKItsVAZIO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'VAZIO'
      Calculated = True
    end
  end
  object DsMoviVKIts: TDataSource
    DataSet = QrMoviVKIts
    Left = 245
    Top = 280
  end
  object QrMovim2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mom.Grade, mom.Cor, mom.Tam, '
      'mom.Val, mom.Qtd, mom.Conta'
      'FROM movim mom'
      'WHERE mom.Controle= :P0')
    Left = 385
    Top = 280
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMovim2Grade: TIntegerField
      FieldName = 'Grade'
    end
    object QrMovim2Cor: TIntegerField
      FieldName = 'Cor'
    end
    object QrMovim2Tam: TIntegerField
      FieldName = 'Tam'
    end
    object QrMovim2Val: TFloatField
      FieldName = 'Val'
    end
    object QrMovim2Qtd: TFloatField
      FieldName = 'Qtd'
    end
    object QrMovim2Conta: TIntegerField
      FieldName = 'Conta'
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 772
    Top = 9
  end
  object QrFutNeg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gra.Nome NOMEGRADE, cor.Nome NOMECOR, tam.Nome NOMETAM, '
      'mom.Conta, mom.Grade, mom.Cor, mom.Tam, '
      'mom.Qtd, mom.Val, pro.EstqQ, pro.EstqV, '
      'pro.EstqQ + mom.Qtd FutQtd,'
      'pro.EstqV + mom.Val FutVal'
      'FROM movim mom'
      'LEFT JOIN produtos pro ON'
      '  pro.Codigo=mom.Grade AND'
      '  pro.Cor=mom.Cor AND'
      '  pro.Tam=mom.Tam'
      'LEFT JOIN grades gra ON gra.Codigo=mom.Grade'
      'LEFT JOIN cores  cor ON cor.Codigo=mom.Cor'
      'LEFT JOIN tamanhos tam ON tam.Codigo=mom.Tam'
      'WHERE mom.Controle=:P0'
      'AND (pro.EstqQ + mom.Qtd) < 0')
    Left = 89
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFutNegConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrFutNegGrade: TIntegerField
      FieldName = 'Grade'
      Required = True
    end
    object QrFutNegCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrFutNegTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrFutNegQtd: TFloatField
      FieldName = 'Qtd'
      Required = True
    end
    object QrFutNegVal: TFloatField
      FieldName = 'Val'
      Required = True
    end
    object QrFutNegEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrFutNegEstqV: TFloatField
      FieldName = 'EstqV'
    end
    object QrFutNegFutQtd: TFloatField
      FieldName = 'FutQtd'
    end
    object QrFutNegFutVal: TFloatField
      FieldName = 'FutVal'
    end
    object QrFutNegNOMEGRADE: TWideStringField
      FieldName = 'NOMEGRADE'
      Size = 30
    end
    object QrFutNegNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
    end
    object QrFutNegNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 5
    end
  end
  object frxDsFutNeg: TfrxDBDataset
    UserName = 'frxDsFutNeg'
    CloseDataSource = False
    DataSet = QrFutNeg
    BCDToCurrency = False
    Left = 117
    Top = 8
  end
  object frxFutNeg: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39140.346533923600000000
    ReportOptions.LastChange = 40135.769807592590000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxFutNegGetValue
    Left = 145
    Top = 8
    Datasets = <
      item
        DataSet = frxDsFutNeg
        DataSetName = 'frxDsFutNeg'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 52.913420000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo1: TfrxMemoView
          Left = 37.795300000000000000
          Top = 11.338590000000000000
          Width = 718.110700000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Mercadorias que ficariam com estoque negativo')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 37.795300000000000000
          Top = 34.015770000000000000
          Width = 45.354298980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Pedido:')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 83.149660000000000000
          Top = 34.015770000000000000
          Width = 90.708658980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_CODMOVIV]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 173.858380000000000000
        Width = 793.701300000000000000
        DataSet = frxDsFutNeg
        DataSetName = 'frxDsFutNeg'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DataField = 'Grade'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%4.4d'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFutNeg."Grade"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 75.590600000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          DataField = 'NOMEGRADE'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFutNeg."NOMEGRADE"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DataField = 'Cor'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%4.4d'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFutNeg."Cor"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 377.953000000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DataField = 'NOMECOR'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFutNeg."NOMECOR"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 491.338900000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DataField = 'Tam'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%4.4d'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFutNeg."Tam"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'NOMETAM'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFutNeg."NOMETAM"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'EstqQ'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFutNeg."EstqQ"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 680.315400000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'FutQtd'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFutNeg."FutQtd"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 75.590600000000000000
        Top = 298.582870000000000000
        Width = 793.701300000000000000
        object Memo107: TfrxMemoView
          Left = 563.149970000000000000
          Top = 11.338590000000010000
          Width = 192.756030000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 132.283550000000000000
        Width = 793.701300000000000000
        object Memo15: TfrxMemoView
          Left = 37.795300000000000000
          Width = 302.362253540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 340.157700000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 491.338582680000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Tamanho')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590526770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Estq. Atual')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 680.315400000000000000
          Width = 75.590526770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Estq. Futuro')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 215.433210000000000000
        Width = 793.701300000000000000
        object Memo8: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779530000000000000
          Width = 718.110553540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
      end
    end
  end
  object QrLocFoto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fot.Nome NOMEFOTO'
      'FROM produtos prd'
      'LEFT JOIN fotos fot ON fot.Codigo = prd.Foto'
      'WHERE prd.Codigo=:P0'
      'AND prd.Cor=:P1'
      'AND prd.Tam=:P2')
    Left = 628
    Top = 297
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrLocFotoNOMEFOTO: TWideStringField
      FieldName = 'NOMEFOTO'
      Size = 255
    end
  end
  object PMImprime: TPopupMenu
    Left = 208
    Top = 436
    object Vendaatual1: TMenuItem
      Caption = '&Venda atual'
      OnClick = Vendaatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Vendaatualcompreo1: TMenuItem
      Caption = 'Venda atual com &pre'#231'o'
      OnClick = Vendaatualcompreo1Click
    end
  end
  object dmkPermissoes2: TdmkPermissoes
    Left = 174
    Top = 8
  end
end
