object FmVendaPesq: TFmVendaPesq
  Left = 339
  Top = 185
  Caption = 'PRD-VENDA-007 :: Pesquisa venda de mercadorias'
  ClientHeight = 462
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 414
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    ExplicitWidth = 862
    object BtReabre: TBitBtn
      Tag = 18
      Left = 7
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Reabrir'
      TabOrder = 0
      OnClick = BtReabreClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      ExplicitLeft = 750
      object BtSaida: TBitBtn
        Tag = 13
        Left = 16
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
    object BtLocaliza: TBitBtn
      Tag = 22
      Left = 100
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Localizar'
      TabOrder = 2
      OnClick = BtLocalizaClick
      NumGlyphs = 2
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Pesquisa Venda de Mercadorias'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    ExplicitWidth = 862
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1006
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = -30
      ExplicitTop = -2
      ExplicitWidth = 436
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 366
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 862
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1006
      Height = 118
      Align = alTop
      TabOrder = 0
      ExplicitWidth = 860
      object Label1: TLabel
        Left = 13
        Top = 4
        Width = 35
        Height = 13
        Caption = 'Cliente:'
      end
      object Label2: TLabel
        Left = 13
        Top = 47
        Width = 36
        Height = 13
        Caption = 'Pedido:'
      end
      object Label111: TLabel
        Left = 99
        Top = 47
        Width = 45
        Height = 13
        Caption = 'Telefone:'
      end
      object Label3: TLabel
        Left = 220
        Top = 47
        Width = 61
        Height = 13
        Caption = 'CNPJ / CPF:'
      end
      object GBPedido: TGroupBox
        Left = 341
        Top = 47
        Width = 233
        Height = 65
        Caption = 'Per'#237'odo do pedido'
        TabOrder = 5
        object CkDataFimPed: TCheckBox
          Left = 121
          Top = 16
          Width = 90
          Height = 17
          Caption = 'Data final:'
          TabOrder = 3
        end
        object TPDataFimPed: TDateTimePicker
          Left = 121
          Top = 34
          Width = 105
          Height = 21
          CalColors.TextColor = clMenuText
          Date = 37636.777157974500000000
          Time = 37636.777157974500000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 2
        end
        object TPDataIniPed: TDateTimePicker
          Left = 10
          Top = 34
          Width = 105
          Height = 21
          CalColors.TextColor = clMenuText
          Date = 37636.777157974500000000
          Time = 37636.777157974500000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
        end
        object CkDataIniPed: TCheckBox
          Left = 10
          Top = 16
          Width = 90
          Height = 17
          Caption = 'Data inicial:'
          TabOrder = 1
        end
      end
      object EdCliente: TdmkEditCB
        Left = 13
        Top = 20
        Width = 50
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        DBLookupComboBox = CBCliente
      end
      object CBCliente: TdmkDBLookupComboBox
        Left = 66
        Top = 20
        Width = 508
        Height = 21
        KeyField = 'Codigo'
        ListField = 'NOMEENTI'
        ListSource = DsCliente
        TabOrder = 1
        dmkEditCB = EdCliente
        UpdType = utYes
      end
      object EdPedido: TdmkEdit
        Left = 13
        Top = 63
        Width = 78
        Height = 21
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdTel: TdmkEdit
        Left = 99
        Top = 63
        Width = 113
        Height = 21
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtTelLongo
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'ETe1'
        UpdCampo = 'ETe1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
      object EdDoc: TdmkEdit
        Left = 220
        Top = 63
        Width = 113
        Height = 21
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtCPFJ
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'ETe1'
        UpdCampo = 'ETe1'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
      end
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 1
      Top = 119
      Width = 1006
      Height = 229
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEENTI'
          Title.Caption = 'Cliente'
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'POSIt'
          Title.Caption = 'Total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESTAT'
          Title.Caption = 'Status atual'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Tipo'
          Title.Caption = 'Departamento'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEVENDEDOR'
          Title.Caption = 'Vendedor'
          Width = 140
          Visible = True
        end>
      Color = clWindow
      DataSource = DsPesq
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEENTI'
          Title.Caption = 'Cliente'
          Width = 250
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'POSIt'
          Title.Caption = 'Total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESTAT'
          Title.Caption = 'Status atual'
          Width = 150
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Tipo'
          Title.Caption = 'Departamento'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMEVENDEDOR'
          Title.Caption = 'Vendedor'
          Width = 140
          Visible = True
        end>
    end
    object StaticText2: TStaticText
      Left = 1
      Top = 348
      Width = 1006
      Height = 17
      Align = alBottom
      Alignment = taCenter
      BorderStyle = sbsSunken
      Caption = 'Duplo click para localizar o registro'
      TabOrder = 2
      ExplicitWidth = 860
    end
  end
  object QrCliente: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, CASE WHEN en.Tipo=0 THEN en.RazaoSocial'
      'ELSE en.Nome END NOMEENTI'
      'FROM entidades en'
      'WHERE en.Cliente1="V"'
      'ORDER BY NOMEENTI')
    Left = 12
    Top = 9
    object QrClienteCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrClienteNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsCliente: TDataSource
    DataSet = QrCliente
    Left = 40
    Top = 9
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPesqCalcFields
    SQL.Strings = (
      
        'SELECT moc.Codigo, moc.Codigo Controle, moc.Cliente, 1 IntTipo, ' +
        'moc.DataPedi Data,'
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTI, (moc.Frete * -1)FRETEA,'
      'moc.Total, moc.Frete, moc.Descon, moc.Pago,'
      'sta.Nome NOMESTAT, grd.Nome Tipo,'
      'CASE moc.UserCad'
      'WHEN -2 THEN "BOSS [Administrador]"'
      'WHEN -1 THEN "MASTER [Admin. DERMATEK]"'
      'WHEN  0 THEN "N'#227'o definido"'
      
        'ELSE (CASE WHEN fun.Tipo=0 THEN fun.RazaoSocial ELSE fun.Nome EN' +
        'D) END NOMEVENDEDOR'
      'FROM moviv moc'
      'LEFT JOIN entidades ent ON ent.Codigo=moc.Cliente'
      'LEFT JOIN statusv sta ON sta.Codigo = moc.StatAtual'
      'LEFT JOIN graded grd ON grd.Codigo = moc.GradeD'
      'LEFT JOIN senhas sen ON sen.Numero = moc.UserCad'
      'LEFT JOIN entidades fun ON fun.Codigo = sen.Funcionario'
      ''
      'UNION'
      ''
      
        'SELECT moc.Codigo, moc.Codigo Controle, moc.Enti Cliente, 2 IntT' +
        'ipo, moc.DataCad Data,'
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTI, 0/1 FRETEA,'
      '0.0 Total, 0.0 Frete, 0.0 Descon, 0.0 Pago,'
      'IF(moc.Status = 0, "APROVADO", "REPROVADO") NOMESTAT,'
      'grd.Nome Tipo,'
      'CASE moc.UserCad'
      'WHEN -2 THEN "BOSS [Administrador]"'
      'WHEN -1 THEN "MASTER [Admin. DERMATEK]"'
      'WHEN  0 THEN "N'#227'o definido"'
      
        'ELSE (CASE WHEN fun.Tipo=0 THEN fun.RazaoSocial ELSE fun.Nome EN' +
        'D) END NOMEVENDEDOR'
      'FROM cartaval moc'
      'LEFT JOIN entidades ent ON ent.Codigo = moc.Enti'
      'LEFT JOIN graded grd ON grd.Codigo = moc.GradeD'
      'LEFT JOIN senhas sen ON sen.Numero = moc.UserCad'
      'LEFT JOIN entidades fun ON fun.Codigo = sen.Funcionario'
      'WHERE moc.Codigo > 0'
      ''
      'UNION'
      ''
      
        'SELECT moc.Codigo, moc.Codigo Controle, moc.Cliente, 3 IntTipo, ' +
        'moc.DataPedi Data,'
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTI, 0/1 FRETEA,'
      'moc.Total, moc.Frete, moc.Descon, moc.Pago, "" NOMESTAT,'
      'grd.Nome Tipo,'
      'CASE moc.UserCad'
      'WHEN -2 THEN "BOSS [Administrador]"'
      'WHEN -1 THEN "MASTER [Admin. DERMATEK]"'
      'WHEN  0 THEN "N'#227'o definido"'
      
        'ELSE (CASE WHEN fun.Tipo=0 THEN fun.RazaoSocial ELSE fun.Nome EN' +
        'D) END NOMEVENDEDOR'
      'FROM movil moc'
      'LEFT JOIN entidades ent ON ent.Codigo = moc.Cliente'
      'LEFT JOIN graded grd ON grd.Codigo = moc.GradeD'
      'LEFT JOIN senhas sen ON sen.Numero = moc.UserCad'
      'LEFT JOIN entidades fun ON fun.Codigo = sen.Funcionario'
      'WHERE moc.Codigo > 0'
      ''
      'UNION'
      ''
      
        'SELECT alu.Codigo, alu.Conta Controle, alu.Entidade Cliente, 4 I' +
        'ntTipo, aul.Data,'
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTI, 0 FRETEA, 0 Total, 0 Frete, '
      '0 Descon, 0 Pago, '#39#39' NOMESTAT, grd.Nome Tipo,'
      'CASE alu.UserCad'
      'WHEN -2 THEN "BOSS [Administrador]"'
      'WHEN -1 THEN "MASTER [Admin. DERMATEK]"'
      'WHEN  0 THEN "N'#227'o definido"'
      
        'ELSE (CASE WHEN fun.Tipo=0 THEN fun.RazaoSocial ELSE fun.Nome EN' +
        'D) END NOMEVENDEDOR'
      'FROM ciclosalu alu'
      'LEFT JOIN ciclosaula aul ON aul.Controle = alu.Controle'
      'LEFT JOIN entidades ent ON ent.Codigo = alu.Entidade'
      'LEFT JOIN graded grd ON grd.Codigo = alu.GradeD'
      'LEFT JOIN senhas sen ON sen.Numero = alu.UserCad'
      'LEFT JOIN entidades fun ON fun.Codigo = sen.Funcionario'
      'WHERE Entidade > 0')
    Left = 68
    Top = 9
    object QrPesqSALDO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'SALDO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrPesqPOSIt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIt'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrPesqNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
    object QrPesqFRETEA: TFloatField
      FieldName = 'FRETEA'
    end
    object QrPesqTotal: TFloatField
      FieldName = 'Total'
    end
    object QrPesqFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrPesqDescon: TFloatField
      FieldName = 'Descon'
    end
    object QrPesqPago: TFloatField
      FieldName = 'Pago'
    end
    object QrPesqNOMESTAT: TWideStringField
      FieldName = 'NOMESTAT'
      Size = 100
    end
    object QrPesqCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPesqCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrPesqIntTipo: TLargeintField
      FieldName = 'IntTipo'
      Required = True
    end
    object QrPesqData: TDateField
      FieldName = 'Data'
    end
    object QrPesqTipo: TWideStringField
      FieldName = 'Tipo'
      Size = 30
    end
    object QrPesqControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPesqNOMEVENDEDOR: TWideStringField
      FieldName = 'NOMEVENDEDOR'
      Size = 100
    end
  end
  object DsPesq: TDataSource
    DataSet = QrPesq
    Left = 96
    Top = 9
  end
end
