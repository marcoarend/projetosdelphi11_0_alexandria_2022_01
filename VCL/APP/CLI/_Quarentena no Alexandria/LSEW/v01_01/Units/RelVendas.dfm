object FmRelVendas: TFmRelVendas
  Left = 339
  Top = 185
  Caption = 'PRD-VENDA-005 :: Relat'#243'rio de vendas'
  ClientHeight = 438
  ClientWidth = 834
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 390
    Width = 834
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtPesquisa: TBitBtn
      Tag = 22
      Left = 7
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Pesquisa'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtPesquisaClick
    end
    object Panel2: TPanel
      Left = 722
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 16
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtImprime: TBitBtn
      Tag = 5
      Left = 103
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Imprime'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtImprimeClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 834
    Height = 48
    Align = alTop
    Caption = 'Relat'#243'rio de vendas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 832
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitTop = 3
      ExplicitWidth = 830
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 834
    Height = 342
    Align = alClient
    TabOrder = 0
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 832
      Height = 100
      Align = alTop
      TabOrder = 0
      object Label1: TLabel
        Left = 9
        Top = 4
        Width = 49
        Height = 13
        Caption = 'Vendedor:'
      end
      object GBDataPed: TGroupBox
        Left = 9
        Top = 47
        Width = 356
        Height = 42
        Caption = 'Per'#237'odo do pedido'
        TabOrder = 2
        object CkDataPedFim: TCheckBox
          Left = 181
          Top = 16
          Width = 71
          Height = 17
          Caption = 'Data final:'
          TabOrder = 2
        end
        object TPDataPedFim: TDateTimePicker
          Left = 255
          Top = 15
          Width = 90
          Height = 20
          CalColors.TextColor = clMenuText
          Date = 37636.000000000000000000
          Time = 37636.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 3
        end
        object TPDataPedIni: TDateTimePicker
          Left = 83
          Top = 15
          Width = 90
          Height = 20
          CalColors.TextColor = clMenuText
          Date = 37636.000000000000000000
          Time = 37636.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          TabOrder = 1
        end
        object CkDataPedIni: TCheckBox
          Left = 6
          Top = 16
          Width = 71
          Height = 17
          Caption = 'Data inicial:'
          TabOrder = 0
        end
      end
      object EdVendedor: TdmkEditCB
        Left = 9
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBVendedor
        IgnoraDBLookupComboBox = False
      end
      object CBVendedor: TdmkDBLookupComboBox
        Left = 65
        Top = 20
        Width = 300
        Height = 21
        KeyField = 'Numero'
        ListField = 'NOMEENT'
        ListSource = DsVendedor
        TabOrder = 1
        dmkEditCB = EdVendedor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object GroupBox1: TGroupBox
        Left = 601
        Top = 1
        Width = 230
        Height = 98
        Align = alRight
        Caption = 'Listas de pre'#231'os'
        TabOrder = 3
        object CkLBGraCusPrc: TCheckListBox
          Left = 2
          Top = 15
          Width = 226
          Height = 81
          Align = alClient
          ItemHeight = 13
          TabOrder = 0
        end
      end
      object GroupBox2: TGroupBox
        Left = 371
        Top = 1
        Width = 230
        Height = 98
        Align = alRight
        Caption = 'Status de venda'
        TabOrder = 4
        object CkLBStatus: TCheckListBox
          Left = 2
          Top = 15
          Width = 226
          Height = 81
          Align = alClient
          ItemHeight = 13
          TabOrder = 0
        end
      end
    end
    object dmkDBGrid1: TdmkDBGrid
      Left = 1
      Top = 101
      Width = 832
      Height = 223
      Align = alClient
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Pedido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DiaHora_TXT'
          Title.Caption = 'Data'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECAD2'
          Title.Caption = 'Vendedor'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESTAT'
          Title.Caption = 'Status'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Total'
          Title.Caption = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descon'
          Title.Caption = 'Desconto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Frete'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TOTALVDA'
          Title.Caption = 'TOTAL'
          Visible = True
        end>
      Color = clWindow
      DataSource = DsMoviV
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'Pedido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DiaHora_TXT'
          Title.Caption = 'Data'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECLI'
          Title.Caption = 'Cliente'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECAD2'
          Title.Caption = 'Vendedor'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMESTAT'
          Title.Caption = 'Status'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Total'
          Title.Caption = 'Valor'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descon'
          Title.Caption = 'Desconto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Frete'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TOTALVDA'
          Title.Caption = 'TOTAL'
          Visible = True
        end>
    end
    object PB1: TProgressBar
      Left = 1
      Top = 324
      Width = 832
      Height = 17
      Align = alBottom
      TabOrder = 2
      Visible = False
    end
  end
  object frxRelVendas: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39141.750700231500000000
    ReportOptions.LastChange = 40107.574384259260000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GroupHeader1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure ReportTitle1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'begin'
      
        '  if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLogoC' +
        'aminho>);  '
      'end.')
    OnGetValue = frxRelVendasGetValue
    Left = 64
    Top = 11
    Datasets = <
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMoviV
        DataSetName = 'frxDsMoviV'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      LargeDesignHeight = True
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 71.811055350000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 56.692950000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo6: TfrxMemoView
          Left = 154.960730000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 551.811380000000000000
          Top = 18.897650000000000000
          Width = 147.401670000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 154.960730000000000000
          Top = 18.897650000000000000
          Width = 393.071120000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 147.401574800000000000
          Height = 49.133890000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object frxMemoView1: TfrxMemoView
          Left = 3.779530000000000000
          Top = 56.692950000000000000
          Width = 544.252320000000000000
          Height = 15.118105350000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 147.401670000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsMaster."Tipo"'
        object Memo2: TfrxMemoView
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Pedido')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 56.692950000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 113.385900000000000000
          Width = 132.283464570000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 245.669450000000000000
          Width = 132.283464570000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Vendedor')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 377.953000000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Status')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 472.441250000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 529.134200000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 585.827150000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Frete')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 642.520100000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 222.992270000000000000
        Width = 699.213050000000000000
        object Memo24: TfrxMemoView
          Top = 3.779530000000000000
          Width = 472.441213390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 472.441250000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviV."Total">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 529.134200000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviV."Descon">)]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 585.827150000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviV."Frete">)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 642.520100000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviV."TOTALVDA">)]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 302.362400000000000000
        Width = 699.213050000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 185.196970000000000000
        Width = 699.213050000000000000
        DataSet = frxDsMoviV
        DataSetName = 'frxDsMoviV'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataField = 'Codigo'
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviV."Codigo"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 56.692950000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataField = 'DataTXT'
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsMoviV."DataTXT"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 113.385900000000000000
          Width = 132.283464570000000000
          Height = 18.897650000000000000
          DataField = 'NOMECLI'
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviV."NOMECLI"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 245.669450000000000000
          Width = 132.283464570000000000
          Height = 18.897650000000000000
          DataField = 'NOMEENT'
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviV."NOMEENT"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 377.953000000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          DataField = 'NOMESTAT'
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviV."NOMESTAT"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 472.441250000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviV."Total"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 529.134200000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviV."Descon"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 585.827150000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviV."Frete"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 642.520100000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviV."TOTALVDA"]')
          ParentFont = False
        end
      end
    end
  end
  object DsMoviV: TDataSource
    DataSet = QrMoviV
    Left = 36
    Top = 11
  end
  object QrMoviV: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrMoviVBeforeClose
    AfterScroll = QrMoviVAfterScroll
    OnCalcFields = QrMoviVCalcFields
    SQL.Strings = (
      'SELECT mov.Codigo, mov.Controle, mov.Total, mov.Pago, '
      'mov.Descon,  mov.UserCad, '
      'DATE_FORMAT(MAX(sta.DiaHora), "%d/%m/%y") DiaHora_TXT,'
      'mov.Frete, CASE WHEN enb.Tipo=0 '
      'THEN enb.RazaoSocial ELSE enb.Nome END NOMECLI,'
      ' CASE WHEN ent.Tipo=0 '
      'THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT,'
      'stv.Nome NOMESTAT, mov.DataPedi, mov.DataReal,'
      'DATE_FORMAT(CASE WHEN DataReal=0 '
      'THEN DataPedi ELSE DataReal END,  "%d/%m/%Y") DataTXT'
      'FROM moviv mov'
      'LEFT JOIN senhas sen ON sen.Numero = mov.UserCad'
      'LEFT JOIN entidades ent ON ent.Codigo = sen.Funcionario'
      'LEFT JOIN entidades enb ON enb.Codigo = mov.Cliente'
      'LEFT JOIN statusv stv ON stv.Codigo = mov.StatAtual'
      'LEFT JOIN movivstat sta ON sta.Codigo = mov.Codigo'
      'GROUP BY mov.Codigo')
    Left = 7
    Top = 11
    object QrMoviVCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrMoviVTotal: TFloatField
      FieldName = 'Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVNOMECLI: TWideStringField
      FieldName = 'NOMECLI'
      Size = 100
    end
    object QrMoviVNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
    object QrMoviVNOMESTAT: TWideStringField
      FieldName = 'NOMESTAT'
      Size = 100
    end
    object QrMoviVDataPedi: TDateField
      FieldName = 'DataPedi'
    end
    object QrMoviVDataReal: TDateField
      FieldName = 'DataReal'
    end
    object QrMoviVDescon: TFloatField
      FieldName = 'Descon'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVFrete: TFloatField
      FieldName = 'Frete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviVTOTALVDA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALVDA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMoviVNOMECAD2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECAD2'
      Calculated = True
    end
    object QrMoviVPago: TFloatField
      FieldName = 'Pago'
    end
    object QrMoviVUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMoviVDataTXT: TWideStringField
      FieldName = 'DataTXT'
      Required = True
      Size = 10
    end
    object QrMoviVDATA_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATA_TXT'
      Size = 0
      Calculated = True
    end
    object QrMoviVDiaHora_TXT: TWideStringField
      FieldName = 'DiaHora_TXT'
      Size = 8
    end
    object QrMoviVControle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrVendedor: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT sen.Numero, CASE WHEN ent.Tipo=0 '
      'THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT'
      'FROM senhas sen'
      'LEFT JOIN entidades ent ON ent.Codigo = sen.Funcionario'
      'WHERE sen.Numero > 0 '
      'ORDER BY NOMEENT')
    Left = 604
    Top = 9
    object QrVendedorNumero: TIntegerField
      FieldName = 'Numero'
    end
    object QrVendedorNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Size = 100
    end
  end
  object DsVendedor: TDataSource
    DataSet = QrVendedor
    Left = 632
    Top = 9
  end
  object QrStatusV: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM statusv'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 548
    Top = 9
    object QrStatusVCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrStatusVNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsStatusV: TDataSource
    DataSet = QrStatusV
    Left = 576
    Top = 9
  end
  object QrGraCusPrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Aplicacao'
      'FROM gracusprc'
      'WHERE Aplicacao & 2')
    Left = 660
    Top = 9
    object QrGraCusPrcCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGraCusPrcNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGraCusPrcAplicacao: TIntegerField
      FieldName = 'Aplicacao'
    end
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 688
    Top = 9
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMoviVCalcFields
    Left = 150
    Top = 11
  end
  object frxDsMoviV: TfrxDBDataset
    UserName = 'frxDsMoviV'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Total=Total'
      'NOMECLI=NOMECLI'
      'NOMEENT=NOMEENT'
      'NOMESTAT=NOMESTAT'
      'DataPedi=DataPedi'
      'DataReal=DataReal'
      'Descon=Descon'
      'Frete=Frete'
      'TOTALVDA=TOTALVDA'
      'NOMECAD2=NOMECAD2'
      'Pago=Pago'
      'UserCad=UserCad'
      'DataTXT=DataTXT'
      'DATA_TXT=DATA_TXT'
      'DiaHora_TXT=DiaHora_TXT'
      'Controle=Controle')
    DataSet = QrMoviV
    BCDToCurrency = False
    Left = 121
    Top = 11
  end
  object frxRelVendasDetal: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39141.750700231500000000
    ReportOptions.LastChange = 40107.574384259300000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GroupHeader1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure ReportTitle1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'begin'
      
        '  if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLogoC' +
        'aminho>);  '
      'end.')
    OnGetValue = frxRelVendasGetValue
    Left = 93
    Top = 11
    Datasets = <
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMoviM
        DataSetName = 'frxDsMoviM'
      end
      item
        DataSet = frxDsMoviV
        DataSetName = 'frxDsMoviV'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      LargeDesignHeight = True
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 71.811055350000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 56.692950000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo6: TfrxMemoView
          Left = 154.960730000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 551.811380000000000000
          Top = 18.897650000000000000
          Width = 147.401670000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 154.960730000000000000
          Top = 18.897650000000000000
          Width = 393.071120000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 147.401574800000000000
          Height = 49.133890000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object frxMemoView1: TfrxMemoView
          Left = 3.779530000000000000
          Top = 56.692950000000000000
          Width = 544.252320000000000000
          Height = 15.118105350000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 151.181200000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsMaster."Tipo"'
        object Memo2: TfrxMemoView
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Pedido')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 56.692950000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 113.385900000000000000
          Width = 132.283464570000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 245.669450000000000000
          Width = 132.283464570000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Vendedor')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 377.953000000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Status')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 472.441250000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 529.134200000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 585.827150000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Frete')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 642.520100000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 347.716760000000000000
        Width = 699.213050000000000000
        object Memo24: TfrxMemoView
          Top = 3.779530000000000000
          Width = 472.441213390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 472.441250000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviV."Total">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 529.134200000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviV."Descon">)]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 585.827150000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviV."Frete">)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 642.520100000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviV."TOTALVDA">)]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 430.866420000000000000
        Width = 699.213050000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 192.756030000000000000
        Width = 699.213050000000000000
        DataSet = frxDsMoviV
        DataSetName = 'frxDsMoviV'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataField = 'Codigo'
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviV."Codigo"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 56.692950000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataField = 'DataTXT'
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviV."DataTXT"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 113.385900000000000000
          Width = 132.283464570000000000
          Height = 18.897650000000000000
          DataField = 'NOMECLI'
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviV."NOMECLI"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 245.669450000000000000
          Width = 132.283464570000000000
          Height = 18.897650000000000000
          DataField = 'NOMEENT'
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviV."NOMEENT"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 377.953000000000000000
          Width = 94.488201180000000000
          Height = 18.897650000000000000
          DataField = 'NOMESTAT'
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviV."NOMESTAT"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 472.441250000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviV."Total"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 529.134200000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviV."Descon"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 585.827150000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviV."Frete"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 642.520100000000000000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviV."TOTALVDA"]')
          ParentFont = False
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 272.126160000000000000
        Width = 699.213050000000000000
        DataSet = frxDsMoviM
        DataSetName = 'frxDsMoviM'
        RowCount = 0
        object Memo28: TfrxMemoView
          Width = 132.283513390000000000
          Height = 15.118110240000000000
          DataField = 'NOMEGRA'
          DataSet = frxDsMoviM
          DataSetName = 'frxDsMoviM'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviM."NOMEGRA"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 132.283550000000000000
          Width = 132.283513390000000000
          Height = 15.118110240000000000
          DataField = 'NOMETAM'
          DataSet = frxDsMoviM
          DataSetName = 'frxDsMoviM'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviM."NOMETAM"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 264.567100000000000000
          Width = 113.385863390000000000
          Height = 15.118110240000000000
          DataField = 'NOMECOR'
          DataSet = frxDsMoviM
          DataSetName = 'frxDsMoviM'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsMoviM."NOMECOR"]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 377.953000000000000000
          Width = 64.251973390000000000
          Height = 15.118110240000000000
          DataField = 'POSIq'
          DataSet = frxDsMoviM
          DataSetName = 'frxDsMoviM'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviM."POSIq"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 442.205010000000000000
          Width = 64.251973390000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviM
          DataSetName = 'frxDsMoviM'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviM."PRECO"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 506.457020000000000000
          Width = 64.251973390000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviM
          DataSetName = 'frxDsMoviM'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviM."POSIv"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 570.709030000000000000
          Width = 64.251973390000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviM
          DataSetName = 'frxDsMoviM'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviM."CUSMEDUNI"]')
          ParentFont = False
        end
        object Memo43: TfrxMemoView
          Left = 634.961040000000000000
          Width = 64.251973390000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviM
          DataSetName = 'frxDsMoviM'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviM."CUSMEDTOT"]')
          ParentFont = False
        end
      end
      object GroupHeader2: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 234.330860000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsMoviM."Controle"'
        object Memo31: TfrxMemoView
          Width = 132.283513390000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Mercadoria')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 377.953000000000000000
          Width = 64.251968500000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 132.283550000000000000
          Width = 132.283513390000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 264.567100000000000000
          Width = 113.385826771653500000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Tamanho')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 442.205010000000000000
          Width = 64.251968500000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Valor Unit.')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 506.457020000000000000
          Width = 64.251968500000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Valor Tot.')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 570.709030000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Cuto M'#233'dio Unit.')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 634.961040000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Cuto M'#233'dio Tot.')
          ParentFont = False
        end
      end
      object GroupFooter2: TfrxGroupFooter
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 309.921460000000000000
        Width = 699.213050000000000000
        object Memo44: TfrxMemoView
          Left = 570.709030000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviM."CUSMEDUNI">)]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 634.961040000000000000
          Width = 64.251968500000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsMoviM."CUSMEDTOT">)]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Width = 570.708988500000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Frame.Width = 0.500000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
      end
    end
  end
  object QrMoviM: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMoviMCalcFields
    SQL.Strings = (
      'SELECT gra.Nome NOMEGRA, tam.Nome NOMETAM,'
      'cor.Nome NOMECOR, mom.*'
      'FROM movim mom'
      'LEFT JOIN grades gra ON gra.Codigo=mom.Grade'
      'LEFT JOIN tamanhos tam ON tam.Codigo=mom.Tam'
      'LEFT JOIN cores cor ON cor.Codigo=mom.Cor'
      'WHERE mom.Controle=:P0')
    Left = 179
    Top = 11
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMoviMNOMEGRA: TWideStringField
      FieldName = 'NOMEGRA'
      Size = 30
    end
    object QrMoviMNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 10
    end
    object QrMoviMNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 30
    end
    object QrMoviMControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrMoviMConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrMoviMGrade: TIntegerField
      FieldName = 'Grade'
    end
    object QrMoviMCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrMoviMTam: TIntegerField
      FieldName = 'Tam'
    end
    object QrMoviMQtd: TFloatField
      FieldName = 'Qtd'
    end
    object QrMoviMVal: TFloatField
      FieldName = 'Val'
    end
    object QrMoviMVen: TFloatField
      FieldName = 'Ven'
    end
    object QrMoviMDataPedi: TDateField
      FieldName = 'DataPedi'
    end
    object QrMoviMDataReal: TDateField
      FieldName = 'DataReal'
    end
    object QrMoviMMotivo: TSmallintField
      FieldName = 'Motivo'
    end
    object QrMoviMSALDOQTD: TFloatField
      FieldName = 'SALDOQTD'
    end
    object QrMoviMSALDOVAL: TFloatField
      FieldName = 'SALDOVAL'
    end
    object QrMoviMLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMoviMDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMoviMDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMoviMUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMoviMUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMoviMAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrMoviMAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrMoviMSubCtrl: TIntegerField
      FieldName = 'SubCtrl'
    end
    object QrMoviMComisTip: TSmallintField
      FieldName = 'ComisTip'
    end
    object QrMoviMComisFat: TFloatField
      FieldName = 'ComisFat'
    end
    object QrMoviMComisVal: TFloatField
      FieldName = 'ComisVal'
    end
    object QrMoviMSubCta: TIntegerField
      FieldName = 'SubCta'
    end
    object QrMoviMKit: TIntegerField
      FieldName = 'Kit'
    end
    object QrMoviMIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
    object QrMoviMPRECO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMoviMPOSIq: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIq'
      DisplayFormat = '#,###,##0.000'
      Calculated = True
    end
    object QrMoviMPOSIv: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIv'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMoviMCUSMEDUNI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSMEDUNI'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMoviMCUSMEDTOT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CUSMEDTOT'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object frxDsMoviM: TfrxDBDataset
    UserName = 'frxDsMoviM'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMEGRA=NOMEGRA'
      'NOMETAM=NOMETAM'
      'NOMECOR=NOMECOR'
      'Controle=Controle'
      'Conta=Conta'
      'Grade=Grade'
      'Cor=Cor'
      'Tam=Tam'
      'Qtd=Qtd'
      'Val=Val'
      'Ven=Ven'
      'DataPedi=DataPedi'
      'DataReal=DataReal'
      'Motivo=Motivo'
      'SALDOQTD=SALDOQTD'
      'SALDOVAL=SALDOVAL'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'SubCtrl=SubCtrl'
      'ComisTip=ComisTip'
      'ComisFat=ComisFat'
      'ComisVal=ComisVal'
      'SubCta=SubCta'
      'Kit=Kit'
      'IDCtrl=IDCtrl'
      'PRECO=PRECO'
      'POSIq=POSIq'
      'POSIv=POSIv'
      'CUSMEDUNI=CUSMEDUNI'
      'CUSMEDTOT=CUSMEDTOT')
    DataSet = QrMoviM
    BCDToCurrency = False
    Left = 208
    Top = 11
  end
  object PMImprime: TPopupMenu
    Left = 184
    Top = 400
    object Sinttico1: TMenuItem
      Caption = '&Sint'#233'tico'
      OnClick = Sinttico1Click
    end
    object Sintticocomcustomdio1: TMenuItem
      Caption = 'Sint'#233'tico com &custo m'#233'dio'
      OnClick = Sintticocomcustomdio1Click
    end
    object Analtico1: TMenuItem
      Caption = '&Anal'#237'tico'
      OnClick = Analtico1Click
    end
  end
  object QrLoc2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle'
      'FROM movim'
      'WHERE Grade=:P0'
      'AND Cor=:P1'
      'AND Tam=:P2'
      'AND Val > 0'
      'AND Qtd > 0 '
      'AND DataReal > 0'
      'ORDER BY DataReal DESC'
      'LIMIT 4')
    Left = 228
    Top = 225
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrLoc2Controle: TIntegerField
      FieldName = 'Controle'
    end
  end
  object QrCustoMedio: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Val) / SUM(Qtd)CUSTOMEDIO'
      'FROM movim'
      'WHERE Grade=:P0'
      'AND Cor=:P1'
      'AND Tam=:P2'
      'AND Val > 0'
      'AND Qtd > 0 '
      'AND DataReal > 0'
      'AND Controle > :P3')
    Left = 256
    Top = 225
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrCustoMedioCUSTOMEDIO: TFloatField
      FieldName = 'CUSTOMEDIO'
    end
  end
  object QrLocMovV: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT *'
      'FROM relvdatmp')
    Left = 352
    Top = 265
    object QrLocMovVCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocMovVDiaHora: TWideStringField
      FieldName = 'DiaHora'
      Size = 30
    end
    object QrLocMovVCliente: TWideStringField
      FieldName = 'Cliente'
      Size = 100
    end
    object QrLocMovVVendedor: TWideStringField
      FieldName = 'Vendedor'
      Size = 100
    end
    object QrLocMovVStatus: TWideStringField
      FieldName = 'Status'
      Size = 100
    end
    object QrLocMovVValor: TFloatField
      FieldName = 'Valor'
    end
    object QrLocMovVDesconto: TFloatField
      FieldName = 'Desconto'
    end
    object QrLocMovVFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrLocMovVTotal: TFloatField
      FieldName = 'Total'
    end
    object QrLocMovVCustoMedio: TFloatField
      FieldName = 'CustoMedio'
    end
  end
  object frxRelVendasCus: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39141.750700231500000000
    ReportOptions.LastChange = 40794.727350243050000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'procedure GroupHeader1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'procedure ReportTitle1OnAfterPrint(Sender: TfrxComponent);'
      'begin'
      'end;'
      ''
      'begin'
      
        '  if <MeuLogoExiste> = True then Picture1.LoadFromFile(<MeuLogoC' +
        'aminho>);  '
      'end.')
    OnGetValue = frxRelVendasGetValue
    Left = 324
    Top = 265
    Datasets = <
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = frxDsLocMovV
        DataSetName = 'frxDsLocMovV'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 5.000000000000000000
      BottomMargin = 5.000000000000000000
      LargeDesignHeight = True
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 71.811055350000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Shape1: TfrxShapeView
          Width = 699.213050000000000000
          Height = 56.692950000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo6: TfrxMemoView
          Left = 154.960730000000000000
          Width = 393.071120000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."NOME_ENT"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line1: TfrxLineView
          Left = 551.811380000000000000
          Top = 18.897650000000000000
          Width = 147.401670000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo14: TfrxMemoView
          Left = 154.960730000000000000
          Top = 18.897650000000000000
          Width = 393.071120000000000000
          Height = 37.795275590000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEndereco."E_ALL"]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 548.031850000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 548.031850000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Picture1: TfrxPictureView
          Left = 3.779530000000000000
          Top = 3.779530000000000000
          Width = 147.401574800000000000
          Height = 49.133890000000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object frxMemoView1: TfrxMemoView
          Left = 3.779530000000000000
          Top = 56.692950000000000000
          Width = 544.252320000000000000
          Height = 15.118105350000000000
          Visible = False
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 151.181200000000000000
        Width = 699.213050000000000000
        Condition = 'frxDsMaster."Tipo"'
        object Memo2: TfrxMemoView
          Width = 56.692913390000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Pedido')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 56.692950000000000000
          Width = 56.692913390000000000
          Height = 15.118110236220470000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Data')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 113.385900000000000000
          Width = 109.606299212598400000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Cliente')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 222.992270000000000000
          Width = 109.606299212598400000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Vendedor')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 332.598640000000000000
          Width = 83.149611180000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Status')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 415.748300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Valor')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 472.441250000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Desconto')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 529.134200000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Frete')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 585.827150000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Total')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 642.520100000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Custo m'#233'dio')
          ParentFont = False
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 18.897640240000000000
        Top = 226.771800000000000000
        Width = 699.213050000000000000
        object Memo24: TfrxMemoView
          Top = 3.779530000000000000
          Width = 415.748263390000000000
          Height = 15.118110240000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 415.748300000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLocMovV."Valor">)]')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 472.441250000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLocMovV."Desconto">)]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 529.134200000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLocMovV."Frete">)]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 585.827150000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLocMovV."Total">)]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 642.520100000000000000
          Top = 3.779530000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftTop]
          HAlign = haRight
          Memo.UTF8W = (
            '[SUM(<frxDsLocMovV."CustoMedio">)]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 306.141930000000000000
        Width = 699.213050000000000000
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 15.118110240000000000
        Top = 188.976500000000000000
        Width = 699.213050000000000000
        DataSet = frxDsLocMovV
        DataSetName = 'frxDsLocMovV'
        RowCount = 0
        object Memo1: TfrxMemoView
          Width = 56.692913390000000000
          Height = 15.118110236220470000
          DataField = 'Codigo'
          DataSet = frxDsMoviV
          DataSetName = 'frxDsMoviV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsMoviV."Codigo"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 56.692950000000000000
          Width = 56.692913390000000000
          Height = 15.118110236220470000
          DataField = 'DiaHora'
          DataSet = frxDsLocMovV
          DataSetName = 'frxDsLocMovV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsLocMovV."DiaHora"]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 113.385900000000000000
          Width = 109.606284570000000000
          Height = 15.118110240000000000
          DataField = 'Cliente'
          DataSet = frxDsLocMovV
          DataSetName = 'frxDsLocMovV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLocMovV."Cliente"]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 222.992270000000000000
          Width = 109.606284570000000000
          Height = 15.118110240000000000
          DataField = 'Vendedor'
          DataSet = frxDsLocMovV
          DataSetName = 'frxDsLocMovV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLocMovV."Vendedor"]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 332.598640000000000000
          Width = 83.149611180000000000
          Height = 15.118110240000000000
          DataField = 'Status'
          DataSet = frxDsLocMovV
          DataSetName = 'frxDsLocMovV'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsLocMovV."Status"]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 415.748300000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Valor'
          DataSet = frxDsLocMovV
          DataSetName = 'frxDsLocMovV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocMovV."Valor"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 472.441250000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Desconto'
          DataSet = frxDsLocMovV
          DataSetName = 'frxDsLocMovV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocMovV."Desconto"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 529.134200000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Frete'
          DataSet = frxDsLocMovV
          DataSetName = 'frxDsLocMovV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocMovV."Frete"]')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 585.827150000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'Total'
          DataSet = frxDsLocMovV
          DataSetName = 'frxDsLocMovV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocMovV."Total"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 642.520100000000000000
          Width = 56.692913390000000000
          Height = 15.118110240000000000
          DataField = 'CustoMedio'
          DataSet = frxDsLocMovV
          DataSetName = 'frxDsLocMovV'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsLocMovV."CustoMedio"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsLocMovV: TfrxDBDataset
    UserName = 'frxDsLocMovV'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'DiaHora=DiaHora'
      'Cliente=Cliente'
      'Vendedor=Vendedor'
      'Status=Status'
      'Valor=Valor'
      'Desconto=Desconto'
      'Frete=Frete'
      'Total=Total'
      'CustoMedio=CustoMedio')
    DataSet = QrLocMovV
    BCDToCurrency = False
    Left = 380
    Top = 265
  end
end
