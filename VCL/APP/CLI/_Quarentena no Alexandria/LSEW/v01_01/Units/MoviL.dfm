object FmMoviL: TFmMoviL
  Left = 354
  Top = 274
  Caption = 'LIN-VENDA-001 :: Venda de lingerie'
  ClientHeight = 684
  ClientWidth = 944
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PainelControle: TPanel
    Left = 0
    Top = 636
    Width = 944
    Height = 48
    Align = alBottom
    TabOrder = 0
    object LaRegistro: TdmkLabel
      Left = 173
      Top = 1
      Width = 301
      Height = 46
      Align = alClient
      Caption = '[N]: 0'
      UpdType = utYes
      SQLType = stNil
      ExplicitWidth = 26
      ExplicitHeight = 13
    end
    object Panel5: TPanel
      Left = 1
      Top = 1
      Width = 172
      Height = 46
      Align = alLeft
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 0
      object SpeedButton4: TBitBtn
        Tag = 4
        Left = 128
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'ltimo registro'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = SpeedButton4Click
      end
      object SpeedButton3: TBitBtn
        Tag = 3
        Left = 88
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Pr'#243'ximo registro'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = SpeedButton3Click
      end
      object SpeedButton2: TBitBtn
        Tag = 2
        Left = 48
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Registro anterior'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = SpeedButton2Click
      end
      object SpeedButton1: TBitBtn
        Tag = 1
        Left = 8
        Top = 4
        Width = 40
        Height = 40
        Cursor = crHandPoint
        Hint = 'Primeiro registro'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 3
        OnClick = SpeedButton1Click
      end
    end
    object Panel3: TPanel
      Left = 474
      Top = 1
      Width = 469
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      ParentColor = True
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 372
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
      object BtPagtos: TBitBtn
        Tag = 187
        Left = 96
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Pagtos'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
        OnClick = BtPagtosClick
      end
      object BtVenda: TBitBtn
        Tag = 303
        Left = 4
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Venda'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 2
        OnClick = BtVendaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 944
    Height = 48
    Align = alTop
    Caption = 'Venda de mercadorias'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TLabel
      Left = 865
      Top = 1
      Width = 78
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      ExplicitLeft = 641
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 48
      Top = 1
      Width = 817
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 52
      ExplicitTop = 3
      ExplicitWidth = 815
      ExplicitHeight = 44
    end
    object PainelBotoes: TPanel
      Left = 1
      Top = 1
      Width = 47
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 2
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 944
    Height = 588
    Align = alClient
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object PnTopo: TPanel
      Left = 1
      Top = 1
      Width = 942
      Height = 110
      Align = alTop
      TabOrder = 0
      object Panel8: TPanel
        Left = 1
        Top = 57
        Width = 940
        Height = 51
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object Panel19: TPanel
          Left = 0
          Top = 0
          Width = 325
          Height = 51
          Align = alLeft
          BevelOuter = bvNone
          TabOrder = 0
          object Label7: TLabel
            Left = 74
            Top = 27
            Width = 64
            Height = 13
            Caption = 'Pesquisa [F4]'
          end
          object Label9: TLabel
            Left = 4
            Top = 6
            Width = 62
            Height = 21
            Caption = 'Pedido:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -19
            Font.Name = 'Berlin Sans FB'
            Font.Style = []
            ParentFont = False
          end
          object DBEdCodigo: TdmkDBEdit
            Left = 74
            Top = 6
            Width = 100
            Height = 21
            DataField = 'Codigo'
            DataSource = DsMoviL
            TabOrder = 0
            OnDblClick = DBEdCodigoDblClick
            OnKeyDown = DBEdCodigoKeyDown
            UpdType = utYes
            Alignment = taLeftJustify
          end
        end
      end
      object Panel1: TPanel
        Left = 1
        Top = 1
        Width = 940
        Height = 56
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 1
        object Label1: TLabel
          Left = 216
          Top = 6
          Width = 35
          Height = 13
          Alignment = taRightJustify
          Caption = 'Cliente:'
        end
        object Label26: TLabel
          Left = 181
          Top = 31
          Width = 70
          Height = 13
          Caption = 'Lista de pre'#231'o:'
        end
        object Label84: TLabel
          Left = 674
          Top = 6
          Width = 63
          Height = 13
          Caption = 'Cadastro por:'
        end
        object Label85: TLabel
          Left = 677
          Top = 31
          Width = 60
          Height = 13
          Caption = 'Alterado por:'
        end
        object Label10: TLabel
          Left = 9
          Top = 6
          Width = 61
          Height = 13
          Caption = 'Data pedido:'
        end
        object Label3: TLabel
          Left = 9
          Top = 31
          Width = 59
          Height = 13
          Caption = 'Data venda:'
        end
        object dmkDBEdit1: TdmkDBEdit
          Left = 743
          Top = 3
          Width = 190
          Height = 21
          DataField = 'NOMECAD2'
          DataSource = DsMoviL
          TabOrder = 2
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit2: TdmkDBEdit
          Left = 743
          Top = 28
          Width = 190
          Height = 21
          DataField = 'NOMEALT2'
          DataSource = DsMoviL
          TabOrder = 5
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object EdDBDataVenda: TdmkDBEdit
          Left = 74
          Top = 29
          Width = 100
          Height = 21
          DataField = 'DATAREAL_TXT'
          DataSource = DsMoviL
          TabOrder = 3
          UpdType = utYes
          Alignment = taCenter
        end
        object EdDBDataPed: TdmkDBEdit
          Left = 74
          Top = 3
          Width = 100
          Height = 21
          DataField = 'DataPedi'
          DataSource = DsMoviL
          TabOrder = 0
          UpdType = utYes
          Alignment = taCenter
        end
        object dmkDBEdit7: TdmkDBEdit
          Left = 256
          Top = 3
          Width = 412
          Height = 21
          DataField = 'NOMEENTI'
          DataSource = DsMoviL
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit8: TdmkDBEdit
          Left = 256
          Top = 28
          Width = 412
          Height = 21
          DataField = 'NOMGRACUSPRC'
          DataSource = DsMoviL
          TabOrder = 4
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
    end
    object PnInfoCli: TPanel
      Left = 1
      Top = 111
      Width = 942
      Height = 52
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel14: TPanel
        Left = 0
        Top = 0
        Width = 684
        Height = 52
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object Label2: TLabel
          Left = 0
          Top = 0
          Width = 684
          Height = 22
          Align = alTop
          Alignment = taCenter
          Caption = 'Informa'#231#245'es sobre o cliente'
          Color = clBtnFace
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Berlin Sans FB, 14'
          Font.Style = []
          ParentColor = False
          ParentFont = False
          ExplicitWidth = 237
        end
        object Label6: TLabel
          Left = 406
          Top = 29
          Width = 75
          Height = 13
          Caption = 'Cadastrado por:'
        end
        object Label5: TLabel
          Left = 193
          Top = 29
          Width = 95
          Height = 13
          Caption = 'Valor '#250'ltima compra:'
        end
        object Label4: TLabel
          Left = 8
          Top = 29
          Width = 73
          Height = 13
          Caption = #218'ltima compra: '
        end
        object dmkDBEdit5: TdmkDBEdit
          Left = 486
          Top = 26
          Width = 190
          Height = 21
          DataField = 'NOMECAD2'
          DataSource = DsHist
          TabOrder = 2
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit4: TdmkDBEdit
          Left = 300
          Top = 26
          Width = 100
          Height = 21
          DataField = 'POSIt'
          DataSource = DsHist
          TabOrder = 1
          UpdType = utYes
          Alignment = taLeftJustify
        end
        object dmkDBEdit3: TdmkDBEdit
          Left = 83
          Top = 26
          Width = 100
          Height = 21
          DataField = 'DATAREAL_TXT'
          DataSource = DsHist
          TabOrder = 0
          UpdType = utYes
          Alignment = taLeftJustify
        end
      end
      object Panel15: TPanel
        Left = 684
        Top = 0
        Width = 258
        Height = 52
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtUltimo: TBitBtn
          Left = 134
          Top = 6
          Width = 120
          Height = 40
          Cursor = crHandPoint
          Caption = '&'#218'ltima compra'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
      end
    end
    object PCProd: TPageControl
      Left = 1
      Top = 163
      Width = 942
      Height = 368
      ActivePage = TabSheet1
      Align = alClient
      TabOrder = 2
      object TabSheet1: TTabSheet
        Caption = 'Mercadorias'
        object Panel2: TPanel
          Left = 0
          Top = 0
          Width = 678
          Height = 340
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          object DBGridProd: TdmkDBGrid
            Left = 0
            Top = 0
            Width = 678
            Height = 340
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMEGRA'
                Title.Caption = 'Mercadoria'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECOR'
                Title.Caption = 'Cor'
                Width = 130
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETAM'
                Title.Caption = 'Tam'
                Width = 90
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIq'
                Title.Caption = 'Qtde.'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRECO'
                Title.Caption = 'Venda'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIv'
                Title.Caption = 'TOTAL'
                Width = 90
                Visible = True
              end>
            Color = clWindow
            DataSource = DsMovim
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'NOMEGRA'
                Title.Caption = 'Mercadoria'
                Width = 200
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECOR'
                Title.Caption = 'Cor'
                Width = 130
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMETAM'
                Title.Caption = 'Tam'
                Width = 90
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIq'
                Title.Caption = 'Qtde.'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PRECO'
                Title.Caption = 'Venda'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'POSIv'
                Title.Caption = 'TOTAL'
                Width = 90
                Visible = True
              end>
          end
        end
        object Panel6: TPanel
          Left = 678
          Top = 0
          Width = 256
          Height = 340
          Align = alRight
          BevelOuter = bvNone
          ParentBackground = False
          TabOrder = 1
          object Panel9: TPanel
            Left = 0
            Top = 0
            Width = 256
            Height = 182
            Align = alTop
            BevelOuter = bvNone
            TabOrder = 0
            object Image2: TImage
              Left = 11
              Top = 5
              Width = 240
              Height = 170
              Stretch = True
            end
          end
          object Panel7: TPanel
            Left = 0
            Top = 182
            Width = 256
            Height = 158
            Align = alClient
            BevelOuter = bvNone
            TabOrder = 1
            object StaticText3: TStaticText
              Left = 0
              Top = 0
              Width = 256
              Height = 17
              Align = alTop
              Alignment = taCenter
              BorderStyle = sbsSunken
              Caption = 'Pagamentos'
              TabOrder = 0
            end
            object GridPagtos: TDBGrid
              Left = 0
              Top = 17
              Width = 256
              Height = 141
              Align = alClient
              DataSource = DsPagtos
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'SEQ'
                  Title.Caption = 'N'#186
                  Width = 22
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Vencimento'
                  Title.Caption = 'Vencto'
                  Width = 56
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Credito'
                  Title.Alignment = taRightJustify
                  Title.Caption = 'Valor'
                  Width = 70
                  Visible = True
                end>
            end
          end
        end
      end
    end
    object PnBottom: TPanel
      Left = 1
      Top = 531
      Width = 942
      Height = 56
      Align = alBottom
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      object Label22: TLabel
        Left = 333
        Top = 8
        Width = 49
        Height = 13
        Caption = 'Desconto:'
      end
      object Label25: TLabel
        Left = 177
        Top = 8
        Width = 27
        Height = 13
        Caption = 'Frete:'
      end
      object Label24: TLabel
        Left = 7
        Top = 32
        Width = 25
        Height = 13
        Caption = 'Obs.:'
      end
      object Label21: TLabel
        Left = 7
        Top = 8
        Width = 42
        Height = 13
        Caption = 'Subtotal:'
      end
      object Label18: TLabel
        Left = 517
        Top = 8
        Width = 28
        Height = 13
        Caption = 'Pago:'
      end
      object Panel4: TPanel
        Left = 764
        Top = 1
        Width = 177
        Height = 54
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 5
        object DBText1: TDBText
          Left = 71
          Top = 16
          Width = 100
          Height = 22
          Alignment = taRightJustify
          DataField = 'TOTALVDA'
          DataSource = DsMoviL
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clGreen
          Font.Height = -19
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
        end
        object Label23: TLabel
          Left = 2
          Top = 16
          Width = 62
          Height = 21
          Caption = 'TOTAL:'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -19
          Font.Name = 'Berlin Sans FB'
          Font.Style = []
          ParentFont = False
        end
      end
      object dmkDBEdit11: TdmkDBEdit
        Left = 55
        Top = 4
        Width = 100
        Height = 21
        DataField = 'Total'
        DataSource = DsMoviL
        TabOrder = 0
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit6: TdmkDBEdit
        Left = 548
        Top = 4
        Width = 100
        Height = 21
        DataField = 'Pago'
        DataSource = DsMoviL
        TabOrder = 3
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit9: TdmkDBEdit
        Left = 208
        Top = 4
        Width = 100
        Height = 21
        DataField = 'Frete'
        DataSource = DsMoviL
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit10: TdmkDBEdit
        Left = 388
        Top = 4
        Width = 100
        Height = 21
        DataField = 'Descon'
        DataSource = DsMoviL
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit12: TdmkDBEdit
        Left = 38
        Top = 29
        Width = 610
        Height = 21
        DataField = 'Observ'
        DataSource = DsMoviL
        TabOrder = 4
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
  end
  object QrMoviL: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrMoviLAfterOpen
    BeforeClose = QrMoviLBeforeClose
    AfterScroll = QrMoviLAfterScroll
    OnCalcFields = QrMoviLCalcFields
    SQL.Strings = (
      'SELECT prc.Nome NOMGRACUSPRC,'
      'CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTI, (moc.Frete * -1)FRETEA, moc.* '
      'FROM movil moc'
      'LEFT JOIN entidades ent ON ent.Codigo=moc.Cliente'
      'LEFT JOIN gracusprc prc ON prc.Codigo = moc.GraCusPrc'
      'WHERE moc.Codigo > 0')
    Left = 64
    Top = 352
    object QrMoviLDATAREAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATAREAL_TXT'
      Size = 10
      Calculated = True
    end
    object QrMoviLNOMGRACUSPRC: TWideStringField
      FieldName = 'NOMGRACUSPRC'
      Origin = 'gracusprc.Nome'
      Size = 30
    end
    object QrMoviLNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
    object QrMoviLFRETEA: TFloatField
      FieldName = 'FRETEA'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviLCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'movil.Codigo'
    end
    object QrMoviLControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'movil.Controle'
    end
    object QrMoviLDataPedi: TDateField
      FieldName = 'DataPedi'
      Origin = 'movil.DataPedi'
    end
    object QrMoviLDataReal: TDateField
      FieldName = 'DataReal'
      Origin = 'movil.DataReal'
    end
    object QrMoviLCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'movil.Cliente'
    end
    object QrMoviLTotal: TFloatField
      FieldName = 'Total'
      Origin = 'movil.Total'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviLPago: TFloatField
      FieldName = 'Pago'
      Origin = 'movil.Pago'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviLLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'movil.Lk'
    end
    object QrMoviLDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'movil.DataCad'
    end
    object QrMoviLDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'movil.DataAlt'
    end
    object QrMoviLUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'movil.UserCad'
    end
    object QrMoviLUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'movil.UserAlt'
    end
    object QrMoviLAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'movil.AlterWeb'
    end
    object QrMoviLAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'movil.Ativo'
    end
    object QrMoviLGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
      Origin = 'movil.GraCusPrc'
    end
    object QrMoviLTransportadora: TIntegerField
      FieldName = 'Transportadora'
      Origin = 'movil.Transportadora'
    end
    object QrMoviLFrete: TFloatField
      FieldName = 'Frete'
      Origin = 'movil.Frete'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviLObserv: TWideStringField
      FieldName = 'Observ'
      Origin = 'movil.Observ'
      Size = 255
    end
    object QrMoviLPOSIt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIt'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMoviLDescon: TFloatField
      FieldName = 'Descon'
      Origin = 'movil.Descon'
      DisplayFormat = '#,###,##0.00'
    end
    object QrMoviLNOMECAD2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECAD2'
      Size = 100
      Calculated = True
    end
    object QrMoviLNOMEALT2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMEALT2'
      Size = 100
      Calculated = True
    end
    object QrMoviLNOMECAD: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMECAD'
      LookupDataSet = QrSenhas
      LookupKeyFields = 'Numero'
      LookupResultField = 'Login'
      KeyFields = 'UserCad'
      Size = 100
      Lookup = True
    end
    object QrMoviLNOMEALT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMEALT'
      LookupDataSet = QrSenhas
      LookupKeyFields = 'Numero'
      LookupResultField = 'Login'
      KeyFields = 'UserAlt'
      Size = 100
      Lookup = True
    end
    object QrMoviLTOTALVDA: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTALVDA'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsMoviL: TDataSource
    DataSet = QrMoviL
    Left = 92
    Top = 352
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtVenda
    CanUpd01 = BtPagtos
    Left = 36
    Top = 352
  end
  object QrTotMovim: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(mom.Ven) TOTITENS'
      'FROM moviv mov'
      'LEFT JOIN movim mom ON mom.Controle=mov.Controle'
      'WHERE mom.Controle=:P0'
      'AND mom.Kit=0')
    Left = 232
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotMovimTOTITENS: TFloatField
      FieldName = 'TOTITENS'
    end
  end
  object DsTotMovim: TDataSource
    DataSet = QrTotMovim
    Left = 260
    Top = 352
  end
  object PMPagtos: TPopupMenu
    Left = 180
    Top = 436
    object IncluiPagtos1: TMenuItem
      Caption = '&Inclui'
      OnClick = IncluiPagtos1Click
    end
    object ExcluiPagtos1: TMenuItem
      Caption = '&Exclui'
      OnClick = ExcluiPagtos1Click
    end
  end
  object PMVenda: TPopupMenu
    Left = 152
    Top = 436
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
  end
  object QrSenhas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM senhas')
    Left = 344
    Top = 352
    object QrSenhasLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'senhas.Login'
      Required = True
      Size = 30
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'senhas.Numero'
    end
    object QrSenhasSenha: TWideStringField
      FieldName = 'Senha'
      Origin = 'senhas.Senha'
      Size = 30
    end
    object QrSenhasPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'senhas.Perfil'
    end
    object QrSenhasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'senhas.Lk'
    end
  end
  object DsSenhas: TDataSource
    Left = 372
    Top = 352
  end
  object QrHist: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, DataReal, Total, '
      'Frete, UserCad'
      'FROM movil'
      'WHERE Cliente=:P0'
      'AND DataReal > 0'
      'ORDER BY DataReal DESC'
      'LIMIT 1')
    Left = 288
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrHistDATAREAL_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DATAREAL_TXT'
      Size = 10
      Calculated = True
    end
    object QrHistPOSIt: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIt'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrHistTotal: TFloatField
      FieldName = 'Total'
      Origin = 'moviv.Total'
    end
    object QrHistFrete: TFloatField
      FieldName = 'Frete'
      Origin = 'moviv.Frete'
    end
    object QrHistDataReal: TDateField
      FieldName = 'DataReal'
      Origin = 'moviv.DataReal'
    end
    object QrHistNOMECAD2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECAD2'
      Size = 100
      Calculated = True
    end
    object QrHistNOMECAD: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMECAD'
      LookupDataSet = QrSenhas
      LookupKeyFields = 'Numero'
      LookupResultField = 'Login'
      KeyFields = 'UserCad'
      Size = 100
      Lookup = True
    end
    object QrHistUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'moviv.UserCad'
    end
    object QrHistCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsHist: TDataSource
    DataSet = QrHist
    Left = 316
    Top = 352
  end
  object QrMovim: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrMovimAfterScroll
    OnCalcFields = QrMovimCalcFields
    SQL.Strings = (
      'SELECT gra.Nome NOMEGRA, tam.Nome NOMETAM,'
      'cor.Nome NOMECOR, mom.*'
      'FROM movim mom'
      'LEFT JOIN grades gra ON gra.Codigo=mom.Grade'
      'LEFT JOIN tamanhos tam ON tam.Codigo=mom.Tam'
      'LEFT JOIN cores cor ON cor.Codigo=mom.Cor'
      'WHERE mom.Controle= :P0'
      'AND mom.Kit=0')
    Left = 120
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMovimNOMEGRA: TWideStringField
      FieldName = 'NOMEGRA'
      Origin = 'grades.Nome'
      Size = 30
    end
    object QrMovimNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'tamanhos.Nome'
      Size = 5
    end
    object QrMovimNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'cores.Nome'
    end
    object QrMovimControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'movim.Controle'
      Required = True
    end
    object QrMovimConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'movim.Conta'
      Required = True
    end
    object QrMovimGrade: TIntegerField
      FieldName = 'Grade'
      Origin = 'movim.Grade'
      Required = True
    end
    object QrMovimCor: TIntegerField
      FieldName = 'Cor'
      Origin = 'movim.Cor'
      Required = True
    end
    object QrMovimTam: TIntegerField
      FieldName = 'Tam'
      Origin = 'movim.Tam'
      Required = True
    end
    object QrMovimQtd: TFloatField
      FieldName = 'Qtd'
      Origin = 'movim.Qtd'
      Required = True
      DisplayFormat = '#,###,##0.000'
    end
    object QrMovimVal: TFloatField
      FieldName = 'Val'
      Origin = 'movim.Val'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMovimLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'movim.Lk'
    end
    object QrMovimDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'movim.DataCad'
    end
    object QrMovimDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'movim.DataAlt'
    end
    object QrMovimUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'movim.UserCad'
    end
    object QrMovimUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'movim.UserAlt'
    end
    object QrMovimDataPedi: TDateField
      FieldName = 'DataPedi'
      Origin = 'movim.DataPedi'
      Required = True
    end
    object QrMovimDataReal: TDateField
      FieldName = 'DataReal'
      Origin = 'movim.DataReal'
      Required = True
    end
    object QrMovimPRECO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMovimMotivo: TSmallintField
      FieldName = 'Motivo'
      Origin = 'movim.Motivo'
      Required = True
    end
    object QrMovimVen: TFloatField
      FieldName = 'Ven'
      Origin = 'movim.Ven'
      Required = True
    end
    object QrMovimPOSIq: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIq'
      DisplayFormat = '#,###,##0.000'
      Calculated = True
    end
    object QrMovimPOSIv: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIv'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMovimVAZIO: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'VAZIO'
      Calculated = True
    end
  end
  object DsMovim: TDataSource
    DataSet = QrMovim
    Left = 148
    Top = 352
  end
  object QrTotPagtos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) Credito'
      'FROM FTabLctALS'
      'WHERE FatID=810'
      'AND FatNum=:P0')
    Left = 400
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotPagtosCredito: TFloatField
      FieldName = 'Credito'
    end
  end
  object QrTotProd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Ven) Total'
      'FROM movim'
      'WHERE Controle=:P0'
      'AND Kit=0')
    Left = 432
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotProdTotal: TFloatField
      FieldName = 'Total'
    end
  end
  object QrPagtos: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrPagtosCalcFields
    SQL.Strings = (
      'SELECT la.Data, la.Vencimento, la.Credito, la.Banco, '
      'la.Agencia, la.FatID, la.ContaCorrente, la.Documento, '
      'la.Descricao, la.FatParcela, la.FatNum, ca.Nome NOMECARTEIRA, '
      'ca.Nome2 NOMECARTEIRA2, ca.Banco1, ca.Agencia1, ca.Conta1, '
      'ca.TipoDoc, la.Controle, ca.Tipo CARTEIRATIPO'
      'FROM FTabLctALS la'
      'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira'
      'WHERE FatID=810'
      'AND FatNum=:P0'
      'ORDER BY la.FatParcela, la.Vencimento')
    Left = 176
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPagtosSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrPagtosData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagtosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrPagtosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPagtosBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrPagtosFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrPagtosContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrPagtosDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrPagtosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrPagtosFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrPagtosFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrPagtosNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrPagtosNOMECARTEIRA2: TWideStringField
      FieldName = 'NOMECARTEIRA2'
      Size = 100
    end
    object QrPagtosBanco1: TIntegerField
      FieldName = 'Banco1'
    end
    object QrPagtosAgencia1: TIntegerField
      FieldName = 'Agencia1'
    end
    object QrPagtosConta1: TWideStringField
      FieldName = 'Conta1'
      Size = 15
    end
    object QrPagtosTipoDoc: TSmallintField
      FieldName = 'TipoDoc'
    end
    object QrPagtosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrPagtosCARTEIRATIPO: TIntegerField
      FieldName = 'CARTEIRATIPO'
      Required = True
    end
    object QrPagtosAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object DsPagtos: TDataSource
    DataSet = QrPagtos
    Left = 204
    Top = 352
  end
  object QrFutNeg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gra.Nome NOMEGRADE, cor.Nome NOMECOR, tam.Nome NOMETAM, '
      'mom.Conta, mom.Grade, mom.Cor, mom.Tam, '
      'mom.Qtd, mom.Val, pro.EstqQ, pro.EstqV, '
      'pro.EstqQ + mom.Qtd FutQtd,'
      'pro.EstqV + mom.Val FutVal'
      'FROM movim mom'
      'LEFT JOIN produtos pro ON'
      '  pro.Codigo=mom.Grade AND'
      '  pro.Cor=mom.Cor AND'
      '  pro.Tam=mom.Tam'
      'LEFT JOIN grades gra ON gra.Codigo=mom.Grade'
      'LEFT JOIN cores  cor ON cor.Codigo=mom.Cor'
      'LEFT JOIN tamanhos tam ON tam.Codigo=mom.Tam'
      'WHERE mom.Controle=:P0'
      'AND (pro.EstqQ + mom.Qtd) < 0')
    Left = 461
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFutNegConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrFutNegGrade: TIntegerField
      FieldName = 'Grade'
      Required = True
    end
    object QrFutNegCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrFutNegTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrFutNegQtd: TFloatField
      FieldName = 'Qtd'
      Required = True
    end
    object QrFutNegVal: TFloatField
      FieldName = 'Val'
      Required = True
    end
    object QrFutNegEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrFutNegEstqV: TFloatField
      FieldName = 'EstqV'
    end
    object QrFutNegFutQtd: TFloatField
      FieldName = 'FutQtd'
    end
    object QrFutNegFutVal: TFloatField
      FieldName = 'FutVal'
    end
    object QrFutNegNOMEGRADE: TWideStringField
      FieldName = 'NOMEGRADE'
      Size = 30
    end
    object QrFutNegNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
    end
    object QrFutNegNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 5
    end
  end
  object frxDsFutNeg: TfrxDBDataset
    UserName = 'frxDsFutNeg'
    CloseDataSource = False
    DataSet = QrFutNeg
    BCDToCurrency = False
    Left = 96
    Top = 436
  end
  object frxFutNeg: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39140.346533923600000000
    ReportOptions.LastChange = 39140.346533923600000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 124
    Top = 436
    Datasets = <
      item
        DataSet = frxDsFutNeg
        DataSetName = 'frxDsFutNeg'
      end
      item
        DataSet = frxMoviL
        DataSetName = 'frxMoviL'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 45.354360000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo1: TfrxMemoView
          Left = 37.795300000000000000
          Top = 11.338590000000000000
          Width = 718.110700000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            
              'Mercadorias que ficariam com estoque negativo (Pedido n'#186' [frxMov' +
              'iL."Codigo"])')
          ParentFont = False
        end
      end
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 83.149660000000000000
        Width = 793.701300000000000000
        object Memo2: TfrxMemoView
          Left = 37.795300000000000000
          Width = 302.362400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Mercadoria')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 340.157700000000000000
          Width = 151.181200000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 491.338900000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            'Tamanho')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Estq. Atual')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 680.315400000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            'Estq. Futuro')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 158.740260000000000000
        Width = 793.701300000000000000
        DataSet = frxDsFutNeg
        DataSetName = 'frxDsFutNeg'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DataField = 'Grade'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%4.4d'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFutNeg."Grade"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 75.590600000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          DataField = 'NOMEGRADE'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFutNeg."NOMEGRADE"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DataField = 'Cor'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%4.4d'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFutNeg."Cor"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 377.953000000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DataField = 'NOMECOR'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFutNeg."NOMECOR"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 491.338900000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DataField = 'Tam'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%4.4d'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFutNeg."Tam"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'NOMETAM'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Memo.UTF8W = (
            '[frxDsFutNeg."NOMETAM"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'EstqQ'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFutNeg."EstqQ"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 680.315400000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'FutQtd'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFutNeg."FutQtd"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 75.590600000000000000
        Top = 234.330860000000000000
        Width = 793.701300000000000000
        object Memo107: TfrxMemoView
          Left = 563.149970000000000000
          Top = 11.338590000000010000
          Width = 192.756030000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
  end
  object frxMoviL: TfrxDBDataset
    UserName = 'frxMoviL'
    CloseDataSource = False
    FieldAliases.Strings = (
      'DATAREAL_TXT=DATAREAL_TXT'
      'NOMGRACUSPRC=NOMGRACUSPRC'
      'NOMEENTI=NOMEENTI'
      'FRETEA=FRETEA'
      'Codigo=Codigo'
      'Controle=Controle'
      'DataPedi=DataPedi'
      'DataReal=DataReal'
      'Cliente=Cliente'
      'Total=Total'
      'Pago=Pago'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'GraCusPrc=GraCusPrc'
      'Transportadora=Transportadora'
      'Frete=Frete'
      'Observ=Observ'
      'POSIt=POSIt'
      'Descon=Descon'
      'NOMECAD2=NOMECAD2'
      'NOMEALT2=NOMEALT2'
      'NOMECAD=NOMECAD'
      'NOMEALT=NOMEALT'
      'TOTALVDA=TOTALVDA')
    DataSet = QrMoviL
    BCDToCurrency = False
    Left = 68
    Top = 436
  end
  object QrMovim2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mom.Grade, mom.Cor, mom.Tam, '
      'mom.Val, mom.Qtd, mom.Conta'
      'FROM movim mom'
      'WHERE mom.Controle= :P0'
      '/*AND mom.Kit=0*/')
    Left = 489
    Top = 352
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMovim2Grade: TIntegerField
      FieldName = 'Grade'
      Origin = 'movim.Grade'
      Required = True
    end
    object QrMovim2Cor: TIntegerField
      FieldName = 'Cor'
      Origin = 'movim.Cor'
      Required = True
    end
    object QrMovim2Tam: TIntegerField
      FieldName = 'Tam'
      Origin = 'movim.Tam'
      Required = True
    end
    object QrMovim2Val: TFloatField
      FieldName = 'Val'
      Origin = 'movim.Val'
      Required = True
    end
    object QrMovim2Qtd: TFloatField
      FieldName = 'Qtd'
      Origin = 'movim.Qtd'
      Required = True
    end
    object QrMovim2Conta: TIntegerField
      FieldName = 'Conta'
      Origin = 'movim.Conta'
      Required = True
    end
  end
  object QrLocFoto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT fot.Nome NOMEFOTO'
      'FROM produtos prd'
      'LEFT JOIN fotos fot ON fot.Codigo = prd.Foto'
      'WHERE prd.Codigo=:P0'
      'AND prd.Cor=:P1'
      'AND prd.Tam=:P2')
    Left = 772
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrLocFotoNOMEFOTO: TWideStringField
      FieldName = 'NOMEFOTO'
      Size = 255
    end
  end
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 415
    Top = 9
  end
end
