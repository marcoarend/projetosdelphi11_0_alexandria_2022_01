object FmMoviVPagto: TFmMoviVPagto
  Left = 339
  Top = 185
  Caption = 'PRD-VENDA-004 :: Venda pagtos'
  ClientHeight = 273
  ClientWidth = 418
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 225
    Width = 418
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 15
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 306
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 6
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 418
    Height = 48
    Align = alTop
    Caption = 'Vendas Pagtos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 416
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 547
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 418
    Height = 177
    Align = alClient
    TabOrder = 0
    object Label3: TLabel
      Left = 15
      Top = 8
      Width = 39
      Height = 13
      Caption = 'Carteira:'
    end
    object Label1: TLabel
      Left = 15
      Top = 48
      Width = 31
      Height = 13
      Caption = 'Conta:'
    end
    object Label2: TLabel
      Left = 15
      Top = 131
      Width = 77
      Height = 13
      Caption = 'N'#186' do envelope:'
    end
    object Label4: TLabel
      Left = 15
      Top = 90
      Width = 119
      Height = 13
      Caption = 'Condi'#231#227'o de pagamento:'
    end
    object EdCarteira: TdmkEditCB
      Left = 15
      Top = 24
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCarteira
      IgnoraDBLookupComboBox = False
    end
    object CBCarteira: TdmkDBLookupComboBox
      Left = 71
      Top = 24
      Width = 331
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsCarteiras
      TabOrder = 1
      dmkEditCB = EdCarteira
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdConta: TdmkEditCB
      Left = 15
      Top = 64
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBConta
      IgnoraDBLookupComboBox = False
    end
    object CBConta: TdmkDBLookupComboBox
      Left = 71
      Top = 64
      Width = 331
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsContas
      TabOrder = 3
      dmkEditCB = EdConta
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdEnvelope: TdmkEdit
      Left = 15
      Top = 148
      Width = 56
      Height = 21
      MaxLength = 10
      TabOrder = 6
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
      ValWarn = False
    end
    object CBCondPagto: TdmkDBLookupComboBox
      Left = 71
      Top = 106
      Width = 331
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsPediPrzCab
      TabOrder = 5
      dmkEditCB = EdCondPagto
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdCondPagto: TdmkEditCB
      Left = 15
      Top = 106
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      DBLookupComboBox = CBCondPagto
      IgnoraDBLookupComboBox = False
    end
    object EdEnvelope1: TdmkEdit
      Left = 71
      Top = 148
      Width = 121
      Height = 21
      Alignment = taRightJustify
      TabOrder = 7
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
      ValWarn = False
    end
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Codigo, car.Nome, '
      'car.ForneceI, car.Tipo'
      'FROM carteiras car'
      'LEFT JOIN carteirasu cau ON cau.Codigo = car.Codigo'
      'WHERE cau.Usuario=:P0'
      'GROUP BY car.Codigo'
      'ORDER BY car.Nome')
    Left = 12
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrCarteirasForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrCarteirasTipo: TIntegerField
      FieldName = 'Tipo'
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 40
    Top = 9
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT con.Codigo, con.Nome'
      'FROM contas con'
      'LEFT JOIN contasu cou ON cou.Codigo = con.Codigo'
      'WHERE cou.Usuario=:P0'
      'GROUP BY con.Codigo'
      'ORDER BY con.Nome')
    Left = 68
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 96
    Top = 9
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 152
    Top = 9
  end
  object QrPediPrzCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cab.Codigo, cab.CodUsu, cab.Nome'
      'FROM pediprzcab cab'
      'ORDER BY Nome')
    Left = 124
    Top = 9
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrPediPrzIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, Dias, Percent1 Percent'
      'FROM pediprzits'
      'WHERE Percent1 > 0'
      'AND Codigo=:P0'
      'ORDER BY Dias')
    Left = 380
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzItsControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrPediPrzItsDias: TIntegerField
      FieldName = 'Dias'
    end
    object QrPediPrzItsPercent: TFloatField
      FieldName = 'Percent'
    end
  end
end
