unit GraGru;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, UnGOTOy, Grids, DBGrids, ComCtrls,
  dmkEdit, dmkEditCB, DBCtrls, dmkDBLookupComboBox, DB, mySQLDbTables,
  dmkDBGrid, dmkLabel, dmkGeral, dmkPermissoes, Menus, UnInternalConsts,
  IdBaseComponent, IdComponent, IdTCPConnection, IdTCPClient, IdHTTP, JPEG,
  UnDmkEnums;

type
  TFmGraGru = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    dmkPermissoes1: TdmkPermissoes;
    PnTopo: TPanel;
    Splitter1: TSplitter;
    PnCenter: TPanel;
    PnBottom: TPanel;
    PnGradeD: TPanel;
    Splitter2: TSplitter;
    PnGradeG: TPanel;
    PnGrades: TPanel;
    Splitter3: TSplitter;
    PnGradesDados: TPanel;
    StaticText1: TStaticText;
    StaticText2: TStaticText;
    StaticText3: TStaticText;
    Panel2: TPanel;
    dmkEdGradeGPesq: TdmkEdit;
    Panel3: TPanel;
    dmkEdGradesPesq: TdmkEdit;
    Panel4: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    dmkDBGrid2: TdmkDBGrid;
    dmkDBGrid3: TdmkDBGrid;
    BtSaida: TBitBtn;
    QrGradeD: TmySQLQuery;
    DsGradeD: TDataSource;
    QrGradeDCodigo: TIntegerField;
    QrGradeDNome: TWideStringField;
    QrGradeDLk: TIntegerField;
    QrGradeDDataCad: TDateField;
    QrGradeDDataAlt: TDateField;
    QrGradeDUserCad: TIntegerField;
    QrGradeDUserAlt: TIntegerField;
    QrGradeDAlterWeb: TSmallintField;
    QrGradeDAtivo: TSmallintField;
    BtDepto: TBitBtn;
    BtGrupo: TBitBtn;
    BtGrade: TBitBtn;
    BtCor: TBitBtn;
    BtTamanho: TBitBtn;
    PMGrade: TPopupMenu;
    Incluinovagrade1: TMenuItem;
    Alteranomedagrade1: TMenuItem;
    PMCor: TPopupMenu;
    Incluinovacorgradeatual1: TMenuItem;
    Alteraacorselecionada1: TMenuItem;
    PMTam: TPopupMenu;
    Incluinovotamanhogradeatual1: TMenuItem;
    Alteratamanhoselecionado1: TMenuItem;
    PMGrupo: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    PMDepto: TPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    QrGradeG: TmySQLQuery;
    QrGradeGCodigo: TIntegerField;
    QrGradeGLk: TIntegerField;
    QrGradeGDataCad: TDateField;
    QrGradeGDataAlt: TDateField;
    QrGradeGUserCad: TIntegerField;
    QrGradeGUserAlt: TIntegerField;
    QrGradeGNome: TWideStringField;
    QrGradeGAlterWeb: TSmallintField;
    QrGradeGAtivo: TSmallintField;
    DsGradeG: TDataSource;
    QrGradeGGradeD: TIntegerField;
    QrGrades: TmySQLQuery;
    DsGrades: TDataSource;
    QrGradesCodigo: TIntegerField;
    QrGradesNome: TWideStringField;
    QrGradesGradeG: TIntegerField;
    QrGradesLk: TIntegerField;
    QrGradesDataCad: TDateField;
    QrGradesDataAlt: TDateField;
    QrGradesUserCad: TIntegerField;
    QrGradesUserAlt: TIntegerField;
    QrGradesAlterWeb: TSmallintField;
    QrGradesAtivo: TSmallintField;
    QrGradesTams: TmySQLQuery;
    QrGradesTamsCodigo: TIntegerField;
    QrGradesTamsControle: TIntegerField;
    QrGradesTamsTam: TIntegerField;
    QrGradesTamsLk: TIntegerField;
    QrGradesTamsDataCad: TDateField;
    QrGradesTamsDataAlt: TDateField;
    QrGradesTamsUserCad: TIntegerField;
    QrGradesTamsUserAlt: TIntegerField;
    QrGradesTamsNOMETAM: TWideStringField;
    DsGradesTams: TDataSource;
    QrGradesCors: TmySQLQuery;
    QrGradesCorsCodigo: TIntegerField;
    QrGradesCorsControle: TIntegerField;
    QrGradesCorsCor: TIntegerField;
    QrGradesCorsLk: TIntegerField;
    QrGradesCorsDataCad: TDateField;
    QrGradesCorsDataAlt: TDateField;
    QrGradesCorsUserCad: TIntegerField;
    QrGradesCorsUserAlt: TIntegerField;
    QrGradesCorsNOMECOR: TWideStringField;
    DsGradesCors: TDataSource;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    GradeA: TStringGrid;
    StaticText4: TStaticText;
    TabSheet2: TTabSheet;
    GradeE: TStringGrid;
    StaticText5: TStaticText;
    TabSheet3: TTabSheet;
    QrProdutos: TmySQLQuery;
    QrProdutosTam: TIntegerField;
    QrProdutosCor: TIntegerField;
    QrProdutosAtivo: TSmallintField;
    QrProdutosEstqQ: TFloatField;
    QrProdutosPrecoV: TFloatField;
    DsProdutos: TDataSource;
    DsGraCusPrc: TDataSource;
    QrGraCusPrc: TmySQLQuery;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    QrGraCusPrcNOMEMOEDA: TWideStringField;
    QrGraCusPrcSIGLAMOEDA: TWideStringField;
    dmkDBGrid4: TdmkDBGrid;
    QrLocPrc: TmySQLQuery;
    QrLocPrcCustoPreco: TFloatField;
    QrGraGruVal: TmySQLQuery;
    QrGraGruValCustoPreco: TFloatField;
    QrGraGruValTam: TIntegerField;
    QrGraGruValCor: TIntegerField;
    QrGraGruValAtivo: TSmallintField;
    QrLocPrcControle: TIntegerField;
    TabSheet4: TTabSheet;
    TabSheet5: TTabSheet;
    GradeX: TStringGrid;
    GradeC: TStringGrid;
    StaticText7: TStaticText;
    DsGraGruVal: TDataSource;
    Panel5: TPanel;
    StaticText6: TStaticText;
    GradeP: TStringGrid;
    QrLoc: TmySQLQuery;
    StaticText8: TStaticText;
    Excluigradedemercadoria1: TMenuItem;
    QrGradesCodUsu: TIntegerField;
    IdHTTP1: TIdHTTP;
    Panel1: TPanel;
    StaticText9: TStaticText;
    Image2: TImage;
    StaticText10: TStaticText;
    BtFotos: TBitBtn;
    Splitter4: TSplitter;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtTamanhoClick(Sender: TObject);
    procedure BtCorClick(Sender: TObject);
    procedure BtGradeClick(Sender: TObject);
    procedure BtGrupoClick(Sender: TObject);
    procedure BtDeptoClick(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure QrGradeDAfterScroll(DataSet: TDataSet);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure Incluinovagrade1Click(Sender: TObject);
    procedure Alteranomedagrade1Click(Sender: TObject);
    procedure QrGradeGAfterScroll(DataSet: TDataSet);
    procedure Incluinovotamanhogradeatual1Click(Sender: TObject);
    procedure Alteratamanhoselecionado1Click(Sender: TObject);
    procedure Incluinovacorgradeatual1Click(Sender: TObject);
    procedure Alteraacorselecionada1Click(Sender: TObject);
    procedure QrGradesAfterScroll(DataSet: TDataSet);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeAClick(Sender: TObject);
    procedure GradeEDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradePDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradePClick(Sender: TObject);
    procedure QrGradeDBeforeClose(DataSet: TDataSet);
    procedure QrGradeGBeforeClose(DataSet: TDataSet);
    procedure QrGradesBeforeClose(DataSet: TDataSet);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure QrGraCusPrcAfterScroll(DataSet: TDataSet);
    procedure dmkEdGradeGPesqChange(Sender: TObject);
    procedure dmkEdGradesPesqChange(Sender: TObject);
    procedure GradeCKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Excluigradedemercadoria1Click(Sender: TObject);
    procedure GradeCClick(Sender: TObject);
    procedure BtFotosClick(Sender: TObject);
  private
    { Private declarations }
    FGradeD, FGradeG : Integer;
    //procedure ReopenGradesTams;
    //procedure ReopenGradesCors;
    //procedure ReopenProdutos;
    procedure CarregaFoto(Produto: String);
  public
    { Public declarations }
    procedure ReopenGradeG(Codigo, GradeD: Integer);
    procedure ReopenGrades(GradeG, Codigo: Integer);
    function PrecoItemCorTam(GridP, GridC, GridX: TStringGrid;
             QrGraCusPrcU: TmySQLQuery): Boolean;
    function PrecoVariosItensCorTam(GridP, GridA, GridC, GridX: TStringGrid;
             QrGraCusPrcU: TmySQLQuery): Boolean;
    function ObtemPreco(var Preco: Double): Boolean;
    function StatusVariosItensCorTam(GridA, GridC, GridX: TStringGrid): Boolean;
    function StatusItemCorTam(GridA, GridC, GridX: TStringGrid): Boolean;
  end;

  var
  FmGraGru: TFmGraGru;

implementation

uses Module, UMySQLModule, MyDBCheck, GraGruDepto, GraGruGrupo, GraGruProd,
  GraGruTam, GraGruCor, MyVCLSkin, ModuleProd, GraGruVal, Principal, UnMyObjects;

{$R *.DFM}


procedure TFmGraGru.Alteraacorselecionada1Click(Sender: TObject);
begin
  if QrGrades.RecordCount > 0 then
  begin
    if DBCheck.CriaFm(TFmGraGruCor, FmGraGruCor, afmoNegarComAviso) then
    begin
      FmGraGruCor.LaTipo.Caption := CO_ALTERACAO;
      //
      FmGraGruCor.ShowModal;
      FmGraGruCor.Destroy;
      //
      DmProd.ConfigGrades1(QrGradesCodigo.Value, QrGraCusPrcCodigo.Value,
        GradeA, GradeX, GradeC, GradeP, GradeE);
      if VAR_COD <> 0 then
        QrGradesCors.Locate('Controle', VAR_COD, []);
    end;
  end else
    Application.MessageBox('Grade n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmGraGru.Alteranomedagrade1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGruProd, FmGraGruProd, afmoNegarComAviso) then
  begin
    FmGraGruProd.LaTipo.Caption := CO_ALTERACAO;
    //
    FmGraGruProd.ShowModal;
    FmGraGruProd.Destroy;
    if VAR_COD <> 0 then
      QrGrades.Locate('Codigo', VAR_COD, []);
  end;
end;

procedure TFmGraGru.Alteratamanhoselecionado1Click(Sender: TObject);
begin
  if QrGrades.RecordCount > 0 then
  begin
    if DBCheck.CriaFm(TFmGraGruTam, FmGraGruTam, afmoNegarComAviso) then
    begin
      FmGraGruTam.LaTipo.Caption := CO_ALTERACAO;
      //
      FmGraGruTam.ShowModal;
      FmGraGruTam.Destroy;
      //
      DmProd.ConfigGrades1(QrGradesCodigo.Value, QrGraCusPrcCodigo.Value,
        GradeA, GradeX, GradeC, GradeP, GradeE);
      if VAR_COD <> 0 then
        QrGradesTams.Locate('Controle', VAR_COD, []);
    end;
  end else
    Application.MessageBox('Grade n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmGraGru.BtCorClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCor, BtCor);
end;

procedure TFmGraGru.BtDeptoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMDepto, BtDepto);
end;

procedure TFmGraGru.BtGradeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMGrade, BtGrade);
end;

procedure TFmGraGru.BtGrupoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMGrupo, BtGrupo);
end;

procedure TFmGraGru.BtSaidaClick(Sender: TObject);
begin
  VAR_COD := QrGradeDCodigo.Value;
  Close;
end;

procedure TFmGraGru.BtTamanhoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMTam, BtTamanho);
end;

procedure TFmGraGru.CarregaFoto(Produto: String);
var
  ImageMem : TMemoryStream;
  ImageJpg: TJPEGImage;
begin
  if Length(Produto) > 0 then
  begin
    if (Pos('http://', LowerCase(Produto)) > 0) or (Pos('www.', LowerCase(Produto)) > 0) then
    begin
      ImageMem := TMemoryStream.Create;
      ImageJpg := TJPEGImage.Create;
      try
        try
          if (Pos('http://', LowerCase(Produto)) > 0) then
            IdHTTP1.Get(Produto, ImageMem)
          else
            IdHTTP1.Get('http://' + Produto, ImageMem);
        except on e: EIdHTTPProtocolException do
          begin
            if e.ErrorCode = 404 then // c�digo de p�gina n�o encontrada
            begin
              // N�o achou!
              Exit;
            end;
          end;
        end;
        ImageMem.Position := 0;
        ImageJpg.LoadFromStream(ImageMem);
        Image2.Picture.Assign(ImageJpg);
      finally
        ImageJpg.Free;
        ImageMem.Free;
      end;
    end else
    begin
      if FmPrincipal.CarregaImg(Produto) then
        Image2.Picture.LoadFromFile(Produto)
      else
        Image2.Picture := nil;
    end;
  end else
    Image2.Picture := nil;
end;

procedure TFmGraGru.dmkEdGradeGPesqChange(Sender: TObject);
begin
  ReopenGradeG(0, 0);
end;

procedure TFmGraGru.dmkEdGradesPesqChange(Sender: TObject);
begin
  ReopenGrades(0, 0);
end;

procedure TFmGraGru.Excluigradedemercadoria1Click(Sender: TObject);
var
  Grade: Integer;
begin
  if QrGrades.RecordCount > 0 then
  begin
    Grade := QrGradesCodigo.Value;
    //
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT *');
    QrLoc.SQL.Add('FROM movim');
    QrLoc.SQL.Add('WHERE Grade=:P0');
    QrLoc.Params[0].AsInteger := Grade;
    QrLoc.Open;
    //
    if QrLoc.RecordCount > 0 then
    begin
      Application.MessageBox('Esta grade n�o pode ser excluida porque'
      +Chr(13)+Chr(10)+ 'possui venda(s) / compra(s) cadastrada(s) para ela.',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    //
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT *');
    QrLoc.SQL.Add('FROM gragruval');
    QrLoc.SQL.Add('WHERE Codigo=:P0');
    QrLoc.Params[0].AsInteger := Grade;
    QrLoc.Open;
    //
    if QrLoc.RecordCount > 0 then
    begin
      Application.MessageBox('Esta grade n�o pode ser excluida porque'
      +Chr(13)+Chr(10)+ 'possui pre�o(s) cadastrado(s) nela.',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    //
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT EstqQ, Tam, Cor');
    QrLoc.SQL.Add('FROM produtos');
    QrLoc.SQL.Add('WHERE Codigo=:P0');
    QrLoc.Params[0].AsInteger := Grade;
    QrLoc.Open;
    //
    if (QrLoc.FieldByName('EstqQ').Value > 0) or
      (QrLoc.RecordCount > 1) then
    begin
      Geral.MB_Aviso('Esta grade n�o pode ser excluida porque' +
        sLineBreak + 'possui quantidade em estoque!');
      Exit;
    end;
    //
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT *');
    QrLoc.SQL.Add('FROM gradestams');
    QrLoc.SQL.Add('WHERE Codigo=:P0');
    QrLoc.Params[0].AsInteger := Grade;
    QrLoc.Open;
    //
    if QrLoc.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Esta grade n�o pode ser excluida porque'
        + sLineBreak + 'existe(m) tamanho(s) cadastrado(s) nela.');
      Exit;
    end;
    //
    QrLoc.Close;
    QrLoc.SQL.Clear;
    QrLoc.SQL.Add('SELECT *');
    QrLoc.SQL.Add('FROM gradescors');
    QrLoc.SQL.Add('WHERE Codigo=:P0');
    QrLoc.Params[0].AsInteger := Grade;
    QrLoc.Open;
    //
    if QrLoc.RecordCount > 0 then
    begin
      Geral.MB_Aviso('Esta grade n�o pode ser excluida porque' + sLineBreak +
        'existe(m) cor(es) cadastrada(s) nela.');
      Exit;
    end;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM grades WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Grade;
    Dmod.QrUpd.ExecSQL;
    //
    ReopenGradeG(QrGradeGCodigo.Value, 0);
  end else
  begin
    Application.MessageBox('N�o foi localizado nenhuma grade.',
      'Aviso', MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmGraGru.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmGraGru.FormCreate(Sender: TObject);
var
  Proxy: Integer;
begin
  Proxy := Geral.ReadAppKey('Proxy', Application.Title, ktInteger, 0,
    HKEY_LOCAL_MACHINE);
  if Proxy = 1 then
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := MLAGeral.ITB(Dmod.QrControleProxBAut.Value);
    IdHTTP1.ProxyParams.ProxyPassword       := Dmod.QrControleProxPass.Value;
    IdHTTP1.ProxyParams.ProxyPort           := Dmod.QrControleProxPort.Value;
    IdHTTP1.ProxyParams.ProxyServer         := Dmod.QrControleProxServ.Value;
    IdHTTP1.ProxyParams.ProxyUsername       := Dmod.QrControleProxUser.Value;
  end else
  begin
    IdHTTP1.ProxyParams.BasicAuthentication := False;
    IdHTTP1.ProxyParams.ProxyPassword       := '';
    IdHTTP1.ProxyParams.ProxyPort           := 0;
    IdHTTP1.ProxyParams.ProxyServer         := '';
    IdHTTP1.ProxyParams.ProxyUsername       := '';
  end;
  //
  QrGradeD.Open;
  PageControl1.ActivePageIndex := 0;
end;

procedure TFmGraGru.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmGraGru.GradeAClick(Sender: TObject);
begin
  if (GradeA.Col = 0) or (GradeA.Row = 0) then
    StatusVariosItensCorTam(GradeA, GradeC, GradeX)
  else
    StatusItemCorTam(GradeA, GradeC, GradeX);
end;

procedure TFmGraGru.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Ativo: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
  begin
    with GradeA.Canvas do
    begin
      if ACol = 0 then
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          PnGradesDados.Color(*FmPrincipal.sd1.Colors[csButtonFace]*), taLeftJustify,
          GradeA.Cells[Acol, ARow], 0, 0, False)
      else
        MyObjects.DesenhaTextoEmStringGrid(GradeA, Rect, clBlack,
          PnGradesDados.Color(*FmPrincipal.sd1.Colors[csButtonFace]*), taCenter,
          GradeA.Cells[Acol, ARow], 0, 0, False);
    end;
  end else begin
    if GradeA.Cells[Acol, ARow] = '' then
      Ativo := 0
    else
      Ativo := Geral.IMV(GradeA.Cells[ACol, ARow]) + 1;
    //
    MeuVCLSkin.DrawGrid4(GradeA, Rect, 0, Ativo);
  end;
end;

procedure TFmGraGru.GradeCClick(Sender: TObject);
var
  Produto: String;
  Coluna, Linha, Prod: Integer;
begin
  Coluna := GradeC.Col;
  Linha  := GradeC.Row;
  //
  if (Coluna = 0) or (Linha = 0) then
    Exit;
  //
  Prod := Geral.IMV(GradeC.Cells[Coluna, Linha]);
  //
  QrLoc.Close;
  QrLoc.SQL.Clear;
  QrLoc.SQL.Add('SELECT fot.Nome NOMEFOTO');
  QrLoc.SQL.Add('FROM produtos prd');
  QrLoc.SQL.Add('LEFT JOIN fotos fot ON fot.Codigo = prd.Foto');
  QrLoc.SQL.Add('WHERE prd.Controle=:P0');
  QrLoc.SQL.Add('AND fot.Nome IS NOT NULL ');
  QrLoc.Params[0].AsInteger := Prod;
  QrLoc.Open;
  if QrLoc.RecordCount > 0 then
    Produto := QrLoc.FieldByName('NOMEFOTO').Value
  else
    Produto := '';
  CarregaFoto(Produto);
end;

procedure TFmGraGru.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := PnGradesDados.Color
  else
    Cor := clWindow;
  if ACol = 0 then
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taLeftJustify,
      GradeC.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeC, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taCenter,
      GradeC.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmGraGru.GradeCKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  Coluna, Linha, Controle, Grade, Tam, Cor: Integer;
begin
  if Key=VK_DELETE then
  begin
    Coluna := GradeC.Col;
    Linha  := GradeC.Row;
    Controle := Geral.IMV(GradeC.Cells[Coluna, Linha]);
    if Controle = 0 then
    begin
      Application.MessageBox('Controle n�o definido.',
      'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    //
    if Geral.MB_Pergunta('Confirma a exclus�o do �tem ' + Geral.FF0(Controle) + '?') = ID_YES
    then begin
      Grade    := QrGradesCodigo.Value;
      if Grade = 0 then
      begin
        Application.MessageBox('Grade n�o definida.',
        'Aviso', MB_OK+MB_ICONWARNING);
        Exit;
      end;
      if Controle <> 0 then
      begin
        // Verifica o estoque
        QrLoc.Close;
        QrLoc.SQL.Clear;
        QrLoc.SQL.Add('SELECT EstqQ, Tam, Cor');
        QrLoc.SQL.Add('FROM produtos');
        QrLoc.SQL.Add('WHERE Controle=:P0');
        QrLoc.Params[0].AsInteger := Controle;
        QrLoc.Open;
        //
        Tam := QrLoc.FieldByName('Tam').Value;
        Cor := QrLoc.FieldByName('Cor').Value;
        if Tam = 0 then
        begin
          Application.MessageBox('Tamanho n�o definido.',
          'Aviso', MB_OK+MB_ICONWARNING);
          Exit;
        end;
        if Cor = 0 then
        begin
          Application.MessageBox('Cor n�o definida.',
          'Aviso', MB_OK+MB_ICONWARNING);
          Exit;
        end;
        //
        if (QrLoc.FieldByName('EstqQ').Value > 0) or
          (QrLoc.RecordCount > 1) then
        begin
          Geral.MB_Aviso('Este tamanho n�o pode ser excluido porque' +
            sLineBreak + 'possui quantidade em estoque.');
          Exit;
        end;
        //
        // Verifica se existe pre�o
        QrLoc.Close;
        QrLoc.SQL.Clear;
        QrLoc.SQL.Add('SELECT ggv.CustoPreco');
        QrLoc.SQL.Add('FROM gragruval ggv');
        QrLoc.SQL.Add('LEFT JOIN produtos prd ON prd.Controle=ggv.GraGruX');
        QrLoc.SQL.Add('WHERE ggv.Codigo=:P0');
        QrLoc.SQL.Add('AND ggv.GraGruX=:P1');
        QrLoc.SQL.Add('AND ggv.CustoPreco > 0');
        QrLoc.Params[0].AsInteger := Grade;
        QrLoc.Params[1].AsInteger := Controle;
        QrLoc.Open;
        //
        if QrLoc.RecordCount > 0 then
        begin
          Application.MessageBox('Este tamanho n�o pode ser excluido porque'
          +Chr(13)+Chr(10)+ 'possui pre�o(s) cadastrado(s) para ele.',
          'Aviso', MB_OK+MB_ICONWARNING);
          Exit;
        end;
        //
        // Verifica se existe compra ou venda
        QrLoc.Close;
        QrLoc.SQL.Clear;
        QrLoc.SQL.Add('SELECT Conta');
        QrLoc.SQL.Add('FROM movim');
        QrLoc.SQL.Add('WHERE Grade=:P0');
        QrLoc.SQL.Add('AND Cor=:P1');
        QrLoc.SQL.Add('AND Tam=:P2');
        QrLoc.Params[0].AsInteger := Grade;
        QrLoc.Params[1].AsInteger := Tam;
        QrLoc.Params[2].AsInteger := Cor;
        QrLoc.Open;
        //
        if QrLoc.RecordCount > 0 then
        begin
          Application.MessageBox('Este tamanho n�o pode ser excluido porque'
          +Chr(13)+Chr(10)+ 'possui venda(s) / compra(s) cadastrada(s) para ele.',
          'Aviso', MB_OK+MB_ICONWARNING);
          Exit;
        end;
        //
        // Exclui produto
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM produtos WHERE Controle=:P0');
        Dmod.QrUpd.Params[0].AsInteger := Controle;
        Dmod.QrUpd.ExecSQL;
        //
        // Exclui valor do produto
        Dmod.QrUpd.SQL.Clear;
        Dmod.QrUpd.SQL.Add('DELETE FROM gragruval WHERE GraGruX=:P0 AND Codigo=:P1');
        Dmod.QrUpd.Params[0].AsInteger := Controle;
        Dmod.QrUpd.Params[1].AsInteger := Grade;
        Dmod.QrUpd.ExecSQL;
        //
        // Verifica se pode excluir o tamanho
        QrLoc.Close;
        QrLoc.SQL.Clear;
        QrLoc.SQL.Add('SELECT *');
        QrLoc.SQL.Add('FROM produtos');
        QrLoc.SQL.Add('WHERE Codigo=:P0');
        QrLoc.SQL.Add('AND Tam=:P1');
        QrLoc.Params[0].AsInteger := Grade;
        QrLoc.Params[1].AsInteger := Tam;
        QrLoc.Open;
        //
        if QrLoc.RecordCount = 0 then
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE FROM gradestams WHERE Codigo=:P0 AND Tam=:P1');
          Dmod.QrUpd.Params[0].AsInteger := Grade;
          Dmod.QrUpd.Params[1].AsInteger := Tam;
          Dmod.QrUpd.ExecSQL;
        end;
        //
        // Verifica se pode excluir a cor
        QrLoc.Close;
        QrLoc.SQL.Clear;
        QrLoc.SQL.Add('SELECT *');
        QrLoc.SQL.Add('FROM produtos');
        QrLoc.SQL.Add('WHERE Codigo=:P0');
        QrLoc.SQL.Add('AND Cor=:P1');
        QrLoc.Params[0].AsInteger := Grade;
        QrLoc.Params[1].AsInteger := Cor;
        QrLoc.Open;
        //
        if QrLoc.RecordCount = 0 then
        begin
          Dmod.QrUpd.SQL.Clear;
          Dmod.QrUpd.SQL.Add('DELETE FROM gradescors WHERE Codigo=:P0 AND Cor=:P1');
          Dmod.QrUpd.Params[0].AsInteger := Grade;
          Dmod.QrUpd.Params[1].AsInteger := Cor;
          Dmod.QrUpd.ExecSQL;
        end;
        //
        ReopenGrades(0, Grade);
      end else
      begin
        Application.MessageBox('N�o foi localizado nenhum tamanho.',
          'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
  end;
end;

procedure TFmGraGru.GradeEDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  OldAlign: Integer;
  Cor: TColor;
begin
  if (ARow = 0) or (ACol = 0) then
  begin
    with GradeE.Canvas do
    begin
      if ACol = 0 then
        MyObjects.DesenhaTextoEmStringGrid(GradeE, Rect, clBlack,
          PnGradesDados.Color, taLeftJustify,
          GradeE.Cells[Acol, ARow], 0, 0, False)
      else
        MyObjects.DesenhaTextoEmStringGrid(GradeE, Rect, clBlack,
          PnGradesDados.Color, taCenter,
          GradeE.Cells[Acol, ARow], 0, 0, False);
    end;
  end else begin
    OldAlign := SetTextAlign(GradeE.Canvas.Handle, TA_RIGHT);
    with GradeE.Canvas do
    begin
      if Geral.IMV(GradeA.Cells[ACol, ARow]) > 0 then
        Cor := clWindowText else Cor := clSilver;
      Font.Color := Cor;
      FillRect(Rect);
      TextOut(Rect.Right - 2, Rect.Top+2,
        Geral.TFT(GradeE.Cells[Acol, ARow], 3, siNegativo));
      SetTextAlign(GradeE.Canvas.Handle, OldAlign);
    end;
  end;
end;

procedure TFmGraGru.GradePClick(Sender: TObject);
begin
  if (GradeP.Col = 0) or (GradeP.Row = 0) then
    PrecoVariosItensCorTam(GradeP, GradeA, GradeC, GradeX, QrGraCusPrc)
  else
    PrecoItemCorTam(GradeP, GradeC, GradeX, QrGraCusPrc);
end;

procedure TFmGraGru.GradePDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
var
  Cor: Integer;
begin
  if (ACol = 0) or (ARow = 0) then
    Cor := PnGradesDados.Color
  else if GradeC.Cells[ACol, ARow] <> '' then
    Cor := clWindow
  else
    Cor := clMenu;

  //

  if (ACol = 0) or (ARow = 0) then
    MyObjects.DesenhaTextoEmStringGrid(GradeP, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taLeftJustify,
      GradeP.Cells[Acol, ARow], 0, 0, False)
  else
    MyObjects.DesenhaTextoEmStringGrid(GradeP, Rect, clBlack,
      Cor(*FmPrincipal.sd1.Colors[csButtonFace]*), taRightJustify,
      GradeP.Cells[Acol, ARow], 0, 0, False);
end;

procedure TFmGraGru.Incluinovacorgradeatual1Click(Sender: TObject);
begin
  if QrGrades.RecordCount > 0 then
  begin
    if DBCheck.CriaFm(TFmGraGruCor, FmGraGruCor, afmoNegarComAviso) then
    begin
      FmGraGruCor.LaTipo.Caption := CO_INCLUSAO;
      //
      FmGraGruCor.ShowModal;
      FmGraGruCor.Destroy;
      //
      DmProd.ConfigGrades1(QrGradesCodigo.Value, QrGraCusPrcCodigo.Value,
        GradeA, GradeX, GradeC, GradeP, GradeE);
      if VAR_COD <> 0 then
        QrGradesCors.Locate('Controle', VAR_COD, []);
    end;
  end else
    Application.MessageBox('Grade n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmGraGru.Incluinovagrade1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGruProd, FmGraGruProd, afmoNegarComAviso) then
  begin
    FmGraGruProd.LaTipo.Caption             := CO_INCLUSAO;
    FmGraGruProd.dmkEdCBGradeG.ValueVariant := QrGradeGCodigo.Value;
    FmGraGruProd.dmkCBGradeG.KeyValue       := QrGradeGCodigo.Value;
    //
    FmGraGruProd.ShowModal;
    FmGraGruProd.Destroy;
    if VAR_COD <> 0 then
      QrGrades.Locate('Codigo', VAR_COD, []);
  end;
end;

procedure TFmGraGru.Incluinovotamanhogradeatual1Click(Sender: TObject);
begin
  if QrGrades.RecordCount > 0 then
  begin
    if DBCheck.CriaFm(TFmGraGruTam, FmGraGruTam, afmoNegarComAviso) then
    begin
      FmGraGruTam.LaTipo.Caption := CO_INCLUSAO;
      //
      FmGraGruTam.ShowModal;
      FmGraGruTam.Destroy;
      //
      DmProd.ConfigGrades1(QrGradesCodigo.Value, QrGraCusPrcCodigo.Value,
        GradeA, GradeX, GradeC, GradeP, GradeE);
      if VAR_COD <> 0 then
        QrGradesTams.Locate('Controle', VAR_COD, []);
    end;
  end else
    Application.MessageBox('Grade n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

procedure TFmGraGru.MenuItem1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGruGrupo, FmGraGruGrupo, afmoNegarComAviso) then
  begin
    FmGraGruGrupo.LaTipo.Caption             := CO_INCLUSAO;
    FmGraGruGrupo.dmkEdCBGradeD.ValueVariant := QrGradeDCodigo.Value;
    FmGraGruGrupo.dmkCBGradeD.KeyValue       := QrGradeDCodigo.Value;
    //
    FmGraGruGrupo.ShowModal;
    FmGraGruGrupo.Destroy;
    if VAR_COD <> 0 then
      QrGradeG.Locate('Codigo', VAR_COD, []);
  end;
end;

procedure TFmGraGru.MenuItem2Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGruGrupo, FmGraGruGrupo, afmoNegarComAviso) then
  begin
    FmGraGruGrupo.LaTipo.Caption := CO_ALTERACAO;
    //
    FmGraGruGrupo.ShowModal;
    FmGraGruGrupo.Destroy;
    if VAR_COD <> 0 then
      QrGradeG.Locate('Codigo', VAR_COD, []);
  end;
end;

procedure TFmGraGru.MenuItem3Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGruDepto, FmGraGruDepto, afmoNegarComAviso) then
  begin
    FmGraGruDepto.LaTipo.Caption := CO_INCLUSAO;
    //
    FmGraGruDepto.ShowModal;
    FmGraGruDepto.Destroy;
    if VAR_COD <> 0 then
      QrGradeD.Locate('Codigo', VAR_COD, []);
  end;
end;

procedure TFmGraGru.MenuItem4Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmGraGruDepto, FmGraGruDepto, afmoNegarComAviso) then
  begin
    FmGraGruDepto.LaTipo.Caption := CO_ALTERACAO;
    //
    FmGraGruDepto.ShowModal;
    FmGraGruDepto.Destroy;
    if VAR_COD <> 0 then
      QrGradeD.Locate('Codigo', VAR_COD, []);
  end;
end;

procedure TFmGraGru.QrGraCusPrcAfterScroll(DataSet: TDataSet);
begin
  DmProd.AtualizaGradesPrecos1(QrGradesCodigo.Value,
    QrGraCusPrcCodigo.Value, GradeP);
end;

procedure TFmGraGru.QrGradeDAfterScroll(DataSet: TDataSet);
begin
  ReopenGradeG(0, 0); 
end;

procedure TFmGraGru.QrGradeDBeforeClose(DataSet: TDataSet);
begin
  QrGradeG.Close;
end;

procedure TFmGraGru.QrGradeGAfterScroll(DataSet: TDataSet);
begin
  ReopenGrades(0, 0); 
end;

procedure TFmGraGru.QrGradeGBeforeClose(DataSet: TDataSet);
begin
  QrGrades.Close;
end;

procedure TFmGraGru.QrGradesAfterScroll(DataSet: TDataSet);
begin
  DmProd.ConfigGrades1(QrGradesCodigo.Value, QrGraCusPrcCodigo.Value,
    GradeA, GradeX, GradeC, GradeP, GradeE);
  QrGraCusPrc.Open;
end;

procedure TFmGraGru.QrGradesBeforeClose(DataSet: TDataSet);
begin
  QrGradesTams.Close;
  QrGradesCors.Close;
  QrProdutos.Close;
  QrGraCusPrc.Close;
  QrGraGruVal.Close;
  //
  MyObjects.LimpaGrade(GradeA, 0, 0, True);
  MyObjects.LimpaGrade(GradeC, 0, 0, True);
  MyObjects.LimpaGrade(GradeE, 0, 0, True);
  MyObjects.LimpaGrade(GradeP, 0, 0, True);
  MyObjects.LimpaGrade(GradeX, 0, 0, True);
end;

procedure TFmGraGru.ReopenGradeG(Codigo, GradeD: Integer);
begin
  QrGradeG.Close;
  QrGradeG.Params[0].AsInteger := QrGradeDCodigo.Value;
  QrGradeG.Params[1].AsString  := '%' + dmkEdGradeGPesq.ValueVariant + '%';
  QrGradeG.Open;
  if Codigo > 0 then
    QrGradeG.Locate('Codigo', Codigo, []);
  if GradeD > 0 then
    QrGradeG.Locate('GradeD', GradeD, [])
  else
    if FGradeD > 0 then
      QrGradeD.Locate('GradeD', FGradeD, []);
end;

procedure TFmGraGru.ReopenGrades(GradeG, Codigo: Integer);
begin
  QrGrades.Close;
  QrGrades.Params[0].AsInteger := QrGradeGCodigo.Value;
  QrGrades.Params[1].AsString  := '%' + dmkEdGradesPesq.ValueVariant + '%';
  QrGrades.Open;
  if Codigo > 0 then
    QrGrades.Locate('Codigo', Codigo, []);
  if GradeG > 0 then
    QrGrades.Locate('GradeG', GradeG, [])
  else
    if FGradeD > 0 then
      QrGrades.Locate('GradeG', FGradeG, []);
end;

{procedure TFmGraGru.ReopenGradesCors;
begin
  QrGradesCors.Close;
  QrGradesCors.Params[0].AsInteger := QrGradesCodigo.Value;
  QrGradesCors.Open;
end;}

{procedure TFmGraGru.ReopenGradesTams;
begin
  QrGradesTams.Close;
  QrGradesTams.Params[0].AsInteger := QrGradesCodigo.Value;
  QrGradesTams.Open;
end;}

{procedure TFmGraGru.ReopenProdutos;
begin
  QrProdutos.Close;
  QrProdutos.Params[0].AsInteger := QrGradesCodigo.Value;
  QrProdutos.Open;
end;}

function TFmGraGru.PrecoItemCorTam(GridP, GridC, GridX: TStringGrid;
QrGraCusPrcU: TmySQLQuery): Boolean;
var
  Controle, c, l, GraGruX, Nivel1, Lista, Coluna, Linha: Integer;
  Preco: Double;
  SQLType: TSQLType;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  c := Geral.IMV(GridX.Cells[Coluna, 0]);
  l := Geral.IMV(GridX.Cells[0, Linha]);
  GraGruX := Geral.IMV(GridC.Cells[Coluna, Linha]);
  if c = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if l = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if GraGruX = 0 then
  begin
    Application.MessageBox('O item nunca foi ativado!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (c = 0) or (l = 0) or (GraGruX = 0) then Exit;
  //
  Nivel1 := QrGradesCodigo.Value;
  Lista  := QrGraCusPrc.FieldByName('Codigo').AsInteger;
  Preco  := Geral.DMV(GridP.Cells[Coluna, Linha]);
  if ObtemPreco(Preco) then
  begin
    QrLocPrc.Close;
    QrLocPrc.Params[00].AsInteger := GraGruX;
    QrLocPrc.Params[01].AsInteger := Nivel1;
    QrLocPrc.Params[02].AsInteger := Lista;
    QrLocPrc.Open;
    //
    Controle := QrLocPrcControle.Value;
    if Controle = 0 then
    begin
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle',
        'gragruval', 'gragruval', 'controle');
      SQLType := stIns;
    end else SQLType := stUpd;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
    'gragrux', 'Codigo', 'gracusprc', 'custopreco'], ['Controle'], [
    GraGruX, Nivel1, Lista, Preco], [Controle], True) then
    DmProd.AtualizaGradesPrecos1(Nivel1, Lista, GridP);
  end;
  Result := True;
  Screen.Cursor := crDefault;
end;

function TFmGraGru.PrecoVariosItensCorTam(GridP, GridA, GridC, GridX:
TStringGrid; QrGraCusPrcU: TmySQLQuery): Boolean;
var
  Coluna, Linha, c, l, Controle, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL, Nivel1, Lista, GraGruX: Integer;
  Preco: Double;
  PrecoTxt, ItensTxt: String;
  SQLType: TSQLType;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  Nivel1 := QrGradesCodigo.Value;
  Lista  := QrGraCusPrc.FieldByName('Codigo').AsInteger;
  Preco  := Geral.DMV(GridP.Cells[ColI, RowI]);
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  if Ativos = 0 then
  begin
    Geral.MB_Aviso('N�o h� nenhum item com c�digo na sele��o para que se possa incluir / alterar o pre�o!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if ObtemPreco(Preco) then
  begin
    PrecoTxt := QrGraCusPrc.FieldByName('SIGLAMOEDA').AsString + ' ' +
      Geral.FFT(Preco, 2, siNegativo);
    if (Coluna = 0) and (Linha = 0) then
    begin
      ItensTxt := 'todo grupo';
    end else if Coluna = 0 then
    begin
      ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
    end else if Linha = 0 then
    begin
      ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
    end;
    if Geral.MB_Pergunta('Confirma o valor de ' + PrecoTxt + ' para ' +
      ItensTxt + '?') <> ID_YES then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    //
    for c := ColI to ColF do
      for l := RowI to RowF do
    begin
      GraGruX := Geral.IMV(GridC.Cells[c, l]);
      //
      if GraGruX > 0 then
      begin
        QrLocPrc.Close;
        QrLocPrc.Params[00].AsInteger := GraGruX;
        QrLocPrc.Params[01].AsInteger := Nivel1;
        QrLocPrc.Params[02].AsInteger := Lista;
        QrLocPrc.Open;
        //
        Controle := QrLocPrcControle.Value;
        if Controle = 0 then
        begin
          Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle',
            'gragruval', 'gragruval', 'controle');
          SQLType := stIns;
        end else SQLType := stUpd;
        //
        UMyMod.SQLInsUpd(Dmod.QrUpd, SQLType, 'gragruval', False, [
        'GraGruX', 'Codigo', 'GraCusPrc', 'CustoPreco'],
        ['Controle'], [GraGruX, Nivel1, Lista, Preco],[Controle], True);
      end;
    end;
    DmProd.AtualizaGradesPrecos1(Nivel1, Lista, GridP);
    Result := True;
  end;
  Screen.Cursor := crDefault;
end;

function TFmGraGru.ObtemPreco(var Preco: Double): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmGraGruVal, FmGraGruVal, afmoNegarComAviso) then
  begin
    FmGraGruVal.EdValor.ValueVariant := Preco;
    FmGraGruVal.ShowModal;
    Result := FmGraGruVal.FConfirmou;
    Preco := FmGraGruVal.EdValor.ValueVariant;
    FmGraGruVal.Destroy;
  end;
end;

function TFmGraGru.StatusVariosItensCorTam(GridA, GridC, GridX: TStringGrid): Boolean;
var
  Coluna, Linha, Ativo, c, l, Controle, Ativos: Integer;
  ColI, ColF, RowI, RowF, CountC, CountL: Integer;
  AtivTxt, ItensTxt: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridA.Col;
  Linha  := GridA.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridA.ColCount - 1;
    RowI := 1;
    RowF := GridA.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridA.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridA.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  //ShowMessage('Ativos: ' + IntToStr(Ativos));
  if Ativos > 0 then Ativo := 0 else Ativo := 1;
  if Ativo = 0 then AtivTxt := ' desativa��o ' else AtivTxt := 'ativa��o';
  //
  if (Coluna = 0) and (Linha = 0) then
  begin
    ItensTxt := 'todo grupo';
  end else if Coluna = 0 then
  begin
    ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
  end else if Linha = 0 then
  begin
    ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
  end;
  if Geral.MB_Pergunta('Confirma a ' + AtivTxt + ' de ' + ItensTxt + '?') <> ID_YES then
  begin
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    Controle := Geral.IMV(GridC.Cells[c, l]);
    if (Ativo = 1) and (Controle = 0) then
    begin
      Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle',
        'produtos', 'produtos', 'controle');
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('INSERT INTO produtos SET Ativo=:P0, Cor=:P1, ');
      Dmod.QrUpd.SQL.Add('Codigo=:P2, Tam=:P3, Controle=:P4');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := Ativo;
      Dmod.QrUpd.Params[01].AsInteger := Geral.IMV(GridX.Cells[0, l]);
      Dmod.QrUpd.Params[02].AsInteger := QrGradesCodigo.Value;
      Dmod.QrUpd.Params[03].AsInteger := Geral.IMV(GridX.Cells[c, 0]);
      Dmod.QrUpd.Params[04].AsInteger := Controle;
      //
      Dmod.QrUpd.ExecSQL;
    end else begin
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('UPDATE produtos SET Ativo=:P0 WHERE Controle=:P1');
      Dmod.QrUpd.SQL.Add('');
      Dmod.QrUpd.Params[00].AsInteger := Ativo;
      Dmod.QrUpd.Params[01].AsInteger := Controle;
      //
      Dmod.QrUpd.ExecSQL;
    end;
  end;
  DmProd.AtualizaGradesAtivos1(QrGradesCodigo.Value, GridA, GridC, GradeE);
  Result := True;
  Screen.Cursor := crDefault;
end;

procedure TFmGraGru.BtFotosClick(Sender: TObject);
begin
  FmPrincipal.MostraGraGruSel(QrGradesCodigo.Value, 0, 0);
  //
  ReopenGradeG(QrGradeGCodigo.Value, 0);
end;

function TFmGraGru.StatusItemCorTam(GridA, GridC, GridX: TStringGrid): Boolean;
var
  Ativo, c, l, Controle, Coluna, Linha: Integer;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Coluna := GridA.Col;
  Linha  := GridA.Row;
  Ativo := Geral.IMV(GridA.Cells[Coluna, Linha]);
  c := Geral.IMV(GridX.Cells[Coluna, 0]);
  l := Geral.IMV(GridX.Cells[0, Linha]);
  Controle := Geral.IMV(GridC.Cells[Coluna, Linha]);
  if c = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if l = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (c = 0) or (l = 0) then Exit;
  if (Ativo = 0) and (Controle = 0) then
  begin
    Ativo := 1;
    Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle',
      'produtos', 'produtos', 'Controle');
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('INSERT INTO produtos SET Ativo=:P0, Cor=:P1, ');
    Dmod.QrUpd.SQL.Add('Codigo=:P2, Tam=:P3, Controle=:P4');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsInteger := l;
    Dmod.QrUpd.Params[02].AsInteger := QrGradesCodigo.Value;
    Dmod.QrUpd.Params[03].AsInteger := c;
    Dmod.QrUpd.Params[04].AsInteger := Controle;
    //
    Dmod.QrUpd.ExecSQL;
  end else begin
    if Ativo = 1 then Ativo := 0 else Ativo := 1;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE produtos SET Ativo=:P0 WHERE Controle=:P1');
    Dmod.QrUpd.SQL.Add('');
    Dmod.QrUpd.Params[00].AsInteger := Ativo;
    Dmod.QrUpd.Params[01].AsInteger := Controle;
    //
    Dmod.QrUpd.ExecSQL;
  end;
  DmProd.AtualizaGradesAtivos1(QrGradesCodigo.Value, GridA, GridC, GradeE);
  Result := True;
  Screen.Cursor := crDefault;
end;

end.






