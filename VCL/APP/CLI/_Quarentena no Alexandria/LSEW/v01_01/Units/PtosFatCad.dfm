object FmPtosFatCad: TFmPtosFatCad
  Left = 368
  Top = 194
  Caption = 'PTO-VENDA-006 :: Faturamento de Pontos de Vendas'
  ClientHeight = 593
  ClientWidth = 1241
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  WindowState = wsMaximized
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelEdita: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 534
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 474
      Width = 1239
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      ExplicitWidth = 1238
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 10
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 1104
        Top = 1
        Width = 133
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 110
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 1239
      Height = 286
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 0
      ExplicitWidth = 1238
      object Label7: TLabel
        Left = 5
        Top = 5
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label8: TLabel
        Left = 79
        Top = 5
        Width = 73
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo: [F4]'
        FocusControl = DBEdit1
      end
      object LaCliente: TLabel
        Left = 182
        Top = 5
        Width = 98
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Ponto de venda:'
      end
      object dmkLabel1: TdmkLabel
        Left = 1078
        Top = 5
        Width = 87
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Encerramento:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label2: TLabel
        Left = 5
        Top = 54
        Width = 59
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Consultor:'
      end
      object SpeedButton5: TSpeedButton
        Left = 802
        Top = 25
        Width = 26
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object dmkLabel10: TdmkLabel
        Left = 935
        Top = 5
        Width = 54
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Abertura:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label6: TLabel
        Left = 610
        Top = 54
        Width = 51
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerente:'
      end
      object Label9: TLabel
        Left = 507
        Top = 54
        Width = 79
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '% Comiss'#227'o:'
      end
      object Label10: TLabel
        Left = 1118
        Top = 54
        Width = 79
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '% Comiss'#227'o:'
      end
      object Label12: TLabel
        Left = 832
        Top = 5
        Width = 79
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '% Comiss'#227'o:'
      end
      object Label16: TLabel
        Left = 5
        Top = 103
        Width = 50
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Carteira:'
      end
      object Label18: TLabel
        Left = 610
        Top = 103
        Width = 152
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Condi'#231#227'o de pagamento:'
      end
      object Label26: TLabel
        Left = 5
        Top = 151
        Width = 212
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Configura'#231#227'o de bloquetos padr'#227'o:'
      end
      object EdCodigo: TdmkEdit
        Left = 5
        Top = 25
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Codigo'
        UpdCampo = 'Codigo'
        UpdType = utInc
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCodUsu: TdmkEdit
        Left = 79
        Top = 25
        Width = 98
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdPontoVda: TdmkEditCB
        Left = 182
        Top = 25
        Width = 69
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdPontoVdaChange
        DBLookupComboBox = CBPontoVda
        IgnoraDBLookupComboBox = False
      end
      object CBPontoVda: TdmkDBLookupComboBox
        Left = 252
        Top = 25
        Width = 547
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsPtosCadPto
        TabOrder = 3
        dmkEditCB = EdPontoVda
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdConsultor: TdmkEditCB
        Left = 5
        Top = 74
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Consultor'
        UpdCampo = 'Consultor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBConsultor
        IgnoraDBLookupComboBox = False
      end
      object CBConsultor: TdmkDBLookupComboBox
        Left = 75
        Top = 74
        Width = 428
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsConsultores
        TabOrder = 6
        dmkEditCB = EdConsultor
        QryCampo = 'Consultor'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdEncerrou: TdmkEdit
        Left = 1078
        Top = 25
        Width = 138
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        TabOrder = 4
        FormatType = dmktfDateTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '30/12/1899 00:00:00'
        QryCampo = 'Encerrou'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdAbertura: TdmkEdit
        Left = 935
        Top = 25
        Width = 138
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        TabOrder = 7
        FormatType = dmktfDateTime
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfLong
        Texto = '30/12/1899 00:00:00'
        QryCampo = 'Abertura'
        UpdCampo = 'Abertura'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdGerente: TdmkEditCB
        Left = 610
        Top = 74
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Gerente'
        UpdCampo = 'Gerente'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGerente
        IgnoraDBLookupComboBox = False
      end
      object CBGerente: TdmkDBLookupComboBox
        Left = 681
        Top = 74
        Width = 433
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'NO_ENT'
        ListSource = DsGerentes
        TabOrder = 9
        dmkEditCB = EdGerente
        QryCampo = 'Gerente'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdComisCon: TdmkEdit
        Left = 507
        Top = 74
        Width = 99
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'ComisCon'
        UpdCampo = 'ComisCon'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdComisGer: TdmkEdit
        Left = 1118
        Top = 74
        Width = 98
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'ComisGer'
        UpdCampo = 'ComisGer'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdComisPto: TdmkEdit
        Left = 832
        Top = 25
        Width = 98
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'ComisPto'
        UpdCampo = 'ComisPto'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCarteira: TdmkEditCB
        Left = 5
        Top = 123
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Carteira'
        UpdCampo = 'Carteira'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCarteira
        IgnoraDBLookupComboBox = False
      end
      object CBCarteira: TdmkDBLookupComboBox
        Left = 75
        Top = 123
        Width = 532
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCarteiras
        TabOrder = 14
        dmkEditCB = EdCarteira
        QryCampo = 'Carteira'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPediPrzCab: TdmkEditCB
        Left = 610
        Top = 123
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 15
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPediPrzCab
        IgnoraDBLookupComboBox = False
      end
      object CBPediPrzCab: TdmkDBLookupComboBox
        Left = 681
        Top = 123
        Width = 536
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsPediPrzCab
        TabOrder = 16
        dmkEditCB = EdPediPrzCab
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCNAB_Cfg: TdmkEditCB
        Left = 5
        Top = 171
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 17
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CNAB_Cfg'
        UpdCampo = 'CNAB_Cfg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCNAB_Cfg
        IgnoraDBLookupComboBox = False
      end
      object CBCNAB_Cfg: TdmkDBLookupComboBox
        Left = 75
        Top = 171
        Width = 532
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCNAB_Cfg
        TabOrder = 18
        dmkEditCB = EdCNAB_Cfg
        QryCampo = 'CNAB_Cfg'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 1241
    Height = 534
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PnCabeca: TPanel
      Left = 1
      Top = 1
      Width = 1239
      Height = 222
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Enabled = False
      TabOrder = 1
      ExplicitWidth = 1238
      object Label1: TLabel
        Left = 5
        Top = 5
        Width = 16
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'ID:'
        FocusControl = DBEdCodigo
      end
      object Label3: TLabel
        Left = 79
        Top = 5
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label11: TLabel
        Left = 182
        Top = 5
        Width = 37
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Nome'
        FocusControl = DBEdit2
      end
      object dmkLabel5: TdmkLabel
        Left = 1078
        Top = 5
        Width = 87
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Encerramento:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label5: TLabel
        Left = 5
        Top = 54
        Width = 59
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Consultor:'
        FocusControl = DBEdit7
      end
      object dmkLabel9: TdmkLabel
        Left = 935
        Top = 5
        Width = 54
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Abertura:'
        UpdType = utYes
        SQLType = stNil
      end
      object Label4: TLabel
        Left = 615
        Top = 54
        Width = 51
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Gerente:'
        FocusControl = DBEdit5
      end
      object Label13: TLabel
        Left = 832
        Top = 5
        Width = 79
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '% Comiss'#227'o:'
        FocusControl = DBEdit6
      end
      object Label14: TLabel
        Left = 512
        Top = 54
        Width = 79
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '% Comiss'#227'o:'
        FocusControl = DBEdit11
      end
      object Label15: TLabel
        Left = 1118
        Top = 54
        Width = 79
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '% Comiss'#227'o:'
        FocusControl = DBEdit12
      end
      object Label17: TLabel
        Left = 5
        Top = 103
        Width = 50
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Carteira:'
        FocusControl = DBEdit13
      end
      object Label19: TLabel
        Left = 615
        Top = 103
        Width = 152
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Condi'#231#227'o de pagamento:'
        FocusControl = DBEdit15
      end
      object Label20: TLabel
        Left = 5
        Top = 151
        Width = 212
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Configura'#231#227'o de bloquetos padr'#227'o:'
      end
      object Label21: TLabel
        Left = 615
        Top = 153
        Width = 78
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Faturamento:'
        FocusControl = DBEdit19
      end
      object Label22: TLabel
        Left = 768
        Top = 153
        Width = 111
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '$ Comiss'#227'o ponto:'
        FocusControl = DBEdit20
      end
      object Label23: TLabel
        Left = 921
        Top = 153
        Width = 131
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '$ Comiss'#227'o consultor:'
        FocusControl = DBEdit21
      end
      object Label24: TLabel
        Left = 1073
        Top = 153
        Width = 123
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '$ Comiss'#227'o gerente:'
        FocusControl = DBEdit22
      end
      object DBEdCodigo: TDBEdit
        Left = 5
        Top = 25
        Width = 69
        Height = 21
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsPtosFatCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdit1: TDBEdit
        Left = 79
        Top = 25
        Width = 98
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'CodUsu'
        DataSource = DsPtosFatCad
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object DBEdit2: TDBEdit
        Left = 251
        Top = 25
        Width = 577
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_PontoVda'
        DataSource = DsPtosFatCad
        TabOrder = 2
      end
      object DBEdit3: TDBEdit
        Left = 1078
        Top = 25
        Width = 143
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataSource = DsPtosFatCad
        TabOrder = 3
      end
      object DBEdit7: TDBEdit
        Left = 79
        Top = 74
        Width = 429
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_Consultor'
        DataSource = DsPtosFatCad
        TabOrder = 4
      end
      object DBEdit8: TDBEdit
        Left = 182
        Top = 25
        Width = 69
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'CU_PontoVda'
        DataSource = DsPtosFatCad
        TabOrder = 5
      end
      object DBEdit9: TDBEdit
        Left = 5
        Top = 74
        Width = 69
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Consultor'
        DataSource = DsPtosFatCad
        TabOrder = 6
      end
      object DBEdit10: TDBEdit
        Left = 935
        Top = 25
        Width = 138
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Abertura'
        DataSource = DsPtosFatCad
        TabOrder = 7
      end
      object DBEdit4: TDBEdit
        Left = 615
        Top = 74
        Width = 69
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Gerente'
        DataSource = DsPtosFatCad
        TabOrder = 8
      end
      object DBEdit5: TDBEdit
        Left = 684
        Top = 74
        Width = 430
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_Gerente'
        DataSource = DsPtosFatCad
        TabOrder = 9
      end
      object DBEdit6: TDBEdit
        Left = 832
        Top = 25
        Width = 98
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'ComisPto'
        DataSource = DsPtosFatCad
        TabOrder = 10
      end
      object DBEdit11: TDBEdit
        Left = 512
        Top = 74
        Width = 98
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'ComisCon'
        DataSource = DsPtosFatCad
        TabOrder = 11
      end
      object DBEdit12: TDBEdit
        Left = 1118
        Top = 74
        Width = 103
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'ComisGer'
        DataSource = DsPtosFatCad
        TabOrder = 12
      end
      object DBEdit13: TDBEdit
        Left = 5
        Top = 123
        Width = 69
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Carteira'
        DataSource = DsPtosFatCad
        TabOrder = 13
      end
      object DBEdit14: TDBEdit
        Left = 79
        Top = 123
        Width = 533
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_Carteira'
        DataSource = DsPtosFatCad
        TabOrder = 14
      end
      object DBEdit15: TDBEdit
        Left = 615
        Top = 123
        Width = 69
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'PediPrzCab'
        DataSource = DsPtosFatCad
        TabOrder = 15
      end
      object DBEdit16: TDBEdit
        Left = 689
        Top = 123
        Width = 532
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_PediPrzCab'
        DataSource = DsPtosFatCad
        TabOrder = 16
      end
      object DBEdit17: TDBEdit
        Left = 5
        Top = 172
        Width = 69
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'CNAB_Cfg'
        DataSource = DsPtosFatCad
        TabOrder = 17
      end
      object DBEdit18: TDBEdit
        Left = 79
        Top = 172
        Width = 533
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NO_CNAB_Cfg'
        DataSource = DsPtosFatCad
        TabOrder = 18
      end
      object DBEdit19: TDBEdit
        Left = 615
        Top = 172
        Width = 148
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'ValorTot'
        DataSource = DsPtosFatCad
        TabOrder = 19
      end
      object DBEdit20: TDBEdit
        Left = 768
        Top = 172
        Width = 148
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'ValorPto'
        DataSource = DsPtosFatCad
        TabOrder = 20
      end
      object DBEdit21: TDBEdit
        Left = 921
        Top = 172
        Width = 147
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'ValorCon'
        DataSource = DsPtosFatCad
        TabOrder = 21
      end
      object DBEdit22: TDBEdit
        Left = 1073
        Top = 172
        Width = 148
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'ValorGer'
        DataSource = DsPtosFatCad
        TabOrder = 22
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 474
      Width = 1239
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      ExplicitWidth = 1238
      object LaRegistro: TdmkLabel
        Left = 213
        Top = 1
        Width = 31
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 212
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 537
        Top = 1
        Width = 700
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtItens: TBitBtn
          Left = 118
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Itens'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtItensClick
        end
        object BtFatura: TBitBtn
          Left = 5
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Fatura'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtFaturaClick
        end
        object Panel2: TPanel
          Left = 566
          Top = 0
          Width = 134
          Height = 57
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtBloqueto: TBitBtn
          Left = 345
          Top = 5
          Width = 110
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Bloqueto'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = BtBloquetoClick
        end
        object CkDesign: TCheckBox
          Left = 458
          Top = 30
          Width = 111
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Design'
          Enabled = False
          TabOrder = 4
        end
        object CkZerado: TCheckBox
          Left = 458
          Top = 7
          Width = 111
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Sem val/vcto.'
          TabOrder = 5
        end
      end
    end
    object PnGrids: TPanel
      Left = 1
      Top = 276
      Width = 1239
      Height = 198
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 2
      ExplicitWidth = 1238
      object PageControl1: TPageControl
        Left = 1
        Top = 1
        Width = 1237
        Height = 196
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        ActivePage = TabSheet1
        Align = alClient
        MultiLine = True
        TabOrder = 0
        ExplicitWidth = 1236
        object TabSheet5: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Itens por grade'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid1: TDBGrid
            Left = 0
            Top = 0
            Width = 350
            Height = 161
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alLeft
            DataSource = DsPtosFatGru
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Visible = True
              end>
          end
          object DBGPtosFatIts: TDBGrid
            Left = 350
            Top = 0
            Width = 876
            Height = 161
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsPtosFatIts
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'Produto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 222
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Cor'
                Title.Caption = 'Cor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Tam'
                Title.Caption = 'Tamanho'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Quanti'
                Title.Caption = 'Quantidade'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Width = 72
                Visible = True
              end>
          end
        end
        object TSItemAItem: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Item a item '
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGPtosStqMov: TDBGrid
            Left = 0
            Top = 0
            Width = 1226
            Height = 161
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsPtosStqMov
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Codigo'
                Title.Caption = 'Produto'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Nome'
                Title.Caption = 'Descri'#231#227'o'
                Width = 273
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Cor'
                Title.Caption = 'Cor'
                Width = 209
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NO_Tam'
                Title.Caption = 'Tamanho'
                Width = 76
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Quanti'
                Title.Caption = 'Quantidade'
                Width = 60
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'IDCtrl'
                Title.Caption = 'ID'
                Visible = True
              end>
          end
        end
        object TabSheet1: TTabSheet
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = ' Faturamento '
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
          ExplicitHeight = 0
          object DBGrid2: TDBGrid
            Left = 0
            Top = 0
            Width = 1226
            Height = 161
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alClient
            DataSource = DsFatura
            TabOrder = 0
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -15
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Controle'
                Title.Caption = 'ID'
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'Data'
                Title.Alignment = taCenter
                Width = 68
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'Vencimento'
                Title.Alignment = taCenter
                Width = 68
                Visible = True
              end
              item
                Alignment = taCenter
                Expanded = False
                FieldName = 'COMPENSADO_TXT'
                Title.Alignment = taCenter
                Title.Caption = 'Compensado'
                Width = 68
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Duplicata'
                Width = 104
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Valor'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'CREDITO'
                Title.Caption = 'Cr'#233'dito'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'MoraDia'
                Title.Caption = '% Juros/m'#234's'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Multa'
                Title.Caption = '% Multa'
                Width = 72
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Vendedor'
                Title.Caption = 'Consultor'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Account'
                Title.Caption = 'Gerente'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Documento'
                Width = 120
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1241
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = '                              Faturamento de Pontos de Vendas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 1138
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
    end
    object Image1: TImage
      Left = 278
      Top = 1
      Width = 860
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 277
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 4
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsPtosFatCad: TDataSource
    DataSet = QrPtosFatCad
    Left = 40
    Top = 12
  end
  object QrPtosFatCad: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPtosFatCadBeforeOpen
    AfterOpen = QrPtosFatCadAfterOpen
    BeforeClose = QrPtosFatCadBeforeClose
    AfterScroll = QrPtosFatCadAfterScroll
    OnCalcFields = QrPtosFatCadCalcFields
    SQL.Strings = (
      'SELECT pcp.CodUsu CU_PontoVda, pcp.Nome NO_PontoVda,'
      'IF(con.Tipo=0,con.RazaoSocial,con.Nome) NO_Consultor,'
      'IF(ger.Tipo=0,ger.RazaoSocial,ger.Nome) NO_Gerente,'
      'cfg.Nome NO_CNAB_Cfg, ppc.Nome NO_PediPrzCab,'
      'ppc.MultaPer, ppc.JurosMes, car.Nome NO_Carteira, '
      'car.Tipo TIPOCART, car.ForneceI CliInt, pfc.*'
      'FROM ptosfatcad pfc'
      'LEFT JOIN ptoscadpto pcp ON pcp.Codigo=pfc.PontoVda'
      'LEFT JOIN entidades con ON con.Codigo=pfc.Consultor'
      'LEFT JOIN entidades ger ON ger.Codigo=pfc.Gerente'
      'LEFT JOIN carteiras car ON car.Codigo=pfc.Carteira'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pfc.PediPrzCab'
      'LEFT JOIN cnab_cfg cfg ON cfg.Codigo=pfc.CNAB_Cfg'
      '')
    Left = 12
    Top = 12
    object QrPtosFatCadCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'ptosfatcad.Codigo'
      Required = True
    end
    object QrPtosFatCadCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Origin = 'ptosfatcad.CodUsu'
      Required = True
    end
    object QrPtosFatCadPontoVda: TIntegerField
      FieldName = 'PontoVda'
      Origin = 'ptosfatcad.PontoVda'
      Required = True
    end
    object QrPtosFatCadConsultor: TIntegerField
      FieldName = 'Consultor'
      Origin = 'ptosfatcad.Consultor'
      Required = True
    end
    object QrPtosFatCadGerente: TIntegerField
      FieldName = 'Gerente'
      Origin = 'ptosfatcad.Gerente'
      Required = True
    end
    object QrPtosFatCadAbertura: TDateTimeField
      FieldName = 'Abertura'
      Origin = 'ptosfatcad.Abertura'
      Required = True
    end
    object QrPtosFatCadEncerrou: TDateTimeField
      FieldName = 'Encerrou'
      Origin = 'ptosfatcad.Encerrou'
      Required = True
    end
    object QrPtosFatCadValorTot: TFloatField
      FieldName = 'ValorTot'
      Origin = 'ptosfatcad.ValorTot'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPtosFatCadComisPto: TFloatField
      FieldName = 'ComisPto'
      Origin = 'ptosfatcad.ComisPto'
      Required = True
      DisplayFormat = '#,###,##0.000000'
    end
    object QrPtosFatCadComisCon: TFloatField
      FieldName = 'ComisCon'
      Origin = 'ptosfatcad.ComisCon'
      Required = True
      DisplayFormat = '#,###,##0.000000'
    end
    object QrPtosFatCadComisGer: TFloatField
      FieldName = 'ComisGer'
      Origin = 'ptosfatcad.ComisGer'
      Required = True
      DisplayFormat = '#,###,##0.000000'
    end
    object QrPtosFatCadValorPto: TFloatField
      FieldName = 'ValorPto'
      Origin = 'ptosfatcad.ValorPto'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPtosFatCadValorCon: TFloatField
      FieldName = 'ValorCon'
      Origin = 'ptosfatcad.ValorCon'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPtosFatCadValorGer: TFloatField
      FieldName = 'ValorGer'
      Origin = 'ptosfatcad.ValorGer'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrPtosFatCadOndeCad: TSmallintField
      FieldName = 'OndeCad'
      Origin = 'ptosfatcad.OndeCad'
      Required = True
    end
    object QrPtosFatCadOndeEnc: TSmallintField
      FieldName = 'OndeEnc'
      Origin = 'ptosfatcad.OndeEnc'
      Required = True
    end
    object QrPtosFatCadLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'ptosfatcad.Lk'
    end
    object QrPtosFatCadDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'ptosfatcad.DataCad'
    end
    object QrPtosFatCadDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'ptosfatcad.DataAlt'
    end
    object QrPtosFatCadUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'ptosfatcad.UserCad'
    end
    object QrPtosFatCadUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'ptosfatcad.UserAlt'
    end
    object QrPtosFatCadAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'ptosfatcad.AlterWeb'
      Required = True
    end
    object QrPtosFatCadAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'ptosfatcad.Ativo'
      Required = True
    end
    object QrPtosFatCadNO_Consultor: TWideStringField
      FieldName = 'NO_Consultor'
      Size = 100
    end
    object QrPtosFatCadNO_Gerente: TWideStringField
      FieldName = 'NO_Gerente'
      Size = 100
    end
    object QrPtosFatCadCU_PontoVda: TIntegerField
      FieldName = 'CU_PontoVda'
      Required = True
    end
    object QrPtosFatCadNO_PontoVda: TWideStringField
      FieldName = 'NO_PontoVda'
      Size = 50
    end
    object QrPtosFatCadQuantTot: TFloatField
      FieldName = 'QuantTot'
    end
    object QrPtosFatCadNO_Carteira: TWideStringField
      FieldName = 'NO_Carteira'
      Size = 100
    end
    object QrPtosFatCadCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrPtosFatCadNO_PediPrzCab: TWideStringField
      FieldName = 'NO_PediPrzCab'
      Size = 50
    end
    object QrPtosFatCadPediPrzCab: TIntegerField
      FieldName = 'PediPrzCab'
    end
    object QrPtosFatCadCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrPtosFatCadNO_CNAB_Cfg: TWideStringField
      FieldName = 'NO_CNAB_Cfg'
      Size = 50
    end
    object QrPtosFatCadTIPOCART: TIntegerField
      FieldName = 'TIPOCART'
      Required = True
    end
    object QrPtosFatCadJurosMes: TFloatField
      FieldName = 'JurosMes'
    end
    object QrPtosFatCadCliInt: TIntegerField
      FieldName = 'CliInt'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtFatura
    CanUpd01 = BtItens
    Left = 68
    Top = 12
  end
  object PMFatura: TPopupMenu
    OnPopup = PMFaturaPopup
    Left = 452
    Top = 404
    object Incluinovafatura1: TMenuItem
      Caption = '&Inclui nova fatura'
      OnClick = Incluinovafatura1Click
    end
    object Alterafaturaatual1: TMenuItem
      Caption = '&Altera fatura atual'
      Enabled = False
      OnClick = Alterafaturaatual1Click
    end
    object Excluifaturaatual1: TMenuItem
      Caption = '&Exclui fatura atual'
      Enabled = False
    end
  end
  object QrPtosCadPto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu, Nome, Consultor, Carteira,'
      'Gerente, ComisPonto, ComisConsu, ComisGeren, '
      'PediPrzCab, CNAB_Cfg'
      'FROM ptoscadpto'
      'ORDER BY Nome')
    Left = 772
    Top = 12
    object QrPtosCadPtoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPtosCadPtoCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPtosCadPtoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPtosCadPtoConsultor: TIntegerField
      FieldName = 'Consultor'
    end
    object QrPtosCadPtoGerente: TIntegerField
      FieldName = 'Gerente'
    end
    object QrPtosCadPtoComisPonto: TFloatField
      FieldName = 'ComisPonto'
    end
    object QrPtosCadPtoComisConsu: TFloatField
      FieldName = 'ComisConsu'
    end
    object QrPtosCadPtoComisGeren: TFloatField
      FieldName = 'ComisGeren'
    end
    object QrPtosCadPtoCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrPtosCadPtoPediPrzCab: TIntegerField
      FieldName = 'PediPrzCab'
    end
    object QrPtosCadPtoCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
  end
  object DsPtosCadPto: TDataSource
    DataSet = QrPtosCadPto
    Left = 800
    Top = 12
  end
  object QrConsultores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0,RazaoSocial,Nome) NO_ENT'
      'FROM entidades'
      'WHERE Fornece6="V"'
      'ORDER BY NO_ENT'
      '')
    Left = 828
    Top = 12
    object QrConsultoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConsultoresNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object DsConsultores: TDataSource
    DataSet = QrConsultores
    Left = 856
    Top = 12
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdPontoVda
    Panel = PainelEdita
    QryCampo = 'PontoVda'
    UpdCampo = 'PontoVda'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 96
    Top = 12
  end
  object PMItens: TPopupMenu
    OnPopup = PMItensPopup
    Left = 536
    Top = 408
    object Imprimeestoque1: TMenuItem
      Caption = 'I&mprime estoque'
      OnClick = Imprimeestoque1Click
    end
    object Incluiitenspeloestoque1: TMenuItem
      Caption = '&Faturamento pelo estoque'
      OnClick = Incluiitenspeloestoque1Click
    end
    object FaturamentoporLeitura1: TMenuItem
      Caption = 'Faturamento por &Leitura'
      OnClick = FaturamentoporLeitura1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Cancelaritens1: TMenuItem
      Caption = '&Cancelar todo faturamento'
      OnClick = Cancelaritens1Click
    end
  end
  object QrPtosFatGru: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrPtosFatGruBeforeClose
    AfterScroll = QrPtosFatGruAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT grs.Codigo, grs.Nome'
      'FROM ptosstqmov psm'
      'LEFT JOIN produtos prd ON prd.Controle=psm.Produto'
      'LEFT JOIN grades grs ON grs.Codigo=prd.Codigo'
      'WHERE psm.TipOut=1'
      'AND psm.CodOut=:P0'
      'ORDER BY grs.Nome')
    Left = 124
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPtosFatGruCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPtosFatGruNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsPtosFatGru: TDataSource
    DataSet = QrPtosFatGru
    Left = 152
    Top = 12
  end
  object DsGerentes: TDataSource
    DataSet = QrGerentes
    Left = 912
    Top = 12
  end
  object QrGerentes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, IF(Tipo=0,RazaoSocial,Nome) NO_ENT'
      'FROM entidades'
      'WHERE Fornece7="V"'
      'ORDER BY NO_ENT'
      '')
    Left = 884
    Top = 12
    object QrGerentesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGerentesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object QrEstqPto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prd.Codigo Grade, prd.Cor, prd.Tam,'
      'psm.Produto, SUM(psm.Quanti) Quanti,'
      'SUM(psm.PrecoR) / SUM(psm.Quanti) PRECOM,'
      'grs.Nome NO_GRADE, grs.CodUsu, '
      'cor.Nome NO_Cor, tam.Nome NO_Tam'
      'FROM ptosstqmov psm'
      'LEFT JOIN produtos prd ON prd.Controle=psm.Produto'
      'LEFT JOIN grades grs ON grs.Codigo=prd.Codigo'
      'LEFT JOIN cores cor ON cor.Codigo=prd.Cor'
      'LEFT JOIN tamanhos tam ON tam.Codigo=prd.Tam'
      'WHERE psm.Status=50'
      'AND psm.Empresa=:P0'
      'GROUP BY psm.Produto'
      'ORDER BY NO_GRADE, NO_Cor, NO_Tam')
    Left = 548
    Top = 320
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEstqPtoGrade: TIntegerField
      FieldName = 'Grade'
    end
    object QrEstqPtoCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrEstqPtoTam: TIntegerField
      FieldName = 'Tam'
    end
    object QrEstqPtoProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrEstqPtoQuanti: TFloatField
      FieldName = 'Quanti'
    end
    object QrEstqPtoPRECOM: TFloatField
      FieldName = 'PRECOM'
    end
    object QrEstqPtoNO_GRADE: TWideStringField
      FieldName = 'NO_GRADE'
      Size = 30
    end
    object QrEstqPtoNO_Cor: TWideStringField
      FieldName = 'NO_Cor'
      Size = 100
    end
    object QrEstqPtoNO_Tam: TWideStringField
      FieldName = 'NO_Tam'
      Size = 100
    end
    object QrEstqPtoCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsEstqPto: TDataSource
    DataSet = QrEstqPto
    Left = 576
    Top = 320
  end
  object frxEstqPto: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    EngineOptions.DoublePass = True
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39655.525740740700000000
    ReportOptions.LastChange = 39655.525740740700000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxEstqPtoGetValue
    Left = 632
    Top = 320
    Datasets = <
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = frxDsEstqPto
        DataSetName = 'frxDsEstqPto'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 20.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 15.000000000000000000
      object PageHeader9: TfrxPageHeader
        FillType = ftBrush
        Height = 79.370130000000000000
        Top = 18.897650000000000000
        Width = 680.315400000000000000
        object Shape7: TfrxShapeView
          Width = 680.315400000000000000
          Height = 37.795300000000000000
          Frame.Width = 0.100000000000000000
          Shape = skRoundRectangle
        end
        object Memo214: TfrxMemoView
          Left = 7.559060000000000000
          Width = 665.197280000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsDono."NOMEDONO"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Line24: TfrxLineView
          Top = 18.897650000000000000
          Width = 680.315400000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
          Frame.Width = 0.100000000000000000
        end
        object Memo215: TfrxMemoView
          Left = 151.181200000000000000
          Top = 18.897650000000000000
          Width = 377.953000000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'ESTOQUE DE PONTO DE VENDA')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo216: TfrxMemoView
          Left = 7.559060000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = 'dd/mm/yyyy'
          DisplayFormat.Kind = fkDateTime
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[Date]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo292: TfrxMemoView
          Left = 529.134200000000000000
          Top = 18.897650000000000000
          Width = 143.622140000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'g. [Page#] de [TotalPages#]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo293: TfrxMemoView
          Top = 41.574830000000000000
          Width = 680.315400000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_NOMEPONTO]')
          ParentFont = False
        end
        object Memo1: TfrxMemoView
          Left = 340.157700000000000000
          Top = 60.472480000000000000
          Width = 113.385851180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 453.543600000000000000
          Top = 60.472479999999990000
          Width = 45.354311180000010000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tamanho')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 56.692950000000010000
          Top = 60.472479999999990000
          Width = 283.464750000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o produto')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 498.897960000000000000
          Top = 60.472480000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Pre'#231'o m'#233'dio')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 574.488560000000000000
          Top = 60.472480000000000000
          Width = 105.826791180000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Quantidade')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Top = 60.472479999999990000
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Refer'#234'ncia')
          ParentFont = False
        end
      end
      object MasterData12: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 154.960730000000000000
        Width = 680.315400000000000000
        DataSet = frxDsEstqPto
        DataSetName = 'frxDsEstqPto'
        RowCount = 0
        object Memo299: TfrxMemoView
          Left = 340.157700000000000000
          Width = 113.385851180000000000
          Height = 18.897650000000000000
          DataField = 'NO_Cor'
          DataSet = frxDsEstqPto
          DataSetName = 'frxDsEstqPto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqPto."NO_Cor"]')
          ParentFont = False
        end
        object Memo300: TfrxMemoView
          Left = 453.543600000000000000
          Width = 45.354311180000000000
          Height = 18.897650000000000000
          DataField = 'NO_Tam'
          DataSet = frxDsEstqPto
          DataSetName = 'frxDsEstqPto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqPto."NO_Tam"]')
          ParentFont = False
        end
        object Memo301: TfrxMemoView
          Left = 56.692950000000010000
          Width = 283.464750000000000000
          Height = 18.897650000000000000
          DataField = 'NO_GRADE'
          DataSet = frxDsEstqPto
          DataSetName = 'frxDsEstqPto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEstqPto."NO_GRADE"]')
          ParentFont = False
        end
        object Memo303: TfrxMemoView
          Left = 498.897960000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataField = 'PRECOM'
          DataSet = frxDsEstqPto
          DataSetName = 'frxDsEstqPto'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqPto."PRECOM"]')
          ParentFont = False
        end
        object Memo305: TfrxMemoView
          Left = 574.488560000000000000
          Width = 30.236191180000000000
          Height = 18.897650000000000000
          DataField = 'Quanti'
          DataSet = frxDsEstqPto
          DataSetName = 'frxDsEstqPto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqPto."Quanti"]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsEstqPto
          DataSetName = 'frxDsEstqPto'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Width = 56.692913390000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEstqPto."CodUsu"]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsEstqPto: TfrxDBDataset
    UserName = 'frxDsEstqPto'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Grade=Grade'
      'Cor=Cor'
      'Tam=Tam'
      'Produto=Produto'
      'Quanti=Quanti'
      'PRECOM=PRECOM'
      'NO_GRADE=NO_GRADE'
      'NO_Cor=NO_Cor'
      'NO_Tam=NO_Tam'
      'CodUsu=CodUsu')
    DataSet = QrEstqPto
    BCDToCurrency = False
    Left = 604
    Top = 320
  end
  object QrPtosFatIts: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT grs.Codigo, grs.Nome, cor.Nome NO_Cor, '
      'tam.Nome NO_Tam, SUM(psm.Quanti) Quanti,'
      'SUM(psm.PrecoR*psm.Quanti) Valor'
      'FROM ptosstqmov psm'
      'LEFT JOIN produtos prd ON prd.Controle=psm.Produto'
      'LEFT JOIN grades grs ON grs.Codigo=prd.Codigo'
      'LEFT JOIN cores cor ON cor.Codigo=prd.Cor'
      'LEFT JOIN tamanhos tam ON tam.Codigo=prd.Tam'
      'WHERE psm.TipOut=1'
      'AND psm.Status=60'
      'AND psm.CodOut=:P0'
      'AND prd.Codigo=:P1'
      'GROUP BY prd.Controle'
      'ORDER BY grs.Nome, cor.Nome, tam.CodUsu')
    Left = 180
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPtosFatItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrPtosFatItsNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPtosFatItsNO_Cor: TWideStringField
      FieldName = 'NO_Cor'
      Size = 30
    end
    object QrPtosFatItsNO_Tam: TWideStringField
      FieldName = 'NO_Tam'
      Size = 10
    end
    object QrPtosFatItsQuanti: TFloatField
      FieldName = 'Quanti'
    end
    object QrPtosFatItsValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object DsPtosFatIts: TDataSource
    DataSet = QrPtosFatIts
    Left = 208
    Top = 12
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE Codigo> 0'
      'AND Tipo=2'
      'ORDER BY Nome')
    Left = 940
    Top = 12
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 968
    Top = 12
  end
  object QrPediPrzCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cab.Codigo, cab.CodUsu, cab.Nome'
      'FROM pediprzcab cab'
      'LEFT JOIN pediprzcli cli ON cli.Codigo=cab.Codigo'
      'WHERE cli.Empresa=:P0 '
      'ORDER BY Nome')
    Left = 156
    Top = 204
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 184
    Top = 204
  end
  object dmkValUsu2: TdmkValUsu
    dmkEditCB = EdPediPrzCab
    Panel = PainelEdita
    QryCampo = 'PediPrzCab'
    UpdCampo = 'PediPrzCab'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 96
    Top = 40
  end
  object QrPtosStqMov: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT grs.Codigo, grs.Nome, cor.Nome NO_Cor, '
      'tam.Nome NO_Tam, psm.Quanti, psm.PrecoR, psm.IDCtrl'
      'FROM ptosstqmov psm'
      'LEFT JOIN produtos prd ON prd.Controle=psm.Produto'
      'LEFT JOIN grades grs ON grs.Codigo=prd.Codigo'
      'LEFT JOIN cores cor ON cor.Codigo=prd.Cor'
      'LEFT JOIN tamanhos tam ON tam.Codigo=prd.Tam'
      'WHERE psm.TipOut=1'
      'AND psm.Status=60'
      'AND psm.CodOut=:P0'
      'ORDER BY grs.Nome, cor.Nome, tam.CodUsu')
    Left = 236
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPtosStqMovCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPtosStqMovNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrPtosStqMovNO_Cor: TWideStringField
      FieldName = 'NO_Cor'
      Size = 30
    end
    object QrPtosStqMovNO_Tam: TWideStringField
      FieldName = 'NO_Tam'
      Size = 10
    end
    object QrPtosStqMovQuanti: TFloatField
      FieldName = 'Quanti'
    end
    object QrPtosStqMovPrecoR: TFloatField
      FieldName = 'PrecoR'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPtosStqMovIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
    end
  end
  object DsPtosStqMov: TDataSource
    DataSet = QrPtosStqMov
    Left = 264
    Top = 12
  end
  object QrCNAB_Cfg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT *'
      'FROM cnab_cfg'
      'ORDER BY Nome')
    Left = 716
    Top = 12
    object QrCNAB_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB_CfgNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrCNAB_CfgCodLidrBco: TWideStringField
      FieldName = 'CodLidrBco'
    end
    object QrCNAB_CfgCedente: TIntegerField
      FieldName = 'Cedente'
    end
    object QrCNAB_CfgSacadAvali: TIntegerField
      FieldName = 'SacadAvali'
    end
    object QrCNAB_CfgCedBanco: TIntegerField
      FieldName = 'CedBanco'
    end
    object QrCNAB_CfgCedAgencia: TIntegerField
      FieldName = 'CedAgencia'
    end
    object QrCNAB_CfgCedConta: TWideStringField
      FieldName = 'CedConta'
      Size = 30
    end
    object QrCNAB_CfgCedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Size = 1
    end
    object QrCNAB_CfgCedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Size = 1
    end
    object QrCNAB_CfgCedDAC_AC: TWideStringField
      FieldName = 'CedDAC_AC'
      Size = 1
    end
    object QrCNAB_CfgCedNome: TWideStringField
      FieldName = 'CedNome'
      Size = 100
    end
    object QrCNAB_CfgCedPosto: TIntegerField
      FieldName = 'CedPosto'
    end
    object QrCNAB_CfgSacAvaNome: TWideStringField
      FieldName = 'SacAvaNome'
      Size = 100
    end
    object QrCNAB_CfgTipoCart: TSmallintField
      FieldName = 'TipoCart'
    end
    object QrCNAB_CfgCartNum: TWideStringField
      FieldName = 'CartNum'
      Size = 3
    end
    object QrCNAB_CfgCartCod: TWideStringField
      FieldName = 'CartCod'
      Size = 3
    end
    object QrCNAB_CfgEspecieTit: TWideStringField
      FieldName = 'EspecieTit'
      Size = 6
    end
    object QrCNAB_CfgAceiteTit: TSmallintField
      FieldName = 'AceiteTit'
    end
    object QrCNAB_CfgInstrCobr1: TWideStringField
      FieldName = 'InstrCobr1'
      Size = 2
    end
    object QrCNAB_CfgInstrCobr2: TWideStringField
      FieldName = 'InstrCobr2'
      Size = 2
    end
    object QrCNAB_CfgInstrDias: TSmallintField
      FieldName = 'InstrDias'
    end
    object QrCNAB_CfgCodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
    end
    object QrCNAB_CfgJurosTipo: TSmallintField
      FieldName = 'JurosTipo'
    end
    object QrCNAB_CfgJurosPerc: TFloatField
      FieldName = 'JurosPerc'
    end
    object QrCNAB_CfgJurosDias: TSmallintField
      FieldName = 'JurosDias'
    end
    object QrCNAB_CfgMultaTipo: TSmallintField
      FieldName = 'MultaTipo'
    end
    object QrCNAB_CfgMultaPerc: TFloatField
      FieldName = 'MultaPerc'
    end
    object QrCNAB_CfgTexto01: TWideStringField
      FieldName = 'Texto01'
      Size = 100
    end
    object QrCNAB_CfgTexto02: TWideStringField
      FieldName = 'Texto02'
      Size = 100
    end
    object QrCNAB_CfgTexto03: TWideStringField
      FieldName = 'Texto03'
      Size = 100
    end
    object QrCNAB_CfgTexto04: TWideStringField
      FieldName = 'Texto04'
      Size = 100
    end
    object QrCNAB_CfgTexto05: TWideStringField
      FieldName = 'Texto05'
      Size = 100
    end
    object QrCNAB_CfgTexto06: TWideStringField
      FieldName = 'Texto06'
      Size = 100
    end
    object QrCNAB_CfgTexto07: TWideStringField
      FieldName = 'Texto07'
      Size = 100
    end
    object QrCNAB_CfgTexto08: TWideStringField
      FieldName = 'Texto08'
      Size = 100
    end
    object QrCNAB_CfgTexto09: TWideStringField
      FieldName = 'Texto09'
      Size = 100
    end
    object QrCNAB_CfgTexto10: TWideStringField
      FieldName = 'Texto10'
      Size = 100
    end
    object QrCNAB_CfgCNAB: TIntegerField
      FieldName = 'CNAB'
    end
    object QrCNAB_CfgEnvEmeio: TSmallintField
      FieldName = 'EnvEmeio'
    end
    object QrCNAB_CfgDiretorio: TWideStringField
      FieldName = 'Diretorio'
      Size = 255
    end
    object QrCNAB_CfgQuemPrint: TWideStringField
      FieldName = 'QuemPrint'
      Size = 1
    end
    object QrCNAB_Cfg_237Mens1: TWideStringField
      FieldName = '_237Mens1'
      Size = 12
    end
    object QrCNAB_Cfg_237Mens2: TWideStringField
      FieldName = '_237Mens2'
      Size = 60
    end
    object QrCNAB_CfgCodOculto: TWideStringField
      FieldName = 'CodOculto'
    end
    object QrCNAB_CfgSeqArq: TIntegerField
      FieldName = 'SeqArq'
    end
    object QrCNAB_CfgLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCNAB_CfgDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCNAB_CfgDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCNAB_CfgUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCNAB_CfgUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCNAB_CfgAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCNAB_CfgAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCNAB_CfgLastNosNum: TLargeintField
      FieldName = 'LastNosNum'
    end
    object QrCNAB_CfgLocalPag: TWideStringField
      FieldName = 'LocalPag'
      Size = 127
    end
    object QrCNAB_CfgEspecieVal: TWideStringField
      FieldName = 'EspecieVal'
      Size = 5
    end
    object QrCNAB_CfgOperCodi: TWideStringField
      FieldName = 'OperCodi'
      Size = 3
    end
    object QrCNAB_CfgIDCobranca: TWideStringField
      FieldName = 'IDCobranca'
      Size = 2
    end
    object QrCNAB_CfgAgContaCed: TWideStringField
      FieldName = 'AgContaCed'
      Size = 40
    end
    object QrCNAB_CfgDirRetorno: TWideStringField
      FieldName = 'DirRetorno'
      Size = 255
    end
    object QrCNAB_CfgEspecieDoc: TWideStringField
      FieldName = 'EspecieDoc'
    end
    object QrCNAB_CfgMultaDias: TSmallintField
      FieldName = 'MultaDias'
    end
    object QrCNAB_CfgCartRetorno: TIntegerField
      FieldName = 'CartRetorno'
    end
    object QrCNAB_CfgPosicoesBB: TSmallintField
      FieldName = 'PosicoesBB'
    end
    object QrCNAB_CfgIndicatBB: TWideStringField
      FieldName = 'IndicatBB'
      Size = 1
    end
    object QrCNAB_CfgTipoCobrBB: TWideStringField
      FieldName = 'TipoCobrBB'
      Size = 5
    end
    object QrCNAB_CfgVariacao: TIntegerField
      FieldName = 'Variacao'
    end
    object QrCNAB_CfgComando: TIntegerField
      FieldName = 'Comando'
    end
    object QrCNAB_CfgDdProtesBB: TIntegerField
      FieldName = 'DdProtesBB'
    end
    object QrCNAB_CfgMsg40posBB: TWideStringField
      FieldName = 'Msg40posBB'
      Size = 40
    end
    object QrCNAB_CfgQuemDistrb: TWideStringField
      FieldName = 'QuemDistrb'
      Size = 1
    end
    object QrCNAB_CfgDesco1Cod: TSmallintField
      FieldName = 'Desco1Cod'
    end
    object QrCNAB_CfgDesco1Dds: TIntegerField
      FieldName = 'Desco1Dds'
    end
    object QrCNAB_CfgDesco1Fat: TFloatField
      FieldName = 'Desco1Fat'
    end
    object QrCNAB_CfgDesco2Cod: TSmallintField
      FieldName = 'Desco2Cod'
    end
    object QrCNAB_CfgDesco2Dds: TIntegerField
      FieldName = 'Desco2Dds'
    end
    object QrCNAB_CfgDesco2Fat: TFloatField
      FieldName = 'Desco2Fat'
    end
    object QrCNAB_CfgDesco3Cod: TSmallintField
      FieldName = 'Desco3Cod'
    end
    object QrCNAB_CfgDesco3Dds: TIntegerField
      FieldName = 'Desco3Dds'
    end
    object QrCNAB_CfgDesco3Fat: TFloatField
      FieldName = 'Desco3Fat'
    end
    object QrCNAB_CfgProtesCod: TSmallintField
      FieldName = 'ProtesCod'
    end
    object QrCNAB_CfgProtesDds: TSmallintField
      FieldName = 'ProtesDds'
    end
    object QrCNAB_CfgBxaDevCod: TSmallintField
      FieldName = 'BxaDevCod'
    end
    object QrCNAB_CfgBxaDevDds: TIntegerField
      FieldName = 'BxaDevDds'
    end
    object QrCNAB_CfgMoedaCod: TWideStringField
      FieldName = 'MoedaCod'
    end
    object QrCNAB_CfgContrato: TFloatField
      FieldName = 'Contrato'
    end
    object QrCNAB_CfgConvCartCobr: TWideStringField
      FieldName = 'ConvCartCobr'
      Size = 2
    end
    object QrCNAB_CfgConvVariCart: TWideStringField
      FieldName = 'ConvVariCart'
      Size = 3
    end
    object QrCNAB_CfgInfNossoNum: TSmallintField
      FieldName = 'InfNossoNum'
    end
    object QrCNAB_CfgCartEmiss: TIntegerField
      FieldName = 'CartEmiss'
    end
  end
  object DsCNAB_Cfg: TDataSource
    DataSet = QrCNAB_Cfg
    Left = 744
    Top = 12
  end
  object QrFatura: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrFaturaCalcFields
    DataSource = DsCarteiras
    SQL.Strings = (
      'SELECT lan.Controle, lan.Data, lan.Vencimento, lan.Documento,'
      'lan.Duplicata, lan.Compensado, lan.Credito Valor, lan.MoraDia, '
      'lan.Multa, lan.Vendedor, lan.Account'
      'FROM FTabLctALS lan'
      'WHERE lan.FatID=801'
      'AND lan.FatNum=:P0'
      'ORDER BY lan.Vencimento, lan.FatParcela')
    Left = 548
    Top = 376
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFaturaControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrFaturaData: TDateField
      FieldName = 'Data'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFaturaVencimento: TDateField
      FieldName = 'Vencimento'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFaturaDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrFaturaDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrFaturaCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yyyy'
    end
    object QrFaturaMoraDia: TFloatField
      FieldName = 'MoraDia'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrFaturaMulta: TFloatField
      FieldName = 'Multa'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrFaturaVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrFaturaAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrFaturaCOMPENSADO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'COMPENSADO_TXT'
      Size = 10
      Calculated = True
    end
    object QrFaturaBLOQUETO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'BLOQUETO'
      Calculated = True
    end
    object QrFaturaValor: TFloatField
      FieldName = 'Valor'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrFaturaCREDITO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CREDITO'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
      Calculated = True
    end
  end
  object DsFatura: TDataSource
    DataSet = QrFatura
    Left = 576
    Top = 376
  end
  object frxBloqA2: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39333.805305983800000000
    ReportOptions.LastChange = 39342.853423206000000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      '  if <LogoBancoExiste> = True then'
      '  begin'
      '    Picture_Bco.LoadFromFile(<LogoBancoPath>);'
      '    Memo_001.Visible := False;'
      '    Picture_Bco1.LoadFromFile(<LogoBancoPath>);'
      '    Memo_101.Visible := False;'
      '  end else begin           '
      '    Memo_001.Visible := True;'
      '    Memo_101.Visible := True;'
      '  end;              '
      'end.')
    OnGetValue = frxBloqA2GetValue
    OnUserFunction = frxBloqA2UserFunction
    Left = 632
    Top = 348
    Datasets = <
      item
        DataSet = frxDsCfg
        DataSetName = 'frxDsCfg'
      end
      item
        DataSet = Dmod.frxDsDono
        DataSetName = 'frxDsDono'
      end
      item
        DataSet = DModG.frxDsEndereco
        DataSetName = 'frxDsEndereco'
      end
      item
        DataSet = frxDsFatura
        DataSetName = 'frxDsFatura'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <
      item
        Name = ' Meu'
        Value = Null
      end
      item
        Name = 'VARI_Soma'
        Value = Null
      end
      item
        Name = 'BancoLogoExiste'
        Value = Null
      end
      item
        Name = 'EmpresaLogoExiste'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      Frame.Width = 0.100000000000000000
      object Line13: TfrxLineView
        Left = 3.779530000000000000
        Top = 718.110700000000000000
        Width = 789.921770000000000000
        Color = clBlack
        ArrowLength = 100
        ArrowWidth = 20
        Frame.Style = fsDash
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo81: TfrxMemoView
        Left = 578.268090000000000000
        Top = 706.772110000000000000
        Width = 162.519790000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'AUTENTICA'#199#195'O MEC'#194'NICA')
        ParentFont = False
      end
      object Memo1: TfrxMemoView
        Left = 56.692950000000000000
        Top = 476.220472440000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_03]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo2: TfrxMemoView
        Left = 602.834645670000000000
        Top = 424.819022130000000000
        Width = 137.952755910000000000
        Height = 17.385826770000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsFatura."CREDITO"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Picture_Bco: TfrxPictureView
        Left = 52.913420000000000000
        Top = 294.803340000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo3: TfrxMemoView
        Left = 602.834645670000000000
        Top = 345.448943390000000000
        Width = 137.952755910000000000
        Height = 23.055118110000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsFatura."Vencimento"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo4: TfrxMemoView
        Left = 602.834645670000000000
        Top = 415.748300000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo5: TfrxMemoView
        Left = 602.834645670000000000
        Top = 336.378170000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line1: TfrxLineView
        Left = 3.779530000000000000
        Top = 294.803340000000000000
        Width = 789.921770000000000000
        Color = clBlack
        ArrowLength = 100
        ArrowWidth = 20
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo6: TfrxMemoView
        Left = 578.268090000000000000
        Top = 279.685220000000000000
        Width = 162.519790000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Univers Light Condensed'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          'SEGUNDA VIA')
        ParentFont = False
      end
      object BarCode2: TfrxBarCodeView
        Left = 90.708661420000000000
        Top = 634.960754410000000000
        Width = 405.000000000000000000
        Height = 49.133858270000000000
        BarType = bcCode_2_5_interleaved
        Expression = '<VARF_CODIGOBARRAS>'
        Rotation = 0
        ShowText = False
        Text = '00000000000000000000000000000000000000000000'
        WideBarRatio = 3.000000000000000000
        Zoom = 1.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
      end
      object Line2: TfrxLineView
        Left = 52.913420000000000000
        Top = 336.378170000000000000
        Width = 687.874015750000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line3: TfrxLineView
        Left = 86.929190000000000000
        Top = 368.504059060000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line4: TfrxLineView
        Left = 52.913420000000000000
        Top = 393.071120000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line5: TfrxLineView
        Left = 52.913420000000000000
        Top = 415.748300000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line6: TfrxLineView
        Left = 226.771653540000000000
        Top = 313.700990000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line7: TfrxLineView
        Left = 294.803152050000000000
        Top = 313.700990000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line8: TfrxLineView
        Left = 602.834645670000000000
        Top = 336.378170000000000000
        Height = 230.551330000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line9: TfrxLineView
        Left = 430.866420000000000000
        Top = 393.071120000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line10: TfrxLineView
        Left = 370.393940000000000000
        Top = 393.071120000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line11: TfrxLineView
        Left = 291.023810000000000000
        Top = 393.071120000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line12: TfrxLineView
        Left = 228.661417320000000000
        Top = 415.748300000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line33: TfrxLineView
        Left = 160.629921260000000000
        Top = 393.071120000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line34: TfrxLineView
        Left = 52.913420000000000000
        Top = 442.205010000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line35: TfrxLineView
        Left = 52.913420000000000000
        Top = 566.929255910000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line36: TfrxLineView
        Left = 52.913420000000000000
        Top = 627.401696850000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 1.500000000000000000
      end
      object Memo7: TfrxMemoView
        Left = 226.771800000000000000
        Top = 313.700990000000000000
        Width = 68.031540000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VAX]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo8: TfrxMemoView
        Left = 294.803340000000000000
        Top = 313.700990000000000000
        Width = 445.984540000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_LINHADIGITAVEL]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo9: TfrxMemoView
        Left = 52.913420000000000000
        Top = 336.378170000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Local de Pagamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo11: TfrxMemoView
        Left = 52.913420000000000000
        Top = 370.393940000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo12: TfrxMemoView
        Left = 52.913420000000000000
        Top = 393.071120000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo13: TfrxMemoView
        Left = 56.692950000000000000
        Top = 347.716760000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCfg."LocalPag"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo14: TfrxMemoView
        Left = 52.913420000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Uso do Banco')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo15: TfrxMemoView
        Left = 166.299320000000000000
        Top = 393.071120000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo16: TfrxMemoView
        Left = 294.803340000000000000
        Top = 393.071120000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo17: TfrxMemoView
        Left = 374.173470000000000000
        Top = 393.071120000000000000
        Width = 26.456710000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Aceite')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo18: TfrxMemoView
        Left = 434.645950000000000000
        Top = 393.071120000000000000
        Width = 68.031540000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Processamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo19: TfrxMemoView
        Left = 166.299320000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Carteira')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo20: TfrxMemoView
        Left = 230.551330000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo21: TfrxMemoView
        Left = 294.803340000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Quantidade')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo22: TfrxMemoView
        Left = 434.645950000000000000
        Top = 415.748300000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo23: TfrxMemoView
        Left = 427.086890000000000000
        Top = 425.196974880000000000
        Width = 7.559060000000000000
        Height = 8.314960630000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Fill.BackColor = clWhite
        Memo.UTF8W = (
          'X')
        ParentFont = False
        WordWrap = False
      end
      object Memo24: TfrxMemoView
        Left = 604.724800000000000000
        Top = 370.393940000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo25: TfrxMemoView
        Left = 604.724800000000000000
        Top = 393.071120000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo26: TfrxMemoView
        Left = 604.724800000000000000
        Top = 442.205010000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Desconto/Abatimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo27: TfrxMemoView
        Left = 604.724800000000000000
        Top = 468.661720000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Outras Dedu'#231#245'es')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo28: TfrxMemoView
        Left = 604.724800000000000000
        Top = 495.118430000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Mora/Multa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo29: TfrxMemoView
        Left = 604.724800000000000000
        Top = 517.795610000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Outros Acr'#233'scimos')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo30: TfrxMemoView
        Left = 604.724800000000000000
        Top = 544.252320000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor Cobrado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line37: TfrxLineView
        Left = 602.834645670000000000
        Top = 466.771775590000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line38: TfrxLineView
        Left = 602.834645670000000000
        Top = 491.338704720000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line39: TfrxLineView
        Left = 602.834645670000000000
        Top = 515.905633860000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line40: TfrxLineView
        Left = 602.834645670000000000
        Top = 540.472562990000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo33: TfrxMemoView
        Left = 589.606680000000000000
        Top = 631.181510000000000000
        Width = 151.181200000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo35: TfrxMemoView
        Left = 52.913420000000000000
        Top = 442.960754410000000000
        Width = 132.283550000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo36: TfrxMemoView
        Left = 56.692950000000000000
        Top = 377.953000000000000000
        Width = 544.252320000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCfg."NOMECED_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo37: TfrxMemoView
        Left = 56.692950000000000000
        Top = 402.141856770000000000
        Width = 102.047310000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsFatura."Data"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo38: TfrxMemoView
        Left = 162.519790000000000000
        Top = 402.141856770000000000
        Width = 128.504020000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsFatura."Duplicata"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo39: TfrxMemoView
        Left = 294.803340000000000000
        Top = 402.141856770000000000
        Width = 71.811070000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCfg."EspecieTit"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo40: TfrxMemoView
        Left = 374.173470000000000000
        Top = 402.141856770000000000
        Width = 52.913420000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCfg."ACEITETIT_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo41: TfrxMemoView
        Left = 434.645950000000000000
        Top = 402.141856770000000000
        Width = 166.299320000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[Date]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo42: TfrxMemoView
        Left = 166.299320000000000000
        Top = 424.819022130000000000
        Width = 60.472480000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCfg."CartNum"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo43: TfrxMemoView
        Left = 230.551330000000000000
        Top = 424.819022130000000000
        Width = 56.692950000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCfg."EspecieVal"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo44: TfrxMemoView
        Left = 604.724800000000000000
        Top = 402.141856770000000000
        Width = 136.063080000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo46: TfrxMemoView
        Left = 56.692950000000000000
        Top = 453.543307090000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_01]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo47: TfrxMemoView
        Left = 56.692950000000000000
        Top = 464.881889760000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_02]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo48: TfrxMemoView
        Left = 56.692950000000000000
        Top = 487.559055120000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_04]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo49: TfrxMemoView
        Left = 56.692950000000000000
        Top = 498.897640240000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_05]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo50: TfrxMemoView
        Left = 56.692950000000000000
        Top = 510.236220470000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_06]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo51: TfrxMemoView
        Left = 56.692950000000000000
        Top = 521.574803150000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_07]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo52: TfrxMemoView
        Left = 56.692950000000000000
        Top = 532.913385830000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_08]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo53: TfrxMemoView
        Left = 604.724800000000000000
        Top = 377.953000000000000000
        Width = 136.063080000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsCfg."AgContaCed"]')
        ParentFont = False
        WordWrap = False
        Wysiwyg = False
        VAlign = vaBottom
      end
      object Memo_001: TfrxMemoView
        Left = 52.913420000000000000
        Top = 306.141930000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCfg."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo58: TfrxMemoView
        Left = 56.692950000000000000
        Top = 37.795300000000000000
        Width = 684.094930000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          'Bloqueto em duas vias')
        ParentFont = False
      end
      object Picture3: TfrxPictureView
        Left = 56.692950000000000000
        Top = 60.472480000000000000
        Width = 168.188976380000000000
        Height = 68.031496060000000000
        HightQuality = True
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo31: TfrxMemoView
        Left = 619.842920000000000000
        Top = 612.283860000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'C'#243'digo de Baixa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo32: TfrxMemoView
        Left = 170.078850000000000000
        Top = 597.165740000000000000
        Width = 449.764070000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."E_ALL"]')
        ParentFont = False
      end
      object Memo34: TfrxMemoView
        Left = 170.078850000000000000
        Top = 582.047620000000000000
        Width = 449.764070000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."NOME_ENT"]')
        ParentFont = False
        WordWrap = False
      end
      object Memo45: TfrxMemoView
        Left = 170.078850000000000000
        Top = 566.929500000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo55: TfrxMemoView
        Left = 226.771800000000000000
        Top = 566.929500000000000000
        Width = 374.173470000000000000
        Height = 15.118112680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'Modelo A2')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo10: TfrxMemoView
        Left = 56.692950000000000000
        Top = 544.252320000000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_09]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo56: TfrxMemoView
        Left = 56.692950000000000000
        Top = 555.590902680000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_10]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo54: TfrxMemoView
        Left = 578.268090000000000000
        Top = 1131.859000000000000000
        Width = 162.519790000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'AUTENTICA'#199#195'O MEC'#194'NICA')
        ParentFont = False
      end
      object Memo57: TfrxMemoView
        Left = 56.692950000000000000
        Top = 901.307362440000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_03]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo59: TfrxMemoView
        Left = 602.834645670000000000
        Top = 849.905912130000000000
        Width = 137.952755910000000000
        Height = 17.385826770000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = '%2.2n'
        DisplayFormat.Kind = fkNumeric
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsFatura."CREDITO"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Picture_Bco1: TfrxPictureView
        Left = 52.913420000000000000
        Top = 719.890230000000000000
        Width = 173.858380000000000000
        Height = 41.574830000000000000
        HightQuality = False
        Transparent = False
        TransparentColor = clWhite
      end
      object Memo60: TfrxMemoView
        Left = 602.834645670000000000
        Top = 770.535833390000000000
        Width = 137.952755910000000000
        Height = 23.055118110000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsFatura."Vencimento"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo61: TfrxMemoView
        Left = 602.834645670000000000
        Top = 840.835190000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo62: TfrxMemoView
        Left = 602.834645670000000000
        Top = 761.465060000000000000
        Width = 137.952755910000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Vencimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object BarCode1: TfrxBarCodeView
        Left = 90.708661420000000000
        Top = 1060.047644410000000000
        Width = 405.000000000000000000
        Height = 49.133858270000000000
        BarType = bcCode_2_5_interleaved
        Expression = '<VARF_CODIGOBARRAS>'
        Rotation = 0
        ShowText = False
        Text = '00000000000000000000000000000000000000000000'
        WideBarRatio = 3.000000000000000000
        Zoom = 1.000000000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
      end
      object Line14: TfrxLineView
        Left = 52.913420000000000000
        Top = 761.465060000000000000
        Width = 687.874015750000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line15: TfrxLineView
        Left = 52.913420000000000000
        Top = 793.590949060000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line16: TfrxLineView
        Left = 52.913420000000000000
        Top = 818.158010000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line17: TfrxLineView
        Left = 52.913420000000000000
        Top = 840.835190000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line18: TfrxLineView
        Left = 226.771653540000000000
        Top = 738.787880000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line19: TfrxLineView
        Left = 294.803152050000000000
        Top = 738.787880000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line20: TfrxLineView
        Left = 602.834645670000000000
        Top = 761.465060000000000000
        Height = 230.551330000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line21: TfrxLineView
        Left = 430.866420000000000000
        Top = 818.158010000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line22: TfrxLineView
        Left = 370.393940000000000000
        Top = 818.158010000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line23: TfrxLineView
        Left = 291.023810000000000000
        Top = 818.158010000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line24: TfrxLineView
        Left = 228.661417320000000000
        Top = 840.835190000000000000
        Height = 22.677180000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line25: TfrxLineView
        Left = 160.629921260000000000
        Top = 818.158010000000000000
        Height = 49.133890000000000000
        Color = clBlack
        Frame.Typ = [ftLeft]
        Frame.Width = 0.100000000000000000
      end
      object Line26: TfrxLineView
        Left = 52.913420000000000000
        Top = 867.291900000000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line27: TfrxLineView
        Left = 52.913420000000000000
        Top = 992.016145910000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line28: TfrxLineView
        Left = 52.913420000000000000
        Top = 1052.488586850000000000
        Width = 687.874460000000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 1.500000000000000000
      end
      object Memo63: TfrxMemoView
        Left = 226.771800000000000000
        Top = 738.787880000000000000
        Width = 68.031540000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -24
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[VAX]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo64: TfrxMemoView
        Left = 294.803340000000000000
        Top = 738.787880000000000000
        Width = 445.984540000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_LINHADIGITAVEL]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo65: TfrxMemoView
        Left = 52.913420000000000000
        Top = 761.465060000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Local de Pagamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo66: TfrxMemoView
        Left = 52.913420000000000000
        Top = 795.480830000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo67: TfrxMemoView
        Left = 52.913420000000000000
        Top = 818.158010000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo68: TfrxMemoView
        Left = 56.692950000000000000
        Top = 772.803650000000000000
        Width = 544.252320000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCfg."LocalPag"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo69: TfrxMemoView
        Left = 52.913420000000000000
        Top = 840.835190000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Uso do Banco')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo70: TfrxMemoView
        Left = 166.299320000000000000
        Top = 818.158010000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'N'#250'mero do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo71: TfrxMemoView
        Left = 294.803340000000000000
        Top = 818.158010000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie do Documento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo72: TfrxMemoView
        Left = 374.173470000000000000
        Top = 818.158010000000000000
        Width = 26.456710000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Aceite')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo73: TfrxMemoView
        Left = 434.645950000000000000
        Top = 818.158010000000000000
        Width = 68.031540000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Data do Processamento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo74: TfrxMemoView
        Left = 166.299320000000000000
        Top = 840.835190000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Carteira')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo75: TfrxMemoView
        Left = 230.551330000000000000
        Top = 840.835190000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Esp'#233'cie')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo76: TfrxMemoView
        Left = 294.803340000000000000
        Top = 840.835190000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Quantidade')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo77: TfrxMemoView
        Left = 434.645950000000000000
        Top = 840.835190000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Valor')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo78: TfrxMemoView
        Left = 427.086890000000000000
        Top = 850.283864880000000000
        Width = 7.559060000000000000
        Height = 8.314960630000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Fill.BackColor = clWhite
        Memo.UTF8W = (
          'X')
        ParentFont = False
        WordWrap = False
      end
      object Memo79: TfrxMemoView
        Left = 604.724800000000000000
        Top = 795.480830000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Ag'#234'ncia/C'#243'digo Cedente')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo80: TfrxMemoView
        Left = 604.724800000000000000
        Top = 818.158010000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Nosso N'#250'mero')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo82: TfrxMemoView
        Left = 604.724800000000000000
        Top = 867.291900000000000000
        Width = 136.063080000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Desconto/Abatimento')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo83: TfrxMemoView
        Left = 604.724800000000000000
        Top = 893.748610000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(-) Outras Dedu'#231#245'es')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo84: TfrxMemoView
        Left = 604.724800000000000000
        Top = 920.205320000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Mora/Multa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo85: TfrxMemoView
        Left = 604.724800000000000000
        Top = 942.882500000000000000
        Width = 64.252010000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(+) Outros Acr'#233'scimos')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo86: TfrxMemoView
        Left = 604.724800000000000000
        Top = 969.339210000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          '(=) Valor Cobrado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Line29: TfrxLineView
        Left = 602.834645670000000000
        Top = 891.858665590000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line30: TfrxLineView
        Left = 602.834645670000000000
        Top = 916.425594720000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line31: TfrxLineView
        Left = 602.834645670000000000
        Top = 940.992523860000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Line32: TfrxLineView
        Left = 602.834645670000000000
        Top = 965.559452990000000000
        Width = 137.952755910000000000
        Color = clBlack
        Frame.Typ = [ftTop]
        Frame.Width = 0.100000000000000000
      end
      object Memo87: TfrxMemoView
        Left = 589.606680000000000000
        Top = 1056.268400000000000000
        Width = 151.181200000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'Autentica'#231#227'o Mec'#226'nica / FICHA DE COMPENSA'#199#195'O')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo88: TfrxMemoView
        Left = 52.913420000000000000
        Top = 868.047644410000000000
        Width = 132.283550000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Instru'#231#245'es (Texto de Responsabilidade do Cedente)')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo89: TfrxMemoView
        Left = 56.692950000000000000
        Top = 803.039890000000000000
        Width = 544.252320000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCfg."NOMECED_IMP"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo90: TfrxMemoView
        Left = 56.692950000000000000
        Top = 827.228746770000000000
        Width = 102.047310000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsFatura."Data"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo91: TfrxMemoView
        Left = 162.519790000000000000
        Top = 827.228746770000000000
        Width = 128.504020000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsFatura."Duplicata"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo92: TfrxMemoView
        Left = 294.803340000000000000
        Top = 827.228746770000000000
        Width = 71.811070000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCfg."EspecieTit"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo93: TfrxMemoView
        Left = 374.173470000000000000
        Top = 827.228746770000000000
        Width = 52.913420000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCfg."ACEITETIT_TXT"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo94: TfrxMemoView
        Left = 434.645950000000000000
        Top = 827.228746770000000000
        Width = 166.299320000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        DisplayFormat.FormatStr = 'dd/mm/yyyy'
        DisplayFormat.Kind = fkDateTime
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[Date]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo95: TfrxMemoView
        Left = 166.299320000000000000
        Top = 849.905912130000000000
        Width = 60.472480000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCfg."CartNum"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo96: TfrxMemoView
        Left = 230.551330000000000000
        Top = 849.905912130000000000
        Width = 56.692950000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = []
        HAlign = haCenter
        Memo.UTF8W = (
          '[frxDsCfg."EspecieVal"]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo97: TfrxMemoView
        Left = 604.724800000000000000
        Top = 827.228746770000000000
        Width = 136.063080000000000000
        Height = 11.338590000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[VARF_NossoNumero]')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo98: TfrxMemoView
        Left = 56.692950000000000000
        Top = 878.630197090000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_01]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo99: TfrxMemoView
        Left = 56.692950000000000000
        Top = 889.968779760000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_02]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo100: TfrxMemoView
        Left = 56.692950000000000000
        Top = 912.645945120000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_04]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo101: TfrxMemoView
        Left = 56.692950000000000000
        Top = 923.984530240000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_05]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo102: TfrxMemoView
        Left = 56.692950000000000000
        Top = 935.323110470000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_06]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo103: TfrxMemoView
        Left = 56.692950000000000000
        Top = 946.661693150000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_07]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo104: TfrxMemoView
        Left = 56.692950000000000000
        Top = 958.000275830000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_08]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo105: TfrxMemoView
        Left = 604.724800000000000000
        Top = 803.039890000000000000
        Width = 136.063080000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Times New Roman'
        Font.Style = [fsBold]
        HAlign = haRight
        Memo.UTF8W = (
          '[frxDsCfg."AgContaCed"]')
        ParentFont = False
        WordWrap = False
        Wysiwyg = False
        VAlign = vaBottom
      end
      object Memo_101: TfrxMemoView
        Left = 52.913420000000000000
        Top = 731.228820000000000000
        Width = 173.858380000000000000
        Height = 30.236240000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsCfg."NOMEBANCO"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo107: TfrxMemoView
        Left = 619.842920000000000000
        Top = 1037.370750000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'C'#243'digo de Baixa')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo108: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1022.252630000000000000
        Width = 449.764070000000000000
        Height = 22.677180000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."E_ALL"]')
        ParentFont = False
      end
      object Memo109: TfrxMemoView
        Left = 170.078850000000000000
        Top = 1007.134510000000000000
        Width = 449.764070000000000000
        Height = 15.118120000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Times New Roman'
        Font.Style = []
        Memo.UTF8W = (
          '[frxDsEndereco."NOME_ENT"]')
        ParentFont = False
        WordWrap = False
      end
      object Memo110: TfrxMemoView
        Left = 170.078850000000000000
        Top = 992.016390000000000000
        Width = 56.692950000000000000
        Height = 9.070866140000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -8
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        Memo.UTF8W = (
          'Sacado')
        ParentFont = False
        WordWrap = False
        VAlign = vaCenter
      end
      object Memo111: TfrxMemoView
        Left = 226.771800000000000000
        Top = 992.016390000000000000
        Width = 374.173470000000000000
        Height = 15.118112680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -9
        Font.Name = 'Univers Light Condensed'
        Font.Style = []
        HAlign = haRight
        Memo.UTF8W = (
          'Modelo A2')
        ParentFont = False
        WordWrap = False
        VAlign = vaBottom
      end
      object Memo112: TfrxMemoView
        Left = 56.692950000000000000
        Top = 969.339210000000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_09]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo113: TfrxMemoView
        Left = 56.692950000000000000
        Top = 980.677792680000000000
        Width = 544.252320000000000000
        Height = 11.338582680000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Courier New'
        Font.Style = []
        Memo.UTF8W = (
          '[VAR_INSTRUCAO_10]')
        ParentFont = False
        VAlign = vaCenter
      end
    end
  end
  object QrCfg: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCfgCalcFields
    SQL.Strings = (
      'SELECT bco.DVB, bco.Nome NOMEBANCO, cfg.*,'
      
        'IF(cfg.Cedente=0,"",IF(ced.Tipo=0,ced.RazaoSocial,ced.Nome)) NOM' +
        'ECED_IMP,'
      
        'IF(cfg.SacadAvali=0,"",IF(ava.Tipo=0,ava.RazaoSocial,ava.Nome)) ' +
        'NOMESAC_IMP,'
      'IF(cfg.CartTxt <> "", cfg.CartTxt, cfg.CartNum) CART_IMP'
      'FROM cnab_cfg cfg'
      'LEFT JOIN entidades ced ON ced.Codigo=cfg.Cedente'
      'LEFT JOIN entidades ava ON ava.Codigo=cfg.SacadAvali'
      'LEFT JOIN bancos bco ON bco.Codigo=cfg.CedBanco'
      'WHERE cfg.Codigo=:P0')
    Left = 576
    Top = 348
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCfgLocalPag: TWideStringField
      FieldName = 'LocalPag'
      Origin = 'cnab_cfg.LocalPag'
      Size = 127
    end
    object QrCfgNOMECED_IMP: TWideStringField
      FieldName = 'NOMECED_IMP'
      Size = 100
    end
    object QrCfgNOMESAC_IMP: TWideStringField
      FieldName = 'NOMESAC_IMP'
      Size = 100
    end
    object QrCfgCedente: TIntegerField
      FieldName = 'Cedente'
      Origin = 'cnab_cfg.Cedente'
    end
    object QrCfgSacadAvali: TIntegerField
      FieldName = 'SacadAvali'
      Origin = 'cnab_cfg.SacadAvali'
    end
    object QrCfgEspecieTit: TWideStringField
      FieldName = 'EspecieTit'
      Origin = 'cnab_cfg.EspecieTit'
      Size = 2
    end
    object QrCfgAceiteTit: TSmallintField
      FieldName = 'AceiteTit'
      Origin = 'cnab_cfg.AceiteTit'
    end
    object QrCfgNOMEBANCO: TWideStringField
      FieldName = 'NOMEBANCO'
      Origin = 'bancos.Nome'
      Size = 100
    end
    object QrCfgCartNum: TWideStringField
      FieldName = 'CartNum'
      Origin = 'cnab_cfg.CartNum'
      Size = 3
    end
    object QrCfgEspecieVal: TWideStringField
      FieldName = 'EspecieVal'
      Origin = 'cnab_cfg.EspecieVal'
      Size = 5
    end
    object QrCfgTipoCart: TSmallintField
      FieldName = 'TipoCart'
      Origin = 'cnab_cfg.TipoCart'
    end
    object QrCfgCedBanco: TIntegerField
      FieldName = 'CedBanco'
      Origin = 'cnab_cfg.CedBanco'
    end
    object QrCfgCedAgencia: TIntegerField
      FieldName = 'CedAgencia'
      Origin = 'cnab_cfg.CedAgencia'
    end
    object QrCfgCedConta: TWideStringField
      FieldName = 'CedConta'
      Origin = 'cnab_cfg.CedConta'
      Size = 30
    end
    object QrCfgCedDAC_A: TWideStringField
      FieldName = 'CedDAC_A'
      Origin = 'cnab_cfg.CedDAC_A'
      Size = 1
    end
    object QrCfgCedDAC_C: TWideStringField
      FieldName = 'CedDAC_C'
      Origin = 'cnab_cfg.CedDAC_C'
      Size = 1
    end
    object QrCfgCedDAC_AC: TWideStringField
      FieldName = 'CedDAC_AC'
      Origin = 'cnab_cfg.CedDAC_AC'
      Size = 1
    end
    object QrCfgCedNome: TWideStringField
      FieldName = 'CedNome'
      Origin = 'cnab_cfg.CedNome'
      Size = 100
    end
    object QrCfgCedPosto: TIntegerField
      FieldName = 'CedPosto'
      Origin = 'cnab_cfg.CedPosto'
    end
    object QrCfgCodEmprBco: TWideStringField
      FieldName = 'CodEmprBco'
      Origin = 'cnab_cfg.CodEmprBco'
    end
    object QrCfgOperCodi: TWideStringField
      FieldName = 'OperCodi'
      Origin = 'cnab_cfg.OperCodi'
      Size = 3
    end
    object QrCfgIDCobranca: TWideStringField
      FieldName = 'IDCobranca'
      Origin = 'cnab_cfg.IDCobranca'
      Size = 2
    end
    object QrCfgDVB: TWideStringField
      FieldName = 'DVB'
      Size = 1
    end
    object QrCfgTexto01: TWideStringField
      FieldName = 'Texto01'
      Size = 100
    end
    object QrCfgTexto02: TWideStringField
      FieldName = 'Texto02'
      Size = 100
    end
    object QrCfgTexto03: TWideStringField
      FieldName = 'Texto03'
      Size = 100
    end
    object QrCfgTexto04: TWideStringField
      FieldName = 'Texto04'
      Size = 100
    end
    object QrCfgTexto05: TWideStringField
      FieldName = 'Texto05'
      Size = 100
    end
    object QrCfgTexto06: TWideStringField
      FieldName = 'Texto06'
      Size = 100
    end
    object QrCfgTexto07: TWideStringField
      FieldName = 'Texto07'
      Size = 100
    end
    object QrCfgTexto08: TWideStringField
      FieldName = 'Texto08'
      Size = 100
    end
    object QrCfgTexto09: TWideStringField
      FieldName = 'Texto09'
      Size = 100
    end
    object QrCfgTexto10: TWideStringField
      FieldName = 'Texto10'
      Size = 100
    end
    object QrCfgAgContaCed: TWideStringField
      FieldName = 'AgContaCed'
      Size = 40
    end
    object QrCfgACEITETIT_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ACEITETIT_TXT'
      Size = 1
      Calculated = True
    end
    object QrCfgTipoCobranca: TIntegerField
      FieldName = 'TipoCobranca'
    end
    object QrCfgEspecieDoc: TWideStringField
      FieldName = 'EspecieDoc'
    end
    object QrCfgCNAB: TIntegerField
      FieldName = 'CNAB'
    end
    object QrCfgCtaCooper: TWideStringField
      FieldName = 'CtaCooper'
      Size = 7
    end
    object QrCfgCorresBco: TIntegerField
      FieldName = 'CorresBco'
    end
    object QrCfgLayoutRem: TWideStringField
      FieldName = 'LayoutRem'
      Size = 50
    end
    object QrCfgCorresAge: TIntegerField
      FieldName = 'CorresAge'
    end
    object QrCfgCorresCto: TWideStringField
      FieldName = 'CorresCto'
      Size = 30
    end
    object QrCfgCART_IMP: TWideStringField
      FieldName = 'CART_IMP'
      Size = 10
    end
  end
  object frxDsCfg: TfrxDBDataset
    UserName = 'frxDsCfg'
    CloseDataSource = False
    FieldAliases.Strings = (
      'LocalPag=LocalPag'
      'NOMECED_IMP=NOMECED_IMP'
      'NOMESAC_IMP=NOMESAC_IMP'
      'Cedente=Cedente'
      'SacadAvali=SacadAvali'
      'EspecieTit=EspecieTit'
      'AceiteTit=AceiteTit'
      'NOMEBANCO=NOMEBANCO'
      'CartNum=CartNum'
      'EspecieVal=EspecieVal'
      'TipoCart=TipoCart'
      'CedBanco=CedBanco'
      'CedAgencia=CedAgencia'
      'CedConta=CedConta'
      'CedDAC_A=CedDAC_A'
      'CedDAC_C=CedDAC_C'
      'CedDAC_AC=CedDAC_AC'
      'CedNome=CedNome'
      'CedPosto=CedPosto'
      'CodEmprBco=CodEmprBco'
      'OperCodi=OperCodi'
      'IDCobranca=IDCobranca'
      'DVB=DVB'
      'Texto01=Texto01'
      'Texto02=Texto02'
      'Texto03=Texto03'
      'Texto04=Texto04'
      'Texto05=Texto05'
      'Texto06=Texto06'
      'Texto07=Texto07'
      'Texto08=Texto08'
      'Texto09=Texto09'
      'Texto10=Texto10'
      'AgContaCed=AgContaCed'
      'ACEITETIT_TXT=ACEITETIT_TXT'
      'TipoCobranca=TipoCobranca'
      'EspecieDoc=EspecieDoc'
      'CNAB=CNAB'
      'CtaCooper=CtaCooper'
      'CorresBco=CorresBco'
      'LayoutRem=LayoutRem'
      'CorresAge=CorresAge'
      'CorresCto=CorresCto'
      'CART_IMP=CART_IMP')
    DataSet = QrCfg
    BCDToCurrency = False
    Left = 604
    Top = 348
  end
  object frxDsFatura: TfrxDBDataset
    UserName = 'frxDsFatura'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Controle=Controle'
      'Data=Data'
      'Vencimento=Vencimento'
      'Documento=Documento'
      'Duplicata=Duplicata'
      'Compensado=Compensado'
      'MoraDia=MoraDia'
      'Multa=Multa'
      'Vendedor=Vendedor'
      'Account=Account'
      'COMPENSADO_TXT=COMPENSADO_TXT'
      'BLOQUETO=BLOQUETO'
      'Valor=Valor'
      'CREDITO=CREDITO')
    DataSet = QrFatura
    BCDToCurrency = False
    Left = 604
    Top = 376
  end
end
