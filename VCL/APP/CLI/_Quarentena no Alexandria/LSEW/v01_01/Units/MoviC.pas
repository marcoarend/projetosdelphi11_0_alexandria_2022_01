unit MoviC;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, ComCtrls,
  Grids, DBGrids, Menus, Variants, dmkPermissoes, dmkGeral, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, frxClass, frxDBSet, dmkLabel, UnDmkProcFunc, UnDmkEnums;

type
  TFmMoviC = class(TForm)
    PainelDados: TPanel;
    DsMoviC: TDataSource;
    QrMoviC: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PnControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtPagtos: TBitBtn;
    BtItens: TBitBtn;
    BtCompra: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TEdit;
    Label10: TLabel;
    PnData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    QrMoviCCodigo: TIntegerField;
    QrMoviCControle: TIntegerField;
    QrMoviCDataPedi: TDateField;
    QrMoviCDataReal: TDateField;
    QrMoviCFornece: TIntegerField;
    QrMoviCLk: TIntegerField;
    QrMoviCDataCad: TDateField;
    QrMoviCDataAlt: TDateField;
    QrMoviCUserCad: TIntegerField;
    QrMoviCUserAlt: TIntegerField;
    Label3: TLabel;
    EdDataPedi: TEdit;
    EdDataReal: TEdit;
    Label4: TLabel;
    EdFornece: TdmkEditCB;
    CBFornece: TdmkDBLookupComboBox;
    QrFornece: TmySQLQuery;
    DsFornece: TDataSource;
    QrForneceCodigo: TIntegerField;
    QrForneceNOMEENTI: TWideStringField;
    Label2: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    QrMoviCNOMEENTI: TWideStringField;
    DBEdit3: TDBEdit;
    QrMoviCDATAREAL_TXT: TWideStringField;
    QrMovim: TmySQLQuery;
    DsMovim: TDataSource;
    QrMovimNOMEGRA: TWideStringField;
    QrMovimNOMETAM: TWideStringField;
    QrMovimNOMECOR: TWideStringField;
    QrMovimControle: TIntegerField;
    QrMovimConta: TIntegerField;
    QrMovimGrade: TIntegerField;
    QrMovimCor: TIntegerField;
    QrMovimTam: TIntegerField;
    QrMovimQtd: TFloatField;
    QrMovimVal: TFloatField;
    QrMovimLk: TIntegerField;
    QrMovimDataCad: TDateField;
    QrMovimDataAlt: TDateField;
    QrMovimUserCad: TIntegerField;
    QrMovimUserAlt: TIntegerField;
    QrMovimDataPedi: TDateField;
    QrMovimDataReal: TDateField;
    PMCompra: TPopupMenu;
    Incluinovacompra1: TMenuItem;
    Alteracompraatual1: TMenuItem;
    Excluicompraatual1: TMenuItem;
    PMItens: TPopupMenu;
    Incluiitensnacompraatual1: TMenuItem;
    Excluiitematual1: TMenuItem;
    PnItens: TPanel;
    Panel2: TPanel;
    BtConfItem: TBitBtn;
    Panel1: TPanel;
    Label7: TLabel;
    Label8: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    DBEdit13: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBGrid2: TDBGrid;
    GradeA: TStringGrid;
    Panel4: TPanel;
    Label13: TLabel;
    EdGrade: TdmkEditCB;
    Label14: TLabel;
    EdCorCod: TEdit;
    EdCorNom: TEdit;
    Label15: TLabel;
    EdTamCod: TEdit;
    EdTamNom: TEdit;
    Label16: TLabel;
    EdQtd: TEdit;
    EdPrc: TEdit;
    Label17: TLabel;
    EdVal: TEdit;
    Label18: TLabel;
    QrGrades: TmySQLQuery;
    DsGrades: TDataSource;
    QrGradesCodigo: TIntegerField;
    QrGradesNome: TWideStringField;
    CBGrade: TdmkDBLookupComboBox;
    QrGradesCors: TmySQLQuery;
    QrGradesCorsCodigo: TIntegerField;
    QrGradesCorsControle: TIntegerField;
    QrGradesCorsCor: TIntegerField;
    QrGradesCorsLk: TIntegerField;
    QrGradesCorsDataCad: TDateField;
    QrGradesCorsDataAlt: TDateField;
    QrGradesCorsUserCad: TIntegerField;
    QrGradesCorsUserAlt: TIntegerField;
    QrGradesCorsNOMECOR: TWideStringField;
    QrGradesTams: TmySQLQuery;
    QrGradesTamsCodigo: TIntegerField;
    QrGradesTamsControle: TIntegerField;
    QrGradesTamsTam: TIntegerField;
    QrGradesTamsLk: TIntegerField;
    QrGradesTamsDataCad: TDateField;
    QrGradesTamsDataAlt: TDateField;
    QrGradesTamsUserCad: TIntegerField;
    QrGradesTamsUserAlt: TIntegerField;
    QrGradesTamsNOMETAM: TWideStringField;
    QrProdutos: TmySQLQuery;
    QrProdutosTam: TIntegerField;
    QrProdutosCor: TIntegerField;
    QrProdutosAtivo: TSmallintField;
    QrProdutosConta: TIntegerField;
    GradeC: TStringGrid;
    LaStatus: TLabel;
    Panel6: TPanel;
    BitBtn2: TBitBtn;
    QrMovim2: TmySQLQuery;
    QrMovim2Grade: TIntegerField;
    QrMovim2Cor: TIntegerField;
    QrMovim2Tam: TIntegerField;
    QrEstq: TmySQLQuery;
    QrEstqEstqQ: TFloatField;
    QrEstqEstqV: TFloatField;
    QrMovimPRECO: TFloatField;
    PnPagtos: TPanel;
    StaticText4: TStaticText;
    GridPagtos: TDBGrid;
    PnDItens: TPanel;
    StaticText1: TStaticText;
    GradeItens: TDBGrid;
    QrPagtos: TmySQLQuery;
    QrPagtosSEQ: TIntegerField;
    DsPagtos: TDataSource;
    QrSumP: TmySQLQuery;
    PMPagtos: TPopupMenu;
    IncluiPagtos1: TMenuItem;
    QrMoviCSALDO: TFloatField;
    QrSumPDebito: TFloatField;
    QrMoviCTotal: TFloatField;
    QrMoviCPago: TFloatField;
    Label19: TLabel;
    DBEdit7: TDBEdit;
    Label20: TLabel;
    DBEdit8: TDBEdit;
    Label21: TLabel;
    DBEdit9: TDBEdit;
    ExcluiPagtos1: TMenuItem;
    QrPagtosData: TDateField;
    QrPagtosVencimento: TDateField;
    QrPagtosDebito: TFloatField;
    QrPagtosBanco: TIntegerField;
    QrPagtosFatID: TIntegerField;
    QrPagtosContaCorrente: TWideStringField;
    QrPagtosDocumento: TFloatField;
    QrPagtosDescricao: TWideStringField;
    QrPagtosFatParcela: TIntegerField;
    QrPagtosNOMECARTEIRA2: TWideStringField;
    QrPagtosBanco1: TIntegerField;
    QrPagtosAgencia1: TIntegerField;
    QrPagtosConta1: TWideStringField;
    QrPagtosTipoDoc: TSmallintField;
    QrPagtosControle: TIntegerField;
    QrPagtosNOMEFORNECEI: TWideStringField;
    QrPagtosCARTEIRATIPO: TIntegerField;
    QrSumM: TmySQLQuery;
    QrSumMVal: TFloatField;
    DBEdit6: TDBEdit;
    Label22: TLabel;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    Label23: TLabel;
    Label24: TLabel;
    DBEdit12: TDBEdit;
    Label25: TLabel;
    QrMoviCAlterWeb: TSmallintField;
    QrMoviCAtivo: TSmallintField;
    QrMovimVen: TFloatField;
    QrMovimMotivo: TSmallintField;
    QrMovimSALDOQTD: TFloatField;
    QrMovimSALDOVAL: TFloatField;
    QrMovimAlterWeb: TSmallintField;
    QrMovimAtivo: TSmallintField;
    QrGradesTamsAlterWeb: TSmallintField;
    QrGradesTamsAtivo: TSmallintField;
    QrGradesCorsAlterWeb: TSmallintField;
    QrGradesCorsAtivo: TSmallintField;
    QrPagtosFatNum: TFloatField;
    QrPagtosNOMECARTEIRA: TWideStringField;
    SpeedButton5: TSpeedButton;
    StaticText2: TStaticText;
    SpeedButton6: TSpeedButton;
    dmkPermissoes1: TdmkPermissoes;
    QrGradesCodUsu: TIntegerField;
    frxPedidosC: TfrxReport;
    frxDsMoviC: TfrxDBDataset;
    frxDsMovim: TfrxDBDataset;
    frxDsCli: TfrxDBDataset;
    QrCli: TmySQLQuery;
    QrCliE_ALL: TWideStringField;
    QrCliCNPJ_TXT: TWideStringField;
    QrCliNOME_TIPO_DOC: TWideStringField;
    QrCliTE1_TXT: TWideStringField;
    QrCliFAX_TXT: TWideStringField;
    QrCliNUMERO_TXT: TWideStringField;
    QrCliCEP_TXT: TWideStringField;
    QrCliCodigo: TIntegerField;
    QrCliTipo: TSmallintField;
    QrCliCodUsu: TIntegerField;
    QrCliNOME_ENT: TWideStringField;
    QrCliCNPJ_CPF: TWideStringField;
    QrCliIE_RG: TWideStringField;
    QrCliRUA: TWideStringField;
    QrCliCOMPL: TWideStringField;
    QrCliBAIRRO: TWideStringField;
    QrCliCIDADE: TWideStringField;
    QrCliNOMELOGRAD: TWideStringField;
    QrCliNOMEUF: TWideStringField;
    QrCliPais: TWideStringField;
    QrCliENDEREF: TWideStringField;
    QrCliTE1: TWideStringField;
    QrCliFAX: TWideStringField;
    QrCliIE: TWideStringField;
    QrCliCAD_FEDERAL: TWideStringField;
    QrCliCAD_ESTADUAL: TWideStringField;
    QrCliIE_TXT: TWideStringField;
    QrCliNUMERO: TLargeintField;
    QrCliUF: TLargeintField;
    QrCliLograd: TLargeintField;
    QrCliCEP: TLargeintField;
    QrPagtosAgencia: TIntegerField;
    Panel7: TPanel;
    BtDesiste: TBitBtn;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrMoviCAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure QrMoviCAfterScroll(DataSet: TDataSet);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMoviCBeforeOpen(DataSet: TDataSet);
    procedure EdDataPediExit(Sender: TObject);
    procedure EdDataRealExit(Sender: TObject);
    procedure QrMoviCCalcFields(DataSet: TDataSet);
    procedure BtCompraClick(Sender: TObject);
    procedure Incluinovacompra1Click(Sender: TObject);
    procedure Alteracompraatual1Click(Sender: TObject);
    procedure Excluicompraatual1Click(Sender: TObject);
    procedure Incluiitensnacompraatual1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtConfItemClick(Sender: TObject);
    procedure EdGradeChange(Sender: TObject);
    procedure BtItensClick(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure GradeAClick(Sender: TObject);
    procedure EdQtdExit(Sender: TObject);
    procedure EdPrcExit(Sender: TObject);
    procedure EdValExit(Sender: TObject);
    procedure EdQtdEnter(Sender: TObject);
    procedure EdPrcEnter(Sender: TObject);
    procedure EdValEnter(Sender: TObject);
    procedure QrMovimCalcFields(DataSet: TDataSet);
    procedure IncluiPagtos1Click(Sender: TObject);
    procedure ExcluiPagtos1Click(Sender: TObject);
    procedure BtPagtosClick(Sender: TObject);
    procedure Excluiitematual1Click(Sender: TObject);
    procedure QrMoviCBeforeClose(DataSet: TDataSet);
    procedure PMCompraPopup(Sender: TObject);
    procedure PMItensPopup(Sender: TObject);
    procedure PMPagtosPopup(Sender: TObject);
    procedure EdDataRealKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SpeedButton5Click(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrCliCalcFields(DataSet: TDataSet);
    procedure FormShow(Sender: TObject);
  private
    FDataReal: TDateTime;
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra : Integer; Status : String; Codigo : Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenMovim(Conta: Integer);
    procedure ReopenGradesTams(Grade: Integer);
    procedure ReopenGradesCors(Grade: Integer);
    procedure ReopenProdutos(Grade: Integer);
    procedure ReopenPagtos;
    procedure ConfigGrades;
    procedure AtualizaGradeA(Grade: Integer);
    procedure LimpaEdits;
    procedure CalculaPreco(Como: TCalcVal);
    procedure AtualizaValores(MoviC, Movim, Pagto: Integer);
    procedure ReopenCli;
  public
    { Public declarations }
    FSeq: Integer;
    FQtd, FPrc, FVal: Double;
    FTabLctALS: String;
  end;

var
  FmMoviC: TFmMoviC;
const
  FFormatFloat = '00000';

implementation

uses Module, MyVCLSkin, UnInternalConsts3, MyDBCheck, Entidades, ModuleProd,
  Principal, ModuleGeral, UnMyObjects, UnPagtos, DmkDAC_PF;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMoviC.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMoviC.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMoviCCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMoviC.DefParams;
begin
  VAR_GOTOTABELA := 'movic';
  VAR_GOTOMYSQLTABLE := QrMoviC;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('');
  VAR_SQLx.Add('SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial');
  VAR_SQLx.Add('ELSE ent.Nome END NOMEENTI, moc.*');
  VAR_SQLx.Add('FROM movic moc');
  VAR_SQLx.Add('LEFT JOIN entidades ent ON ent.Codigo=moc.Fornece');
  VAR_SQLx.Add('WHERE moc.Codigo > -1000');
  //
  VAR_SQL1.Add('AND moc.Codigo=:P0');
  //
  VAR_SQLa.Add('');//AND Nome Like :P0');
  //
end;

procedure TFmMoviC.MostraEdicao(Mostra : Integer; Status : String; Codigo : Integer);
begin
  case Mostra of
    0:
    begin
      PnPagtos.Align         := alTop;
      PnControle.Visible     := True;
      PnPagtos.Align         := alBottom;
      //
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
      PnItens.Visible        := False;
    end;
    1:
    begin
      PainelEdita.Visible    := True;
      PainelDados.Visible    := False;
      PnControle.Visible     := False;
      if Status = CO_INCLUSAO then
      begin
        FDataReal            := 0;
        EdCodigo.Text        := '';
        EdDataPedi.Text      := Geral.FDT(Date, 2);
        EdDataReal.Text      := '000000';
        EdFornece.Text       := '';
        CBFornece.KeyValue   := Null;
      end else begin
        FDataReal            := QrMoviCDataReal.Value;
        EdCodigo.Text        := IntToStr(QrMoviCCodigo.Value);
        EdDataPedi.Text      := Geral.FDT(QrMoviCDataPedi.Value, 2);
        EdDataReal.Text      := QrMoviCDATAREAL_TXT.Value;
        EdFornece.Text       := IntToStr(QrMoviCFornece.Value);
        CBFornece.KeyValue   := QrMoviCFornece.Value;
      end;
      if EdDataReal.Text = '' then EdDataReal.Text := '0000';
      EdDataPedi.SetFocus;
    end;
    2:
    begin
      if (QrMoviC.State <> dsBrowse) or (QrMoviC.RecordCount = 0) then Exit;
      if QrMoviCDataReal.Value > 1 then
        if DmProd.ImpedePeloBalanco(QrMoviCDataReal.Value) then Exit;
      PnItens.Visible        := True;
      PainelDados.Visible    := False;
      PnControle.Visible     := False;
      if Status = CO_INCLUSAO then
      begin

      end else begin

      end;
    end;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
  if Codigo <> 0 then LocCod(Codigo, Codigo);
end;

procedure TFmMoviC.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmMoviC.QueryPrincipalAfterOpen;
begin

end;

procedure TFmMoviC.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMoviC.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMoviC.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMoviC.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMoviC.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMoviC.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrMoviCCodigo.Value;
  Close;
end;

procedure TFmMoviC.BtConfirmaClick(Sender: TObject);
var
  Codigo, Controle, Fornece: Integer;
  DataPedi, DataReal: TDateTime;
  QtdX, ValX, CusX: Double;
begin
  DataPedi := Geral.ValidaDataSimples(EdDataPedi.Text, False);
  DataReal := Geral.ValidaDataSimples(EdDataReal.Text, False);
  if (DataReal > 0) then if DmProd.ImpedePeloBalanco(DataReal) then Exit;
  if (DataReal = 0) and (FDataReal > 0) then
  begin
    Application.MessageBox('Entrada n�o pode mais ser anulada!', 'Aviso',
    MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if DataReal > 0 then
    if DmProd.ImpedePeloBalanco(DataReal) then Exit;
  Fornece := Geral.IMV(EdFornece.Text);
  if Fornece = 0 then
  begin
    Application.MessageBox('Defina um fornecedor.', 'Erro', MB_OK+MB_ICONERROR);
    EdFornece.SetFocus;
    Exit;
  end;
  Dmod.QrUpdU.SQL.Clear;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    Dmod.QrUpdU.SQL.Add('INSERT INTO movic SET ');
    Codigo := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle',
    'movic', 'movic', 'codigo');

    Controle := Dmod.BuscaProximoMovix;
  end else begin
    Dmod.QrUpdU.SQL.Add('UPDATE movic SET ');
    Codigo := QrMoviCCodigo.Value;
    Controle := QrMoviCControle.Value;
  end;
  Dmod.QrUpdU.SQL.Add('Fornece=:P0, DataPedi=:P1, DataReal=:P2, ');
  //
  if LaTipo.Caption = CO_INCLUSAO then
    Dmod.QrUpdU.SQL.Add('DataCad=:Pa, UserCad=:Pb, Codigo=:Pc, Controle=:Pd')
  else Dmod.QrUpdU.SQL.Add(
    'DataAlt=:Pa, UserAlt=:Pb WHERE Codigo=:Pc AND Controle=:Pd');
  Dmod.QrUpdU.Params[00].AsInteger := Fornece;
  Dmod.QrUpdU.Params[01].AsString  := Geral.FDT(DataPedi, 1);
  Dmod.QrUpdU.Params[02].AsString  := Geral.FDT(DataReal, 1);
  //
  Dmod.QrUpdU.Params[03].AsString  := FormatDateTime(VAR_FORMATDATE, Date);
  Dmod.QrUpdU.Params[04].AsInteger := VAR_USUARIO;
  Dmod.QrUpdU.Params[05].AsInteger := Codigo;
  Dmod.QrUpdU.Params[06].AsInteger := Controle;
  Dmod.QrUpdU.ExecSQL;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('UPDATE movim SET DataPedi=:P0, DataReal=:P1');
  Dmod.QrUpdU.SQL.Add('WHERE Controle=:P2');
  Dmod.QrUpdU.Params[00].AsString  := Geral.FDT(DataPedi, 1);
  Dmod.QrUpdU.Params[01].AsString  := Geral.FDT(DataReal, 1);
  //
  Dmod.QrUpdU.Params[02].AsInteger := Controle;
  Dmod.QrUpdU.ExecSQL;
  //if LaTipo.Caption = CO_ALTERACAO then
  //begin
    //if (DataReal > 0) and (QrMoviCDataReal.Value = 0) then
    //begin
      QrMovim2.Close;
      QrMovim2.Params[0].AsInteger := QrMoviCControle.Value;
      QrMovim2.Open;
      QrMovim2.First;
      while not QrMovim2.Eof do
      begin
        DmProd.AtualizaEstoqueMercadoria(QrMovim2Grade.Value,
          QrMovim2Cor.Value, QrMovim2Tam.Value, True, True, QtdX, ValX, CusX);
        QrMovim2.Next;
      end;
    //end;
  //end;
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'movic', 'Codigo');
  AtualizaValores(Codigo, QrMovimConta.Value, QrPagtosControle.Value);
  MostraEdicao(0, CO_TRAVADO, 0);
  LocCod(Codigo,Codigo);
  if FSeq = 1 then Close;
end;

procedure TFmMoviC.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', 'movic', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'movic', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'movic', 'Codigo');
end;

procedure TFmMoviC.FormCreate(Sender: TObject);
begin
  FSeq := 0;
  FDataReal := 0;
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  PnDItens.Align    := alClient;
  GradeA.Align      := alClient;
  QrFornece.Open;
  QrGrades.Open;
  GradeA.ColWidths[0] := 100;
  FQtd := 0;
  FPrc := 0;
  FVal := 0;
  DModG.ReopenEndereco(Dmod.QrMasterDono.Value);
end;

procedure TFmMoviC.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMoviCCodigo.Value,LaRegistro.Caption);
end;

procedure TFmMoviC.SbImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxPedidosC, 'Pedidos');
end;

procedure TFmMoviC.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMoviC.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_MARCA := QrMoviCCodigo.Value;
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmMoviC.QrCliCalcFields(DataSet: TDataSet);
begin
  QrCliTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrCliTe1.Value);
  QrCliFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrCliFax.Value);
  QrCliNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrCliRUA.Value, QrCliNumero.Value, False);
  //
  QrCliE_ALL.Value := Uppercase(QrCliNOMELOGRAD.Value);
  if Trim(QrCliE_ALL.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ';
  QrCliE_ALL.Value := QrCliE_ALL.Value + Uppercase(QrCliRua.Value);
  if Trim(QrCliRua.Value) <> '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNUMERO_TXT.Value;
  if Trim(QrCliCompl.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' ' + Uppercase(QrCliCompl.Value);
  if Trim(QrCliBairro.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliBairro.Value);
  if QrCliCEP.Value > 0 then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' CEP ' + Geral.FormataCEP_NT(QrCliCEP.Value);
  if Trim(QrCliCidade.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + Uppercase(QrCliCidade.Value);
  if Trim(QrCliNOMEUF.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ', ' + QrCliNOMEUF.Value;
  if Trim(QrCliPais.Value) <>  '' then QrCliE_ALL.Value :=
    QrCliE_ALL.Value + ' - ' + QrCliPais.Value;
end;

procedure TFmMoviC.QrMoviCAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmMoviC.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //if UMyMod.NegaInclusaoY(Dmod.MyDB, 'Controle', 'MoviC', 'Livres', 99) then
  //BtInclui.Enabled := False;
  if FSeq = 1 then MostraEdicao(1, CO_INCLUSAO, 0);
end;

procedure TFmMoviC.QrMoviCAfterScroll(DataSet: TDataSet);
begin
  ReopenMovim(0);
  ReopenPagtos;
  ReopenCli;
end;

procedure TFmMoviC.SbQueryClick(Sender: TObject);
begin
  LocCod(QrMoviCCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'movic', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmMoviC.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmMoviC.FormShow(Sender: TObject);
begin
  CriaOForm;
end;

procedure TFmMoviC.QrMoviCBeforeOpen(DataSet: TDataSet);
begin
  QrMoviCCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmMoviC.EdDataPediExit(Sender: TObject);
var
  Data: TDateTime;
begin
  Data := Geral.ValidaDataSimples(EdDataPedi.Text, False);
  if Data > 2 then
  begin
    EdDataPedi.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdDataPedi.SetFocus;
end;

procedure TFmMoviC.EdDataRealExit(Sender: TObject);
var
  Data, Emis: TDateTime;
  Casas: Integer;
begin
  Casas := Length(EdDataReal.Text);
  Data := Geral.ValidaDataSimples(EdDataReal.Text, False);
  Emis := Geral.ValidaDataSimples(EdDataPedi.Text, False);
  if Data > 2 then
  begin
    if Data <= Emis then
      if Casas < 6 then Data := IncMonth(Data, 12);
    if Data < Emis then
    begin
      if Application.MessageBox('Data j� passada! Deseja incrementar um ano?',
      'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
      begin
        Data := IncMonth(Data, 12);
        EdDataReal.SetFocus;
      end else begin
        EdDataReal.SetFocus;
        Exit;
      end;
    end;
    EdDataReal.Text := FormatDateTime('dd/mm/yyyy', Data);
  end else EdDataReal.Text := '000000';// else EdDataReal.SetFocus;
end;

procedure TFmMoviC.QrMoviCCalcFields(DataSet: TDataSet);
begin
  QrMoviCDATAREAL_TXT.Value := Geral.FDT(QrMoviCDataReal.Value, 2);
  QrMoviCSALDO.Value := QrMoviCPago.Value - QrMoviCTotal.Value;
end;

procedure TFmMoviC.ReopenMovim(Conta: Integer);
begin
  QrMovim.Close;
  QrMovim.Params[0].AsInteger := QrMoviCControle.Value;
  QrMovim.Open;
  //
  if Conta > 0 then QrMovim.Locate('Conta', Conta, []);
end;

procedure TFmMoviC.BtCompraClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMCompra, BtCompra);
end;

procedure TFmMoviC.Incluinovacompra1Click(Sender: TObject);
begin
  MostraEdicao(1, CO_INCLUSAO, 0);
end;

procedure TFmMoviC.Alteracompraatual1Click(Sender: TObject);
var
  MoviC : Integer;
begin
  if QrMoviCDataReal.Value > 1 then
    if DmProd.ImpedePeloBalanco(QrMoviCDataReal.Value) then Exit;
  MoviC := QrMoviCCodigo.Value;
  if not UMyMod.SelLockY(MoviC, Dmod.MyDB, 'movic', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(MoviC, Dmod.MyDB, 'movic', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmMoviC.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmMoviC.Incluiitensnacompraatual1Click(Sender: TObject);
begin
  MostraEdicao(2, CO_INCLUSAO, 0);
end;

procedure TFmMoviC.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, CO_TRAVADO, 0);
end;

procedure TFmMoviC.EdGradeChange(Sender: TObject);
begin
  ConfigGrades;
  LimpaEdits;
end;

procedure TFmMoviC.ReopenCli;
begin
  QrCli.Close;
  QrCli.Params[0].AsInteger := QrMoviCFornece.Value;
  QrCli.Open;
end;

procedure TFmMoviC.ReopenGradesCors(Grade: Integer);
begin
  QrGradesCors.Close;
  QrGradesCors.Params[0].AsInteger := Grade;
  QrGradesCors.Open;
end;

procedure TFmMoviC.ReopenGradesTams(Grade: Integer);
begin
  QrGradesTams.Close;
  QrGradesTams.Params[0].AsInteger := Grade;
  QrGradesTams.Open;
end;

procedure TFmMoviC.ReopenProdutos(Grade: Integer);
begin
  QrProdutos.Close;
  QrProdutos.Params[00].AsInteger := QrMoviCControle.Value;
  QrProdutos.Params[01].AsInteger := Grade;
  QrProdutos.Open;
end;

procedure TFmMoviC.ConfigGrades;
var
  c, l{, ci, li}, g: Integer;
begin
  for c := 0 to GradeA.ColCount - 1 do
    for l := 0 to GradeA.ColCount - 1 do
      GradeA.Cells[c, l] := '';
  g := Geral.IMV(EdGrade.Text);
  if g > 0 then
  begin
    g := QrGradesCodigo.Value;
    ReopenGradesCors(g);
    ReopenGradesTams(g);
    //
    c := QrGradesTams.RecordCount + 1;
    if c < 2 then c := 2;
    l := QrGradesCors.RecordCount + 1;
    if l < 2 then l := 2;
    //
    GradeA.ColCount  := c;
    GradeA.RowCount  := l;
    GradeA.FixedCols := 1;
    GradeA.FixedRows := 1;
    //
    GradeC.ColCount  := c;
    GradeC.RowCount  := l;
    //
    QrGradesCors.First;
    while not QrGradesCors.Eof do
    begin
      GradeA.Cells[0, QrGradesCors.RecNo] := QrGradesCorsNOMECOR.Value;
      GradeC.Cells[0, QrGradesCors.RecNo] := IntToStr(QrGradesCorsCor.Value);
      QrGradesCors.Next;
    end;
    //
    QrGradesTams.First;
    while not QrGradesTams.Eof do
    begin
      GradeA.Cells[QrGradesTams.RecNo, 0] := QrGradesTamsNOMETAM.Value;
      GradeC.Cells[QrGradesTams.RecNo, 0] := IntToStr(QrGradesTamsTam.Value);
      QrGradesTams.Next;
    end;
    AtualizaGradeA(g);
  end else begin
    GradeA.ColCount := 1;
    GradeA.RowCount := 1;
  end;
end;

procedure TFmMoviC.AtualizaGradeA(Grade: Integer);
var
  c, l: Integer;
begin
  ReopenProdutos(Grade);
  QrProdutos.First;
  while not QrProdutos.Eof do
  begin
    if QrGradesTams.Locate('Tam', QrProdutosTam.Value, []) then
    begin
      if QrGradesCors.Locate('Cor', QrProdutosCor.Value, []) then
      begin
        c := QrGradesTams.RecNo;
        l := QrGradesCors.RecNo;
        if QrProdutosConta.Value > 0 then
          GradeA.Cells[c, l] := '2'
        else
          GradeA.Cells[c, l] := '1';
      end;
    end;
    QrProdutos.Next;
  end;
end;

procedure TFmMoviC.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  if (ACol <> 0) and (ARow <> 0) then
    MeuVCLSkin.DrawGrid3(GradeA, Rect, 1, Geral.IMV(GradeA.Cells[ACol, ARow]));
end;

procedure TFmMoviC.GradeAClick(Sender: TObject);
var
  Status, Grade, Cor, Tam: Integer;
begin
  if GradeA.Cells[GradeA.Col, GradeA.Row] <> '' then
  begin
    EdTamCod.Text := GradeC.Cells[GradeA.Col, 0];
    EdCorCod.Text := GradeC.Cells[0, GradeA.Row];
    //
    EdTamNom.Text := GradeA.Cells[GradeA.Col, 0];
    EdCorNom.Text := GradeA.Cells[0, GradeA.Row];
    //
  end else LimpaEdits;
  Status := Geral.IMV(GradeA.Cells[GradeA.Col, GradeA.Row]);
  case Status of
    0: LaStatus.Caption := CO_TRAVADO;
    1: LaStatus.Caption := CO_INCLUSAO;
    else begin
      LaStatus.Caption := CO_ALTERACAO;
      Grade := QrGradesCodigo.Value;
      Cor := Geral.IMV(EdCorCod.Text);
      Tam := Geral.IMV(EdTamCod.Text);
      if QrMovim.Locate('Grade;Cor;Tam', VarArrayOf([Grade, Cor, Tam]), []) then
      begin
        EdQtd.Text := Geral.FFT(QrMovimQtd.Value, 3, siPositivo);
        EdVal.Text := Geral.FFT(QrMovimVal.Value, 2, siPositivo);
        CalculaPreco(tcvVal);
      end else LimpaEdits;
    end;
  end;
  BtConfItem.Enabled := Geral.IntToBool_0(Status);
  EdQtd.SetFocus;
end;

procedure TFmMoviC.EdQtdExit(Sender: TObject);
begin
  EdQtd.Text := Geral.TFT(EdQtd.Text, 3, siPositivo);
  if Geral.DMV(EdQtd.Text) <> FQtd then CalculaPreco(tcvQtd);
end;

procedure TFmMoviC.EdPrcExit(Sender: TObject);
begin
  EdPrc.Text := Geral.TFT(EdPrc.Text, 2, siPositivo);
  if Geral.DMV(EdPrc.Text) <> FPrc then CalculaPreco(tcvPrc);
end;

procedure TFmMoviC.EdValExit(Sender: TObject);
begin
  EdVal.Text := Geral.TFT(EdVal.Text, 2, siPositivo);
  if Geral.DMV(EdVal.Text) <> FVal then CalculaPreco(tcvVal);
end;

procedure TFmMoviC.LimpaEdits;
begin
  EdTamCod.Text := '';
  EdCorCod.Text := '';
  //
  EdTamNom.Text := '';
  EdCorNom.Text := '';
  //
end;

procedure TFmMoviC.EdQtdEnter(Sender: TObject);
begin
  FQtd := Geral.DMV(EdQtd.Text);
end;

procedure TFmMoviC.EdPrcEnter(Sender: TObject);
begin
  FPrc := Geral.DMV(EdPrc.Text);
end;

procedure TFmMoviC.EdValEnter(Sender: TObject);
begin
  FVal := Geral.DMV(EdVal.Text);
end;

procedure TFmMoviC.CalculaPreco(Como: TCalcVal);
var
  Qtd, Prc, Val: Double;
begin
  Qtd := Geral.DMV(EdQtd.Text);
  Prc := Geral.DMV(EdPrc.Text);
  Val := Geral.DMV(EdVal.Text);
  case Como of
    tcvQtd: Val := Qtd * Prc;
    tcvPrc: Val := Qtd * Prc;
    tcvVal: if Qtd = 0 then Prc := 0 else Prc := Val / Qtd;
  end;
  EdQtd.Text := Geral.FFT(Qtd, 2, siPositivo);
  EdPrc.Text := Geral.FFT(Prc, 2, siPositivo);
  EdVal.Text := Geral.FFT(Val, 2, siPositivo);
end;

procedure TFmMoviC.BtConfItemClick(Sender: TObject);
var
  Qtd, Val, QtF, VaF, Qtdx, Valx, Cusx: Double;
  Controle, Conta, Grade, Cor, Tam: Integer;
  DataReal: TDate;
begin
  if LaStatus.Caption = CO_TRAVADO then Exit;
  Qtd := Geral.DMV(EdQtd.Text);
  Val := Geral.DMV(EdVal.Text);
  //
  Controle := QrMoviCControle.Value;
  Grade := QrGradesCodigo.Value;
  Cor := Geral.IMV(EdCorCod.Text);
  Tam := Geral.IMV(EdTamCod.Text);
  Dmod.QrUpd.SQL.Clear;
  if LaStatus.Caption = CO_INCLUSAO then
  begin
    Conta := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle',
    'movim', 'movim', 'conta');
    Dmod.QrUpd.SQL.Add('INSERT INTO movim SET Motivo=11, '); // Entrada por compra
    Dmod.QrUpd.SQL.Add('Qtd=:P0, Val=:P1, DataCad=:Pa, UserCad=:Pb, ');
    Dmod.QrUpd.SQL.Add('Grade=:Pc, Cor=:Pd, Tam=:Pe, Controle=:Pf, Conta=:Pg, ');
    Dmod.QrUpd.SQL.Add('DataPedi=:Ph, DataReal=:Pi ');
  end else begin
    DmProd.AtualizaEstoqueMercadoria(Grade, Cor, Tam, True, True, Qtdx, Valx, Cusx);
    QrEstq.Close;
    QrEstq.Params[00].AsInteger := Grade;
    QrEstq.Params[01].AsInteger := Cor;
    QrEstq.Params[02].AsInteger := Tam;
    QrEstq.Open;
    QtF := QrEstqEstqQ.Value - QrMovimQtd.Value + Qtd;
    VaF := QrEstqEstqV.Value - QrMovimVal.Value + Val;
    Conta := DmProd.LocalizaConta(QrMoviCControle.Value, 0, FormatFloat('0',
      QrGradesCodigo.Value), EdCorCod.Text, EdTamCod.Text);
    if Conta > 0 then
    begin
      if QtF < 0 then
      begin
        Conta := -1;
        Application.MessageBox('Altera��o cancelada. Estoque (Qtd) ficaria negativo.',
        'Aviso', MB_OK+MB_ICONWARNING);
      end;
      if VaF < 0 then
      begin
        Conta := -1;
        Application.MessageBox('Altera��o cancelada. Estoque (Valor) ficaria negativo.',
        'Aviso', MB_OK+MB_ICONWARNING);
      end;
    end;
    if Conta > 0 then
    begin
      Dmod.QrUpd.SQL.Add('UPDATE movim SET ');
      Dmod.QrUpd.SQL.Add('Qtd=:P0, Val=:P1, ');
      Dmod.QrUpd.SQL.Add('DataAlt=:Pa, UserAlt=:Pb ');
      Dmod.QrUpd.SQL.Add('WHERE Conta=:Pg');
    end;
  end;
  if Conta > 0 then
  begin
    Dmod.QrUpd.Params[00].AsFloat   := Qtd;
    Dmod.QrUpd.Params[01].AsFloat   := Val;
    Dmod.QrUpd.Params[02].AsString  := Geral.FDT(Date, 1);
    Dmod.QrUpd.Params[03].AsInteger := VAR_USUARIO;
    if LaStatus.Caption = CO_INCLUSAO then
    begin
      Dmod.QrUpd.Params[04].AsInteger := Grade;
      Dmod.QrUpd.Params[05].AsInteger := Cor;
      Dmod.QrUpd.Params[06].AsInteger := Tam;
      Dmod.QrUpd.Params[07].AsInteger := Controle;
      Dmod.QrUpd.Params[08].AsInteger := Conta;
      Dmod.QrUpd.Params[09].AsString  := Geral.FDT(QrMoviCDataPedi.Value, 1);
      Dmod.QrUpd.Params[10].AsString  := Geral.FDT(QrMoviCDataReal.Value, 1);
    end else begin
      Dmod.QrUpd.Params[04].AsInteger := Conta;
    end;
    Dmod.QrUpd.ExecSQL;
    ReopenMovim(Conta);
    AtualizaGradeA(Grade);
    EdQtd.Text := '';
    EdPrc.Text := '';
    EdVal.Text := '';
    DmProd.AtualizaEstoqueMercadoria(Grade, Cor, Tam, 
      QrMoviCDataReal.Value > 1, True, Qtdx, Valx, CusX);
  end;
  //
  DataReal := QrMoviCDataReal.Value;
  if (DataReal > 0) then if DmProd.ImpedePeloBalanco(DataReal) then Exit;
  if DataReal > 0 then
    if DmProd.ImpedePeloBalanco(DataReal) then Exit;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('UPDATE movim SET DataPedi=:P0, DataReal=:P1');
  Dmod.QrUpdU.SQL.Add('WHERE Controle=:P2');
  Dmod.QrUpdU.Params[00].AsString  := Geral.FDT(QrMoviCDataPedi.Value, 1);
  Dmod.QrUpdU.Params[01].AsString  := Geral.FDT(DataReal, 1);
  Dmod.QrUpdU.Params[02].AsInteger := Controle;
  Dmod.QrUpdU.ExecSQL;
  //
  QrMovim2.Close;
  QrMovim2.Params[0].AsInteger := QrMoviCControle.Value;
  QrMovim2.Open;
  QrMovim2.First;
  while not QrMovim2.Eof do
  begin
    DmProd.AtualizaEstoqueMercadoria(QrMovim2Grade.Value,
      QrMovim2Cor.Value, QrMovim2Tam.Value, True, True, QtdX, ValX, CusX);
    QrMovim2.Next;
  end;
  //
  AtualizaValores(QrMoviCCodigo.Value, Conta, QrPagtosControle.Value);
  EdQtd.SetFocus;
end;

procedure TFmMoviC.QrMovimCalcFields(DataSet: TDataSet);
begin
  if QrMovimQtd.Value = 0 then QrMovimPRECO.Value := 0 else
  QrMovimPRECO.Value := QrMovimVal.Value / QrMovimQtd.Value;
end;

procedure TFmMoviC.IncluiPagtos1Click(Sender: TObject);
var
  Terceiro, Cod, Genero, Carteira: Integer;
  Valor: Double;
begin
  if (QrMoviC.State <> dsBrowse) or (QrMoviC.RecordCount = 0) then Exit;
  IC3_ED_FatNum := QrMoviCControle.Value;
  IC3_ED_NF     := 0;
  IC3_ED_Data   := Date;
  IC3_ED_Vencto := Date;
  Cod           := QrMoviCControle.Value;
  Terceiro      := QrMoviCFornece.Value;
  Valor         := QrMoviCSALDO.Value;
  Genero        := Dmod.QrControleContCom.Value;
  Carteira      := Dmod.QrControleCartCom.Value;
  //
  UPagtos.Pagto(QrPagtos, tpDeb, Cod, Terceiro, VAR_FATID_0500, Genero, stIns,
    'Pagamento de Mercadoria', Valor, VAR_USUARIO, 0, Dmod.QrDonoCodigo.Value,
    mmNenhum, 0, 0, True, False, Carteira, 0, 0, 0, 0, FTabLctALS);
  //
  AtualizaValores(QrMoviCCodigo.Value, QrMovimConta.Value, QrPagtosControle.Value);
end;

procedure TFmMoviC.AtualizaValores(MoviC, Movim, Pagto: Integer);
(*var
  Status: Integer;
  Saldo: Double;*)
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSumP, Dmod.MyDB, [
    'SELECT SUM(Debito) Debito ',
    'FROM ' + FTabLctALS,
    'WHERE FatID=' + Geral.FF0(VAR_FATID_0500),
    'AND FatNum=' + Geral.FF0(QrMoviCControle.Value),
    '']);
  //
  QrSumM.Close;
  QrSumM.Params[0].AsInteger := QrMoviCControle.Value;
  QrSumM.Open;
  (*Saldo := QrAlugueisItsValAluguel.Value - QrAlugueisItsValBonus.Value +
  QrAlugueisItsValJuros.Value + QrAlugueisItsValMulta.Value - QrSumPCredito.Value;
  //
  if Saldo <= 0 then Status := 2
  else if QrSumPCredito.Value > 0 then Status := 1
  else Status := 0;*)
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE movic SET Pago=:P0, Total=:P1 ');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:Pa');
  Dmod.QrUpd.Params[00].AsFloat   := QrSumPDebito.Value;
  Dmod.QrUpd.Params[01].AsFloat   := QrSumMVal.Value;
  //Dmod.QrUpd.Params[01].AsInteger := Status;
  //
  Dmod.QrUpd.Params[02].AsInteger := QrMoviCControle.Value;
  Dmod.QrUpd.ExecSQL;
  //
  LocCod(QrMoviCCodigo.Value, QrMoviCCodigo.Value);
  if QrMovim.State = dsBrowse then QrMovim.Locate('Conta', Movim, []);
  if QrPagtos.State = dsBrowse then QrPagtos.Locate('Controle', Pagto, []);
end;

procedure TFmMoviC.ReopenPagtos;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPagtos, Dmod.MyDB, [
    'SELECT la.Data, la.Vencimento, la.Debito, la.Banco, la.Agencia, la.FatID, ',
    'la.ContaCorrente, la.Documento, la.Descricao, la.FatParcela, la.FatNum, ',
    'ca.Nome NOMECARTEIRA, ca.Nome2 NOMECARTEIRA2, ca.Banco1, ',
    'ca.Agencia1, ca.Conta1, ca.TipoDoc, la.Controle, ',
    'CASE WHEN fo.Tipo=0 THEN fo.RazaoSocial ',
    'ELSE fo.Nome END NOMEFORNECEI, ',
    'ca.Tipo CARTEIRATIPO ',
    'FROM ' + FTabLctALS + ' la ',
    'LEFT JOIN carteiras ca ON ca.Codigo=la.Carteira ',
    'LEFT JOIN entidades fo ON fo.Codigo=ca.ForneceI ',
    'WHERE FatID=' + Geral.FF0(VAR_FATID_0500),
    'AND FatNum=' + Geral.FF0(QrMoviCControle.Value),
    'ORDER BY la.FatParcela, la.Vencimento ',
    '']);
end;

procedure TFmMoviC.ExcluiPagtos1Click(Sender: TObject);
var
  FatID, FatParcela : Integer;
  FatNum : Double;
begin
  FatParcela := QrPagtosFatParcela.Value;
  FatNum := QrPagtosFatNum.Value;
  FatID := QrPagtosFatID.Value;
  if MLAGeral.NaoPermiteExclusaoDeLancto(FatParcela, FatNum, FatID) then Exit;
  if Application.MessageBox('Confirma a exclus�o deste item de pagamento?',
  'Exclus�o de Pagamento', MB_YESNOCANCEL+MB_DEFBUTTON2+MB_ICONQUESTION) =
  ID_YES then
  begin
    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM ' + FTabLctALS + ' WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrPagtosControle.Value;
    Dmod.QrUpd.ExecSQL;

    Dmod.QrUpd.Close;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ' + FTabLctALS + ' SET FatParcela=FatParcela-1');
    Dmod.QrUpd.SQL.Add('WHERE FatParcela>:P0 AND FatID=:P1 AND FatNum=:P2');
    Dmod.QrUpd.Params[0].AsInteger := FatParcela;
    Dmod.QrUpd.Params[1].AsInteger := FatID;
    Dmod.QrUpd.Params[2].AsFloat   := FatNum;
    Dmod.QrUpd.ExecSQL;

    //Dmod.RecalcSaldoCarteira(QrPagtosTipo.Value, QrPagtosCarteira.Value, 0);

    QrPagtos.Close;
    QrPagtos.Params[0].AsFloat := FatNum;
    QrPagtos.Open;
    //QrPagtos.Locate('Controle', Controle, []);
    AtualizaValores(QrMoviCCodigo.Value, QrMovimConta.Value, QrPagtosControle.Value);
  end;
end;

procedure TFmMoviC.BtPagtosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMPagtos, BtPagtos);
end;

procedure TFmMoviC.Excluiitematual1Click(Sender: TObject);
var
  Grade, Cor, Tam: Integer;
  QtF, VaF, QtdX, ValX, CusX: Double;
  Exclui: Boolean;
begin
  if (QrMoviC.State <> dsBrowse) or (QrMoviC.RecordCount = 0) then Exit;
  if QrMovim.RecordCount = 0 then Exit;
  if QrMoviCDataReal.Value > 1 then
    if DmProd.ImpedePeloBalanco(QrMoviCDataReal.Value) then Exit;
  //
  Exclui := True;
  Grade := QrMovimGrade.Value;
  Cor   := QrMovimCor.Value;
  Tam   := QrMovimTam.Value;
  DmProd.AtualizaEstoqueMercadoria(Grade, Cor, Tam, True, True, QtdX, ValX, CusX);
  QrEstq.Close;
  QrEstq.Params[00].AsInteger := Grade;
  QrEstq.Params[01].AsInteger := Cor;
  QrEstq.Params[02].AsInteger := Tam;
  QrEstq.Open;
  QtF := QrEstqEstqQ.Value - QrMovimQtd.Value;
  VaF := QrEstqEstqV.Value - QrMovimVal.Value;
  if QtF < 0 then
  begin
    Exclui := False;
    Application.MessageBox('Exclus�o cancelada. Estoque (Qtd) ficaria negativo.',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  if VaF < 0 then
  begin
    Exclui := False;
    Application.MessageBox('Exclus�o cancelada. Estoque (Valor) ficaria negativo.',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  if Exclui then
  begin
    if Application.MessageBox('Confirma a exclus�o do item selecionado?',
    'Confirma��o de exclus�o', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
    begin
      Screen.Cursor := crHourGlass;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM movim ');
      Dmod.QrUpd.SQL.Add('WHERE Conta=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrMovimConta.Value;
      Dmod.QrUpd.ExecSQL;
      //
      DmProd.AtualizaEstoqueMercadoria(Grade, Cor, Tam, True, True, QtdX, ValX, CusX);
      AtualizaValores(QrMoviCCodigo.Value, 0, QrPagtosControle.Value);
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmMoviC.Excluicompraatual1Click(Sender: TObject);
begin
  if QrMovim.RecordCount > 0 then
  begin
    Application.MessageBox('Exclus�o cancelada. Compra possui itens.',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if QrPagtos.RecordCount > 0 then
  begin
    Application.MessageBox('Exclus�o cancelada. Compra possui pagamentos.',
    'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  if Application.MessageBox('Confirma a exclus�o da compra?',
  'Confirma��o de exclus�o', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('DELETE FROM movic ');
    Dmod.QrUpd.SQL.Add('WHERE Controle=:P0');
    Dmod.QrUpd.Params[0].AsInteger := QrMoviCControle.Value;
    Dmod.QrUpd.ExecSQL;
    //
    QrMoviC.Close;
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmMoviC.QrMoviCBeforeClose(DataSet: TDataSet);
begin
  QrMovim.Close;
  QrPagtos.Close;
end;

procedure TFmMoviC.PMCompraPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := not ((QrMoviC.State <> dsBrowse) or (QrMoviC.RecordCount = 0));
  Alteracompraatual1.Enabled := Enab;
  Excluicompraatual1.Enabled := Enab;
end;

procedure TFmMoviC.PMItensPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := not ((QrMoviC.State <> dsBrowse) or (QrMoviC.RecordCount = 0));
  Incluiitensnacompraatual1.Enabled := Enab;
  if Enab then Excluiitematual1.Enabled := QrMovim.RecordCount > 0
  else Excluiitematual1.Enabled := False;
end;

procedure TFmMoviC.PMPagtosPopup(Sender: TObject);
var
  Enab: Boolean;
begin
  Enab := not ((QrMoviC.State <> dsBrowse) or (QrMoviC.RecordCount = 0));
  IncluiPagtos1.Enabled := Enab;
  if Enab then ExcluiPagtos1.Enabled := QrPagtos.RecordCount > 0
  else ExcluiPagtos1.Enabled := False;
end;

procedure TFmMoviC.EdDataRealKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_F4 then EdDataReal.Text := EdDataPedi.Text;
end;

procedure TFmMoviC.SpeedButton5Click(Sender: TObject);
begin
  VAR_ENTIDADE := 0;
  //
  FmPrincipal.CadastroDeEntidades(EdFornece.ValueVariant);
  //
  if VAR_ENTIDADE <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdFornece, CBFornece, QrFornece, VAR_ENTIDADE);
    //
    EdFornece.SetFocus;
  end;
end;

procedure TFmMoviC.SpeedButton6Click(Sender: TObject);
begin
  VAR_COD := 0;
  //
  FmPrincipal.CadastroDeGrades();
  //
  if VAR_COD <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdGrade, CBGrade, QrGrades, VAR_COD);
    //
    EdGrade.SetFocus;
  end;
end;

end.





