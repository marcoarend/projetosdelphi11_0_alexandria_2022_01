unit MD5Cab;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, dmkGeral,
  dmkPermissoes, dmkEdit, dmkLabel, dmkRadioGroup, Grids, DBGrids, dmkDBGrid,
  Menus, UnDmkProcFunc, UnDmkEnums;

type
  TFmMD5Cab = class(TForm)
    PainelDados: TPanel;
    DsMD5Cab: TDataSource;
    QrMD5Cab: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtItens: TBitBtn;
    BtGrupo: TBitBtn;
    PainelEdit: TPanel;
    EdCodigo: TdmkEdit;
    PnCabeca: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    Label7: TLabel;
    Label9: TLabel;
    QrMD5Fxa: TmySQLQuery;
    DsMD5Fxa: TDataSource;
    QrMD5CabCodigo: TIntegerField;
    QrMD5CabNome: TWideStringField;
    QrMD5CabSenha: TWideStringField;
    QrMD5FxaControle: TIntegerField;
    QrMD5FxaNumIni: TIntegerField;
    QrMD5FxaNumFim: TIntegerField;
    EdSenha: TdmkEdit;
    Label3: TLabel;
    PMGrupo: TPopupMenu;
    Incluinovogrupodesenhas1: TMenuItem;
    PMItens: TPopupMenu;
    Incluinovasequnciadesenhas1: TMenuItem;
    Imprimeafaixaatual1: TMenuItem;
    RGForca: TdmkRadioGroup;
    QrMD5CabForca: TSmallintField;
    RGAtivo: TdmkRadioGroup;
    Alteragrupodesenhas1: TMenuItem;
    Label4: TLabel;
    RadioGroup1: TDBRadioGroup;
    RadioGroup2: TDBRadioGroup;
    QrMD5CabAtivo: TSmallintField;
    DBEdit1: TDBEdit;
    BtAlunos: TBitBtn;
    PanelGrids: TPanel;
    DBGImp: TDBGrid;
    DBGAlu: TDBGrid;
    QrMD5Alu: TmySQLQuery;
    DsMD5Alu: TDataSource;
    QrMD5AluEntidade: TIntegerField;
    QrMD5AluMD5Num: TIntegerField;
    QrMD5AluNOMEALU: TWideStringField;
    PMAlunos: TPopupMenu;
    Adicionanovoaluno1: TMenuItem;
    Retiraalunoatual1: TMenuItem;
    QrMD5AluMD5Cab: TIntegerField;
    RGQuartos: TdmkRadioGroup;
    QrMD5CabQuartos: TSmallintField;
    DBRadioGroup1: TDBRadioGroup;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrMD5CabAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrMD5CabBeforeOpen(DataSet: TDataSet);
    procedure SbNovoClick(Sender: TObject);
    procedure BtGrupoClick(Sender: TObject);
    procedure PMItensPopup(Sender: TObject);
    procedure QrMD5CabBeforeClose(DataSet: TDataSet);
    procedure QrMD5CabAfterScroll(DataSet: TDataSet);
    procedure BtItensClick(Sender: TObject);
    procedure Incluinovogrupodesenhas1Click(Sender: TObject);
    procedure Incluinovasequnciadesenhas1Click(Sender: TObject);
    procedure Imprimeafaixaatual1Click(Sender: TObject);
    procedure Alteragrupodesenhas1Click(Sender: TObject);
    procedure Adicionanovoaluno1Click(Sender: TObject);
    procedure Retiraalunoatual1Click(Sender: TObject);
    procedure BtAlunosClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
  public
    { Public declarations }
    procedure ReopenMD5Fxa(Controle: Integer);
    procedure ReopenMD5Alu(Entidade: Integer);
  end;

var
  FmMD5Cab: TFmMD5Cab;
const
  FFormatFloat = '00000';

implementation

uses Module, MyDBCheck, MD5Fxa, MD5Imp, MD5Alu, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMD5Cab.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMD5Cab.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMD5CabCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMD5Cab.DefParams;
begin
  VAR_GOTOTABELA := 'md5cab';
  VAR_GOTOMYSQLTABLE := QrMD5Cab;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;

  VAR_SQLx.Add('SELECT Codigo, Nome, Senha, Forca, Ativo, Quartos');
  VAR_SQLx.Add('FROM md5cab');
  VAR_SQLx.Add('WHERE Codigo>-1000');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQL2.Add('');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmMD5Cab.MostraEdicao(Mostra: Integer; Status: String; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if Status = CO_INCLUSAO then
      begin
        EdCodigo.Text := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text := '';
        //...
      end else begin
        EdCodigo.Text := DBEdCodigo.Text;
        EdNome.Text := DBEdNome.Text;
        //...
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmMD5Cab.PMItensPopup(Sender: TObject);
begin
{
  Incluinovoitemdeatributo1.Enabled := (QrMD5Cab.State <> dsInactive) and
    (QrMD5Cab.RecordCount >0);
  Alteraitemdeatributoatual1.Enabled := (QrMD5Fxa.State <> dsInactive) and
    (QrMD5Fxa.RecordCount >0);
}    
end;

procedure TFmMD5Cab.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

{
procedure TFmMD5Cab.AlteraRegistro;
var
  MD5Cab : Integer;
begin
  MD5Cab := QrMD5CabCodigo.Value;
  if QrMD5CabCodigo.Value = 0 then
  begin
    BtAltera.Enabled := False;
    Exit;
  end;
  if not UMyMod.SelLockY(MD5Cab, Dmod.MyDB, 'MD5Cab', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(MD5Cab, Dmod.MyDB, 'MD5Cab', 'Codigo');
      MostraEdicao(1, CO_ALTERACAO, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmMD5Cab.IncluiRegistro;
var
  Cursor : TCursor;
  MD5Cab : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    MD5Cab := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
    'MD5Cab', 'MD5Cab', 'Codigo');
    if Length(FormatFloat(FFormatFloat, MD5Cab))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, CO_INCLUSAO, MD5Cab);
  finally
    Screen.Cursor := Cursor;
  end;
end;
}

procedure TFmMD5Cab.QueryPrincipalAfterOpen;
begin
end;

procedure TFmMD5Cab.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMD5Cab.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMD5Cab.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMD5Cab.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMD5Cab.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMD5Cab.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrMD5CabCodigo.Value;
  Close;
end;

procedure TFmMD5Cab.BtGrupoClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMGrupo, BtGrupo);
end;

procedure TFmMD5Cab.Adicionanovoaluno1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMD5Alu, FmMD5Alu, afmoNegarComAviso) then
  begin
    FmMD5Alu.ShowModal;
    FmMD5Alu.Destroy;
  end;
end;

procedure TFmMD5Cab.Alteragrupodesenhas1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stUpd, Self, PainelEdita, QrMD5Cab, [PainelDados],
  [PainelEdita], EdNome, LaTipo, 'md5cab');
end;

procedure TFmMD5Cab.BtAlunosClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMAlunos, BtAlunos);
end;

procedure TFmMD5Cab.BtConfirmaClick(Sender: TObject);
var
  Codigo: Integer;
  Nome, Senha: String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    EdNome.SetFocus;
    Exit;
  end;
  Senha := EdSenha.Text;
  if (Length(Senha) < 5) or (Length(Senha) > 8) then
  begin
    Application.MessageBox('Defina uma senha de 5 a 8 caracteres.', 'Erro', MB_OK+MB_ICONERROR);
    EdSenha.SetFocus;
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def('md5cab', 'Codigo', LaTipo.SQLType,
    QrMD5CabCodigo.Value);
  if UMyMod.ExecSQLInsUpdPanel(LaTipo.SQLType, FmMD5Cab, PainelEdit,
    'md5cab', Codigo, Dmod.QrUpd, [PainelEdita], [PainelDados], LaTipo, True) then
  begin
    LocCod(Codigo, Codigo);
  end;
end;

procedure TFmMD5Cab.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'Livres', 'md5cab', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'md5cab', 'Codigo');
  MostraEdicao(0, CO_TRAVADO, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'md5cab', 'Codigo');
end;

procedure TFmMD5Cab.BtItensClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMItens, BtItens);
end;

procedure TFmMD5Cab.FormCreate(Sender: TObject);
begin
  PainelEdit.Align  := alClient;
  PanelGrids.Align  := alClient;
  CriaOForm;
end;

procedure TFmMD5Cab.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMD5CabCodigo.Value, LaRegistro.Caption);
end;

procedure TFmMD5Cab.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMD5Cab.SbNovoClick(Sender: TObject);
begin
  //LaRegistro.Caption := GOTOy.CodUsu(QrMD5CabCodUsu.Value, LaRegistro.Caption);
end;

procedure TFmMD5Cab.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmMD5Cab.QrMD5CabAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmMD5Cab.QrMD5CabAfterScroll(DataSet: TDataSet);
begin
  ReopenMD5Fxa(0);
  ReopenMD5Alu(0);
end;

procedure TFmMD5Cab.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if LaTipo.SQLType = stIns then
    RGAtivo.ItemIndex := 1;
end;

procedure TFmMD5Cab.SbQueryClick(Sender: TObject);
begin
  LocCod(QrMD5CabCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'md5cab', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmMD5Cab.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmMD5Cab.Imprimeafaixaatual1Click(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMD5Imp, FmMD5Imp, afmoNegarComAviso) then
  begin
    FmMD5Imp.ShowModal;
    FmMD5Imp.Destroy;
  end;
end;

procedure TFmMD5Cab.Incluinovasequnciadesenhas1Click(Sender: TObject);
begin
  UmyMod.FormInsUpd_Show(TFmMD5Fxa, FmMD5Fxa, afmoNegarComAviso,
    QrMD5Fxa, stIns);
end;

procedure TFmMD5Cab.Incluinovogrupodesenhas1Click(Sender: TObject);
begin
  UMyMod.ConfigPanelInsUpd(stIns, Self, PainelEdit, QrMD5Cab, [PainelDados],
  [PainelEdita], EdNome, LaTipo, 'md5cab');
end;

procedure TFmMD5Cab.QrMD5CabBeforeClose(DataSet: TDataSet);
begin
  QrMD5Fxa.Close;
end;

procedure TFmMD5Cab.QrMD5CabBeforeOpen(DataSet: TDataSet);
begin
  QrMD5CabCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmMD5Cab.ReopenMD5Fxa(Controle: Integer);
begin
  QrMD5Fxa.Close;
  QrMD5Fxa.Params[0].AsInteger :=   QrMD5CabCodigo.Value;
  QrMD5Fxa.Open;
  //
  if Controle <> 0 then
    QrMD5Fxa.Locate('Controle', Controle, []);
end;

procedure TFmMD5Cab.Retiraalunoatual1Click(Sender: TObject);
begin
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrMD5Alu, DBGAlu, 'md5alu',
  ['Entidade', 'MD5Cab', 'MD5Num'], ['Entidade', 'MD5Cab', 'MD5Num'],
  istPergunta, '');
end;

procedure TFmMD5Cab.ReopenMD5Alu(Entidade: Integer);
begin
  QrMD5Alu.Close;
  QrMD5Alu.Params[0].AsInteger :=   QrMD5CabCodigo.Value;
  QrMD5Alu.Open;
  //
  if Entidade <> 0 then
    QrMD5Alu.Locate('Entidade', Entidade, []);
end;

end.


