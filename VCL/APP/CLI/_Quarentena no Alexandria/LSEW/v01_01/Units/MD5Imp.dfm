object FmMD5Imp: TFmMD5Imp
  Left = 339
  Top = 185
  Caption = 'CIC-CRYPT-003 :: Impress'#227'o de senhas para alunos'
  ClientHeight = 263
  ClientWidth = 476
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 215
    Width = 476
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      TabOrder = 0
      OnClick = BtOKClick
      NumGlyphs = 2
    end
    object Panel2: TPanel
      Left = 364
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
        NumGlyphs = 2
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 476
    Height = 48
    Align = alTop
    Caption = 'Impress'#227'o de senhas para alunos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 474
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 476
    Height = 167
    Align = alClient
    TabOrder = 0
    object Label4: TLabel
      Left = 12
      Top = 8
      Width = 131
      Height = 13
      Caption = 'Configura'#231#227'o de impress'#227'o:'
    end
    object SpeedButton1: TSpeedButton
      Left = 444
      Top = 24
      Width = 21
      Height = 21
      OnClick = SpeedButton1Click
    end
    object Label5: TLabel
      Left = 164
      Top = 52
      Width = 68
      Height = 13
      Caption = 'Espa'#231'amento:'
    end
    object Label6: TLabel
      Left = 240
      Top = 52
      Width = 68
      Height = 13
      Caption = 'Coluna inicial: '
    end
    object Label7: TLabel
      Left = 316
      Top = 52
      Width = 58
      Height = 13
      Caption = 'Linha inicial:'
    end
    object LaRepeticoes: TLabel
      Left = 392
      Top = 52
      Width = 57
      Height = 13
      Caption = 'Repeti'#231#245'es:'
      Enabled = False
    end
    object Label1: TLabel
      Left = 12
      Top = 52
      Width = 69
      Height = 13
      Caption = 'N'#250'mero inicial:'
    end
    object Label2: TLabel
      Left = 88
      Top = 52
      Width = 65
      Height = 13
      Caption = 'N'#250'mero final: '
    end
    object EdConfImp: TdmkEditCB
      Left = 12
      Top = 24
      Width = 58
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBConfImp
    end
    object CBConfImp: TdmkDBLookupComboBox
      Left = 72
      Top = 24
      Width = 372
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsPage
      TabOrder = 1
      dmkEditCB = EdConfImp
      UpdType = utYes
    end
    object EdEspacamento: TdmkEdit
      Left = 164
      Top = 68
      Width = 73
      Height = 21
      Alignment = taRightJustify
      TabOrder = 2
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '3'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 3
    end
    object EdColIni: TdmkEdit
      Left = 240
      Top = 68
      Width = 73
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
    end
    object EdLinIni: TdmkEdit
      Left = 316
      Top = 68
      Width = 73
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
    end
    object EdRepeticoes: TdmkEdit
      Left = 392
      Top = 68
      Width = 73
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 5
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 1
    end
    object Progress: TProgressBar
      Left = 8
      Top = 140
      Width = 457
      Height = 17
      TabOrder = 6
    end
    object CkInfoCod: TCheckBox
      Left = 12
      Top = 100
      Width = 153
      Height = 17
      Caption = 'Informa c'#243'digo de cadastro.'
      Checked = True
      State = cbChecked
      TabOrder = 7
    end
    object CkGrade: TCheckBox
      Left = 168
      Top = 101
      Width = 61
      Height = 17
      Caption = 'Grade.'
      TabOrder = 8
    end
    object CkDesign: TCheckBox
      Left = 232
      Top = 100
      Width = 97
      Height = 17
      Caption = 'Design mode.'
      TabOrder = 9
    end
    object EdNumIni: TdmkEdit
      Left = 12
      Top = 68
      Width = 73
      Height = 21
      Alignment = taRightJustify
      TabOrder = 10
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object EdNumFim: TdmkEdit
      Left = 88
      Top = 68
      Width = 73
      Height = 21
      Alignment = taRightJustify
      TabOrder = 11
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
  end
  object QrPage: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * FROM pages')
    Left = 324
    Top = 144
    object QrPageNome: TWideStringField
      FieldName = 'Nome'
      Size = 128
    end
    object QrPageObserva: TWideMemoField
      FieldName = 'Observa'
      BlobType = ftWideMemo
      Size = 1
    end
    object QrPageCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPagePagCol: TIntegerField
      FieldName = 'PagCol'
    end
    object QrPagePagLin: TIntegerField
      FieldName = 'PagLin'
    end
    object QrPagePagLef: TIntegerField
      FieldName = 'PagLef'
    end
    object QrPagePagTop: TIntegerField
      FieldName = 'PagTop'
    end
    object QrPagePagWid: TIntegerField
      FieldName = 'PagWid'
    end
    object QrPagePagHei: TIntegerField
      FieldName = 'PagHei'
    end
    object QrPagePagGap: TIntegerField
      FieldName = 'PagGap'
    end
    object QrPageBanCol: TIntegerField
      FieldName = 'BanCol'
    end
    object QrPageBanLin: TIntegerField
      FieldName = 'BanLin'
    end
    object QrPageBanWid: TIntegerField
      FieldName = 'BanWid'
    end
    object QrPageBanHei: TIntegerField
      FieldName = 'BanHei'
    end
    object QrPageBanLef: TIntegerField
      FieldName = 'BanLef'
    end
    object QrPageBanTop: TIntegerField
      FieldName = 'BanTop'
    end
    object QrPageBanGap: TIntegerField
      FieldName = 'BanGap'
    end
    object QrPageOrient: TIntegerField
      FieldName = 'Orient'
    end
    object QrPageDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPageDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPageUserCad: TSmallintField
      FieldName = 'UserCad'
    end
    object QrPageUserAlt: TSmallintField
      FieldName = 'UserAlt'
    end
    object QrPageLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPageColWid: TIntegerField
      FieldName = 'ColWid'
    end
    object QrPageBa2Hei: TIntegerField
      FieldName = 'Ba2Hei'
    end
    object QrPageFoSize: TSmallintField
      FieldName = 'FoSize'
    end
    object QrPageMemHei: TIntegerField
      FieldName = 'MemHei'
    end
    object QrPageEspaco: TSmallintField
      FieldName = 'Espaco'
    end
    object QrPageBarLef: TIntegerField
      FieldName = 'BarLef'
    end
    object QrPageBarTop: TIntegerField
      FieldName = 'BarTop'
    end
    object QrPageBarWid: TIntegerField
      FieldName = 'BarWid'
    end
    object QrPageBarHei: TIntegerField
      FieldName = 'BarHei'
    end
  end
  object DsPage: TDataSource
    DataSet = QrPage
    Left = 352
    Top = 144
  end
  object QrMinhaEtiq: TmySQLQuery
    Database = Dmod.MyLocDatabase
    SQL.Strings = (
      'SELECT * FROM minhaetiq'
      'ORDER BY Conta')
    Left = 380
    Top = 144
    object QrMinhaEtiqNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'minhaetiq.Nome'
      Size = 100
    end
    object QrMinhaEtiqConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'minhaetiq.Conta'
    end
    object QrMinhaEtiqTexto1: TWideStringField
      FieldName = 'Texto1'
      Origin = 'minhaetiq.Texto1'
      Size = 100
    end
    object QrMinhaEtiqTexto2: TWideStringField
      FieldName = 'Texto2'
      Origin = 'minhaetiq.Texto2'
      Size = 100
    end
    object QrMinhaEtiqTexto3: TWideStringField
      FieldName = 'Texto3'
      Origin = 'minhaetiq.Texto3'
      Size = 100
    end
    object QrMinhaEtiqTexto4: TWideStringField
      FieldName = 'Texto4'
      Origin = 'minhaetiq.Texto4'
      Size = 100
    end
  end
  object frxEtiquetas: TfrxReport
    Version = '4.10.1'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39721.525010486100000000
    ReportOptions.LastChange = 39788.771021226850000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxEtiquetasGetValue
    Left = 408
    Top = 144
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsMinhaEtiq
        DataSetName = 'frxDsMinhaEtiq'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 256
      DataSet = frxDsMinhaEtiq
      DataSetName = 'frxDsMinhaEtiq'
    end
  end
  object frxDsMinhaEtiq: TfrxDBDataset
    UserName = 'frxDsMinhaEtiq'
    CloseDataSource = False
    DataSet = QrMinhaEtiq
    BCDToCurrency = False
    Left = 436
    Top = 144
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 260
    Top = 12
  end
end
