unit MoviVKMsg;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkLabel,
  dmkCheckGroup, DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, DB,
  mySQLDbTables, dmkGeral;

type
  TFmMoviVKMsg = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LbMsg: TdmkLabel;
    QrProdutos: TmySQLQuery;
    QrProdutosNOMEGRA: TWideStringField;
    QrProdutosNOMETAM: TWideStringField;
    QrProdutosNOMECOR: TWideStringField;
    Grade: TStringGrid;
    StaticText1: TStaticText;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    procedure MontaStringGrid();
    procedure ReopenProdutos(Controle: Integer);
  public
    { Public declarations }
    FTipo: Integer;
    FLista: TStringList;
  end;

  var
  FmMoviVKMsg: TFmMoviVKMsg;

implementation

uses UnMyObjects;

{$R *.DFM}

procedure TFmMoviVKMsg.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMoviVKMsg.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmMoviVKMsg.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmMoviVKMsg.FormShow(Sender: TObject);
begin
  MontaStringGrid;
end;

procedure TFmMoviVKMsg.MontaStringGrid();
var
  i, Controle: Integer;
begin
  //Tipo 0 = Inativos => 1 = Sem estoque
  case FTipo of
    0: LbMsg.Caption := 'Os seguintes produtos pertencem ao kit selecionado mas est�o ' +
      sLineBreak + 'cadastrados como inativos e por isso n�o ser�o inclu�dos:';
    1: LbMsg.Caption := 'Os seguintes produtos pertencem ao kit selecionado mas est�o ' +
      sLineBreak + 'com o estoque zerado ou negativo e por isso n�o ser�o inclu�dos!';
  end;
  Grade.RowCount  := FLista.Count + 1;
  Grade.ColCount  := 3;
  Grade.FixedRows := 1;
  Grade.FixedCols := 0;
  //
  Grade.ColWidths[0] := 200;
  Grade.ColWidths[1] := 130;
  Grade.ColWidths[2] := 90;
  //
  //Monta cabe�alho
  Grade.Cells[0, 0] := 'Mercadoria';
  Grade.Cells[1, 0] := 'Cor';
  Grade.Cells[2, 0] := 'Tamanho';
  //
  for i := 0 to FLista.Count - 1 do
  begin
    Controle := Geral.IMV(FLista[i]);
    //
    ReopenProdutos(Controle);
    //
    Grade.Cells[0, i + 1] := QrProdutosNOMEGRA.Value;
    Grade.Cells[1, i + 1] := QrProdutosNOMECOR.Value;
    Grade.Cells[2, i + 1] := QrProdutosNOMETAM.Value; 
  end;
end;

procedure TFmMoviVKMsg.ReopenProdutos(Controle: Integer);
begin
  QrProdutos.Close;
  QrProdutos.Params[0].AsInteger := Controle;
  QrProdutos.Open;
end;

end.
