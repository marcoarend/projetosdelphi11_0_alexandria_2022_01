unit CiclosPro_Edit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Mask, DBCtrls, dmkEdit, dmkRadioGroup,
  dmkCheckBox, dmkDBEdit, dmkLabel, dmkMemo, ExtDlgs, ComCtrls, dmkGeral,
  dmkEditDateTimePicker, DB, mySQLDbTables, dmkPermissoes, UnDmkEnums,
  dmkDBLookupComboBox, dmkEditCB;

type
  TValAluCalc = (vacAlunos, vacPreco, vacTotal,
    vacComPromPer, vacComPromVal, vacComProfPer, vacComProfVal,
    vacDTSPromPer, vacDTSPromVal, vacDTSProfPer, vacDTSProfVal);
  TFmCiclosPro_Edit = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    Panel4: TPanel;
    Label2: TLabel;
    EdControle: TdmkEdit;
    Label5: TLabel;
    TPData: TdmkEditDateTimePicker;
    CkContinuar: TCheckBox;
    EdPromotor: TdmkEdit;
    Label34: TLabel;
    Label1: TLabel;
    EdProfessor: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    EdUF: TdmkEdit;
    Label7: TLabel;
    EdCodMunici: TdmkEditCB;
    Label105: TLabel;
    CBCodMunici: TdmkDBLookupComboBox;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdUFExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
    FOnCreate: Boolean;
  public
    { Public declarations }
  end;

  var
  FmCiclosPro_Edit: TFmCiclosPro_Edit;

implementation

uses UnMyVCLref, UmySQlModule, UnFinanceiro, Module, UnInternalConsts,
ModuleCiclos, CiclosProm, CiclosProf, UnMyObjects;

{$R *.DFM}


procedure TFmCiclosPro_Edit.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCiclosPro_Edit.EdUFExit(Sender: TObject);
var
  UF: String;
begin
  UF := EdUF.ValueVariant;
  //
  DmCiclos.ReopenMunici(UF);
end;

procedure TFmCiclosPro_Edit.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //
  if FmCiclosProm <> nil then
    EdPromotor.ValueVariant := Geral.IMV(FmCiclosProm.EdPromotor.Text);
  if FmCiclosProf <> nil then
    EdProfessor.ValueVariant := Geral.IMV(FmCiclosProf.EdProfessor.Text);
  //
  if FOnCreate then
  begin
    FOncreate := False;
    //
    if LaTipo.SQLType = stIns then
    begin
      //
    end;
  end;
end;

procedure TFmCiclosPro_Edit.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCiclosPro_Edit.FormShow(Sender: TObject);
var
  UF: String;
begin
  UF := EdUF.ValueVariant;
  //
  if UF <> '' then
    DmCiclos.ReopenMunici(UF);
end;

procedure TFmCiclosPro_Edit.BtOKClick(Sender: TObject);
var
  Ctrl,
  Controle, CodiCidade: Integer;
begin
  //s� usa stIns
  (*
  if FmCiclosProm <> nil then
    Ctrl := FmCiclosProm.QrCiclosAulaControle.Value
  else
  if FmCiclosProm <> nil then
    Ctrl := FmCiclosProm.QrCiclosAulaControle.Value
  else
  *)
  CodiCidade := EdCodMunici.ValueVariant;
  //
  if MyObjects.FIC(CodiCidade = 0, EdCodMunici, 'Cidade n�o definida!') then Exit;
  //
  if not Geral.ConfereUFeMunici_IBGE(EdUF.Text, EdCodMunici.ValueVariant, 'Turma') then Exit;
  //
  Ctrl     := 0;
  Controle := UMyMod.BuscaEmLivreY_Def('ciclosaula', 'controle', LaTipo.SQLType, Ctrl);
  //
  if UMyMod.ExecSQLInsUpdFm(FmCiclosPro_Edit, LaTipo.SQLType, 'ciclosaula',
    Controle, Dmod.QrUpd) then
  begin
    if FmCiclosProm <> nil then
    begin
      FmCiclosProm.ReopenCiclosAula(EdControle.ValueVariant);
    end;
    if FmCiclosProf <> nil then
    begin
      FmCiclosProf.ReopenCiclosAula(EdControle.ValueVariant);
    end;
    if CkContinuar.Checked then
    begin
      TPData.Date              := TPData.Date + 1;
      EdCodMunici.ValueVariant := 0;
      //
      TPData.SetFocus;
    end else
      Close;
  end;
end;

procedure TFmCiclosPro_Edit.FormCreate(Sender: TObject);
begin
  TPData.Date := Date;
  FOnCreate   := True;
  //
  if FmCiclosProm <> nil then
  begin
    EdPromotor.QryCampo  := 'Promotor';
    EdPromotor.UpdCampo  := 'Promotor';
    EdProfessor.QryCampo := '';
    EdProfessor.UpdCampo := '';
  end;
  if FmCiclosProf <> nil then
  begin
    EdPromotor.QryCampo  := '';
    EdPromotor.UpdCampo  := '';
    EdProfessor.QryCampo := 'Professor';
    EdProfessor.UpdCampo := 'Professor';
  end;
end;

end.
