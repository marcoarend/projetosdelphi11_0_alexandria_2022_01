unit GraGruVal;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkEdit;

type
  TFmGraGruVal = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Label1: TLabel;
    EdValor: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FConfirmou: Boolean;
  end;

  var
  FmGraGruVal: TFmGraGruVal;

implementation

uses Module, UnMyObjects;

{$R *.DFM}

procedure TFmGraGruVal.BtOKClick(Sender: TObject);
begin
  FConfirmou := True;
  Close;
end;

procedure TFmGraGruVal.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmGraGruVal.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmGraGruVal.FormCreate(Sender: TObject);
begin
  FConfirmou := False;
end;

procedure TFmGraGruVal.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
