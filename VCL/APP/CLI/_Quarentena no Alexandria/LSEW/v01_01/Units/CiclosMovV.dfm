object FmCiclosMovV: TFmCiclosMovV
  Left = 251
  Top = 168
  Caption = 'CIC-CICLO-003 :: Venda de Mercadorias'
  ClientHeight = 496
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnItens: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 448
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object Panel1: TPanel
      Left = 1
      Top = 399
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 4
      object BtConfItem: TBitBtn
        Tag = 14
        Left = 8
        Top = 4
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Confima Inclus'#227'o / altera'#231#227'o'
        Caption = '&Confirma'
        Default = True
        Enabled = False
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfItemClick
      end
      object Panel6: TPanel
        Left = 684
        Top = 1
        Width = 105
        Height = 46
        Align = alRight
        Alignment = taRightJustify
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn2: TBitBtn
          Tag = 13
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Hint = 'Cancela inclus'#227'o / altera'#231#227'o'
          Caption = '&Sa'#237'da'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn2Click
        end
      end
    end
    object DBGProd: TDBGrid
      Left = 1
      Top = 1
      Width = 790
      Height = 77
      Align = alTop
      DataSource = DmCiclos.DsMovimL
      Enabled = False
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMEGRA'
          Title.Caption = 'Mercadoria'
          Width = 230
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECOR'
          Title.Caption = 'Cor'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETAM'
          Title.Caption = 'Tamanho'
          Width = 68
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'POSIq'
          Title.Caption = 'Quant.'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PRECO'
          Title.Caption = 'Pre'#231'o'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'POSIv'
          Title.Caption = 'Total'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComisTipNOME'
          Title.Caption = 'T'
          Width = 10
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComisFat'
          Title.Caption = 'Fator'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComisVal'
          Title.Caption = 'Comiss'#227'o'
          Visible = True
        end>
    end
    object GradeA: TStringGrid
      Left = 1
      Top = 251
      Width = 790
      Height = 148
      Align = alBottom
      ColCount = 1
      DefaultColWidth = 18
      DefaultRowHeight = 18
      FixedCols = 0
      RowCount = 1
      FixedRows = 0
      TabOrder = 1
      OnClick = GradeAClick
      OnDrawCell = GradeADrawCell
      RowHeights = (
        18)
    end
    object GradeC: TStringGrid
      Left = 248
      Top = 116
      Width = 361
      Height = 89
      ColCount = 2
      DefaultColWidth = 18
      DefaultRowHeight = 18
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
      TabOrder = 2
      Visible = False
      RowHeights = (
        18
        18)
    end
    object Panel4: TPanel
      Left = 1
      Top = 168
      Width = 790
      Height = 83
      Align = alBottom
      TabOrder = 3
      object Label13: TLabel
        Left = 8
        Top = 4
        Width = 56
        Height = 13
        Caption = 'Mercadoria:'
      end
      object Label14: TLabel
        Left = 340
        Top = 4
        Width = 19
        Height = 13
        Caption = 'Cor:'
      end
      object Label15: TLabel
        Left = 488
        Top = 4
        Width = 48
        Height = 13
        Caption = 'Tamanho:'
      end
      object Label16: TLabel
        Left = 560
        Top = 4
        Width = 58
        Height = 13
        Caption = 'Quantidade:'
      end
      object Label17: TLabel
        Left = 636
        Top = 4
        Width = 31
        Height = 13
        Caption = 'Pre'#231'o:'
      end
      object Label18: TLabel
        Left = 712
        Top = 4
        Width = 27
        Height = 13
        Caption = 'Total:'
      end
      object SpeedButton6: TSpeedButton
        Left = 310
        Top = 20
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton6Click
      end
      object Label1: TLabel
        Left = 407
        Top = 42
        Width = 59
        Height = 13
        Caption = '% Comiss'#227'o:'
      end
      object Label2: TLabel
        Left = 483
        Top = 42
        Width = 57
        Height = 13
        Caption = '$ Comiss'#227'o:'
      end
      object Label3: TLabel
        Left = 9
        Top = 42
        Width = 86
        Height = 13
        Caption = 'Rol de comiss'#245'es:'
      end
      object Label4: TLabel
        Left = 560
        Top = 42
        Width = 58
        Height = 13
        Caption = 'Qtd anterior:'
        Enabled = False
      end
      object Label5: TLabel
        Left = 636
        Top = 42
        Width = 69
        Height = 13
        Caption = 'Pre'#231'o anterior:'
        Enabled = False
      end
      object Label6: TLabel
        Left = 712
        Top = 42
        Width = 65
        Height = 13
        Caption = 'Total anterior:'
        Enabled = False
      end
      object SpeedButton1: TSpeedButton
        Left = 380
        Top = 58
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton1Click
      end
      object EdCorCod: TdmkEdit
        Left = 340
        Top = 20
        Width = 32
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        ReadOnly = True
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdCorNom: TdmkEdit
        Left = 371
        Top = 20
        Width = 113
        Height = 21
        Enabled = False
        ReadOnly = True
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdTamCod: TdmkEdit
        Left = 488
        Top = 20
        Width = 32
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        ReadOnly = True
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdTamNom: TdmkEdit
        Left = 519
        Top = 20
        Width = 37
        Height = 21
        Enabled = False
        ReadOnly = True
        TabOrder = 5
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdQtd: TdmkEdit
        Left = 560
        Top = 20
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnEnter = EdQtdEnter
        OnExit = EdQtdExit
      end
      object EdPrc: TdmkEdit
        Left = 636
        Top = 20
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnEnter = EdPrcEnter
        OnExit = EdPrcExit
      end
      object EdVen: TdmkEdit
        Left = 712
        Top = 20
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdVenChange
        OnEnter = EdVenEnter
        OnExit = EdVenExit
      end
      object EdQta: TdmkEdit
        Left = 560
        Top = 58
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        ReadOnly = True
        TabOrder = 13
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 3
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdCua: TdmkEdit
        Left = 636
        Top = 58
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        ReadOnly = True
        TabOrder = 14
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdVla: TdmkEdit
        Left = 712
        Top = 58
        Width = 72
        Height = 21
        Alignment = taRightJustify
        Enabled = False
        ReadOnly = True
        TabOrder = 15
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdGrade: TdmkEditCB
        Left = 8
        Top = 20
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdGradeChange
        DBLookupComboBox = CBGrade
        IgnoraDBLookupComboBox = False
      end
      object CBGrade: TdmkDBLookupComboBox
        Left = 64
        Top = 20
        Width = 242
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsGrades
        TabOrder = 1
        dmkEditCB = EdGrade
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdComisFat: TdmkEdit
        Left = 407
        Top = 58
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 6
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,000000'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnEnter = EdComisFatEnter
        OnExit = EdComisFatExit
      end
      object EdComisVal: TdmkEdit
        Left = 483
        Top = 58
        Width = 72
        Height = 21
        Alignment = taRightJustify
        TabOrder = 12
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnEnter = EdComisValEnter
        OnExit = EdComisValExit
      end
      object EdRolComis: TdmkEditCB
        Left = 9
        Top = 58
        Width = 63
        Height = 21
        Alignment = taRightJustify
        TabOrder = 9
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdRolComisChange
        DBLookupComboBox = CBRolComis
        IgnoraDBLookupComboBox = False
      end
      object CBRolComis: TdmkDBLookupComboBox
        Left = 71
        Top = 58
        Width = 305
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsRolComis
        TabOrder = 10
        dmkEditCB = EdRolComis
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object RGComissao: TRadioGroup
      Left = 16
      Top = 130
      Width = 153
      Height = 41
      Caption = ' Forma de comissionamento: '
      Columns = 3
      Enabled = False
      ItemIndex = 2
      Items.Strings = (
        '???'
        'Valor'
        '%')
      TabOrder = 5
      Visible = False
    end
    object GradeTurmas: TDBGrid
      Left = 1
      Top = 78
      Width = 790
      Height = 40
      Align = alTop
      Enabled = False
      TabOrder = 6
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Cidade'
          Width = 124
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AlunosInsc'
          Title.Caption = 'Incritos'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'AlunosAula'
          Title.Caption = 'Presen'#231'a'
          Width = 36
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ValorTot'
          Title.Caption = 'Valor'
          Width = 72
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComProfDeb'
          Title.Caption = 'D.Prf.Comis.'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTSProfDeb'
          Title.Caption = 'D.Prf.13'#186
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'ComPromDeb'
          Title.Caption = 'D.Prm.Comis.'
          Width = 60
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DTSPromDeb'
          Title.Caption = 'D.Prm.13'#186
          Width = 60
          Visible = True
        end>
    end
    object GradeX: TStringGrid
      Left = 288
      Top = 268
      Width = 361
      Height = 89
      ColCount = 2
      DefaultColWidth = 18
      DefaultRowHeight = 18
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goEditing]
      TabOrder = 7
      Visible = False
      RowHeights = (
        18
        18)
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = 'Venda de Mercadorias'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 708
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 706
      ExplicitHeight = 44
    end
    object LaStatus: TdmkLabel
      Left = 709
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 708
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object QrGrades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT pro.Codigo, gra.CodUsu, gra.Nome'
      'FROM produtos pro'
      'LEFT JOIN grades gra ON gra.Codigo=pro.Codigo'
      'LEFT JOIN gradeg grg ON grg.Codigo=gra.GradeG'
      'LEFT JOIN graded grd ON grd.Codigo=grg.GradeD'
      'WHERE pro.Ativo = 1'
      'AND grd.Codigo=:P0'
      'ORDER BY Nome')
    Left = 12
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGradesCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
  end
  object DsGrades: TDataSource
    DataSet = QrGrades
    Left = 40
    Top = 12
  end
  object QrRolComis: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM equicom'
      '')
    Left = 404
    Top = 216
    object QrRolComisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRolComisNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsRolComis: TDataSource
    DataSet = QrRolComis
    Left = 432
    Top = 216
  end
  object QrPerc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT PerceProd'
      'FROM equicomits'
      'WHERE Codigo=:P0'
      'AND Produto=:P1'
      ''
      '')
    Left = 72
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPercPerceProd: TFloatField
      FieldName = 'PerceProd'
    end
  end
  object QrEquiConTrf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT PerceGene'
      'FROM equicontrf'
      'WHERE Codigo=:P0'
      'AND GradeComis=:P1'
      ' ')
    Left = 628
    Top = 112
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrEquiConTrfPerceGene: TFloatField
      FieldName = 'PerceGene'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 116
    Top = 12
  end
end
