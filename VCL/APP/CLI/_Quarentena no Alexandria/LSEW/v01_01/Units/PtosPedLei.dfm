object FmPtosPedLei: TFmPtosPedLei
  Left = 339
  Top = 185
  Caption = 
    'PTO-VENDA-004 :: Itens de Pedidos de Pontos de Vendas - por Leit' +
    'ura'
  ClientHeight = 614
  ClientWidth = 1008
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 1008
    Height = 518
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object PnLeitura: TPanel
      Left = 0
      Top = 0
      Width = 1008
      Height = 49
      Align = alTop
      BevelOuter = bvNone
      ParentBackground = False
      TabOrder = 0
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 545
        Height = 49
        Align = alLeft
        TabOrder = 0
        object Label3: TLabel
          Left = 4
          Top = 4
          Width = 89
          Height = 13
          Caption = 'Leitura / digita'#231#227'o:'
        end
        object LaQtdeLei: TLabel
          Left = 188
          Top = 4
          Width = 26
          Height = 13
          Caption = 'Qtde:'
        end
        object Label1: TLabel
          Left = 232
          Top = 4
          Width = 63
          Height = 13
          Caption = 'Pre'#231'o tabela:'
          Enabled = False
        end
        object Label2: TLabel
          Left = 376
          Top = 4
          Width = 51
          Height = 13
          Caption = 'Pre'#231'o real:'
          Enabled = False
        end
        object Label10: TLabel
          Left = 304
          Top = 4
          Width = 57
          Height = 13
          Caption = '%Desconto:'
        end
        object EdLeituraCodigoDeBarras: TEdit
          Left = 4
          Top = 20
          Width = 180
          Height = 21
          MaxLength = 20
          TabOrder = 0
          OnChange = EdLeituraCodigoDeBarrasChange
          OnKeyDown = EdLeituraCodigoDeBarrasKeyDown
        end
        object EdQtdLei: TdmkEdit
          Left = 188
          Top = 20
          Width = 40
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 1
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = True
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '1'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 1
          OnEnter = EdQtdLeiEnter
          OnKeyDown = EdQtdLeiKeyDown
        end
        object BtOK: TBitBtn
          Tag = 14
          Left = 448
          Top = 3
          Width = 90
          Height = 40
          Caption = '&OK'
          NumGlyphs = 2
          TabOrder = 5
          OnClick = BtOKClick
        end
        object EdPrecoO: TdmkEdit
          Left = 232
          Top = 20
          Width = 68
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 2
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnChange = EdPrecoOChange
        end
        object EdPrecoR: TdmkEdit
          Left = 376
          Top = 20
          Width = 68
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 3
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
        end
        object EdDescoP: TdmkEdit
          Left = 304
          Top = 20
          Width = 68
          Height = 21
          Alignment = taRightJustify
          Enabled = False
          TabOrder = 4
          FormatType = dmktfDouble
          MskType = fmtNone
          DecimalSize = 2
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMax = '99'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0,00'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0.000000000000000000
          OnChange = EdDescoPChange
        end
      end
      object Panel9: TPanel
        Left = 545
        Top = 0
        Width = 463
        Height = 49
        Align = alClient
        Enabled = False
        TabOrder = 1
        object Label4: TLabel
          Left = 4
          Top = 4
          Width = 86
          Height = 13
          Caption = 'Grupo de produto:'
          FocusControl = DBEdit1
        end
        object Label5: TLabel
          Left = 264
          Top = 4
          Width = 19
          Height = 13
          Caption = 'Cor:'
          FocusControl = DBEdit2
        end
        object Label6: TLabel
          Left = 388
          Top = 4
          Width = 48
          Height = 13
          Caption = 'Tamanho:'
          FocusControl = DBEdit3
        end
        object DBEdit1: TDBEdit
          Left = 4
          Top = 20
          Width = 257
          Height = 21
          DataField = 'NO_GRADE'
          DataSource = DsItem
          TabOrder = 0
        end
        object DBEdit2: TDBEdit
          Left = 264
          Top = 20
          Width = 121
          Height = 21
          DataField = 'NO_COR'
          DataSource = DsItem
          TabOrder = 1
        end
        object DBEdit3: TDBEdit
          Left = 388
          Top = 20
          Width = 69
          Height = 21
          DataField = 'NO_TAM'
          DataSource = DsItem
          TabOrder = 2
        end
      end
    end
    object DBGPtosStqMov: TDBGrid
      Left = 0
      Top = 49
      Width = 1008
      Height = 469
      Align = alClient
      DataSource = DsPtosStqMov
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'EAN13'
          Title.Caption = 'C'#243'digo de barras'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Reduzido'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DataIns'
          Title.Caption = 'Data / hora'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CO_Produto'
          Title.Caption = 'Produto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Produto'
          Title.Caption = 'Descri'#231#227'o'
          Width = 180
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Cor'
          Title.Caption = 'Descri'#231#227'o'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NO_Tam'
          Title.Caption = 'Tamanho'
          Width = 78
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Quanti'
          Title.Caption = 'Qtde'
          Width = 32
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoO'
          Title.Caption = 'Pre'#231'o lista'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'DescoP'
          Title.Caption = '% desconto'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PrecoR'
          Title.Caption = 'Valor'
          Width = 69
          Visible = True
        end>
    end
  end
  object PainelConfirma: TPanel
    Left = 0
    Top = 566
    Width = 1008
    Height = 48
    Align = alBottom
    TabOrder = 1
    object Label7: TLabel
      Left = 4
      Top = 4
      Width = 50
      Height = 13
      Caption = 'Valor total:'
      FocusControl = DBEdValTot
    end
    object Panel2: TPanel
      Left = 896
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtExclui: TBitBtn
      Tag = 12
      Left = 453
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Exclui'
      NumGlyphs = 2
      TabOrder = 1
      OnClick = BtExcluiClick
    end
    object CkFixo: TCheckBox
      Left = 85
      Top = 16
      Width = 125
      Height = 17
      Caption = 'Quantidade fixa.'
      Checked = True
      State = cbChecked
      TabOrder = 2
      OnClick = CkFixoClick
    end
    object DBEdValTot: TDBEdit
      Left = 4
      Top = 20
      Width = 75
      Height = 21
      DataField = 'TOT_PrecoR'
      DataSource = DsSUMPtosStqMov
      TabOrder = 3
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1008
    Height = 48
    Align = alTop
    Caption = 'Itens de Pedidos de Pontos de Vendas - por Leitura'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 924
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 925
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 620
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object QrItem: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrItemBeforeClose
    SQL.Strings = (
      'SELECT ggs.Nome NO_GRADE, cor.Nome NO_COR,  '
      'tam.Nome NO_TAM, prd.Controle GraGruX, ggv.CustoPreco'
      'FROM produtos prd '
      'LEFT JOIN grades   ggs ON ggs.Codigo=prd.Codigo'
      'LEFT JOIN cores cor ON cor.Codigo=prd.Cor'
      'LEFT JOIN tamanhos tam ON tam.Codigo=prd.Tam'
      
        'LEFT JOIN gragruval ggv ON ggv.GraGruX=prd.Controle AND ggv.GraC' +
        'usPrc=:P0'
      'WHERE prd.Controle=:P1')
    Left = 68
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrItemNO_GRADE: TWideStringField
      FieldName = 'NO_GRADE'
      Size = 30
    end
    object QrItemNO_COR: TWideStringField
      FieldName = 'NO_COR'
      Size = 100
    end
    object QrItemNO_TAM: TWideStringField
      FieldName = 'NO_TAM'
      Size = 100
    end
    object QrItemGraGruX: TIntegerField
      FieldName = 'GraGruX'
      Required = True
    end
    object QrItemCustoPreco: TFloatField
      FieldName = 'CustoPreco'
    end
  end
  object DsItem: TDataSource
    DataSet = QrItem
    Left = 96
    Top = 8
  end
  object QrPreco: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT pvi.PrecoF, '
      '(QuantP-QuantC-QuantV) QuantF'
      'FROM ptosstqmov pvi'
      'WHERE pvi.Codigo=:P0'
      'AND pvi.GraGruX=:P1'
      ' ')
    Left = 124
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrPrecoPrecoF: TFloatField
      FieldName = 'PrecoF'
      Required = True
    end
    object QrPrecoQuantF: TFloatField
      FieldName = 'QuantF'
      Required = True
    end
  end
  object DsPreco: TDataSource
    DataSet = QrPreco
    Left = 152
    Top = 8
  end
  object QrFator: TmySQLQuery
    Database = Dmod.MyDB
    Left = 180
    Top = 8
  end
  object QrPtosStqMov: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPtosStqMovAfterScroll
    OnCalcFields = QrPtosStqMovCalcFields
    SQL.Strings = (
      'SELECT prd.Controle Reduzido, grs.Codigo CO_Produto, '
      'grs.Nome NO_Produto, cor.Nome NO_Cor, tam.Nome NO_Tam, '
      'tam.CodUsu CU_Tam, psm.Quanti, psm.PrecoO, '
      'psm.DescoP, psm.PrecoR, psm.DataIns, psm.IDCtrl'
      'FROM ptosstqmov psm'
      'LEFT JOIN produtos prd ON prd.Controle=psm.Produto'
      'LEFT JOIN grades grs ON grs.Codigo=prd.Codigo'
      'LEFT JOIN tamanhos tam ON tam.Codigo=prd.Tam'
      'LEFT JOIN cores cor ON cor.Codigo=prd.Cor'
      'WHERE psm.CodInn=:P0'
      'ORDER BY DataIns DESC')
    Left = 708
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPtosStqMovNO_Produto: TWideStringField
      FieldName = 'NO_Produto'
      Size = 30
    end
    object QrPtosStqMovNO_Cor: TWideStringField
      FieldName = 'NO_Cor'
      Size = 100
    end
    object QrPtosStqMovNO_Tam: TWideStringField
      FieldName = 'NO_Tam'
      Size = 100
    end
    object QrPtosStqMovCU_Tam: TIntegerField
      FieldName = 'CU_Tam'
    end
    object QrPtosStqMovQuanti: TFloatField
      FieldName = 'Quanti'
      Required = True
    end
    object QrPtosStqMovPrecoO: TFloatField
      FieldName = 'PrecoO'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPtosStqMovDescoP: TFloatField
      FieldName = 'DescoP'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPtosStqMovPrecoR: TFloatField
      FieldName = 'PrecoR'
      Required = True
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrPtosStqMovDataIns: TDateTimeField
      FieldName = 'DataIns'
      Required = True
      DisplayFormat = 'dd/mm/yyyy hh:nn:ss'
    end
    object QrPtosStqMovCO_Produto: TIntegerField
      FieldName = 'CO_Produto'
    end
    object QrPtosStqMovIDCtrl: TIntegerField
      FieldName = 'IDCtrl'
      Required = True
    end
    object QrPtosStqMovReduzido: TIntegerField
      FieldName = 'Reduzido'
    end
    object QrPtosStqMovEAN13: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'EAN13'
      Size = 13
      Calculated = True
    end
  end
  object DsPtosStqMov: TDataSource
    DataSet = QrPtosStqMov
    Left = 736
    Top = 8
  end
  object QrLista: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT GraCusPrc'
      'FROM fisregmvt'
      'WHERE TipoMov=1'
      'AND Empresa=:P0'
      'AND Codigo=:P1')
    Left = 208
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrListaGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
  end
  object QrExiste_: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Controle, QuantP, PrecoR, DescoP'
      'FROM ptosstqmov'
      'WHERE Codigo=:P0'
      'AND GraGruX=:P1'
      '')
    Left = 764
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrExiste_Controle: TIntegerField
      FieldName = 'Controle'
    end
    object QrExiste_QuantP: TFloatField
      FieldName = 'QuantP'
    end
    object QrExiste_PrecoR: TFloatField
      FieldName = 'PrecoR'
    end
    object QrExiste_DescoP: TFloatField
      FieldName = 'DescoP'
    end
  end
  object QrPediVdaLei: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Conta, Sequencia'
      'FROM pedivdalei'
      'WHERE Controle=:P0'
      'ORDER BY Sequencia')
    Left = 792
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediVdaLeiConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrPediVdaLeiSequencia: TIntegerField
      FieldName = 'Sequencia'
    end
  end
  object DsPediVdaLei: TDataSource
    DataSet = QrPediVdaLei
    Left = 820
    Top = 8
  end
  object Itens_por_grupo: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPtosStqMovAfterScroll
    SQL.Strings = (
      'SELECT grs.Codigo CO_Produto, grs.Nome NO_Produto, '
      'cor.Nome NO_Cor, tam.Nome NO_Tam, tam.CodUsu CU_Tam, '
      'SUM(psm.Quanti) Quanti, SUM(psm.PrecoR) Valor,'
      'SUM(psm.PrecoO) / SUM(psm.Quanti)  PrecoO,'
      'SUM(psm.PrecoR) / SUM(psm.Quanti)  PrecoR,'
      'SUM(psm.DescoP) / SUM(psm.Quanti)  DescoP'
      'FROM ptosstqmov psm'
      'LEFT JOIN produtos prd ON prd.Controle=psm.Produto'
      'LEFT JOIN grades grs ON grs.Codigo=prd.Codigo'
      'LEFT JOIN tamanhos tam ON tam.Codigo=prd.Tam'
      'LEFT JOIN cores cor ON cor.Codigo=prd.Cor'
      'WHERE psm.CodInn=:P0'
      'GROUP BY prd.Cor, prd.Tam'
      'ORDER BY NO_Produto, NO_Cor, CU_Tam, NO_Tam')
    Left = 708
    Top = 36
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object StringField1: TWideStringField
      FieldName = 'NO_Produto'
      Size = 30
    end
    object StringField2: TWideStringField
      FieldName = 'NO_Cor'
      Size = 100
    end
    object StringField3: TWideStringField
      FieldName = 'NO_Tam'
      Size = 100
    end
    object IntegerField1: TIntegerField
      FieldName = 'CU_Tam'
    end
    object FloatField1: TFloatField
      FieldName = 'Quanti'
    end
    object FloatField2: TFloatField
      FieldName = 'Valor'
    end
    object FloatField3: TFloatField
      FieldName = 'PrecoO'
    end
    object FloatField4: TFloatField
      FieldName = 'PrecoR'
    end
    object FloatField5: TFloatField
      FieldName = 'DescoP'
    end
    object IntegerField2: TIntegerField
      FieldName = 'CO_Produto'
    end
  end
  object QrSUMPtosStqMov: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrPtosStqMovAfterScroll
    OnCalcFields = QrPtosStqMovCalcFields
    SQL.Strings = (
      'SELECT SUM(psm.PrecoR) TOT_PrecoR'
      'FROM ptosstqmov psm'
      'WHERE psm.CodInn=:P0')
    Left = 500
    Top = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSUMPtosStqMovTOT_PrecoR: TFloatField
      FieldName = 'TOT_PrecoR'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
  end
  object DsSUMPtosStqMov: TDataSource
    DataSet = QrSUMPtosStqMov
    Left = 528
    Top = 312
  end
end
