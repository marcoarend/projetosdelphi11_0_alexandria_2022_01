object FmPediPrzIts: TFmPediPrzIts
  Left = 339
  Top = 185
  Caption = 'PED-PRAZO-002 :: Edi'#231#227'o de Item de Prazo'
  ClientHeight = 442
  ClientWidth = 406
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCloseQuery = FormCloseQuery
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnControla: TPanel
    Left = 0
    Top = 394
    Width = 406
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtInclui: TBitBtn
      Tag = 10
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Inclui'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtIncluiClick
    end
    object Panel2: TPanel
      Left = 294
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtAltera: TBitBtn
      Tag = 11
      Left = 112
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Altera'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtAlteraClick
    end
    object BtExclui: TBitBtn
      Tag = 12
      Left = 204
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Exclui'
      NumGlyphs = 2
      TabOrder = 3
      OnClick = BtExcluiClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 406
    Height = 48
    Align = alTop
    Caption = 'Edi'#231#227'o de Item de Prazo'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 322
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 323
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object PnEdita: TPanel
    Left = 0
    Top = 292
    Width = 406
    Height = 102
    Align = alBottom
    TabOrder = 2
    Visible = False
    object Label4: TLabel
      Left = 240
      Top = 8
      Width = 49
      Height = 13
      Caption = '% parcela:'
    end
    object Label5: TLabel
      Left = 12
      Top = 8
      Width = 62
      Height = 13
      Caption = 'ID Condi'#231#227'o:'
    end
    object Label6: TLabel
      Left = 112
      Top = 8
      Width = 43
      Height = 13
      Caption = 'ID prazo:'
    end
    object Label8: TLabel
      Left = 196
      Top = 8
      Width = 24
      Height = 13
      Caption = 'Dias:'
    end
    object Label9: TLabel
      Left = 304
      Top = 8
      Width = 49
      Height = 13
      Caption = '% parcela:'
      Visible = False
    end
    object Panel1: TPanel
      Left = 1
      Top = 53
      Width = 404
      Height = 48
      Align = alBottom
      TabOrder = 5
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 20
        Top = 4
        Width = 90
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel4: TPanel
        Left = 292
        Top = 1
        Width = 111
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 2
          Top = 3
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object EdDias: TdmkEdit
      Left = 196
      Top = 24
      Width = 40
      Height = 21
      TabOrder = 2
      FormatType = dmktfString
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '1'
      ValMax = '0'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      QryCampo = 'Dias'
      UpdCampo = 'Dias'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = ''
    end
    object EdPercent1: TdmkEdit
      Left = 240
      Top = 24
      Width = 60
      Height = 21
      Alignment = taRightJustify
      TabOrder = 3
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      QryCampo = 'Percent1'
      UpdCampo = 'Percent1'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
    object EdControle: TdmkEdit
      Left = 112
      Top = 24
      Width = 80
      Height = 21
      Alignment = taRightJustify
      Enabled = False
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
    object dmkDBEdit1: TdmkDBEdit
      Left = 12
      Top = 24
      Width = 97
      Height = 21
      DataField = 'Codigo'
      DataSource = FmPediPrzCab.DsPediPrzCab
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utYes
      Alignment = taLeftJustify
    end
    object EdPercent2: TdmkEdit
      Left = 304
      Top = 24
      Width = 60
      Height = 21
      Alignment = taRightJustify
      TabOrder = 4
      Visible = False
      FormatType = dmktfDouble
      MskType = fmtNone
      DecimalSize = 4
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0,0000'
      QryCampo = 'Percent2'
      UpdCampo = 'Percent2'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0.000000000000000000
    end
  end
  object PnCabeca: TPanel
    Left = 0
    Top = 48
    Width = 406
    Height = 85
    Align = alTop
    Enabled = False
    TabOrder = 3
    object Label1: TLabel
      Left = 4
      Top = 4
      Width = 14
      Height = 13
      Caption = 'ID:'
      FocusControl = DBEdCodigo
    end
    object Label2: TLabel
      Left = 4
      Top = 44
      Width = 51
      Height = 13
      Caption = 'Descri'#231#227'o:'
      FocusControl = DBEdNome
    end
    object Label3: TLabel
      Left = 64
      Top = 4
      Width = 36
      Height = 13
      Caption = 'C'#243'digo:'
      FocusControl = DBEdit1
    end
    object Label7: TLabel
      Left = 320
      Top = 4
      Width = 54
      Height = 13
      Caption = 'Percentual:'
      FocusControl = DBEdPercent
    end
    object Label10: TLabel
      Left = 148
      Top = 4
      Width = 49
      Height = 13
      Caption = '% parcela:'
      FocusControl = DBEdPercent
      Visible = False
    end
    object Label11: TLabel
      Left = 232
      Top = 4
      Width = 49
      Height = 13
      Caption = '% parcela:'
      FocusControl = DBEdPercent
      Visible = False
    end
    object DBEdCodigo: TDBEdit
      Left = 4
      Top = 20
      Width = 56
      Height = 21
      Hint = 'N'#186' do banco'
      TabStop = False
      DataField = 'Codigo'
      DataSource = FmPediPrzCab.DsPediPrzCab
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 0
    end
    object DBEdNome: TDBEdit
      Left = 4
      Top = 60
      Width = 397
      Height = 21
      Hint = 'Nome do banco'
      Color = clWhite
      DataField = 'Nome'
      DataSource = FmPediPrzCab.DsPediPrzCab
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      TabOrder = 1
    end
    object DBEdit1: TDBEdit
      Left = 64
      Top = 20
      Width = 80
      Height = 21
      DataField = 'CodUsu'
      DataSource = FmPediPrzCab.DsPediPrzCab
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 2
    end
    object DBEdPercent: TDBEdit
      Left = 320
      Top = 20
      Width = 81
      Height = 21
      DataField = 'PercentT'
      DataSource = FmPediPrzCab.DsPediPrzCab
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnChange = DBEdPercentChange
    end
    object DBEdit2: TDBEdit
      Left = 148
      Top = 20
      Width = 81
      Height = 21
      DataField = 'Percent1'
      DataSource = FmPediPrzCab.DsPediPrzCab
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 4
      Visible = False
      OnChange = DBEdPercentChange
    end
    object DBEdit3: TDBEdit
      Left = 232
      Top = 20
      Width = 81
      Height = 21
      DataField = 'Percent2'
      DataSource = FmPediPrzCab.DsPediPrzCab
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
      TabOrder = 5
      Visible = False
      OnChange = DBEdPercentChange
    end
  end
  object DBGrid1: TDBGrid
    Left = 0
    Top = 133
    Width = 406
    Height = 159
    Align = alClient
    DataSource = FmPediPrzCab.DsPediPrzIts
    TabOrder = 4
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
    Columns = <
      item
        Expanded = False
        FieldName = 'Dias'
        Width = 48
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'PERCENTT'
        Title.Caption = '% total'
        Width = 84
        Visible = True
      end
      item
        Expanded = False
        FieldName = 'Controle'
        Title.Caption = 'ID'
        Visible = True
      end>
  end
  object QrSum1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(Dias) Parcelas, '
      'SUM(Percent1 + Percent2) PercentT,'
      'SUM(Percent1) Percent1, SUM(Percent2) Percent2,'
      'SUM(Dias) / COUNT(Dias) MedDDSimpl,'
      '(SUM((Percent1 + Percent2) * Dias) / '
      '  SUM(Percent1 + Percent2)) MedDDReal,'
      'SUM(Percent1 * Dias) / SUM(Percent1) MedDDPerc1,'
      'SUM(Percent2 * Dias) / SUM(Percent2) MedDDPerc2'
      'FROM pediprzits'
      'WHERE Codigo=:P0')
    Left = 8
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSum1Parcelas: TLargeintField
      FieldName = 'Parcelas'
      Required = True
    end
    object QrSum1PercentT: TFloatField
      FieldName = 'PercentT'
    end
    object QrSum1Percent1: TFloatField
      FieldName = 'Percent1'
    end
    object QrSum1Percent2: TFloatField
      FieldName = 'Percent2'
    end
    object QrSum1MedDDSimpl: TFloatField
      FieldName = 'MedDDSimpl'
    end
    object QrSum1MedDDReal: TFloatField
      FieldName = 'MedDDReal'
    end
    object QrSum1MedDDPerc1: TFloatField
      FieldName = 'MedDDPerc1'
    end
    object QrSum1MedDDPerc2: TFloatField
      FieldName = 'MedDDPerc2'
    end
  end
end
