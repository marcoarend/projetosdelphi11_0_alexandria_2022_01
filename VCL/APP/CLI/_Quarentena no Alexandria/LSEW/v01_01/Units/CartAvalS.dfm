object FmCartAvalS: TFmCartAvalS
  Left = 339
  Top = 185
  Caption = 'CAD-CARTA-003 :: Cartas de avalia'#231#227'o status'
  ClientHeight = 165
  ClientWidth = 633
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 117
    Width = 633
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 521
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 633
    Height = 48
    Align = alTop
    Caption = 'Cartas de avalia'#231#227'o status'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 549
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 547
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 550
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 932
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 633
    Height = 69
    Align = alClient
    TabOrder = 0
    object Label3: TLabel
      Left = 15
      Top = 12
      Width = 35
      Height = 13
      Caption = 'Motivo:'
    end
    object SpeedButton1: TSpeedButton
      Left = 404
      Top = 28
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = SpeedButton1Click
    end
    object Label1: TLabel
      Left = 432
      Top = 12
      Width = 57
      Height = 13
      Caption = 'Data status:'
    end
    object Label2: TLabel
      Left = 524
      Top = 12
      Width = 57
      Height = 13
      Caption = 'Hora status:'
    end
    object TPData: TDateTimePicker
      Left = 432
      Top = 28
      Width = 90
      Height = 21
      Date = 39422.782497824100000000
      Time = 39422.782497824100000000
      Enabled = False
      TabOrder = 2
    end
    object TPHora: TDateTimePicker
      Left = 524
      Top = 28
      Width = 90
      Height = 21
      Date = 39422.782497824100000000
      Time = 39422.782497824100000000
      Enabled = False
      Kind = dtkTime
      TabOrder = 3
    end
    object EdMotivo: TdmkEditCB
      Left = 15
      Top = 28
      Width = 56
      Height = 21
      Alignment = taRightJustify
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      DBLookupComboBox = CBMotivo
      IgnoraDBLookupComboBox = False
    end
    object CBMotivo: TdmkDBLookupComboBox
      Left = 71
      Top = 28
      Width = 331
      Height = 21
      KeyField = 'Codigo'
      ListField = 'Nome'
      ListSource = DsMotivos
      TabOrder = 1
      dmkEditCB = EdMotivo
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
  end
  object QrMotivos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM motivos'
      'WHERE Ativo > 0')
    Left = 20
    Top = 9
    object QrMotivosCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'statusv.Codigo'
    end
    object QrMotivosNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'statusv.Nome'
      Size = 100
    end
  end
  object DsMotivos: TDataSource
    DataSet = QrMotivos
    Left = 48
    Top = 9
  end
end
