object FmGraGruSel: TFmGraGruSel
  Left = 339
  Top = 185
  Caption = 'PRD-GRADE-017 :: Seleciona produtos'
  ClientHeight = 262
  ClientWidth = 684
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PnCabeca: TPanel
    Left = 0
    Top = 0
    Width = 684
    Height = 48
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 0
    object GB_R: TGroupBox
      Left = 636
      Top = 0
      Width = 48
      Height = 48
      Align = alRight
      TabOrder = 0
      object ImgTipo: TdmkImage
        Left = 8
        Top = 11
        Width = 32
        Height = 32
        Transparent = True
        SQLType = stNil
      end
    end
    object GB_L: TGroupBox
      Left = 0
      Top = 0
      Width = 48
      Height = 48
      Align = alLeft
      TabOrder = 1
    end
    object GB_M: TGroupBox
      Left = 48
      Top = 0
      Width = 588
      Height = 48
      Align = alClient
      TabOrder = 2
      object LaTitulo1A: TLabel
        Left = 7
        Top = 9
        Width = 233
        Height = 32
        Caption = 'Seleciona produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clSilver
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        Visible = False
      end
      object LaTitulo1B: TLabel
        Left = 9
        Top = 11
        Width = 233
        Height = 32
        Caption = 'Seleciona produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clGradientActiveCaption
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
      object LaTitulo1C: TLabel
        Left = 8
        Top = 10
        Width = 233
        Height = 32
        Caption = 'Seleciona produtos'
        Color = clBtnFace
        Font.Charset = ANSI_CHARSET
        Font.Color = clHotLight
        Font.Height = -27
        Font.Name = 'Arial'
        Font.Style = []
        ParentColor = False
        ParentFont = False
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 48
    Width = 684
    Height = 100
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 1
    object Panel3: TPanel
      Left = 0
      Top = 0
      Width = 684
      Height = 100
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object GroupBox1: TGroupBox
        Left = 0
        Top = 0
        Width = 684
        Height = 100
        Align = alClient
        TabOrder = 0
      end
    end
  end
  object GBAvisos1: TGroupBox
    Left = 0
    Top = 148
    Width = 684
    Height = 44
    Align = alBottom
    Caption = ' Avisos: '
    TabOrder = 2
    object Panel4: TPanel
      Left = 2
      Top = 15
      Width = 680
      Height = 27
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object LaAviso1: TLabel
        Left = 13
        Top = 2
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clSilver
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
      object LaAviso2: TLabel
        Left = 12
        Top = 1
        Width = 120
        Height = 16
        Caption = '..............................'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clRed
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = []
        ParentFont = False
        Transparent = True
      end
    end
  end
  object GBRodaPe: TGroupBox
    Left = 0
    Top = 192
    Width = 684
    Height = 70
    Align = alBottom
    TabOrder = 3
    object PnSaiDesis: TPanel
      Left = 538
      Top = 15
      Width = 144
      Height = 53
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Cursor = crHandPoint
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object Panel1: TPanel
      Left = 2
      Top = 15
      Width = 536
      Height = 53
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object BtOK: TBitBtn
        Tag = 14
        Left = 12
        Top = 3
        Width = 120
        Height = 40
        Caption = '&OK'
        NumGlyphs = 2
        TabOrder = 0
        OnClick = BtOKClick
      end
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 48
    Width = 684
    Height = 100
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 4
    object TabSheet1: TTabSheet
      Caption = 'Mercadorias'
      object Label19: TLabel
        Left = 3
        Top = 9
        Width = 77
        Height = 13
        Caption = 'Mercadoria: [F7]'
      end
      object Label30: TLabel
        Left = 364
        Top = 9
        Width = 19
        Height = 13
        Caption = 'Cor:'
      end
      object Label31: TLabel
        Left = 522
        Top = 9
        Width = 48
        Height = 13
        Caption = 'Tamanho:'
      end
      object EdProd: TdmkEditCB
        Left = 3
        Top = 25
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBProd
        IgnoraDBLookupComboBox = False
      end
      object CBProd: TdmkDBLookupComboBox
        Left = 61
        Top = 25
        Width = 290
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsGrades
        TabOrder = 1
        dmkEditCB = EdProd
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object CBCor: TdmkDBLookupComboBox
        Left = 364
        Top = 25
        Width = 145
        Height = 21
        KeyField = 'Cor'
        ListField = 'NOMECOR'
        ListSource = DsGradesCors
        TabOrder = 2
        dmkEditCB = EdCor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCor: TdmkEditCB
        Left = 474
        Top = 1
        Width = 35
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCor
        IgnoraDBLookupComboBox = False
      end
      object EdTam: TdmkEditCB
        Left = 632
        Top = 1
        Width = 35
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBTam
        IgnoraDBLookupComboBox = False
      end
      object CBTam: TdmkDBLookupComboBox
        Left = 522
        Top = 25
        Width = 145
        Height = 21
        KeyField = 'Tam'
        ListField = 'NOMETAM'
        ListSource = DsGradesTams
        TabOrder = 5
        dmkEditCB = EdTam
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
    object TabSheet2: TTabSheet
      Caption = 'Kits'
      ImageIndex = 1
      object Label34: TLabel
        Left = 6
        Top = 9
        Width = 15
        Height = 13
        Caption = 'Kit:'
      end
      object EdKit: TdmkEditCB
        Left = 6
        Top = 25
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBKit
        IgnoraDBLookupComboBox = False
      end
      object CBKit: TdmkDBLookupComboBox
        Left = 65
        Top = 25
        Width = 607
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGradeK
        TabOrder = 1
        dmkEditCB = EdKit
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 328
    Top = 9
  end
  object QrGrades: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGradesBeforeClose
    AfterScroll = QrGradesAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT pro.Codigo, gra.CodUsu, gra.Nome'
      'FROM produtos pro'
      'LEFT JOIN grades gra ON gra.Codigo=pro.Codigo'
      'WHERE pro.Ativo = 1'
      'ORDER BY Nome')
    Left = 356
    Top = 9
    object QrGradesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGradesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsGrades: TDataSource
    DataSet = QrGrades
    Left = 384
    Top = 9
  end
  object DsGradesCors: TDataSource
    DataSet = QrGradesCors
    Left = 440
    Top = 9
  end
  object QrGradesCors: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cor.Nome NOMECOR, grc.* '
      'FROM gradescors grc'
      'LEFT JOIN cores cor ON cor.Codigo=grc.Cor'
      'WHERE grc.Codigo =:P0'
      'ORDER BY Controle')
    Left = 412
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesCorsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'cores.Nome'
      Size = 30
    end
    object QrGradesCorsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'gradescors.Codigo'
      Required = True
    end
    object QrGradesCorsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'gradescors.Controle'
      Required = True
    end
    object QrGradesCorsCor: TIntegerField
      FieldName = 'Cor'
      Origin = 'gradescors.Cor'
      Required = True
    end
    object QrGradesCorsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'gradescors.Lk'
    end
    object QrGradesCorsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'gradescors.DataCad'
    end
    object QrGradesCorsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'gradescors.DataAlt'
    end
    object QrGradesCorsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'gradescors.UserCad'
    end
    object QrGradesCorsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'gradescors.UserAlt'
    end
    object QrGradesCorsAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'gradescors.AlterWeb'
      Required = True
    end
    object QrGradesCorsAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'gradescors.Ativo'
      Required = True
    end
  end
  object DsGradesTams: TDataSource
    DataSet = QrGradesTams
    Left = 496
    Top = 9
  end
  object QrGradesTams: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tam.Nome NOMETAM, grt.* '
      'FROM gradestams grt'
      'LEFT JOIN tamanhos tam ON tam.Codigo=grt.Tam'
      'WHERE grt.Codigo =:P0'
      'ORDER BY Controle')
    Left = 468
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesTamsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'gradestams.Codigo'
    end
    object QrGradesTamsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'gradestams.Controle'
    end
    object QrGradesTamsTam: TIntegerField
      FieldName = 'Tam'
      Origin = 'gradestams.Tam'
    end
    object QrGradesTamsLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'gradestams.Lk'
    end
    object QrGradesTamsDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'gradestams.DataCad'
    end
    object QrGradesTamsDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'gradestams.DataAlt'
    end
    object QrGradesTamsUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'gradestams.UserCad'
    end
    object QrGradesTamsUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'gradestams.UserAlt'
    end
    object QrGradesTamsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'tamanhos.Nome'
      Size = 5
    end
  end
  object QrGradeK: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gradek'
      'WHERE Ativo = 1')
    Left = 524
    Top = 9
    object QrGradeKCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'gradek.Codigo'
    end
    object QrGradeKNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'gradek.Nome'
      Size = 100
    end
  end
  object DsGradeK: TDataSource
    DataSet = QrGradeK
    Left = 552
    Top = 9
  end
end
