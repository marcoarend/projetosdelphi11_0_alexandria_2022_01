object FmPromoWeb: TFmPromoWeb
  Left = 339
  Top = 185
  Caption = 'CAD-PROMO-002 :: Importa'#231#227'o de promotores WEB'
  ClientHeight = 608
  ClientWidth = 965
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelConfirma: TPanel
    Left = 0
    Top = 549
    Width = 965
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 25
      Top = 5
      Width = 110
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 827
      Top = 1
      Width = 137
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 965
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Importa'#231#227'o de promotores WEB'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 963
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 965
    Height = 490
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 0
    object DBGrid1: TDBGrid
      Left = 1
      Top = 1
      Width = 963
      Height = 488
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      DataSource = DsTmpProm
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'Codigo'
          Title.Caption = 'ID'
          Width = 75
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Nome'
          Width = 350
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'CPF'
          Width = 150
          Visible = True
        end>
    end
  end
  object QrPromoWEB: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT Id, Nome, Cpf'
      'FROM intranet_usuarios'
      'WHERE Nivel IN (1, 2)'
      'ORDER BY Nome')
    Left = 15
    Top = 9
    object QrPromoWEBNome: TWideStringField
      FieldName = 'Nome'
      Size = 250
    end
    object QrPromoWEBCpf: TWideStringField
      FieldName = 'Cpf'
      Size = 250
    end
    object QrPromoWEBId: TWordField
      FieldName = 'Id'
      Required = True
    end
  end
  object DsPromoWEB: TDataSource
    DataSet = QrPromoWEB
    Left = 43
    Top = 9
  end
  object QrTmpProm: TmySQLQuery
    Database = DModG.MyPID_DB
    SQL.Strings = (
      'SELECT Codigo, Nome, CPF'
      'FROM promweb')
    Left = 71
    Top = 9
    object QrTmpPromCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrTmpPromNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
    object QrTmpPromCPF: TWideStringField
      FieldName = 'CPF'
      Size = 18
    end
  end
  object DsTmpProm: TDataSource
    DataSet = QrTmpProm
    Left = 99
    Top = 9
  end
end
