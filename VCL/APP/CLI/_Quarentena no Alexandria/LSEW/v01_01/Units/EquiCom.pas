unit EquiCom;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, Menus, Grids,
  DBGrids, dmkDBGridDAC, dmkDBGrid, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  dmkLabel, frxClass, frxDBSet, dmkGeral, Variants, dmkRadioGroup, ComCtrls,
  dmkPermissoes, UnDmkProcFunc, UnDmkEnums;

type
  TFmEquiCom = class(TForm)
    PainelDados: TPanel;
    DsEquiCom: TDataSource;
    QrEquiCom: TmySQLQuery;
    PainelTitulo: TPanel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtEquiComIts: TBitBtn;
    BtEquiCom: TBitBtn;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    EdNome: TEdit;
    PMEquiCom: TPopupMenu;
    PainelItens: TPanel;
    Panel6: TPanel;
    BitBtn1: TBitBtn;
    Panel7: TPanel;
    BitBtn2: TBitBtn;
    Panel8: TPanel;
    Panel9: TPanel;
    Label7: TLabel;
    DBEdit19: TDBEdit;
    DBEdit20: TDBEdit;
    Label8: TLabel;
    Panel10: TPanel;
    Label11: TLabel;
    QrEquiComIts: TmySQLQuery;
    DsEquiComIts: TDataSource;
    EdProduto: TdmkEditCB;
    CBProduto: TdmkDBLookupComboBox;
    PMEquiComIts: TPopupMenu;
    QrProdutos: TmySQLQuery;
    DsProdutos: TDataSource;
    QrProdutosCodigo: TIntegerField;
    LaTipo: TdmkLabel;
    QrSoma: TmySQLQuery;
    QrSomaValor: TFloatField;
    frxPrecoCurso: TfrxReport;
    frxDsEquiComIts: TfrxDBDataset;
    frxDsEquiCom: TfrxDBDataset;
    QrEquiComCodigo: TIntegerField;
    QrEquiComNome: TWideStringField;
    Incluinovoroldecomisses1: TMenuItem;
    Excluiitemdecomissoselecionado1: TMenuItem;
    Alteraitemdecomissoselecionado1: TMenuItem;
    Incluiitemdecomisso1: TMenuItem;
    Excluiroldecomissesatual1: TMenuItem;
    Alteraroldecomissesatual1: TMenuItem;
    QrEquiComItsCodigo: TIntegerField;
    QrEquiComItsControle: TIntegerField;
    QrEquiComItsProduto: TIntegerField;
    EdPerceProd: TdmkEdit;
    LaComisQtd: TLabel;
    QrProdutosNome: TWideStringField;
    QrEquiComItsNOMEMEPROD: TWideStringField;
    SpeedButton5: TSpeedButton;
    PainelContas: TPanel;
    Panel11: TPanel;
    BitBtn3: TBitBtn;
    Panel12: TPanel;
    BitBtn4: TBitBtn;
    Panel13: TPanel;
    PainelEditCta: TPanel;
    Panel15: TPanel;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    BtRateio: TBitBtn;
    PainelGrades: TPanel;
    dmkDBGrid1: TdmkDBGrid;
    PMRateio: TPopupMenu;
    Incluicontaderateio1: TMenuItem;
    Alterarateioselecionado1: TMenuItem;
    Excluirateios1: TMenuItem;
    QrEquiComItsTotCre: TFloatField;
    QrEquiComItsTotDeb: TFloatField;
    QrEquiComItsPerceProd: TFloatField;
    QrTotCom: TmySQLQuery;
    QrTotComPerceGene: TFloatField;
    QrTotCre: TmySQLQuery;
    QrTotDeb: TmySQLQuery;
    QrTotCrePerceGene: TFloatField;
    QrTotDebPerceGene: TFloatField;
    Panel4: TPanel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit22: TDBEdit;
    DBEdit21: TDBEdit;
    DBEdit2: TDBEdit;
    Label13: TLabel;
    DBEdit1: TDBEdit;
    Panel14: TPanel;
    RGCredDeb: TRadioGroup;
    Label3: TLabel;
    EdGenero: TdmkEditCB;
    CBGenero: TdmkDBLookupComboBox;
    SpeedButton6: TSpeedButton;
    EdPerceGene: TdmkEdit;
    Label4: TLabel;
    RGRateio: TRadioGroup;
    CkEhComProf: TCheckBox;
    CkContinuar: TCheckBox;
    QrEquiConCre: TmySQLQuery;
    DsEquiConCre: TDataSource;
    QrEquiConDeb: TmySQLQuery;
    DsEquiConDeb: TDataSource;
    QrContasDebito: TWideStringField;
    QrContasCredito: TWideStringField;
    Panel18: TPanel;
    QrEquiComItsParDebFix: TFloatField;
    QrEquiComItsParDebVar: TFloatField;
    QrPartial: TmySQLQuery;
    QrPartialPerceGene: TFloatField;
    QrEquiComItsParCreFix: TFloatField;
    QrEquiComItsParCreVar: TFloatField;
    Panel20: TPanel;
    Panel21: TPanel;
    StaticText3: TStaticText;
    dmkDBGrid2: TdmkDBGrid;
    Splitter1: TSplitter;
    Panel22: TPanel;
    StaticText4: TStaticText;
    dmkDBGrid5: TdmkDBGrid;
    Panel17: TPanel;
    Panel23: TPanel;
    StaticText2: TStaticText;
    dmkDBGrid4: TdmkDBGrid;
    Panel16: TPanel;
    StaticText1: TStaticText;
    dmkDBGrid3: TdmkDBGrid;
    Splitter2: TSplitter;
    QrEquiConCreNOMECREDDEBRAT: TWideStringField;
    QrEquiConCreNOMECONTA: TWideStringField;
    QrEquiConCreCodigo: TIntegerField;
    QrEquiConCreControle: TIntegerField;
    QrEquiConCreConta: TIntegerField;
    QrEquiConCreGenero: TIntegerField;
    QrEquiConCreCredDeb: TSmallintField;
    QrEquiConCrePerceGene: TFloatField;
    QrEquiConCreEhComProf: TSmallintField;
    QrEquiConCreRateio: TSmallintField;
    QrEquiConDebNOMECREDDEBRAT: TWideStringField;
    QrEquiConDebNOMECONTA: TWideStringField;
    QrEquiConDebCodigo: TIntegerField;
    QrEquiConDebControle: TIntegerField;
    QrEquiConDebConta: TIntegerField;
    QrEquiConDebGenero: TIntegerField;
    QrEquiConDebCredDeb: TSmallintField;
    QrEquiConDebPerceGene: TFloatField;
    QrEquiConDebEhComProf: TSmallintField;
    QrEquiConDebRateio: TSmallintField;
    Crdito1: TMenuItem;
    Dbito1: TMenuItem;
    Crdito2: TMenuItem;
    Dbito2: TMenuItem;
    GroupBox1: TGroupBox;
    DBEdit5: TDBEdit;
    Label15: TLabel;
    DBEdit6: TDBEdit;
    Label16: TLabel;
    Label17: TLabel;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    Label18: TLabel;
    CkCompoe: TCheckBox;
    QrEquiComItsCompoe: TSmallintField;
    QrEquiComItsC: TWideStringField;
    QrEquiComItsParDebPos: TFloatField;
    QrEquiComItsParCrePos: TFloatField;
    Label21: TLabel;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    Label22: TLabel;
    DBEdit13: TDBEdit;
    Label23: TLabel;
    Label24: TLabel;
    DBEdit14: TDBEdit;
    DBEdit9: TDBEdit;
    Label19: TLabel;
    Label20: TLabel;
    DBEdit10: TDBEdit;
    DBEdit15: TDBEdit;
    Label25: TLabel;
    Label26: TLabel;
    DBEdit16: TDBEdit;
    DBEdit3: TDBEdit;
    Label12: TLabel;
    Label14: TLabel;
    DBEdit4: TDBEdit;
    DBEdit17: TDBEdit;
    Label27: TLabel;
    Label28: TLabel;
    DBEdit18: TDBEdit;
    RGCartIdx: TdmkRadioGroup;
    RichEdit1: TRichEdit;
    QrEquiConCreCartIdx: TIntegerField;
    QrEquiConDebCartIdx: TIntegerField;
    N1: TMenuItem;
    DuplicaRolAtua1: TMenuItem;
    QrEquiConTrf: TmySQLQuery;
    QrEquiConTrfNOMECREDDEBRAT: TWideStringField;
    QrEquiConTrfNOMECONTA: TWideStringField;
    QrEquiConTrfCodigo: TIntegerField;
    QrEquiConTrfControle: TIntegerField;
    QrEquiConTrfConta: TIntegerField;
    QrEquiConTrfGenero: TIntegerField;
    QrEquiConTrfPerceGene: TFloatField;
    QrEquiConTrfCredDeb: TSmallintField;
    QrEquiConTrfEhComProf: TSmallintField;
    QrEquiConTrfRateio: TSmallintField;
    QrEquiConTrfCartIdx: TSmallintField;
    EdMinQtde: TdmkEdit;
    Label29: TLabel;
    Label30: TLabel;
    QrEquiConCreMinQtde: TFloatField;
    QrEquiConDebMinQtde: TFloatField;
    QrEquiConTrfMinQtde: TFloatField;
    Panel19: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Label31: TLabel;
    QrGrades: TmySQLQuery;
    DsGrades: TDataSource;
    EdGradeComis: TdmkEditCB;
    CBGradeComis: TdmkDBLookupComboBox;
    QrEquiConCreGradeComis: TIntegerField;
    QrEquiConDebGradeComis: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    frxPrecoCurso2: TfrxReport;
    frxDsEquiConDeb: TfrxDBDataset;
    frxDsEquiConCre: TfrxDBDataset;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtEquiComClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrEquiComAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrEquiComBeforeOpen(DataSet: TDataSet);
    procedure Crianovogrupo1Click(Sender: TObject);
    procedure Alteragrupoatual1Click(Sender: TObject);
    procedure Excluigrupoatual1Click(Sender: TObject);
    procedure BtEquiComItsClick(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn1Click(Sender: TObject);
    procedure QrEquiComAfterScroll(DataSet: TDataSet);
    procedure Incluiitemdereceita1Click(Sender: TObject);
    procedure Alteraitemdereceitaatual1Click(Sender: TObject);
    procedure PMEquiComPopup(Sender: TObject);
    procedure Excluiitemdereceitaatual1Click(Sender: TObject);
    procedure PMEquiComItsPopup(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure SpeedButton5Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure QrEquiComItsAfterScroll(DataSet: TDataSet);
    procedure QrEquiComItsBeforeClose(DataSet: TDataSet);
    procedure QrEquiComBeforeClose(DataSet: TDataSet);
    procedure BtRateioClick(Sender: TObject);
    procedure Incluicontaderateio1Click(Sender: TObject);
    procedure QrEquiComItsAfterOpen(DataSet: TDataSet);
    procedure SpeedButton6Click(Sender: TObject);
    procedure RGCredDebClick(Sender: TObject);
    procedure Crdito1Click(Sender: TObject);
    procedure Dbito1Click(Sender: TObject);
    procedure Crdito2Click(Sender: TObject);
    procedure Dbito2Click(Sender: TObject);
    procedure DuplicaRolAtua1Click(Sender: TObject);
    procedure LocCod(Atual, Codigo: Integer);
  private
    FConta: Integer;
    //
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure Va(Para: TVaiPara);
    //
    procedure ReopenEquiComIts(Controle: Integer);
    procedure ReopenEquiConTrf(Conta: Integer);
    procedure RecalculaTotal(Curso: Integer);
    procedure RecalculaItem(Controle, CredDeb: Integer);
  public
    { Public declarations }
  end;

var
  FmEquiCom: TFmEquiCom;
const
  FFormatFloat = '00000';

implementation

uses Module, ModuleGeral, UnFinanceiro, Principal, MyDBCheck, UnMyObjects,
  UnFinanceiroJan;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmEquiCom.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmEquiCom.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrEquiComCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmEquiCom.DefParams;
begin
  VAR_GOTOTABELA := 'equicom';
  VAR_GOTOMYSQLTABLE := QrEquiCom;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM equicom');
  VAR_SQLx.Add('WHERE Codigo <> 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmEquiCom.DuplicaRolAtua1Click(Sender: TObject);
var
  Codigo, Controle, Conta: Integer;
  Nome: String;
begin
  if Geral.MB_Pergunta('Tem certeza que deseja duplicar todo rol atual?')= ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      Nome := QrEquiComNome.Value + ' [c�pia]';
      Codigo := UMyMod.BuscaEmLivreY_Def('equicom', 'codigo', stIns, 0);
      if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'equicom', False,
      ['Nome'], ['Codigo'], [Nome], [Codigo], True) then
      begin
        QrEquiComIts.First;
        while not QrEquiComIts.Eof do
        begin
          Controle := UMyMod.BuscaEmLivreY_Def('equicomits', 'controle', stIns, 0);
          if UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'equicomits', False, [
            'Codigo', 'Produto', 'PerceProd',
            'TotCre', 'TotDeb', 'ParDebFix',
            'ParDebVar', 'ParCreFix', 'ParCreVar',
            'Compoe', 'ParDebPos', 'ParCrePos'
          ], ['Controle'], [
            Codigo, QrEquiComItsProduto.Value, QrEquiComItsPerceProd.Value,
            QrEquiComItsTotCre.Value, QrEquiComItsTotDeb.Value,
            QrEquiComItsParDebFix.Value, QrEquiComItsParDebVar.Value,
            QrEquiComItsParCreFix.Value, QrEquiComItsParCreVar.Value,
            QrEquiComItsCompoe.Value, QrEquiComItsParDebPos.Value,
            QrEquiComItsParCrePos.Value
          ], [Controle], True) then
          begin
            //
            QrEquiConTrf.Close;
            QrEquiConTrf.Params[0].AsInteger := QrEquiComItsControle.Value;
            QrEquiConTrf.Open;
            QrEquiConTrf.First;
            while not QrEquiConTrf.Eof do
            begin
              Conta := UMyMod.BuscaEmLivreY_Def('equicontrf', 'conta', stIns, 0);
              UMyMod.SQLInsUpd(Dmod.QrUpd, stIns, 'equicontrf', False, [
              'Codigo', 'Controle', 'Genero',
              'PerceGene', 'CredDeb', 'EhComProf',
              'Rateio', 'CartIdx', 'MinQtde'], [
              'Conta'], [
              Codigo, Controle,
              QrEquiConTrfGenero.Value, QrEquiConTrfPerceGene.Value,
              QrEquiConTrfCredDeb.Value, QrEquiConTrfEhComProf.Value,
              QrEquiConTrfRateio.Value, QrEquiConTrfCartIdx.Value,
              QrEquiConTrfMinQtde.Value], [
              Conta], True);
              //
              QrEquiConTrf.Next;
            end;
          end;
          QrEquiComIts.Next;
        end;
      end;
      QrEquiConTrf.Close;
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmEquiCom.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible := True;
      PainelDados.Visible    := True;
      PainelEdita.Visible    := False;
      PainelItens.Visible    := False;
      PainelContas.Visible   := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.Text   := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text     := '';
      end else begin
        EdCodigo.Text   := DBEdCodigo.Text;
        EdNome.Text     := DBEdNome.Text;
      end;
      EdNome.SetFocus;
    end;
    2:
    begin
      PainelItens.Visible    := True;
      PainelDados.Visible    := False;
      PainelControle.Visible := False;
      //
      QrProdutos.Close;
      QrProdutos.SQL.Clear;
      QrProdutos.SQL.Add('SELECT *');
      QrProdutos.SQL.Add('FROM grades');
      QrProdutos.SQL.Add('WHERE Ativo = 1 ');
      QrProdutos.SQL.Add('AND Codigo NOT IN (');
      QrProdutos.SQL.Add('  SELECT Produto');
      QrProdutos.SQL.Add('  FROM equicomits');
      QrProdutos.SQL.Add('  WHERE Codigo=' + FormatFloat('0', QrEquiComCodigo.Value));
      QrProdutos.SQL.Add(')');
      if SQLType = stUpd then
        QrProdutos.SQL.Add('OR Codigo=' + FormatFloat('0', QrEquiComItsProduto.Value));
      QrProdutos.SQL.Add('ORDER BY Nome');
      QrProdutos.Open;
      if SQLType = stIns then
      begin
        EdProduto.ValueVariant   := 0;
        CBProduto.KeyValue       := Null;
        EdPerceProd.ValueVariant := '';
        CkCompoe.Checked         := False;
      end else
      begin
        EdProduto.ValueVariant   := QrEquiComItsProduto.Value;
        CBProduto.KeyValue       := QrEquiComItsProduto.Value;
        EdPerceProd.ValueVariant := QrEquiComItsPerceProd.Value;
        CkCompoe.Checked         := Geral.IntToBool(QrEquiComItsCompoe.Value);
      end;
      //
      EdProduto.SetFocus;
    end;
    3..4:
    begin
      PainelContas.Visible       := True;
      PainelDados.Visible        := False;
      PainelControle.Visible     := False;
      //
      if SQLType = stIns then
      begin
        EdGenero.Text             := '';
        CBGenero.KeyValue         := Null;
        EdPerceGene.Text          := '';
        CkContinuar.Visible       := True;
        CkEhComProf.Checked       := False;
        RGCartIdx.ItemIndex       := 0;
        RGRateio.ItemIndex        := 0;
        RGCredDeb.ItemIndex       := 0; // deve ser por �ltimo
        EdMinQtde.ValueVariant    := 0;
        EdGradeComis.ValueVariant := 0;
        CBGradeComis.KeyValue     := Null;
      end else begin
        if Mostra = 3 then
        begin
          FConta := QrEquiConCreConta.Value;
          //
          EdGradeComis.ValueVariant := QrEquiConCreGradeComis.Value;
          CBGradeComis.KeyValue     := QrEquiConCreGradeComis.Value;
          EdGenero.ValueVariant     := QrEquiConCreGenero.Value;
          CBGenero.KeyValue         := QrEquiConCreGenero.Value;
          EdPerceGene.ValueVariant  := QrEquiConCrePerceGene.Value;
          CkEhComProf.Checked       := Geral.IntToBool(QrEquiConCreEhComProf.Value);
          RGCartIdx.ItemIndex       := QrEquiConCreCartIdx.Value;
          RGRateio.ItemIndex        := QrEquiConCreRateio.Value;
          RGCredDeb.ItemIndex       := QrEquiConCreCredDeb.Value;// Deve ser por �ltimo
          EdMinQtde.ValueVariant    := QrEquiConCreMinQtde.Value;
        end else if Mostra = 4 then
        begin
          FConta := QrEquiConDebConta.Value;
          //
          EdGradeComis.ValueVariant := QrEquiConDebGradeComis.Value;
          CBGradeComis.KeyValue     := QrEquiConDebGradeComis.Value;
          EdGenero.ValueVariant     := QrEquiConDebGenero.Value;
          CBGenero.KeyValue         := QrEquiConDebGenero.Value;
          EdPerceGene.ValueVariant  := QrEquiConDebPerceGene.Value;
          CkEhComProf.Checked       := Geral.IntToBool(QrEquiConDebEhComProf.Value);
          RGCartIdx.ItemIndex       := QrEquiConDebCartIdx.Value;
          RGRateio.ItemIndex        := QrEquiConDebRateio.Value;
          RGCredDeb.ItemIndex       := QrEquiConDebCredDeb.Value;// Deve ser por �ltimo
          EdMinQtde.ValueVariant    := QrEquiConDebMinQtde.Value;
        end;
        CkContinuar.Visible         := False;
      end;
      RGCredDeb.SetFocus;
    end;
    else
      Geral.MB_Aviso('A��o de Inclus�o/altera��o n�o definida!');
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmEquiCom.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmEquiCom.AlteraRegistro;
var
  EquiCom : Integer;
begin
  EquiCom := QrEquiComCodigo.Value;
  if not UMyMod.SelLockY(EquiCom, Dmod.MyDB, 'equicom', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(EquiCom, Dmod.MyDB, 'equicom', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmEquiCom.IncluiRegistro;
var
  Cursor : TCursor;
  EquiCom : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    EquiCom := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'livres', 'controle',
    'equicom', 'equicom', 'codigo');
    if Length(FormatFloat(FFormatFloat, EquiCom))>Length(FFormatFloat) then
    begin
      Geral.MB_Erro('Inclus�o cancelada. Limite de cadastros extrapolado');
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, EquiCom);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmEquiCom.QueryPrincipalAfterOpen;
begin
end;

procedure TFmEquiCom.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmEquiCom.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmEquiCom.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmEquiCom.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmEquiCom.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmEquiCom.BtEquiComClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEquiCom, BtEquiCom);
end;

procedure TFmEquiCom.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrEquiComCodigo.Value;
  Close;
end;

procedure TFmEquiCom.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  //
  if MyObjects.FIC(Length(Nome) = 0, EdNome, 'Defina uma descri��o!') then Exit;
  //
  Codigo := Geral.IMV(EdCodigo.Text);
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'equicom', False,
  [
    'Nome'
  ], ['Codigo'],
  [
  EdNome.Text
  ], [Codigo]) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'equicom', 'Codigo');
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmEquiCom.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.Caption = CO_INCLUSAO then UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', 'equicom', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'equicom', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'equicom', 'Codigo');
end;

procedure TFmEquiCom.FormCreate(Sender: TObject);
begin
  Panel13.Align       := alClient;
  PainelEdita.Align   := alClient;
  PainelDados.Align   := alClient;
  PainelEdit.Align    := alClient;
  PainelGrades.Align  := alClient;
  PainelEditCta.Align := alClient;
  CriaOForm;
  //
  UMyMod.AbreQuery(QrProdutos, Dmod.MyDB);
  UMyMod.AbreQuery(QrContas, Dmod.MyDB);
  UMyMod.AbreQuery(QrGrades, Dmod.MyDB);
  //
  if UpperCase(Application.Title) = 'LESEW' then
  begin
    RGCartIdx.Items[0] := 'Professor';
    RGCartIdx.Items.Add('Promotor');
    RGCartIdx.Columns := 2;
    RGCartIdx.Visible := True;
  end;
end;

procedure TFmEquiCom.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrEquiComCodigo.Value,LaRegistro.Caption);
end;

procedure TFmEquiCom.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmEquiCom.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmEquiCom.QrEquiComAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
  BtEquiComIts.Enabled := QrEquiCom.RecordCount > 0;
end;

procedure TFmEquiCom.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmEquiCom.SbQueryClick(Sender: TObject);
begin
  LocCod(QrEquiComCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'equicom', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmEquiCom.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmEquiCom.QrEquiComBeforeOpen(DataSet: TDataSet);
begin
  QrEquiComCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmEquiCom.Crianovogrupo1Click(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmEquiCom.Alteragrupoatual1Click(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmEquiCom.Excluigrupoatual1Click(Sender: TObject);
begin
  UMyMod.SQLDel1(Dmod.QrUpd, QrEquiCom, 'equicom', 'Codigo',
  QrEquiComCodigo.Value, True, 'Confirma a exclus�o do curso "' +
  QrEquiComNome.Value + '"?', True);
end;

procedure TFmEquiCom.BtEquiComItsClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMEquiComIts, BtEquiComIts);
end;

procedure TFmEquiCom.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmEquiCom.BitBtn1Click(Sender: TObject);
var
  Produto, Codigo, Controle, Compoe: Integer;
begin
  Codigo  := QrEquiComCodigo.Value;
  Produto := Geral.IMV(EdProduto.Text);
  Compoe  := Geral.BoolToInt(CkCompoe.Checked);
  //
  if MyObjects.FIC(Codigo = 0, nil, 'Defina o curso!') then Exit;
  if MyObjects.FIC(Produto = 0, EdProduto, 'Defina a mercadoria!') then Exit;
  //
  Controle := UMyMod.BuscaEmLivreY_Def('equicomits', 'controle',
    LaTipo.SQLType, QrEquiComItsControle.Value);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'equicomits', False,
  [
    'Codigo', 'Produto', 'PerceProd', 'Compoe'
  ], ['Controle'],
  [
  Codigo, Produto, EdPerceProd.ValueVariant, Compoe
  ], [Controle], True) then
  begin
    MostraEdicao(0, stLok, 0);
    RecalculaTotal(Codigo);
    ReopenEquiComIts(Controle);
  end;
end;

procedure TFmEquiCom.QrEquiComAfterScroll(DataSet: TDataSet);
begin
  ReopenEquiComIts(0);
end;

procedure TFmEquiCom.ReopenEquiComIts(Controle: Integer);
begin
  QrEquiComIts.Close;
  QrEquiComIts.Params[0].AsInteger := QrEquiComCodigo.Value;
  QrEquiComIts.Open;
  //
  if Controle > 0 then
    QrEquiComIts.Locate('Controle', Controle, []);
end;

procedure TFmEquiCom.Incluiitemdereceita1Click(Sender: TObject);
begin
  MostraEdicao(2, stIns, 0);
end;

procedure TFmEquiCom.Alteraitemdereceitaatual1Click(Sender: TObject);
begin
  MostraEdicao(2, stUpd, 0);
end;

procedure TFmEquiCom.Excluiitemdereceitaatual1Click(Sender: TObject);
var
  Prox: Integer;
begin
  if (QrEquiConCre.State <> dsInactive) and (QrEquiConCre.RecordCount > 0) then
  begin
    Geral.MB_Aviso('O item atual possui contas de rateio do tipo cr�dito!' +
      sLineBreak + 'Exclua as contas e tente novamente!');
    Exit;
  end;
  if (QrEquiConDeb.State <> dsInactive) and (QrEquiConDeb.RecordCount > 0) then
  begin
    Geral.MB_Aviso('O item atual possui contas de rateio do tipo d�bito!' +
      sLineBreak + 'Exclua as contas e tente novamente!');
    Exit;
  end;
  //
  if Geral.MB_Pergunta('Confirma a retirada o item selecionado?') = ID_YES then
  begin
    Screen.Cursor := crHourGlass;
    try
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('DELETE FROM equicomits WHERE Controle=:P0');
      Dmod.QrUpd.Params[0].AsInteger := QrEquiComItsControle.Value;
      Dmod.QrUpd.ExecSQL;
      //
      RecalculaTotal(QrEquiComItsCodigo.Value);
      Prox := UMyMod.ProximoRegistro(QrEquiComIts, 'Controle',
        QrEquiComItsControle.Value);
      ReopenEquiComIts(Prox);
    finally
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TFmEquiCom.PMEquiComPopup(Sender: TObject);
begin
  UMyMod.HabilitaMenuItemInt([Alteraroldecomissesatual1, Excluiroldecomissesatual1],
    QrEquiCom, 'Codigo', 1, 0);
  Excluiroldecomissesatual1.Enabled :=
    Excluiroldecomissesatual1.Enabled and (QrEquiComIts.RecordCount = 0);
end;

procedure TFmEquiCom.PMEquiComItsPopup(Sender: TObject);
begin
  UMyMod.HabilitaMenuItemInt([Incluiitemdecomisso1],
    QrEquiCom, 'Codigo', 1, 0);
  UMyMod.HabilitaMenuItemInt([Alteraitemdecomissoselecionado1, Excluiitemdecomissoselecionado1],
    QrEquiComIts, 'Controle', 1, 0);
end;

procedure TFmEquiCom.RecalculaTotal(Curso: Integer);
begin
  Exit;

  QrSoma.Close;
  QrSoma.Params[0].AsInteger := Curso;
  QrSoma.Open;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE equicom SET Valor=:P0 WHERE Codigo=:P1');
  Dmod.QrUpd.Params[00].AsFloat   := QrSomaValor.Value;
  Dmod.QrUpd.Params[01].AsInteger := Curso;
  Dmod.QrUpd.ExecSQL;
  //
  LocCod(Curso, Curso);
end;

procedure TFmEquiCom.SbImprimeClick(Sender: TObject);
begin
    MyObjects.frxDefineDataSets(frxPrecoCurso2, [
      DmodG.frxDsMaster,
      frxDsEquiCom,
      frxDsEquiComIts,
      frxDsEquiConCre,
      frxDsEquiConDeb
      ]);
  //
  MyObjects.frxMostra(frxPrecoCurso2, 'Composi��o de pre�o de curso');
end;

procedure TFmEquiCom.SpeedButton5Click(Sender: TObject);
begin
  VAR_COD := EdProduto.ValueVariant;
  //
  FmPrincipal.CadastroDeGrades();
  //
  if VAR_COD <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdProduto, CBProduto, QrProdutos, VAR_COD);
    //
    EdProduto.SetFocus;
  end;
end;

procedure TFmEquiCom.BitBtn3Click(Sender: TObject);
var
  EhComProf, Codigo, Controle, Conta, Genero: Integer;
begin
  Codigo    := QrEquiComCodigo.Value;
  Controle  := QrEquiComItsControle.Value;
  Genero    := Geral.IMV(EdGenero.Text);
  EhComProf := Geral.BoolToInt(CkEhComProf.Checked);
  //
  if MyObjects.FIC(Genero = 0, EdGenero, 'Defina a conta do plano de contas!') then Exit;
  //
  if ((QrContasCredito.Value <> 'V') and (RGCredDeb.ItemIndex = 1))
  or ((QrContasDebito.Value <> 'V')  and (RGCredDeb.ItemIndex = 2)) then
  begin
    Geral.MB_Aviso('A forma de rateio difere do cadastro da ' +
      'conta do plano de contas!' + sLineBreak + 'Voc� est� tentando usar uma conta de ' +
      'cr�dito para d�bito ou vice-versa!');
    EdGenero.SetFocus;
    Exit;
  end;
  //
  if MyObjects.FIC(RGCredDeb.ItemIndex = 0, RGCredDeb, 'Defina a orienta��o do rateio!') then Exit;
  if MyObjects.FIC(RGRateio.ItemIndex = 0, RGRateio, 'Defina a forma de rateio!') then Exit;
  //
  if (EhComProf = 1) and (RGCredDeb.ItemIndex = 1) then
  begin
    Geral.MB_Aviso('A comiss�o de professor n�o pode ser um cr�dito!');
    CkEhComProf.SetFocus;
    Exit;
  end;
  //
  if (RGRateio.ItemIndex = 3) and (RGCredDeb.ItemIndex = 2) then
  begin
    Geral.MB_Aviso('O rateio de d�bito p�s-fixado n�o est� ' +
      'previsto neste aplicativo!');
    RGRateio.SetFocus;
    Exit;
  end;
  //
  Conta := UMyMod.BuscaEmLivreY_Def('equicontrf', 'Conta',
    LaTipo.SQLType, FConta);
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'equicontrf', False,
  [
    'Codigo', 'Controle', 'Genero',
    'PerceGene', 'CredDeb', 'EhComProf',
    'Rateio', 'CartIdx', 'MinQtde',
    'GradeComis'
  ], ['Conta'],
  [
  Codigo, Controle, Genero,
  EdPerceGene.ValueVariant, RGCredDeb.ItemIndex, EhComProf,
  RGRateio.ItemIndex, RGCartIdx.ItemIndex, Geral.DMV(EdMinQtde.Text),
  Geral.IMV(EdGradeComis.Text)
  ], [Conta], True) then
  begin
    MostraEdicao(0, stLok, 0);
    RecalculaItem(Controle, RGCredDeb.ItemIndex);
    RecalculaTotal(Codigo);
    ReopenEquiConTrf(Conta);
    if CkContinuar.Visible and CkContinuar.Checked then
      MostraEdicao(3, stIns, 0);
  end;
end;

procedure TFmEquiCom.QrEquiComItsAfterScroll(DataSet: TDataSet);
begin
  ReopenEquiConTrf(0);
end;

procedure TFmEquiCom.ReopenEquiConTrf(Conta: Integer);
begin
  QrEquiConCre.Close;
  QrEquiConCre.Params[0].AsInteger := QrEquiComItsControle.Value;
  QrEquiConCre.Open;
  //
  QrEquiConDeb.Close;
  QrEquiConDeb.Params[0].AsInteger := QrEquiComItsControle.Value;
  QrEquiConDeb.Open;
  //
  if Conta <> 0 then
  begin
    QrEquiConCre.Locate('Conta', Conta, []);
    QrEquiConDeb.Locate('Conta', Conta, []);
  end;
end;

procedure TFmEquiCom.QrEquiComItsBeforeClose(DataSet: TDataSet);
begin
  QrEquiConCre.Close;
  QrEquiConDeb.Close;
  BtRateio.Enabled := False;
end;

procedure TFmEquiCom.QrEquiComBeforeClose(DataSet: TDataSet);
begin
  QrEquiComIts.Close;
  BtEquiComIts.Enabled := False;
end;

procedure TFmEquiCom.BtRateioClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMRateio, BtRateio);
end;

procedure TFmEquiCom.Incluicontaderateio1Click(Sender: TObject);
begin
  MostraEdicao(3, stIns, 0);
end;

procedure TFmEquiCom.QrEquiComItsAfterOpen(DataSet: TDataSet);
begin
  BtRateio.Enabled := QrEquiComIts.RecordCount > 0;
end;

procedure TFmEquiCom.RecalculaItem(Controle, CredDeb: Integer);
  function Partial(Controle, CredDeb, Rateio: Integer): Double;
  begin
    QrPartial.Close;
    QrPartial.Params[00].AsInteger := CredDeb;
    QrPartial.Params[01].AsInteger := Rateio;
    QrPartial.Params[02].AsInteger := Controle;
    QrPartial.Open;
    //
    Result := QrPartialPerceGene.Value;
  end;
var
  ContaCre, ContaDeb: Integer;
  TotCre, TotDeb,
  ParDebFix, ParDebVar, ParCreFix, ParCreVar, ParCrePos, ParDebPos: Double;
begin
  QrTotCom.Close;
  QrTotCom.Params[0].AsInteger := Controle;
  QrTotCom.Open;
  //
  ParCreFix := Partial(Controle, 1, 1);
  ParCreVar := Partial(Controle, 1, 2);
  ParCrePos := Partial(Controle, 1, 3);
  ParDebFix := Partial(Controle, 2, 1);
  ParDebVar := Partial(Controle, 2, 2);
  ParDebPos := Partial(Controle, 2, 3);
  //
  //CreVar := ((100 - ParCreFix) * ParCreVar) / 100;
  TotCre := ParCreFix + ParCreVar + ParCrePos;
  //DebVar := ((100 - ParDebFix) * ParDebVar) / 100;
  TotDeb := ParDebFix + ParDebVar;
  //
  if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'equicomits', False,
  [
    'TotCre', 'TotDeb', 'PerceProd',
    'ParCreFix', 'ParCreVar', 'ParCrePos',
    'ParDebFix', 'ParDebVar', 'ParDebPos'
  ], ['Controle'],
  [
    TotCre, TotDeb, QrTotComPerceGene.Value,
    ParCreFix, ParCreVar, ParCrePos,
    ParDebFix, ParDebVar, ParDebPos
  ], [Controle], True) then
  begin
    ContaCre := QrEquiConCreConta.Value;
    ContaDeb := QrEquiCondebConta.Value;
    ReopenEquiComIts(Controle);
    QrEquiConCre.Locate('Conta', ContaCre, []);
    QrEquiConDeb.Locate('Conta', ContaDeb, []);
  end;
end;

procedure TFmEquiCom.SpeedButton6Click(Sender: TObject);
begin
  VAR_CONTA := EdGenero.ValueVariant;
  //
  FinanceiroJan.CadastroDeContas(EdGenero.ValueVariant);
  //
  if VAR_CONTA <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdGenero, CBGenero, QrContas, VAR_CADASTRO);
    //
    EdGenero.SetFocus;
  end;
end;

procedure TFmEquiCom.RGCredDebClick(Sender: TObject);
begin
  case RGCredDeb.ItemIndex of
    1:
    begin
      RGRateio.Enabled    := True;
      //
      CkEhComProf.Checked := False;
      CkEhComProf.Enabled := False;
    end;
    2:
    begin
      RGRateio.Enabled    := True;
      CkEhComProf.Enabled := True;
      //
      //RGRateio.ItemIndex  := 0;
      //RGRateio.Enabled    := False;
    end;
    else//0 e 1:
    begin
      RGRateio.ItemIndex  := 0;
      RGRateio.Enabled    := False;
      //
      CkEhComProf.Checked := False;
      CkEhComProf.Enabled := False;
    end;
  end;
  //
end;

procedure TFmEquiCom.Crdito1Click(Sender: TObject);
begin
  if (QrEquiConCre.State <> dsInactive) and (QrEquiConCre.RecordCount > 0) then
    MostraEdicao(3, stUpd, 0)
  else
    Geral.MB_Aviso('N�o h� itens para editar!');
end;

procedure TFmEquiCom.Dbito1Click(Sender: TObject);
begin
  if (QrEquiConDeb.State <> dsInactive) and (QrEquiConDeb.RecordCount > 0) then
    MostraEdicao(4, stUpd, 0)
  else
    Geral.MB_Aviso('N�o h� itens para editar!');
end;

procedure TFmEquiCom.Crdito2Click(Sender: TObject);
var
  Controle, CredDeb: Integer;
begin
  Controle := QrEquiConCreControle.Value;
  CredDeb  := QrEquiConCreCredDeb.Value;
  //
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEquiConCre, TDBGrid(dmkDBGrid2),
    'equicontrf', ['conta'], ['conta'], istPergunta, '');
  RecalculaItem(Controle, CredDeb);
end;

procedure TFmEquiCom.Dbito2Click(Sender: TObject);
var
  Controle, CredDeb: Integer;
begin
  Controle := QrEquiConDebControle.Value;
  CredDeb  := QrEquiConDebCredDeb.Value;
  //
  DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrEquiConDeb, TDBGrid(dmkDBGrid2),
    'equicontrf', ['conta'], ['conta'], istPergunta, '');
  RecalculaItem(Controle, CredDeb);
end;

end.



