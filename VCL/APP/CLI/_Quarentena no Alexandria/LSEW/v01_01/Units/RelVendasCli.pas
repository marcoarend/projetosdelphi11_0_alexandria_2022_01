unit RelVendasCli;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, UnInternalConsts, Db, MyDBCheck,
  mySQLDbTables, DBCtrls, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UMySQLModule, dmkLabel, ABSMain, Grids, DBGrids, dmkDBGrid, frxClass,
  frxDBSet, Mask, dmkDBEdit, dmkCheckGroup, CheckLst;

type
  TFmRelVendasCli = class(TForm)
    PainelConfirma: TPanel;
    BtPesquisa: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtImprime: TBitBtn;
    Panel3: TPanel;
    DBGrid1: TDBGrid;
    CkPeriodo: TRadioGroup;
    CkCliente: TRadioGroup;
    PnPeriodo: TPanel;
    EdDiasIni: TdmkEdit;
    LaIni: TLabel;
    EdDiasFin: TdmkEdit;
    LaFin: TLabel;
    TPDataFin: TDateTimePicker;
    TPDataIni: TDateTimePicker;
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CkPeriodoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FCodigo, FControle: Integer;
  end;

  var
  FmRelVendasCli: TFmRelVendasCli;

implementation

{$R *.DFM}

uses Module, MyListas, dmkGeral, ModuleGeral, UnMyObjects;

procedure TFmRelVendasCli.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmRelVendasCli.CkPeriodoClick(Sender: TObject);
begin
  PnPeriodo.Visible := CkPeriodo.ItemIndex > -1;
  case CkPeriodo.ItemIndex of
    0:
    begin
      LaIni.Top         := 8;
      LaFin.Top         := 8;
      EdDiasIni.Visible := True;
      EdDiasFin.Visible := True;
      TPDataIni.Visible := False;
      TPDataFin.Visible := False;
    end;
    1:
    begin
      LaIni.Top         := 34;
      LaFin.Top         := 34;
      EdDiasIni.Visible := False;
      EdDiasFin.Visible := False;
      TPDataIni.Visible := True;
      TPDataFin.Visible := True;
    end;
  end;
end;

procedure TFmRelVendasCli.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //
  CkCliente.SetFocus;
end;

procedure TFmRelVendasCli.FormCreate(Sender: TObject);
begin
  CkCliente.ItemIndex    := -1;
  CkPeriodo.ItemIndex    := -1;
  EdDiasIni.ValueVariant := 0;
  EdDiasFin.ValueVariant := 0;
  TPDataIni.DateTime     := Date;
  TPDataFin.DateTime     := Date;
end;

procedure TFmRelVendasCli.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.



