object FmMD5Fxa: TFmMD5Fxa
  Left = 339
  Top = 185
  Caption = 'CIC-CRYPT-002 :: Faixa de Senhas'
  ClientHeight = 219
  ClientWidth = 509
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 171
    Width = 509
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 20
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 397
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 509
    Height = 48
    Align = alTop
    Caption = 'Faixa de Senhas'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 425
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 788
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 426
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 620
      ExplicitTop = 2
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 509
    Height = 123
    Align = alClient
    TabOrder = 0
    object Label1: TLabel
      Left = 20
      Top = 8
      Width = 32
      Height = 13
      Caption = 'Grupo:'
      FocusControl = DBEdit1
    end
    object Label4: TLabel
      Left = 104
      Top = 8
      Width = 14
      Height = 13
      Caption = 'ID:'
    end
    object DBEdit1: TdmkDBEdit
      Left = 20
      Top = 24
      Width = 80
      Height = 21
      TabStop = False
      DataField = 'Codigo'
      DataSource = FmMD5Cab.DsMD5Cab
      TabOrder = 0
      UpdCampo = 'Codigo'
      UpdType = utInc
      Alignment = taLeftJustify
    end
    object GroupBox1: TGroupBox
      Left = 12
      Top = 52
      Width = 185
      Height = 61
      Caption = ' Faixa de numera'#231#227'o: '
      TabOrder = 2
      object Label2: TLabel
        Left = 8
        Top = 16
        Width = 69
        Height = 13
        Caption = 'N'#250'mero inicial:'
      end
      object Label3: TLabel
        Left = 92
        Top = 16
        Width = 62
        Height = 13
        Caption = 'N'#250'mero final:'
      end
      object EdNumIni: TdmkEdit
        Left = 8
        Top = 32
        Width = 80
        Height = 21
        TabStop = False
        Alignment = taRightJustify
        ReadOnly = True
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'NumIni'
        UpdCampo = 'NumIni'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
      object EdNumFim: TdmkEdit
        Left = 92
        Top = 32
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 1
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'NumFim'
        UpdCampo = 'NumFim'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
      end
    end
    object EdControle: TdmkEdit
      Left = 104
      Top = 24
      Width = 80
      Height = 21
      TabStop = False
      Alignment = taRightJustify
      ReadOnly = True
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Controle'
      UpdCampo = 'Controle'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
    end
  end
  object QrPesq: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(mdf.NumFim) FIM'
      'FROM md5fxa mdf'
      'WHERE mdf.Codigo=:P0'
      '')
    Left = 8
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPesqFIM: TIntegerField
      FieldName = 'FIM'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 39
    Top = 9
  end
end
