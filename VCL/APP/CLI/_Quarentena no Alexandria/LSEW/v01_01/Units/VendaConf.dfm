object FmVendaConf: TFmVendaConf
  Left = 339
  Top = 185
  Caption = 'PRD-VENDA-008 :: Confirma'#231#227'o de Venda'
  ClientHeight = 360
  ClientWidth = 398
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 312
    Width = 398
    Height = 48
    Align = alBottom
    TabOrder = 0
    object BtOK: TBitBtn
      Tag = 14
      Left = 15
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 286
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 6
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 398
    Height = 48
    Align = alTop
    Caption = 'Confirma'#231#227'o de Venda'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 396
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 547
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 398
    Height = 264
    Align = alClient
    TabOrder = 2
    object PBVenda: TProgressBar
      Left = 1
      Top = 246
      Width = 396
      Height = 17
      Align = alBottom
      TabOrder = 0
      Visible = False
    end
    object MCData: TMonthCalendar
      Left = 1
      Top = 49
      Width = 396
      Height = 197
      Align = alClient
      Date = 40101.737230104170000000
      TabOrder = 1
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 396
      Height = 48
      Align = alTop
      TabOrder = 2
      object Label69: TLabel
        Left = 9
        Top = 5
        Width = 39
        Height = 13
        Caption = 'Carteira:'
      end
      object EdCarteira: TdmkEditCB
        Left = 9
        Top = 21
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCarteira
        IgnoraDBLookupComboBox = False
      end
      object CBCarteira: TdmkDBLookupComboBox
        Left = 65
        Top = 21
        Width = 316
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCarteira
        TabOrder = 1
        dmkEditCB = EdCarteira
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object QrFutNeg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT gra.Nome NOMEGRADE, cor.Nome NOMECOR, tam.Nome NOMETAM, '
      'mom.Conta, mom.Grade, mom.Cor, mom.Tam, '
      'mom.Qtd, mom.Val, pro.EstqQ, pro.EstqV, '
      'pro.EstqQ + mom.Qtd FutQtd,'
      'pro.EstqV + mom.Val FutVal'
      'FROM movim mom'
      'LEFT JOIN produtos pro ON'
      '  pro.Codigo=mom.Grade AND'
      '  pro.Cor=mom.Cor AND'
      '  pro.Tam=mom.Tam'
      'LEFT JOIN grades gra ON gra.Codigo=mom.Grade'
      'LEFT JOIN cores  cor ON cor.Codigo=mom.Cor'
      'LEFT JOIN tamanhos tam ON tam.Codigo=mom.Tam'
      'WHERE mom.Controle=:P0'
      'AND (pro.EstqQ + mom.Qtd) < 0')
    Left = 13
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrFutNegConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrFutNegGrade: TIntegerField
      FieldName = 'Grade'
      Required = True
    end
    object QrFutNegCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrFutNegTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrFutNegQtd: TFloatField
      FieldName = 'Qtd'
      Required = True
    end
    object QrFutNegVal: TFloatField
      FieldName = 'Val'
      Required = True
    end
    object QrFutNegEstqQ: TFloatField
      FieldName = 'EstqQ'
    end
    object QrFutNegEstqV: TFloatField
      FieldName = 'EstqV'
    end
    object QrFutNegFutQtd: TFloatField
      FieldName = 'FutQtd'
    end
    object QrFutNegFutVal: TFloatField
      FieldName = 'FutVal'
    end
    object QrFutNegNOMEGRADE: TWideStringField
      FieldName = 'NOMEGRADE'
      Size = 30
    end
    object QrFutNegNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
    end
    object QrFutNegNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 5
    end
  end
  object frxDsFutNeg: TfrxDBDataset
    UserName = 'frxDsFutNeg'
    CloseDataSource = False
    DataSet = QrFutNeg
    BCDToCurrency = False
    Left = 41
    Top = 8
  end
  object frxFutNeg: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39140.346533923600000000
    ReportOptions.LastChange = 40135.769807592590000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    OnGetValue = frxFutNegGetValue
    Left = 69
    Top = 8
    Datasets = <
      item
        DataSet = frxDsFutNeg
        DataSetName = 'frxDsFutNeg'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 52.913420000000000000
        Top = 18.897650000000000000
        Width = 793.701300000000000000
        object Memo1: TfrxMemoView
          Left = 37.795300000000000000
          Top = 11.338590000000000000
          Width = 718.110700000000000000
          Height = 22.677180000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -19
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            'Mercadorias que ficariam com estoque negativo')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 37.795300000000000000
          Top = 34.015770000000000000
          Width = 45.354298980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'Pedido:')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 83.149660000000000000
          Top = 34.015770000000000000
          Width = 90.708658980000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          HAlign = haCenter
          Memo.UTF8W = (
            '[VAR_CODMOVIV]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 166.299320000000000000
        Width = 793.701300000000000000
        DataSet = frxDsFutNeg
        DataSetName = 'frxDsFutNeg'
        RowCount = 0
        object Memo3: TfrxMemoView
          Left = 37.795300000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DataField = 'Grade'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%4.4d'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFutNeg."Grade"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 75.590600000000000000
          Width = 264.567100000000000000
          Height = 18.897650000000000000
          DataField = 'NOMEGRADE'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFutNeg."NOMEGRADE"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 340.157700000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DataField = 'Cor'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%4.4d'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFutNeg."Cor"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 377.953000000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DataField = 'NOMECOR'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFutNeg."NOMECOR"]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 491.338900000000000000
          Width = 37.795300000000000000
          Height = 18.897650000000000000
          DataField = 'Tam'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%4.4d'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsFutNeg."Tam"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 529.134200000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'NOMETAM'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsFutNeg."NOMETAM"]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'EstqQ'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFutNeg."EstqQ"]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 680.315400000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'FutQtd'
          DataSet = frxDsFutNeg
          DataSetName = 'frxDsFutNeg'
          DisplayFormat.DecimalSeparator = ','
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsFutNeg."FutQtd"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 75.590600000000000000
        Top = 283.464750000000000000
        Width = 793.701300000000000000
        object Memo107: TfrxMemoView
          Left = 563.149970000000000000
          Top = 11.338590000000010000
          Width = 192.756030000000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          HAlign = haRight
          Memo.UTF8W = (
            'P'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 128.504020000000000000
        Width = 793.701300000000000000
        object Memo15: TfrxMemoView
          Left = 37.795300000000000000
          Width = 302.362253540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Descri'#231#227'o')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 340.157700000000000000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Cor')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 491.338582680000000000
          Width = 113.385826770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Tamanho')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590526770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Estq. Atual')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 680.315400000000000000
          Width = 75.590526770000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Memo.UTF8W = (
            'Estq. Futuro')
          ParentFont = False
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Height = 22.677180000000000000
        Top = 204.094620000000000000
        Width = 793.701300000000000000
        object Memo8: TfrxMemoView
          Left = 37.795300000000000000
          Top = 3.779530000000000000
          Width = 718.110553540000000000
          Height = 18.897650000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = [fsBold]
          Frame.Typ = [ftTop]
          Memo.UTF8W = (
            '')
          ParentFont = False
        end
      end
    end
  end
  object QrMovim2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mom.Grade, mom.Cor, mom.Tam, '
      'mom.Val, mom.Qtd, mom.Conta'
      'FROM movim mom'
      'WHERE mom.Controle= :P0'
      '/*AND mom.Kit=0*/')
    Left = 41
    Top = 36
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMovim2Grade: TIntegerField
      FieldName = 'Grade'
      Origin = 'movim.Grade'
      Required = True
    end
    object QrMovim2Cor: TIntegerField
      FieldName = 'Cor'
      Origin = 'movim.Cor'
      Required = True
    end
    object QrMovim2Tam: TIntegerField
      FieldName = 'Tam'
      Origin = 'movim.Tam'
      Required = True
    end
    object QrMovim2Val: TFloatField
      FieldName = 'Val'
      Origin = 'movim.Val'
      Required = True
    end
    object QrMovim2Qtd: TFloatField
      FieldName = 'Qtd'
      Origin = 'movim.Qtd'
      Required = True
    end
    object QrMovim2Conta: TIntegerField
      FieldName = 'Conta'
      Origin = 'movim.Conta'
      Required = True
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Controle, DataReal'
      'FROM moviv'
      'WHERE Controle=:P0')
    Left = 13
    Top = 36
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrLocCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrLocControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrLocDataReal: TDateField
      FieldName = 'DataReal'
    end
  end
  object QrCarteira: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE ForneceI=:P0'
      'AND Ativo = 1'
      'AND Tipo = 1'
      'ORDER BY Nome')
    Left = 97
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField12: TIntegerField
      FieldName = 'Codigo'
    end
    object StringField12: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCarteira: TDataSource
    DataSet = QrCarteira
    Left = 125
    Top = 8
  end
end
