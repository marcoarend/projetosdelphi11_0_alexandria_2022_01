unit GraGruDepto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, dmkValUsu, dmkLabel, Mask, dmkGeral,
  UnInternalConsts;

type
  TFmGraGruDepto = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    PnEdita: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    LaTipo: TdmkLabel;
    PnNiveis: TPanel;
    Label12: TLabel;
    dmkEdNome: TdmkEdit;
    Label13: TLabel;
    dmkEdCodigo: TdmkEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmGraGruDepto: TFmGraGruDepto;

implementation

uses Module, UMySQLModule, UnMySQLCuringa, GraGru, UnMyObjects;

{$R *.DFM}

procedure TFmGraGruDepto.BtOKClick(Sender: TObject);
var
  Codigo: Integer;
  Nome: String;
begin
  Nome    := dmkEdNome.ValueVariant;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descrição.', 'Falta de informações', MB_OK+MB_ICONWARNING);
    dmkEdNome.SetFocus;
    Exit;
  end;
  Codigo := UMyMod.BuscaEmLivreY_Def_Old('graded', 'Codigo', LaTipo.Caption, FmGraGru.QrGradeDCodigo.Value);
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, LaTipo.Caption, 'graded', False,
  [
    'Nome'
  ], ['Codigo'], [
    dmkEdNome.ValueVariant
  ], [Codigo]) then
  begin
    FmGraGru.QrGradeD.Close;
    FmGraGru.QrGradeD.Open;
    VAR_COD := Codigo;
    //
    Close;
  end;
end;

procedure TFmGraGruDepto.BtSaidaClick(Sender: TObject);
begin
  VAR_COD := FmGraGru.QrGradeDCodigo.Value;
  Close;
end;

procedure TFmGraGruDepto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  if LaTipo.Caption = CO_INCLUSAO then
  begin
    dmkEdCodigo.ValueVariant := 0;
    dmkEdNome.ValueVariant   := '';
  end else
  begin
    dmkEdCodigo.ValueVariant := FmGraGru.QrGradeDCodigo.Value;
    dmkEdNome.ValueVariant   := FmGraGru.QrGradeDNome.Value;
  end;
  dmkEdNome.SetFocus;
end;

procedure TFmGraGruDepto.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
