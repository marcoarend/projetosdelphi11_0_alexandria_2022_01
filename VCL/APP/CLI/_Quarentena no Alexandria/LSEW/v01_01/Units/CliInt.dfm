object FmCliInt: TFmCliInt
  Left = 339
  Top = 185
  Caption = 'CAD-CLIIN-001 :: Clientes Internos'
  ClientHeight = 610
  ClientWidth = 975
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 975
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Clientes Internos'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 872
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object LaTipo: TdmkLabel
      Left = 873
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
    end
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 59
    Width = 975
    Height = 551
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    ActivePage = TabSheet1
    Align = alClient
    TabOrder = 1
    object TabSheet1: TTabSheet
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Cadastro'
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 967
        Height = 520
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object PnInclui: TPanel
          Left = 0
          Top = 392
          Width = 967
          Height = 128
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          TabOrder = 1
          Visible = False
          object Label1: TLabel
            Left = 10
            Top = 7
            Width = 57
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Entidade:'
          end
          object PainelConfirma: TPanel
            Left = 1
            Top = 68
            Width = 965
            Height = 59
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alBottom
            TabOrder = 0
            object BtOK: TBitBtn
              Tag = 14
              Left = 25
              Top = 5
              Width = 110
              Height = 49
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Confirma'
              NumGlyphs = 2
              TabOrder = 0
              OnClick = BtOKClick
            end
            object Panel2: TPanel
              Left = 828
              Top = 1
              Width = 136
              Height = 57
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Align = alRight
              BevelOuter = bvNone
              TabOrder = 1
              object BtSaida: TBitBtn
                Tag = 15
                Left = 2
                Top = 4
                Width = 111
                Height = 49
                Cursor = crHandPoint
                Hint = 'Sai da janela atual'
                Margins.Left = 4
                Margins.Top = 4
                Margins.Right = 4
                Margins.Bottom = 4
                Caption = '&Desiste'
                NumGlyphs = 2
                ParentShowHint = False
                ShowHint = True
                TabOrder = 0
                OnClick = BtSaidaClick
              end
            end
          end
          object CBCliente: TdmkDBLookupComboBox
            Left = 79
            Top = 25
            Width = 853
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            KeyField = 'Codigo'
            ListField = 'NOMEENT'
            ListSource = DsEntidades
            TabOrder = 1
            dmkEditCB = EdCliente
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdCliente: TdmkEditCB
            Left = 10
            Top = 25
            Width = 69
            Height = 25
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Alignment = taRightJustify
            TabOrder = 2
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBCliente
            IgnoraDBLookupComboBox = False
          end
        end
        object PnControle: TPanel
          Left = 0
          Top = 333
          Width = 967
          Height = 59
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          TabOrder = 0
          object BitBtn1: TBitBtn
            Tag = 10
            Left = 25
            Top = 5
            Width = 110
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Inclui'
            NumGlyphs = 2
            TabOrder = 0
            OnClick = BitBtn1Click
          end
          object Panel6: TPanel
            Left = 829
            Top = 1
            Width = 137
            Height = 57
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Align = alRight
            BevelOuter = bvNone
            TabOrder = 1
            object BitBtn2: TBitBtn
              Tag = 13
              Left = 2
              Top = 4
              Width = 111
              Height = 49
              Cursor = crHandPoint
              Hint = 'Sai da janela atual'
              Margins.Left = 4
              Margins.Top = 4
              Margins.Right = 4
              Margins.Bottom = 4
              Caption = '&Sa'#237'da'
              NumGlyphs = 2
              ParentShowHint = False
              ShowHint = True
              TabOrder = 0
              OnClick = BtSaidaClick
            end
          end
          object BitBtn3: TBitBtn
            Tag = 12
            Left = 418
            Top = 5
            Width = 111
            Height = 49
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Exclui'
            NumGlyphs = 2
            TabOrder = 2
            OnClick = BitBtn3Click
          end
        end
        object dmkDBGrid1: TdmkDBGrid
          Left = 0
          Top = 0
          Width = 967
          Height = 333
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cliente'
              Title.Caption = 'Entidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLIENTE'
              Title.Caption = 'Raz'#227'o Social / Nome'
              Visible = True
            end>
          Color = clWindow
          DataSource = DsCliInt
          Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
          TabOrder = 2
          TitleFont.Charset = DEFAULT_CHARSET
          TitleFont.Color = clWindowText
          TitleFont.Height = -14
          TitleFont.Name = 'MS Sans Serif'
          TitleFont.Style = []
          Columns = <
            item
              Expanded = False
              FieldName = 'Codigo'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'Cliente'
              Title.Caption = 'Entidade'
              Visible = True
            end
            item
              Expanded = False
              FieldName = 'NOMECLIENTE'
              Title.Caption = 'Raz'#227'o Social / Nome'
              Visible = True
            end>
        end
      end
    end
  end
  object QrEntidades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo,'
      'IF(Tipo=0, RazaoSocial, Nome) NOMEENT'
      'FROM entidades'
      'WHERE CliInt=0'
      'AND Codigo <> 0'
      'ORDER BY NOMEENT')
    Left = 272
    Top = 400
    object QrEntidadesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntidadesNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object DsEntidades: TDataSource
    DataSet = QrEntidades
    Left = 300
    Top = 400
  end
  object DsCliInt: TDataSource
    DataSet = QrCliInt
    Left = 36
    Top = 4
  end
  object QrCliInt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cli.*,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMECLIENTE'
      'FROM cliint cli'
      'LEFT JOIN entidades ent ON ent.Codigo=cli.Cliente')
    Left = 8
    Top = 4
    object QrCliIntCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliIntCliente: TIntegerField
      FieldName = 'Cliente'
      Required = True
    end
    object QrCliIntNOMECLIENTE: TWideStringField
      FieldName = 'NOMECLIENTE'
      Size = 100
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BitBtn1
    CanDel01 = BitBtn3
    Left = 76
    Top = 4
  end
end
