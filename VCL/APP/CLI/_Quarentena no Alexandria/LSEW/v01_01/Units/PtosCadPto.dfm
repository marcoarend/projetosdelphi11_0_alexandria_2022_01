object FmPtosCadPto: TFmPtosCadPto
  Left = 368
  Top = 194
  Caption = 'PTO-VENDA-001 :: Cadastro de Pontos de Venda'
  ClientHeight = 464
  ClientWidth = 792
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 416
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 352
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label3: TLabel
        Left = 8
        Top = 4
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
        FocusControl = DBEdit1
      end
      object Label13: TLabel
        Left = 92
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label14: TLabel
        Left = 636
        Top = 4
        Width = 80
        Height = 13
        Caption = 'Limite de cr'#233'dito:'
      end
      object Label15: TLabel
        Left = 720
        Top = 4
        Width = 59
        Height = 13
        Caption = 'Dias correio:'
      end
      object Label16: TLabel
        Left = 8
        Top = 44
        Width = 79
        Height = 13
        Caption = 'Ponto de venda:'
      end
      object Label17: TLabel
        Left = 668
        Top = 44
        Width = 48
        Height = 13
        Caption = 'Comiss'#227'o:'
      end
      object Label18: TLabel
        Left = 8
        Top = 84
        Width = 47
        Height = 13
        Caption = 'Consultor:'
      end
      object Label19: TLabel
        Left = 668
        Top = 84
        Width = 48
        Height = 13
        Caption = 'Comiss'#227'o:'
      end
      object Label20: TLabel
        Left = 668
        Top = 124
        Width = 48
        Height = 13
        Caption = 'Comiss'#227'o:'
      end
      object Label21: TLabel
        Left = 8
        Top = 124
        Width = 86
        Height = 13
        Caption = 'Consultor gerente:'
      end
      object Label22: TLabel
        Left = 8
        Top = 164
        Width = 111
        Height = 13
        Caption = 'Lista de pre'#231'os padr'#227'o:'
      end
      object Label23: TLabel
        Left = 8
        Top = 204
        Width = 75
        Height = 13
        Caption = 'Carteira padr'#227'o:'
      end
      object Label25: TLabel
        Left = 8
        Top = 244
        Width = 155
        Height = 13
        Caption = 'Condi'#231#227'o de pagamento padr'#227'o:'
      end
      object Label27: TLabel
        Left = 8
        Top = 284
        Width = 166
        Height = 13
        Caption = 'Configura'#231#227'o de bloquetos padr'#227'o:'
      end
      object DBEdit1: TDBEdit
        Left = 8
        Top = 20
        Width = 80
        Height = 21
        DataField = 'CodUsu'
        DataSource = DsPtosCadPto
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 92
        Top = 20
        Width = 485
        Height = 21
        Hint = 'Nome do banco'
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsPtosCadPto
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBCheckBox1: TDBCheckBox
        Left = 580
        Top = 24
        Width = 45
        Height = 17
        Caption = 'Ativo'
        DataField = 'Ativo'
        DataSource = DsPtosCadPto
        TabOrder = 2
        ValueChecked = '1'
        ValueUnchecked = '0'
      end
      object DBEdit2: TDBEdit
        Left = 636
        Top = 20
        Width = 80
        Height = 21
        DataField = 'LimiCred'
        DataSource = DsPtosCadPto
        TabOrder = 3
      end
      object DBEdit3: TDBEdit
        Left = 720
        Top = 20
        Width = 60
        Height = 21
        DataField = 'DdEntrega'
        DataSource = DsPtosCadPto
        TabOrder = 4
      end
      object DBEdit4: TDBEdit
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        DataField = 'CU_PONTO'
        DataSource = DsPtosCadPto
        TabOrder = 5
      end
      object DBEdit5: TDBEdit
        Left = 64
        Top = 60
        Width = 600
        Height = 21
        DataField = 'NO_EntiPonto'
        DataSource = DsPtosCadPto
        TabOrder = 6
      end
      object DBEdit6: TDBEdit
        Left = 8
        Top = 100
        Width = 56
        Height = 21
        DataField = 'CU_CONSULTOR'
        DataSource = DsPtosCadPto
        TabOrder = 7
      end
      object DBEdit7: TDBEdit
        Left = 64
        Top = 100
        Width = 600
        Height = 21
        DataField = 'NO_Consultor'
        DataSource = DsPtosCadPto
        TabOrder = 8
      end
      object DBEdit8: TDBEdit
        Left = 8
        Top = 140
        Width = 56
        Height = 21
        DataField = 'CU_GERENTE'
        DataSource = DsPtosCadPto
        TabOrder = 9
      end
      object DBEdit9: TDBEdit
        Left = 64
        Top = 140
        Width = 600
        Height = 21
        DataField = 'NO_Gerente'
        DataSource = DsPtosCadPto
        TabOrder = 10
      end
      object DBEdit10: TDBEdit
        Left = 668
        Top = 60
        Width = 112
        Height = 21
        DataField = 'ComisPonto'
        DataSource = DsPtosCadPto
        TabOrder = 11
      end
      object DBEdit11: TDBEdit
        Left = 668
        Top = 100
        Width = 112
        Height = 21
        DataField = 'ComisConsu'
        DataSource = DsPtosCadPto
        TabOrder = 12
      end
      object DBEdit12: TDBEdit
        Left = 668
        Top = 140
        Width = 112
        Height = 21
        DataField = 'ComisGeren'
        DataSource = DsPtosCadPto
        TabOrder = 13
      end
      object DBEdit13: TDBEdit
        Left = 8
        Top = 180
        Width = 56
        Height = 21
        DataField = 'GraCusPrc'
        DataSource = DsPtosCadPto
        TabOrder = 14
      end
      object DBEdit14: TDBEdit
        Left = 64
        Top = 180
        Width = 717
        Height = 21
        DataField = 'NO_GraCusPrc'
        DataSource = DsPtosCadPto
        TabOrder = 15
      end
      object DBEdit15: TDBEdit
        Left = 8
        Top = 220
        Width = 56
        Height = 21
        DataField = 'Carteira'
        DataSource = DsPtosCadPto
        TabOrder = 16
      end
      object DBEdit16: TDBEdit
        Left = 64
        Top = 220
        Width = 717
        Height = 21
        DataField = 'NO_Carteira'
        DataSource = DsPtosCadPto
        TabOrder = 17
      end
      object DBEdit17: TDBEdit
        Left = 8
        Top = 260
        Width = 56
        Height = 21
        DataField = 'CU_PediPrzCab'
        DataSource = DsPtosCadPto
        TabOrder = 18
      end
      object DBEdit18: TDBEdit
        Left = 64
        Top = 260
        Width = 717
        Height = 21
        DataField = 'NO_PediPrzCab'
        DataSource = DsPtosCadPto
        TabOrder = 19
      end
      object DBEdit19: TDBEdit
        Left = 8
        Top = 300
        Width = 56
        Height = 21
        DataField = 'CNAB_Cfg'
        DataSource = DsPtosCadPto
        TabOrder = 20
      end
      object DBEdit20: TDBEdit
        Left = 64
        Top = 300
        Width = 717
        Height = 21
        DataField = 'NO_CNAB_Cfg'
        DataSource = DsPtosCadPto
        TabOrder = 21
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 367
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 26
        Height = 13
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 320
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtExclui: TBitBtn
          Tag = 12
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Exclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
        end
        object BtAltera: TBitBtn
          Tag = 11
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Altera'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtAlteraClick
        end
        object BtInclui: TBitBtn
          Tag = 10
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Inclui'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtIncluiClick
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 3
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 792
    Height = 416
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 367
      Width = 790
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 681
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 790
      Height = 328
      Align = alTop
      TabOrder = 0
      object Label8: TLabel
        Left = 8
        Top = 4
        Width = 57
        Height = 13
        Caption = 'C'#243'digo: [F4]'
        FocusControl = DBEdit1
      end
      object Label9: TLabel
        Left = 92
        Top = 4
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object LaCliente: TLabel
        Left = 8
        Top = 44
        Width = 45
        Height = 13
        Caption = 'Entidade:'
      end
      object Label4: TLabel
        Left = 668
        Top = 44
        Width = 48
        Height = 13
        Caption = 'Comiss'#227'o:'
      end
      object Label5: TLabel
        Left = 8
        Top = 84
        Width = 47
        Height = 13
        Caption = 'Consultor:'
      end
      object Label6: TLabel
        Left = 668
        Top = 84
        Width = 48
        Height = 13
        Caption = 'Comiss'#227'o:'
      end
      object SpeedButton6: TSpeedButton
        Left = 644
        Top = 100
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton6Click
      end
      object Label10: TLabel
        Left = 8
        Top = 124
        Width = 86
        Height = 13
        Caption = 'Consultor gerente:'
      end
      object Label11: TLabel
        Left = 668
        Top = 124
        Width = 48
        Height = 13
        Caption = 'Comiss'#227'o:'
      end
      object SpeedButton7: TSpeedButton
        Left = 644
        Top = 140
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton7Click
      end
      object SpeedButton5: TSpeedButton
        Left = 644
        Top = 60
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton5Click
      end
      object Label7: TLabel
        Left = 636
        Top = 4
        Width = 80
        Height = 13
        Caption = 'Limite de cr'#233'dito:'
      end
      object Label12: TLabel
        Left = 720
        Top = 4
        Width = 59
        Height = 13
        Caption = 'Dias correio:'
      end
      object Label1: TLabel
        Left = 8
        Top = 164
        Width = 111
        Height = 13
        Caption = 'Lista de pre'#231'os padr'#227'o:'
      end
      object Label2: TLabel
        Left = 8
        Top = 204
        Width = 75
        Height = 13
        Caption = 'Carteira padr'#227'o:'
      end
      object Label24: TLabel
        Left = 8
        Top = 244
        Width = 155
        Height = 13
        Caption = 'Condi'#231#227'o de pagamento padr'#227'o:'
      end
      object Label26: TLabel
        Left = 8
        Top = 284
        Width = 166
        Height = 13
        Caption = 'Configura'#231#227'o de bloquetos padr'#227'o:'
      end
      object SpeedButton8: TSpeedButton
        Left = 760
        Top = 180
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton8Click
      end
      object SpeedButton9: TSpeedButton
        Left = 760
        Top = 220
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton9Click
      end
      object SpeedButton10: TSpeedButton
        Left = 760
        Top = 260
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton10Click
      end
      object SpeedButton11: TSpeedButton
        Left = 760
        Top = 300
        Width = 21
        Height = 21
        Caption = '...'
        OnClick = SpeedButton11Click
      end
      object EdCodUsu: TdmkEdit
        Left = 8
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CodUsu'
        UpdCampo = 'CodUsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnKeyDown = EdCodUsuKeyDown
      end
      object EdNome: TdmkEdit
        Left = 92
        Top = 20
        Width = 485
        Height = 21
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Nome'
        UpdCampo = 'Nome'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object EdCodigo: TdmkEditCB
        Left = 8
        Top = 60
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 5
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCodigoChange
        DBLookupComboBox = CBCodigo
        IgnoraDBLookupComboBox = False
      end
      object CBCodigo: TdmkDBLookupComboBox
        Left = 65
        Top = 60
        Width = 580
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'NO_ENT'
        ListSource = DsClientes
        TabOrder = 6
        dmkEditCB = EdCodigo
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdComisPonto: TdmkEdit
        Left = 668
        Top = 60
        Width = 112
        Height = 21
        Alignment = taRightJustify
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        QryCampo = 'ComisPonto'
        UpdCampo = 'ComisPonto'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdConsultor: TdmkEditCB
        Left = 8
        Top = 100
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 8
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBConsultor
        IgnoraDBLookupComboBox = False
      end
      object CBConsultor: TdmkDBLookupComboBox
        Left = 65
        Top = 100
        Width = 580
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'NO_ENT'
        ListSource = DsConsultores
        TabOrder = 9
        dmkEditCB = EdConsultor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdComisConsu: TdmkEdit
        Left = 668
        Top = 100
        Width = 112
        Height = 21
        Alignment = taRightJustify
        TabOrder = 10
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        QryCampo = 'ComisConsu'
        UpdCampo = 'ComisConsu'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdGerente: TdmkEditCB
        Left = 8
        Top = 140
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 11
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGerente
        IgnoraDBLookupComboBox = False
      end
      object CBGerente: TdmkDBLookupComboBox
        Left = 65
        Top = 140
        Width = 580
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'NO_ENT'
        ListSource = DsGerentes
        TabOrder = 12
        dmkEditCB = EdGerente
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdComisGeren: TdmkEdit
        Left = 668
        Top = 140
        Width = 112
        Height = 21
        Alignment = taRightJustify
        TabOrder = 13
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 4
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0000'
        QryCampo = 'ComisGeren'
        UpdCampo = 'ComisGeren'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object EdLimiCred: TdmkEdit
        Left = 636
        Top = 20
        Width = 80
        Height = 21
        Alignment = taRightJustify
        TabOrder = 3
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        QryCampo = 'LimiCred'
        UpdCampo = 'LimiCred'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object CkAtivo: TdmkCheckBox
        Left = 584
        Top = 24
        Width = 45
        Height = 17
        Caption = 'Ativo.'
        TabOrder = 2
        QryCampo = 'Ativo'
        UpdCampo = 'Ativo'
        UpdType = utYes
        ValCheck = '1'
        ValUncheck = '0'
        OldValor = #0
      end
      object dmkEdit2: TdmkEdit
        Left = 720
        Top = 20
        Width = 60
        Height = 21
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'DdEntrega'
        UpdCampo = 'DdEntrega'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object EdGraCusPrc: TdmkEditCB
        Left = 8
        Top = 180
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 14
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'GraCusPrc'
        UpdCampo = 'GraCusPrc'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBGraCusPrc
        IgnoraDBLookupComboBox = False
      end
      object CBGraCusPrc: TdmkDBLookupComboBox
        Left = 65
        Top = 180
        Width = 696
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGraCusPrc
        TabOrder = 15
        dmkEditCB = EdGraCusPrc
        QryCampo = 'GraCusPrc'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCarteira: TdmkEditCB
        Left = 8
        Top = 220
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 16
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'Carteira'
        UpdCampo = 'Carteira'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCarteira
        IgnoraDBLookupComboBox = False
      end
      object CBCarteira: TdmkDBLookupComboBox
        Left = 65
        Top = 220
        Width = 696
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCarteiras
        TabOrder = 17
        dmkEditCB = EdCarteira
        QryCampo = 'Carteira'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdPediPrzCab: TdmkEditCB
        Left = 8
        Top = 260
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 18
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBPediPrzCab
        IgnoraDBLookupComboBox = False
      end
      object CBPediPrzCab: TdmkDBLookupComboBox
        Left = 65
        Top = 260
        Width = 696
        Height = 21
        KeyField = 'CodUsu'
        ListField = 'Nome'
        ListSource = DsPediPrzCab
        TabOrder = 19
        dmkEditCB = EdPediPrzCab
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCNAB_Cfg: TdmkEditCB
        Left = 8
        Top = 300
        Width = 56
        Height = 21
        Alignment = taRightJustify
        TabOrder = 20
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryCampo = 'CNAB_Cfg'
        UpdCampo = 'CNAB_Cfg'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCNAB_Cfg
        IgnoraDBLookupComboBox = False
      end
      object CBCNAB_Cfg: TdmkDBLookupComboBox
        Left = 65
        Top = 300
        Width = 696
        Height = 21
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCNAB_Cfg
        TabOrder = 21
        dmkEditCB = EdCNAB_Cfg
        QryCampo = 'CNAB_Cfg'
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 792
    Height = 48
    Align = alTop
    Caption = '                              Cadastro de Pontos de Venda'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object LaTipo: TdmkLabel
      Left = 709
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 603
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 483
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 227
      ExplicitTop = 2
      ExplicitWidth = 376
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
        OnClick = SbNovoClick
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsPtosCadPto: TDataSource
    DataSet = QrPtosCadPto
    Left = 40
    Top = 12
  end
  object QrPtosCadPto: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrPtosCadPtoBeforeOpen
    AfterOpen = QrPtosCadPtoAfterOpen
    SQL.Strings = (
      'SELECT pto.CodUsu CU_PONTO, cst.CodUsu '
      'CU_CONSULTOR, ger.CodUsu CU_GERENTE,'
      'IF(pto.Tipo=0,pto.RazaoSocial,pto.Nome) NO_EntiPonto,'
      'IF(cst.Tipo=0,cst.RazaoSocial,cst.Nome) NO_Consultor,'
      'IF(ger.Tipo=0,ger.RazaoSocial,ger.Nome) NO_Gerente,'
      'car.Nome NO_Carteira, gcp.Nome NO_GraCusPrc, '
      'ppc.Nome NO_PediPrzCab, ppc.CodUsu CU_PediPrzCab, '
      'cfg.Nome NO_CNAB_Cfg, pcp.*'
      'FROM ptoscadpto pcp'
      'LEFT JOIN entidades pto ON pto.Codigo=pcp.Codigo'
      'LEFT JOIN entidades cst ON cst.Codigo=pcp.Consultor'
      'LEFT JOIN entidades ger ON ger.Codigo=pcp.Gerente'
      'LEFT JOIN gracusprc gcp ON gcp.Codigo=pcp.GraCusPrc'
      'LEFT JOIN carteiras car ON car.Codigo=pcp.Carteira'
      'LEFT JOIN pediprzcab ppc ON ppc.Codigo=pcp.PediPrzCab'
      'LEFT JOIN cnab_cfg cfg ON cfg.Codigo=pcp.CNAB_Cfg')
    Left = 12
    Top = 12
    object QrPtosCadPtoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPtosCadPtoConsultor: TIntegerField
      FieldName = 'Consultor'
    end
    object QrPtosCadPtoGerente: TIntegerField
      FieldName = 'Gerente'
    end
    object QrPtosCadPtoComisPonto: TFloatField
      FieldName = 'ComisPonto'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPtosCadPtoComisConsu: TFloatField
      FieldName = 'ComisConsu'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPtosCadPtoComisGeren: TFloatField
      FieldName = 'ComisGeren'
      DisplayFormat = '#,###,##0.0000'
    end
    object QrPtosCadPtoDdEntrega: TIntegerField
      FieldName = 'DdEntrega'
    end
    object QrPtosCadPtoLimiCred: TFloatField
      FieldName = 'LimiCred'
      DisplayFormat = '#,###,##0.00'
    end
    object QrPtosCadPtoCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPtosCadPtoNO_EntiPonto: TWideStringField
      FieldName = 'NO_EntiPonto'
      Size = 100
    end
    object QrPtosCadPtoNO_Consultor: TWideStringField
      FieldName = 'NO_Consultor'
      Size = 100
    end
    object QrPtosCadPtoNO_Gerente: TWideStringField
      FieldName = 'NO_Gerente'
      Size = 100
    end
    object QrPtosCadPtoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrPtosCadPtoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrPtosCadPtoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrPtosCadPtoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrPtosCadPtoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrPtosCadPtoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrPtosCadPtoAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrPtosCadPtoNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrPtosCadPtoCU_PONTO: TIntegerField
      FieldName = 'CU_PONTO'
      Required = True
    end
    object QrPtosCadPtoCU_CONSULTOR: TIntegerField
      FieldName = 'CU_CONSULTOR'
      Required = True
    end
    object QrPtosCadPtoCU_GERENTE: TIntegerField
      FieldName = 'CU_GERENTE'
      Required = True
    end
    object QrPtosCadPtoGraCusPrc: TIntegerField
      FieldName = 'GraCusPrc'
    end
    object QrPtosCadPtoCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrPtosCadPtoNO_Carteira: TWideStringField
      FieldName = 'NO_Carteira'
      Size = 100
    end
    object QrPtosCadPtoNO_GraCusPrc: TWideStringField
      FieldName = 'NO_GraCusPrc'
      Size = 30
    end
    object QrPtosCadPtoPediPrzCab: TIntegerField
      FieldName = 'PediPrzCab'
    end
    object QrPtosCadPtoNO_PediPrzCab: TWideStringField
      FieldName = 'NO_PediPrzCab'
      Size = 50
    end
    object QrPtosCadPtoCU_PediPrzCab: TIntegerField
      FieldName = 'CU_PediPrzCab'
      Required = True
    end
    object QrPtosCadPtoCNAB_Cfg: TIntegerField
      FieldName = 'CNAB_Cfg'
    end
    object QrPtosCadPtoNO_CNAB_Cfg: TWideStringField
      FieldName = 'NO_CNAB_Cfg'
      Size = 50
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = BtInclui
    CanUpd01 = BtAltera
    CanDel01 = BtExclui
    Left = 68
    Top = 12
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM carteiras'
      'WHERE Codigo> 0'
      'AND Tipo=2'
      'ORDER BY Nome')
    Left = 304
    Top = 108
  end
  object QrClientes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu,'
      'IF(Tipo=0,RazaoSocial,Nome)  NO_ENT'
      'FROM entidades'
      'WHERE Cliente3="V"'
      'ORDER BY NO_ENT')
    Left = 616
    Top = 16
    object QrClientesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrClientesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrClientesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object DsClientes: TDataSource
    DataSet = QrClientes
    Left = 644
    Top = 16
  end
  object dmkValUsu1: TdmkValUsu
    dmkEditCB = EdCodigo
    Panel = PainelEdita
    QryCampo = 'Codigo'
    UpdCampo = 'Codigo'
    RefCampo = 'Codigo'
    UpdType = utIdx
    Left = 96
    Top = 12
  end
  object dmkValUsu2: TdmkValUsu
    dmkEditCB = EdConsultor
    Panel = PainelEdita
    QryCampo = 'Consultor'
    UpdCampo = 'Consultor'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 124
    Top = 12
  end
  object QrConsultores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu,'
      'IF(Tipo=0,RazaoSocial,Nome)  NO_ENT'
      'FROM entidades'
      'WHERE Fornece6="V"'
      'ORDER BY NO_ENT')
    Left = 672
    Top = 16
    object QrConsultoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrConsultoresCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrConsultoresNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object DsConsultores: TDataSource
    DataSet = QrConsultores
    Left = 700
    Top = 16
  end
  object QrGerentes: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, CodUsu,'
      'IF(Tipo=0,RazaoSocial,Nome)  NO_ENT'
      'FROM entidades'
      'WHERE Fornece7="V"'
      'ORDER BY NO_ENT')
    Left = 728
    Top = 16
    object QrGerentesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGerentesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrGerentesNO_ENT: TWideStringField
      FieldName = 'NO_ENT'
      Required = True
      Size = 100
    end
  end
  object DsGerentes: TDataSource
    DataSet = QrGerentes
    Left = 756
    Top = 16
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 332
    Top = 108
  end
  object QrGraCusPrc: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM gracusprc'
      'WHERE Codigo> 0'
      'ORDER BY Nome')
    Left = 304
    Top = 136
  end
  object DsGraCusPrc: TDataSource
    DataSet = QrGraCusPrc
    Left = 332
    Top = 136
  end
  object QrPediPrzCab: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cab.Codigo, cab.CodUsu, cab.Nome'
      'FROM pediprzcab cab'
      'LEFT JOIN pediprzcli cli ON cli.Codigo=cab.Codigo'
      'WHERE cli.Empresa=:P0 '
      'ORDER BY Nome')
    Left = 304
    Top = 164
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPediPrzCabCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrPediPrzCabCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrPediPrzCabNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsPediPrzCab: TDataSource
    DataSet = QrPediPrzCab
    Left = 332
    Top = 164
  end
  object dmkValUsu3: TdmkValUsu
    dmkEditCB = EdGerente
    Panel = PainelEdita
    QryCampo = 'Gerente'
    UpdCampo = 'Gerente'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 152
    Top = 12
  end
  object dmkValUsu4: TdmkValUsu
    dmkEditCB = EdPediPrzCab
    Panel = PainelEdita
    QryCampo = 'PediPrzCab'
    UpdCampo = 'PediPrzCab'
    RefCampo = 'Codigo'
    UpdType = utYes
    Left = 180
    Top = 12
  end
  object QrCNAB_Cfg: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cnab_cfg cfg'
      'ORDER BY Nome')
    Left = 304
    Top = 192
    object QrCNAB_CfgCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCNAB_CfgNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object DsCNAB_Cfg: TDataSource
    DataSet = QrCNAB_Cfg
    Left = 332
    Top = 192
  end
end
