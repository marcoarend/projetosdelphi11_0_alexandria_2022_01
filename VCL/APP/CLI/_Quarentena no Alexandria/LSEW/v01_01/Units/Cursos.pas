unit Cursos;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, mySQLDbTables, UnMySQLCuringa, 
  Menus, Grids, DBGrids, dmkDBGridDAC, dmkDBGrid, dmkEdit, dmkLabel, dmkGeral,
  dmkPermissoes, UnDmkProcFunc, UnDmkEnums;

type
  TFmCursos = class(TForm)
    PainelDados: TPanel;
    DsCursos: TDataSource;
    QrCursos: TmySQLQuery;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelEdita: TPanel;
    PainelConfirma: TPanel;
    BtConfirma: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    PainelEdit: TPanel;
    Label9: TLabel;
    EdCodigo: TEdit;
    Label10: TLabel;
    PainelData: TPanel;
    Label1: TLabel;
    DBEdCodigo: TDBEdit;
    DBEdNome: TDBEdit;
    Label2: TLabel;
    Panel1: TPanel;
    BtDesiste: TBitBtn;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Label3: TLabel;
    DBEdit1: TDBEdit;
    Label5: TLabel;
    BtInclui: TBitBtn;
    BtAltera: TBitBtn;
    BtExclui: TBitBtn;
    QrCursosCodigo: TIntegerField;
    QrCursosNome: TWideStringField;
    QrCursosValor: TFloatField;
    QrCursosLk: TIntegerField;
    QrCursosDataCad: TDateField;
    QrCursosDataAlt: TDateField;
    QrCursosUserCad: TIntegerField;
    QrCursosUserAlt: TIntegerField;
    QrCursosAlterWeb: TSmallintField;
    QrCursosAtivo: TSmallintField;
    EdNome: TdmkEdit;
    EdValor: TdmkEdit;
    dmkPermissoes1: TdmkPermissoes;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtConfirmaClick(Sender: TObject);
    procedure BtDesisteClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrCursosAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure SbQueryClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrCursosBeforeOpen(DataSet: TDataSet);
    procedure BitBtn2Click(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
  private
    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    //
  public
    { Public declarations }
  end;

var
  FmCursos: TFmCursos;
const
  FFormatFloat = '00000';

implementation

uses Module, UnMyObjects;

{$R *.DFM}

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmCursos.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmCursos.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrCursosCodigo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmCursos.DefParams;
begin
  VAR_GOTOTABELA := 'cursos';
  VAR_GOTOMYSQLTABLE := QrCursos;
  VAR_GOTONEG := gotoAll;
  VAR_GOTOCAMPO := CO_CODIGO;
  VAR_GOTONOME := CO_NOME;
  VAR_GOTOMySQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM cursos');
  VAR_SQLx.Add('WHERE Codigo <> 0');
  //
  VAR_SQL1.Add('AND Codigo=:P0');
  //
  VAR_SQLa.Add('AND Nome Like :P0');
  //
end;

procedure TFmCursos.MostraEdicao(Mostra: Integer; SQLType: TSQLType; Codigo: Integer);
begin
  case Mostra of
    0:
    begin
      PainelControle.Visible:=True;
      PainelDados.Visible := True;
      PainelEdita.Visible := False;
    end;
    1:
    begin
      PainelEdita.Visible := True;
      PainelDados.Visible := False;
      PainelControle.Visible:=False;
      if SQLType = stIns then
      begin
        EdCodigo.Text        := FormatFloat(FFormatFloat, Codigo);
        EdNome.Text          := '';
        EdValor.Text         := '';
      end else begin
        EdCodigo.Text        := DBEdCodigo.Text;
        EdNome.Text          := DBEdNome.Text;
        EdValor.ValueVariant := QrCursosValor.Value;
      end;
      EdNome.SetFocus;
    end;
    else Application.MessageBox('A��o de Inclus�o/altera��o n�o definida!',
    'Aviso', MB_OK+MB_ICONWARNING);
  end;
  LaTipo.SQLType := SQLType;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmCursos.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmCursos.AlteraRegistro;
var
  cursos : Integer;
begin
  cursos := QrCursosCodigo.Value;
  if not UMyMod.SelLockY(cursos, Dmod.MyDB, 'cursos', 'Codigo') then
  begin
    try
      UMyMod.UpdLockY(cursos, Dmod.MyDB, 'cursos', 'Codigo');
      MostraEdicao(1, stUpd, 0);
    finally
      Screen.Cursor := Cursor;
    end;
  end;
end;

procedure TFmCursos.IncluiRegistro;
var
  Cursor : TCursor;
  cursos : Integer;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    cursos := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'controle',
    'cursos', 'cursos', 'codigo');
    if Length(FormatFloat(FFormatFloat, cursos))>Length(FFormatFloat) then
    begin
      Application.MessageBox(
      'Inclus�o cancelada. Limite de cadastros extrapolado', 'Erro',
      MB_OK+MB_ICONERROR);
      Screen.Cursor := Cursor;
      Exit;
    end;
    MostraEdicao(1, stIns, cursos);
  finally
    Screen.Cursor := Cursor;
  end;
end;

procedure TFmCursos.QueryPrincipalAfterOpen;
begin
end;

procedure TFmCursos.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmCursos.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmCursos.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmCursos.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmCursos.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmCursos.BtSaidaClick(Sender: TObject);
begin
  VAR_CADASTRO := QrCursosCodigo.Value;
  Close;
end;

procedure TFmCursos.BtConfirmaClick(Sender: TObject);
var
  Codigo : Integer;
  Nome : String;
begin
  Nome := EdNome.Text;
  if Length(Nome) = 0 then
  begin
    Application.MessageBox('Defina uma descri��o.', 'Erro', MB_OK+MB_ICONERROR);
    Exit;
  end;
  Codigo := Geral.IMV(EdCodigo.Text);
  if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'cursos', False,
  [
    'Nome', 'Valor'
  ], ['Codigo'],
  [
  EdNome.Text, EdValor.ValueVariant
  ], [Codigo], True) then
  begin
    UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cursos', 'Codigo');
    MostraEdicao(0, stLok, 0);
    LocCod(Codigo,Codigo);
  end;
end;

procedure TFmCursos.BtDesisteClick(Sender: TObject);
var
  Codigo : Integer;
begin
  Codigo := Geral.IMV(EdCodigo.Text);
  if LaTipo.SQLType = stIns then UMyMod.PoeEmLivreY(Dmod.MyDB, 'livres', 'cursos', Codigo);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cursos', 'Codigo');
  MostraEdicao(0, stLok, 0);
  UMyMod.UpdUnlockY(Codigo, Dmod.MyDB, 'cursos', 'Codigo');
end;

procedure TFmCursos.FormCreate(Sender: TObject);
begin
  PainelEdita.Align := alClient;
  PainelDados.Align := alClient;
  PainelEdit.Align  := alClient;
  CriaOForm;
  //
end;

procedure TFmCursos.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrCursosCodigo.Value,LaRegistro.Caption);
end;

procedure TFmCursos.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmCursos.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmCursos.QrCursosAfterOpen(DataSet: TDataSet);
begin
  QueryPrincipalAfterOpen;
end;

procedure TFmCursos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCursos.SbQueryClick(Sender: TObject);
begin
  LocCod(QrCursosCodigo.Value,
  CuringaLoc.CriaForm(CO_CODIGO, CO_NOME, 'cursos', Dmod.MyDB, CO_VAZIO));
end;

procedure TFmCursos.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 30);
end;

procedure TFmCursos.QrCursosBeforeOpen(DataSet: TDataSet);
begin
  QrCursosCodigo.DisplayFormat := FFormatFloat;
end;

procedure TFmCursos.BitBtn2Click(Sender: TObject);
begin
  MostraEdicao(0, stLok, 0);
end;

procedure TFmCursos.BtIncluiClick(Sender: TObject);
begin
  IncluiRegistro;
end;

procedure TFmCursos.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

end.


