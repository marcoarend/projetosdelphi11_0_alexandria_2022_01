unit PtosPedLei;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DBCtrls, dmkDBLookupComboBox,
  dmkEdit, dmkEditCB, DB, mySQLDbTables, Mask, Variants, Grids, DBGrids,
  ComCtrls, dmkLabel, dmkDBEdit, dmkDBGrid, UnDmkEnums;

type
  TFmPtosPedLei = class(TForm)
    PainelConfirma: TPanel;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrItem: TmySQLQuery;
    DsItem: TDataSource;
    BtExclui: TBitBtn;
    CkFixo: TCheckBox;
    LaTipo: TdmkLabel;
    QrPreco: TmySQLQuery;
    DsPreco: TDataSource;
    QrFator: TmySQLQuery;
    QrPtosStqMov: TmySQLQuery;
    QrPrecoPrecoF: TFloatField;
    QrPrecoQuantF: TFloatField;
    DsPtosStqMov: TDataSource;
    PnLeitura: TPanel;
    Panel4: TPanel;
    Label3: TLabel;
    LaQtdeLei: TLabel;
    EdLeituraCodigoDeBarras: TEdit;
    EdQtdLei: TdmkEdit;
    BtOK: TBitBtn;
    Panel9: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBGPtosStqMov: TDBGrid;
    EdPrecoO: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    EdPrecoR: TdmkEdit;
    EdDescoP: TdmkEdit;
    Label10: TLabel;
    QrLista: TmySQLQuery;
    QrListaGraCusPrc: TIntegerField;
    QrExiste_: TmySQLQuery;
    QrExiste_Controle: TIntegerField;
    QrExiste_QuantP: TFloatField;
    QrPediVdaLei: TmySQLQuery;
    DsPediVdaLei: TDataSource;
    QrPediVdaLeiConta: TIntegerField;
    QrPediVdaLeiSequencia: TIntegerField;
    QrExiste_PrecoR: TFloatField;
    QrExiste_DescoP: TFloatField;
    Itens_por_grupo: TmySQLQuery;
    StringField1: TWideStringField;
    StringField2: TWideStringField;
    StringField3: TWideStringField;
    IntegerField1: TIntegerField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    IntegerField2: TIntegerField;
    QrPtosStqMovNO_Produto: TWideStringField;
    QrPtosStqMovNO_Cor: TWideStringField;
    QrPtosStqMovNO_Tam: TWideStringField;
    QrPtosStqMovCU_Tam: TIntegerField;
    QrPtosStqMovQuanti: TFloatField;
    QrPtosStqMovPrecoO: TFloatField;
    QrPtosStqMovDescoP: TFloatField;
    QrPtosStqMovPrecoR: TFloatField;
    QrPtosStqMovDataIns: TDateTimeField;
    QrPtosStqMovCO_Produto: TIntegerField;
    QrPtosStqMovIDCtrl: TIntegerField;
    QrItemNO_GRADE: TWideStringField;
    QrItemNO_COR: TWideStringField;
    QrItemNO_TAM: TWideStringField;
    QrItemGraGruX: TIntegerField;
    QrItemCustoPreco: TFloatField;
    QrPtosStqMovReduzido: TIntegerField;
    QrPtosStqMovEAN13: TWideStringField;
    QrSUMPtosStqMov: TmySQLQuery;
    DsSUMPtosStqMov: TDataSource;
    QrSUMPtosStqMovTOT_PrecoR: TFloatField;
    Label7: TLabel;
    DBEdValTot: TDBEdit;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdLeituraCodigoDeBarrasChange(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdQtdLeiKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtExcluiClick(Sender: TObject);
    procedure QrItemBeforeClose(DataSet: TDataSet);
    procedure EdQtdLeiEnter(Sender: TObject);
    procedure QrPtosStqMovAfterScroll(DataSet: TDataSet);
    procedure CkFixoClick(Sender: TObject);
    procedure EdPrecoOChange(Sender: TObject);
    procedure EdDescoPChange(Sender: TObject);
    procedure EdLeituraCodigoDeBarrasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure QrPtosStqMovCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    FTamAll: Boolean;
    FGraGruX, FSequencia: Integer;
    function ReopenItem(): Boolean;
    procedure InsereItem();
    procedure CalculaPrecoReal();
  public
    { Public declarations }
    procedure ReopenPtosStqMov(GraGruX: Integer);
    procedure ReopenPediVdaLei(Conta: Integer);

  end;

  var
  FmPtosPedLei: TFmPtosPedLei;

implementation

uses Module, UMySQLModule, dmkGeral, ModuleGeral, UnInternalConsts,
MyDBCheck, GetValor, UnMyObjects, Principal, ModuleProd, PtosPedCad, QuaisItens;

{$R *.DFM}

const
  FTamMax = 13; // EAN13
  FTamPrd = 12; // tudo � codigo do produto menos o d�gito (n�o tem lote e etc.)
  FTamMin = 1; // Digito verificador

procedure TFmPtosPedLei.BtExcluiClick(Sender: TObject);
begin
 DBCheck.QuaisItens_Exclui(Dmod.QrUpd, QrPtosStqMov, DBGPtosStqMov,
   'ptosstqmov', ['IDCtrl'], ['IDCtrl'], istPergunta, '');
 //
 QrSUMPtosStqMov.Close;
 QrSUMPtosStqMov.Open;
end;

procedure TFmPtosPedLei.BtOKClick(Sender: TObject);
begin
  InsereItem();
end;

procedure TFmPtosPedLei.InsereItem();
const
  Quanti = 1;
  Status = 0;
  CodOut = 0;
  TipOut = 0;
var
  Qtd, PrecoO, PrecoR, DescoP: Double;
  DataIns: String;
  QuantP, IDCtrl, I, GraGruX, CodInn, Empresa: Integer;
begin
  if (FGragruX = 0) then
  begin
    Application.MessageBox('Defina o produto!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdLeituraCodigoDeBarras.SetFocus;
    Exit;
  end;
  Screen.Cursor := crHourGlass;
  try
    DataIns := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
    Empresa   := FmPtosPedCad.QrPtosPedCadPontoVda.Value;
    CodInn    := FmPtosPedCad.QrPtosPedCadCodigo.Value;
    GraGruX := 0;
    //ErrPreco  := 0;
    //AvisoPrc  := 0;
    //QtdRed    := 0;
    Qtd := Geral.IMV(EdQtdLei.Text);
    if Qtd = 0 then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox(
        'Definina a quantidade!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end;
    PrecoR := Geral.DMV(EdPrecoR.Text);
    if PrecoR = 0 then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox('Defina o pre�o de faturamento!', 'Aviso',
        MB_OK+MB_ICONWARNING);
      // est� desabilitado
      //EdPrecoR.SetFocus;
      Exit;
    end;
    if FGraGruX > 0 then
    begin
      GraGruX    := FGraGruX;
      FGraGruX   := 0;
      FSequencia := 0;
      QuantP     := Geral.IMV(EdQtdLei.Text);
      //
      LaTipo.SQLType := stIns;
      PrecoO     := Geral.DMV(EdPrecoO.Text);
      DescoP     := Geral.DMV(EdDescoP.Text);
      for I := 1 to QuantP do
      begin
        IDCtrl := UMyMod.BuscaEmLivreY_Def('ptosstqmov', 'IDCtrl', LaTipo.SQLType, 0);
        UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'ptosstqmov', False, [
        'CodInn', 'CodOut', 'TipOut', 'DataIns', 'Empresa', 'Produto',
        'Quanti', 'PrecoO', 'PrecoR', 'DescoP', 'Status'], ['IDCtrl'], [
        CodInn, CodOut, TipOut, DataIns, Empresa, GraGruX,
        Quanti, PrecoO, PrecoR, DescoP, Status], [IDCtrl], False);
      end;
    end;
    LaTipo.SQLType := stIns;
    EdLeituraCodigoDeBarras.Text := '';
    EdLeituraCodigoDeBarras.SetFocus;
    ReopenPtosStqMov(GraGruX);
    //ReopenPtosPedGru(0);
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPtosPedLei.QrItemBeforeClose(DataSet: TDataSet);
begin
  EdPrecoO.ValueVariant := 0;
end;

procedure TFmPtosPedLei.QrPtosStqMovAfterScroll(DataSet: TDataSet);
begin
  ReopenPediVdaLei(0);
end;

procedure TFmPtosPedLei.QrPtosStqMovCalcFields(DataSet: TDataSet);
var
  EAN13: String;
begin
  EAN13 := FormatFloat('000000000000', QrPtosStqMovReduzido.Value);
  QrPtosStqMovEAN13.Value := EAN13 + Geral.ModuloEAN13(EAN13);
end;

procedure TFmPtosPedLei.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPtosPedLei.CalculaPrecoReal();
var
  Fator, PrecoO: Double;
begin
  PrecoO := Geral.DMV(EdPrecoO.Text);
  Fator := 1 - (Geral.DMV(EdDescoP.Text) / 100);
  EdPrecoR.ValueVariant := Fator * PrecoO;
end;

procedure TFmPtosPedLei.CkFixoClick(Sender: TObject);
begin
  EdQtdLei.Enabled := not CkFixo.Checked;
  EdDescoP.Enabled := not CkFixo.Checked;
end;

procedure TFmPtosPedLei.EdDescoPChange(Sender: TObject);
begin
  CalculaPrecoReal();
end;

procedure TFmPtosPedLei.EdLeituraCodigoDeBarrasChange(Sender: TObject);
var
  Tam: Integer;
begin
  Tam := Length(EdLeituraCodigoDeBarras.Text);
  if Tam = FTamMax then
  begin
    FGraGruX   := Geral.IMV(Copy(EdLeituraCodigoDeBarras.Text, 07, 6));
    FSequencia := Geral.IMV(Copy(EdLeituraCodigoDeBarras.Text, 13, 8));
    if ReopenItem() then
    begin
      if CkFixo.Checked then
      begin
        FTamAll := True;
        InsereItem();
        EdLeituraCodigoDeBarras.SetFocus;
        EdLeituraCodigoDeBarras.Text := '';
      end else EdQtdLei.SetFocus;
    end;
  end else if Tam = 0 then QrItem.Close;
end;

procedure TFmPtosPedLei.EdLeituraCodigoDeBarrasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Txt: String;
  Tam: Integer;
begin
  if Key = VK_RETURN then
  begin
    if  (Length(EdLeituraCodigoDeBarras.Text) <= FTamPrd)
    and (Length(EdLeituraCodigoDeBarras.Text) >  FTamMin) then
    begin
      Txt := EdLeituraCodigoDeBarras.Text;
      Tam := Length(Txt);
      Txt := Copy(Txt, 1, Tam-1);
      FGraGruX := Geral.IMV(Txt);
      if ReopenItem() then
      begin
        if CkFixo.Checked then
        begin
          FTamAll := True;
          InsereItem();
          EdLeituraCodigoDeBarras.SetFocus;
          EdLeituraCodigoDeBarras.Text := '';
        end else EdQtdLei.SetFocus;
      end;
    end;
  end;  
end;

procedure TFmPtosPedLei.EdPrecoOChange(Sender: TObject);
begin
  CalculaPrecoReal();
end;

procedure TFmPtosPedLei.EdQtdLeiEnter(Sender: TObject);
begin
  if CkFixo.Checked and (EdLeituraCodigoDeBarras.Text = '') and FTamAll then
  begin
    FTamAll := False;
    // est� desabilitado
    //EdPrecoR.SetFocus;
  end;
end;

procedure TFmPtosPedLei.EdQtdLeiKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = VK_RETURN then
    InsereItem();
end;

procedure TFmPtosPedLei.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPtosPedLei.FormCreate(Sender: TObject);
begin
  ReopenPtosStqMov(0);
  EdQtdLei.ValueVariant := 1;
end;

procedure TFmPtosPedLei.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

function TFmPtosPedLei.ReopenItem(): Boolean;
begin
  Result := False;
  QrItem.Close;
  if FGraGruX > 0 then
  begin
    QrItem.Params[00].AsInteger := FmPtosPedCad.QrPtosPedCadGraCusPrc.Value;
    QrItem.Params[01].AsInteger := FGraGrux;
    QrItem.Open;
    //
    if QrItem.RecordCount > 0 then
    begin
      Result := True;
      EdPrecoO.ValueVariant := QrItemCustoPreco.Value;
    end else
      Application.MessageBox('Reduzido n�o localizado!', 'Aviso',
      MB_OK+MB_ICONWARNING);
  end;
end;

procedure TFmPtosPedLei.ReopenPtosStqMov(GraGruX: Integer);
begin
  QrPtosStqMov.Close;
  QrPtosStqMov.Params[0].AsInteger := FmPtosPedCad.QrPtosPedCadCodigo.Value;
  QrPtosStqMov.Open;
  //
  QrSUMPtosStqMov.Close;
  QrSUMPtosStqMov.Params[0].AsInteger := FmPtosPedCad.QrPtosPedCadCodigo.Value;
  QrSUMPtosStqMov.Open;
  //
  if GraGruX <> 0 then
    QrPtosStqMov.Locate('CO_Produto', GraGruX, []);
end;

procedure TFmPtosPedLei.ReopenPediVdaLei(Conta: Integer);
begin
  {
  QrPediVdaLei.Close;
  QrPediVdaLei.Params[0].AsInteger := QrPtosStqMovControle.Value;
  QrPediVdaLei.Open;
  //
  if Conta <> 0 then
    QrPediVdaLei.Locate('Conta', Conta, []);
  }
end;

end.

