object FmEquiGru: TFmEquiGru
  Left = 368
  Top = 194
  Caption = 'Equipes'
  ClientHeight = 428
  ClientWidth = 975
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelEdita: TPanel
    Left = 0
    Top = 59
    Width = 975
    Height = 369
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 309
      Width = 973
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 10
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 838
        Top = 1
        Width = 133
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 110
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 973
      Height = 241
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 20
        Top = 10
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 84
        Top = 10
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
      end
      object Label5: TLabel
        Left = 20
        Top = 123
        Width = 387
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'Carteira preferencial para cr'#233'ditos e d'#233'bitos em ciclos de curso' +
          's:'
      end
      object SpeedButton5: TSpeedButton
        Left = 635
        Top = 143
        Width = 26
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        OnClick = SpeedButton5Click
      end
      object EdCodigo: TEdit
        Left = 20
        Top = 30
        Width = 59
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = '00'
      end
      object EdNome: TEdit
        Left = 84
        Top = 30
        Width = 577
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 1
      end
      object RGTipo: TRadioGroup
        Left = 20
        Top = 59
        Width = 641
        Height = 60
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = ' Tipo de grupo: '
        Columns = 3
        ItemIndex = 0
        Items.Strings = (
          'Indefinido'
          'Promotor'
          'Professor')
        TabOrder = 2
      end
      object CBCartPadr: TdmkDBLookupComboBox
        Left = 94
        Top = 143
        Width = 537
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsCarteiras
        TabOrder = 3
        dmkEditCB = EdCartPadr
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCartPadr: TdmkEditCB
        Left = 20
        Top = 143
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 4
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        DBLookupComboBox = CBCartPadr
        IgnoraDBLookupComboBox = False
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 975
    Height = 369
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 309
      Width = 973
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 213
        Top = 1
        Width = 31
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 212
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 394
        Top = 1
        Width = 577
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtEquiEnt: TBitBtn
          Tag = 123
          Left = 118
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Prof/Prom'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtEquiEntClick
        end
        object BtEquiGru: TBitBtn
          Tag = 101
          Left = 5
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Equipe'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtEquiGruClick
        end
        object Panel2: TPanel
          Left = 443
          Top = 0
          Width = 134
          Height = 57
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 973
      Height = 64
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      Enabled = False
      TabOrder = 1
      object Label1: TLabel
        Left = 20
        Top = 10
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'C'#243'digo:'
        FocusControl = DBEdCodigo
      end
      object Label2: TLabel
        Left = 84
        Top = 10
        Width = 65
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Descri'#231#227'o:'
        FocusControl = DBEdNome
      end
      object Label6: TLabel
        Left = 571
        Top = 10
        Width = 387
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 
          'Carteira preferencial para cr'#233'ditos e d'#233'bitos em ciclos de curso' +
          's:'
        FocusControl = DBEdit1
      end
      object DBEdCodigo: TDBEdit
        Left = 20
        Top = 30
        Width = 59
        Height = 21
        Hint = 'N'#186' do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabStop = False
        DataField = 'Codigo'
        DataSource = DsEquiGru
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ParentShowHint = False
        ReadOnly = True
        ShowHint = True
        TabOrder = 0
      end
      object DBEdNome: TDBEdit
        Left = 84
        Top = 30
        Width = 483
        Height = 21
        Hint = 'Nome do banco'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Color = clWhite
        DataField = 'Nome'
        DataSource = DsEquiGru
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 1
      end
      object DBEdit1: TDBEdit
        Left = 571
        Top = 30
        Width = 394
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'NOMECARTEIRA'
        DataSource = DsEquiGru
        TabOrder = 2
      end
    end
    object Panel4: TPanel
      Left = 1
      Top = 113
      Width = 973
      Height = 196
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 2
      object dmkDBGrid1: TdmkDBGrid
        Left = 0
        Top = 0
        Width = 972
        Height = 196
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Columns = <
          item
            Expanded = False
            FieldName = 'Entidade'
            Title.Caption = 'ID'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEENTI'
            Title.Caption = 'Nome promotor / professor'
            Width = 238
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TEL1ENTI_TXT'
            Title.Caption = 'Telefone'
            Width = 88
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CELUENTI_TXT'
            Title.Caption = 'Celular'
            Width = 88
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEROLCOMIS'
            Title.Caption = 'Rol de comiss'#245'es'
            Width = 293
            Visible = True
          end>
        Color = clWindow
        DataSource = DsEquiEnt
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -15
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Entidade'
            Title.Caption = 'ID'
            Width = 45
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEENTI'
            Title.Caption = 'Nome promotor / professor'
            Width = 238
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TEL1ENTI_TXT'
            Title.Caption = 'Telefone'
            Width = 88
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'CELUENTI_TXT'
            Title.Caption = 'Celular'
            Width = 88
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEROLCOMIS'
            Title.Caption = 'Rol de comiss'#245'es'
            Width = 293
            Visible = True
          end>
      end
    end
  end
  object PainelItens: TPanel
    Left = 0
    Top = 59
    Width = 975
    Height = 369
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object Panel6: TPanel
      Left = 1
      Top = 309
      Width = 973
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 1
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 10
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BitBtn1Click
      end
      object Panel7: TPanel
        Left = 838
        Top = 1
        Width = 133
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn2: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 110
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn2Click
        end
      end
    end
    object Panel8: TPanel
      Left = 1
      Top = 1
      Width = 973
      Height = 241
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 0
      object Panel10: TPanel
        Left = 1
        Top = 65
        Width = 971
        Height = 175
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        TabOrder = 1
        ExplicitWidth = 970
        object LaProfProm: TLabel
          Left = 20
          Top = 5
          Width = 57
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Entidade:'
        end
        object Label3: TLabel
          Left = 15
          Top = 148
          Width = 404
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 
            'Obs.: Entidades j'#225' selecionadas n'#227'o aparecem na lista de sele'#231#227'o' +
            '.'
        end
        object Label4: TLabel
          Left = 20
          Top = 54
          Width = 112
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Rol de comiss'#245'es:'
        end
        object EdEntidade: TdmkEditCB
          Left = 20
          Top = 25
          Width = 69
          Height = 25
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBEntidade
          IgnoraDBLookupComboBox = False
        end
        object CBEntidade: TdmkDBLookupComboBox
          Left = 89
          Top = 25
          Width = 572
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'NOMEENTI'
          ListSource = DsProfPromo
          TabOrder = 1
          dmkEditCB = EdEntidade
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdRolComis: TdmkEditCB
          Left = 20
          Top = 74
          Width = 69
          Height = 26
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Alignment = taRightJustify
          TabOrder = 2
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBRolComis
          IgnoraDBLookupComboBox = False
        end
        object CBRolComis: TdmkDBLookupComboBox
          Left = 89
          Top = 74
          Width = 572
          Height = 24
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsRolComis
          TabOrder = 3
          dmkEditCB = EdRolComis
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
      end
      object Panel9: TPanel
        Left = 1
        Top = 1
        Width = 971
        Height = 64
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alTop
        TabOrder = 0
        ExplicitWidth = 970
        object Label7: TLabel
          Left = 20
          Top = 10
          Width = 47
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit2
        end
        object Label8: TLabel
          Left = 84
          Top = 10
          Width = 65
          Height = 16
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdit3
        end
        object DBEdit2: TDBEdit
          Left = 20
          Top = 30
          Width = 59
          Height = 21
          Hint = 'N'#186' do banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsEquiGru
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdit3: TDBEdit
          Left = 84
          Top = 30
          Width = 577
          Height = 21
          Hint = 'Nome do banco'
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsEquiGru
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 975
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = '                              Equipes'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 278
      Top = 1
      Width = 595
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
    object LaTipo: TdmkLabel
      Left = 873
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 277
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 4
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsEquiGru: TDataSource
    DataSet = QrEquiGru
    Left = 46
    Top = 11
  end
  object QrEquiGru: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrEquiGruBeforeOpen
    AfterOpen = QrEquiGruAfterOpen
    AfterScroll = QrEquiGruAfterScroll
    SQL.Strings = (
      'SELECT car.Nome NOMECARTEIRA, eqg.* '
      'FROM equigru eqg'
      'LEFT JOIN carteiras car ON car.Codigo=eqg.CartPadr'
      'WHERE eqg.Codigo > 0')
    Left = 18
    Top = 11
    object QrEquiGruCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'equigru.Codigo'
    end
    object QrEquiGruNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'equigru.Nome'
      Size = 100
    end
    object QrEquiGruTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'equigru.Tipo'
    end
    object QrEquiGruCartPadr: TIntegerField
      FieldName = 'CartPadr'
    end
    object QrEquiGruNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
  end
  object PMEquiGru: TPopupMenu
    OnPopup = PMEquiGruPopup
    Left = 320
    Top = 8
    object Incluinovaequipe1: TMenuItem
      Caption = '&Inclui nova equipe'
      OnClick = Crianovogrupo1Click
    end
    object Alteraequipeatual1: TMenuItem
      Caption = '&Altera equipe atual'
      Enabled = False
      OnClick = Alteragrupoatual1Click
    end
    object Excluiequipeatual1: TMenuItem
      Caption = '&Exclui equipe atual'
      Enabled = False
      OnClick = Excluigrupoatual1Click
    end
  end
  object QrEquiEnt: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEquiEntCalcFields
    SQL.Strings = (
      'SELECT ee.Codigo Equipe, ee.Entidade, ee.Controle,'
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEENTI,'
      'IF(en.Tipo=0, en.Ete1, en.PTe1) TEL1ENTI, '
      'IF(en.Tipo=0, en.ECel, en.PCel) CELUENTI,'
      'ee.RolComis, ec.Nome NOMEROLCOMIS '
      'FROM equient ee'
      'LEFT JOIN entidades en ON en.Codigo=ee.Entidade'
      'LEFT JOIN equicom ec ON ec.Codigo=ee.RolComis'
      'WHERE ee.Codigo=:P0')
    Left = 102
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEquiEntControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'equient.Controle'
    end
    object QrEquiEntEntidade: TIntegerField
      FieldName = 'Entidade'
      Origin = 'equient.Entidade'
    end
    object QrEquiEntNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
    object QrEquiEntTEL1ENTI: TWideStringField
      FieldName = 'TEL1ENTI'
    end
    object QrEquiEntCELUENTI: TWideStringField
      FieldName = 'CELUENTI'
    end
    object QrEquiEntTEL1ENTI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TEL1ENTI_TXT'
      Size = 40
      Calculated = True
    end
    object QrEquiEntCELUENTI_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CELUENTI_TXT'
      Size = 40
      Calculated = True
    end
    object QrEquiEntEquipe: TIntegerField
      FieldName = 'Equipe'
      Origin = 'equient.Codigo'
      Required = True
    end
    object QrEquiEntNOMEROLCOMIS: TWideStringField
      FieldName = 'NOMEROLCOMIS'
      Origin = 'equicom.Nome'
      Size = 100
    end
    object QrEquiEntRolComis: TIntegerField
      FieldName = 'RolComis'
      Origin = 'equient.RolComis'
      Required = True
    end
  end
  object DsEquiEnt: TDataSource
    DataSet = QrEquiEnt
    Left = 130
    Top = 10
  end
  object PMEquiEnt: TPopupMenu
    OnPopup = PMEquiEntPopup
    Left = 348
    Top = 8
    object Incluientidade1: TMenuItem
      Caption = '&Inclui entidade'
      OnClick = Incluiitemdereceita1Click
    end
    object Alteraentidade1: TMenuItem
      Caption = '&Altera entidade'
      Enabled = False
      OnClick = Alteraitemdereceitaatual1Click
    end
    object Retiraentidade1: TMenuItem
      Caption = '&Retira entidade'
      Enabled = False
      OnClick = Excluiitemdereceitaatual1Click
    end
  end
  object QrProfPromo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT en.Codigo, '
      'IF(en.Tipo=0, en.RazaoSocial, en.Nome) NOMEENTI '
      'FROM entidades en '
      'WHERE en.Fornece2="V" '
      'OR en.Fornece3="V"'
      'ORDER BY NOMEENTI')
    Left = 388
    Top = 8
    object QrProfPromoCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProfPromoNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Required = True
      Size = 100
    end
  end
  object DsProfPromo: TDataSource
    DataSet = QrProfPromo
    Left = 416
    Top = 8
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor'
      'FROM equient'
      'WHERE Codigo=:P0')
    Left = 552
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object frxPrecoCurso: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39673.632371770800000000
    ReportOptions.LastChange = 39673.632371770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <MeuLogoExiste> = True then Picture2.LoadFromFile(<MeuLogoC' +
        'aminho>);  '
      'end.')
    Left = 186
    Top = 10
    Datasets = <
      item
        DataSet = frxDsEquiEnt
        DataSetName = 'frxDsEquiEnt'
      end
      item
        DataSet = frxDsEquiGru
        DataSetName = 'frxDsEquiGru'
      end
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 90.708720000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object frxDsEquiGruCodigo: TfrxMemoView
          Left = 211.653680000000000000
          Top = 26.456710000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiGru
          DataSetName = 'frxDsEquiGru'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'EQUIPE N'#186' [frxDsEquiGru."Codigo"]')
          ParentFont = False
        end
        object frxDsEquiGruNome: TfrxMemoView
          Left = 211.653680000000000000
          Top = 45.354360000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DataField = 'Nome'
          DataSet = frxDsEquiGru
          DataSetName = 'frxDsEquiGru'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEquiGru."Nome"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 37.795300000000000000
          Top = 71.811070000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiEnt
          DataSetName = 'frxDsEquiEnt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 94.488250000000000000
          Top = 71.811070000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiEnt
          DataSetName = 'frxDsEquiEnt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 566.929500000000000000
          Top = 71.811070000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiEnt
          DataSetName = 'frxDsEquiEnt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Celular')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          Left = 37.795300000000000000
          Width = 170.078740157480000000
          Height = 68.031496062992100000
          Stretched = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 211.653567720000000000
          Width = 468.850340000000000000
          Height = 22.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 453.543600000000000000
          Top = 71.811070000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiEnt
          DataSetName = 'frxDsEquiEnt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Telefone')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 170.078850000000000000
        Width = 718.110700000000000000
        DataSet = frxDsEquiEnt
        DataSetName = 'frxDsEquiEnt'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 37.795300000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataField = 'Entidade'
          DataSet = frxDsEquiEnt
          DataSetName = 'frxDsEquiEnt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiEnt."Entidade"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 94.488250000000000000
          Width = 359.055350000000000000
          Height = 18.897650000000000000
          DataField = 'NOMEENTI'
          DataSet = frxDsEquiEnt
          DataSetName = 'frxDsEquiEnt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEquiEnt."NOMEENTI"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 566.929500000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DataField = 'CELUENTI_TXT'
          DataSet = frxDsEquiEnt
          DataSetName = 'frxDsEquiEnt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEquiEnt."CELUENTI_TXT"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 453.543600000000000000
          Width = 113.385900000000000000
          Height = 18.897650000000000000
          DataField = 'TEL1ENTI_TXT'
          DataSet = frxDsEquiEnt
          DataSetName = 'frxDsEquiEnt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsEquiEnt."TEL1ENTI_TXT"]')
          ParentFont = False
        end
      end
      object ReportSummary1: TfrxReportSummary
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 249.448980000000000000
        Width = 718.110700000000000000
        object Memo7: TfrxMemoView
          Left = 585.827150000000000000
          Top = 7.559059999999990000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiEnt
          DataSetName = 'frxDsEquiEnt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[COUNT(MasterData1)]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 37.795300000000000000
          Top = 7.559059999999990000
          Width = 548.031850000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiEnt
          DataSetName = 'frxDsEquiEnt'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'CADASTROS ')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 306.141930000000000000
        Width = 718.110700000000000000
        object Memo9: TfrxMemoView
          Left = 37.795300000000000000
          Width = 642.520100000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsEquiEnt: TfrxDBDataset
    UserName = 'frxDsEquiEnt'
    CloseDataSource = False
    DataSet = QrEquiEnt
    BCDToCurrency = False
    Left = 158
    Top = 10
  end
  object frxDsEquiGru: TfrxDBDataset
    UserName = 'frxDsEquiGru'
    CloseDataSource = False
    DataSet = QrEquiGru
    BCDToCurrency = False
    Left = 74
    Top = 10
  end
  object QrRolComis: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT eco.Codigo, eco.Nome'
      'FROM equicom eco'
      'ORDER BY eco.Nome')
    Left = 444
    Top = 8
    object QrRolComisCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrRolComisNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsRolComis: TDataSource
    DataSet = QrRolComis
    Left = 472
    Top = 8
  end
  object QrCarteiras: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Codigo, car.Nome '
      'FROM carteiras car'
      'LEFT JOIN controle ctr ON ctr.Dono=car.ForneceI'
      'WHERE car.Tipo=0'
      'ORDER BY car.Nome')
    Left = 573
    Top = 77
    object QrCarteirasCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCarteirasNome: TWideStringField
      FieldName = 'Nome'
      Required = True
      Size = 100
    end
  end
  object DsCarteiras: TDataSource
    DataSet = QrCarteiras
    Left = 597
    Top = 81
  end
end
