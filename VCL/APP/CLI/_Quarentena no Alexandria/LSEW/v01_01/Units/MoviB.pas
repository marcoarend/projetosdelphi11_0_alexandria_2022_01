unit MoviB;
interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls, Mask, DBCtrls, Db, (*DBTables,*) JPEG, ExtDlgs,
  ZCF2, ResIntStrings, UnMLAGeral, UnGOTOy, UnInternalConsts, UnMsgInt,
  UnInternalConsts2, UMySQLModule, Grids, DBGrids, Menus, ComCtrls,
  mySQLDbTables, dmkPermissoes, dmkGeral, dmkLabel, UnDmkProcFunc;

type
  TFmMoviB = class(TForm)
    PnDados: TPanel;
    PainelConfirma: TPanel;
    BtTrava: TBitBtn;
    DsMoviB: TDataSource;
    DsMovim: TDataSource;
    BtIncluiIts: TBitBtn;
    BtAlteraIts: TBitBtn;
    BtExcluiIts: TBitBtn;
    DsArtigo: TDataSource;
    PainelDados1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    RGMerca: TRadioGroup;
    RGGrupo: TRadioGroup;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    PainelDados2: TPanel;
    Progress1: TProgressBar;
    Panel1: TPanel;
    Panel2: TPanel;
    PnRegistros: TPanel;
    Panel4: TPanel;
    PnTempo: TPanel;
    BtRefresh: TBitBtn;
    DBCheckBox1: TDBCheckBox;
    QrMoviB: TmySQLQuery;
    QrMovim: TmySQLQuery;
    QrArtigo: TmySQLQuery;
    QrMax: TmySQLQuery;
    QrSoma: TmySQLQuery;
    QrMoviBPeriodo2: TWideStringField;
    PainelTitulo: TPanel;
    LaTipo: TLabel;
    Image1: TImage;
    PanelFill2: TPanel;
    SbImprime: TBitBtn;
    SbNovo: TBitBtn;
    SbNumero: TBitBtn;
    SbNome: TBitBtn;
    SbQuery: TBitBtn;
    PainelControle: TPanel;
    LaRegistro: TdmkLabel;
    Panel5: TPanel;
    SpeedButton4: TBitBtn;
    SpeedButton3: TBitBtn;
    SpeedButton2: TBitBtn;
    SpeedButton1: TBitBtn;
    Panel3: TPanel;
    BtSaida: TBitBtn;
    BtExclui: TBitBtn;
    BtAltera: TBitBtn;
    BtInclui: TBitBtn;
    QrArtigoCodigo: TIntegerField;
    QrArtigoNome: TWideStringField;
    QrMaxPeriodo: TIntegerField;
    BtPrivativo: TBitBtn;
    QrProdutos: TmySQLQuery;
    QrProdutosCodigo: TIntegerField;
    QrProdutosEstqQ: TFloatField;
    QrProdutosEstqV: TFloatField;
    QrMoviBPeriodo: TIntegerField;
    QrMoviBQtd: TFloatField;
    QrMoviBVal: TFloatField;
    QrMoviBLk: TIntegerField;
    QrMoviBDataCad: TDateField;
    QrMoviBDataAlt: TDateField;
    QrMoviBUserCad: TIntegerField;
    QrMoviBUserAlt: TIntegerField;
    QrMovimNOMEGRUPO: TWideStringField;
    QrMovimNOMEGRADE: TWideStringField;
    QrMovimNOMETAM: TWideStringField;
    QrMovimNOMECOR: TWideStringField;
    QrMovimQtd: TFloatField;
    QrMovimVal: TFloatField;
    QrAtualiza: TmySQLQuery;
    QrAtualizaCodigo: TIntegerField;
    QrAtualizaCor: TIntegerField;
    QrAtualizaTam: TIntegerField;
    QrSomaQtd: TFloatField;
    QrSomaVal: TFloatField;
    QrMoviBControle: TIntegerField;
    QrMovimCUSTO: TFloatField;
    DBGItens: TDBGrid;
    dmkPermissoes1: TdmkPermissoes;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure BtAlteraClick(Sender: TObject);
    procedure BtSaidaClick(Sender: TObject);
    procedure BtTravaClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SbNumeroClick(Sender: TObject);
    procedure SbNomeClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrMoviBAfterOpen(DataSet: TDataSet);
    procedure FormActivate(Sender: TObject);
    procedure BtIncluiClick(Sender: TObject);
    procedure QrMoviBCalcFields(DataSet: TDataSet);
    procedure DBGItensKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure BtIncluiItsClick(Sender: TObject);
    procedure BtAlteraItsClick(Sender: TObject);
    procedure QrMovimAfterOpen(DataSet: TDataSet);
    procedure BtExcluiItsClick(Sender: TObject);
    procedure RGMercaClick(Sender: TObject);
    procedure RGGrupoClick(Sender: TObject);
    procedure BtExcluiClick(Sender: TObject);
    procedure BtRefreshClick(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure SbImprimeClick(Sender: TObject);
    procedure QrMoviBAfterScroll(DataSet: TDataSet);
    procedure BtPrivativoClick(Sender: TObject);
    procedure QrMovimCalcFields(DataSet: TDataSet);
  private

    procedure CriaOForm;
    procedure DefineONomeDoForm;
    procedure QueryPrincipalAfterOpen;
    procedure IncluiRegistro;
    procedure AlteraRegistro;
   ////Procedures do form
    procedure EditaProduto(Tipo: String);
    procedure SomaBalanco;

  public
    { Public declarations }

    procedure DefParams;
    procedure LocCod(Atual, Codigo: Integer);
    procedure Va(Para: TVaiPara);
    procedure MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
    //
    procedure ReindexaTabela(Locate: Boolean);
  end;

var
  FmMoviB: TFmMoviB;

implementation

uses Module, Principal, ModuleProd, MoviBNew, MoviBIts, UnDmkEnums, UnMyObjects;//, PQx, MoviBNew, MoviBEdit, ;

{$R *.DFM}

var
  TimerIni: TDateTime;

/////////////////////////////////////////////////////////////////////////////////////
procedure TFmMoviB.LocCod(Atual, Codigo: Integer);
begin
  DefParams;
  GOTOy.LC(Atual, Codigo);
end;

procedure TFmMoviB.Va(Para: TVaiPara);
begin
  DefParams;
  LaRegistro.Caption := GOTOy.Go(Para, QrMoviBPeriodo.Value, LaRegistro.Caption[2]);
end;
/////////////////////////////////////////////////////////////////////////////////////

procedure TFmMoviB.DefParams;
begin
  VAR_GOTOTABELA := 'movib';
  VAR_GOTOMYSQLTABLE := QrMoviB;
  VAR_GOTONEG := gotoPos;
  VAR_GOTOCAMPO := CO_PERIODO;
  VAR_GOTONOME := '';//CO_NOME;
  VAR_GOTOMYSQLDBNAME := Dmod.MyDB;
  VAR_GOTOVAR := 0;

  GOTOy.LimpaVAR_SQL;
  VAR_SQLx.Add('SELECT *');
  VAR_SQLx.Add('FROM movib');
  VAR_SQLx.Add('WHERE Periodo > 0');
  //
  VAR_SQL1.Add('AND Periodo=:P0');
  //
  VAR_SQLa.Add(''); //AND Nome Like :P0
  //
end;

procedure TFmMoviB.MostraEdicao(Mostra : Boolean; Status : String; Codigo : Integer);
begin
  if Mostra then
  begin
    PainelControle.Visible:=False;
    PainelConfirma.Visible:=True;
  end else begin
    PainelControle.Visible:=True;
    PainelConfirma.Visible:=False;
  end;
  LaTipo.Caption := Status;
  GOTOy.BotoesSb(LaTipo.Caption);
end;

procedure TFmMoviB.CriaOForm;
begin
  DefineONomeDoForm;
  DefParams;
  Va(vpLast);
end;

procedure TFmMoviB.AlteraRegistro;
begin
  MostraEdicao(True, CO_INCLUSAO, 0);
  UMyMod.UpdLockY(QrMoviBPeriodo.Value, Dmod.MyDB, 'movib', 'periodo');
end;

procedure TFmMoviB.IncluiRegistro;
var
  Cursor : TCursor;
begin
  Cursor := Screen.Cursor;
  Screen.Cursor := crHourglass;
  Refresh;
  try
    MyObjects.CriaForm_AcessoTotal(TFmMoviBNew, FmMoviBNew);
  finally
    Screen.Cursor := Cursor;
  end;
  FmMoviBNew.ShowModal;
  FmMoviBNew.Destroy;
end;

procedure TFmMoviB.QueryPrincipalAfterOpen;
begin
  if (QrMoviBPeriodo.Value = DmProd.VerificaBalanco(* +1 ? *)) then
    BtAltera.Enabled := True
  else
  begin
    QrMax.Close;
    QrMax.Open;
    if QrMaxPeriodo.Value <> QrMoviBPeriodo.Value then
    begin
      BtAltera.Enabled := False;
      ReindexaTabela(False);
    end else BtAltera.Enabled := True;
  end;
  ///
end;

procedure TFmMoviB.DefineONomeDoForm;
begin
end;

///////////////////// FIM DAS MINHAS DECLARA��ES///////////////////

procedure TFmMoviB.SpeedButton1Click(Sender: TObject);
begin
  Va(vpFirst);
end;

procedure TFmMoviB.SpeedButton2Click(Sender: TObject);
begin
  Va(vpPrior);
end;

procedure TFmMoviB.SpeedButton3Click(Sender: TObject);
begin
  Va(vpNext);
end;

procedure TFmMoviB.SpeedButton4Click(Sender: TObject);
begin
  Va(vpLast);
end;

procedure TFmMoviB.BtAlteraClick(Sender: TObject);
begin
  AlteraRegistro;
end;

procedure TFmMoviB.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMoviB.BtTravaClick(Sender: TObject);
var
  Periodo : Integer;
  QtdX, ValX, CusX: Double;
begin
  SomaBalanco;
  Periodo := QrMoviBPeriodo.Value;
  UMyMod.UpdUnlockY(Periodo, Dmod.MyDB, 'movib', 'periodo');
  MostraEdicao(False, CO_TRAVADO, 0);
  LocCod(Periodo, Periodo);
  //
  try
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('UPDATE produtos SET EstqQ=0, EstqV=0, EstqC=0, PediQ=0');
    Dmod.QrUpdU.ExecSQL;
    //
    QrAtualiza.Close;
    QrAtualiza.Open;
    //
    Progress1.Position := 0;
    Progress1.Max := GOTOy.Registros(QrAtualiza);
    PainelDados2.Visible := True;
    PainelDados2.Refresh;
    PainelControle.Refresh;
    Panel1.Refresh;
    Panel2.Refresh;
    TimerIni := Now();
    while not QrAtualiza.Eof do
    begin
      Progress1.Position := Progress1.Position + 1;
      PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
      PnRegistros.Refresh;
      PnTempo.Caption := FormatDateTime(VAR_FORMATTIME, Now() - TimerIni)+'  ';
      PnTempo.Refresh;
      DmProd.AtualizaEstoqueMercadoria(QrAtualizaCodigo.Value,
        QrAtualizaCor.Value, QrAtualizaTam.Value, True, True, QtdX, ValX, CusX);
      QrAtualiza.Next;
    end;
    Application.MessageBox('Atualiza��o de estoques finalizada', 'Informa��o',
    MB_OK+MB_ICONINFORMATION);
    PainelDados2.Visible := False;
    QrAtualiza.Close;
    //if QrMoviBConfirmado.Value = 'F' then FmPrincipal.RefreshMercadorias;
  except
    raise;
    Application.MessageBox('Ocorreu um erro nas atualiza��es de estoque.',
    'Erro', MB_OK+MB_ICONERROR);
  end;
end;

procedure TFmMoviB.FormCreate(Sender: TObject);
begin
  CriaOForm;
end;

procedure TFmMoviB.SbNumeroClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Codigo(QrMoviBPeriodo.Value, LaRegistro.Caption);
end;

procedure TFmMoviB.SbNomeClick(Sender: TObject);
begin
  LaRegistro.Caption := GOTOy.Nome(LaRegistro.Caption);
end;

procedure TFmMoviB.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  VAR_PERIODOBAL := MLAGeral.PrimeiroDiaDoPeriodo(
    DmProd.VerificaBalanco, dtSystem);
  Action := GOTOy.Fechar(LaTipo.Caption);
end;

procedure TFmMoviB.QrMoviBAfterOpen(DataSet: TDataSet);
begin
  GOTOy.BtEnabled(QrMoviBPeriodo.Value, False);
  QueryPrincipalAfterOpen;
end;

procedure TFmMoviB.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmMoviB.BtIncluiClick(Sender: TObject);
begin
  BtTravaClick(Self);
  IncluiRegistro;
end;

procedure TFmMoviB.QrMoviBCalcFields(DataSet: TDataSet);
begin
  QrMoviBPeriodo2.Value := ' Balan�o de '+
  UPPERCASE(MLAGeral.PrimeiroDiaDoPeriodo(QrMoviBPeriodo.Value, dtTexto));
end;

procedure TFmMoviB.DBGItensKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if LaTipo.Caption <> CO_TRAVADO then
  begin
    if (key=13)        then EditaProduto(CO_ALTERACAO);
    if (key=VK_INSERT) then EditaProduto(CO_INCLUSAO);
  end;
end;

procedure TFmMoviB.ReindexaTabela(Locate: Boolean);
var
  //Indice, Produto : Integer;
  Ordem, Descricao: String;
  //Fornecedo: Double;
begin
  Ordem := '';
  (*Indice := (RGMerca.ItemIndex*10)+ RGGrupo.ItemIndex;
  case Indice of
     0 : Ordem := 'ORDER BY pg.Codigo, ar.Nome';
     1 : Ordem := 'ORDER BY ar.Nome';
    10 : Ordem := 'ORDER BY pg.Codigo, cu.Produto';
    11 : Ordem := 'ORDER BY cu.Produto';
  end;*)
  //
  //Fornecedo := VAR_LOCATE011;
  Descricao := VAR_LOCATE012;
  //Produto   := VAR_LOCATE014;
  //
  QrMovim.Close;
  QrMovim.SQL.Clear;
  QrMovim.SQL.Add('SELECT grg.Nome NOMEGRUPO, gra.Nome NOMEGRADE,');
  QrMovim.SQL.Add('tam.Nome NOMETAM, cor.Nome NOMECOR,');
  QrMovim.SQL.Add('pro.Qtd, pro.Val');
  QrMovim.SQL.Add('FROM movim pro');
  QrMovim.SQL.Add('LEFT JOIN grades   gra ON gra.Codigo=pro.Grade');
  QrMovim.SQL.Add('LEFT JOIN tamanhos tam ON tam.Codigo=pro.Tam');
  QrMovim.SQL.Add('LEFT JOIN cores    cor ON cor.Codigo=pro.Cor');
  QrMovim.SQL.Add('LEFT JOIN gradeg   grg ON grg.Codigo=gra.GradeG');
  QrMovim.SQL.Add('WHERE Controle= :P0');
  QrMovim.SQL.Add('');

  //ORDER BY NOMEGRUPO, NOMEGRADE, NOMECOR, NOMETAM
  QrMovim.SQL.Add(Ordem);
  QrMovim.Params[0].AsInteger := QrMoviBControle.Value;
  QrMovim.Open;
end;

procedure TFmMoviB.EditaProduto(Tipo: String);
begin
  MyObjects.CriaForm_AcessoTotal(TFmMoviBIts, FmMoviBIts);
  FmMoviBIts.FMoviBControle := QrMoviBControle.Value;
  FmMoviBIts.ShowModal;
  FmMoviBIts.Destroy;
end;

procedure TFmMoviB.BtIncluiItsClick(Sender: TObject);
begin
  EditaProduto(CO_INCLUSAO);
end;

procedure TFmMoviB.BtAlteraItsClick(Sender: TObject);
begin
  EditaProduto(CO_ALTERACAO);
end;

procedure TFmMoviB.QrMovimAfterOpen(DataSet: TDataSet);
begin
  if QrMovim.RecordCount > 0 then
  begin
    BtAlteraIts.Enabled := True;
    BtExclui.Enabled := False;
  end else begin
    BtAlteraIts.Enabled := False;
    BtExclui.Enabled := Geral.IntToBool_0(QrMoviB.RecordCount);
  end;
end;

procedure TFmMoviB.SomaBalanco;
begin
  QrSoma.Close;
  QrSoma.Params[0].AsInteger := QrMoviBPeriodo.Value;
  QrSoma.Open;
  //
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('UPDATE movib SET ');
  Dmod.QrUpdU.SQL.Add('Qtd=:P0, Val=:P1 ');
  Dmod.QrUpdU.SQL.Add('WHERE Periodo=:P2');
  Dmod.QrUpdU.Params[0].AsFloat   := QrSomaQtd.Value;
  Dmod.QrUpdU.Params[1].AsFloat   := QrSomaVal.Value;
  Dmod.QrUpdU.Params[2].AsInteger := QrMoviBControle.Value;
  Dmod.QrUpdU.ExecSQL;
  //
  LocCod(QrMoviBPeriodo.Value, QrMoviBPeriodo.Value);
end;

procedure TFmMoviB.BtExcluiItsClick(Sender: TObject);
//var
  //Periodo, Insumo, Controle: Integer;
begin
  (*QrMovim.DisableControls;
  if Application.MessageBox('Deseja excluir este item de balan�o?', PChar(VAR_APPNAME),
  MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
  MB_APPLMODAL) = ID_YES then
  begin
    Periodo  := QrMoviBPeriodo.Value;
    Insumo   := QrMovimInsumo.Value;
    Controle := QrMovimOrigemCtrl.Value;
    //
    QrMovim.Prior;
    if QrMovimOrigemCtrl.Value = Controle then QrMovim.Next;
    //VAR_LOCATE011 := QrMovimFORNECEDO.Value;
    VAR_LOCATE014 := QrMovimInsumo.Value;
    //
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM pqx WHERE Tipo=0 AND Insumo=:P0 ');
    Dmod.QrUpdU.SQL.Add('AND OrigemCodi=:P1 AND OrigemCtrl=:P2');
    Dmod.QrUpdU.Params[0].AsInteger := Insumo;
    Dmod.QrUpdU.Params[1].AsInteger := Periodo;
    Dmod.QrUpdU.Params[2].AsInteger := Controle;
    Dmod.QrUpdU.ExecSQL;
  end;
  //
  QrMovim.EnableControls;
  SomaBalanco;*)
end;

procedure TFmMoviB.RGMercaClick(Sender: TObject);
begin
  ReindexaTabela(True);
end;

procedure TFmMoviB.RGGrupoClick(Sender: TObject);
begin
  ReindexaTabela(True);
end;

procedure TFmMoviB.BtExcluiClick(Sender: TObject);
var
  Periodo: Integer;
begin
  if Application.MessageBox('Deseja excluir este balan�o?', PChar(VAR_APPNAME),
  MB_ICONQUESTION+MB_YESNO+MB_DEFBUTTON1+
  MB_APPLMODAL) = ID_YES then
  begin
    Periodo := QrMoviBPeriodo.Value;
    //
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM movib WHERE Periodo=:P0 ');
    Dmod.QrUpdU.Params[0].AsInteger := Periodo;
    Dmod.QrUpdU.ExecSQL;
    Periodo := QrMoviBPeriodo.Value;
    LocCod(Periodo, Periodo);
    if QrMoviBPeriodo.Value = Periodo then BtTravaClick(Self);
    MLAGeral.LoadTextBitmapToPanel(0, 0, QrMoviBPeriodo2.Value, Image1, PainelTitulo, True, 5);
    // Se n�o houver invent�rio atualiza estoque assim mesmo
    Va(vpLast);
    VAR_PERIODOBAL := MLAGeral.PrimeiroDiaDoPeriodo(
      DmProd.VerificaBalanco, dtSystem);
    if QrMoviBPeriodo.Value = 0 then BtTravaClick(Self);
  end;
end;

procedure TFmMoviB.BtRefreshClick(Sender: TObject);
//var
  //Periodo: Integer;
  //Sit: String;
begin
  (*if QrMoviBConfirmado.Value = 'V' then Sit := 'F' else Sit := 'V';
  Periodo := QrMoviBPeriodo.Value;
  Dmod.QrUpdM.SQL.Clear;
  Dmod.QrUpdM.SQL.Add('UPDATE movib SET Confirmado="'+Sit
  +'" WHERE Periodo='+IntToStr(Periodo));
  Dmod.QrUpdM.ExecSQL;
  LocCod(Periodo, Periodo);*)
end;

procedure TFmMoviB.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, QrMoviBPeriodo2.Value, Image1, PainelTitulo, True, 5);
end;

procedure TFmMoviB.SbImprimeClick(Sender: TObject);
begin
  //FmPrincipal.Relatrios1Click(Self);
end;

procedure TFmMoviB.QrMoviBAfterScroll(DataSet: TDataSet);
begin
  PainelTitulo.Caption := '                              '+QrMoviBPeriodo2.Value;
  MLAGeral.LoadTextBitmapToPanel(0, 0, QrMoviBPeriodo2.Value, Image1, PainelTitulo, True, 5);
  ReindexaTabela(False);
end;

procedure TFmMoviB.BtPrivativoClick(Sender: TObject);
//var
  //Conta: Double;
begin
  (*if Geral.MB_Pergunta('Todos itens deste balan�o ser�o excluidos, '
    + 'e ser�o inclu�dos itens para zerar estoques negativos. Deseja executar '+
    'esta a��o assim mesmo?') = ID_YES then
  begin
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('DELETE FROM movibits WHERE Periodo=:P0 ');
    Dmod.QrUpdU.Params[0].AsInteger := QrMoviBPeriodo.Value;
    Dmod.QrUpdU.ExecSQL;
    //
  end;
  Dmod.QrUpdU.SQL.Clear;
  Dmod.QrUpdU.SQL.Add('UPDATE produtos SET EstqQ=0, EstqV=0');
  Dmod.QrUpdU.ExecSQL;

  //////////////////////////////////////////////////////////////////////////////

  QrAtualiza.Close;
  QrAtualiza.Open;
  Progress1.Position := 0;
  Progress1.Max := QrAtualiza.RecordCount;
  PainelDados2.Visible := True;
  PainelDados2.Refresh;
  PainelControle.Refresh;
  Panel1.Refresh;
  Panel2.Refresh;
  TimerIni := Now();
  while not QrAtualiza.Eof do
  begin
    Progress1.Position := Progress1.Position + 1;
    PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
    PnRegistros.Refresh;
    PnTempo.Caption := FormatDateTime(VAR_FORMATTIME, Now() - TimerIni)+'  ';
    PnTempo.Refresh;
    DmProd.AtualizaEstoquePQ(QrAtualizaProduto.Value, aeNenhum, CO_VAZIO);
    QrAtualiza.Next;
  end;
  //PainelDados2.Visible := False;
  QrAtualiza.Close;

  //////////////////////////////////////////////////////////////////////////////

  QrProdutos.Close;
  QrProdutos.Open;
  Progress1.Position := 0;
  Progress1.Max := QrProdutos.RecordCount;
  while not QrProdutos.Eof do
  begin
    Progress1.Position := Progress1.Position + 1;
    PnRegistros.Caption := IntToStr(Progress1.Position)+'  ';
    PnRegistros.Refresh;
    PnTempo.Caption := FormatDateTime(VAR_FORMATTIME, Now() - TimerIni)+'  ';
    PnTempo.Refresh;
    //
    Conta := UMyMod.BuscaEmLivreY_Double(
    Dmod.MyDB, 'Livres', 'Controle', 'MoviBIts','MoviBIts','Conta');
    Dmod.QrUpdU.SQL.Clear;
    Dmod.QrUpdU.SQL.Add('INSERT INTO movibits SET EstqQ=:P0, ');
    Dmod.QrUpdU.SQL.Add('EstqV=:P1, Produto=:P2, Periodo=:P3, Conta=:P4');
    Dmod.QrUpdU.Params[00].AsFloat   := -QrProdutosEstqQ.Value;
    Dmod.QrUpdU.Params[01].AsFloat   := -QrProdutosEstqV.Value;
    Dmod.QrUpdU.Params[02].AsInteger :=  QrProdutosCodigo.Value;
    Dmod.QrUpdU.Params[03].AsInteger :=  QrMoviBPeriodo.Value;
    Dmod.QrUpdU.Params[04].AsFloat   :=  Conta;
    Dmod.QrUpdU.ExecSQL;
    QrProdutos.Next;
  end;
  //////////////////////////////////////////////////////////////////////////////
  QrProdutos.Close;
  PainelDados2.Visible := False;
  LocCod(QrMoviBPeriodo.Value, QrMoviBPeriodo.Value);*)
end;

procedure TFmMoviB.QrMovimCalcFields(DataSet: TDataSet);
begin
  if QrMovimQtd.Value = 0 then QrMovimCUSTO.Value := 0 else
  QrMovimCUSTO.Value := QrMovimVal.Value / QrMovimQtd.Value;
end;

end.





