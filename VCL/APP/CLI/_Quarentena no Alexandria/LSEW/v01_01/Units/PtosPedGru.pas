unit PtosPedGru;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, Grids, ComCtrls,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkGeral, Variants,
  dmkLabel, Mask, UnDmkEnums;

type
  TFmPtosPedGru = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrProdutos: TmySQLQuery;
    DsProdutos: TDataSource;
    PnSeleciona: TPanel;
    EdGraGru1: TdmkEditCB;
    CBGraGru1: TdmkDBLookupComboBox;
    Label1: TLabel;
    PageControl1: TPageControl;
    TabSheet3: TTabSheet;
    GradeA: TStringGrid;
    StaticText2: TStaticText;
    TabSheet5: TTabSheet;
    GradeQ: TStringGrid;
    StaticText1: TStaticText;
    TabSheet6: TTabSheet;
    GradeC: TStringGrid;
    StaticText6: TStaticText;
    TabSheet9: TTabSheet;
    GradeX: TStringGrid;
    QrLoc: TmySQLQuery;
    QrLocUltimo: TIntegerField;
    PB1: TProgressBar;
    TabSheet1: TTabSheet;
    GradeL: TStringGrid;
    LaTipo: TdmkLabel;
    TabSheet4: TTabSheet;
    GradeD: TStringGrid;
    StaticText3: TStaticText;
    QrLista: TmySQLQuery;
    QrListaGraCusPrc: TIntegerField;
    QrProdutosCodigo: TIntegerField;
    QrProdutosNome: TWideStringField;
    TabSheet2: TTabSheet;
    GradeF: TStringGrid;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdGraGru1Change(Sender: TObject);
    procedure EdGraGru1Exit(Sender: TObject);
    procedure GradeQDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeCDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure BtOKClick(Sender: TObject);
    procedure GradeQDblClick(Sender: TObject);
    procedure GradeQKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GradeLDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeFDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeFKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure LaTipoTextChange(Sender: TObject);
    procedure EdGraGru1Enter(Sender: TObject);
    procedure GradeDDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure GradeDKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GradeDClick(Sender: TObject);
    procedure GradeDSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure GradeXDrawCell(Sender: TObject; ACol, ARow: Integer; Rect: TRect;
      State: TGridDrawState);
    procedure CBGraGru1Exit(Sender: TObject);
    procedure PageControl1Changing(Sender: TObject; var AllowChange: Boolean);
  private
    { Private declarations }
    FOldNivel1: Integer;
    procedure ReconfiguraGradeQ();
    procedure ReconfiguraGradeF();
    // Qtde de itens
    function QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
    function QtdeVariosItensCorTam(GridP, GridA, GridC, GridX: TStringGrid): Boolean;
    function ObtemQtde(var Qtde: Double): Boolean;
    //
    function DescontoVariosItensCorTam(GridD, GridA, GridC, GridX:
             TStringGrid): Boolean;
    function ObtemDesconto(var Desconto: Double): Boolean;
  public
    { Public declarations }
  end;

  var
  FmPtosPedGru: TFmPtosPedGru;

implementation

uses Module, MyVCLSkin, GetValor, UMySQLModule, UnInternalConsts,
  UnMyObjects, GraGruVal, MyDBCheck, PtosPedCad, ModuleProd, ModuleGeral;

{$R *.DFM}

procedure TFmPtosPedGru.BtOKClick(Sender: TObject);
const
  Quanti = 1;
  Status = 0;
  CodOut = 0;
  TipOut = 0;
var
  //PrecoF, DescoI, DescoV, ValBru, ValLiq,
  Tot, PrecoO, PrecoR, {QuantP,}
  DescoP: Double;
  DataIns{, SQL}: String;
  Qtd, Col, Row, J, Nivel1, GraGruX, CodInn{, Controle}, Empresa, IDCtrl,
  ErrPreco, AvisoPrc, QtdRed{, Casas}: Integer;
begin
  ReconfiguraGradeF();
  if EdGraGru1.ValueVariant = 0 then
  begin
    Application.MessageBox('Defina o produto!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  Nivel1 := QrProdutosCodigo.Value;
  Screen.Cursor := crHourGlass;
  try
    DataIns := FormatDateTime('yyyy-mm-dd hh:nn:ss', DModG.ObtemAgora());
    Empresa   := FmPtosPedCad.QrPtosPedCadPontoVda.Value;
    CodInn    := FmPtosPedCad.QrPtosPedCadCodigo.Value;
    Tot       := 0;
    ErrPreco  := 0;
    AvisoPrc  := 0;
    QtdRed    := 0;
    for Col := 1 to GradeQ.ColCount -1 do
    begin
      for Row := 1 to GradeQ.RowCount - 1 do
      begin
        Qtd := Geral.IMV(GradeQ.Cells[Col,Row]);
        Tot := Tot + Qtd;
        if Qtd > 0 then
          QtdRed := QtdRed + 1;
        PrecoO := Geral.DMV(GradeL.Cells[Col,Row]);
        PrecoR := Geral.DMV(GradeF.Cells[Col,Row]);
        if (PrecoO <>  PrecoR) and (Qtd = 0) then
          AvisoPrc := AvisoPrc + 1;
        if (Qtd > 0) and (PrecoR = 0) then
          ErrPreco := ErrPreco + 1;
      end;
    end;
    if Tot = 0 then
    begin
      Screen.Cursor := crDefault;
      Application.MessageBox(
        'N�o h� defini��o de quantidades!', 'Aviso', MB_OK+MB_ICONWARNING);
      Exit;
    end
    else if ErrPreco > 0 then
    begin
      Screen.Cursor := crDefault;
      Geral.MB_Aviso('Existem ' + Geral.FF0(ErrPreco) +
        ' itens sem pre�o de faturamento definido!');
      Exit;
    end
    else if AvisoPrc > 0 then
    begin
      if Geral.MB_Pergunta('Existem ' + Geral.FF0(AvisoPrc) +
        ' itens com pre�o de faturamento alterado, mas n�o h� defini��o de ' +
        'quantidades de itens!' + sLineBreak +
        'Deseja continuar assim mesmo?') <> ID_YES then
      begin
        Screen.Cursor := crDefault;
        Exit;
      end;
    end;
    PB1.Position := 0;
    PB1.Max := QtdRed;
    //try
      //UMyMod.StartTransaction(Dmod.QrUpd);
      //Memo1.Lines.Add('START TRANSACTION;');
      for Col := 1 to GradeQ.ColCount -1 do
      begin
        if Geral.IMV(GradeX.Cells[Col,0]) > 0 then
        begin
          for Row := 1 to GradeQ.RowCount - 1 do
          begin
            if Geral.IMV(GradeX.Cells[0,Row]) > 0 then
            begin
              Qtd := Geral.IMV(GradeQ.Cells[Col, Row]);
              if Qtd > 0 then
              begin
                for J := 1 to Qtd do
                begin
                  GraGruX := Geral.IMV(GradeC.Cells[Col, Row]);
                  if GraGruX > 0 then
                  begin
                    //Casas  := Dmod.QrControleCasasProd.Value;
                    PrecoO := Geral.DMV(GradeL.Cells[Col, Row]);
                    PrecoR := Geral.DMV(GradeF.Cells[Col, Row]);
                    //QuantP := Geral.DMV(GradeQ.Cells[Col, Row]);
                    DescoP := Geral.DMV(GradeD.Cells[Col, Row]);
                    {
                    DescoI := MLAGeral.FFF(PrecoR * DescoP / 100, Casas, siPositivo);
                    PrecoF := PrecoR - DescoI;
                    DescoV := DescoI * QuantP;
                    ValBru := PrecoR * QuantP;
                    ValLiq := ValBru - DescoV;
                    }
                    //
{
    IDCtrl  := UMyMod.BuscaEmLivreY_Def('stqmovits', 'IDCtrl', LaTipo.SQLType, 0);
    UMyMod.CarregaSQLInsUpd(
    //UMyMod.SQLInsUpd(
    Dmod.QrUpd, LaTipo.SQLType, 'stqmovits', False, [
    'DataIns', 'Empresa', 'Produto', 'Quanti', 'PrecoO', 'PrecoR',
    'DescoP', 'Status'], ['IDCtrl'], [
    DataIns, Empresa, GraGrux, Quanti, PrecoO, PrecoR,
    DescoP, Status], [IDCtrl], False, False);
    SQL := Dmod.QrUpd.SQL.Text;
    //
    Controle := UMyMod.BuscaEmLivreY_Def('ptospedits', 'Controle', LaTipo.SQLType, 0);
    UMyMod.CarregaSQLInsUpd(
    //UMyMod.SQLInsUpd(
    Dmod.QrUpd, LaTipo.SQLType, 'ptospedits', False, [
    'Codigo', 'StqMovIts'], ['Controle'], [
    Codigo, IDCTrl], [Controle], False, False);
    Dmod.QrUpd.SQL.Text := SQL + Dmod.QrUpd.SQL.Text;
    Dmod.QrUpd.ExecSQL;
}                    // Parei aqui !  Fazer
IDCtrl := UMyMod.BuscaEmLivreY_Def('ptosstqmov', 'IDCtrl', LaTipo.SQLType, 0);
if UMyMod.SQLInsUpd(Dmod.QrUpd, LaTipo.SQLType, 'ptosstqmov', False, [
'CodInn', 'CodOut', 'TipOut', 'DataIns', 'Empresa', 'Produto',
'Quanti', 'PrecoO', 'PrecoR', 'DescoP', 'Status'], ['IDCtrl'], [
CodInn, CodOut, TipOut, DataIns, Empresa, GraGruX,
Quanti, PrecoO, PrecoR, DescoP, Status], [IDCtrl], False) then

                    FmPtosPedCad.ReopenPtosPedGru(0);
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
      //UMyMod.Commit(Dmod.QrUpd);
      //Memo1.Lines.Add('COMMIT;');
    //except
      //Memo1.Lines.Add('ROLLBACK;');
      //UMyMod.Rollback(Dmod.QrUpd);
      //raise;
    //end;
    FmPtosPedCad.ReopenPtosPedGru(Nivel1);
    EdGraGru1.ValueVariant := 0;
    EdGraGru1.SetFocus;
  finally
    Screen.Cursor := crDefault;
  end;
end;

procedure TFmPtosPedGru.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPtosPedGru.CBGraGru1Exit(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  GradeQ.SetFocus;
  GradeQ.Col := 1;
  GradeQ.Row := 1;
end;

function TFmPtosPedGru.DescontoVariosItensCorTam(GridD, GridA, GridC,
  GridX: TStringGrid): Boolean;
var
  Coluna, Linha, c, l, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL: Integer;
  Desconto: Double;
  DescontoTxt, ItensTxt: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridD.Col;
  Linha  := GridD.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridD.ColCount - 1;
    RowI := 1;
    RowF := GridD.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridD.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridD.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  Desconto  := Geral.DMV(GridD.Cells[ColI, RowI]);
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  if Ativos = 0 then
  begin
    Geral.MB_Aviso('N�o h� nenhum item com c�digo na sele��o ' +
      'para que se possa incluir / alterar o pre�o!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if ObtemDesconto(Desconto) then
  begin
    DescontoTxt := Geral.FFT(Desconto, 2, siNegativo);
    if (Coluna = 0) and (Linha = 0) then
    begin
      ItensTxt := 'todo grupo';
    end else if Coluna = 0 then
    begin
      ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
    end else if Linha = 0 then
    begin
      ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
    end;
    if Geral.MB_Pergunta('Confirma o valor de ' + DescontoTxt + ' para ' +
      ItensTxt + '?') <> ID_YES then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    //
    for c := ColI to ColF do
      for l := RowI to RowF do
        GridD.Cells[c, l] := DescontoTxt;
    Result := True;
  end;
  Screen.Cursor := crDefault;
end;

procedure TFmPtosPedGru.EdGraGru1Change(Sender: TObject);
begin
  if EdGraGru1.Focused = False then
    ReconfiguraGradeQ();
end;

procedure TFmPtosPedGru.EdGraGru1Enter(Sender: TObject);
begin
  FOldNivel1 := EdGraGru1.ValueVariant;
end;

procedure TFmPtosPedGru.EdGraGru1Exit(Sender: TObject);
begin
  if FOldNivel1 <> EdGraGru1.ValueVariant then
  begin
    FOldNivel1 := EdGraGru1.ValueVariant;
    ReconfiguraGradeQ();
  end;
end;

procedure TFmPtosPedGru.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPtosPedGru.FormCreate(Sender: TObject);
begin
  {
  ST1.Caption := '';
  ST2.Caption := '';
  ST3.Caption := '';
  ST1.Font.Color := clRed;
  ST2.Font.Color := clRed;
  ST3.Font.Color := clRed;
  ST1.Color := PnSeleciona.Color;
  ST2.Color := PnSeleciona.Color;
  ST3.Color := PnSeleciona.Color;
  }
  //
  PageControl1.ActivePageIndex := 0;
  GradeA.ColWidths[0] := 128;
  GradeC.ColWidths[0] := 128;
  GradeX.ColWidths[0] := 128;
  GradeQ.ColWidths[0] := 128;
  GradeL.ColWidths[0] := 128;
  GradeF.ColWidths[0] := 128;
  GradeD.ColWidths[0] := 128;
  //
  QrProdutos.Close;
  {
  QrProdutos.SQL.Clear;
  QrProdutos.SQL.Add('SELECT gg1.Codusu, gg1.Nivel3, gg1.Nivel2, gg1.Nivel1,');
  QrProdutos.SQL.Add('gg1.Nome, gg1.PrdGrupTip, gg1.GraTamCad,');
  QrProdutos.SQL.Add('gtc.Nome NOMEGRATAMCAD, gtc.CodUsu CODUSUGRATAMCAD,');
  QrProdutos.SQL.Add('gg1.CST_A, gg1.CST_B, gg1.UnidMed, gg1.NCM, gg1.Peso,');
  QrProdutos.SQL.Add('unm.Sigla SIGLAUNIDMED, unm.CodUsu CODUSUUNIDMED,');
  QrProdutos.SQL.Add('unm.Nome NOMEUNIDMED, ');
  QrProdutos.SQL.Add('CONCAT(LPAD(gg1.CodUsu, 8, "0"), " - ", gg1.Nome) NOME_EX ');
  QrProdutos.SQL.Add('FROM gragru1 gg1');
  QrProdutos.SQL.Add('LEFT JOIN gratamcad gtc ON gtc.Codigo=gg1.GraTamCad');
  QrProdutos.SQL.Add('LEFT JOIN unidmed   unm ON unm.Codigo=gg1.UnidMed');
  QrProdutos.SQL.Add('WHERE gg1.PrdGrupTip=1');
  QrProdutos.SQL.Add('');
  QrProdutos.SQL.Add('AND gg1.Nivel1 NOT IN (');
  QrProdutos.SQL.Add('  SELECT DISTINCT gg1.Nivel1');
  QrProdutos.SQL.Add('  FROM pedivdaits pvi');
  QrProdutos.SQL.Add('  LEFT JOIN gragrux   ggx ON ggx.Controle=pvi.GraGruX');
  QrProdutos.SQL.Add('  LEFT JOIN gragru1   gg1 ON gg1.Nivel1=ggx.GraGru1');
  QrProdutos.SQL.Add('  WHERE pvi.Codigo=:P0');
  QrProdutos.SQL.Add(')');
  QrProdutos.SQL.Add('');
  QrProdutos.SQL.Add('ORDER BY gg1.Nome');
  QrProdutos.Params[0].AsInteger := FmPtosPedCad.QrPtosPedCadCodigo.Value;
  }
  QrProdutos.Params[0].AsInteger := Dmod.QrControlePtosGradeD.Value;
  QrProdutos.Open;
  //
end;

procedure TFmPtosPedGru.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmPtosPedGru.GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeA(GradeA, ACol, ARow, Rect, State, True);
end;

procedure TFmPtosPedGru.GradeCDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeC, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmPtosPedGru.GradeDClick(Sender: TObject);
begin
  if (GradeD.Col = 0) or (GradeD.Row = 0) then
    DescontoVariosItensCorTam(GradeD, GradeA, GradeC, GradeX)
end;

procedure TFmPtosPedGru.GradeDDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeD, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc + ' %', 0, 0, False);
end;

procedure TFmPtosPedGru.GradeDKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInput(GradeD, GradeC, Key, Shift);
    ReconfiguraGradeF();
end;

procedure TFmPtosPedGru.GradeDSelectCell(Sender: TObject; ACol, ARow: Integer;
  var CanSelect: Boolean);
begin
  if (ACol = 0) or (ARow = 0) then
    GradeD.Options := GradeD.Options - [goEditing]
  else
    GradeD.Options := GradeD.Options + [goEditing];
end;

procedure TFmPtosPedGru.GradeFDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeF, GradeA, GradeL, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, False);
end;

procedure TFmPtosPedGru.GradeFKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInput(GradeF, GradeC, Key, Shift);
end;

procedure TFmPtosPedGru.GradeLDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeL, GradeA, nil, ACol, ARow, Rect, State,
  Dmod.FStrFmtPrc, 0, 0, True);
end;

procedure TFmPtosPedGru.GradeQDblClick(Sender: TObject);
begin
  if (GradeQ.Col = 0) or (GradeQ.Row = 0) then
    QtdeVariosItensCorTam(GradeQ, GradeA, GradeC, GradeX)
  else
    QtdeItemCorTam(GradeQ, GradeC, GradeX);
end;

procedure TFmPtosPedGru.GradeQDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeQ, GradeA, nil, ACol, ARow, Rect, State,
  '0', 0, 0, False);
end;

procedure TFmPtosPedGru.GradeQKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  MyObjects.PulaCelulaGradeInputEx(GradeQ, GradeC, Key, Shift, BtOK);
end;

procedure TFmPtosPedGru.GradeXDrawCell(Sender: TObject; ACol, ARow: Integer;
  Rect: TRect; State: TGridDrawState);
begin
  MyObjects.DesenhaGradeN(GradeX, nil, nil, ACol, ARow, Rect, State,
  '0', 0, 0, True);
end;

procedure TFmPtosPedGru.LaTipoTextChange(Sender: TObject);
begin
  if LaTipo.SQLType in ([stIns, stUpd]) then
  begin
    BtOK.Enabled   := True;
    GradeQ.Options := GradeQ.Options + [goEditing];
    GradeD.Options := GradeD.Options + [goEditing];
  end else begin
    BtOK.Enabled   := False;
    GradeQ.Options := GradeQ.Options - [goEditing];
    GradeD.Options := GradeQ.Options - [goEditing];
  end;
end;

procedure TFmPtosPedGru.ReconfiguraGradeF();
var
  C, L: Integer;
  Desco, Fator, Preco, Valor: Double;
begin
  for C := 1 to GradeL.ColCount - 1 do
    for L := 1 to GradeL.RowCount - 1 do
    begin
      Desco := Geral.DMV(GradeD.Cells[C,L]);
      Fator := 1 - (Desco / 100);
      Preco := Geral.DMV(GradeL.Cells[C,L]);
      //
      Valor := Preco * Fator;
      //
      GradeF.Cells[C,L] := FormatFloat(Dmod.FStrFmtPrc, Valor);
      //
    end;
end;

procedure TFmPtosPedGru.ReconfiguraGradeQ();
var
  Juros: Double;
  Grade, Lista: Integer;
begin
  Juros := 0;
  Grade := Geral.IMV(EdGraGru1.Text);
  PageControl1.Visible := Grade <> 0;
  Lista := FmPtosPedCad.QrPtosPedCadGraCusPrc.Value;
  DmProd.ConfigGrades001(Grade, Lista,
   GradeA, GradeX, GradeC, GradeL, GradeF, GradeQ, GradeD, Juros, LaTipo);
end;

function TFmPtosPedGru.ObtemDesconto(var Desconto: Double): Boolean;
begin
  Result := False;
  if DBCheck.CriaFm(TFmGraGruVal, FmGraGruVal, afmoNegarComAviso) then
  begin
    FmGraGruVal.EdValor.ValueVariant := Desconto;
    FmGraGruVal.ShowModal;
    Result   := FmGraGruVal.FConfirmou;
    Desconto := FmGraGruVal.EdValor.ValueVariant;
    FmGraGruVal.Destroy;
  end;
end;

function TFmPtosPedGru.ObtemQtde(var Qtde: Double): Boolean;
var
  ResVar: Variant;
begin
  Qtde := 0;
  Result := False;
  if MyObjects.GetValorDmk(TFmGetValor, FmGetValor, dmktfDouble,
  0, 3, 0, '', '', True, 'Itens', 'Informe a quantidade de itens: ',
  0, ResVar) then
  begin
    Qtde := Geral.IMV(ResVar);
    Result := True;
  end;
end;

procedure TFmPtosPedGru.PageControl1Changing(Sender: TObject;
  var AllowChange: Boolean);
begin
  if PageControl1.ActivePageIndex = 3 then
    ReconfiguraGradeF();
end;

function TFmPtosPedGru.QtdeItemCorTam(GridP, GridC, GridX: TStringGrid): Boolean;
var
  Qtde: Double;
  c, l, GraGruX, Coluna, Linha: Integer;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  c := Geral.IMV(GridX.Cells[Coluna, 0]);
  l := Geral.IMV(GridX.Cells[0, Linha]);
  GraGruX := Geral.IMV(GridC.Cells[Coluna, Linha]);
  if c = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if l = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if GraGruX = 0 then
  begin
    Application.MessageBox('O item nunca foi ativado!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if (c = 0) or (l = 0) or (GraGruX = 0) then Exit;
  //
  //Nivel1 := QrProdutosNivel1.Value;
  Qtde  := Geral.DMV(GridP.Cells[Coluna, Linha]);
  if ObtemQtde(Qtde) then
    GridP.Cells[Coluna, Linha] := FloatToStr(Qtde);
  Result := True;
  Screen.Cursor := crDefault;
end;

function TFmPtosPedGru.QtdeVariosItensCorTam(GridP, GridA, GridC, GridX:
TStringGrid): Boolean;
var
  Coluna, Linha, c, l, Ativos, ColI, ColF, RowI, RowF,
  CountC, CountL, GraGruX: Integer;
  Qtde: Double;
  QtdeTxt: String;
begin
  Result := False;
  Screen.Cursor := crHourGlass;
  Ativos := 0;
  RowI := 0;
  RowF := 0;
  ColI := 0;
  ColF := 0;
  Coluna := GridP.Col;
  Linha  := GridP.Row;
  if (Coluna = 0) and (Linha = 0) then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end else if Coluna = 0 then
  begin
    ColI := 1;
    ColF := GridP.ColCount - 1;
    RowI := Linha;
    RowF := Linha;
  end else if Linha = 0 then
  begin
    ColI := Coluna;
    ColF := Coluna;
    RowI := 1;
    RowF := GridP.RowCount - 1;
  end;
  //
  CountC := 0;
  CountL := 0;
  for c := ColI to ColF do
    for l := RowI to RowF do
  begin
    CountC := CountC + Geral.IMV(GridX.Cells[c, 0]);
    CountL := CountL + Geral.IMV(GridX.Cells[0, l]);
  end;
  if CountC = 0 then
  begin
    Application.MessageBox('Tamanho n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  if CountL = 0 then
  begin
    Application.MessageBox('Cor n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  //Nivel1 := QrProdutosCodigo.Value;
  Qtde   := Geral.IMV(GridP.Cells[ColI, RowI]);
  for c := ColI to ColF do
    for l := RowI to RowF do
      Ativos := Ativos + Geral.IMV(GridA.Cells[c, l]);
  if Ativos = 0 then
  begin
    Geral.MB_Aviso('N�o h� nenhum item com c�digo na sele��o ' +
      'para que se possa incluir / alterar o pre�o!');
    Screen.Cursor := crDefault;
    Exit;
  end;
  //
  if ObtemQtde(Qtde) then
  begin
    QtdeTxt := FloatToStr(Qtde);
    {
    if (Coluna = 0) and (Linha = 0) then
    begin
      ItensTxt := 'todo grupo';
    end else if Coluna = 0 then
    begin
      ItensTxt := 'todos tamanhos da cor ' + GridA.Cells[0, Linha];
    end else if Linha = 0 then
    begin
      ItensTxt := 'todas cores do tamanho ' + GridA.Cells[Coluna, 0];
    end;
    if Geral.MB_Pergunta('Confirma a quantidade de ' + QtdeTxt +
      ' para ' + ItensTxt + '?') <> ID_YES then
    begin
      Screen.Cursor := crDefault;
      Exit;
    end;
    //
    }
    for c := ColI to ColF do
      for l := RowI to RowF do
    begin
      GraGruX := Geral.IMV(GridC.Cells[c, l]);
      //
      if GraGruX > 0 then
        GradeQ.Cells[c,l] := QtdeTxt;
    end;
    Result := True;
  end;
  Screen.Cursor := crDefault;
end;

end.

