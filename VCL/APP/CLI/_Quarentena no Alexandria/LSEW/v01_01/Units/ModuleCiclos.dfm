object DmCiclos: TDmCiclos
  OldCreateOrder = False
  Height = 579
  Width = 870
  object QrMovimL: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMovimLCalcFields
    SQL.Strings = (
      'SELECT gra.Nome NOMEGRA, tam.Nome NOMETAM,'
      'cor.Nome NOMECOR, mom.*'
      'FROM movim mom'
      'LEFT JOIN grades gra ON gra.Codigo=mom.Grade'
      'LEFT JOIN tamanhos tam ON tam.Codigo=mom.Tam'
      'LEFT JOIN cores cor ON cor.Codigo=mom.Cor'
      'WHERE  mom.SubCtrl=1'
      'AND mom.Controle= :P0')
    Left = 32
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMovimLNOMEGRA: TWideStringField
      FieldName = 'NOMEGRA'
      Origin = 'grades.Nome'
      Size = 30
    end
    object QrMovimLNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'tamanhos.Nome'
      Size = 10
    end
    object QrMovimLNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'cores.Nome'
      Size = 30
    end
    object QrMovimLControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'movim.Controle'
      Required = True
    end
    object QrMovimLConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'movim.Conta'
      Required = True
    end
    object QrMovimLGrade: TIntegerField
      FieldName = 'Grade'
      Origin = 'movim.Grade'
      Required = True
    end
    object QrMovimLCor: TIntegerField
      FieldName = 'Cor'
      Origin = 'movim.Cor'
      Required = True
    end
    object QrMovimLTam: TIntegerField
      FieldName = 'Tam'
      Origin = 'movim.Tam'
      Required = True
    end
    object QrMovimLQtd: TFloatField
      FieldName = 'Qtd'
      Origin = 'movim.Qtd'
      Required = True
    end
    object QrMovimLVal: TFloatField
      FieldName = 'Val'
      Origin = 'movim.Val'
      Required = True
    end
    object QrMovimLVen: TFloatField
      FieldName = 'Ven'
      Origin = 'movim.Ven'
      Required = True
    end
    object QrMovimLDataPedi: TDateField
      FieldName = 'DataPedi'
      Origin = 'movim.DataPedi'
      Required = True
    end
    object QrMovimLDataReal: TDateField
      FieldName = 'DataReal'
      Origin = 'movim.DataReal'
      Required = True
    end
    object QrMovimLMotivo: TSmallintField
      FieldName = 'Motivo'
      Origin = 'movim.Motivo'
      Required = True
    end
    object QrMovimLSALDOQTD: TFloatField
      FieldName = 'SALDOQTD'
      Origin = 'movim.SALDOQTD'
      Required = True
    end
    object QrMovimLSALDOVAL: TFloatField
      FieldName = 'SALDOVAL'
      Origin = 'movim.SALDOVAL'
      Required = True
    end
    object QrMovimLLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'movim.Lk'
    end
    object QrMovimLDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'movim.DataCad'
    end
    object QrMovimLDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'movim.DataAlt'
    end
    object QrMovimLUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'movim.UserCad'
    end
    object QrMovimLUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'movim.UserAlt'
    end
    object QrMovimLAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'movim.AlterWeb'
      Required = True
    end
    object QrMovimLAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'movim.Ativo'
      Required = True
    end
    object QrMovimLPRECO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
      Calculated = True
    end
    object QrMovimLPOSIq: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIq'
      DisplayFormat = '#,###,##0.000'
      Calculated = True
    end
    object QrMovimLPOSIv: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIv'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMovimLSubCtrl: TIntegerField
      FieldName = 'SubCtrl'
      Origin = 'movim.SubCtrl'
      Required = True
    end
    object QrMovimLComisTip: TSmallintField
      FieldName = 'ComisTip'
      Origin = 'movim.ComisTip'
      Required = True
    end
    object QrMovimLComisFat: TFloatField
      FieldName = 'ComisFat'
      Origin = 'movim.ComisFat'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMovimLComisVal: TFloatField
      FieldName = 'ComisVal'
      Origin = 'movim.ComisVal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMovimLComisTipNOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ComisTipNOME'
      Size = 1
      Calculated = True
    end
  end
  object DsMovimL: TDataSource
    DataSet = QrMovimL
    Left = 88
  end
  object QrMovimT: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMovimTCalcFields
    SQL.Strings = (
      'SELECT gra.Nome NOMEGRA, tam.Nome NOMETAM,'
      'cor.Nome NOMECOR, mom.*'
      'FROM movim mom'
      'LEFT JOIN grades gra ON gra.Codigo=mom.Grade'
      'LEFT JOIN tamanhos tam ON tam.Codigo=mom.Tam'
      'LEFT JOIN cores cor ON cor.Codigo=mom.Cor'
      'WHERE  mom.SubCtrl=2'
      'AND mom.Controle= :P0')
    Left = 32
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMovimTNOMEGRA: TWideStringField
      FieldName = 'NOMEGRA'
      Size = 30
    end
    object QrMovimTNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 100
    end
    object QrMovimTNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 100
    end
    object QrMovimTControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMovimTConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrMovimTGrade: TIntegerField
      FieldName = 'Grade'
      Required = True
    end
    object QrMovimTCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrMovimTTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrMovimTQtd: TFloatField
      FieldName = 'Qtd'
      Required = True
    end
    object QrMovimTVal: TFloatField
      FieldName = 'Val'
      Required = True
    end
    object QrMovimTVen: TFloatField
      FieldName = 'Ven'
      Required = True
    end
    object QrMovimTDataPedi: TDateField
      FieldName = 'DataPedi'
      Required = True
    end
    object QrMovimTDataReal: TDateField
      FieldName = 'DataReal'
      Required = True
    end
    object QrMovimTMotivo: TSmallintField
      FieldName = 'Motivo'
      Required = True
    end
    object QrMovimTSALDOQTD: TFloatField
      FieldName = 'SALDOQTD'
      Required = True
    end
    object QrMovimTSALDOVAL: TFloatField
      FieldName = 'SALDOVAL'
      Required = True
    end
    object QrMovimTLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMovimTDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMovimTDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMovimTUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMovimTUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMovimTAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrMovimTAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrMovimTPRECO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
      Calculated = True
    end
    object QrMovimTPOSIq: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIq'
      DisplayFormat = '#,###,##0.000'
      Calculated = True
    end
    object QrMovimTPOSIv: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIv'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMovimTSubCtrl: TIntegerField
      FieldName = 'SubCtrl'
      Required = True
    end
    object QrMovimTComisTip: TSmallintField
      FieldName = 'ComisTip'
      Required = True
    end
    object QrMovimTComisFat: TFloatField
      FieldName = 'ComisFat'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMovimTComisVal: TFloatField
      FieldName = 'ComisVal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMovimTComisTipNOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ComisTipNOME'
      Size = 1
      Calculated = True
    end
  end
  object DsMovimT: TDataSource
    DataSet = QrMovimT
    Left = 88
    Top = 48
  end
  object DsMovimV: TDataSource
    DataSet = QrMovimV
    Left = 88
    Top = 96
  end
  object QrMovimV: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMovimVCalcFields
    SQL.Strings = (
      'SELECT gra.Nome NOMEGRA, tam.Nome NOMETAM,'
      'cor.Nome NOMECOR, mom.*'
      'FROM movim mom'
      'LEFT JOIN grades gra ON gra.Codigo=mom.Grade'
      'LEFT JOIN tamanhos tam ON tam.Codigo=mom.Tam'
      'LEFT JOIN cores cor ON cor.Codigo=mom.Cor'
      'WHERE  mom.SubCtrl=3'
      'AND mom.Controle= :P0')
    Left = 32
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMovimVNOMEGRA: TWideStringField
      FieldName = 'NOMEGRA'
      Size = 30
    end
    object QrMovimVNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 100
    end
    object QrMovimVNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 100
    end
    object QrMovimVControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMovimVConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrMovimVGrade: TIntegerField
      FieldName = 'Grade'
      Required = True
    end
    object QrMovimVCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrMovimVTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrMovimVQtd: TFloatField
      FieldName = 'Qtd'
      Required = True
    end
    object QrMovimVVal: TFloatField
      FieldName = 'Val'
      Required = True
    end
    object QrMovimVVen: TFloatField
      FieldName = 'Ven'
      Required = True
    end
    object QrMovimVDataPedi: TDateField
      FieldName = 'DataPedi'
      Required = True
    end
    object QrMovimVDataReal: TDateField
      FieldName = 'DataReal'
      Required = True
    end
    object QrMovimVMotivo: TSmallintField
      FieldName = 'Motivo'
      Required = True
    end
    object QrMovimVSALDOQTD: TFloatField
      FieldName = 'SALDOQTD'
      Required = True
    end
    object QrMovimVSALDOVAL: TFloatField
      FieldName = 'SALDOVAL'
      Required = True
    end
    object QrMovimVLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMovimVDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMovimVDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMovimVUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMovimVUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMovimVAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrMovimVAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrMovimVPRECO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
      Calculated = True
    end
    object QrMovimVPOSIq: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIq'
      DisplayFormat = '#,###,##0.000'
      Calculated = True
    end
    object QrMovimVPOSIv: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIv'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMovimVSubCtrl: TIntegerField
      FieldName = 'SubCtrl'
      Required = True
    end
    object QrMovimVComisTip: TSmallintField
      FieldName = 'ComisTip'
      Required = True
    end
    object QrMovimVComisFat: TFloatField
      FieldName = 'ComisFat'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMovimVComisVal: TFloatField
      FieldName = 'ComisVal'
      Required = True
      DisplayFormat = '#,###,##0.00'
    end
    object QrMovimVComisTipNOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ComisTipNOME'
      Size = 1
      Calculated = True
    end
  end
  object QrProfessores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(Tipo=0, RazaoSocial, Nome) NOMEPROFE, Codigo'
      'FROM entidades'
      'WHERE Fornece3="V"'
      'ORDER BY NOMEPROFE'
      '')
    Left = 32
    Top = 144
    object QrProfessoresNOMEPROFE: TWideStringField
      FieldName = 'NOMEPROFE'
      Required = True
      Size = 100
    end
    object QrProfessoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsProfessores: TDataSource
    DataSet = QrProfessores
    Left = 88
    Top = 144
  end
  object QrPromotores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT IF(Tipo=0, RazaoSocial, Nome) NOMEPROMO, Codigo'
      'FROM entidades'
      'WHERE Fornece2="V"'
      'ORDER BY NOMEPROMO'
      '')
    Left = 144
    Top = 144
    object QrPromotoresNOMEPROMO: TWideStringField
      FieldName = 'NOMEPROMO'
      Required = True
      Size = 100
    end
    object QrPromotoresCodigo: TIntegerField
      FieldName = 'Codigo'
    end
  end
  object DsPromotores: TDataSource
    DataSet = QrPromotores
    Left = 200
    Top = 144
  end
  object QrSProVen: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Val) VAL, SUM(Ven) VEN,'
      'SUM(Ven+Val) MARGEM, SUM(ComisVal) COMISVAL'
      'FROM movim mom'
      'WHERE mom.SubCtrl=3 '
      'AND mom.Controle=:P0'
      'AND (mom.Grade NOT IN ('
      '  SELECT eci.Produto  '
      '  FROM equicomits eci '
      '  WHERE eci.Codigo=:P1'
      '  AND eci.Compoe=1'
      '))')
    Left = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSProVenVAL: TFloatField
      FieldName = 'VAL'
    end
    object QrSProVenVEN: TFloatField
      FieldName = 'VEN'
    end
    object QrSProVenMARGEM: TFloatField
      FieldName = 'MARGEM'
    end
    object QrSProVenCOMISVAL: TFloatField
      FieldName = 'COMISVAL'
    end
  end
  object QrDespProf: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDespProfCalcFields
    SQL.Strings = (
      'SELECT car.Nome NOMECARTEIRA, con.Nome NOMECONTA, '
      'lan.*'
      'FROM FTabLctALS lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas con ON con.Codigo=lan.Genero'
      'WHERE lan.FatID=701'
      'AND lan.FatID_Sub=:P0'
      'AND lan.FatNum=:P1'
      '')
    Left = 32
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDespProfNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrDespProfNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrDespProfSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrDespProfKGT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'KGT'
      Calculated = True
    end
    object QrDespProfData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDespProfVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDespProfCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDespProfCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrDespProfDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00'
    end
    object QrDespProfControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDespProfSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrDespProfDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrDespProfGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrDespProfCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDespProfMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrDespProfNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrDespProfSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrDespProfDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrDespProfCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDespProfFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrDespProfCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrDespProfForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrDespProfDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrDespProfTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrDespProfAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrDespProfQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrDespProfSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrDespProfFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrDespProfFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrDespProfFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrDespProfFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrDespProfID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrDespProfID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrDespProfID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrDespProfFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrDespProfEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrDespProfBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrDespProfContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrDespProfCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrDespProfLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrDespProfCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrDespProfLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrDespProfOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrDespProfLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrDespProfPago: TFloatField
      FieldName = 'Pago'
    end
    object QrDespProfMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrDespProfMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrDespProfMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrDespProfMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrDespProfProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrDespProfDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrDespProfCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrDespProfNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrDespProfVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrDespProfAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrDespProfICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrDespProfICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrDespProfDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrDespProfDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrDespProfDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrDespProfDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrDespProfUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrDespProfNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrDespProfAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrDespProfExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrDespProfDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrDespProfCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrDespProfTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrDespProfReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrDespProfAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrDespProfPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrDespProfPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrDespProfSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrDespProfMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrDespProfLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrDespProfDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDespProfDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDespProfUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDespProfUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrDespProfAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrDespProfAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrDespProfAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object DsDespProf: TDataSource
    DataSet = QrDespProf
    Left = 88
    Top = 192
  end
  object QrSProPgt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lan.Credito) Valor'
      'FROM FTabLctALS lan'
      'WHERE lan.FatID=721'
      'AND lan.FatNum=:P0')
    Left = 144
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSProPgtValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrTransfAll: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTransfAllCalcFields
    SQL.Strings = (
      'SELECT lan.Data, lan.Tipo, lan.Carteira, lan.Controle,'
      'lan.descricao, lan.credito, lan.Debito, lan.SerieCH, '
      'lan.Documento, lan.Vencimento, lan.FatID, lan.FatNum,'
      'lan.Sub, lan.CliInt, lan.Genero,'
      'car.Nome NOMECART  '
      'FROM FTabLctALS lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE lan.FatID=700'
      'AND lan.FatNum=:P0'
      'ORDER BY Data, Controle, Sub')
    Left = 32
    Top = 332
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTransfAllData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTransfAllTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrTransfAllCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrTransfAllControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrTransfAlldescricao: TWideStringField
      FieldName = 'descricao'
      Size = 100
    end
    object QrTransfAllcredito: TFloatField
      FieldName = 'credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTransfAllDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTransfAllSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrTransfAllDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrTransfAllVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTransfAllFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrTransfAllFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrTransfAllSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrTransfAllCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrTransfAllGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrTransfAllNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrTransfAllSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
  end
  object DsTransfAll: TDataSource
    DataSet = QrTransfAll
    Left = 88
    Top = 332
  end
  object QrSTransf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lan.Credito) Credito, SUM(lan.Debito) Debito'
      'FROM FTabLctALS lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE lan.FatID=700'
      'AND lan.FatNum=:P0'
      'AND car.EntiDent=:P1')
    Left = 144
    Top = 332
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSTransfCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrSTransfDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrRateio: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT lan.Data, lan.Tipo, lan.Carteira, lan.Controle,'
      'lan.descricao, lan.credito, lan.Debito, lan.SerieCH, '
      'lan.Documento, lan.Vencimento, lan.FatID, lan.FatNum,'
      'lan.Sub, lan.CliInt, lan.Genero,'
      'car.Nome NOMECART  '
      'FROM FTabLctALS lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE lan.FatID=750'
      'AND lan.FatNum=:P0')
    Left = 32
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField6: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrRateioData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrRateioTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrRateioCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrRateioControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrRateiodescricao: TWideStringField
      FieldName = 'descricao'
      Size = 100
    end
    object QrRateiocredito: TFloatField
      FieldName = 'credito'
    end
    object QrRateioDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrRateioSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrRateioDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrRateioVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
    end
    object QrRateioFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrRateioFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrRateioSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrRateioCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrRateioGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrRateioNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
  end
  object DsRateio: TDataSource
    DataSet = QrRateio
    Left = 88
    Top = 380
  end
  object QrSDespProf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Debito) Valor'
      'FROM FTabLctALS lan'
      'WHERE lan.FatID=701'
      'AND lan.FatNum=:P0')
    Left = 144
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSDespProfValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrRolComis: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Entidade, RolComis'
      'FROM equient'
      'WHERE Codigo=:P0'
      '')
    Left = 144
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object IntegerField1: TIntegerField
      FieldName = 'Entidade'
    end
    object IntegerField2: TIntegerField
      FieldName = 'RolComis'
    end
  end
  object QrCursos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM cursos'
      'ORDER BY Nome')
    Left = 32
    Top = 428
    object QrCursosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCursosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsCursos: TDataSource
    DataSet = QrCursos
    Left = 88
    Top = 428
  end
  object QrItensCurso: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cui.Controle, cui.Genero, cui.Valor, cui.Rateio,'
      'cta.Nome NOMECTA '
      'FROM cursosits cui'
      'LEFT JOIN contas cta ON cta.Codigo=cui.Genero'
      'WHERE cui.Codigo=:P0'
      'AND cui.Rateio=:P1'
      'ORDER BY cui.Valor')
    Left = 200
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrItensCursoControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrItensCursoGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrItensCursoValor: TFloatField
      FieldName = 'Valor'
    end
    object QrItensCursoRateio: TSmallintField
      FieldName = 'Rateio'
    end
    object QrItensCursoNOMECTA: TWideStringField
      FieldName = 'NOMECTA'
      Size = 50
    end
  end
  object QrBaseRat: TmySQLQuery
    Database = Dmod.MyDBn
    SQL.Strings = (
      'SELECT SUM(Valor) Total'
      'FROM cursosits'
      'WHERE Codigo=:P0'
      'AND Rateio=:P1')
    Left = 200
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrBaseRatTotal: TFloatField
      FieldName = 'Total'
    end
  end
  object QrRatSub: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) Credito, FatID_Sub '
      'FROM FTabLctALS'
      'WHERE FatID= 711'
      'AND FatNum=:P0'
      'GROUP BY FatID_Sub')
    Left = 144
    Top = 380
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrRatSubCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrRatSubFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
  end
  object QrItens: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT gra.Nome NOMEGRA, tam.Nome NOMETAM,'
      'cor.Nome NOMECOR, mom.Grade, mom.Cor, mom.Tam'
      'FROM movim mom'
      'LEFT JOIN grades gra ON gra.Codigo=mom.Grade'
      'LEFT JOIN tamanhos tam ON tam.Codigo=mom.Tam'
      'LEFT JOIN cores cor ON cor.Codigo=mom.Cor'
      'WHERE mom.Controle=:P0')
    Left = 144
    Top = 428
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrItensNOMEGRA: TWideStringField
      FieldName = 'NOMEGRA'
      Size = 30
    end
    object QrItensNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 100
    end
    object QrItensNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 100
    end
    object QrItensGrade: TIntegerField
      FieldName = 'Grade'
      Required = True
    end
    object QrItensCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrItensTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
  end
  object QrVCPC: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mom.Grade, mom.Cor, mom.Tam, SUM(mom.Ven) Ven, '
      'SUM(mom.Val) Val, mom.SubCtrl, mom.ComisVal, mom.Qtd'
      'FROM movim mom'
      'LEFT JOIN grades gra ON gra.Codigo=mom.Grade'
      'LEFT JOIN tamanhos tam ON tam.Codigo=mom.Tam'
      'LEFT JOIN cores cor ON cor.Codigo=mom.Cor'
      'WHERE mom.Controle=:P0'
      'AND mom.SubCtrl=:P1'
      'GROUP BY mom.Grade, mom.Cor, mom.Tam')
    Left = 200
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrVCPCGrade: TIntegerField
      FieldName = 'Grade'
      Required = True
    end
    object QrVCPCCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrVCPCTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrVCPCVen: TFloatField
      FieldName = 'Ven'
      Required = True
    end
    object QrVCPCVal: TFloatField
      FieldName = 'Val'
      Required = True
    end
    object QrVCPCSubCtrl: TIntegerField
      FieldName = 'SubCtrl'
      Required = True
    end
    object QrVCPCComisVal: TFloatField
      FieldName = 'ComisVal'
      Required = True
    end
    object QrVCPCQtd: TFloatField
      FieldName = 'Qtd'
      Required = True
    end
  end
  object QrLMovV: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrLMovVCalcFields
    SQL.Strings = (
      'SELECT * '
      'FROM movv'
      'ORDER BY NomeGra, NomeCor, NomeTam')
    Left = 144
    Top = 96
    object QrLMovVCodiGra: TIntegerField
      FieldName = 'CodiGra'
    end
    object QrLMovVCodiCor: TIntegerField
      FieldName = 'CodiCor'
    end
    object QrLMovVCodiTam: TIntegerField
      FieldName = 'CodiTam'
    end
    object QrLMovVNomeGra: TWideStringField
      FieldName = 'NomeGra'
      Size = 30
    end
    object QrLMovVNomeCor: TWideStringField
      FieldName = 'NomeCor'
      Size = 30
    end
    object QrLMovVNomeTam: TWideStringField
      FieldName = 'NomeTam'
      Size = 30
    end
    object QrLMovVLevaQtd: TFloatField
      FieldName = 'LevaQtd'
    end
    object QrLMovVVendQtd: TFloatField
      FieldName = 'VendQtd'
    end
    object QrLMovVVendVal: TFloatField
      FieldName = 'VendVal'
    end
    object QrLMovVCmisVal: TFloatField
      FieldName = 'CmisVal'
    end
    object QrLMovVRETORNOU: TFloatField
      FieldKind = fkCalculated
      FieldName = 'RETORNOU'
      Calculated = True
    end
    object QrLMovVKGT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'KGT'
      Calculated = True
    end
  end
  object frxDsLMovV: TfrxDBDataset
    UserName = 'frxDsLMovV'
    CloseDataSource = False
    DataSet = QrLMovV
    BCDToCurrency = False
    Left = 200
    Top = 96
  end
  object QrTransfC: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTransfCCalcFields
    SQL.Strings = (
      'SELECT lan.Data, lan.Credito, Descricao'
      'FROM FTabLctALS lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE Credito > 0'
      'AND lan.FatID=700'
      'AND lan.FatNum=:P0'
      'AND car.EntiDent=:P1')
    Left = 200
    Top = 332
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTransfCData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrTransfCCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrTransfCDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrTransfCKGT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'KGT'
      Calculated = True
    end
  end
  object frxDsTransfC: TfrxDBDataset
    UserName = 'frxDsTransfC'
    CloseDataSource = False
    DataSet = QrTransfC
    BCDToCurrency = False
    Left = 256
    Top = 332
  end
  object QrTransfD: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTransfDCalcFields
    SQL.Strings = (
      'SELECT lan.Data, lan.Debito, Descricao'
      'FROM FTabLctALS lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE Debito > 0'
      'AND lan.FatID=700'
      'AND lan.FatNum=:P0'
      'AND car.EntiDent=:P1')
    Left = 312
    Top = 332
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTransfDData: TDateField
      FieldName = 'Data'
      Required = True
    end
    object QrTransfDDebito: TFloatField
      FieldName = 'Debito'
    end
    object QrTransfDDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrTransfDKGT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'KGT'
      Calculated = True
    end
  end
  object frxDsTransfD: TfrxDBDataset
    UserName = 'frxDsTransfD'
    CloseDataSource = False
    DataSet = QrTransfD
    BCDToCurrency = False
    Left = 368
    Top = 332
  end
  object QrAuto: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrAutoCalcFields
    SQL.Strings = (
      'SELECT car.Nome NOMECARTEIRA, con.Nome NOMECONTA, lan.*'
      'FROM FTabLctALS lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas con ON con.Codigo=lan.Genero'
      'WHERE lan.FatID in (702,711,721)'
      'AND lan.FatNum=:P0')
    Left = 32
    Top = 476
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrAutoNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrAutoNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrAutoSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrAutoData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrAutoVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrAutoCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrAutoDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrAutoDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrAutoFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrAutoFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrAutoControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrAutoSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrAutoGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrAutoCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrAutoTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrAutoAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrAutoQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrAutoNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrAutoCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrAutoSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrAutoDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrAutoSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrAutoFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrAutoFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrAutoID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrAutoID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrAutoID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrAutoFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrAutoEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrAutoBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrAutoContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrAutoCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrAutoLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrAutoCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrAutoLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrAutoOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrAutoLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrAutoPago: TFloatField
      FieldName = 'Pago'
    end
    object QrAutoMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrAutoFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrAutoCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrAutoCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrAutoForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrAutoMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrAutoMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrAutoMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrAutoMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrAutoProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrAutoDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrAutoCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrAutoNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrAutoVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrAutoAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrAutoICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrAutoICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrAutoDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrAutoDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrAutoDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrAutoDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrAutoDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrAutoUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrAutoNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrAutoAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrAutoExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrAutoDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrAutoCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrAutoTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrAutoReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrAutoAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrAutoPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrAutoPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrAutoSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrAutoMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrAutoLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrAutoDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrAutoDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrAutoUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrAutoUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrAutoAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrAutoAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrAutoAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object DsAuto: TDataSource
    DataSet = QrAuto
    Left = 88
    Top = 476
  end
  object QrSAuto: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) Valor'
      'FROM FTabLctALS lan'
      'WHERE lan.FatID=711'
      'AND lan.FatNum=:P0')
    Left = 144
    Top = 476
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSAutoValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrSProCom: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Val) VAL, SUM(Ven) VEN,'
      'SUM(Ven+Val) MARGEM, SUM(ComisVal) COMISVAL'
      'FROM movim mom'
      'WHERE mom.SubCtrl=3 '
      'AND mom.Controle=:P0')
    Left = 200
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSProComVAL: TFloatField
      FieldName = 'VAL'
    end
    object QrSProComVEN: TFloatField
      FieldName = 'VEN'
    end
    object QrSProComMARGEM: TFloatField
      FieldName = 'MARGEM'
    end
    object QrSProComCOMISVAL: TFloatField
      FieldName = 'COMISVAL'
    end
  end
  object QrSComTrf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lan.Debito) Debito'
      'FROM FTabLctALS lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas con ON con.Codigo=lan.Genero'
      'WHERE lan.FatID = 702'
      'AND lan.FatID_Sub = 1'
      'AND lan.FatNum=:P0')
    Left = 256
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSComTrfDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrConta: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Nome'
      'FROM contas'
      'WHERE Codigo=:P0')
    Left = 32
    Top = 524
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrContaNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
  end
  object QrEmprestimos: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrEmprestimosCalcFields
    SQL.Strings = (
      'SELECT car.Nome NOMECARTEIRA, con.Nome NOMECONTA, lan.*'
      'FROM FTabLctALS lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas con ON con.Codigo=lan.Genero'
      'WHERE lan.FatID = 731'
      'AND lan.FatNum=:P0')
    Left = 88
    Top = 524
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEmprestimosNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrEmprestimosNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrEmprestimosSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrEmprestimosData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmprestimosVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrEmprestimosCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEmprestimosDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00;-#,###,###,##0.00; '
    end
    object QrEmprestimosDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrEmprestimosFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrEmprestimosFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrEmprestimosControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEmprestimosSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrEmprestimosGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrEmprestimosTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrEmprestimosCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrEmprestimosAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrEmprestimosQtde: TFloatField
      FieldName = 'Qtde'
    end
    object QrEmprestimosNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrEmprestimosCompensado: TDateField
      FieldName = 'Compensado'
    end
    object QrEmprestimosSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrEmprestimosDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrEmprestimosSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrEmprestimosFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrEmprestimosFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrEmprestimosID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrEmprestimosID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrEmprestimosID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrEmprestimosFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrEmprestimosEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrEmprestimosBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrEmprestimosContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrEmprestimosCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrEmprestimosLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrEmprestimosCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrEmprestimosLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrEmprestimosOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrEmprestimosLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrEmprestimosPago: TFloatField
      FieldName = 'Pago'
    end
    object QrEmprestimosMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrEmprestimosFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrEmprestimosCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrEmprestimosCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrEmprestimosForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrEmprestimosMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrEmprestimosMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrEmprestimosMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrEmprestimosMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrEmprestimosProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrEmprestimosDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrEmprestimosCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrEmprestimosNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrEmprestimosVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrEmprestimosAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrEmprestimosICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrEmprestimosICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrEmprestimosDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrEmprestimosDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrEmprestimosDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrEmprestimosDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrEmprestimosDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrEmprestimosUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrEmprestimosNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrEmprestimosAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrEmprestimosExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrEmprestimosDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrEmprestimosCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrEmprestimosTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrEmprestimosReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrEmprestimosAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrEmprestimosPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrEmprestimosPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrEmprestimosSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrEmprestimosMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrEmprestimosLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrEmprestimosDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrEmprestimosDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrEmprestimosUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrEmprestimosUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrEmprestimosAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrEmprestimosAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrEmprestimosAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object DsEmprestimos: TDataSource
    DataSet = QrEmprestimos
    Left = 144
    Top = 524
  end
  object QrSEmprestimos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Credito) Credito, SUM(Debito) Debito'
      'FROM FTabLctALS lan'
      'WHERE lan.FatID=731'
      'AND lan.FatNum=:P0')
    Left = 200
    Top = 524
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSEmprestimosCredito: TFloatField
      FieldName = 'Credito'
    end
    object QrSEmprestimosDebito: TFloatField
      FieldName = 'Debito'
    end
  end
  object QrCiclo: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT AlunosVal'
      'FROM ciclos'
      'WHERE Codigo=:P0')
    Left = 200
    Top = 476
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCicloAlunosVal: TFloatField
      FieldName = 'AlunosVal'
    end
  end
  object QrSAlun: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(AlunosInsc) AlunosInsc,'
      'SUM(AlunosAula) AlunosAula, SUM(ValorTot) ValorTot,'
      'SUM(ComProfAlu) ComProfAlu, '
      'SUM(ComProfVal) ComProfVal'
      'FROM ciclosaula cia'
      'WHERE Codigo=:P0')
    Left = 256
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSAlunAlunosInsc: TFloatField
      FieldName = 'AlunosInsc'
    end
    object QrSAlunAlunosAula: TFloatField
      FieldName = 'AlunosAula'
    end
    object QrSAlunValorTot: TFloatField
      FieldName = 'ValorTot'
    end
    object QrSAlunComProfAlu: TFloatField
      FieldName = 'ComProfAlu'
    end
    object QrSAlunComProfVal: TFloatField
      FieldName = 'ComProfVal'
    end
  end
  object QrDespProm: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDespPromCalcFields
    SQL.Strings = (
      'SELECT car.Nome NOMECARTEIRA, con.Nome NOMECONTA, '
      'lan.*'
      'FROM FTabLctALS lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas con ON con.Codigo=lan.Genero'
      'WHERE lan.FatID=703'
      'AND lan.FatID_Sub=:P0'
      'AND lan.FatNum=:P1'
      '')
    Left = 32
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrDespPromNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrDespPromNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Size = 100
    end
    object QrDespPromSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrDespPromKGT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'KGT'
      Calculated = True
    end
    object QrDespPromData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDespPromTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrDespPromCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrDespPromControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrDespPromSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrDespPromAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
    end
    object QrDespPromGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrDespPromQtde: TFloatField
      FieldName = 'Qtde'
      DisplayFormat = '#,###,##0.000;-#,###,##0.000; '
    end
    object QrDespPromDescricao: TWideStringField
      FieldName = 'Descricao'
      Size = 100
    end
    object QrDespPromNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
    end
    object QrDespPromDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrDespPromCredito: TFloatField
      FieldName = 'Credito'
      DisplayFormat = '#,###,###,##0.00'
    end
    object QrDespPromCompensado: TDateField
      FieldName = 'Compensado'
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDespPromSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrDespPromDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrDespPromSit: TIntegerField
      FieldName = 'Sit'
    end
    object QrDespPromVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrDespPromFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrDespPromFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
    end
    object QrDespPromFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrDespPromFatParcela: TIntegerField
      FieldName = 'FatParcela'
    end
    object QrDespPromID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Required = True
    end
    object QrDespPromID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Required = True
    end
    object QrDespPromID_Sub: TSmallintField
      FieldName = 'ID_Sub'
    end
    object QrDespPromFatura: TWideStringField
      FieldName = 'Fatura'
      Size = 1
    end
    object QrDespPromEmitente: TWideStringField
      FieldName = 'Emitente'
      Size = 30
    end
    object QrDespPromBanco: TIntegerField
      FieldName = 'Banco'
    end
    object QrDespPromContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Size = 15
    end
    object QrDespPromCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Size = 15
    end
    object QrDespPromLocal: TIntegerField
      FieldName = 'Local'
    end
    object QrDespPromCartao: TIntegerField
      FieldName = 'Cartao'
    end
    object QrDespPromLinha: TIntegerField
      FieldName = 'Linha'
    end
    object QrDespPromOperCount: TIntegerField
      FieldName = 'OperCount'
    end
    object QrDespPromLancto: TIntegerField
      FieldName = 'Lancto'
    end
    object QrDespPromPago: TFloatField
      FieldName = 'Pago'
    end
    object QrDespPromMez: TIntegerField
      FieldName = 'Mez'
      Required = True
    end
    object QrDespPromFornecedor: TIntegerField
      FieldName = 'Fornecedor'
    end
    object QrDespPromCliente: TIntegerField
      FieldName = 'Cliente'
    end
    object QrDespPromCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrDespPromForneceI: TIntegerField
      FieldName = 'ForneceI'
    end
    object QrDespPromMoraDia: TFloatField
      FieldName = 'MoraDia'
    end
    object QrDespPromMulta: TFloatField
      FieldName = 'Multa'
    end
    object QrDespPromMoraVal: TFloatField
      FieldName = 'MoraVal'
    end
    object QrDespPromMultaVal: TFloatField
      FieldName = 'MultaVal'
    end
    object QrDespPromProtesto: TDateField
      FieldName = 'Protesto'
    end
    object QrDespPromDataDoc: TDateField
      FieldName = 'DataDoc'
    end
    object QrDespPromCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
    end
    object QrDespPromNivel: TIntegerField
      FieldName = 'Nivel'
    end
    object QrDespPromVendedor: TIntegerField
      FieldName = 'Vendedor'
    end
    object QrDespPromAccount: TIntegerField
      FieldName = 'Account'
    end
    object QrDespPromICMS_P: TFloatField
      FieldName = 'ICMS_P'
    end
    object QrDespPromICMS_V: TFloatField
      FieldName = 'ICMS_V'
    end
    object QrDespPromDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Size = 13
    end
    object QrDespPromDepto: TIntegerField
      FieldName = 'Depto'
    end
    object QrDespPromDescoPor: TIntegerField
      FieldName = 'DescoPor'
    end
    object QrDespPromDescoVal: TFloatField
      FieldName = 'DescoVal'
    end
    object QrDespPromDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Required = True
    end
    object QrDespPromUnidade: TIntegerField
      FieldName = 'Unidade'
      Required = True
    end
    object QrDespPromNFVal: TFloatField
      FieldName = 'NFVal'
      Required = True
    end
    object QrDespPromAntigo: TWideStringField
      FieldName = 'Antigo'
    end
    object QrDespPromExcelGru: TIntegerField
      FieldName = 'ExcelGru'
    end
    object QrDespPromDoc2: TWideStringField
      FieldName = 'Doc2'
    end
    object QrDespPromCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
    end
    object QrDespPromTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Required = True
    end
    object QrDespPromReparcel: TIntegerField
      FieldName = 'Reparcel'
      Required = True
    end
    object QrDespPromAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Required = True
    end
    object QrDespPromPagMul: TFloatField
      FieldName = 'PagMul'
      Required = True
    end
    object QrDespPromPagJur: TFloatField
      FieldName = 'PagJur'
      Required = True
    end
    object QrDespPromSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Required = True
    end
    object QrDespPromMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
    end
    object QrDespPromLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrDespPromDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrDespPromDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrDespPromUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrDespPromUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrDespPromAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrDespPromAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrDespPromAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object DsDespProm: TDataSource
    DataSet = QrDespProm
    Left = 88
    Top = 240
  end
  object QrSDespProm: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Debito) Valor'
      'FROM FTabLctALS lan'
      'WHERE lan.FatID=703'
      'AND lan.FatNum=:P0')
    Left = 144
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSDespPromValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrEnt: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.CartPref, ent.RolComis, car.Tipo TIPOCART'
      'FROM entidades ent'
      'LEFT JOIN carteiras car ON car.Codigo=ent.CartPref'
      'WHERE ent.Codigo=:P0')
    Left = 200
    Top = 428
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEntCartPref: TIntegerField
      FieldName = 'CartPref'
      Required = True
    end
    object QrEntTIPOCART: TIntegerField
      FieldName = 'TIPOCART'
      Required = True
    end
  end
  object QrDespesas: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrDespesasCalcFields
    SQL.Strings = (
      'SELECT car.Nome NOMECARTEIRA, con.Nome NOMECONTA, '
      'lan.*'
      'FROM FTabLctALS lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'LEFT JOIN contas con ON con.Codigo=lan.Genero'
      'WHERE lan.FatID in (701,703)'
      'AND lan.FatNum=:P0')
    Left = 200
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrDespesasNOMECARTEIRA: TWideStringField
      FieldName = 'NOMECARTEIRA'
      Origin = 'carteiras.Nome'
      Size = 100
    end
    object QrDespesasNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrDespesasData: TDateField
      FieldName = 'Data'
      Origin = 'lanctos.Data'
      Required = True
    end
    object QrDespesasTipo: TSmallintField
      FieldName = 'Tipo'
      Origin = 'lanctos.Tipo'
      Required = True
    end
    object QrDespesasCarteira: TIntegerField
      FieldName = 'Carteira'
      Origin = 'lanctos.Carteira'
      Required = True
    end
    object QrDespesasControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'lanctos.Controle'
      Required = True
    end
    object QrDespesasSub: TSmallintField
      FieldName = 'Sub'
      Origin = 'lanctos.Sub'
      Required = True
    end
    object QrDespesasAutorizacao: TIntegerField
      FieldName = 'Autorizacao'
      Origin = 'lanctos.Autorizacao'
    end
    object QrDespesasGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'lanctos.Genero'
    end
    object QrDespesasQtde: TFloatField
      FieldName = 'Qtde'
      Origin = 'lanctos.Qtde'
    end
    object QrDespesasDescricao: TWideStringField
      FieldName = 'Descricao'
      Origin = 'lanctos.Descricao'
      Size = 100
    end
    object QrDespesasNotaFiscal: TIntegerField
      FieldName = 'NotaFiscal'
      Origin = 'lanctos.NotaFiscal'
    end
    object QrDespesasDebito: TFloatField
      FieldName = 'Debito'
      Origin = 'lanctos.Debito'
    end
    object QrDespesasCredito: TFloatField
      FieldName = 'Credito'
      Origin = 'lanctos.Credito'
    end
    object QrDespesasCompensado: TDateField
      FieldName = 'Compensado'
      Origin = 'lanctos.Compensado'
    end
    object QrDespesasSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Origin = 'lanctos.SerieCH'
      Size = 10
    end
    object QrDespesasDocumento: TFloatField
      FieldName = 'Documento'
      Origin = 'lanctos.Documento'
    end
    object QrDespesasSit: TIntegerField
      FieldName = 'Sit'
      Origin = 'lanctos.Sit'
    end
    object QrDespesasVencimento: TDateField
      FieldName = 'Vencimento'
      Origin = 'lanctos.Vencimento'
      Required = True
    end
    object QrDespesasFatID: TIntegerField
      FieldName = 'FatID'
      Origin = 'lanctos.FatID'
    end
    object QrDespesasFatID_Sub: TIntegerField
      FieldName = 'FatID_Sub'
      Origin = 'lanctos.FatID_Sub'
    end
    object QrDespesasFatNum: TFloatField
      FieldName = 'FatNum'
      Origin = 'lanctos.FatNum'
    end
    object QrDespesasFatParcela: TIntegerField
      FieldName = 'FatParcela'
      Origin = 'lanctos.FatParcela'
    end
    object QrDespesasID_Pgto: TIntegerField
      FieldName = 'ID_Pgto'
      Origin = 'lanctos.ID_Pgto'
      Required = True
    end
    object QrDespesasID_Quit: TIntegerField
      FieldName = 'ID_Quit'
      Origin = 'lanctos.ID_Quit'
      Required = True
    end
    object QrDespesasID_Sub: TSmallintField
      FieldName = 'ID_Sub'
      Origin = 'lanctos.ID_Sub'
    end
    object QrDespesasFatura: TWideStringField
      FieldName = 'Fatura'
      Origin = 'lanctos.Fatura'
      Size = 1
    end
    object QrDespesasEmitente: TWideStringField
      FieldName = 'Emitente'
      Origin = 'lanctos.Emitente'
      Size = 30
    end
    object QrDespesasBanco: TIntegerField
      FieldName = 'Banco'
      Origin = 'lanctos.Banco'
    end
    object QrDespesasContaCorrente: TWideStringField
      FieldName = 'ContaCorrente'
      Origin = 'lanctos.ContaCorrente'
      Size = 15
    end
    object QrDespesasCNPJCPF: TWideStringField
      FieldName = 'CNPJCPF'
      Origin = 'lanctos.CNPJCPF'
      Size = 15
    end
    object QrDespesasLocal: TIntegerField
      FieldName = 'Local'
      Origin = 'lanctos.Local'
    end
    object QrDespesasCartao: TIntegerField
      FieldName = 'Cartao'
      Origin = 'lanctos.Cartao'
    end
    object QrDespesasLinha: TIntegerField
      FieldName = 'Linha'
      Origin = 'lanctos.Linha'
    end
    object QrDespesasOperCount: TIntegerField
      FieldName = 'OperCount'
      Origin = 'lanctos.OperCount'
    end
    object QrDespesasLancto: TIntegerField
      FieldName = 'Lancto'
      Origin = 'lanctos.Lancto'
    end
    object QrDespesasPago: TFloatField
      FieldName = 'Pago'
      Origin = 'lanctos.Pago'
    end
    object QrDespesasMez: TIntegerField
      FieldName = 'Mez'
      Origin = 'lanctos.Mez'
      Required = True
    end
    object QrDespesasFornecedor: TIntegerField
      FieldName = 'Fornecedor'
      Origin = 'lanctos.Fornecedor'
    end
    object QrDespesasCliente: TIntegerField
      FieldName = 'Cliente'
      Origin = 'lanctos.Cliente'
    end
    object QrDespesasCliInt: TIntegerField
      FieldName = 'CliInt'
      Origin = 'lanctos.CliInt'
    end
    object QrDespesasForneceI: TIntegerField
      FieldName = 'ForneceI'
      Origin = 'lanctos.ForneceI'
    end
    object QrDespesasMoraDia: TFloatField
      FieldName = 'MoraDia'
      Origin = 'lanctos.MoraDia'
    end
    object QrDespesasMulta: TFloatField
      FieldName = 'Multa'
      Origin = 'lanctos.Multa'
    end
    object QrDespesasMoraVal: TFloatField
      FieldName = 'MoraVal'
      Origin = 'lanctos.MoraVal'
    end
    object QrDespesasMultaVal: TFloatField
      FieldName = 'MultaVal'
      Origin = 'lanctos.MultaVal'
    end
    object QrDespesasProtesto: TDateField
      FieldName = 'Protesto'
      Origin = 'lanctos.Protesto'
    end
    object QrDespesasDataDoc: TDateField
      FieldName = 'DataDoc'
      Origin = 'lanctos.DataDoc'
    end
    object QrDespesasCtrlIni: TIntegerField
      FieldName = 'CtrlIni'
      Origin = 'lanctos.CtrlIni'
    end
    object QrDespesasNivel: TIntegerField
      FieldName = 'Nivel'
      Origin = 'lanctos.Nivel'
    end
    object QrDespesasVendedor: TIntegerField
      FieldName = 'Vendedor'
      Origin = 'lanctos.Vendedor'
    end
    object QrDespesasAccount: TIntegerField
      FieldName = 'Account'
      Origin = 'lanctos.Account'
    end
    object QrDespesasICMS_P: TFloatField
      FieldName = 'ICMS_P'
      Origin = 'lanctos.ICMS_P'
    end
    object QrDespesasICMS_V: TFloatField
      FieldName = 'ICMS_V'
      Origin = 'lanctos.ICMS_V'
    end
    object QrDespesasDuplicata: TWideStringField
      FieldName = 'Duplicata'
      Origin = 'lanctos.Duplicata'
      Size = 13
    end
    object QrDespesasDepto: TIntegerField
      FieldName = 'Depto'
      Origin = 'lanctos.Depto'
    end
    object QrDespesasDescoPor: TIntegerField
      FieldName = 'DescoPor'
      Origin = 'lanctos.DescoPor'
    end
    object QrDespesasDescoVal: TFloatField
      FieldName = 'DescoVal'
      Origin = 'lanctos.DescoVal'
    end
    object QrDespesasDescoControle: TIntegerField
      FieldName = 'DescoControle'
      Origin = 'lanctos.DescoControle'
      Required = True
    end
    object QrDespesasUnidade: TIntegerField
      FieldName = 'Unidade'
      Origin = 'lanctos.Unidade'
      Required = True
    end
    object QrDespesasNFVal: TFloatField
      FieldName = 'NFVal'
      Origin = 'lanctos.NFVal'
      Required = True
    end
    object QrDespesasAntigo: TWideStringField
      FieldName = 'Antigo'
      Origin = 'lanctos.Antigo'
    end
    object QrDespesasExcelGru: TIntegerField
      FieldName = 'ExcelGru'
      Origin = 'lanctos.ExcelGru'
    end
    object QrDespesasDoc2: TWideStringField
      FieldName = 'Doc2'
      Origin = 'lanctos.Doc2'
    end
    object QrDespesasCNAB_Sit: TSmallintField
      FieldName = 'CNAB_Sit'
      Origin = 'lanctos.CNAB_Sit'
    end
    object QrDespesasTipoCH: TSmallintField
      FieldName = 'TipoCH'
      Origin = 'lanctos.TipoCH'
      Required = True
    end
    object QrDespesasReparcel: TIntegerField
      FieldName = 'Reparcel'
      Origin = 'lanctos.Reparcel'
      Required = True
    end
    object QrDespesasAtrelado: TIntegerField
      FieldName = 'Atrelado'
      Origin = 'lanctos.Atrelado'
      Required = True
    end
    object QrDespesasPagMul: TFloatField
      FieldName = 'PagMul'
      Origin = 'lanctos.PagMul'
      Required = True
    end
    object QrDespesasPagJur: TFloatField
      FieldName = 'PagJur'
      Origin = 'lanctos.PagJur'
      Required = True
    end
    object QrDespesasSubPgto1: TIntegerField
      FieldName = 'SubPgto1'
      Origin = 'lanctos.SubPgto1'
      Required = True
    end
    object QrDespesasMultiPgto: TIntegerField
      FieldName = 'MultiPgto'
      Origin = 'lanctos.MultiPgto'
    end
    object QrDespesasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'lanctos.Lk'
    end
    object QrDespesasDataCad: TDateField
      FieldName = 'DataCad'
      Origin = 'lanctos.DataCad'
    end
    object QrDespesasDataAlt: TDateField
      FieldName = 'DataAlt'
      Origin = 'lanctos.DataAlt'
    end
    object QrDespesasUserCad: TIntegerField
      FieldName = 'UserCad'
      Origin = 'lanctos.UserCad'
    end
    object QrDespesasUserAlt: TIntegerField
      FieldName = 'UserAlt'
      Origin = 'lanctos.UserAlt'
    end
    object QrDespesasAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Origin = 'lanctos.AlterWeb'
      Required = True
    end
    object QrDespesasAtivo: TSmallintField
      FieldName = 'Ativo'
      Origin = 'lanctos.Ativo'
      Required = True
    end
    object QrDespesasKGT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'KGT'
      Calculated = True
    end
    object QrDespesasSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
    object QrDespesasAgencia: TIntegerField
      FieldName = 'Agencia'
    end
  end
  object frxDsDespesas: TfrxDBDataset
    UserName = 'frxDsDespesas'
    CloseDataSource = False
    DataSet = QrDespesas
    BCDToCurrency = False
    Left = 200
    Top = 240
  end
  object QrCartPrf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT COUNT(car.Codigo) Qtde,'
      'IF(ent.Tipo=0, ent.RazaoSocial, ent.Nome) NOMEENT'
      'FROM entidades ent'
      'LEFT JOIN carteiras car ON ent.Codigo=car.EntiDent'
      'WHERE ent.Codigo=:P0'
      'GROUP BY ent.Codigo')
    Left = 256
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCartPrfQtde: TLargeintField
      FieldName = 'Qtde'
      Required = True
    end
    object QrCartPrfNOMEENT: TWideStringField
      FieldName = 'NOMEENT'
      Required = True
      Size = 100
    end
  end
  object QrSDespCom: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lan.Debito-lan.Credito) Valor'
      'FROM FTabLctALS lan'
      'WHERE lan.FatID IN (:P0) '
      'AND lan.FatID_Sub=:P1'
      'AND lan.Genero IN ('
      '  SELECT Codigo '
      '  FROM contas   '
      '  WHERE CentroCusto=:P2'
      '  OR CentroCusto=0'
      ')')
    Left = 256
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSDespComValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrEquiCom: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome '
      'FROM equicom'
      'ORDER BY Nome'
      '')
    Left = 256
    Top = 380
    object QrRolComisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrRolComisNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsEquiCom: TDataSource
    DataSet = QrEquiCom
    Left = 312
    Top = 380
  end
  object QrCurso: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Valor '
      'FROM cursos'
      'WHERE Codigo=:P0')
    Left = 256
    Top = 240
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCursoValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrCtaSew: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ect.PerceGene, ect.MinQtde'
      'FROM equicontrf ect'
      'WHERE ect.Codigo=:P0'
      'AND ect.Genero=:P1')
    Left = 256
    Top = 284
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrCtaSewPerceGene: TFloatField
      FieldName = 'PerceGene'
      Required = True
    end
    object QrCtaSewMinQtde: TFloatField
      FieldName = 'MinQtde'
      Required = True
    end
  end
  object QrSDespDTS: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lan.Debito-lan.Credito) Valor'
      'FROM FTabLctALS lan'
      'WHERE lan.FatID IN (:P0) '
      'AND lan.FatID_Sub=:P1'
      'AND lan.Genero IN ('
      '  SELECT Codigo '
      '  FROM contas   '
      '  WHERE CentroCusto=:P2'
      '  AND CentroCusto<>0'
      ')')
    Left = 256
    Top = 192
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSDespDTSValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrTransfDia: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTransfDiaCalcFields
    SQL.Strings = (
      'SELECT lan.Data, lan.Tipo, lan.Carteira, lan.Controle,'
      'lan.descricao, lan.credito, lan.Debito, lan.SerieCH, '
      'lan.Documento, lan.Vencimento, lan.FatID, lan.FatNum,'
      'lan.Sub, lan.CliInt, lan.Genero,'
      'car.Nome NOMECART  '
      'FROM FTabLctALS lan'
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira'
      'WHERE lan.FatID=700'
      'AND lan.FatID_Sub=:P0'
      'AND lan.FatNum=:P1'
      'ORDER BY Data, Controle, Sub')
    Left = 256
    Top = 428
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTransfDiaData: TDateField
      FieldName = 'Data'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTransfDiaTipo: TSmallintField
      FieldName = 'Tipo'
      Required = True
    end
    object QrTransfDiaCarteira: TIntegerField
      FieldName = 'Carteira'
      Required = True
    end
    object QrTransfDiaControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrTransfDiadescricao: TWideStringField
      FieldName = 'descricao'
      Size = 100
    end
    object QrTransfDiacredito: TFloatField
      FieldName = 'credito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTransfDiaDebito: TFloatField
      FieldName = 'Debito'
      DisplayFormat = '#,###,##0.00;-#,###,##0.00; '
    end
    object QrTransfDiaSerieCH: TWideStringField
      FieldName = 'SerieCH'
      Size = 10
    end
    object QrTransfDiaDocumento: TFloatField
      FieldName = 'Documento'
    end
    object QrTransfDiaVencimento: TDateField
      FieldName = 'Vencimento'
      Required = True
      DisplayFormat = 'dd/mm/yy'
    end
    object QrTransfDiaFatID: TIntegerField
      FieldName = 'FatID'
    end
    object QrTransfDiaFatNum: TFloatField
      FieldName = 'FatNum'
    end
    object QrTransfDiaSub: TSmallintField
      FieldName = 'Sub'
      Required = True
    end
    object QrTransfDiaCliInt: TIntegerField
      FieldName = 'CliInt'
    end
    object QrTransfDiaGenero: TIntegerField
      FieldName = 'Genero'
    end
    object QrTransfDiaNOMECART: TWideStringField
      FieldName = 'NOMECART'
      Size = 100
    end
    object QrTransfDiaSEQ: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'SEQ'
      Calculated = True
    end
  end
  object DsTransfDia: TDataSource
    DataSet = QrTransfDia
    Left = 312
    Top = 428
  end
  object QrMovimD: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrMovimDCalcFields
    SQL.Strings = (
      'SELECT gra.Nome NOMEGRA, tam.Nome NOMETAM,'
      'cor.Nome NOMECOR, mom.*'
      'FROM movim mom'
      'LEFT JOIN grades gra ON gra.Codigo=mom.Grade'
      'LEFT JOIN tamanhos tam ON tam.Codigo=mom.Tam'
      'LEFT JOIN cores cor ON cor.Codigo=mom.Cor'
      'WHERE  mom.SubCtrl=3'
      'AND mom.Controle= :P0'
      'AND mom.SubCta= :P1')
    Left = 312
    Top = 96
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMovimDComisTipNOME: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'ComisTipNOME'
      Size = 1
      Calculated = True
    end
    object QrMovimDPOSIv: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIv'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrMovimDPOSIq: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIq'
      DisplayFormat = '#,###,##0.000'
      Calculated = True
    end
    object QrMovimDPRECO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.0000'
      Calculated = True
    end
    object QrMovimDNOMEGRA: TWideStringField
      FieldName = 'NOMEGRA'
      Size = 30
    end
    object QrMovimDNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 100
    end
    object QrMovimDNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 100
    end
    object QrMovimDControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrMovimDConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrMovimDGrade: TIntegerField
      FieldName = 'Grade'
      Required = True
    end
    object QrMovimDCor: TIntegerField
      FieldName = 'Cor'
      Required = True
    end
    object QrMovimDTam: TIntegerField
      FieldName = 'Tam'
      Required = True
    end
    object QrMovimDQtd: TFloatField
      FieldName = 'Qtd'
      Required = True
    end
    object QrMovimDVal: TFloatField
      FieldName = 'Val'
      Required = True
    end
    object QrMovimDVen: TFloatField
      FieldName = 'Ven'
      Required = True
    end
    object QrMovimDDataPedi: TDateField
      FieldName = 'DataPedi'
      Required = True
    end
    object QrMovimDDataReal: TDateField
      FieldName = 'DataReal'
      Required = True
    end
    object QrMovimDMotivo: TSmallintField
      FieldName = 'Motivo'
      Required = True
    end
    object QrMovimDSALDOQTD: TFloatField
      FieldName = 'SALDOQTD'
      Required = True
    end
    object QrMovimDSALDOVAL: TFloatField
      FieldName = 'SALDOVAL'
      Required = True
    end
    object QrMovimDLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrMovimDDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrMovimDDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrMovimDUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrMovimDUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrMovimDAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
      Required = True
    end
    object QrMovimDAtivo: TSmallintField
      FieldName = 'Ativo'
      Required = True
    end
    object QrMovimDSubCtrl: TIntegerField
      FieldName = 'SubCtrl'
      Required = True
    end
    object QrMovimDComisTip: TSmallintField
      FieldName = 'ComisTip'
      Required = True
    end
    object QrMovimDComisFat: TFloatField
      FieldName = 'ComisFat'
      Required = True
    end
    object QrMovimDComisVal: TFloatField
      FieldName = 'ComisVal'
      Required = True
    end
    object QrMovimDSubCta: TIntegerField
      FieldName = 'SubCta'
      Required = True
    end
  end
  object DsMovimD: TDataSource
    DataSet = QrMovimD
    Left = 368
    Top = 96
  end
  object QrSDiaVen: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ComisVal) COMISVAL'
      'FROM movim mom'
      'WHERE mom.SubCtrl=3 '
      'AND mom.Controle=:P0'
      'AND mom.SubCta=:P1'
      'AND (mom.Grade NOT IN ('
      '  SELECT eci.Produto  '
      '  FROM equicomits eci '
      '  WHERE eci.Codigo=:P2'
      '  AND eci.Compoe=1'
      '))')
    Left = 312
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrSDiaVenCOMISVAL: TFloatField
      FieldName = 'COMISVAL'
    end
  end
  object QrSDiaCom: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ComisVal) COMISVAL'
      'FROM movim mom'
      'WHERE mom.SubCtrl=3 '
      'AND mom.Controle=:P0'
      'AND mom.SubCta=:P1'
      '')
    Left = 368
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrSDiaComCOMISVAL: TFloatField
      FieldName = 'COMISVAL'
    end
  end
  object QrMaxDia: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT MAX(Controle) Controle'
      'FROM ciclosaula'
      'WHERE Codigo=:P0'
      'AND Data=('
      '   SELECT MAX(Data)'
      '   FROM ciclosaula'
      '   WHERE Codigo=:P1'
      ')')
    Left = 312
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrMaxDiaControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object QrPagComProf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(lan.Debito-lan.Credito) Valor'
      'FROM FTabLctALS lan'
      'WHERE lan.FatID IN (705) '
      'AND lan.FatID_Sub=:P0')
    Left = 368
    Top = 48
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrPagComProfValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrProdAlu: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT prd.Nome'
      'FROM equicomits eci '
      'LEFT JOIN grades prd ON prd.Codigo=eci.Produto'
      'WHERE eci.Compoe=1'
      'AND eci.Codigo=:P0')
    Left = 312
    Top = 144
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdAluNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
  end
  object DsBacen_Pais1: TDataSource
    DataSet = QrBacen_Pais
    Left = 471
  end
  object QrBacen_Pais: TmySQLQuery
    Database = DModG.AllID_DB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM bacen_pais'
      'ORDER BY Nome'
      '')
    Left = 419
    object QrBacen_PaisCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrBacen_PaisNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object QrMunici: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT dmu.Codigo, dmu.Nome, duf.Sigla'
      'FROM dtb_munici dmu'
      'LEFT JOIN dtb_ufs duf ON duf.Codigo = dmu.DTB_UF'
      'WHERE duf.Sigla=:P0'
      'ORDER BY Nome')
    Left = 420
    Top = 52
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrMuniciCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'dtb_munici.Codigo'
    end
    object QrMuniciNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'dtb_munici.Nome'
      Size = 100
    end
    object QrMuniciSigla: TWideStringField
      FieldName = 'Sigla'
      Origin = 'dtb_ufs.Sigla'
      Size = 2
    end
  end
  object DsMunici: TDataSource
    DataSet = QrMunici
    Left = 471
    Top = 48
  end
end
