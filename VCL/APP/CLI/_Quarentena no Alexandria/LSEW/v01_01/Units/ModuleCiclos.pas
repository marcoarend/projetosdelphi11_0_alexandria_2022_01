unit ModuleCiclos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, mySQLDbTables, dmkGeral, frxClass, frxDBSet, UnDmkEnums, UnDmkProcFunc;

type
  TDmCiclos = class(TDataModule)
    QrMovimL: TmySQLQuery;
    QrMovimLNOMEGRA: TWideStringField;
    QrMovimLNOMETAM: TWideStringField;
    QrMovimLNOMECOR: TWideStringField;
    QrMovimLControle: TIntegerField;
    QrMovimLConta: TIntegerField;
    QrMovimLGrade: TIntegerField;
    QrMovimLCor: TIntegerField;
    QrMovimLTam: TIntegerField;
    QrMovimLQtd: TFloatField;
    QrMovimLVal: TFloatField;
    QrMovimLVen: TFloatField;
    QrMovimLDataPedi: TDateField;
    QrMovimLDataReal: TDateField;
    QrMovimLMotivo: TSmallintField;
    QrMovimLSALDOQTD: TFloatField;
    QrMovimLSALDOVAL: TFloatField;
    QrMovimLLk: TIntegerField;
    QrMovimLDataCad: TDateField;
    QrMovimLDataAlt: TDateField;
    QrMovimLUserCad: TIntegerField;
    QrMovimLUserAlt: TIntegerField;
    QrMovimLAlterWeb: TSmallintField;
    QrMovimLAtivo: TSmallintField;
    QrMovimLPRECO: TFloatField;
    QrMovimLPOSIq: TFloatField;
    QrMovimLPOSIv: TFloatField;
    DsMovimL: TDataSource;
    QrMovimT: TmySQLQuery;
    QrMovimTNOMEGRA: TWideStringField;
    QrMovimTNOMETAM: TWideStringField;
    QrMovimTNOMECOR: TWideStringField;
    QrMovimTControle: TIntegerField;
    QrMovimTConta: TIntegerField;
    QrMovimTGrade: TIntegerField;
    QrMovimTCor: TIntegerField;
    QrMovimTTam: TIntegerField;
    QrMovimTQtd: TFloatField;
    QrMovimTVal: TFloatField;
    QrMovimTVen: TFloatField;
    QrMovimTDataPedi: TDateField;
    QrMovimTDataReal: TDateField;
    QrMovimTMotivo: TSmallintField;
    QrMovimTSALDOQTD: TFloatField;
    QrMovimTSALDOVAL: TFloatField;
    QrMovimTLk: TIntegerField;
    QrMovimTDataCad: TDateField;
    QrMovimTDataAlt: TDateField;
    QrMovimTUserCad: TIntegerField;
    QrMovimTUserAlt: TIntegerField;
    QrMovimTAlterWeb: TSmallintField;
    QrMovimTAtivo: TSmallintField;
    QrMovimTPRECO: TFloatField;
    QrMovimTPOSIq: TFloatField;
    QrMovimTPOSIv: TFloatField;
    DsMovimT: TDataSource;
    DsMovimV: TDataSource;
    QrMovimV: TmySQLQuery;
    QrMovimVNOMEGRA: TWideStringField;
    QrMovimVNOMETAM: TWideStringField;
    QrMovimVNOMECOR: TWideStringField;
    QrMovimVControle: TIntegerField;
    QrMovimVConta: TIntegerField;
    QrMovimVGrade: TIntegerField;
    QrMovimVCor: TIntegerField;
    QrMovimVTam: TIntegerField;
    QrMovimVQtd: TFloatField;
    QrMovimVVal: TFloatField;
    QrMovimVVen: TFloatField;
    QrMovimVDataPedi: TDateField;
    QrMovimVDataReal: TDateField;
    QrMovimVMotivo: TSmallintField;
    QrMovimVSALDOQTD: TFloatField;
    QrMovimVSALDOVAL: TFloatField;
    QrMovimVLk: TIntegerField;
    QrMovimVDataCad: TDateField;
    QrMovimVDataAlt: TDateField;
    QrMovimVUserCad: TIntegerField;
    QrMovimVUserAlt: TIntegerField;
    QrMovimVAlterWeb: TSmallintField;
    QrMovimVAtivo: TSmallintField;
    QrMovimVPRECO: TFloatField;
    QrMovimVPOSIq: TFloatField;
    QrMovimVPOSIv: TFloatField;
    QrProfessores: TmySQLQuery;
    DsProfessores: TDataSource;
    QrPromotores: TmySQLQuery;
    DsPromotores: TDataSource;
    QrSProVen: TmySQLQuery;
    QrDespProf: TmySQLQuery;
    DsDespProf: TDataSource;
    QrDespProfNOMECARTEIRA: TWideStringField;
    QrDespProfNOMECONTA: TWideStringField;
    QrDespProfSEQ: TIntegerField;
    QrSProVenVAL: TFloatField;
    QrSProVenVEN: TFloatField;
    QrSProVenMARGEM: TFloatField;
    QrSProPgt: TmySQLQuery;
    QrSProPgtValor: TFloatField;
    QrTransfAll: TmySQLQuery;
    DsTransfAll: TDataSource;
    QrTransfAllData: TDateField;
    QrTransfAllTipo: TSmallintField;
    QrTransfAllCarteira: TIntegerField;
    QrTransfAllControle: TIntegerField;
    QrTransfAlldescricao: TWideStringField;
    QrTransfAllcredito: TFloatField;
    QrTransfAllDebito: TFloatField;
    QrTransfAllSerieCH: TWideStringField;
    QrTransfAllDocumento: TFloatField;
    QrTransfAllVencimento: TDateField;
    QrTransfAllFatID: TIntegerField;
    QrTransfAllFatNum: TFloatField;
    QrTransfAllSub: TSmallintField;
    QrTransfAllCliInt: TIntegerField;
    QrTransfAllSEQ: TIntegerField;
    QrTransfAllNOMECART: TWideStringField;
    QrTransfAllGenero: TIntegerField;
    QrSTransf: TmySQLQuery;
    QrSTransfCredito: TFloatField;
    QrSTransfDebito: TFloatField;
    QrRateio: TmySQLQuery;
    IntegerField6: TIntegerField;
    DsRateio: TDataSource;
    QrRateioData: TDateField;
    QrRateioTipo: TSmallintField;
    QrRateioCarteira: TIntegerField;
    QrRateioControle: TIntegerField;
    QrRateiodescricao: TWideStringField;
    QrRateiocredito: TFloatField;
    QrRateioDebito: TFloatField;
    QrRateioSerieCH: TWideStringField;
    QrRateioDocumento: TFloatField;
    QrRateioVencimento: TDateField;
    QrRateioFatID: TIntegerField;
    QrRateioFatNum: TFloatField;
    QrRateioSub: TSmallintField;
    QrRateioCliInt: TIntegerField;
    QrRateioGenero: TIntegerField;
    QrRateioNOMECART: TWideStringField;
    QrDespProfData: TDateField;
    QrDespProfVencimento: TDateField;
    QrDespProfCredito: TFloatField;
    QrDespProfDebito: TFloatField;
    QrDespProfControle: TIntegerField;
    QrDespProfSub: TSmallintField;
    QrDespProfCompensado: TDateField;
    QrDespProfDescricao: TWideStringField;
    QrDespProfGenero: TIntegerField;
    QrDespProfCarteira: TIntegerField;
    QrDespProfMez: TIntegerField;
    QrDespProfNotaFiscal: TIntegerField;
    QrDespProfSerieCH: TWideStringField;
    QrDespProfDocumento: TFloatField;
    QrDespProfCliente: TIntegerField;
    QrDespProfFornecedor: TIntegerField;
    QrDespProfCliInt: TIntegerField;
    QrDespProfForneceI: TIntegerField;
    QrDespProfDuplicata: TWideStringField;
    QrDespProfTipo: TSmallintField;
    QrDespProfAutorizacao: TIntegerField;
    QrDespProfQtde: TFloatField;
    QrDespProfSit: TIntegerField;
    QrDespProfFatID: TIntegerField;
    QrDespProfFatID_Sub: TIntegerField;
    QrDespProfFatNum: TFloatField;
    QrDespProfFatParcela: TIntegerField;
    QrDespProfID_Pgto: TIntegerField;
    QrDespProfID_Quit: TIntegerField;
    QrDespProfID_Sub: TSmallintField;
    QrDespProfFatura: TWideStringField;
    QrDespProfEmitente: TWideStringField;
    QrDespProfBanco: TIntegerField;
    QrDespProfContaCorrente: TWideStringField;
    QrDespProfCNPJCPF: TWideStringField;
    QrDespProfLocal: TIntegerField;
    QrDespProfCartao: TIntegerField;
    QrDespProfLinha: TIntegerField;
    QrDespProfOperCount: TIntegerField;
    QrDespProfLancto: TIntegerField;
    QrDespProfPago: TFloatField;
    QrDespProfMoraDia: TFloatField;
    QrDespProfMulta: TFloatField;
    QrDespProfMoraVal: TFloatField;
    QrDespProfMultaVal: TFloatField;
    QrDespProfProtesto: TDateField;
    QrDespProfDataDoc: TDateField;
    QrDespProfCtrlIni: TIntegerField;
    QrDespProfNivel: TIntegerField;
    QrDespProfVendedor: TIntegerField;
    QrDespProfAccount: TIntegerField;
    QrDespProfICMS_P: TFloatField;
    QrDespProfICMS_V: TFloatField;
    QrDespProfDepto: TIntegerField;
    QrDespProfDescoPor: TIntegerField;
    QrDespProfDescoVal: TFloatField;
    QrDespProfDescoControle: TIntegerField;
    QrDespProfUnidade: TIntegerField;
    QrDespProfNFVal: TFloatField;
    QrDespProfAntigo: TWideStringField;
    QrDespProfExcelGru: TIntegerField;
    QrDespProfDoc2: TWideStringField;
    QrDespProfCNAB_Sit: TSmallintField;
    QrDespProfTipoCH: TSmallintField;
    QrDespProfReparcel: TIntegerField;
    QrDespProfAtrelado: TIntegerField;
    QrDespProfPagMul: TFloatField;
    QrDespProfPagJur: TFloatField;
    QrDespProfSubPgto1: TIntegerField;
    QrDespProfMultiPgto: TIntegerField;
    QrDespProfLk: TIntegerField;
    QrDespProfDataCad: TDateField;
    QrDespProfDataAlt: TDateField;
    QrDespProfUserCad: TIntegerField;
    QrDespProfUserAlt: TIntegerField;
    QrDespProfAlterWeb: TSmallintField;
    QrDespProfAtivo: TSmallintField;
    QrSDespProf: TmySQLQuery;
    QrRolComis: TmySQLQuery;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    QrCursos: TmySQLQuery;
    DsCursos: TDataSource;
    QrCursosCodigo: TIntegerField;
    QrCursosNome: TWideStringField;
    QrItensCurso: TmySQLQuery;
    QrItensCursoControle: TIntegerField;
    QrItensCursoGenero: TIntegerField;
    QrItensCursoValor: TFloatField;
    QrItensCursoRateio: TSmallintField;
    QrBaseRat: TmySQLQuery;
    QrBaseRatTotal: TFloatField;
    QrItensCursoNOMECTA: TWideStringField;
    QrRatSub: TmySQLQuery;
    QrRatSubCredito: TFloatField;
    QrRatSubFatID_Sub: TIntegerField;
    QrMovimVSubCtrl: TIntegerField;
    QrMovimVComisTip: TSmallintField;
    QrMovimVComisFat: TFloatField;
    QrMovimVComisVal: TFloatField;
    QrMovimVComisTipNOME: TWideStringField;
    QrMovimTSubCtrl: TIntegerField;
    QrMovimTComisTip: TSmallintField;
    QrMovimTComisFat: TFloatField;
    QrMovimTComisVal: TFloatField;
    QrMovimLSubCtrl: TIntegerField;
    QrMovimLComisTip: TSmallintField;
    QrMovimLComisFat: TFloatField;
    QrMovimLComisVal: TFloatField;
    QrMovimTComisTipNOME: TWideStringField;
    QrMovimLComisTipNOME: TWideStringField;
    QrSProVenCOMISVAL: TFloatField;
    QrItens: TmySQLQuery;
    QrItensNOMEGRA: TWideStringField;
    QrItensNOMETAM: TWideStringField;
    QrItensNOMECOR: TWideStringField;
    QrItensGrade: TIntegerField;
    QrItensCor: TIntegerField;
    QrItensTam: TIntegerField;
    QrVCPC: TmySQLQuery;
    QrVCPCGrade: TIntegerField;
    QrVCPCCor: TIntegerField;
    QrVCPCTam: TIntegerField;
    QrVCPCVen: TFloatField;
    QrVCPCVal: TFloatField;
    QrVCPCSubCtrl: TIntegerField;
    QrVCPCComisVal: TFloatField;
    QrVCPCQtd: TFloatField;
    QrLMovV: TmySQLQuery;
    frxDsLMovV: TfrxDBDataset;
    QrLMovVCodiGra: TIntegerField;
    QrLMovVCodiCor: TIntegerField;
    QrLMovVCodiTam: TIntegerField;
    QrLMovVNomeGra: TWideStringField;
    QrLMovVNomeCor: TWideStringField;
    QrLMovVNomeTam: TWideStringField;
    QrLMovVLevaQtd: TFloatField;
    QrLMovVVendQtd: TFloatField;
    QrLMovVVendVal: TFloatField;
    QrLMovVCmisVal: TFloatField;
    QrLMovVRETORNOU: TFloatField;
    QrLMovVKGT: TIntegerField;
    QrDespProfKGT: TIntegerField;
    QrTransfC: TmySQLQuery;
    frxDsTransfC: TfrxDBDataset;
    QrTransfCData: TDateField;
    QrTransfCCredito: TFloatField;
    QrTransfCDescricao: TWideStringField;
    QrTransfCKGT: TIntegerField;
    QrTransfD: TmySQLQuery;
    frxDsTransfD: TfrxDBDataset;
    QrTransfDData: TDateField;
    QrTransfDDebito: TFloatField;
    QrTransfDDescricao: TWideStringField;
    QrTransfDKGT: TIntegerField;
    QrAuto: TmySQLQuery;
    DsAuto: TDataSource;
    QrSAuto: TmySQLQuery;
    QrSAutoValor: TFloatField;
    QrAutoNOMECARTEIRA: TWideStringField;
    QrAutoNOMECONTA: TWideStringField;
    QrAutoSEQ: TIntegerField;
    QrSProCom: TmySQLQuery;
    QrSProComVAL: TFloatField;
    QrSProComVEN: TFloatField;
    QrSProComMARGEM: TFloatField;
    QrSProComCOMISVAL: TFloatField;
    QrSComTrf: TmySQLQuery;
    QrSComTrfDebito: TFloatField;
    QrPromotoresNOMEPROMO: TWideStringField;
    QrPromotoresCodigo: TIntegerField;
    QrConta: TmySQLQuery;
    QrContaNome: TWideStringField;
    QrEmprestimos: TmySQLQuery;
    DsEmprestimos: TDataSource;
    QrAutoData: TDateField;
    QrAutoVencimento: TDateField;
    QrAutoCredito: TFloatField;
    QrAutoDebito: TFloatField;
    QrAutoDescricao: TWideStringField;
    QrAutoFatID: TIntegerField;
    QrAutoFatID_Sub: TIntegerField;
    QrAutoControle: TIntegerField;
    QrEmprestimosNOMECARTEIRA: TWideStringField;
    QrEmprestimosNOMECONTA: TWideStringField;
    QrEmprestimosSEQ: TIntegerField;
    QrEmprestimosData: TDateField;
    QrEmprestimosVencimento: TDateField;
    QrEmprestimosCredito: TFloatField;
    QrEmprestimosDebito: TFloatField;
    QrEmprestimosDescricao: TWideStringField;
    QrEmprestimosFatID: TIntegerField;
    QrEmprestimosFatID_Sub: TIntegerField;
    QrEmprestimosControle: TIntegerField;
    QrAutoSub: TSmallintField;
    QrEmprestimosSub: TSmallintField;
    QrAutoGenero: TIntegerField;
    QrEmprestimosGenero: TIntegerField;
    QrSEmprestimos: TmySQLQuery;
    QrSEmprestimosCredito: TFloatField;
    QrSEmprestimosDebito: TFloatField;
    QrAutoCarteira: TIntegerField;
    QrAutoTipo: TSmallintField;
    QrAutoAutorizacao: TIntegerField;
    QrAutoQtde: TFloatField;
    QrAutoNotaFiscal: TIntegerField;
    QrAutoCompensado: TDateField;
    QrAutoSerieCH: TWideStringField;
    QrAutoDocumento: TFloatField;
    QrAutoSit: TIntegerField;
    QrAutoFatNum: TFloatField;
    QrAutoFatParcela: TIntegerField;
    QrAutoID_Pgto: TIntegerField;
    QrAutoID_Quit: TIntegerField;
    QrAutoID_Sub: TSmallintField;
    QrAutoFatura: TWideStringField;
    QrAutoEmitente: TWideStringField;
    QrAutoBanco: TIntegerField;
    QrAutoContaCorrente: TWideStringField;
    QrAutoCNPJCPF: TWideStringField;
    QrAutoLocal: TIntegerField;
    QrAutoCartao: TIntegerField;
    QrAutoLinha: TIntegerField;
    QrAutoOperCount: TIntegerField;
    QrAutoLancto: TIntegerField;
    QrAutoPago: TFloatField;
    QrAutoMez: TIntegerField;
    QrAutoFornecedor: TIntegerField;
    QrAutoCliente: TIntegerField;
    QrAutoCliInt: TIntegerField;
    QrAutoForneceI: TIntegerField;
    QrAutoMoraDia: TFloatField;
    QrAutoMulta: TFloatField;
    QrAutoMoraVal: TFloatField;
    QrAutoMultaVal: TFloatField;
    QrAutoProtesto: TDateField;
    QrAutoDataDoc: TDateField;
    QrAutoCtrlIni: TIntegerField;
    QrAutoNivel: TIntegerField;
    QrAutoVendedor: TIntegerField;
    QrAutoAccount: TIntegerField;
    QrAutoICMS_P: TFloatField;
    QrAutoICMS_V: TFloatField;
    QrAutoDuplicata: TWideStringField;
    QrAutoDepto: TIntegerField;
    QrAutoDescoPor: TIntegerField;
    QrAutoDescoVal: TFloatField;
    QrAutoDescoControle: TIntegerField;
    QrAutoUnidade: TIntegerField;
    QrAutoNFVal: TFloatField;
    QrAutoAntigo: TWideStringField;
    QrAutoExcelGru: TIntegerField;
    QrAutoDoc2: TWideStringField;
    QrAutoCNAB_Sit: TSmallintField;
    QrAutoTipoCH: TSmallintField;
    QrAutoReparcel: TIntegerField;
    QrAutoAtrelado: TIntegerField;
    QrAutoPagMul: TFloatField;
    QrAutoPagJur: TFloatField;
    QrAutoSubPgto1: TIntegerField;
    QrAutoMultiPgto: TIntegerField;
    QrAutoLk: TIntegerField;
    QrAutoDataCad: TDateField;
    QrAutoDataAlt: TDateField;
    QrAutoUserCad: TIntegerField;
    QrAutoUserAlt: TIntegerField;
    QrAutoAlterWeb: TSmallintField;
    QrAutoAtivo: TSmallintField;
    QrEmprestimosTipo: TSmallintField;
    QrEmprestimosCarteira: TIntegerField;
    QrEmprestimosAutorizacao: TIntegerField;
    QrEmprestimosQtde: TFloatField;
    QrEmprestimosNotaFiscal: TIntegerField;
    QrEmprestimosCompensado: TDateField;
    QrEmprestimosSerieCH: TWideStringField;
    QrEmprestimosDocumento: TFloatField;
    QrEmprestimosSit: TIntegerField;
    QrEmprestimosFatNum: TFloatField;
    QrEmprestimosFatParcela: TIntegerField;
    QrEmprestimosID_Pgto: TIntegerField;
    QrEmprestimosID_Quit: TIntegerField;
    QrEmprestimosID_Sub: TSmallintField;
    QrEmprestimosFatura: TWideStringField;
    QrEmprestimosEmitente: TWideStringField;
    QrEmprestimosBanco: TIntegerField;
    QrEmprestimosContaCorrente: TWideStringField;
    QrEmprestimosCNPJCPF: TWideStringField;
    QrEmprestimosLocal: TIntegerField;
    QrEmprestimosCartao: TIntegerField;
    QrEmprestimosLinha: TIntegerField;
    QrEmprestimosOperCount: TIntegerField;
    QrEmprestimosLancto: TIntegerField;
    QrEmprestimosPago: TFloatField;
    QrEmprestimosMez: TIntegerField;
    QrEmprestimosFornecedor: TIntegerField;
    QrEmprestimosCliente: TIntegerField;
    QrEmprestimosCliInt: TIntegerField;
    QrEmprestimosForneceI: TIntegerField;
    QrEmprestimosMoraDia: TFloatField;
    QrEmprestimosMulta: TFloatField;
    QrEmprestimosMoraVal: TFloatField;
    QrEmprestimosMultaVal: TFloatField;
    QrEmprestimosProtesto: TDateField;
    QrEmprestimosDataDoc: TDateField;
    QrEmprestimosCtrlIni: TIntegerField;
    QrEmprestimosNivel: TIntegerField;
    QrEmprestimosVendedor: TIntegerField;
    QrEmprestimosAccount: TIntegerField;
    QrEmprestimosICMS_P: TFloatField;
    QrEmprestimosICMS_V: TFloatField;
    QrEmprestimosDuplicata: TWideStringField;
    QrEmprestimosDepto: TIntegerField;
    QrEmprestimosDescoPor: TIntegerField;
    QrEmprestimosDescoVal: TFloatField;
    QrEmprestimosDescoControle: TIntegerField;
    QrEmprestimosUnidade: TIntegerField;
    QrEmprestimosNFVal: TFloatField;
    QrEmprestimosAntigo: TWideStringField;
    QrEmprestimosExcelGru: TIntegerField;
    QrEmprestimosDoc2: TWideStringField;
    QrEmprestimosCNAB_Sit: TSmallintField;
    QrEmprestimosTipoCH: TSmallintField;
    QrEmprestimosReparcel: TIntegerField;
    QrEmprestimosAtrelado: TIntegerField;
    QrEmprestimosPagMul: TFloatField;
    QrEmprestimosPagJur: TFloatField;
    QrEmprestimosSubPgto1: TIntegerField;
    QrEmprestimosMultiPgto: TIntegerField;
    QrEmprestimosLk: TIntegerField;
    QrEmprestimosDataCad: TDateField;
    QrEmprestimosDataAlt: TDateField;
    QrEmprestimosUserCad: TIntegerField;
    QrEmprestimosUserAlt: TIntegerField;
    QrEmprestimosAlterWeb: TSmallintField;
    QrEmprestimosAtivo: TSmallintField;
    QrCiclo: TmySQLQuery;
    QrCicloAlunosVal: TFloatField;
    QrSAlun: TmySQLQuery;
    QrSAlunAlunosInsc: TFloatField;
    QrSAlunAlunosAula: TFloatField;
    QrSAlunValorTot: TFloatField;
    QrDespProm: TmySQLQuery;
    DsDespProm: TDataSource;
    QrSDespProm: TmySQLQuery;
    QrDespPromNOMECONTA: TWideStringField;
    QrDespPromData: TDateField;
    QrDespPromTipo: TSmallintField;
    QrDespPromCarteira: TIntegerField;
    QrDespPromControle: TIntegerField;
    QrDespPromSub: TSmallintField;
    QrDespPromAutorizacao: TIntegerField;
    QrDespPromGenero: TIntegerField;
    QrDespPromQtde: TFloatField;
    QrDespPromDescricao: TWideStringField;
    QrDespPromNotaFiscal: TIntegerField;
    QrDespPromDebito: TFloatField;
    QrDespPromCredito: TFloatField;
    QrDespPromCompensado: TDateField;
    QrDespPromSerieCH: TWideStringField;
    QrDespPromDocumento: TFloatField;
    QrDespPromSit: TIntegerField;
    QrDespPromVencimento: TDateField;
    QrDespPromFatID: TIntegerField;
    QrDespPromFatID_Sub: TIntegerField;
    QrDespPromFatNum: TFloatField;
    QrDespPromFatParcela: TIntegerField;
    QrDespPromID_Pgto: TIntegerField;
    QrDespPromID_Quit: TIntegerField;
    QrDespPromID_Sub: TSmallintField;
    QrDespPromFatura: TWideStringField;
    QrDespPromEmitente: TWideStringField;
    QrDespPromBanco: TIntegerField;
    QrDespPromContaCorrente: TWideStringField;
    QrDespPromCNPJCPF: TWideStringField;
    QrDespPromLocal: TIntegerField;
    QrDespPromCartao: TIntegerField;
    QrDespPromLinha: TIntegerField;
    QrDespPromOperCount: TIntegerField;
    QrDespPromLancto: TIntegerField;
    QrDespPromPago: TFloatField;
    QrDespPromMez: TIntegerField;
    QrDespPromFornecedor: TIntegerField;
    QrDespPromCliente: TIntegerField;
    QrDespPromCliInt: TIntegerField;
    QrDespPromForneceI: TIntegerField;
    QrDespPromMoraDia: TFloatField;
    QrDespPromMulta: TFloatField;
    QrDespPromMoraVal: TFloatField;
    QrDespPromMultaVal: TFloatField;
    QrDespPromProtesto: TDateField;
    QrDespPromDataDoc: TDateField;
    QrDespPromCtrlIni: TIntegerField;
    QrDespPromNivel: TIntegerField;
    QrDespPromVendedor: TIntegerField;
    QrDespPromAccount: TIntegerField;
    QrDespPromICMS_P: TFloatField;
    QrDespPromICMS_V: TFloatField;
    QrDespPromDuplicata: TWideStringField;
    QrDespPromDepto: TIntegerField;
    QrDespPromDescoPor: TIntegerField;
    QrDespPromDescoVal: TFloatField;
    QrDespPromDescoControle: TIntegerField;
    QrDespPromUnidade: TIntegerField;
    QrDespPromNFVal: TFloatField;
    QrDespPromAntigo: TWideStringField;
    QrDespPromExcelGru: TIntegerField;
    QrDespPromDoc2: TWideStringField;
    QrDespPromCNAB_Sit: TSmallintField;
    QrDespPromTipoCH: TSmallintField;
    QrDespPromReparcel: TIntegerField;
    QrDespPromAtrelado: TIntegerField;
    QrDespPromPagMul: TFloatField;
    QrDespPromPagJur: TFloatField;
    QrDespPromSubPgto1: TIntegerField;
    QrDespPromMultiPgto: TIntegerField;
    QrDespPromLk: TIntegerField;
    QrDespPromDataCad: TDateField;
    QrDespPromDataAlt: TDateField;
    QrDespPromUserCad: TIntegerField;
    QrDespPromUserAlt: TIntegerField;
    QrDespPromAlterWeb: TSmallintField;
    QrDespPromAtivo: TSmallintField;
    QrSDespProfValor: TFloatField;
    QrProfessoresNOMEPROFE: TWideStringField;
    QrProfessoresCodigo: TIntegerField;
    QrEnt: TmySQLQuery;
    QrEntCartPref: TIntegerField;
    QrEntTIPOCART: TIntegerField;
    QrDespPromSEQ: TIntegerField;
    QrDespPromKGT: TIntegerField;
    QrDespPromNOMECARTEIRA: TWideStringField;
    QrSDespPromValor: TFloatField;
    QrDespesas: TmySQLQuery;
    frxDsDespesas: TfrxDBDataset;
    QrDespesasNOMECARTEIRA: TWideStringField;
    QrDespesasNOMECONTA: TWideStringField;
    QrDespesasData: TDateField;
    QrDespesasTipo: TSmallintField;
    QrDespesasCarteira: TIntegerField;
    QrDespesasControle: TIntegerField;
    QrDespesasSub: TSmallintField;
    QrDespesasAutorizacao: TIntegerField;
    QrDespesasGenero: TIntegerField;
    QrDespesasQtde: TFloatField;
    QrDespesasDescricao: TWideStringField;
    QrDespesasNotaFiscal: TIntegerField;
    QrDespesasDebito: TFloatField;
    QrDespesasCredito: TFloatField;
    QrDespesasCompensado: TDateField;
    QrDespesasSerieCH: TWideStringField;
    QrDespesasDocumento: TFloatField;
    QrDespesasSit: TIntegerField;
    QrDespesasVencimento: TDateField;
    QrDespesasFatID: TIntegerField;
    QrDespesasFatID_Sub: TIntegerField;
    QrDespesasFatNum: TFloatField;
    QrDespesasFatParcela: TIntegerField;
    QrDespesasID_Pgto: TIntegerField;
    QrDespesasID_Quit: TIntegerField;
    QrDespesasID_Sub: TSmallintField;
    QrDespesasFatura: TWideStringField;
    QrDespesasEmitente: TWideStringField;
    QrDespesasBanco: TIntegerField;
    QrDespesasContaCorrente: TWideStringField;
    QrDespesasCNPJCPF: TWideStringField;
    QrDespesasLocal: TIntegerField;
    QrDespesasCartao: TIntegerField;
    QrDespesasLinha: TIntegerField;
    QrDespesasOperCount: TIntegerField;
    QrDespesasLancto: TIntegerField;
    QrDespesasPago: TFloatField;
    QrDespesasMez: TIntegerField;
    QrDespesasFornecedor: TIntegerField;
    QrDespesasCliente: TIntegerField;
    QrDespesasCliInt: TIntegerField;
    QrDespesasForneceI: TIntegerField;
    QrDespesasMoraDia: TFloatField;
    QrDespesasMulta: TFloatField;
    QrDespesasMoraVal: TFloatField;
    QrDespesasMultaVal: TFloatField;
    QrDespesasProtesto: TDateField;
    QrDespesasDataDoc: TDateField;
    QrDespesasCtrlIni: TIntegerField;
    QrDespesasNivel: TIntegerField;
    QrDespesasVendedor: TIntegerField;
    QrDespesasAccount: TIntegerField;
    QrDespesasICMS_P: TFloatField;
    QrDespesasICMS_V: TFloatField;
    QrDespesasDuplicata: TWideStringField;
    QrDespesasDepto: TIntegerField;
    QrDespesasDescoPor: TIntegerField;
    QrDespesasDescoVal: TFloatField;
    QrDespesasDescoControle: TIntegerField;
    QrDespesasUnidade: TIntegerField;
    QrDespesasNFVal: TFloatField;
    QrDespesasAntigo: TWideStringField;
    QrDespesasExcelGru: TIntegerField;
    QrDespesasDoc2: TWideStringField;
    QrDespesasCNAB_Sit: TSmallintField;
    QrDespesasTipoCH: TSmallintField;
    QrDespesasReparcel: TIntegerField;
    QrDespesasAtrelado: TIntegerField;
    QrDespesasPagMul: TFloatField;
    QrDespesasPagJur: TFloatField;
    QrDespesasSubPgto1: TIntegerField;
    QrDespesasMultiPgto: TIntegerField;
    QrDespesasLk: TIntegerField;
    QrDespesasDataCad: TDateField;
    QrDespesasDataAlt: TDateField;
    QrDespesasUserCad: TIntegerField;
    QrDespesasUserAlt: TIntegerField;
    QrDespesasAlterWeb: TSmallintField;
    QrDespesasAtivo: TSmallintField;
    QrDespesasKGT: TIntegerField;
    QrDespesasSEQ: TIntegerField;
    QrCartPrf: TmySQLQuery;
    QrCartPrfQtde: TLargeintField;
    QrCartPrfNOMEENT: TWideStringField;
    QrSDespCom: TmySQLQuery;
    QrSDespComValor: TFloatField;
    QrEquiCom: TmySQLQuery;
    QrRolComisCodigo: TIntegerField;
    QrRolComisNome: TWideStringField;
    DsEquiCom: TDataSource;
    QrCurso: TmySQLQuery;
    QrCtaSew: TmySQLQuery;
    QrCtaSewPerceGene: TFloatField;
    QrCtaSewMinQtde: TFloatField;
    QrCursoValor: TFloatField;
    QrSDespDTS: TmySQLQuery;
    QrSDespDTSValor: TFloatField;
    QrTransfDia: TmySQLQuery;
    DsTransfDia: TDataSource;
    QrTransfDiaData: TDateField;
    QrTransfDiaTipo: TSmallintField;
    QrTransfDiaCarteira: TIntegerField;
    QrTransfDiaControle: TIntegerField;
    QrTransfDiadescricao: TWideStringField;
    QrTransfDiacredito: TFloatField;
    QrTransfDiaDebito: TFloatField;
    QrTransfDiaSerieCH: TWideStringField;
    QrTransfDiaDocumento: TFloatField;
    QrTransfDiaVencimento: TDateField;
    QrTransfDiaFatID: TIntegerField;
    QrTransfDiaFatNum: TFloatField;
    QrTransfDiaSub: TSmallintField;
    QrTransfDiaCliInt: TIntegerField;
    QrTransfDiaGenero: TIntegerField;
    QrTransfDiaNOMECART: TWideStringField;
    QrTransfDiaSEQ: TIntegerField;
    QrMovimD: TmySQLQuery;
    DsMovimD: TDataSource;
    QrMovimDComisTipNOME: TWideStringField;
    QrMovimDPOSIv: TFloatField;
    QrMovimDPOSIq: TFloatField;
    QrMovimDPRECO: TFloatField;
    QrMovimDNOMEGRA: TWideStringField;
    QrMovimDNOMETAM: TWideStringField;
    QrMovimDNOMECOR: TWideStringField;
    QrMovimDControle: TIntegerField;
    QrMovimDConta: TIntegerField;
    QrMovimDGrade: TIntegerField;
    QrMovimDCor: TIntegerField;
    QrMovimDTam: TIntegerField;
    QrMovimDQtd: TFloatField;
    QrMovimDVal: TFloatField;
    QrMovimDVen: TFloatField;
    QrMovimDDataPedi: TDateField;
    QrMovimDDataReal: TDateField;
    QrMovimDMotivo: TSmallintField;
    QrMovimDSALDOQTD: TFloatField;
    QrMovimDSALDOVAL: TFloatField;
    QrMovimDLk: TIntegerField;
    QrMovimDDataCad: TDateField;
    QrMovimDDataAlt: TDateField;
    QrMovimDUserCad: TIntegerField;
    QrMovimDUserAlt: TIntegerField;
    QrMovimDAlterWeb: TSmallintField;
    QrMovimDAtivo: TSmallintField;
    QrMovimDSubCtrl: TIntegerField;
    QrMovimDComisTip: TSmallintField;
    QrMovimDComisFat: TFloatField;
    QrMovimDComisVal: TFloatField;
    QrMovimDSubCta: TIntegerField;
    QrSDiaVen: TmySQLQuery;
    QrSDiaCom: TmySQLQuery;
    QrSDiaVenCOMISVAL: TFloatField;
    QrSDiaComCOMISVAL: TFloatField;
    QrSAlunComProfAlu: TFloatField;
    QrSAlunComProfVal: TFloatField;
    QrMaxDia: TmySQLQuery;
    QrMaxDiaControle: TIntegerField;
    QrPagComProf: TmySQLQuery;
    QrPagComProfValor: TFloatField;
    QrProdAlu: TmySQLQuery;
    QrProdAluNome: TWideStringField;
    DsBacen_Pais1: TDataSource;
    QrBacen_Pais: TmySQLQuery;
    QrBacen_PaisCodigo: TIntegerField;
    QrBacen_PaisNome: TWideStringField;
    QrMunici: TmySQLQuery;
    QrMuniciCodigo: TIntegerField;
    QrMuniciNome: TWideStringField;
    DsMunici: TDataSource;
    QrMuniciSigla: TWideStringField;
    QrDespProfAgencia: TIntegerField;
    QrAutoAgencia: TIntegerField;
    QrEmprestimosAgencia: TIntegerField;
    QrDespPromAgencia: TIntegerField;
    QrDespesasAgencia: TIntegerField;
    procedure QrMovimLCalcFields(DataSet: TDataSet);
    procedure QrMovimTCalcFields(DataSet: TDataSet);
    procedure QrMovimVCalcFields(DataSet: TDataSet);
    procedure QrDespProfCalcFields(DataSet: TDataSet);
    procedure QrTransfAllCalcFields(DataSet: TDataSet);
    procedure QrLMovVCalcFields(DataSet: TDataSet);
    procedure QrTransfCCalcFields(DataSet: TDataSet);
    procedure QrTransfDCalcFields(DataSet: TDataSet);
    procedure QrAutoCalcFields(DataSet: TDataSet);
    procedure QrEmprestimosCalcFields(DataSet: TDataSet);
    procedure QrDespPromCalcFields(DataSet: TDataSet);
    procedure QrDespesasCalcFields(DataSet: TDataSet);
    procedure QrTransfDiaCalcFields(DataSet: TDataSet);
    procedure QrMovimDCalcFields(DataSet: TDataSet);
  private
    { Private declarations }
    MovV: String;
  public
    { Public declarations }
    FNotCiclo: Boolean;
    procedure InsereLanctoRateio(Data, Descricao, Vencimento, Compensado:
               String; Tipo, Carteira, Genero, FatID, FatID_Sub, FatNum,
               Cliente, Fornecedor: Integer; Credito, Debito: Double;
               TabLctA: String);
    procedure InsereLanctoEmprestimo(Data, Descricao, Vencimento, Compensado:
              String; Tipo, Carteira, Genero, FatID, FatID_Sub, FatNum,
              Cliente, Fornecedor: Integer; Credito, Debito: Double;
              TabLctA: String);
    function  PreparaImpressaoCiclo(Ciclo, Controle, Professor: Integer;
              TabLctA: String): Boolean;
    procedure CalculaCiclo(Ciclo, Controle, Professor, RolComis: Integer;
              Janela, TabLctA: String);
    function  PodeEncerrarCiclo(Ciclo, Professor: Integer): Boolean;
    function  PodeReabrirCiclo(Ciclo, Professor: Integer): Boolean;
    function  SaldosProfessor(CicloAtual, Professor: Integer;
              SdoProfMov: Double): Boolean;
    procedure CalculaDespPromTurma(Controle: Integer; TabLctA: String);
    procedure CalculaDespProfTurma(Controle: Integer; TabLctA: String);
    procedure CalculaVenComDia(Controle, SubCta, RolComis: Integer);
    procedure CalculaPagtoComisProf(Controle: Integer; TabLctA: String);
    procedure ReopenMunici(Sigla_UF: String);
    function  LocalizaMesCiclosPromProf(Query: TMySQLQuery; Valor: Integer): Boolean;
  end;

var
  DmCiclos: TDmCiclos;

implementation

uses Module, UMySQLModule, UnFinanceiro, UnInternalConsts, UnMLAGeral, UCreate,
Ciclos2, ModuleGeral, DmkDAC_PF;

{$R *.DFM}

procedure TDmCiclos.QrMovimDCalcFields(DataSet: TDataSet);
begin
  if QrMovimDQtd.Value = 0 then QrMovimDPRECO.Value := 0 else
  QrMovimDPRECO.Value := QrMovimDVen.Value / - QrMovimDQtd.Value;
  QrMovimDPOSIq.Value := - QrMovimDQtd.Value;
  QrMovimDPOSIv.Value := QrMovimDVen.Value;
  case QrMovimDComisTip.Value of
    1: QrMovimDComisTipNOME.Value   := '$';
    2: QrMovimDComisTipNOME.Value   := '%';
    else QrMovimDComisTipNOME.Value := '?';
  end;
end;

procedure TDmCiclos.QrMovimLCalcFields(DataSet: TDataSet);
begin
  if QrMovimLQtd.Value = 0 then QrMovimLPRECO.Value := 0 else
  QrMovimLPRECO.Value := QrMovimLVen.Value / QrMovimLQtd.Value;
  QrMovimLPOSIq.Value := QrMovimLQtd.Value * -1;
  QrMovimLPOSIv.Value := QrMovimLVen.Value * -1;
  case QrMovimLComisTip.Value of
    1: QrMovimLComisTipNOME.Value   := '$';
    2: QrMovimLComisTipNOME.Value   := '%';
    else QrMovimLComisTipNOME.Value := '?';
  end;
end;

procedure TDmCiclos.QrMovimTCalcFields(DataSet: TDataSet);
begin
  if QrMovimTQtd.Value = 0 then QrMovimTPRECO.Value := 0 else
  QrMovimTPRECO.Value := -QrMovimTVen.Value / QrMovimTQtd.Value;
  QrMovimTPOSIq.Value := QrMovimTQtd.Value; //* -1;
  QrMovimTPOSIv.Value := QrMovimTVen.Value * -1;
  case QrMovimTComisTip.Value of
    1: QrMovimTComisTipNOME.Value   := '$';
    2: QrMovimTComisTipNOME.Value   := '%';
    else QrMovimTComisTipNOME.Value := '?';
  end;
end;

procedure TDmCiclos.QrMovimVCalcFields(DataSet: TDataSet);
begin
  if QrMovimVQtd.Value = 0 then QrMovimVPRECO.Value := 0 else
  QrMovimVPRECO.Value := QrMovimVVen.Value / - QrMovimVQtd.Value;
  QrMovimVPOSIq.Value := - QrMovimVQtd.Value;
  QrMovimVPOSIv.Value := QrMovimVVen.Value;
  case QrMovimVComisTip.Value of
    1: QrMovimVComisTipNOME.Value   := '$';
    2: QrMovimVComisTipNOME.Value   := '%';
    else QrMovimVComisTipNOME.Value := '?';
  end;
end;

procedure TDmCiclos.QrDespesasCalcFields(DataSet: TDataSet);
begin
  if not FNotCiclo then
  begin
    QrDespesasSEQ.Value := QrDespProf.RecNo;
    QrDespesasKGT.Value := 0;
  end;
end;

procedure TDmCiclos.QrDespProfCalcFields(DataSet: TDataSet);
begin
  QrDespProfSEQ.Value := QrDespProf.RecNo;
  QrDespprofKGT.Value := 0;
end;

procedure TDmCiclos.QrDespPromCalcFields(DataSet: TDataSet);
begin
  QrDespPromSEQ.Value := QrDespProm.RecNo;
  QrDesppromKGT.Value := 0;
end;

procedure TDmCiclos.QrTransfAllCalcFields(DataSet: TDataSet);
begin
  QrTransfAllSEQ.Value := QrTransfAll.RecNo;
end;

procedure TDmCiclos.InsereLanctoRateio(Data, Descricao, Vencimento, Compensado:
String; Tipo, Carteira, Genero, FatID, FatID_Sub, FatNum, Cliente, Fornecedor:
Integer; Credito, Debito: Double; TabLctA: String);
var
  Controle: Double;
begin
  UFinanceiro.LancamentoDefaultVARS;
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                     TabLctA, LAN_CTOS, 'Controle');
  //
  FLAN_Data          := Data;
  FLAN_Vencimento    := Data;
  //FLAN_DataCad       := Geral.FDT(Date, 1);
  //FLAN_Mez           := '';
  FLAN_Descricao     := Descricao;
  FLAN_Compensado    := Data;
  //FLAN_Duplicata     := '';
  //FLAN_Doc2          := '';
  //FLAN_SerieCH       := '';

  //FLAN_Documento     := 0;
  FLAN_Tipo          := Tipo;
  FLAN_Carteira      := Carteira;
  FLAN_Credito       := Credito;
  FLAN_Debito        := Debito;
  FLAN_Genero        := Genero;
  //FLAN_NotaFiscal    := 0;
  FLAN_Sit           := 2;
  FLAN_Controle      := Trunc(Controle);
  //FLAN_Sub           := 0;
  //FLAN_ID_Pgto       := 0;
  //FLAN_Cartao        := 0;
  //FLAN_Linha         := 0;
  FLAN_Fornecedor    := Fornecedor;
  FLAN_Cliente       := Cliente;
  //FLAN_MoraDia       := 0;
  //FLAN_Multa         := 0;
  FLAN_UserCad       := VAR_USUARIO;
  FLAN_DataDoc       := Geral.FDT(Date, 1);
  //FLAN_Vendedor      := 0;
  //FLAN_Account       := 0;
  //FLAN_ICMS_P        := 0;
  //FLAN_ICMS_V        := 0;
  FLAN_CliInt        := Dmod.QrControleCiclCliInt.Value;
  //FLAN_Depto         := 0;
  //FLAN_DescoPor      := 0;
  //FLAN_ForneceI      := 0;
  //FLAN_DescoVal      := 0;
  //FLAN_NFVal         := 0;
  //FLAN_Unidade       := 0;
  //FLAN_Qtde          := 0;
  FLAN_FatID         := FatID;
  FLAN_FatID_Sub     := FatID_Sub;
  FLAN_FatNum        := FatNum;
  //FLAN_FatParcela    := 0;
  //
  //FLAN_MultaVal      := 0;
  //FLAN_MoraVal       := 0;
  //FLAN_CtrlIni       := 0;
  //FLAN_Nivel         := 0;
  //FLAN_CNAB_Sit      := 0;
  //FLAN_TipoCH        := 0;
  //FLAN_Atrelado      := 0;
  //FLAN_SubPgto1      := 0;
  //FLAN_MultiPgto     := 0;
  //
  //FLAN_Emitente      := '';
  //FLAN_CNPJCPF       := '';
  //FLAN_Banco         := 0;
  //FLAN_Agencia       := '';
  //FLAN_ContaCorrente := '';
  UFinanceiro.InsereLancamento(TabLctA);
end;

function TDmCiclos.LocalizaMesCiclosPromProf(Query: TMySQLQuery;
  Valor: Integer): Boolean;
var
  Mes: Integer;
begin
  Result := False;
  //
  Query.DisableControls;
  try
    Query.First;
    //
    while not Query.EOF do
    begin
      Mes := Trunc(Query.FieldByName('Mes').AsFloat);
      //
      if Valor = Mes then
      begin
        Result := True;
        Exit;
      end;
      Query.Next;
    end;
  finally
    Query.EnableControls;
  end;
end;

procedure TDmCiclos.CalculaCiclo(Ciclo, Controle, Professor, RolComis: Integer;
  Janela, TabLctA: String);
var
  Despesas, AlunosPgt, ProdVal, ProdMrg, ProdPgt, TransfCre, TransfDeb,
  RatDebVar, RatTrfFix, RatTrfVar, ComisVal, ComisTrf, ComisPgt, ProfVal,
  EmprestC, EmprestD, AlunosPrc, AlunosVal, DespProf, DespProm,
  ComProfVal, CamProfAlu, ValAluDil, ValAluVen, ComProfSaq: Double;
  AlunosInsc, AlunosAula: Integer;
begin
  Screen.Cursor := crHourGlass;
  try
    // Vendas menos o que comp�e o curso (Moldes)
    QrSProVen.Close;
    QrSProVen.Params[00].AsInteger := Controle;
    QrSProVen.Params[01].AsInteger := RolComis;
    QrSProVen.open;
    ProdVal  := QrSProVenVEN.Value;
    ProdMrg  := QrSProVenMARGEM.Value;
    //
    // Todas vendas, inclusive que comp�e o curso (Moldes)
    // para ver a comiss�o a ser paga ao professor
    QrSProCom.Close;
    QrSProCom.Params[00].AsInteger := Controle;
    QrSProCom.open;
    ComisVal := QrSProComCOMISVAL.Value;
    //
    // Comiss�o paga (transferida) ao professor
    UnDmkDAC_PF.AbreMySQLQuery0(QrSComTrf, Dmod.MyDB, [
      'SELECT SUM(lan.Debito) Debito ',
      'FROM ' + TabLctA + ' lan ',
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ',
      'LEFT JOIN contas con ON con.Codigo=lan.Genero ',
      'WHERE lan.FatID=' + Geral.FF0(VAR_FATID_0702),
      'AND lan.FatID_Sub=1 ',
      'AND lan.FatNum=' + Geral.FF0(Ciclo),
      '']);
    ComisTrf := QrSComTrfDebito.Value;
    //
    // Empr�stimos ao professor e seus pagamentos
    UnDmkDAC_PF.AbreMySQLQuery0(QrSEmprestimos, Dmod.MyDB, [
      'SELECT SUM(Credito) Credito, SUM(Debito) Debito ',
      'FROM ' + TabLctA + ' lan ',
      'WHERE lan.FatID=' + Geral.FF0(VAR_FATID_0731),
      'AND lan.FatNum=' + Geral.FF0(Ciclo),
      '']);
    EmprestC := QrSEmprestimosCredito.Value;
    EmprestD := QrSEmprestimosDebito.Value;
    // Realmente pago ao professor
    ComisPgt := ComisTrf + EmprestD - EmprestC;
    //
    //  Pagamentos das mercadorias
    UnDmkDAC_PF.AbreMySQLQuery0(QrSProPgt, Dmod.MyDB, [
      'SELECT SUM(lan.Credito) Valor ',
      'FROM ' + TabLctA + ' lan ',
      'WHERE lan.FatID=' + Geral.FF0(VAR_FATID_0721),
      'AND lan.FatNum=' + Geral.FF0(Ciclo),
      '']);
    //
    ProdPgt := QrSProPgtValor.Value;
    //
    // Tranmsfer�ncias entre carteiras
    UnDmkDAC_PF.AbreMySQLQuery0(QrSTransf, Dmod.MyDB, [
      'SELECT SUM(lan.Credito) Credito, SUM(lan.Debito) Debito ',
      'FROM ' + TabLctA + ' lan ',
      'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ',
      'WHERE lan.FatID=' + Geral.FF0(VAR_FATID_0700),
      'AND lan.FatNum=' + Geral.FF0(Ciclo),
      'AND car.EntiDent=' + Geral.FF0(Professor),
      '']);
    TransfCre := QrSTransfCredito.Value;
    TransfDeb := QrSTransfDebito.Value;
    //
    //  Pagamentos de alunos
    UnDmkDAC_PF.AbreMySQLQuery0(QrSAuto, Dmod.MyDB, [
      'SELECT SUM(Credito) Valor ',
      'FROM ' + TabLctA + ' lan ',
      'WHERE lan.FatID=' + Geral.FF0(VAR_FATID_0711),
      'AND lan.FatNum=' + Geral.FF0(Ciclo),
      '']);
    AlunosPgt := QrSAutoValor.Value;
    //
    // lan�amentos de despesas
    UnDmkDAC_PF.AbreMySQLQuery0(QrSDespProf, Dmod.MyDB, [
      'SELECT SUM(Debito) Valor ',
      'FROM ' + TabLctA + ' lan ',
      'WHERE lan.FatID=' + Geral.FF0(VAR_FATID_0701),
      'AND lan.FatNum=' + Geral.FF0(Ciclo),
      '']);
    DespProf := QrSDespProfValor.Value;
    //
    UnDmkDAC_PF.AbreMySQLQuery0(QrSDespProm, Dmod.MyDB, [
      'SELECT SUM(Debito) Valor ',
      'FROM ' + TabLctA + ' lan ',
      'WHERE lan.FatID=' + Geral.FF0(VAR_FATID_0703),
      'AND lan.FatNum=' + Geral.FF0(Ciclo),
      '']);
    DespProm := QrSDespPromValor.Value;
    Despesas := DespProf + DespProm;
    //
    // Outros n�meros
    QrCiclo.Close;
    QrCiclo.Params[0].AsInteger := Ciclo;
    QrCiclo.Open;
    ComProfSaq := QrCicloAlunosVal.Value + (*ERRO*)
      ProdVal + TransfCre - TransfDeb - Despesas;
    ProfVal := ComisVal - ComProfSaq;
    //
    // Somas por tipos de rateios
    UnDmkDAC_PF.AbreMySQLQuery0(QrRatSub, Dmod.MyDB, [
      'SELECT SUM(Credito) Credito, FatID_Sub ',
      'FROM ' + TabLctA,
      'WHERE FatID=' + Geral.FF0(VAR_FATID_0711),
      'AND FatNum=' + Geral.FF0(Ciclo),
      'GROUP BY FatID_Sub ',
      '']);
    RatDebVar := 0;
    RatTrfFix := 0;
    RatTrfVar := 0;
    //
    // Alunos e valor de curso
    QrSAlun.Close;
    QrSAlun.Params[0].AsInteger := Ciclo;
    QrSAlun.Open;
    AlunosInsc := Trunc(QrSAlunAlunosInsc.Value);
    AlunosAula := Trunc(QrSAlunAlunosAula.Value);
    AlunosVal  := QrSAlunValorTot.Value;
    ValAluDil  := QrSAlunComProfVal.Value;
    ValAluVen  := QrSAlunComProfAlu.Value;
    if AlunosAula > 0  then
      AlunosPrc  := AlunosVal / AlunosAula
    else AlunosPrc := 0;
    //
    while not QrRatSub.Eof do
    begin
      case QrRatSubFatID_Sub.Value of
        1: RatDebVar := QrRatSubCredito.Value;
        2: RatTrfFix := QrRatSubCredito.Value;
        3: RatTrfVar := QrRatSubCredito.Value;
      end;
      QrRatSub.Next;
    end;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ciclosaula SET ComProfSaq=0 WHERE Codigo=:P0');
    Dmod.QrUpd.Params[0].AsInteger := Ciclo;
    Dmod.QrUpd.ExecSQL;
    //
    QrMaxDia.Close;
    QrMaxDia.Params[00].AsInteger := Ciclo;
    QrMaxDia.Params[01].AsInteger := Ciclo;
    QrMaxDia.Open;
    if QrMaxDiaControle.Value > 0 then
    begin
      UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ciclosaula', False, [
        'ComProfSaq'
      ], ['Controle'], [
         ComProfSaq
      ], [QrMaxDiaControle.Value], True)
    end;
    //
    if UMyMod.SQLInsUpd(Dmod.QrUpd, stUpd, 'ciclos', false, [
      'ProdVal', 'ProdMrg', 'ProdPgt', 'TransfCre', 'TransfDeb', 'AlunosPgt',
      'Despesas', 'RatDebVar', 'RatTrfFix', 'RatTrfVar', 'ComisVal',
      'ComisTrf', 'ComisPgt', 'ProfVal', 'EmprestC', 'EmprestD',
      'AlunosInsc', 'AlunosAula', 'AlunosPrc', 'AlunosVal',
      'DespProf', 'DespProm', 'ValAluDil', 'ValAluVen'
    ], ['Codigo'], [
       Prodval,   ProdMrg,   ProdPgt,   TransfCre,   TransfDeb,   AlunosPgt,
       Despesas,   RatDebVar,   RatTrfFix,   RatTrfVar,   ComisVal,
       ComisTrf,   ComisPgt,   ProfVal,   EmprestC,   EmprestD,
       AlunosInsc, AlunosAula, AlunosPrc, AlunosVal,
       DespProf, DespProm, ValAluDil, ValAluVen
    ], [Ciclo], True) then
    begin
      if Length(Janela) > 0 then
      begin
        FmCiclos2.LocCod(Ciclo, Ciclo);
      end;
    end;

  finally
    Screen.Cursor := crDefault;
  end;
end;

function TDmCiclos.PreparaImpressaoCiclo(Ciclo, Controle, Professor: Integer;
  TabLctA: String): Boolean;
begin
  MovV := UCriar.RecriaTempTable('movv', DModG.QrUpdPID1, False);
  //
  QrItens.Close;
  QrItens.Params[0].AsInteger := Controle;
  QrItens.Open;
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('INSERT INTO movv SET ');
  DmodG.QrUpdPID1.SQL.Add('CodiGra=:P0, CodiCor=:P1, CodiTam=:P2,');
  DmodG.QrUpdPID1.SQL.Add('NomeGra=:P3, NomeCor=:P4, NomeTam=:P5');
  //DmodG.QrUpdPID1.SQL.Add('VendQtd=:P6, VendVal=:P7, CmisVal=:P8');
  while not QrItens.Eof do
  begin
    DmodG.QrUpdPID1.Params[00].AsInteger := QrItensGrade.Value;
    DmodG.QrUpdPID1.Params[01].AsInteger := QrItensCor.Value;
    DmodG.QrUpdPID1.Params[02].AsInteger := QrItensTam.Value;
    DmodG.QrUpdPID1.Params[03].AsString  := QrItensNOMEGRA.Value;
    DmodG.QrUpdPID1.Params[04].AsString  := QrItensNOMECOR.Value;
    DmodG.QrUpdPID1.Params[05].AsString  := QrItensNOMETAM.Value;
    DmodG.QrUpdPID1.ExecSQL;
    //
    QrItens.Next;
  end;
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('UPDATE movv SET ');
  DmodG.QrUpdPID1.SQL.Add('LevaQtd=:P0');
  DmodG.QrUpdPID1.SQL.Add('WHERE CodiGra=:Pa AND CodiCor=:Pb AND CodiTam=:Pc');
  QrVCPC.Close;
  QrVCPC.Params[00].AsInteger := Controle;
  QrVCPC.Params[01].AsInteger := 1;
  QrVCPC.Open;
  //
  while not QrVCPC.Eof do
  begin
    DmodG.QrUpdPID1.Params[00].AsFloat   := -QrVCPCQtd.Value;
    DmodG.QrUpdPID1.Params[01].AsInteger := QrVCPCGrade.Value;
    DmodG.QrUpdPID1.Params[02].AsInteger := QrVCPCCor.Value;
    DmodG.QrUpdPID1.Params[03].AsInteger := QrVCPCTam.Value;
    DmodG.QrUpdPID1.ExecSQL;
    QrVCPC.Next;
  end;
  //
  DmodG.QrUpdPID1.SQL.Clear;
  DmodG.QrUpdPID1.SQL.Add('UPDATE movv SET ');
  DmodG.QrUpdPID1.SQL.Add('VendQtd=:P0, VendVal=:P1, CmisVal=:P2 ');
  DmodG.QrUpdPID1.SQL.Add('WHERE CodiGra=:Pa AND CodiCor=:Pb AND CodiTam=:Pc');
  QrVCPC.Close;
  QrVCPC.Params[00].AsInteger := Controle;
  QrVCPC.Params[01].AsInteger := 3;
  QrVCPC.Open;
  //
  while not QrVCPC.Eof do
  begin
    DmodG.QrUpdPID1.Params[00].AsFloat   := -QrVCPCQtd.Value;
    DmodG.QrUpdPID1.Params[01].AsFloat   := QrVCPCVen.Value;
    DmodG.QrUpdPID1.Params[02].AsFloat   := QrVCPCComisVal.Value;
    DmodG.QrUpdPID1.Params[03].AsInteger := QrVCPCGrade.Value;
    DmodG.QrUpdPID1.Params[04].AsInteger := QrVCPCCor.Value;
    DmodG.QrUpdPID1.Params[05].AsInteger := QrVCPCTam.Value;
    DmodG.QrUpdPID1.ExecSQL;
    QrVCPC.Next;
  end;
  //
  QrLMovV.Close;
  QrLMovV.Database := DModG.MyPID_DB;
  QrLMovV.Open;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTransfC, Dmod.MyDB, [
    'SELECT lan.Data, lan.Credito, Descricao ',
    'FROM ' + TabLctA + ' lan ',
    'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ',
    'WHERE Credito > 0 ',
    'AND lan.FatID=' + Geral.FF0(VAR_FATID_0700),
    'AND lan.FatNum=' + Geral.FF0(Ciclo),
    'AND car.EntiDent=' + Geral.FF0(Professor),
    '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrTransfD, Dmod.MyDB, [
    'SELECT lan.Data, lan.Debito, Descricao ',
    'FROM ' + TabLctA + ' lan ',
    'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ',
    'WHERE Debito > 0 ',
    'AND lan.FatID=' + Geral.FF0(VAR_FATID_0700),
    'AND lan.FatNum=' + Geral.FF0(Ciclo),
    'AND car.EntiDent=' + Geral.FF0(Professor),
    '']);
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrDespesas, Dmod.MyDB, [
    'SELECT car.Nome NOMECARTEIRA, con.Nome NOMECONTA, ',
    'lan.* ',
    'FROM ' + TabLctA + ' lan ',
    'LEFT JOIN carteiras car ON car.Codigo=lan.Carteira ',
    'LEFT JOIN contas con ON con.Codigo=lan.Genero ',
    'WHERE lan.FatID in (701,703) ',
    'AND lan.FatNum=' + Geral.FF0(Ciclo),
    '']);
  //
  if (QrTransfC.RecordCount = 0) and (QrTransfD.RecordCount = 0) then
  begin
    QrCartPrf.Close;
    QrCartPrf.Params[0].AsInteger := Professor;
    QrCartPrf.Open;
    //
    if QrCartPrfQtde.Value = 0 then
      Geral.MB_Aviso('A entidade  ' + QrCartPrfNOMEENT.Value +
        ' n�o � detentora de nenhuma carteira, o que impede de separar as ' +
        'transfer�ncias!');
  end;
  //
  Result := True;
end;

procedure TDmCiclos.QrLMovVCalcFields(DataSet: TDataSet);
begin
  if QrLMovVLevaQtd.Value = 0 then
    QrLMovVRETORNOU.Value := 0
  else
    QrLMovVRETORNOU.Value := QrLMovVLevaQtd.Value - QrLMovVVendQtd.Value;
  QrLMovVKGT.Value := 0;
end;

procedure TDmCiclos.QrTransfCCalcFields(DataSet: TDataSet);
begin
  QrTransfCKGT.Value := 0;
end;

procedure TDmCiclos.QrTransfDCalcFields(DataSet: TDataSet);
begin
  QrTransfDKGT.Value := 0;
end;

procedure TDmCiclos.QrTransfDiaCalcFields(DataSet: TDataSet);
begin
  QrTransfDiaSEQ.Value := QrTransfDia.RecNo;
end;

procedure TDmCiclos.ReopenMunici(Sigla_UF: String);
begin
  QrMunici.Close;
  QrMunici.Database := DModG.AllID_DB;
  QrMunici.SQL.Clear;
  QrMunici.SQL.Add('SELECT dmu.Codigo, dmu.Nome, duf.Sigla');
  QrMunici.SQL.Add('FROM dtb_munici dmu');
  QrMunici.SQL.Add('LEFT JOIN dtb_ufs duf ON duf.Codigo = dmu.DTB_UF');
  if Length(Sigla_UF) > 0 then
    QrMunici.SQL.Add('WHERE LEFT(dmu.Codigo, 2)="' +
      Geral.FF0(Geral.GetCodigoUF_IBGE_DTB_da_SiglaUF(Sigla_UF)) + '"');
  QrMunici.SQL.Add('ORDER BY Nome');
  QrMunici.Open;
end;

function TDmCiclos.PodeEncerrarCiclo(Ciclo, professor: Integer): Boolean;
begin
  Result := False;
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT *');
  Dmod.QrAux.SQL.Add('FROM ciclos');
  Dmod.QrAux.SQL.Add('WHERE Codigo<:P0');
  Dmod.QrAux.SQL.Add('AND Professor=:P1');
  Dmod.QrAux.SQL.Add('AND Status < 9');
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.Params[00].AsInteger := Ciclo;
  Dmod.QrAux.Params[01].AsInteger := Professor;
  Dmod.QrAux.Open;
  //
  case Dmod.QrAux.RecordCount of
    0: Result := True;
    1: Geral.MB_Aviso('O ciclo n�o pode ser encerrado, pois h� ' +
        'um ciclo anterior com o mesmo professor que n�o foi encerrado ainda!');
    else Geral.MB_Aviso('O ciclo n�o pode ser encerrado, pois h� ' +
           Geral.FF0(Dmod.QrAux.RecordCount) + ' ciclos anteriores com o mesmo ' +
           'professor que n�o foram encerrados ainda!');
  end;
end;

function TDmCiclos.PodeReabrirCiclo(Ciclo, Professor: Integer): Boolean;
begin
  Result := False;
  //
  Dmod.QrAux.Close;
  Dmod.QrAux.SQL.Clear;
  Dmod.QrAux.SQL.Add('SELECT *');
  Dmod.QrAux.SQL.Add('FROM ciclos');
  Dmod.QrAux.SQL.Add('WHERE Codigo>:P0');
  Dmod.QrAux.SQL.Add('AND Professor=:P1');
  Dmod.QrAux.SQL.Add('AND Status = 9');
  Dmod.QrAux.SQL.Add('');
  Dmod.QrAux.Params[00].AsInteger := Ciclo;
  Dmod.QrAux.Params[01].AsInteger := Professor;
  Dmod.QrAux.Open;
  //
  case Dmod.QrAux.RecordCount of
    0: Result := True;
    1: Geral.MB_Aviso('O ciclo n�o pode ser reaberto, pois h� ' +
        'um ciclo posterior com o mesmo professor j� encerrado!');
    else Geral.MB_Aviso('O ciclo n�o pode ser reaberto, pois h� ' +
      Geral.FF0(Dmod.QrAux.RecordCount) + ' ciclos posteriores com o mesmo ' +
      'professor j� encerrados!');
  end;
end;

function TDmCiclos.SaldosProfessor(CicloAtual, Professor: Integer;
SdoProfMov: Double): Boolean;
var
  UltimoCiclo: Integer;
  SdoProfIni: Double;
var
  Val: String;
begin
  //Result := False;
  Screen.Cursor := crHourGlass;
  try
    Dmod.QrAux.Close;
    Dmod.QrAux.SQL.Clear;
    Dmod.QrAux.SQL.Add('SELECT MAX(Codigo) Codigo');
    Dmod.QrAux.SQL.Add('FROM ciclos');
    Dmod.QrAux.SQL.Add('WHERE Codigo<:P0');
    Dmod.QrAux.SQL.Add('AND Professor=:P1');
    Dmod.QrAux.Params[00].AsInteger := CicloAtual;
    Dmod.QrAux.Params[01].AsInteger := Professor;
    Dmod.QrAux.Open;
    SdoProfIni  := 0;
    UltimoCiclo := Dmod.QrAux.FieldByName('Codigo').AsInteger;
    if UltimoCiclo = 0 then
    begin
      Val := '0,00';
      if InputQuery('Saldo Anterior Professor', 'Informe o valor:', Val) then
        SdoProfIni := Geral.DMV(Val);
    end else begin
      Dmod.QrAux.Close;
      Dmod.QrAux.SQL.Clear;
      Dmod.QrAux.SQL.Add('SELECT SdoProfFim');
      Dmod.QrAux.SQL.Add('FROM ciclos');
      Dmod.QrAux.SQL.Add('WHERE Codigo=:P0');
      Dmod.QrAux.Params[00].AsInteger := UltimoCiclo;
      Dmod.QrAux.Open;
      SdoProfIni := Dmod.QrAux.FieldByName('SdoProfFim').AsFloat;
    end;
    //
    Dmod.QrUpd.SQL.Clear;
    Dmod.QrUpd.SQL.Add('UPDATE ciclos SET SdoProfIni=:P0, SdoProfFim=:P1 ');
    Dmod.QrUpd.SQL.Add('WHERE Codigo=:P2');
    Dmod.QrUpd.Params[00].AsFloat   := SdoProfIni;
    Dmod.QrUpd.Params[01].AsFloat   := SdoProfIni + SdoProfMov;
    Dmod.QrUpd.Params[02].AsInteger := CicloAtual;
    Dmod.QrUpd.ExecSQL;
  finally
    Screen.Cursor := crDefault;
  end;
  Result := True;
end;

procedure TDmCiclos.QrAutoCalcFields(DataSet: TDataSet);
begin
  QrAutoSEQ.Value := QrAuto.RecNo;
end;

procedure TDmCiclos.InsereLanctoEmprestimo(Data, Descricao, Vencimento, Compensado:
String; Tipo, Carteira, Genero, FatID, FatID_Sub, FatNum, Cliente, Fornecedor: Integer;
Credito, Debito: Double; TabLctA: String);
var
  Controle: Double;
begin
  UFinanceiro.LancamentoDefaultVARS;
  Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres', 'Controle',
                     TabLctA, LAN_CTOS, 'Controle');
  //
  FLAN_Data          := Data;
  FLAN_Vencimento    := Data;
  //FLAN_DataCad       := Geral.FDT(Date, 1);
  //FLAN_Mez           := '';
  FLAN_Descricao     := Descricao;
  FLAN_Compensado    := Data;
  //FLAN_Duplicata     := '';
  //FLAN_Doc2          := '';
  //FLAN_SerieCH       := '';

  //FLAN_Documento     := 0;
  FLAN_Tipo          := Tipo;
  FLAN_Carteira      := Carteira;
  FLAN_Credito       := Credito;
  FLAN_Debito        := Debito;
  FLAN_Genero        := Genero;
  //FLAN_NotaFiscal    := 0;
  FLAN_Sit           := 2;
  FLAN_Controle      := Trunc(Controle);
  //FLAN_Sub           := 0;
  //FLAN_ID_Pgto       := 0;
  //FLAN_Cartao        := 0;
  //FLAN_Linha         := 0;
  FLAN_Fornecedor    := Fornecedor;
  FLAN_Cliente       := Cliente;
  //FLAN_MoraDia       := 0;
  //FLAN_Multa         := 0;
  FLAN_UserCad       := VAR_USUARIO;
  FLAN_DataDoc       := Geral.FDT(Date, 1);
  //FLAN_Vendedor      := 0;
  //FLAN_Account       := 0;
  //FLAN_ICMS_P        := 0;
  //FLAN_ICMS_V        := 0;
  FLAN_CliInt        := Dmod.QrControleDono.Value;
  //FLAN_Depto         := 0;
  //FLAN_DescoPor      := 0;
  //FLAN_ForneceI      := 0;
  //FLAN_DescoVal      := 0;
  //FLAN_NFVal         := 0;
  //FLAN_Unidade       := 0;
  //FLAN_Qtde          := 0;
  FLAN_FatID         := FatID;
  FLAN_FatID_Sub     := FatID_Sub;
  FLAN_FatNum        := FatNum;
  //FLAN_FatParcela    := 0;
  //
  //FLAN_MultaVal      := 0;
  //FLAN_MoraVal       := 0;
  //FLAN_CtrlIni       := 0;
  //FLAN_Nivel         := 0;
  //FLAN_CNAB_Sit      := 0;
  //FLAN_TipoCH        := 0;
  //FLAN_Atrelado      := 0;
  //FLAN_SubPgto1      := 0;
  //FLAN_MultiPgto     := 0;
  //
  //FLAN_Emitente      := '';
  //FLAN_CNPJCPF       := '';
  //FLAN_Banco         := 0;
  //FLAN_Agencia       := '';
  //FLAN_ContaCorrente := '';
  UFinanceiro.InsereLancamento(TabLctA);
end;

procedure TDmCiclos.QrEmprestimosCalcFields(DataSet: TDataSet);
begin
  QrEmprestimosSEQ.Value := QrEmprestimos.RecNo;
end;

procedure TDmCiclos.CalculaDespPromTurma(Controle: Integer; TabLctA: String);
var
  ComDeb, DTSDeb: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSDespCom, Dmod.MyDB, [
    'SELECT SUM(lan.Debito-lan.Credito) Valor ',
    'FROM ' + TabLctA + ' lan ',
    'WHERE lan.FatID IN (703,704) ',
    'AND lan.FatID_Sub=' + Geral.FF0(Controle),
    'AND lan.Genero IN ( ',
    '  SELECT Codigo ',
    '  FROM contas ',
    '  WHERE CentroCusto=' + Geral.FF0(Dmod.QrControleCCuPromCom.Value),
    '  OR CentroCusto=0 ',
    ') ',
    '']);
  ComDeb := QrSDespComValor.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSDespDTS, Dmod.MyDB, [
    'SELECT SUM(lan.Debito-lan.Credito) Valor ',
    'FROM ' + TabLctA + ' lan ',
    'WHERE lan.FatID IN (703,704) ',
    'AND lan.FatID_Sub=' + Geral.FF0(Controle),
    'AND lan.Genero IN ( ',
    '  SELECT Codigo ',
    '  FROM contas ',
    '  WHERE CentroCusto=' + Geral.FF0(Dmod.QrControleCCuPromDTS.Value),
    '  AND CentroCusto<>0 ',
    ') ',
    '']);
  DTSDeb := QrSDespDTSValor.Value;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ciclosaula SET ComPromDeb=:P0, DTSPromDeb=:P1');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P2');
  Dmod.QrUpd.Params[00].AsFloat   := ComDeb;
  Dmod.QrUpd.Params[01].AsFloat   := DTSDeb;
  Dmod.QrUpd.Params[02].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TDmCiclos.CalculaDespProfTurma(Controle: Integer; TabLctA: String);
var
  ComDeb, DTSDeb: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrSDespCom, Dmod.MyDB, [
    'SELECT SUM(lan.Debito-lan.Credito) Valor ',
    'FROM ' + TabLctA + ' lan ',
    'WHERE lan.FatID IN (' + Geral.FF0(VAR_FATID_0701) + ') ',
    'AND lan.FatID_Sub=' + Geral.FF0(Controle),
    'AND lan.Genero IN ( ',
    '  SELECT Codigo ',
    '  FROM contas ',
    '  WHERE CentroCusto=' + Geral.FF0(Dmod.QrControleCCuProfCom.Value),
    '  OR CentroCusto=0 ',
    ') ',
    '']);
  ComDeb := QrSDespComValor.Value;
  //
  UnDmkDAC_PF.AbreMySQLQuery0(QrSDespDTS, Dmod.MyDB, [
    'SELECT SUM(lan.Debito-lan.Credito) Valor ',
    'FROM ' + TabLctA + ' lan ',
    'WHERE lan.FatID IN (' + Geral.FF0(VAR_FATID_0701) + ') ',
    'AND lan.FatID_Sub=' + Geral.FF0(Controle),
    'AND lan.Genero IN ( ',
    '  SELECT Codigo ',
    '  FROM contas ',
    '  WHERE CentroCusto=' + Geral.FF0(Dmod.QrControleCCuProfDTS.Value),
    '  AND CentroCusto<>0 ',
    ') ',
    '']);
  DTSDeb := QrSDespDTSValor.Value;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ciclosaula SET ComProfDeb=:P0, DTSProfDeb=:P1');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P2');
  Dmod.QrUpd.Params[00].AsFloat   := ComDeb;
  Dmod.QrUpd.Params[01].AsFloat   := DTSDeb;
  Dmod.QrUpd.Params[02].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
end;

procedure TDmCiclos.CalculaVenComDia(Controle, SubCta, RolComis: Integer);
var
  PrdVal, AluVal, AllVal: Double;
begin
  // Vendas menos o que comp�e o curso (Moldes)
  QrSDiaVen.Close;
  QrSDiaVen.Params[00].AsInteger := Controle;
  QrSDiaVen.Params[01].AsInteger := SubCta;
  QrSDiaVen.Params[02].AsInteger := RolComis;
  QrSDiaVen.Open;
  PrdVal  := QrSDiaVenCOMISVAL.Value;
  //
  // Todas vendas, inclusive que comp�e o curso (Moldes)
  // para ver a comiss�o a ser paga ao professor
  QrSDiaCom.Close;
  QrSDiaCom.Params[00].AsInteger := Controle;
  QrSDiaCom.Params[01].AsInteger := SubCta;
  QrSDiaCom.open;
  AllVal := QrSDiaComCOMISVAL.Value;
  //
  AluVal := AllVal - PrdVal;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ciclosaula SET ComProfAlu=:P0, ComProfPrd=:P1, ');
  Dmod.QrUpd.SQL.Add('ComProfAll=:P2 WHERE Controle=:P3');
  Dmod.QrUpd.Params[00].AsFloat   := AluVal;
  Dmod.QrUpd.Params[01].AsFloat   := PrdVal;
  Dmod.QrUpd.Params[02].AsFloat   := AllVal;
  Dmod.QrUpd.Params[03].AsInteger := SubCta;
  Dmod.QrUpd.ExecSQL;
  //
end;

procedure TDmCiclos.CalculaPagtoComisProf(Controle: Integer; TabLctA: String);
var
  ComVal: Double;
begin
  UnDmkDAC_PF.AbreMySQLQuery0(QrPagComProf, Dmod.MyDB, [
    'SELECT SUM(lan.Debito-lan.Credito) Valor ',
    'FROM ' + TabLctA + ' lan ',
    'WHERE lan.FatID IN (705) ',
    'AND lan.FatID_Sub=' + Geral.FF0(Controle),
    '']);
  ComVal := QrPagComProfValor.Value;
  //
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('UPDATE ciclosaula SET ComProfPgt=:P0');
  Dmod.QrUpd.SQL.Add('WHERE Controle=:P1');
  Dmod.QrUpd.Params[00].AsFloat   := ComVal;
  Dmod.QrUpd.Params[01].AsInteger := Controle;
  Dmod.QrUpd.ExecSQL;
  //
end;

end.




