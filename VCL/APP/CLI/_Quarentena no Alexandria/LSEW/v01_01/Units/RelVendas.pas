unit RelVendas;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, UnInternalConsts, Db, MyDBCheck,
  mySQLDbTables, DBCtrls, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UMySQLModule, dmkLabel, ABSMain, Grids, DBGrids, dmkDBGrid, frxClass,
  frxDBSet, Mask, dmkDBEdit, dmkCheckGroup, CheckLst, Menus, UnDmkProcFunc,
  UnDmkEnums;

type
  TFmRelVendas = class(TForm)
    PainelConfirma: TPanel;
    BtPesquisa: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    BtImprime: TBitBtn;
    Panel3: TPanel;
    GBDataPed: TGroupBox;
    CkDataPedFim: TCheckBox;
    TPDataPedFim: TDateTimePicker;
    TPDataPedIni: TDateTimePicker;
    CkDataPedIni: TCheckBox;
    Label1: TLabel;
    EdVendedor: TdmkEditCB;
    CBVendedor: TdmkDBLookupComboBox;
    frxRelVendas: TfrxReport;
    DsMoviV: TDataSource;
    QrMoviV: TmySQLQuery;
    QrVendedor: TmySQLQuery;
    DsVendedor: TDataSource;
    QrStatusV: TmySQLQuery;
    DsStatusV: TDataSource;
    QrStatusVCodigo: TIntegerField;
    QrStatusVNome: TWideStringField;
    QrMoviVCodigo: TIntegerField;
    QrMoviVTotal: TFloatField;
    QrMoviVNOMECLI: TWideStringField;
    QrMoviVNOMEENT: TWideStringField;
    QrMoviVNOMESTAT: TWideStringField;
    QrMoviVDataPedi: TDateField;
    QrMoviVDataReal: TDateField;
    QrMoviVDescon: TFloatField;
    QrMoviVFrete: TFloatField;
    QrMoviVTOTALVDA: TFloatField;
    QrMoviVNOMECAD2: TWideStringField;
    QrMoviVPago: TFloatField;
    QrMoviVUserCad: TIntegerField;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    CkLBStatus: TCheckListBox;
    CkLBGraCusPrc: TCheckListBox;
    QrGraCusPrc: TmySQLQuery;
    QrGraCusPrcCodigo: TIntegerField;
    QrGraCusPrcNome: TWideStringField;
    DsGraCusPrc: TDataSource;
    QrGraCusPrcAplicacao: TIntegerField;
    QrLoc: TmySQLQuery;
    QrMoviVDataTXT: TWideStringField;
    QrMoviVDATA_TXT: TWideStringField;
    frxDsMoviV: TfrxDBDataset;
    QrVendedorNumero: TIntegerField;
    QrVendedorNOMEENT: TWideStringField;
    QrMoviVDiaHora_TXT: TWideStringField;
    dmkDBGrid1: TdmkDBGrid;
    frxRelVendasDetal: TfrxReport;
    QrMoviM: TmySQLQuery;
    frxDsMoviM: TfrxDBDataset;
    QrMoviVControle: TIntegerField;
    PMImprime: TPopupMenu;
    Sinttico1: TMenuItem;
    Analtico1: TMenuItem;
    QrMoviMNOMEGRA: TWideStringField;
    QrMoviMNOMETAM: TWideStringField;
    QrMoviMNOMECOR: TWideStringField;
    QrMoviMControle: TIntegerField;
    QrMoviMConta: TIntegerField;
    QrMoviMGrade: TIntegerField;
    QrMoviMCor: TIntegerField;
    QrMoviMTam: TIntegerField;
    QrMoviMQtd: TFloatField;
    QrMoviMVal: TFloatField;
    QrMoviMVen: TFloatField;
    QrMoviMDataPedi: TDateField;
    QrMoviMDataReal: TDateField;
    QrMoviMMotivo: TSmallintField;
    QrMoviMSALDOQTD: TFloatField;
    QrMoviMSALDOVAL: TFloatField;
    QrMoviMLk: TIntegerField;
    QrMoviMDataCad: TDateField;
    QrMoviMDataAlt: TDateField;
    QrMoviMUserCad: TIntegerField;
    QrMoviMUserAlt: TIntegerField;
    QrMoviMAlterWeb: TSmallintField;
    QrMoviMAtivo: TSmallintField;
    QrMoviMSubCtrl: TIntegerField;
    QrMoviMComisTip: TSmallintField;
    QrMoviMComisFat: TFloatField;
    QrMoviMComisVal: TFloatField;
    QrMoviMSubCta: TIntegerField;
    QrMoviMKit: TIntegerField;
    QrMoviMIDCtrl: TIntegerField;
    QrMoviMPRECO: TFloatField;
    QrMoviMPOSIq: TFloatField;
    QrMoviMPOSIv: TFloatField;
    QrLoc2: TmySQLQuery;
    QrMoviMCUSMEDUNI: TFloatField;
    QrCustoMedio: TmySQLQuery;
    QrCustoMedioCUSTOMEDIO: TFloatField;
    QrMoviMCUSMEDTOT: TFloatField;
    QrLoc2Controle: TIntegerField;
    PB1: TProgressBar;
    Sintticocomcustomdio1: TMenuItem;
    QrLocMovV: TmySQLQuery;
    QrLocMovVCodigo: TIntegerField;
    QrLocMovVDiaHora: TWideStringField;
    QrLocMovVCliente: TWideStringField;
    QrLocMovVVendedor: TWideStringField;
    QrLocMovVStatus: TWideStringField;
    QrLocMovVValor: TFloatField;
    QrLocMovVDesconto: TFloatField;
    QrLocMovVFrete: TFloatField;
    QrLocMovVTotal: TFloatField;
    QrLocMovVCustoMedio: TFloatField;
    frxRelVendasCus: TfrxReport;
    frxDsLocMovV: TfrxDBDataset;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtPesquisaClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure QrMoviVCalcFields(DataSet: TDataSet);
    procedure frxRelVendasGetValue(const VarName: string; var Value: Variant);
    procedure DBGrid1DblClick(Sender: TObject);
    procedure QrMoviVAfterScroll(DataSet: TDataSet);
    procedure QrMoviVBeforeClose(DataSet: TDataSet);
    procedure Sinttico1Click(Sender: TObject);
    procedure Analtico1Click(Sender: TObject);
    procedure QrMoviMCalcFields(DataSet: TDataSet);
    procedure Sintticocomcustomdio1Click(Sender: TObject);
  private
    { Private declarations }
    FRelVdaTmp: String;
    procedure CarregaListaStatus;
    procedure CarregaGraCusPrc;
    procedure ReopenMoviM;
    function ObtemControle(Grade, Cor, Tamanho, Limit: Integer): Integer;
    function CalculaCustoMedio(Grade, Cor, Tamanho, Controle: Integer): Double;
  public
    { Public declarations }
    FCodigo, FControle: Integer;
  end;

  var
    FmRelVendas: TFmRelVendas;

implementation

{$R *.DFM}

uses Module, MyListas, dmkGeral, ModuleGeral, MoviV, UCreate, UnMyObjects;

procedure TFmRelVendas.Analtico1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxRelVendasDetal, 'Relat�rio de vendas');
end;

procedure TFmRelVendas.BtImprimeClick(Sender: TObject);
begin
  MyObjects.MostraPopUpDeBotao(PMImprime, BtImprime);
end;

procedure TFmRelVendas.BtPesquisaClick(Sender: TObject);
var
  GraCusPrcAtu, GraCusPrcInt, StatAtu, StatusInt, i, Vendedor: Integer;
  GraCusPrc, Status, SQLStr: String;
begin
  Screen.Cursor := crHourGlass;
  //
  StatAtu      := 0;
  GraCusPrcInt := 0;
  StatusInt    := 0;
  GraCusPrcAtu := 0;
  SQLStr       := '';
  Vendedor     := EdVendedor.ValueVariant;
  //
  QrMoviV.Close;
  QrMoviV.SQL.Clear;
  QrMoviV.SQL.Add('SELECT mov.Codigo, mov.Controle, mov.Total, mov.Descon, mov.Frete, mov.UserCad, ');
  QrMoviV.SQL.Add('mov.Descon, mov.Frete, mov.Pago, mov.StatAtual, CASE WHEN enb.Tipo=0');
  QrMoviV.SQL.Add('THEN enb.RazaoSocial ELSE enb.Nome END NOMECLI,');
  QrMoviV.SQL.Add('sta.DiaHora, CASE WHEN ent.Tipo=0');
  QrMoviV.SQL.Add('THEN ent.RazaoSocial ELSE ent.Nome END NOMEENT,');
  QrMoviV.SQL.Add('stv.Nome NOMESTAT, mov.DataPedi, mov.DataReal, ');
  QrMoviV.SQL.Add('DATE_FORMAT(CASE WHEN DataReal=0 ');
  QrMoviV.SQL.Add('THEN DataPedi ELSE DataReal END,  "%d/%m/%y") DataTXT, ');
  QrMoviV.SQL.Add('DATE_FORMAT(MAX(sta.DiaHora), "%d/%m/%Y") DiaHora_TXT');
  QrMoviV.SQL.Add('FROM moviv mov');
  QrMoviV.SQL.Add('LEFT JOIN senhas sen ON sen.Numero = mov.UserCad');
  QrMoviV.SQL.Add('LEFT JOIN entidades ent ON ent.Codigo = sen.Funcionario');
  QrMoviV.SQL.Add('LEFT JOIN entidades enb ON enb.Codigo = mov.Cliente');
  QrMoviV.SQL.Add('LEFT JOIN statusv stv ON stv.Codigo = mov.StatAtual');
  QrMoviV.SQL.Add('LEFT JOIN movivstat sta ON sta.Codigo = mov.Codigo ');
  QrMoviV.SQL.Add('WHERE mov.Codigo > 0');
  //
  //Seleciona o vendedor
  if Vendedor > 0 then
    QrMoviV.SQL.Add('AND mov.UserCad =' + IntToStr(Vendedor));
  //
  //Adiciona status selecionado
  SQLStr := '';
  if CkLBStatus.Items.Count > 0 then
  begin
    for i := 0 to CkLBStatus.Items.Count -1 do
    begin
      if CkLBStatus.Checked[i] = True then
      begin
        Status := CkLBStatus.Items[i];
        //
        QrLoc.Close;
        QrLoc.SQL.Clear;
        QrLoc.SQL.Add('SELECT Codigo');
        QrLoc.SQL.Add('FROM statusv');
        QrLoc.SQL.Add('WHERE Nome LIKE :P0');
        QrLoc.Params[0].AsString := Trim(Status);
        QrLoc.Open;
        if QrLoc.RecordCount > 0 then
          StatusInt := QrLoc.FieldByName('Codigo').Value;
        //
        if StatusInt <> 0 then
        begin
          if StatAtu <> 0 then
            SQLStr := SQLStr + ', ';
          SQLStr := SQLStr + IntToStr(StatusInt);
          StatAtu := StatusInt;
        end;
      end;
    end;
    if Length(SQLStr) > 0 then
      QrMoviV.SQL.Add('AND mov.StatAtual IN (' + SQLStr + ')');
  end;
  //Adiciona lista de pre�os selecionada
  SQLStr := '';
  if CkLBGraCusPrc.Items.Count > 0 then
  begin
    for i := 0 to CkLBGraCusPrc.Items.Count -1 do
    begin
      if CkLBGraCusPrc.Checked[i] = True then
      begin
        GraCusPrc := CkLBGraCusPrc.Items[i];
        //
        QrLoc.Close;
        QrLoc.SQL.Clear;
        QrLoc.SQL.Add('SELECT Codigo');
        QrLoc.SQL.Add('FROM gracusprc');
        QrLoc.SQL.Add('WHERE Nome LIKE :P0');
        QrLoc.Params[0].AsString := Trim(GraCusPrc);
        QrLoc.Open;
        if QrLoc.RecordCount > 0 then
          GraCusPrcInt := QrLoc.FieldByName('Codigo').Value;
        //
        if GraCusPrcInt <> 0 then
        begin
          if GraCusPrcAtu <> 0 then
            SQLStr := SQLStr + ', ';
          SQLStr       := SQLStr + IntToStr(GraCusPrcInt);
          GraCusPrcAtu := GraCusPrcInt;
        end;
      end;
    end;
    if Length(SQLStr) > 0 then
      QrMoviV.SQL.Add('AND mov.GraCusPrc IN (' + SQLStr + ')');
  end;
  QrMoviV.SQL.Add(dmkPF.SQL_Periodo('AND sta.DiaHora ',
    TPDataPedIni.Date, TPDataPedFim.Date, CkDataPedIni.Checked, CkDataPedFim.Checked));
  QrMoviV.SQL.Add('GROUP BY mov.Codigo');
  QrMoviV.Open;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmRelVendas.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmRelVendas.CalculaCustoMedio(Grade, Cor, Tamanho,
  Controle: Integer): Double;
begin
  QrCustoMedio.Close;
  QrCustoMedio.Params[0].AsInteger := Grade;
  QrCustoMedio.Params[1].AsInteger := Cor;
  QrCustoMedio.Params[2].AsInteger := Tamanho;
  QrCustoMedio.Params[3].AsInteger := Controle;
  QrCustoMedio.Open;
  if QrCustoMedio.RecordCount > 0 then
    Result := QrCustoMedioCUSTOMEDIO.Value
  else
    Result := 0;
end;

procedure TFmRelVendas.CarregaGraCusPrc;
var
  Nome: String;
begin
  QrGraCusPrc.Close;
  QrGraCusPrc.Open;
  //
  QrGraCusPrc.First;
  //
  while not QrGraCusPrc.Eof do
  begin
    Nome := QrGraCusPrcNome.Value;
    CkLBGraCusPrc.Items.Add(Nome);
    //
    QrGraCusPrc.Next;
  end;
end;

procedure TFmRelVendas.CarregaListaStatus;
var
  Nome: String;
begin
  QrStatusV.Close;
  QrStatusV.Open;
  //
  QrStatusV.First;
  //
  while not QrStatusV.Eof do
  begin
    Nome := QrStatusVNome.Value;
    CkLBStatus.Items.Add(Nome);
    //
    QrStatusV.Next;
  end;
end;

procedure TFmRelVendas.DBGrid1DblClick(Sender: TObject);
begin
  if DBCheck.CriaFm(TFmMoviV, FmMoviV, afmoNegarComAviso) then
  begin
    FmMoviV.LocCod(QrMoviVCodigo.Value, QrMoviVCodigo.Value);
    //
    FmMoviV.ShowModal;
    FmMoviV.Destroy;
  end;
end;

procedure TFmRelVendas.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmRelVendas.FormCreate(Sender: TObject);
begin
  TPDataPedIni.Date := Date;
  TPDataPedFim.Date := Date;
  //
  DModG.ReopenEndereco(Dmod.QrMasterDono.Value);
  QrVendedor.Open;
  CarregaListaStatus;
  CarregaGraCusPrc;
end;

procedure TFmRelVendas.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmRelVendas.frxRelVendasGetValue(const VarName: string;
  var Value: Variant);
begin
  if AnsiCompareText(VarName, 'VARF_PERIODO') = 0 then
    Value := dmkPF.PeriodoImp2(TPDataPedIni.Date, TPDataPedFim.Date,
      CkDataPedIni.Checked, CkDataPedFim.Checked, 'Per�odo da venda: ', '', '')
end;

function TFmRelVendas.ObtemControle(Grade, Cor, Tamanho, Limit: Integer): Integer;
begin
  QrLoc2.Close;
  QrLoc2.SQL.Clear;
  QrLoc2.SQL.Add('SELECT Controle');
  QrLoc2.SQL.Add('FROM movim');
  QrLoc2.SQL.Add('WHERE Grade=:P0');
  QrLoc2.SQL.Add('AND Cor=:P1');
  QrLoc2.SQL.Add('AND Tam=:P2');
  QrLoc2.SQL.Add('AND Val > 0');
  QrLoc2.SQL.Add('AND Qtd > 0');
  QrLoc2.SQL.Add('AND DataReal > 0');
  QrLoc2.SQL.Add('ORDER BY DataReal DESC');
  QrLoc2.SQL.Add('LIMIT ' + FormatFloat('0', Limit));
  QrLoc2.Params[0].AsInteger := Grade;
  QrLoc2.Params[1].AsInteger := Cor;
  QrLoc2.Params[2].AsInteger := Tamanho;
  QrLoc2.Open;
  if QrLoc2.RecordCount > 0 then
  begin
    QrLoc2.Last;
    //
    Result := QrLoc2Controle.Value;
  end else
    Result := 0;
end;

procedure TFmRelVendas.QrMoviMCalcFields(DataSet: TDataSet);
var
  Controle, Grade, Cor, Tam: Integer;
begin
  Grade    := QrMoviMGrade.Value;
  Cor      := QrMoviMCor.Value;
  Tam      := QrMoviMTam.Value;
  Controle := ObtemControle(Grade, Cor, Tam, 4);
  if Controle > 0 then
  begin
    QrMoviMCUSMEDUNI.Value := CalculaCustoMedio(Grade, Cor, Tam, Controle);
    QrMoviMCUSMEDTOT.Value := QrMoviMCUSMEDUNI.Value * -QrMoviMQtd.Value;
  end else
  begin
    QrMoviMCUSMEDUNI.Value := 0;
    QrMoviMCUSMEDTOT.Value := 0;
  end;
  //
  if QrMovimQtd.Value = 0 then QrMovimPRECO.Value := 0 else
  QrMovimPRECO.Value := QrMovimVen.Value / -QrMovimQtd.Value;
  QrMovimPOSIq.Value := -QrMovimQtd.Value;
  QrMovimPOSIv.Value := QrMovimVen.Value;
end;

procedure TFmRelVendas.QrMoviVAfterScroll(DataSet: TDataSet);
begin
  ReopenMoviM();
end;

procedure TFmRelVendas.QrMoviVBeforeClose(DataSet: TDataSet);
begin
  QrMoviM.Close;
end;

procedure TFmRelVendas.QrMoviVCalcFields(DataSet: TDataSet);
begin
  QrMoviVTOTALVDA.Value := QrMoviVTotal.Value - QrMoviVDescon.Value + QrMoviVFrete.Value;
  case QrMoviVUserCad.Value of
    -2: QrMoviVNOMECAD2.Value := 'BOSS [Administrador]';
    -1: QrMoviVNOMECAD2.Value := 'MASTER [Admin. DERMATEK]';
     0: QrMoviVNOMECAD2.Value := 'N�o definido';
    else QrMoviVNOMECAD2.Value := QrMoviVNOMEENT.Value;
  end;
end;

procedure TFmRelVendas.ReopenMoviM;
begin
  QrMoviM.Close;
  QrMoviM.Params[0].AsInteger := QrMoviVControle.Value;
  QrMoviM.Open;
end;

procedure TFmRelVendas.Sinttico1Click(Sender: TObject);
begin
  MyObjects.frxMostra(frxRelVendas, 'Relat�rio de vendas');
end;

procedure TFmRelVendas.Sintticocomcustomdio1Click(Sender: TObject);
var
  Controle, Grade, Cor, Tam: Integer;
  CustoM�dio, CusMedTot: Double;
begin
  if (QrMoviV.State <> dsInactive) and (QrMoviV.RecordCount > 0) then
  begin
    Screen.Cursor := crHourGlass;
    //
    FRelVdaTmp := UCriar.RecriaTempTable('relvdatmp', DmodG.QrUpdPID1, False);
    //
    PB1.Visible  := True;
    PB1.Max      := QrMoviV.RecordCount;
    PB1.Position := 0;
    //
    DModG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO ' + FRelVdaTmp + ' SET ');
    DModG.QrUpdPID1.SQL.Add('Codigo=:P0, DiaHora=:P1, Cliente=:P2, ');
    DModG.QrUpdPID1.SQL.Add('Vendedor=:P3, Status=:P4, Valor=:P5, ');
    DModG.QrUpdPID1.SQL.Add('Desconto=:P6, Frete=:P7, Total=:P8, ');
    DModG.QrUpdPID1.SQL.Add('CustoMedio=:P9');
    //
    QrMoviV.First;
    while not QrMoviV.Eof do
    begin
      CusMedTot := 0;
      ReopenMoviM;
      //
      QrMoviM.First;
      while not QrMoviM.Eof do
      begin
        Grade    := QrMoviMGrade.Value;
        Cor      := QrMoviMCor.Value;
        Tam      := QrMoviMTam.Value;
        Controle := ObtemControle(Grade, Cor, Tam, 4);
        //
        if Controle > 0 then
        begin
          CustoM�dio := CalculaCustoMedio(Grade, Cor, Tam, Controle);
          CusMedTot  := CusMedTot + (CustoM�dio * -QrMoviMQtd.Value);
        end else
          CusMedTot := 0;
        //
        QrMoviM.Next;
      end;
      //
      DModG.QrUpdPID1.Params[00].AsInteger := QrMoviVCodigo.Value;
      DModG.QrUpdPID1.Params[01].AsString  := QrMoviVDiaHora_TXT.Value;
      DModG.QrUpdPID1.Params[02].AsString  := QrMoviVNOMECLI.Value;
      DModG.QrUpdPID1.Params[03].AsString  := QrMoviVNOMECAD2.Value;
      DModG.QrUpdPID1.Params[04].AsString  := QrMoviVNOMESTAT.Value;
      DModG.QrUpdPID1.Params[05].AsFloat   := QrMoviVTotal.Value;
      DModG.QrUpdPID1.Params[06].AsFloat   := QrMoviVDescon.Value;
      DModG.QrUpdPID1.Params[07].AsFloat   := QrMoviVFrete.Value;
      DModG.QrUpdPID1.Params[08].AsFloat   := QrMoviVTOTALVDA.Value;
      DModG.QrUpdPID1.Params[09].AsFloat   := CusMedTot;
      DModG.QrUpdPID1.ExecSQL;
      //
      QrMoviV.Next;
      //
      Application.ProcessMessages;
      PB1.Position := PB1.Position + 1;
    end;
    //
    QrLocMovV.Close;
    QrLocMovV.Open;
    //
    PB1.Visible   := False;
    Screen.Cursor := crDefault;
    //
    MyObjects.frxMostra(frxRelVendasCus, 'Relat�rio de vendas');
  end else
    Geral.MensagemBox('N�o h� itens para serem impressos!', 'Aviso', MB_OK+MB_ICONWARNING);
end;

end.



