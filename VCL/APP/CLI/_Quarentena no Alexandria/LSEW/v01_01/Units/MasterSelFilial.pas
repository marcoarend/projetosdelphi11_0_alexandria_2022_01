unit MasterSelFilial;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, dmkLabel, DBCtrls,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  TFmMasterSelFilial = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    EdFilial: TdmkEditCB;
    CBFilial: TdmkDBLookupComboBox;
    dmkLabel1: TdmkLabel;
    QrFiliais: TmySQLQuery;
    DsFiliais: TDataSource;
    QrFiliaisFilial: TIntegerField;
    QrFiliaisCodigo: TIntegerField;
    QrFiliaisNOMEFILIAL: TWideStringField;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    SQLExtra: String;
    FSelecionou: Boolean;
  end;

  var
  FmMasterSelFilial: TFmMasterSelFilial;

implementation

uses Module, UnMyObjects;

{$R *.DFM}

procedure TFmMasterSelFilial.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMasterSelFilial.BtOKClick(Sender: TObject);
begin
  FSelecionou := True;
  Close;
end;

procedure TFmMasterSelFilial.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmMasterSelFilial.FormCreate(Sender: TObject);
begin
  // QrFiliais deve ser setado e aberto pelo form que chama este form
  // QrFiliais.Open;
  //
  FSelecionou := False;
end;

procedure TFmMasterSelFilial.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
