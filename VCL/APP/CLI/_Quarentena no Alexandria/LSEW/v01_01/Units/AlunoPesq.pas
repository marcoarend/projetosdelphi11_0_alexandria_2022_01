unit AlunoPesq;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkDBGrid, dmkEdit,
  DB, mySQLDbTables, MyDBCheck, dmkGeral, dmkPermissoes, UnDmkEnums;

type
  TFmAlunoPesq = class(TForm)
    PainelConfirma: TPanel;
    BtPesq: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Panel3: TPanel;
    EdNome: TdmkEdit;
    EdCNPJ: TdmkEdit;
    EdSerial: TdmkEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    dmkDBGrid1: TdmkDBGrid;
    QrPesq: TmySQLQuery;
    DsPesq: TDataSource;
    QrPesqCodigo: TIntegerField;
    QrPesqTipo: TSmallintField;
    QrPesqRespons1: TWideStringField;
    QrPesqNOME_ENT: TWideStringField;
    QrPesqCNPJ_CPF: TWideStringField;
    QrPesqIE_RG: TWideStringField;
    QrPesqNIRE_: TWideStringField;
    QrPesqRUA: TWideStringField;
    QrPesqNUMERO: TLargeintField;
    QrPesqCOMPL: TWideStringField;
    QrPesqBAIRRO: TWideStringField;
    QrPesqCIDADE: TWideStringField;
    QrPesqNOMELOGRAD: TWideStringField;
    QrPesqNOMEUF: TWideStringField;
    QrPesqPais: TWideStringField;
    QrPesqLograd: TLargeintField;
    QrPesqCEP: TLargeintField;
    QrPesqTE1: TWideStringField;
    QrPesqFAX: TWideStringField;
    QrPesqCEP_TXT: TWideStringField;
    QrPesqNUMERO_TXT: TWideStringField;
    QrPesqE_ALL: TWideStringField;
    QrPesqCNPJ_TXT: TWideStringField;
    QrPesqFAX_TXT: TWideStringField;
    QrPesqTE2_TXT: TWideStringField;
    QrPesqNATAL_TXT: TWideStringField;
    QrPesqTE2: TWideStringField;
    QrPesqTE3: TWideStringField;
    QrPesqCEL: TWideStringField;
    QrPesqEmail: TWideStringField;
    QrPesqTE3_TXT: TWideStringField;
    QrPesqCEL_TXT: TWideStringField;
    QrPesqTE1_TXT: TWideStringField;
    QrPesqENatal: TDateField;
    QrPesqPNatal: TDateField;
    BtEdita: TBitBtn;
    QrPesqCliente1: TWideStringField;
    QrPesqCliente2: TWideStringField;
    QrPesqEUF: TSmallintField;
    QrPesqPUF: TSmallintField;
    QrPesqSSP: TWideStringField;
    QrPesqDataRG: TDateField;
    QrPesqMD5Num: TIntegerField;
    QrPesqMD5Forca: TSmallintField;
    QrPesqMD5Senha: TWideStringField;
    dmkPermissoes1: TdmkPermissoes;
    Label4: TLabel;
    dmkEdTel: TdmkEdit;
    Label5: TLabel;
    dmkEdFax: TdmkEdit;
    dmkEdCel: TdmkEdit;
    Label6: TLabel;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure QrPesqCalcFields(DataSet: TDataSet);
    procedure BtPesqClick(Sender: TObject);
    procedure BtEditaClick(Sender: TObject);
    procedure QrPesqBeforeClose(DataSet: TDataSet);
    procedure QrPesqAfterOpen(DataSet: TDataSet);
    procedure dmkEdTelExit(Sender: TObject);
    procedure dmkEdFaxExit(Sender: TObject);
    procedure dmkEdCelExit(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmAlunoPesq: TFmAlunoPesq;

implementation

uses Module, uEncrypt, EntiRapido, Principal, UnitMD5, UnMyObjects;

{$R *.DFM}

procedure TFmAlunoPesq.BtEditaClick(Sender: TObject);
var
  TipoEnti, k: Integer;
  MD5: String;
begin
  if DBCheck.CriaFm(TFmEntiRapido, FmEntiRapido, afmoNegarComAviso) then
  begin
    // s� funciona aqui
    FmEntiRapido.FCodiEnti  := QrPesqCodigo.Value;
    FmEntiRapido.FAlunoPesq := True;
    TipoEnti := 0;
    if Uppercase(QrPesqCliente1.Value) = 'V' then TipoEnti := TipoEnti + 1;
    if Uppercase(QrPesqCliente2.Value) = 'V' then TipoEnti := TipoEnti + 2;
    FmEntiRapido.LaTipo.SQLType       := stUpd;
    FmEntiRapido.RGTipo.ItemIndex     := QrPesqTipo.Value + 1;
    FmEntiRapido.RGTipoEnti.ItemIndex := TipoEnti;
    FmEntiRapido.EdNome.Text          := QrPesqNOME_ENT.Value;
    FmEntiRapido.EdCNPJCPF.Text       := QrPesqCNPJ_TXT.Value;
    FmEntiRapido.EdEmeio.Text         := QrPesqEmail.Text;
    FmEntiRapido.EdIERG.Text          := QrPesqIE_RG.Text;
    FmEntiRapido.EdTe1.Text           := QrPesqTE1_TXT.Value;
    FmEntiRapido.EdTe2.Text           := QrPesqTE2_TXT.Value;
    FmEntiRapido.EdTe3.Text           := QrPesqTE3_TXT.Value;
    FmEntiRapido.EdCel.Text           := QrPesqCEL_TXT.Value;
    FmEntiRapido.EdFax.Text           := QrPesqFAX_TXT.Value;
    FmEntiRapido.CBLograd.KeyValue    := QrPesqLograd.Value;
    FmEntiRapido.EdRua.Text           := QrPesqRUA.Value;
    FmEntiRapido.EdNumero.Text        := IntToStr(QrPesqNumero.Value);
    FmEntiRapido.EdCompl.Text         := QrPesqCOMPL.Value;
    FmEntiRapido.EdBairro.Text        := QrPesqBAIRRO.Value;
    FmEntiRapido.EdCidade.Text        := QrPesqCidade.Value;
    FmEntiRapido.EdCEP.Text           := QrPesqCEP_TXT.Value;
    FmEntiRapido.EdPais.Text          := QrPesqPais.Value;
    FmEntiRapido.EdSSP.Text           := QrPesqSSP.Value;
    FmEntiRapido.TPDataRG.Date        := QrPesqDataRG.Value;
    case QrPesqTipo.Value of
      0: FmEntiRapido.CBUF.KeyValue   := QrPesqEUF.Value;
      1: FmEntiRapido.CBUF.KeyValue   := QrPesqPUF.Value;
    end;
    k := QrPesqMD5Num.Value;
    if (Trim(QrPesqMD5Senha.Value) <> '') then
    begin
      case QrPesqMD5Forca.Value of
        0: MD5 := UnEncrypt.Encrypt(FormatFloat('0000', k) +
          QrPesqMD5Senha.Value, FmPrincipal.Fmy_key_uEncrypt);
        1: MD5 := UnMD5.StrMD5(FormatFloat('00000000000', k) + QrPesqMD5Senha.Value);
      end;
    end else MD5 := '';
    FmEntiRapido.EdSerial.Text := MLAGeral.ParticionaTexto(MD5, 4);
    FmEntiRapido.LocalizaSenhaAluno();
    //
    FmEntiRapido.ShowModal;
    FmEntiRapido.Destroy;
  end;
end;

procedure TFmAlunoPesq.BtPesqClick(Sender: TObject);
var
  Nome, CNPJ, Cryp, Tel, Cel, Fax: String;
  Grupo, Numero: Integer;
begin
  Nome := EdNome.Text;
  CNPJ := Geral.SoNumero_TT(EdCNPJ.Text);
  Cryp := UnEncrypt.LimpaStringUEncrypt(EdSerial.Text);
  Tel := MLAGeral.SoNumeroESinal_TT(dmkEdTel.ValueVariant);
  if Length(Tel) > 0 then
  begin
    if (Length(Tel) - 1 = 8) or (Length(Tel) - 1 = 10) then
    begin
      case Length(Tel) - 1 of
         8: Tel := Copy(Tel, 2, 8);
        10: Tel := Tel;
      end;
    end else
    begin
      Tel := '';
      Application.MessageBox('O n�mero de telefone deve ter 8, 10 ou 11 d�gitos!',
      'Aviso', MB_OK+ MB_ICONWARNING);
      dmkEdTel.SetFocus;
      Exit;
    end;
  end;
  Cel := MLAGeral.SoNumeroESinal_TT(dmkEdCel.ValueVariant);
  if Length(Cel) > 0 then
  begin
    if (Length(Cel) - 1 = 8) or (Length(Cel) - 1 = 10) then
    begin
      case Length(Cel) - 1 of
         8: Cel := Copy(Cel, 2, 8);
        10: Cel := Cel;
      end;
    end else
    begin
      Cel := '';
      Application.MessageBox('O n�mero do celular deve ter 8, 10 ou 11 d�gitos!',
      'Aviso', MB_OK+ MB_ICONWARNING);
      dmkEdCel.SetFocus;
      Exit;
    end;
  end;
  Fax := MLAGeral.SoNumeroESinal_TT(dmkEdFax.ValueVariant);
  if Length(Fax) > 0 then
  begin
    if (Length(Fax) - 1 = 8) or (Length(Fax) - 1 = 10) then
    begin
      case Length(Fax) - 1 of
         8: Fax := Copy(Fax, 2, 8);
        10: Fax := Fax;
      end;
    end else
    begin
      Fax := '';
      Application.MessageBox('O n�mero do fax deve ter 8, 10 ou 11 d�gitos!',
      'Aviso', MB_OK+ MB_ICONWARNING);
      dmkEdFax.SetFocus;
      Exit;
    end;
  end;
  QrPesq.Close;
  QrPesq.SQL.Clear;
  QrPesq.SQL.Add('SELECT en.Codigo, en.Tipo, en.Respons1, en.ENatal, en.PNatal, ');
  QrPesq.SQL.Add('en.Cliente1, en.Cliente2, en.EUF, en.PUF, en.SSP, en.DataRG, ');
  QrPesq.SQL.Add('alu.MD5Num, cab.Senha MD5Senha, cab.Forca MD5Forca,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome END NOME_ENT,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF  END CNPJ_CPF,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG   END IE_RG,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.NIRE        ELSE ""      END NIRE_,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua END RUA,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero END NUMERO,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl END COMPL,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro END BAIRRO,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade END CIDADE,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome   END NOMELOGRAD,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome   END NOMEUF,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais   END Pais,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd END Lograd,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP    END CEP,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1    END TE1,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.ETe2        ELSE en.PTe2    END TE2,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.ETe3        ELSE en.PTe3    END TE3,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.ECel        ELSE en.PCel    END CEL,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax    END FAX,');
  QrPesq.SQL.Add('CASE WHEN en.Tipo=0 THEN en.EEmail      ELSE en.PEMail  END Email');
  QrPesq.SQL.Add('FROM entidades en');
  QrPesq.SQL.Add('LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF');
  QrPesq.SQL.Add('LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF');
  QrPesq.SQL.Add('LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd');
  QrPesq.SQL.Add('LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd');
  QrPesq.SQL.Add('LEFT JOIN md5alu      alu ON alu.Entidade=en.Codigo');
  QrPesq.SQL.Add('LEFT JOIN md5cab      cab ON cab.Codigo=alu.MD5Cab');
  QrPesq.SQL.Add('');
  QrPesq.SQL.Add('WHERE (en.Cliente1="V" OR en.Cliente2="V")');
  // Nome
  if Trim(Nome) <> '' then
    QrPesq.SQL.Add('AND (IF(en.Tipo=0,en.RazaoSocial,en.Nome) LIKE "%' +
    Nome + '%")');
  if Trim(CNPJ) <> '' then
    QrPesq.SQL.Add('AND (IF(en.Tipo=0,en.CNPJ,en.CPF) = ' + CNPJ + ')');
  if Trim(Tel) <> '' then
  begin
    QrPesq.SQL.Add('AND (IF(en.Tipo=0,en.ETe1,en.PTe1) LIKE "%' + Tel + '%")');
    QrPesq.SQL.Add('OR (IF(en.Tipo=0,en.ETe2,en.PTe2) LIKE "%' + Tel + '%")');
    QrPesq.SQL.Add('OR (IF(en.Tipo=0,en.ETe3,en.PTe3) LIKE "%' + Tel + '%")');
  end;
  if Trim(Cel) <> '' then
    QrPesq.SQL.Add('AND (IF(en.Tipo=0,en.ECel,en.PCel) LIKE "%' + Cel + '%")');
  if Trim(Fax) <> '' then
    QrPesq.SQL.Add('AND (IF(en.Tipo=0,en.EFax,en.PFax) LIKE "%' + Fax + '%")');
  if Trim(Cryp) <> '' then
  begin
    Dmod.VerificaSenhaEntidade(EdSerial.Text, Grupo, Numero,
      FmPrincipal.Fmy_key_uEncrypt);
    if Grupo = 0 then Grupo := -1;
    QrPesq.SQL.Add('AND alu.MD5Cab=' + FormatFloat('0', Grupo));
    QrPesq.SQL.Add('AND alu.MD5Num=' + FormatFloat('0', Numero));
  end;

  //MLAGeral.LeMeuSQLy(QrPesq, '', nil, True, True);

  QrPesq.Open;
end;

procedure TFmAlunoPesq.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmAlunoPesq.dmkEdCelExit(Sender: TObject);
begin
  dmkEdCel.ValueVariant := Geral.FormataTelefone_TT(Geral.SoNumero_TT(dmkEdCel.ValueVariant));
end;

procedure TFmAlunoPesq.dmkEdFaxExit(Sender: TObject);
begin
  dmkEdFax.ValueVariant := Geral.FormataTelefone_TT(Geral.SoNumero_TT(dmkEdFax.ValueVariant));
end;

procedure TFmAlunoPesq.dmkEdTelExit(Sender: TObject);
begin
  dmkEdTel.ValueVariant := Geral.FormataTelefone_TT(Geral.SoNumero_TT(dmkEdTel.ValueVariant));
end;

procedure TFmAlunoPesq.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmAlunoPesq.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmAlunoPesq.QrPesqAfterOpen(DataSet: TDataSet);
begin
  BtEdita.Enabled := QrPesq.RecordCount > 0;
end;

procedure TFmAlunoPesq.QrPesqBeforeClose(DataSet: TDataSet);
begin
  BtEdita.Enabled := False;
end;

procedure TFmAlunoPesq.QrPesqCalcFields(DataSet: TDataSet);
var
  Natal: TDateTime;
begin
  QrPesqTE1_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrPesqTe1.Value);
  QrPesqTE2_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrPesqTE2.Value);
  QrPesqTE3_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrPesqTe3.Value);
  QrPesqCEL_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrPesqCEL.Value);
  QrPesqFAX_TXT.Value :=
    MLAGeral.FormataTelefone_TT_Curto(QrPesqFax.Value);
  QrPesqCNPJ_TXT.Value :=
    Geral.FormataCNPJ_TT(QrPesqCNPJ_CPF.Value);
  //
  QrPesqNUMERO_TXT.Value :=
    MLAGeral.FormataNumeroDeRua(QrPesqRUA.Value, QrPesqNumero.Value, False);
  QrPesqE_ALL.Value := QrPesqNOMELOGRAD.Value;
  if Trim(QrPesqE_ALL.Value) <> '' then QrPesqE_ALL.Value :=
    QrPesqE_ALL.Value + ' ';
  QrPesqE_ALL.Value := QrPesqE_ALL.Value + QrPesqRua.Value;
  if Trim(QrPesqRua.Value) <> '' then QrPesqE_ALL.Value :=
    QrPesqE_ALL.Value + ', ' + QrPesqNUMERO_TXT.Value;
  if Trim(QrPesqCompl.Value) <>  '' then QrPesqE_ALL.Value :=
    QrPesqE_ALL.Value + ' ' + QrPesqCompl.Value;
  if Trim(QrPesqBairro.Value) <>  '' then QrPesqE_ALL.Value :=
    QrPesqE_ALL.Value + ' - ' + QrPesqBairro.Value;
  if QrPesqCEP.Value > 0 then QrPesqE_ALL.Value :=
    QrPesqE_ALL.Value + ' CEP ' + Geral.FormataCEP_NT(QrPesqCEP.Value);
  if Trim(QrPesqCidade.Value) <>  '' then QrPesqE_ALL.Value :=
    QrPesqE_ALL.Value + ' - ' + QrPesqCidade.Value;
  //
  QrPesqCEP_TXT.Value := Geral.FormataCEP_NT(QrPesqCEP.Value);
  //
  if QrPesqTipo.Value = 0 then Natal := QrPesqENatal.Value
  else Natal := QrPesqPNatal.Value;
  if Natal < 2 then QrPesqNATAL_TXT.Value := ''
  else QrPesqNATAL_TXT.Value := Geral.FDT(Natal, 2);
end;

end.



