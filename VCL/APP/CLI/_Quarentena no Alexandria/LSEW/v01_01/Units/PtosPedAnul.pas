unit PtosPedAnul;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, dmkDBGrid, Mask,
  DBCtrls, dmkEdit, DB, mySQLDbTables, dmkGeral; 

type
  TFmPtosPedAnul = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    dmkDBGrid1: TdmkDBGrid;
    BtTodos: TBitBtn;
    BtNenhum: TBitBtn;
    PnLeitura: TPanel;
    Panel4: TPanel;
    Label3: TLabel;
    EdLeituraCodigoDeBarras: TEdit;
    BitBtn1: TBitBtn;
    Panel9: TPanel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    Panel3: TPanel;
    QrLoc: TmySQLQuery;
    QrLocIDCtrl: TIntegerField;
    QrLocAtivo: TSmallintField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure dmkDBGrid1CellClick(Column: TColumn);
    procedure BtTodosClick(Sender: TObject);
    procedure BtNenhumClick(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure EdLeituraCodigoDeBarrasChange(Sender: TObject);
    procedure EdLeituraCodigoDeBarrasKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    FTamAll: Boolean;
    FGraGruX, FSequencia: Integer;
    procedure AtivaItens(Status: Integer);
    procedure SelecionaItem(ID, Ativo: Integer);
    function ObtemIDCtrl(GragruX: Integer): Integer;
  public
    { Public declarations }
  end;

  var
  FmPtosPedAnul: TFmPtosPedAnul;

implementation

{$R *.DFM}

uses ModuleProd, ModuleGeral, UnMyObjects;

const
  FTamMax = 13; // EAN13
  FTamPrd = 12; // tudo � codigo do produto menos o d�gito (n�o tem lote e etc.)
  FTamMin = 1; // Digito verificador

procedure TFmPtosPedAnul.AtivaItens(Status: Integer);
var
  IDCtrl: Integer;
begin
  IDCtrl := DmProd.QrPedItsAnulIDCtrl.Value;
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE peditsanul SET Ativo=:P0');
  DModG.QrUpdPID1.Params[00].AsInteger := Status;
  DModG.QrUpdPID1.ExecSQL;
  //
  DmProd.ReopenPedItsAnul(2);
  DmProd.QrPedItsAnul.Locate('IDCtrl', IDCtrl, []);
end;

procedure TFmPtosPedAnul.BtNenhumClick(Sender: TObject);
begin
  AtivaItens(0);
end;

procedure TFmPtosPedAnul.BtOKClick(Sender: TObject);
begin
  if Application.MessageBox('Confima o cancelamento dos itens selecionados?',
  'Pergunta', MB_YESNOCANCEL+MB_ICONQUESTION) = ID_YES then
  begin
    DmProd.CancelaItensPedido();
    Close;
  end;
end;

procedure TFmPtosPedAnul.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPtosPedAnul.BtTodosClick(Sender: TObject);
begin
  AtivaItens(1);
end;

procedure TFmPtosPedAnul.dmkDBGrid1CellClick(Column: TColumn);
var
  Status, IDCtrl: Integer;
begin
  if Column.FieldName = 'Ativo' then
  begin
    Status := DmProd.QrPedItsAnulAtivo.Value;
    if Status = 0 then Status := 1 else Status := 0;
    IDCtrl := DmProd.QrPedItsAnulIDCtrl.Value;
    SelecionaItem(IDCtrl, Status);
  end;
end;

procedure TFmPtosPedAnul.EdLeituraCodigoDeBarrasChange(Sender: TObject);
var
  Tam: Integer;
begin
  Tam := Length(EdLeituraCodigoDeBarras.Text);
  if Tam = FTamMax then
  begin
    FGraGruX   := Geral.IMV(Copy(EdLeituraCodigoDeBarras.Text, 07, 6));
    FSequencia := Geral.IMV(Copy(EdLeituraCodigoDeBarras.Text, 13, 8));
    //
    if FGraGruX > 0 then
    begin
      SelecionaItem(ObtemIDCtrl(FGraGruX), 1);
      EdLeituraCodigoDeBarras.SetFocus;
      EdLeituraCodigoDeBarras.Text := '';
    end;
  end;;
end;

procedure TFmPtosPedAnul.EdLeituraCodigoDeBarrasKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  Txt: String;
  Tam: Integer;
begin
  if Key = VK_RETURN then
  begin
    if  (Length(EdLeituraCodigoDeBarras.Text) <= FTamPrd)
    and (Length(EdLeituraCodigoDeBarras.Text) >  FTamMin) then
    begin
      Txt := EdLeituraCodigoDeBarras.Text;
      Tam := Length(Txt);
      Txt := Copy(Txt, 1, Tam-1);
      FGraGruX := Geral.IMV(Txt);
      if FGraGruX > 0 then
      begin
        SelecionaItem(ObtemIDCtrl(FGraGruX), 1);
        EdLeituraCodigoDeBarras.SetFocus;
        EdLeituraCodigoDeBarras.Text := '';
      end;
    end;
  end;
end;

procedure TFmPtosPedAnul.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPtosPedAnul.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

function TFmPtosPedAnul.ObtemIDCtrl(GragruX: Integer): Integer;
begin
  QrLoc.Close;
  QrLoc.Params[0].AsInteger := GragruX;
  QrLoc.Open;
  //
  Result := QrLocIDCtrl.Value;
end;

procedure TFmPtosPedAnul.SelecionaItem(ID, Ativo: Integer);
begin
  DModG.QrUpdPID1.SQL.Clear;
  DModG.QrUpdPID1.SQL.Add('UPDATE peditsanul SET Ativo=:P0');
  DModG.QrUpdPID1.SQL.Add('WHERE IDCtrl=:P1;');
  DModG.QrUpdPID1.Params[00].AsInteger := Ativo;
  DModG.QrUpdPID1.Params[01].AsInteger := ID;
  DModG.QrUpdPID1.ExecSQL;
  //
  DmProd.ReopenPedItsAnul(2);
  DmProd.QrPedItsAnul.Locate('IDCtrl', ID, []);
end;

end.
