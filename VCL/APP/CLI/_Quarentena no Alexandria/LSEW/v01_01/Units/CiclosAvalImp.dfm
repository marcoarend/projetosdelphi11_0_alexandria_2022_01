object FmCiclosAvalImp: TFmCiclosAvalImp
  Left = 473
  Top = 173
  Caption = 'CIC-CAIMP-007 :: CiclosAval'
  ClientHeight = 295
  ClientWidth = 692
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelConfirma: TPanel
    Left = 0
    Top = 247
    Width = 692
    Height = 48
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 19
      Top = 4
      Width = 90
      Height = 40
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 580
      Top = 1
      Width = 111
      Height = 46
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 5
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
    object BtImprime: TBitBtn
      Tag = 5
      Left = 111
      Top = 4
      Width = 90
      Height = 40
      Caption = '&Imprime'
      NumGlyphs = 2
      TabOrder = 2
      OnClick = BtImprimeClick
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 692
    Height = 48
    Align = alTop
    Caption = 'CiclosAval'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 690
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 2
      ExplicitTop = 2
      ExplicitWidth = 588
      ExplicitHeight = 44
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 48
    Width = 692
    Height = 199
    Align = alClient
    TabOrder = 0
    object dmkDBGrid1: TdmkDBGrid
      Left = 1
      Top = 63
      Width = 690
      Height = 135
      Align = alClient
      Color = clWindow
      DataSource = DsQuery
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 690
      Height = 62
      Align = alTop
      TabOrder = 1
      object Label32: TLabel
        Left = 20
        Top = 12
        Width = 52
        Height = 13
        Caption = 'M'#234's inicial:'
      end
      object LaAnoI: TLabel
        Left = 199
        Top = 11
        Width = 51
        Height = 13
        Caption = 'Ano inicial:'
      end
      object Label2: TLabel
        Left = 291
        Top = 11
        Width = 44
        Height = 13
        Caption = 'Ano final:'
      end
      object LaPeriodo: TLabel
        Left = 377
        Top = 29
        Width = 67
        Height = 20
        Caption = 'Per'#237'odo:'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -16
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object CBMesIni: TComboBox
        Left = 20
        Top = 29
        Width = 172
        Height = 21
        Style = csDropDownList
        Color = clWhite
        DropDownCount = 12
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 0
      end
      object CBAnoIni: TComboBox
        Left = 199
        Top = 29
        Width = 80
        Height = 21
        Style = csDropDownList
        Color = clWhite
        DropDownCount = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
      end
      object CBAnoFin: TComboBox
        Left = 291
        Top = 29
        Width = 80
        Height = 21
        Style = csDropDownList
        Color = clWhite
        DropDownCount = 3
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 7622183
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 2
      end
    end
  end
  object QrCiclos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(cic.AlunosAula)TOTALU,'
      ' DATE_FORMAT(DataAcert, "%m")MES, '
      'DATE_FORMAT(DataAcert, "%Y/%m")MESANO'
      'FROM ciclos cic'
      'LEFT JOIN entidades ent ON ent.Codigo = cic.Promotor'
      'WHERE cic.Promotor=:P0'
      'AND cic.DataAcert BETWEEN :P1 AND :P2'
      'GROUP BY MESANO'
      'ORDER BY MESANO')
    Left = 73
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrCiclosMES: TWideStringField
      FieldName = 'MES'
      Size = 2
    end
    object QrCiclosMESANO: TWideStringField
      FieldName = 'MESANO'
      Size = 7
    end
    object QrCiclosTOTALU: TFloatField
      FieldName = 'TOTALU'
    end
  end
  object DsCiclos: TDataSource
    DataSet = QrCiclos
    Left = 101
    Top = 9
  end
  object Query: TABSQuery
    CurrentVersion = '7.20 '
    DatabaseName = 'MEMORY'
    InMemory = True
    ReadOnly = False
    RequestLive = True
    SQL.Strings = (
      'SELECT *'
      'FROM aval'
      '')
    Left = 284
    Top = 8
    object QueryAno: TIntegerField
      FieldName = 'Ano'
    end
    object QueryNomeProm: TWideStringField
      FieldName = 'NomeProm'
      Size = 100
    end
    object QueryMes01: TIntegerField
      FieldName = 'Mes01'
    end
    object QueryMes02: TIntegerField
      FieldName = 'Mes02'
    end
    object QueryMes03: TIntegerField
      FieldName = 'Mes03'
    end
    object QueryMes04: TIntegerField
      FieldName = 'Mes04'
    end
    object QueryMes05: TIntegerField
      FieldName = 'Mes05'
    end
    object QueryMes06: TIntegerField
      FieldName = 'Mes06'
    end
    object QueryMes07: TIntegerField
      FieldName = 'Mes07'
    end
    object QueryMes08: TIntegerField
      FieldName = 'Mes08'
    end
    object QueryMes09: TIntegerField
      FieldName = 'Mes09'
    end
    object QueryMes10: TIntegerField
      FieldName = 'Mes10'
    end
    object QueryMes11: TIntegerField
      FieldName = 'Mes11'
    end
    object QueryMes12: TIntegerField
      FieldName = 'Mes12'
    end
    object QueryTotalAno: TIntegerField
      FieldName = 'TotalAno'
    end
    object QueryMediaM: TIntegerField
      FieldName = 'MediaM'
    end
    object QueryAvaliacaoM: TWideStringField
      FieldName = 'AvaliacaoM'
      Size = 50
    end
    object QueryMediaR: TIntegerField
      FieldName = 'MediaR'
    end
    object QueryAvaliacaoR: TWideStringField
      FieldName = 'AvaliacaoR'
      Size = 50
    end
  end
  object DsQuery: TDataSource
    DataSet = Query
    Left = 312
    Top = 8
  end
  object QrPromotores: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT DISTINCT cic.Promotor,'
      'IF(ent.Tipo=0,ent.RazaoSocial, ent.Nome) NOMEPROM'
      'FROM ciclos cic'
      'LEFT JOIN entidades ent ON ent.Codigo = cic.Promotor'
      'ORDER BY NOMEPROM')
    Left = 17
    Top = 9
    object QrPromotoresPromotor: TIntegerField
      FieldName = 'Promotor'
      Origin = 'ciclos.Promotor'
      Required = True
    end
    object QrPromotoresNOMEPROM: TWideStringField
      FieldName = 'NOMEPROM'
      Size = 100
    end
  end
  object DsPromotores: TDataSource
    DataSet = QrPromotores
    Left = 45
    Top = 9
  end
  object frxCiclosAvalImp: TfrxReport
    Version = '5.3.9'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39745.603151574100000000
    ReportOptions.LastChange = 39849.386249270840000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <MeuLogoExiste> = True then Picture2.LoadFromFile(<MeuLogoC' +
        'aminho>);  '
      'end.  ')
    OnGetValue = frxCiclosAvalImpGetValue
    Left = 136
    Top = 9
    Datasets = <
      item
        DataSet = Dmod.frxDsMaster
        DataSetName = 'frxDsMaster'
      end
      item
        DataSet = frxDsQuery
        DataSetName = 'frxDsQuery'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 88.441002000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object Picture2: TfrxPictureView
          Top = 0.755905999999999500
          Width = 170.078740160000000000
          Height = 68.031496060000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo1: TfrxMemoView
          Top = 74.078788000000000000
          Width = 30.236220470000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            'Ano')
          ParentFont = False
        end
        object Memo25: TfrxMemoView
          Left = 30.102381000000000000
          Top = 74.078788000000000000
          Width = 123.968564470000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          Memo.UTF8W = (
            'Promotor')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 154.070965000000000000
          Top = 74.078788000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES01]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Left = 180.527675000000000000
          Top = 74.078788000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES02]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Left = 206.984385000000000000
          Top = 74.078788000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES03]')
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Left = 233.441095000000000000
          Top = 74.078788000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES04]')
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Left = 259.897805000000000000
          Top = 74.078788000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES05]')
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Left = 286.354515000000000000
          Top = 74.078788000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES06]')
          ParentFont = False
        end
        object Memo15: TfrxMemoView
          Left = 313.189178000000000000
          Top = 74.078788000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES07]')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 339.645888000000000000
          Top = 74.078788000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES08]')
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Left = 366.102598000000000000
          Top = 74.078788000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES09]')
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Left = 392.559308000000000000
          Top = 74.078788000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES10]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Left = 419.016018000000000000
          Top = 74.078788000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES11]')
          ParentFont = False
        end
        object Memo18: TfrxMemoView
          Left = 445.472728000000000000
          Top = 74.078788000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            '[VARF_MES12]')
          ParentFont = False
        end
        object Memo19: TfrxMemoView
          Left = 471.929438000000000000
          Top = 74.078788000000000000
          Width = 34.015748030000000000
          Height = 14.362204720000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAL')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Left = 505.945208000000000000
          Top = 73.944929000000000000
          Width = 52.913385830000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#233'd. mens.')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 558.858627999999900000
          Top = 74.078788000000000000
          Width = 52.913385830000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          Memo.UTF8W = (
            'Aval. mens.')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Left = 611.394095000000000000
          Top = 73.834694000000000000
          Width = 52.913385830000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          HAlign = haCenter
          Memo.UTF8W = (
            'M'#233'dia real')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Left = 664.236643000000000000
          Top = 73.787446000000000000
          Width = 52.913385830000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Fill.BackColor = clSilver
          Memo.UTF8W = (
            'Aval. real')
          ParentFont = False
        end
        object frxDsQueryMes01: TfrxMemoView
          Left = 170.456803000000000000
          Top = 50.645702000000000000
          Width = 241.134014000000000000
          Height = 18.897650000000000000
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Memo.UTF8W = (
            '[VARF_PERIODO]')
          ParentFont = False
        end
        object Memo39: TfrxMemoView
          Left = 170.456915280000000000
          Top = 31.748052000000000000
          Width = 393.070817320000000000
          Height = 18.897650000000000000
          DataSet = FmCiclos2.frxDsCiclos
          DataSetName = 'frxDsCiclos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'M'#201'DIA DE PRESEN'#199'AS')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 170.456803000000000000
          Top = 5.291342000000000000
          Width = 393.070817320000000000
          Height = 22.000000000000000000
          DisplayFormat.DecimalSeparator = ','
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 14.362214000000000000
        Top = 165.165461000000000000
        Width = 718.110700000000000000
        DataSet = frxDsQuery
        DataSetName = 'frxDsQuery'
        RowCount = 0
        object Memo2: TfrxMemoView
          Width = 30.236220470000000000
          Height = 14.362214000000000000
          DataField = 'Ano'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."Ano"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 30.236240000000000000
          Width = 123.968564470000000000
          Height = 14.362214000000000000
          DataField = 'NomeProm'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsQuery."NomeProm"]')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 154.204824000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          DataField = 'Mes01'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."Mes01"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 180.661534000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          DataField = 'Mes02'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."Mes02"]')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 207.118244000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          DataField = 'Mes03'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."Mes03"]')
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Left = 233.574954000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          DataField = 'Mes04'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."Mes04"]')
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Left = 260.031664000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          DataField = 'Mes05'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."Mes05"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 286.488374000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          DataField = 'Mes06'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."Mes06"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 312.945084000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          DataField = 'Mes07'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."Mes07"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 339.401794000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          DataField = 'Mes08'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."Mes08"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Left = 365.858504000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          DataField = 'Mes09'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."Mes09"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 392.315214000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          DataField = 'Mes10'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."Mes10"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 419.149877000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          DataField = 'Mes11'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."Mes11"]')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 445.606587000000000000
          Width = 26.456692910000000000
          Height = 14.362214000000000000
          DataField = 'Mes12'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."Mes12"]')
          ParentFont = False
        end
        object Memo34: TfrxMemoView
          Left = 472.063297000000000000
          Width = 34.015748030000000000
          Height = 14.362204720000000000
          DataField = 'TotalAno'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."TotalAno"]')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Left = 558.992487000000000000
          Width = 52.535432830000000000
          Height = 14.362214000000000000
          DataField = 'AvaliacaoM'
          DataSet = frxDsQuery
          DataSetName = 'frxDsQuery'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsQuery."AvaliacaoM"]')
          ParentFont = False
        end
        object Memo35: TfrxMemoView
          Left = 506.079067000000000000
          Width = 52.913385830000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."MediaM"]')
          ParentFont = False
        end
        object Memo37: TfrxMemoView
          Left = 611.527954000000000000
          Width = 52.913385830000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsQuery."MediaR"]')
          ParentFont = False
        end
        object Memo38: TfrxMemoView
          Left = 664.440945130000000000
          Width = 53.291338830000000000
          Height = 14.362214000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            '[frxDsQuery."AvaliacaoR"]')
          ParentFont = False
        end
      end
    end
    object Page2: TfrxReportPage
      PaperWidth = 215.900000000000000000
      PaperHeight = 279.400000000000000000
      PaperSize = 1
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      PrintOnPreviousPage = True
      object MasterData3: TfrxMasterData
        FillType = ftBrush
        Height = 336.378170000000000000
        Top = 16.000000000000000000
        Width = 740.409927000000000000
        RowCount = 1
        object Chart2: TfrxChartView
          Top = 11.338590000000000000
          Width = 718.110236220000000000
          Height = 325.039580000000000000
          HighlightColor = clBlack
          Chart = {
            5450463006544368617274054368617274044C656674020003546F7002000557
            696474680390010648656967687403FA00144261636B57616C6C2E4272757368
            2E436F6C6F720707636C5768697465144261636B57616C6C2E42727573682E53
            74796C6507076273436C656172204261636B57616C6C2E42727573682E477261
            6469656E742E456E64436F6C6F7204C1C47500204261636B57616C6C2E427275
            73682E4772616469656E742E4D6964436F6C6F7204F9F99B00224261636B5761
            6C6C2E42727573682E4772616469656E742E5374617274436F6C6F7204C1C475
            001F4261636B57616C6C2E42727573682E4772616469656E742E56697369626C
            6509144261636B57616C6C2E50656E2E56697369626C6508144261636B57616C
            6C2E5472616E73706172656E7408124C6567656E642E4C6567656E645374796C
            6507086C73536572696573105469746C652E466F6E742E436F6C6F720707636C
            426C61636B115469746C652E466F6E742E48656967687402ED125469746C652E
            546578742E537472696E67730114140000004DC3A96469612064652070726573
            656EC3A761730019426F74746F6D417869732E4461746554696D65466F726D61
            7406074D4D2F5959595918426F74746F6D417869732E45786163744461746554
            696D650815426F74746F6D417869732E4C6162656C5374796C65070774616C54
            65787418426F74746F6D417869732E5469746C652E43617074696F6E06054D45
            5345530D4672616D652E56697369626C6508164C656674417869732E5469746C
            652E43617074696F6E140E0000004EC2BA20444520414C554E4F532006566965
            77334408165669657733444F7074696F6E732E526F746174696F6E02000A4265
            76656C4F75746572070662764E6F6E6505436F6C6F720709636C4D656E754261
            7211436F6C6F7250616C65747465496E646578020D000F54466173744C696E65
            536572696573054D65736573134D61726B732E4172726F772E56697369626C65
            09194D61726B732E43616C6C6F75742E42727573682E436F6C6F720707636C42
            6C61636B1B4D61726B732E43616C6C6F75742E4172726F772E56697369626C65
            090D4D61726B732E56697369626C65080B536572696573436F6C6F720706636C
            4E6F6E650C53686F77496E4C6567656E6408055469746C650609496E73637269
            746F730D4C696E6550656E2E436F6C6F720706636C4E6F6E650D4C696E655065
            6E2E576964746802020C5856616C7565732E4E616D650601580D5856616C7565
            732E4F72646572070B6C6F417363656E64696E670C5956616C7565732E4E616D
            650601590D5956616C7565732E4F7264657207066C6F4E6F6E6500000F544661
            73744C696E6553657269657309496E73637269746F73134D61726B732E417272
            6F772E56697369626C6509194D61726B732E43616C6C6F75742E42727573682E
            436F6C6F720707636C426C61636B1B4D61726B732E43616C6C6F75742E417272
            6F772E56697369626C6509114D61726B732E5472616E73706172656E74090D4D
            61726B732E56697369626C65090B536572696573436F6C6F7203D50005546974
            6C650609496E73637269746F730D4C696E6550656E2E436F6C6F7203D5000D4C
            696E6550656E2E5374796C650709707344617368446F740D4C696E6550656E2E
            576964746802020C5856616C7565732E4E616D650601580D5856616C7565732E
            4F72646572070B6C6F417363656E64696E670C5956616C7565732E4E616D6506
            01590D5956616C7565732E4F7264657207066C6F4E6F6E6500000F5446617374
            4C696E655365726965730950726573656E746573134D61726B732E4172726F77
            2E56697369626C6509194D61726B732E43616C6C6F75742E42727573682E436F
            6C6F720707636C426C61636B1B4D61726B732E43616C6C6F75742E4172726F77
            2E56697369626C6509114D61726B732E5472616E73706172656E74090D4D6172
            6B732E56697369626C65090B536572696573436F6C6F72040D2C4D0005546974
            6C65060950726573656E7465730D4C696E6550656E2E436F6C6F72040D2C4D00
            0D4C696E6550656E2E576964746802020C5856616C7565732E4E616D65060158
            0D5856616C7565732E4F72646572070B6C6F417363656E64696E670C5956616C
            7565732E4E616D650601590D5956616C7565732E4F7264657207066C6F4E6F6E
            65000000}
          ChartElevation = 345
          SeriesData = <
            item
              InheritedName = 'TfrxSeriesItem2'
              DataType = dtDBData
              DataSetName = 'frxDsGrafData'
              SortOrder = soAscending
              TopN = 40
              TopNCaption = 'teste'
              XType = xtText
              Source1 = 'frxDsGrafData."Texto"'
              Source2 = 'frxDsGrafData."Texto"'
              XSource = 'frxDsGrafData."Texto"'
              YSource = 'frxDsGrafData."Texto"'
            end
            item
              InheritedName = 'TfrxSeriesItem3'
              DataType = dtDBData
              DataSetName = 'frxDsGrafVal1'
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDsGrafVal1."Val01"'
              Source2 = 'frxDsGrafVal1."Val01"'
              XSource = 'frxDsGrafVal1."Val01"'
              YSource = 'frxDsGrafVal1."Val01"'
            end
            item
              InheritedName = 'TfrxSeriesItem4'
              DataType = dtDBData
              DataSetName = 'frxDsGrafVal2'
              SortOrder = soNone
              TopN = 0
              XType = xtText
              Source1 = 'frxDsGrafVal2."Val02"'
              Source2 = 'frxDsGrafVal2."Val02"'
              XSource = 'frxDsGrafVal2."Val02"'
              YSource = 'frxDsGrafVal2."Val02"'
            end>
        end
      end
    end
  end
  object frxDsQuery: TfrxDBDataset
    UserName = 'frxDsQuery'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Ano=Ano'
      'NomeProm=NomeProm'
      'Mes01=Mes01'
      'Mes02=Mes02'
      'Mes03=Mes03'
      'Mes04=Mes04'
      'Mes05=Mes05'
      'Mes06=Mes06'
      'Mes07=Mes07'
      'Mes08=Mes08'
      'Mes09=Mes09'
      'Mes10=Mes10'
      'Mes11=Mes11'
      'Mes12=Mes12'
      'TotalAno=TotalAno'
      'MediaM=MediaM'
      'AvaliacaoM=AvaliacaoM'
      'MediaR=MediaR'
      'AvaliacaoR=AvaliacaoR')
    DataSet = Query
    BCDToCurrency = False
    Left = 256
    Top = 9
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 392
    Top = 11
  end
end
