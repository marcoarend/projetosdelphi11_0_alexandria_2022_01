unit CiclosAvalImp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, ComCtrls, Db, mySQLDbTables,
  frxClass, frxDBSet, DBCtrls, Grids, DBGrids, Mask, ABSMain, dmkEdit, dmkGeral,
  dmkDBGrid, dmkPermissoes, UnDmkProcFunc;

type
  TFmCiclosAvalImp = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    DsCiclos: TDataSource;
    QrCiclos: TmySQLQuery;
    Query: TABSQuery;
    DsQuery: TDataSource;
    QrPromotores: TmySQLQuery;
    DsPromotores: TDataSource;
    dmkDBGrid1: TdmkDBGrid;
    Panel3: TPanel;
    Label32: TLabel;
    CBMesIni: TComboBox;
    LaAnoI: TLabel;
    CBAnoIni: TComboBox;
    CBAnoFin: TComboBox;
    Label2: TLabel;
    QrPromotoresPromotor: TIntegerField;
    QrCiclosMES: TWideStringField;
    QrCiclosMESANO: TWideStringField;
    QrCiclosTOTALU: TFloatField;
    QrPromotoresNOMEPROM: TWideStringField;
    LaPeriodo: TLabel;
    frxCiclosAvalImp: TfrxReport;
    frxDsQuery: TfrxDBDataset;
    BtImprime: TBitBtn;
    QueryNomeProm: TWideStringField;
    QueryMes01: TIntegerField;
    QueryMes02: TIntegerField;
    QueryMes03: TIntegerField;
    QueryMes04: TIntegerField;
    QueryMes05: TIntegerField;
    QueryMes06: TIntegerField;
    QueryMes07: TIntegerField;
    QueryMes08: TIntegerField;
    QueryMes09: TIntegerField;
    QueryMes10: TIntegerField;
    QueryMes11: TIntegerField;
    QueryMes12: TIntegerField;
    QueryTotalAno: TIntegerField;
    QueryMediaM: TIntegerField;
    QueryAvaliacaoM: TWideStringField;
    QueryMediaR: TIntegerField;
    QueryAvaliacaoR: TWideStringField;
    QueryAno: TIntegerField;
    dmkPermissoes1: TdmkPermissoes;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
    procedure BtImprimeClick(Sender: TObject);
    procedure frxCiclosAvalImpGetValue(const VarName: string;
      var Value: Variant);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

  var
  FmCiclosAvalImp: TFmCiclosAvalImp;

implementation

{$R *.DFM}

uses Module, UnMsgInt, UnInternalConsts, UnMyObjects;

procedure TFmCiclosAvalImp.BtImprimeClick(Sender: TObject);
begin
  MyObjects.frxMostra(frxCiclosAvalImp, 'M�dia de presen�as por per�odo');
end;

procedure TFmCiclosAvalImp.BtOKClick(Sender: TObject);
var
  Meses: array[1..23] of Integer;
  i, k, m, TotAlu, Cols : Integer;
  AnoI, MesI, AnoF, MesF, AnoA, MesA, Intervalo: Word;
  IniX, FimX, MesExt,
  Ano, NomeProm, Mes01, Mes02, Mes03, Mes04, Mes05, Mes06, Mes07, Mes08, Mes09,
  Mes10, Mes11, Mes12, TotalAno, MediaM, MediaR, AvaliacaoM, AvaliacaoR : String;
begin
  if CBMesIni.ItemIndex = -1 then
  begin
    Application.MessageBox('Voc� deve selecionar o m�s inicial!',
      'Aviso', MB_OK+MB_ICONWARNING);
    CBMesIni.SetFocus;
    Exit;
  end;
  if CBAnoIni.ItemIndex = -1 then
  begin
    Application.MessageBox('Voc� deve selecionar o ano inicial!',
      'Aviso', MB_OK+MB_ICONWARNING);
    CBAnoIni.SetFocus;
    Exit;
  end;
  if CBAnoFin.ItemIndex = -1 then
  begin
    Application.MessageBox('Voc� deve selecionar o ano final!',
      'Aviso', MB_OK+MB_ICONWARNING);
    CBAnoFin.SetFocus;
    Exit;
  end;
  if CBAnoIni.ItemIndex > CBAnoFin.ItemIndex then
  begin
    Application.MessageBox('O ano final deve ser menor que o ano inicial!',
      'Aviso', MB_OK+MB_ICONWARNING);
    CBAnoIni.SetFocus;
    Exit;
  end;
  //
  Screen.Cursor := crHourGlass;
  //
  Query.Close;
  Query.SQL.Clear;
  Query.SQL.Add('DROP TABLE aval;         ');
  Query.SQL.Add(' CREATE TABLE aval      (');
  Query.SQL.Add(' Ano integer,            ');
  Query.SQL.Add(' NomeProm varchar(100),  ');
  Query.SQL.Add(' Mes01 integer,          ');
  Query.SQL.Add(' Mes02 integer,          ');
  Query.SQL.Add(' Mes03 integer,          ');
  Query.SQL.Add(' Mes04 integer,          ');
  Query.SQL.Add(' Mes05 integer,          ');
  Query.SQL.Add(' Mes06 integer,          ');
  Query.SQL.Add(' Mes07 integer,          ');
  Query.SQL.Add(' Mes08 integer,          ');
  Query.SQL.Add(' Mes09 integer,          ');
  Query.SQL.Add(' Mes10 integer,          ');
  Query.SQL.Add(' Mes11 integer,          ');
  Query.SQL.Add(' Mes12 integer,          ');
  Query.SQL.Add(' TotalAno integer,       ');
  Query.SQL.Add(' MediaM integer,         ');
  Query.SQL.Add(' AvaliacaoM varchar(50), ');
  Query.SQL.Add(' MediaR integer,          ');
  Query.SQL.Add(' AvaliacaoR varchar(50)   ');
  Query.SQL.Add(');');
  //
  QrPromotores.Open;
  QrPromotores.First;
  if QrPromotores.RecordCount > 0 then
  begin
    while not QrPromotores.Eof do
    begin
      Intervalo := Geral.IMV(CBAnoFin.Items[CBAnoFin.ItemIndex]) -
        Geral.IMV(CBAnoIni.Items[CBAnoIni.ItemIndex]);
      AnoA := Geral.IMV(CBAnoIni.Items[CBAnoIni.ItemIndex]) - 1;
      for i := 0 to Intervalo do
      begin
        AnoA := AnoA + 1;
        MesI := CBMesIni.ItemIndex +  1;
        if MesI > 6 then
          AnoI := AnoA -1
        else
          AnoI := AnoA;
        if MesI > 1 then
          MesF := MesI - 1
        else
          MesF := 12;
        if ((MesI=1) or (MesI>6)) then
          AnoF := AnoA
        else
          AnoF := AnoA + 1;
        //
        IniX := Geral.FDT(EncodeDate(AnoI, MesI, 1), 1);
        if MesF = 12 then
          FimX := Geral.FDT(EncodeDate(AnoF + 1, 1, 1) -1, 1)
        else
          FimX := Geral.FDT(EncodeDate(AnoF, MesF + 1, 1) -1, 1);
        //
        QrCiclos.Close;
        QrCiclos.Params[0].AsInteger := QrPromotoresPromotor.Value;
        QrCiclos.Params[1].AsString  := IniX;
        QrCiclos.Params[2].AsString  := FimX;
        //MLAGeral.LeMeuSQLy(QrCiclos, '', nil, True, True);
        QrCiclos.Open;
        //
        //MesA := MesA + 1;
        if QrCiclos.RecordCount > 0 then
        begin
          for k := 1 to 23 do
            Meses[k] := 0;
          QrCiclos.First;
          while not QrCiclos.Eof do
          begin
            Cols := 0;
            case MesI of
               1:
                 Cols := 1;
               2:
                 Cols := 3;
               3:
                 Cols := 5;
               4:
                 Cols := 7;
               5:
                 Cols := 9;
               6:
                 Cols := 11;
               7:
                 Cols := 13;
               8:
                 Cols := 15;
               9:
                 Cols := 5;
              10:
                 Cols := 7;
              11:
                 Cols := 9;
              12:
                 Cols := 11;
            end;
            MesA := Geral.IMV(QrCiclosMES.Value) + MesI - Cols;
            Meses[MesA] := Meses[MesA] + Trunc(QrCiclosTOTALU.Value);
            //
            QrCiclos.Next;
          end;
          TotAlu := 0;
          for m := 1 to 23 do
            TotAlu := TotAlu + Meses[m];
          //
          Ano        := '"'+ IntToStr(AnoA)  +'"';
          NomeProm   := '"'+ QrPromotoresNOMEPROM.Value +'"';
          Mes01      := dmkPF.FFP(Meses[01] + Meses[13], 0);
          Mes02      := dmkPF.FFP(Meses[02] + Meses[14], 0);
          Mes03      := dmkPF.FFP(Meses[03] + Meses[15], 0);
          Mes04      := dmkPF.FFP(Meses[04] + Meses[16], 0);
          Mes05      := dmkPF.FFP(Meses[05] + Meses[17], 0);
          Mes06      := dmkPF.FFP(Meses[06] + Meses[18], 0);
          Mes07      := dmkPF.FFP(Meses[07] + Meses[19], 0);
          Mes08      := dmkPF.FFP(Meses[08] + Meses[20], 0);
          Mes09      := dmkPF.FFP(Meses[09] + Meses[21], 0);
          Mes10      := dmkPF.FFP(Meses[10] + Meses[22], 0);
          Mes11      := dmkPF.FFP(Meses[11] + Meses[23], 0);
          Mes12      := dmkPF.FFP(Meses[12], 0);
          TotalAno   := dmkPF.FFP(TotAlu, 0);
          MediaM     := '""';
          AvaliacaoM := '""';
          MediaR     := '""';
          AvaliacaoR := '""';
          //
          Query.SQL.Add('INSERT INTO aval');
          Query.SQL.Add('(Ano, NomeProm, Mes01, Mes02, Mes03, Mes04, Mes05, ');
          Query.SQL.Add('Mes06, Mes07, Mes08, Mes09, Mes10, Mes11, Mes12,  ');
          Query.SQL.Add('TotalAno, MediaM, AvaliacaoM, MediaR, AvaliacaoR) VALUES(');
          Query.SQL.Add(Ano +', '+ NomeProm +', '+ Mes01 +', '+ Mes02 +', ');
          Query.SQL.Add(Mes03 +', '+ Mes04 +', '+ Mes05 +', '+ Mes06 +', ');
          Query.SQL.Add(Mes07 +', '+ Mes08 +', '+ Mes09 +', '+ Mes10 +', ');
          Query.SQL.Add(Mes11 +', '+ Mes12 +', '+ TotalAno +', ');
          Query.SQL.Add(MediaM +', '+ AvaliacaoM +', '+ MediaR +', ');
          Query.SQL.Add(AvaliacaoR +');');
        end;
      end;
      QrPromotores.Next;
    end;
  end;
  Query.SQL.Add('SELECT * FROM aval');
  Query.Open;
  //
  if Query.RecordCount > 0 then
  begin
    k := 1;
    MesA := CBMesIni.ItemIndex; 
    for i := 1 to 12 do
    begin
      MesA := MesA + 1;
      if MesA > 12 then
        MesA := MesA - 12;
      case MesA of
        1:
          MesExt := 'Janeiro';
        2:
          MesExt := 'Fevereiro';
        3:
          MesExt := 'Mar�o';
        4:
          MesExt := 'Abril';
        5:
          MesExt := 'Maio';
        6:
          MesExt := 'Junho';
        7:
          MesExt := 'Julho';
        8:
          MesExt := 'Agosto';
        9:
          MesExt := 'Setembro';
        10:
          MesExt := 'Outubro';
        11:
          MesExt := 'Novembro';
        12:
          MesExt := 'Dezembro';
      end;
      k := k + 1;
      dmkDBGrid1.Columns[k].Title.Caption := MesExt;            
    end;
    dmkDBGrid1.Columns[1].Title.Caption      := 'Promotor';
    dmkDBGrid1.Columns[14].Title.Caption     := 'Total ano';
    dmkDBGrid1.Columns[15].Title.Caption     := 'Media mes';
    dmkDBGrid1.Columns[16].Title.Caption     := 'Avalia��o mes';
    dmkDBGrid1.Columns[17].Title.Caption     := 'Media real';
    dmkDBGrid1.Columns[18].Title.Caption     := 'Avalia��o real';
    //
    MesI := CBMesIni.ItemIndex + 1;
    if MesI > 6 then
      AnoI := Geral.IMV(CBAnoIni.Items[CBAnoIni.ItemIndex]) - 1
    else
      AnoI := Geral.IMV(CBAnoIni.Items[CBAnoIni.ItemIndex]);
    if MesI > 1 then
      MesF := MesI - 1
    else
      MesF := 12;
    if ((MesI=1) or (MesI>6)) then
      AnoF := Geral.IMV(CBAnoIni.Items[CBAnoFin.ItemIndex])
    else
      AnoF := Geral.IMV(CBAnoIni.Items[CBAnoFin.ItemIndex]) + 1;
    //
    IniX := Geral.FDT(EncodeDate(AnoI, MesI, 1), 1);
    if MesF = 12 then
      FimX := Geral.FDT(EncodeDate(AnoF + 1, 1, 1) -1, 1)
    else
      FimX := Geral.FDT(EncodeDate(AnoF, MesF + 1, 1) -1, 1);
    //
    LaPeriodo.Caption := 'Per�odo: '+ IniX +' � '+ FimX +'.';
    LaPeriodo.Visible := True; 
  end;
  //
  Screen.Cursor := crDefault;
end;

procedure TFmCiclosAvalImp.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCiclosAvalImp.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCiclosAvalImp.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCiclosAvalImp.frxCiclosAvalImpGetValue(const VarName: string;
  var Value: Variant);
begin
  if VarName = 'VARF_PERIODO' then
    Value := LaPeriodo.Caption
  else if VarName = 'VARF_MES01' then
    Value := Copy(dmkDBGrid1.Columns[2].Title.Caption, 0, 3)
  else if VarName = 'VARF_MES02' then
    Value := Copy(dmkDBGrid1.Columns[3].Title.Caption, 0, 3)
  else if VarName = 'VARF_MES03' then
    Value := Copy(dmkDBGrid1.Columns[4].Title.Caption, 0, 3)
  else if VarName = 'VARF_MES04' then
    Value := Copy(dmkDBGrid1.Columns[5].Title.Caption, 0, 3)
  else if VarName = 'VARF_MES05' then
    Value := Copy(dmkDBGrid1.Columns[6].Title.Caption, 0, 3)
  else if VarName = 'VARF_MES06' then
    Value := Copy(dmkDBGrid1.Columns[7].Title.Caption, 0, 3)
  else if VarName = 'VARF_MES07' then
    Value := Copy(dmkDBGrid1.Columns[8].Title.Caption, 0, 3)
  else if VarName = 'VARF_MES08' then
    Value := Copy(dmkDBGrid1.Columns[9].Title.Caption, 0, 3)
  else if VarName = 'VARF_MES09' then
    Value := Copy(dmkDBGrid1.Columns[10].Title.Caption, 0, 3)
  else if VarName = 'VARF_MES10' then
    Value := Copy(dmkDBGrid1.Columns[11].Title.Caption, 0, 3)
  else if VarName = 'VARF_MES11' then
    Value := Copy(dmkDBGrid1.Columns[12].Title.Caption, 0, 3)
  else if VarName = 'VARF_MES12' then
    Value := Copy(dmkDBGrid1.Columns[13].Title.Caption, 0, 3);
end;

procedure TFmCiclosAvalImp.FormCreate(Sender: TObject);
var
  i : Integer;
  Ano, Mes, Dia : Word;
begin
  with CBMesIni.Items do
  begin
    Add(FIN_JANEIRO);
    Add(FIN_FEVEREIRO);
    Add(FIN_MARCO);
    Add(FIN_ABRIL);
    Add(FIN_MAIO);
    Add(FIN_JUNHO);
    Add(FIN_JULHO);
    Add(FIN_AGOSTO);
    Add(FIN_SETEMBRO);
    Add(FIN_OUTUBRO);
    Add(FIN_NOVEMBRO);
    Add(FIN_DEZEMBRO);
  end;
  DecodeDate(Date, Ano, Mes, Dia);
  with CBAnoIni.Items do
  begin
    for i := Ano-50 to Ano+50 do Add(IntToStr(i));
  end;
  with CBAnoFin.Items do
  begin
    for i := Ano-50 to Ano+50 do Add(IntToStr(i));
  end;
  LaPeriodo.Visible := False;
end;

end.



