unit MoviLPagtos;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, DBCtrls, Db, (*DBTables,*) UnMsgInt, ComCtrls, Buttons, Mask,
  UCreate, UMySQLModule, UnInternalConsts, UnInternalConsts2, UnInternalConsts3,
  UnMLAGeral, Grids, DBGrids, mySQLDbTables, Variants, dmkGeral,
  dmkDBLookupComboBox, dmkEdit, dmkEditCB, dmkEditDateTimePicker, dmkLabel,
  dmkMemo, UnDmkProcFunc, UnDmkEnums;

type
  TFmMoviLPagtos = class(TForm)
    QrGeneros: TmySQLQuery;
    QrGenerosCodigo: TIntegerField;
    QrGenerosNome: TWideStringField;
    PainelDados: TPanel;
    Label1: TLabel;
    LaValor: TLabel;
    LaDocumento: TLabel;
    LaVencimento: TLabel;
    Label21: TLabel;
    SpeedButton2: TSpeedButton;
    Label2: TLabel;
    EdValor: TdmkEdit;
    EdDocumento: TdmkEdit;
    TPVencimento: TdmkEditDateTimePicker;
    Panel3: TPanel;
    Label12: TLabel;
    Label13: TLabel;
    EdTrocoVal: TdmkEdit;
    EdTrocoPara: TdmkEdit;
    EdGenero: TdmkEditCB;
    CBGenero: TdmkDBLookupComboBox;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    EdCondPagto: TdmkEditCB;
    CBCondPagto: TdmkDBLookupComboBox;
    CkRecibo: TCheckBox;
    PainelTitulo: TPanel;
    LaTipo: TdmkLabel;
    Image1: TImage;
    GBCheque: TGroupBox;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    EdNome: TdmkEdit;
    EdBanco: TdmkEdit;
    EdAgencia: TdmkEdit;
    EdConta: TdmkEdit;
    EdCNPJCPF: TdmkEdit;
    QrCarteiras: TmySQLQuery;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    DsCarteiras: TDataSource;
    QrContas: TmySQLQuery;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    DsContas: TDataSource;
    QrPediPrzCab: TmySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    DsPediPrzCab: TDataSource;
    QrPesq: TmySQLQuery;
    PainelControle: TPanel;
    BtConfirma: TBitBtn;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    QrPediPrzIts: TmySQLQuery;
    QrTerceiro: TmySQLQuery;
    QrTerceiroCodigo: TIntegerField;
    QrTerceiroRazaoSocial: TWideStringField;
    QrTerceiroFantasia: TWideStringField;
    QrTerceiroRespons1: TWideStringField;
    QrTerceiroRespons2: TWideStringField;
    QrTerceiroPai: TWideStringField;
    QrTerceiroMae: TWideStringField;
    QrTerceiroCNPJ: TWideStringField;
    QrTerceiroIE: TWideStringField;
    QrTerceiroNome: TWideStringField;
    QrTerceiroApelido: TWideStringField;
    QrTerceiroCPF: TWideStringField;
    QrTerceiroRG: TWideStringField;
    QrTerceiroERua: TWideStringField;
    QrTerceiroECompl: TWideStringField;
    QrTerceiroEBairro: TWideStringField;
    QrTerceiroECIDADE: TWideStringField;
    QrTerceiroEUF: TSmallintField;
    QrTerceiroECEP: TIntegerField;
    QrTerceiroEPais: TWideStringField;
    QrTerceiroETe1: TWideStringField;
    QrTerceiroETe2: TWideStringField;
    QrTerceiroETe3: TWideStringField;
    QrTerceiroECel: TWideStringField;
    QrTerceiroEFax: TWideStringField;
    QrTerceiroEEMail: TWideStringField;
    QrTerceiroEContato: TWideStringField;
    QrTerceiroENatal: TDateField;
    QrTerceiroPRua: TWideStringField;
    QrTerceiroPCompl: TWideStringField;
    QrTerceiroPBairro: TWideStringField;
    QrTerceiroPCIDADE: TWideStringField;
    QrTerceiroPUF: TSmallintField;
    QrTerceiroPCEP: TIntegerField;
    QrTerceiroPPais: TWideStringField;
    QrTerceiroPTe1: TWideStringField;
    QrTerceiroPTe2: TWideStringField;
    QrTerceiroPTe3: TWideStringField;
    QrTerceiroPCel: TWideStringField;
    QrTerceiroPFax: TWideStringField;
    QrTerceiroPEMail: TWideStringField;
    QrTerceiroPContato: TWideStringField;
    QrTerceiroPNatal: TDateField;
    QrTerceiroSexo: TWideStringField;
    QrTerceiroResponsavel: TWideStringField;
    QrTerceiroProfissao: TWideStringField;
    QrTerceiroCargo: TWideStringField;
    QrTerceiroRecibo: TSmallintField;
    QrTerceiroDiaRecibo: TSmallintField;
    QrTerceiroAjudaEmpV: TFloatField;
    QrTerceiroAjudaEmpP: TFloatField;
    QrTerceiroCliente1: TWideStringField;
    QrTerceiroCliente2: TWideStringField;
    QrTerceiroFornece1: TWideStringField;
    QrTerceiroFornece2: TWideStringField;
    QrTerceiroFornece3: TWideStringField;
    QrTerceiroFornece4: TWideStringField;
    QrTerceiroTerceiro: TWideStringField;
    QrTerceiroCadastro: TDateField;
    QrTerceiroInformacoes: TWideStringField;
    QrTerceiroLogo: TBlobField;
    QrTerceiroVeiculo: TIntegerField;
    QrTerceiroMensal: TWideStringField;
    QrTerceiroObservacoes: TWideMemoField;
    QrTerceiroTipo: TSmallintField;
    QrTerceiroLk: TIntegerField;
    QrTerceiroGrupo: TIntegerField;
    QrTerceiroDataCad: TDateField;
    QrTerceiroDataAlt: TDateField;
    QrTerceiroUserCad: TSmallintField;
    QrTerceiroUserAlt: TSmallintField;
    QrTerceiroCRua: TWideStringField;
    QrTerceiroCCompl: TWideStringField;
    QrTerceiroCBairro: TWideStringField;
    QrTerceiroCCIDADE: TWideStringField;
    QrTerceiroCUF: TSmallintField;
    QrTerceiroCCEP: TIntegerField;
    QrTerceiroCPais: TWideStringField;
    QrTerceiroCTel: TWideStringField;
    QrTerceiroCFax: TWideStringField;
    QrTerceiroCCel: TWideStringField;
    QrTerceiroCContato: TWideStringField;
    QrTerceiroLRua: TWideStringField;
    QrTerceiroLCompl: TWideStringField;
    QrTerceiroLBairro: TWideStringField;
    QrTerceiroLCIDADE: TWideStringField;
    QrTerceiroLUF: TSmallintField;
    QrTerceiroLCEP: TIntegerField;
    QrTerceiroLPais: TWideStringField;
    QrTerceiroLTel: TWideStringField;
    QrTerceiroLFax: TWideStringField;
    QrTerceiroLCel: TWideStringField;
    QrTerceiroLContato: TWideStringField;
    QrTerceiroComissao: TFloatField;
    QrTerceiroSituacao: TSmallintField;
    QrTerceiroNivel: TWideStringField;
    QrTerceiroNOMEUFP: TWideStringField;
    QrTerceiroNOMEUFE: TWideStringField;
    QrTerceiroENumero: TIntegerField;
    QrTerceiroPNumero: TIntegerField;
    QrPediPrzItsControle: TIntegerField;
    QrPediPrzItsDias: TIntegerField;
    QrPediPrzItsPercent: TFloatField;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure EdDocumentoExit(Sender: TObject);
    procedure EdValorChange(Sender: TObject);
    procedure EdTrocoParaChange(Sender: TObject);
    procedure EdCNPJCPFExit(Sender: TObject);
    procedure EdNomeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure BtConfirmaClick(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
  private
    { Private declarations }
    FNome, FCNPJCPF, FConta: String;
    FAgencia, FBanco: Integer;
    //
    procedure ReopenCarteiras;
    procedure ReopenContas;
    procedure CalculaTroco;
    procedure InsereParcela(Valor: Double; Descricao: String;
              Vencimento: TDateTime; TabLctA: String);
  public
    { Public declarations }
    FTotValor: Double;
    FControle, FDevedor, FParcela: Integer;
  end;

var
  FmMoviLPagtos: TFmMoviLPagtos;

implementation

uses Module, UnGOTOy, UnFinanceiro, ModuleGeral, ResIntStrings, Recibos, MoviL,
UnMyObjects;

{$R *.DFM}

procedure TFmMoviLPagtos.BtConfirmaClick(Sender: TObject);
var
  Codigo, Carteira, Genero, CondPgto, PosIni: Integer;
  Val, Valor, V1, T1: Double;
  Variavel, Descricao: String;
begin
  Codigo   := FmMoviL.QrMoviLCodigo.Value;
  Carteira := EdCarteira.ValueVariant;
  Genero   := EdGenero.ValueVariant;
  CondPgto := EdCondPagto.ValueVariant;
  Valor    := EdValor.ValueVariant;
  Val      := Valor;
  T1       := Valor; 
  //
  if Carteira = 0 then
  begin
    Application.MessageBox('Carteira n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBCarteira.SetFocus;
    Exit;
  end;
  if Genero = 0 then
  begin
    Application.MessageBox('Conta n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBGenero.SetFocus;
    Exit;
  end;
  if CondPgto = 0 then
  begin
    Application.MessageBox('Condi��o de pagamento n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBCondPagto.SetFocus;
    Exit;
  end;
  if Valor = 0 then
  begin
    Application.MessageBox('Valor n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdValor.SetFocus;
    Exit;
  end;
  if FTotValor < Valor then
  begin
    Application.MessageBox('Valor acima do m�ximo permitido!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdValor.ValueVariant := FormatFloat('#,###,##0.00', FTotValor);
    EdValor.SetFocus;
    Exit;
  end;
  //
  if GBCheque.Visible then
  begin
    FNome    := EdNome.ValueVariant;
    FCNPJCPF := MLAGeral.FormataCNPJ_TFT(EdCNPJCPF.ValueVariant);
    FAgencia := EdAgencia.ValueVariant;
    FConta   := EdConta.ValueVariant;
    FBanco   := Geral.IMV(EdBanco.ValueVariant);
  end else begin
    FNome    := '';
    FCNPJCPF := '';
    FAgencia := 0;
    FConta   := '';
    FBanco   := 0;
  end;
  //
  QrPediPrzIts.Close;
  QrPediPrzIts.Params[0].AsInteger := CondPgto;
  QrPediPrzIts.Open;
  //
  QrPediPrzIts.First;
  while not QrPediPrzIts.Eof do
  begin
    if QrPediPrzIts.RecordCount = QrPediPrzIts.RecNo then
      V1 := Val
    else begin
      V1 := (Round(T1 * (QrPediPrzItsPercent.Value / 100 * 100))) / 100;
      //
      Val := Val - V1;
    end;
    //
    Descricao := Dmod.QrControleTxtVdaPro.Value;
    PosIni     := Pos('[', Descricao);
    if PosIni > 0 then
      Variavel  := Copy(Descricao, PosIni, 8);
    if Variavel = '[CODIGO]' then
      Descricao := Copy(Descricao, 1, PosIni - 1) + IntToStr(Codigo);
    Descricao := Descricao + ' - ' + dmkPF.ParcelaFatura(QrPediPrzIts.RecNo, 0)
                 + '/' + IntToStr(QrPediPrzIts.RecordCount);
    //
    InsereParcela(V1, Descricao, TPVencimento.Date, FmMoviL.FTabLctALS);
    //
    QrPediPrzIts.Next;
  end;
  //
  if FTotValor = Valor then
  begin
    FmMoviL.FPagto := True;
    Close;
  end else
  begin
    FTotValor := FTotValor - Valor;
    EdValor.ValueVariant := FTotValor;
    EdValor.SetFocus;
  end;
end;

procedure TFmMoviLPagtos.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMoviLPagtos.CalculaTroco;
var
  APagar, ATrocar, Troco: Double;
begin
  APagar  := Geral.DMV(EdValor.Text);
  ATrocar := Geral.DMV(EdTrocoPara.Text);
  //
  Troco   := ATrocar-APagar;
  EdTrocoVal.Text := Geral.TFT(FloatToStr(Troco), 2, siNegativo);
end;

procedure TFmMoviLPagtos.EdCNPJCPFExit(Sender: TObject);
var
  Num : String;
  CPF : String;
begin
  CPF := Geral.SoNumero_TT(EdCNPJCPF.Text);
  if CPF <> CO_VAZIO then
    begin
    Num := MLAGeral.CalculaCNPJCPF(CPF);
    if MLAGeral.FormataCNPJ_TFT(CPF) <> Num then
    begin
      Geral.MB_Erro(SMLA_NUMEROINVALIDO2);
      EdCNPJCPF.SetFocus;
    end else EdCNPJCPF.Text := Geral.FormataCNPJ_TT(CPF);
  end else EdCNPJCPF.Text := CO_VAZIO;
end;

procedure TFmMoviLPagtos.EdDocumentoExit(Sender: TObject);
var
  Doc: Boolean;
begin
  Doc := EdDocumento.ValueVariant > 0;
  if Doc then
  begin
    GBCheque.Visible := True;
    EdNome.SetFocus;
  end else
  begin
    GBCheque.Visible := False;
    CkRecibo.SetFocus;
  end; 
end;

procedure TFmMoviLPagtos.EdNomeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key=VK_F4 then
  begin
    QrPesq.Close;
    QrPesq.SQL.Clear;
    QrPesq.SQL.Add('SELECT CASE WHEN Tipo=0 THEN RazaoSocial ELSE Nome END NOMEENT,');
    QrPesq.SQL.Add('CASE WHEN Tipo=0 THEN CNPJ ELSE CPF END DOCENT');
    QrPesq.SQL.Add('FROM entidades');
    QrPesq.SQL.Add('WHERE Codigo=:P0');
    QrPesq.Params[0].AsInteger := FDevedor;
    QrPesq.Open;
    if QrPesq.RecordCount > 0 then
    begin
      EdNome.ValueVariant    := QrPesq.FieldByName('NOMEENT').Value;
      EdCNPJCPF.ValueVariant := QrPesq.FieldByName('DOCENT').Value;
    end;
  end;
end;

procedure TFmMoviLPagtos.EdTrocoParaChange(Sender: TObject);
begin
  CalculaTroco;
end;

procedure TFmMoviLPagtos.EdValorChange(Sender: TObject);
begin
  CalculaTroco;
end;

procedure TFmMoviLPagtos.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  //
  EdValor.ValueVariant := FTotValor;
end;

procedure TFmMoviLPagtos.FormCreate(Sender: TObject);
begin
  QrPediPrzCab.Open;
  ReopenCarteiras;
  ReopenContas;
  //
  TPVencimento.Date        := Date;
  EdCarteira.ValueVariant  := Dmod.QrControleLinCart.Value;
  CBCarteira.KeyValue      := Dmod.QrControleLinCart.Value;
  EdGenero.ValueVariant    := Dmod.QrControleLinConta.Value;
  CBGenero.KeyValue        := Dmod.QrControleLinConta.Value;
  EdCondPagto.ValueVariant := Dmod.QrControleLinCPgto.Value;
  CBCondPagto.KeyValue     := Dmod.QrControleLinCPgto.Value;
  //
  FmMoviL.FPagto := False;
end;

procedure TFmMoviLPagtos.InsereParcela(Valor: Double; Descricao: String;
  Vencimento: TDateTime; TabLctA: String);
var
  Agora: TDateTime;
  Documento: Double;
  Genero, Sit: Integer;
  Compensado, Vencto: String;
begin
  Agora     := DModG.ObtemAgora();
  Genero    := Geral.IMV(EdGenero.Text);
  Documento := Geral.DMV(EdDocumento.Text);
  Vencto    := Geral.FDT(Vencimento + QrPediPrzItsDias.Value, 1);
  //
  if QrCarteirasTipo.Value = 2 then
  begin
    Compensado := '';
    Sit := 0;
  end else begin
    Compensado := Vencto;
    Sit := 3;
  end;
  //
  UFinanceiro.LancamentoDefaultVARS;
  FLAN_Documento     := Documento;
  FLAN_Data          := Geral.FDT(Agora, 1);
  FLAN_Vencimento    := Vencto;
  FLAN_DataCad       := Geral.FDT(Agora, 1);
  FLAN_DataDoc       := Geral.FDT(Date, 1);
  FLAN_UserCad       := VAR_USUARIO;
  FLAN_Descricao     := Descricao;
  FLAN_Tipo          := QrCarteirasTipo.Value;
  FLAN_Carteira      := EdCarteira.ValueVariant;
  FLAN_Credito       := Valor;
  FLAN_Genero        := Genero;
  FLAN_Cliente       := FmMoviL.QrMoviLCliente.Value;
  FLAN_Vendedor      := VAR_USUARIO;
  FLAN_CliInt        := QrCarteirasForneceI.Value; // Carteiras.ForneceI = Cliente interno
  FLAN_FatID         := 810;
  FLAN_FatID_Sub     := 0;
  FLAN_FatNum        := FmMoviL.QrMoviLControle.Value;
  FLAN_FatParcela    := QrPediPrzIts.RecNo;
  FLAN_Emitente      := FNome;
  FLAN_CNPJCPF       := FCNPJCPF;
  FLAN_Banco         := FBanco;
  FLAN_Agencia       := FAgencia;
  FLAN_ContaCorrente := FConta;
  FLAN_Sit           := Sit;
  FLAN_Compensado    := Compensado;
  //
  FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
    'Controle', TabLctA, LAN_CTOS, 'Controle');
  //
  if UFinanceiro.InsereLancamento(FmMoviL.FTabLctALS) then
  begin
    if FTotValor = Valor then
    begin
      if CkRecibo.Checked then
      begin
        GOTOy.EmiteRecibo(FmMoviL.QrMoviLCliente.Value, QrCarteirasForneceI.Value,
        FmMoviL.QrMoviLTOTALVDA.Value, 0, 0, IntToStr(FmMoviL.QrMoviLCodigo.Value),
        Descricao, '', '', Agora, 0);
      end;
    end;
  end;
end;

procedure TFmMoviLPagtos.ReopenCarteiras;
begin
  QrCarteiras.Close;
  QrCarteiras.SQL.Clear;
  QrCarteiras.SQL.Add('SELECT car.Codigo, car.Nome, ');
  QrCarteiras.SQL.Add('car.ForneceI, car.Tipo');
  QrCarteiras.SQL.Add('FROM carteiras car');
  QrCarteiras.SQL.Add('LEFT JOIN carteirasu cau ON cau.Codigo = car.Codigo');
  if VAR_USUARIO > 0 then
    QrCarteiras.SQL.Add('WHERE cau.Usuario =' + IntToStr(VAR_USUARIO));
  QrCarteiras.SQL.Add('GROUP BY car.Codigo');
  QrCarteiras.SQL.Add('ORDER BY car.Nome');
  QrCarteiras.Open;
end;

procedure TFmMoviLPagtos.ReopenContas;
begin
  QrContas.Close;
  QrContas.SQL.Clear;
  QrContas.SQL.Add('SELECT con.Codigo, con.Nome');
  QrContas.SQL.Add('FROM contas con');
  QrContas.SQL.Add('LEFT JOIN contasu cou ON cou.Codigo = con.Codigo');
  if VAR_USUARIO > 0 then
    QrContas.SQL.Add('WHERE cou.Usuario =' + IntToStr(VAR_USUARIO));
  QrContas.SQL.Add('GROUP BY con.Codigo');
  QrContas.SQL.Add('ORDER BY con.Nome');
  QrContas.Open;
end;

procedure TFmMoviLPagtos.SpeedButton2Click(Sender: TObject);
var
  Dias : Integer;
begin
  Dias := Geral.IMV(
    InputBox(Caption, 'Digite o n�mero de dias.', '' ));
  TPVencimento.Date := TPVencimento.Date + Dias;
end;

end.

