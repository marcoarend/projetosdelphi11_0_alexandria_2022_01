unit PromoWeb;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, Grids, DBGrids, DB, mySQLDbTables,
  UMySQLModule, dmkGeral, UnInternalConsts, UCreate, UnDmkEnums;

type
  TFmPromoWeb = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    QrPromoWEB: TmySQLQuery;
    DsPromoWEB: TDataSource;
    QrPromoWEBNome: TWideStringField;
    QrPromoWEBCpf: TWideStringField;
    QrPromoWEBId: TWordField;
    DBGrid1: TDBGrid;
    QrTmpProm: TmySQLQuery;
    DsTmpProm: TDataSource;
    QrTmpPromCodigo: TIntegerField;
    QrTmpPromNome: TWideStringField;
    QrTmpPromCPF: TWideStringField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    PromWeb: String;
    procedure MontaGrade;
    function CadastraEntidade(CPF, Nome: String): Integer;
    function VerificaEntidade(CPF: String): Integer;
    function CadastraPromotor(CodUsu, Entidade: Integer): Integer;
  public
    { Public declarations }
  end;

  var
  FmPromoWeb: TFmPromoWeb;

implementation

uses Module, ModuleGeral, UnMyObjects;

{$R *.DFM}

procedure TFmPromoWeb.BtOKClick(Sender: TObject);
var
  CodUsu, Aviso, Entidade, i: Integer;
  Nome, CPF: String;
begin
  Aviso := 0;
  if DBGrid1.SelectedRows.Count > 1 then
  begin
    with DBGrid1.DataSource.DataSet do
    begin
      for i := 0 to DBGrid1.SelectedRows.Count - 1 do
      begin
        GotoBookmark(pointer(DBGrid1.SelectedRows.Items[i]));
        //
        CPF := QrTmpPromCPF.Value;
        CPF := Geral.SoNumero_TT(CPF);
        Nome := QrTmpPromNome.Value;
        CodUsu := QrTmpPromCodigo.Value;
        //
        if Length(CPF) > 0 then
        begin
          Entidade := VerificaEntidade(CPF);
          if Entidade = 0 then
            Entidade := CadastraEntidade(CPF, Nome);
          VAR_CADASTRO := CadastraPromotor(CodUsu, Entidade);
        end else
        begin
          Geral.MB_Aviso('O promotor selecionado n�o possui CPF cadastrado!' +
            sLineBreak + 'Preencha o CPF e tente novamente!');
          Aviso := Aviso + 1;
        end;
      end;
    end;
    MontaGrade;
    if Aviso = 0 then
      Close;
  end else
  begin
    CPF := QrTmpPromCPF.Value;
    CPF := Geral.SoNumero_TT(CPF);
    Nome := QrTmpPromNome.Value;
    CodUsu := QrTmpPromCodigo.Value;
    //
    if Length(CPF) > 0 then
    begin
      Entidade := VerificaEntidade(CPF);
      if Entidade = 0 then
        Entidade := CadastraEntidade(CPF, Nome);
      VAR_CADASTRO := CadastraPromotor(CodUsu, Entidade);
      Close;
    end else
      Geral.MB_Aviso('O promotor selecionado n�o possui CPF cadastrado!' +
        sLineBreak + 'Preencha o CPF e tente novamente!');
  end;
end;

procedure TFmPromoWeb.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

function TFmPromoWeb.CadastraEntidade(CPF, Nome: String): Integer;
var
  Codigo: Integer;
begin
  Codigo := UMyMod.BuscaEmLivreY_Def('entidades', 'Codigo', stIns, 0);
  Nome   := UpperCase(Nome);
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_INCLUSAO, 'entidades', False,
  [
    'Nome', 'CPF', 'Fornece2', 'CodUsu'
  ], ['Codigo'], [
    UpperCase(Nome), CPF, 'V', Codigo
  ], [Codigo]) then
    Result := Codigo
  else
    Result := 0;
end;

function TFmPromoWeb.CadastraPromotor(CodUsu, Entidade: Integer): Integer;
var
  Codigo: Integer;
begin
  Codigo := UMyMod.BuscaEmLivreY_Def('promotores', 'Codigo', stIns, 0);
  if UMyMod.SQLInsUpd_Old(Dmod.QrUpd, CO_INCLUSAO, 'promotores', False,
  [
    'Entidade', 'CodUsu'
  ], ['Codigo'], [
    Entidade, CodUsu
  ], [Codigo]) then
    Result := Codigo
  else
    Result := 0;
end;

procedure TFmPromoWeb.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPromoWeb.FormCreate(Sender: TObject);
begin
  QrTmpProm.Close;
  QrTmpProm.Database := DModG.MyPID_DB;
  MontaGrade;
end;

procedure TFmPromoWeb.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmPromoWeb.MontaGrade;
  procedure AdicionaTemp(Codigo: Integer; Nome, CPF: String);
  begin
    DmodG.QrUpdPID1.SQL.Clear;
    DModG.QrUpdPID1.SQL.Add('INSERT INTO promweb SET Codigo=:P0, Nome=:P1, CPF=:P2');
    DModG.QrUpdPID1.Params[0].AsInteger := Codigo;
    DModG.QrUpdPID1.Params[1].AsString  := Nome;
    DModG.QrUpdPID1.Params[2].AsString  := CPF;
    UMyMod.ExecutaQuery(DmodG.QrUpdPID1);
  end;
var
  Codigo: Integer;
  Nome, CPF: String;
begin
  QrPromoWEB.Close;
  QrPromoWEB.Open;
  //
  PromWeb := UCriar.RecriaTempTable('promweb', DModG.QrUpdPID1, False);
  //
  if QrPromoWEB.RecordCount > 0 then
  begin
    while not QrPromoWEB.Eof do
    begin
      Codigo := QrPromoWEBid.Value;
      Nome   := QrPromoWEBNome.Value;
      CPF    := QrPromoWEBCpf.Value;
      //
      Dmod.QrUpd.Close;
      Dmod.QrUpd.SQL.Clear;
      Dmod.QrUpd.SQL.Add('SELECT CodUsu FROM promotores WHERE CodUsu=:P0');
      Dmod.QrUpd.Params[0].AsInteger := Codigo;
      Dmod.QrUpd.Open;
      if Dmod.QrUpd.RecordCount = 0 then
      begin
        if Length(CPF) > 0 then
          CPF := Geral.FormataCNPJ_TT(CPF);
        AdicionaTemp(Codigo, Nome, CPF);
      end;
      //
      QrPromoWEB.Next;
    end;
  end;
  QrTmpProm.Close;
  QrTmpProm.Open;
end;

function TFmPromoWeb.VerificaEntidade(CPF: String): Integer;
begin
  Dmod.QrUpd.Close;
  Dmod.QrUpd.SQL.Clear;
  Dmod.QrUpd.SQL.Add('SELECT Codigo');
  Dmod.QrUpd.SQL.Add('FROM entidades');
  Dmod.QrUpd.SQL.Add('WHERE CPF=:P0');
  Dmod.QrUpd.Params[0].AsString := CPF;
  Dmod.QrUpd.Open;
  if Dmod.QrUpd.RecordCount > 0 then
    Result := Dmod.QrUpd.FieldByName('Codigo').Value
  else
    Result := 0;
  Dmod.QrUpd.Close;
end;

end.
