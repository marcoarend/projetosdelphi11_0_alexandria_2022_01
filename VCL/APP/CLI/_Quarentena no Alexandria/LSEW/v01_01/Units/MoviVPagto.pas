unit MoviVPagto;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, UnInternalConsts, Db, MyDBCheck,
  mySQLDbTables, DBCtrls, ComCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB,
  UMySQLModule, dmkLabel, UnFinanceiro, dmkGeral, UnDmkProcFunc;

type
  TFmMoviVPagto = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtSaida: TBitBtn;
    Label3: TLabel;
    EdCarteira: TdmkEditCB;
    CBCarteira: TdmkDBLookupComboBox;
    Label1: TLabel;
    EdConta: TdmkEditCB;
    CBConta: TdmkDBLookupComboBox;
    Label2: TLabel;
    EdEnvelope: TdmkEdit;
    QrCarteiras: TmySQLQuery;
    DsCarteiras: TDataSource;
    QrContas: TmySQLQuery;
    DsContas: TDataSource;
    QrCarteirasCodigo: TIntegerField;
    QrCarteirasNome: TWideStringField;
    QrContasCodigo: TIntegerField;
    QrContasNome: TWideStringField;
    CBCondPagto: TdmkDBLookupComboBox;
    Label4: TLabel;
    EdCondPagto: TdmkEditCB;
    DsPediPrzCab: TDataSource;
    QrPediPrzCab: TmySQLQuery;
    QrPediPrzCabCodigo: TIntegerField;
    QrPediPrzCabCodUsu: TIntegerField;
    QrPediPrzCabNome: TWideStringField;
    QrPediPrzIts: TmySQLQuery;
    QrCarteirasForneceI: TIntegerField;
    QrCarteirasTipo: TIntegerField;
    EdEnvelope1: TdmkEdit;
    QrPediPrzItsControle: TIntegerField;
    QrPediPrzItsDias: TIntegerField;
    QrPediPrzItsPercent: TFloatField;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure ReopenCarteiras;
    procedure ReopenContas;
    procedure InserirParcela(TabLctA, Descricao: String; Valor: Double);
  public
    { Public declarations }
    FCodigo, FControle: Integer;
  end;

  var
  FmMoviVPagto: TFmMoviVPagto;

implementation

{$R *.DFM}

uses Module, MyListas, ModuleGeral, MoviV, UnMyObjects;

procedure TFmMoviVPagto.BtOKClick(Sender: TObject);
var
  Codigo, Carteira, Conta, CondPgto, Documento, PosIni: Integer;
  T1, V1, Valor: Double;
  Variavel, Descricao, DocTxt: String;
begin
  Codigo    := FmMoviV.QrMoviVCodigo.Value;
  Carteira  := EdCarteira.ValueVariant;
  Conta     := EdConta.ValueVariant;
  CondPgto  := EdCondPagto.ValueVariant;
  DocTxt    := EdEnvelope.ValueVariant;
  Documento := EdEnvelope1.ValueVariant;
  Valor     := FmMoviV.QrMoviVTotal.Value + FmMoviV.QrMoviVFrete.Value - FmMoviV.QrMoviVDescon.Value;
  T1        := Valor;
  //
  if Carteira = 0 then
  begin
    Application.MessageBox('Carteira n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBCarteira.SetFocus;
    Exit;
  end;
  if Conta = 0 then
  begin
    Application.MessageBox('Conta n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBConta.SetFocus;
    Exit;
  end;
  if CondPgto = 0 then
  begin
    Application.MessageBox('Condi��o de pagamento n�o definida!', 'Aviso', MB_OK+MB_ICONWARNING);
    CBCondPagto.SetFocus;
    Exit;
  end;
  if (Length(DocTxt) = 0) or (Documento = 0) then
  begin
    Application.MessageBox('N� do envelope n�o definido!', 'Aviso', MB_OK+MB_ICONWARNING);
    EdEnvelope.SetFocus;
    Exit;
  end;
  if Valor = 0 then
  begin
    Application.MessageBox('Para lan�ar o pagamento a venda deve possuir um valor!', 'Aviso', MB_OK+MB_ICONWARNING);
    Exit;
  end;
  //
  QrPediPrzIts.Close;
  QrPediPrzIts.Params[0].AsInteger := CondPgto;
  QrPediPrzIts.Open;
  //
  QrPediPrzIts.First;
  while not QrPediPrzIts.Eof do
  begin
    if QrPediPrzIts.RecordCount = QrPediPrzIts.RecNo then
      V1 := Valor
    else begin
      V1 := (Round(T1 * (QrPediPrzItsPercent.Value / 100 * 100))) / 100;
      //
      Valor := Valor - V1;
    end;
    //
    Descricao := Dmod.QrControleTxtVdaPro.Value;
    PosIni     := Pos('[', Descricao);
    if PosIni > 0 then
      Variavel  := Copy(Descricao, PosIni, 8);
    if Variavel = '[CODIGO]' then
      Descricao := Copy(Descricao, 1, PosIni - 1) + IntToStr(Codigo);
    Descricao := Descricao + ' - ' + dmkPF.ParcelaFatura(QrPediPrzIts.RecNo, 0)
                 + '/' + IntToStr(QrPediPrzIts.RecordCount);
    //
    InserirParcela(FmMoviV.FTabLctALS, Descricao, V1);
    //
    QrPediPrzIts.Next;
  end;
  FmMoviV.FPagto := True;
  Close;
end;

procedure TFmMoviVPagto.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmMoviVPagto.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
  EdEnvelope.SetFocus;
end;

procedure TFmMoviVPagto.FormCreate(Sender: TObject);
begin
  ReopenCarteiras;
  ReopenContas;
  QrPediPrzCab.Open;
  //
  EdCarteira.ValueVariant  := Dmod.QrControleVdaCart.Value;
  CBCarteira.KeyValue      := Dmod.QrControleVdaCart.Value;
  EdConta.ValueVariant     := Dmod.QrControleVdaConta.Value;
  CBConta.KeyValue         := Dmod.QrControleVdaConta.Value;
  EdCondPagto.ValueVariant := Dmod.QrControleVdaConPgto.Value;
  CBCondPagto.KeyValue     := Dmod.QrControleVdaConPgto.Value;
  //
  FmMoviV.FPagto := False;
end;

procedure TFmMoviVPagto.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmMoviVPagto.InserirParcela(TabLctA, Descricao: String; Valor: Double);
var
  Agora: TDateTime;
  Vencimento, Compensado: String;
  Sit: Integer;
begin
  Agora      := DModG.ObtemAgora();
  Vencimento := Geral.FDT(Agora + QrPediPrzItsDias.Value, 1);
  //
  if QrCarteirasTipo.Value = 2 then
  begin
    Compensado := '';
    Sit := 0;
  end else begin
    Compensado := Vencimento;
    Sit := 3;
  end;
  //
  FLAN_SerieCH       := EdEnvelope.ValueVariant;
  FLAN_Documento     := EdEnvelope1.ValueVariant;
  FLAN_Data          := Geral.FDT(Agora, 1);
  FLAN_Vencimento    := Vencimento;
  FLAN_DataCad       := Geral.FDT(Agora, 1);
  FLAN_DataDoc       := Geral.FDT(Date, 1);
  FLAN_UserCad       := VAR_USUARIO;
  FLAN_Descricao     := Descricao;
  FLAN_Tipo          := QrCarteirasTipo.Value;
  FLAN_Carteira      := EdCarteira.ValueVariant;
  FLAN_Credito       := Valor;
  FLAN_Genero        := EdConta.ValueVariant;
  FLAN_Cliente       := FmMoviV.QrMoviVCliente.Value;
  FLAN_Vendedor      := VAR_USUARIO;
  FLAN_CliInt        := QrCarteirasForneceI.Value; // Carteiras.ForneceI = Cliente interno
  FLAN_FatID         := 510;
  FLAN_FatID_Sub     := 0;
  FLAN_FatNum        := FmMoviV.QrMoviVControle.Value;
  FLAN_FatParcela    := QrPediPrzIts.RecNo;
  FLAN_Sit           := Sit;
  FLAN_Compensado    := Compensado;
  //
  FLAN_Controle := UMyMod.BuscaEmLivreY(Dmod.MyDB, 'Livres',
    'Controle', TabLctA, LAN_CTOS, 'Controle');
  UFinanceiro.InsereLancamento(FmMoviV.FTabLctALS);
end;

procedure TFmMoviVPagto.ReopenCarteiras;
begin
  QrCarteiras.Close;
  QrCarteiras.SQL.Clear;
  QrCarteiras.SQL.Add('SELECT car.Codigo, car.Nome, ');
  QrCarteiras.SQL.Add('car.ForneceI, car.Tipo');
  QrCarteiras.SQL.Add('FROM carteiras car');
  QrCarteiras.SQL.Add('LEFT JOIN carteirasu cau ON cau.Codigo = car.Codigo');
  if VAR_USUARIO > 0 then
    QrCarteiras.SQL.Add('WHERE cau.Usuario =' + IntToStr(VAR_USUARIO));
  QrCarteiras.SQL.Add('GROUP BY car.Codigo');
  QrCarteiras.SQL.Add('ORDER BY car.Nome');
  QrCarteiras.Open;
end;

procedure TFmMoviVPagto.ReopenContas;
begin
  QrContas.Close;
  QrContas.SQL.Clear;
  QrContas.SQL.Add('SELECT con.Codigo, con.Nome');
  QrContas.SQL.Add('FROM contas con');
  QrContas.SQL.Add('LEFT JOIN contasu cou ON cou.Codigo = con.Codigo');
  if VAR_USUARIO > 0 then
    QrContas.SQL.Add('WHERE cou.Usuario =' + IntToStr(VAR_USUARIO));
  QrContas.SQL.Add('GROUP BY con.Codigo');
  QrContas.SQL.Add('ORDER BY con.Nome');
  QrContas.Open;
end;

end.

