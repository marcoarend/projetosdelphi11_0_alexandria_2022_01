unit PedPrzCli;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, DB, mySQLDbTables, dmkLabel,
  DBCtrls, dmkDBLookupComboBox, dmkEdit, dmkEditCB;

type
  TFmPediPrzCli = class(TForm)
    PainelConfirma: TPanel;
    BtOK: TBitBtn;
    PainelTitulo: TPanel;
    Image1: TImage;
    Panel1: TPanel;
    Panel2: TPanel;
    BtDesiste: TBitBtn;
    EdEntidade: TdmkEditCB;
    CBEntidade: TdmkDBLookupComboBox;
    dmkLabel1: TdmkLabel;
    QrEntidades: TmySQLQuery;
    DsEntidades: TDataSource;
    QrEntidadesCodigo: TIntegerField;
    QrEntidadesNO_ENT: TWideStringField;
    procedure BtDesisteClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    SQLExtra: String;
    FSelecionou: Boolean;
  end;

  var
  FmPediPrzCli: TFmPediPrzCli;

implementation

uses Module, UnMyObjects;

{$R *.DFM}

procedure TFmPediPrzCli.BtDesisteClick(Sender: TObject);
begin
  Close;
end;

procedure TFmPediPrzCli.BtOKClick(Sender: TObject);
begin
  FSelecionou := True;
  Close;
end;

procedure TFmPediPrzCli.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmPediPrzCli.FormCreate(Sender: TObject);
begin
  // QrFiliais deve ser setado e aberto pelo form que chama este form
  // QrFiliais.Open;
  //
  FSelecionou := False;
end;

procedure TFmPediPrzCli.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

end.
