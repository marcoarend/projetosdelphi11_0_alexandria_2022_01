object FmCiclosRat3: TFmCiclosRat3
  Left = 339
  Top = 185
  Caption = 'CIC-CICLO-007 :: Rateio de Valores de Vendas de Mercadorias'
  ClientHeight = 610
  ClientWidth = 975
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelConfirma: TPanel
    Left = 0
    Top = 551
    Width = 975
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object BtOK: TBitBtn
      Tag = 14
      Left = 25
      Top = 5
      Width = 110
      Height = 49
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '&OK'
      NumGlyphs = 2
      TabOrder = 0
      OnClick = BtOKClick
    end
    object Panel2: TPanel
      Left = 837
      Top = 1
      Width = 137
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 1
      object BtSaida: TBitBtn
        Tag = 13
        Left = 2
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Desiste'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 975
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Rateio de Valores de Vendas de Mercadorias'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 973
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 975
    Height = 492
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 0
    object DBGrid2: TDBGrid
      Left = 1
      Top = 46
      Width = 973
      Height = 445
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      DataSource = DmCiclos.DsAuto
      Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgMultiSelect]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      Columns = <
        item
          Expanded = False
          FieldName = 'SEQ'
          Title.Caption = 'N'#186
          Width = 22
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Data'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Vencimento'
          Title.Caption = 'Vencto'
          Width = 56
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Credito'
          Title.Alignment = taRightJustify
          Title.Caption = 'Valor'
          Width = 70
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Debito'
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'Descricao'
          Title.Caption = 'Descri'#231#227'o'
          Width = 200
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECARTEIRA'
          Title.Caption = 'Carteira'
          Width = 175
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECONTA'
          Title.Caption = 'Conta (Plano de contas)'
          Width = 64
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 973
      Height = 45
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      BevelOuter = bvNone
      TabOrder = 1
      object Panel4: TPanel
        Left = 0
        Top = 0
        Width = 16
        Height = 44
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 0
      end
      object Panel5: TPanel
        Left = 955
        Top = 0
        Width = 17
        Height = 44
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
      end
      object Panel6: TPanel
        Left = 16
        Top = 0
        Width = 939
        Height = 44
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 2
        object STAvisos: TStaticText
          Left = 0
          Top = 2
          Width = 16
          Height = 20
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          Caption = '...'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBackground
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          TabOrder = 0
        end
        object PB1: TProgressBar
          Left = 0
          Top = 23
          Width = 939
          Height = 21
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alBottom
          TabOrder = 1
        end
      end
    end
  end
  object QrProds: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT mom.Grade, SUM(Ven) Valor'
      'FROM movim mom'
      'WHERE  mom.SubCtrl=3'
      'AND mom.Controle=:P0'
      'GROUP BY mom.Grade')
    Left = 12
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrProdsGrade: TIntegerField
      FieldName = 'Grade'
      Origin = 'movim.Grade'
      Required = True
    end
    object QrProdsValor: TFloatField
      FieldName = 'Valor'
      Origin = 'Valor'
    end
  end
  object QrComProd: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ecf.Genero, ecf.PerceGene, '
      'ecf.CredDeb, ecf.EhComProf, ecf.Rateio,'
      'eci.Compoe  '
      'FROM equicontrf ecf'
      'LEFT JOIN equicomits eci ON eci.Controle=ecf.Controle'
      'WHERE ecf.Codigo=:P0 '
      'AND eci.Produto=:P1')
    Left = 40
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrComProdGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'equicontrf.Genero'
      Required = True
    end
    object QrComProdPerceGene: TFloatField
      FieldName = 'PerceGene'
      Origin = 'equicontrf.PerceGene'
      Required = True
    end
    object QrComProdCredDeb: TSmallintField
      FieldName = 'CredDeb'
      Origin = 'equicontrf.CredDeb'
      Required = True
    end
    object QrComProdEhComProf: TSmallintField
      FieldName = 'EhComProf'
      Origin = 'equicontrf.EhComProf'
      Required = True
    end
    object QrComProdRateio: TSmallintField
      FieldName = 'Rateio'
      Origin = 'equicontrf.Rateio'
      Required = True
    end
    object QrComProdCompoe: TSmallintField
      FieldName = 'Compoe'
      Origin = 'equicomits.Compoe'
    end
  end
  object QrImpede3: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ecf.Genero, ecf.PerceGene, '
      'ecf.CredDeb, ecf.EhComProf, ecf.Rateio  '
      'FROM equicontrf ecf'
      'LEFT JOIN equicomits eci ON eci.Controle=ecf.Controle'
      'WHERE ecf.Codigo=:P0'
      'AND ecf.Rateio=3')
    Left = 68
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
  end
  object QrRat: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrRatCalcFields
    SQL.Strings = (
      'SELECT ecf.Genero, ecf.PerceGene, ecf.CredDeb, '
      'ecf.EhComProf, ecf.Rateio, ecf.CartIdx,'
      'cta.Nome NOMECONTA, eci.Compoe  '
      'FROM equicontrf ecf'
      'LEFT JOIN equicomits eci ON eci.Controle=ecf.Controle'
      'LEFT JOIN contas cta ON ecf.Genero=cta.Codigo'
      'WHERE ecf.Rateio=:P0'
      'AND ecf.CredDeb=:P1'
      'AND ecf.Codigo=:P2'
      'AND eci.Produto=:P3'
      'ORDER BY PerceGene DESC')
    Left = 684
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrRatGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'equicontrf.Genero'
      Required = True
    end
    object QrRatPerceGene: TFloatField
      FieldName = 'PerceGene'
      Origin = 'equicontrf.PerceGene'
      Required = True
    end
    object QrRatCredDeb: TSmallintField
      FieldName = 'CredDeb'
      Origin = 'equicontrf.CredDeb'
      Required = True
    end
    object QrRatEhComProf: TSmallintField
      FieldName = 'EhComProf'
      Origin = 'equicontrf.EhComProf'
      Required = True
    end
    object QrRatRateio: TSmallintField
      FieldName = 'Rateio'
      Origin = 'equicontrf.Rateio'
      Required = True
    end
    object QrRatCompoe: TSmallintField
      FieldName = 'Compoe'
      Origin = 'equicomits.Compoe'
    end
    object QrRatNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrRatFATID: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'FATID'
      Calculated = True
    end
    object QrRatFATID_SUB: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'FATID_SUB'
      Calculated = True
    end
    object QrRatCartIdx: TSmallintField
      FieldName = 'CartIdx'
      Required = True
    end
  end
  object QrSum: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(ecf.PerceGene) Total'
      'FROM equicontrf ecf'
      'LEFT JOIN equicomits eci ON eci.Controle=ecf.Controle'
      'WHERE ecf.Rateio=:P0'
      'AND ecf.CredDeb=:P1'
      'AND ecf.Codigo=:P2'
      'AND eci.Produto=:P3'
      '')
    Left = 712
    Top = 8
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P3'
        ParamType = ptUnknown
      end>
    object QrSumTotal: TFloatField
      FieldName = 'Total'
    end
  end
  object QrTrf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Carteira, SUM(Credito-Debito) Valor'
      'FROM lanctos'
      'WHERE Genero > -1 '
      'AND FatID BETWEEN 700 and 749'
      'AND FatNum=:P0'
      'AND Carteira<>:P1'
      'GROUP BY Carteira')
    Left = 96
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTrfCarteira: TIntegerField
      FieldName = 'Carteira'
    end
    object QrTrfValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object QrClc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 740
    Top = 8
  end
  object dmkPermissoes1: TdmkPermissoes
    Left = 148
    Top = 12
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 412
    Top = 168
  end
end
