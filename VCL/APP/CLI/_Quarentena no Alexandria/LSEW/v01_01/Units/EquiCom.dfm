object FmEquiCom: TFmEquiCom
  Left = 339
  Top = 168
  Caption = 'CAD-COMIS-001 :: Rol de Comiss'#245'es'
  ClientHeight = 562
  ClientWidth = 933
  Color = clBtnFace
  Constraints.MinHeight = 260
  Constraints.MinWidth = 640
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object PainelItens: TPanel
    Left = 0
    Top = 48
    Width = 933
    Height = 514
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 3
    Visible = False
    object Panel6: TPanel
      Left = 1
      Top = 465
      Width = 931
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BitBtn1: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BitBtn1Click
      end
      object Panel7: TPanel
        Left = 822
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn2: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn2Click
        end
      end
    end
    object Panel8: TPanel
      Left = 1
      Top = 1
      Width = 931
      Height = 196
      Align = alTop
      TabOrder = 0
      object Panel10: TPanel
        Left = 1
        Top = 53
        Width = 929
        Height = 142
        Align = alClient
        TabOrder = 1
        object Label11: TLabel
          Left = 16
          Top = 4
          Width = 56
          Height = 13
          Caption = 'Mercadoria:'
        end
        object LaComisQtd: TLabel
          Left = 456
          Top = 4
          Width = 54
          Height = 13
          Caption = 'Percentual:'
          Enabled = False
        end
        object SpeedButton5: TSpeedButton
          Left = 432
          Top = 20
          Width = 21
          Height = 21
          Caption = '...'
          OnClick = SpeedButton5Click
        end
        object EdProduto: TdmkEditCB
          Left = 16
          Top = 20
          Width = 56
          Height = 21
          Alignment = taRightJustify
          TabOrder = 0
          FormatType = dmktfInteger
          MskType = fmtNone
          DecimalSize = 0
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ValMin = '-2147483647'
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = 0
          ValWarn = False
          DBLookupComboBox = CBProduto
          IgnoraDBLookupComboBox = False
        end
        object CBProduto: TdmkDBLookupComboBox
          Left = 71
          Top = 20
          Width = 360
          Height = 21
          KeyField = 'Codigo'
          ListField = 'Nome'
          ListSource = DsProdutos
          TabOrder = 1
          dmkEditCB = EdProduto
          UpdType = utYes
          LocF7SQLMasc = '$#'
        end
        object EdPerceProd: TdmkEdit
          Left = 456
          Top = 20
          Width = 80
          Height = 21
          Enabled = False
          TabOrder = 2
          FormatType = dmktfString
          MskType = fmtNone
          DecimalSize = 6
          LeftZeros = 0
          NoEnterToTab = False
          NoForceUppercase = False
          ForceNextYear = False
          DataFormat = dmkdfShort
          HoraFormat = dmkhfShort
          Texto = '0'
          UpdType = utYes
          Obrigatorio = False
          PermiteNulo = False
          ValueVariant = '0'
          ValWarn = False
        end
        object CkCompoe: TCheckBox
          Left = 16
          Top = 44
          Width = 521
          Height = 17
          Caption = 'Esta mercadoria comp'#245'e o valor do curso.'
          TabOrder = 3
        end
      end
      object Panel9: TPanel
        Left = 1
        Top = 1
        Width = 929
        Height = 52
        Align = alTop
        TabOrder = 0
        object Label7: TLabel
          Left = 16
          Top = 8
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdit1
        end
        object Label8: TLabel
          Left = 68
          Top = 8
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdit20
        end
        object DBEdit19: TDBEdit
          Left = 16
          Top = 24
          Width = 48
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsEquiCom
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdit20: TDBEdit
          Left = 68
          Top = 24
          Width = 469
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsEquiCom
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
      end
    end
  end
  object PainelContas: TPanel
    Left = 0
    Top = 48
    Width = 933
    Height = 514
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 4
    Visible = False
    object Panel11: TPanel
      Left = 1
      Top = 465
      Width = 931
      Height = 48
      Align = alBottom
      TabOrder = 1
      object Label30: TLabel
        Left = 124
        Top = 16
        Width = 321
        Height = 13
        Caption = 
          'MI*: M'#237'nimo de itens para aplicar o percentual na inclus'#227'o de tu' +
          'rma.'
      end
      object BitBtn3: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BitBtn3Click
      end
      object Panel12: TPanel
        Left = 822
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BitBtn4: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BitBtn2Click
        end
      end
    end
    object Panel13: TPanel
      Left = 1
      Top = 1
      Width = 931
      Height = 428
      Align = alTop
      TabOrder = 0
      object PainelEditCta: TPanel
        Left = 1
        Top = 93
        Width = 929
        Height = 334
        Align = alClient
        TabOrder = 1
        object Panel14: TPanel
          Left = 1
          Top = 1
          Width = 927
          Height = 180
          Align = alTop
          BevelOuter = bvLowered
          TabOrder = 0
          object Label3: TLabel
            Left = 8
            Top = 52
            Width = 131
            Height = 13
            Caption = 'Conta (do plano de contas):'
          end
          object SpeedButton6: TSpeedButton
            Left = 360
            Top = 68
            Width = 21
            Height = 21
            Caption = '...'
            OnClick = SpeedButton6Click
          end
          object Label4: TLabel
            Left = 383
            Top = 52
            Width = 54
            Height = 13
            Caption = 'Percentual:'
          end
          object Label29: TLabel
            Left = 467
            Top = 52
            Width = 19
            Height = 13
            Caption = 'MI*:'
          end
          object Label31: TLabel
            Left = 532
            Top = 52
            Width = 123
            Height = 13
            Caption = 'Mercadoria comissionada:'
          end
          object RGCredDeb: TRadioGroup
            Left = 8
            Top = 4
            Width = 225
            Height = 41
            Caption = ' Orienta'#231#227'o do rateio a d'#233'bito ou cr'#233'dito: '
            Columns = 3
            ItemIndex = 0
            Items.Strings = (
              'Indefinido'
              'Cr'#233'dito'
              'D'#233'bito')
            TabOrder = 0
            OnClick = RGCredDebClick
          end
          object EdGenero: TdmkEditCB
            Left = 8
            Top = 68
            Width = 43
            Height = 21
            Alignment = taRightJustify
            TabOrder = 3
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBGenero
            IgnoraDBLookupComboBox = False
          end
          object CBGenero: TdmkDBLookupComboBox
            Left = 52
            Top = 68
            Width = 309
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsContas
            TabOrder = 4
            dmkEditCB = EdGenero
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
          object EdPerceGene: TdmkEdit
            Left = 383
            Top = 68
            Width = 80
            Height = 21
            Alignment = taRightJustify
            TabOrder = 5
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 6
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object RGRateio: TRadioGroup
            Left = 236
            Top = 4
            Width = 489
            Height = 41
            Caption = ' Forma de rateio do valor (como '#233' calculado): '
            Columns = 5
            Enabled = False
            Items.Strings = (
              '? ? ?'
              'Valor pr'#233'-fixado'
              'Valor vari'#225'vel'
              'Valor p'#243's-fixado'
              'C'#225'lc. por turma')
            TabOrder = 1
          end
          object CkEhComProf: TCheckBox
            Left = 10
            Top = 94
            Width = 337
            Height = 17
            Caption = 
              'O valor gerado para esta conta faz parte da comiss'#227'o ao professo' +
              'r.'
            Enabled = False
            TabOrder = 9
          end
          object CkContinuar: TCheckBox
            Left = 358
            Top = 94
            Width = 113
            Height = 17
            Caption = 'Continuar inserindo.'
            Checked = True
            State = cbChecked
            TabOrder = 10
            Visible = False
          end
          object RGCartIdx: TdmkRadioGroup
            Left = 728
            Top = 4
            Width = 189
            Height = 41
            Caption = ' Carteira:'
            ItemIndex = 0
            Items.Strings = (
              'Carrega em tempo de execu'#231#227'o...')
            TabOrder = 2
            UpdType = utYes
            OldValor = 0
          end
          object RichEdit1: TRichEdit
            Left = 8
            Top = 116
            Width = 909
            Height = 61
            TabStop = False
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            Lines.Strings = (
              
                'Valor pr'#233'-fixado: Quando j'#225' se sabe a % fixa que se aplicar'#225' no ' +
                'rateio.'
              
                'Valor vari'#225'vel: Quando se informa a % que se aplicar'#225' ao saldo d' +
                'a diferen'#231'a do valor bruto j'#225' subtraido o valor pr'#233'-fixado e o v' +
                'alor p'#243's-fixado.'
              
                'Valor p'#243's-fixado: Quando se informa um % para provisionar, mas s' +
                'e utilizar'#225' o valor realmente utilizado.'
              
                'C'#225'lc. por turma: O valor ser'#225' definido pela soma dos valores inf' +
                'ormados nas turmas cadastradas pela conta selecionada. (ex. comi' +
                'ss'#227'o e 13'#186').')
            ParentFont = False
            ReadOnly = True
            TabOrder = 11
          end
          object EdMinQtde: TdmkEdit
            Left = 467
            Top = 68
            Width = 61
            Height = 21
            Alignment = taRightJustify
            TabOrder = 6
            FormatType = dmktfDouble
            MskType = fmtNone
            DecimalSize = 3
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0,000'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0.000000000000000000
            ValWarn = False
          end
          object EdGradeComis: TdmkEditCB
            Left = 532
            Top = 68
            Width = 56
            Height = 21
            Alignment = taRightJustify
            TabOrder = 7
            FormatType = dmktfInteger
            MskType = fmtNone
            DecimalSize = 0
            LeftZeros = 0
            NoEnterToTab = False
            NoForceUppercase = False
            ValMin = '-2147483647'
            ForceNextYear = False
            DataFormat = dmkdfShort
            HoraFormat = dmkhfShort
            Texto = '0'
            UpdType = utYes
            Obrigatorio = False
            PermiteNulo = False
            ValueVariant = 0
            ValWarn = False
            DBLookupComboBox = CBGradeComis
            IgnoraDBLookupComboBox = False
          end
          object CBGradeComis: TdmkDBLookupComboBox
            Left = 588
            Top = 68
            Width = 329
            Height = 21
            KeyField = 'Codigo'
            ListField = 'Nome'
            ListSource = DsGrades
            TabOrder = 8
            dmkEditCB = EdGradeComis
            UpdType = utYes
            LocF7SQLMasc = '$#'
          end
        end
        object Panel17: TPanel
          Left = 1
          Top = 181
          Width = 927
          Height = 152
          Align = alClient
          BevelOuter = bvLowered
          TabOrder = 1
          object Splitter2: TSplitter
            Left = 1
            Top = 84
            Width = 925
            Height = 5
            Cursor = crVSplit
            Align = alTop
            ExplicitLeft = 21
            ExplicitTop = 89
          end
          object Panel23: TPanel
            Left = 1
            Top = 89
            Width = 925
            Height = 62
            Align = alClient
            BevelOuter = bvLowered
            TabOrder = 0
            object StaticText2: TStaticText
              Left = 1
              Top = 1
              Width = 923
              Height = 17
              Align = alTop
              Alignment = taCenter
              Caption = 'D'#233'bitos'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clHighlight
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
            object dmkDBGrid4: TdmkDBGrid
              Left = 1
              Top = 18
              Width = 923
              Height = 43
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Genero'
                  Title.Caption = 'Conta (P)'
                  Width = 50
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECONTA'
                  Title.Caption = 'Descri'#231#227'o da conta (do plano de contas)'
                  Width = 252
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PerceGene'
                  Title.Caption = '% transferir'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECREDDEBRAT'
                  Title.Caption = 'Forma de rateio'
                  Width = 150
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsEquiConDeb
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Genero'
                  Title.Caption = 'Conta (P)'
                  Width = 50
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECONTA'
                  Title.Caption = 'Descri'#231#227'o da conta (do plano de contas)'
                  Width = 252
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PerceGene'
                  Title.Caption = '% transferir'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECREDDEBRAT'
                  Title.Caption = 'Forma de rateio'
                  Width = 150
                  Visible = True
                end>
            end
          end
          object Panel16: TPanel
            Left = 1
            Top = 1
            Width = 925
            Height = 83
            Align = alTop
            BevelOuter = bvLowered
            TabOrder = 1
            object StaticText1: TStaticText
              Left = 1
              Top = 1
              Width = 923
              Height = 17
              Align = alTop
              Alignment = taCenter
              Caption = 'Cr'#233'ditos'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clHighlight
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
            object dmkDBGrid3: TdmkDBGrid
              Left = 1
              Top = 18
              Width = 923
              Height = 64
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Genero'
                  Title.Caption = 'Conta (P)'
                  Width = 50
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECONTA'
                  Title.Caption = 'Descri'#231#227'o da conta (do plano de contas)'
                  Width = 252
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PerceGene'
                  Title.Caption = '% transferir'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECREDDEBRAT'
                  Title.Caption = 'Forma de rateio'
                  Width = 150
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsEquiConCre
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Genero'
                  Title.Caption = 'Conta (P)'
                  Width = 50
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECONTA'
                  Title.Caption = 'Descri'#231#227'o da conta (do plano de contas)'
                  Width = 252
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PerceGene'
                  Title.Caption = '% transferir'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECREDDEBRAT'
                  Title.Caption = 'Forma de rateio'
                  Width = 150
                  Visible = True
                end>
            end
          end
        end
      end
      object Panel15: TPanel
        Left = 1
        Top = 1
        Width = 929
        Height = 92
        Align = alTop
        TabOrder = 0
        object Panel4: TPanel
          Left = 1
          Top = 1
          Width = 927
          Height = 90
          Align = alClient
          BevelOuter = bvLowered
          TabOrder = 0
          object Label5: TLabel
            Left = 16
            Top = 4
            Width = 36
            Height = 13
            Caption = 'C'#243'digo:'
            FocusControl = DBEdit21
          end
          object Label6: TLabel
            Left = 68
            Top = 4
            Width = 51
            Height = 13
            Caption = 'Descri'#231#227'o:'
            FocusControl = DBEdit22
          end
          object Label13: TLabel
            Left = 440
            Top = 4
            Width = 40
            Height = 13
            Caption = 'Produto:'
            FocusControl = DBEdit2
          end
          object Label15: TLabel
            Left = 16
            Top = 44
            Width = 55
            Height = 13
            Caption = 'Cr'#233'dito fixo:'
            FocusControl = DBEdit5
          end
          object Label16: TLabel
            Left = 100
            Top = 44
            Width = 76
            Height = 13
            Caption = 'Cr'#233'dito vari'#225'vel:'
            FocusControl = DBEdit6
          end
          object Label17: TLabel
            Left = 436
            Top = 44
            Width = 53
            Height = 13
            Caption = 'D'#233'bito fixo:'
            FocusControl = DBEdit7
          end
          object Label18: TLabel
            Left = 520
            Top = 44
            Width = 74
            Height = 13
            Caption = 'D'#233'bito vari'#225'vel:'
            FocusControl = DBEdit8
          end
          object Label21: TLabel
            Left = 184
            Top = 44
            Width = 72
            Height = 13
            Caption = 'Cr'#233'dito p'#243's-fix.:'
            FocusControl = DBEdit11
          end
          object Label22: TLabel
            Left = 268
            Top = 44
            Width = 74
            Height = 13
            Caption = 'TOTAL Cr'#233'dito:'
            FocusControl = DBEdit12
          end
          object Label23: TLabel
            Left = 604
            Top = 44
            Width = 70
            Height = 13
            Caption = 'D'#233'bito p'#243's-fix.:'
            FocusControl = DBEdit13
          end
          object Label24: TLabel
            Left = 688
            Top = 44
            Width = 72
            Height = 13
            Caption = 'TOTAL D'#233'bito:'
            FocusControl = DBEdit14
          end
          object DBEdit22: TDBEdit
            Left = 68
            Top = 20
            Width = 369
            Height = 21
            Hint = 'Nome do banco'
            Color = clWhite
            DataField = 'Nome'
            DataSource = DsEquiCom
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
          end
          object DBEdit21: TDBEdit
            Left = 16
            Top = 20
            Width = 48
            Height = 21
            Hint = 'N'#186' do banco'
            TabStop = False
            DataField = 'Codigo'
            DataSource = DsEquiCom
            Font.Charset = DEFAULT_CHARSET
            Font.Color = 8281908
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            ParentShowHint = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
          end
          object DBEdit2: TDBEdit
            Left = 440
            Top = 20
            Width = 41
            Height = 21
            DataField = 'Produto'
            DataSource = DsEquiComIts
            TabOrder = 2
          end
          object DBEdit1: TDBEdit
            Left = 484
            Top = 20
            Width = 285
            Height = 21
            DataField = 'NOMEMEPROD'
            DataSource = DsEquiComIts
            TabOrder = 3
          end
          object DBEdit5: TDBEdit
            Left = 16
            Top = 60
            Width = 80
            Height = 21
            DataField = 'ParCreFix'
            DataSource = DsEquiComIts
            TabOrder = 4
          end
          object DBEdit6: TDBEdit
            Left = 100
            Top = 60
            Width = 80
            Height = 21
            DataField = 'ParCreVar'
            DataSource = DsEquiComIts
            TabOrder = 5
          end
          object DBEdit7: TDBEdit
            Left = 436
            Top = 60
            Width = 80
            Height = 21
            DataField = 'ParDebFix'
            DataSource = DsEquiComIts
            TabOrder = 6
          end
          object DBEdit8: TDBEdit
            Left = 520
            Top = 60
            Width = 80
            Height = 21
            DataField = 'ParDebVar'
            DataSource = DsEquiComIts
            TabOrder = 7
          end
          object DBEdit11: TDBEdit
            Left = 184
            Top = 60
            Width = 80
            Height = 21
            DataField = 'ParCrePos'
            DataSource = DsEquiComIts
            TabOrder = 8
          end
          object DBEdit12: TDBEdit
            Left = 268
            Top = 60
            Width = 80
            Height = 21
            DataField = 'TotCre'
            DataSource = DsEquiComIts
            TabOrder = 9
          end
          object DBEdit13: TDBEdit
            Left = 604
            Top = 60
            Width = 80
            Height = 21
            DataField = 'ParDebPos'
            DataSource = DsEquiComIts
            TabOrder = 10
          end
          object DBEdit14: TDBEdit
            Left = 688
            Top = 60
            Width = 80
            Height = 21
            DataField = 'TotDeb'
            DataSource = DsEquiComIts
            TabOrder = 11
          end
        end
      end
    end
  end
  object PainelEdita: TPanel
    Left = 0
    Top = 48
    Width = 933
    Height = 514
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    object PainelConfirma: TPanel
      Left = 1
      Top = 465
      Width = 931
      Height = 48
      Align = alBottom
      TabOrder = 1
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 8
        Top = 3
        Width = 90
        Height = 40
        Cursor = crHandPoint
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 822
        Top = 1
        Width = 108
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 7
          Top = 2
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object PainelEdit: TPanel
      Left = 1
      Top = 1
      Width = 931
      Height = 196
      Align = alTop
      TabOrder = 0
      object Label9: TLabel
        Left = 16
        Top = 8
        Width = 36
        Height = 13
        Caption = 'C'#243'digo:'
      end
      object Label10: TLabel
        Left = 68
        Top = 8
        Width = 51
        Height = 13
        Caption = 'Descri'#231#227'o:'
      end
      object EdCodigo: TEdit
        Left = 16
        Top = 24
        Width = 48
        Height = 21
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8281908
        Font.Height = -11
        Font.Name = 'MS Sans Serif'
        Font.Style = [fsBold]
        ParentFont = False
        ReadOnly = True
        TabOrder = 0
        Text = '00'
      end
      object EdNome: TEdit
        Left = 68
        Top = 24
        Width = 469
        Height = 21
        TabOrder = 1
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 48
    Width = 933
    Height = 514
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    object PainelControle: TPanel
      Left = 1
      Top = 465
      Width = 931
      Height = 48
      Align = alBottom
      TabOrder = 0
      object LaRegistro: TdmkLabel
        Left = 173
        Top = 1
        Width = 288
        Height = 46
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
        ExplicitWidth = 26
        ExplicitHeight = 13
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 172
        Height = 46
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 128
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 88
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 48
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 8
          Top = 4
          Width = 40
          Height = 40
          Cursor = crHandPoint
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 461
        Top = 1
        Width = 469
        Height = 46
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object BtEquiComIts: TBitBtn
          Tag = 30
          Left = 96
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Mercad.'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtEquiComItsClick
        end
        object BtEquiCom: TBitBtn
          Tag = 194
          Left = 4
          Top = 4
          Width = 90
          Height = 40
          Cursor = crHandPoint
          Caption = '&Rol'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtEquiComClick
        end
        object Panel2: TPanel
          Left = 360
          Top = 0
          Width = 109
          Height = 46
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 2
          object BtSaida: TBitBtn
            Tag = 13
            Left = 4
            Top = 4
            Width = 90
            Height = 40
            Cursor = crHandPoint
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtRateio: TBitBtn
          Tag = 324
          Left = 188
          Top = 4
          Width = 90
          Height = 40
          Caption = '&Rateio'
          TabOrder = 3
          OnClick = BtRateioClick
        end
      end
    end
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 931
      Height = 108
      Align = alTop
      Enabled = False
      TabOrder = 1
      object GroupBox1: TGroupBox
        Left = 540
        Top = 1
        Width = 390
        Height = 106
        Align = alLeft
        Caption = ' Dados extras da mercadoria selecionada (somas parciais): '
        TabOrder = 0
        object Label19: TLabel
          Left = 12
          Top = 16
          Width = 55
          Height = 13
          Caption = 'Cr'#233'dito fixo:'
          FocusControl = DBEdit9
        end
        object Label20: TLabel
          Left = 96
          Top = 16
          Width = 76
          Height = 13
          Caption = 'Cr'#233'dito vari'#225'vel:'
          FocusControl = DBEdit10
        end
        object Label25: TLabel
          Left = 180
          Top = 16
          Width = 72
          Height = 13
          Caption = 'Cr'#233'dito p'#243's-fix.:'
          FocusControl = DBEdit15
        end
        object Label26: TLabel
          Left = 264
          Top = 16
          Width = 74
          Height = 13
          Caption = 'TOTAL Cr'#233'dito:'
          FocusControl = DBEdit16
        end
        object Label12: TLabel
          Left = 96
          Top = 60
          Width = 74
          Height = 13
          Caption = 'D'#233'bito vari'#225'vel:'
          FocusControl = DBEdit4
        end
        object Label14: TLabel
          Left = 12
          Top = 60
          Width = 53
          Height = 13
          Caption = 'D'#233'bito fixo:'
          FocusControl = DBEdit3
        end
        object Label27: TLabel
          Left = 180
          Top = 60
          Width = 70
          Height = 13
          Caption = 'D'#233'bito p'#243's-fix.:'
          FocusControl = DBEdit17
        end
        object Label28: TLabel
          Left = 264
          Top = 60
          Width = 72
          Height = 13
          Caption = 'TOTAL D'#233'bito:'
          FocusControl = DBEdit18
        end
        object DBEdit9: TDBEdit
          Left = 12
          Top = 32
          Width = 80
          Height = 21
          DataField = 'ParCreFix'
          DataSource = DsEquiComIts
          TabOrder = 0
        end
        object DBEdit10: TDBEdit
          Left = 96
          Top = 32
          Width = 80
          Height = 21
          DataField = 'ParCreVar'
          DataSource = DsEquiComIts
          TabOrder = 1
        end
        object DBEdit15: TDBEdit
          Left = 180
          Top = 32
          Width = 80
          Height = 21
          DataField = 'ParCrePos'
          DataSource = DsEquiComIts
          TabOrder = 2
        end
        object DBEdit16: TDBEdit
          Left = 264
          Top = 32
          Width = 80
          Height = 21
          DataField = 'TotCre'
          DataSource = DsEquiComIts
          TabOrder = 3
        end
        object DBEdit3: TDBEdit
          Left = 12
          Top = 76
          Width = 80
          Height = 21
          DataField = 'ParDebFix'
          DataSource = DsEquiComIts
          TabOrder = 4
        end
        object DBEdit4: TDBEdit
          Left = 96
          Top = 76
          Width = 80
          Height = 21
          DataField = 'ParDebVar'
          DataSource = DsEquiComIts
          TabOrder = 5
        end
        object DBEdit17: TDBEdit
          Left = 180
          Top = 76
          Width = 80
          Height = 21
          DataField = 'ParDebPos'
          DataSource = DsEquiComIts
          TabOrder = 6
        end
        object DBEdit18: TDBEdit
          Left = 264
          Top = 76
          Width = 80
          Height = 21
          DataField = 'TotDeb'
          DataSource = DsEquiComIts
          TabOrder = 7
        end
      end
      object Panel19: TPanel
        Left = 1
        Top = 1
        Width = 539
        Height = 106
        Align = alLeft
        BevelOuter = bvNone
        TabOrder = 1
        object Label1: TLabel
          Left = 7
          Top = 8
          Width = 36
          Height = 13
          Caption = 'C'#243'digo:'
          FocusControl = DBEdCodigo
        end
        object Label2: TLabel
          Left = 59
          Top = 8
          Width = 51
          Height = 13
          Caption = 'Descri'#231#227'o:'
          FocusControl = DBEdNome
        end
        object DBEdCodigo: TDBEdit
          Left = 7
          Top = 24
          Width = 48
          Height = 21
          Hint = 'N'#186' do banco'
          TabStop = False
          DataField = 'Codigo'
          DataSource = DsEquiCom
          Font.Charset = DEFAULT_CHARSET
          Font.Color = 8281908
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = [fsBold]
          ParentFont = False
          ParentShowHint = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 0
        end
        object DBEdNome: TDBEdit
          Left = 59
          Top = 24
          Width = 469
          Height = 21
          Hint = 'Nome do banco'
          Color = clWhite
          DataField = 'Nome'
          DataSource = DsEquiCom
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
        end
      end
    end
    object PainelGrades: TPanel
      Left = 1
      Top = 180
      Width = 931
      Height = 285
      Align = alBottom
      TabOrder = 2
      object dmkDBGrid1: TdmkDBGrid
        Left = 1
        Top = 1
        Width = 432
        Height = 283
        Align = alLeft
        Columns = <
          item
            Expanded = False
            FieldName = 'Produto'
            Title.Caption = 'ID'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEMEPROD'
            Title.Caption = 'Descri'#231#227'o da Mercadoria'
            Width = 136
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PerceProd'
            Title.Caption = '% Comiss'#227'o'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TotCre'
            Title.Caption = '% Cr'#233'ditos'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TotDeb'
            Title.Caption = '% D'#233'bitos'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'C'
            Width = 18
            Visible = True
          end>
        Color = clWindow
        DataSource = DsEquiComIts
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        Columns = <
          item
            Expanded = False
            FieldName = 'Produto'
            Title.Caption = 'ID'
            Width = 36
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'NOMEMEPROD'
            Title.Caption = 'Descri'#231#227'o da Mercadoria'
            Width = 136
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'PerceProd'
            Title.Caption = '% Comiss'#227'o'
            Width = 72
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TotCre'
            Title.Caption = '% Cr'#233'ditos'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'TotDeb'
            Title.Caption = '% D'#233'bitos'
            Visible = True
          end
          item
            Expanded = False
            FieldName = 'C'
            Width = 18
            Visible = True
          end>
      end
      object Panel18: TPanel
        Left = 433
        Top = 1
        Width = 497
        Height = 283
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object Splitter1: TSplitter
          Left = 0
          Top = 189
          Width = 497
          Height = 5
          Cursor = crVSplit
          Align = alTop
        end
        object Panel20: TPanel
          Left = 0
          Top = 0
          Width = 497
          Height = 189
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object Panel21: TPanel
            Left = 0
            Top = 0
            Width = 497
            Height = 189
            Align = alClient
            BevelOuter = bvLowered
            TabOrder = 0
            object StaticText3: TStaticText
              Left = 1
              Top = 1
              Width = 495
              Height = 17
              Align = alTop
              Alignment = taCenter
              Caption = 'Cr'#233'ditos'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clHighlight
              Font.Height = -11
              Font.Name = 'MS Sans Serif'
              Font.Style = [fsBold]
              ParentFont = False
              TabOrder = 0
            end
            object dmkDBGrid2: TdmkDBGrid
              Left = 1
              Top = 18
              Width = 495
              Height = 170
              Align = alClient
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Genero'
                  Title.Caption = 'Conta (P)'
                  Width = 50
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECONTA'
                  Title.Caption = 'Descri'#231#227'o da conta (do plano de contas)'
                  Width = 213
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PerceGene'
                  Title.Caption = '% transferir'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECREDDEBRAT'
                  Title.Caption = 'Forma de rateio'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Title.Caption = 'ID'
                  Visible = True
                end>
              Color = clWindow
              DataSource = DsEquiConCre
              Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
              TabOrder = 1
              TitleFont.Charset = DEFAULT_CHARSET
              TitleFont.Color = clWindowText
              TitleFont.Height = -11
              TitleFont.Name = 'MS Sans Serif'
              TitleFont.Style = []
              Columns = <
                item
                  Expanded = False
                  FieldName = 'Genero'
                  Title.Caption = 'Conta (P)'
                  Width = 50
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECONTA'
                  Title.Caption = 'Descri'#231#227'o da conta (do plano de contas)'
                  Width = 213
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'PerceGene'
                  Title.Caption = '% transferir'
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'NOMECREDDEBRAT'
                  Title.Caption = 'Forma de rateio'
                  Width = 150
                  Visible = True
                end
                item
                  Expanded = False
                  FieldName = 'Conta'
                  Title.Caption = 'ID'
                  Visible = True
                end>
            end
          end
        end
        object Panel22: TPanel
          Left = 0
          Top = 194
          Width = 497
          Height = 89
          Align = alClient
          BevelOuter = bvLowered
          TabOrder = 1
          object StaticText4: TStaticText
            Left = 1
            Top = 1
            Width = 495
            Height = 17
            Align = alTop
            Alignment = taCenter
            Caption = 'D'#233'bitos'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clHighlight
            Font.Height = -11
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
            TabOrder = 0
          end
          object dmkDBGrid5: TdmkDBGrid
            Left = 1
            Top = 18
            Width = 495
            Height = 70
            Align = alClient
            Columns = <
              item
                Expanded = False
                FieldName = 'Genero'
                Title.Caption = 'Conta (P)'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECONTA'
                Title.Caption = 'Descri'#231#227'o da conta (do plano de contas)'
                Width = 213
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PerceGene'
                Title.Caption = '% transferir'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECREDDEBRAT'
                Title.Caption = 'Forma de rateio'
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Conta'
                Title.Caption = 'ID'
                Visible = True
              end>
            Color = clWindow
            DataSource = DsEquiConDeb
            Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit]
            TabOrder = 1
            TitleFont.Charset = DEFAULT_CHARSET
            TitleFont.Color = clWindowText
            TitleFont.Height = -11
            TitleFont.Name = 'MS Sans Serif'
            TitleFont.Style = []
            Columns = <
              item
                Expanded = False
                FieldName = 'Genero'
                Title.Caption = 'Conta (P)'
                Width = 50
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECONTA'
                Title.Caption = 'Descri'#231#227'o da conta (do plano de contas)'
                Width = 213
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'PerceGene'
                Title.Caption = '% transferir'
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'NOMECREDDEBRAT'
                Title.Caption = 'Forma de rateio'
                Width = 150
                Visible = True
              end
              item
                Expanded = False
                FieldName = 'Conta'
                Title.Caption = 'ID'
                Visible = True
              end>
          end
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 933
    Height = 48
    Align = alTop
    Caption = '                              Rol de Comiss'#245'es'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 1
    object Image1: TImage
      Left = 226
      Top = 1
      Width = 624
      Height = 46
      Align = alClient
      Transparent = True
      ExplicitLeft = 221
      ExplicitWidth = 622
      ExplicitHeight = 44
    end
    object LaTipo: TdmkLabel
      Left = 850
      Top = 1
      Width = 82
      Height = 46
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 849
      ExplicitTop = 2
      ExplicitHeight = 44
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 225
      Height = 46
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 4
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 46
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 88
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 130
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 172
        Top = 3
        Width = 40
        Height = 40
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsEquiCom: TDataSource
    DataSet = QrEquiCom
    Left = 46
    Top = 11
  end
  object QrEquiCom: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrEquiComBeforeOpen
    AfterOpen = QrEquiComAfterOpen
    BeforeClose = QrEquiComBeforeClose
    AfterScroll = QrEquiComAfterScroll
    SQL.Strings = (
      'SELECT * FROM equicom'
      'WHERE Codigo > 0')
    Left = 18
    Top = 11
    object QrEquiComCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'equicom.Codigo'
    end
    object QrEquiComNome: TWideStringField
      FieldName = 'Nome'
      Origin = 'equicom.Nome'
      Size = 100
    end
  end
  object PMEquiCom: TPopupMenu
    OnPopup = PMEquiComPopup
    Left = 724
    Top = 12
    object Incluinovoroldecomisses1: TMenuItem
      Caption = '&Inclui novo rol de comiss'#245'es'
      OnClick = Crianovogrupo1Click
    end
    object Alteraroldecomissesatual1: TMenuItem
      Caption = '&Altera rol de comiss'#245'es atual'
      Enabled = False
      OnClick = Alteragrupoatual1Click
    end
    object Excluiroldecomissesatual1: TMenuItem
      Caption = '&Exclui rol de comiss'#245'es atual'
      Enabled = False
      OnClick = Excluigrupoatual1Click
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object DuplicaRolAtua1: TMenuItem
      Caption = '&Duplica Rol Atual'
      OnClick = DuplicaRolAtua1Click
    end
  end
  object QrEquiComIts: TmySQLQuery
    Database = Dmod.MyDB
    AfterOpen = QrEquiComItsAfterOpen
    BeforeClose = QrEquiComItsBeforeClose
    AfterScroll = QrEquiComItsAfterScroll
    SQL.Strings = (
      'SELECT ELT(eci.Compoe+1, " ", "S", "?") C, '
      'grg.Nome NOMEMEPROD, eci.*'
      'FROM equicomits eci'
      'LEFT JOIN grades grg ON grg.Codigo=eci.Produto'
      'WHERE eci.Codigo=:P0')
    Left = 102
    Top = 10
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEquiComItsCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'equicomits.Codigo'
      Required = True
    end
    object QrEquiComItsControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'equicomits.Controle'
      Required = True
    end
    object QrEquiComItsProduto: TIntegerField
      FieldName = 'Produto'
      Origin = 'equicomits.Produto'
      Required = True
    end
    object QrEquiComItsNOMEMEPROD: TWideStringField
      FieldName = 'NOMEMEPROD'
      Origin = 'grades.Nome'
      Size = 100
    end
    object QrEquiComItsTotCre: TFloatField
      FieldName = 'TotCre'
      Origin = 'equicomits.TotCre'
      Required = True
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrEquiComItsTotDeb: TFloatField
      FieldName = 'TotDeb'
      Origin = 'equicomits.TotDeb'
      Required = True
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrEquiComItsPerceProd: TFloatField
      FieldName = 'PerceProd'
      Origin = 'equicomits.PerceProd'
      DisplayFormat = '0.000000;-0.000000; '
    end
    object QrEquiComItsParDebFix: TFloatField
      FieldName = 'ParDebFix'
      Origin = 'equicomits.ParDebFix'
      Required = True
      DisplayFormat = '0.000000'
    end
    object QrEquiComItsParDebVar: TFloatField
      FieldName = 'ParDebVar'
      Origin = 'equicomits.ParDebVar'
      Required = True
      DisplayFormat = '0.000000'
    end
    object QrEquiComItsParCreFix: TFloatField
      FieldName = 'ParCreFix'
      Origin = 'equicomits.ParCreFix'
      Required = True
      DisplayFormat = '0.000000'
    end
    object QrEquiComItsParCreVar: TFloatField
      FieldName = 'ParCreVar'
      Origin = 'equicomits.ParCreVar'
      Required = True
      DisplayFormat = '0.000000'
    end
    object QrEquiComItsParDebPos: TFloatField
      FieldName = 'ParDebPos'
      Required = True
      DisplayFormat = '0.000000'
    end
    object QrEquiComItsParCrePos: TFloatField
      FieldName = 'ParCrePos'
      Required = True
      DisplayFormat = '0.000000'
    end
    object QrEquiComItsCompoe: TSmallintField
      FieldName = 'Compoe'
    end
    object QrEquiComItsC: TWideStringField
      FieldName = 'C'
      Size = 1
    end
  end
  object DsEquiComIts: TDataSource
    DataSet = QrEquiComIts
    Left = 130
    Top = 10
  end
  object PMEquiComIts: TPopupMenu
    OnPopup = PMEquiComItsPopup
    Left = 752
    Top = 12
    object Incluiitemdecomisso1: TMenuItem
      Caption = '&Inclui item de comiss'#227'o'
      OnClick = Incluiitemdereceita1Click
    end
    object Alteraitemdecomissoselecionado1: TMenuItem
      Caption = '&Altera item de comiss'#227'o selecionado'
      Enabled = False
      OnClick = Alteraitemdereceitaatual1Click
    end
    object Excluiitemdecomissoselecionado1: TMenuItem
      Caption = '&Exclui item de comiss'#227'o selecionado'
      Enabled = False
      OnClick = Excluiitemdereceitaatual1Click
    end
  end
  object QrProdutos: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT * '
      'FROM grades'
      'WHERE Ativo = 1'
      'AND Codigo NOT IN ('
      '  SELECT Produto'
      '  FROM equicomits'
      '  WHERE Codigo=1 '
      ') '
      'OR Codigo=1'
      'ORDER BY Nome')
    Left = 472
    Top = 12
    object QrProdutosCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrProdutosNome: TWideStringField
      FieldName = 'Nome'
      Size = 100
    end
  end
  object DsProdutos: TDataSource
    DataSet = QrProdutos
    Left = 500
    Top = 12
  end
  object QrSoma: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(Valor) Valor'
      'FROM equicomits'
      'WHERE Codigo=:P0')
    Left = 528
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrSomaValor: TFloatField
      FieldName = 'Valor'
    end
  end
  object frxPrecoCurso: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 39673.632371770800000000
    ReportOptions.LastChange = 39673.632371770800000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <MeuLogoExiste> = True then Picture2.LoadFromFile(<MeuLogoC' +
        'aminho>);  '
      'end.')
    Left = 186
    Top = 10
    Datasets = <
      item
        DataSet = frxDsEquiCom
        DataSetName = 'frxDsEquiCom'
      end
      item
        DataSet = frxDsEquiComIts
        DataSetName = 'frxDsEquiComIts'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 90.708720000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object frxDsEquiComCodigo: TfrxMemoView
          Left = 211.653680000000000000
          Top = 26.456710000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiCom
          DataSetName = 'frxDsEquiCom'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'ROL DE COMISS'#213'ES N'#186' [frxDsEquiCom."Codigo"]')
          ParentFont = False
        end
        object frxDsEquiComNome: TfrxMemoView
          Left = 211.653680000000000000
          Top = 45.354360000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DataField = 'Nome'
          DataSet = frxDsEquiCom
          DataSetName = 'frxDsEquiCom'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEquiCom."Nome"]')
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 94.488250000000000000
          Top = 71.811070000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Nome da mercadoria')
          ParentFont = False
        end
        object Memo6: TfrxMemoView
          Left = 604.724800000000000000
          Top = 71.811070000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% / valor')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          Left = 37.795300000000000000
          Width = 170.078742600000000000
          Height = 68.031498500000000000
          Frame.Width = 0.100000000000000000
          Stretched = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo26: TfrxMemoView
          Left = 211.653567720000000000
          Width = 468.850340000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Memo10: TfrxMemoView
          Left = 510.236550000000000000
          Top = 71.811070000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Tipo comiss'#227'o')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 37.795300000000000000
          Top = 71.811070000000000000
          Width = 56.692950000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Mercad.')
          ParentFont = False
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 170.078850000000000000
        Width = 718.110700000000000000
        DataSet = frxDsEquiComIts
        DataSetName = 'frxDsEquiComIts'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 37.795300000000000000
          Width = 56.692950000000010000
          Height = 18.897650000000000000
          DataField = 'Produto'
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiComIts."Produto"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 94.488250000000000000
          Width = 415.748300000000000000
          Height = 18.897650000000000000
          DataField = 'NOMEMEPROD'
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEquiComIts."NOMEMEPROD"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 604.724800000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'ComisQtd'
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          DisplayFormat.FormatStr = '%2.2n'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiComIts."ComisQtd"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 510.236550000000000000
          Width = 94.488250000000000000
          Height = 18.897650000000000000
          DataField = 'NOMECOMIS'
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEquiComIts."NOMECOMIS"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 34.015770000000000000
        Top = 249.448980000000000000
        Width = 718.110700000000000000
        object Memo9: TfrxMemoView
          Left = 37.795300000000000000
          Width = 642.520100000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
    end
  end
  object frxDsEquiComIts: TfrxDBDataset
    UserName = 'frxDsEquiComIts'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Controle=Controle'
      'Produto=Produto'
      'NOMEMEPROD=NOMEMEPROD'
      'TotCre=TotCre'
      'TotDeb=TotDeb'
      'PerceProd=PerceProd'
      'ParDebFix=ParDebFix'
      'ParDebVar=ParDebVar'
      'ParCreFix=ParCreFix'
      'ParCreVar=ParCreVar'
      'ParDebPos=ParDebPos'
      'ParCrePos=ParCrePos'
      'Compoe=Compoe'
      'C=C')
    DataSet = QrEquiComIts
    BCDToCurrency = False
    Left = 158
    Top = 10
  end
  object frxDsEquiCom: TfrxDBDataset
    UserName = 'frxDsEquiCom'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Codigo=Codigo'
      'Nome=Nome')
    DataSet = QrEquiCom
    BCDToCurrency = False
    Left = 74
    Top = 10
  end
  object QrContas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome, Debito, Credito'
      'FROM contas'
      'WHERE Codigo>0'
      'AND Ativo = 1'
      'ORDER BY Nome')
    Left = 556
    Top = 12
    object QrContasCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrContasNome: TWideStringField
      FieldName = 'Nome'
      Size = 50
    end
    object QrContasDebito: TWideStringField
      FieldName = 'Debito'
      Size = 1
    end
    object QrContasCredito: TWideStringField
      FieldName = 'Credito'
      Size = 1
    end
  end
  object DsContas: TDataSource
    DataSet = QrContas
    Left = 584
    Top = 12
  end
  object PMRateio: TPopupMenu
    Left = 780
    Top = 12
    object Incluicontaderateio1: TMenuItem
      Caption = '&Inclui conta de rateio'
      OnClick = Incluicontaderateio1Click
    end
    object Alterarateioselecionado1: TMenuItem
      Caption = '&Altera rateio selecionado'
      object Crdito1: TMenuItem
        Caption = 'Altera rateio de &Cr'#233'dito'
        OnClick = Crdito1Click
      end
      object Dbito1: TMenuItem
        Caption = 'Altera rateio de &D'#233'bito'
        OnClick = Dbito1Click
      end
    end
    object Excluirateios1: TMenuItem
      Caption = '&Exclui rateio(s)'
      object Crdito2: TMenuItem
        Caption = 'Exclui rateio de &Cr'#233'dito'
        OnClick = Crdito2Click
      end
      object Dbito2: TMenuItem
        Caption = 'Exclui rateio de &D'#233'bito'
        OnClick = Dbito2Click
      end
    end
  end
  object QrTotCom: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(PerceGene) PerceGene '
      'FROM equicontrf'
      'WHERE EhComProf=1'
      'AND Controle=:P0')
    Left = 612
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotComPerceGene: TFloatField
      FieldName = 'PerceGene'
    end
  end
  object QrTotCre: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(PerceGene) PerceGene '
      'FROM equicontrf'
      'WHERE CredDeb=1'
      'AND Controle=:P0')
    Left = 640
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotCrePerceGene: TFloatField
      FieldName = 'PerceGene'
    end
  end
  object QrTotDeb: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(PerceGene) PerceGene '
      'FROM equicontrf'
      'WHERE CredDeb=2'
      'AND Controle=:P0')
    Left = 668
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrTotDebPerceGene: TFloatField
      FieldName = 'PerceGene'
    end
  end
  object QrEquiConCre: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT CONCAT(ELT(ect.CredDeb, '#39'Credito'#39', '#39'D'#233'bito'#39', '#39'? ? ?'#39'), '#39' ' +
        'de '#39', '
      'ELT(ect.Rateio+1,'
      ' "? ? ?", '
      ' "valor pr'#233'-fixado",'
      ' "valor vari'#225'vel",'
      ' "valor p'#243's-fixado",'
      ' "c'#225'culo por turma"'
      ')) NOMECREDDEBRAT,'
      'cta.Nome NOMECONTA, ect.*'
      'FROM equicontrf ect'
      'LEFT JOIN contas cta ON cta.Codigo=ect.Genero'
      ''
      'WHERE ect.Controle=:P0'
      'AND ect.CredDeb=1')
    Left = 387
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEquiConCreNOMECREDDEBRAT: TWideStringField
      FieldName = 'NOMECREDDEBRAT'
      Size = 26
    end
    object QrEquiConCreNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrEquiConCreCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEquiConCreControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEquiConCreConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEquiConCreGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrEquiConCreCredDeb: TSmallintField
      FieldName = 'CredDeb'
      Required = True
    end
    object QrEquiConCrePerceGene: TFloatField
      FieldName = 'PerceGene'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrEquiConCreEhComProf: TSmallintField
      FieldName = 'EhComProf'
      Required = True
    end
    object QrEquiConCreRateio: TSmallintField
      FieldName = 'Rateio'
      Required = True
    end
    object QrEquiConCreCartIdx: TIntegerField
      FieldName = 'CartIdx'
    end
    object QrEquiConCreMinQtde: TFloatField
      FieldName = 'MinQtde'
      Required = True
    end
    object QrEquiConCreGradeComis: TIntegerField
      FieldName = 'GradeComis'
      Required = True
    end
  end
  object DsEquiConCre: TDataSource
    DataSet = QrEquiConCre
    Left = 415
    Top = 172
  end
  object QrEquiConDeb: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT CONCAT(ELT(ect.CredDeb, '#39'Credito'#39', '#39'D'#233'bito'#39', '#39'? ? ?'#39'), '#39' ' +
        'de '#39', '
      'ELT(ect.Rateio+1,'
      ' "? ? ?", '
      ' "valor pr'#233'-fixado",'
      ' "valor vari'#225'vel",'
      ' "valor p'#243's-fixado"'
      ')) NOMECREDDEBRAT,'
      'cta.Nome NOMECONTA, ect.*'
      'FROM equicontrf ect'
      'LEFT JOIN contas cta ON cta.Codigo=ect.Genero'
      ''
      'WHERE ect.Controle=:P0'
      'AND ect.CredDeb=2')
    Left = 472
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEquiConDebNOMECREDDEBRAT: TWideStringField
      FieldName = 'NOMECREDDEBRAT'
      Size = 26
    end
    object QrEquiConDebNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Size = 50
    end
    object QrEquiConDebCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrEquiConDebControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
    object QrEquiConDebConta: TIntegerField
      FieldName = 'Conta'
      Required = True
    end
    object QrEquiConDebGenero: TIntegerField
      FieldName = 'Genero'
      Required = True
    end
    object QrEquiConDebCredDeb: TSmallintField
      FieldName = 'CredDeb'
      Required = True
    end
    object QrEquiConDebPerceGene: TFloatField
      FieldName = 'PerceGene'
      Required = True
      DisplayFormat = '0.0000;-0.0000; '
    end
    object QrEquiConDebEhComProf: TSmallintField
      FieldName = 'EhComProf'
      Required = True
    end
    object QrEquiConDebRateio: TSmallintField
      FieldName = 'Rateio'
      Required = True
    end
    object QrEquiConDebCartIdx: TIntegerField
      FieldName = 'CartIdx'
    end
    object QrEquiConDebMinQtde: TFloatField
      FieldName = 'MinQtde'
      Required = True
    end
    object QrEquiConDebGradeComis: TIntegerField
      FieldName = 'GradeComis'
      Required = True
    end
  end
  object DsEquiConDeb: TDataSource
    DataSet = QrEquiConDeb
    Left = 500
    Top = 172
  end
  object QrPartial: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT SUM(PerceGene) PerceGene '
      'FROM equicontrf'
      'WHERE CredDeb=:P0'
      'AND Rateio=:P1'
      'AND Controle=:P2')
    Left = 696
    Top = 12
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P2'
        ParamType = ptUnknown
      end>
    object QrPartialPerceGene: TFloatField
      FieldName = 'PerceGene'
    end
  end
  object QrEquiConTrf: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      
        'SELECT CONCAT(ELT(ect.CredDeb, '#39'Credito'#39', '#39'D'#233'bito'#39', '#39'? ? ?'#39'), '#39' ' +
        'de '#39', '
      'ELT(ect.Rateio+1,'
      ' "? ? ?", '
      ' "valor pr'#233'-fixado",'
      ' "valor vari'#225'vel",'
      ' "valor p'#243's-fixado",'
      ' "c'#225'culo por turma"'
      ')) NOMECREDDEBRAT,'
      'cta.Nome NOMECONTA, ect.*'
      'FROM equicontrf ect'
      'LEFT JOIN contas cta ON cta.Codigo=ect.Genero'
      ''
      'WHERE ect.Controle=:P0'
      '')
    Left = 557
    Top = 172
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrEquiConTrfNOMECREDDEBRAT: TWideStringField
      FieldName = 'NOMECREDDEBRAT'
      Size = 27
    end
    object QrEquiConTrfNOMECONTA: TWideStringField
      FieldName = 'NOMECONTA'
      Origin = 'contas.Nome'
      Size = 50
    end
    object QrEquiConTrfCodigo: TIntegerField
      FieldName = 'Codigo'
      Origin = 'equicontrf.Codigo'
      Required = True
    end
    object QrEquiConTrfControle: TIntegerField
      FieldName = 'Controle'
      Origin = 'equicontrf.Controle'
      Required = True
    end
    object QrEquiConTrfConta: TIntegerField
      FieldName = 'Conta'
      Origin = 'equicontrf.Conta'
      Required = True
    end
    object QrEquiConTrfGenero: TIntegerField
      FieldName = 'Genero'
      Origin = 'equicontrf.Genero'
      Required = True
    end
    object QrEquiConTrfPerceGene: TFloatField
      FieldName = 'PerceGene'
      Origin = 'equicontrf.PerceGene'
      Required = True
    end
    object QrEquiConTrfCredDeb: TSmallintField
      FieldName = 'CredDeb'
      Origin = 'equicontrf.CredDeb'
      Required = True
    end
    object QrEquiConTrfEhComProf: TSmallintField
      FieldName = 'EhComProf'
      Origin = 'equicontrf.EhComProf'
      Required = True
    end
    object QrEquiConTrfRateio: TSmallintField
      FieldName = 'Rateio'
      Origin = 'equicontrf.Rateio'
      Required = True
    end
    object QrEquiConTrfCartIdx: TSmallintField
      FieldName = 'CartIdx'
      Origin = 'equicontrf.CartIdx'
      Required = True
    end
    object QrEquiConTrfMinQtde: TFloatField
      FieldName = 'MinQtde'
      Origin = 'equicontrf.MinQtde'
      Required = True
    end
  end
  object QrGrades: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT Codigo, Nome'
      'FROM grades'
      'WHERE Ativo = 1'
      'ORDER BY Nome')
    Left = 680
    Top = 180
  end
  object DsGrades: TDataSource
    DataSet = QrGrades
    Left = 708
    Top = 180
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Incluinovoroldecomisses1
    CanIns02 = Incluiitemdecomisso1
    CanIns03 = Incluicontaderateio1
    CanUpd01 = Dbito1
    CanUpd02 = Alteraitemdecomissoselecionado1
    CanUpd03 = Alterarateioselecionado1
    CanDel01 = Dbito2
    CanDel02 = Excluiitemdecomissoselecionado1
    CanDel03 = Excluirateios1
    Left = 260
    Top = 12
  end
  object frxPrecoCurso2: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 42503.356173958300000000
    ReportOptions.LastChange = 42503.358603113400000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 128
    Top = 136
    Datasets = <
      item
        DataSet = frxDsEquiCom
        DataSetName = 'frxDsEquiCom'
      end
      item
        DataSet = frxDsEquiComIts
        DataSetName = 'frxDsEquiComIts'
      end
      item
        DataSet = frxDsEquiConCre
        DataSetName = 'frxDsEquiConCre'
      end
      item
        DataSet = frxDsEquiConDeb
        DataSetName = 'frxDsEquiConDeb'
      end
      item
        DataSet = DModG.frxDsMaster
        DataSetName = 'frxDsMaster'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 10.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object PageHeader1: TfrxPageHeader
        FillType = ftBrush
        Height = 75.590600000000000000
        Top = 18.897650000000000000
        Width = 718.110700000000000000
        object frxDsEquiComCodigo: TfrxMemoView
          Left = 211.653680000000000000
          Top = 26.456710000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiCom
          DataSetName = 'frxDsEquiCom'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            'ROL DE COMISS'#213'ES N'#186' [frxDsEquiCom."Codigo"]')
          ParentFont = False
        end
        object frxDsEquiComNome: TfrxMemoView
          Left = 211.653680000000000000
          Top = 45.354360000000000000
          Width = 468.661720000000000000
          Height = 18.897650000000000000
          DataField = 'Nome'
          DataSet = frxDsEquiCom
          DataSetName = 'frxDsEquiCom'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Memo.UTF8W = (
            '[frxDsEquiCom."Nome"]')
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Left = 211.653567720000000000
          Width = 468.850340000000000000
          Height = 22.000000000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Univers Light Condensed'
          Font.Style = []
          Memo.UTF8W = (
            '[frxDsMaster."Em"]')
          ParentFont = False
        end
        object Picture2: TfrxPictureView
          Left = 41.574830000000000000
          Width = 170.078742600000000000
          Height = 68.031498500000000000
          Frame.Width = 0.100000000000000000
          Stretched = False
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
      end
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 196.535560000000000000
        Width = 718.110700000000000000
        DataSet = frxDsEquiComIts
        DataSetName = 'frxDsEquiComIts'
        RowCount = 0
        object Memo1: TfrxMemoView
          Left = 37.795300000000000000
          Width = 56.692950000000010000
          Height = 18.897650000000000000
          DataField = 'Produto'
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiComIts."Produto"]')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 94.488250000000000000
          Width = 313.700990000000000000
          Height = 18.897650000000000000
          DataField = 'NOMEMEPROD'
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEquiComIts."NOMEMEPROD"]')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 408.189240000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiComIts."PerceProd"]')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 483.779840000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiComIts."TotCre"]')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 559.370440000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiComIts."TotDeb"]')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 634.961040000000000000
          Width = 45.354360000000000000
          Height = 18.897650000000000000
          DataField = 'C'
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEquiComIts."C"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Height = 34.015770000000010000
        Top = 672.756340000000000000
        Width = 718.110700000000000000
        object Memo9: TfrxMemoView
          Left = 37.795300000000000000
          Width = 642.520100000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -9
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'p'#225'gina [Page#] de [TotalPages#]')
          ParentFont = False
        end
      end
      object GroupHeader1: TfrxGroupHeader
        FillType = ftBrush
        Height = 18.897759840000000000
        Top = 154.960730000000000000
        Width = 718.110700000000000000
        Condition = 'frxDsEquiComIts."Controle"'
        object Memo5: TfrxMemoView
          Left = 94.488250000000000000
          Width = 313.700990000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o da Mercadoria')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo6: TfrxMemoView
          Left = 483.779840000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Cr'#233'ditos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo10: TfrxMemoView
          Left = 408.189240000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% Comiss'#227'o')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo4: TfrxMemoView
          Left = 37.795300000000000000
          Width = 56.692950000000010000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'ID')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo29: TfrxMemoView
          Left = 559.370440000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '% D'#233'bitos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo30: TfrxMemoView
          Left = 634.961040000000000000
          Top = 0.000109839999993255
          Width = 45.354311180000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'C')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object GroupFooter1: TfrxGroupFooter
        FillType = ftBrush
        Height = 124.724490000000000000
        Top = 487.559370000000000000
        Width = 718.110700000000000000
        object Memo7: TfrxMemoView
          Left = 37.795300000000000000
          Top = 18.897650000000000000
          Width = 321.259842520000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cr'#233'dito')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo8: TfrxMemoView
          Left = 359.055350000000000000
          Top = 18.897650000000000000
          Width = 321.259842520000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'D'#233'bito')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo12: TfrxMemoView
          Left = 37.795300000000000000
          Top = 37.795299999999940000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Fixo:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo13: TfrxMemoView
          Left = 37.795300000000000000
          Top = 56.692950000000050000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vari'#225'vel:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo14: TfrxMemoView
          Left = 37.795300000000000000
          Top = 75.590599999999940000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#243's-fix.:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo15: TfrxMemoView
          Left = 37.795300000000000000
          Top = 94.488250000000050000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo16: TfrxMemoView
          Left = 188.976500000000000000
          Top = 37.795299999999940000
          Width = 170.078740160000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiComIts."ParCreFix"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo17: TfrxMemoView
          Left = 188.976500000000000000
          Top = 56.692950000000050000
          Width = 170.078740160000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiComIts."ParCreVar"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo18: TfrxMemoView
          Left = 188.976500000000000000
          Top = 75.590599999999940000
          Width = 170.078740160000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiComIts."ParCrePos"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo19: TfrxMemoView
          Left = 188.976500000000000000
          Top = 94.488250000000050000
          Width = 170.078740160000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiComIts."TotCre"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo20: TfrxMemoView
          Left = 359.055350000000000000
          Top = 37.795299999999940000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Fixo:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo21: TfrxMemoView
          Left = 359.055350000000000000
          Top = 56.692950000000050000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'Vari'#225'vel:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo22: TfrxMemoView
          Left = 359.055350000000000000
          Top = 75.590599999999940000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'P'#243's-fix.:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo23: TfrxMemoView
          Left = 359.055350000000000000
          Top = 94.488250000000050000
          Width = 151.181102360000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            'TOTAL:')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo24: TfrxMemoView
          Left = 510.236550000000000000
          Top = 37.795299999999940000
          Width = 170.078740160000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiComIts."ParDebFix"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo25: TfrxMemoView
          Left = 510.236550000000000000
          Top = 56.692950000000050000
          Width = 170.078740160000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiComIts."ParDebVar"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo27: TfrxMemoView
          Left = 510.236550000000000000
          Top = 75.590599999999940000
          Width = 170.078740160000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiComIts."ParDebPos"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo28: TfrxMemoView
          Left = 510.236550000000000000
          Top = 94.488250000000050000
          Width = 170.078740160000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiComIts."TotDeb"]')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo51: TfrxMemoView
          Left = 37.795300000000000000
          Width = 642.519892520000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'TOTAIS')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object DetailData1: TfrxDetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 298.582870000000000000
        Width = 718.110700000000000000
        DataSet = frxDsEquiConCre
        DataSetName = 'frxDsEquiConCre'
        RowCount = 0
        object Memo39: TfrxMemoView
          Left = 37.795300000000000000
          Width = 56.692950000000010000
          Height = 18.897650000000000000
          DataField = 'Genero'
          DataSet = frxDsEquiConCre
          DataSetName = 'frxDsEquiConCre'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiConCre."Genero"]')
          ParentFont = False
        end
        object Memo40: TfrxMemoView
          Left = 94.488250000000000000
          Width = 313.700990000000000000
          Height = 18.897650000000000000
          DataField = 'NOMECONTA'
          DataSet = frxDsEquiConCre
          DataSetName = 'frxDsEquiConCre'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEquiConCre."NOMECONTA"]')
          ParentFont = False
        end
        object Memo41: TfrxMemoView
          Left = 408.189240000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'PerceGene'
          DataSet = frxDsEquiConCre
          DataSetName = 'frxDsEquiConCre'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiConCre."PerceGene"]')
          ParentFont = False
        end
        object Memo42: TfrxMemoView
          Left = 483.779840000000000000
          Width = 196.535560000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiConCre
          DataSetName = 'frxDsEquiConCre'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEquiConCre."NOMECREDDEBRAT"]')
          ParentFont = False
        end
      end
      object DetailData2: TfrxDetailData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 423.307360000000000000
        Width = 718.110700000000000000
        DataSet = frxDsEquiConDeb
        DataSetName = 'frxDsEquiConDeb'
        RowCount = 0
        object Memo43: TfrxMemoView
          Left = 37.795300000000000000
          Width = 56.692950000000010000
          Height = 18.897650000000000000
          DataField = 'Genero'
          DataSet = frxDsEquiConDeb
          DataSetName = 'frxDsEquiConDeb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftLeft]
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiConDeb."Genero"]')
          ParentFont = False
        end
        object Memo44: TfrxMemoView
          Left = 94.488250000000000000
          Width = 313.700990000000000000
          Height = 18.897650000000000000
          DataField = 'NOMECONTA'
          DataSet = frxDsEquiConDeb
          DataSetName = 'frxDsEquiConDeb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEquiConDeb."NOMECONTA"]')
          ParentFont = False
        end
        object Memo45: TfrxMemoView
          Left = 408.189240000000000000
          Width = 75.590600000000000000
          Height = 18.897650000000000000
          DataField = 'PerceGene'
          DataSet = frxDsEquiConDeb
          DataSetName = 'frxDsEquiConDeb'
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Width = 0.100000000000000000
          HAlign = haRight
          Memo.UTF8W = (
            '[frxDsEquiConDeb."PerceGene"]')
          ParentFont = False
        end
        object Memo46: TfrxMemoView
          Left = 483.779840000000000000
          Width = 196.535560000000000000
          Height = 18.897650000000000000
          DataField = 'NOMECREDDEBRAT'
          DataSet = frxDsEquiConDeb
          DataSetName = 'frxDsEquiConDeb'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          Frame.Typ = [ftRight]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '[frxDsEquiConDeb."NOMECREDDEBRAT"]')
          ParentFont = False
        end
      end
      object Header1: TfrxHeader
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 238.110390000000000000
        Width = 718.110700000000000000
        object Memo33: TfrxMemoView
          Left = 37.795300000000000000
          Width = 642.520100000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'Cr'#233'ditos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo35: TfrxMemoView
          Left = 37.795300000000000000
          Top = 18.897650000000000000
          Width = 56.692901180000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta (P)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo36: TfrxMemoView
          Left = 94.488250000000000000
          Top = 18.897650000000000000
          Width = 313.700941180000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o da conta (do plano de contas)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo37: TfrxMemoView
          Left = 408.189240000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '% transferir')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo38: TfrxMemoView
          Left = 483.779840000000000000
          Top = 18.897650000000000000
          Width = 196.535511180000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Forma de rateio')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer1: TfrxFooter
        FillType = ftBrush
        Top = 340.157700000000000000
        Width = 718.110700000000000000
      end
      object Header2: TfrxHeader
        FillType = ftBrush
        Height = 37.795300000000000000
        Top = 362.834880000000000000
        Width = 718.110700000000000000
        object Memo34: TfrxMemoView
          Left = 37.795300000000000000
          Width = 642.520100000000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop]
          Frame.Width = 0.100000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            'D'#233'bitos')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo47: TfrxMemoView
          Left = 37.795300000000000000
          Top = 18.897650000000000000
          Width = 56.692901180000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Conta (P)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo48: TfrxMemoView
          Left = 94.488250000000000000
          Top = 18.897650000000000000
          Width = 313.700941180000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Descri'#231#227'o da conta (do plano de contas)')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo49: TfrxMemoView
          Left = 408.189240000000000000
          Top = 18.897650000000000000
          Width = 75.590551180000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            '% transferir')
          ParentFont = False
          VAlign = vaCenter
        end
        object Memo50: TfrxMemoView
          Left = 483.779840000000000000
          Top = 18.897650000000000000
          Width = 196.535511180000000000
          Height = 18.897650000000000000
          DataSet = frxDsEquiComIts
          DataSetName = 'frxDsEquiComIts'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -11
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          Frame.Typ = [ftRight, ftBottom]
          Frame.Width = 0.100000000000000000
          Memo.UTF8W = (
            'Forma de rateio')
          ParentFont = False
          VAlign = vaCenter
        end
      end
      object Footer2: TfrxFooter
        FillType = ftBrush
        Top = 464.882190000000000000
        Width = 718.110700000000000000
      end
    end
  end
  object frxDsEquiConDeb: TfrxDBDataset
    UserName = 'frxDsEquiConDeb'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMECREDDEBRAT=NOMECREDDEBRAT'
      'NOMECONTA=NOMECONTA'
      'Codigo=Codigo'
      'Controle=Controle'
      'Conta=Conta'
      'Genero=Genero'
      'CredDeb=CredDeb'
      'PerceGene=PerceGene'
      'EhComProf=EhComProf'
      'Rateio=Rateio'
      'CartIdx=CartIdx'
      'MinQtde=MinQtde'
      'GradeComis=GradeComis')
    DataSet = QrEquiConDeb
    BCDToCurrency = False
    Left = 528
    Top = 172
  end
  object frxDsEquiConCre: TfrxDBDataset
    UserName = 'frxDsEquiConCre'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMECREDDEBRAT=NOMECREDDEBRAT'
      'NOMECONTA=NOMECONTA'
      'Codigo=Codigo'
      'Controle=Controle'
      'Conta=Conta'
      'Genero=Genero'
      'CredDeb=CredDeb'
      'PerceGene=PerceGene'
      'EhComProf=EhComProf'
      'Rateio=Rateio'
      'CartIdx=CartIdx'
      'MinQtde=MinQtde'
      'GradeComis=GradeComis')
    DataSet = QrEquiConCre
    BCDToCurrency = False
    Left = 443
    Top = 172
  end
end
