object FmMoviVK: TFmMoviVK
  Left = 339
  Top = 185
  Caption = 'PRD-VENDA-003 :: Venda de kits'
  ClientHeight = 539
  ClientWidth = 1131
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 120
  TextHeight = 16
  object PainelConfirma: TPanel
    Left = 0
    Top = 480
    Width = 1131
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alBottom
    TabOrder = 1
    object Panel2: TPanel
      Left = 804
      Top = 1
      Width = 326
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      BevelOuter = bvNone
      TabOrder = 0
      object BtSaida: TBitBtn
        Tag = 13
        Left = 207
        Top = 4
        Width = 111
        Height = 49
        Cursor = crHandPoint
        Hint = 'Sai da janela atual'
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Sa'#237'da'
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtSaidaClick
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1131
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = 'Venda de kits'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 2
    object Image1: TImage
      Left = 1
      Top = 1
      Width = 1129
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 59
    Width = 1131
    Height = 421
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    TabOrder = 0
    object DBGrid2: TDBGrid
      Left = 1
      Top = 41
      Width = 1129
      Height = 293
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      DataSource = DsGradeKit
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
      TabOrder = 0
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -14
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
      OnKeyDown = DBGrid2KeyDown
      Columns = <
        item
          Expanded = False
          FieldName = 'NOMEGRADE'
          Title.Caption = 'Grade'
          Width = 185
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMECOR'
          Title.Caption = 'Cor'
          Width = 120
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'NOMETAM'
          Title.Caption = 'Tamanho'
          Width = 100
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'POSIq'
          Title.Caption = 'Quantidade'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'PRECO'
          ReadOnly = True
          Title.Caption = 'PRE'#199'O'
          Width = 65
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TOTAL'
          ReadOnly = True
          Visible = True
        end>
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 1129
      Height = 40
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 1
      object Label6: TLabel
        Left = 7
        Top = 12
        Width = 17
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Kit:'
      end
      object Label1: TLabel
        Left = 855
        Top = 12
        Width = 47
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'TOTAL:'
      end
      object Label2: TLabel
        Left = 658
        Top = 12
        Width = 34
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Frete:'
      end
      object dmkDBEdit5: TdmkDBEdit
        Left = 32
        Top = 6
        Width = 622
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'Nome'
        DataSource = FmMoviV.DsMoviVK
        Enabled = False
        TabOrder = 0
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit1: TdmkDBEdit
        Left = 907
        Top = 6
        Width = 149
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'TOT'
        DataSource = DsTot
        TabOrder = 1
        UpdType = utYes
        Alignment = taLeftJustify
      end
      object dmkDBEdit2: TdmkDBEdit
        Left = 699
        Top = 6
        Width = 149
        Height = 21
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        DataField = 'FRETE'
        DataSource = FmMoviV.DsMoviVK
        TabOrder = 2
        UpdType = utYes
        Alignment = taLeftJustify
      end
    end
    object PnEdita: TPanel
      Left = 1
      Top = 354
      Width = 1129
      Height = 66
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 2
      object Label16: TLabel
        Left = 887
        Top = 6
        Width = 34
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Total:'
      end
      object Label15: TLabel
        Left = 756
        Top = 6
        Width = 73
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Quantidade:'
      end
      object Label14: TLabel
        Left = 571
        Top = 6
        Width = 61
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tamanho:'
      end
      object Label13: TLabel
        Left = 377
        Top = 6
        Width = 24
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cor:'
      end
      object Label12: TLabel
        Left = 9
        Top = 6
        Width = 72
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Mercadoria:'
      end
      object CBProd: TdmkDBLookupComboBox
        Left = 9
        Top = 26
        Width = 357
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Codigo'
        ListField = 'Nome'
        ListSource = DsGrades
        TabOrder = 0
        OnEnter = CBProdEnter
        dmkEditCB = EdProd
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdProd: TdmkEditCB
        Left = 154
        Top = 26
        Width = 69
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 1
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdProdChange
        DBLookupComboBox = CBProd
        IgnoraDBLookupComboBox = False
      end
      object CBCor: TdmkDBLookupComboBox
        Left = 377
        Top = 26
        Width = 178
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Cor'
        ListField = 'NOMECOR'
        ListSource = DsGradesCors
        TabOrder = 2
        dmkEditCB = EdCor
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdCor: TdmkEditCB
        Left = 453
        Top = 26
        Width = 43
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 3
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdCorChange
        DBLookupComboBox = CBCor
        IgnoraDBLookupComboBox = False
      end
      object CBTam: TdmkDBLookupComboBox
        Left = 567
        Top = 26
        Width = 179
        Height = 24
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        KeyField = 'Tam'
        ListField = 'NOMETAM'
        ListSource = DsGradesTams
        TabOrder = 4
        dmkEditCB = EdTam
        UpdType = utYes
        LocF7SQLMasc = '$#'
      end
      object EdTam: TdmkEditCB
        Left = 647
        Top = 26
        Width = 43
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 5
        Visible = False
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ValMin = '-2147483647'
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
        OnChange = EdTamChange
        DBLookupComboBox = CBTam
        IgnoraDBLookupComboBox = False
      end
      object EdProdQtd: TdmkEdit
        Left = 756
        Top = 26
        Width = 123
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 6
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 1
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,0'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
        OnChange = EdProdQtdChange
      end
      object EdProdTot: TdmkEdit
        Left = 887
        Top = 26
        Width = 123
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        Enabled = False
        ReadOnly = True
        TabOrder = 7
        FormatType = dmktfDouble
        MskType = fmtNone
        DecimalSize = 2
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0,00'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0.000000000000000000
        ValWarn = False
      end
      object BtConfItem: TBitBtn
        Tag = 14
        Left = 1019
        Top = 6
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 8
        OnClick = BtConfItemClick
      end
      object BtDesItems: TBitBtn
        Tag = 15
        Left = 1071
        Top = 6
        Width = 49
        Height = 49
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        ParentShowHint = False
        ShowHint = True
        TabOrder = 9
        OnClick = BtDesItemsClick
      end
    end
    object StaticText2: TStaticText
      Left = 1
      Top = 334
      Width = 1129
      Height = 20
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      Alignment = taCenter
      BorderStyle = sbsSunken
      Caption = 
        'Ctrl + Delete para excluir - Enter para editar - F4 para adicion' +
        'ar item'
      TabOrder = 3
      ExplicitWidth = 400
    end
  end
  object QrGradeKit: TmySQLQuery
    Database = Dmod.MyDB
    AfterScroll = QrGradeKitAfterScroll
    OnCalcFields = QrGradeKitCalcFields
    SQL.Strings = (
      'SELECT pro.Codigo, pro.Cor, pro.Tam,'
      'mom.Controle, mom.Conta,  mom.Grade,'
      'mom.DataPedi, mom.Kit, mom.Qtd,'
      'gra.Nome NOMEGRADE, cor.Nome NOMECOR, '
      'tam.Nome NOMETAM, pro.Controle Produto,'
      'mom.Ven, grk.Frete, mok.Qtd QTDKIT'
      'FROM movim mom'
      'LEFT JOIN grades gra ON gra.Codigo = mom.Grade'
      'LEFT JOIN tamanhos tam ON tam.Codigo = mom.Tam'
      'LEFT JOIN cores cor ON cor.Codigo = mom.Cor'
      'LEFT JOIN movivk mok ON mok.Controle = mom.Kit'
      'LEFT JOIN gradek grk ON grk.Codigo = mok.GradeK'
      'LEFT JOIN produtos pro ON pro.Codigo = mom.Grade '
      '    AND pro.Cor = mom.Cor '
      '    AND pro.Tam = mom.Tam'
      'WHERE mom.Controle=:P0'
      'AND mom.Kit=:P1')
    Left = 16
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrGradeKitControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrGradeKitConta: TIntegerField
      FieldName = 'Conta'
    end
    object QrGradeKitGrade: TIntegerField
      FieldName = 'Grade'
    end
    object QrGradeKitDataPedi: TDateField
      FieldName = 'DataPedi'
    end
    object QrGradeKitKit: TIntegerField
      FieldName = 'Kit'
    end
    object QrGradeKitCor: TIntegerField
      FieldName = 'Cor'
    end
    object QrGradeKitTam: TIntegerField
      FieldName = 'Tam'
    end
    object QrGradeKitQtd: TFloatField
      FieldName = 'Qtd'
    end
    object QrGradeKitNOMEGRADE: TWideStringField
      FieldName = 'NOMEGRADE'
      Size = 30
    end
    object QrGradeKitNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Size = 30
    end
    object QrGradeKitTOTAL: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrGradeKitPRECO: TFloatField
      FieldKind = fkCalculated
      FieldName = 'PRECO'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrGradeKitNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Size = 30
    end
    object QrGradeKitProduto: TIntegerField
      FieldName = 'Produto'
      Required = True
    end
    object QrGradeKitPOSIq: TFloatField
      FieldKind = fkCalculated
      FieldName = 'POSIq'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
    object QrGradeKitVen: TFloatField
      FieldName = 'Ven'
    end
    object QrGradeKitFrete: TFloatField
      FieldName = 'Frete'
    end
    object QrGradeKitQTDKIT: TIntegerField
      FieldName = 'QTDKIT'
    end
    object QrGradeKitCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
  end
  object DsGradeKit: TDataSource
    DataSet = QrGradeKit
    Left = 44
    Top = 9
  end
  object QrTot: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrTotCalcFields
    SQL.Strings = (
      'SELECT SUM(Ven) TOTAL'
      'FROM movim'
      'WHERE Controle=:P0'
      'AND Kit=:P1')
    Left = 72
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end
      item
        DataType = ftUnknown
        Name = 'P1'
        ParamType = ptUnknown
      end>
    object QrTotTOTAL: TFloatField
      FieldName = 'TOTAL'
      DisplayFormat = '#,###,##0.00'
    end
    object QrTotTOT: TFloatField
      FieldKind = fkCalculated
      FieldName = 'TOT'
      DisplayFormat = '#,###,##0.00'
      Calculated = True
    end
  end
  object DsTot: TDataSource
    DataSet = QrTot
    Left = 100
    Top = 9
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 128
    Top = 9
  end
  object QrGrades: TmySQLQuery
    Database = Dmod.MyDB
    BeforeClose = QrGradesBeforeClose
    AfterScroll = QrGradesAfterScroll
    SQL.Strings = (
      'SELECT DISTINCT pro.Codigo, gra.CodUsu, gra.Nome'
      'FROM produtos pro'
      'LEFT JOIN grades gra ON gra.Codigo=pro.Codigo'
      'WHERE pro.Ativo = 1'
      'ORDER BY Nome')
    Left = 156
    Top = 9
    object QrGradesCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrGradesNome: TWideStringField
      FieldName = 'Nome'
      Size = 30
    end
    object QrGradesCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
  end
  object DsGrades: TDataSource
    DataSet = QrGrades
    Left = 184
    Top = 9
  end
  object QrGradesTams: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT tam.Nome NOMETAM, grt.* '
      'FROM gradestams grt'
      'LEFT JOIN tamanhos tam ON tam.Codigo=grt.Tam'
      'WHERE grt.Codigo =:P0'
      'ORDER BY Controle')
    Left = 212
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesTamsTam: TIntegerField
      FieldName = 'Tam'
      Origin = 'gradestams.Tam'
    end
    object QrGradesTamsNOMETAM: TWideStringField
      FieldName = 'NOMETAM'
      Origin = 'tamanhos.Nome'
      Size = 5
    end
  end
  object DsGradesTams: TDataSource
    DataSet = QrGradesTams
    Left = 240
    Top = 9
  end
  object QrGradesCors: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT cor.Nome NOMECOR, pro.Cor'
      'FROM produtos pro'
      'LEFT JOIN cores cor ON cor.Codigo = pro.Cor'
      'WHERE pro.Codigo=:P0'
      'AND pro.Ativo=1'
      'GROUP BY pro.Cor'
      'ORDER BY NOMECOR'
      '')
    Left = 268
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrGradesCorsNOMECOR: TWideStringField
      FieldName = 'NOMECOR'
      Origin = 'cores.Nome'
      Size = 30
    end
    object QrGradesCorsCor: TIntegerField
      FieldName = 'Cor'
      Origin = 'gradescors.Cor'
      Required = True
    end
  end
  object DsGradesCors: TDataSource
    DataSet = QrGradesCors
    Left = 296
    Top = 9
  end
end
