unit CiclosMovV;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, StdCtrls, Buttons, UnMLAGeral, dmkLabel, DBCtrls, Grids, DBGrids,
  Mask, dmkDBLookupComboBox, dmkEdit, dmkEditCB, Db, mySQLDbTables, dmkGeral,
  Variants, dmkPermissoes, UnDmkEnums;

type
  TFmCiclosMovV = class(TForm)
    PainelTitulo: TPanel;
    Image1: TImage;
    PnItens: TPanel;
    Panel1: TPanel;
    BtConfItem: TBitBtn;
    Panel6: TPanel;
    BitBtn2: TBitBtn;
    DBGProd: TDBGrid;
    GradeA: TStringGrid;
    GradeC: TStringGrid;
    QrGrades: TmySQLQuery;
    QrGradesCodigo: TIntegerField;
    QrGradesNome: TWideStringField;
    DsGrades: TDataSource;
    Panel4: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    SpeedButton6: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    EdCorCod: TdmkEdit;
    EdCorNom: TdmkEdit;
    EdTamCod: TdmkEdit;
    EdTamNom: TdmkEdit;
    EdQtd: TdmkEdit;
    EdPrc: TdmkEdit;
    EdVen: TdmkEdit;
    EdQta: TdmkEdit;
    EdCua: TdmkEdit;
    EdVla: TdmkEdit;
    EdGrade: TdmkEditCB;
    CBGrade: TdmkDBLookupComboBox;
    EdComisFat: TdmkEdit;
    EdComisVal: TdmkEdit;
    EdRolComis: TdmkEditCB;
    Label3: TLabel;
    QrRolComis: TmySQLQuery;
    DsRolComis: TDataSource;
    QrRolComisCodigo: TIntegerField;
    QrRolComisNome: TWideStringField;
    CBRolComis: TdmkDBLookupComboBox;
    QrPerc: TmySQLQuery;
    QrPercPerceProd: TFloatField;
    QrEquiConTrf: TmySQLQuery;
    QrEquiConTrfPerceGene: TFloatField;
    LaStatus: TdmkLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    RGComissao: TRadioGroup;
    GradeTurmas: TDBGrid;
    dmkPermissoes1: TdmkPermissoes;
    GradeX: TStringGrid;
    QrGradesCodUsu: TIntegerField;
    SpeedButton1: TSpeedButton;
    procedure BtSaidaClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton6Click(Sender: TObject);
    procedure BtConfItemClick(Sender: TObject);
    procedure EdGradeChange(Sender: TObject);
    procedure EdQtdEnter(Sender: TObject);
    procedure EdQtdExit(Sender: TObject);
    procedure EdPrcEnter(Sender: TObject);
    procedure EdPrcExit(Sender: TObject);
    procedure EdVenEnter(Sender: TObject);
    procedure EdVenExit(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure GradeAClick(Sender: TObject);
    procedure GradeADrawCell(Sender: TObject; ACol, ARow: Integer;
      Rect: TRect; State: TGridDrawState);
    procedure EdComisFatEnter(Sender: TObject);
    procedure EdComisFatExit(Sender: TObject);
    procedure EdComisValEnter(Sender: TObject);
    procedure EdComisValExit(Sender: TObject);
    procedure EdVenChange(Sender: TObject);
    procedure EdRolComisChange(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
  private
    { Private declarations }
    FQtd, FPrc, FVal: Double;
    FComisFat, FComisVal: Double;
    procedure ReopenMovim(Conta: Integer);
    procedure DefinePercentualComissao;
    procedure ConfiguraComissao();
    procedure ReopenGrades;
  public
    { Public declarations }
    FRolComis, FControle, FSubCtrl, FMotivo, FSubCta: Integer;
    FDataPedi, FDataReal: TDateTime;
    FQrMovim: TmySQLQuery;

  end;

  var
  FmCiclosMovV: TFmCiclosMovV;

implementation

uses Principal, UnInternalConsts, MyDBCheck, Module, UMySQLModule, ModuleProd,
MyVCLSkin, UnFinanceiro, ModuleCiclos, UnMyObjects;

{$R *.DFM}

procedure TFmCiclosMovV.BtSaidaClick(Sender: TObject);
begin
  Close;
end;

procedure TFmCiclosMovV.ConfiguraComissao;
var
  RolComis, Grade: Integer;
begin
  RolComis := Geral.IMV(EdRolComis.Text);
  Grade    := Geral.IMV(EdGrade.Text);
  if (Grade <> 0) and (RolComis <> 0) then
  begin
    Grade := QrGradesCodigo.Value;
    QrEquiConTrf.Close;
    QrEquiConTrf.Params[00].AsInteger := RolComis;
    QrEquiConTrf.Params[01].AsInteger := Grade;
    QrEquiConTrf.Open;
    //
    case QrEquiConTrf.RecordCount of
      0:;///
      1:
      begin
        EdComisFat.ValueVariant := QrEquiConTrfPerceGene.Value;
        DmProd.CalculaComisProd(tcvComisFat,
          RGComissao, EdQtd, EdVen, EdComisFat, EdComisVal);
      end;
      else
        Geral.MB_Aviso('H� mais de uma conta de d�bito para a mercadoria selecionada neste rol de comiss�es!');
    end;
  end;
end;

procedure TFmCiclosMovV.FormActivate(Sender: TObject);
begin
  MyObjects.CorIniComponente;
end;

procedure TFmCiclosMovV.FormResize(Sender: TObject);
begin
  MLAGeral.LoadTextBitmapToPanel(0, 0, Caption, Image1, PainelTitulo, True, 0);
end;

procedure TFmCiclosMovV.FormCreate(Sender: TObject);
begin
  QrRolComis.Open;
  DBGProd.Align := alClient;
  ReopenGrades;
  GradeA.ColWidths[0] := 100;
  FQtd := 0;
  FPrc := 0;
  FVal := 0;
end;

procedure TFmCiclosMovV.SpeedButton1Click(Sender: TObject);
begin
  VAR_CADASTRO := 0;
  //
  FmPrincipal.CadastroEquiCom(EdRolComis.ValueVariant);
  //
  if VAR_CADASTRO <> 0 then
  begin
    UMyMod.SetaCodigoPesquisado(EdRolComis, CBRolComis, DmCiclos.QrEquiCom, VAR_CADASTRO);
    //
    EdRolComis.SetFocus;
  end;
end;

procedure TFmCiclosMovV.SpeedButton6Click(Sender: TObject);
begin
  VAR_COD := 0;
  //
  FmPrincipal.CadastroDeGrades();
  //
  if VAR_COD <> 0 then
  begin
    UMyMod.SetaCodUsuDeCodigo(EdGrade, CBGrade, QrGrades, VAR_CADASTRO);
    //
    EdGrade.SetFocus;
  end;
end;

procedure TFmCiclosMovV.BtConfItemClick(Sender: TObject);
var
  Qta, Qtd, Ven, QtF, VaF, QtdX, ValX, CusX, DifQ, DifV: Double;
  Conta, Grade, Cor, Tam: Integer;
begin
  if LaStatus.Caption = CO_TRAVADO then Exit;
  Qtd := Geral.DMV(EdQtd.Text);
  Ven := Geral.DMV(EdVen.Text);
  Qta := Geral.DMV(EdQta.Text);
  //
  //Controle := QrMoviVControle.Value; -> FControle
  Grade := QrGradesCodigo.Value;
  Cor := Geral.IMV(EdCorCod.Text);
  Tam := Geral.IMV(EdTamCod.Text);
  if (Grade = 0) or (Cor = 0) or (Tam = 0) then
  begin
    Geral.MB_Aviso('� necess�rio informar o produto a cor e o ' +
      'tamanho! Para informar a cor e o tamanho, utilize o mouse para clicar ' +
      'na c�lula correspondente da grade de cores/tamanhos!');
    Exit;
  end;
  DmProd.AtualizaEstoqueMercadoria(Grade, Cor, Tam, True, True, QtdX, ValX, CusX);
  DmProd.QrEstq.Close;
  DmProd.QrEstq.Params[00].AsInteger := Grade;
  DmProd.QrEstq.Params[01].AsInteger := Cor;
  DmProd.QrEstq.Params[02].AsInteger := Tam;
  DmProd.QrEstq.Open;
  if FQrMovim.Locate('Grade;Cor;Tam', VarArrayOf([Grade, Cor, Tam]), []) then
  begin
    DifQ  := FQrMovim.FieldByName('Qtd').AsFloat;
    DifV  := FQrMovim.FieldByName('Val').AsFloat;
    Conta := FQrMovim.FieldByName('Conta').AsInteger;
  end else begin
    DifQ  := 0;
    DifV  := 0;
    Conta := 0;
  end;
  QtF := DmProd.QrEstqEstqQ.Value - DifQ - Qtd;
  VaF := DmProd.QrEstqEstqV.Value - DifV - (CusX * Qtd);
  //if QrMoviVDataReal.Value > 0 then
  if FDataReal > 0 then
  begin
    if Qtd - Qta > QtdX then
    begin
      Geral.MB_Aviso('Venda de mercadoria cancelada.' + sLineBreak +
        'Estoque insuficiente:' + sLineBreak +
        'Venda = ' + Geral.FFT(Qtd, 3, siNegativo) + sLineBreak +
        'Estq. = ' + Geral.FFT(QtdX + Qta, 3, siNegativo) + sLineBreak +
        'Saldo = ' + Geral.FFT(QtdX - Qtd + Qta, 3, siNegativo));
      Screen.Cursor := crDefault;
      Exit;
    end;
  end;

  //
  Dmod.QrUpd.SQL.Clear;
  if LaStatus.SQLType = stIns then
    Conta := UMyMod.BuscaEmLivreY_Def('movim', 'conta', LaStatus.SQLType, Conta);
  //else Conta :=
    //DmProd.LocalizaConta(FControle, EdGrade.Text, EdCorCod.Text, EdTamCod.Text);
  if Conta > 0 then
  begin
    if QtF < 0 then
    begin
      Conta := -1;
      Geral.MB_Aviso('Inclus�o / altera��o cancelada. ' +
        'O estoque (quantidade) ficaria negativo: '+
        Geral.FFT(QtF, 3, siNegativo));
    end;
    if VaF < 0 then
    begin
      Conta := -1;
      Geral.MB_Aviso('Inclus�o / altera��o cancelada. ' +
        'O estoque (valor) ficaria negativo: '+
        Geral.FFT(VaF, 2, siNegativo));
    end;
  end;
  //
  if Conta > 0 then
  begin
    if UMyMod.SQLInsUpd(Dmod.QrUpd, LaStatus.SQLType, 'movim', False, [
      'Motivo', 'Qtd', 'Val', 'Ven', 'Grade', 'Cor', 'Tam', 'Controle',
      'SubCtrl', 'DataPedi', 'DataReal',
      'ComisTip', 'ComisFat', 'ComisVal',
      'SubCta'
    ], ['Conta'], [
      FMotivo, - Qtd, - CusX * Qtd, Ven, Grade, Cor, Tam, FControle,
      FSubCtrl, Geral.FDT(FDataPedi, 1), Geral.FDT(FDataReal, 1),
      RGComissao.ItemIndex, EdComisFat.ValueVariant, EdComisVal.ValueVariant,
      FSubCta
    ], [Conta], True) then
    begin
      ReopenMovim(Conta);
      EdCorCod.Text := '';
      EdCorNom.Text := '';
      EdTamCod.Text := '';
      EdTamNom.Text := '';
      //AtualizaGradeA(Grade);
      DmProd.AtualizaGradeA(Grade, FControle, FSubctrl, FSubCta, GradeA);
      DmProd.AtualizaGradeX(Grade, GradeX);
      EdQtd.Text := '';
      EdPrc.Text := '';
      EdVen.Text := '';
      //RGComissao.ItemIndex := 0;
      EdComisFat.Text := '';
      EdComisVal.Text := '';
      DmCiclos.CalculaVenComDia(FControle, FSubCta, FRolComis);
      DmProd.AtualizaEstoqueMercadoria(Grade, Cor, Tam,
        FDataReal > 1, True, QtdX, ValX, CusX);
    end;
  end;
  // Fazer no form que chama
  //AtualizaValores(QrMoviVCodigo.Value, Conta, QrPagtosControle.Value);
  EdGrade.SetFocus;
end;

procedure TFmCiclosMovV.ReopenGrades;
begin
  QrGrades.Close;
  QrGrades.Params[0].AsInteger := Dmod.QrControleCiclGradeD.Value;
  QrGrades.Open;
end;

procedure TFmCiclosMovV.ReopenMovim(Conta: Integer);
begin
  FQrMovim.Close;
  FQrMovim.Params[0].AsInteger := FControle;
  FQrMovim.Open;
  //
  if Conta > 0 then FQrMovim.Locate('Conta', Conta, []);
end;

procedure TFmCiclosMovV.EdGradeChange(Sender: TObject);
begin
  DmProd.ConfigGrades(QrGradesCodigo.Value, FControle, FSubCtrl, FSubCta,
    GradeA, GradeC, GradeX);
  DmProd.LimpaEdits(EdTamCod, EdCorCod, EdTamNom, EdCorNom);
  ConfiguraComissao();
end;

procedure TFmCiclosMovV.EdQtdEnter(Sender: TObject);
begin
  FQtd := Geral.DMV(EdQtd.Text);
end;

procedure TFmCiclosMovV.EdQtdExit(Sender: TObject);
begin
  if Geral.DMV(EdQtd.Text) <> FQtd then
    DmProd.CalculaPreco(tcvQtd, EdQtd, EdPrc, EdVen,
      RGComissao, EdComisFat, EdComisVal);
end;

procedure TFmCiclosMovV.EdRolComisChange(Sender: TObject);
begin
  ConfiguraComissao();
end;

procedure TFmCiclosMovV.EdPrcEnter(Sender: TObject);
begin
  FPrc := Geral.DMV(EdPrc.Text);
end;

procedure TFmCiclosMovV.EdPrcExit(Sender: TObject);
begin
  if Geral.DMV(EdPrc.Text) <> FPrc then
    DmProd.CalculaPreco(tcvQtd, EdQtd, EdPrc, EdVen,
    RGComissao, EdComisFat, EdComisVal);
end;

procedure TFmCiclosMovV.EdVenEnter(Sender: TObject);
begin
  FVal := Geral.DMV(EdVen.Text);
end;

procedure TFmCiclosMovV.EdVenExit(Sender: TObject);
begin
  if Geral.DMV(EdVen.Text) <> FVal then
    DmProd.CalculaPreco(tcvVal, EdQtd, EdPrc, EdVen,
    RGComissao, EdComisFat, EdComisVal);
end;

procedure TFmCiclosMovV.BitBtn2Click(Sender: TObject);
begin
  Close;
end;

procedure TFmCiclosMovV.GradeAClick(Sender: TObject);
var
  Grade, Cor, Tam: Integer;
begin
  Grade := QrGradesCodigo.Value;
  Cor   := Geral.IMV(EdCorCod.Text);
  Tam   := Geral.IMV(EdTamCod.Text);
  //
  DmProd.SelecionaItemGradeA(Grade, Cor, Tam, GradeA, GradeC, GradeX,
    EdTamCod, EdCorCod, EdTamNom, EdCorNom,
    EdQtd, EdQta, EdCua, EdVla, EdVen, EdPrc,
    LaStatus, FQrMovim, BtConfItem, FRolComis,
    RGComissao, EdComisFat, EdComisVal);
  //
  DefinePercentualComissao;
end;

procedure TFmCiclosMovV.GradeADrawCell(Sender: TObject; ACol,
  ARow: Integer; Rect: TRect; State: TGridDrawState);
begin
  if (ACol <> 0) and (ARow <> 0) then
    MeuVCLSkin.DrawGrid3(GradeA, Rect, 1, Geral.IMV(GradeA.Cells[ACol, ARow]));
end;

//colocar pre�o em consigna��o

procedure TFmCiclosMovV.EdComisFatEnter(Sender: TObject);
begin
  FComisFat := EdComisFat.ValueVariant;
end;

procedure TFmCiclosMovV.EdComisFatExit(Sender: TObject);
begin
  if EdComisFat.ValueVariant <> FComisFat then
  DmProd.CalculaComisProd(tcvComisFat,
    RGComissao, EdQtd, EdVen, EdComisFat, EdComisVal);
end;

procedure TFmCiclosMovV.EdComisValEnter(Sender: TObject);
begin
  FComisVal := EdComisVal.ValueVariant;
end;

procedure TFmCiclosMovV.EdComisValExit(Sender: TObject);
begin
  if EdComisVal.ValueVariant <> FComisVal then
  DmProd.CalculaComisProd(tcvComisVal,
    RGComissao, EdQtd, EdVen, EdComisFat, EdComisVal);
end;

procedure TFmCiclosMovV.EdVenChange(Sender: TObject);
begin
  DmProd.CalculaComisProd(tcvComisFat,
    RGComissao, EdQtd, EdVen, EdComisFat, EdComisVal);
end;

procedure TFmCiclosMovV.DefinePercentualComissao;
begin
  QrPerc.Close;
  QrPerc.Params[0].AsInteger := QrRolComisCodigo.Value;
  QrPerc.Params[1].AsInteger := QrGradesCodigo.Value;
  QrPerc.Open;
  //
  EdComisFat.ValueVariant := QrPercPerceProd.Value;
  DmProd.CalculaComisProd(tcvComisFat,
    RGComissao, EdQtd, EdVen, EdComisFat, EdComisVal);
end;

end.
