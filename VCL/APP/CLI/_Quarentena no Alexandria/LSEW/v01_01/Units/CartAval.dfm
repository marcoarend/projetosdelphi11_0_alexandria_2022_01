object FmCartAval: TFmCartAval
  Left = 368
  Top = 194
  ActiveControl = SbImprime
  Caption = 'CAD-CARTA-001 :: Carta de avalia'#231#227'o'
  ClientHeight = 674
  ClientWidth = 1001
  Color = clBtnFace
  Constraints.MinHeight = 320
  Constraints.MinWidth = 788
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -14
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  PixelsPerInch = 120
  TextHeight = 16
  object PainelEdita: TPanel
    Left = 0
    Top = 59
    Width = 1001
    Height = 615
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clBtnShadow
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Visible = False
    ExplicitHeight = 581
    object Label9: TLabel
      Left = 21
      Top = 12
      Width = 47
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'C'#243'digo:'
    end
    object Label10: TLabel
      Left = 116
      Top = 7
      Width = 37
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Aluno:'
    end
    object LaTexto: TLabel
      Left = 21
      Top = 135
      Width = 37
      Height = 16
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Texto:'
    end
    object SBCliente: TSpeedButton
      Left = 603
      Top = 31
      Width = 25
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '...'
      OnClick = SBClienteClick
    end
    object Label18: TLabel
      Left = 630
      Top = 33
      Width = 74
      Height = 20
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Expira'#231#227'o:'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object LaDataExp2: TLabel
      Left = 716
      Top = 32
      Width = 91
      Height = 20
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = '24/05/2010'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -17
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object PainelConfirma: TPanel
      Left = 1
      Top = 555
      Width = 999
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 7
      ExplicitTop = 521
      object BtConfirma: TBitBtn
        Tag = 14
        Left = 10
        Top = 3
        Width = 111
        Height = 50
        Cursor = crHandPoint
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = '&Confirma'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'MS Sans Serif'
        Font.Style = []
        NumGlyphs = 2
        ParentFont = False
        ParentShowHint = False
        ShowHint = True
        TabOrder = 0
        OnClick = BtConfirmaClick
      end
      object Panel1: TPanel
        Left = 865
        Top = 1
        Width = 133
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        TabOrder = 1
        object BtDesiste: TBitBtn
          Tag = 15
          Left = 9
          Top = 2
          Width = 110
          Height = 50
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Desiste'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'MS Sans Serif'
          Font.Style = []
          NumGlyphs = 2
          ParentFont = False
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = BtDesisteClick
        end
      end
    end
    object EdCodigo: TdmkEdit
      Left = 21
      Top = 31
      Width = 87
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      Color = clInactiveCaption
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBackground
      Font.Height = -15
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      TabOrder = 0
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Codigo'
      UpdCampo = 'Codigo'
      UpdType = utInc
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object RGStatus: TdmkRadioGroup
      Left = 21
      Top = 65
      Width = 217
      Height = 63
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Caption = 'Status'
      Columns = 2
      Items.Strings = (
        'Aprovado '
        'Reprovado')
      TabOrder = 4
      OnClick = RGStatusClick
      QryCampo = 'Status'
      UpdCampo = 'Status'
      UpdType = utYes
      OldValor = 0
    end
    object CBEntidade: TdmkDBLookupComboBox
      Left = 187
      Top = 31
      Width = 414
      Height = 24
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      KeyField = 'Codigo'
      ListField = 'NOMEENTI'
      ListSource = DsEnti
      TabOrder = 2
      dmkEditCB = EdEntidade
      QryCampo = 'Enti'
      UpdType = utYes
      LocF7SQLMasc = '$#'
    end
    object EdEntidade: TdmkEditCB
      Left = 116
      Top = 31
      Width = 69
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 1
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ValMin = '-2147483647'
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'Enti'
      UpdCampo = 'Enti'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
      OnChange = EdEntidadeChange
      DBLookupComboBox = CBEntidade
      IgnoraDBLookupComboBox = False
    end
    object MeTexto: TdmkMemo
      Left = 21
      Top = 158
      Width = 788
      Height = 109
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Lines.Strings = (
        'dmkMemo1')
      TabOrder = 6
      QryCampo = 'Texto'
      UpdCampo = 'Texto'
      UpdType = utYes
    end
    object EdDepto: TdmkEdit
      Left = 816
      Top = 31
      Width = 98
      Height = 26
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Alignment = taRightJustify
      TabOrder = 3
      Visible = False
      FormatType = dmktfInteger
      MskType = fmtNone
      DecimalSize = 0
      LeftZeros = 0
      NoEnterToTab = False
      NoForceUppercase = False
      ForceNextYear = False
      DataFormat = dmkdfShort
      HoraFormat = dmkhfShort
      Texto = '0'
      QryCampo = 'GradeD'
      UpdCampo = 'GradeD'
      UpdType = utYes
      Obrigatorio = False
      PermiteNulo = False
      ValueVariant = 0
      ValWarn = False
    end
    object PnDados: TPanel
      Left = 12
      Top = 274
      Width = 810
      Height = 180
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      BevelOuter = bvNone
      TabOrder = 5
      object Label5: TLabel
        Left = 9
        Top = 10
        Width = 49
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Modelo:'
      end
      object Label13: TLabel
        Left = 9
        Top = 64
        Width = 46
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tecido:'
      end
      object Label14: TLabel
        Left = 693
        Top = 64
        Width = 73
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Quantidade:'
      end
      object Label15: TLabel
        Left = 9
        Top = 119
        Width = 24
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Cor:'
      end
      object Label16: TLabel
        Left = 409
        Top = 119
        Width = 58
        Height = 16
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Caption = 'Tamanho'
      end
      object EdModelo: TdmkEdit
        Left = 9
        Top = 30
        Width = 787
        Height = 25
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 0
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Modelo'
        UpdCampo = 'Modelo'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object dmkEdit1: TdmkEdit
        Left = 693
        Top = 84
        Width = 103
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Alignment = taRightJustify
        TabOrder = 2
        FormatType = dmktfInteger
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        Texto = '0'
        QryName = 'Qtde'
        UpdCampo = 'Qtde'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = 0
        ValWarn = False
      end
      object dmkEdit2: TdmkEdit
        Left = 9
        Top = 84
        Width = 674
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 1
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Tecido'
        UpdCampo = 'Tecido'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object dmkEdit3: TdmkEdit
        Left = 9
        Top = 139
        Width = 387
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 3
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Cor'
        UpdCampo = 'Cor'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
      object dmkEdit4: TdmkEdit
        Left = 409
        Top = 139
        Width = 387
        Height = 26
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        TabOrder = 4
        FormatType = dmktfString
        MskType = fmtNone
        DecimalSize = 0
        LeftZeros = 0
        NoEnterToTab = False
        NoForceUppercase = False
        ForceNextYear = False
        DataFormat = dmkdfShort
        HoraFormat = dmkhfShort
        QryCampo = 'Tamanho'
        UpdCampo = 'Tamanho'
        UpdType = utYes
        Obrigatorio = False
        PermiteNulo = False
        ValueVariant = ''
        ValWarn = False
      end
    end
  end
  object PainelDados: TPanel
    Left = 0
    Top = 59
    Width = 1001
    Height = 615
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alClient
    Color = clAppWorkSpace
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    ExplicitHeight = 581
    object PainelData: TPanel
      Left = 1
      Top = 1
      Width = 999
      Height = 520
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alTop
      TabOrder = 1
      ExplicitLeft = 0
      ExplicitTop = 3
      object Panel6: TPanel
        Left = 1
        Top = 1
        Width = 702
        Height = 518
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        ExplicitLeft = 168
        ExplicitTop = 96
        ExplicitWidth = 185
        ExplicitHeight = 41
        object Panel7: TPanel
          Left = 0
          Top = 0
          Width = 702
          Height = 235
          Align = alTop
          TabOrder = 0
          ExplicitLeft = 1
          ExplicitTop = 1
          ExplicitWidth = 997
          object Img4: TImage
            Left = 551
            Top = 118
            Width = 139
            Height = 105
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Stretch = True
            OnClick = Img4Click
          end
          object Img3: TImage
            Left = 406
            Top = 118
            Width = 139
            Height = 105
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Stretch = True
            OnClick = Img3Click
          end
          object Img2: TImage
            Left = 260
            Top = 118
            Width = 139
            Height = 105
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Stretch = True
            OnClick = Img2Click
          end
          object Img1: TImage
            Left = 113
            Top = 118
            Width = 139
            Height = 105
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Stretch = True
            OnClick = Img1Click
          end
          object Label3: TLabel
            Left = 6
            Top = 57
            Width = 72
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Inclu'#237'do por:'
          end
          object Label1: TLabel
            Left = 6
            Top = 9
            Width = 83
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'ID: Pesq. [F4] '
          end
          object Label2: TLabel
            Left = 103
            Top = 9
            Width = 37
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Aluno:'
          end
          object Label4: TLabel
            Left = 512
            Top = 32
            Width = 74
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Expira'#231#227'o:'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -17
            Font.Name = 'MS Sans Serif'
            Font.Style = []
            ParentFont = False
          end
          object LaDataExp: TLabel
            Left = 598
            Top = 31
            Width = 91
            Height = 20
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '24/05/2010'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -17
            Font.Name = 'MS Sans Serif'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object DBRadioGroup1: TDBRadioGroup
            Left = 7
            Top = 118
            Width = 99
            Height = 105
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Status'
            DataField = 'Status'
            DataSource = DsCartAval
            Items.Strings = (
              'Aprovado'
              'Reprovado')
            ParentBackground = True
            TabOrder = 0
            Values.Strings = (
              '0'
              '1')
          end
          object dmkDBEdit3: TdmkDBEdit
            Left = 6
            Top = 78
            Width = 684
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMECAD2'
            DataSource = DsCartAval
            TabOrder = 1
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit1: TdmkDBEdit
            Left = 7
            Top = 27
            Width = 91
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Codigo'
            DataSource = DsCartAval
            TabOrder = 2
            OnDblClick = dmkDBEdit1DblClick
            OnKeyDown = dmkDBEdit1KeyDown
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit2: TdmkDBEdit
            Left = 103
            Top = 27
            Width = 404
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'NOMEENTI'
            DataSource = DsCartAval
            TabOrder = 3
            UpdType = utYes
            Alignment = taLeftJustify
          end
        end
        object PnAprov: TPanel
          Left = 0
          Top = 235
          Width = 702
          Height = 155
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          ExplicitLeft = -7
          ExplicitTop = 232
          object Label6: TLabel
            Left = 6
            Top = 5
            Width = 49
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Modelo:'
          end
          object Label7: TLabel
            Left = 6
            Top = 54
            Width = 46
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tecido:'
          end
          object Label8: TLabel
            Left = 586
            Top = 54
            Width = 73
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Quantidade:'
          end
          object Label12: TLabel
            Left = 352
            Top = 104
            Width = 61
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Tamanho:'
          end
          object Label11: TLabel
            Left = 6
            Top = 104
            Width = 24
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Cor:'
          end
          object DBEdModelo: TdmkDBEdit
            Left = 6
            Top = 24
            Width = 683
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Modelo'
            DataSource = DsCartAval
            TabOrder = 0
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit5: TdmkDBEdit
            Left = 6
            Top = 73
            Width = 572
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Tecido'
            DataSource = DsCartAval
            TabOrder = 1
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit6: TdmkDBEdit
            Left = 586
            Top = 73
            Width = 103
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Qtde'
            DataSource = DsCartAval
            TabOrder = 2
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit8: TdmkDBEdit
            Left = 352
            Top = 124
            Width = 338
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Tamanho'
            DataSource = DsCartAval
            TabOrder = 3
            UpdType = utYes
            Alignment = taLeftJustify
          end
          object dmkDBEdit7: TdmkDBEdit
            Left = 6
            Top = 124
            Width = 338
            Height = 24
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Cor'
            DataSource = DsCartAval
            TabOrder = 4
            UpdType = utYes
            Alignment = taLeftJustify
          end
        end
        object PnReprov: TPanel
          Left = 0
          Top = 390
          Width = 702
          Height = 100
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object LaDBTexto: TLabel
            Left = 6
            Top = 5
            Width = 37
            Height = 16
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = 'Texto:'
          end
          object MeDBTexto: TDBMemo
            Left = 6
            Top = 26
            Width = 684
            Height = 64
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            DataField = 'Texto'
            DataSource = DsCartAval
            ScrollBars = ssVertical
            TabOrder = 0
          end
        end
      end
      object Panel4: TPanel
        Left = 703
        Top = 1
        Width = 295
        Height = 518
        Align = alRight
        Caption = 'Panel4'
        TabOrder = 1
        ExplicitTop = 236
        ExplicitHeight = 283
        object Label17: TLabel
          Left = 1
          Top = 1
          Width = 293
          Height = 19
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alTop
          Alignment = taCenter
          Caption = 'Motivos'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -17
          Font.Name = 'Berlin Sans FB'
          Font.Style = []
          ParentFont = False
          ExplicitWidth = 54
        end
        object DBLookupListBox2: TDBLookupListBox
          Left = 1
          Top = 20
          Width = 293
          Height = 484
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alClient
          KeyField = 'Controle'
          ListField = 'MOTIVO_STR'
          ListSource = DsCartAvalS
          TabOrder = 0
          ExplicitLeft = 41
          ExplicitTop = 78
        end
      end
    end
    object PainelControle: TPanel
      Left = 1
      Top = 555
      Width = 999
      Height = 59
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alBottom
      TabOrder = 0
      ExplicitTop = 521
      object LaRegistro: TdmkLabel
        Left = 213
        Top = 1
        Width = 208
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alClient
        Caption = '[N]: 0'
        UpdType = utYes
        SQLType = stNil
        ExplicitWidth = 31
        ExplicitHeight = 16
      end
      object Panel5: TPanel
        Left = 1
        Top = 1
        Width = 212
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alLeft
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 0
        object SpeedButton4: TBitBtn
          Tag = 4
          Left = 158
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 0
          OnClick = SpeedButton4Click
        end
        object SpeedButton3: TBitBtn
          Tag = 3
          Left = 108
          Top = 5
          Width = 50
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = SpeedButton3Click
        end
        object SpeedButton2: TBitBtn
          Tag = 2
          Left = 59
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = SpeedButton2Click
        end
        object SpeedButton1: TBitBtn
          Tag = 1
          Left = 10
          Top = 5
          Width = 49
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 3
          OnClick = SpeedButton1Click
        end
      end
      object Panel3: TPanel
        Left = 421
        Top = 1
        Width = 577
        Height = 57
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Align = alRight
        BevelOuter = bvNone
        ParentColor = True
        TabOrder = 1
        object Panel2: TPanel
          Left = 443
          Top = 0
          Width = 134
          Height = 57
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Align = alRight
          Alignment = taRightJustify
          BevelOuter = bvNone
          TabOrder = 0
          object BtSaida: TBitBtn
            Tag = 13
            Left = 5
            Top = 5
            Width = 111
            Height = 49
            Cursor = crHandPoint
            Margins.Left = 4
            Margins.Top = 4
            Margins.Right = 4
            Margins.Bottom = 4
            Caption = '&Sa'#237'da'
            NumGlyphs = 2
            ParentShowHint = False
            ShowHint = True
            TabOrder = 0
            OnClick = BtSaidaClick
          end
        end
        object BtCarta: TBitBtn
          Left = 6
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Carta'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 1
          OnClick = BtCartaClick
        end
        object BtMotivo: TBitBtn
          Left = 118
          Top = 5
          Width = 111
          Height = 49
          Cursor = crHandPoint
          Margins.Left = 4
          Margins.Top = 4
          Margins.Right = 4
          Margins.Bottom = 4
          Caption = '&Motivo'
          NumGlyphs = 2
          ParentShowHint = False
          ShowHint = True
          TabOrder = 2
          OnClick = BtMotivoClick
        end
      end
    end
  end
  object PainelTitulo: TPanel
    Left = 0
    Top = 0
    Width = 1001
    Height = 59
    Margins.Left = 4
    Margins.Top = 4
    Margins.Right = 4
    Margins.Bottom = 4
    Align = alTop
    Caption = '                              Cartas de avalia'#231#227'o'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -38
    Font.Name = 'Times New Roman'
    Font.Style = []
    ParentColor = True
    ParentFont = False
    TabOrder = 0
    object LaTipo: TdmkLabel
      Left = 899
      Top = 1
      Width = 101
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alRight
      Alignment = taCenter
      AutoSize = False
      Caption = 'Travado'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8281908
      Font.Height = -18
      Font.Name = 'Times New Roman'
      Font.Style = [fsBold]
      ParentFont = False
      Transparent = True
      UpdType = utYes
      SQLType = stLok
      ExplicitLeft = 898
    end
    object Image1: TImage
      Left = 278
      Top = 1
      Width = 621
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alClient
      Transparent = True
      ExplicitWidth = 620
    end
    object PanelFill2: TPanel
      Left = 1
      Top = 1
      Width = 277
      Height = 57
      Margins.Left = 4
      Margins.Top = 4
      Margins.Right = 4
      Margins.Bottom = 4
      Align = alLeft
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Times New Roman'
      Font.Style = []
      ParentColor = True
      ParentFont = False
      TabOrder = 0
      object SbImprime: TBitBtn
        Tag = 5
        Left = 5
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 0
        OnClick = SbImprimeClick
      end
      object SbNovo: TBitBtn
        Tag = 6
        Left = 57
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        NumGlyphs = 2
        TabOrder = 1
      end
      object SbNumero: TBitBtn
        Tag = 7
        Left = 108
        Top = 4
        Width = 50
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        NumGlyphs = 2
        TabOrder = 2
        OnClick = SbNumeroClick
      end
      object SbNome: TBitBtn
        Tag = 8
        Left = 160
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        NumGlyphs = 2
        TabOrder = 3
        OnClick = SbNomeClick
      end
      object SbQuery: TBitBtn
        Tag = 9
        Left = 212
        Top = 4
        Width = 49
        Height = 49
        Margins.Left = 4
        Margins.Top = 4
        Margins.Right = 4
        Margins.Bottom = 4
        Enabled = False
        NumGlyphs = 2
        TabOrder = 4
        OnClick = SbQueryClick
      end
    end
  end
  object DsCartAval: TDataSource
    DataSet = QrCartAval
    Left = 784
    Top = 133
  end
  object QrCartAval: TmySQLQuery
    Database = Dmod.MyDB
    BeforeOpen = QrCartAvalBeforeOpen
    AfterOpen = QrCartAvalAfterOpen
    AfterScroll = QrCartAvalAfterScroll
    OnCalcFields = QrCartAvalCalcFields
    SQL.Strings = (
      'SELECT ava.*, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTI'
      'FROM cartaval ava'
      'LEFT JOIN entidades ent ON ent.Codigo = ava.Enti'
      'WHERE ava.Codigo > 0')
    Left = 756
    Top = 133
    object QrCartAvalNOMECAD: TWideStringField
      FieldKind = fkLookup
      FieldName = 'NOMECAD'
      LookupDataSet = QrSenhas
      LookupKeyFields = 'Numero'
      LookupResultField = 'NOMEENTI'
      KeyFields = 'UserCad'
      Size = 100
      Lookup = True
    end
    object QrCartAvalNOMECAD2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOMECAD2'
      Size = 100
      Calculated = True
    end
    object QrCartAvalCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartAvalEnti: TIntegerField
      FieldName = 'Enti'
    end
    object QrCartAvalStatus: TIntegerField
      FieldName = 'Status'
    end
    object QrCartAvalLk: TIntegerField
      FieldName = 'Lk'
    end
    object QrCartAvalDataCad: TDateField
      FieldName = 'DataCad'
    end
    object QrCartAvalDataAlt: TDateField
      FieldName = 'DataAlt'
    end
    object QrCartAvalUserCad: TIntegerField
      FieldName = 'UserCad'
    end
    object QrCartAvalUserAlt: TIntegerField
      FieldName = 'UserAlt'
    end
    object QrCartAvalAlterWeb: TSmallintField
      FieldName = 'AlterWeb'
    end
    object QrCartAvalAtivo: TSmallintField
      FieldName = 'Ativo'
    end
    object QrCartAvalTexto: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
    object QrCartAvalFoto1: TWideStringField
      FieldName = 'Foto1'
      Size = 255
    end
    object QrCartAvalFoto2: TWideStringField
      FieldName = 'Foto2'
      Size = 255
    end
    object QrCartAvalFoto3: TWideStringField
      FieldName = 'Foto3'
      Size = 255
    end
    object QrCartAvalFoto4: TWideStringField
      FieldName = 'Foto4'
      Size = 255
    end
    object QrCartAvalNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
    object QrCartAvalGradeD: TIntegerField
      FieldName = 'GradeD'
    end
    object QrCartAvalModelo: TWideStringField
      FieldName = 'Modelo'
      Size = 50
    end
    object QrCartAvalTecido: TWideStringField
      FieldName = 'Tecido'
      Size = 50
    end
    object QrCartAvalCor: TWideStringField
      FieldName = 'Cor'
      Size = 50
    end
    object QrCartAvalTamanho: TWideStringField
      FieldName = 'Tamanho'
      Size = 50
    end
    object QrCartAvalQtde: TIntegerField
      FieldName = 'Qtde'
    end
    object QrCartAvalValTot: TFloatField
      FieldName = 'ValTot'
      DisplayFormat = '#,###,##0.00'
    end
  end
  object dmkPermissoes1: TdmkPermissoes
    CanIns01 = Inclui1
    CanIns02 = MenuItem1
    CanUpd01 = Altera1
    CanDel02 = MenuItem2
    Left = 229
    Top = 9
  end
  object QrEnti: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT ent.Codigo, CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTI'
      'FROM entidades ent'
      'WHERE ent.Cliente1 = "V"'
      'AND ent.Cliente2 = "V"'
      'ORDER BY NOMEENTI')
    Left = 548
    Top = 9
    object QrEntiCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrEntiNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsEnti: TDataSource
    DataSet = QrEnti
    Left = 576
    Top = 9
  end
  object frxDsCarta1: TfrxDBDataset
    UserName = 'frxDsCarta1'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Texto=Texto')
    DataSet = QrCarta1
    BCDToCurrency = False
    Left = 789
    Top = 397
  end
  object frxCartAval: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40143.467221817100000000
    ReportOptions.LastChange = 40150.692094166670000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      
        '  if <VAR_IMGEXISTE>  = True then Picture1.LoadFromFile(<VAR_IMG' +
        '>);  '
      
        '  if <VAR_IMGEXISTE1> = True then Picture2.LoadFromFile(<VAR_IMG' +
        '1>);  '
      
        '  if <VAR_IMGEXISTE2> = True then Picture3.LoadFromFile(<VAR_IMG' +
        '2>);  '
      
        '  if <VAR_IMGEXISTE3> = True then Picture4.LoadFromFile(<VAR_IMG' +
        '3>);  '
      
        '  if <VAR_IMGEXISTE4> = True then Picture5.LoadFromFile(<VAR_IMG' +
        '4>);      '
      '  //'
      '  Memo1.Visible := <VAR_IMGEXISTE1> = True;'
      '  Memo2.Visible := <VAR_IMGEXISTE2> = True;'
      '  Memo3.Visible := <VAR_IMGEXISTE3> = True;'
      '  Memo4.Visible := <VAR_IMGEXISTE4> = True;        '
      'end.')
    OnGetValue = frxCartAvalGetValue
    Left = 761
    Top = 397
    Datasets = <
      item
        DataSet = frxDsAvaEmpresa
        DataSetName = 'frxDsAvaEmpresa'
      end
      item
        DataSet = frxDsCarta1
        DataSetName = 'frxDsCarta1'
      end
      item
        DataSet = frxDsCarta2
        DataSetName = 'frxDsCarta2'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 15.000000000000000000
      RightMargin = 10.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      object MasterData1: TfrxMasterData
        FillType = ftBrush
        Height = 151.181200000000000000
        Top = 147.401670000000000000
        Width = 699.213050000000000000
        DataSet = frxDsCarta1
        DataSetName = 'frxDsCarta1'
        RowCount = 0
        Stretched = True
        object Rich1: TfrxRichView
          Width = 699.213050000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          DataField = 'Texto'
          DataSet = frxDsCarta1
          DataSetName = 'frxDsCarta1'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C205461686F6D613B7D7D0D0A7B5C2A5C67656E657261746F7220
            52696368656432302031302E302E31353036337D5C766965776B696E64345C75
            6331200D0A5C706172645C66305C667331365C7061720D0A7D0D0A00}
        end
        object Picture2: TfrxPictureView
          Top = 18.897650000000000000
          Width = 170.078740160000000000
          Height = 113.385826770000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Picture3: TfrxPictureView
          Left = 175.559060000000000000
          Top = 18.897650000000000000
          Width = 170.078740160000000000
          Height = 113.385826770000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Picture4: TfrxPictureView
          Left = 349.795300000000000000
          Top = 18.897650000000000000
          Width = 170.078740160000000000
          Height = 113.385826770000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Picture5: TfrxPictureView
          Left = 525.354670000000000000
          Top = 18.897650000000000000
          Width = 170.078740160000000000
          Height = 113.385826770000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo1: TfrxMemoView
          Top = 132.283550000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Imagem 1')
          ParentFont = False
        end
        object Memo2: TfrxMemoView
          Left = 175.748031500000000000
          Top = 132.283550000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Imagem 2')
          ParentFont = False
        end
        object Memo3: TfrxMemoView
          Left = 349.984251970000000000
          Top = 132.283550000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Imagem 3')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 525.354670000000000000
          Top = 132.283550000000000000
          Width = 170.078850000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            'Imagem 4')
          ParentFont = False
        end
      end
      object ReportTitle1: TfrxReportTitle
        FillType = ftBrush
        Height = 68.031496060000000000
        Top = 18.897650000000000000
        Width = 699.213050000000000000
        object Picture1: TfrxPictureView
          Width = 170.078740160000000000
          Height = 68.031496060000000000
          HightQuality = False
          Transparent = False
          TransparentColor = clWhite
        end
        object Memo5: TfrxMemoView
          Left = 491.338900000000000000
          Width = 207.874150000000000000
          Height = 37.795300000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Arial'
          Font.Style = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[frxDsAvaEmpresa."Site"]'
            '[frxDsAvaEmpresa."TE1_TXT"]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object MasterData2: TfrxMasterData
        FillType = ftBrush
        Height = 18.897650000000000000
        Top = 321.260050000000000000
        Width = 699.213050000000000000
        AllowSplit = True
        DataSet = frxDsCarta2
        DataSetName = 'frxDsCarta2'
        RowCount = 0
        Stretched = True
        object Rich2: TfrxRichView
          Width = 699.213050000000000000
          Height = 18.897650000000000000
          StretchMode = smMaxHeight
          DataField = 'Texto'
          DataSet = frxDsCarta2
          DataSetName = 'frxDsCarta2'
          GapX = 2.000000000000000000
          GapY = 1.000000000000000000
          RichEdit = {
            7B5C727466315C616E73695C616E7369637067313235325C64656666305C6E6F
            7569636F6D7061745C6465666C616E67313034367B5C666F6E7474626C7B5C66
            305C666E696C205461686F6D613B7D7D0D0A7B5C2A5C67656E657261746F7220
            52696368656432302031302E302E31353036337D5C766965776B696E64345C75
            6331200D0A5C706172645C66305C667331365C7061720D0A7D0D0A00}
        end
      end
    end
  end
  object QrCarta1: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Texto '
      'FROM cartas car'
      'LEFT JOIN cartag cag ON cag.Codigo = car.CartaG'
      'WHERE car.CartaG=:P0'
      'AND car.Pagina=1')
    Left = 366
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCarta1Texto: TWideMemoField
      FieldName = 'Texto'
      Origin = 'cartas.Texto'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object QrCarta2: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT car.Texto '
      'FROM cartas car'
      'LEFT JOIN cartag cag ON cag.Codigo = car.CartaG'
      'WHERE car.CartaG=:P0'
      'AND car.Pagina=2')
    Left = 394
    Top = 9
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object MemoField1: TWideMemoField
      FieldName = 'Texto'
      BlobType = ftWideMemo
      Size = 4
    end
  end
  object frxDsCarta2: TfrxDBDataset
    UserName = 'frxDsCarta2'
    CloseDataSource = False
    FieldAliases.Strings = (
      'Texto=Texto')
    DataSet = QrCarta2
    BCDToCurrency = False
    Left = 821
    Top = 397
  end
  object QrSenhas: TmySQLQuery
    Database = Dmod.MyDB
    SQL.Strings = (
      'SELECT CASE WHEN ent.Tipo=0 THEN ent.RazaoSocial'
      'ELSE ent.Nome END NOMEENTI, sen.* '
      'FROM senhas sen'
      'LEFT JOIN entidades ent ON ent.Codigo = sen.Funcionario ')
    Left = 492
    Top = 9
    object QrSenhasLogin: TWideStringField
      FieldName = 'Login'
      Origin = 'senhas.Login'
      Required = True
      Size = 30
    end
    object QrSenhasNumero: TIntegerField
      FieldName = 'Numero'
      Origin = 'senhas.Numero'
    end
    object QrSenhasSenha: TWideStringField
      FieldName = 'Senha'
      Origin = 'senhas.Senha'
      Size = 30
    end
    object QrSenhasPerfil: TIntegerField
      FieldName = 'Perfil'
      Origin = 'senhas.Perfil'
    end
    object QrSenhasLk: TIntegerField
      FieldName = 'Lk'
      Origin = 'senhas.Lk'
    end
    object QrSenhasFuncionario: TIntegerField
      FieldName = 'Funcionario'
    end
    object QrSenhasNOMEENTI: TWideStringField
      FieldName = 'NOMEENTI'
      Size = 100
    end
  end
  object DsSenhas: TDataSource
    Left = 520
    Top = 9
  end
  object frxVerso: TfrxReport
    Version = '5.5'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = 'Default'
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 40143.467221817100000000
    ReportOptions.LastChange = 40927.414179884260000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      ''
      'end.')
    Left = 733
    Top = 397
    Datasets = <
      item
        DataSet = frxDsAvaEmpresa
        DataSetName = 'frxDsAvaEmpresa'
      end
      item
        DataSet = frxDsCli
        DataSetName = 'frxDsCli'
      end>
    Variables = <>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      PaperWidth = 210.000000000000000000
      PaperHeight = 297.000000000000000000
      PaperSize = 9
      LeftMargin = 25.000000000000000000
      RightMargin = 25.000000000000000000
      TopMargin = 25.000000000000000000
      BottomMargin = 25.000000000000000000
      object Memo50: TfrxMemoView
        Left = 71.543434020000010000
        Top = 434.645950000000000000
        Width = 467.527861000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -19
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsCli."NOME_ENT"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo52: TfrxMemoView
        Top = 434.645669291338600000
        Width = 71.811070000000000000
        Height = 18.897650000000000000
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -11
        Font.Name = 'Arial'
        Font.Style = []
        Memo.UTF8W = (
          'Destinat'#225'rio:')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo1: TfrxMemoView
        Left = 71.433153860000000000
        Top = 454.323130000000000000
        Width = 467.527581020000000000
        Height = 22.677170240000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsCli."E_ALL"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo4: TfrxMemoView
        Left = 71.433153860000000000
        Top = 477.118399000000000000
        Width = 467.527581020000000000
        Height = 22.677165350000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsCli."E_ALL2"]')
        ParentFont = False
        VAlign = vaCenter
      end
      object Memo3: TfrxMemoView
        Left = 0.755817640000000000
        Top = 151.181102360000000000
        Width = 603.590941000000000000
        Height = 18.897650000000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -21
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsAvaEmpresa."NOME_ENT"]')
        ParentFont = False
        Rotation = 180
        VAlign = vaCenter
      end
      object Memo6: TfrxMemoView
        Left = 0.755817640000000000
        Top = 128.283550000000000000
        Width = 603.590661020000000000
        Height = 22.677170240000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsAvaEmpresa."E_ALL"]')
        ParentFont = False
        Rotation = 180
        VAlign = vaCenter
      end
      object Memo7: TfrxMemoView
        Left = 0.755817640000000000
        Top = 105.944929000000000000
        Width = 603.590661020000000000
        Height = 22.677165350000000000
        DisplayFormat.DecimalSeparator = ','
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -16
        Font.Name = 'Calibri'
        Font.Style = [fsBold]
        Memo.UTF8W = (
          '[frxDsAvaEmpresa."E_ALL2"]')
        ParentFont = False
        Rotation = 180
        VAlign = vaCenter
      end
    end
  end
  object QrCli: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCliCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, CodUsu, IE, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END NUM' +
        'ERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL, '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CID' +
        'ADE, '
      'CASE WHEN en.Tipo=0 THEN en.EUF     ELSE en.PUF  END UF, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pai' +
        's, '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END Log' +
        'rad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END CEP' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ', '
      
        'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", ' +
        '"RG") CAD_ESTADUAL'
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 881
    Top = 209
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCliE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrCliCNPJ_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliNOME_TIPO_DOC: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrCliTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrCliNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrCliCEP_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object QrCliCodigo: TIntegerField
      FieldName = 'Codigo'
      Required = True
    end
    object QrCliTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrCliCodUsu: TIntegerField
      FieldName = 'CodUsu'
      Required = True
    end
    object QrCliNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrCliCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrCliIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrCliRUA: TWideStringField
      FieldName = 'RUA'
      Size = 30
    end
    object QrCliCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 30
    end
    object QrCliBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 30
    end
    object QrCliCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 25
    end
    object QrCliNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrCliNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrCliPais: TWideStringField
      FieldName = 'Pais'
    end
    object QrCliENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrCliTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrCliFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrCliIE: TWideStringField
      FieldName = 'IE'
    end
    object QrCliCAD_FEDERAL: TWideStringField
      FieldName = 'CAD_FEDERAL'
      Required = True
      Size = 4
    end
    object QrCliCAD_ESTADUAL: TWideStringField
      FieldName = 'CAD_ESTADUAL'
      Required = True
      Size = 4
    end
    object QrCliIE_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrCliE_ALL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL2'
      Size = 256
      Calculated = True
    end
    object QrCliLograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrCliNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrCliCEP: TFloatField
      FieldName = 'CEP'
    end
    object QrCliSite: TWideStringField
      FieldName = 'Site'
      Size = 100
    end
    object QrCliUF: TFloatField
      FieldName = 'UF'
    end
  end
  object frxDsCartAval: TfrxDBDataset
    UserName = 'frxDsCartAval'
    CloseDataSource = False
    FieldAliases.Strings = (
      'NOMECAD=NOMECAD'
      'NOMECAD2=NOMECAD2'
      'Codigo=Codigo'
      'Enti=Enti'
      'Status=Status'
      'Lk=Lk'
      'DataCad=DataCad'
      'DataAlt=DataAlt'
      'UserCad=UserCad'
      'UserAlt=UserAlt'
      'AlterWeb=AlterWeb'
      'Ativo=Ativo'
      'Texto=Texto'
      'Foto1=Foto1'
      'Foto2=Foto2'
      'Foto3=Foto3'
      'Foto4=Foto4'
      'NOMEENTI=NOMEENTI'
      'Motivo=Motivo')
    DataSet = QrCartAval
    BCDToCurrency = False
    Left = 845
    Top = 397
  end
  object IdHTTP1: TIdHTTP
    AllowCookies = True
    ProxyParams.BasicAuthentication = False
    ProxyParams.ProxyPort = 0
    Request.ContentLength = -1
    Request.ContentRangeEnd = -1
    Request.ContentRangeStart = -1
    Request.ContentRangeInstanceLength = -1
    Request.Accept = 'text/html, */*'
    Request.BasicAuthentication = False
    Request.UserAgent = 'Mozilla/3.0 (compatible; Indy Library)'
    Request.Ranges.Units = 'bytes'
    Request.Ranges = <>
    HTTPOptions = [hoForceEncodeParams]
    Left = 257
    Top = 9
  end
  object PMCarta: TPopupMenu
    Left = 873
    Top = 397
    object Inclui1: TMenuItem
      Caption = '&Inclui'
      OnClick = Inclui1Click
    end
    object Altera1: TMenuItem
      Caption = '&Altera'
      OnClick = Altera1Click
    end
  end
  object PMMotivo: TPopupMenu
    Left = 901
    Top = 397
    object MenuItem1: TMenuItem
      Caption = '&Inclui'
      OnClick = MenuItem1Click
    end
    object MenuItem2: TMenuItem
      Caption = '&Exclui'
      OnClick = MenuItem2Click
    end
  end
  object DsCartAvalS: TDataSource
    DataSet = QrCartAvalS
    Left = 840
    Top = 133
  end
  object QrCartAvalS: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrCartAvalSCalcFields
    SQL.Strings = (
      'SELECT mvs.Codigo, mvs.Controle, '
      'mvs.Motivo, mvs.DiaHora,'
      'stv.Nome NOMEMOTIVO'
      'FROM cartavals mvs'
      'LEFT JOIN motivos stv ON stv.Codigo = mvs.Motivo'
      'WHERE mvs.Codigo=:P0'
      'ORDER BY DiaHora DESC')
    Left = 812
    Top = 133
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object QrCartAvalSCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrCartAvalSControle: TIntegerField
      FieldName = 'Controle'
    end
    object QrCartAvalSMotivo: TIntegerField
      FieldName = 'Motivo'
    end
    object QrCartAvalSDiaHora: TDateTimeField
      FieldName = 'DiaHora'
    end
    object QrCartAvalSNOMEMOTIVO: TWideStringField
      FieldName = 'NOMEMOTIVO'
      Size = 30
    end
    object QrCartAvalSMOTIVO_STR: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'MOTIVO_STR'
      Size = 100
      Calculated = True
    end
  end
  object QrLoc: TmySQLQuery
    Database = Dmod.MyDB
    Left = 423
    Top = 9
    object QrLocControle: TIntegerField
      FieldName = 'Controle'
      Required = True
    end
  end
  object frxDsCli: TfrxDBDataset
    UserName = 'frxDsCli'
    CloseDataSource = False
    FieldAliases.Strings = (
      'E_ALL=E_ALL'
      'CNPJ_TXT=CNPJ_TXT'
      'NOME_TIPO_DOC=NOME_TIPO_DOC'
      'TE1_TXT=TE1_TXT'
      'FAX_TXT=FAX_TXT'
      'NUMERO_TXT=NUMERO_TXT'
      'CEP_TXT=CEP_TXT'
      'Codigo=Codigo'
      'Tipo=Tipo'
      'CodUsu=CodUsu'
      'NOME_ENT=NOME_ENT'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'RUA=RUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMEUF=NOMEUF'
      'Pais=Pais'
      'ENDEREF=ENDEREF'
      'TE1=TE1'
      'FAX=FAX'
      'IE=IE'
      'CAD_FEDERAL=CAD_FEDERAL'
      'CAD_ESTADUAL=CAD_ESTADUAL'
      'IE_TXT=IE_TXT'
      'E_ALL2=E_ALL2'
      'Lograd=Lograd'
      'NUMERO=NUMERO'
      'CEP=CEP'
      'Site=Site'
      'UF=UF')
    DataSet = QrCli
    BCDToCurrency = False
    Left = 909
    Top = 209
  end
  object QrAvaEmpresa: TmySQLQuery
    Database = Dmod.MyDB
    OnCalcFields = QrAvaEmpresaCalcFields
    SQL.Strings = (
      'SELECT en.Codigo, Tipo, CodUsu, IE, '
      
        'CASE WHEN en.Tipo=0 THEN en.RazaoSocial ELSE en.Nome     END NOM' +
        'E_ENT, '
      
        'CASE WHEN en.Tipo=0 THEN en.CNPJ        ELSE en.CPF      END CNP' +
        'J_CPF, '
      
        'CASE WHEN en.Tipo=0 THEN en.IE          ELSE en.RG       END IE_' +
        'RG, '
      
        'CASE WHEN en.Tipo=0 THEN en.ERua        ELSE en.PRua     END RUA' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.ENumero     ELSE en.PNumero  END NUM' +
        'ERO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECompl      ELSE en.PCompl   END COM' +
        'PL, '
      
        'CASE WHEN en.Tipo=0 THEN en.EBairro     ELSE en.PBairro  END BAI' +
        'RRO, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECidade     ELSE en.PCidade  END CID' +
        'ADE, '
      'CASE WHEN en.Tipo=0 THEN en.EUF     ELSE en.PUF  END UF, '
      
        'CASE WHEN en.Tipo=0 THEN lle.Nome       ELSE llp.Nome    END NOM' +
        'ELOGRAD, '
      
        'CASE WHEN en.Tipo=0 THEN ufe.Nome       ELSE ufp.Nome    END NOM' +
        'EUF, '
      
        'CASE WHEN en.Tipo=0 THEN en.EPais       ELSE en.PPais    END Pai' +
        's, '
      
        'CASE WHEN en.Tipo=0 THEN en.ELograd     ELSE en.PLograd  END Log' +
        'rad, '
      
        'CASE WHEN en.Tipo=0 THEN en.ECEP        ELSE en.PCEP     END CEP' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EEndeRef    ELSE en.PEndeRef END END' +
        'EREF, '
      
        'CASE WHEN en.Tipo=0 THEN en.ETe1        ELSE en.PTe1     END TE1' +
        ', '
      
        'CASE WHEN en.Tipo=0 THEN en.EFax        ELSE en.PFax     END FAX' +
        ','
      
        'CASE WHEN en.Tipo=0 THEN en.ESite        ELSE en.PSite     END S' +
        'ite,'
      
        'IF(en.Tipo=0, "CNPJ", "CPF") CAD_FEDERAL, IF(en.Tipo=0, "I.E.", ' +
        '"RG") CAD_ESTADUAL'
      'FROM entidades en '
      'LEFT JOIN ufs ufe ON ufe.Codigo=en.EUF'
      'LEFT JOIN ufs ufp ON ufp.Codigo=en.PUF'
      'LEFT JOIN listalograd lle ON lle.Codigo=en.ELograd'
      'LEFT JOIN listalograd llp ON llp.Codigo=en.PLograd'
      'WHERE en.Codigo=:P0')
    Left = 785
    Top = 257
    ParamData = <
      item
        DataType = ftUnknown
        Name = 'P0'
        ParamType = ptUnknown
      end>
    object StringField2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CNPJ_TXT'
      Size = 30
      Calculated = True
    end
    object StringField3: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NOME_TIPO_DOC'
      Size = 10
      Calculated = True
    end
    object QrAvaEmpresaE_ALL: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL'
      Size = 256
      Calculated = True
    end
    object QrAvaEmpresaTE1_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'TE1_TXT'
      Size = 40
      Calculated = True
    end
    object QrAvaEmpresaNUMERO_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'NUMERO_TXT'
      Size = 30
      Calculated = True
    end
    object QrAvaEmpresaFAX_TXT: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'FAX_TXT'
      Size = 40
      Calculated = True
    end
    object QrAvaEmpresaE_ALL2: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'E_ALL2'
      Size = 256
      Calculated = True
    end
    object StringField7: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'CEP_TXT'
      Calculated = True
    end
    object StringField24: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'IE_TXT'
      Size = 50
      Calculated = True
    end
    object QrAvaEmpresaCodigo: TIntegerField
      FieldName = 'Codigo'
    end
    object QrAvaEmpresaTipo: TSmallintField
      FieldName = 'Tipo'
    end
    object QrAvaEmpresaCodUsu: TIntegerField
      FieldName = 'CodUsu'
    end
    object QrAvaEmpresaIE: TWideStringField
      FieldName = 'IE'
    end
    object QrAvaEmpresaNOME_ENT: TWideStringField
      FieldName = 'NOME_ENT'
      Size = 100
    end
    object QrAvaEmpresaCNPJ_CPF: TWideStringField
      FieldName = 'CNPJ_CPF'
      Size = 18
    end
    object QrAvaEmpresaIE_RG: TWideStringField
      FieldName = 'IE_RG'
    end
    object QrAvaEmpresaRUA: TWideStringField
      FieldName = 'RUA'
      Size = 60
    end
    object QrAvaEmpresaCOMPL: TWideStringField
      FieldName = 'COMPL'
      Size = 60
    end
    object QrAvaEmpresaBAIRRO: TWideStringField
      FieldName = 'BAIRRO'
      Size = 60
    end
    object QrAvaEmpresaCIDADE: TWideStringField
      FieldName = 'CIDADE'
      Size = 60
    end
    object QrAvaEmpresaNOMELOGRAD: TWideStringField
      FieldName = 'NOMELOGRAD'
      Size = 10
    end
    object QrAvaEmpresaNOMEUF: TWideStringField
      FieldName = 'NOMEUF'
      Size = 2
    end
    object QrAvaEmpresaPais: TWideStringField
      FieldName = 'Pais'
      Size = 60
    end
    object QrAvaEmpresaENDEREF: TWideStringField
      FieldName = 'ENDEREF'
      Size = 100
    end
    object QrAvaEmpresaTE1: TWideStringField
      FieldName = 'TE1'
    end
    object QrAvaEmpresaFAX: TWideStringField
      FieldName = 'FAX'
    end
    object QrAvaEmpresaCAD_FEDERAL: TWideStringField
      FieldName = 'CAD_FEDERAL'
      Required = True
      Size = 4
    end
    object QrAvaEmpresaCAD_ESTADUAL: TWideStringField
      FieldName = 'CAD_ESTADUAL'
      Required = True
      Size = 4
    end
    object QrAvaEmpresaSite: TWideStringField
      FieldName = 'Site'
      Size = 100
    end
    object QrAvaEmpresaNUMERO: TFloatField
      FieldName = 'NUMERO'
    end
    object QrAvaEmpresaUF: TFloatField
      FieldName = 'UF'
    end
    object QrAvaEmpresaLograd: TFloatField
      FieldName = 'Lograd'
    end
    object QrAvaEmpresaCEP: TFloatField
      FieldName = 'CEP'
    end
  end
  object frxDsAvaEmpresa: TfrxDBDataset
    UserName = 'frxDsAvaEmpresa'
    CloseDataSource = False
    FieldAliases.Strings = (
      'CNPJ_TXT=CNPJ_TXT'
      'NOME_TIPO_DOC=NOME_TIPO_DOC'
      'E_ALL=E_ALL'
      'TE1_TXT=TE1_TXT'
      'NUMERO_TXT=NUMERO_TXT'
      'FAX_TXT=FAX_TXT'
      'E_ALL2=E_ALL2'
      'CEP_TXT=CEP_TXT'
      'IE_TXT=IE_TXT'
      'Codigo=Codigo'
      'Tipo=Tipo'
      'CodUsu=CodUsu'
      'IE=IE'
      'NOME_ENT=NOME_ENT'
      'CNPJ_CPF=CNPJ_CPF'
      'IE_RG=IE_RG'
      'RUA=RUA'
      'COMPL=COMPL'
      'BAIRRO=BAIRRO'
      'CIDADE=CIDADE'
      'NOMELOGRAD=NOMELOGRAD'
      'NOMEUF=NOMEUF'
      'Pais=Pais'
      'ENDEREF=ENDEREF'
      'TE1=TE1'
      'FAX=FAX'
      'CAD_FEDERAL=CAD_FEDERAL'
      'CAD_ESTADUAL=CAD_ESTADUAL'
      'Site=Site'
      'NUMERO=NUMERO'
      'UF=UF'
      'Lograd=Lograd'
      'CEP=CEP')
    DataSet = QrAvaEmpresa
    BCDToCurrency = False
    Left = 813
    Top = 257
  end
end
